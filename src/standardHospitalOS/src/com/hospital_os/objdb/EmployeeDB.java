package com.hospital_os.objdb;

import com.hospital_os.object.Active;
import com.hospital_os.object.Employee;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.ComboFix;
import com.hospital_os.utility.Gutil;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public class EmployeeDB {

    public ConnectionInf theConnectionInf;
    public Employee dbObj;
    final public String idtable = "157";/*
     * "149";
     */

    private final PersonDB personDB;

    /**
     * @param db
     * @roseuid 3F65897F0326
     */
    public EmployeeDB(ConnectionInf db) {
        theConnectionInf = db;
        personDB = new PersonDB(theConnectionInf);
        dbObj = new Employee();
        initConfig();
    }

    private boolean initConfig() {
        dbObj.table = "b_employee";
        dbObj.pk_field = "b_employee_id";
        dbObj.employee_id = "employee_login";
        dbObj.password = "employee_password";
        dbObj.employee_no = "employee_number";
        dbObj.last_login = "employee_last_login";
        dbObj.last_logout = "employee_last_logout";
        dbObj.active = "employee_active";
        dbObj.default_service_id = "b_service_point_id";
        dbObj.level_id = "f_employee_level_id";
        dbObj.rule_id = "f_employee_rule_id";
        dbObj.authentication_id = "f_employee_authentication_id";
        dbObj.warning_dx = "employee_warning_dx";
        dbObj.default_tab = "b_employee_default_tab";
        dbObj.record_date_time = "record_date_time";
        dbObj.update_date_time = "update_date_time";
        dbObj.warning_icd10 = "employee_warning_icd10";
        dbObj.t_person_id = "t_person_id";
        dbObj.provider = "provider";
        dbObj.f_provider_council_code_id = "f_provider_council_code_id";
        dbObj.f_provider_type_id = "f_provider_type_id";
        dbObj.start_date = "start_date";
        dbObj.out_date = "out_date";
        dbObj.move_from = "move_from";
        dbObj.move_to = "move_to";
        dbObj.b_visit_clinic_id = "b_visit_clinic_id";
        dbObj.is_main_doctor = "is_main_doctor";
        return true;
    }

    /**
     * @param o
     * @return int
     * @throws Exception
     * @roseuid 3F6574DE0394
     */
    public int insert(Employee o) throws Exception {
        Employee p = o;
        p.generateOID(idtable);
        p.provider = getNextProviderId();
        String sql = "INSERT INTO b_employee(\n"
                + "            b_employee_id, employee_login, employee_password, \n"
                + "            employee_number, employee_last_login, employee_last_logout, \n"
                + "            employee_active, b_service_point_id, f_employee_level_id, f_employee_rule_id, \n"
                + "            f_employee_authentication_id, b_employee_default_tab, employee_warning_dx, \n"
                + "            record_date_time, update_date_time, employee_warning_icd10, t_person_id, \n"
                + "            provider, f_provider_council_code_id, f_provider_type_id, start_date, \n"
                + "            out_date, move_from, move_to, employee_number_issue_date, b_visit_clinic_id, is_main_doctor)\n"
                + "    VALUES (?, ?, ?, \n"
                + "            ?, ?, ?, \n"
                + "            ?, ?, ?, ?, \n"
                + "            ?, ?, ?, \n"
                + "            ?, ?, ?, ?, \n"
                + "            ?, ?, ?, ?, \n"
                + "            ?, ?, ?, ?, ?, ?)";
        PreparedStatement ps = theConnectionInf.ePQuery(sql);
        int index = 1;
        ps.setString(index++, p.getObjectId());
        ps.setString(index++, p.employee_id);
        ps.setString(index++, p.password);
        ps.setString(index++, p.employee_no);
        ps.setString(index++, p.last_login);
        ps.setString(index++, p.last_logout);
        ps.setString(index++, p.active);
        ps.setString(index++, p.default_service_id);
        ps.setString(index++, p.level_id);
        ps.setString(index++, p.rule_id);
        ps.setString(index++, p.authentication_id);
        ps.setString(index++, p.default_tab);
        ps.setString(index++, p.warning_dx);
        ps.setString(index++, p.record_date_time);
        ps.setString(index++, p.update_date_time);
        ps.setString(index++, p.warning_icd10);
        ps.setString(index++, p.person.getObjectId());
        ps.setString(index++, p.provider);
        ps.setString(index++, p.f_provider_council_code_id);
        ps.setString(index++, p.f_provider_type_id.trim());
        ps.setString(index++, p.start_date);
        ps.setString(index++, p.out_date);
        ps.setString(index++, p.move_from);
        ps.setString(index++, p.move_to);
        ps.setDate(index++, p.employee_number_issue_date == null ? null : new java.sql.Date(p.employee_number_issue_date.getTime()));
        ps.setString(index++, p.b_visit_clinic_id);
        ps.setString(index++, p.is_main_doctor);
        return theConnectionInf.eUpdate(ps.toString());
    }

    public int update(Employee o) throws Exception {
        Employee p = o;
        String sql = "UPDATE b_employee\n"
                + "   SET employee_login=?, employee_password=?, employee_number=?, employee_last_login=?, \n"
                + "       employee_last_logout=?, employee_active=?, b_service_point_id=?, \n"
                + "       f_employee_level_id=?, f_employee_rule_id=?, f_employee_authentication_id=?, \n"
                + "       b_employee_default_tab=?, employee_warning_dx=?, \n"
                + "       update_date_time=?, employee_warning_icd10=?, t_person_id=?, \n"
                + "       provider=?, f_provider_council_code_id=?, f_provider_type_id=?, \n"
                + "       start_date=?, out_date=?, move_from=?, move_to=?, employee_number_issue_date=?, b_visit_clinic_id=?, is_main_doctor=?\n"
                + " WHERE b_employee_id=?";
        PreparedStatement ps = theConnectionInf.ePQuery(sql);
        int index = 1;
        ps.setString(index++, p.employee_id);
        ps.setString(index++, p.password);
        ps.setString(index++, p.employee_no);
        ps.setString(index++, p.last_login);
        ps.setString(index++, p.last_logout);
        ps.setString(index++, p.active);
        ps.setString(index++, p.default_service_id);
        ps.setString(index++, p.level_id);
        ps.setString(index++, p.rule_id);
        ps.setString(index++, p.authentication_id);
        ps.setString(index++, p.default_tab);
        ps.setString(index++, p.warning_dx);
        ps.setString(index++, p.update_date_time);
        ps.setString(index++, p.warning_icd10);
        ps.setString(index++, p.person != null
                && p.person.getObjectId() != null
                && !p.person.getObjectId().isEmpty()
                ? p.person.getObjectId() : p.t_person_id);
        ps.setString(index++, p.provider == null || p.provider.isEmpty() ? getNextProviderId() : p.provider);
        ps.setString(index++, p.f_provider_council_code_id);
        ps.setString(index++, p.f_provider_type_id.trim());
        ps.setString(index++, p.start_date);
        ps.setString(index++, p.out_date);
        ps.setString(index++, p.move_from);
        ps.setString(index++, p.move_to);
        ps.setDate(index++, p.employee_number_issue_date == null ? null : new java.sql.Date(p.employee_number_issue_date.getTime()));
        ps.setString(index++, p.b_visit_clinic_id);
        ps.setString(index++, p.is_main_doctor);
        ps.setString(index++, p.getObjectId());
        return theConnectionInf.eUpdate(ps.toString());
    }

    public int updateLogout(Employee o) throws Exception {
        Employee p = o;
        StringBuffer sql = new StringBuffer("update ").append(dbObj.table).append(" set ").
                append(dbObj.last_login).append("='").append(p.last_login).
                append("', ").append(dbObj.last_logout).append("='").append(p.last_logout).
                append("' where ").append(dbObj.pk_field).append("='").append(p.getObjectId()).append("'");
        return theConnectionInf.eUpdate(sql.toString());
    }

    public int delete(Employee o) throws Exception {
        StringBuffer sql = new StringBuffer("delete from ").append(dbObj.table).
                append(" where ").append(dbObj.pk_field).append("='").append(o.getObjectId()).append("'");
        return theConnectionInf.eUpdate(sql.toString());
    }

    public Employee selectByPK(String pk) throws Exception {
        StringBuffer sql = new StringBuffer("select b_employee.* \n"
                + ", t_person.person_firstname\n"
                + ", t_person.person_lastname\n"
                + ", case when f_patient_prefix.f_patient_prefix_id is not null then case when f_patient_prefix.f_patient_prefix_id = '000' then '' else f_patient_prefix.patient_prefix_description end else '' end as prefix\n"
                + " FROM b_employee\n"
                + "INNER JOIN t_person on t_person.t_person_id = b_employee.t_person_id\n"
                + "left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_person.f_prefix_id\n"
                + " where ").
                append(dbObj.pk_field).append(" = '").append(pk).append("'");

        Vector v = eQuery(sql.toString());
        if (v.isEmpty()) {
            return null;
        } else {
            return (Employee) v.get(0);
        }
    }

    /**
     * @deprecated @param pk
     * @return
     * @throws Exception
     */
    public ComboFix selectComboBoxByPK(String pk) throws Exception {
        StringBuffer sql = new StringBuffer("b_employee.* \n"
                + ", t_person.person_firstname\n"
                + ", t_person.person_lastname\n"
                + ", case when f_patient_prefix.f_patient_prefix_id is not null then case when f_patient_prefix.f_patient_prefix_id = '000' then '' else f_patient_prefix.patient_prefix_description end else '' end as prefix\n"
                + " FROM b_employee\n"
                + "INNER JOIN t_person on t_person.t_person_id = b_employee.t_person_id\n"
                + "left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_person.f_prefix_id\n"
                + " where ").append(dbObj.pk_field).append(" = '").append(pk).
                append("' and b_employee.").append(dbObj.active).append(" = '").append(Active.isEnable()).append("' order by t_person.person_firstname, t_person.person_lastname");

        Vector vc = veQuery(sql.toString());
        if (vc.isEmpty()) {
            return null;
        } else {
            return (ComboFix) vc.get(0);
        }
    }

    public Employee selectByUsername(String pk) throws Exception {
        StringBuffer sql = new StringBuffer("select b_employee.* \n"
                + ", t_person.person_firstname\n"
                + ", t_person.person_lastname\n"
                + ", case when f_patient_prefix.f_patient_prefix_id is not null then case when f_patient_prefix.f_patient_prefix_id = '000' then '' else f_patient_prefix.patient_prefix_description end else '' end as prefix\n"
                + " FROM b_employee\n"
                + "INNER JOIN t_person on t_person.t_person_id = b_employee.t_person_id\n"
                + "left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_person.f_prefix_id\n"
                + " where ").append(dbObj.employee_id).append(" = '").append(Gutil.CheckReservedWords(pk)).append("'").append(
                " AND ").append(dbObj.active).append("='").append(Active.isEnable()).append("' order by ").append(dbObj.employee_id);

        Vector v = eQuery(sql.toString());
        if (v.isEmpty()) {
            return null;
        } else {
            return (Employee) v.get(0);
        }
    }

    /**
     * Note ��� stock module ��㹡���Ң����Ţͧ�����ҡ ������ѡ ���
     * ����ʴ�������� �� active �� null �����ʹ�,�� 1 �����੾�з��
     * active, �� 0 �����੾�з�� inactive
     *
     * @param pk �� ���ʢͧ���ҧ
     * @param active
     * @active �� string ��� �� null �����ʹ�,�� 1 �����੾�з�� active,
     * �� 0 �����੾�з�� inactive
     * @return �� Object �ͧ Employee �������դ�Ҩ��� null
     *
     * @throws Exception
     */
    public Employee getEmployeeName(String pk, String active) throws Exception {
        StringBuffer sql = new StringBuffer("select b_employee.* \n"
                + ", t_person.person_firstname\n"
                + ", t_person.person_lastname\n"
                + ", case when f_patient_prefix.f_patient_prefix_id is not null then case when f_patient_prefix.f_patient_prefix_id = '000' then '' else f_patient_prefix.patient_prefix_description end else '' end as prefix\n"
                + " FROM b_employee\n"
                + "INNER JOIN t_person on t_person.t_person_id = b_employee.t_person_id\n"
                + "left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_person.f_prefix_id\n"
                + " where ").append(dbObj.pk_field).append(" = '").append(pk).append("'");
        if (active != null) {
            sql.append(" AND b_employee.").append(dbObj.active).append("='").append(active).append("'");
        }

        Vector v = eQuery(sql.toString());
        if (v.isEmpty()) {
            return null;
        } else {
            return (Employee) v.get(0);
        }
    }

    public Vector selectNurse(String pk) throws Exception {
        StringBuffer sql = new StringBuffer("select b_employee.* \n"
                + ", t_person.person_firstname\n"
                + ", t_person.person_lastname\n"
                + ", case when f_patient_prefix.f_patient_prefix_id is not null then case when f_patient_prefix.f_patient_prefix_id = '000' then '' else f_patient_prefix.patient_prefix_description end else '' end as prefix\n"
                + " FROM b_employee\n"
                + "INNER JOIN t_person on t_person.t_person_id = b_employee.t_person_id\n"
                + "left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_person.f_prefix_id\n"
                + " where ").
                append(dbObj.authentication_id).append(" = '").append(pk).append("' and b_employee.employee_active = '1' order by t_person.person_firstname, t_person.person_lastname");

        Vector vc = veQuery(sql.toString());
        if (vc.isEmpty()) {
            return null;
        } else {
            return vc;
        }
    }

    public Vector selectNurseAndDoctorAll() throws Exception {
        return selectNurseAndDoctorByPk("");
    }

    public Vector selectNurseAndDoctorByPk(String pk) throws Exception {
        pk = Gutil.CheckReservedWords(pk);
        StringBuffer sql = new StringBuffer("select b_employee.* \n"
                + ", t_person.person_firstname\n"
                + ", t_person.person_lastname\n"
                + ", case when f_patient_prefix.f_patient_prefix_id is not null then case when f_patient_prefix.f_patient_prefix_id = '000' then '' else f_patient_prefix.patient_prefix_description end else '' end as prefix\n"
                + " FROM b_employee\n"
                + "INNER JOIN t_person on t_person.t_person_id = b_employee.t_person_id\n"
                + "left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_person.f_prefix_id\n"
                + " where (t_person.person_firstname like '%").append(pk).append("%'").append(
                " or t_person.person_lastname like '%").append(pk).append("%' )").append(
                " and (").append(dbObj.authentication_id).append(" = '2'").append(
                " or ").append(dbObj.authentication_id).append(" = '3')").append(
                " and (b_employee.").append(dbObj.active).append(" = '1' ) order by t_person.person_firstname, t_person.person_lastname");
        return eQuery(sql.toString());
    }

    public int updatePassword(Employee o) throws Exception {
        StringBuffer sql = new StringBuffer("update ").append(dbObj.table).
                append(" set ").append(dbObj.password).append(" = '").append(o.password).
                append("' where ").append(dbObj.pk_field).append(" = '").append(o.getObjectId()).append("'");

        return theConnectionInf.eUpdate(sql.toString());

    }

    public Vector selectAll(String keyword, String active, String authen) throws Exception {
        StringBuilder sql = new StringBuilder("select b_employee.* \n"
                + ", t_person.person_firstname\n"
                + ", t_person.person_lastname\n"
                + ", case when f_patient_prefix.f_patient_prefix_id is not null then case when f_patient_prefix.f_patient_prefix_id = '000' then '' else f_patient_prefix.patient_prefix_description end else '' end as prefix\n"
                + " FROM b_employee\n"
                + "INNER JOIN t_person on t_person.t_person_id = b_employee.t_person_id\n"
                + "left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_person.f_prefix_id\n"
                + " where ");
        if (!keyword.isEmpty()) {
            keyword = Gutil.CheckReservedWords(keyword);
            if (keyword.trim().contains(" ")) {
                sql.append("(b_employee.employee_login like '%").append(keyword.substring(0, keyword.indexOf(" ")).trim()).append("%'").
                        append(" or t_person.person_lastname like '%").append(keyword.substring(keyword.indexOf(" ") + 1).trim()).append("%'").
                        append(" or t_person.person_firstname like '%").append(keyword.substring(0, keyword.indexOf(" ")).trim()).append("%'").
                        append(") and ");
            } else {
                sql.append("(b_employee.employee_login like '%").append(keyword).append("%'").
                        append(" or t_person.person_lastname like '%").append(keyword).append("%'").
                        append(" or t_person.person_firstname like '%").append(keyword).append("%'").
                        append(") and ");
            }
        }
        if (authen != null) {
            sql.append(dbObj.authentication_id).append(" = '").append(authen).append("'").append(" and ");
        }

        sql.append("b_employee.").append(dbObj.active).append(" = '").append(active).append("'").append(" order by t_person.person_firstname, t_person.person_lastname");

        return eQuery(sql.toString());
    }

    public Vector selectAllByName() throws Exception {
        StringBuilder sql = new StringBuilder("select b_employee.* \n"
                + ", t_person.person_firstname\n"
                + ", t_person.person_lastname\n"
                + ", case when f_patient_prefix.f_patient_prefix_id is not null then case when f_patient_prefix.f_patient_prefix_id = '000' then '' else f_patient_prefix.patient_prefix_description end else '' end as prefix\n"
                + " FROM b_employee\n"
                + "INNER JOIN t_person on t_person.t_person_id = b_employee.t_person_id\n"
                + "left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_person.f_prefix_id\n"
                + " order by t_person.person_firstname, t_person.person_lastname");
        return eQuery(sql.toString());
    }

    public Vector selectActive(String active) throws Exception {
        StringBuffer sql = new StringBuffer("select b_employee.* \n"
                + ", t_person.person_firstname\n"
                + ", t_person.person_lastname\n"
                + ", case when f_patient_prefix.f_patient_prefix_id is not null then case when f_patient_prefix.f_patient_prefix_id = '000' then '' else f_patient_prefix.patient_prefix_description end else '' end as prefix\n"
                + " FROM b_employee\n"
                + "INNER JOIN t_person on t_person.t_person_id = b_employee.t_person_id\n"
                + "left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_person.f_prefix_id\n"
                + " where b_employee.").
                append(dbObj.active).append(" = '").append(active).append("'");
        sql.append(" order by t_person.person_firstname, t_person.person_lastname");
        return eQuery(sql.toString());
    }

    public Vector selectIdnameEmployeeAll() throws Exception {
        StringBuffer sql = new StringBuffer("SELECT b_employee.* \n"
                + ", t_person.person_firstname\n"
                + ", t_person.person_lastname\n"
                + ", case when f_patient_prefix.f_patient_prefix_id is not null then case when f_patient_prefix.f_patient_prefix_id = '000' then '' else f_patient_prefix.patient_prefix_description end else '' end as prefix\n"
                + " FROM b_employee\n"
                + "INNER JOIN t_person on t_person.t_person_id = b_employee.t_person_id\n"
                + "left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_person.f_prefix_id\n"
                + " where b_employee.").
                append(dbObj.active).append("='").append(Active.isEnable()).
                append("' order by t_person.person_firstname, t_person.person_lastname");
        Vector v = veQuery(sql.toString());
        if (v.isEmpty()) {
            return null;
        } else {
            return v;
        }
    }

    public Vector selectEmployeeByPersonId(String personId) throws Exception {
        StringBuffer sql = new StringBuffer("SELECT b_employee.* \n"
                + ", t_person.person_firstname\n"
                + ", t_person.person_lastname\n"
                + ", case when f_patient_prefix.f_patient_prefix_id is not null then case when f_patient_prefix.f_patient_prefix_id = '000' then '' else f_patient_prefix.patient_prefix_description end else '' end as prefix\n"
                + " FROM b_employee\n"
                + "INNER JOIN t_person on t_person.t_person_id = b_employee.t_person_id\n"
                + "left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_person.f_prefix_id\n"
                + " where b_employee.t_person_id ='").append(personId).
                append("' order by t_person.person_firstname, t_person.person_lastname");
        Vector v = eQuery(sql.toString());
        if (v.isEmpty()) {
            return null;
        } else {
            return v;
        }
    }

    public Vector veQuery(String sql) throws Exception {
        ComboFix p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        while (rs.next()) {
            String personId = rs.getString(dbObj.t_person_id);
            p = new ComboFix();
            p.code = rs.getString(dbObj.pk_field);
            p.name = (rs.getString(dbObj.authentication_id).equals("99")
                    ? "ᾷ��Ἱ�� - " : "");
            p.name += personId != null && !personId.isEmpty() ? rs.getString("person_firstname")
                    : "�����ҹ��� �ѧ����к� Provider ID";
            p.name += "  ";
            p.name += personId != null && !personId.isEmpty() ? rs.getString("person_lastname")
                    : "";
            list.add(p);
        }
        rs.close();
        return list;
    }

    public Vector eQuery(String sql) throws Exception {
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());

        while (rs.next()) {
            Employee p = new Employee();
            p.setObjectId(rs.getString(dbObj.pk_field));
            p.employee_id = rs.getString(dbObj.employee_id);
            p.password = rs.getString(dbObj.password);
            p.employee_no = rs.getString(dbObj.employee_no);
            p.last_login = rs.getString(dbObj.last_login);
            p.last_logout = rs.getString(dbObj.last_logout);
            p.active = rs.getString(dbObj.active);
            p.default_service_id = rs.getString(dbObj.default_service_id);
            p.level_id = rs.getString(dbObj.level_id);
            p.rule_id = rs.getString(dbObj.rule_id);
            p.authentication_id = rs.getString(dbObj.authentication_id);
            p.warning_dx = rs.getString(dbObj.warning_dx);
            p.default_tab = rs.getString(dbObj.default_tab);
            p.record_date_time = rs.getString(dbObj.record_date_time);
            p.update_date_time = rs.getString(dbObj.update_date_time);
            p.warning_icd10 = rs.getString(dbObj.warning_icd10);
            p.t_person_id = rs.getString(dbObj.t_person_id);
            p.provider = rs.getString(dbObj.provider);
            p.f_provider_council_code_id = rs.getString(dbObj.f_provider_council_code_id);
            p.f_provider_type_id = rs.getString(dbObj.f_provider_type_id);
            p.start_date = rs.getString(dbObj.start_date);
            p.out_date = rs.getString(dbObj.out_date);
            p.move_from = rs.getString(dbObj.move_from);
            p.move_to = rs.getString(dbObj.move_to);
            p.employee_number_issue_date = rs.getDate("employee_number_issue_date");
            p.b_visit_clinic_id = rs.getString(dbObj.b_visit_clinic_id);
            p.is_main_doctor = rs.getString(dbObj.is_main_doctor);
            try {
                p.person.person_firstname = rs.getString("person_firstname");
                p.person.person_lastname = rs.getString("person_lastname");
                p.person.prefix.description = rs.getString("prefix");
            } catch (Exception ex) {
                if (p.t_person_id != null && !p.t_person_id.isEmpty()) {
                    p.person = personDB.select(p.t_person_id);
                }
            }
            list.add(p);
        }
        rs.close();
        return list;
    }

    // 3.9.1b01 ����� select ��ҡ���� authenid
    public Vector selectDoctor(String... ids) throws Exception {
        String authens = "";
        for (String id : ids) {
            authens += authens.isEmpty() ? "'" + id + "'" : ",'" + id + "'";
        }
        StringBuilder sql = new StringBuilder("select b_employee.* \n"
                + ", t_person.person_firstname\n"
                + ", t_person.person_lastname\n"
                + ", case when f_patient_prefix.f_patient_prefix_id is not null then case when f_patient_prefix.f_patient_prefix_id = '000' then '' else f_patient_prefix.patient_prefix_description end else '' end as prefix\n"
                + " FROM b_employee\n"
                + "INNER JOIN t_person on t_person.t_person_id = b_employee.t_person_id\n"
                + "left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_person.f_prefix_id\n ");
        sql.append(" where ");
        sql.append(dbObj.authentication_id);
        sql.append(" in (");
        sql.append(authens);
        sql.append(") AND b_employee.");
        sql.append(dbObj.active);
        sql.append("='");
        sql.append(Active.isEnable());
        sql.append("' order by t_person.person_firstname, t_person.person_lastname");

        return eQuery(sql.toString());
    }

    public Vector selectDrugSetOwner() throws Exception {
        StringBuffer sql = new StringBuffer("select distinct(b_employee.*), t_person.person_firstname\n"
                + ", t_person.person_lastname\n"
                + ", case when f_patient_prefix.f_patient_prefix_id is not null then case when f_patient_prefix.f_patient_prefix_id = '000' then '' else f_patient_prefix.patient_prefix_description end else '' end as prefix\n"
                + " from b_employee ").
                append(" inner join b_item_group ").
                append(" on b_item_group.item_group_staff_owner = b_employee.b_employee_id ").
                append(" INNER JOIN t_person on t_person.t_person_id = b_employee.t_person_id\n"
                        + "left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_person.f_prefix_id\n ").
                append("where b_employee.employee_active = '1' order by t_person.person_firstname, t_person.person_lastname");
        return eQuery(sql.toString());
    }

    public Vector<Employee> selectByIds(String... ids) throws Exception {
        String empIds = "";
        for (String id : ids) {
            empIds += empIds.isEmpty() ? "'" + id + "'" : ",'" + id + "'";
        }
        StringBuffer sql = new StringBuffer("select b_employee.* \n"
                + ", t_person.person_firstname\n"
                + ", t_person.person_lastname\n"
                + ", case when f_patient_prefix.f_patient_prefix_id is not null then case when f_patient_prefix.f_patient_prefix_id = '000' then '' else f_patient_prefix.patient_prefix_description end else '' end as prefix\n"
                + " FROM b_employee\n"
                + "INNER JOIN t_person on t_person.t_person_id = b_employee.t_person_id\n"
                + "left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_person.f_prefix_id\n ").
                append(" where ");
        sql.append(dbObj.pk_field).append(" in (").append(empIds).append(") ");
        sql.append("order by t_person.person_firstname, t_person.person_lastname");

        return eQuery(sql.toString());
    }

    public Vector selectAuthenAllByName(String keyword, String active, String[] authenticationIds) throws Exception {
        StringBuffer sql = new StringBuffer("select b_employee.* \n"
                + ", t_person.person_firstname\n"
                + ", t_person.person_lastname\n"
                + ", case when f_patient_prefix.f_patient_prefix_id is not null then case when f_patient_prefix.f_patient_prefix_id = '000' then '' else f_patient_prefix.patient_prefix_description end else '' end as prefix\n"
                + " FROM b_employee\n"
                + "INNER JOIN t_person on t_person.t_person_id = b_employee.t_person_id\n"
                + "left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_person.f_prefix_id\n ").
                append(" where 1=1 ");
        if (keyword != null && !keyword.isEmpty()) {
            keyword = Gutil.CheckReservedWords(keyword);
            if (keyword.trim().contains(" ")) {
                sql.append("and (b_employee.employee_login ilike '%").append(keyword.substring(0, keyword.indexOf(" ")).trim()).append("%'").
                        append(" or t_person.person_lastname ilike '%").append(keyword.substring(keyword.indexOf(" ") + 1).trim()).append("%'").
                        append(" or t_person.person_firstname ilike '%").append(keyword.substring(0, keyword.indexOf(" ")).trim()).append("%'").
                        append(")\n");
            } else {
                sql.append("and (b_employee.employee_login ilike '%").append(keyword).append("%'").
                        append(" or t_person.person_lastname ilike '%").append(keyword).append("%'").
                        append(" or t_person.person_firstname ilike '%").append(keyword).append("%'").
                        append(")\n");
            }
        }
        if (!(authenticationIds == null || authenticationIds.equals(""))) {
            String authens = "";
            for (String id : authenticationIds) {
                authens += authens.isEmpty() ? "'" + id + "'" : ",'" + id + "'";
            }
            sql.append(" and ").append(dbObj.authentication_id).append(" in (").append(authens).append(") ");
        }

        sql.append(" and ").append(dbObj.active).append(" = '").append(active).append("'\n").
                append("order by t_person.person_firstname, t_person.person_lastname");

        return eQuery(sql.toString());
    }
    //////////////////////////////////////////////////////////////////////////////

    public Vector selectAllByName(String keyword, String active) throws Exception {
        StringBuffer sql = new StringBuffer("select b_employee.* \n"
                + ", t_person.person_firstname\n"
                + ", t_person.person_lastname\n"
                + ", case when f_patient_prefix.f_patient_prefix_id is not null then case when f_patient_prefix.f_patient_prefix_id = '000' then '' else f_patient_prefix.patient_prefix_description end else '' end as prefix\n"
                + " FROM b_employee\n"
                + "INNER JOIN t_person on t_person.t_person_id = b_employee.t_person_id\n"
                + "left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_person.f_prefix_id\n ").append(
                " where \n");
        if (!keyword.isEmpty()) {
            keyword = Gutil.CheckReservedWords(keyword);
            if (keyword.trim().contains(" ")) {
                sql.append("(b_employee.employee_login like '%").append(keyword.substring(0, keyword.indexOf(" ")).trim()).append("%'").
                        append(" or t_person.person_lastname like '%").append(keyword.substring(keyword.indexOf(" ") + 1).trim()).append("%'").
                        append(" or t_person.person_firstname like '%").append(keyword.substring(0, keyword.indexOf(" ")).trim()).append("%'").
                        append(") \nand ");
            } else {
                sql.append("(b_employee.employee_login like '%").append(keyword).append("%'").
                        append(" or t_person.person_lastname like '%").append(keyword).append("%'").
                        append(" or t_person.person_firstname like '%").append(keyword).append("%'").
                        append(") \nand ");
            }
        }
        sql.append(dbObj.active).append(" = '").append(1).append("'\n").append(
                " order by t_person.person_firstname, t_person.person_lastname");
        return eQuery(sql.toString());
    }

    public String getNextProviderId() throws Exception {
        String id = "";
        String sql = "select \n"
                + "case when max(b_employee.provider) = '' \n"
                + "then max(b_site.b_visit_office_id) || '00001'\n"
                + "else to_char(to_number(max(b_employee.provider), '0000000000') + 1,'0000000000')\n"
                + "end as nextid \n"
                + "from b_employee \n"
                + "cross join b_site";
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            id = rs.getString(1);
        }
        rs.close();
        return id.trim();
    }

    public Vector<Employee> selectByClinicAndDoctorName(String clinicId, String keyword) throws Exception {
        String sql = "select b_employee.* \n"
                + ", t_person.person_firstname\n"
                + ", t_person.person_lastname\n"
                + ", case when f_patient_prefix.f_patient_prefix_id is not null then \n"
                + "case when f_patient_prefix.f_patient_prefix_id = '000' then '' \n"
                + "else f_patient_prefix.patient_prefix_description end else '' end as prefix\n"
                + " FROM b_employee\n "
                + " INNER JOIN t_person on t_person.t_person_id = b_employee.t_person_id\n"
                + "left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_person.f_prefix_id\n"
                + " where employee_active='1' \n"
                + " and f_employee_authentication_id in ('3', '99') \n";
        if (clinicId != null && !clinicId.isEmpty()) {
            sql += "and b_employee.b_visit_clinic_id = ?\n";
        }
        if (keyword != null && !keyword.isEmpty()) {
            if (keyword.trim().contains(" ")) {
                sql += "and (t_person.person_lastname ilike ? or t_person.person_firstname ilike ?)\n";
            } else {
                sql += "and (t_person.person_lastname ilike ? or t_person.person_firstname ilike ?)\n";
            }
        }
        sql += "order by t_person.person_firstname, t_person.person_lastname";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            if (clinicId != null && !clinicId.isEmpty()) {
                ePQuery.setString(index++, clinicId);
            }

            if (keyword != null && !keyword.isEmpty()) {
                if (keyword.trim().contains(" ")) {
                    ePQuery.setString(index++, "%" + keyword.substring(keyword.indexOf(" ") + 1).trim() + "%");
                    ePQuery.setString(index++, "%" + keyword.substring(0, keyword.indexOf(" ")).trim() + "%");
                } else {
                    ePQuery.setString(index++, "%" + keyword + "%");
                    ePQuery.setString(index++, "%" + keyword + "%");
                }
            }
            return eQuery(ePQuery.toString());
        }
    }
}

/*
 * HosSubject.java
 *
 * Created on 11 ����Ҥ� 2548, 10:45 �.
 */
package com.hosv3.subject;

/**
 *
 * @author administrator
 */
@SuppressWarnings("ClassWithoutLogger")
public class HosSubject {

    public DiagnosisSubject theDiagnosisSubject;
    public SystemSubject theSystemSubject;
    public BillingSubject theBillingSubject;
    public ResultSubject theResultSubject;
    public OrderSubject theOrderSubject;
    public PatientSubject thePatientSubject;
    public PrintSubject thePrintSubject;
    public SetupSubject theSetupSubject;
    public VisitSubject theVisitSubject;
    public VitalSubject theVitalSubject;
    public VPaymentSubject theVPaymentSubject;
    public BalloonSubject theBalloonSubject;
    public ItemDxSubject theItemDxSubject;
    public ICD10GroupChronicSubject theICD10GroupChronicSubject;
    public ICD10GroupSurveilSubject theICD10GroupSurveilSubject;
    public ReDxSubject theReDxSubject;
    public CurrentNotificationSubject theCurrentNotificationSubject;
    public ServiceLoaderSubject theServiceLoaderSubject;
    public ReferSubject theReferSubject;
    public AppointmentSubject theAppointmentSubject;
    public AdlAnd2QPlusSubject theAdlAnd2QPlusSubject;
    public QueueSubject theQueueSubject;

    /**
     * Creates a new instance of HosSubject
     */
    public HosSubject() {
        theBillingSubject = new BillingSubject();
        theSystemSubject = new SystemSubject();
        theDiagnosisSubject = new DiagnosisSubject();
        theResultSubject = new ResultSubject();
        theOrderSubject = new OrderSubject();
        thePatientSubject = new PatientSubject();
        thePrintSubject = new PrintSubject();
        theSetupSubject = new SetupSubject();
        theVisitSubject = new VisitSubject();
        theVitalSubject = new VitalSubject();
        theVPaymentSubject = new VPaymentSubject();
        theBalloonSubject = new BalloonSubject();
        theItemDxSubject = new ItemDxSubject();
        theICD10GroupChronicSubject = new ICD10GroupChronicSubject();
        theICD10GroupSurveilSubject = new ICD10GroupSurveilSubject();
        theReDxSubject = new ReDxSubject();
        theCurrentNotificationSubject = new CurrentNotificationSubject();
        theServiceLoaderSubject = new ServiceLoaderSubject();
        theReferSubject = new ReferSubject();
        theAppointmentSubject = new AppointmentSubject();
        theAdlAnd2QPlusSubject = new AdlAnd2QPlusSubject();
        theQueueSubject = new QueueSubject();
    }

    public void initSubject() {
        theBillingSubject.removeAttach();
        theDiagnosisSubject.removeAttach();
        theResultSubject.removeAttach();
        thePatientSubject.removeAttach();
        thePrintSubject.removeAttach();
        theVitalSubject.removeAttach();
        theVPaymentSubject.removeAttach();
        theVisitSubject.removeAttach();
        theOrderSubject.removeAttach();
        theResultSubject.removeAttach();
        theItemDxSubject.removeAttach();
        theICD10GroupChronicSubject.removeAttach();
        theICD10GroupSurveilSubject.removeAttach();
        theReDxSubject.removeAttach();
        theCurrentNotificationSubject.removeAllObservers();
        theServiceLoaderSubject.removeAttach();
        theReferSubject.removeAttach();
        theAppointmentSubject.removeAttach();
        theAdlAnd2QPlusSubject.removeAttach();
        theQueueSubject.removeAttach();
    }
}

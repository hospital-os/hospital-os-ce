/*
 * PrintDrugRx2.java
 *
 * Created on 1 �á�Ҥ� 2551, 14:18 �.
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package com.hosv3.object.printobject;

import com.printing.object.DrugRx.PrintDrugRx;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Pu
 */
public class PrintDrugRx2 extends PrintDrugRx {

    public String pprint_date = "print_date";
    public String pprint_time = "print_time";
    private final String drugAllergy = "drug_allergy";
    private final String drugAllergyDetail = "drug_allergy_detail";
    private final String surveillance_drug_allergy = "surveillance_drug_allergy";
    private final String suspected_drug_allergy = "suspected_drug_allergy";
    public Map PrintDrugRx2;

    /**
     * Creates a new instance of PrintDrugRx2
     */
    public PrintDrugRx2() {
        PrintDrugRx2 = new HashMap();
    }

    public void setPrintDate(String name) {
        setMap(pprint_date, name);
    }

    public void setPrintTime(String name) {
        setMap(pprint_time, name);
    }

    @Override
    public void setTime(String name) {
        setMap(ptime, name);
    }

    @Override
    public void setStartDateCard(String name) {
        setMap(pstartdatecard, name);
    }

    @Override
    public void setCardID(String name) {
        setMap(pcardid, name);
    }

    @Override
    public void setExpireDateCard(String name) {
        setMap(pexpiredatecard, name);
    }

    @Override
    public void setHospital(String name) {
        setMap(phospital, name);
    }

    @Override
    public void setName(String name) {
        setMap(pname, name);
    }

    @Override
    public void setAge(String name) {
        setMap(page, name);
    }

    @Override
    public void setHn(String name) {
        setMap(phn, name);
    }

    @Override
    public void setVn(String name) {
        setMap(pvn, name);
    }

    @Override
    public void setDateVisit(String name) {
        setMap(pdateVisit, name);
    }

    @Override
    public void setPatientType(String name) {
        setMap(ppatientType, name);
    }

    @Override
    public void setDx(String name) {
        setMap(pdx, name);
    }

    @Override
    public void setDoctor(String name) {
        setMap(pdoctor, name);
    }

    @Override
    public void setPlan(String name) {
        setMap(pplan, name);
    }

    @Override
    public void setAddress(String name) {
        setMap(pAddress, name);
    }

    @Override
    public void setHeight(String name) {
        setMap(pheight, name);
    }

    @Override
    public void setWeight(String name) {
        setMap(pweight, name);
    }

    /**
     * ʶҹ��Һ����ѡ
     *
     * @param name �繢����ŷ���ͧ��è���� map �Ѻ parameter ����˹�� xml
     * @author padungrat(tong) @date 15/03/49,14:42
     */
    @Override
    public void setMainHospital(String name) {
        setMap(pMainHospital, name);
    }

    /**
     * ʶҹ��Һ���ͧ
     *
     * @param name �繢����ŷ���ͧ��è���� map �Ѻ parameter ����˹�� xml
     * @author padungrat(tong) @date 15/03/49,14:42
     */
    @Override
    public void setSubHospital(String name) {
        setMap(pSubHospital, name);
    }

    /**
     * �Ţ�ѵû�ЪҪ�
     *
     * @param name �繢����ŷ���ͧ��è���� map �Ѻ parameter ����˹�� xml
     * @author padungrat(tong) @date 15/03/49,14:42
     */
    @Override
    public void setPid(String name) {
        setMap(pPid, name);
    }

    /**
     * �����������������
     *
     * @param name �繢����ŷ���ͧ��è���� map �Ѻ parameter ����˹�� xml
     * @author padungrat(tong) @date 15/03/49,14:42
     */
    @Override
    public void setTotalPrice(String name) {
        setMap(total_price, name);
    }

    /**
     * �Ţ�ѵû�ЪҢ�����¡�����е��
     *
     * @param name �繢����ŷ���ͧ��è���� map �Ѻ parameter ����˹�� xml
     * @author padungrat(tong) @date 15/03/49,14:42
     */
    @Override
    public void setArrayPid(String name) {
        int size = aPid.length;
        if (name.trim().length() == 13) {
            for (int i = 0; i < size; i++) {
                setMap(aPid[i], String.valueOf(name.charAt(i)));
            }
        } else {
            for (int i = 0; i < size; i++) {
                setMap(aPid[i], "");
            }
        }
    }

    /**
     * �Ţ�ѵû�ЪҢ�����¡�����е��
     *
     * @param name �繢����ŷ���ͧ��è���� map �Ѻ parameter ����˹�� xml
     * @author pongtorn(henbe) @date 15/05/49,14:42
     */
    @Override
    public void setPersonId(String name) {
        setMap(p_person_id, name);
    }

    @Override
    public void setDiagIcd10(String name) {
        setMap(p_diag_icd10, name);
    }

    @Override
    public void setDxNote(String name) {
        setMap(p_dx_note, name);
    }

    /**
     * Dx ������
     *
     * @param name �繢����ŷ���ͧ��è���� map �Ѻ parameter ����˹�� xml
     * @author sumo @date 14/08/2549
     */
    @Override
    public void setDiseaseThai(String name) {
        setMap(p_disease_th, name);
    }

    @Override
    public Map getData() {
        return PrintDrugRx2;
    }

    @Override
    public void setMap(String Param, String Data) {
        PrintDrugRx2.put(Param, Data);
    }

    public void setDrugAllergy(String name) {
        setMap(drugAllergy, name);
    }

    public void setDrugAllergyDetail(String name) {
        setMap(drugAllergyDetail, name);
    }

    public void setSurveillanceDrugAllergy(String string) {
        setMap(surveillance_drug_allergy, string);
    }

    public void setSuspectedDrugAllergy(String string) {
        setMap(suspected_drug_allergy, string);
    }
}

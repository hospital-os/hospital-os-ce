package com.hosv3.objdb;

import com.hospital_os.objdb.BillingInvoiceSubgroupDB;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.Gutil;

public class BillingInvoiceSubgroup2DB extends BillingInvoiceSubgroupDB {

    public BillingInvoiceSubgroup2DB(ConnectionInf db) {
        super(db);
    }

    //////////////////////////////////////////////////////////////////////////////
    public int updateBbyBi(String bid, String biid) throws Exception {
        String sql = "update " + dbObj.table + " set ";
        String field = dbObj.billing_id + "='" + bid
                + "' where " + dbObj.billing_invoice_id + "='" + biid + "'";
        sql = Gutil.convertSQLToMySQL(sql + field, theConnectionInf.gettypeDatabase());
        return theConnectionInf.eUpdate(sql);
    }
    //////////////////////////////////////////////////////////////////////////////

    public int updateAbyBi(String active, String biid) throws Exception {
        String sql = "update " + dbObj.table + " set ";
        String field = dbObj.active + "='" + active
                + "' where " + dbObj.billing_invoice_id + "='" + biid + "'";
        sql = Gutil.convertSQLToMySQL(sql + field, theConnectionInf.gettypeDatabase());
        return theConnectionInf.eUpdate(sql);
    }
    //////////////////////////////////////////////////////////////////////////////

    public int updateBbyB(String bid, String billing_id) throws Exception {
        String sql = "update " + dbObj.table + " set ";
        String field = dbObj.billing_id + "='" + bid
                + "' where " + dbObj.billing_id + "='" + billing_id + "'";
        sql = Gutil.convertSQLToMySQL(sql + field, theConnectionInf.gettypeDatabase());
        return theConnectionInf.eUpdate(sql);
    }
    //////////////////////////////////////////////////////////////////////////////

    public int deletebyBi(String biid) throws Exception {
        String sql = "delete from " + dbObj.table
                + " where " + dbObj.billing_invoice_id + "='" + biid + "'";
        sql = Gutil.convertSQLToMySQL(sql, theConnectionInf.gettypeDatabase());
        return theConnectionInf.eUpdate(sql);
    }
    //////////////////////////////////////////////////////////////////////////////
}

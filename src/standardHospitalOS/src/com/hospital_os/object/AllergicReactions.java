/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
public class AllergicReactions extends Persistent {

    public String t_patient_id = "";
    public String f_allergic_reactions_type_id = "";
    public String allergic_reactions_list = "";
    public String allergic_reactions_check = "";
    public String allergic_reactions_checker = "";
    public String allergic_reactions_check_detail = "";
    public String user_record = "";
    public String record_date_time = "";
    public String user_modify = "";
    public String modify_date_time = "";
    public String active = "";
}

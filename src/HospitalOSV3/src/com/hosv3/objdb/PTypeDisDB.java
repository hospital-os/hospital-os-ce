/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.hosv3.objdb;

import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.ComboFix;
import java.sql.ResultSet;
import java.util.Vector;

/**
 *
 * @author Khanate
 */
public class PTypeDisDB {
    public ConnectionInf theConnectionInf;
    private String id = "f_ptypedis_id";
    private String des = "description";

    public PTypeDisDB(ConnectionInf db) {
        theConnectionInf=db;

    }
    
     public Vector selectAll() throws Exception
    {   
        Vector vc = new Vector();
        String sql ="select * from f_ptypedis order by f_ptypedis_id";
        vc = veQuery(sql);
        
        if(vc.isEmpty())
            return null;
        else
            return vc;
    }
    
    public Vector veQuery(String sql) throws Exception
    {
        ComboFix p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while(rs.next())
        {
            p = new ComboFix();
            p.code = rs.getString(id);
            p.name = rs.getString(des);
            list.add(p);
        }
        rs.close();
        return list;
    }
    
    
}

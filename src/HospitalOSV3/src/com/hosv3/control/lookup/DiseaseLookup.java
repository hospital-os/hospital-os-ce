/*
 * PlanLookup.java
 *
 * Created on 27 �á�Ҥ� 2548, 15:22 �.
 */
package com.hosv3.control.lookup;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.LookupControlInf;
import com.hosv3.control.LookupControl;

/**
 *
 * @author kingland
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class DiseaseLookup implements LookupControlInf {

    private LookupControl theLookup;

    /**
     * Creates a new instance of PlanLookup
     */
    public DiseaseLookup(LookupControl lookup) {
        theLookup = lookup;
    }

    @Override
    public java.util.Vector listData(String str) {
        /*
         * Vector vPlan; if(str==null || str.equals("") || str.equals("%"))
         * vPlan = theLookup.listDisease(); else vPlan = theLookup.listDisease(str);
         */
        return theLookup.listDisease(str);
    }

    public CommonInf readData(String str) {
        return theLookup.readDiseaseById(str);
    }

    @Override
    public CommonInf readHosData(String pk) {
        return theLookup.readDiseaseById(pk);
    }
}

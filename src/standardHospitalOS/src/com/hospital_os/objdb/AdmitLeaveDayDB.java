/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.AdmitLeaveDay;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.Gutil;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class AdmitLeaveDayDB {

    public ConnectionInf theConnectionInf;
    public AdmitLeaveDay dbObj;
    final public String idtable = "500";

    public AdmitLeaveDayDB(ConnectionInf db) {
        theConnectionInf = db;
        dbObj = new AdmitLeaveDay();
    }

    public int insert(AdmitLeaveDay p) throws Exception {
        p.generateOID(idtable);
        StringBuilder sql = new StringBuilder("insert into t_admit_leave_day (");
        sql.append("t_admit_leave_day_id, ");
        sql.append("t_visit_id, ");
        sql.append("leave_seq, ");
        sql.append("date_out, ");
        sql.append("time_out, ");
        sql.append("comeback, ");
        sql.append("date_in, ");
        sql.append("time_in, ");
        sql.append("leave_cause, ");
        sql.append("doctor_approve, ");
        sql.append("staff_record, ");
        sql.append("record_datetime, ");
        sql.append("staff_modify, ");
        sql.append("modify_datetime, ");
        sql.append("active ");
        sql.append(") values (");
        sql.append("'").append(p.getObjectId()).append("'");
        sql.append(",'").append(p.t_visit_id).append("'");
        sql.append(",").append(p.leave_seq);
        sql.append(",'").append(p.date_out).append("'");
        sql.append(",'").append(p.time_out).append("'");
        sql.append(",'").append(p.comeback).append("'");
        sql.append(",'").append(p.date_in).append("'");
        sql.append(",'").append(p.time_in).append("'");
        sql.append(",'").append(Gutil.CheckReservedWords(p.leave_cause)).append("'");
        sql.append(",'").append(p.doctor_approve).append("'");
        sql.append(",'").append(p.staff_record).append("'");
        sql.append(",'").append(p.record_datetime).append("'");
        sql.append(",'").append(p.staff_modify).append("'");
        sql.append(",'").append(p.modify_datetime).append("'");
        sql.append(",'").append(p.active).append("'");
        sql.append(")");
        return theConnectionInf.eUpdate(sql.toString());
    }

    public int update(AdmitLeaveDay p) throws Exception {
        StringBuilder sql = new StringBuilder("update t_admit_leave_day set ");
//        sql.append("t_visit_id = '").append(p.t_visit_id).append("', ");
//        sql.append("leave_seq = ").append(p.leave_seq).append(", ");
        sql.append("date_out = '").append(p.date_out).append("', ");
        sql.append("time_out = '").append(p.time_out).append("', ");
        sql.append("comeback = '").append(p.comeback).append("', ");
        sql.append("date_in = '").append(p.date_in).append("', ");
        sql.append("time_in = '").append(p.time_in).append("', ");
        sql.append("leave_cause = '").append(Gutil.CheckReservedWords(p.leave_cause)).append("', ");
        sql.append("doctor_approve = '").append(p.doctor_approve).append("', ");
//        sql.append("staff_record = '").append(p.staff_record).append("', ");
//        sql.append("record_datetime = '").append(p.record_datetime).append("', ");
        sql.append("staff_modify = '").append(p.staff_modify).append("', ");
        sql.append("modify_datetime = '").append(p.modify_datetime).append("' ");
//        sql.append("complete  = '").append(p.complete).append("', ");
//        sql.append("active  = '").append(p.active).append("' ");
        sql.append("where t_admit_leave_day_id = '").append(p.getObjectId()).append("'");
        return theConnectionInf.eUpdate(sql.toString());
    }

    public AdmitLeaveDay findById(String id) throws Exception {
        String sql = "select * from t_admit_leave_day where t_admit_leave_day_id = '" + id + "'";
        List<AdmitLeaveDay> alds = this.eQuery(sql);
        return alds.isEmpty() ? null : alds.get(0);
    }

    public AdmitLeaveDay findLastestByVisitId(String visitId) throws Exception {
        String sql = "select * from t_admit_leave_day where t_visit_id = '%s' and active = '1' and leave_seq = (select max(leave_seq) from t_admit_leave_day where t_visit_id = '%s')";
        List<AdmitLeaveDay> list = this.eQuery(String.format(sql, visitId, visitId));

        return list.isEmpty() ? null : list.get(0);
    }

    public List<AdmitLeaveDay> listByVisitId(String visitId) throws Exception {
        String sql = "select * from t_admit_leave_day where t_visit_id = '" + visitId + "'";
        return this.eQuery(sql);
    }

    public List<AdmitLeaveDay> listByVisitIdAndIntervalDateOut(String visitId, String startDate, String endDate) throws Exception {
        String sql = "select * from t_admit_leave_day where t_visit_id = '%s' and date_out >= '%s' and date_out <= '%s'";
        sql = String.format(sql, visitId, startDate, endDate);
        return this.eQuery(sql);
    }

    public List<AdmitLeaveDay> listByVisitIdAndIntervalDateIn(String visitId, String startDate, String endDate) throws Exception {
        String sql = "select * from t_admit_leave_day where t_visit_id = '%s' and date_in >= '%s' and date_in <= '%s'";
        sql = String.format(sql, visitId, startDate, endDate);
        return this.eQuery(sql);
    }

    public int getMaxSeqByVisitId(String visitId) throws Exception {
        String sql = "select max(leave_seq) from t_admit_leave_day where t_visit_id = '" + visitId + "'";
        ResultSet rs = theConnectionInf.eQuery(sql);
        int max = 0;
        while (rs.next()) {
            max = rs.getInt(1);
        }
        rs.close();
        return max;
    }

    public int countAdmitDay(String visitId) throws Exception {
        String sql = "select "
                + "sum(date_part('day',(cast(t_visit.visit_staff_doctor_discharge_date_time as timestamp)  "
                + "- cast(t_visit.visit_begin_admit_date_time as timestamp))) "
                + "-date_part('day',(cast(t_admit_leave_day.date_in||','||t_admit_leave_day.time_in||':00' as timestamp)  "
                + "- cast(t_admit_leave_day.date_out||','||t_admit_leave_day.time_out||':00' as timestamp)))  "
                + "+(case when (date_part('hour',(cast(t_visit.visit_staff_doctor_discharge_date_time as timestamp)  "
                + "- cast(t_visit.visit_begin_admit_date_time as timestamp)))) >= 6  "
                + "then 1 "
                + " else 0 "
                + "end)) as totalDay "
                + "from t_admit_leave_day  "
                + "inner join t_visit on t_visit.t_visit_id = t_admit_leave_day.t_visit_id "
                + "where t_visit.t_visit_id = '" + visitId + "' "
                + "and t_visit.f_visit_type_id = '1' "
                + "and t_visit.visit_doctor_discharge_status = '1' and t_admit_leave_day.comeback = '1'";
        ResultSet rs = theConnectionInf.eQuery(sql);
        int max = 0;
        while (rs.next()) {
            max = rs.getInt(1);
        }
        rs.close();
        return max;
    }

    private List<AdmitLeaveDay> eQuery(String sql) throws Exception {
        AdmitLeaveDay p;
        List<AdmitLeaveDay> list = new ArrayList<AdmitLeaveDay>();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            p = new AdmitLeaveDay();
            p.setObjectId(rs.getString("t_admit_leave_day_id"));
            p.t_visit_id = rs.getString("t_visit_id");
            p.leave_seq = rs.getInt("leave_seq");
            p.date_out = rs.getString("date_out");
            p.time_out = rs.getString("time_out");
            p.comeback = rs.getString("comeback");
            p.date_in = rs.getString("date_in");
            p.time_in = rs.getString("time_in");
            p.leave_cause = rs.getString("leave_cause");
            p.doctor_approve = rs.getString("doctor_approve");
            p.staff_record = rs.getString("staff_record");
            p.record_datetime = rs.getString("record_datetime");
            p.staff_modify = rs.getString("staff_modify");
            p.modify_datetime = rs.getString("modify_datetime");
            p.active = rs.getString("active");
            list.add(p);
        }
        rs.close();
        return list;
    }

    public List<Object[]> listAdmitLeaveDayByVisitId(String visitId) throws Exception {
        String sql = "select \n"
                + "t_admit_leave_day.t_admit_leave_day_id as id\n"
                + ",t_admit_leave_day.date_out || ',' || t_admit_leave_day.time_out as datetime_out\n"
                + ",case when t_admit_leave_day.date_in is null or t_admit_leave_day.date_in = '' then '' \n"
                + "else t_admit_leave_day.date_in || (case when t_admit_leave_day.time_in is null or t_admit_leave_day.time_in = '' then '' \n"
                + "else ',' || t_admit_leave_day.time_in end) end as datetime_in\n"
                + ",case when f_patient_prefix.f_patient_prefix_id is not null then \n"
                + "case when f_patient_prefix.f_patient_prefix_id = '000' then '' \n"
                + "else f_patient_prefix.patient_prefix_description end else '' end\n"
                + "|| t_person.person_firstname || ' ' || t_person.person_lastname as dc \n"
                + "from t_admit_leave_day \n"
                + "inner join b_employee on b_employee.b_employee_id = t_admit_leave_day.doctor_approve \n"
                + "inner join t_person on t_person.t_person_id = b_employee.t_person_id \n"
                + "left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_person.f_prefix_id \n"
                + "where t_admit_leave_day.t_visit_id = '%s' and t_admit_leave_day.active = '1' \n"
                + "order by t_admit_leave_day.leave_seq desc";
        sql = String.format(sql, visitId);
        ResultSet rs = theConnectionInf.eQuery(sql);
        List<Object[]> list = new ArrayList<Object[]>();
        while (rs.next()) {
            Object[] objects = new Object[4];
            objects[0] = rs.getString("id");
            objects[1] = rs.getString("datetime_out");
            objects[2] = rs.getString("datetime_in");
            objects[3] = rs.getString("dc");
            list.add(objects);
        }
        rs.close();
        return list;
    }

    public int deleteAdmitLeaveDayById(String id) throws Exception {
        String sql = "update t_admit_leave_day set active = '0' where t_admit_leave_day_id = '%s'";
        return theConnectionInf.eUpdate(String.format(sql, id));
    }
}

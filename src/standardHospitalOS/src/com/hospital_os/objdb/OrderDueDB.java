/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.OrderDue;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.Gutil;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class OrderDueDB {

    private final ConnectionInf theConnectionInf;
    private final String idtable = "989";

    public OrderDueDB(ConnectionInf theConnectionInf) {
        this.theConnectionInf = theConnectionInf;
    }

    public int insert(OrderDue o) throws Exception {
        String sql = "INSERT INTO t_order_due (t_order_due_id, t_order_id, "
                + "map_due_type, b_due_type_detail_id, evaluate_detail, user_record_id, record_date_time) "
                + "VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s')";
        return theConnectionInf.eUpdate(String.format(sql,
                o.getGenID(idtable),
                o.t_order_id,
                o.map_due_type,
                o.b_due_type_detail_id,
                Gutil.CheckReservedWords(o.evaluate_detail),
                o.user_record_id,
                o.record_date_time));
    }

    public int deleteAllByOrderId(String orderId) throws Exception {
        String sql = "DELETE FROM t_order_due "
                + "WHERE t_order_id = '%s'";
        return theConnectionInf.eUpdate(String.format(sql, orderId));
    }

    public List<OrderDue> listByOrderId(String id) throws Exception {
        String sql = "select t_order_due.*, b_due_type_detail.description from t_order_due "
                + "left join b_due_type_detail on b_due_type_detail.b_due_type_detail_id = t_order_due.b_due_type_detail_id and b_due_type_detail.active = '1' "
                + "where t_order_id = '%s'";
        List<OrderDue> list = eQuery(String.format(sql, id));
        return list;
    }

    public List<OrderDue> eQuery(String sql) throws Exception {
        List<OrderDue> list = new ArrayList<OrderDue>();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        while (rs.next()) {
            OrderDue p = new OrderDue();
            p.setObjectId(rs.getString("t_order_due_id"));
            p.t_order_id = rs.getString("t_order_id");
            p.map_due_type = rs.getString("map_due_type");
            p.b_due_type_detail_id = rs.getString("b_due_type_detail_id");
            p.evaluate_detail = rs.getString("evaluate_detail");
            p.user_record_id = rs.getString("user_record_id");
            p.record_date_time = rs.getString("record_date_time");
            try {
                p.due_type_detail_desc = rs.getString("description");
            } catch (Exception ex) {
            }
            list.add(p);
        }
        rs.close();
        return list;
    }
}

package com.hosv3.usecase.transaction;

/**
 *
 * @author somprasong@hospital-os.com
 */
public interface ManageQueueResp {

    public void notifyUpdateQueue(String str, int status);
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.control;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.usecase.connection.LookupControlInf;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hospital_os.utility.ComboFix;
import com.hospital_os.utility.Gutil;
import com.hosv3.utility.ConnectionDBMgr;
import com.hosv3.utility.ResourceBundle;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author henbe
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class MapCon implements LookupControlInf {

    private static final Logger LOG = Logger.getLogger(BillingControl.class.getName());
    public static String[] LOOK_PERSON = new String[]{"",
        "select t_health_family_id as id"
        + " ,case when length(patient_birthday)>10 "
        + " then patient_name||' '|| patient_last_name || ' ' || sex_description || ' '  ||to_number(to_char(current_date,'yyyy'),'9999')- to_number(substr(patient_birthday,1,4),'9999')+543 || ' ��, �Ţ�ѵ� '|| patient_pid"
        + " else  patient_name||' '|| patient_last_name || ' ' || sex_description || ', �Ţ�ѵ� '|| patient_pid end as des"
        + " ,case when length(patient_birthday)>10  "
        + " then  patient_name||' '|| patient_last_name || ' ' || sex_description || ' '  ||to_number(to_char(current_date,'yyyy'),'9999')- to_number(substr(patient_birthday,1,4),'9999')+543 || ' ��, �Ţ�ѵ� '|| patient_pid"
        + " else  patient_name||' '|| patient_last_name || ' ' || sex_description || ', �Ţ�ѵ� '|| patient_pid end as text"
        + " from t_health_family inner join f_sex on t_health_family.f_sex_id = f_sex.f_sex_id ", "", "", "", "0"
    };
    public static String[] LOOK_ITEMCATEGORY = new String[]{"",
        "select b_item_subgroup_id as id"
        + " ,item_subgroup_description  as des"
        + " ,item_subgroup_description  as text"
        + " from b_item_subgroup ", "", "", "", "1"
    };
    protected UpdateStatus theUS;
    protected ConnectionInf theConnectionInf;
    protected String sqlmap;
    protected String sqllookup;
    protected String sqlupdate;
    protected String sqldelete;
    protected String sqlselect_map;
    protected boolean empty_search = true;

    public MapCon(String[] str, UpdateStatus us, ConnectionInf con) {
        sqlmap = str[0];
        sqllookup = str[1];
        if (str.length > 2) {
            sqlupdate = str[2];
        }
        if (str.length > 3) {
            sqldelete = str[3];
        }
        if (str.length > 4) {
            sqlselect_map = str[4];
        }
        if (str.length > 5) {
            empty_search = str[5].equals("1");
        }
        theUS = us;
        theConnectionInf = con;
    }

    public int deleteMap(String[] map) {
        if (!theUS.confirmBox(ResourceBundle.getBundleGlobal("TEXT.CONFIRM")
                + ResourceBundle.getBundleText("com.hosv3.control.MapCon.DELETE_MAPPING"), UpdateStatus.WARNING)) {
            return -1;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            PreparedStatement ps = theConnectionInf.getConnection().prepareStatement(sqldelete);
            ps.setString(1, map[0]);
            int ret = ps.executeUpdate();
            theConnectionInf.getConnection().commit();
            if (ret == 0) {
                theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.MapCon.NO_UPDATE_MAPPING"), UpdateStatus.WARNING);
            }
            return ret;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            return -1;
        } finally {
            theConnectionInf.close();
        }
    }

    public Vector listLookup(String text) {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector vData = new Vector();
            if (text.trim().length() == 0 && !empty_search) {
                return vData;
            }
            String key[] = text.split(" ");
            if (key.length == 0) {
                key = new String[]{text};
            }
            String sql = "select * from (" + sqllookup + ") as query where text ilike '%" + key[0] + "%'";
            for (int i = 1; key.length > 1 && i < key.length; i++) {
                sql += " and text ilike '%" + key[i] + "%' ";
            }
            sql += "order by des limit 500 ";
            ResultSet rs = theConnectionInf.eQuery(sql);
            while (rs.next()) {
                String[] data = new String[]{
                    rs.getString(1),
                    rs.getString(2),
                    rs.getString(3)
                };
                vData.add(data);
            }
            theConnectionInf.getConnection().commit();
            return vData;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            return null;
        } finally {
            theConnectionInf.close();
        }
    }

    public Vector listParent(String text) {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector vData = new Vector();
            if (text.trim().length() == 0 && !empty_search) {
                return vData;
            }
            String key[] = text.split(" ");
            if (key.length == 0) {
                key = new String[]{text};
            }
            String sql = "select * from (" + sqllookup + ") as query "
                    + "inner join (select health_family_active,t_health_family_id from t_health_family) as family "
                    + "on family.t_health_family_id = query.id and family.health_family_active = '1' "
                    + "where text ilike '%" + Gutil.CheckReservedWords(key[0]) + "%'";
            for (int i = 1; key.length > 1 && i < key.length; i++) {
                sql += " and text ilike '%" + Gutil.CheckReservedWords(key[i]) + "%' ";
            }
            sql += "order by des limit 500 ";
            ResultSet rs = theConnectionInf.eQuery(sql);
            while (rs.next()) {
                String[] data = new String[]{
                    rs.getString(1),
                    rs.getString(2),
                    rs.getString(3)
                };
                vData.add(data);
            }
            theConnectionInf.getConnection().commit();
            return vData;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            return null;
        } finally {
            theConnectionInf.close();
        }
    }

    /**
     * use in Report18File Module konshow ������Ѻ����Ѻ�� resultset 4 ���
     *
     * @param text
     * @return
     */
    public Vector listLookup2(String text) {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector vData = new Vector();
            if (text.trim().length() == 0 && !empty_search) {
                return vData;
            }
            String key[] = text.split(" ");
            if (key.length == 0) {
                key = new String[]{text};
            }
            String sql = "select * from (" + sqllookup + ") as query where text ilike '%" + key[0] + "%'";
            for (int i = 1; key.length > 1 && i < key.length; i++) {
                sql += " and text ilike '%" + key[i] + "%' ";
            }
            sql += "order by des limit 500 ";
            ResultSet rs = theConnectionInf.eQuery(sql);
            while (rs.next()) {
                String[] data = new String[]{
                    rs.getString(1),
                    rs.getString(2),
                    rs.getString(3),
                    rs.getString(4)
                };
                vData.add(data);
            }
            theConnectionInf.getConnection().commit();
            return vData;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            return null;
        } finally {
            theConnectionInf.close();
        }
    }

//    /**
//     * @deprecated unused Somprasong 2013-09-03
//     * @param map
//     * @param lookup
//     * @return 
//     */
//    public int mapData(String[] map, String[] lookup) {
//
//        if (lookup == null) {
//            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.MapCon.PLEASE.SELECT.MAPPED"), UpdateStatus.WARNING);
//            return 0;
//        }
//        if (map == null) {
//            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.MapCon.PLEASE.SELECT.ITEM"), UpdateStatus.WARNING);
//            return 0;
//        }
//        if (map[3] != null) {
//            if (!theUS.confirmBox(Constant.getTextBundle("�׹�ѹ��úѹ�֡�Ѻ�����ŨѺ�������ͧ")
//                    + " " + map[1], UpdateStatus.WARNING)) {
//                return 0;
//            }
//        }
//
//        theConnectionInf.open();
//        try {
//            theConnectionInf.getConnection().setAutoCommit(false);
//            int ret = intMapData(map, lookup);
//            theConnectionInf.getConnection().commit();
//            if (ret == 0) {
//                theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.MapCon.NO_UPDATE_MAPPING"), UpdateStatus.WARNING);
//            }
//            return ret;
//        } catch (Exception ex) {
//            LOG.log(java.util.logging.Level.SEVERE, null, ex);
//            try {
//                theConnectionInf.getConnection().rollback();
//            } catch (SQLException ex1) {
//                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
//            }
//            return -1;
//        } finally {
//            theConnectionInf.close();
//        }
//    }
    // henbe comment 230210 kong �����������ǡѹ�Ѻ mapData parameter ����ҧ�ѹ�����͡��ҿѧ�ѹ����ҧ�ѹ�ͧ
    // konshow ������� mapData ����
    public int mapData(Vector mapV, String[] lookup) {
        if (lookup == null) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.MapCon.PLEASE.SELECT.MAPPED"), UpdateStatus.WARNING);
            return 0;
        }
        if (mapV == null) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.MapCon.PLEASE.SELECT.ITEM"), UpdateStatus.WARNING);
            return 0;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            int ret = 0;
            for (int i = 0; i < mapV.size(); i++) {
                String[] map = (String[]) mapV.get(i);
                if (map != null && map[3] != null) {
                    if (!theUS.confirmBox(
                            MessageFormat.format(
                            ResourceBundle.getBundleText("com.hosv3.control.MapCon.CONFIRM.REPLACE.MAPPED"), map[1]), UpdateStatus.WARNING)) {
                        continue;
                    }
                }
                ret = intMapData(map, lookup);
            }
            if (ret == 0) {
                theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.MapCon.NO_UPDATE_MAPPING"), UpdateStatus.WARNING);
            }
            return ret;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            return -1;
        } finally {
            theConnectionInf.close();
        }
    }

    public Vector listMap(String text, boolean unmap) {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String key[] = text.split(" ");
            String sql = "select * from (" + sqlmap + ")"
                    + " as query  where text3 ilike '%" + key[0] + "%'";
            for (int i = 1; key.length > 1 && i < key.length; i++) {
                sql += " and text3 ilike '%" + key[i] + "%' ";
            }
            if (unmap) {
                sql += " and text2 is null ";
            }
            sql += " order by des1 limit 500 ";
            ResultSet rs = theConnectionInf.eQuery(sql);
            Vector vData = new Vector();
            while (rs.next()) {
                String[] data = new String[]{
                    rs.getString(1),
                    rs.getString(2),
                    rs.getString(3),
                    rs.getString(4),
                    rs.getString(5),
                    rs.getString(6),
                    rs.getString(7)};
                vData.add(data);
            }
            theConnectionInf.getConnection().commit();
            return vData;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            return null;
        } finally {
            theConnectionInf.close();
        }
    }

    /**
     * ��ҹ�ҡ�ҹ�鹷ҧ select local,match from match ����繵��ҧ���� insert
     * values ����繵��ҧ��� update set where
     *
     * @param target_db
     */
    public void importMap(String[] target_db) {
        ConnectionInf target_con = new ConnectionDBMgr(target_db);
        int count = 0;
        theConnectionInf.open();
        target_con.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            int target_map_total = 0, local_map_total = 0;
            int target_map = 0, local_map = 0;
            ResultSet rs = target_con.eQuery(sqlmap);
            String[] target = new String[10];
            String[] local = new String[10];
            while (rs.next()) {
                if (target_map_total < 10) {
                    target[target_map_total] = rs.getString("id1");
                }
                target_map_total++;
                if (rs.getString("id2") != null) {
                    target_map++;
                }
            }
            rs = theConnectionInf.eQuery(sqlmap);
            while (rs.next()) {
                if (local_map_total < 10) {
                    local[local_map_total] = rs.getString("id1");
                }
                local_map_total++;
                if (rs.getString("id2") != null) {
                    local_map++;
                }
            }
            if (target_map_total == 0) {
                theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.MapCon.NOTFOUND.SELECTED.MAPPING"), UpdateStatus.WARNING);
                return;
            }
            boolean match = true;
            for (int i = 0; i < target.length; i++) {
                if (!target[i].equals(local[i])) {
                    match = false;
                    break;
                }
            }
            String confirm = MessageFormat.format(ResourceBundle.getBundleText("com.hosv3.control.MapCon.CONFIRM.REPLACE.MAPPED2"),
                    target_map_total, target_map, local_map_total, local_map);
            if (!match) {
                confirm += "\n\n"
                        + ResourceBundle.getBundleText("com.hosv3.control.MapCon.WARNING.MISSING.MAPPING");
            }
            if (!theUS.confirmBox(confirm, UpdateStatus.WARNING)) {
                return;
            }
            rs = target_con.eQuery(sqlselect_map);
            while (rs.next()) {
                count++;
                String map_id = rs.getString(1);
                String lookup_id = rs.getString(2);
                String[] map = new String[]{map_id, map_id};
                String[] lookup = new String[]{lookup_id, lookup_id};
                intMapData(map, lookup);
//                if (count % 100 == 1) {
//                }
//                if (count % 1000 == 1) {
//                }
            }
            theConnectionInf.getConnection().commit();

            theUS.setStatus(ResourceBundle.getBundleGlobal("TEXT.TRANSACTION.IMPORT")
                    + ResourceBundle.getBundleGlobal("TEXT.MAPPING")
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS")
                    + " " + count + " "
                    + ResourceBundle.getBundleGlobal("TETX.ITEMS"), UpdateStatus.COMPLETE);

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(ResourceBundle.getBundleGlobal("TEXT.TRANSACTION.IMPORT")
                    + ResourceBundle.getBundleGlobal("TEXT.MAPPING")
                    + ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
            try {
                target_con.getConnection().close();
            } catch (SQLException ex) {
                Logger.getLogger(MapCon.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    protected int intMapData(String[] map, String[] lookup) throws SQLException {
        PreparedStatement ps = theConnectionInf.getConnection().prepareStatement(sqlupdate);
        ps.setString(1, lookup[0]);
        ps.setString(2, map[0]);
        return ps.executeUpdate();
    }

    @Override
    public CommonInf readHosData(String pk) {
        Vector v = listData(pk);
        if (v.isEmpty()) {
            return null;
        }
        return (CommonInf) v.get(0);
    }

    @Override
    public Vector listData(String str) {
        if (str.length() == 0) {
            return null;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ResultSet rs = theConnectionInf.eQuery("select id,des,text from (" + sqllookup
                    + ") query where des like '%" + str + "%'");
            Vector vret = new Vector();
            while (rs.next()) {
                ComboFix c = new ComboFix(rs.getString(1), rs.getString(2));
                vret.add(c);
            }
            theConnectionInf.getConnection().commit();
            return vret;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            return null;
        } finally {
            theConnectionInf.close();
        }
    }
}

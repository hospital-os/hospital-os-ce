/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.Specimen;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class SpecimenDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "808";

    public SpecimenDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public int insert(Specimen obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO b_specimen(\n"
                    + "            b_specimen_id, code, description, active, user_record, record_datetime, user_modify, modify_datetime)\n"
                    + "    VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getGenID(tableId));
            preparedStatement.setString(2, obj.code);
            preparedStatement.setString(3, obj.description);
            preparedStatement.setString(4, obj.active);
            preparedStatement.setString(5, obj.user_record);
            preparedStatement.setString(6, obj.record_datetime);
            preparedStatement.setString(7, obj.user_modify);
            preparedStatement.setString(8, obj.modify_datetime);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(Specimen obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE b_specimen\n");
            sql.append("   SET code=?, description=?, \n");
            sql.append("       active=?, user_modify=?, modify_datetime=?\n");
            sql.append(" WHERE b_specimen_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.code);
            preparedStatement.setString(2, obj.description);
            preparedStatement.setString(3, obj.active);
            preparedStatement.setString(4, obj.user_modify);
            preparedStatement.setString(5, obj.modify_datetime);
            preparedStatement.setString(6, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(Specimen obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM b_specimen\n");
            sql.append(" WHERE b_specimen_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int inactive(Specimen obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE b_specimen\n");
            sql.append("   SET active=?, user_modify=?, modify_datetime=?\n");
            sql.append(" WHERE b_specimen_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, "0");
            preparedStatement.setString(2, obj.user_modify);
            preparedStatement.setString(3, obj.modify_datetime);
            preparedStatement.setString(4, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public Specimen select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_specimen where b_specimen_id = ? order by description";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<Specimen> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public Specimen selectByCode(String code) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_specimen where code = ? order by description";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, code);
            List<Specimen> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<Specimen> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_specimen where active = '1' order by description";
            preparedStatement = connectionInf.ePQuery(sql);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<Specimen> listByKeyword(String keyword, String active) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_specimen where active = ? and (upper(code) like upper(?) or upper(description) like upper(?)) order by description";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, active);
            preparedStatement.setString(2, "%" + (keyword == null || keyword.isEmpty() ? "" : keyword) + "%");
            preparedStatement.setString(3, "%" + (keyword == null || keyword.isEmpty() ? "" : keyword) + "%");
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<Specimen> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<Specimen> list = new ArrayList<Specimen>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Specimen obj = new Specimen();
                obj.setObjectId(rs.getString("b_specimen_id"));
                obj.code = rs.getString("code");
                obj.description = rs.getString("description");
                obj.active = rs.getString("active");
                obj.user_record = rs.getString("user_record");
                obj.record_datetime = rs.getString("record_datetime");
                obj.user_modify = rs.getString("user_modify");
                obj.modify_datetime = rs.getString("modify_datetime");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (Specimen obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }
}

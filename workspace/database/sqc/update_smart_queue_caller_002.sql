CREATE OR REPLACE FUNCTION smart_queue_clear (p_queue_name text)
RETURNS text  AS $$
DECLARE		
	response_queue json;
BEGIN
	select
		row_to_json(http((
			          method,
			           uri||'/list',
			           ARRAY[http_header('x-access-token-name',headers::json->>'x-access-token-name'),http_header('x-access-token',headers::json->>'x-access-token')],
			           content_type,
					   data.content
			        )::http_request)) into response_queue
		from b_smart_queue_api ,
			(select (json_build_object('queue-name' , p_queue_name
		,'data', json_agg(json_object('{}')))) as content) as data
		where code = 'queue';
	RETURN ((response_queue->>'content')::json)->>'message';
END;
$$ LANGUAGE plpgsql;

--select smart_queue_clear('pharmacy');

CREATE OR REPLACE FUNCTION smart_queue_delete_vn(p_vn text )
RETURNS text  AS $$
DECLARE		
	active_id text := (select _id
						from t_smart_queue_logs inner join t_visit_queue_transfer 
							on t_smart_queue_logs.t_visit_queue_transfer_id = t_visit_queue_transfer.t_visit_queue_transfer_id
						where 
							status = 'active'
							and t_visit_queue_transfer.visit_vn = p_vn);
	response_queue json := '{"content":{"status":"Data Not Found"}}'::json ;
BEGIN
					
		IF (active_id is not null) THEN
			 select smart_queue_delete(active_id) into response_queue;	
		END IF;
		RETURN ((response_queue->>'content')::json)->>'status'; 
END;
$$ LANGUAGE plpgsql;

--select smart_queue_delete_vn('06300052');

CREATE OR REPLACE FUNCTION smart_queue_despense_delete_vn(p_vn text )
RETURNS text  AS $$
DECLARE		
	active_id text := (select _id
						from t_smart_queue_despense_logs inner join t_visit_queue_despense
							on t_smart_queue_despense_logs.t_visit_queue_despense_id = t_visit_queue_despense.t_visit_queue_despense_id
						where 
							status = 'active'
							and t_visit_queue_despense.visit_queue_despense_vn = p_vn);
	response_queue json := '{"content":{"status":"Data Not Found"}}'::json ;
BEGIN
					
		IF (active_id is not null) THEN
			 select smart_queue_despense_delete(active_id) into response_queue;	
		END IF;
		RETURN ((response_queue->>'content')::json)->>'status'; 
END;
$$ LANGUAGE plpgsql;

--select smart_queue_despense_delete_vn('06200348');


CREATE OR REPLACE FUNCTION smart_queue_data (p_t_visit_queue_transfer_id text , p_status text ,p_channel text)
RETURNS VOID AS $$
DECLARE				
BEGIN
	PERFORM
		smart_queue_post((json_build_object('queue-name',"queue-name"
		-- กรณีเลขคิวเป็น vn เปลี่ยน "queue-code" เป็น vn 
		--,'queue-code',"queue-code" 
		,'queue-code',vn 
		,'queue-datetime',"queue-datetime"
		,'visit',json_build_object('hn',hn,'vn',vn,'name',name)
		,'language',language
		,'channel',channel
		,'status',status
		,'hcode',hcode))::text
		,t_visit_queue_transfer_id::text)
	from (
	select
		b_smart_queue_service_point.queue_name as "queue-name"
		,case when b_visit_queue_setup.visit_queue_setup_number is null then '-'
      		else (b_visit_queue_setup.visit_queue_setup_number||t_visit_queue_transfer.visit_queue_map_queue)
      		end as "queue-code"
		,current_timestamp as "queue-datetime"
		,t_patient.patient_hn as hn
		,t_visit.visit_vn as vn
		,case when f_patient_prefix.f_patient_prefix_id != '000'
				then f_patient_prefix.patient_prefix_description||t_patient.patient_firstname||' '||t_patient.patient_lastname
				else t_patient.patient_firstname||' '||t_patient.patient_lastname end as name
		,case when t_patient.f_patient_nation_id = '99'
			then 'th' else 'en' end as language
		,p_channel as channel
		,p_status as status
		,t_visit_queue_transfer.t_visit_queue_transfer_id 
		,b_visit_office_id as hcode
	from t_visit_queue_transfer inner join t_visit on t_visit_queue_transfer.t_visit_id = t_visit.t_visit_id
		inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
		left join f_patient_prefix on t_patient.f_patient_prefix_id = f_patient_prefix.f_patient_prefix_id
		inner join b_smart_queue_service_point on t_visit_queue_transfer.b_service_point_id = b_smart_queue_service_point.b_service_point_id
		left join t_visit_queue_map on t_visit_queue_map.t_visit_id = t_visit.t_visit_id
		left join b_visit_queue_setup on b_visit_queue_setup.b_visit_queue_setup_id = t_visit_queue_map.b_visit_queue_setup_id
		cross join b_site
	where
		t_visit_queue_transfer.t_visit_queue_transfer_id = p_t_visit_queue_transfer_id
		-- เมื่อสร้างคิวจ่ายยา แยกออกจากห้องยา ให่เพิ่ม
		-- and b_smart_queue_service_point.despense = false  
	) as q;
END;
$$ LANGUAGE plpgsql;
 
--t_visit_queue_despense
ALTER TABLE public.b_smart_queue_service_point ADD despense boolean NOT NULL DEFAULT false;

CREATE TABLE t_smart_queue_despense_logs (
	t_visit_queue_despense_id text,
	http_request json null,	
	http_response json null,
	_id text null,
	status text not null,
	error json null,
	logs_date_time timestamp without time zone NOT NULL default current_timestamp,
    CONSTRAINT t_smart_queue_despense_logs_pkey PRIMARY KEY (t_visit_queue_despense_id),
    CONSTRAINT t_smart_queue_despense_logs_id_unique UNIQUE(_id)
);


CREATE OR REPLACE FUNCTION smart_queue_despense_post(p_content text , p_t_visit_queue_despense_id text)
RETURNS json AS $$
DECLARE
	response_queue json;
	err_message text;
	err_context text;
	
BEGIN
	BEGIN	
		select row_to_json(http((
			          method,
			           uri,
			           ARRAY[http_header('x-access-token-name',headers::json->>'x-access-token-name'),http_header('x-access-token',headers::json->>'x-access-token')],
			           content_type,
					   p_content
			        )::http_request)) into response_queue
		from b_smart_queue_api
		where code = 'queue';
    exception when others then
    	GET STACKED DIAGNOSTICS err_message = MESSAGE_TEXT
							,err_context = PG_EXCEPTION_CONTEXT;
   	 	INSERT INTO t_smart_queue_despense_logs (http_request,error,status,t_visit_queue_despense_id)
            values (p_content::json,(json_build_object('MESSAGE_TEXT',err_message,'PG_EXCEPTION_CONTEXT',err_context)),'error',p_t_visit_queue_despense_id)
        ON CONFLICT (t_visit_queue_despense_id) DO
        UPDATE SET http_request = p_content::json
        			,error = (json_build_object('MESSAGE_TEXT',err_message,'PG_EXCEPTION_CONTEXT',err_context))
        			,status = 'error'
							,logs_date_time = current_timestamp;
    return null;
   END;
   INSERT INTO t_smart_queue_despense_logs (http_request,http_response,_id,status,t_visit_queue_despense_id,error)
        values (p_content::json,response_queue::json,(response_queue::json->>'content')::json->>'_id'
						,(case when response_queue->>'status' = '200' then 'active' else 'error' end)
						,p_t_visit_queue_despense_id
						,(case when response_queue->>'status' != '200' then (response_queue->>'content')::json end))
   ON CONFLICT (t_visit_queue_despense_id) DO
        UPDATE SET http_request = p_content::json
        			,http_response = response_queue::json
        			,_id = (response_queue::json->>'content')::json->>'_id'
        			,status = (case when response_queue->>'status' = '200' then 'active' else 'error' end)
							,error = (case when response_queue->>'status' != '200' then (response_queue->>'content')::json end)
							,logs_date_time = current_timestamp;
   RETURN response_queue;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION smart_queue_despense_delete( p_id text)
RETURNS json AS $$
DECLARE
	response_queue json;
	err_message text;
	err_context text;
	
BEGIN
	BEGIN	
		select row_to_json(http((
			          'DELETE',
			           uri||'/'||p_id,
			           ARRAY[http_header('x-access-token-name',headers::json->>'x-access-token-name'),http_header('x-access-token',headers::json->>'x-access-token')],
			           null,
					   null
			        )::http_request)) into response_queue
		from b_smart_queue_api
		where code = 'queue';
    exception when others then
    	GET STACKED DIAGNOSTICS err_message = MESSAGE_TEXT
							,err_context = PG_EXCEPTION_CONTEXT;
   	 	update t_smart_queue_despense_logs set error = (json_build_object('MESSAGE_TEXT',err_message,'PG_EXCEPTION_CONTEXT',err_context)), status = 'error', logs_date_time = current_timestamp where _id = p_id;
    return null;
   END;
  	IF response_queue->>'status' = '200' THEN
   		update t_smart_queue_despense_logs set http_response = response_queue,status = 'delete' ,logs_date_time = current_timestamp where _id = p_id;
    END IF;
   RETURN response_queue;
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION smart_queue_despense_manage()
RETURNS trigger AS $$
DECLARE		
	active_id	text := (select _id
							from t_smart_queue_despense_logs 
						where 
							status = 'active'
							and t_smart_queue_despense_logs.t_visit_queue_despense_id = NEW.t_visit_queue_despense_id);
BEGIN
	IF (TG_OP = 'INSERT') THEN 	
		PERFORM smart_queue_despense_data(NEW.t_visit_queue_despense_id, 'waiting',null);
		RETURN NEW;
	ELSIF (TG_OP = 'UPDATE') THEN
		active_id = (select _id
						from t_smart_queue_despense_logs 
						where 
							status = 'active'
							and t_smart_queue_despense_logs.t_visit_queue_despense_id = NEW.t_visit_queue_despense_id);
						
		IF ((OLD.assign_date_time <> NEW.assign_date_time) 
			and (active_id is not null)) THEN
			
			PERFORM smart_queue_despense_delete(active_id);	
		END IF;
	
		IF (OLD.assign_date_time <> NEW.assign_date_time) then
			PERFORM smart_queue_despense_data(NEW.t_visit_queue_despense_id, 'waiting',null);
	   	END IF;
	    RETURN NEW;
  	END IF;   
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER smart_queue_despense_manage
  AFTER INSERT OR UPDATE ON t_visit_queue_despense  
  FOR EACH ROW EXECUTE PROCEDURE smart_queue_despense_manage();

CREATE OR REPLACE FUNCTION smart_queue_despense_cancel()
RETURNS trigger AS $$
DECLARE		
	active_id text := (select _id
						from t_smart_queue_despense_logs 
						where 
							status = 'active'
							and t_smart_queue_despense_logs.t_visit_queue_despense_id = OLD.t_visit_queue_despense_id);
BEGIN
					
		IF (active_id is not null) THEN
			 PERFORM smart_queue_despense_delete(active_id);	
		END IF;
		RETURN OLD; 
END;
$$ LANGUAGE plpgsql;
 
CREATE TRIGGER smart_queue_despense_cancel
  AFTER DELETE ON t_visit_queue_despense  
  FOR EACH ROW EXECUTE PROCEDURE smart_queue_despense_cancel();


CREATE OR REPLACE FUNCTION smart_queue_despense_data (p_t_visit_queue_despense_id text , p_status text ,p_channel text)
RETURNS VOID AS $$
DECLARE				
BEGIN
	PERFORM
		smart_queue_despense_post((json_build_object('queue-name',"queue-name"
		-- กรณีเลขคิวเป็น vn เปลี่ยน "queue-code" เป็น vn 
		--,'queue-code',"queue-code" 
		,'queue-code',vn 
		,'queue-datetime',"queue-datetime"
		,'visit',json_build_object('hn',hn,'vn',vn,'name',name)
		,'language',language
		,'channel',channel
		,'status',status
		,'hcode',hcode))::text
		,t_visit_queue_despense_id::text)
	from (
	select
		b_smart_queue_service_point.queue_name as "queue-name"
		,case when b_visit_queue_setup.visit_queue_setup_number is null then '-'
      		else (b_visit_queue_setup.visit_queue_setup_number || '-' || LPAD(t_visit_queue_despense.visit_queue_number, 4, '0'))
      		end as "queue-code"
		,current_timestamp as "queue-datetime"
		,t_patient.patient_hn as hn
		,t_visit.visit_vn as vn
		,case when f_patient_prefix.f_patient_prefix_id != '000'
				then f_patient_prefix.patient_prefix_description||t_patient.patient_firstname||' '||t_patient.patient_lastname
				else t_patient.patient_firstname||' '||t_patient.patient_lastname end as name
		,case when t_patient.f_patient_nation_id = '99'
			then 'th' else 'en' end as language
		,p_channel as channel
		,p_status as status
		,t_visit_queue_despense.t_visit_queue_despense_id 
		,b_visit_office_id as hcode
	from t_visit_queue_despense inner join t_visit on t_visit_queue_despense.t_visit_id = t_visit.t_visit_id
		inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
		left join f_patient_prefix on t_patient.f_patient_prefix_id = f_patient_prefix.f_patient_prefix_id
		left join t_visit_queue_map on t_visit_queue_map.t_visit_id = t_visit.t_visit_id
		left join b_visit_queue_setup on b_visit_queue_setup.b_visit_queue_setup_id = t_visit_queue_map.b_visit_queue_setup_id
		cross join b_smart_queue_service_point
		cross join b_site
	where
		b_smart_queue_service_point.despense = true
		and t_visit_queue_despense.t_visit_queue_despense_id = p_t_visit_queue_despense_id
	) as q;
END;
$$ LANGUAGE plpgsql;
 
--visit_money_discharge_status = '1'
CREATE OR REPLACE FUNCTION smart_queue_money_discharge()
RETURNS trigger AS $$
DECLARE		
	active_id text := (select _id
						from t_smart_queue_logs inner join t_visit_queue_transfer 
							on t_smart_queue_logs.t_visit_queue_transfer_id = t_visit_queue_transfer.t_visit_queue_transfer_id
						where 
							status = 'active'
							and t_visit_queue_transfer.t_visit_id = OLD.t_visit_id);
BEGIN
					
		IF (active_id is not null) THEN
			 PERFORM smart_queue_delete(active_id);	
		END IF;
		RETURN OLD; 
END;
$$ LANGUAGE plpgsql;
 
CREATE TRIGGER smart_queue_money_discharge
  AFTER update ON t_visit  
  FOR EACH ROW 
  WHEN (NEW.visit_money_discharge_status = '1')
  EXECUTE PROCEDURE smart_queue_money_discharge();   


CREATE INDEX IF NOT EXISTS t_smart_queue_logs_logs_date_time ON t_smart_queue_logs( logs_date_time );
CREATE INDEX IF NOT EXISTS t_smart_queue_despense_logs_logs_date_time ON t_smart_queue_despense_logs( logs_date_time );

CREATE OR REPLACE FUNCTION smart_queue_delete_logs()
RETURNS trigger AS $$
DECLARE		
	set_date date := current_date - 30;
	count_logs integer := (select count(*) 
						from t_smart_queue_logs 
						where logs_date_time::date < set_date);
BEGIN
					
		IF (count_logs > 0) THEN
			 delete from t_smart_queue_logs 
			 where logs_date_time::date < set_date;	
		END IF;
		RETURN NEW; 
END;
$$ LANGUAGE plpgsql;


DROP TRIGGER IF EXISTS smart_queue_delete_logs
  ON t_smart_queue_logs;
CREATE TRIGGER smart_queue_delete_logs
  AFTER INSERT ON t_smart_queue_logs  
  FOR EACH ROW EXECUTE PROCEDURE smart_queue_delete_logs();
 
CREATE OR REPLACE FUNCTION smart_queue_delete_despense_logs()
RETURNS trigger AS $$
DECLARE		
	set_date date := current_date - 30;
	count_logs integer := (select count(*) 
						from t_smart_queue_despense_logs 
						where logs_date_time::date < set_date);
BEGIN
					
		IF (count_logs > 0) THEN
			 delete from t_smart_queue_despense_logs 
			 where logs_date_time::date < set_date;	
		END IF;
		RETURN NEW; 
END;
$$ LANGUAGE plpgsql;


DROP TRIGGER IF EXISTS smart_queue_delete_despense_logs
  ON t_smart_queue_despense_logs;
CREATE TRIGGER smart_queue_delete_despense_logs
  AFTER INSERT ON t_smart_queue_despense_logs  
  FOR EACH ROW EXECUTE PROCEDURE smart_queue_delete_despense_logs();


INSERT INTO s_smart_queue_caller_version VALUES ('2', '2', 'Smart Queue Caller Module', '1.3.0', '1.2.0', (select current_date) || ','|| (select current_time));

INSERT INTO s_script_update_log values ('Smart_Queue_Caller_Module','update_smart_queue_caller_002.sql',(select current_date) || ','|| (select current_time),'Update Smart Queue Caller Module');




package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;
import com.hospital_os.usecase.connection.CommonInf;
import java.util.List;

public class NutritionType extends Persistent implements CommonInf {

    public static String NORMAL = "N";
    public static String THIN1 = "1";
    public static String THIN2 = "2";
    public static String THIN3 = "3";
    public static String THIN4 = "4";
    public static String FAT1 = "5";
    public static String FAT2 = "6";
    public static String FAT3 = "7";
    public String description;
    public String max;
    public String min;
    public String standard;

    /**
     * @roseuid 3F658BBB036E
     */
    public NutritionType() {
    }

    @Override
    public String getCode() {
        return getObjectId();
    }

    @Override
    public String getName() {
        return description;
    }

    @Override
    public String toString() {
        return description;
    }

    /**
     * over 6 years
     *
     * @param bmi
     * @param nu
     * @return
     */
    public static String calculateNutritionCodeByBMI(String bmi, List<NutritionType> nutritionTypes) {
        if(bmi == null || bmi.isEmpty()){
            return null;
        }
        float fbmi = Float.parseFloat(bmi);
        float fmax, fmin;
        for (NutritionType nutritionType : nutritionTypes) {
            fmax = Float.parseFloat(nutritionType.max == null || nutritionType.max.isEmpty() ? "0" : nutritionType.max);
            fmin = Float.parseFloat(nutritionType.min == null || nutritionType.min.isEmpty() ? "0" : nutritionType.min);
            if (fmax == 0.0f && fmin == 0.0f) {
                continue;
            }
            if (fmax == 0.0f && fmin > 0.0f && fbmi >= fmin) {
                return nutritionType.getCode();
            } else if (fmax > 0.0f && fmin == 0.0f && fmax >= fbmi) {
                return nutritionType.getCode();
            } else if (fmax >= fbmi && fbmi >= fmin) {
                return nutritionType.getCode();
            }
        }
        return null;
    }

    /*
     * @Author amp @date 27/04/2549 @see �ӹǳ�дѺ����ҡ�� ���ٵèҡ HCIS
     * under 6 years
     */
    public static int calculateIndexComboBoxNutrition(String sex, String months, String weights) {
        int index = 5;
        if ("1".equals(sex))//�Ȫ��
        {
            if ("".equals(weights)) {
                return 5;
            }
            int month = Integer.parseInt(months);
            float weight = Float.parseFloat(weights);
            switch (month) {
                case 0:
                    if (weight < 2.7) {
                        index = 4;
                    } else if (weight >= 2.7 && weight < 2.8) {
                        index = 3;
                    } else if (weight >= 2.8 && weight < 4.0) {
                        index = 2;
                    } else if (weight >= 4.0 && weight < 4.1) {
                        index = 1;
                    } else if (weight >= 4.1) {
                        index = 0;
                    }
                    break;
                case 1:
                    if (weight < 3.3) {
                        index = 4;
                    } else if (weight >= 3.3 && weight < 3.4) {
                        index = 3;
                    } else if (weight >= 3.4 && weight < 4.8) {
                        index = 2;
                    } else if (weight >= 4.8 && weight < 5.1) {
                        index = 1;
                    } else if (weight >= 5.1) {
                        index = 0;
                    }
                    break;
                case 2:
                    if (weight < 3.9) {
                        index = 4;
                    } else if (weight >= 3.9 && weight < 4.2) {
                        index = 3;
                    } else if (weight >= 4.2 && weight < 5.6) {
                        index = 2;
                    } else if (weight >= 5.6 && weight < 5.9) {
                        index = 1;
                    } else if (weight >= 5.9) {
                        index = 0;
                    }
                    break;
                case 3:
                    if (weight < 4.5) {
                        index = 4;
                    } else if (weight >= 4.5 && weight < 4.8) {
                        index = 3;
                    } else if (weight >= 4.8 && weight < 6.5) {
                        index = 2;
                    } else if (weight >= 6.5 && weight < 6.8) {
                        index = 1;
                    } else if (weight >= 6.8) {
                        index = 0;
                    }
                    break;
                case 4:
                    if (weight < 5.0) {
                        index = 4;
                    } else if (weight >= 5.0 && weight < 5.3) {
                        index = 3;
                    } else if (weight >= 5.3 && weight < 7.2) {
                        index = 2;
                    } else if (weight >= 7.2 && weight < 7.5) {
                        index = 1;
                    } else if (weight >= 7.5) {
                        index = 0;
                    }
                    break;
                case 5:
                    if (weight < 5.5) {
                        index = 4;
                    } else if (weight >= 5.5 && weight < 5.8) {
                        index = 3;
                    } else if (weight >= 5.8 && weight < 7.9) {
                        index = 2;
                    } else if (weight >= 7.9 && weight < 8.2) {
                        index = 1;
                    } else if (weight >= 8.2) {
                        index = 0;
                    }
                    break;
                case 6:
                    if (weight < 6.0) {
                        index = 4;
                    } else if (weight >= 6.0 && weight < 6.3) {
                        index = 3;
                    } else if (weight >= 6.3 && weight < 8.5) {
                        index = 2;
                    } else if (weight >= 8.5 && weight < 8.9) {
                        index = 1;
                    } else if (weight >= 8.9) {
                        index = 0;
                    }
                    break;
                case 7:
                    if (weight < 6.4) {
                        index = 4;
                    } else if (weight >= 6.4 && weight < 6.8) {
                        index = 3;
                    } else if (weight >= 6.8 && weight < 9.1) {
                        index = 2;
                    } else if (weight >= 9.1 && weight < 9.5) {
                        index = 1;
                    } else if (weight >= 9.5) {
                        index = 0;
                    }
                    break;
                case 8:
                    if (weight < 6.8) {
                        index = 4;
                    } else if (weight >= 6.8 && weight < 7.2) {
                        index = 3;
                    } else if (weight >= 7.2 && weight < 9.6) {
                        index = 2;
                    } else if (weight >= 9.6 && weight < 10.0) {
                        index = 1;
                    } else if (weight >= 10.0) {
                        index = 0;
                    }
                    break;
                case 9:
                    if (weight < 7.2) {
                        index = 4;
                    } else if (weight >= 7.2 && weight < 7.6) {
                        index = 3;
                    } else if (weight >= 7.6 && weight < 10.0) {
                        index = 2;
                    } else if (weight >= 10.0 && weight < 10.4) {
                        index = 1;
                    } else if (weight >= 10.4) {
                        index = 0;
                    }
                    break;
                case 10:
                    if (weight < 7.5) {
                        index = 4;
                    } else if (weight >= 7.5 && weight < 7.9) {
                        index = 3;
                    } else if (weight >= 7.9 && weight < 10.4) {
                        index = 2;
                    } else if (weight >= 10.4 && weight < 10.8) {
                        index = 1;
                    } else if (weight >= 10.8) {
                        index = 0;
                    }
                    break;
                case 11:
                    if (weight < 7.7) {
                        index = 4;
                    } else if (weight >= 7.7 && weight < 8.1) {
                        index = 3;
                    } else if (weight >= 8.1 && weight < 10.7) {
                        index = 2;
                    } else if (weight >= 10.7 && weight < 11.2) {
                        index = 1;
                    } else if (weight >= 11.2) {
                        index = 0;
                    }
                    break;
                case 12:
                    if (weight < 7.9) {
                        index = 4;
                    } else if (weight >= 7.9 && weight < 8.3) {
                        index = 3;
                    } else if (weight >= 8.3 && weight < 11.1) {
                        index = 2;
                    } else if (weight >= 11.1 && weight < 11.6) {
                        index = 1;
                    } else if (weight >= 11.6) {
                        index = 0;
                    }
                    break;
                case 13:
                    if (weight < 8.1) {
                        index = 4;
                    } else if (weight >= 8.1 && weight < 8.5) {
                        index = 3;
                    } else if (weight >= 8.5 && weight < 11.4) {
                        index = 2;
                    } else if (weight >= 11.4 && weight < 11.9) {
                        index = 1;
                    } else if (weight >= 11.9) {
                        index = 0;
                    }
                    break;
                case 14:
                    if (weight < 8.3) {
                        index = 4;
                    } else if (weight >= 8.3 && weight < 8.7) {
                        index = 3;
                    } else if (weight >= 8.7 && weight < 11.8) {
                        index = 2;
                    } else if (weight >= 11.8 && weight < 12.3) {
                        index = 1;
                    } else if (weight >= 12.3) {
                        index = 0;
                    }
                    break;
                case 15:
                    if (weight < 8.4) {
                        index = 4;
                    } else if (weight >= 8.4 && weight < 8.9) {
                        index = 3;
                    } else if (weight >= 8.9 && weight < 12.1) {
                        index = 2;
                    } else if (weight >= 12.1 && weight < 12.6) {
                        index = 1;
                    } else if (weight >= 12.6) {
                        index = 0;
                    }
                    break;
                case 16:
                    if (weight < 8.6) {
                        index = 4;
                    } else if (weight >= 8.6 && weight < 9.1) {
                        index = 3;
                    } else if (weight >= 9.1 && weight < 12.4) {
                        index = 2;
                    } else if (weight >= 12.4 && weight < 12.9) {
                        index = 1;
                    } else if (weight >= 12.9) {
                        index = 0;
                    }
                    break;
                case 17:
                    if (weight < 8.8) {
                        index = 4;
                    } else if (weight >= 8.8 && weight < 9.3) {
                        index = 3;
                    } else if (weight >= 9.3 && weight < 12.7) {
                        index = 2;
                    } else if (weight >= 12.7 && weight < 13.2) {
                        index = 1;
                    } else if (weight >= 13.2) {
                        index = 0;
                    }
                    break;
                case 18:
                    if (weight < 8.9) {
                        index = 4;
                    } else if (weight >= 8.9 && weight < 9.4) {
                        index = 3;
                    } else if (weight >= 9.4 && weight < 13.0) {
                        index = 2;
                    } else if (weight >= 13.0 && weight < 13.7) {
                        index = 1;
                    } else if (weight >= 13.7) {
                        index = 0;
                    }
                    break;
                case 19:
                    if (weight < 9.1) {
                        index = 4;
                    } else if (weight >= 9.1 && weight < 9.6) {
                        index = 3;
                    } else if (weight >= 9.6 && weight < 13.3) {
                        index = 2;
                    } else if (weight >= 13.3 && weight < 14.0) {
                        index = 1;
                    } else if (weight >= 14.0) {
                        index = 0;
                    }
                    break;
                case 20:
                    if (weight < 9.3) {
                        index = 4;
                    } else if (weight >= 9.3 && weight < 9.8) {
                        index = 3;
                    } else if (weight >= 9.8 && weight < 13.6) {
                        index = 2;
                    } else if (weight >= 13.6 && weight < 14.3) {
                        index = 1;
                    } else if (weight >= 14.3) {
                        index = 0;
                    }
                    break;
                case 21:
                    if (weight < 9.4) {
                        index = 4;
                    } else if (weight >= 9.4 && weight < 9.9) {
                        index = 3;
                    } else if (weight >= 9.9 && weight < 13.9) {
                        index = 2;
                    } else if (weight >= 13.9 && weight < 14.6) {
                        index = 1;
                    } else if (weight >= 14.6) {
                        index = 0;
                    }
                    break;
                case 22:
                    if (weight < 9.6) {
                        index = 4;
                    } else if (weight >= 9.6 && weight < 10.2) {
                        index = 3;
                    } else if (weight >= 10.2 && weight < 14.1) {
                        index = 2;
                    } else if (weight >= 14.1 && weight < 14.8) {
                        index = 1;
                    } else if (weight >= 14.8) {
                        index = 0;
                    }
                    break;
                case 23:
                    if (weight < 9.6) {
                        index = 4;
                    } else if (weight >= 9.6 && weight < 10.3) {
                        index = 3;
                    } else if (weight >= 10.3 && weight < 14.3) {
                        index = 2;
                    } else if (weight >= 14.3 && weight < 15.0) {
                        index = 1;
                    } else if (weight >= 15.0) {
                        index = 0;
                    }
                    break;
                case 24:
                    if (weight < 9.8) {
                        index = 4;
                    } else if (weight >= 9.8 && weight < 10.5) {
                        index = 3;
                    } else if (weight >= 10.5 && weight < 14.5) {
                        index = 2;
                    } else if (weight >= 14.5 && weight < 15.2) {
                        index = 1;
                    } else if (weight >= 15.2) {
                        index = 0;
                    }
                    break;
                case 25:
                    if (weight < 9.9) {
                        index = 4;
                    } else if (weight >= 9.9 && weight < 10.6) {
                        index = 3;
                    } else if (weight >= 10.6 && weight < 14.7) {
                        index = 2;
                    } else if (weight >= 14.7 && weight < 15.4) {
                        index = 1;
                    } else if (weight >= 15.4) {
                        index = 0;
                    }
                    break;
                case 26:
                    if (weight < 10.1) {
                        index = 4;
                    } else if (weight >= 10.1 && weight < 10.8) {
                        index = 3;
                    } else if (weight >= 10.8 && weight < 14.9) {
                        index = 2;
                    } else if (weight >= 14.9 && weight < 15.6) {
                        index = 1;
                    } else if (weight >= 15.6) {
                        index = 0;
                    }
                    break;
                case 27:
                    if (weight < 10.2) {
                        index = 4;
                    } else if (weight >= 10.2 && weight < 10.9) {
                        index = 3;
                    } else if (weight >= 10.9 && weight < 15.2) {
                        index = 2;
                    } else if (weight >= 15.2 && weight < 15.9) {
                        index = 1;
                    } else if (weight >= 15.9) {
                        index = 0;
                    }
                    break;
                case 28:
                    if (weight < 10.3) {
                        index = 4;
                    } else if (weight >= 10.3 && weight < 11.0) {
                        index = 3;
                    } else if (weight >= 11.0 && weight < 15.5) {
                        index = 2;
                    } else if (weight >= 15.5 && weight < 16.2) {
                        index = 1;
                    } else if (weight >= 16.2) {
                        index = 0;
                    }
                    break;
                case 29:
                    if (weight < 10.5) {
                        index = 4;
                    } else if (weight >= 10.5 && weight < 11.2) {
                        index = 3;
                    } else if (weight >= 11.2 && weight < 15.7) {
                        index = 2;
                    } else if (weight >= 15.7 && weight < 16.4) {
                        index = 1;
                    } else if (weight >= 16.4) {
                        index = 0;
                    }
                    break;
                case 30:
                    if (weight < 10.6) {
                        index = 4;
                    } else if (weight >= 10.6 && weight < 11.4) {
                        index = 3;
                    } else if (weight >= 11.4 && weight < 15.9) {
                        index = 2;
                    } else if (weight >= 15.9 && weight < 16.7) {
                        index = 1;
                    } else if (weight >= 16.7) {
                        index = 0;
                    }
                    break;
                case 31:
                    if (weight < 10.7) {
                        index = 4;
                    } else if (weight >= 10.7 && weight < 11.5) {
                        index = 3;
                    } else if (weight >= 11.5 && weight < 16.2) {
                        index = 2;
                    } else if (weight >= 16.2 && weight < 17.0) {
                        index = 1;
                    } else if (weight >= 17.0) {
                        index = 0;
                    }
                    break;
                case 32:
                    if (weight < 10.9) {
                        index = 4;
                    } else if (weight >= 10.9 && weight < 11.7) {
                        index = 3;
                    } else if (weight >= 11.7 && weight < 16.4) {
                        index = 2;
                    } else if (weight >= 16.4 && weight < 17.2) {
                        index = 1;
                    } else if (weight >= 17.2) {
                        index = 0;
                    }
                    break;
                case 33:
                    if (weight < 11.0) {
                        index = 4;
                    } else if (weight >= 11.0 && weight < 11.8) {
                        index = 3;
                    } else if (weight >= 11.8 && weight < 16.7) {
                        index = 2;
                    } else if (weight >= 16.7 && weight < 17.5) {
                        index = 1;
                    } else if (weight >= 17.5) {
                        index = 0;
                    }
                    break;
                case 34:
                    if (weight < 11.1) {
                        index = 4;
                    } else if (weight >= 11.1 && weight < 11.9) {
                        index = 3;
                    } else if (weight >= 11.9 && weight < 16.9) {
                        index = 2;
                    } else if (weight >= 16.9 && weight < 17.7) {
                        index = 1;
                    } else if (weight >= 17.7) {
                        index = 0;
                    }
                    break;
                case 35:
                    if (weight < 11.2) {
                        index = 4;
                    } else if (weight >= 11.2 && weight < 12.0) {
                        index = 3;
                    } else if (weight >= 12.0 && weight < 17.2) {
                        index = 2;
                    } else if (weight >= 17.2 && weight < 18.0) {
                        index = 1;
                    } else if (weight >= 18.0) {
                        index = 0;
                    }
                    break;
                case 36:
                    if (weight < 11.3) {
                        index = 4;
                    } else if (weight >= 11.3 && weight < 12.1) {
                        index = 3;
                    } else if (weight >= 12.1 && weight < 17.3) {
                        index = 2;
                    } else if (weight >= 17.3 && weight < 18.2) {
                        index = 1;
                    } else if (weight >= 18.2) {
                        index = 0;
                    }
                    break;
                case 37:
                    if (weight < 11.4) {
                        index = 4;
                    } else if (weight >= 11.4 && weight < 12.2) {
                        index = 3;
                    } else if (weight >= 12.2 && weight < 17.6) {
                        index = 2;
                    } else if (weight >= 17.6 && weight < 18.5) {
                        index = 1;
                    } else if (weight >= 18.5) {
                        index = 0;
                    }
                    break;
                case 38:
                    if (weight < 11.6) {
                        index = 4;
                    } else if (weight >= 11.6 && weight < 12.4) {
                        index = 3;
                    } else if (weight >= 12.4 && weight < 17.8) {
                        index = 2;
                    } else if (weight >= 17.8 && weight < 18.7) {
                        index = 1;
                    } else if (weight >= 18.7) {
                        index = 0;
                    }
                    break;
                case 39:
                    if (weight < 11.7) {
                        index = 4;
                    } else if (weight >= 11.7 && weight < 12.5) {
                        index = 3;
                    } else if (weight >= 12.5 && weight < 18.1) {
                        index = 2;
                    } else if (weight >= 18.1 && weight < 19.0) {
                        index = 1;
                    } else if (weight >= 19.0) {
                        index = 0;
                    }
                    break;
                case 40:
                    if (weight < 11.8) {
                        index = 4;
                    } else if (weight >= 11.8 && weight < 12.6) {
                        index = 3;
                    } else if (weight >= 12.6 && weight < 18.2) {
                        index = 2;
                    } else if (weight >= 18.2 && weight < 19.2) {
                        index = 1;
                    } else if (weight >= 19.2) {
                        index = 0;
                    }
                    break;
                case 41:
                    if (weight < 11.9) {
                        index = 4;
                    } else if (weight >= 11.9 && weight < 12.7) {
                        index = 3;
                    } else if (weight >= 12.7 && weight < 18.5) {
                        index = 2;
                    } else if (weight >= 18.5 && weight < 19.5) {
                        index = 1;
                    } else if (weight >= 19.5) {
                        index = 0;
                    }
                    break;
                case 42:
                    if (weight < 12.0) {
                        index = 4;
                    } else if (weight >= 12.0 && weight < 12.8) {
                        index = 3;
                    } else if (weight >= 12.8 && weight < 18.7) {
                        index = 2;
                    } else if (weight >= 18.7 && weight < 19.8) {
                        index = 1;
                    } else if (weight >= 19.8) {
                        index = 0;
                    }
                    break;
                case 43:
                    if (weight < 12.2) {
                        index = 4;
                    } else if (weight >= 12.2 && weight < 13.0) {
                        index = 3;
                    } else if (weight >= 13.0 && weight < 18.9) {
                        index = 2;
                    } else if (weight >= 18.9 && weight < 20.1) {
                        index = 1;
                    } else if (weight >= 20.1) {
                        index = 0;
                    }
                    break;
                case 44:
                    if (weight < 12.3) {
                        index = 4;
                    } else if (weight >= 12.3 && weight < 13.1) {
                        index = 3;
                    } else if (weight >= 13.1 && weight < 19.1) {
                        index = 2;
                    } else if (weight >= 19.1 && weight < 20.3) {
                        index = 1;
                    } else if (weight >= 20.3) {
                        index = 0;
                    }
                    break;
                case 45:
                    if (weight < 12.4) {
                        index = 4;
                    } else if (weight >= 12.4 && weight < 13.2) {
                        index = 3;
                    } else if (weight >= 13.2 && weight < 19.4) {
                        index = 2;
                    } else if (weight >= 19.4 && weight < 20.6) {
                        index = 1;
                    } else if (weight >= 20.6) {
                        index = 0;
                    }
                    break;
                case 46:
                    if (weight < 12.5) {
                        index = 4;
                    } else if (weight >= 12.5 && weight < 13.4) {
                        index = 3;
                    } else if (weight >= 13.4 && weight < 19.6) {
                        index = 2;
                    } else if (weight >= 19.6 && weight < 20.8) {
                        index = 1;
                    } else if (weight >= 20.8) {
                        index = 0;
                    }
                    break;
                case 47:
                    if (weight < 12.6) {
                        index = 4;
                    } else if (weight >= 12.6 && weight < 13.5) {
                        index = 3;
                    } else if (weight >= 13.5 && weight < 19.8) {
                        index = 2;
                    } else if (weight >= 19.8 && weight < 21.0) {
                        index = 1;
                    } else if (weight >= 21.0) {
                        index = 0;
                    }
                    break;
                case 48:
                    if (weight < 12.7) {
                        index = 4;
                    } else if (weight >= 12.7 && weight < 13.6) {
                        index = 3;
                    } else if (weight >= 13.6 && weight < 20.0) {
                        index = 2;
                    } else if (weight >= 20.0 && weight < 21.3) {
                        index = 1;
                    } else if (weight >= 21.3) {
                        index = 0;
                    }
                    break;
                case 49:
                    if (weight < 12.8) {
                        index = 4;
                    } else if (weight >= 12.8 && weight < 13.7) {
                        index = 3;
                    } else if (weight >= 13.7 && weight < 20.3) {
                        index = 2;
                    } else if (weight >= 20.3 && weight < 21.6) {
                        index = 1;
                    } else if (weight >= 21.6) {
                        index = 0;
                    }
                    break;
                case 50:
                    if (weight < 12.9) {
                        index = 4;
                    } else if (weight >= 12.9 && weight < 13.8) {
                        index = 3;
                    } else if (weight >= 13.8 && weight < 20.5) {
                        index = 2;
                    } else if (weight >= 20.5 && weight < 21.8) {
                        index = 1;
                    } else if (weight >= 21.8) {
                        index = 0;
                    }
                    break;
                case 51:
                    if (weight < 13.0) {
                        index = 4;
                    } else if (weight >= 13.0 && weight < 13.9) {
                        index = 3;
                    } else if (weight >= 13.9 && weight < 20.7) {
                        index = 2;
                    } else if (weight >= 20.7 && weight < 22.0) {
                        index = 1;
                    } else if (weight >= 22.0) {
                        index = 0;
                    }
                    break;
                case 52:
                    if (weight < 13.1) {
                        index = 4;
                    } else if (weight >= 13.1 && weight < 14.0) {
                        index = 3;
                    } else if (weight >= 14.0 && weight < 20.9) {
                        index = 2;
                    } else if (weight >= 20.9 && weight < 22.2) {
                        index = 1;
                    } else if (weight >= 22.2) {
                        index = 0;
                    }
                    break;
                case 53:
                    if (weight < 13.2) {
                        index = 4;
                    } else if (weight >= 13.2 && weight < 14.1) {
                        index = 3;
                    } else if (weight >= 14.1 && weight < 21.1) {
                        index = 2;
                    } else if (weight >= 21.1 && weight < 22.2) {
                        index = 1;
                    } else if (weight >= 22.2) {
                        index = 0;
                    }
                    break;
                case 54:
                    if (weight < 13.3) {
                        index = 4;
                    } else if (weight >= 13.3 && weight < 14.2) {
                        index = 3;
                    } else if (weight >= 14.2 && weight < 21.3) {
                        index = 2;
                    } else if (weight >= 21.3 && weight < 22.7) {
                        index = 1;
                    } else if (weight >= 22.7) {
                        index = 0;
                    }
                    break;
                case 55:
                    if (weight < 13.5) {
                        index = 4;
                    } else if (weight >= 13.5 && weight < 14.4) {
                        index = 3;
                    } else if (weight >= 14.4 && weight < 21.6) {
                        index = 2;
                    } else if (weight >= 21.6 && weight < 23.0) {
                        index = 1;
                    } else if (weight >= 23.0) {
                        index = 0;
                    }
                    break;
                case 56:
                    if (weight < 13.6) {
                        index = 4;
                    } else if (weight >= 13.6 && weight < 14.5) {
                        index = 3;
                    } else if (weight >= 14.5 && weight < 21.8) {
                        index = 2;
                    } else if (weight >= 21.8 && weight < 23.3) {
                        index = 1;
                    } else if (weight >= 23.3) {
                        index = 0;
                    }
                    break;
                case 57:
                    if (weight < 13.8) {
                        index = 4;
                    } else if (weight >= 13.8 && weight < 14.7) {
                        index = 3;
                    } else if (weight >= 14.7 && weight < 22.0) {
                        index = 2;
                    } else if (weight >= 22.0 && weight < 23.6) {
                        index = 1;
                    } else if (weight >= 23.6) {
                        index = 0;
                    }
                    break;
                case 58:
                    if (weight < 13.9) {
                        index = 4;
                    } else if (weight >= 13.9 && weight < 14.8) {
                        index = 3;
                    } else if (weight >= 14.8 && weight < 22.2) {
                        index = 2;
                    } else if (weight >= 22.2 && weight < 23.8) {
                        index = 1;
                    } else if (weight >= 23.8) {
                        index = 0;
                    }
                    break;
                case 59:
                    if (weight < 14.0) {
                        index = 4;
                    } else if (weight >= 14.0 && weight < 14.9) {
                        index = 3;
                    } else if (weight >= 14.9 && weight < 22.5) {
                        index = 2;
                    } else if (weight >= 22.5 && weight < 24.1) {
                        index = 1;
                    } else if (weight >= 24.1) {
                        index = 0;
                    }
                    break;
                case 60:
                    if (weight < 14.1) {
                        index = 4;
                    } else if (weight >= 14.1 && weight < 15.0) {
                        index = 3;
                    } else if (weight >= 15.0 && weight < 22.7) {
                        index = 2;
                    } else if (weight >= 22.7 && weight < 24.3) {
                        index = 1;
                    } else if (weight >= 24.3) {
                        index = 0;
                    }
                    break;
                case 61:
                    if (weight < 14.2) {
                        index = 4;
                    } else if (weight >= 14.2 && weight < 15.1) {
                        index = 3;
                    } else if (weight >= 15.1 && weight < 23.0) {
                        index = 2;
                    } else if (weight >= 23.0 && weight < 24.6) {
                        index = 1;
                    } else if (weight >= 24.6) {
                        index = 0;
                    }
                    break;
                case 62:
                    if (weight < 14.4) {
                        index = 4;
                    } else if (weight >= 14.4 && weight < 15.3) {
                        index = 3;
                    } else if (weight >= 15.3 && weight < 23.2) {
                        index = 2;
                    } else if (weight >= 23.2 && weight < 24.8) {
                        index = 1;
                    } else if (weight >= 24.8) {
                        index = 0;
                    }
                    break;
                case 63:
                    if (weight < 14.5) {
                        index = 4;
                    } else if (weight >= 14.5 && weight < 15.4) {
                        index = 3;
                    } else if (weight >= 15.4 && weight < 23.4) {
                        index = 2;
                    } else if (weight >= 23.4 && weight < 25.1) {
                        index = 1;
                    } else if (weight >= 25.1) {
                        index = 0;
                    }
                    break;
                case 64:
                    if (weight < 14.6) {
                        index = 4;
                    } else if (weight >= 14.6 && weight < 15.5) {
                        index = 3;
                    } else if (weight >= 15.5 && weight < 23.6) {
                        index = 2;
                    } else if (weight >= 23.6 && weight < 25.3) {
                        index = 1;
                    } else if (weight >= 25.3) {
                        index = 0;
                    }
                    break;
                case 65:
                    if (weight < 14.7) {
                        index = 4;
                    } else if (weight >= 14.7 && weight < 15.7) {
                        index = 3;
                    } else if (weight >= 15.7 && weight < 23.9) {
                        index = 2;
                    } else if (weight >= 23.9 && weight < 25.6) {
                        index = 1;
                    } else if (weight >= 25.6) {
                        index = 0;
                    }
                    break;
                case 66:
                    if (weight < 14.8) {
                        index = 4;
                    } else if (weight >= 14.8 && weight < 15.8) {
                        index = 3;
                    } else if (weight >= 15.8 && weight < 24.1) {
                        index = 2;
                    } else if (weight >= 24.1 && weight < 25.8) {
                        index = 1;
                    } else if (weight >= 25.8) {
                        index = 0;
                    }
                    break;
                case 67:
                    if (weight < 14.9) {
                        index = 4;
                    } else if (weight >= 14.9 && weight < 15.9) {
                        index = 3;
                    } else if (weight >= 15.9 && weight < 24.4) {
                        index = 2;
                    } else if (weight >= 24.4 && weight < 26.1) {
                        index = 1;
                    } else if (weight >= 26.1) {
                        index = 0;
                    }
                    break;
                case 68:
                    if (weight < 15.1) {
                        index = 4;
                    } else if (weight >= 15.1 && weight < 16.1) {
                        index = 3;
                    } else if (weight >= 16.1 && weight < 24.5) {
                        index = 2;
                    } else if (weight >= 24.5 && weight < 26.3) {
                        index = 1;
                    } else if (weight >= 26.3) {
                        index = 0;
                    }
                    break;
                case 69:
                    if (weight < 15.2) {
                        index = 4;
                    } else if (weight >= 15.2 && weight < 16.2) {
                        index = 3;
                    } else if (weight >= 16.2 && weight < 24.7) {
                        index = 2;
                    } else if (weight >= 24.7 && weight < 26.5) {
                        index = 1;
                    } else if (weight >= 26.5) {
                        index = 0;
                    }
                    break;
                case 70:
                    if (weight < 15.4) {
                        index = 4;
                    } else if (weight >= 15.4 && weight < 16.4) {
                        index = 3;
                    } else if (weight >= 16.4 && weight < 25.0) {
                        index = 2;
                    } else if (weight >= 25.0 && weight < 26.8) {
                        index = 1;
                    } else if (weight >= 26.8) {
                        index = 0;
                    }
                    break;
                case 71:
                    if (weight < 15.5) {
                        index = 4;
                    } else if (weight >= 15.5 && weight < 16.5) {
                        index = 3;
                    } else if (weight >= 16.5 && weight < 25.3) {
                        index = 2;
                    } else if (weight >= 25.3 && weight < 27.1) {
                        index = 1;
                    } else if (weight >= 27.1) {
                        index = 0;
                    }
                    break;
                case 72:
                    if (weight < 15.5) {
                        index = 4;
                    } else if (weight >= 15.5 && weight < 16.6) {
                        index = 3;
                    } else if (weight >= 16.6 && weight < 25.5) {
                        index = 2;
                    } else if (weight >= 25.5 && weight < 27.3) {
                        index = 1;
                    } else if (weight >= 27.3) {
                        index = 0;
                    }
                    break;
                default:
                    break;
            }
            return index;
        } else if ("2".equals(sex))//��˭ԧ
        {
            if ("".equals(weights)) {
                return 5;
            }
            int month = Integer.parseInt(months);
            double weight = Double.parseDouble(weights);
            switch (month) {
                case 0:
                    if (weight < 2.6) {
                        index = 4;
                    } else if (weight >= 2.6 && weight < 2.7) {
                        index = 3;
                    } else if (weight >= 2.7 && weight < 3.8) {
                        index = 2;
                    } else if (weight >= 3.8 && weight < 3.9) {
                        index = 1;
                    } else if (weight >= 3.9) {
                        index = 0;
                    }
                    break;
                case 1:
                    if (weight < 3.2) {
                        index = 4;
                    } else if (weight >= 3.2 && weight < 3.3) {
                        index = 3;
                    } else if (weight >= 3.3 && weight < 3.5) {
                        index = 2;
                    } else if (weight >= 3.5 && weight < 3.8) {
                        index = 1;
                    } else if (weight >= 3.8) {
                        index = 0;
                    }
                    break;
                case 2:
                    if (weight < 3.7) {
                        index = 4;
                    } else if (weight >= 3.7 && weight < 3.8) {
                        index = 3;
                    } else if (weight >= 3.8 && weight < 5.3) {
                        index = 2;
                    } else if (weight >= 5.3 && weight < 5.6) {
                        index = 1;
                    } else if (weight >= 5.6) {
                        index = 0;
                    }
                    break;
                case 3:
                    if (weight < 4.1) {
                        index = 4;
                    } else if (weight >= 4.1 && weight < 4.4) {
                        index = 3;
                    } else if (weight >= 4.4 && weight < 6.1) {
                        index = 2;
                    } else if (weight >= 6.1 && weight < 6.4) {
                        index = 1;
                    } else if (weight >= 6.4) {
                        index = 0;
                    }
                    break;
                case 4:
                    if (weight < 4.6) {
                        index = 4;
                    } else if (weight >= 4.6 && weight < 4.9) {
                        index = 3;
                    } else if (weight >= 4.9 && weight < 6.8) {
                        index = 2;
                    } else if (weight >= 6.8 && weight < 7.1) {
                        index = 1;
                    } else if (weight >= 7.1) {
                        index = 0;
                    }
                    break;
                case 5:
                    if (weight < 5.0) {
                        index = 4;
                    } else if (weight >= 5.0 && weight < 5.3) {
                        index = 3;
                    } else if (weight >= 5.3 && weight < 7.4) {
                        index = 2;
                    } else if (weight >= 7.4 && weight < 7.8) {
                        index = 1;
                    } else if (weight >= 7.8) {
                        index = 0;
                    }
                    break;
                case 6:
                    if (weight < 5.5) {
                        index = 4;
                    } else if (weight >= 5.5 && weight < 5.8) {
                        index = 3;
                    } else if (weight >= 5.8 && weight < 8.0) {
                        index = 2;
                    } else if (weight >= 8.0 && weight < 8.4) {
                        index = 1;
                    } else if (weight >= 8.4) {
                        index = 0;
                    }
                    break;
                case 7:
                    if (weight < 5.8) {
                        index = 4;
                    } else if (weight >= 5.8 && weight < 6.2) {
                        index = 3;
                    } else if (weight >= 6.2 && weight < 8.6) {
                        index = 2;
                    } else if (weight >= 8.6 && weight < 9.0) {
                        index = 1;
                    } else if (weight >= 9.0) {
                        index = 0;
                    }
                    break;
                case 8:
                    if (weight < 6.2) {
                        index = 4;
                    } else if (weight >= 6.2 && weight < 6.6) {
                        index = 3;
                    } else if (weight >= 6.6 && weight < 9.1) {
                        index = 2;
                    } else if (weight >= 9.1 && weight < 9.5) {
                        index = 1;
                    } else if (weight >= 9.5) {
                        index = 0;
                    }
                    break;
                case 9:
                    if (weight < 6.5) {
                        index = 4;
                    } else if (weight >= 6.5 && weight < 6.9) {
                        index = 3;
                    } else if (weight >= 6.9 && weight < 9.4) {
                        index = 2;
                    } else if (weight >= 9.4 && weight < 9.9) {
                        index = 1;
                    } else if (weight >= 9.9) {
                        index = 0;
                    }
                    break;
                case 10:
                    if (weight < 6.8) {
                        index = 4;
                    } else if (weight >= 6.8 && weight < 7.2) {
                        index = 3;
                    } else if (weight >= 7.2 && weight < 9.9) {
                        index = 2;
                    } else if (weight >= 9.9 && weight < 10.4) {
                        index = 1;
                    } else if (weight >= 10.4) {
                        index = 0;
                    }
                    break;
                case 11:
                    if (weight < 7.1) {
                        index = 4;
                    } else if (weight >= 7.1 && weight < 7.5) {
                        index = 3;
                    } else if (weight >= 7.5 && weight < 10.3) {
                        index = 2;
                    } else if (weight >= 10.3 && weight < 10.8) {
                        index = 1;
                    } else if (weight >= 10.8) {
                        index = 0;
                    }
                    break;
                case 12:
                    if (weight < 7.3) {
                        index = 4;
                    } else if (weight >= 7.3 && weight < 7.7) {
                        index = 3;
                    } else if (weight >= 7.7 && weight < 10.6) {
                        index = 2;
                    } else if (weight >= 10.6 && weight < 11.1) {
                        index = 1;
                    } else if (weight >= 10.1) {
                        index = 0;
                    }
                    break;
                case 13:
                    if (weight < 7.5) {
                        index = 4;
                    } else if (weight >= 7.5 && weight < 7.9) {
                        index = 3;
                    } else if (weight >= 7.9 && weight < 10.9) {
                        index = 2;
                    } else if (weight >= 10.9 && weight < 11.4) {
                        index = 1;
                    } else if (weight >= 11.4) {
                        index = 0;
                    }
                    break;
                case 14:
                    if (weight < 7.7) {
                        index = 4;
                    } else if (weight >= 7.7 && weight < 8.1) {
                        index = 3;
                    } else if (weight >= 8.1 && weight < 11.2) {
                        index = 2;
                    } else if (weight >= 11.2 && weight < 11.7) {
                        index = 1;
                    } else if (weight >= 11.7) {
                        index = 0;
                    }
                    break;
                case 15:
                    if (weight < 7.9) {
                        index = 4;
                    } else if (weight >= 7.9 && weight < 8.3) {
                        index = 3;
                    } else if (weight >= 8.3 && weight < 11.4) {
                        index = 2;
                    } else if (weight >= 11.4 && weight < 12.0) {
                        index = 1;
                    } else if (weight >= 12.0) {
                        index = 0;
                    }
                    break;
                case 16:
                    if (weight < 8.0) {
                        index = 4;
                    } else if (weight >= 8.0 && weight < 8.4) {
                        index = 3;
                    } else if (weight >= 8.4 && weight < 11.7) {
                        index = 2;
                    } else if (weight >= 11.7 && weight < 12.2) {
                        index = 1;
                    } else if (weight >= 12.2) {
                        index = 0;
                    }
                    break;
                case 17:
                    if (weight < 8.2) {
                        index = 4;
                    } else if (weight >= 8.2 && weight < 8.6) {
                        index = 3;
                    } else if (weight >= 8.6 && weight < 11.9) {
                        index = 2;
                    } else if (weight >= 11.9 && weight < 12.5) {
                        index = 1;
                    } else if (weight >= 12.5) {
                        index = 0;
                    }
                    break;
                case 18:
                    if (weight < 8.3) {
                        index = 4;
                    } else if (weight >= 8.3 && weight < 8.8) {
                        index = 3;
                    } else if (weight >= 8.8 && weight < 12.2) {
                        index = 2;
                    } else if (weight >= 12.2 && weight < 12.9) {
                        index = 1;
                    } else if (weight >= 12.9) {
                        index = 0;
                    }
                    break;
                case 19:
                    if (weight < 8.5) {
                        index = 4;
                    } else if (weight >= 8.5 && weight < 9.0) {
                        index = 3;
                    } else if (weight >= 9.0 && weight < 12.5) {
                        index = 2;
                    } else if (weight >= 12.5 && weight < 13.2) {
                        index = 1;
                    } else if (weight >= 13.2) {
                        index = 0;
                    }
                    break;
                case 20:
                    if (weight < 8.6) {
                        index = 4;
                    } else if (weight >= 8.6 && weight < 9.1) {
                        index = 3;
                    } else if (weight >= 9.1 && weight < 12.7) {
                        index = 2;
                    } else if (weight >= 12.7 && weight < 13.4) {
                        index = 1;
                    } else if (weight >= 13.4) {
                        index = 0;
                    }
                    break;
                case 21:
                    if (weight < 8.8) {
                        index = 4;
                    } else if (weight >= 8.8 && weight < 9.3) {
                        index = 3;
                    } else if (weight >= 9.3 && weight < 13.0) {
                        index = 2;
                    } else if (weight >= 13.0 && weight < 13.7) {
                        index = 1;
                    } else if (weight >= 13.7) {
                        index = 0;
                    }
                    break;
                case 22:
                    if (weight < 8.9) {
                        index = 4;
                    } else if (weight >= 8.9 && weight < 9.4) {
                        index = 3;
                    } else if (weight >= 9.4 && weight < 13.2) {
                        index = 2;
                    } else if (weight >= 13.2 && weight < 13.9) {
                        index = 1;
                    } else if (weight >= 13.9) {
                        index = 0;
                    }
                    break;
                case 23:
                    if (weight < 9.0) {
                        index = 4;
                    } else if (weight >= 9.0 && weight < 9.5) {
                        index = 3;
                    } else if (weight >= 9.5 && weight < 13.5) {
                        index = 2;
                    } else if (weight >= 13.5 && weight < 14.2) {
                        index = 1;
                    } else if (weight >= 14.2) {
                        index = 0;
                    }
                    break;
                case 24:
                    if (weight < 9.1) {
                        index = 4;
                    } else if (weight >= 9.1 && weight < 9.7) {
                        index = 3;
                    } else if (weight >= 9.7 && weight < 13.8) {
                        index = 2;
                    } else if (weight >= 13.8 && weight < 14.5) {
                        index = 1;
                    } else if (weight >= 14.5) {
                        index = 0;
                    }
                    break;
                case 25:
                    if (weight < 9.2) {
                        index = 4;
                    } else if (weight >= 9.2 && weight < 9.8) {
                        index = 3;
                    } else if (weight >= 9.8 && weight < 14.0) {
                        index = 2;
                    } else if (weight >= 14.0 && weight < 14.7) {
                        index = 1;
                    } else if (weight >= 14.7) {
                        index = 0;
                    }
                    break;
                case 26:
                    if (weight < 9.3) {
                        index = 4;
                    } else if (weight >= 9.3 && weight < 10.0) {
                        index = 3;
                    } else if (weight >= 10.0 && weight < 14.3) {
                        index = 2;
                    } else if (weight >= 14.3 && weight < 15.0) {
                        index = 1;
                    } else if (weight >= 15) {
                        index = 0;
                    }
                    break;
                case 27:
                    if (weight < 9.5) {
                        index = 4;
                    } else if (weight >= 9.5 && weight < 10.1) {
                        index = 3;
                    } else if (weight >= 10.1 && weight < 14.5) {
                        index = 2;
                    } else if (weight >= 14.5 && weight < 15.2) {
                        index = 1;
                    } else if (weight >= 15.2) {
                        index = 0;
                    }
                    break;
                case 28:
                    if (weight < 9.6) {
                        index = 4;
                    } else if (weight >= 9.6 && weight < 10.2) {
                        index = 3;
                    } else if (weight >= 10.2 && weight < 14.7) {
                        index = 2;
                    } else if (weight >= 14.7 && weight < 15.5) {
                        index = 1;
                    } else if (weight >= 15.5) {
                        index = 0;
                    }
                    break;
                case 29:
                    if (weight < 9.7) {
                        index = 4;
                    } else if (weight >= 9.7 && weight < 10.4) {
                        index = 3;
                    } else if (weight >= 10.4 && weight < 15.0) {
                        index = 2;
                    } else if (weight >= 15.0 && weight < 15.8) {
                        index = 1;
                    } else if (weight >= 15.8) {
                        index = 0;
                    }
                    break;
                case 30:
                    if (weight < 9.8) {
                        index = 4;
                    } else if (weight >= 9.8 && weight < 10.6) {
                        index = 3;
                    } else if (weight >= 10.6 && weight < 15.2) {
                        index = 2;
                    } else if (weight >= 15.2 && weight < 16.0) {
                        index = 1;
                    } else if (weight >= 16.0) {
                        index = 0;
                    }
                    break;
                case 31:
                    if (weight < 10.0) {
                        index = 4;
                    } else if (weight >= 10.0 && weight < 10.8) {
                        index = 3;
                    } else if (weight >= 10.8 && weight < 15.5) {
                        index = 2;
                    } else if (weight >= 15.5 && weight < 16.3) {
                        index = 1;
                    } else if (weight >= 16.3) {
                        index = 0;
                    }
                    break;
                case 32:
                    if (weight < 10.1) {
                        index = 4;
                    } else if (weight >= 10.1 && weight < 10.9) {
                        index = 3;
                    } else if (weight >= 10.9 && weight < 15.7) {
                        index = 2;
                    } else if (weight >= 15.7 && weight < 16.5) {
                        index = 1;
                    } else if (weight >= 16.5) {
                        index = 0;
                    }
                    break;
                case 33:
                    if (weight < 10.3) {
                        index = 4;
                    } else if (weight >= 10.3 && weight < 11.1) {
                        index = 3;
                    } else if (weight >= 11.1 && weight < 16.0) {
                        index = 2;
                    } else if (weight >= 16.0 && weight < 16.8) {
                        index = 1;
                    } else if (weight >= 16.8) {
                        index = 0;
                    }
                    break;
                case 34:
                    if (weight < 10.5) {
                        index = 4;
                    } else if (weight >= 10.5 && weight < 11.2) {
                        index = 3;
                    } else if (weight >= 11.2 && weight < 16.2) {
                        index = 2;
                    } else if (weight >= 16.2 && weight < 17.0) {
                        index = 1;
                    } else if (weight >= 17.0) {
                        index = 0;
                    }
                    break;
                case 35:
                    if (weight < 10.6) {
                        index = 4;
                    } else if (weight >= 10.6 && weight < 11.4) {
                        index = 3;
                    } else if (weight >= 11.4 && weight < 16.5) {
                        index = 2;
                    } else if (weight >= 16.5 && weight < 17.3) {
                        index = 1;
                    } else if (weight >= 17.3) {
                        index = 0;
                    }
                    break;
                case 36:
                    if (weight < 10.7) {
                        index = 4;
                    } else if (weight >= 10.7 && weight < 11.5) {
                        index = 3;
                    } else if (weight >= 11.5 && weight < 16.6) {
                        index = 2;
                    } else if (weight >= 16.6 && weight < 17.5) {
                        index = 1;
                    } else if (weight >= 17.5) {
                        index = 0;
                    }
                    break;
                case 37:
                    if (weight < 10.9) {
                        index = 4;
                    } else if (weight >= 10.9 && weight < 11.7) {
                        index = 3;
                    } else if (weight >= 11.7 && weight < 16.9) {
                        index = 2;
                    } else if (weight >= 16.9 && weight < 17.8) {
                        index = 1;
                    } else if (weight >= 17.8) {
                        index = 0;
                    }
                    break;
                case 38:
                    if (weight < 11.0) {
                        index = 4;
                    } else if (weight >= 11.0 && weight < 11.8) {
                        index = 3;
                    } else if (weight >= 11.8 && weight < 17.1) {
                        index = 2;
                    } else if (weight >= 17.1 && weight < 18.0) {
                        index = 1;
                    } else if (weight >= 18.0) {
                        index = 0;
                    }
                    break;
                case 39:
                    if (weight < 11.1) {
                        index = 4;
                    } else if (weight >= 11.1 && weight < 11.9) {
                        index = 3;
                    } else if (weight >= 11.9 && weight < 17.4) {
                        index = 2;
                    } else if (weight >= 17.4 && weight < 18.3) {
                        index = 1;
                    } else if (weight >= 18.3) {
                        index = 0;
                    }
                    break;
                case 40:
                    if (weight < 11.2) {
                        index = 4;
                    } else if (weight >= 11.2 && weight < 12.0) {
                        index = 3;
                    } else if (weight >= 12.0 && weight < 17.6) {
                        index = 2;
                    } else if (weight >= 17.6 && weight < 18.5) {
                        index = 1;
                    } else if (weight >= 18.5) {
                        index = 0;
                    }
                    break;
                case 41:
                    if (weight < 11.4) {
                        index = 4;
                    } else if (weight >= 11.4 && weight < 12.2) {
                        index = 3;
                    } else if (weight >= 12.2 && weight < 17.8) {
                        index = 2;
                    } else if (weight >= 17.8 && weight < 18.5) {
                        index = 1;
                    } else if (weight >= 18.8) {
                        index = 0;
                    }
                    break;
                case 42:
                    if (weight < 11.5) {
                        index = 4;
                    } else if (weight >= 11.5 && weight < 12.3) {
                        index = 3;
                    } else if (weight >= 12.3 && weight < 18.0) {
                        index = 2;
                    } else if (weight >= 18.0 && weight < 19.0) {
                        index = 1;
                    } else if (weight >= 19.0) {
                        index = 0;
                    }
                    break;
                case 43:
                    if (weight < 11.6) {
                        index = 4;
                    } else if (weight >= 11.6 && weight < 12.4) {
                        index = 3;
                    } else if (weight >= 12.4 && weight < 18.2) {
                        index = 2;
                    } else if (weight >= 18.2 && weight < 19.2) {
                        index = 1;
                    } else if (weight >= 19.2) {
                        index = 0;
                    }
                    break;
                case 44:
                    if (weight < 11.7) {
                        index = 4;
                    } else if (weight >= 11.7 && weight < 12.6) {
                        index = 3;
                    } else if (weight >= 12.6 && weight < 18.5) {
                        index = 2;
                    } else if (weight >= 18.5 && weight < 19.6) {
                        index = 1;
                    } else if (weight >= 19.6) {
                        index = 0;
                    }
                    break;
                case 45:
                    if (weight < 11.8) {
                        index = 4;
                    } else if (weight >= 11.8 && weight < 12.7) {
                        index = 3;
                    } else if (weight >= 12.7 && weight < 18.7) {
                        index = 2;
                    } else if (weight >= 18.7 && weight < 19.8) {
                        index = 1;
                    } else if (weight >= 19.8) {
                        index = 0;
                    }
                    break;
                case 46:
                    if (weight < 11.9) {
                        index = 4;
                    } else if (weight >= 11.9 && weight < 12.8) {
                        index = 3;
                    } else if (weight >= 12.8 && weight < 18.8) {
                        index = 2;
                    } else if (weight >= 18.8 && weight < 20.0) {
                        index = 1;
                    } else if (weight >= 20.0) {
                        index = 0;
                    }
                    break;
                case 47:
                    if (weight < 12.0) {
                        index = 4;
                    } else if (weight >= 12.0 && weight < 12.9) {
                        index = 3;
                    } else if (weight >= 12.9 && weight < 19.0) {
                        index = 2;
                    } else if (weight >= 19.0 && weight < 20.2) {
                        index = 1;
                    } else if (weight >= 20.2) {
                        index = 0;
                    }
                    break;
                case 48:
                    if (weight < 12.1) {
                        index = 4;
                    } else if (weight >= 12.1 && weight < 13.0) {
                        index = 3;
                    } else if (weight >= 13.0 && weight < 19.3) {
                        index = 2;
                    } else if (weight >= 19.3 && weight < 20.5) {
                        index = 1;
                    } else if (weight >= 20.5) {
                        index = 0;
                    }
                    break;
                case 49:
                    if (weight < 12.2) {
                        index = 4;
                    } else if (weight >= 12.2 && weight < 13.1) {
                        index = 3;
                    } else if (weight >= 13.1 && weight < 19.5) {
                        index = 2;
                    } else if (weight >= 19.5 && weight < 20.7) {
                        index = 1;
                    } else if (weight >= 20.7) {
                        index = 0;
                    }
                    break;
                case 50:
                    if (weight < 12.3) {
                        index = 4;
                    } else if (weight >= 12.3 && weight < 13.2) {
                        index = 3;
                    } else if (weight >= 13.2 && weight < 19.7) {
                        index = 2;
                    } else if (weight >= 19.7 && weight < 20.9) {
                        index = 1;
                    } else if (weight >= 20.9) {
                        index = 0;
                    }
                    break;
                case 51:
                    if (weight < 12.4) {
                        index = 4;
                    } else if (weight >= 12.4 && weight < 13.3) {
                        index = 3;
                    } else if (weight >= 13.3 && weight < 19.9) {
                        index = 2;
                    } else if (weight >= 19.9 && weight < 21.1) {
                        index = 1;
                    } else if (weight >= 21.1) {
                        index = 0;
                    }
                    break;
                case 52:
                    if (weight < 12.6) {
                        index = 4;
                    } else if (weight >= 12.6 && weight < 13.5) {
                        index = 3;
                    } else if (weight >= 13.5 && weight < 20.0) {
                        index = 2;
                    } else if (weight >= 20.0 && weight < 21.3) {
                        index = 1;
                    } else if (weight >= 21.3) {
                        index = 0;
                    }
                    break;
                case 53:
                    if (weight < 12.7) {
                        index = 4;
                    } else if (weight >= 12.7 && weight < 13.6) {
                        index = 3;
                    } else if (weight >= 13.6 && weight < 20.3) {
                        index = 2;
                    } else if (weight >= 20.3 && weight < 21.6) {
                        index = 1;
                    } else if (weight >= 21.6) {
                        index = 0;
                    }
                    break;
                case 54:
                    if (weight < 12.8) {
                        index = 4;
                    } else if (weight >= 12.8 && weight < 13.7) {
                        index = 3;
                    } else if (weight >= 13.7 && weight < 20.4) {
                        index = 2;
                    } else if (weight >= 20.4 && weight < 21.7) {
                        index = 1;
                    } else if (weight >= 21.7) {
                        index = 0;
                    }
                    break;
                case 55:
                    if (weight < 12.9) {
                        index = 4;
                    } else if (weight >= 12.9 && weight < 13.8) {
                        index = 3;
                    } else if (weight >= 13.8 && weight < 20.6) {
                        index = 2;
                    } else if (weight >= 20.6 && weight < 21.9) {
                        index = 1;
                    } else if (weight >= 21.9) {
                        index = 0;
                    }
                    break;
                case 56:
                    if (weight < 13.0) {
                        index = 4;
                    } else if (weight >= 13.0 && weight < 13.9) {
                        index = 3;
                    } else if (weight >= 13.9 && weight < 20.8) {
                        index = 2;
                    } else if (weight >= 20.8 && weight < 22.1) {
                        index = 1;
                    } else if (weight >= 22.1) {
                        index = 0;
                    }
                    break;
                case 57:
                    if (weight < 13.1) {
                        index = 4;
                    } else if (weight >= 13.1 && weight < 14.0) {
                        index = 3;
                    } else if (weight >= 14.0 && weight < 21.1) {
                        index = 2;
                    } else if (weight >= 21.1 && weight < 22.4) {
                        index = 1;
                    } else if (weight >= 22.4) {
                        index = 0;
                    }
                    break;
                case 58:
                    if (weight < 13.2) {
                        index = 4;
                    } else if (weight >= 13.2 && weight < 14.1) {
                        index = 3;
                    } else if (weight >= 14.1 && weight < 21.3) {
                        index = 2;
                    } else if (weight >= 21.3 && weight < 22.6) {
                        index = 1;
                    } else if (weight >= 22.6) {
                        index = 0;
                    }
                    break;
                case 59:
                    if (weight < 13.2) {
                        index = 4;
                    } else if (weight >= 13.2 && weight < 14.1) {
                        index = 3;
                    } else if (weight >= 14.1 && weight < 21.3) {
                        index = 2;
                    } else if (weight >= 21.3 && weight < 22.6) {
                        index = 1;
                    } else if (weight >= 22.6) {
                        index = 0;
                    }
                    break;
                case 60:
                    if (weight < 13.5) {
                        index = 4;
                    } else if (weight >= 13.5 && weight < 14.4) {
                        index = 3;
                    } else if (weight >= 14.4 && weight < 21.8) {
                        index = 2;
                    } else if (weight >= 21.8 && weight < 23.2) {
                        index = 1;
                    } else if (weight >= 23.2) {
                        index = 0;
                    }
                    break;
                case 61:
                    if (weight < 13.6) {
                        index = 4;
                    } else if (weight >= 13.6 && weight < 14.5) {
                        index = 3;
                    } else if (weight >= 14.5 && weight < 22.1) {
                        index = 2;
                    } else if (weight >= 22.1 && weight < 23.6) {
                        index = 1;
                    } else if (weight >= 23.6) {
                        index = 0;
                    }
                    break;
                case 62:
                    if (weight < 13.7) {
                        index = 4;
                    } else if (weight >= 13.7 && weight < 14.7) {
                        index = 3;
                    } else if (weight >= 14.7 && weight < 22.3) {
                        index = 2;
                    } else if (weight >= 22.3 && weight < 23.9) {
                        index = 1;
                    } else if (weight >= 23.9) {
                        index = 0;
                    }
                    break;
                case 63:
                    if (weight < 13.9) {
                        index = 4;
                    } else if (weight >= 13.9 && weight < 14.9) {
                        index = 3;
                    } else if (weight >= 14.9 && weight < 22.6) {
                        index = 2;
                    } else if (weight >= 22.6 && weight < 24.2) {
                        index = 1;
                    } else if (weight >= 24.2) {
                        index = 0;
                    }
                    break;
                case 64:
                    if (weight < 14.0) {
                        index = 4;
                    } else if (weight >= 14.0 && weight < 15.0) {
                        index = 3;
                    } else if (weight >= 15.0 && weight < 22.8) {
                        index = 2;
                    } else if (weight >= 22.8 && weight < 24.4) {
                        index = 1;
                    } else if (weight >= 24.4) {
                        index = 0;
                    }
                    break;
                case 65:
                    if (weight < 14.2) {
                        index = 4;
                    } else if (weight >= 14.2 && weight < 15.2) {
                        index = 3;
                    } else if (weight >= 15.2 && weight < 23.1) {
                        index = 2;
                    } else if (weight >= 23.1 && weight < 24.7) {
                        index = 1;
                    } else if (weight >= 24.7) {
                        index = 0;
                    }
                    break;
                case 66:
                    if (weight < 14.3) {
                        index = 4;
                    } else if (weight >= 14.3 && weight < 15.3) {
                        index = 3;
                    } else if (weight >= 15.3 && weight < 23.4) {
                        index = 2;
                    } else if (weight >= 23.4 && weight < 25.0) {
                        index = 1;
                    } else if (weight >= 25.0) {
                        index = 0;
                    }
                    break;
                case 67:
                    if (weight < 14.4) {
                        index = 4;
                    } else if (weight >= 14.4 && weight < 15.4) {
                        index = 3;
                    } else if (weight >= 15.4 && weight < 23.6) {
                        index = 2;
                    } else if (weight >= 23.6 && weight < 25.3) {
                        index = 1;
                    } else if (weight >= 25.3) {
                        index = 0;
                    }
                    break;
                case 68:
                    if (weight < 14.6) {
                        index = 4;
                    } else if (weight >= 14.6 && weight < 15.6) {
                        index = 3;
                    } else if (weight >= 15.6 && weight < 23.9) {
                        index = 2;
                    } else if (weight >= 23.9 && weight < 25.6) {
                        index = 1;
                    } else if (weight >= 25.6) {
                        index = 0;
                    }
                    break;
                case 69:
                    if (weight < 14.6) {
                        index = 4;
                    } else if (weight >= 14.6 && weight < 15.7) {
                        index = 3;
                    } else if (weight >= 15.7 && weight < 24.1) {
                        index = 2;
                    } else if (weight >= 24.1 && weight < 25.8) {
                        index = 1;
                    } else if (weight >= 25.8) {
                        index = 0;
                    }
                    break;
                case 70:
                    if (weight < 14.9) {
                        index = 4;
                    } else if (weight >= 14.9 && weight < 15.9) {
                        index = 3;
                    } else if (weight >= 15.9 && weight < 24.3) {
                        index = 2;
                    } else if (weight >= 24.3 && weight < 26.0) {
                        index = 1;
                    } else if (weight >= 26.0) {
                        index = 0;
                    }
                    break;
                case 71:
                    if (weight < 14.9) {
                        index = 4;
                    } else if (weight >= 14.9 && weight < 16.0) {
                        index = 3;
                    } else if (weight >= 16.0 && weight < 24.6) {
                        index = 2;
                    } else if (weight >= 24.6 && weight < 26.3) {
                        index = 1;
                    } else if (weight >= 26.3) {
                        index = 0;
                    }
                    break;
                case 72:
                    if (weight < 15.0) {
                        index = 4;
                    } else if (weight >= 15.0 && weight < 16.1) {
                        index = 3;
                    } else if (weight >= 16.1 && weight < 24.8) {
                        index = 2;
                    } else if (weight >= 24.8 && weight < 26.5) {
                        index = 1;
                    } else if (weight >= 26.5) {
                        index = 0;
                    }
                    break;
                default:
                    break;
            }
            return index;
        } else//����к�(3)
        {
            return 6;
        }
    }
}

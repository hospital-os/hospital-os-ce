/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author tanakrit
 */
public class LabAtkProduct extends Persistent implements CommonInf {

    public String lab_atk_device_name = "";
    public String lab_atk_fda_reg_no = "";
    public String lab_atk_manufacturer_code = "";
    public String lab_atk_manufacturer_name = "";
    public String lab_atk_manufacturer_country = "";
    public String lab_atk_manufacturer_url = "";
    public String lab_atk_product_code = "";
    public String product_type = "";

    @Override
    public String getCode() {
        return getObjectId();
    }

    @Override
    public String getName() {
        return (lab_atk_fda_reg_no.isEmpty() ? "" : lab_atk_fda_reg_no + " : ") + lab_atk_device_name;
    }

    @Override
    public String toString() {
        return getName();
    }
}

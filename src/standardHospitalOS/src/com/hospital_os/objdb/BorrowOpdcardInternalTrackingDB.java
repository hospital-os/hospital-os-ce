/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.BorrowOpdcardInternalTracking;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class BorrowOpdcardInternalTrackingDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "848";

    public BorrowOpdcardInternalTrackingDB(ConnectionInf c) {
        this.connectionInf = c;
    }

    public int insert(BorrowOpdcardInternalTracking obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO t_borrow_opdcard_internal_tracking(\n"
                    + "            t_borrow_opdcard_internal_tracking_id, t_borrow_opdcard_internal_id, b_service_point_sender_id, sender_id, b_service_point_receiver_id)\n"
                    + "    VALUES (?, ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getGenID(tableId));
            preparedStatement.setString(2, obj.t_borrow_opdcard_internal_id);
            preparedStatement.setString(3, obj.b_service_point_sender_id);
            preparedStatement.setString(4, obj.sender_id);
            preparedStatement.setString(5, obj.b_service_point_receiver_id);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<BorrowOpdcardInternalTracking> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List< BorrowOpdcardInternalTracking> list = new ArrayList< BorrowOpdcardInternalTracking>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                BorrowOpdcardInternalTracking obj = new BorrowOpdcardInternalTracking();
                obj.setObjectId(rs.getString("t_borrow_opdcard_internal_tracking_id"));
                obj.t_borrow_opdcard_internal_id = rs.getString("t_borrow_opdcard_internal_id");
                obj.b_service_point_sender_id = rs.getString("b_service_point_sender_id");
                obj.sender_id = rs.getString("sender_id");
                obj.send_datetime = rs.getTimestamp("send_datetime");
                obj.b_service_point_receiver_id = rs.getString("b_service_point_receiver_id");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }
}

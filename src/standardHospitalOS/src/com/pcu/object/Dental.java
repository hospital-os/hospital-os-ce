/*
 * Counsel.java
 *
 * Created on 20 �Զع�¹ 2548, 18:22 �.
 */
package com.pcu.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Noom
 */
@SuppressWarnings("ClassWithoutLogger")
public class Dental extends Persistent {

    private static final long serialVersionUID = 1L;
    private static String init = "";
    public String t_health_dental_id = init;
    public String dental_num_tooth = init;
    public String dental_num_bad_tooth = init;
    public String dental_num_milktooth = init;
    public String dental_num_bad_milktooth = init;
    public String f_health_gum_level_id = init;
    public String dental_detail = init;
    public String dental_remark = init;
    public String dental_false_teeth_need = init;
    public String dental_record_time = init;
    public String dental_modify_time = init;
    public String dental_cancle_time = init;
    public String dental_staff_record = init;
    public String dental_staff_modify = init;
    public String dental_staff_cancle = init;
    public String dental_active = init;
    public String visit_id = init;
    public String patient_id = init;
    public String family_id = init;
    public String survey_date = init;
    public String f_dent_type_id = "5";
    public String pfilling = "";
    public String pextract = "";
    public String dfilling = "";
    public String dextract = "";
    public String need_fluoride = "2";
    public String need_scaling = "2";
    public String need_sealant = "";
    public String need_pfilling = "";
    public String need_dfilling = "";
    public String need_pextract = "";
    public String need_dextract = "";
    public String nprosthesis = "4";
    public String permanent_perma = "";
    public String permanent_prost = "";
    public String prosthesis_prost = "";
    public String gum_id = "999999";
    public String f_school_type_id = "0";
    public String school_class = "";
}

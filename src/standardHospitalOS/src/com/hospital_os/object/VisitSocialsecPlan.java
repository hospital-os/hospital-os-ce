/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;
import java.util.Date;

/**
 *
 * @author Somprasong
 */
public class VisitSocialsecPlan extends Persistent {

    public String t_visit_payment_id = "";
    public String socialsec_number = "";
    public String approve_status = "";
    public String active = "1";
    public Date record_datetime = new Date();
    public String user_record_id = "";
    public Date update_datetime = new Date();
    public String user_update_id = "";
}

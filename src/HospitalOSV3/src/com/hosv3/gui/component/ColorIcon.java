/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.gui.component;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import javax.swing.Icon;

/**
 *
 * @author Somprasong
 */
public class ColorIcon implements Icon {

    private int height = 16;
    private int width = 16;
    private Color color;

    public ColorIcon(Color color) {
        this.color = color;
    }
    
    public ColorIcon(Color color, int width, int height) {
        this.color = color;
        this.width = width;
        this.height = height;
    }
    
    @Override
    public int getIconHeight() {
        return height;
    }

    @Override
    public int getIconWidth() {
        return width;
    }

    public Color getColor() {
        return color;
    }

    @Override
    public void paintIcon(Component c, Graphics g, int x, int y) {
        g.setColor(color);
        g.fillRect(x, y, width - 1, height - 1);

        g.setColor(Color.black);
        g.drawRect(x, y, width - 1, height - 1);
    }
}
/*
 * OrderContinueImageTableCellRendererContinue.java
 *
 * Created on 3 ����Ҥ� 2547, 10:38 �.
 */
package com.hospital_os.utility;

import java.awt.Component;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author amp
 */
public class CelRendererContinue extends JLabel implements TableCellRenderer {

    private static final long serialVersionUID = 1L;

    @Override
    public Component getTableCellRendererComponent(JTable table,
            Object value, boolean isSelected, boolean hasFocus,
            int row, int column) {
        setOpaque(true);
        setHorizontalAlignment(CENTER);
        if (isSelected) {
            this.setBackground(table.getSelectionBackground());
            this.setForeground(table.getSelectionForeground());
        } else {
            this.setBackground(table.getBackground());
            this.setForeground(table.getForeground());
        }
        try {
            if (((String) value).equals("1")) {
                this.setIcon(new ImageIcon(getClass().getResource("/com/hospital_os/images/order_continue.gif")));
                return this;
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }

        this.setIcon(new ImageIcon(getClass().getResource("/com/hospital_os/images/order_discontinue.gif")));
        return this;
    }
    private static final Logger LOG = Logger.getLogger(CelRendererContinue.class.getName());
}

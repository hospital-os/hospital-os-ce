/*
 * CalDateAppointment.java
 *
 * Created on 7 �ԧ�Ҥ� 2549, 16:58 �.
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;
import com.hospital_os.usecase.connection.CommonInf;

/**
 *
 * @author sumo
 */
public class CalDateAppointment extends Persistent implements CommonInf {

    /*��������´*/
    public String description = "";

    /** Creates a new instance of CalDateAppointment */
    public CalDateAppointment() {
    }

    @Override
    public String getCode() {
        return this.getObjectId();
    }

    @Override
    public String getName() {
        return description;
    }

    @Override
    public String toString() {
        return getName();
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.object.printobject;

import com.printing.object.Refer.PrintRefer;

/**
 *
 * @author Somprasong
 */
public class PrintRefer2 extends PrintRefer {

    private final String othdetail = "visit_refer_in_out_othdetail";
    private final String refer_treatment = "visit_refer_in_out_treatment";
    private final String refer_observe = "visit_refer_in_out_observe";
    private final String refer_lab = "visit_refer_in_out_lab";
    private final String refer_result = "visit_refer_in_out_result_request";
    private final String infectious = "visit_refer_in_out_infection_inform";

    public PrintRefer2() {
        super();
    }

    public void setOthdetail(String string) {
        setMap(othdetail, string);
    }

    public void setReferLab(String string) {
        setMap(refer_lab, string);
    }

    public void setReferTreatment(String string) {
        setMap(refer_treatment, string);
    }

    public void setReferObserve(String string) {
        setMap(refer_observe, string);
    }

    public void setReferResult(String string) {
        setMap(refer_result, string);
    }

    public void setInfectious(String string) {
        setMap(infectious, string);
    }
}

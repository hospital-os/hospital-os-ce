update f_ncd_eyes_result_type set f_ncd_eyes_result_type_id = '19' where f_ncd_eyes_result_type_id = '16';
update f_ncd_eyes_result_type set f_ncd_eyes_result_type_id = '18' where f_ncd_eyes_result_type_id = '15';
update f_ncd_eyes_result_type set f_ncd_eyes_result_type_id = '17' where f_ncd_eyes_result_type_id = '14';
update f_ncd_eyes_result_type set f_ncd_eyes_result_type_id = '16' where f_ncd_eyes_result_type_id = '13';
update f_ncd_eyes_result_type set f_ncd_eyes_result_type_id = '15' where f_ncd_eyes_result_type_id = '12';
update f_ncd_eyes_result_type set f_ncd_eyes_result_type_id = '13' where f_ncd_eyes_result_type_id = '11';
update f_ncd_eyes_result_type set f_ncd_eyes_result_type_id = '12' where f_ncd_eyes_result_type_id = '10';

update t_ncd_eyes_complications set left_eyes_result = '19' where left_eyes_result = '16';
update t_ncd_eyes_complications set left_eyes_result = '18' where left_eyes_result = '15';
update t_ncd_eyes_complications set left_eyes_result = '17' where left_eyes_result = '14';
update t_ncd_eyes_complications set left_eyes_result = '16' where left_eyes_result = '13';
update t_ncd_eyes_complications set left_eyes_result = '15' where left_eyes_result = '12';
update t_ncd_eyes_complications set left_eyes_result = '13' where left_eyes_result = '11';
update t_ncd_eyes_complications set left_eyes_result = '12' where left_eyes_result = '10';

update t_ncd_eyes_complications set right_eyes_result = '19' where right_eyes_result = '16';
update t_ncd_eyes_complications set right_eyes_result = '18' where right_eyes_result = '15';
update t_ncd_eyes_complications set right_eyes_result = '17' where right_eyes_result = '14';
update t_ncd_eyes_complications set right_eyes_result = '16' where right_eyes_result = '13';
update t_ncd_eyes_complications set right_eyes_result = '15' where right_eyes_result = '12';
update t_ncd_eyes_complications set right_eyes_result = '13' where right_eyes_result = '11';
update t_ncd_eyes_complications set right_eyes_result = '12' where right_eyes_result = '10';

insert into f_ncd_eyes_result_type values ('10','20/50');
insert into f_ncd_eyes_result_type values ('11','20/40');
insert into f_ncd_eyes_result_type values ('14','20/15');

INSERT INTO s_ncd_version VALUES ('9760000000002', '2', 'NCD Module', '1.0.20120116', '1.0.20120116', (select current_date) || ','|| (select current_time));

INSERT INTO s_script_update_log values ('NCD_Module','update_ncd_002.sql',(select current_date) || ','|| (select current_time),'UPdate DB NCD Module');
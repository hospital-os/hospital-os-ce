/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.Persistent;
import java.util.Date;

/**
 *
 * @author Somprasong
 */
public class BDayOff extends Persistent implements CommonInf {

    public String dayoff_name = "";
    public Date dayoff_date = new Date();
    public String dayoff_description = "";
    public Date record_date_time;
    public String user_record = "";
    public Date modify_date_time;
    public String user_modify = "";

    @Override
    public String getCode() {
        return getObjectId();
    }

    @Override
    public String getName() {
        return dayoff_name;
    }

    @Override
    public String toString() {
        return getName();
    }
}

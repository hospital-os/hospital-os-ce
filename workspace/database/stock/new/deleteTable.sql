delete from imed.item_price;
delete from imed.base_service_point ;
delete from imed.base_service_point;
delete from imed.item;
delete from imed.base_address;
delete from imed.base_site;
delete from imed.base_unit;
delete from imed.employee;
delete from imed.employee_role;

delete from b_employee;
delete from b_item;
delete from b_item_price;
delete from b_service_point;
delete from b_visit_ward;
delete from b_item_drug_uom;
delete from b_site;
delete from f_address;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.PersonForeigner;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class PersonForeignerDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "851";

    public PersonForeignerDB(ConnectionInf c) {
        this.connectionInf = c;
    }

    public int insert(PersonForeigner obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO t_person_foreigner(\n"
                    + "            t_person_foreigner_id, t_person_id, foreigner_group, passport_fname, passport_lname, passport_no, passport_no_exp, foreigner_no_type, foreigner_no, "
                    + "foreigner_no_exp, f_person_foreigner_id, employer_type, employer_name, employer_id, employer_registration,\n"
                    + "employer_address_house, employer_address_moo, employer_address_road, employer_address_tambon, employer_address_amphur,\n"
                    + "employer_address_changwat, employer_address_postcode, employer_contact_phone_number, employer_contact_mobile_phone_number,\n"
                    + "active, user_record_id, user_update_id)\n"
                    + "    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.getGenID(tableId));
            preparedStatement.setString(index++, obj.t_person_id);
            preparedStatement.setInt(index++, obj.foreigner_group);
            preparedStatement.setString(index++, obj.passport_fname);
            preparedStatement.setString(index++, obj.passport_lname);
            preparedStatement.setString(index++, obj.passport_no);
            preparedStatement.setDate(index++, obj.passport_no_exp != null
                    ? new java.sql.Date(obj.passport_no_exp.getTime())
                    : null);
            preparedStatement.setString(index++, obj.foreigner_no_type);
            preparedStatement.setString(index++, obj.foreigner_no);
            preparedStatement.setDate(index++, obj.foreigner_no_exp != null
                    ? new java.sql.Date(obj.foreigner_no_exp.getTime())
                    : null);
            preparedStatement.setString(index++, obj.f_person_foreigner_id);
            preparedStatement.setInt(index++, obj.employer_type);
            preparedStatement.setString(index++, obj.employer_name);
            preparedStatement.setString(index++, obj.employer_id);
            preparedStatement.setString(index++, obj.employer_registration);
            preparedStatement.setString(index++, obj.employer_address_house);
            preparedStatement.setString(index++, obj.employer_address_moo);
            preparedStatement.setString(index++, obj.employer_address_road);
            preparedStatement.setString(index++, obj.employer_address_tambon);
            preparedStatement.setString(index++, obj.employer_address_amphur);
            preparedStatement.setString(index++, obj.employer_address_changwat);
            preparedStatement.setString(index++, obj.employer_address_postcode);
            preparedStatement.setString(index++, obj.employer_contact_phone_number);
            preparedStatement.setString(index++, obj.employer_contact_mobile_phone_number);
            preparedStatement.setString(index++, obj.active);
            preparedStatement.setString(index++, obj.user_record_id);
            preparedStatement.setString(index++, obj.user_update_id);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(PersonForeigner obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_person_foreigner\n");
            sql.append("   SET foreigner_group=?, passport_fname=?, \n");
            sql.append("       passport_lname=?, passport_no=?,\n");
            sql.append("       passport_no_exp=?,\n");
            sql.append("       foreigner_no_type=?, foreigner_no=?,\n");
            sql.append("       foreigner_no_exp=?, f_person_foreigner_id=?,\n");
            sql.append("       employer_type=?, employer_name=?,\n");
            sql.append("       employer_id=?, employer_registration=?,\n");
            sql.append("       employer_address_house=?, employer_address_moo=?,\n");
            sql.append("       employer_address_road=?, employer_address_tambon=?,\n");
            sql.append("       employer_address_amphur=?, employer_address_changwat=?,\n");
            sql.append("       employer_address_postcode=?, employer_contact_phone_number=?,\n");
            sql.append("       employer_contact_mobile_phone_number=?,\n");
            sql.append("       active = ?, user_update_id=?, update_datetime = current_timestamp\n");
            sql.append(" WHERE t_person_foreigner_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setInt(index++, obj.foreigner_group);
            preparedStatement.setString(index++, obj.passport_fname);
            preparedStatement.setString(index++, obj.passport_lname);
            preparedStatement.setString(index++, obj.passport_no);
            preparedStatement.setDate(index++, obj.passport_no_exp != null
                    ? new java.sql.Date(obj.passport_no_exp.getTime())
                    : null);
            preparedStatement.setString(index++, obj.foreigner_no_type);
            preparedStatement.setString(index++, obj.foreigner_no);
            preparedStatement.setDate(index++, obj.foreigner_no_exp != null
                    ? new java.sql.Date(obj.foreigner_no_exp.getTime())
                    : null);
            preparedStatement.setString(index++, obj.f_person_foreigner_id);
            preparedStatement.setInt(index++, obj.employer_type);
            preparedStatement.setString(index++, obj.employer_name);
            preparedStatement.setString(index++, obj.employer_id);
            preparedStatement.setString(index++, obj.employer_registration);
            preparedStatement.setString(index++, obj.employer_address_house);
            preparedStatement.setString(index++, obj.employer_address_moo);
            preparedStatement.setString(index++, obj.employer_address_road);
            preparedStatement.setString(index++, obj.employer_address_tambon);
            preparedStatement.setString(index++, obj.employer_address_amphur);
            preparedStatement.setString(index++, obj.employer_address_changwat);
            preparedStatement.setString(index++, obj.employer_address_postcode);
            preparedStatement.setString(index++, obj.employer_contact_phone_number);
            preparedStatement.setString(index++, obj.employer_contact_mobile_phone_number);
            preparedStatement.setString(index++, obj.active);
            preparedStatement.setString(index++, obj.user_update_id);
            preparedStatement.setString(index++, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int updateActive(PersonForeigner obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("update t_person_foreigner set active = ?,user_update_id=?, update_datetime = current_timestamp\n");
            sql.append(" WHERE t_person_foreigner_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.active);
            preparedStatement.setString(2, obj.user_update_id);
            preparedStatement.setString(3, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public PersonForeigner select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from t_person_foreigner where t_person_foreigner_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<PersonForeigner> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public PersonForeigner selectByPersonId(String t_person_id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from t_person_foreigner  where t_person_id = ? and active = '1'";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, t_person_id);
            List<PersonForeigner> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<PersonForeigner> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<PersonForeigner> list = new ArrayList<PersonForeigner>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                PersonForeigner obj = new PersonForeigner();
                obj.setObjectId(rs.getString("t_person_foreigner_id"));
                obj.t_person_id = rs.getString("t_person_id");
                obj.passport_fname = rs.getString("passport_fname");
                obj.foreigner_group = rs.getInt("foreigner_group");
                obj.passport_no = rs.getString("passport_no");
                obj.passport_lname = rs.getString("passport_lname");
                obj.passport_no_exp = rs.getDate("passport_no_exp");
                obj.foreigner_no_type = rs.getString("foreigner_no_type");
                obj.foreigner_no = rs.getString("foreigner_no");
                obj.foreigner_no_exp = rs.getDate("foreigner_no_exp");
                obj.f_person_foreigner_id = rs.getString("f_person_foreigner_id");
                obj.employer_type = rs.getInt("employer_type");
                obj.employer_name = rs.getString("employer_name");
                obj.employer_id = rs.getString("employer_id");
                obj.employer_registration = rs.getString("employer_registration");
                obj.employer_address_house = rs.getString("employer_address_house");
                obj.employer_address_moo = rs.getString("employer_address_moo");
                obj.employer_address_road = rs.getString("employer_address_road");
                obj.employer_address_tambon = rs.getString("employer_address_tambon");
                obj.employer_address_amphur = rs.getString("employer_address_amphur");
                obj.employer_address_changwat = rs.getString("employer_address_changwat");
                obj.employer_address_postcode = rs.getString("employer_address_postcode");
                obj.employer_contact_phone_number = rs.getString("employer_contact_phone_number");
                obj.employer_contact_mobile_phone_number = rs.getString("employer_contact_mobile_phone_number");
                obj.active = rs.getString("active");
                obj.user_record_id = rs.getString("user_record_id");
                obj.record_datetime = rs.getTimestamp("record_datetime");
                obj.user_update_id = rs.getString("user_update_id");
                obj.update_datetime = rs.getTimestamp("update_datetime");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }
}

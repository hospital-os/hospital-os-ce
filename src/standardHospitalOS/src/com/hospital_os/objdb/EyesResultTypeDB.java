/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.EyesResultType;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.ComboFix;
import java.sql.ResultSet;
import java.util.Vector;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class EyesResultTypeDB {

    private ConnectionInf theConnectionInf;
    private final EyesResultType dbObj = new EyesResultType();

    public EyesResultTypeDB(ConnectionInf db) {
        theConnectionInf = db;
        dbObj.table = "f_eyes_result_type";
        dbObj.pk_field = "f_eyes_result_type_id";
        dbObj.description = "description";
    }

    public Vector<ComboFix> getComboboxDatasources() throws Exception {
        String sql = "select * from " + dbObj.table + " order by " + dbObj.pk_field;
        return veQuery(sql);
    }

    public Vector<ComboFix> veQuery(String sql) throws Exception {
        ComboFix p;

        Vector<ComboFix> list = new Vector<ComboFix>();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            p = new ComboFix();
            p.code = rs.getString(dbObj.pk_field);
            p.name = rs.getString(dbObj.description);
            list.add(p);
        }
        rs.close();
        return list;
    }
}

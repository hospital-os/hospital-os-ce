/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;
import java.util.Date;

/**
 *
 * @author sompr
 */
public class Audiometry extends Persistent {

    public String t_visit_id = "";
    public String left_result = "";
    public String right_result = "";
    public String user_test_id = "";
    public String active = "1";
    public Date record_date_time;
    public String user_record_id = "";
    public Date update_date_time;
    public String user_update_id = "";

}

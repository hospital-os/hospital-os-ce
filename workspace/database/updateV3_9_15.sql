ALTER TABLE t_visit ADD COLUMN modify_discharge_datetime character varying(255) DEFAULT '';

ALTER TABLE b_receipt_sequence ADD COLUMN receipt_sequence_day character varying(255) DEFAULT '';

ALTER TABLE b_receipt_sequence ADD COLUMN receipt_sequence_month character varying(255) DEFAULT '';

ALTER TABLE b_receipt_sequence ADD COLUMN receipt_sequence_service_point_seq character varying(255) DEFAULT '';

ALTER TABLE b_receipt_sequence ADD COLUMN receipt_sequence_book_no character varying(255) DEFAULT '';

ALTER TABLE b_receipt_sequence ADD COLUMN receipt_sequence_begin_no character varying(255) DEFAULT '';

ALTER TABLE b_receipt_sequence ADD COLUMN receipt_sequence_end_no character varying(255) DEFAULT '';

ALTER TABLE b_receipt_sequence ADD COLUMN receipt_sequence_receipt_qty character varying(255) DEFAULT '';

ALTER TABLE t_billing_receipt ADD COLUMN billing_receipt_book_no character varying(255) DEFAULT '';

ALTER TABLE t_billing_receipt ADD COLUMN billing_receipt_begin_no character varying(255) DEFAULT '';

INSERT INTO s_version VALUES ('9701000000048', '48', 'Hospital OS, Community Edition', '3.9.15', '3.18.060511', '2554-05-06 15:28:00');

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_15.sql',(select current_date) || ','|| (select current_time),'ปรับแก้สำหรับ hospitalOS3.9.15');
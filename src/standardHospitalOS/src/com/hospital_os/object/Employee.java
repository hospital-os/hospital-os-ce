package com.hospital_os.object;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.Persistent;
import java.util.Date;

@SuppressWarnings("ClassWithoutLogger")
public class Employee extends Persistent implements CommonInf {

    private static final long serialVersionUID = 1L;
    public String employee_id = "";
    public String password = "";
    public String employee_no = "";
    public Date employee_number_issue_date;
    public String last_login = "";
    public String last_logout = "";
    public String active = "";
    public String default_service_id = "";
    public String level_id = "";
    public String rule_id = "";
    public String authentication_id = "";
    public String warning_dx = "0";
    public String default_tab = "";
    public String record_date_time = "";
    public String update_date_time = "";
    public String warning_icd10 = "0";
    public String t_person_id = "";
    public String provider = "";
    public String f_provider_council_code_id = "0";
    public String f_provider_type_id = "";
    public String start_date = "";
    public String out_date = "";
    public String move_from = "";
    public String move_to = "";
    public String b_visit_clinic_id = "";
    public String is_main_doctor = "0";
    public Person person = new Person();

    /**
     * @roseuid 3F658BBB036E
     */
    public Employee() {
    }

    public Employee(String eid) {
        setObjectId(eid);
    }

    @Override
    public String getCode() {
        return this.getObjectId();
    }

    @Override
    public String getName() {
        // 3.9.16b01
        return (this.authentication_id.equals("99") ? "ᾷ��Ἱ�� - " : "")
                + this.person.getPersonName();
    }

    @Override
    public String toString() {
        return getName();
    }
}

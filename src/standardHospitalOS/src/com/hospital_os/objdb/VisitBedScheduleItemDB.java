/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.VisitBedScheduleItem;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class VisitBedScheduleItemDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "854";

    public VisitBedScheduleItemDB(ConnectionInf c) {
        this.connectionInf = c;
    }

    public int insert(VisitBedScheduleItem obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO t_visit_bed_schedule_item(\n"
                    + "            t_visit_bed_schedule_item_id, t_visit_id, schedule_item_date_time)\n"
                    + "    VALUES (?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.getGenID(tableId));
            preparedStatement.setString(index++, obj.t_visit_id);
            preparedStatement.setTimestamp(index++, obj.schedule_item_date_time == null ? null
                    : new Timestamp(obj.schedule_item_date_time.getTime()));
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int deleteByVisitId(String visitId) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM t_visit_bed_schedule_item\n");
            sql.append(" WHERE t_visit_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, visitId);
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<VisitBedScheduleItem> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<VisitBedScheduleItem> list = new ArrayList<VisitBedScheduleItem>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                VisitBedScheduleItem obj = new VisitBedScheduleItem();
                obj.setObjectId(rs.getString("t_visit_bed_schedule_item_id"));
                obj.t_visit_id = rs.getString("t_visit_id");
                obj.schedule_item_date_time = rs.getTimestamp("schedule_item_date_time");
                obj.record_date_time = rs.getTimestamp("record_date_time");
                obj.last_order_date_time = rs.getTimestamp("last_order_date_time");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }
}

-- #307 ประเภทข้อมูลเป้าหมาย
CREATE TABLE IF NOT EXISTS f_vaccine_person_type (
    f_vaccine_person_type_id   INTEGER NOT NULL,
    vaccine_person_type_name   CHARACTER VARYING(255) NOT NULL,
    priority                   INTEGER NOT NULL,
    auto_eligible              CHARACTER VARYING(1) NOT NULL,
CONSTRAINT f_vaccine_person_type_pkey PRIMARY KEY (f_vaccine_person_type_id),
CONSTRAINT f_vaccine_person_type_unique UNIQUE (f_vaccine_person_type_id));

BEGIN;
INSERT INTO f_vaccine_person_type SELECT 1,'บุคลากรทางการแพทย์',1,'1'
WHERE NOT EXISTS (SELECT 1 FROM f_vaccine_person_type WHERE f_vaccine_person_type_id = 1); 
COMMIT;
BEGIN;
INSERT INTO f_vaccine_person_type SELECT 2,'ผู้มีอายุ 60 ปีขึ้นไป',4,'1'
WHERE NOT EXISTS (SELECT 1 FROM f_vaccine_person_type WHERE f_vaccine_person_type_id = 2); 
COMMIT;
BEGIN;
INSERT INTO f_vaccine_person_type SELECT 3,'บุคคลที่มีโรคประจำตัว',3,'1'
WHERE NOT EXISTS (SELECT 1 FROM f_vaccine_person_type WHERE f_vaccine_person_type_id = 3); 
COMMIT;
BEGIN;
INSERT INTO f_vaccine_person_type SELECT 4,'เจ้าหน้าที่ที่เกี่ยวข้องกับการควบคุมโรคโควิด 19 ที่มีโอกาสสัมผัสผู้ป่วย',2,'1'
WHERE NOT EXISTS (SELECT 1 FROM f_vaccine_person_type WHERE f_vaccine_person_type_id = 4); 
COMMIT;
BEGIN;
INSERT INTO f_vaccine_person_type SELECT 5,'ประชาชนทั่วไป',5,'0'
WHERE NOT EXISTS (SELECT 1 FROM f_vaccine_person_type WHERE f_vaccine_person_type_id = 5); 
COMMIT;

-- ประเภทย่อยข้อมูลเป้าหมาย
CREATE TABLE IF NOT EXISTS f_vaccine_person_risk_type (
    f_vaccine_person_risk_type_id   INTEGER NOT NULL,
    vaccine_person_risk_type_name   CHARACTER VARYING(255) NOT NULL,
    f_vaccine_person_type_id        INTEGER NOT NULL,
CONSTRAINT f_vaccine_person_risk_type_pkey PRIMARY KEY (f_vaccine_person_risk_type_id),
CONSTRAINT f_vaccine_person_risk_type_unique UNIQUE (f_vaccine_person_risk_type_id),
CONSTRAINT f_vaccine_person_type_fkey FOREIGN KEY (f_vaccine_person_type_id) 
    REFERENCES f_vaccine_person_type (f_vaccine_person_type_id) 
    MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION
);

BEGIN;
INSERT INTO f_vaccine_person_risk_type SELECT 101,'บุคลากรทางการแพทย์และสาธารณสุขภาครัฐ',1
WHERE NOT EXISTS (SELECT 1 FROM f_vaccine_person_risk_type WHERE f_vaccine_person_risk_type_id = 101); 
COMMIT;
BEGIN;
INSERT INTO f_vaccine_person_risk_type SELECT 102,'อาสาสมัครสาธารณสุขประจำหมู่บ้าน (อสม.)/ อาสาสมัครสาธารณสุขต่างด้าว (อสต.)',1
WHERE NOT EXISTS (SELECT 1 FROM f_vaccine_person_risk_type WHERE f_vaccine_person_risk_type_id = 102); 
COMMIT;
BEGIN;
INSERT INTO f_vaccine_person_risk_type SELECT 201,'ผู้มีอายุ 60 ปีขึ้นไป',2
WHERE NOT EXISTS (SELECT 1 FROM f_vaccine_person_risk_type WHERE f_vaccine_person_risk_type_id = 201); 
COMMIT;
BEGIN;
INSERT INTO f_vaccine_person_risk_type SELECT 301,'โรคทางเดินหายใจเรื้อรังรุนแรง เช่น ปอดอุดกั้นเรื้อรัง และโรคหืดหอบ',3
WHERE NOT EXISTS (SELECT 1 FROM f_vaccine_person_risk_type WHERE f_vaccine_person_risk_type_id = 301); 
COMMIT;
BEGIN;
INSERT INTO f_vaccine_person_risk_type SELECT 302,'โรคหัวใจและหลอดเลือด',3
WHERE NOT EXISTS (SELECT 1 FROM f_vaccine_person_risk_type WHERE f_vaccine_person_risk_type_id = 302); 
COMMIT;
BEGIN;
INSERT INTO f_vaccine_person_risk_type SELECT 303,'โรคไตเรื้อรัง',3
WHERE NOT EXISTS (SELECT 1 FROM f_vaccine_person_risk_type WHERE f_vaccine_person_risk_type_id = 303); 
COMMIT;
BEGIN;
INSERT INTO f_vaccine_person_risk_type SELECT 304,'โรคหลอดเลือดสมอง',3
WHERE NOT EXISTS (SELECT 1 FROM f_vaccine_person_risk_type WHERE f_vaccine_person_risk_type_id = 304); 
COMMIT;
BEGIN;
INSERT INTO f_vaccine_person_risk_type SELECT 305,'โรคมะเร็ง',3
WHERE NOT EXISTS (SELECT 1 FROM f_vaccine_person_risk_type WHERE f_vaccine_person_risk_type_id = 305); 
COMMIT;
BEGIN;
INSERT INTO f_vaccine_person_risk_type SELECT 306,'โรคเบาหวาน',3
WHERE NOT EXISTS (SELECT 1 FROM f_vaccine_person_risk_type WHERE f_vaccine_person_risk_type_id = 306); 
COMMIT;
BEGIN;
INSERT INTO f_vaccine_person_risk_type SELECT 307,'โรคอ้วน',3
WHERE NOT EXISTS (SELECT 1 FROM f_vaccine_person_risk_type WHERE f_vaccine_person_risk_type_id = 307); 
COMMIT;
BEGIN;
INSERT INTO f_vaccine_person_risk_type SELECT 401,'ทหารที่เกี่ยวข้องกับการควบคุมโรคโควิด 19 ที่มีโอกาสสัมผัสผู้ป่วย หรือ ปฏิบัติหน้าที่ ณ ช่องทางเข้าออกระหว่างประเทศ ที่ปฏิบัติหน้าที่ควบคุมโรคชายแดน',4
WHERE NOT EXISTS (SELECT 1 FROM f_vaccine_person_risk_type WHERE f_vaccine_person_risk_type_id = 401); 
COMMIT;
BEGIN;
INSERT INTO f_vaccine_person_risk_type SELECT 402,'ตำรวจที่เกี่ยวข้องกับการควบคุมโรคโควิด 19 ที่มีโอกาสสัมผัสผู้ป่วย หรือ ปฏิบัติหน้าที่ ณ ช่องทางเข้าออกระหว่างประเทศ ที่ปฏิบัติหน้าที่ควบคุมโรคชายแดน',4
WHERE NOT EXISTS (SELECT 1 FROM f_vaccine_person_risk_type WHERE f_vaccine_person_risk_type_id = 402); 
COMMIT;
BEGIN;
INSERT INTO f_vaccine_person_risk_type SELECT 403,'เจ้าหน้าที่ที่ปฏิบัติหน้าที่ ณ ช่องทางเข้าออกระหว่างประเทศ ที่ปฏิบัติหน้าที่ควบคุมโรคชายแดน ที่เกี่ยวข้องกับการควบคุมโรคโควิด 19 ที่มีโอกาสสัมผัสผู้ป่วย',4
WHERE NOT EXISTS (SELECT 1 FROM f_vaccine_person_risk_type WHERE f_vaccine_person_risk_type_id = 403); 
COMMIT;
BEGIN;
INSERT INTO f_vaccine_person_risk_type SELECT 404,'เจ้าหน้าที่อื่นๆ ที่เกี่ยวข้องกับการควบคุมโรคโควิด 19 ที่มีโอกาสสัมผัสผู้ป่วย',4
WHERE NOT EXISTS (SELECT 1 FROM f_vaccine_person_risk_type WHERE f_vaccine_person_risk_type_id = 404); 
COMMIT;
BEGIN;
INSERT INTO f_vaccine_person_risk_type SELECT 501,'ประชาชนทั่วไป',5
WHERE NOT EXISTS (SELECT 1 FROM f_vaccine_person_risk_type WHERE f_vaccine_person_risk_type_id = 501); 
COMMIT;

-- เก็บข้อมูลประเภทกลุ่มเป้าหมาย และความต้องการฉีดวัคซีน
CREATE TABLE IF NOT EXISTS t_patient_vaccine_group (
    t_patient_vaccine_group_id      CHARACTER VARYING(50) NOT NULL,
    t_patient_id                    CHARACTER VARYING(50) NOT NULL,
    f_vaccine_person_risk_type_id   INTEGER NOT NULL,
    need_vaccine                    INTEGER NOT NULL, -- 1= Yes, 0 = No
    sent_data                       BOOLEAN NOT NULL DEFAULT FALSE,
    record_datetime                 TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    user_record_id                  CHARACTER VARYING(50) NOT NULL,
    update_datetime                 TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    user_update_id                  CHARACTER VARYING(50) NOT NULL,
CONSTRAINT t_patient_vaccine_group_pkey PRIMARY KEY (t_patient_vaccine_group_id),
CONSTRAINT t_patient_fkey FOREIGN KEY (t_patient_id) 
    REFERENCES t_patient (t_patient_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
CONSTRAINT f_vaccine_person_risk_type_fkey FOREIGN KEY (f_vaccine_person_risk_type_id) 
    REFERENCES f_vaccine_person_risk_type (f_vaccine_person_risk_type_id) ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE OR REPLACE FUNCTION moph_immunization_target_V39(patient_id text)
RETURNS TABLE (person_type json) AS $$
    DECLARE
    BEGIN
return query 
select json_build_object(
        'hospital',json_build_object(
            'hospital_code',vaccine_person_type.hospital_code
            ,'hospital_name',vaccine_person_type.hospital_name
            ,'his_identifier',vaccine_person_type.his_identifier
            )
        ,'target',json_agg(json_build_object(
            'cid',vaccine_person_type.cid
            ,'prefix',vaccine_person_type.prefix
            ,'first_name',vaccine_person_type.first_name
            ,'last_name',vaccine_person_type.last_name
            ,'birth_date',vaccine_person_type.birth_date
            ,'gender',vaccine_person_type.gender
            ,'moo',vaccine_person_type.moo
            ,'chw_code',vaccine_person_type.chw_code
            ,'amp_code',vaccine_person_type.amp_code
            ,'tmb_code',vaccine_person_type.tmb_code
            ,'mobile_phone',vaccine_person_type.mobile_phone
            ,'person_type_id',vaccine_person_type.person_type_id
            ,'person_risk_type_id',vaccine_person_type.person_risk_type_id
            ,'need_vaccine',vaccine_person_type.need_vaccine
            ,'vaccine_eligible',vaccine_person_type.vaccine_eligible
            ))
        ) as person_type
from (select b_site.b_visit_office_id as hospital_code
        ,b_site.site_full_name as hospital_name
        ,'HospitalOS ' || (select version_application_number from s_version order by s_version.version_update_time desc limit 1) as his_identifier
        ,t_patient.patient_pid as cid
        ,case when f_patient_prefix.patient_prefix_description is null then ''
              else f_patient_prefix.patient_prefix_description end as prefix
        ,t_patient.patient_firstname as first_name
        ,t_patient.patient_lastname as last_name
        ,to_char(text_to_timestamp(t_patient.patient_birthday),'YYYY-MM-DD') as birth_date
        ,case when t_patient.f_sex_id in ('1','2') then t_patient.f_sex_id
              else '0' end::int as gender
        ,case when t_patient.patient_moo is not null then t_patient.patient_moo
              else '' end as moo
        ,case when t_patient.patient_tambon is not null then substr(t_patient.patient_tambon,1,2)
              else '' end as chw_code
        ,case when t_patient.patient_tambon is not null then substr(t_patient.patient_tambon,3,2)
              else '' end as amp_code
        ,case when t_patient.patient_tambon is not null then substr(t_patient.patient_tambon,5,2)
              else '' end as tmb_code
        ,max(case when t_patient.patient_patient_mobile_phone <> '' then t_patient.patient_patient_mobile_phone
                  else '' end) as mobile_phone
        ,f_vaccine_person_type.f_vaccine_person_type_id as person_type_id
        ,f_vaccine_person_risk_type.f_vaccine_person_risk_type_id as person_risk_type_id
        ,case when t_patient_vaccine_group.need_vaccine = 1 then 'Y'
              else 'N' end as need_vaccine
        ,case when f_vaccine_person_type.auto_eligible = '1' then 'Y'
              else 'N' end as vaccine_eligible
    from t_patient
        inner join t_patient_vaccine_group on t_patient.t_patient_id = t_patient_vaccine_group.t_patient_id
        left join f_vaccine_person_risk_type on t_patient_vaccine_group.f_vaccine_person_risk_type_id = f_vaccine_person_risk_type.f_vaccine_person_risk_type_id
        left join f_vaccine_person_type on f_vaccine_person_risk_type.f_vaccine_person_type_id = f_vaccine_person_type.f_vaccine_person_type_id
        left join f_patient_prefix on t_patient.f_patient_prefix_id = f_patient_prefix.f_patient_prefix_id
        cross join b_site
    where t_patient.patient_active = '1'
        and t_patient.t_patient_id = $1
    group by b_site.b_visit_office_id
        ,b_site.site_full_name
        ,t_patient.patient_pid
        ,f_patient_prefix.patient_prefix_description
        ,t_patient.patient_firstname
        ,t_patient.patient_lastname
        ,t_patient.patient_birthday
        ,t_patient.f_sex_id
        ,t_patient.patient_moo
        ,t_patient.patient_tambon
        ,f_vaccine_person_type.f_vaccine_person_type_id
        ,f_vaccine_person_risk_type.f_vaccine_person_risk_type_id
        ,t_patient_vaccine_group.need_vaccine
     ) as vaccine_person_type
group by vaccine_person_type.hospital_code
    ,vaccine_person_type.hospital_name
    ,vaccine_person_type.his_identifier;
END;
$$
LANGUAGE 'plpgsql';

-- #320
ALTER TABLE b_health_vaccine RENAME TO b_health_vaccine_old;
ALTER TABLE b_health_vaccine_old DROP CONSTRAINT b_health_vaccine_pkey;

CREATE TABLE IF NOT EXISTS b_health_vaccine (
    b_health_vaccine_id         CHARACTER VARYING(255) NOT NULL,
    barcode                     CHARACTER VARYING(10) NOT NULL DEFAULT lpad(nextval('barcode_id'::regclass)::text, 10, '0'::text),
    vaccine_name                CHARACTER VARYING(255) NOT NULL,
    serial_number               CHARACTER VARYING(255) NOT NULL,
    lot_number                  CHARACTER VARYING(255) NOT NULL,
    f_vaccine_manufacturer_id   INTEGER NOT NULL,
    receive_date                DATE NOT NULL DEFAULT CURRENT_DATE,
    expire_date                 DATE NOT NULL DEFAULT CURRENT_DATE,
    qty                         INTEGER NOT NULL,
    dose                        INTEGER NOT NULL,
    bottle_number               INTEGER NOT NULL,
    dose_number                 INTEGER NOT NULL,
    b_health_epi_group_id       CHARACTER VARYING(255) NOT NULL,
    use_status                  CHARACTER VARYING(1) NOT NULL DEFAULT '1', --1 = ยังไม่ถูกใช้งาน, 2 = ถูกใช้งานแล้ว
    active                      CHARACTER VARYING(1) NOT NULL DEFAULT '1', --0 = inactive, 1 = active
    user_record_id              CHARACTER VARYING(255) NOT NULL,
    record_date_time            TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    user_cancel_id              CHARACTER VARYING(255),
    cancel_date_time            TIMESTAMP,
CONSTRAINT b_health_vaccine_pkey PRIMARY KEY (b_health_vaccine_id),
CONSTRAINT b_health_vaccine_unique UNIQUE (b_health_vaccine_id));

INSERT INTO b_health_vaccine(b_health_vaccine_id, barcode, vaccine_name, serial_number, lot_number, f_vaccine_manufacturer_id, receive_date, expire_date, qty, dose, bottle_number, dose_number, b_health_epi_group_id, use_status, active, user_record_id, record_date_time, user_cancel_id, cancel_date_time)
SELECT b_health_vaccine_id, barcode, vaccine_name, serial_number, lot_number, f_vaccine_manufacturer_id, receive_date, expire_date, qty, dose,dose_number as bottle_number,1 as dose_number, b_health_epi_group_id, use_status, active, user_record_id, record_date_time, user_cancel_id, cancel_date_time FROM b_health_vaccine_old;

-- #323
CREATE OR REPLACE FUNCTION moph_immunization_V39(health_vaccine_covid19_id text)
RETURNS TABLE (immunize_query json) AS $$
    DECLARE
    BEGIN
return query 
select json_build_object(
        'hospital',json_build_object(
            'hospital_code',visit_covid19.hospital_code
            ,'hospital_name',visit_covid19.hospital_name
            ,'his_identifier',visit_covid19.his_identifier
        )
        ,'patient',json_build_object(
            'CID',visit_covid19.cid
            ,'passport_no',visit_covid19.passport_no
            ,'hn',visit_covid19.hn
            ,'patient_guid',visit_covid19.patient_guid
            ,'prefix',visit_covid19.prefix
            ,'first_name',visit_covid19.first_name
            ,'last_name',visit_covid19.last_name
            ,'prefix_eng',visit_covid19.prefix_eng
            ,'first_name_eng',visit_covid19.first_name_eng
            ,'middle_name_eng',''
            ,'last_name_eng',visit_covid19.last_name_eng
            ,'gender',visit_covid19.gender
            ,'birth_date',visit_covid19.birth_date
            ,'marital_status_id',visit_covid19.marital_status_id
            ,'address',visit_covid19.address
            ,'moo',visit_covid19.moo
            ,'road',visit_covid19.road
            ,'chw_code',visit_covid19.chw_code
            ,'amp_code',visit_covid19.amp_code
            ,'tmb_code',visit_covid19.tmb_code
            ,'address_full_thai',visit_covid19.address_full_thai
            ,'address_full_english',visit_covid19.address_full_english
            ,'installed_line_connect',visit_covid19.installed_line_connect
            ,'home_phone',visit_covid19.home_phone
            ,'mobile_phone',visit_covid19.mobile_phone
            ,'nationality',visit_covid19.nationality
            ,'ncd',COALESCE(json_agg(visit_covid19.ncd) FILTER (WHERE visit_covid19.ncd IS NOT NULL), '[]')
            )
        ,'lab',COALESCE(json_agg(visit_covid19.lab) FILTER (WHERE visit_covid19.lab IS NOT NULL), '[]')
    ,'immunization_plan',json_agg(json_build_object(
            'vaccine_code',visit_covid19.vaccine_code
            ,'immunization_plan_ref_code',visit_covid19.immunization_plan_ref_code
            ,'treatment_plan_name',visit_covid19.treatment_plan_name
            ,'practitioner_license_number',visit_covid19.practitioner_license_number
            ,'practitioner_name',visit_covid19.practitioner_name
            ,'practitioner_role',visit_covid19.practitioner_role
            ,'vaccine_ref_name',visit_covid19.vaccine_ref_name
            ,'schedule',visit_covid19.schedule
            ))
    ,'visit',json_build_object(
        'visit_guid',visit_covid19.visit_guid
        ,'visit_ref_code',visit_covid19.visit_ref_code
        ,'visit_datetime',visit_covid19.visit_datetime
        ,'claim_fund_pcode',visit_covid19.claim_fund_pcode
        ,'visit_observation',json_build_object(
                'systolic_blood_pressure',visit_covid19.systolic_blood_pressure
                ,'diastolic_blood_pressure',visit_covid19.diastolic_blood_pressure
                ,'body_weight_kg',visit_covid19.body_weight_kg
                ,'body_height_cm',visit_covid19.body_height_cm
                ,'temperature',visit_covid19.temperature)
        ,'visit_immunization',json_agg(json_build_object(
                'visit_immunization_ref_code',visit_covid19.visit_immunization_ref_code
                ,'immunization_datetime',visit_covid19.immunization_datetime
                ,'vaccine_code',visit_covid19.vaccine_code
                ,'lot_number',visit_covid19.lot_number
                ,'expiration_date',visit_covid19.expiration_date
                ,'vaccine_note',visit_covid19.vaccine_note
                ,'vaccine_ref_name',visit_covid19.vaccine_ref_name
                ,'serial_no',visit_covid19.serial_no
                ,'vaccine_manufacturer',visit_covid19.vaccine_manufacturer
                ,'vaccine_plan_no',visit_covid19.vaccine_plan_no
                ,'vaccine_route_name',visit_covid19.vaccine_route_name
                ,'practitioner',json_build_object(
                        'license_number',visit_covid19.immunize_license_number
                        ,'name',visit_covid19.immunize_name
                        ,'role',visit_covid19.immunize_role
                        )
                ,'immunization_plan_ref_code',visit_covid19.immunization_plan_ref_code
                ,'immunization_plan_schedule_ref_code',visit_covid19.immunization_plan_schedule_ref_code
                ))
        ,'visit_immunization_reaction',visit_covid19.visit_immunization_reaction::json
        ,'appointment',json_agg(json_build_object(
                'appointment_ref_code',visit_covid19.appointment_ref_code
                ,'appointment_datetime',visit_covid19.appointment_datetime
                ,'appointment_note',visit_covid19.appointment_note
                ,'appointment_cause',visit_covid19.appointment_cause
                ,'provis_aptype_code',visit_covid19.provis_aptype_code
                ,'practitioner',json_build_object(
                        'license_number',visit_covid19.appointment_license_number
                        ,'name',visit_covid19.appointment_name
                        ,'role',visit_covid19.appointment_role
                        )
                ))
        )
    )::json as immunize_query
from (select visit_covid19.hospital_code
            ,visit_covid19.hospital_name
            ,visit_covid19.his_identifier
            ,visit_covid19.cid
            ,visit_covid19.passport_no
            ,visit_covid19.hn
            ,visit_covid19.patient_guid
            ,visit_covid19.prefix
            ,visit_covid19.first_name
            ,visit_covid19.last_name
            ,visit_covid19.prefix_eng
            ,visit_covid19.first_name_eng
            ,visit_covid19.last_name_eng
            ,visit_covid19.gender
            ,visit_covid19.birth_date
            ,visit_covid19.marital_status_id
            ,visit_covid19.address
            ,visit_covid19.moo
            ,visit_covid19.road
            ,visit_covid19.chw_code
            ,visit_covid19.amp_code
            ,visit_covid19.tmb_code
            ,visit_covid19.address_full_thai
            ,visit_covid19.address_full_english
            ,visit_covid19.nationality
            ,visit_covid19.installed_line_connect
            ,visit_covid19.home_phone
            ,visit_covid19.mobile_phone
            ,visit_covid19.ncd
            ,visit_covid19.lab
            ,visit_covid19.vaccine_code
            ,visit_covid19.immunization_plan_ref_code
            ,visit_covid19.treatment_plan_name
            ,visit_covid19.practitioner_license_number
            ,visit_covid19.practitioner_name
            ,visit_covid19.practitioner_role
            ,visit_covid19.vaccine_ref_name
            ,json_agg(visit_covid19.schedule) as schedule
            ,visit_covid19.visit_guid
            ,visit_covid19.visit_ref_code
            ,visit_covid19.visit_datetime
            ,visit_covid19.claim_fund_pcode
            ,visit_covid19.systolic_blood_pressure
            ,visit_covid19.diastolic_blood_pressure
            ,visit_covid19.body_weight_kg
            ,visit_covid19.body_height_cm
            ,visit_covid19.temperature
            ,visit_covid19.visit_immunization_ref_code
            ,visit_covid19.immunization_datetime
            ,visit_covid19.lot_number
            ,visit_covid19.expiration_date
            ,visit_covid19.vaccine_note
            ,visit_covid19.serial_no
            ,visit_covid19.vaccine_manufacturer
            ,visit_covid19.vaccine_plan_no
            ,visit_covid19.vaccine_route_name
            ,visit_covid19.immunize_license_number
            ,visit_covid19.immunize_name
            ,visit_covid19.immunize_role
            ,visit_covid19.immunization_plan_schedule_ref_code
            ,visit_covid19.visit_immunization_reaction
            ,case when visit_covid19.appointment_ref_code is not null 
				  then visit_covid19.appointment_ref_code::text
				  else '' end as appointment_ref_code
            ,visit_covid19.appointment_datetime
            ,visit_covid19.appointment_note
            ,visit_covid19.appointment_cause
            ,visit_covid19.provis_aptype_code
            ,visit_covid19.appointment_license_number
            ,visit_covid19.appointment_name
            ,visit_covid19.appointment_role
    from (select b_site.b_visit_office_id as hospital_code
            ,b_site.site_full_name as hospital_name
            ,'HospitalOS ' || (select version_application_number from s_version order by s_version.version_update_time desc limit 1) as his_identifier
            ,t_patient.patient_pid as cid
            ,t_health_family.passport_no as passport_no
            ,t_patient.patient_hn as hn
            ,case when t_patient.patient_guid is not null then '{' || upper(t_patient.patient_guid::text) || '}'
                  else '' end as patient_guid
            ,case when f_patient_prefix.patient_prefix_description is null then ''
                  else f_patient_prefix.patient_prefix_description end as prefix
            ,t_patient.patient_firstname as first_name
            ,t_patient.patient_lastname as last_name
            ,case when f_patient_prefix.patient_prefix_description_eng is null then ''
                  else f_patient_prefix.patient_prefix_description_eng end as prefix_eng
            ,case when t_health_family.patient_firstname_eng is null then ''
                  else t_health_family.patient_firstname_eng end as first_name_eng
            ,case when t_health_family.patient_lastname_eng is null then ''
                  else t_health_family.patient_lastname_eng end as last_name_eng
            ,case when t_patient.f_sex_id in ('1','2') then t_patient.f_sex_id
                  else '0' end::int as gender
            ,to_char(text_to_timestamp(t_patient.patient_birthday),'YYYY-MM-DD') as birth_date
            ,case when t_patient.f_patient_marriage_status_id <> '' then t_patient.f_patient_marriage_status_id::int
                  else null end as marital_status_id
            ,max(case when t_patient.patient_phone_number <> '' then t_patient.patient_phone_number
                  else '' end) as home_phone
            ,max(case when t_patient.patient_patient_mobile_phone <> '' then t_patient.patient_patient_mobile_phone
                  else '' end) as mobile_phone
            ,case when t_health_vaccine_covid19.line_hmo_promp = true then 'Y'
                  else 'N' end as installed_line_connect
            ,case when t_patient.patient_house <> '' then t_patient.patient_house
                  else '' end as address
            ,case when t_patient.patient_moo <> '' then t_patient.patient_moo
                  else '' end as moo
            ,case when t_patient.patient_road <> '' then t_patient.patient_road
                  else '' end as road
            ,case when t_patient.patient_tambon <> '' then substr(t_patient.patient_tambon,1,2)
                  else '' end as chw_code
            ,case when t_patient.patient_tambon <> '' then substr(t_patient.patient_tambon,3,2)
                  else '' end as amp_code
            ,case when t_patient.patient_tambon <> '' then substr(t_patient.patient_tambon,5,2)
                  else '' end as tmb_code
            ,case when t_patient.patient_house <> '' then t_patient.patient_house else '' end 
                || case when t_patient.patient_moo <> '' then ' หมู่ ' || t_patient.patient_moo else '' end 
                || case when t_patient.patient_road <> '' then ' ถนน' || t_patient.patient_road else '' end
                || case when t_patient.patient_tambon <> '' then ' ตำบล' || tambon.address_description else '' end
                || case when t_patient.patient_amphur <> '' then ' อำเภอ' || amphur.address_description else '' end
                || case when t_patient.patient_changwat <> '' then ' จังหวัด' || changwat.address_description else '' end
                || case when t_patient.patient_postcode <> '' then ' ' || t_patient.patient_postcode
                        when t_patient.patient_tambon <> '' then ' ' || tambon.postcode else '' end
                as address_full_thai
            ,case when t_patient.patient_house <> '' then t_patient.patient_house else '' end 
                || case when t_patient.patient_moo <> '' then ' Moo ' || t_patient.patient_moo else '' end 
                || case when t_patient.patient_road <> '' then t_patient.patient_road || ' Road' else '' end
                || case when t_patient.patient_tambon <> '' then ' ' || tambon.address_description_eng else '' end
                || case when t_patient.patient_amphur <> '' then ' ,' || amphur.address_description_eng else '' end
                || case when t_patient.patient_changwat <> '' then ' ,' || changwat.address_description_eng else '' end
                || case when t_patient.patient_postcode <> '' then ' ' || t_patient.patient_postcode
                        when t_patient.patient_tambon <> '' then ' ' || tambon.postcode else '' end
                as address_full_english
            ,f_patient_nation.r_rp1853_nation_id as nationality
            ,null as ncd
            ,null as lab
            ,b_health_epi_group.health_epi_group_description_particular as vaccine_code
            ,b_health_vaccine.f_vaccine_manufacturer_id::text || t_health_vaccine_covid19.ref_code::text as immunization_plan_ref_code
            ,t_patient_appointment.patient_appointment_name as treatment_plan_name
            ,case when prescriber_appoint.f_employee_authentication_id = '3' and prescriber_appoint.employee_number  <> ''
                        and (case when prescriber_person.t_person_id is not null
                        then prescriber_prefix.patient_prefix_description not ilike 'ทพ.%' or prescriber_prefix.patient_prefix_description not ilike 'ทพญ.%'
                        else prescriber_appoint.employee_firstname not ilike 'ทพ.%' or prescriber_appoint.employee_firstname not ilike 'ทพญ.%'  end )
                        then  'ว.'||prescriber_appoint.employee_number
                        when prescriber_appoint.f_employee_authentication_id = '3' and prescriber_appoint.employee_number  <> ''
                        and (case when prescriber_person.t_person_id is not null
                        then prescriber_prefix.patient_prefix_description ilike 'ทพ.%' or prescriber_prefix.patient_prefix_description ilike 'ทพญ.%'
                        else prescriber_appoint.employee_firstname ilike 'ทพ.%' or prescriber_appoint.employee_firstname ilike 'ทพญ.%'  end)
                        then  'ท.'||prescriber_appoint.employee_number
                        when prescriber_appoint.f_employee_authentication_id = '2' and prescriber_appoint.employee_number  <> ''  then  'พ'||prescriber_appoint.employee_number
                          when prescriber_appoint.f_employee_authentication_id = '6' and prescriber_appoint.employee_number  <> ''  then  'ภ'||prescriber_appoint.employee_number
                    when prescriber_appoint.f_employee_authentication_id not in ('2','3','6') and prescriber_appoint.employee_number  <> ''  then  '-'||prescriber_appoint.employee_number
                        else ''  end as practitioner_license_number
            ,case when prescriber_prefix.patient_prefix_description is null then ''
                  else prescriber_prefix.patient_prefix_description end
                  || prescriber_person.person_firstname
                  || ' ' || prescriber_person.person_lastname as practitioner_name
            ,f_employee_authentication.employee_authentication_description as practitioner_role
            ,b_health_vaccine.vaccine_name as vaccine_ref_name
            ,json_build_object(
                'immunization_plan_schedule_ref_code',case when t_patient_appointment.ref_code is not null then t_patient_appointment.ref_code::text else '' end
                ,'schedule_date',case when t_patient_appointment.patient_appointment_date <> '' then to_char(text_to_timestamp(t_patient_appointment.patient_appointment_date),'YYYY-MM-DD') else '' end
                ,'treatment_number',ROW_NUMBER () OVER (ORDER BY t_patient_appointment.patient_appointment_date asc)::int
                ,'schedule_description',t_patient_appointment.patient_appointment_notice
                ,'complete',case when (t_patient_appointment.patient_appointment_status <> '0' 
                                      and (t_patient_appointment.t_visit_id is not null or t_patient_appointment.t_visit_id <> '')) then 'Y'
                                 else 'N' end
                ,'visit_date',to_char(text_to_timestamp(visit_appoint.visit_begin_visit_time),'YYYY-MM-DD')) as schedule
            ,case when t_visit.visit_guid is not null then '{' || upper(t_visit.visit_guid::text) || '}'
                  else '' end as visit_guid
            ,t_visit.visit_vn || to_char(text_to_timestamp(t_visit.visit_begin_visit_time),'MMDD') as visit_ref_code
            ,to_char(text_to_timestamp(t_visit.visit_begin_visit_time),'YYYY-MM-DDThh24:mi:ss.ms') as visit_datetime
            ,b_contract_plans.contract_plans_pttype as claim_fund_pcode
            ,vital_sign.sbp as systolic_blood_pressure
            ,vital_sign.dbp as diastolic_blood_pressure
            ,vital_sign.weight as body_weight_kg
            ,vital_sign.height as body_height_cm
            ,vital_sign.temperature as temperature
            ,t_health_vaccine_covid19.ref_code::text as visit_immunization_ref_code
            ,to_char(t_health_vaccine_covid19.receive_date,'YYYY-MM-DD') 
                || 'T' || to_char(t_health_vaccine_covid19.receive_time,'HH24:MI:SS.MS') as immunization_datetime
            ,b_health_vaccine.lot_number as lot_number
            ,to_char(b_health_vaccine.expire_date,'YYYY-MM-DD') as expiration_date
            ,t_health_vaccine_covid19.description as vaccine_note
            ,b_health_vaccine.serial_number as serial_no
            ,f_vaccine_manufacturer.vaccine_manufacturer_name as vaccine_manufacturer
            ,t_health_vaccine_covid19.vaccine_no as vaccine_plan_no
            ,vaccine_instruction.vaccine_route_name
            ,case when prescriber_immunize.f_employee_authentication_id = '3' and prescriber_immunize.employee_number  <> ''
                        and (case when prescriber_immunize.t_person_id is not null
                        then immunize_prefix.patient_prefix_description not ilike 'ทพ.%' or immunize_prefix.patient_prefix_description not ilike 'ทพญ.%'
                        else prescriber_immunize.employee_firstname not ilike 'ทพ.%' or prescriber_immunize.employee_firstname not ilike 'ทพญ.%'  end )
                        then  'ว.'||prescriber_immunize.employee_number
                        when prescriber_immunize.f_employee_authentication_id = '3' and prescriber_immunize.employee_number  <> ''
                        and (case when prescriber_immunize.t_person_id is not null
                        then immunize_prefix.patient_prefix_description ilike 'ทพ.%' or immunize_prefix.patient_prefix_description ilike 'ทพญ.%'
                        else prescriber_immunize.employee_firstname ilike 'ทพ.%' or prescriber_immunize.employee_firstname ilike 'ทพญ.%'  end)
                        then  'ท.'||prescriber_immunize.employee_number
                        when prescriber_immunize.f_employee_authentication_id = '2' and prescriber_immunize.employee_number  <> ''  then  'พ'||prescriber_immunize.employee_number
                          when prescriber_immunize.f_employee_authentication_id = '6' and prescriber_immunize.employee_number  <> ''  then  'ภ'||prescriber_immunize.employee_number
                    when prescriber_immunize.f_employee_authentication_id not in ('2','3','6') and prescriber_immunize.employee_number  <> ''  then  '-'||prescriber_immunize.employee_number
                        else ''  end as immunize_license_number
            ,case when immunize_prefix.patient_prefix_description is null then ''
                  else immunize_prefix.patient_prefix_description end
                  || immunize_person.person_firstname
                  || ' ' || immunize_person.person_lastname as immunize_name
            ,immunize_authen.employee_authentication_description as immunize_role
            ,vaccine_instruction.ref_code::text as immunization_plan_schedule_ref_code
            ,case when reaction.visit_immunization_reaction::text is null then '[]' else reaction.visit_immunization_reaction::text end as visit_immunization_reaction
            ,appointment.appointment_ref_code::text as appointment_ref_code
            ,appointment.appointment_datetime
            ,appointment.appointment_note
            ,appointment.appointment_cause
            ,appointment.provis_aptype_code
            ,appointment.appointment_license_number
            ,appointment.appointment_name
            ,appointment.appointment_role
        from t_health_vaccine_covid19
            inner join b_health_epi_group on t_health_vaccine_covid19.b_health_epi_group_id = b_health_epi_group.b_health_epi_group_id
            inner join b_health_vaccine on t_health_vaccine_covid19.b_health_vaccine_id = b_health_vaccine.b_health_vaccine_id
            left join f_vaccine_manufacturer on b_health_vaccine.f_vaccine_manufacturer_id = f_vaccine_manufacturer.f_vaccine_manufacturer_id
            left join (select t_health_vaccine_covid19.t_health_vaccine_covid19_id
                            ,COALESCE(json_agg(reaction.visit_immunization_reaction) FILTER (WHERE reaction.visit_immunization_reaction IS NOT NULL), '[]') as visit_immunization_reaction
                        from t_health_vaccine_covid19
                            inner join (select t_health_vaccine_covid19.t_health_vaccine_covid19_id
                                ,json_build_object(
                                            'visit_immunization_reaction_ref_code',case when t_health_vaccine_covid19_reaction.ref_code is not null
                                              then t_health_vaccine_covid19_reaction.ref_code::text else '' end
                                            ,'visit_immunization_ref_code',case when t_health_vaccine_covid19.ref_code is not null
                                              then t_health_vaccine_covid19.ref_code::text else '' end
                                            ,'report_datetime',to_char(t_health_vaccine_covid19_reaction.record_date_time,'YYYY-MM-DDThh24:mi:ss.ms')
                                            ,'reaction_detail_text',case when t_health_vaccine_covid19_reaction.f_vaccine_reaction_symptom_id = 10 
                                              then f_vaccine_reaction_symptom.vaccine_reaction_symptom_name || ' : ' || t_health_vaccine_covid19_reaction.symptom_description
                                              when (t_health_vaccine_covid19_reaction.f_vaccine_reaction_symptom_id is not null 
                                                    and t_health_vaccine_covid19_reaction.f_vaccine_reaction_symptom_id <> 10)
                                              then f_vaccine_reaction_symptom.vaccine_reaction_symptom_name 
                                              else '' end
                                            ,'vaccine_reaction_type_id',t_health_vaccine_covid19_reaction.f_vaccine_reaction_type_id
                                            ,'reaction_date',to_char(t_health_vaccine_covid19_reaction.reaction_date,'YYYY-MM-DD')
                                            ,'vaccine_reaction_stage_id',t_health_vaccine_covid19_reaction.f_vaccine_reaction_stage_id
                                            ,'vaccine_reaction_symptom_id',t_health_vaccine_covid19_reaction.f_vaccine_reaction_symptom_id
                                            ) as visit_immunization_reaction 
                            from t_health_vaccine_covid19
                                inner join t_health_vaccine_covid19_reaction on t_health_vaccine_covid19.t_health_vaccine_covid19_id
                                                                                = t_health_vaccine_covid19_reaction.t_health_vaccine_covid19_id
                                                                             and t_health_vaccine_covid19_reaction.active = '1'
                                left join f_vaccine_reaction_symptom on t_health_vaccine_covid19_reaction.f_vaccine_reaction_symptom_id
                                                                        = f_vaccine_reaction_symptom.f_vaccine_reaction_symptom_id
                            where t_health_vaccine_covid19.t_health_vaccine_covid19_id = $1
                                and t_health_vaccine_covid19.active = '1'
                            group by t_health_vaccine_covid19.t_health_vaccine_covid19_id
                                ,t_health_vaccine_covid19_reaction.ref_code
                                ,t_health_vaccine_covid19_reaction.record_date_time
                                ,t_health_vaccine_covid19_reaction.f_vaccine_reaction_symptom_id
                                ,f_vaccine_reaction_symptom.vaccine_reaction_symptom_name
                                ,t_health_vaccine_covid19_reaction.symptom_description
                                ,t_health_vaccine_covid19_reaction.f_vaccine_reaction_type_id
                                ,t_health_vaccine_covid19_reaction.reaction_date
                                ,t_health_vaccine_covid19_reaction.f_vaccine_reaction_stage_id
                            ) as reaction on t_health_vaccine_covid19.t_health_vaccine_covid19_id = reaction.t_health_vaccine_covid19_id
                        group by t_health_vaccine_covid19.t_health_vaccine_covid19_id
                   ) as reaction on t_health_vaccine_covid19.t_health_vaccine_covid19_id = reaction.t_health_vaccine_covid19_id
            inner join t_visit on t_health_vaccine_covid19.t_visit_id = t_visit.t_visit_id
                              and t_visit.f_visit_status_id <> '4'
            inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
            inner join t_health_family on t_patient.t_health_family_id = t_health_family.t_health_family_id
            left join t_patient_appointment on t_visit.t_visit_id = t_patient_appointment.visit_id_make_appointment
                                           and t_patient_appointment.patient_appointment_active = '1'
                                           and t_patient_appointment.patient_appointment_use_set = true
            inner join t_visit_payment on t_visit.t_visit_id = t_visit_payment.t_visit_id
                                      and t_visit_payment.visit_payment_priority = '0'
                                      and t_visit_payment.visit_payment_active = '1'
            left join b_contract_plans on t_visit_payment.b_contract_plans_id = b_contract_plans.b_contract_plans_id
            left join t_visit as visit_appoint on t_patient_appointment.t_visit_id = visit_appoint.t_visit_id
                                              and visit_appoint.f_visit_status_id <> '4'
            left join (select t_visit_vital_sign.t_visit_id
                        ,case when t_visit_vital_sign.visit_vital_sign_blood_presure <> ''
                              then cast(substring(t_visit_vital_sign.visit_vital_sign_blood_presure,1
                                    ,(position('/' in t_visit_vital_sign.visit_vital_sign_blood_presure)-1)) as numeric)
                              else null end as sbp
                        ,case when t_visit_vital_sign.visit_vital_sign_blood_presure <> ''
                              then cast(substring(t_visit_vital_sign.visit_vital_sign_blood_presure
                                    ,(position('/' in t_visit_vital_sign.visit_vital_sign_blood_presure)+1)) as numeric)
                              else null end as dbp
                        ,case when t_visit_vital_sign.visit_vital_sign_weight <> ''
                              then cast(t_visit_vital_sign.visit_vital_sign_weight as numeric)
                              else 0 end as weight
                        ,case when t_visit_vital_sign.visit_vital_sign_height <> ''
                              then cast(t_visit_vital_sign.visit_vital_sign_height as numeric)
                              else 0 end as height
                        ,case when t_visit_vital_sign.visit_vital_sign_temperature <> ''
                              then cast(t_visit_vital_sign.visit_vital_sign_temperature as numeric)
                              else 0 end as temperature
                    from t_visit_vital_sign
                        inner join (select t_visit_vital_sign.t_visit_id
                                        ,max(text_to_timestamp(t_visit_vital_sign.record_date || ',' || t_visit_vital_sign.record_time)) as record_date_time
                                    from t_visit_vital_sign
                                        inner join t_visit on t_visit_vital_sign.t_visit_id = t_visit.t_visit_id
                                                              and t_visit.f_visit_status_id <> '4'
                                        inner join t_health_vaccine_covid19 on t_visit.t_visit_id = t_health_vaccine_covid19.t_visit_id
                                    where t_visit_vital_sign.visit_vital_sign_active = '1'
                                        and t_health_vaccine_covid19.active = '1'
                                        and t_health_vaccine_covid19.t_health_vaccine_covid19_id = $1
                                    group by t_visit_vital_sign.t_visit_id
                                    ) as max_vital_sign on t_visit_vital_sign.t_visit_id = max_vital_sign.t_visit_id
                                                       and text_to_timestamp(t_visit_vital_sign.record_date || ',' || t_visit_vital_sign.record_time) = max_vital_sign.record_date_time
                                ) as vital_sign on t_visit.t_visit_id = vital_sign.t_visit_id
            left join (select t_order.t_visit_id
                            ,t_order.b_item_id
                            ,b_item_drug_instruction.item_drug_instruction_description as vaccine_route_name
                            ,t_patient_appointment.ref_code
                        from t_order
                            inner join t_visit on t_order.t_visit_id = t_visit.t_visit_id
                                              and t_visit.f_visit_status_id <> '4'
                            inner join t_patient_appointment on t_visit.t_visit_id = t_patient_appointment.t_visit_id
                                                           and t_patient_appointment.patient_appointment_active = '1'
                            inner join t_patient_appointment_order on t_patient_appointment.t_patient_appointment_id = t_patient_appointment_order.t_patient_appointment_id
                                                                  and t_order.b_item_id = t_patient_appointment_order.b_item_id
                            inner join t_order_drug on t_order.t_order_id = t_order_drug.t_order_id
                                                   and t_order_drug.order_drug_active = '1'
                            left join b_item_drug_instruction on t_order_drug.b_item_drug_instruction_id = b_item_drug_instruction.b_item_drug_instruction_id
                            inner join t_health_vaccine_covid19 on t_visit.t_visit_id = t_health_vaccine_covid19.t_visit_id
                        where t_order.f_order_status_id not in ('0','3')
                            and t_health_vaccine_covid19.active = '1'
                            and t_health_vaccine_covid19.t_health_vaccine_covid19_id = $1
                        group by t_order.t_visit_id
                            ,t_order.b_item_id
                            ,t_patient_appointment.ref_code
                            ,b_item_drug_instruction.item_drug_instruction_description  
                      ) as vaccine_instruction on t_visit.t_visit_id = vaccine_instruction.t_visit_id
            left join (select t_visit.t_visit_id
                            ,t_visit.t_patient_id
                            ,t_patient_appointment.ref_code as appointment_ref_code
                            ,to_char(text_to_timestamp(t_patient_appointment.patient_appointment_date 
                                || ',' || t_patient_appointment.patient_appointment_time),'YYYY-MM-DDThh24:mi:ss.ms') as appointment_datetime
                            ,t_patient_appointment.patient_appointment_notice as appointment_note
                            ,t_patient_appointment.patient_appointment as appointment_cause
                            ,t_patient_appointment.r_rp1853_aptype_id as provis_aptype_code
                            ,case when b_employee.f_employee_authentication_id = '3' and b_employee.employee_number  <> ''
                                        and (case when t_person.t_person_id is not null
                                        then f_patient_prefix.patient_prefix_description not ilike 'ทพ.%' or f_patient_prefix.patient_prefix_description not ilike 'ทพญ.%'
                                        else b_employee.employee_firstname not ilike 'ทพ.%' or b_employee.employee_firstname not ilike 'ทพญ.%'  end )
                                        then  'ว.'||b_employee.employee_number
                                        when b_employee.f_employee_authentication_id = '3' and b_employee.employee_number  <> ''
                                        and (case when t_person.t_person_id is not null
                                        then f_patient_prefix.patient_prefix_description ilike 'ทพ.%' or f_patient_prefix.patient_prefix_description ilike 'ทพญ.%'
                                        else b_employee.employee_firstname ilike 'ทพ.%' or b_employee.employee_firstname ilike 'ทพญ.%'  end)
                                        then  'ท.'||b_employee.employee_number
                                        when b_employee.f_employee_authentication_id = '2' and b_employee.employee_number  <> ''  then  'พ'||b_employee.employee_number
                                          when b_employee.f_employee_authentication_id = '6' and b_employee.employee_number  <> ''  then  'ภ'||b_employee.employee_number
                                    when b_employee.f_employee_authentication_id not in ('2','3','6') and b_employee.employee_number  <> ''  then  '-'||b_employee.employee_number
                                        else ''  end as appointment_license_number
                            ,case when f_patient_prefix.patient_prefix_description is null then ''
                                  else f_patient_prefix.patient_prefix_description end
                                  || t_person.person_firstname
                                  || ' ' || t_person.person_lastname as appointment_name
                            ,f_employee_authentication.employee_authentication_description as appointment_role
                        from t_patient_appointment
                            inner join (select t_patient_appointment.t_patient_id
                                            ,t_patient_appointment.visit_id_make_appointment as t_visit_id
                                            ,min(t_patient_appointment.patient_appointment_date) as appointment_date
                                        from t_patient_appointment
                                            inner join t_visit on t_patient_appointment.visit_id_make_appointment = t_visit.t_visit_id
                                            inner join t_health_vaccine_covid19 on t_visit.t_visit_id = t_health_vaccine_covid19.t_visit_id
                                        where t_patient_appointment.patient_appointment_active = '1'
                                            and text_to_timestamp(t_visit.visit_begin_visit_time)::date < text_to_timestamp(t_patient_appointment.patient_appointment_date)::date
											and t_patient_appointment.patient_appointment_use_set = true
                                            and t_health_vaccine_covid19.active = '1'
                                            and t_health_vaccine_covid19.t_health_vaccine_covid19_id = $1
                                        group by t_patient_appointment.t_patient_id
                                            ,t_patient_appointment.visit_id_make_appointment
                                        ) as next_appointment on t_patient_appointment.t_patient_id = next_appointment.t_patient_id
                                                             and t_patient_appointment.visit_id_make_appointment = next_appointment.t_visit_id
                                                             and t_patient_appointment.patient_appointment_date = next_appointment.appointment_date
                            inner join t_visit on t_patient_appointment.visit_id_make_appointment = t_visit.t_visit_id
                            inner join t_health_vaccine_covid19 on t_visit.t_visit_id = t_health_vaccine_covid19.t_visit_id
                            inner join b_employee on t_patient_appointment.patient_appointment_doctor = b_employee.b_employee_id
                            left join t_person on b_employee.t_person_id = t_person.t_person_id
                            left join f_patient_prefix on t_person.f_prefix_id = f_patient_prefix.f_patient_prefix_id
                            left join f_employee_authentication on b_employee.f_employee_authentication_id = f_employee_authentication.f_employee_authentication_id
                        where t_patient_appointment.patient_appointment_active = '1'
                            and t_patient_appointment.patient_appointment_use_set = true
                            and t_health_vaccine_covid19.active = '1'
                            and t_health_vaccine_covid19.t_health_vaccine_covid19_id = $1
                        group by t_visit.t_visit_id
                            ,t_visit.t_patient_id
                            ,t_patient_appointment.ref_code
                            ,t_patient_appointment.patient_appointment_date
                            ,t_patient_appointment.patient_appointment_time
                            ,t_patient_appointment.patient_appointment_notice
                            ,t_patient_appointment.patient_appointment
                            ,t_patient_appointment.r_rp1853_aptype_id
                            ,b_employee.f_employee_authentication_id
                            ,b_employee.employee_number
                            ,t_person.t_person_id
                            ,f_patient_prefix.patient_prefix_description
                            ,b_employee.employee_firstname
                            ,f_employee_authentication.employee_authentication_description
                      ) as appointment on t_visit.t_visit_id = appointment.t_visit_id
                                      and t_visit.t_patient_id = appointment.t_patient_id
            left join b_employee as prescriber_appoint on t_patient_appointment.patient_appointment_doctor = prescriber_appoint.b_employee_id
            left join t_person as prescriber_person on prescriber_appoint.t_person_id = prescriber_person.t_person_id
            left join f_patient_prefix as prescriber_prefix on prescriber_person.f_prefix_id = prescriber_prefix.f_patient_prefix_id
            left join f_employee_authentication on prescriber_appoint.f_employee_authentication_id = f_employee_authentication.f_employee_authentication_id
            left join b_employee as prescriber_immunize on t_health_vaccine_covid19.user_procedure_id = prescriber_immunize.b_employee_id
            left join t_person as immunize_person on prescriber_immunize.t_person_id = immunize_person.t_person_id
            left join f_patient_prefix as immunize_prefix on immunize_person.f_prefix_id = immunize_prefix.f_patient_prefix_id
            left join f_employee_authentication as immunize_authen on prescriber_immunize.f_employee_authentication_id = immunize_authen.f_employee_authentication_id
            left join b_template_appointment on b_template_appointment.b_template_appointment_id = t_patient_appointment.b_template_appointment_id
            left join f_patient_prefix on t_patient.f_patient_prefix_id = f_patient_prefix.f_patient_prefix_id
            left join f_patient_nation on t_patient.f_patient_nation_id = f_patient_nation.f_patient_nation_id
            left join f_address as tambon on t_patient.patient_tambon = tambon.f_address_id
            left join f_address as amphur on t_patient.patient_amphur = amphur.f_address_id
            left join f_address as changwat on t_patient.patient_changwat = changwat.f_address_id
            cross join b_site
        where t_health_vaccine_covid19.t_health_vaccine_covid19_id = $1
            and t_health_vaccine_covid19.active = '1'
        group by b_site.b_visit_office_id
            ,b_site.site_full_name
            ,t_patient.patient_pid
            ,t_health_family.passport_no
            ,t_patient.patient_hn
            ,t_patient.patient_guid
            ,f_patient_prefix.patient_prefix_description
            ,t_patient.patient_firstname
            ,t_patient.patient_lastname
            ,f_patient_prefix.patient_prefix_description_eng
            ,t_health_family.patient_firstname_eng
            ,t_health_family.patient_lastname_eng
            ,t_patient.f_sex_id
            ,t_patient.patient_birthday
            ,t_patient.f_patient_marriage_status_id
            ,t_patient.patient_house
            ,t_patient.patient_moo
            ,t_patient.patient_road
            ,t_patient.patient_tambon
            ,tambon.address_description
            ,t_patient.patient_amphur
            ,amphur.address_description
            ,t_patient.patient_changwat
            ,changwat.address_description
            ,t_patient.patient_postcode
            ,tambon.postcode
            ,tambon.address_description_eng
            ,amphur.address_description_eng
            ,changwat.address_description_eng
            ,f_patient_nation.r_rp1853_nation_id
            ,t_health_vaccine_covid19.line_hmo_promp
            ,b_health_epi_group.health_epi_group_description_particular
            ,t_patient_appointment.ref_code
            ,b_template_appointment.template_appointment_name
            ,b_health_vaccine.vaccine_name
            ,prescriber_appoint.f_employee_authentication_id
            ,prescriber_appoint.employee_number
            ,prescriber_person.t_person_id
            ,prescriber_appoint.employee_firstname
            ,prescriber_prefix.patient_prefix_description
            ,f_employee_authentication.employee_authentication_description
            ,prescriber_immunize.f_employee_authentication_id
            ,prescriber_immunize.employee_number
            ,prescriber_immunize.t_person_id
            ,immunize_prefix.patient_prefix_description
            ,prescriber_immunize.employee_firstname
            ,immunize_person.person_firstname
            ,immunize_person.person_lastname
            ,immunize_authen.employee_authentication_description
            ,t_patient_appointment.patient_appointment_name
            ,b_health_vaccine.f_vaccine_manufacturer_id
            ,t_visit.t_visit_id
            ,t_patient_appointment.patient_appointment_date
            ,t_patient_appointment.patient_appointment_notice
            ,t_patient_appointment.patient_appointment_status
            ,t_patient_appointment.t_visit_id
            ,visit_appoint.visit_begin_visit_time
            ,b_contract_plans.contract_plans_pttype
            ,vital_sign.sbp
            ,vital_sign.dbp
            ,vital_sign.weight
            ,vital_sign.height
            ,vital_sign.temperature
            ,t_health_vaccine_covid19.ref_code
            ,t_health_vaccine_covid19.receive_date
            ,t_health_vaccine_covid19.receive_time
            ,b_health_vaccine.lot_number
            ,b_health_vaccine.expire_date
            ,t_health_vaccine_covid19.description
            ,b_health_vaccine.serial_number
            ,f_vaccine_manufacturer.vaccine_manufacturer_name
            ,t_health_vaccine_covid19.vaccine_no
            ,vaccine_instruction.vaccine_route_name
            ,vaccine_instruction.ref_code
            ,reaction.visit_immunization_reaction::text
            ,appointment.appointment_ref_code
            ,appointment.appointment_datetime
            ,appointment.appointment_note
            ,appointment.appointment_cause
            ,appointment.provis_aptype_code
            ,appointment.appointment_license_number
            ,appointment.appointment_name
            ,appointment.appointment_role
        ) as visit_covid19
    group by visit_covid19.hospital_code
            ,visit_covid19.hospital_name
            ,visit_covid19.his_identifier
            ,visit_covid19.cid
            ,visit_covid19.passport_no
            ,visit_covid19.hn
            ,visit_covid19.patient_guid
            ,visit_covid19.prefix
            ,visit_covid19.first_name
            ,visit_covid19.last_name
            ,visit_covid19.prefix_eng
            ,visit_covid19.first_name_eng
            ,visit_covid19.last_name_eng
            ,visit_covid19.gender
            ,visit_covid19.birth_date
            ,visit_covid19.marital_status_id
            ,visit_covid19.address
            ,visit_covid19.moo
            ,visit_covid19.road
            ,visit_covid19.chw_code
            ,visit_covid19.amp_code
            ,visit_covid19.tmb_code
            ,visit_covid19.address_full_thai
            ,visit_covid19.address_full_english
            ,visit_covid19.installed_line_connect
            ,visit_covid19.home_phone
            ,visit_covid19.mobile_phone
            ,visit_covid19.nationality
            ,visit_covid19.ncd
            ,visit_covid19.lab
            ,visit_covid19.vaccine_code
            ,visit_covid19.immunization_plan_ref_code
            ,visit_covid19.treatment_plan_name
            ,visit_covid19.practitioner_license_number
            ,visit_covid19.practitioner_name
            ,visit_covid19.practitioner_role
            ,visit_covid19.vaccine_ref_name
            ,visit_covid19.visit_guid
            ,visit_covid19.visit_ref_code
            ,visit_covid19.visit_datetime
            ,visit_covid19.claim_fund_pcode
            ,visit_covid19.systolic_blood_pressure
            ,visit_covid19.diastolic_blood_pressure
            ,visit_covid19.body_weight_kg
            ,visit_covid19.body_height_cm
            ,visit_covid19.temperature
            ,visit_covid19.visit_immunization_ref_code
            ,visit_covid19.immunization_datetime
            ,visit_covid19.vaccine_code
            ,visit_covid19.lot_number
            ,visit_covid19.expiration_date
            ,visit_covid19.vaccine_note
            ,visit_covid19.vaccine_ref_name
            ,visit_covid19.serial_no
            ,visit_covid19.vaccine_manufacturer
            ,visit_covid19.vaccine_plan_no
            ,visit_covid19.vaccine_route_name
            ,visit_covid19.immunize_license_number
            ,visit_covid19.immunize_name
            ,visit_covid19.immunize_role
            ,visit_covid19.immunization_plan_ref_code
            ,visit_covid19.immunization_plan_schedule_ref_code
            ,visit_covid19.visit_immunization_reaction
            ,visit_covid19.appointment_ref_code
            ,visit_covid19.appointment_datetime
            ,visit_covid19.appointment_note
            ,visit_covid19.appointment_cause
            ,visit_covid19.provis_aptype_code
            ,visit_covid19.appointment_license_number
            ,visit_covid19.appointment_name
            ,visit_covid19.appointment_role
    ) as visit_covid19
group by visit_covid19.hospital_code
    ,visit_covid19.hospital_name
    ,visit_covid19.his_identifier
    ,visit_covid19.cid
    ,visit_covid19.passport_no
    ,visit_covid19.hn
    ,visit_covid19.patient_guid
    ,visit_covid19.prefix
    ,visit_covid19.first_name
    ,visit_covid19.last_name
    ,visit_covid19.prefix_eng
    ,visit_covid19.first_name_eng
    ,visit_covid19.last_name_eng
    ,visit_covid19.gender
    ,visit_covid19.birth_date
    ,visit_covid19.marital_status_id
    ,visit_covid19.address
    ,visit_covid19.moo
    ,visit_covid19.road
    ,visit_covid19.chw_code
    ,visit_covid19.amp_code
    ,visit_covid19.tmb_code
    ,visit_covid19.address_full_thai
    ,visit_covid19.address_full_english
    ,visit_covid19.installed_line_connect
    ,visit_covid19.home_phone
    ,visit_covid19.mobile_phone
    ,visit_covid19.nationality
    ,visit_covid19.vaccine_code
    ,visit_covid19.immunization_plan_ref_code
    ,visit_covid19.treatment_plan_name
    ,visit_covid19.practitioner_license_number
    ,visit_covid19.practitioner_name
    ,visit_covid19.practitioner_role
    ,visit_covid19.vaccine_ref_name
    ,visit_covid19.visit_guid
    ,visit_covid19.visit_ref_code
    ,visit_covid19.visit_datetime
    ,visit_covid19.claim_fund_pcode
    ,visit_covid19.systolic_blood_pressure
    ,visit_covid19.diastolic_blood_pressure
    ,visit_covid19.body_weight_kg
    ,visit_covid19.body_height_cm
    ,visit_covid19.temperature
    ,visit_covid19.visit_immunization_ref_code
    ,visit_covid19.immunization_datetime
    ,visit_covid19.lot_number
    ,visit_covid19.expiration_date
    ,visit_covid19.vaccine_note
    ,visit_covid19.vaccine_ref_name
    ,visit_covid19.serial_no
    ,visit_covid19.vaccine_manufacturer
    ,visit_covid19.vaccine_plan_no
    ,visit_covid19.vaccine_route_name
    ,visit_covid19.immunize_license_number
    ,visit_covid19.immunize_name
    ,visit_covid19.immunize_role
    ,visit_covid19.immunization_plan_schedule_ref_code
    ,visit_covid19.visit_immunization_reaction
    ,visit_covid19.appointment_ref_code
    ,visit_covid19.appointment_datetime
    ,visit_covid19.appointment_note
    ,visit_covid19.appointment_cause
    ,visit_covid19.provis_aptype_code
    ,visit_covid19.appointment_license_number
    ,visit_covid19.appointment_name
    ,visit_covid19.appointment_role;

END;
$$
LANGUAGE 'plpgsql';

INSERT INTO s_health_version VALUES ('9710000000009','9','PCU, Community Edition','1.32.0','1.3.0', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

insert into s_script_update_log values ('pcu','update_pcu_ph9.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'update PCU ph8 -> ph9');
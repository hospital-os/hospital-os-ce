CREATE INDEX ON public.t_notify_note USING btree(f_notify_type_id,t_patient_hn,active);
CREATE INDEX ON public.t_notify_note USING btree(t_notify_note_id,show_with_custom);
CREATE INDEX ON public.t_billing_receipt_billing_subgroup USING btree(t_billing_receipt_id,billing_receipt_billing_subgroup_active,billing_receipt_billing_subgroup_paid);
CREATE INDEX ON public.t_billing_receipt_item USING btree(t_billing_receipt_id,billing_receipt_item_active);
CREATE INDEX ON public.b_item USING btree(item_active);
CREATE INDEX ON public.t_visit_queue_lab USING btree(visit_queue_lab_remain);
CREATE INDEX ON public.t_visit_queue_coding USING btree(t_visit_id);
CREATE INDEX ON public.b_icd10 USING btree(active);
CREATE INDEX ON public.t_notify_note USING btree(t_notify_note_id,t_patient_hn,f_notify_type_id,active,show_on_visit);
CREATE INDEX ON public.t_visit USING btree(f_visit_type_id,f_visit_status_id);
CREATE INDEX ON public.t_visit USING btree(t_visit_id,f_visit_status_id);
CREATE INDEX ON public.t_order USING btree(t_order_id,f_order_status_id);
CREATE INDEX ON public.t_order(f_order_status_id,t_patient_id);


-- update db version
INSERT INTO s_version VALUES ('9701000000100', '100', 'Hospital OS, Community Edition', '3.9.64b04', '3.44.2', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_64_b04.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.64b04');
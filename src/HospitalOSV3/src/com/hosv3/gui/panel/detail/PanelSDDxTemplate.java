/*
 * PanelSDDxTemplate.java
 *
 * Created on April 9, 2009, 5:12 PM
 */
package com.hosv3.gui.panel.detail;

import com.hospital_os.object.DxTemplate;
import com.hospital_os.object.DxTemplateMapItem;
import com.hospital_os.object.DxTemplateMapItemRisk;
import com.hospital_os.object.ICD10;
import com.hospital_os.object.Item;
import com.hospital_os.usecase.connection.Persistent;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hospital_os.utility.ComboboxModel;
import com.hospital_os.utility.Gutil;
import com.hospital_os.utility.TaBleModel;
import com.hosv3.control.HosControl;
import com.hosv3.control.LookupControl;
import com.hosv3.control.SetupControl;
import com.hosv3.control.lookup.ItemDrugLookup;
import com.hosv3.control.lookup.ItemLookup;
import com.hosv3.gui.component.PanelSetupImp;
import com.hosv3.object.HosObject;
import com.hosv3.subject.HosSubject;
import com.hosv3.subject.SetupSubject;
import com.hosv3.utility.Constant;
import com.hosv3.utility.GuiLang;
import java.awt.event.KeyEvent;
import java.util.Vector;

/**
 *
 * @author Administrator
 */
@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public class PanelSDDxTemplate extends javax.swing.JPanel implements PanelSetupImp {

    private static final long serialVersionUID = 1L;
    UpdateStatus theUS;
    HosControl theHC;
    HosObject theHO;
    HosSubject theHS;
    SetupControl theSetupControl;
    LookupControl theLookupControl;
    SetupSubject theSetupSubject;
    private DxTemplate theDxTemplate2;
    Item theItem;
    ICD10 theICD10;
    UpdateStatus aFrameSetup;
    Vector dxtemplete = new Vector();
//    private String guideafterdx = "";
    int offset = 22;
    int next = 0;
    int prev = 0;
    int saved = 0; // 0 ��� �������ö insert�� 1 ��� insert ��
    int category = 0;
    /**
     * pu : 10/08/2549 : �� Index �ͧ Item �����ҧ�ش�ͧ˹�һѨ�غѹ
     */
    int curNext = 0;
    /**
     * pu : 10/08/2549 : �� Index �ͧ Item �����ҧ�ش�ͧ˹�ҡ�͹˹�һѨ�غѹ
     */
    int curPrev = 0;
    String[] col = {"Diagnosis"};
    String[] col_ItemDx = {"����", "������¡�õ�Ǩ�ѡ��"};

    public PanelSDDxTemplate() {
        initComponents();
        setLanguage();
    }

    public PanelSDDxTemplate(HosControl hc, UpdateStatus us) {
        initComponents();
        setLanguage();
        setControl(hc, us);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroupIcd = new javax.swing.ButtonGroup();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTextFieldCode = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jTextFieldIcdCode = new javax.swing.JTextField();
        jRadioButtonGroup = new javax.swing.JRadioButton();
        jRadioButtonCode = new javax.swing.JRadioButton();
        jPanel7 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextAreaGuide = new javax.swing.JTextArea();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jTextFieldIcdThaiName = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        balloonTextFieldItem = new com.hosv3.gui.component.BalloonTextField();
        jPanel6 = new javax.swing.JPanel();
        jButtonItemDel = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTableItemDx = new javax.swing.JTable();
        jComboBoxClinic = new javax.swing.JComboBox();
        jLabel8 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        balloonTextFieldItemDrug = new com.hosv3.gui.component.BalloonTextField();
        jPanel9 = new javax.swing.JPanel();
        jButtonItemRiskDel = new javax.swing.JButton();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTableItemRiskDx = new javax.swing.JTable();

        setLayout(new java.awt.GridBagLayout());

        jPanel2.setMinimumSize(new java.awt.Dimension(300, 350));
        jPanel2.setPreferredSize(new java.awt.Dimension(300, 350));
        jPanel2.setLayout(new java.awt.GridBagLayout());

        jLabel1.setFont(jLabel1.getFont());
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("com/hosv3/property/thai"); // NOI18N
        jLabel1.setText(bundle.getString("Description")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 0, 5);
        jPanel2.add(jLabel1, gridBagConstraints);

        jTextFieldCode.setFont(jTextFieldCode.getFont());
        jTextFieldCode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldCodeActionPerformed(evt);
            }
        });
        jTextFieldCode.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldCodeKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 5);
        jPanel2.add(jTextFieldCode, gridBagConstraints);

        jLabel2.setFont(jLabel2.getFont());
        jLabel2.setText("�����ä");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 0, 5);
        jPanel2.add(jLabel2, gridBagConstraints);

        jTextFieldIcdCode.setFont(jTextFieldIcdCode.getFont());
        jTextFieldIcdCode.setMaximumSize(new java.awt.Dimension(50, 21));
        jTextFieldIcdCode.setMinimumSize(new java.awt.Dimension(50, 21));
        jTextFieldIcdCode.setPreferredSize(new java.awt.Dimension(50, 21));
        jTextFieldIcdCode.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextFieldIcdCodeFocusLost(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 0, 0, 5);
        jPanel2.add(jTextFieldIcdCode, gridBagConstraints);

        buttonGroupIcd.add(jRadioButtonGroup);
        jRadioButtonGroup.setFont(jRadioButtonGroup.getFont());
        jRadioButtonGroup.setText("�����");
        jRadioButtonGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonGroupActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 0, 0, 5);
        jPanel2.add(jRadioButtonGroup, gridBagConstraints);

        buttonGroupIcd.add(jRadioButtonCode);
        jRadioButtonCode.setFont(jRadioButtonCode.getFont());
        jRadioButtonCode.setText("����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 0, 0, 12);
        jPanel2.add(jRadioButtonCode, gridBagConstraints);

        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder("���й���ѧ��Ǩ"));
        jPanel7.setLayout(new java.awt.GridBagLayout());

        jScrollPane2.setMaximumSize(new java.awt.Dimension(103, 30));
        jScrollPane2.setMinimumSize(new java.awt.Dimension(103, 30));
        jScrollPane2.setPreferredSize(new java.awt.Dimension(103, 30));

        jTextAreaGuide.setFont(jTextAreaGuide.getFont());
        jTextAreaGuide.setLineWrap(true);
        jTextAreaGuide.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextAreaGuideKeyReleased(evt);
            }
        });
        jScrollPane2.setViewportView(jTextAreaGuide);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 3, 5);
        jPanel7.add(jScrollPane2, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        jPanel2.add(jPanel7, gridBagConstraints);

        jLabel5.setFont(jLabel5.getFont());
        jLabel5.setText("������ F1 �������͡���й���ѧ��Ǩ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 3, 5);
        jPanel2.add(jLabel5, gridBagConstraints);

        jLabel6.setFont(jLabel6.getFont());
        jLabel6.setText("�����ä");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 0, 5);
        jPanel2.add(jLabel6, gridBagConstraints);

        jTextFieldIcdThaiName.setFont(jTextFieldIcdThaiName.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 0, 0, 5);
        jPanel2.add(jTextFieldIcdThaiName, gridBagConstraints);

        jLabel7.setFont(jLabel7.getFont());
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("����Ѻ�ʴ��������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.insets = new java.awt.Insets(2, 0, 0, 5);
        jPanel2.add(jLabel7, gridBagConstraints);

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("��¡�õ�Ǩ�ѡ�Ңͧ Dx"));
        jPanel5.setLayout(new java.awt.GridBagLayout());

        balloonTextFieldItem.setFont(balloonTextFieldItem.getFont());
        balloonTextFieldItem.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                balloonTextFieldItemKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel5.add(balloonTextFieldItem, gridBagConstraints);

        jPanel6.setLayout(new java.awt.GridBagLayout());

        jButtonItemDel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/minus16.gif"))); // NOI18N
        jButtonItemDel.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonItemDel.setMaximumSize(new java.awt.Dimension(26, 26));
        jButtonItemDel.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonItemDel.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonItemDel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonItemDelActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTHEAST;
        gridBagConstraints.weighty = 1.0;
        jPanel6.add(jButtonItemDel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 3, 0, 0);
        jPanel5.add(jPanel6, gridBagConstraints);

        jScrollPane4.setPreferredSize(new java.awt.Dimension(454, 420));

        jTableItemDx.setFont(jTableItemDx.getFont());
        jTableItemDx.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTableItemDx.setFillsViewportHeight(true);
        jTableItemDx.setSurrendersFocusOnKeystroke(true);
        jTableItemDx.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTableItemDxKeyReleased(evt);
            }
        });
        jScrollPane4.setViewportView(jTableItemDx);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 0);
        jPanel5.add(jScrollPane4, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 5, 5);
        jPanel2.add(jPanel5, gridBagConstraints);

        jComboBoxClinic.setFont(jComboBoxClinic.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 0, 0, 5);
        jPanel2.add(jComboBoxClinic, gridBagConstraints);

        jLabel8.setFont(jLabel8.getFont());
        jLabel8.setText("�������ä");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 0, 5);
        jPanel2.add(jLabel8, gridBagConstraints);

        jPanel8.setBorder(javax.swing.BorderFactory.createTitledBorder("�ҷ���ռšѺ�ä"));
        jPanel8.setLayout(new java.awt.GridBagLayout());

        balloonTextFieldItemDrug.setFont(balloonTextFieldItemDrug.getFont());
        balloonTextFieldItemDrug.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                balloonTextFieldItemDrugKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel8.add(balloonTextFieldItemDrug, gridBagConstraints);

        jPanel9.setLayout(new java.awt.GridBagLayout());

        jButtonItemRiskDel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/minus16.gif"))); // NOI18N
        jButtonItemRiskDel.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonItemRiskDel.setMaximumSize(new java.awt.Dimension(26, 26));
        jButtonItemRiskDel.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonItemRiskDel.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonItemRiskDel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonItemRiskDelActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTHEAST;
        gridBagConstraints.weighty = 1.0;
        jPanel9.add(jButtonItemRiskDel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 3, 0, 0);
        jPanel8.add(jPanel9, gridBagConstraints);

        jScrollPane5.setPreferredSize(new java.awt.Dimension(454, 420));

        jTableItemRiskDx.setFont(jTableItemRiskDx.getFont());
        jTableItemRiskDx.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTableItemRiskDx.setFillsViewportHeight(true);
        jTableItemRiskDx.setSurrendersFocusOnKeystroke(true);
        jTableItemRiskDx.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTableItemRiskDxKeyReleased(evt);
            }
        });
        jScrollPane5.setViewportView(jTableItemRiskDx);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 0);
        jPanel8.add(jScrollPane5, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 5, 5);
        jPanel2.add(jPanel8, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        add(jPanel2, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jTextFieldCodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldCodeActionPerformed
    }//GEN-LAST:event_jTextFieldCodeActionPerformed

    private void jTextFieldCodeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldCodeKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_DOWN || evt.getKeyCode() == KeyEvent.VK_ENTER) {
            jTextFieldIcdCode.requestFocus();
        }
    }//GEN-LAST:event_jTextFieldCodeKeyReleased

    private void jTextFieldIcdCodeFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextFieldIcdCodeFocusLost
        String icd10 = jTextFieldIcdCode.getText();
        if (icd10.length() < 3 && icd10.length() != 0) {
            theUS.setStatus("���� ICD10 ���ٻẺ���١��ͧ", UpdateStatus.WARNING);
            jTextFieldIcdCode.requestFocus();
        }
    }//GEN-LAST:event_jTextFieldIcdCodeFocusLost

    private void jRadioButtonGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonGroupActionPerformed
        String icd10 = jTextFieldIcdCode.getText();
        if (icd10.length() > 3) {
            if (icd10.substring(3, 4).equals(".")) {
                theUS.setStatus("������кѹ�֡���� ICD10 ੾�� 3 ��ѡ�á", UpdateStatus.WARNING);
                jTextFieldIcdCode.requestFocus();
            }
        }
    }//GEN-LAST:event_jRadioButtonGroupActionPerformed

    private void jTextAreaGuideKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextAreaGuideKeyReleased
        /*if(evt.getKeyCode()==evt.VK_F1)
         {
         addGuideAfterDx();  //���й���ѧ��Ǩ
         }  */
    }//GEN-LAST:event_jTextAreaGuideKeyReleased

    private void jButtonItemDelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonItemDelActionPerformed
        setSelectItemDx(this.jTableItemDx.getSelectedRows());
    }//GEN-LAST:event_jButtonItemDelActionPerformed

    private void jTableItemDxKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTableItemDxKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_DELETE) {
            jButtonItemDelActionPerformed(null);
        }
    }//GEN-LAST:event_jTableItemDxKeyReleased

    private void balloonTextFieldItemKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_balloonTextFieldItemKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            setTableItemDx();
            balloonTextFieldItem.setText("");
        }
    }//GEN-LAST:event_balloonTextFieldItemKeyReleased

    private void balloonTextFieldItemDrugKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_balloonTextFieldItemDrugKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            setTableItemRiskDx();
            balloonTextFieldItemDrug.setText("");
        }
    }//GEN-LAST:event_balloonTextFieldItemDrugKeyReleased

    private void jButtonItemRiskDelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonItemRiskDelActionPerformed
        setSelectItemRiskDx(this.jTableItemRiskDx.getSelectedRows());
    }//GEN-LAST:event_jButtonItemRiskDelActionPerformed

    private void jTableItemRiskDxKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTableItemRiskDxKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_DELETE) {
            jButtonItemRiskDelActionPerformed(null);
        }
    }//GEN-LAST:event_jTableItemRiskDxKeyReleased

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.hosv3.gui.component.BalloonTextField balloonTextFieldItem;
    private com.hosv3.gui.component.BalloonTextField balloonTextFieldItemDrug;
    private javax.swing.ButtonGroup buttonGroupIcd;
    private javax.swing.JButton jButtonItemDel;
    private javax.swing.JButton jButtonItemRiskDel;
    private javax.swing.JComboBox jComboBoxClinic;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JRadioButton jRadioButtonCode;
    private javax.swing.JRadioButton jRadioButtonGroup;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JTable jTableItemDx;
    private javax.swing.JTable jTableItemRiskDx;
    private javax.swing.JTextArea jTextAreaGuide;
    private javax.swing.JTextField jTextFieldCode;
    private javax.swing.JTextField jTextFieldIcdCode;
    private javax.swing.JTextField jTextFieldIcdThaiName;
    // End of variables declaration//GEN-END:variables

    public void setSelectItemDx(int[] rows) {
        theHC.theSetupControl.deleteItemDx(rows);
        setTableItemDx();
    }

    public void setSelectItemRiskDx(int[] rows) {
        theHC.theSetupControl.deleteItemRiskDx(rows);
        setTableItemRiskDx();
    }

    @Override
    public void clearAll() {
        this.setTheDxTemplate2(new DxTemplate());
    }

    @Override
    public Persistent getXPer() {
        return this.getTheDxTemplate2();
    }

    @Override
    public void setXPer(Persistent x) {
        this.setTheDxTemplate2((DxTemplate) x);
    }

    @Override
    public void setLanguage() {
        //GuiLang.setLanguage(jLabel3);

        GuiLang.setLanguage(jLabel1);
        GuiLang.setLanguage(jLabel2);
        GuiLang.setLanguage(jLabel6);
        GuiLang.setLanguage(jLabel7);
        GuiLang.setLanguage(jLabel5);
        //GuiLang.setLanguage(jButtonSave);
        GuiLang.setLanguage(col_ItemDx);
        GuiLang.setLanguage(col);
        // GuiLang.setLanguage(jRadioButtonBegin);
        // GuiLang.setLanguage(jRadioButtonConsist);
        GuiLang.setLanguage(jRadioButtonCode);
        GuiLang.setLanguage(jRadioButtonGroup);
        GuiLang.setLanguage(jLabel8);
        GuiLang.setTextBundle(jPanel7);
        GuiLang.setTextBundle(jPanel5);
    }

    @Override
    public void setControl(HosControl hc, UpdateStatus us) {
        theUS = us;
        theHC = hc;
        theHO = hc.theHO;
        theHS = hc.theHS;
        //jTable1.setGuiMode(true);
        initData();
        theSetupControl = hc.theSetupControl;
        theLookupControl = hc.theLookupControl;
        theSetupSubject = hc.theHS.theSetupSubject;
        hc.theHS.theSetupSubject.addpanelrefrash(this);
        hc.theHS.theSetupSubject.addForLiftAttach(this);
        //   hc.theHS.theItemDxSubject.attachItemDx(this);
        theSetupSubject.addGuideAfterDxAttach(this);
        setupLookup();
        setEnabled(false);
    }

    private void initData() {
        jLabel5.setVisible(false);
    }

    @Override
    public void setEnabled(boolean var) {
        jTextFieldCode.setEditable(var);
        jComboBoxClinic.setEnabled(var);
        if (theDxTemplate2 != null && theDxTemplate2.icd_code != null) {
            jTextFieldIcdCode.setText(theDxTemplate2.icd_code.toUpperCase());
        }
    }

    @Override
    public void setupLookup() {
        balloonTextFieldItem.setControl(new ItemLookup(theHC.theLookupControl), theUS.getJFrame());
        balloonTextFieldItem.setEControl(new ItemLookup(theHC.theSetupControl));
        theHS.theBalloonSubject.attachBalloon(balloonTextFieldItem);
        ComboboxModel.initComboBox(jComboBoxClinic, theLookupControl.listClinic());
        balloonTextFieldItemDrug.setControl(new ItemDrugLookup(theHC.theLookupControl), theUS.getJFrame());
        balloonTextFieldItemDrug.setEControl(new ItemDrugLookup(theHC.theSetupControl));
        theHS.theBalloonSubject.attachBalloon(balloonTextFieldItemDrug);
    }

    @Override
    public boolean deleteXPer(Persistent x) {
        return (this.theSetupControl.deleteDxTemplate((DxTemplate) x) > 0);
    }

    @Override
    public boolean saveXPer(Persistent x) {
        return (this.theSetupControl.saveDxTemplate((DxTemplate) x) > 0);
    }

    @Override
    public Vector listXPer(String key, String active, int offset) {
        return theLookupControl.listDxTemplateByName(key);
    }

    @Override
    public boolean isActiveVisible() {
        return false;
    }

    public boolean isStartVisible() {
        return true;
    }
    public static String TITLE = Constant.getTextBundle("DX ��辺����");

    @Override
    public String getTitle() {
        return TITLE;
    }

    public DxTemplate getTheDxTemplate2() {
        theDxTemplate2.description = jTextFieldCode.getText();
        if (theDxTemplate2.description.isEmpty()) {
            theUS.setStatus("��س��к���������´�ͧ Dx ��辺����", UpdateStatus.WARNING);
            jTextFieldCode.requestFocus();
        }
        String icd_code_original = jTextFieldIcdCode.getText();
        if (icd_code_original.length() > 3 && icd_code_original.length() != 0) {
            if (jRadioButtonGroup.isSelected()) {
                String hos = icd_code_original.substring(0, 3);
                theDxTemplate2.icd_code = hos;
            } else {
                if (icd_code_original.substring(3, 4).equals(".")) {
                    theDxTemplate2.icd_code = icd_code_original;
                } else {
                    String hos = icd_code_original.substring(0, 3);
                    String sub = icd_code_original.substring(3);
                    theDxTemplate2.icd_code = hos + "." + sub;

                }
            }

            ICD10 icd10 = theSetupControl.listIcd10ByCode(theDxTemplate2.icd_code, 1);
            if (icd10 == null) {
                theUS.setStatus("����� ICD10 ���㹰ҹ������ ���Ͷ١ inactive ���� ��س��к� ICD10 ����", UpdateStatus.WARNING);
                jTextFieldIcdCode.requestFocus();
                //return;
            }
        } else {
            theDxTemplate2.icd_code = icd_code_original;
        }
        theDxTemplate2.guide_after_dx = jTextAreaGuide.getText();//Gutil.CheckReservedWords(jTextAreaGuide.getText());
        theDxTemplate2.thaidescription = jTextFieldIcdThaiName.getText();//Gutil.CheckReservedWords(jTextFieldIcdThaiName.getText());
        theDxTemplate2.clinic_code = Gutil.getGuiData(jComboBoxClinic);

        if (jRadioButtonCode.isSelected()) {
            theDxTemplate2.icd_type = "1";
        } else {
            theDxTemplate2.icd_type = "2";
        }

        return theDxTemplate2;
    }

    public void setTheDxTemplate2(DxTemplate item) {
        theDxTemplate2 = item;
        jTextFieldCode.setText(theDxTemplate2.description);
        if (!theDxTemplate2.icd_code.endsWith("99")) {
            jTextFieldIcdCode.setText(theDxTemplate2.icd_code);
        }
        if (theDxTemplate2.icd_type.equals("1") && theDxTemplate2.icd_type != null) {
            jRadioButtonCode.setSelected(true);
        } else {
            jRadioButtonGroup.setSelected(true);
        }
        jTextAreaGuide.setText(theDxTemplate2.guide_after_dx);
        jTextFieldIcdThaiName.setText(theDxTemplate2.thaidescription);
        ComboboxModel.setCodeComboBox(jComboBoxClinic, theDxTemplate2.clinic_code);
        this.theSetupControl.listItemDxByDxTemplate(item.getObjectId());
        this.theSetupControl.listItemRiskDxByDxTemplate(item.getObjectId());
        setTableItemDx();
        setTableItemRiskDx();
    }

    private void setTableItemDx() {
        TaBleModel tm;
        if (theHO.vItemDx == null) {
            tm = new TaBleModel(col_ItemDx, 0);
            jTableItemDx.setModel(tm);
            return;
        }
        tm = new TaBleModel(col_ItemDx, theHO.vItemDx.size());
        for (int i = 0; i < theHO.vItemDx.size(); i++) {
            DxTemplateMapItem p = (DxTemplateMapItem) theHO.vItemDx.get(i);
            tm.setValueAt(p.code, i, 0);
            tm.setValueAt(p.description, i, 1);
        }
        jTableItemDx.setModel(tm);
        jTableItemDx.getColumnModel().getColumn(0).setPreferredWidth(10); // ����
        jTableItemDx.getColumnModel().getColumn(1).setPreferredWidth(80); // ������¡�� Item
    }

    private void setTableItemRiskDx() {
        TaBleModel tm;
        if (theHO.vItemRiskDx == null) {
            tm = new TaBleModel(col_ItemDx, 0);
            jTableItemRiskDx.setModel(tm);
            return;
        }
        tm = new TaBleModel(col_ItemDx, theHO.vItemRiskDx.size());
        for (int i = 0; i < theHO.vItemRiskDx.size(); i++) {
            DxTemplateMapItemRisk p = (DxTemplateMapItemRisk) theHO.vItemRiskDx.get(i);
            tm.setValueAt(p.code, i, 0);
            tm.setValueAt(p.description, i, 1);
        }
        jTableItemRiskDx.setModel(tm);
        jTableItemRiskDx.getColumnModel().getColumn(0).setPreferredWidth(10);
        jTableItemRiskDx.getColumnModel().getColumn(1).setPreferredWidth(80);
    }

    @Override
    public void doAddAction() {
        //
    }

    @Override
    public boolean isAddNewAfterSaveNewXPer() {
        return true;
    }

}

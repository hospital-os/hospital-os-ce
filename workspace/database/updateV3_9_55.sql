ALTER TABLE t_allergic_reactions ALTER COLUMN t_patient_id TYPE varchar(30) USING t_patient_id::varchar;
ALTER TABLE t_patient_drug_allergy ALTER COLUMN t_patient_id TYPE varchar(30) USING t_patient_id::varchar;
ALTER TABLE t_g_6_pd ALTER COLUMN t_patient_id TYPE varchar(30) USING t_patient_id::varchar;
ALTER TABLE b_template_dx_map_item ALTER COLUMN b_template_dx_map_item_id TYPE varchar(30) USING b_template_dx_map_item_id::varchar;

ALTER TABLE t_lis_ln ADD exec_user_id varchar(30) NULL; -- ตกไปจาก patch 53
ALTER TABLE t_lis_ln ADD exec_location_id varchar(30) NULL; -- ตกไปจาก patch 53

ALTER TABLE b_item_drug ADD COLUMN item_drug_print_equal_quantity varchar(1)NOT NULL DEFAULT '0';

ALTER TABLE t_order_drug ADD COLUMN print_equal_quantity varchar(1)NOT NULL DEFAULT '0';
ALTER TABLE t_order_drug ADD drug_sticker_quantity integer NOT NULL DEFAULT 1;

ALTER TABLE t_visit_vital_sign ADD COLUMN visit_vital_sign_map varchar(5) DEFAULT '';

CREATE TABLE t_log_print_receipt (
	id bigserial NOT NULL,
	t_billing_receipt_id varchar(30) NOT NULL,
	user_id varchar(30) NOT NULL,
	action_type varchar(1) NOT NULL DEFAULT '1'::character varying, -- 1 is preview and 2 is print
	record_datetime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP);

-- Column comments

COMMENT ON COLUMN t_log_print_receipt.action_type IS '1 is preview and 2 is print';


ALTER TABLE b_item_price ADD item_share_doctor float8 NOT NULL DEFAULT 0;
ALTER TABLE b_item_price ADD item_share_hospital float8 NOT NULL DEFAULT 0;
ALTER TABLE b_item_price ADD item_editable_price varchar(1) NOT NULL DEFAULT '0';
ALTER TABLE b_item_price ADD item_limit_price_min float8 NOT NULL DEFAULT 0;
ALTER TABLE b_item_price ADD item_limit_price_max float8 NOT NULL DEFAULT 0;

UPDATE b_item_price SET item_editable_price = '1'  ,item_limit_price_max = 99999  WHERE b_item_id IN (SELECT b_item_id FROM b_item
    INNER JOIN b_item_subgroup ON b_item_subgroup.b_item_subgroup_id = b_item.b_item_subgroup_id
    INNER JOIN f_item_group ON f_item_group.f_item_group_id = b_item_subgroup.f_item_group_id WHERE f_item_group.f_item_group_id ='5');

ALTER TABLE t_order ADD order_xray_film_price float8 NOT NULL DEFAULT 0;
ALTER TABLE t_order ADD order_share_doctor float8 NOT NULL DEFAULT 0;
ALTER TABLE t_order ADD order_share_hospital float8 NOT NULL DEFAULT 0;
ALTER TABLE t_order ADD order_editable_price varchar(1) NOT NULL DEFAULT '0';
ALTER TABLE t_order ADD order_edited_price varchar(1) NOT NULL DEFAULT '0';
ALTER TABLE t_order ADD order_limit_price_min float8 NOT NULL DEFAULT 0;
ALTER TABLE t_order ADD order_limit_price_max float8 NOT NULL DEFAULT 0;

update t_order set order_xray_film_price = ds.xray_price 
from (select t_order_id, sum((xray_film_amount::numeric - result_xray_size_damaging_xray_film_amount::numeric) * result_xray_size_price) as xray_price from t_result_xray_size where result_xray_size_active = '1' group by t_order_id) as ds 
where t_order.t_order_id = ds.t_order_id;

ALTER TABLE t_insurance_claim_billing ADD total_discount float8 NOT NULL DEFAULT 0;

-- update 43 file
---dental
update f_dent_type set description = 'กลุ่มเด็กก่อนวัยเรียน (อายุ 0-5 ปี)' where f_dent_type_id = '2';
update f_dent_type set description = 'กลุ่มเด็กวัยเรียน (อายุ 6-12 ปี)' where f_dent_type_id = '3';
update f_dent_type set description = 'กลุ่มผู้สูงอายุ (อายุ 60 ปีขึ้นไป)' where f_dent_type_id = '4';

update f_gum set description = 'เหงือกอักเสบ' where f_gum_id = '1';
update f_gum set description = 'มีหินน้ำลายชัดเจน' where f_gum_id = '2';
update f_gum set description = 'ปริทันต์อักเสบหรือมีฟันโยก' where f_gum_id = '3';
update f_gum set description = 'ไม่มีฟันหรือตรวจไม่ได้' where f_gum_id = '9';


---provider
update f_provider_type set description = 'ทันตาภิบาล' where code = '06';

BEGIN;
INSERT INTO f_provider_type SELECT '000010','081','แพทย์แผนไทย/แพทย์แผนไทยประยุกต์ : (ที่มีใบประกอบวิชาชีพฯ)'
WHERE NOT EXISTS (SELECT 1 FROM f_provider_type WHERE code = '081'); 
COMMIT;
BEGIN;
INSERT INTO f_provider_type SELECT '000011','082','แพทย์พื้นบ้าน (ที่มีใบประกอบวิชาชีพฯ หรือได้รับการรับรองตามระเบียบการแพทย์แผนไทยฯ)'
WHERE NOT EXISTS (SELECT 1 FROM f_provider_type WHERE code = '082'); 
COMMIT;
BEGIN;
INSERT INTO f_provider_type SELECT '000012','083','แพทย์แผนจีน (ที่มีใบประกอบวิชาชีพฯ)'
WHERE NOT EXISTS (SELECT 1 FROM f_provider_type WHERE code = '083'); 
COMMIT;
BEGIN;
INSERT INTO f_provider_type SELECT '000013','084','ผู้ช่วยแพทย์แผนไทย (ที่ผ่านการอบรมตามเกณฑ์)'
WHERE NOT EXISTS (SELECT 1 FROM f_provider_type WHERE code = '086'); 
COMMIT;
BEGIN;
INSERT INTO f_provider_type SELECT '000014','085','บุคลากรแพทย์แผนไทย แพทย์พื้นบ้าน แพทย์แผนจีน แพทย์ทางเลือก (ที่มีวุฒิการศึกษาหรือผ่านการอบรมตามเกณฑ์ แต่ที่ไม่มีใบประกอบวิชาชีพ)'
WHERE NOT EXISTS (SELECT 1 FROM f_provider_type WHERE code = '085'); 
COMMIT;
BEGIN;
INSERT INTO f_provider_type SELECT '000015','10','ผู้ดูแลผู้ป่วยที่บ้าน หรือผู้ช่วยเหลือดูแลผู้สูงอายุ (Care Giver: CG)'
WHERE NOT EXISTS (SELECT 1 FROM f_provider_type WHERE code = '10'); 
COMMIT;
BEGIN;
INSERT INTO f_provider_type SELECT '000016','11','เภสัชกร'
WHERE NOT EXISTS (SELECT 1 FROM f_provider_type WHERE code = '11'); 
COMMIT;
BEGIN;
INSERT INTO f_provider_type SELECT '000017','12','บุคลากรด้านเวชศาสตร์ฟื้นฟู (รวม นักกายภาพบำบัด/นักเวชกรรมฟื้นฟู)'
WHERE NOT EXISTS (SELECT 1 FROM f_provider_type WHERE code = '12'); 
COMMIT;
BEGIN;
INSERT INTO f_provider_type SELECT '000018','13','บุคลากรด้านเทคนิคการแพทย์'
WHERE NOT EXISTS (SELECT 1 FROM f_provider_type WHERE code = '13'); 
COMMIT;
BEGIN;
INSERT INTO f_provider_type SELECT '000019','14','ผู้จัดการดูแล (Care Manager: CM)'
WHERE NOT EXISTS (SELECT 1 FROM f_provider_type WHERE code = '14'); 
COMMIT;


----specialpp
BEGIN; 
UPDATE f_specialpp_code SET description = 'การคัดกรองมะเร็งปากมดลูก ด้วยวิธี HPV Genotype Testing' WHERE f_specialpp_code_id = '1B0046'; 
INSERT INTO f_specialpp_code SELECT '1B0046','การคัดกรองมะเร็งปากมดลูก ด้วยวิธี HPV Genotype Testing'
WHERE NOT EXISTS (SELECT 1 FROM f_specialpp_code WHERE f_specialpp_code_id = '1B0046'); 
COMMIT;
BEGIN; 
UPDATE f_specialpp_code SET description = 'การคัดกรองผู้ป่วยจิตเวชที่มีความเสี่ยงสูง พบมีประวัติทำร้ายตนเองด้วยวิธีรุนแรง มุ่งหวังให้เสียชีวิต' WHERE f_specialpp_code_id = '1B030'; 
INSERT INTO f_specialpp_code SELECT '1B030','การคัดกรองผู้ป่วยจิตเวชที่มีความเสี่ยงสูง พบมีประวัติทำร้ายตนเองด้วยวิธีรุนแรง มุ่งหวังให้เสียชีวิต'
WHERE NOT EXISTS (SELECT 1 FROM f_specialpp_code WHERE f_specialpp_code_id = '1B030'); 
COMMIT;
BEGIN; 
UPDATE f_specialpp_code SET description = 'การคัดกรองผู้ป่วยจิตเวชที่มีความเสี่ยงสูง พบมีประวัติทำร้ายผู้อื่นด้วยวิธีรุนแรง/ก่อเหตุการณ์รุนแรงในชุมชน' WHERE f_specialpp_code_id = '1B031'; 
INSERT INTO f_specialpp_code SELECT '1B031','การคัดกรองผู้ป่วยจิตเวชที่มีความเสี่ยงสูง พบมีประวัติทำร้ายผู้อื่นด้วยวิธีรุนแรง/ก่อเหตุการณ์รุนแรงในชุมชน'
WHERE NOT EXISTS (SELECT 1 FROM f_specialpp_code WHERE f_specialpp_code_id = '1B031'); 
COMMIT;
BEGIN; 
UPDATE f_specialpp_code SET description = 'การคัดกรองผู้ป่วยจิตเวชที่มีความเสี่ยงสูง พบมีอาการหลงผิด มีความคิดทำร้ายผู้อื่นให้ถึงกับชีวิตหรือมุ่งร้ายผู้อื่นแบบเฉพาะเจาะจง เช่น ระบุชื่อคนที่จะมุ่งร้าย' WHERE f_specialpp_code_id = '1B032'; 
INSERT INTO f_specialpp_code SELECT '1B032','การคัดกรองผู้ป่วยจิตเวชที่มีความเสี่ยงสูง พบมีอาการหลงผิด มีความคิดทำร้ายผู้อื่นให้ถึงกับชีวิตหรือมุ่งร้ายผู้อื่นแบบเฉพาะเจาะจง เช่น ระบุชื่อคนที่จะมุ่งร้าย'
WHERE NOT EXISTS (SELECT 1 FROM f_specialpp_code WHERE f_specialpp_code_id = '1B032'); 
COMMIT;
BEGIN; 
UPDATE f_specialpp_code SET description = 'การคัดกรองผู้ป่วยจิตเวชที่มีความเสี่ยงสูง พบมีอาการหลงผิด มีความคิดทำร้ายผู้อื่นให้ถึงกับชีวิตหรือมุ่งร้ายผู้อื่นแบบเฉพาะเจาะจง เช่น ระบุชื่อคนที่จะมุ่งร้าย' WHERE f_specialpp_code_id = '1B033'; 
INSERT INTO f_specialpp_code SELECT '1B033','การคัดกรองผู้ป่วยจิตเวชที่มีความเสี่ยงสูง พบมีอาการหลงผิด มีความคิดทำร้ายผู้อื่นให้ถึงกับชีวิตหรือมุ่งร้ายผู้อื่นแบบเฉพาะเจาะจง เช่น ระบุชื่อคนที่จะมุ่งร้าย'
WHERE NOT EXISTS (SELECT 1 FROM f_specialpp_code WHERE f_specialpp_code_id = '1B033'); 
COMMIT;
BEGIN; 
UPDATE f_specialpp_code SET description = 'การคัดกรองผู้ป่วยจิตเวชที่มีความเสี่ยงสูง พบมีอาการหลงผิด มีความคิดหมกมุ่นผิดปกติที่เกี่ยวข้องแบบเฉพาะเจาะจงกับราชวงศ์ จนเกิดพฤติกรรมวุ่นวาย รบกวนในงานพิธีสำคัญ' WHERE f_specialpp_code_id = '1B034'; 
INSERT INTO f_specialpp_code SELECT '1B034','การคัดกรองผู้ป่วยจิตเวชที่มีความเสี่ยงสูง พบมีอาการหลงผิด มีความคิดหมกมุ่นผิดปกติที่เกี่ยวข้องแบบเฉพาะเจาะจงกับราชวงศ์ จนเกิดพฤติกรรมวุ่นวาย รบกวนในงานพิธีสำคัญ'
WHERE NOT EXISTS (SELECT 1 FROM f_specialpp_code WHERE f_specialpp_code_id = '1B034'); 
COMMIT;
BEGIN; 
UPDATE f_specialpp_code SET description = 'การตรวจคัดกรองสมรรถภาพการได้ยินของการตรวจที่มีผลครั้งเดียว มีผลปกติ (ระดับการได้ยินของหูทั้ง 2 ข้าง ไม่เกิน 25 เดซิเบล ทุกความถี่)' WHERE f_specialpp_code_id = '1B1140'; 
INSERT INTO f_specialpp_code SELECT '1B1140','การตรวจคัดกรองสมรรถภาพการได้ยินของการตรวจที่มีผลครั้งเดียว มีผลปกติ (ระดับการได้ยินของหูทั้ง 2 ข้าง ไม่เกิน 25 เดซิเบล ทุกความถี่)'
WHERE NOT EXISTS (SELECT 1 FROM f_specialpp_code WHERE f_specialpp_code_id = '1B1140'); 
COMMIT;
BEGIN; 
UPDATE f_specialpp_code SET description = 'การตรวจคัดกรองสมรรถภาพการได้ยินของการตรวจที่มีผลครั้งเดียว มีผลตรวจระดับการได้ยินมากกว่า 25 เดซิเบล ที่ความถี่ใด ความถี่หนึ่งของหูข้างใดข้างหนึ่ง' WHERE f_specialpp_code_id = '1B1141'; 
INSERT INTO f_specialpp_code SELECT '1B1141','การตรวจคัดกรองสมรรถภาพการได้ยินของการตรวจที่มีผลครั้งเดียว มีผลตรวจระดับการได้ยินมากกว่า 25 เดซิเบล ที่ความถี่ใด ความถี่หนึ่งของหูข้างใดข้างหนึ่ง'
WHERE NOT EXISTS (SELECT 1 FROM f_specialpp_code WHERE f_specialpp_code_id = '1B1141'); 
COMMIT;
BEGIN; 
UPDATE f_specialpp_code SET description = 'การตรวจคัดกรองสมรรถภาพการได้ยิน เมื่อเทียบผลการตรวจกับ Baseline audiogram (พบ 15 dB-shift และไม่ได้รับการตรวจยืนยัน: Confirmation audiogram ภายใน 30 วัน)' WHERE f_specialpp_code_id = '1B1144'; 
INSERT INTO f_specialpp_code SELECT '1B1144','การตรวจคัดกรองสมรรถภาพการได้ยิน เมื่อเทียบผลการตรวจกับ Baseline audiogram (พบ 15 dB-shift และไม่ได้รับการตรวจยืนยัน: Confirmation audiogram ภายใน 30 วัน)'
WHERE NOT EXISTS (SELECT 1 FROM f_specialpp_code WHERE f_specialpp_code_id = '1B1144'); 
COMMIT;
BEGIN; 
UPDATE f_specialpp_code SET description = 'การตรวจคัดกรองสมรรถนะผู้สูงอายุเกี่ยวกับความสามารถในการทำกิจวัตรประจำวันพบว่าช่วยเหลือตัวเองได้ /ติดสังคม (ADL 12-20 คะแนน) และได้รับการจัดทำ Care Plan' WHERE f_specialpp_code_id = '1B1280'; 
INSERT INTO f_specialpp_code SELECT '1B1280','การตรวจคัดกรองสมรรถนะผู้สูงอายุเกี่ยวกับความสามารถในการทำกิจวัตรประจำวันพบว่าช่วยเหลือตัวเองได้ /ติดสังคม (ADL 12-20 คะแนน) และได้รับการจัดทำ Care Plan'
WHERE NOT EXISTS (SELECT 1 FROM f_specialpp_code WHERE f_specialpp_code_id = '1B1280'); 
COMMIT;
BEGIN; 
UPDATE f_specialpp_code SET description = 'การตรวจคัดกรองสมรรถนะผู้สูงอายุเกี่ยวกับความสามารถในการทำกิจวัตรประจำวันพบว่าช่วยเหลือตัวเองได้บ้าง / บางส่วน /ติดบ้าน (ADL 5-11 คะแนน) และได้รับการจัดทำ Care Plan' WHERE f_specialpp_code_id = '1B1281'; 
INSERT INTO f_specialpp_code SELECT '1B1281','การตรวจคัดกรองสมรรถนะผู้สูงอายุเกี่ยวกับความสามารถในการทำกิจวัตรประจำวันพบว่าช่วยเหลือตัวเองได้บ้าง / บางส่วน /ติดบ้าน (ADL 5-11 คะแนน) และได้รับการจัดทำ Care Plan'
WHERE NOT EXISTS (SELECT 1 FROM f_specialpp_code WHERE f_specialpp_code_id = '1B1281'); 
COMMIT;
BEGIN; 
UPDATE f_specialpp_code SET description = 'การตรวจคัดกรองสมรรถนะผู้สูงอายุเกี่ยวกับความสามารถในการทำกิจวัตรประจำวัน พบว่าช่วยเหลือตัวเองได้น้อย / ไม่ได้เลย /ภาวะติดเตียง (ADL 0-4 คะแนน) และได้รับการจัดทำ Care Plan' WHERE f_specialpp_code_id = '1B1282'; 
INSERT INTO f_specialpp_code SELECT '1B1282','การตรวจคัดกรองสมรรถนะผู้สูงอายุเกี่ยวกับความสามารถในการทำกิจวัตรประจำวัน พบว่าช่วยเหลือตัวเองได้น้อย / ไม่ได้เลย /ภาวะติดเตียง (ADL 0-4 คะแนน) และได้รับการจัดทำ Care Plan'
WHERE NOT EXISTS (SELECT 1 FROM f_specialpp_code WHERE f_specialpp_code_id = '1B1282'); 
COMMIT;
BEGIN; 
UPDATE f_specialpp_code SET description = 'การติดตามผู้มีพฤติกรรมการดื่มสุราแบบเสี่ยงสูง (ผู้ติดสุรา) ระยะเวลา 1 เดือน ไม่ดื่มเลย' WHERE f_specialpp_code_id = '1B620'; 
INSERT INTO f_specialpp_code SELECT '1B620','การติดตามผู้มีพฤติกรรมการดื่มสุราแบบเสี่ยงสูง (ผู้ติดสุรา) ระยะเวลา 1 เดือน ไม่ดื่มเลย'
WHERE NOT EXISTS (SELECT 1 FROM f_specialpp_code WHERE f_specialpp_code_id = '1B620'); 
COMMIT;
BEGIN; 
UPDATE f_specialpp_code SET description = 'การติดตามผู้มีพฤติกรรมการดื่มสุราแบบเสี่ยงสูง (ผู้ติดสุรา) ระยะเวลา 1 เดือน ดื่มลดลง' WHERE f_specialpp_code_id = '1B621'; 
INSERT INTO f_specialpp_code SELECT '1B621','การติดตามผู้มีพฤติกรรมการดื่มสุราแบบเสี่ยงสูง (ผู้ติดสุรา) ระยะเวลา 1 เดือน ดื่มลดลง'
WHERE NOT EXISTS (SELECT 1 FROM f_specialpp_code WHERE f_specialpp_code_id = '1B621'); 
COMMIT;
BEGIN; 
UPDATE f_specialpp_code SET description = 'การติดตามผู้มีพฤติกรรมการดื่มสุราแบบเสี่ยงสูง (ผู้ติดสุรา) ระยะเวลา 1 เดือน ดื่มเท่าเดิม' WHERE f_specialpp_code_id = '1B622'; 
INSERT INTO f_specialpp_code SELECT '1B622','การติดตามผู้มีพฤติกรรมการดื่มสุราแบบเสี่ยงสูง (ผู้ติดสุรา) ระยะเวลา 1 เดือน ดื่มเท่าเดิม'
WHERE NOT EXISTS (SELECT 1 FROM f_specialpp_code WHERE f_specialpp_code_id = '1B622'); 
COMMIT;
BEGIN; 
UPDATE f_specialpp_code SET description = 'การติดตามผู้มีพฤติกรรมการดื่มสุราแบบเสี่ยงสูง (ผู้ติดสุรา) ระยะเวลา 1 เดือน ดื่มมากขึ้น' WHERE f_specialpp_code_id = '1B623'; 
INSERT INTO f_specialpp_code SELECT '1B623','การติดตามผู้มีพฤติกรรมการดื่มสุราแบบเสี่ยงสูง (ผู้ติดสุรา) ระยะเวลา 1 เดือน ดื่มมากขึ้น'
WHERE NOT EXISTS (SELECT 1 FROM f_specialpp_code WHERE f_specialpp_code_id = '1B623'); 
COMMIT;
BEGIN; 
UPDATE f_specialpp_code SET description = 'การติดตามผู้มีพฤติกรรมการดื่มสุราแบบเสี่ยงสูง (ผู้ติดสุรา) ระยะเวลา 3 เดือน ไม่ดื่มเลย' WHERE f_specialpp_code_id = '1B630'; 
INSERT INTO f_specialpp_code SELECT '1B630','การติดตามผู้มีพฤติกรรมการดื่มสุราแบบเสี่ยงสูง (ผู้ติดสุรา) ระยะเวลา 3 เดือน ไม่ดื่มเลย'
WHERE NOT EXISTS (SELECT 1 FROM f_specialpp_code WHERE f_specialpp_code_id = '1B630'); 
COMMIT;
BEGIN; 
UPDATE f_specialpp_code SET description = 'การติดตามผู้มีพฤติกรรมการดื่มสุราแบบเสี่ยงสูง (ผู้ติดสุรา) ระยะเวลา 3 เดือน ดื่มลดลง' WHERE f_specialpp_code_id = '1B631'; 
INSERT INTO f_specialpp_code SELECT '1B631','การติดตามผู้มีพฤติกรรมการดื่มสุราแบบเสี่ยงสูง (ผู้ติดสุรา) ระยะเวลา 3 เดือน ดื่มลดลง'
WHERE NOT EXISTS (SELECT 1 FROM f_specialpp_code WHERE f_specialpp_code_id = '1B631'); 
COMMIT;
BEGIN; 
UPDATE f_specialpp_code SET description = 'การติดตามผู้มีพฤติกรรมการดื่มสุราแบบเสี่ยงสูง (ผู้ติดสุรา) ระยะเวลา 3 เดือน ดื่มเท่าเดิม' WHERE f_specialpp_code_id = '1B632'; 
INSERT INTO f_specialpp_code SELECT '1B632','การติดตามผู้มีพฤติกรรมการดื่มสุราแบบเสี่ยงสูง (ผู้ติดสุรา) ระยะเวลา 3 เดือน ดื่มเท่าเดิม'
WHERE NOT EXISTS (SELECT 1 FROM f_specialpp_code WHERE f_specialpp_code_id = '1B632'); 
COMMIT;
BEGIN; 
UPDATE f_specialpp_code SET description = 'การติดตามผู้มีพฤติกรรมการดื่มสุราแบบเสี่ยงสูง (ผู้ติดสุรา) ระยะเวลา 3 เดือน ดื่มมากขึ้น' WHERE f_specialpp_code_id = '1B633'; 
INSERT INTO f_specialpp_code SELECT '1B633','การติดตามผู้มีพฤติกรรมการดื่มสุราแบบเสี่ยงสูง (ผู้ติดสุรา) ระยะเวลา 3 เดือน ดื่มมากขึ้น'
WHERE NOT EXISTS (SELECT 1 FROM f_specialpp_code WHERE f_specialpp_code_id = '1B633'); 
COMMIT;
BEGIN; 
UPDATE f_specialpp_code SET description = 'การติดตามผู้มีพฤติกรรมการดื่มสุราแบบเสี่ยงสูง (ผู้ติดสุรา) ระยะเวลา 6 เดือน ไม่ดื่มเลย' WHERE f_specialpp_code_id = '1B640'; 
INSERT INTO f_specialpp_code SELECT '1B640','การติดตามผู้มีพฤติกรรมการดื่มสุราแบบเสี่ยงสูง (ผู้ติดสุรา) ระยะเวลา 6 เดือน ไม่ดื่มเลย'
WHERE NOT EXISTS (SELECT 1 FROM f_specialpp_code WHERE f_specialpp_code_id = '1B640'); 
COMMIT;
BEGIN; 
UPDATE f_specialpp_code SET description = 'การติดตามผู้มีพฤติกรรมการดื่มสุราแบบเสี่ยงสูง (ผู้ติดสุรา) ระยะเวลา 6 เดือน ดื่มลดลง' WHERE f_specialpp_code_id = '1B641'; 
INSERT INTO f_specialpp_code SELECT '1B641','การติดตามผู้มีพฤติกรรมการดื่มสุราแบบเสี่ยงสูง (ผู้ติดสุรา) ระยะเวลา 6 เดือน ดื่มลดลง'
WHERE NOT EXISTS (SELECT 1 FROM f_specialpp_code WHERE f_specialpp_code_id = '1B641'); 
COMMIT;
BEGIN; 
UPDATE f_specialpp_code SET description = 'การติดตามผู้มีพฤติกรรมการดื่มสุราแบบเสี่ยงสูง (ผู้ติดสุรา) ระยะเวลา 6 เดือน ดื่มเท่าเดิม' WHERE f_specialpp_code_id = '1B642'; 
INSERT INTO f_specialpp_code SELECT '1B642','การติดตามผู้มีพฤติกรรมการดื่มสุราแบบเสี่ยงสูง (ผู้ติดสุรา) ระยะเวลา 6 เดือน ดื่มเท่าเดิม'
WHERE NOT EXISTS (SELECT 1 FROM f_specialpp_code WHERE f_specialpp_code_id = '1B642'); 
COMMIT;
BEGIN; 
UPDATE f_specialpp_code SET description = 'การติดตามผู้มีพฤติกรรมการดื่มสุราแบบเสี่ยงสูง (ผู้ติดสุรา) ระยะเวลา 6 เดือน ดื่มมากขึ้น' WHERE f_specialpp_code_id = '1B643'; 
INSERT INTO f_specialpp_code SELECT '1B643','การติดตามผู้มีพฤติกรรมการดื่มสุราแบบเสี่ยงสูง (ผู้ติดสุรา) ระยะเวลา 6 เดือน ดื่มมากขึ้น'
WHERE NOT EXISTS (SELECT 1 FROM f_specialpp_code WHERE f_specialpp_code_id = '1B643'); 
COMMIT;
BEGIN; 
UPDATE f_specialpp_code SET description = 'การติดตามผู้มีพฤติกรรมการดื่มสุราแบบเสี่ยงสูง (ผู้ติดสุรา) ระยะเวลา 12 เดือน ไม่ดื่มเลย' WHERE f_specialpp_code_id = '1B650'; 
INSERT INTO f_specialpp_code SELECT '1B650','การติดตามผู้มีพฤติกรรมการดื่มสุราแบบเสี่ยงสูง (ผู้ติดสุรา) ระยะเวลา 12 เดือน ไม่ดื่มเลย'
WHERE NOT EXISTS (SELECT 1 FROM f_specialpp_code WHERE f_specialpp_code_id = '1B650'); 
COMMIT;
BEGIN; 
UPDATE f_specialpp_code SET description = 'การติดตามผู้มีพฤติกรรมการดื่มสุราแบบเสี่ยงสูง (ผู้ติดสุรา) ระยะเวลา 12 เดือน ดื่มลดลง' WHERE f_specialpp_code_id = '1B651'; 
INSERT INTO f_specialpp_code SELECT '1B651','การติดตามผู้มีพฤติกรรมการดื่มสุราแบบเสี่ยงสูง (ผู้ติดสุรา) ระยะเวลา 12 เดือน ดื่มลดลง'
WHERE NOT EXISTS (SELECT 1 FROM f_specialpp_code WHERE f_specialpp_code_id = '1B651'); 
COMMIT;
BEGIN; 
UPDATE f_specialpp_code SET description = 'การติดตามผู้มีพฤติกรรมการดื่มสุราแบบเสี่ยงสูง (ผู้ติดสุรา) ระยะเวลา 12 เดือน ดื่มเท่าเดิม' WHERE f_specialpp_code_id = '1B652'; 
INSERT INTO f_specialpp_code SELECT '1B652','การติดตามผู้มีพฤติกรรมการดื่มสุราแบบเสี่ยงสูง (ผู้ติดสุรา) ระยะเวลา 12 เดือน ดื่มเท่าเดิม'
WHERE NOT EXISTS (SELECT 1 FROM f_specialpp_code WHERE f_specialpp_code_id = '1B652'); 
COMMIT;
BEGIN; 
UPDATE f_specialpp_code SET description = 'การติดตามผู้มีพฤติกรรมการดื่มสุราแบบเสี่ยงสูง (ผู้ติดสุรา) ระยะเวลา 12 เดือน ดื่มมากขึ้น' WHERE f_specialpp_code_id = '1B653'; 
INSERT INTO f_specialpp_code SELECT '1B653','การติดตามผู้มีพฤติกรรมการดื่มสุราแบบเสี่ยงสูง (ผู้ติดสุรา) ระยะเวลา 12 เดือน ดื่มมากขึ้น'
WHERE NOT EXISTS (SELECT 1 FROM f_specialpp_code WHERE f_specialpp_code_id = '1B653'); 
COMMIT;


------comservice
insert into f_comservice values ('260','1A015','เยี่ยมผู้ป่วย/ผู้สัมผัสโรคติดต่อทางเพศสัมพันธ์');
insert into f_comservice values ('261','1F10','การให้บริการปรึกษา/ให้ความรู้ ผู้ที่ติดสุราเพื่อลด/เลิกสุรา');
insert into f_comservice values ('262','1F11','การให้บริการปรึกษา/ให้ความรู้ ผู้ที่ติดบุหรี่เพื่อลด/เลิกบุหรี่');
insert into f_comservice values ('263','1F12','การให้บริการปรึกษา/ให้ความรู้ ผู้ที่ติดยาเสพติดเพื่อลด/เลิกยาเสพติด');
insert into f_comservice values ('264','1F18','การให้บริการปรึกษา/ให้ความรู้ ทางด้านสารเสพติดอื่นๆ');
insert into f_comservice values ('265','1F19','การให้บริการปรึกษา/ให้ความรู้ ทางด้านสารเสพติด  ไม่ระบุรายละเอียด');
insert into f_comservice values ('266','1F20','การให้บริการปรึกษา/ให้ความรู้ เรื่องเพศสัมพันธ์');
insert into f_comservice values ('267','1H2080','การตรวจคัดกรองการได้ยิน ปกติทั้ง 2 ข้าง');
insert into f_comservice values ('268','1H2081','การตรวจคัดกรองการได้ยินผิดปกติทั้ง 2 ข้างหรือข้างใดข้างหนึ่ง ได้รับการรักษา');
insert into f_comservice values ('269','1H2082','การตรวจคัดกรองการได้ยินผิดปกติทั้ง 2 ข้างหรือข้างใดข้างหนึ่ง ได้รับการส่งต่อ');
insert into f_comservice values ('270','1H2083','การตรวจคัดกรองการได้ยินผิดปกติทั้ง 2 ข้างหรือข้างใดข้างหนึ่ง ไม่ได้ส่งต่อและรักษา');
insert into f_comservice values ('271','1H2180','การตรวจคัดกรองสายตา ปกติทั้ง 2 ข้าง');
insert into f_comservice values ('272','1H2181','การตรวจคัดกรองสายตา ผิดปกติทั้ง 2 ข้างหรือข้างใดข้างหนึ่ง (VA<20/40) ได้รับการรักษา');
insert into f_comservice values ('273','1H2182','การตรวจคัดกรองสายตา ผิดปกติทั้ง 2 ข้างหรือข้างใดข้างหนึ่ง (VA<20/40) ได้รับการส่งต่อ');
insert into f_comservice values ('274','1H2183','การตรวจคัดกรองสายตา ผิดปกติทั้ง 2 ข้างหรือข้างใดข้างหนึ่ง (VA<20/40) ไม่ได้ส่งต่อและรักษา');
insert into f_comservice values ('275','1H3010','ตรวจคัดกรองระดับความเข้มข้นของเม็ดเลือดแดง (HB) หรือปริมาณเม็ดเลือดแดงอัดแน่น (HCT) พบปกติ ');
insert into f_comservice values ('276','1H3011','ตรวจคัดกรองระดับความเข้มข้นของเม็ดเลือดแดง (HB) หรือปริมาณเม็ดเลือดแดงอัดแน่น (HCT) พบผิดปกติ และได้ยาเสริมธาตุเหล็กเพื่อรักษา');
insert into f_comservice values ('277','1H3012','ติดตามผลการตรวจคัดกรองระดับความเข้มข้นของเม็ดเลือดแดง (HB) หรือปริมาณเม็ดเลือดแดงอัดแน่น (HCT) หลังรับประทานยา 1 เดือน ผลปกติ');
insert into f_comservice values ('278','1H3013','ติดตามผลการตรวจคัดกรองระดับความเข้มข้นของเม็ดเลือดแดง (HB) หรือปริมาณเม็ดเลือดแดงอัดแน่น (HCT) หลังรับประทานยา 1 เดือน ผลผิดปกติและส่งต่อ');
insert into f_comservice values ('279','1H3030','นักเรียนได้รับการตรวจคัดกรองเด็กอ้วนกลุ่มเสี่ยง Obesity Sign ไม่พบ Obesity Sign');
insert into f_comservice values ('280','1H3031','นักเรียนได้รับการตรวจคัดกรองเด็กอ้วนกลุ่มเสี่ยง พบ Obesity Sign 3 ใน 4 ข้อ ส่งต่อรักษา');
insert into f_comservice values ('281','1H3032','นักเรียนได้รับการตรวจคัดกรองเด็กอ้วนกลุ่มเสี่ยง พบ Obesity Sign 3 ใน 4 ข้อ ไม่ได้ส่งต่อรักษา');
insert into f_comservice values ('282','1I01','การบริการนวดเพื่อการส่งเสริมสุขภาพที่บ้าน');
insert into f_comservice values ('283','1I020','การบริการประคบสมุนไพรเพื่อการส่งเสริมสุขภาพที่บ้าน');
insert into f_comservice values ('284','1I04','การบริการอบสมุนไพรเพื่อการส่งเสริมสุขภาพที่บ้าน');
insert into f_comservice values ('285','1I05','การบริการหญิงหลังคลอดด้วยการอบสมุนไพรที่บ้าน');
insert into f_comservice values ('286','1I050','การบริการหญิงหลังคลอดด้วยการอาบสมุนไพรที่บ้าน');
insert into f_comservice values ('287','1I051','การบริการหญิงหลังคลอดด้วยการประคบสมุนไพรที่บ้าน');
insert into f_comservice values ('288','1I052','การบริการหญิงหลังคลอดด้วยการนวดที่บ้าน');
insert into f_comservice values ('289','1I053','การบริการหญิงหลังคลอดด้วยการนวดเต้านมที่บ้าน');
insert into f_comservice values ('290','1I058','การบริการหญิงหลังคลอดด้วยวิธีอื่น ที่บ้าน');
insert into f_comservice values ('291','1I06','การบริการหญิงหลังคลอดด้วยการทับหม้อเกลือที่บ้าน');
insert into f_comservice values ('292','1I060','การบริการหญิงหลังคลอดด้วยการนั่งถ่านที่บ้าน');
insert into f_comservice values ('293','1I070','การให้คำแนะนำ การสอน สาธิตการบริหารร่างกายด้วยมณีเวชที่บ้าน');
insert into f_comservice values ('294','1I071','การให้คำแนะนำ หญิงหลังคลอด และการบริบาลทารกด้านการแพทย์แผนไทยที่บ้าน');
insert into f_comservice values ('295','1I080','การให้บริการพอกยาสมุนไพร ที่บ้าน');
insert into f_comservice values ('296','1I081','การให้บริการแช่ยาสมุนไพร ที่บ้าน');
insert into f_comservice values ('297','1I100','การให้บริการกดจุดบำบัด (Acupressure)');
insert into f_comservice values ('298','1I101','การให้บริการนวดปรับสมดุลร่างกาย');
insert into f_comservice values ('299','1I102','การให้บริการสมาธิบำบัด');
insert into f_comservice values ('300','1I103','การให้บริการนวดสวีดิช (Swedish Massage)');
insert into f_comservice values ('301','1I104','การให้บริการนวดเพื่อสุขภาพแบบเนิฟว์แอสซิสต์ (Nerve Assist)');
insert into f_comservice values ('302','1I105','การให้บริการกดจุดสะท้อนเท้า (Foot Reflexology)');
insert into f_comservice values ('303','1I110','การให้บริการเกอร์สันบำบัด (Gerson Therapy)');
insert into f_comservice values ('304','1I111','การให้บริการคีโตเจนิคไดเอต (Ketogenic Diet) /อาหารพร่องแป้ง (Low-Carbohydrate Diet)');
insert into f_comservice values ('305','1I112','การให้บริการแมคโครไบโอติกส์ (Macrobiotics)');
insert into f_comservice values ('306','1I113','การให้บริการอาหารปรับสมดุลฤทธิ์ ร้อน – เย็น');
insert into f_comservice values ('307','1I180','การให้บริการจินตภาพบำบัด (Visualization Therapy)');
insert into f_comservice values ('308','1I181','การให้สมาธิบำบัด/พลังบำบัด');
insert into f_comservice values ('309','1I182','การให้บริการกัวซา (Guasa)');
insert into f_comservice values ('310','1I183','การให้บริการการแพทย์ทางเลือกวิถีธรรม');


-----comactivity
insert into f_comactivity values ('48','2D040','รณรงค์การแนะนําการใช้สมุนไพรควบคุมป้องกันโรค');
insert into f_comactivity values ('49','2D041','รณรงค์การ/แนะนําการใช้สมุนไพรในโรงเรียน');
insert into f_comactivity values ('50','2D080','ให้ความรู้/สถานที่ตรวจบริการ ด้านเอชไอวีโรคติดต่อทางเพศสัมพันธ์ และ หรือ Harm Reduction');
insert into f_comactivity values ('51','2D081','ให้อุปกรณ์ป้องกันการติดเชื้อเอชไอวี และโรคติดต่อทางเพศสัมพันธ์ (ถุงยางอนามัย สารหล่อลื่น และ หรือ กระบอกฉีดและเข็มสะอาด)');
insert into f_comactivity values ('52','2F301','แมคโครไบโอติกส์ (Macrobiotics) ');
insert into f_comactivity values ('53','2F302','มังสวิรัติ');
insert into f_comactivity values ('54','2F303','คีโตเจนิค ไดเอต (Ketogenic Diet) / อาหารพร่องแป้ง (Low-Carbohydrate Diet)');
insert into f_comactivity values ('55','2F304','อาหารปรับสมดุลฤทธิ์ร้อน-เย็น/อาหารสุขภาพ/เกอร์สันบำบัด (Gerson Therapy)');
insert into f_comactivity values ('56','2F310','การให้ความรู้ เรื่อง การนำหลักธรรมศาสนามาปฏิบัติในชีวิตประจำวัน');
insert into f_comactivity values ('57','2F311','การให้ความรู้ เรื่อง การปรับสมดุลร่างกาย / ดุลยภาพบำบัด');
insert into f_comactivity values ('58','2F312','การให้ความรู้ เรื่อง กายและจิต (Mind and Body Intervention) / การออกกำลังกาย');
insert into f_comactivity values ('59','2F313','การให้ความรู้ เรื่อง สวดมนต์บำบัดและ/หรือสมาธิบำบัด');
insert into f_comactivity values ('60','2F314','การให้ความรู้ เรื่อง สมุนไพรในการดูแลสุขภาพ');
insert into f_comactivity values ('61','2F315','การให้ความรู้ เรื่อง พลังบำบัด');


---procedure_icd9
insert into b_icd9 values ('0555000001595','238703A','Sealant per tooth (Sealant 16)','Sealant per tooth (Sealant 16)','','','1');
insert into b_icd9 values ('0555000001596','238703B','Sealant per tooth (Sealant 17)','Sealant per tooth (Sealant 17)','','','1');
insert into b_icd9 values ('0555000001597','238703C','Sealant per tooth (Sealant 26)','Sealant per tooth (Sealant 26)','','','1');
insert into b_icd9 values ('0555000001598','238703D','Sealant per tooth (Sealant 27)','Sealant per tooth (Sealant 27)','','','1');
insert into b_icd9 values ('0555000001599','238703E','Sealant per tooth (Sealant 36)','Sealant per tooth (Sealant 36)','','','1');
insert into b_icd9 values ('0555000001600','238703F','Sealant per tooth (Sealant 37)','Sealant per tooth (Sealant 37)','','','1');
insert into b_icd9 values ('0555000001601','238703G','Sealant per tooth (Sealant 46)','Sealant per tooth (Sealant 46)','','','1');
insert into b_icd9 values ('0555000001602','238703H','Sealant per tooth (Sealant 47)','Sealant per tooth (Sealant 47)','','','1');

--labfu
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์โครโมโซมเพื่อศึกษาความแตกแยก  จากเลือด',lab_ncd_note = 'Chromosome analysis, breakage study, whole blood' where lab_icd10tm = '0290201';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000001','','การวิเคราะห์โครโมโซมเพื่อศึกษาความแตกแยก  จากเลือด','Chromosome analysis, breakage study, whole blood','1','0290201'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0290201');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์โครโมโซมโดยการเพาะเลี้ยงและการกระตุ้นการทำให้เกิด กระบวนการไมโทซิสในเซลล์  จากเลือด',lab_ncd_note = 'Chromosome analysis, cultured with mitogenic stimulation, whole blood' where lab_icd10tm = '0290401';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000002','','การวิเคราะห์โครโมโซมโดยการเพาะเลี้ยงและการกระตุ้นการทำให้เกิด กระบวนการไมโทซิสในเซลล์  จากเลือด','Chromosome analysis, cultured with mitogenic stimulation, whole blood','1','0290401'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0290401');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์โครโมโซมการเพาะเลี้ยงโดยไม่กระตุ้นการทำให้เกิดกระบวนการ ไมโทซิสในเซลล์  จากเลือด',lab_ncd_note = 'Chromosome analysis, cultured without mitogenic stimulation, whole blood' where lab_icd10tm = '0290601';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000003','','การวิเคราะห์โครโมโซมการเพาะเลี้ยงโดยไม่กระตุ้นการทำให้เกิดกระบวนการ ไมโทซิสในเซลล์  จากเลือด','Chromosome analysis, cultured without mitogenic stimulation, whole blood','1','0290601'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0290601');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์โครโมโซมโดยการเพาะเลี้ยงเซลล์  จากน้ำคร่ำ',lab_ncd_note = 'Chromosome analysis, cultured, amniotic fluid ' where lab_icd10tm = '0290805';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000004','','การวิเคราะห์โครโมโซมโดยการเพาะเลี้ยงเซลล์  จากน้ำคร่ำ','Chromosome analysis, cultured, amniotic fluid ','1','0290805'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0290805');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์โครโมโซมโดยการเพาะเลี้ยง  จากไขกระดูก',lab_ncd_note = 'Chromosome analysis, cultured, bone marrow ' where lab_icd10tm = '0290827';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000005','','การวิเคราะห์โครโมโซมโดยการเพาะเลี้ยง  จากไขกระดูก','Chromosome analysis, cultured, bone marrow ','1','0290827'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0290827');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์โครโมโซมโดยการเพาะเลี้ยงเซลล์  จากต่อมน้ำเหลือง',lab_ncd_note = 'Chromosome analysis, cultured, lymph nodes ' where lab_icd10tm = '0290828';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000006','','การวิเคราะห์โครโมโซมโดยการเพาะเลี้ยงเซลล์  จากต่อมน้ำเหลือง','Chromosome analysis, cultured, lymph nodes ','1','0290828'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0290828');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์โครโมโซมโดยการเพาะเลี้ยงเซลล์  จากผลิตภัณฑ์จากการปฏิสนธิ',lab_ncd_note = 'Chromosome analysis, cultured, products of conception ' where lab_icd10tm = '0290829';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000007','','การวิเคราะห์โครโมโซมโดยการเพาะเลี้ยงเซลล์  จากผลิตภัณฑ์จากการปฏิสนธิ','Chromosome analysis, cultured, products of conception ','1','0290829'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0290829');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์โครโมโซมโดยการเพาะเลี้ยงเซลล์  จากผิวหนัง',lab_ncd_note = 'Chromosome analysis, cultured, skin ' where lab_icd10tm = '0290830';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000008','','การวิเคราะห์โครโมโซมโดยการเพาะเลี้ยงเซลล์  จากผิวหนัง','Chromosome analysis, cultured, skin ','1','0290830'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0290830');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์โครโมโซมโดยการเพาะเลี้ยงเซลล์  จากมะเร็งชนิดเป็นก้อน',lab_ncd_note = 'Chromosome analysis, cultured, solid tumor ' where lab_icd10tm = '0290831';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000009','','การวิเคราะห์โครโมโซมโดยการเพาะเลี้ยงเซลล์  จากมะเร็งชนิดเป็นก้อน','Chromosome analysis, cultured, solid tumor ','1','0290831'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0290831');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์โครโมโซมโดยการเพาะเลี้ยงเซลล์  จากเนื้อเยื่ออื่นๆ ',lab_ncd_note = 'Chromosome analysis, cultured, tissue NEC ' where lab_icd10tm = '0290850';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000010','','การวิเคราะห์โครโมโซมโดยการเพาะเลี้ยงเซลล์  จากเนื้อเยื่ออื่นๆ ','Chromosome analysis, cultured, tissue NEC ','1','0290850'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0290850');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์โครโมโซมโดยตรง [เช่น immediate harvest]  จากไขกระดูก',lab_ncd_note = 'Chromosome analysis, direct [e.g. immediate harvest], bone marrow ' where lab_icd10tm = '0291027';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000011','','การวิเคราะห์โครโมโซมโดยตรง [เช่น immediate harvest]  จากไขกระดูก','Chromosome analysis, direct [e.g. immediate harvest], bone marrow ','1','0291027'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0291027');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์โครโมโซมเอ็กซ์ที่เปราะบาง  จากเลือด',lab_ncd_note = 'Chromosome analysis, fragile X, whole blood' where lab_icd10tm = '0291401';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000012','','การวิเคราะห์โครโมโซมเอ็กซ์ที่เปราะบาง  จากเลือด','Chromosome analysis, fragile X, whole blood','1','0291401'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0291401');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์โครโมโซมด้วยเทคนิคที่ให้ความละเอียดสูงร่วมกับด้วยการกระตุ้นการทำ ให้เกิดกระบวนการไมโทซิสในเซลล์ จากเลือด',lab_ncd_note = 'Chromosome analysis, hi-resolution with mitogenic stimulation, whole blood' where lab_icd10tm = '0291601';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000013','','การวิเคราะห์โครโมโซมด้วยเทคนิคที่ให้ความละเอียดสูงร่วมกับด้วยการกระตุ้นการทำ ให้เกิดกระบวนการไมโทซิสในเซลล์ จากเลือด','Chromosome analysis, hi-resolution with mitogenic stimulation, whole blood','1','0291601'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0291601');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์โครโมโซมด้วยเทคนิคที่ให้ความละเอียดสูงและไม่มีการกระตุ้นการทำให้ เกิดกระบวนการไมโทซิสในเซลล์  จากเลือด',lab_ncd_note = 'Chromosome analysis, hi-resolution without mitogenic stimulation, whole blood' where lab_icd10tm = '0291801';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000014','','การวิเคราะห์โครโมโซมด้วยเทคนิคที่ให้ความละเอียดสูงและไม่มีการกระตุ้นการทำให้ เกิดกระบวนการไมโทซิสในเซลล์  จากเลือด','Chromosome analysis, hi-resolution without mitogenic stimulation, whole blood','1','0291801'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0291801');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์โครโมโซมด้วยเทคนิคที่ให้ความละเอียดสูง  จากน้ำคร่ำ',lab_ncd_note = 'Chromosome analysis, hi-resolution, amniotic fluid' where lab_icd10tm = '0292005';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000015','','การวิเคราะห์โครโมโซมด้วยเทคนิคที่ให้ความละเอียดสูง  จากน้ำคร่ำ','Chromosome analysis, hi-resolution, amniotic fluid','1','0292005'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0292005');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์โครโมโซมด้วยเทคนิคที่ให้ความละเอียดสูง  จากไขกระดูก',lab_ncd_note = 'Chromosome analysis, hi-resolution, bone marrow' where lab_icd10tm = '0292027';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000016','','การวิเคราะห์โครโมโซมด้วยเทคนิคที่ให้ความละเอียดสูง  จากไขกระดูก','Chromosome analysis, hi-resolution, bone marrow','1','0292027'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0292027');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์โครโมโซมโดยการเพาะเลี้ยงเซลล์ที่ใช้เวลานาน [เช่น > 72 ชั่วโมง]  จากระบบหัวใจและหลอดเลือด',lab_ncd_note = 'Chromosome analysis, long term culture [e.g. > 72 hours], CVS' where lab_icd10tm = '0292232';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000017','','การวิเคราะห์โครโมโซมโดยการเพาะเลี้ยงเซลล์ที่ใช้เวลานาน [เช่น > 72 ชั่วโมง]  จากระบบหัวใจและหลอดเลือด','Chromosome analysis, long term culture [e.g. > 72 hours], CVS','1','0292232'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0292232');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์โครโมโซมโดยการเพาะเลี้ยงเซลล์ที่ใช้เวลาสั้น [เช่น < 72 ชั่วโมง]  จากระบบหัวใจและหลอดเลือด',lab_ncd_note = 'Chromosome analysis, short term culture [e.g. < 72 hours], CVS' where lab_icd10tm = '0292432';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000018','','การวิเคราะห์โครโมโซมโดยการเพาะเลี้ยงเซลล์ที่ใช้เวลาสั้น [เช่น < 72 ชั่วโมง]  จากระบบหัวใจและหลอดเลือด','Chromosome analysis, short term culture [e.g. < 72 hours], CVS','1','0292432'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0292432');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์โครโมโซมโดยการเพาะเลี้ยงเซลล์ที่ล้มเหลวหรือถูกยกเลิก จากตัวอย่าง สิ่งส่งตรวจชนิดอื่นๆ',lab_ncd_note = 'Chromosome analysis, failed/abandoned culture any specimen types' where lab_icd10tm = '0296099';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000019','','การวิเคราะห์โครโมโซมโดยการเพาะเลี้ยงเซลล์ที่ล้มเหลวหรือถูกยกเลิก จากตัวอย่าง สิ่งส่งตรวจชนิดอื่นๆ','Chromosome analysis, failed/abandoned culture any specimen types','1','0296099'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0296099');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'Chromosome analysis by fluorescence in situ hybridization (FISH)' where lab_icd10tm = '0296101';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000020','','-','Chromosome analysis by fluorescence in situ hybridization (FISH)','1','0296101'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0296101');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'Chromosome analysis in Pradi Willi syndrome' where lab_icd10tm = '0296102';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000021','','-','Chromosome analysis in Pradi Willi syndrome','1','0296102'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0296102');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'Chromosome analysis in Angelman syndrome' where lab_icd10tm = '0296103';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000022','','-','Chromosome analysis in Angelman syndrome','1','0296103'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0296103');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'Chromosome analysis in spinal muscular dystrophy' where lab_icd10tm = '0296104';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000023','','-','Chromosome analysis in spinal muscular dystrophy','1','0296104'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0296104');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'Sry gene detection' where lab_icd10tm = '0296105';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000024','','-','Sry gene detection','1','0296105'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0296105');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเก็บและตรวจชิ้นเนื้อศัลยกรรม - ระดับ 1  โดยการตรวจลักษณะภายนอกเท่านั้น',lab_ncd_note = 'Surgical specimen - Level 1, gross examination only' where lab_icd10tm = '0300250';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000025','','การเก็บและตรวจชิ้นเนื้อศัลยกรรม - ระดับ 1  โดยการตรวจลักษณะภายนอกเท่านั้น','Surgical specimen - Level 1, gross examination only','1','0300250'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0300250');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเก็บและตรวจชิ้นเนื้อศัลยกรรม - ระดับ 2  โดยการตรวจลักษณะภายนอกและ การตรวจด้วยกล้องจุลทรรศน์ - เพื่อการระบุเท่านั้น ',lab_ncd_note = 'Surgical specimen - Level 2, gross and microscopic examination - identification only' where lab_icd10tm = '0300450';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000026','','การเก็บและตรวจชิ้นเนื้อศัลยกรรม - ระดับ 2  โดยการตรวจลักษณะภายนอกและ การตรวจด้วยกล้องจุลทรรศน์ - เพื่อการระบุเท่านั้น ','Surgical specimen - Level 2, gross and microscopic examination - identification only','1','0300450'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0300450');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเก็บและตรวจชิ้นเนื้อศัลยกรรม - ระดับ 3  โดยการตรวจลักษณะภายนอกและ การตรวจด้วยกล้องจุลทรรศน์ - ที่ไม่ยุ่งยาก',lab_ncd_note = 'Surgical specimen - Level 3, gross and microscopic examination – uncomplicated' where lab_icd10tm = '0300650';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000027','','การเก็บและตรวจชิ้นเนื้อศัลยกรรม - ระดับ 3  โดยการตรวจลักษณะภายนอกและ การตรวจด้วยกล้องจุลทรรศน์ - ที่ไม่ยุ่งยาก','Surgical specimen - Level 3, gross and microscopic examination – uncomplicated','1','0300650'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0300650');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเก็บและตรวจชิ้นเนื้อศัลยกรรม - ระดับ 4  โดยการตรวจลักษณะภายนอกและ การตรวจด้วยกล้องจุลทรรศน์ - ที่ยุ่งยากโดยปราศจากการชำแหละ',lab_ncd_note = 'Surgical specimen - Level 4, gross and microscopic examination - complicated without dissection' where lab_icd10tm = '0300850';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000028','','การเก็บและตรวจชิ้นเนื้อศัลยกรรม - ระดับ 4  โดยการตรวจลักษณะภายนอกและ การตรวจด้วยกล้องจุลทรรศน์ - ที่ยุ่งยากโดยปราศจากการชำแหละ','Surgical specimen - Level 4, gross and microscopic examination - complicated without dissection','1','0300850'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0300850');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเก็บและตรวจชิ้นเนื้อศัลยกรรม - ระดับ 5  โดยการตรวจลักษณะภายนอกและ การตรวจด้วยกล้องจุลทรรศน์ - ที่ยุ่งยากร่วมกับมีการชำแหละ',lab_ncd_note = 'Surgical specimen - Level 5, gross and microscopic examination - complicated with dissection' where lab_icd10tm = '0301050';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000029','','การเก็บและตรวจชิ้นเนื้อศัลยกรรม - ระดับ 5  โดยการตรวจลักษณะภายนอกและ การตรวจด้วยกล้องจุลทรรศน์ - ที่ยุ่งยากร่วมกับมีการชำแหละ','Surgical specimen - Level 5, gross and microscopic examination - complicated with dissection','1','0301050'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0301050');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเก็บและตรวจชิ้นเนื้อศัลยกรรม - ระดับ 6  โดยการตรวจลักษณะภายนอกและ การตรวจด้วยกล้องจุลทรรศน์ - ที่มีความซับซ้อน',lab_ncd_note = 'Surgical specimen - Level 6, gross and microscopic examination – complex' where lab_icd10tm = '0301250';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000030','','การเก็บและตรวจชิ้นเนื้อศัลยกรรม - ระดับ 6  โดยการตรวจลักษณะภายนอกและ การตรวจด้วยกล้องจุลทรรศน์ - ที่มีความซับซ้อน','Surgical specimen - Level 6, gross and microscopic examination – complex','1','0301250'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0301250');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเก็บและตรวจชิ้นเนื้อศัลยกรรม  โดยการตรวจด้วยกล้องจุลทรรศน์เท่านั้น  แผ่น สไลด์ และ/หรือ บล็อกเนื้อเยื่อ เท่านั้น',lab_ncd_note = 'Surgical specimen, microscopic examination only, slide(s) and/or block(s) only ' where lab_icd10tm = '0302250';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000031','','การเก็บและตรวจชิ้นเนื้อศัลยกรรม  โดยการตรวจด้วยกล้องจุลทรรศน์เท่านั้น  แผ่น สไลด์ และ/หรือ บล็อกเนื้อเยื่อ เท่านั้น','Surgical specimen, microscopic examination only, slide(s) and/or block(s) only ','1','0302250'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0302250');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การชันสูตรศพ โดยการตรวจลักษณะภายนอกและการตรวจด้วยกล้องจุลทรรศน์',lab_ncd_note = 'Autopsy, gross and microscopic examination' where lab_icd10tm = '0310275';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000032','','การชันสูตรศพ โดยการตรวจลักษณะภายนอกและการตรวจด้วยกล้องจุลทรรศน์','Autopsy, gross and microscopic examination','1','0310275'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0310275');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การชันสูตรศพ โดยการตรวจลักษณะภายนอกและการตรวจด้วยกล้องจุลทรรศน์ ร่วมกับมีการตรวจเนื้อสมอง',lab_ncd_note = 'Autopsy, gross and microscopic examination, with brain' where lab_icd10tm = '0310475';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000033','','การชันสูตรศพ โดยการตรวจลักษณะภายนอกและการตรวจด้วยกล้องจุลทรรศน์ ร่วมกับมีการตรวจเนื้อสมอง','Autopsy, gross and microscopic examination, with brain','1','0310475'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0310475');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การชันสูตรศพ โดยการตรวจลักษณะภายนอกและการตรวจด้วย กล้องจุลทรรศน์ร่วมกับมีการตรวจเนื้อสมองและไขสันหลัง',lab_ncd_note = 'Autopsy, gross and microscopic examination, with brain and spinal cord' where lab_icd10tm = '0310675';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000034','','การชันสูตรศพ โดยการตรวจลักษณะภายนอกและการตรวจด้วย กล้องจุลทรรศน์ร่วมกับมีการตรวจเนื้อสมองและไขสันหลัง','Autopsy, gross and microscopic examination, with brain and spinal cord','1','0310675'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0310675');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การชันสูตรศพ โดยการตรวจลักษณะภายนอกและการตรวจด้วยกล้องจุลทรรศน์ โดยจำกัดอยู่ที่อวัยวะเดียวหรือบริเวณเดียว ',lab_ncd_note = 'Autopsy, gross and microscopic examination, limited [e.g. single organ or region]' where lab_icd10tm = '0310875';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000035','','การชันสูตรศพ โดยการตรวจลักษณะภายนอกและการตรวจด้วยกล้องจุลทรรศน์ โดยจำกัดอยู่ที่อวัยวะเดียวหรือบริเวณเดียว ','Autopsy, gross and microscopic examination, limited [e.g. single organ or region]','1','0310875'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0310875');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การชันสูตรศพทางนิติเวชศาสตร์ โดยการตรวจลักษณะภายนอกและการตรวจด้วย กล้องจุลทรรศน์      ',lab_ncd_note = 'Autopsy, gross and microscopic examination, medico-legal/forensic' where lab_icd10tm = '0311075';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000036','','การชันสูตรศพทางนิติเวชศาสตร์ โดยการตรวจลักษณะภายนอกและการตรวจด้วย กล้องจุลทรรศน์      ','Autopsy, gross and microscopic examination, medico-legal/forensic','1','0311075'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0311075');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การชันสูตรศพ  โดยการตรวจลักษณะภายนอกเท่านั้น',lab_ncd_note = 'Autopsy, gross examination only' where lab_icd10tm = '0311275';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000037','','การชันสูตรศพ  โดยการตรวจลักษณะภายนอกเท่านั้น','Autopsy, gross examination only','1','0311275'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0311275');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การชันสูตรศพ โดยการตรวจลักษณะภายนอกเท่านั้น ร่วมกับมีการตรวจเนื้อสมอง',lab_ncd_note = 'Autopsy, gross examination only, with brain' where lab_icd10tm = '0311475';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000038','','การชันสูตรศพ โดยการตรวจลักษณะภายนอกเท่านั้น ร่วมกับมีการตรวจเนื้อสมอง','Autopsy, gross examination only, with brain','1','0311475'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0311475');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การชันสูตรศพ โดยการตรวจลักษณะภายนอกเท่านั้น ร่วมกับมีการตรวจเนื้อสมอง และไขสันหลัง',lab_ncd_note = 'Autopsy, gross examination only, with brain and spinal cord' where lab_icd10tm = '0311675';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000039','','การชันสูตรศพ โดยการตรวจลักษณะภายนอกเท่านั้น ร่วมกับมีการตรวจเนื้อสมอง และไขสันหลัง','Autopsy, gross examination only, with brain and spinal cord','1','0311675'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0311675');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การชันสูตรศพ โดยการตรวจลักษณะภายนอกเท่านั้น และเป็นการตรวจภายนอกที่จำกัดอยู่ที่อวัยวะเดียวหรือบริเวณเดียว',lab_ncd_note = 'Autopsy, gross examination only, limited [e.g. single organ/region/external exam' where lab_icd10tm = '0311875';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000040','','การชันสูตรศพ โดยการตรวจลักษณะภายนอกเท่านั้น และเป็นการตรวจภายนอกที่จำกัดอยู่ที่อวัยวะเดียวหรือบริเวณเดียว','Autopsy, gross examination only, limited [e.g. single organ/region/external exam','1','0311875'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0311875');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การชันสูตรศพทางนิติเวชศาสตร์โดยการตรวจลักษณะภายนอกเท่านั้น',lab_ncd_note = 'Autopsy, gross examination only, medico-legal/forensic' where lab_icd10tm = '0312075';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000041','','การชันสูตรศพทางนิติเวชศาสตร์โดยการตรวจลักษณะภายนอกเท่านั้น','Autopsy, gross examination only, medico-legal/forensic','1','0312075'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0312075');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองความผิดปกติของเซลล์ (จากสิ่งป้าย)  จากปัสสาวะ',lab_ncd_note = 'Cytology screening (of smears), urine' where lab_icd10tm = '0320203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000042','','การตรวจคัดกรองความผิดปกติของเซลล์ (จากสิ่งป้าย)  จากปัสสาวะ','Cytology screening (of smears), urine','1','0320203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0320203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองความผิดปกติของเซลล์ (จากสิ่งป้าย)  จากเสมหะ',lab_ncd_note = 'Cytology screening (of smears), sputum' where lab_icd10tm = '0320209';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000043','','การตรวจคัดกรองความผิดปกติของเซลล์ (จากสิ่งป้าย)  จากเสมหะ','Cytology screening (of smears), sputum','1','0320209'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0320209');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองความผิดปกติของเซลล์ (จากสิ่งป้าย)  จากของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Cytology screening (of smears), fluid NEC' where lab_icd10tm = '0320215';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000044','','การตรวจคัดกรองความผิดปกติของเซลล์ (จากสิ่งป้าย)  จากของเหลวอื่นๆ มิได้จำแนก','Cytology screening (of smears), fluid NEC','1','0320215'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0320215');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองความผิดปกติของเซลล์ (จากสิ่งป้าย) จากแผ่นสไลด์/สิ่งป้ายอื่นๆ มิได้จำแนก',lab_ncd_note = 'Cytology screening (of smears), slide/smear NEC' where lab_icd10tm = '0320271';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000045','','การตรวจคัดกรองความผิดปกติของเซลล์ (จากสิ่งป้าย) จากแผ่นสไลด์/สิ่งป้ายอื่นๆ มิได้จำแนก','Cytology screening (of smears), slide/smear NEC','1','0320271'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0320271');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองความผิดปกติของเซลล์ (จากสิ่งป้าย) จากปากมดลูก-ช่องคลอด [เช่น สิ่งส่งตรวจทางนรีเวช] รวมถึงการตรวจมะเร็งปากมดลูก',lab_ncd_note = 'Cytology screening (of smears), cervico-vaginal [e.g. GYN specimen]  inc.  exam of PAP smear' where lab_icd10tm = '0320277';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000046','','การตรวจคัดกรองความผิดปกติของเซลล์ (จากสิ่งป้าย) จากปากมดลูก-ช่องคลอด [เช่น สิ่งส่งตรวจทางนรีเวช] รวมถึงการตรวจมะเร็งปากมดลูก','Cytology screening (of smears), cervico-vaginal [e.g. GYN specimen]  inc.  exam of PAP smear','1','0320277'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0320277');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองความผิดปกติของเซลล์ (จากสิ่งป้าย)  จากการดูดเนื้อเยื่อหรือเซลล์ ในร่างกายด้วยเข็มขนาดเล็ก (ตำแหน่งต่างๆ)',lab_ncd_note = 'Cytology screening (of smears), fine needle aspirate (any site)' where lab_icd10tm = '0320278';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000047','','การตรวจคัดกรองความผิดปกติของเซลล์ (จากสิ่งป้าย)  จากการดูดเนื้อเยื่อหรือเซลล์ ในร่างกายด้วยเข็มขนาดเล็ก (ตำแหน่งต่างๆ)','Cytology screening (of smears), fine needle aspirate (any site)','1','0320278'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0320278');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองความผิดปกติของเซลล์ (จากสิ่งป้าย)  จากการแปรงหรือล้าง  (ตำแหน่งต่างๆ)',lab_ncd_note = 'Cytology screening (of smears), brushing/washing (any site)' where lab_icd10tm = '0320279';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000048','','การตรวจคัดกรองความผิดปกติของเซลล์ (จากสิ่งป้าย)  จากการแปรงหรือล้าง  (ตำแหน่งต่างๆ)','Cytology screening (of smears), brushing/washing (any site)','1','0320279'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0320279');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจโครมาทินเพื่อระบุเพศ  จากแผ่นสไลด์/สิ่งป้ายอื่นๆ มิได้จำแนก',lab_ncd_note = 'Sex chromatin identification, slide/smear NEC' where lab_icd10tm = '0323271';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000049','','การตรวจโครมาทินเพื่อระบุเพศ  จากแผ่นสไลด์/สิ่งป้ายอื่นๆ มิได้จำแนก','Sex chromatin identification, slide/smear NEC','1','0323271'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0323271');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การประเมินผลความผิดปกติของเซลล์ที่ผลิตฮอร์โมน (Cytohormonal)  จากแผ่น สไลด์/สิ่งป้ายอื่นๆ มิได้จำแนก',lab_ncd_note = 'Cytohormonal evaluation, slide/smear NEC' where lab_icd10tm = '0323471';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000050','','การประเมินผลความผิดปกติของเซลล์ที่ผลิตฮอร์โมน (Cytohormonal)  จากแผ่น สไลด์/สิ่งป้ายอื่นๆ มิได้จำแนก','Cytohormonal evaluation, slide/smear NEC','1','0323471'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0323471');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจของเหลวอื่นๆ มิได้จำแนก ด้วยกล้องจุลทรรศน์อิเล็กตรอน',lab_ncd_note = 'Electron microscopy, fluid NEC' where lab_icd10tm = '0330215';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000051','','การตรวจของเหลวอื่นๆ มิได้จำแนก ด้วยกล้องจุลทรรศน์อิเล็กตรอน','Electron microscopy, fluid NEC','1','0330215'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0330215');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจเนื้อเยื่ออื่นๆ มิได้จำแนก ด้วยกล้องจุลทรรศน์อิเล็กตรอน',lab_ncd_note = 'Electron microscopy, tissue NEC' where lab_icd10tm = '0330250';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000052','','การตรวจเนื้อเยื่ออื่นๆ มิได้จำแนก ด้วยกล้องจุลทรรศน์อิเล็กตรอน','Electron microscopy, tissue NEC','1','0330250'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0330250');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจแยก (จุลินทรีย์) ด้วยกล้องจุลทรรศน์อิเล็กตรอน',lab_ncd_note = 'Electron microscopy, isolate (microbial)' where lab_icd10tm = '0330270';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000053','','การตรวจแยก (จุลินทรีย์) ด้วยกล้องจุลทรรศน์อิเล็กตรอน','Electron microscopy, isolate (microbial)','1','0330270'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0330270');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ block ด้วยกล้องจุลทรรศน์อิเล็กตรอน',lab_ncd_note = 'Electron microscopy, block' where lab_icd10tm = '0330274';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000054','','การตรวจ block ด้วยกล้องจุลทรรศน์อิเล็กตรอน','Electron microscopy, block','1','0330274'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0330274');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ grid ด้วยกล้องจุลทรรศน์อิเล็กตรอน',lab_ncd_note = 'Electron microscopy, grid' where lab_icd10tm = '0330276';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000055','','การตรวจ grid ด้วยกล้องจุลทรรศน์อิเล็กตรอน','Electron microscopy, grid','1','0330276'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0330276');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์ทางสัณฐานวิทยาของกล้ามเนื้อ',lab_ncd_note = 'Morphometric analysis, muscle' where lab_icd10tm = '0340226';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000056','','การวิเคราะห์ทางสัณฐานวิทยาของกล้ามเนื้อ','Morphometric analysis, muscle','1','0340226'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0340226');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์ทางสัณฐานวิทยาของมะเร็งชนิดเป็นก้อน',lab_ncd_note = 'Morphometric analysis, solid tumor' where lab_icd10tm = '0340231';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000057','','การวิเคราะห์ทางสัณฐานวิทยาของมะเร็งชนิดเป็นก้อน','Morphometric analysis, solid tumor','1','0340231'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0340231');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์ทางสัณฐานวิทยาของของกระดูก',lab_ncd_note = 'Morphometric analysis, bone' where lab_icd10tm = '0340233';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000058','','การวิเคราะห์ทางสัณฐานวิทยาของของกระดูก','Morphometric analysis, bone','1','0340233'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0340233');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์ทางสัณฐานวิทยาของเส้นประสาท',lab_ncd_note = 'Morphometric analysis, nerve' where lab_icd10tm = '0340234';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000059','','การวิเคราะห์ทางสัณฐานวิทยาของเส้นประสาท','Morphometric analysis, nerve','1','0340234'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0340234');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์ทางสัณฐานวิทยาของเนื้อเยื่ออื่นๆ มิได้จำแนก',lab_ncd_note = 'Morphometric analysis, tissue NEC' where lab_icd10tm = '0340250';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000060','','การวิเคราะห์ทางสัณฐานวิทยาของเนื้อเยื่ออื่นๆ มิได้จำแนก','Morphometric analysis, tissue NEC','1','0340250'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0340250');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์น้ำอสุจิด้วยการตรวจครบถ้วน',lab_ncd_note = 'Semen analysis, complete examination' where lab_icd10tm = '0350208';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000061','','การวิเคราะห์น้ำอสุจิด้วยการตรวจครบถ้วน','Semen analysis, complete examination','1','0350208'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0350208');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์น้ำอสุจิ โดยการตรวจดูจากเคลื่อนไหวและนับจำนวนเท่านั้น',lab_ncd_note = 'Semen analysis, motility and count only' where lab_icd10tm = '0350408';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000062','','การวิเคราะห์น้ำอสุจิ โดยการตรวจดูจากเคลื่อนไหวและนับจำนวนเท่านั้น','Semen analysis, motility and count only','1','0350408'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0350408');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์น้ำอสุจิ โดยการตรวจดูจากเคลื่อนไหวและนับจำนวนเท่านั้น ภายหลังจากการหลั่งเข้าสู่ช่องคลอด ',lab_ncd_note = 'Semen analysis, motility and count only, on post-coital vaginal secretions' where lab_icd10tm = '0350477';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000063','','การวิเคราะห์น้ำอสุจิ โดยการตรวจดูจากเคลื่อนไหวและนับจำนวนเท่านั้น ภายหลังจากการหลั่งเข้าสู่ช่องคลอด ','Semen analysis, motility and count only, on post-coital vaginal secretions','1','0350477'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0350477');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์น้ำอสุจิ โดยการตรวจว่าพบตัวอสุจิ และ/หรือ พบการเคลื่อนไหวของตัวอสุจิเท่านั้น',lab_ncd_note = 'Semen analysis, presence and/or motility of sperm only' where lab_icd10tm = '0350608';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000064','','การวิเคราะห์น้ำอสุจิ โดยการตรวจว่าพบตัวอสุจิ และ/หรือ พบการเคลื่อนไหวของตัวอสุจิเท่านั้น','Semen analysis, presence and/or motility of sperm only','1','0350608'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0350608');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์น้ำอสุจิ โดยการตรวจดูการเคลื่อนไหว และ / หรือ การมีชีวิตของตัวอสุจิในมูกปากมดลูก ',lab_ncd_note = 'Semen analysis, motility and/or viability of sperm, in the presence of cervical mucus' where lab_icd10tm = '0350877';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000065','','การวิเคราะห์น้ำอสุจิ โดยการตรวจดูการเคลื่อนไหว และ / หรือ การมีชีวิตของตัวอสุจิในมูกปากมดลูก ','Semen analysis, motility and/or viability of sperm, in the presence of cervical mucus','1','0350877'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0350877');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตัดชิ้นเนื้อโดยวิธีแช่แข็ง การเตรียมชิ้นเนื้อเยื่ออื่นๆ  มิได้จำแนก',lab_ncd_note = 'Frozen section(s), preparation of, tissue NEC' where lab_icd10tm = '0361250';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000066','','การตัดชิ้นเนื้อโดยวิธีแช่แข็ง การเตรียมชิ้นเนื้อเยื่ออื่นๆ  มิได้จำแนก','Frozen section(s), preparation of, tissue NEC','1','0361250'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0361250');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเก็บรักษาเนื้อเยื่อ/อวัยวะอื่นๆ มิได้จำแนก  แบบแช่แข็ง ',lab_ncd_note = 'Cryopreservation of tissue/organ, tissue NEC' where lab_icd10tm = '0364450';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000067','','การเก็บรักษาเนื้อเยื่อ/อวัยวะอื่นๆ มิได้จำแนก  แบบแช่แข็ง ','Cryopreservation of tissue/organ, tissue NEC','1','0364450'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0364450');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจกระตุ้นเส้นประสาท ',lab_ncd_note = 'Teasing, Nerve' where lab_icd10tm = '0366234';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000068','','การตรวจกระตุ้นเส้นประสาท ','Teasing, Nerve','1','0366234'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0366234');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การให้คำปรึกษาภายในหน่วยทางพยาธิวิทยาศัลยกรรม',lab_ncd_note = 'Referred-in consultation, surgical pathology' where lab_icd10tm = '0370200';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000069','','การให้คำปรึกษาภายในหน่วยทางพยาธิวิทยาศัลยกรรม','Referred-in consultation, surgical pathology','1','0370200'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0370200');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การให้คำปรึกษาภายในหน่วยทางการชันสูตรศพ',lab_ncd_note = 'Referred-in consultation, autopsy' where lab_icd10tm = '0370600';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000070','','การให้คำปรึกษาภายในหน่วยทางการชันสูตรศพ','Referred-in consultation, autopsy','1','0370600'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0370600');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การให้คำปรึกษาภายในหน่วยทางการตรวจด้วยกล้องจุลทรรศน์อิเล็กตรอน',lab_ncd_note = 'Referred-in consultation, electron microscopy' where lab_icd10tm = '0371000';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000071','','การให้คำปรึกษาภายในหน่วยทางการตรวจด้วยกล้องจุลทรรศน์อิเล็กตรอน','Referred-in consultation, electron microscopy','1','0371000'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0371000');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การให้คำปรึกษาการตรวจความผิดปกติของเซลล์ภายในหน่วยทางนรีเวชวิทยา',lab_ncd_note = 'Referred-in consultation, GYN cytology' where lab_icd10tm = '0371200';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000072','','การให้คำปรึกษาการตรวจความผิดปกติของเซลล์ภายในหน่วยทางนรีเวชวิทยา','Referred-in consultation, GYN cytology','1','0371200'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0371200');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การให้คำปรึกษาการตรวจความผิดปกติของเซลล์ภายในหน่วยที่ไม่ใช่ทางนรีเวชวิทยา',lab_ncd_note = 'Referred-in consultation, Non-GYN cytology' where lab_icd10tm = '0371400';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000073','','การให้คำปรึกษาการตรวจความผิดปกติของเซลล์ภายในหน่วยที่ไม่ใช่ทางนรีเวชวิทยา','Referred-in consultation, Non-GYN cytology','1','0371400'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0371400');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การให้คำปรึกษาระหว่างการผ่าตัด ในการตัดชิ้นเนื้อโดยวิธีแช่แข็ง',lab_ncd_note = 'Intra-operative consultation, with frozen section(s)' where lab_icd10tm = '0390200';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000074','','การให้คำปรึกษาระหว่างการผ่าตัด ในการตัดชิ้นเนื้อโดยวิธีแช่แข็ง','Intra-operative consultation, with frozen section(s)','1','0390200'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0390200');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การให้คำปรึกษาระหว่างผ่าตัด โดยปราศจากการตัดชิ้นเนื้อโดยวิธีแช่แข็ง ',lab_ncd_note = 'Intra-operative consultation, without frozen section(s)' where lab_icd10tm = '0390400';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000075','','การให้คำปรึกษาระหว่างผ่าตัด โดยปราศจากการตัดชิ้นเนื้อโดยวิธีแช่แข็ง ','Intra-operative consultation, without frozen section(s)','1','0390400'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0390400');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทบทวนทางพยาธิวิทยาเกี่ยวกับการตรวจชิ้นเนื้อพยาธิวิทยาศัลยกรรม',lab_ncd_note = 'Pathology review, surgical pathology' where lab_icd10tm = '0410200';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000076','','การทบทวนทางพยาธิวิทยาเกี่ยวกับการตรวจชิ้นเนื้อพยาธิวิทยาศัลยกรรม','Pathology review, surgical pathology','1','0410200'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0410200');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทบทวนทางพยาธิวิทยาเกี่ยวกับการชันสูตรศพ',lab_ncd_note = 'Pathology review, autopsy' where lab_icd10tm = '0410600';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000077','','การทบทวนทางพยาธิวิทยาเกี่ยวกับการชันสูตรศพ','Pathology review, autopsy','1','0410600'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0410600');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทบทวนทางพยาธิวิทยาเกี่ยวกับการตรวจด้วยกล้องจุลทรรศน์อิเล็กตรอน',lab_ncd_note = 'Pathology review, electron microscopy' where lab_icd10tm = '0411000';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000078','','การทบทวนทางพยาธิวิทยาเกี่ยวกับการตรวจด้วยกล้องจุลทรรศน์อิเล็กตรอน','Pathology review, electron microscopy','1','0411000'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0411000');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทบทวนทางพยาธิวิทยาเกี่ยวกับการตรวจความผิดปกติของเซลล์ทางนรีเวช',lab_ncd_note = 'Pathology review, GYN cytology' where lab_icd10tm = '0411200';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000079','','การทบทวนทางพยาธิวิทยาเกี่ยวกับการตรวจความผิดปกติของเซลล์ทางนรีเวช','Pathology review, GYN cytology','1','0411200'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0411200');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทบทวนทางพยาธิวิทยาเกี่ยวกับการตรวจความผิดปกติของเซลล์ที่มิใช่ทางนรีเวช',lab_ncd_note = 'Pathology review, Non-GYN cytology' where lab_icd10tm = '0411400';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000080','','การทบทวนทางพยาธิวิทยาเกี่ยวกับการตรวจความผิดปกติของเซลล์ที่มิใช่ทางนรีเวช','Pathology review, Non-GYN cytology','1','0411400'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0411400');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจปริมาณ DNA ในเลือด',lab_ncd_note = 'DNA quantitation, whole blood' where lab_icd10tm = '0420201';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000081','','การตรวจปริมาณ DNA ในเลือด','DNA quantitation, whole blood','1','0420201'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0420201');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจปริมาณ DNA ในของเหลว อื่นๆ มิได้จำแนก',lab_ncd_note = 'DNA quantitation, other fluid NEC' where lab_icd10tm = '0420215';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000082','','การตรวจปริมาณ DNA ในของเหลว อื่นๆ มิได้จำแนก','DNA quantitation, other fluid NEC','1','0420215'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0420215');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจปริมาณ DNA ในไขกระดูก',lab_ncd_note = 'DNA quantitation, bone marrow' where lab_icd10tm = '0420227';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000083','','การตรวจปริมาณ DNA ในไขกระดูก','DNA quantitation, bone marrow','1','0420227'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0420227');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจปริมาณ DNA, ในเนื้อเยื่อ อื่นๆ มิได้จำแนก',lab_ncd_note = 'DNA quantitation, tissue NEC' where lab_icd10tm = '0420250';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000084','','การตรวจปริมาณ DNA, ในเนื้อเยื่อ อื่นๆ มิได้จำแนก','DNA quantitation, tissue NEC','1','0420250'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0420250');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจปริมาณ DNA ในสิ่งส่งตรวจ อื่นๆ มิได้จำแนก',lab_ncd_note = 'DNA quantitation, specimen type NEC' where lab_icd10tm = '0420290';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000085','','การตรวจปริมาณ DNA ในสิ่งส่งตรวจ อื่นๆ มิได้จำแนก','DNA quantitation, specimen type NEC','1','0420290'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0420290');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจปริมาณจีโนไทป์ จากสารละลายกรดนิวคลีอิก',lab_ncd_note = 'Genotyping - quantitative, nucleic acid solution' where lab_icd10tm = '0422281';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000086','','การตรวจปริมาณจีโนไทป์ จากสารละลายกรดนิวคลีอิก','Genotyping - quantitative, nucleic acid solution','1','0422281'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0422281');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจปริมาณจีโนไทป์ จากสารละลายโปรตีน',lab_ncd_note = 'Genotyping - quantitative, protein solution' where lab_icd10tm = '0422282';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000087','','การตรวจปริมาณจีโนไทป์ จากสารละลายโปรตีน','Genotyping - quantitative, protein solution','1','0422282'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0422282');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจปริมาณจีโนไทป์ จากสารละลายกรดนิวคลีอิก',lab_ncd_note = 'Genotyping - quantitative, nucleic acid solution' where lab_icd10tm = '0422481';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000088','','การตรวจปริมาณจีโนไทป์ จากสารละลายกรดนิวคลีอิก','Genotyping - quantitative, nucleic acid solution','1','0422481'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0422481');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจปริมาณจีโนไทป์ จากสารละลายโปรตีน',lab_ncd_note = 'Genotyping - quantitative, protein solution' where lab_icd10tm = '0422482';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000089','','การตรวจปริมาณจีโนไทป์ จากสารละลายโปรตีน','Genotyping - quantitative, protein solution','1','0422482'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0422482');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาคุณภาพยีน จากสารละลายกรดนิวคลีอิก',lab_ncd_note = 'Gene detection - qualitative, nucleic acid solution' where lab_icd10tm = '0423281';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000090','','การตรวจหาคุณภาพยีน จากสารละลายกรดนิวคลีอิก','Gene detection - qualitative, nucleic acid solution','1','0423281'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0423281');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาคุณภาพยีน จากสารละลายโปรตีน',lab_ncd_note = 'Gene detection - qualitative, protein solution' where lab_icd10tm = '0423282';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000091','','การตรวจหาคุณภาพยีน จากสารละลายโปรตีน','Gene detection - qualitative, protein solution','1','0423282'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0423282');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาปริมาณยีน จากสารละลายกรดนิวคลีอิก',lab_ncd_note = 'Gene detection - quantitative, nucleic acid solution' where lab_icd10tm = '0423481';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000092','','การตรวจหาปริมาณยีน จากสารละลายกรดนิวคลีอิก','Gene detection - quantitative, nucleic acid solution','1','0423481'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0423481');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาปริมาณยีน จากสารละลายโปรตีน',lab_ncd_note = 'Gene detection - quantitative, protein solution' where lab_icd10tm = '0423482';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000093','','การตรวจหาปริมาณยีน จากสารละลายโปรตีน','Gene detection - quantitative, protein solution','1','0423482'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0423482');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาคุณภาพยีน จากสารละลายกรดนิวคลีอิก',lab_ncd_note = 'Gene dose - qualitative, nucleic acid solution' where lab_icd10tm = '0424281';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000094','','การตรวจหาคุณภาพยีน จากสารละลายกรดนิวคลีอิก','Gene dose - qualitative, nucleic acid solution','1','0424281'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0424281');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาคุณภาพยีน จากสารละลายกรดนิวคลีอิก',lab_ncd_note = 'Gene dose - quantitative, nucleic acid solution' where lab_icd10tm = '0424481';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000095','','การตรวจหาคุณภาพยีน จากสารละลายกรดนิวคลีอิก','Gene dose - quantitative, nucleic acid solution','1','0424481'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0424481');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจการแสดงออกของยีน จากสารละลายกรดนิวคลีอิก',lab_ncd_note = 'Gene expression, nucleic acid solution' where lab_icd10tm = '0425281';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000096','','การตรวจการแสดงออกของยีน จากสารละลายกรดนิวคลีอิก','Gene expression, nucleic acid solution','1','0425281'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0425281');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจการแสดงออกของยีน จากสารละลายโปรตีน',lab_ncd_note = 'Gene expression, protein solution' where lab_icd10tm = '0425282';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000097','','การตรวจการแสดงออกของยีน จากสารละลายโปรตีน','Gene expression, protein solution','1','0425282'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0425282');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์หาตำแหน่งกลายพันธุ์ จากสารละลายกรดนิวคลีอิก',lab_ncd_note = 'Point mutation analysis, nucleic acid solution' where lab_icd10tm = '0426281';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000098','','การวิเคราะห์หาตำแหน่งกลายพันธุ์ จากสารละลายกรดนิวคลีอิก','Point mutation analysis, nucleic acid solution','1','0426281'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0426281');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์การกลายพันธุ์อื่น ๆ จากสารละลายกรดนิวคลีอิก',lab_ncd_note = 'Other mutation analysis, nucleic acid solution' where lab_icd10tm = '0426481';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000099','','การวิเคราะห์การกลายพันธุ์อื่น ๆ จากสารละลายกรดนิวคลีอิก','Other mutation analysis, nucleic acid solution','1','0426481'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0426481');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์ความเชื่อมโยงทางพันธุกรรม - ระดับ 1 จากสารละลายกรดนิวคลีอิก',lab_ncd_note = 'Linkage analysis - Level 1, nucleic acid solution' where lab_icd10tm = '0427281';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000100','','การวิเคราะห์ความเชื่อมโยงทางพันธุกรรม - ระดับ 1 จากสารละลายกรดนิวคลีอิก','Linkage analysis - Level 1, nucleic acid solution','1','0427281'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0427281');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์ความเชื่อมโยงทางพันธุกรรม - ระดับ 2 จากสารละลายกรดนิวคลีอิก',lab_ncd_note = 'Linkage analysis - Level 2, nucleic acid solution' where lab_icd10tm = '0427481';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000101','','การวิเคราะห์ความเชื่อมโยงทางพันธุกรรม - ระดับ 2 จากสารละลายกรดนิวคลีอิก','Linkage analysis - Level 2, nucleic acid solution','1','0427481'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0427481');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์ความเชื่อมโยงทางพันธุกรรม - ระดับ 3 จากสารละลายกรดนิวคลีอิก',lab_ncd_note = 'Linkage analysis - Level 3, nucleic acid solution' where lab_icd10tm = '0427681';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000102','','การวิเคราะห์ความเชื่อมโยงทางพันธุกรรม - ระดับ 3 จากสารละลายกรดนิวคลีอิก','Linkage analysis - Level 3, nucleic acid solution','1','0427681'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0427681');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์ความเชื่อมโยงทางพันธุกรรมอื่นๆ จากสารละลายกรดนิวคลีอิก',lab_ncd_note = 'Linkage analysis NOS, nucleic acid solution' where lab_icd10tm = '0427881';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000103','','การวิเคราะห์ความเชื่อมโยงทางพันธุกรรมอื่นๆ จากสารละลายกรดนิวคลีอิก','Linkage analysis NOS, nucleic acid solution','1','0427881'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0427881');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเตรียมสารละลายโปรตีน (ดิบหรือบริสุทธิ์) จากเลือด',lab_ncd_note = 'Preparation of protein solution (crude or pure), whole blood' where lab_icd10tm = '0428201';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000104','','การเตรียมสารละลายโปรตีน (ดิบหรือบริสุทธิ์) จากเลือด','Preparation of protein solution (crude or pure), whole blood','1','0428201'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0428201');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเตรียมสารละลายโปรตีน (ดิบหรือบริสุทธิ์) จากน้ำไขสันหลัง',lab_ncd_note = 'Preparation of protein solution (crude or pure), CSF' where lab_icd10tm = '0428204';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000105','','การเตรียมสารละลายโปรตีน (ดิบหรือบริสุทธิ์) จากน้ำไขสันหลัง','Preparation of protein solution (crude or pure), CSF','1','0428204'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0428204');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเตรียมสารละลายโปรตีน (ดิบหรือบริสุทธิ์) จากน้ำคร่ำ',lab_ncd_note = 'Preparation of protein solution (crude or pure), amniotic fluid' where lab_icd10tm = '0428205';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000106','','การเตรียมสารละลายโปรตีน (ดิบหรือบริสุทธิ์) จากน้ำคร่ำ','Preparation of protein solution (crude or pure), amniotic fluid','1','0428205'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0428205');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเตรียมสารละลายโปรตีน (ดิบหรือบริสุทธิ์)  จากของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Preparation of protein solution (crude or pure), other fluid NEC' where lab_icd10tm = '0428215';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000107','','การเตรียมสารละลายโปรตีน (ดิบหรือบริสุทธิ์)  จากของเหลวอื่นๆ มิได้จำแนก','Preparation of protein solution (crude or pure), other fluid NEC','1','0428215'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0428215');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเตรียมสารละลายโปรตีน (ดิบหรือบริสุทธิ์)  จากเซลล์เม็ดเลือดขาว',lab_ncd_note = 'Preparation of protein solution (crude or pure), WBC' where lab_icd10tm = '0428216';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000108','','การเตรียมสารละลายโปรตีน (ดิบหรือบริสุทธิ์)  จากเซลล์เม็ดเลือดขาว','Preparation of protein solution (crude or pure), WBC','1','0428216'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0428216');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเตรียมสารละลายโปรตีน (ดิบหรือบริสุทธิ์)  จากไขกระดูก ',lab_ncd_note = 'Preparation of protein solution (crude or pure), bone marrow' where lab_icd10tm = '0428227';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000109','','การเตรียมสารละลายโปรตีน (ดิบหรือบริสุทธิ์)  จากไขกระดูก ','Preparation of protein solution (crude or pure), bone marrow','1','0428227'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0428227');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเตรียมสารละลายโปรตีน (ดิบหรือบริสุทธิ์)  จากระบบหัวใจและหลอดเลือด',lab_ncd_note = 'Preparation of protein solution (crude or pure), CVS' where lab_icd10tm = '0428232';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000110','','การเตรียมสารละลายโปรตีน (ดิบหรือบริสุทธิ์)  จากระบบหัวใจและหลอดเลือด','Preparation of protein solution (crude or pure), CVS','1','0428232'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0428232');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเตรียมสารละลายโปรตีน (ดิบหรือบริสุทธิ์)  สด / เนื้อเยื่อแช่แข็งอื่นๆ มิได้จำแนก',lab_ncd_note = 'Preparation of protein solution (crude or pure), fresh/frozen tissue NEC' where lab_icd10tm = '0428250';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000111','','การเตรียมสารละลายโปรตีน (ดิบหรือบริสุทธิ์)  สด / เนื้อเยื่อแช่แข็งอื่นๆ มิได้จำแนก','Preparation of protein solution (crude or pure), fresh/frozen tissue NEC','1','0428250'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0428250');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเตรียมสารละลายโปรตีน (ดิบหรือบริสุทธิ์)  จากการแยก (จุลินทรีย์) ',lab_ncd_note = 'Preparation of protein solution (crude or pure), isolate (microbial)' where lab_icd10tm = '0428270';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000112','','การเตรียมสารละลายโปรตีน (ดิบหรือบริสุทธิ์)  จากการแยก (จุลินทรีย์) ','Preparation of protein solution (crude or pure), isolate (microbial)','1','0428270'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0428270');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเตรียมสารละลายโปรตีน (ดิบหรือบริสุทธิ์)  เนื้อเยื่อที่ผ่านการคงสภาพอื่นๆ มิได้จำแนก',lab_ncd_note = 'Preparation of protein solution (crude or pure), embedded/fixed tissue NEC' where lab_icd10tm = '0428274';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000113','','การเตรียมสารละลายโปรตีน (ดิบหรือบริสุทธิ์)  เนื้อเยื่อที่ผ่านการคงสภาพอื่นๆ มิได้จำแนก','Preparation of protein solution (crude or pure), embedded/fixed tissue NEC','1','0428274'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0428274');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเตรียมสารละลายโปรตีน (ดิบหรือบริสุทธิ์)  จากตัวอย่างสิ่งส่งตรวจประเภทอื่นๆ มิได้จำแนก',lab_ncd_note = 'Preparation of protein solution (crude or pure), specimen type NEC' where lab_icd10tm = '0428290';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000114','','การเตรียมสารละลายโปรตีน (ดิบหรือบริสุทธิ์)  จากตัวอย่างสิ่งส่งตรวจประเภทอื่นๆ มิได้จำแนก','Preparation of protein solution (crude or pure), specimen type NEC','1','0428290'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0428290');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเตรียมสารละลายโปรตีน (ดีเอ็นเอหรืออาร์เอ็นเอ)  จากเลือด ',lab_ncd_note = 'Preparation of protein solution (DNA or RNA), whole blood' where lab_icd10tm = '0428401';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000115','','การเตรียมสารละลายโปรตีน (ดีเอ็นเอหรืออาร์เอ็นเอ)  จากเลือด ','Preparation of protein solution (DNA or RNA), whole blood','1','0428401'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0428401');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเตรียมสารละลายโปรตีน (ดีเอ็นเอหรืออาร์เอ็นเอ)  จากน้ำไขสันหลัง ',lab_ncd_note = 'Preparation of protein solution ((DNA or RNA), CSF' where lab_icd10tm = '0428404';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000116','','การเตรียมสารละลายโปรตีน (ดีเอ็นเอหรืออาร์เอ็นเอ)  จากน้ำไขสันหลัง ','Preparation of protein solution ((DNA or RNA), CSF','1','0428404'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0428404');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเตรียมสารละลายโปรตีน (ดีเอ็นเอหรืออาร์เอ็นเอ)  จากน้ำคร่ำ ',lab_ncd_note = 'Preparation of protein solution ((DNA or RNA), amniotic fluid' where lab_icd10tm = '0428405';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000117','','การเตรียมสารละลายโปรตีน (ดีเอ็นเอหรืออาร์เอ็นเอ)  จากน้ำคร่ำ ','Preparation of protein solution ((DNA or RNA), amniotic fluid','1','0428405'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0428405');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเตรียมสารละลายโปรตีน (ดีเอ็นเอหรืออาร์เอ็นเอ) จากของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Preparation of protein solution ((DNA or RNA), other fluid NEC' where lab_icd10tm = '0428415';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000118','','การเตรียมสารละลายโปรตีน (ดีเอ็นเอหรืออาร์เอ็นเอ) จากของเหลวอื่นๆ มิได้จำแนก','Preparation of protein solution ((DNA or RNA), other fluid NEC','1','0428415'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0428415');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเตรียมสารละลายโปรตีน (ดีเอ็นเอหรืออาร์เอ็นเอ)  จากเม็ดเลือดขาว',lab_ncd_note = 'Preparation of protein solution ((DNA or RNA), WBC' where lab_icd10tm = '0428416';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000119','','การเตรียมสารละลายโปรตีน (ดีเอ็นเอหรืออาร์เอ็นเอ)  จากเม็ดเลือดขาว','Preparation of protein solution ((DNA or RNA), WBC','1','0428416'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0428416');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเตรียมสารละลายโปรตีน (ดีเอ็นเอหรืออาร์เอ็นเอ)  จากไขกระดูก ',lab_ncd_note = 'Preparation of protein solution ((DNA or RNA), bone marrow' where lab_icd10tm = '0428427';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000120','','การเตรียมสารละลายโปรตีน (ดีเอ็นเอหรืออาร์เอ็นเอ)  จากไขกระดูก ','Preparation of protein solution ((DNA or RNA), bone marrow','1','0428427'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0428427');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเตรียมสารละลายโปรตีน (ดีเอ็นเอหรืออาร์เอ็นเอ)  จากระบบหัวใจและหลอดเลือด',lab_ncd_note = 'Preparation of protein solution ((DNA or RNA), CVS' where lab_icd10tm = '0428432';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000121','','การเตรียมสารละลายโปรตีน (ดีเอ็นเอหรืออาร์เอ็นเอ)  จากระบบหัวใจและหลอดเลือด','Preparation of protein solution ((DNA or RNA), CVS','1','0428432'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0428432');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเตรียมสารละลายโปรตีน(ดีเอ็นเอหรืออาร์เอ็นเอ)เนื้อเยื่อสด/ แช่แข็งอื่นๆมิได้จำแนก',lab_ncd_note = 'Preparation of protein solution ((DNA or RNA), fresh/frozen tissue NEC' where lab_icd10tm = '0428450';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000122','','การเตรียมสารละลายโปรตีน(ดีเอ็นเอหรืออาร์เอ็นเอ)เนื้อเยื่อสด/ แช่แข็งอื่นๆมิได้จำแนก','Preparation of protein solution ((DNA or RNA), fresh/frozen tissue NEC','1','0428450'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0428450');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเตรียมสารละลายโปรตีน (ดีเอ็นเอหรืออาร์เอ็นเอ)จากการแยก(จุลินทรีย์) ',lab_ncd_note = 'Preparation of protein solution ((DNA or RNA), isolate (microbial)' where lab_icd10tm = '0428470';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000123','','การเตรียมสารละลายโปรตีน (ดีเอ็นเอหรืออาร์เอ็นเอ)จากการแยก(จุลินทรีย์) ','Preparation of protein solution ((DNA or RNA), isolate (microbial)','1','0428470'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0428470');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเตรียมสารละลายโปรตีน(ดีเอ็นเอหรืออาร์เอ็นเอ)เนื้อเยื่อที่ผ่านการคงสภาพอื่นๆ มิได้จำแนก',lab_ncd_note = 'Preparation of protein solution ((DNA or RNA), embedded/fixed tissue NEC' where lab_icd10tm = '0428474';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000124','','การเตรียมสารละลายโปรตีน(ดีเอ็นเอหรืออาร์เอ็นเอ)เนื้อเยื่อที่ผ่านการคงสภาพอื่นๆ มิได้จำแนก','Preparation of protein solution ((DNA or RNA), embedded/fixed tissue NEC','1','0428474'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0428474');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเตรียมสารละลายโปรตีน(ดีเอ็นเอหรืออาร์เอ็นเอ)จากตัวอย่างสิ่งส่งตรวจประเภทอื่นๆ มิได้จำแนก',lab_ncd_note = 'Preparation of protein solution ((DNA or RNA), specimen type NEC' where lab_icd10tm = '0428490';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000125','','การเตรียมสารละลายโปรตีน(ดีเอ็นเอหรืออาร์เอ็นเอ)จากตัวอย่างสิ่งส่งตรวจประเภทอื่นๆ มิได้จำแนก','Preparation of protein solution ((DNA or RNA), specimen type NEC','1','0428490'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0428490');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การคัดกรองกรดอะมิโน  การทดสอบหลายอย่าง จากเลือด ',lab_ncd_note = 'Amino acids screen, multiple tests, whole blood' where lab_icd10tm = '0430201';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000126','','การคัดกรองกรดอะมิโน  การทดสอบหลายอย่าง จากเลือด ','Amino acids screen, multiple tests, whole blood','1','0430201'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0430201');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การคัดกรองกรดอะมิโน  การทดสอบหลายอย่าง จากซีรั่ม / พลาสม่า ',lab_ncd_note = 'Amino acids screen, multiple tests, serum/plasma' where lab_icd10tm = '0430202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000127','','การคัดกรองกรดอะมิโน  การทดสอบหลายอย่าง จากซีรั่ม / พลาสม่า ','Amino acids screen, multiple tests, serum/plasma','1','0430202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0430202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การคัดกรองกรดอะมิโน  การทดสอบหลายอย่าง จากปัสสาวะ ',lab_ncd_note = 'Amino acids screen, multiple tests, urine' where lab_icd10tm = '0430203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000128','','การคัดกรองกรดอะมิโน  การทดสอบหลายอย่าง จากปัสสาวะ ','Amino acids screen, multiple tests, urine','1','0430203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0430203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การคัดกรองกรดอะมิโน  การทดสอบหลายอย่าง จากน้ำไขสันหลัง',lab_ncd_note = 'Amino acids screen, multiple tests, CSF' where lab_icd10tm = '0430204';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000129','','การคัดกรองกรดอะมิโน  การทดสอบหลายอย่าง จากน้ำไขสันหลัง','Amino acids screen, multiple tests, CSF','1','0430204'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0430204');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบหลายอย่างของกรดอะมิโน จากเลือด ',lab_ncd_note = 'Amino acids, multiple tests, whole blood ' where lab_icd10tm = '0430401';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000130','','การทดสอบหลายอย่างของกรดอะมิโน จากเลือด ','Amino acids, multiple tests, whole blood ','1','0430401'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0430401');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบหลายอย่างของกรดอะมิโน จากซีรั่ม / พลาสม่า ',lab_ncd_note = 'Amino acids, multiple tests, serum/plasma' where lab_icd10tm = '0430402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000131','','การทดสอบหลายอย่างของกรดอะมิโน จากซีรั่ม / พลาสม่า ','Amino acids, multiple tests, serum/plasma','1','0430402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0430402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบหลายอย่างของกรดอะมิโน  จากปัสสาวะ ',lab_ncd_note = 'Amino acids, multiple tests, urine' where lab_icd10tm = '0430403';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000132','','การทดสอบหลายอย่างของกรดอะมิโน  จากปัสสาวะ ','Amino acids, multiple tests, urine','1','0430403'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0430403');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบหลายอย่างของกรดอะมิโน  จากน้ำไขสันหลัง ',lab_ncd_note = 'Amino acids, multiple tests, CSF' where lab_icd10tm = '0430404';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000133','','การทดสอบหลายอย่างของกรดอะมิโน  จากน้ำไขสันหลัง ','Amino acids, multiple tests, CSF','1','0430404'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0430404');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การคัดกรองกรดอะมิโน  การทดสอบครั้งเดียว  จากเลือด',lab_ncd_note = 'Amino acids screen, single test, whole blood' where lab_icd10tm = '0431201';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000134','','การคัดกรองกรดอะมิโน  การทดสอบครั้งเดียว  จากเลือด','Amino acids screen, single test, whole blood','1','0431201'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0431201');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การคัดกรองกรดอะมิโน  การทดสอบครั้งเดียว  จากซีรั่ม / พลาสม่า  ',lab_ncd_note = 'Amino acids screen, single test, serum/plasma' where lab_icd10tm = '0431202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000135','','การคัดกรองกรดอะมิโน  การทดสอบครั้งเดียว  จากซีรั่ม / พลาสม่า  ','Amino acids screen, single test, serum/plasma','1','0431202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0431202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การคัดกรองกรดอะมิโน  การทดสอบครั้งเดียว  จากปัสสาวะ ',lab_ncd_note = 'Amino acids screen, single test, urine' where lab_icd10tm = '0431203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000136','','การคัดกรองกรดอะมิโน  การทดสอบครั้งเดียว  จากปัสสาวะ ','Amino acids screen, single test, urine','1','0431203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0431203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การคัดกรองกรดอะมิโน  การทดสอบครั้งเดียว  จากน้ำไขสันหลัง ',lab_ncd_note = 'Amino acids screen, single test,  CSF' where lab_icd10tm = '0431204';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000137','','การคัดกรองกรดอะมิโน  การทดสอบครั้งเดียว  จากน้ำไขสันหลัง ','Amino acids screen, single test,  CSF','1','0431204'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0431204');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบกรดอะมิโนครั้งเดียวในเลือด',lab_ncd_note = 'Amino acids single test, whole blood' where lab_icd10tm = '0431401';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000138','','การทดสอบกรดอะมิโนครั้งเดียวในเลือด','Amino acids single test, whole blood','1','0431401'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0431401');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบกรดอะมิโนครั้งเดียวในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Amino acids  single test, serum/plasma' where lab_icd10tm = '0431402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000139','','การทดสอบกรดอะมิโนครั้งเดียวในซีรั่ม / พลาสม่า ','Amino acids  single test, serum/plasma','1','0431402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0431402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบกรดอะมิโนครั้งเดียวในปัสสาวะ ',lab_ncd_note = 'Amino acids  single test, urine' where lab_icd10tm = '0431403';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000140','','การทดสอบกรดอะมิโนครั้งเดียวในปัสสาวะ ','Amino acids  single test, urine','1','0431403'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0431403');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบกรดอะมิโนครั้งเดียวในน้ำไขสันหลัง ',lab_ncd_note = 'Amino acids  single test,  CSF' where lab_icd10tm = '0431404';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000141','','การทดสอบกรดอะมิโนครั้งเดียวในน้ำไขสันหลัง ','Amino acids  single test,  CSF','1','0431404'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0431404');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจไกลโคเลตในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Glycolate, serum/plasma' where lab_icd10tm = '0434202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000142','','การตรวจไกลโคเลตในซีรั่ม / พลาสม่า ','Glycolate, serum/plasma','1','0434202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0434202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจไกลโคเลตในปัสสาวะ ',lab_ncd_note = 'Glycolate, urine' where lab_icd10tm = '0434203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000143','','การตรวจไกลโคเลตในปัสสาวะ ','Glycolate, urine','1','0434203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0434203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจฮีสตามีนในเลือด ',lab_ncd_note = 'Histamine, whole Blood' where lab_icd10tm = '0434401';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000144','','การตรวจฮีสตามีนในเลือด ','Histamine, whole Blood','1','0434401'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0434401');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจฮีสตามีนในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Histamine, serum/plasma' where lab_icd10tm = '0434402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000145','','การตรวจฮีสตามีนในซีรั่ม / พลาสม่า ','Histamine, serum/plasma','1','0434402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0434402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจฮีสตามีนในปัสสาวะ ',lab_ncd_note = 'Histamine, urine' where lab_icd10tm = '0434403';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000146','','การตรวจฮีสตามีนในปัสสาวะ ','Histamine, urine','1','0434403'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0434403');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจกรด homogentisic ในปัสสาวะ ',lab_ncd_note = 'Homogentisic acid, urine' where lab_icd10tm = '0434603';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000147','','การตรวจกรด homogentisic ในปัสสาวะ ','Homogentisic acid, urine','1','0434603'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0434603');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจเมลานินในปัสสาวะ ',lab_ncd_note = 'Melanin, urine' where lab_icd10tm = '0434803';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000148','','การตรวจเมลานินในปัสสาวะ ','Melanin, urine','1','0434803'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0434803');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การคัดกรอง methylmalonate ในปัสสาวะ ',lab_ncd_note = 'Methylmalonate screen, urine' where lab_icd10tm = '0435003';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000149','','การคัดกรอง methylmalonate ในปัสสาวะ ','Methylmalonate screen, urine','1','0435003'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0435003');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ methylmalonate ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Methylmalonate serum/plasma' where lab_icd10tm = '0435202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000150','','การตรวจ methylmalonate ในซีรั่ม / พลาสม่า','Methylmalonate serum/plasma','1','0435202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0435202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ methymalonate ในปัสสาวะ ',lab_ncd_note = 'Methymalonate, urine' where lab_icd10tm = '0435203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000151','','การตรวจ methymalonate ในปัสสาวะ ','Methymalonate, urine','1','0435203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0435203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจออกซาเลตในซีรั่ม/พลาสม่า  ',lab_ncd_note = 'Oxalate, serum/plasma' where lab_icd10tm = '0435402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000152','','การตรวจออกซาเลตในซีรั่ม/พลาสม่า  ','Oxalate, serum/plasma','1','0435402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0435402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจออกซาเลตในปัสสาวะ ',lab_ncd_note = 'Oxalate, urine' where lab_icd10tm = '0435403';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000153','','การตรวจออกซาเลตในปัสสาวะ ','Oxalate, urine','1','0435403'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0435403');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การคัดกรอง phenylpyruvate ในปัสสาวะ ',lab_ncd_note = 'Phenylpyruvate screen, urine' where lab_icd10tm = '0435603';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000154','','การคัดกรอง phenylpyruvate ในปัสสาวะ ','Phenylpyruvate screen, urine','1','0435603'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0435603');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ phenylpyruvate ในเลือด ',lab_ncd_note = 'Phenylpyruvate, whole blood' where lab_icd10tm = '0435801';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000155','','การตรวจ phenylpyruvate ในเลือด ','Phenylpyruvate, whole blood','1','0435801'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0435801');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ phenylpyruvate ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Phenylpyruvate, serum/plasma' where lab_icd10tm = '0435802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000156','','การตรวจ phenylpyruvate ในซีรั่ม / พลาสม่า ','Phenylpyruvate, serum/plasma','1','0435802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0435802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ phenylpyruvate ในปัสสาวะ  ',lab_ncd_note = 'Phenylpyruvate, urine' where lab_icd10tm = '0435803';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000157','','การตรวจ phenylpyruvate ในปัสสาวะ  ','Phenylpyruvate, urine','1','0435803'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0435803');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ thiocyanate ในเลือด ',lab_ncd_note = 'Thiocyanate, whole blood' where lab_icd10tm = '0436001';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000158','','การตรวจ thiocyanate ในเลือด ','Thiocyanate, whole blood','1','0436001'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0436001');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ thiocyanate ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Thiocyanate, serum/plasma' where lab_icd10tm = '0436002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000159','','การตรวจ thiocyanate ในซีรั่ม / พลาสม่า','Thiocyanate, serum/plasma','1','0436002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0436002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ thiocyanate ในปัสสาวะ ',lab_ncd_note = 'Thiocyanate, urine' where lab_icd10tm = '0436003';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000160','','การตรวจ thiocyanate ในปัสสาวะ ','Thiocyanate, urine','1','0436003'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0436003');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจแซนทีนในซีรั่ม/ พลาสม่า ',lab_ncd_note = 'Xanthine, serum/plasma' where lab_icd10tm = '0436202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000161','','การตรวจแซนทีนในซีรั่ม/ พลาสม่า ','Xanthine, serum/plasma','1','0436202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0436202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจแซนทีนในปัสสาวะ ',lab_ncd_note = 'Xanthine, urine' where lab_icd10tm = '0436203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000162','','การตรวจแซนทีนในปัสสาวะ ','Xanthine, urine','1','0436203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0436203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจอัลบูมินในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Albumin, serum' where lab_icd10tm = '0440202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000163','','การตรวจอัลบูมินในซีรั่ม / พลาสม่า ','Albumin, serum','1','0440202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0440202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจอัลบูมินในปัสสาวะ / ตรวจโปรตีน macroalbumin ในปัสสาวะ (ใน filed ผลการตรวจใส่ค่า 0=negative, 1=trace, 2=positive)',lab_ncd_note = 'Albumin, urine' where lab_icd10tm = '0440203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000164','','การตรวจอัลบูมินในปัสสาวะ / ตรวจโปรตีน macroalbumin ในปัสสาวะ (ใน filed ผลการตรวจใส่ค่า 0=negative, 1=trace, 2=positive)','Albumin, urine','1','0440203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0440203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจโปรตีน microalbumin ในปัสสาวะ (ใน filed ผลการตรวจใส่ค่า 0=negative, 1=trace, 2=positive)',lab_ncd_note = 'Microalbumin protein' where lab_icd10tm = '0440204';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000165','','การตรวจโปรตีน microalbumin ในปัสสาวะ (ใน filed ผลการตรวจใส่ค่า 0=negative, 1=trace, 2=positive)','Microalbumin protein','1','0440204'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0440204');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ UPCR (Urine protein creatinine ratio)',lab_ncd_note = 'UPCR (Urine protein creatinine ratio)' where lab_icd10tm = '0440205';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000166','','การตรวจ UPCR (Urine protein creatinine ratio)','UPCR (Urine protein creatinine ratio)','1','0440205'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0440205');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ Urine albumin creatinine ratio',lab_ncd_note = 'Urine albumin creatinine ratio' where lab_icd10tm = '0440206';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000167','','การตรวจ Urine albumin creatinine ratio','Urine albumin creatinine ratio','1','0440206'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0440206');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจอัลบูมินในของเหลวอื่น ๆ ที่มิได้จำแนก',lab_ncd_note = 'Albumin, other fluid NEC' where lab_icd10tm = '0440215';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000168','','การตรวจอัลบูมินในของเหลวอื่น ๆ ที่มิได้จำแนก','Albumin, other fluid NEC','1','0440215'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0440215');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจแอลฟาฟีโตโปรตีน ในเซรั่ม / พลาสม่า ',lab_ncd_note = 'Alpha-fetoprotein, serum/plasma' where lab_icd10tm = '0441202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000169','','การตรวจแอลฟาฟีโตโปรตีน ในเซรั่ม / พลาสม่า ','Alpha-fetoprotein, serum/plasma','1','0441202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0441202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจแอลฟาฟีโตโปรตีน ในน้ำคร่ำ ',lab_ncd_note = 'Alpha-fetoprotein, amniotic fluid' where lab_icd10tm = '0441205';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000170','','การตรวจแอลฟาฟีโตโปรตีน ในน้ำคร่ำ ','Alpha-fetoprotein, amniotic fluid','1','0441205'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0441205');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจแอลฟาฟีโตโปรตีน ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Alhpa-fetoprotein, other fluid NEC' where lab_icd10tm = '0441215';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000171','','การตรวจแอลฟาฟีโตโปรตีน ในของเหลวอื่นๆ มิได้จำแนก','Alhpa-fetoprotein, other fluid NEC','1','0441215'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0441215');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจเบต้า-2-ไมโครโกลบูลิน ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Beta-2-microglobulin, serum/plasma' where lab_icd10tm = '0441402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000172','','การตรวจเบต้า-2-ไมโครโกลบูลิน ในซีรั่ม / พลาสม่า ','Beta-2-microglobulin, serum/plasma','1','0441402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0441402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจเบต้า-2-ไมโครโกลบูลินในปัสสาวะ ',lab_ncd_note = 'Beta-2-microglobulin, urine' where lab_icd10tm = '0441403';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000173','','การตรวจเบต้า-2-ไมโครโกลบูลินในปัสสาวะ ','Beta-2-microglobulin, urine','1','0441403'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0441403');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจเบต้า-2-ไมโครโกลบูลินในของเหลวอื่นๆมิได้จำแนก',lab_ncd_note = 'Beta-2-microglobulin, other fluid NEC' where lab_icd10tm = '0441415';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000174','','การตรวจเบต้า-2-ไมโครโกลบูลินในของเหลวอื่นๆมิได้จำแนก','Beta-2-microglobulin, other fluid NEC','1','0441415'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0441415');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจเซรูโลแพล็ซมิน ในซีรั่ม/พลาสม่า ',lab_ncd_note = 'Ceruloplasmin, serum' where lab_icd10tm = '0441602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000175','','การตรวจเซรูโลแพล็ซมิน ในซีรั่ม/พลาสม่า ','Ceruloplasmin, serum','1','0441602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0441602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ C-reactive proteinในซีรั่ม/ พลาสม่า ',lab_ncd_note = 'C-reactive protein, serum' where lab_icd10tm = '0441802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000176','','การตรวจ C-reactive proteinในซีรั่ม/ พลาสม่า ','C-reactive protein, serum','1','0441802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0441802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจเฟอร์ริทิน  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Ferritin, serum/plasma' where lab_icd10tm = '0442002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000177','','การตรวจเฟอร์ริทิน  ในซีรั่ม / พลาสม่า','Ferritin, serum/plasma','1','0442002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0442002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจเฟอร์ริทิน  ในของเหลวอื่น ๆ มิได้จำแนก',lab_ncd_note = 'Ferritin, other fluid NEC' where lab_icd10tm = '0442015';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000178','','การตรวจเฟอร์ริทิน  ในของเหลวอื่น ๆ มิได้จำแนก','Ferritin, other fluid NEC','1','0442015'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0442015');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ haptoglobin ในซีรั่ม/พลาสม่า ',lab_ncd_note = 'Haptoglobin, serum/plasma' where lab_icd10tm = '0442202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000179','','การตรวจ haptoglobin ในซีรั่ม/พลาสม่า ','Haptoglobin, serum/plasma','1','0442202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0442202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ hemopexin ในซีรั่ม/พลาสม่า ',lab_ncd_note = 'Hemopexin, serum/plasma' where lab_icd10tm = '0442402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000180','','การตรวจ hemopexin ในซีรั่ม/พลาสม่า ','Hemopexin, serum/plasma','1','0442402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0442402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ hemopexinในปัสสาวะ ',lab_ncd_note = 'Hemopexin, urine' where lab_icd10tm = '0442403';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000181','','การตรวจ hemopexinในปัสสาวะ ','Hemopexin, urine','1','0442403'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0442403');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ lactoferrin ในซีรั่ม/พลาสม่า ',lab_ncd_note = 'Lactoferrin, serum/plasma' where lab_icd10tm = '0442602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000182','','การตรวจ lactoferrin ในซีรั่ม/พลาสม่า ','Lactoferrin, serum/plasma','1','0442602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0442602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจไลโซไซม์  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Lysozyme, serum/plasma' where lab_icd10tm = '0442802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000183','','การตรวจไลโซไซม์  ในซีรั่ม / พลาสม่า','Lysozyme, serum/plasma','1','0442802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0442802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจไลโซไซม์  ในปัสสาวะ ',lab_ncd_note = 'Lysozyme, urine' where lab_icd10tm = '0442803';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000184','','การตรวจไลโซไซม์  ในปัสสาวะ ','Lysozyme, urine','1','0442803'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0442803');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ myelin basic protein  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Myelin basic protein, serum/plasma' where lab_icd10tm = '0443002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000185','','การตรวจ myelin basic protein  ในซีรั่ม / พลาสม่า ','Myelin basic protein, serum/plasma','1','0443002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0443002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ myelin basic protein  ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Myelin basic protein, other fluid NEC' where lab_icd10tm = '0443015';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000186','','การตรวจ myelin basic protein  ในของเหลวอื่นๆ มิได้จำแนก','Myelin basic protein, other fluid NEC','1','0443015'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0443015');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ osteocalcin  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Osteocalcin, serum/plasma' where lab_icd10tm = '0443202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000187','','การตรวจ osteocalcin  ในซีรั่ม / พลาสม่า ','Osteocalcin, serum/plasma','1','0443202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0443202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจพรีอัลบูมิน  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Prealbumin, serum/plasma' where lab_icd10tm = '0443402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000188','','การตรวจพรีอัลบูมิน  ในซีรั่ม / พลาสม่า ','Prealbumin, serum/plasma','1','0443402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0443402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจโปรตีนทั้งหมด  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Protein, total, serum/plasma' where lab_icd10tm = '0443602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000189','','การตรวจโปรตีนทั้งหมด  ในซีรั่ม / พลาสม่า ','Protein, total, serum/plasma','1','0443602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0443602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจโปรตีนทั้งหมดในปัสสาวะ ',lab_ncd_note = 'Protein, total, urine' where lab_icd10tm = '0443603';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000190','','การตรวจโปรตีนทั้งหมดในปัสสาวะ ','Protein, total, urine','1','0443603'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0443603');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจโปรตีนทั้งหมดในของเหลวอื่นๆ มิได้ระบุ',lab_ncd_note = 'Protein, total, other fluid NEC' where lab_icd10tm = '0443615';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000191','','การตรวจโปรตีนทั้งหมดในของเหลวอื่นๆ มิได้ระบุ','Protein, total, other fluid NEC','1','0443615'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0443615');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจโปรตีนอิเลคโทรโฟริซิส  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Protein electrophoresis, serum/plasma' where lab_icd10tm = '0443802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000192','','การตรวจโปรตีนอิเลคโทรโฟริซิส  ในซีรั่ม / พลาสม่า ','Protein electrophoresis, serum/plasma','1','0443802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0443802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจโปรตีนอิเลคโทรโฟริซิส  ในปัสสาวะ ',lab_ncd_note = 'Protein electrophoresis, urine' where lab_icd10tm = '0443803';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000193','','การตรวจโปรตีนอิเลคโทรโฟริซิส  ในปัสสาวะ ','Protein electrophoresis, urine','1','0443803'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0443803');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจโปรตีนอิเลคโทรโฟริซิส  ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Protein electrophoresis, other fluid NEC' where lab_icd10tm = '0443815';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000194','','การตรวจโปรตีนอิเลคโทรโฟริซิส  ในของเหลวอื่นๆ มิได้จำแนก','Protein electrophoresis, other fluid NEC','1','0443815'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0443815');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ protein immuno-fixation/immuno-electrophoresis  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Protein immuno-fixation/immuno-electrophoresis, serum/plasma' where lab_icd10tm = '0444002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000195','','การตรวจ protein immuno-fixation/immuno-electrophoresis  ในซีรั่ม / พลาสม่า ','Protein immuno-fixation/immuno-electrophoresis, serum/plasma','1','0444002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0444002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ protein immuno-fixation/immuno-electrophoresis  ในปัสสาวะ ',lab_ncd_note = 'Protein immuno-fixation/immuno-electrophoresis, urine' where lab_icd10tm = '0444003';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000196','','การตรวจ protein immuno-fixation/immuno-electrophoresis  ในปัสสาวะ ','Protein immuno-fixation/immuno-electrophoresis, urine','1','0444003'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0444003');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ protein immuno-fixation/immuno-electrophoresis  ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Protein immuno-fixation/immuno-electrophoresis, other fluid NEC' where lab_icd10tm = '0444015';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000197','','การตรวจ protein immuno-fixation/immuno-electrophoresis  ในของเหลวอื่นๆ มิได้จำแนก','Protein immuno-fixation/immuno-electrophoresis, other fluid NEC','1','0444015'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0444015');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจเร็ตติคูลินในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Reticulin, serum/plasma' where lab_icd10tm = '0444202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000198','','การตรวจเร็ตติคูลินในซีรั่ม / พลาสม่า ','Reticulin, serum/plasma','1','0444202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0444202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ retinol binding protein  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Retinol binding protein, serum/plasma' where lab_icd10tm = '0444402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000199','','การตรวจ retinol binding protein  ในซีรั่ม / พลาสม่า ','Retinol binding protein, serum/plasma','1','0444402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0444402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ transcobalamin  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Transcobalamin, serum/plasma' where lab_icd10tm = '0444602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000200','','การตรวจ transcobalamin  ในซีรั่ม / พลาสม่า','Transcobalamin, serum/plasma','1','0444602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0444602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ transferrin  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Transferrin, serum/plasma' where lab_icd10tm = '0444802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000201','','การตรวจ transferrin  ในซีรั่ม / พลาสม่า','Transferrin, serum/plasma','1','0444802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0444802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์ complement C1  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Complement C1 assay, serum/plasma' where lab_icd10tm = '0450202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000202','','การวิเคราะห์ complement C1  ในซีรั่ม / พลาสม่า','Complement C1 assay, serum/plasma','1','0450202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0450202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์ complement C2  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Complement C2 assay, serum/plasma' where lab_icd10tm = '0450202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000202','','การวิเคราะห์ complement C2  ในซีรั่ม / พลาสม่า ','Complement C2 assay, serum/plasma','1','0450202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0450202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์ complement C3  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Complement C3 assay, serum/plasma' where lab_icd10tm = '0450602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000204','','การวิเคราะห์ complement C3  ในซีรั่ม / พลาสม่า ','Complement C3 assay, serum/plasma','1','0450602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0450602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์ complement C3  ในของเหลวอื่น ๆ มิได้จำแนก',lab_ncd_note = 'Complement C3 assay, other fluid NEC' where lab_icd10tm = '0450615';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000205','','การวิเคราะห์ complement C3  ในของเหลวอื่น ๆ มิได้จำแนก','Complement C3 assay, other fluid NEC','1','0450615'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0450615');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์ complement C4  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Complement C4 assay, serum/plasma' where lab_icd10tm = '0450802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000206','','การวิเคราะห์ complement C4  ในซีรั่ม / พลาสม่า ','Complement C4 assay, serum/plasma','1','0450802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0450802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์ complement CH50  ในซีรั่ม /พลาสม่า',lab_ncd_note = 'Complement CH50 assay, serum/plasma' where lab_icd10tm = '0452002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000207','','การวิเคราะห์ complement CH50  ในซีรั่ม /พลาสม่า','Complement CH50 assay, serum/plasma','1','0452002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0452002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์การแตกตัวของเม็ดเลือดแดง ของ complement  ใน ซีรั่ม / พลาสม่า (รวมถึง CH 100)',lab_ncd_note = 'Complement total hemolytic assay, serum/plasma (Inc. CH100)' where lab_icd10tm = '0452202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000208','','การวิเคราะห์การแตกตัวของเม็ดเลือดแดง ของ complement  ใน ซีรั่ม / พลาสม่า (รวมถึง CH 100)','Complement total hemolytic assay, serum/plasma (Inc. CH100)','1','0452202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0452202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวัดปฏิกิริยา complement C3 proactivator  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Complement C3 proactivator assay, serum/plasma' where lab_icd10tm = '0452802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000209','','การวัดปฏิกิริยา complement C3 proactivator  ในซีรั่ม / พลาสม่า','Complement C3 proactivator assay, serum/plasma','1','0452802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0452802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวัดปฏิกิริยา complement C3 proactivator  ในของเหลวอื่น ๆ ที่มิได้จำแนก',lab_ncd_note = 'Complement C3 proactivator assay, other fluid NEC' where lab_icd10tm = '0452815';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000210','','การวัดปฏิกิริยา complement C3 proactivator  ในของเหลวอื่น ๆ ที่มิได้จำแนก','Complement C3 proactivator assay, other fluid NEC','1','0452815'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0452815');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวัดปฏิกิริยา complement C1 esterese  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Complement C1 esterase assay, serum/plasma' where lab_icd10tm = '0453202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000211','','การวัดปฏิกิริยา complement C1 esterese  ในซีรั่ม / พลาสม่า ','Complement C1 esterase assay, serum/plasma','1','0453202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0453202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวัดปฏิกิริยา complement C1  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Complement C1q assay, serum/plasma' where lab_icd10tm = '0453402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000212','','การวัดปฏิกิริยา complement C1  ในซีรั่ม / พลาสม่า ','Complement C1q assay, serum/plasma','1','0453402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0453402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวัดปฏิกิริยา complement C1r  ใน ซีรั่ม / พลาสม่า ',lab_ncd_note = 'Complement C1r assay, serum/plasma' where lab_icd10tm = '0453602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000213','','การวัดปฏิกิริยา complement C1r  ใน ซีรั่ม / พลาสม่า ','Complement C1r assay, serum/plasma','1','0453602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0453602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวัดปฏิกิริยา complement C1s  ใน ซีรั่ม / พลาสม่า ',lab_ncd_note = 'Complement C1s assay, serum/plasma' where lab_icd10tm = '0453802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000214','','การวัดปฏิกิริยา complement C1s  ใน ซีรั่ม / พลาสม่า ','Complement C1s assay, serum/plasma','1','0453802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0453802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวัดปฏิกิริยา พันธะ complement C1q ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Complement C1q binding assay, serum/plasma' where lab_icd10tm = '0454202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000215','','การวัดปฏิกิริยา พันธะ complement C1q ในซีรั่ม / พลาสม่า ','Complement C1q binding assay, serum/plasma','1','0454202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0454202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวัดปฏิกิริยา complement binding protein Cm ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Complement binding protein assay Cm, serum/plasma' where lab_icd10tm = '0454802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000216','','การวัดปฏิกิริยา complement binding protein Cm ในซีรั่ม / พลาสม่า ','Complement binding protein assay Cm, serum/plasma','1','0454802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0454802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวัดปฏิกิริยา complement C1 esterase inhibitor ใน ซีรั่ม / พลาสม่า ',lab_ncd_note = 'Complement C1 esterase inhibitor assay, serum/plasma' where lab_icd10tm = '0455202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000217','','การวัดปฏิกิริยา complement C1 esterase inhibitor ใน ซีรั่ม / พลาสม่า ','Complement C1 esterase inhibitor assay, serum/plasma','1','0455202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0455202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวัดปฏิกิริยา complement ที่มิได้จำแนก ใน ซีรั่ม / พลาสม่า',lab_ncd_note = 'Complement assay NEC, serum/plasma' where lab_icd10tm = '0459002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000218','','การวัดปฏิกิริยา complement ที่มิได้จำแนก ใน ซีรั่ม / พลาสม่า','Complement assay NEC, serum/plasma','1','0459002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0459002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวัดปฏิกิริยา complement ที่มิได้จำแนก ในของเหลวอื่นๆ ที่มิได้จำแนก',lab_ncd_note = 'Complement assay NEC, other fluid NEC' where lab_icd10tm = '0459015';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000219','','การวัดปฏิกิริยา complement ที่มิได้จำแนก ในของเหลวอื่นๆ ที่มิได้จำแนก','Complement assay NEC, other fluid NEC','1','0459015'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0459015');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ อิมมูโนโกลบูลิน เอ  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Immunoglobulin A, serum/plasma' where lab_icd10tm = '0460202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000220','','การตรวจ อิมมูโนโกลบูลิน เอ  ในซีรั่ม / พลาสม่า','Immunoglobulin A, serum/plasma','1','0460202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0460202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ อิมมูโนโกลบูลิน เอ  ในปัสสาวะ ',lab_ncd_note = 'Immunoglobulin A, urine' where lab_icd10tm = '0460203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000221','','การตรวจ อิมมูโนโกลบูลิน เอ  ในปัสสาวะ ','Immunoglobulin A, urine','1','0460203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0460203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ อิมมูโนโกลบูลิน เอ  ในของเหลวอื่นๆ มิได้จำแนก ',lab_ncd_note = 'Immunoglobulin A, other fluid NEC' where lab_icd10tm = '0460215';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000222','','การตรวจ อิมมูโนโกลบูลิน เอ  ในของเหลวอื่นๆ มิได้จำแนก ','Immunoglobulin A, other fluid NEC','1','0460215'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0460215');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ อิมมูโนโกลบูลิน จี  ในเซรั่ม / พลาสม่า ',lab_ncd_note = 'Immunoglobulin G, serum/plasma' where lab_icd10tm = '0460402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000223','','การตรวจ อิมมูโนโกลบูลิน จี  ในเซรั่ม / พลาสม่า ','Immunoglobulin G, serum/plasma','1','0460402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0460402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ อิมมูโนโกลบูลิน จี  ในปัสสาวะ ',lab_ncd_note = 'Immunoglobulin G, urine' where lab_icd10tm = '0460403';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000224','','การตรวจ อิมมูโนโกลบูลิน จี  ในปัสสาวะ ','Immunoglobulin G, urine','1','0460403'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0460403');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ อิมมูโนโกลบูลิน จี  ในของเหลวอื่นๆ มิได้ระบุ',lab_ncd_note = 'Immunoglobulin G, other fluid NEC' where lab_icd10tm = '0460415';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000225','','การตรวจ อิมมูโนโกลบูลิน จี  ในของเหลวอื่นๆ มิได้ระบุ','Immunoglobulin G, other fluid NEC','1','0460415'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0460415');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ อิมมูโนโกลบูลิน เอ็ม ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Immunoglobulin M, serum/plasma' where lab_icd10tm = '0460602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000226','','การตรวจ อิมมูโนโกลบูลิน เอ็ม ในซีรั่ม / พลาสม่า ','Immunoglobulin M, serum/plasma','1','0460602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0460602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ อิมมูโนโกลบูลิน เอ็ม  ในปัสสาวะ ',lab_ncd_note = 'Immunoglobulin M, urine' where lab_icd10tm = '0460603';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000227','','การตรวจ อิมมูโนโกลบูลิน เอ็ม  ในปัสสาวะ ','Immunoglobulin M, urine','1','0460603'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0460603');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ อิมมูโนโกลบูลิน เอ็ม  ในของเหลวอื่นๆ มิได้ระบุ',lab_ncd_note = 'Immunoglobulin M, other fluid NEC' where lab_icd10tm = '0460615';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000228','','การตรวจ อิมมูโนโกลบูลิน เอ็ม  ในของเหลวอื่นๆ มิได้ระบุ','Immunoglobulin M, other fluid NEC','1','0460615'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0460615');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ อิมมูโนโกลบูลิน ดี  ในเซรั่ม / พลาสม่า',lab_ncd_note = 'Immunoglobulin  D, serum/plasma' where lab_icd10tm = '0460802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000229','','การตรวจ อิมมูโนโกลบูลิน ดี  ในเซรั่ม / พลาสม่า','Immunoglobulin  D, serum/plasma','1','0460802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0460802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ อิมมูโนโกลบูลิน อี  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Immunoglobulin E, serum/plasma' where lab_icd10tm = '0461002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000230','','การตรวจ อิมมูโนโกลบูลิน อี  ในซีรั่ม / พลาสม่า ','Immunoglobulin E, serum/plasma','1','0461002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0461002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ อิมมูโนโกลบูลิน อี  ในของเหลวอื่นๆ มิได้ระบุ',lab_ncd_note = 'Immunoglobulin E, other fluid NEC' where lab_icd10tm = '0461015';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000231','','การตรวจ อิมมูโนโกลบูลิน อี  ในของเหลวอื่นๆ มิได้ระบุ','Immunoglobulin E, other fluid NEC','1','0461015'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0461015');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจกลุ่มย่อยของอิมมูโนโกลบูลิน จี  ในเซรั่ม / พลาสม่า ',lab_ncd_note = 'Immunoglobulin G subclass(es), serum/plasma' where lab_icd10tm = '0462002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000232','','การตรวจกลุ่มย่อยของอิมมูโนโกลบูลิน จี  ในเซรั่ม / พลาสม่า ','Immunoglobulin G subclass(es), serum/plasma','1','0462002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0462002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'Immunoglobulin G1 subclass(es), serum/plasma' where lab_icd10tm = '0462003';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000233','','-','Immunoglobulin G1 subclass(es), serum/plasma','1','0462003'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0462003');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'Immunoglobulin G2 subclass(es), serum/plasma' where lab_icd10tm = '0462004';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000234','','-','Immunoglobulin G2 subclass(es), serum/plasma','1','0462004'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0462004');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'Immunoglobulin G3 subclass(es), serum/plasma' where lab_icd10tm = '0462005';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000235','','-','Immunoglobulin G3 subclass(es), serum/plasma','1','0462005'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0462005');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'Immunoglobulin G4 subclass(es), serum/plasma' where lab_icd10tm = '0462006';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000236','','-','Immunoglobulin G4 subclass(es), serum/plasma','1','0462006'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0462006');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'Free light chain kappa, serum ' where lab_icd10tm = '0462016';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000237','','-','Free light chain kappa, serum ','1','0462016'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0462016');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'Free light chain kappa, urine' where lab_icd10tm = '0462017';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000238','','-','Free light chain kappa, urine','1','0462017'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0462017');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'Free light chain lambda, serum' where lab_icd10tm = '0462018';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000239','','-','Free light chain lambda, serum','1','0462018'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0462018');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'Free light chain lambda, urine' where lab_icd10tm = '0462019';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000240','','-','Free light chain lambda, urine','1','0462019'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0462019');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'Capillary immunotyping electrophoresis, serum' where lab_icd10tm = '0462020';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000241','','-','Capillary immunotyping electrophoresis, serum','1','0462020'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0462020');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'Immonofixation, serum' where lab_icd10tm = '0462021';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000242','','-','Immonofixation, serum','1','0462021'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0462021');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'Immonofixation, urine' where lab_icd10tm = '0462022';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000243','','-','Immonofixation, urine','1','0462022'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0462022');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจอิมมูโนโกลบูลินอื่น ๆ มิได้จำแนก ในสิ่งส่งตรวจชนิดต่างๆ ',lab_ncd_note = 'Immunoglobulin NEC, any specimen type' where lab_icd10tm = '0469099';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000244','','การตรวจอิมมูโนโกลบูลินอื่น ๆ มิได้จำแนก ในสิ่งส่งตรวจชนิดต่างๆ ','Immunoglobulin NEC, any specimen type','1','0469099'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0469099');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ 2,3-diphosphoglycerate ใน ซีรั่ม/พลาสม่า ',lab_ncd_note = '2,3-Diphosphoglycerate, serum/plasma' where lab_icd10tm = '0470202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000245','','การตรวจ 2,3-diphosphoglycerate ใน ซีรั่ม/พลาสม่า ','2,3-Diphosphoglycerate, serum/plasma','1','0470202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0470202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ 2,3-diphosphoglycerate ในเซลล์เม็ดเลือดแดง',lab_ncd_note = '2,3-Diphosphoglycerate, RBC' where lab_icd10tm = '0470217';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000246','','การตรวจ 2,3-diphosphoglycerate ในเซลล์เม็ดเลือดแดง','2,3-Diphosphoglycerate, RBC','1','0470217'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0470217');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ galactokinase ในเลือด ',lab_ncd_note = 'Galactokinase, whole blood' where lab_icd10tm = '0470401';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000247','','การตรวจ galactokinase ในเลือด ','Galactokinase, whole blood','1','0470401'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0470401');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ galactokinase ในเม็ดเลือดแดง',lab_ncd_note = 'Galactokinase, RBC' where lab_icd10tm = '0470417';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000248','','การตรวจ galactokinase ในเม็ดเลือดแดง','Galactokinase, RBC','1','0470417'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0470417');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ galactose-1-phosphate uridyltransferase ในเลือด ',lab_ncd_note = 'Galactose-1-phosphate uridyltransferase, whole blood' where lab_icd10tm = '0470601';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000249','','การตรวจ galactose-1-phosphate uridyltransferase ในเลือด ','Galactose-1-phosphate uridyltransferase, whole blood','1','0470601'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0470601');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ galactose-1-phosphate uridyltransferase ในเม็ดเลือดแดง ',lab_ncd_note = 'Galactose-1-phosphate uridyltransferase, RBC' where lab_icd10tm = '0470617';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000250','','การตรวจ galactose-1-phosphate uridyltransferase ในเม็ดเลือดแดง ','Galactose-1-phosphate uridyltransferase, RBC','1','0470617'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0470617');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การคัดกรอง galactose-6-phosphate dehydrogenase ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Gluclose-6-phosphate dehydrogenase screen, serum/plasma ' where lab_icd10tm = '0470802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000251','','การคัดกรอง galactose-6-phosphate dehydrogenase ในซีรั่ม / พลาสม่า ','Gluclose-6-phosphate dehydrogenase screen, serum/plasma ','1','0470802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0470802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การคัดกรอง galactose-6-phosphate dehydrogenase ในเม็ดเซลล์เลือดแดง',lab_ncd_note = 'Gluclose-6-phosphate dehydrogenase screen, RBC' where lab_icd10tm = '0470817';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000252','','การคัดกรอง galactose-6-phosphate dehydrogenase ในเม็ดเซลล์เลือดแดง','Gluclose-6-phosphate dehydrogenase screen, RBC','1','0470817'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0470817');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ galactose-6-phosphate dehydrogenase ในซีรั่ม/พลาสม่า ',lab_ncd_note = 'Gluclose-6-phosphate dehydrogenase, serum/plasma' where lab_icd10tm = '0471002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000253','','การตรวจ galactose-6-phosphate dehydrogenase ในซีรั่ม/พลาสม่า ','Gluclose-6-phosphate dehydrogenase, serum/plasma','1','0471002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0471002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ galactose-6-phosphate dehydrogenase ในเซลล์เม็ดเลือดแดง',lab_ncd_note = 'Gluclose-6-phosphate dehydrogenase, RBC' where lab_icd10tm = '0471017';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000254','','การตรวจ galactose-6-phosphate dehydrogenase ในเซลล์เม็ดเลือดแดง','Gluclose-6-phosphate dehydrogenase, RBC','1','0471017'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0471017');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ กลูตาไธโอน ในเซลล์เม็ดเลือดแดง',lab_ncd_note = 'Glutathione, RBC' where lab_icd10tm = '0471217';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000255','','การตรวจ กลูตาไธโอน ในเซลล์เม็ดเลือดแดง','Glutathione, RBC','1','0471217'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0471217');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ hexokinase  ใน ซีรั่ม / พลาสม่า ',lab_ncd_note = 'Hexokinase, serum/plasma' where lab_icd10tm = '0471402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000256','','การตรวจ hexokinase  ใน ซีรั่ม / พลาสม่า ','Hexokinase, serum/plasma','1','0471402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0471402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ hexokinase ใน เซลล์เม็ดเลือดแดง',lab_ncd_note = 'Hexokinase, RBC' where lab_icd10tm = '0471417';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000257','','การตรวจ hexokinase ใน เซลล์เม็ดเลือดแดง','Hexokinase, RBC','1','0471417'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0471417');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ methemoglobin reductase  ในเซลล์เม็ดเลือดแดง',lab_ncd_note = 'Methemoglobin reductase, RBC' where lab_icd10tm = '0471617';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000258','','การตรวจ methemoglobin reductase  ในเซลล์เม็ดเลือดแดง','Methemoglobin reductase, RBC','1','0471617'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0471617');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ NADH dehydrogenase  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'NADH dehydrogenase, serum/plasma' where lab_icd10tm = '0471802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000259','','การตรวจ NADH dehydrogenase  ในซีรั่ม / พลาสม่า ','NADH dehydrogenase, serum/plasma','1','0471802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0471802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ NADH dehydrogenase  ในเซลล์เม็ดเลือดแดง',lab_ncd_note = 'NADH dehydrogenase, RBC' where lab_icd10tm = '0471817';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000260','','การตรวจ NADH dehydrogenase  ในเซลล์เม็ดเลือดแดง','NADH dehydrogenase, RBC','1','0471817'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0471817');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ phosphofructokinase  ในเลือด',lab_ncd_note = 'Phosphofructokinase, whole blood' where lab_icd10tm = '0472001';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000261','','การตรวจ phosphofructokinase  ในเลือด','Phosphofructokinase, whole blood','1','0472001'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0472001');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ phosphofructokinase  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Phosphofructokinase, serum/plasma' where lab_icd10tm = '0472002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000262','','การตรวจ phosphofructokinase  ในซีรั่ม / พลาสม่า ','Phosphofructokinase, serum/plasma','1','0472002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0472002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ phosphofructokinase  ในเซลล์เม็ดเลือดขาว',lab_ncd_note = 'Phosphofructokinase, WBC' where lab_icd10tm = '0472016';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000263','','การตรวจ phosphofructokinase  ในเซลล์เม็ดเลือดขาว','Phosphofructokinase, WBC','1','0472016'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0472016');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ phosphofructokinase  ในเซลล์เม็ดเลือดแดง',lab_ncd_note = 'Phosphofructokinase, RBC' where lab_icd10tm = '0472017';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000264','','การตรวจ phosphofructokinase  ในเซลล์เม็ดเลือดแดง','Phosphofructokinase, RBC','1','0472017'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0472017');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ pyrimidine 5 nucleotidase  ในเลือด ',lab_ncd_note = 'Pyrimidine 5-nucleotidase, whole blood' where lab_icd10tm = '0472201';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000265','','การตรวจ pyrimidine 5 nucleotidase  ในเลือด ','Pyrimidine 5-nucleotidase, whole blood','1','0472201'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0472201');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ pyrimidine 5 – nucleotidase  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Pyrimidine 5-nucleotidase, serum/plasma' where lab_icd10tm = '0472202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000266','','การตรวจ pyrimidine 5 – nucleotidase  ในซีรั่ม / พลาสม่า ','Pyrimidine 5-nucleotidase, serum/plasma','1','0472202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0472202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ pyruvate kinase ใน เซรั่ม / พลาสม่า ',lab_ncd_note = 'Pyruvate kinase, serum/plasma' where lab_icd10tm = '0472402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000267','','การตรวจ pyruvate kinase ใน เซรั่ม / พลาสม่า ','Pyruvate kinase, serum/plasma','1','0472402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0472402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ pyruvate kinase ในเซลล์เม็ดเลือดแดง',lab_ncd_note = 'Pyruvate kinase, RBC' where lab_icd10tm = '0472417';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000268','','การตรวจ pyruvate kinase ในเซลล์เม็ดเลือดแดง','Pyruvate kinase, RBC','1','0472417'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0472417');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ Sphingomyelinase  ในเลือด',lab_ncd_note = 'Sphingomyelinase, whole blood' where lab_icd10tm = '0472601';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000269','','การตรวจ Sphingomyelinase  ในเลือด','Sphingomyelinase, whole blood','1','0472601'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0472601');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ Sphingomyelinase  ในเนื้อเยื่ออื่นๆ มิได้จำแนก',lab_ncd_note = 'Sphingomyelinase, tissue NEC' where lab_icd10tm = '0472650';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000270','','การตรวจ Sphingomyelinase  ในเนื้อเยื่ออื่นๆ มิได้จำแนก','Sphingomyelinase, tissue NEC','1','0472650'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0472650');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจเอ็นไซม์ของเซลล์เม็ดเลือดแดงอื่นๆมิได้จำแนก จากสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Red blood cell enzyme NEC, any specimen type' where lab_icd10tm = '0479099';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000271','','การตรวจเอ็นไซม์ของเซลล์เม็ดเลือดแดงอื่นๆมิได้จำแนก จากสิ่งส่งตรวจชนิดต่างๆ','Red blood cell enzyme NEC, any specimen type','1','0479099'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0479099');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ alpha-acetylglucosaminidase ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Alpha-acetylglucosaminidase, serum/plasma' where lab_icd10tm = '0480202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000272','','การตรวจ alpha-acetylglucosaminidase ในซีรั่ม / พลาสม่า','Alpha-acetylglucosaminidase, serum/plasma','1','0480202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0480202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ alpha - acetylglucosaminidase ในเนื้อเยื่ออื่นๆ มิได้จำแนก',lab_ncd_note = 'Alpha-acetylglucosaminidase, tissue NEC' where lab_icd10tm = '0480250';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000273','','การตรวจ alpha - acetylglucosaminidase ในเนื้อเยื่ออื่นๆ มิได้จำแนก','Alpha-acetylglucosaminidase, tissue NEC','1','0480250'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0480250');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ alpha-fucosidase ในเซลล์เม็ดเลือดขาว',lab_ncd_note = 'Alpha-fucosidase, WBC' where lab_icd10tm = '0480416';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000274','','การตรวจ alpha-fucosidase ในเซลล์เม็ดเลือดขาว','Alpha-fucosidase, WBC','1','0480416'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0480416');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ alpha-fucosidase ในเนื้อเยื่ออื่นๆ มิได้จำแนก',lab_ncd_note = 'Alhpa-fucosidase, tissue NEC' where lab_icd10tm = '0480450';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000275','','การตรวจ alpha-fucosidase ในเนื้อเยื่ออื่นๆ มิได้จำแนก','Alhpa-fucosidase, tissue NEC','1','0480450'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0480450');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ alpha-galactosidase ในเซลล์เม็ดเลือดขาว',lab_ncd_note = 'Alpha-galactosidase,WBC' where lab_icd10tm = '0480616';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000276','','การตรวจ alpha-galactosidase ในเซลล์เม็ดเลือดขาว','Alpha-galactosidase,WBC','1','0480616'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0480616');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ alpha-galactosidase ในเนื้อเยื่ออื่นๆ มิได้จำแนก',lab_ncd_note = 'Alpha-galactosidase, tissue NEC' where lab_icd10tm = '0480650';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000277','','การตรวจ alpha-galactosidase ในเนื้อเยื่ออื่นๆ มิได้จำแนก','Alpha-galactosidase, tissue NEC','1','0480650'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0480650');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ alpha-galactosidase ในเซลล์เม็ดเลือดขาว',lab_ncd_note = 'Alpha-glucosidase, WBC' where lab_icd10tm = '0480816';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000278','','การตรวจ alpha-galactosidase ในเซลล์เม็ดเลือดขาว','Alpha-glucosidase, WBC','1','0480816'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0480816');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ alpha-galactosidase ในเนื้อเยื่ออื่นๆ มิได้จำแนก',lab_ncd_note = 'Alpha-glucosidase, tissue NEC' where lab_icd10tm = '0480850';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000279','','การตรวจ alpha-galactosidase ในเนื้อเยื่ออื่นๆ มิได้จำแนก','Alpha-glucosidase, tissue NEC','1','0480850'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0480850');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ alpha-iduronidase ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Alpha-iduronidase, serum/plasma' where lab_icd10tm = '0481002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000280','','การตรวจ alpha-iduronidase ในซีรั่ม / พลาสม่า','Alpha-iduronidase, serum/plasma','1','0481002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0481002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ alpha-iduronidase ในเซลล์เม็ดเลือดขาว',lab_ncd_note = 'Alpha-iduronidase, WBC' where lab_icd10tm = '0481016';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000281','','การตรวจ alpha-iduronidase ในเซลล์เม็ดเลือดขาว','Alpha-iduronidase, WBC','1','0481016'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0481016');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ alpha-iduronidase ในเนื้อเยื่ออื่นๆ มิได้จำแนก',lab_ncd_note = 'Alpha-iduronidase, tissue NEC' where lab_icd10tm = '0481050';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000282','','การตรวจ alpha-iduronidase ในเนื้อเยื่ออื่นๆ มิได้จำแนก','Alpha-iduronidase, tissue NEC','1','0481050'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0481050');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ alpha-mannosidase ในเซลล์เม็ดเลือดขาว',lab_ncd_note = 'Alpha-mannosidase, WBC' where lab_icd10tm = '0481216';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000283','','การตรวจ alpha-mannosidase ในเซลล์เม็ดเลือดขาว','Alpha-mannosidase, WBC','1','0481216'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0481216');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ alpha-mannosidase ในเนื้อเยื่ออื่นๆมิได้จำแนก',lab_ncd_note = 'Alpha-mannosidase, tissue NEC' where lab_icd10tm = '0481250';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000284','','การตรวจ alpha-mannosidase ในเนื้อเยื่ออื่นๆมิได้จำแนก','Alpha-mannosidase, tissue NEC','1','0481250'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0481250');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ alpha-neuraminidase ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Alpha-neuraminidase, serum/plasma' where lab_icd10tm = '0481402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000285','','การตรวจ alpha-neuraminidase ในซีรั่ม / พลาสม่า ','Alpha-neuraminidase, serum/plasma','1','0481402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0481402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ alpha-neuraminidase ในเนื้อเยื่ออื่นๆ มิได้จำแนก',lab_ncd_note = 'Alpha-neuraminidase, tissue NEC' where lab_icd10tm = '0481450';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000286','','การตรวจ alpha-neuraminidase ในเนื้อเยื่ออื่นๆ มิได้จำแนก','Alpha-neuraminidase, tissue NEC','1','0481450'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0481450');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ arylsulfatase A  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Arylsulfatase A, serum/plasma' where lab_icd10tm = '0481602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000287','','การตรวจ arylsulfatase A  ในซีรั่ม / พลาสม่า ','Arylsulfatase A, serum/plasma','1','0481602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0481602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ arylsulfatase A ใน ของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Arylsulfatase A, other fluid NEC' where lab_icd10tm = '0481615';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000288','','การตรวจ arylsulfatase A ใน ของเหลวอื่นๆ มิได้จำแนก','Arylsulfatase A, other fluid NEC','1','0481615'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0481615');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ arylsulfatase A  ในเซลล์เม็ดเลือดขาว',lab_ncd_note = 'Arylsulfatase A, WBC' where lab_icd10tm = '0481616';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000289','','การตรวจ arylsulfatase A  ในเซลล์เม็ดเลือดขาว','Arylsulfatase A, WBC','1','0481616'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0481616');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ arylsulfatase A  ในเนื้อเยื่ออื่นๆมิได้จำแนก',lab_ncd_note = 'Arylsulfatase A, tissue NEC' where lab_icd10tm = '0481650';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000290','','การตรวจ arylsulfatase A  ในเนื้อเยื่ออื่นๆมิได้จำแนก','Arylsulfatase A, tissue NEC','1','0481650'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0481650');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ arylsulfatase B  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Arylsulfatase B, serum/plasma' where lab_icd10tm = '0481802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000291','','การตรวจ arylsulfatase B  ในซีรั่ม / พลาสม่า','Arylsulfatase B, serum/plasma','1','0481802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0481802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ arylsulfatase B  ในของเหลวอื่น ๆ ที่มิได้จำแนก',lab_ncd_note = 'Arylsulfatase B, other fluid NEC' where lab_icd10tm = '0481815';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000292','','การตรวจ arylsulfatase B  ในของเหลวอื่น ๆ ที่มิได้จำแนก','Arylsulfatase B, other fluid NEC','1','0481815'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0481815');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ arylsulfatase B ในเซลล์เม็ดเลือดขาว',lab_ncd_note = 'Arylsulfatase B, WBC' where lab_icd10tm = '0481816';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000293','','การตรวจ arylsulfatase B ในเซลล์เม็ดเลือดขาว','Arylsulfatase B, WBC','1','0481816'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0481816');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ arylsulfatase B  ในเนื้อเยื่ออื่นๆ มิได้จำแนก',lab_ncd_note = 'Arylsulfatase B, tissue NEC' where lab_icd10tm = '0481850';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000294','','การตรวจ arylsulfatase B  ในเนื้อเยื่ออื่นๆ มิได้จำแนก','Arylsulfatase B, tissue NEC','1','0481850'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0481850');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ arylsulfatase อื่นๆ ที่ไม่ได้ระบุ  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Arylsulfatase NOS, serum/plasma' where lab_icd10tm = '0482002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000295','','การตรวจ arylsulfatase อื่นๆ ที่ไม่ได้ระบุ  ในซีรั่ม / พลาสม่า ','Arylsulfatase NOS, serum/plasma','1','0482002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0482002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ arylsulfatase อื่นๆ ที่ไม่ได้ระบุ  ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Arylsulfatase NOS, other fluid NEC' where lab_icd10tm = '0482015';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000296','','การตรวจ arylsulfatase อื่นๆ ที่ไม่ได้ระบุ  ในของเหลวอื่นๆ มิได้จำแนก','Arylsulfatase NOS, other fluid NEC','1','0482015'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0482015');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ arylsulfatase อื่นๆที่ไม่ได้ระบุ ในเซลล์เม็ดเลือดขาว',lab_ncd_note = 'Arylsulfatase NOS, WBC' where lab_icd10tm = '0482016';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000297','','การตรวจ arylsulfatase อื่นๆที่ไม่ได้ระบุ ในเซลล์เม็ดเลือดขาว','Arylsulfatase NOS, WBC','1','0482016'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0482016');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ arylsulfatase อื่นๆที่มิได้ระบุ  ในเนื้อเยื่ออื่นๆ มิได้จำแนก',lab_ncd_note = 'Arylsulfatase NOS, tissue NEC' where lab_icd10tm = '0482050';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000298','','การตรวจ arylsulfatase อื่นๆที่มิได้ระบุ  ในเนื้อเยื่ออื่นๆ มิได้จำแนก','Arylsulfatase NOS, tissue NEC','1','0482050'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0482050');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจเอ็นไซม์ beta-fructofuranosidase ใน ซีรั่ม / พลาสม่า ',lab_ncd_note = 'Beta-fructofuranosidase, serum/plasma' where lab_icd10tm = '0482202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000299','','การตรวจเอ็นไซม์ beta-fructofuranosidase ใน ซีรั่ม / พลาสม่า ','Beta-fructofuranosidase, serum/plasma','1','0482202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0482202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจเอ็นไซม์ beta-fructofuranosidase ในเซลล์เม็ดเลือดขาว',lab_ncd_note = 'Beta-fructofuranosidase, WBC' where lab_icd10tm = '0482216';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000300','','การตรวจเอ็นไซม์ beta-fructofuranosidase ในเซลล์เม็ดเลือดขาว','Beta-fructofuranosidase, WBC','1','0482216'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0482216');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจเอ็นไซม์ beta - galactosidase  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Beta-galactosidase, serum/plasma' where lab_icd10tm = '0482402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000301','','การตรวจเอ็นไซม์ beta - galactosidase  ในซีรั่ม / พลาสม่า','Beta-galactosidase, serum/plasma','1','0482402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0482402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจเอ็นไซม์ beta - galactosidase  ในเซลล์เม็ดเลือดขาว',lab_ncd_note = 'Beta-galactosidase, WBC' where lab_icd10tm = '0482402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000301','','การตรวจเอ็นไซม์ beta - galactosidase  ในเซลล์เม็ดเลือดขาว','Beta-galactosidase, WBC','1','0482402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0482402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจเอ็นไซม์ beta - galactosidase ในเนื้อเยื่ออื่นๆ มิได้จำแนก',lab_ncd_note = 'Beta-galactosidase, tissue NEC' where lab_icd10tm = '0482450';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000303','','การตรวจเอ็นไซม์ beta - galactosidase ในเนื้อเยื่ออื่นๆ มิได้จำแนก','Beta-galactosidase, tissue NEC','1','0482450'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0482450');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ beta glucocerebrosidase  ในเนื้อเยื่ออื่นๆ มิได้จำแนก',lab_ncd_note = 'Beta-glucocerebrosidase, tissue NEC' where lab_icd10tm = '0482650';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000304','','การตรวจ beta glucocerebrosidase  ในเนื้อเยื่ออื่นๆ มิได้จำแนก','Beta-glucocerebrosidase, tissue NEC','1','0482650'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0482650');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ beta – glucosidase  ในเซลล์เม็ดเลือดขาว ',lab_ncd_note = 'Beta-glucosidase, WBC' where lab_icd10tm = '0482816';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000305','','การตรวจ beta – glucosidase  ในเซลล์เม็ดเลือดขาว ','Beta-glucosidase, WBC','1','0482816'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0482816');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ beta – glucosidase  ในเนื้อเยื่ออื่นๆมิได้จำแนก',lab_ncd_note = 'Beta-glucosidase, tissue NEC' where lab_icd10tm = '0482850';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000306','','การตรวจ beta – glucosidase  ในเนื้อเยื่ออื่นๆมิได้จำแนก','Beta-glucosidase, tissue NEC','1','0482850'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0482850');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ beta – glucuronidase  ในเซลล์เม็ดเลือดขาว',lab_ncd_note = 'Beta-glucuronidase, WBC' where lab_icd10tm = '0483016';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000307','','การตรวจ beta – glucuronidase  ในเซลล์เม็ดเลือดขาว','Beta-glucuronidase, WBC','1','0483016'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0483016');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ beta - glucuronidase  ในเนื้อเยื่ออื่นๆ มิได้จำแนก',lab_ncd_note = 'Beta-glucuronidase, tissue NEC' where lab_icd10tm = '0483050';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000308','','การตรวจ beta - glucuronidase  ในเนื้อเยื่ออื่นๆ มิได้จำแนก','Beta-glucuronidase, tissue NEC','1','0483050'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0483050');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ galactosylceramidase  ในเนื้อเยื่ออื่นๆ มิได้จำแนก ',lab_ncd_note = 'Galactosylceramidase, tissue NEC' where lab_icd10tm = '0483250';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000309','','การตรวจ galactosylceramidase  ในเนื้อเยื่ออื่นๆ มิได้จำแนก ','Galactosylceramidase, tissue NEC','1','0483250'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0483250');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ hexosaminidase A  ในเซรั่ม / พลาสม่า',lab_ncd_note = 'Hexosaminidase A, serum/plasma' where lab_icd10tm = '0483402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000310','','การตรวจ hexosaminidase A  ในเซรั่ม / พลาสม่า','Hexosaminidase A, serum/plasma','1','0483402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0483402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ hexosaminidase B  ในเซรั่ม / พลาสม่า',lab_ncd_note = 'Hexosaminidase B, serum/plasma' where lab_icd10tm = '0483602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000311','','การตรวจ hexosaminidase B  ในเซรั่ม / พลาสม่า','Hexosaminidase B, serum/plasma','1','0483602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0483602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ hexosaminidase อื่นๆ ที่ไม่ได้ระบุ  ในเซรั่ม / พลาสม่า',lab_ncd_note = 'Hexosaminidase NOS, serum/plasma' where lab_icd10tm = '0483802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000312','','การตรวจ hexosaminidase อื่นๆ ที่ไม่ได้ระบุ  ในเซรั่ม / พลาสม่า','Hexosaminidase NOS, serum/plasma','1','0483802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0483802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ hexosaminidase อื่นๆ ที่ไม่ได้ระบุ  ในเซลล์เม็ดเลือดขาว ',lab_ncd_note = 'Hexosaminidase NOS, WBC' where lab_icd10tm = '0483816';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000313','','การตรวจ hexosaminidase อื่นๆ ที่ไม่ได้ระบุ  ในเซลล์เม็ดเลือดขาว ','Hexosaminidase NOS, WBC','1','0483816'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0483816');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ hexosaminidase  ในเนื้อเยื่ออื่นๆ  มิได้จำแนก',lab_ncd_note = 'Hexosaminidase NOS, tissue NEC' where lab_icd10tm = '0483850';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000314','','การตรวจ hexosaminidase  ในเนื้อเยื่ออื่นๆ  มิได้จำแนก','Hexosaminidase NOS, tissue NEC','1','0483850'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0483850');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ ridyl transferase  ในเซลล์เม็ดเลือดขาว ',lab_ncd_note = 'Uridyl transferase, WBC' where lab_icd10tm = '0484016';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000315','','การตรวจ ridyl transferase  ในเซลล์เม็ดเลือดขาว ','Uridyl transferase, WBC','1','0484016'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0484016');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ ridyl transferase  ในเนื้อเยื่ออื่นๆ มิได้จำแนก',lab_ncd_note = 'Uridyl transferase, tissue NEC' where lab_icd10tm = '0484050';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000316','','การตรวจ ridyl transferase  ในเนื้อเยื่ออื่นๆ มิได้จำแนก','Uridyl transferase, tissue NEC','1','0484050'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0484050');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ lysosomal อื่นๆมิได้จำแนก ในสิ่งส่งตรวจต่าง ๆ',lab_ncd_note = 'Lysosomal enzyme NEC, any specimen type' where lab_icd10tm = '0489099';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000317','','การตรวจ lysosomal อื่นๆมิได้จำแนก ในสิ่งส่งตรวจต่าง ๆ','Lysosomal enzyme NEC, any specimen type','1','0489099'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0489099');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ 5-Nucleotidase ในซีรั่ม / พลาสม่า ',lab_ncd_note = '5-Nucleotidase, serum/plasma' where lab_icd10tm = '0490202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000318','','การตรวจ 5-Nucleotidase ในซีรั่ม / พลาสม่า ','5-Nucleotidase, serum/plasma','1','0490202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0490202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ 5-Nucleotidase ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = '5-Nucleotidase, other fluid NEC' where lab_icd10tm = '0490215';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000319','','การตรวจ 5-Nucleotidase ในของเหลวอื่นๆ มิได้จำแนก','5-Nucleotidase, other fluid NEC','1','0490215'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0490215');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ acetylcholinesterase  ในเซลล์เม็ดเลือดแดง',lab_ncd_note = 'Acetylcholinesterase,RBC' where lab_icd10tm = '0490417';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000320','','การตรวจ acetylcholinesterase  ในเซลล์เม็ดเลือดแดง','Acetylcholinesterase,RBC','1','0490417'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0490417');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ acid phosphatase  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Acid phosphatase, serum/plasma' where lab_icd10tm = '0490602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000321','','การตรวจ acid phosphatase  ในซีรั่ม / พลาสม่า','Acid phosphatase, serum/plasma','1','0490602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0490602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ Acid phosphatase  ในของเหลวอื่น ๆ มิได้จำแนก',lab_ncd_note = 'Acid phosphatase, other fluid NEC' where lab_icd10tm = '0490615';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000322','','การตรวจ Acid phosphatase  ในของเหลวอื่น ๆ มิได้จำแนก','Acid phosphatase, other fluid NEC','1','0490615'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0490615');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ adenosine deaminase  ในเลือด ',lab_ncd_note = 'Adenosine deaminase, whole blood' where lab_icd10tm = '0490801';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000323','','การตรวจ adenosine deaminase  ในเลือด ','Adenosine deaminase, whole blood','1','0490801'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0490801');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ alanine aminotransferase ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Alanine aminotransferase, serum/plasma' where lab_icd10tm = '0491002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000324','','การตรวจ alanine aminotransferase ในซีรั่ม / พลาสม่า ','Alanine aminotransferase, serum/plasma','1','0491002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0491002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ alanine aminotransferase ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Alanine aminotransferase, other fluid NEC' where lab_icd10tm = '0491015';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000325','','การตรวจ alanine aminotransferase ในของเหลวอื่นๆ มิได้จำแนก','Alanine aminotransferase, other fluid NEC','1','0491015'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0491015');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ alkaline phosphatase ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Alkaline phosphatase, serum/plasma' where lab_icd10tm = '0491202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000326','','การตรวจ alkaline phosphatase ในซีรั่ม / พลาสม่า ','Alkaline phosphatase, serum/plasma','1','0491202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0491202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ alkaline phosphatase  ในของเหลวอื่น ๆ มิได้จำแนก',lab_ncd_note = 'Alkaline phosphatase, other fluid NEC' where lab_icd10tm = '0491215';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000327','','การตรวจ alkaline phosphatase  ในของเหลวอื่น ๆ มิได้จำแนก','Alkaline phosphatase, other fluid NEC','1','0491215'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0491215');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ อะมีเลส ในเซรั่ม / พลาสม่า',lab_ncd_note = 'Amylase, serum/plasma' where lab_icd10tm = '0491402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000328','','การตรวจ อะมีเลส ในเซรั่ม / พลาสม่า','Amylase, serum/plasma','1','0491402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0491402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ อะมีเลส ในของเหลวอื่นๆ มิได้จำแนก ',lab_ncd_note = 'Amylase, other fluid NEC' where lab_icd10tm = '0491415';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000329','','การตรวจ อะมีเลส ในของเหลวอื่นๆ มิได้จำแนก ','Amylase, other fluid NEC','1','0491415'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0491415');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ angiotensin converting  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Angiotensin converting enzyme, serum/plasma' where lab_icd10tm = '0491602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000330','','การตรวจ angiotensin converting  ในซีรั่ม / พลาสม่า','Angiotensin converting enzyme, serum/plasma','1','0491602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0491602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ aspartate aminotransferase  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Aspartate aminotransferase, serum/plasma' where lab_icd10tm = '0491802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000331','','การตรวจ aspartate aminotransferase  ในซีรั่ม / พลาสม่า ','Aspartate aminotransferase, serum/plasma','1','0491802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0491802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ aspartate aminotransferase  ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Aspartate aminotransferase, other fluid NEC' where lab_icd10tm = '0491815';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000332','','การตรวจ aspartate aminotransferase  ในของเหลวอื่นๆ มิได้จำแนก','Aspartate aminotransferase, other fluid NEC','1','0491815'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0491815');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ biotinidase ในเลือด ',lab_ncd_note = 'Biotinidase, whole blood' where lab_icd10tm = '0492001';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000333','','การตรวจ biotinidase ในเลือด ','Biotinidase, whole blood','1','0492001'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0492001');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ biotinidase  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Biotinidase, serum/plasma' where lab_icd10tm = '0492002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000334','','การตรวจ biotinidase  ในซีรั่ม / พลาสม่า ','Biotinidase, serum/plasma','1','0492002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0492002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ cholinesterase (เทียม)  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Cholinesterase (pseudo), serum/plasma' where lab_icd10tm = '0492202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000335','','การตรวจ cholinesterase (เทียม)  ในซีรั่ม / พลาสม่า ','Cholinesterase (pseudo), serum/plasma','1','0492202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0492202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ cholinesterase (เทียม)  ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Cholinesterase (pseudo), other fluid NEC' where lab_icd10tm = '0492215';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000336','','การตรวจ cholinesterase (เทียม)  ในของเหลวอื่นๆ มิได้จำแนก','Cholinesterase (pseudo), other fluid NEC','1','0492215'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0492215');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ cholinesterase phenotyping  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Cholinesterase phenotyping, serum/plasma' where lab_icd10tm = '0492402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000337','','การตรวจ cholinesterase phenotyping  ในซีรั่ม / พลาสม่า ','Cholinesterase phenotyping, serum/plasma','1','0492402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0492402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ chymotrypsin  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Chymotrypsin, serum/plasma' where lab_icd10tm = '0492602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000338','','การตรวจ chymotrypsin  ในซีรั่ม / พลาสม่า ','Chymotrypsin, serum/plasma','1','0492602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0492602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ chymotrypsin  ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Chymotrypsin, other fluic NEC' where lab_icd10tm = '0492615';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000339','','การตรวจ chymotrypsin  ในของเหลวอื่นๆ มิได้จำแนก','Chymotrypsin, other fluic NEC','1','0492615'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0492615');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ chymotrypsin  ในอุจจาระ',lab_ncd_note = 'Chymotrypsin, stool' where lab_icd10tm = '0492666';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000340','','การตรวจ chymotrypsin  ในอุจจาระ','Chymotrypsin, stool','1','0492666'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0492666');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ creatine kinase  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Creatine kinase, serum/plasma' where lab_icd10tm = '0492802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000341','','การตรวจ creatine kinase  ในซีรั่ม / พลาสม่า ','Creatine kinase, serum/plasma','1','0492802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0492802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ creatine kinase  ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Creatine kinase, other fluid NEC' where lab_icd10tm = '0492815';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000342','','การตรวจ creatine kinase  ในของเหลวอื่นๆ มิได้จำแนก','Creatine kinase, other fluid NEC','1','0492815'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0492815');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ dihydropteridine reductase  ในเลือด ',lab_ncd_note = 'Dihydropteridine reductase, whole blood' where lab_icd10tm = '0493001';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000343','','การตรวจ dihydropteridine reductase  ในเลือด ','Dihydropteridine reductase, whole blood','1','0493001'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0493001');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ dihydropteridine reductase  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Dihydropteridine reductase, serum/plasma' where lab_icd10tm = '0493002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000344','','การตรวจ dihydropteridine reductase  ในซีรั่ม / พลาสม่า ','Dihydropteridine reductase, serum/plasma','1','0493002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0493002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ esterase ในเซลล์เม็ดเลือดขาว',lab_ncd_note = 'Esterase, WBC' where lab_icd10tm = '0493216';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000345','','การตรวจ esterase ในเซลล์เม็ดเลือดขาว','Esterase, WBC','1','0493216'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0493216');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจเอ็นไซม์ แกมมา กลูตามิล ทรานสเฟอเรส   ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Gamma glutamyl transferase, serum/plasma' where lab_icd10tm = '0493402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000346','','การตรวจเอ็นไซม์ แกมมา กลูตามิล ทรานสเฟอเรส   ในซีรั่ม / พลาสม่า ','Gamma glutamyl transferase, serum/plasma','1','0493402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0493402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจเอ็นไซม์ แกมมา กลูตามิล ทรานสเฟอเรส  ในของเหลวอื่น ๆ มิได้จำแนก',lab_ncd_note = 'Gamma glutamyl transferase, other fluid NEC' where lab_icd10tm = '0493415';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000347','','การตรวจเอ็นไซม์ แกมมา กลูตามิล ทรานสเฟอเรส  ในของเหลวอื่น ๆ มิได้จำแนก','Gamma glutamyl transferase, other fluid NEC','1','0493415'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0493415');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจเอ็นไซม์ กลูตาเมตดีไฮโดรจีเนส  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Glutamate dehydrogenase, serum/plasma' where lab_icd10tm = '0493602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000348','','การตรวจเอ็นไซม์ กลูตาเมตดีไฮโดรจีเนส  ในซีรั่ม / พลาสม่า ','Glutamate dehydrogenase, serum/plasma','1','0493602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0493602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ hydroxybutyrate dehydrogenase ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Hydroxybutyrate dehydrogenase, serum/plasma' where lab_icd10tm = '0493802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000349','','การตรวจ hydroxybutyrate dehydrogenase ในซีรั่ม / พลาสม่า ','Hydroxybutyrate dehydrogenase, serum/plasma','1','0493802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0493802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ isocitrate dehydrogenase ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Isocitrate dehydrogenase, serum/plasma' where lab_icd10tm = '0494002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000350','','การตรวจ isocitrate dehydrogenase ในซีรั่ม / พลาสม่า ','Isocitrate dehydrogenase, serum/plasma','1','0494002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0494002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ lactatedehydrogenase ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Lactatedehydrogenase, serum/plasma' where lab_icd10tm = '0494202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000351','','การตรวจ lactatedehydrogenase ในซีรั่ม / พลาสม่า ','Lactatedehydrogenase, serum/plasma','1','0494202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0494202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ lactatedehydrogenase  ในของเหลวอื่นๆ ที่มิได้จำแนก',lab_ncd_note = 'Lactatedehydrogenase, other fluid NEC' where lab_icd10tm = '0494215';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000352','','การตรวจ lactatedehydrogenase  ในของเหลวอื่นๆ ที่มิได้จำแนก','Lactatedehydrogenase, other fluid NEC','1','0494215'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0494215');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ leucine aminopeptidase  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Leucine aminopeptidase, serum/plasma' where lab_icd10tm = '0494402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000353','','การตรวจ leucine aminopeptidase  ในซีรั่ม / พลาสม่า ','Leucine aminopeptidase, serum/plasma','1','0494402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0494402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ เอ็นไซม์ไลเปส  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Lipase, serum/plasma' where lab_icd10tm = '0494602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000354','','การตรวจ เอ็นไซม์ไลเปส  ในซีรั่ม / พลาสม่า','Lipase, serum/plasma','1','0494602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0494602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ เอ็นไซม์ไลเปส  ในของเหลวอื่นๆ  มิได้จำแนก',lab_ncd_note = 'Lipase, other fluid NEC' where lab_icd10tm = '0494615';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000355','','การตรวจ เอ็นไซม์ไลเปส  ในของเหลวอื่นๆ  มิได้จำแนก','Lipase, other fluid NEC','1','0494615'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0494615');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ Macroamylase  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Macroamylase, serum/plasma' where lab_icd10tm = '0494802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000356','','การตรวจ Macroamylase  ในซีรั่ม / พลาสม่า ','Macroamylase, serum/plasma','1','0494802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0494802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ Malate dehydrogenase  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Malate dehydrogenase, serum/plasma' where lab_icd10tm = '0495002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000357','','การตรวจ Malate dehydrogenase  ในซีรั่ม / พลาสม่า ','Malate dehydrogenase, serum/plasma','1','0495002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0495002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจเอ็นไซม์ เพพซิน  ในปัสสาวะ ',lab_ncd_note = 'Pepsin, urine' where lab_icd10tm = '0495203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000358','','การตรวจเอ็นไซม์ เพพซิน  ในปัสสาวะ ','Pepsin, urine','1','0495203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0495203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจเอ็นไซม์เพพซิน จากของเหลวในกระเพาะอาหาร',lab_ncd_note = 'Pepsin, gastric fluid/content' where lab_icd10tm = '0495206';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000359','','การตรวจเอ็นไซม์เพพซิน จากของเหลวในกระเพาะอาหาร','Pepsin, gastric fluid/content','1','0495206'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0495206');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจเอ็นไซม์ เพพซิน  ในของเหลวอื่นๆ  มิได้จำแนก',lab_ncd_note = 'Pepsin, other fluid NEC' where lab_icd10tm = '0495215';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000360','','การตรวจเอ็นไซม์ เพพซิน  ในของเหลวอื่นๆ  มิได้จำแนก','Pepsin, other fluid NEC','1','0495215'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0495215');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ pepsinogen  ในเลือด',lab_ncd_note = 'Pepsinogen, whole blood' where lab_icd10tm = '0495401';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000361','','การตรวจ pepsinogen  ในเลือด','Pepsinogen, whole blood','1','0495401'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0495401');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ pepsinogen  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Pepsinogen, serum/plasma' where lab_icd10tm = '0495402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000362','','การตรวจ pepsinogen  ในซีรั่ม / พลาสม่า ','Pepsinogen, serum/plasma','1','0495402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0495402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ pepsinogen  ในปัสสาวะ ',lab_ncd_note = 'Pepsinogen, urine' where lab_icd10tm = '0495403';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000363','','การตรวจ pepsinogen  ในปัสสาวะ ','Pepsinogen, urine','1','0495403'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0495403');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ pepsinogen  ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Pepsinogen, other fluid NEC' where lab_icd10tm = '0495415';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000364','','การตรวจ pepsinogen  ในของเหลวอื่นๆ มิได้จำแนก','Pepsinogen, other fluid NEC','1','0495415'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0495415');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ terminal transferase deoxyribonucleotidyl  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Terminal deoxyribonucleotidyl transferase, serum/plasma' where lab_icd10tm = '0495602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000365','','การตรวจ terminal transferase deoxyribonucleotidyl  ในซีรั่ม / พลาสม่า ','Terminal deoxyribonucleotidyl transferase, serum/plasma','1','0495602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0495602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ transketolase  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Transketolase, serum/plasma' where lab_icd10tm = '0495802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000366','','การตรวจ transketolase  ในซีรั่ม / พลาสม่า ','Transketolase, serum/plasma','1','0495802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0495802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ typsin  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Typsin, serum/plasma' where lab_icd10tm = '0496002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000367','','การตรวจ typsin  ในซีรั่ม / พลาสม่า ','Typsin, serum/plasma','1','0496002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0496002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ typsin, ใน ของเหลวอื่นๆ ที่มิได้จำแนก',lab_ncd_note = 'Typsin, other fluid NEC' where lab_icd10tm = '0496015';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000368','','การตรวจ typsin, ใน ของเหลวอื่นๆ ที่มิได้จำแนก','Typsin, other fluid NEC','1','0496015'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0496015');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ typsin  ในอุจจาระ ',lab_ncd_note = 'Typsin, stool' where lab_icd10tm = '0496066';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000369','','การตรวจ typsin  ในอุจจาระ ','Typsin, stool','1','0496066'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0496066');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจเอนไซม์อื่นๆ  มิได้ระบุ  จากสิ่งส่งตรวจต่าง ๆ ',lab_ncd_note = 'Enzyme NEC, any specimen type' where lab_icd10tm = '0499099';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000370','','การตรวจเอนไซม์อื่นๆ  มิได้ระบุ  จากสิ่งส่งตรวจต่าง ๆ ','Enzyme NEC, any specimen type','1','0499099'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0499099');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ alkaline phosphatase isoenzymes ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Alkaline phosphatase isoenzymes, serum/plasma' where lab_icd10tm = '0500202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000371','','การตรวจ alkaline phosphatase isoenzymes ในซีรั่ม / พลาสม่า ','Alkaline phosphatase isoenzymes, serum/plasma','1','0500202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0500202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ creatine kinase isoenzyme ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Creatine kinase isoenzyme, serum/plasma' where lab_icd10tm = '0501202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000372','','การตรวจ creatine kinase isoenzyme ในซีรั่ม / พลาสม่า ','Creatine kinase isoenzyme, serum/plasma','1','0501202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0501202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ creatine kinase MB fraction เท่านั้น  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Creatine kinase, MB fraction only, serum/plasma' where lab_icd10tm = '0501402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000373','','การตรวจ creatine kinase MB fraction เท่านั้น  ในซีรั่ม / พลาสม่า ','Creatine kinase, MB fraction only, serum/plasma','1','0501402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0501402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ lactate dehydrogenase isoenzymes  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Lactate dehydrogenase isoenzymes, serum/plasma' where lab_icd10tm = '0502002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000374','','การตรวจ lactate dehydrogenase isoenzymes  ในซีรั่ม / พลาสม่า ','Lactate dehydrogenase isoenzymes, serum/plasma','1','0502002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0502002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ lactate dehydrogenase isoenzymes  ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Lactate dehydrogenase isoenzymes, other fluid NEC' where lab_icd10tm = '0502015';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000375','','การตรวจ lactate dehydrogenase isoenzymes  ในของเหลวอื่นๆ มิได้จำแนก','Lactate dehydrogenase isoenzymes, other fluid NEC','1','0502015'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0502015');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสารเแคลเซียม  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Calcium, serum/plasma' where lab_icd10tm = '0510202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000376','','การตรวจหาสารเแคลเซียม  ในซีรั่ม / พลาสม่า','Calcium, serum/plasma','1','0510202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0510202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสารแคลเซียม  ในปัสสาวะ ',lab_ncd_note = 'Calcium, urine' where lab_icd10tm = '0510203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000377','','การตรวจหาสารแคลเซียม  ในปัสสาวะ ','Calcium, urine','1','0510203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0510203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสารแคลเซียม  ในของเหลวอื่นๆ ที่มิได้จำแนกไว้',lab_ncd_note = 'Calcium, other fluid NEC' where lab_icd10tm = '0510215';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000378','','การตรวจหาสารแคลเซียม  ในของเหลวอื่นๆ ที่มิได้จำแนกไว้','Calcium, other fluid NEC','1','0510215'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0510215');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาคาร์บอนไดออกไซด์  ในซีรั่ม, พลาสม่า',lab_ncd_note = 'Carbon dioxide, serum/plasma' where lab_icd10tm = '0510402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000379','','การตรวจหาคาร์บอนไดออกไซด์  ในซีรั่ม, พลาสม่า','Carbon dioxide, serum/plasma','1','0510402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0510402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาคาร์บอนไดออกไซด์   ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Carbon dioxide, other fluid NEC' where lab_icd10tm = '0510415';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000380','','การตรวจหาคาร์บอนไดออกไซด์   ในของเหลวอื่นๆ มิได้จำแนก','Carbon dioxide, other fluid NEC','1','0510415'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0510415');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาคาร์บอนไดออกไซด์ ในลมหายใจ',lab_ncd_note = 'Carbon dioxide, breath' where lab_icd10tm = '0510468';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000381','','การตรวจหาคาร์บอนไดออกไซด์ ในลมหายใจ','Carbon dioxide, breath','1','0510468'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0510468');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสารคลอไรด์  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Chloride, serum/plasma' where lab_icd10tm = '0510602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000382','','การตรวจหาสารคลอไรด์  ในซีรั่ม / พลาสม่า','Chloride, serum/plasma','1','0510602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0510602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสารคลอไรด์  ในปัสสาวะ ',lab_ncd_note = 'Chloride, urine' where lab_icd10tm = '0510603';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000383','','การตรวจหาสารคลอไรด์  ในปัสสาวะ ','Chloride, urine','1','0510603'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0510603');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสารคลอไรด์  ในเหงื่อ ',lab_ncd_note = 'Chloride, sweat' where lab_icd10tm = '0510607';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000384','','การตรวจหาสารคลอไรด์  ในเหงื่อ ','Chloride, sweat','1','0510607'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0510607');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสารคลอไรด์  ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Chloride, other fluid NEC' where lab_icd10tm = '0510615';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000385','','การตรวจหาสารคลอไรด์  ในของเหลวอื่นๆ มิได้จำแนก','Chloride, other fluid NEC','1','0510615'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0510615');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสารแคลเซียมบริสุทธิ์ในเลือด ',lab_ncd_note = 'Ionized calcium, whole blood' where lab_icd10tm = '0510801';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000386','','การตรวจหาสารแคลเซียมบริสุทธิ์ในเลือด ','Ionized calcium, whole blood','1','0510801'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0510801');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสารแคลเซียมบริสุทธิ์ ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Ionized calcium, serum/plasma' where lab_icd10tm = '0510802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000387','','การตรวจหาสารแคลเซียมบริสุทธิ์ ในซีรั่ม / พลาสม่า','Ionized calcium, serum/plasma','1','0510802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0510802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุแมกนีเซียม ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Magnesium, serum/plasma' where lab_icd10tm = '0511002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000388','','การตรวจหาธาตุแมกนีเซียม ในซีรั่ม / พลาสม่า ','Magnesium, serum/plasma','1','0511002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0511002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุแมกนีเซียม ในปัสสาวะ ',lab_ncd_note = 'Magnesium, urine' where lab_icd10tm = '0511003';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000389','','การตรวจหาธาตุแมกนีเซียม ในปัสสาวะ ','Magnesium, urine','1','0511003'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0511003');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุแมกนีเซียมของเหลวอื่นๆ  มิได้จำแนก',lab_ncd_note = 'Magnesium, other fluid NEC' where lab_icd10tm = '0511015';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000390','','การตรวจหาธาตุแมกนีเซียมของเหลวอื่นๆ  มิได้จำแนก','Magnesium, other fluid NEC','1','0511015'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0511015');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุแมกนีเซียม ในเซลล์เม็ดเลือดแดง',lab_ncd_note = 'Magnesium, RBC' where lab_icd10tm = '0511017';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000391','','การตรวจหาธาตุแมกนีเซียม ในเซลล์เม็ดเลือดแดง','Magnesium, RBC','1','0511017'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0511017');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสารฟอสเฟต (อนินทรีย์)  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Phosphate (inorganic), serum/plasma' where lab_icd10tm = '0511202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000392','','การตรวจหาสารฟอสเฟต (อนินทรีย์)  ในซีรั่ม / พลาสม่า ','Phosphate (inorganic), serum/plasma','1','0511202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0511202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสารฟอสเฟต (อนินทรีย์)  ในปัสสาวะ ',lab_ncd_note = 'Phosphate (inorganic), urine' where lab_icd10tm = '0511203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000393','','การตรวจหาสารฟอสเฟต (อนินทรีย์)  ในปัสสาวะ ','Phosphate (inorganic), urine','1','0511203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0511203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสารฟอสเฟต (อนินทรีย์)  ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Phosphate (inorganic), other fluid NEC' where lab_icd10tm = '0511215';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000394','','การตรวจหาสารฟอสเฟต (อนินทรีย์)  ในของเหลวอื่นๆ มิได้จำแนก','Phosphate (inorganic), other fluid NEC','1','0511215'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0511215');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสารโพแทสเซียมในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Potassium, serum/plasma' where lab_icd10tm = '0511402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000395','','การตรวจหาสารโพแทสเซียมในซีรั่ม / พลาสม่า ','Potassium, serum/plasma','1','0511402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0511402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสารโพแทสเซียม  ในปัสสาวะ',lab_ncd_note = 'Potassium, urine' where lab_icd10tm = '0511403';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000396','','การตรวจหาสารโพแทสเซียม  ในปัสสาวะ','Potassium, urine','1','0511403'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0511403');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสารโพแทสเซียม  ในของเหลวอื่นๆ  มิได้จำแนก',lab_ncd_note = 'Potassium, other fluid NEC' where lab_icd10tm = '0511415';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000397','','การตรวจหาสารโพแทสเซียม  ในของเหลวอื่นๆ  มิได้จำแนก','Potassium, other fluid NEC','1','0511415'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0511415');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสาร  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Sodium, serum/plasma' where lab_icd10tm = '0511602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000398','','การตรวจหาสาร  ในซีรั่ม / พลาสม่า','Sodium, serum/plasma','1','0511602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0511602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสารโซเดียม  ในปัสสาวะ',lab_ncd_note = 'Sodium, urine' where lab_icd10tm = '0511603';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000399','','การตรวจหาสารโซเดียม  ในปัสสาวะ','Sodium, urine','1','0511603'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0511603');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสารโซเดียม ในของเหลวอื่นๆ  มิได้จำแนก',lab_ncd_note = 'Sodium, other fluid NEC' where lab_icd10tm = '0511615';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000400','','การตรวจหาสารโซเดียม ในของเหลวอื่นๆ  มิได้จำแนก','Sodium, other fluid NEC','1','0511615'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0511615');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุโครเมียมในเลือด ',lab_ncd_note = 'Chromium, whole blood' where lab_icd10tm = '0520201';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000401','','การตรวจหาธาตุโครเมียมในเลือด ','Chromium, whole blood','1','0520201'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0520201');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุโครเมี่ยม  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Chromium, serum/plasma' where lab_icd10tm = '0520202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000402','','การตรวจหาธาตุโครเมี่ยม  ในซีรั่ม / พลาสม่า ','Chromium, serum/plasma','1','0520202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0520202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุโครเมียม  ในปัสสาวะ',lab_ncd_note = 'Chromium, urine' where lab_icd10tm = '0520203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000403','','การตรวจหาธาตุโครเมียม  ในปัสสาวะ','Chromium, urine','1','0520203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0520203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุโครเมียม  ในของเหลวอื่นๆ  มิได้จำแนก',lab_ncd_note = 'Chromium, other fluid NEC' where lab_icd10tm = '0520215';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000404','','การตรวจหาธาตุโครเมียม  ในของเหลวอื่นๆ  มิได้จำแนก','Chromium, other fluid NEC','1','0520215'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0520215');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุโคบอลต์  ในเลือด ',lab_ncd_note = 'Cobalt, whole blood' where lab_icd10tm = '0520401';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000405','','การตรวจหาธาตุโคบอลต์  ในเลือด ','Cobalt, whole blood','1','0520401'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0520401');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุโคบอลต์  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Cobalt, serum/plasma' where lab_icd10tm = '0520402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000406','','การตรวจหาธาตุโคบอลต์  ในซีรั่ม / พลาสม่า ','Cobalt, serum/plasma','1','0520402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0520402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุโคบอลต์  ในปัสสาวะ ',lab_ncd_note = 'Cobalt, urine' where lab_icd10tm = '0520403';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000407','','การตรวจหาธาตุโคบอลต์  ในปัสสาวะ ','Cobalt, urine','1','0520403'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0520403');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุโคบอลต์  ในของเหลวอื่นๆ  มิได้จำแนก',lab_ncd_note = 'Cobalt, other fluid NEC' where lab_icd10tm = '0520415';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000408','','การตรวจหาธาตุโคบอลต์  ในของเหลวอื่นๆ  มิได้จำแนก','Cobalt, other fluid NEC','1','0520415'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0520415');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุทองแดง  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Copper, serum/plasma' where lab_icd10tm = '0520602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000409','','การตรวจหาธาตุทองแดง  ในซีรั่ม / พลาสม่า ','Copper, serum/plasma','1','0520602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0520602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุทองแดง  ในปัสสาวะ ',lab_ncd_note = 'Copper, urine' where lab_icd10tm = '0520603';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000410','','การตรวจหาธาตุทองแดง  ในปัสสาวะ ','Copper, urine','1','0520603'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0520603');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุทองแดง  ในของเหลวอื่น ๆ มิได้จำแนก',lab_ncd_note = 'Copper, other fluid NEC' where lab_icd10tm = '0520615';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000411','','การตรวจหาธาตุทองแดง  ในของเหลวอื่น ๆ มิได้จำแนก','Copper, other fluid NEC','1','0520615'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0520615');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสารฟลูออไรด์  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Fluoride, serum/plasma' where lab_icd10tm = '0520802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000412','','การตรวจหาสารฟลูออไรด์  ในซีรั่ม / พลาสม่า ','Fluoride, serum/plasma','1','0520802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0520802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสารฟลูออไรด์  ในปัสสาวะ',lab_ncd_note = 'Fluoride, urine' where lab_icd10tm = '0520803';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000413','','การตรวจหาสารฟลูออไรด์  ในปัสสาวะ','Fluoride, urine','1','0520803'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0520803');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสารฟลูออไรด์  ในของเหลวอื่น ๆ มิได้จำแนก',lab_ncd_note = 'Fluoride, other fluid NEC' where lab_icd10tm = '0520815';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000414','','การตรวจหาสารฟลูออไรด์  ในของเหลวอื่น ๆ มิได้จำแนก','Fluoride, other fluid NEC','1','0520815'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0520815');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุไอโอดีน  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Iodine, serum/plasma' where lab_icd10tm = '0521002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000415','','การตรวจหาธาตุไอโอดีน  ในซีรั่ม / พลาสม่า','Iodine, serum/plasma','1','0521002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0521002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุไอโอดีน  ในปัสสาวะ',lab_ncd_note = 'Iodine, urine' where lab_icd10tm = '0521003';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000416','','การตรวจหาธาตุไอโอดีน  ในปัสสาวะ','Iodine, urine','1','0521003'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0521003');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุไอโอดีน  ในอุจจาระ',lab_ncd_note = 'Iodine, stool' where lab_icd10tm = '0521066';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000417','','การตรวจหาธาตุไอโอดีน  ในอุจจาระ','Iodine, stool','1','0521066'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0521066');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุเหล็ก  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Iron, serum/plasma' where lab_icd10tm = '0521202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000418','','การตรวจหาธาตุเหล็ก  ในซีรั่ม / พลาสม่า','Iron, serum/plasma','1','0521202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0521202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุเหล็ก  ในเนื้อเยื่ออื่นๆ มิได้จำแนก',lab_ncd_note = 'Iron, tissue NEC' where lab_icd10tm = '0521250';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000419','','การตรวจหาธาตุเหล็ก  ในเนื้อเยื่ออื่นๆ มิได้จำแนก','Iron, tissue NEC','1','0521250'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0521250');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุแมงกานีสในเลือด',lab_ncd_note = 'Manganese, whole blood' where lab_icd10tm = '0521401';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000420','','การตรวจหาธาตุแมงกานีสในเลือด','Manganese, whole blood','1','0521401'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0521401');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุแมงกานีส  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Manganese, serum/plasma' where lab_icd10tm = '0521402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000421','','การตรวจหาธาตุแมงกานีส  ในซีรั่ม / พลาสม่า','Manganese, serum/plasma','1','0521402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0521402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุแมงกานีส  ในปัสสาวะ',lab_ncd_note = 'Manganese, urine' where lab_icd10tm = '0521403';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000422','','การตรวจหาธาตุแมงกานีส  ในปัสสาวะ','Manganese, urine','1','0521403'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0521403');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุแมงกานีส  ในของเหลวอื่นๆ  มิได้จำแนก',lab_ncd_note = 'Manganese, other fluid NEC' where lab_icd10tm = '0521415';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000423','','การตรวจหาธาตุแมงกานีส  ในของเหลวอื่นๆ  มิได้จำแนก','Manganese, other fluid NEC','1','0521415'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0521415');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุแมงกานีส  ในเซลล์เม็ดเลือดแดง',lab_ncd_note = 'Manganese, RBC' where lab_icd10tm = '0521417';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000424','','การตรวจหาธาตุแมงกานีส  ในเซลล์เม็ดเลือดแดง','Manganese, RBC','1','0521417'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0521417');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุ molybdenum  ในเลือด',lab_ncd_note = 'Molybdenum, whole blood' where lab_icd10tm = '0521601';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000425','','การตรวจหาธาตุ molybdenum  ในเลือด','Molybdenum, whole blood','1','0521601'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0521601');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุ molybdenum  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Molybdenum, serum/plasma' where lab_icd10tm = '0521602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000426','','การตรวจหาธาตุ molybdenum  ในซีรั่ม / พลาสม่า','Molybdenum, serum/plasma','1','0521602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0521602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุ molybdenum  ในปัสสาวะ ',lab_ncd_note = 'Molybdenum, urine' where lab_icd10tm = '0521603';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000427','','การตรวจหาธาตุ molybdenum  ในปัสสาวะ ','Molybdenum, urine','1','0521603'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0521603');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุ molybdenum  ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Molybdenum, other fluid NEC' where lab_icd10tm = '0521615';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000428','','การตรวจหาธาตุ molybdenum  ในของเหลวอื่นๆ มิได้จำแนก','Molybdenum, other fluid NEC','1','0521615'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0521615');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุนิเกิล  ในเลือด ',lab_ncd_note = 'Nickel, whole blood' where lab_icd10tm = '0521801';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000429','','การตรวจหาธาตุนิเกิล  ในเลือด ','Nickel, whole blood','1','0521801'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0521801');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุนิเกิล  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Nickel, serum/plasma' where lab_icd10tm = '0521802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000430','','การตรวจหาธาตุนิเกิล  ในซีรั่ม / พลาสม่า','Nickel, serum/plasma','1','0521802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0521802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุนิเกิล  ในปัสสาวะ',lab_ncd_note = 'Nickel, urine' where lab_icd10tm = '0521803';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000431','','การตรวจหาธาตุนิเกิล  ในปัสสาวะ','Nickel, urine','1','0521803'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0521803');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุนิกเกิล  ในของเหลวอื่นๆ  มิได้จำแนก',lab_ncd_note = 'Nickel, other fluid NEC' where lab_icd10tm = '0521815';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000432','','การตรวจหาธาตุนิกเกิล  ในของเหลวอื่นๆ  มิได้จำแนก','Nickel, other fluid NEC','1','0521815'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0521815');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุซีลีเนียม  ในเลือด',lab_ncd_note = 'Selenium, whole blood' where lab_icd10tm = '0522001';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000433','','การตรวจหาธาตุซีลีเนียม  ในเลือด','Selenium, whole blood','1','0522001'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0522001');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุซีลีเนียม  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Selenium, serum/plasma' where lab_icd10tm = '0522002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000434','','การตรวจหาธาตุซีลีเนียม  ในซีรั่ม / พลาสม่า','Selenium, serum/plasma','1','0522002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0522002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุซีลีเนียมใน  ปัสสาวะ',lab_ncd_note = 'Selenium, urine' where lab_icd10tm = '0522003';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000435','','การตรวจหาธาตุซีลีเนียมใน  ปัสสาวะ','Selenium, urine','1','0522003'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0522003');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุซีลีเนียม   ในของเหลวอื่นๆ  มิได้จำแนก',lab_ncd_note = 'Selenium, other fluid NEC' where lab_icd10tm = '0522015';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000436','','การตรวจหาธาตุซีลีเนียม   ในของเหลวอื่นๆ  มิได้จำแนก','Selenium, other fluid NEC','1','0522015'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0522015');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสารซิลิโคน ในเลือด',lab_ncd_note = 'Silicone, whole blood' where lab_icd10tm = '0522201';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000437','','การตรวจหาสารซิลิโคน ในเลือด','Silicone, whole blood','1','0522201'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0522201');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสารซิลิโคน  ใน ซีรั่ม / พลาสม่า ',lab_ncd_note = 'Silicone, serum/plasma' where lab_icd10tm = '0522202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000438','','การตรวจหาสารซิลิโคน  ใน ซีรั่ม / พลาสม่า ','Silicone, serum/plasma','1','0522202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0522202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสารซิลิโคน  ในปัสสาวะ',lab_ncd_note = 'Silicone, urine' where lab_icd10tm = '0522203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000439','','การตรวจหาสารซิลิโคน  ในปัสสาวะ','Silicone, urine','1','0522203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0522203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสารซิลิโคน  ในของเหลว อื่น ๆที่มิได้จำแนก',lab_ncd_note = 'Silicone, other fluid NEC' where lab_icd10tm = '0522215';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000440','','การตรวจหาสารซิลิโคน  ในของเหลว อื่น ๆที่มิได้จำแนก','Silicone, other fluid NEC','1','0522215'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0522215');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุสังกะสี  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Zinc, serum/plasma' where lab_icd10tm = '0522402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000441','','การตรวจหาธาตุสังกะสี  ในซีรั่ม / พลาสม่า ','Zinc, serum/plasma','1','0522402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0522402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุสังกะสี  ในปัสสาวะ',lab_ncd_note = 'Zinc, urine' where lab_icd10tm = '0522403';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000442','','การตรวจหาธาตุสังกะสี  ในปัสสาวะ','Zinc, urine','1','0522403'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0522403');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุสังกะสี  ในของเหลวอื่นๆ  มิได้จำแนก',lab_ncd_note = 'Zinc, other fluid NEC' where lab_icd10tm = '0522415';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000443','','การตรวจหาธาตุสังกะสี  ในของเหลวอื่นๆ  มิได้จำแนก','Zinc, other fluid NEC','1','0522415'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0522415');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุสังกะสี  ในเม็ดเลือดแดง',lab_ncd_note = 'Zinc, RBC' where lab_icd10tm = '0522417';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000444','','การตรวจหาธาตุสังกะสี  ในเม็ดเลือดแดง','Zinc, RBC','1','0522417'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0522417');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุสังกะสี  ในเนื้อเยื่ออื่นๆ  มิได้จำแนก',lab_ncd_note = 'Zinc, tissue NEC' where lab_icd10tm = '0522450';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000445','','การตรวจหาธาตุสังกะสี  ในเนื้อเยื่ออื่นๆ  มิได้จำแนก','Zinc, tissue NEC','1','0522450'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0522450');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาธาตุอื่นๆ  มิได้จำแนก จากสิ่งส่งตรวจต่าง ๆ',lab_ncd_note = 'Trace element NEC, any specimen type' where lab_icd10tm = '0529099';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000446','','การตรวจหาธาตุอื่นๆ  มิได้จำแนก จากสิ่งส่งตรวจต่าง ๆ','Trace element NEC, any specimen type','1','0529099'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0529099');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การคัดกรอง/ระบุ คาร์โบไฮเดรต  ในเลือด',lab_ncd_note = 'Carbohydrate screen/identification, whole blood' where lab_icd10tm = '0530201';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000447','','การคัดกรอง/ระบุ คาร์โบไฮเดรต  ในเลือด','Carbohydrate screen/identification, whole blood','1','0530201'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0530201');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การคัดกรอง/ระบุ คาร์โบไฮเดรต  ในปัสสาวะ',lab_ncd_note = 'Carbohydrate screen/identification, urine' where lab_icd10tm = '0530203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000448','','การคัดกรอง/ระบุ คาร์โบไฮเดรต  ในปัสสาวะ','Carbohydrate screen/identification, urine','1','0530203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0530203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การคัดกรอง/ระบุ คาร์โบไฮเดรต  ในอุจจาระ ',lab_ncd_note = 'Carbohydrate screen/identification, stool' where lab_icd10tm = '0530266';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000449','','การคัดกรอง/ระบุ คาร์โบไฮเดรต  ในอุจจาระ ','Carbohydrate screen/identification, stool','1','0530266'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0530266');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา fructosamine ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Fructosamine, serum/plasma' where lab_icd10tm = '0530402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000450','','การตรวจหา fructosamine ในซีรั่ม / พลาสม่า','Fructosamine, serum/plasma','1','0530402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0530402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา fructose  ในปัสสาวะ',lab_ncd_note = 'Fructose, urine' where lab_icd10tm = '0530603';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000451','','การตรวจหา fructose  ในปัสสาวะ','Fructose, urine','1','0530603'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0530603');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา fructose  ในน้ำอสุจิ',lab_ncd_note = 'Fructose, semen' where lab_icd10tm = '0530608';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000452','','การตรวจหา fructose  ในน้ำอสุจิ','Fructose, semen','1','0530608'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0530608');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา galactose ในเลือด',lab_ncd_note = 'Galactose, whole blood' where lab_icd10tm = '0530801';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000453','','การตรวจหา galactose ในเลือด','Galactose, whole blood','1','0530801'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0530801');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา galactose  ในซีรั่ม/พลาสม่า',lab_ncd_note = 'Galactose,serum/plasma' where lab_icd10tm = '0530802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000454','','การตรวจหา galactose  ในซีรั่ม/พลาสม่า','Galactose,serum/plasma','1','0530802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0530802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา galactose  ในปัสสาวะ',lab_ncd_note = 'Galactose, urine' where lab_icd10tm = '0530803';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000455','','การตรวจหา galactose  ในปัสสาวะ','Galactose, urine','1','0530803'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0530803');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาน้ำตาลลูโคสในเลือด',lab_ncd_note = 'Glucose, whole blood' where lab_icd10tm = '0531001';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000456','','การตรวจหาน้ำตาลลูโคสในเลือด','Glucose, whole blood','1','0531001'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0531001');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาน้ำตาลลูโคส  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Glucose, serum/plasma' where lab_icd10tm = '0531002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000457','','การตรวจหาน้ำตาลลูโคส  ในซีรั่ม / พลาสม่า','Glucose, serum/plasma','1','0531002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0531002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาน้ำตาลกลูโคส  ในปัสสาวะ',lab_ncd_note = 'Glucose, urine' where lab_icd10tm = '0531003';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000458','','การตรวจหาน้ำตาลกลูโคส  ในปัสสาวะ','Glucose, urine','1','0531003'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0531003');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจน้ำตาลในเลือด จากหลอดเลือดดำ โดยอดอาหาร',lab_ncd_note = 'Glucose, vein (NPO)' where lab_icd10tm = '0531004';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000459','','การตรวจน้ำตาลในเลือด จากหลอดเลือดดำ โดยอดอาหาร','Glucose, vein (NPO)','1','0531004'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0531004');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาน้ำตาลกลูโคส  ในของเหลวอื่นๆ มิได้ระบุ',lab_ncd_note = 'Glucose, other fluid NEC' where lab_icd10tm = '0531015';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000460','','การตรวจหาน้ำตาลกลูโคส  ในของเหลวอื่นๆ มิได้ระบุ','Glucose, other fluid NEC','1','0531015'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0531015');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาน้ำตาลกลูโคส กึ่งเชิงปริมาณ (โดยใช้แถบทดสอบ)  ในเลือด',lab_ncd_note = 'Glucose, semi-quantitative (tes strip), whole blood' where lab_icd10tm = '0531101';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000461','','การตรวจหาน้ำตาลกลูโคส กึ่งเชิงปริมาณ (โดยใช้แถบทดสอบ)  ในเลือด','Glucose, semi-quantitative (tes strip), whole blood','1','0531101'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0531101');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจน้ำตาลในเลือด จากเส้นเลือดฝอย โดยไม่อดอาหาร ',lab_ncd_note = 'Glucose, capillaries' where lab_icd10tm = '0531102';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000462','','การตรวจน้ำตาลในเลือด จากเส้นเลือดฝอย โดยไม่อดอาหาร ','Glucose, capillaries','1','0531102'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0531102');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา glycerate  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Glycerate, serum/plasma' where lab_icd10tm = '0531202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000463','','การตรวจหา glycerate  ในซีรั่ม / พลาสม่า','Glycerate, serum/plasma','1','0531202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0531202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา glycerate  ในปัสสาวะ',lab_ncd_note = 'Glycerate, urine' where lab_icd10tm = '0531203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000464','','การตรวจหา glycerate  ในปัสสาวะ','Glycerate, urine','1','0531203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0531203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การคัดกรองไกลโคโปรตีน  ในปัสสาวะ',lab_ncd_note = 'Glycoproteins screen, urine' where lab_icd10tm = '0531403';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000465','','การคัดกรองไกลโคโปรตีน  ในปัสสาวะ','Glycoproteins screen, urine','1','0531403'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0531403');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา glycosylated hemoglobin ในเลือด ',lab_ncd_note = 'Glycosylated hemoglobin whole blood (HbA1c)' where lab_icd10tm = '0531601';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000466','','การตรวจหา glycosylated hemoglobin ในเลือด ','Glycosylated hemoglobin whole blood (HbA1c)','1','0531601'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0531601');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา lactate  ในเลือด',lab_ncd_note = 'Lactate, whole blood' where lab_icd10tm = '0531801';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000467','','การตรวจหา lactate  ในเลือด','Lactate, whole blood','1','0531801'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0531801');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา lactate  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Lactate, serum/plasma' where lab_icd10tm = '0531802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000468','','การตรวจหา lactate  ในซีรั่ม / พลาสม่า ','Lactate, serum/plasma','1','0531802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0531802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา Lactate  ในของเหลว อื่น ๆ มิได้จำแนก',lab_ncd_note = 'Lactate, other fluid NEC' where lab_icd10tm = '0531815';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000469','','การตรวจหา Lactate  ในของเหลว อื่น ๆ มิได้จำแนก','Lactate, other fluid NEC','1','0531815'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0531815');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา lactose  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Lactose, serum/plasma' where lab_icd10tm = '0532002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000470','','การตรวจหา lactose  ในซีรั่ม / พลาสม่า ','Lactose, serum/plasma','1','0532002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0532002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา lactose  ในปัสสาวะ ',lab_ncd_note = 'Lactose, urine' where lab_icd10tm = '0532003';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000471','','การตรวจหา lactose  ในปัสสาวะ ','Lactose, urine','1','0532003'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0532003');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรอง mucopolysaccharide  ในปัสสาวะ',lab_ncd_note = 'Mucopolysaccharide screen, urine' where lab_icd10tm = '0532203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000472','','การตรวจคัดกรอง mucopolysaccharide  ในปัสสาวะ','Mucopolysaccharide screen, urine','1','0532203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0532203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา mucopolysaccharide  ในปัสสาวะ',lab_ncd_note = 'Mucopolysaccharide , urine' where lab_icd10tm = '0532403';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000473','','การตรวจหา mucopolysaccharide  ในปัสสาวะ','Mucopolysaccharide , urine','1','0532403'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0532403');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา mucoprotein  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Mucoprotein, serum/plasma' where lab_icd10tm = '0532602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000474','','การตรวจหา mucoprotein  ในซีรั่ม / พลาสม่า','Mucoprotein, serum/plasma','1','0532602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0532602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา pentose  ในปัสสาวะ',lab_ncd_note = 'Pentose, urine' where lab_icd10tm = '0532803';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000475','','การตรวจหา pentose  ในปัสสาวะ','Pentose, urine','1','0532803'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0532803');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา pyruvate  ในเลือด',lab_ncd_note = 'Pyruvate, whole blood' where lab_icd10tm = '0533001';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000476','','การตรวจหา pyruvate  ในเลือด','Pyruvate, whole blood','1','0533001'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0533001');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา pyruvate  ในปัสสาวะ',lab_ncd_note = 'Pyruvate, urine' where lab_icd10tm = '0533003';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000477','','การตรวจหา pyruvate  ในปัสสาวะ','Pyruvate, urine','1','0533003'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0533003');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ pyruvate  ในของเหลวอื่นๆ  มิได้จำแนก ',lab_ncd_note = 'Pyruvate, other fluid NEC' where lab_icd10tm = '0533015';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000478','','การตรวจ pyruvate  ในของเหลวอื่นๆ  มิได้จำแนก ','Pyruvate, other fluid NEC','1','0533015'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0533015');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหา reducing substances  ในปัสสาวะ',lab_ncd_note = 'Reducing substances screen, urine' where lab_icd10tm = '0533203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000479','','การตรวจคัดกรองหา reducing substances  ในปัสสาวะ','Reducing substances screen, urine','1','0533203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0533203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหา reducing substances  ในอุจจาระ',lab_ncd_note = 'Reducing substances screen, stool' where lab_icd10tm = '0533266';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000480','','การตรวจคัดกรองหา reducing substances  ในอุจจาระ','Reducing substances screen, stool','1','0533266'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0533266');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาน้ำตาลซูโครส  ในอุจจาระ',lab_ncd_note = 'Sucrose, stool' where lab_icd10tm = '0533466';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000481','','การตรวจหาน้ำตาลซูโครส  ในอุจจาระ','Sucrose, stool','1','0533466'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0533466');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ xylose  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Xylose, serum/plasma' where lab_icd10tm = '0533602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000482','','การตรวจ xylose  ในซีรั่ม / พลาสม่า','Xylose, serum/plasma','1','0533602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0533602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา xylose  ในปัสสาวะ',lab_ncd_note = 'Xylose, urine' where lab_icd10tm = '0533603';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000483','','การตรวจหา xylose  ในปัสสาวะ','Xylose, urine','1','0533603'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0533603');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์ apolipoprotein A  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Apolipoprotein A assay, serum/plasm' where lab_icd10tm = '0540202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000484','','การวิเคราะห์ apolipoprotein A  ในซีรั่ม / พลาสม่า','Apolipoprotein A assay, serum/plasm','1','0540202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0540202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์ apolipoprotein  B assay  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Apolipoprotein B assay, serum/plasm' where lab_icd10tm = '0540402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000485','','การวิเคราะห์ apolipoprotein  B assay  ในซีรั่ม / พลาสม่า ','Apolipoprotein B assay, serum/plasm','1','0540402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0540402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์ apolipoprotein  อื่นๆ  มิได้จำแนก ในเซรั่ม / พลาสม่า',lab_ncd_note = 'Apolipoprotein assay NEC, serum/plasm' where lab_icd10tm = '0540602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000486','','การวิเคราะห์ apolipoprotein  อื่นๆ  มิได้จำแนก ในเซรั่ม / พลาสม่า','Apolipoprotein assay NEC, serum/plasm','1','0540602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0540602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาคอเลสเตอรอลชนิด HDL  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'HDL Cholesterol, serum/plasma' where lab_icd10tm = '0541202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000487','','การตรวจหาคอเลสเตอรอลชนิด HDL  ในซีรั่ม / พลาสม่า ','HDL Cholesterol, serum/plasma','1','0541202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0541202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาคอเลสเตอรอลชนิด LDL  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'LDL Cholesterol, serum/plasma' where lab_icd10tm = '0541402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000488','','การตรวจหาคอเลสเตอรอลชนิด LDL  ในซีรั่ม / พลาสม่า ','LDL Cholesterol, serum/plasma','1','0541402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0541402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาคอเลสเตอรอลทั้งหมด  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Cholesterol, total, serum/plasma' where lab_icd10tm = '0541602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000489','','การตรวจหาคอเลสเตอรอลทั้งหมด  ในซีรั่ม / พลาสม่า','Cholesterol, total, serum/plasma','1','0541602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0541602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาไขมันในอุจจาระ',lab_ncd_note = 'Fat, stool' where lab_icd10tm = '0542466';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000490','','การตรวจหาไขมันในอุจจาระ','Fat, stool','1','0542466'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0542466');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจกรดไขมัน, esterified  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Fatty Acids, esterified, serum/plasma' where lab_icd10tm = '0542602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000491','','การตรวจกรดไขมัน, esterified  ในซีรั่ม / พลาสม่า','Fatty Acids, esterified, serum/plasma','1','0542602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0542602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา กรดไขมัน non-esterified   ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Fatty Acids, non-esterified, serum/plasma' where lab_icd10tm = '0542802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000492','','การตรวจหา กรดไขมัน non-esterified   ในซีรั่ม / พลาสม่า','Fatty Acids, non-esterified, serum/plasma','1','0542802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0542802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหากรดไขมันทั้งหมด  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Fatty Acids, total, serum/plasma' where lab_icd10tm = '0543002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000493','','การตรวจหากรดไขมันทั้งหมด  ในซีรั่ม / พลาสม่า','Fatty Acids, total, serum/plasma','1','0543002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0543002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา กลีเซอรอลอิสระ ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Glycerol, free, serum/plasma' where lab_icd10tm = '0543202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000494','','การตรวจหา กลีเซอรอลอิสระ ในซีรั่ม / พลาสม่า','Glycerol, free, serum/plasma','1','0543202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0543202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา กลีเซอรอลทั้งหมด ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Glycerol, total, serum/plasma' where lab_icd10tm = '0543402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000495','','การตรวจหา กลีเซอรอลทั้งหมด ในซีรั่ม / พลาสม่า','Glycerol, total, serum/plasma','1','0543402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0543402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา ลิปิด (ทั้งหมด)  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Lipids (total), serum/plasma' where lab_icd10tm = '0544002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000496','','การตรวจหา ลิปิด (ทั้งหมด)  ในซีรั่ม / พลาสม่า','Lipids (total), serum/plasma','1','0544002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0544002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา ลิปิด (ทั้งหมด)  ในอุจจาระ',lab_ncd_note = 'Lipids (total), stool' where lab_icd10tm = '0544066';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000497','','การตรวจหา ลิปิด (ทั้งหมด)  ในอุจจาระ','Lipids (total), stool','1','0544066'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0544066');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา Lipoprotein electrophoresis / phenotyping  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Lipoprotein electrophoresis/phenotyping, serum/plasma' where lab_icd10tm = '0544402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000498','','การตรวจหา Lipoprotein electrophoresis / phenotyping  ในซีรั่ม / พลาสม่า','Lipoprotein electrophoresis/phenotyping, serum/plasma','1','0544402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0544402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา Palmitate  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Palmitate, serum/plasma' where lab_icd10tm = '0545202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000499','','การตรวจหา Palmitate  ในซีรั่ม / พลาสม่า','Palmitate, serum/plasma','1','0545202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0545202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา phospholipids  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Phospholipids, serum/plasma' where lab_icd10tm = '0545402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000500','','การตรวจหา phospholipids  ในซีรั่ม / พลาสม่า','Phospholipids, serum/plasma','1','0545402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0545402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา Phytanate  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Phytanate, serum/plasma' where lab_icd10tm = '0545602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000501','','การตรวจหา Phytanate  ในซีรั่ม / พลาสม่า','Phytanate, serum/plasma','1','0545602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0545602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา propionate  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Propionate, serum/plasma' where lab_icd10tm = '0545802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000502','','การตรวจหา propionate  ในซีรั่ม / พลาสม่า','Propionate, serum/plasma','1','0545802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0545802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา propionate  ในปัสสาวะ ',lab_ncd_note = 'Propionate, urine' where lab_icd10tm = '0545803';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000503','','การตรวจหา propionate  ในปัสสาวะ ','Propionate, urine','1','0545803'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0545803');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา prostaglandins  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Prostaglandins, serum/plasma' where lab_icd10tm = '0546002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000504','','การตรวจหา prostaglandins  ในซีรั่ม / พลาสม่า','Prostaglandins, serum/plasma','1','0546002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0546002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา prostaglandins  ในของเหลวอื่น ๆ มิได้จำแนก',lab_ncd_note = 'Prostaglandins, other fluid NEC' where lab_icd10tm = '0546015';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000505','','การตรวจหา prostaglandins  ในของเหลวอื่น ๆ มิได้จำแนก','Prostaglandins, other fluid NEC','1','0546015'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0546015');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ prostaglandins phenotype  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Prostaglandins phenotype, serum/plasma' where lab_icd10tm = '0546202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000506','','การตรวจ prostaglandins phenotype  ในซีรั่ม / พลาสม่า','Prostaglandins phenotype, serum/plasma','1','0546202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0546202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา triglycerides  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Triglycerides, serum/plasma' where lab_icd10tm = '0546602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000507','','การตรวจหา triglycerides  ในซีรั่ม / พลาสม่า','Triglycerides, serum/plasma','1','0546602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0546602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา triglycerides  ในของเหลวอื่น ๆ  มิได้จำแนก',lab_ncd_note = 'Triglycerides, other fluid NEC' where lab_icd10tm = '0546615';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000508','','การตรวจหา triglycerides  ในของเหลวอื่น ๆ  มิได้จำแนก','Triglycerides, other fluid NEC','1','0546615'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0546615');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'Liporportein (a), plasma ' where lab_icd10tm = '0546616';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000509','','-','Liporportein (a), plasma ','1','0546616'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0546616');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา Amino levulinic acid  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Amino levulinic acid, serum/plasma' where lab_icd10tm = '0550202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000510','','การตรวจหา Amino levulinic acid  ในซีรั่ม / พลาสม่า','Amino levulinic acid, serum/plasma','1','0550202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0550202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา Amino levulinic acid  ในปัสสาวะ',lab_ncd_note = 'Amino levulinic acid, urine' where lab_icd10tm = '0550203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000511','','การตรวจหา Amino levulinic acid  ในปัสสาวะ','Amino levulinic acid, urine','1','0550203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0550203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา Bilirubin, conjugated  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Bilirubin, conjugated, serum/plasma' where lab_icd10tm = '0550402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000512','','การตรวจหา Bilirubin, conjugated  ในซีรั่ม / พลาสม่า','Bilirubin, conjugated, serum/plasma','1','0550402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0550402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาบิลิรูบินทั้งหมด ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Bilirubin, total, serum/plasma' where lab_icd10tm = '0550602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000513','','การตรวจหาบิลิรูบินทั้งหมด ในซีรั่ม / พลาสม่า','Bilirubin, total, serum/plasma','1','0550602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0550602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาบิลิรูบินทั้งหมดในปัสสาวะ ',lab_ncd_note = 'Bilirubin, total, urine' where lab_icd10tm = '0550603';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000514','','การตรวจหาบิลิรูบินทั้งหมดในปัสสาวะ ','Bilirubin, total, urine','1','0550603'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0550603');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาบิลิรูบินทั้งหมดในของเหลวอื่น ๆ มิได้จำแนก',lab_ncd_note = 'Bilirubin, total, other fluid NEC' where lab_icd10tm = '0550615';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000515','','การตรวจหาบิลิรูบินทั้งหมดในของเหลวอื่น ๆ มิได้จำแนก','Bilirubin, total, other fluid NEC','1','0550615'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0550615');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา methemalbumin ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Methemalbumin, serum/plasma' where lab_icd10tm = '0550802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000516','','การตรวจหา methemalbumin ในซีรั่ม / พลาสม่า','Methemalbumin, serum/plasma','1','0550802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0550802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรอง methemalbubin  ในเลือด',lab_ncd_note = 'Methemalbubin, screen, whole blood' where lab_icd10tm = '0551001';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000517','','การตรวจคัดกรอง methemalbubin  ในเลือด','Methemalbubin, screen, whole blood','1','0551001'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0551001');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา methemalbubin  ในเลือด ',lab_ncd_note = 'Methemalbubin, whole blood' where lab_icd10tm = '0551201';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000518','','การตรวจหา methemalbubin  ในเลือด ','Methemalbubin, whole blood','1','0551201'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0551201');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การคัดกรอง myoglobin  ในปัสสาวะ',lab_ncd_note = 'Myoglobin screen, urine' where lab_icd10tm = '0551403';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000519','','การคัดกรอง myoglobin  ในปัสสาวะ','Myoglobin screen, urine','1','0551403'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0551403');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา myoglobin  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Myoglobin, serum/plasma ' where lab_icd10tm = '0551602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000520','','การตรวจหา myoglobin  ในซีรั่ม / พลาสม่า','Myoglobin, serum/plasma ','1','0551602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0551602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา myoglobin  ในปัสสาวะ',lab_ncd_note = 'Myoglobin, urine ' where lab_icd10tm = '0551603';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000521','','การตรวจหา myoglobin  ในปัสสาวะ','Myoglobin, urine ','1','0551603'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0551603');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา porphobilin  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Porphobilin, serum/plasma' where lab_icd10tm = '0551802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000522','','การตรวจหา porphobilin  ในซีรั่ม / พลาสม่า','Porphobilin, serum/plasma','1','0551802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0551802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การคัดกรอง porphobilinogen ในปัสสาวะ',lab_ncd_note = 'Porphobilinogen, screen, urine' where lab_icd10tm = '0552003';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000523','','การคัดกรอง porphobilinogen ในปัสสาวะ','Porphobilinogen, screen, urine','1','0552003'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0552003');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา porphobilinogen ในปัสสาวะ',lab_ncd_note = 'Porphobilinogen, urine' where lab_icd10tm = '0552203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000524','','การตรวจหา porphobilinogen ในปัสสาวะ','Porphobilinogen, urine','1','0552203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0552203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรอง porphyrins ในปัสสาวะ',lab_ncd_note = 'Porphyrins screen, urine' where lab_icd10tm = '0552403';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000525','','การตรวจคัดกรอง porphyrins ในปัสสาวะ','Porphyrins screen, urine','1','0552403'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0552403');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรอง porphyrins ในอุจจาระ',lab_ncd_note = 'Porphyrins screen, stool' where lab_icd10tm = '0552466';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000526','','การตรวจคัดกรอง porphyrins ในอุจจาระ','Porphyrins screen, stool','1','0552466'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0552466');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาเศษ porphyrins ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Porphyrins, fractionated, serum/plasma' where lab_icd10tm = '0552602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000527','','การตรวจหาเศษ porphyrins ในซีรั่ม / พลาสม่า','Porphyrins, fractionated, serum/plasma','1','0552602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0552602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาเศษ porphyrins ในปัสสาวะ',lab_ncd_note = 'Porphyrins, fractionated, urine' where lab_icd10tm = '0552603';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000528','','การตรวจหาเศษ porphyrins ในปัสสาวะ','Porphyrins, fractionated, urine','1','0552603'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0552603');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาเศษ porphyrins ในเม็ดเลือดแดง',lab_ncd_note = 'Porphyrins, fractionated, RBC' where lab_icd10tm = '0552617';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000529','','การตรวจหาเศษ porphyrins ในเม็ดเลือดแดง','Porphyrins, fractionated, RBC','1','0552617'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0552617');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาเศษ porphyrins  ในอุจจาระ',lab_ncd_note = 'Porphyrins, fractionated, stool' where lab_icd10tm = '0552666';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000530','','การตรวจหาเศษ porphyrins  ในอุจจาระ','Porphyrins, fractionated, stool','1','0552666'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0552666');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา porphyrins ทั้งหมด  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Porphyrins, total, serum/plasma' where lab_icd10tm = '0552802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000531','','การตรวจหา porphyrins ทั้งหมด  ในซีรั่ม / พลาสม่า','Porphyrins, total, serum/plasma','1','0552802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0552802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา porphyrins ทั้งหมด  ในปัสสาวะ',lab_ncd_note = 'Porphyrins, total, urine' where lab_icd10tm = '0552803';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000532','','การตรวจหา porphyrins ทั้งหมด  ในปัสสาวะ','Porphyrins, total, urine','1','0552803'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0552803');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา porphyrins ทั้งหมด  ในเม็ดเลือดแดง',lab_ncd_note = 'Porphyrins, total, RBC' where lab_icd10tm = '0552817';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000533','','การตรวจหา porphyrins ทั้งหมด  ในเม็ดเลือดแดง','Porphyrins, total, RBC','1','0552817'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0552817');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา porphyrins ทั้งหมด  ในอุจจาระ',lab_ncd_note = 'Porphyrins, total, stool' where lab_icd10tm = '0552866';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000534','','การตรวจหา porphyrins ทั้งหมด  ในอุจจาระ','Porphyrins, total, stool','1','0552866'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0552866');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา urobilin  ในปัสสาวะ ',lab_ncd_note = 'Urobilin, urine' where lab_icd10tm = '0553203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000535','','การตรวจหา urobilin  ในปัสสาวะ ','Urobilin, urine','1','0553203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0553203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา urobilin  ในอุจจาระ',lab_ncd_note = 'Urobilin, stool' where lab_icd10tm = '0553266';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000536','','การตรวจหา urobilin  ในอุจจาระ','Urobilin, stool','1','0553266'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0553266');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา urobilinogen  ในปัสสาวะ',lab_ncd_note = 'Urobilinogen, urine' where lab_icd10tm = '0553403';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000537','','การตรวจหา urobilinogen  ในปัสสาวะ','Urobilinogen, urine','1','0553403'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0553403');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา urobilinogen  ในอุจจาระ',lab_ncd_note = 'Urobilinogen, stool' where lab_icd10tm = '0553466';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000538','','การตรวจหา urobilinogen  ในอุจจาระ','Urobilinogen, stool','1','0553466'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0553466');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา zinc protoporphyrins  ในเม็ดเลือดแดง',lab_ncd_note = 'Zinc protoporphyrins, RBC' where lab_icd10tm = '0554017';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000539','','การตรวจหา zinc protoporphyrins  ในเม็ดเลือดแดง','Zinc protoporphyrins, RBC','1','0554017'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0554017');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การใช้ชุดทดสอบแบบรวดเร็ว  ทดสอบหลายครั้ง  ในปัสสาวะ ',lab_ncd_note = 'Rapid screening test, multiple tests, urine' where lab_icd10tm = '0560203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000540','','การใช้ชุดทดสอบแบบรวดเร็ว  ทดสอบหลายครั้ง  ในปัสสาวะ ','Rapid screening test, multiple tests, urine','1','0560203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0560203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การใช้ชุดทดสอบแบบรวดเร็ว  ทดสอบหลายครั้ง  ในของเหลวอื่นๆ  มิได้จำแนก',lab_ncd_note = 'Rapid screening test, multiple tests, other fluid NEC' where lab_icd10tm = '0560215';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000541','','การใช้ชุดทดสอบแบบรวดเร็ว  ทดสอบหลายครั้ง  ในของเหลวอื่นๆ  มิได้จำแนก','Rapid screening test, multiple tests, other fluid NEC','1','0560215'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0560215');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การใช้ชุดทดสอบแบบรวดเร็ว  ทดสอบครั้งเดียว  ในปัสสาวะ',lab_ncd_note = 'Rapid screening test, single test, urine' where lab_icd10tm = '0560403';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000542','','การใช้ชุดทดสอบแบบรวดเร็ว  ทดสอบครั้งเดียว  ในปัสสาวะ','Rapid screening test, single test, urine','1','0560403'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0560403');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การใช้ชุดทดสอบแบบรวดเร็ว  ทดสอบครั้งเดียว  ในของเหลวอื่นๆ  มิได้จำแนก',lab_ncd_note = 'Rapid screening test, single test, other fluid NEC' where lab_icd10tm = '0560415';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000543','','การใช้ชุดทดสอบแบบรวดเร็ว  ทดสอบครั้งเดียว  ในของเหลวอื่นๆ  มิได้จำแนก','Rapid screening test, single test, other fluid NEC','1','0560415'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0560415');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาความถ่วงจำเพาะในปัสสาวะ ',lab_ncd_note = 'Specific gravity, urine' where lab_icd10tm = '0560603';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000544','','การตรวจหาความถ่วงจำเพาะในปัสสาวะ ','Specific gravity, urine','1','0560603'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0560603');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาความถ่วงจำเพาะในของเหลวอื่นๆ  มิได้จำแนก',lab_ncd_note = 'Specific gravity, other fluid NEC' where lab_icd10tm = '0560615';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000545','','การตรวจหาความถ่วงจำเพาะในของเหลวอื่นๆ  มิได้จำแนก','Specific gravity, other fluid NEC','1','0560615'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0560615');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจด้วยกล้องจุลทรรศน์ในปัสสาวะ ',lab_ncd_note = 'Microscopic examination, urine' where lab_icd10tm = '0561403';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000546','','การตรวจด้วยกล้องจุลทรรศน์ในปัสสาวะ ','Microscopic examination, urine','1','0561403'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0561403');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจด้วยกล้องจุลทรรศน์ในของเหลวจากข้อกระดูก',lab_ncd_note = 'Microscopic examination, synovial fluid' where lab_icd10tm = '0561410';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000547','','การตรวจด้วยกล้องจุลทรรศน์ในของเหลวจากข้อกระดูก','Microscopic examination, synovial fluid','1','0561410'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0561410');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจด้วยกล้องจุลทรรศน์ในของเหลวอื่นๆ  มิได้จำแนก',lab_ncd_note = 'Microscopic examination, other fluid NEC' where lab_icd10tm = '0561415';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000548','','การตรวจด้วยกล้องจุลทรรศน์ในของเหลวอื่นๆ  มิได้จำแนก','Microscopic examination, other fluid NEC','1','0561415'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0561415');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'Stool examination for parasite' where lab_icd10tm = '0561457';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000549','','-','Stool examination for parasite','1','0561457'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0561457');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'Stool examination for occult blood' where lab_icd10tm = '0561458';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000550','','-','Stool examination for occult blood','1','0561458'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0561458');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'Stool fat' where lab_icd10tm = '0561459';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000551','','-','Stool fat','1','0561459'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0561459');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'Microscopic examination, stool' where lab_icd10tm = '0561466';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000552','','-','Microscopic examination, stool','1','0561466'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0561466');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา adenine  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Adenine, serum/plasma' where lab_icd10tm = '0570202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000553','','การตรวจหา adenine  ในซีรั่ม / พลาสม่า','Adenine, serum/plasma','1','0570202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0570202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา ascorbic acid  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Ascorbic acid, serum/plasma (vitamin C)' where lab_icd10tm = '0570402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000554','','การตรวจหา ascorbic acid  ในซีรั่ม / พลาสม่า','Ascorbic acid, serum/plasma (vitamin C)','1','0570402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0570402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา ascorbic acid  ในปัสสาวะ',lab_ncd_note = 'Ascorbic acid, urine' where lab_icd10tm = '0570403';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000555','','การตรวจหา ascorbic acid  ในปัสสาวะ','Ascorbic acid, urine','1','0570403'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0570403');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา biotin  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Biotin, serum/plasma' where lab_icd10tm = '0570602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000556','','การตรวจหา biotin  ในซีรั่ม / พลาสม่า','Biotin, serum/plasma','1','0570602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0570602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา calcidiol  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Calcidiol, serum/plasma' where lab_icd10tm = '0570802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000557','','การตรวจหา calcidiol  ในซีรั่ม / พลาสม่า','Calcidiol, serum/plasma','1','0570802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0570802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา calciferol  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Calciferol, serum/plasma (vitamin D)' where lab_icd10tm = '0571002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000558','','การตรวจหา calciferol  ในซีรั่ม / พลาสม่า','Calciferol, serum/plasma (vitamin D)','1','0571002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0571002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา calcitriol (1,25)  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Calcitriol (1,25), serum/plasma (active form vitamin D)' where lab_icd10tm = '0571202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000559','','การตรวจหา calcitriol (1,25)  ในซีรั่ม / พลาสม่า','Calcitriol (1,25), serum/plasma (active form vitamin D)','1','0571202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0571202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา cobalamin  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Cobalamin, serum/plasma (vitamin B12)' where lab_icd10tm = '0571402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000560','','การตรวจหา cobalamin  ในซีรั่ม / พลาสม่า','Cobalamin, serum/plasma (vitamin B12)','1','0571402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0571402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา ergocalciferol  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Ergocalciferol, serum/plasma ' where lab_icd10tm = '0571602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000561','','การตรวจหา ergocalciferol  ในซีรั่ม / พลาสม่า','Ergocalciferol, serum/plasma ','1','0571602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0571602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา folate  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Folate, serum/plasma' where lab_icd10tm = '0572002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000562','','การตรวจหา folate  ในซีรั่ม / พลาสม่า','Folate, serum/plasma','1','0572002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0572002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา folate  ในเม็ดเลือดแดง',lab_ncd_note = 'Folate, RBC' where lab_icd10tm = '0572017';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000563','','การตรวจหา folate  ในเม็ดเลือดแดง','Folate, RBC','1','0572017'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0572017');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา niacin  ในปัสสาวะ',lab_ncd_note = 'Niacin, urine' where lab_icd10tm = '0572203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000564','','การตรวจหา niacin  ในปัสสาวะ','Niacin, urine','1','0572203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0572203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา pantothenate  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Pantothenate, serum/plasma' where lab_icd10tm = '0572402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000565','','การตรวจหา pantothenate  ในซีรั่ม / พลาสม่า','Pantothenate, serum/plasma','1','0572402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0572402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา pyridoxine  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Pyridoxine, serum/plasma (vitamin B6)' where lab_icd10tm = '0572602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000566','','การตรวจหา pyridoxine  ในซีรั่ม / พลาสม่า','Pyridoxine, serum/plasma (vitamin B6)','1','0572602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0572602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา quinone  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Quinone, serum/plasm' where lab_icd10tm = '0573002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000567','','การตรวจหา quinone  ในซีรั่ม / พลาสม่า','Quinone, serum/plasm','1','0573002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0573002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา retinol  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Retinol, serum/plasma (vitamin A)' where lab_icd10tm = '0573202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000568','','การตรวจหา retinol  ในซีรั่ม / พลาสม่า','Retinol, serum/plasma (vitamin A)','1','0573202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0573202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา riboflavin  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Riboflavin, serum/plasma (vitamin B2)' where lab_icd10tm = '0573402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000569','','การตรวจหา riboflavin  ในซีรั่ม / พลาสม่า','Riboflavin, serum/plasma (vitamin B2)','1','0573402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0573402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา thiamin  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Thiamin, serum/plasma (vitamin B1)' where lab_icd10tm = '0573602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000570','','การตรวจหา thiamin  ในซีรั่ม / พลาสม่า','Thiamin, serum/plasma (vitamin B1)','1','0573602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0573602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา thiamin  ในเลือด',lab_ncd_note = 'Thiamin, whole blood' where lab_icd10tm = '0573801';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000571','','การตรวจหา thiamin  ในเลือด','Thiamin, whole blood','1','0573801'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0573801');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา tocopherol  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Tocopherol, serum/plasma (vitamin E)' where lab_icd10tm = '0574002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000572','','การตรวจหา tocopherol  ในซีรั่ม / พลาสม่า','Tocopherol, serum/plasma (vitamin E)','1','0574002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0574002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา acetylcholine  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Acetylcholine, serum/plasma' where lab_icd10tm = '0580102';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000573','','การตรวจหา acetylcholine  ในซีรั่ม / พลาสม่า','Acetylcholine, serum/plasma','1','0580102'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0580102');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวัดปริมาณกรดที่ไตเตรทได้  ในของเหลว / อาหารจากกระเพาะอาหาร',lab_ncd_note = 'Acidity (titratable), gastric fluid/content' where lab_icd10tm = '0580206';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000574','','การวัดปริมาณกรดที่ไตเตรทได้  ในของเหลว / อาหารจากกระเพาะอาหาร','Acidity (titratable), gastric fluid/content','1','0580206'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0580206');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา adenosine diphosphate  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Adenosine diphosphate, serum/plasma' where lab_icd10tm = '0580302';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000575','','การตรวจหา adenosine diphosphate  ในซีรั่ม / พลาสม่า','Adenosine diphosphate, serum/plasma','1','0580302'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0580302');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา adenosine triphosphate  ในเลือด',lab_ncd_note = 'Adenosine triphosphate, whole blood' where lab_icd10tm = '0580401';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000576','','การตรวจหา adenosine triphosphate  ในเลือด','Adenosine triphosphate, whole blood','1','0580401'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0580401');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาด่าง  ในของเหลวอื่น ๆ  มิได้จำแนก',lab_ncd_note = 'Alkalinity, other fluid NEC' where lab_icd10tm = '0580515';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000577','','การตรวจหาด่าง  ในของเหลวอื่น ๆ  มิได้จำแนก','Alkalinity, other fluid NEC','1','0580515'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0580515');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาแอมโมเนีย  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Ammonia, serum/plasma' where lab_icd10tm = '0580602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000578','','การตรวจหาแอมโมเนีย  ในซีรั่ม / พลาสม่า','Ammonia, serum/plasma','1','0580602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0580602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา amniotic fluid scan (OD rise)  ในน้ำคร่ำ',lab_ncd_note = 'Amniotic fluid scan (OD rise), amniotic fluid ' where lab_icd10tm = '0580705';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000579','','การตรวจหา amniotic fluid scan (OD rise)  ในน้ำคร่ำ','Amniotic fluid scan (OD rise), amniotic fluid ','1','0580705'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0580705');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา beta-hydroxybutyrate  ในเลือด ',lab_ncd_note = 'Beta-hydroxybutyrate, whole blood' where lab_icd10tm = '0580801';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000580','','การตรวจหา beta-hydroxybutyrate  ในเลือด ','Beta-hydroxybutyrate, whole blood','1','0580801'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0580801');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา beta-hydroxybutyrate ในปัสสาวะ ',lab_ncd_note = 'Beta-hydroxybutyrate, urine' where lab_icd10tm = '0580803';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000581','','การตรวจหา beta-hydroxybutyrate ในปัสสาวะ ','Beta-hydroxybutyrate, urine','1','0580803'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0580803');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา beta-hydroxybutyrate ในของเหลวอื่น ๆ มิได้จำแนก',lab_ncd_note = 'Beta-hydroxybutyrate, other fluid NEC' where lab_icd10tm = '0580815';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000582','','การตรวจหา beta-hydroxybutyrate ในของเหลวอื่น ๆ มิได้จำแนก','Beta-hydroxybutyrate, other fluid NEC','1','0580815'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0580815');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา bile acids/salts ในซีรั่ม, พลาสม่า',lab_ncd_note = 'Bile acids/salts, serum/plasma' where lab_icd10tm = '0580902';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000583','','การตรวจหา bile acids/salts ในซีรั่ม, พลาสม่า','Bile acids/salts, serum/plasma','1','0580902'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0580902');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา bile acids/salts ในปัสสาวะ',lab_ncd_note = 'Bile acids/salts, urine' where lab_icd10tm = '0580903';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000584','','การตรวจหา bile acids/salts ในปัสสาวะ','Bile acids/salts, urine','1','0580903'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0580903');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา bile acids/salts ในของเหลว อื่น ๆ มิได้จำแนก ',lab_ncd_note = 'Bile acids/salts, other fluid NEC' where lab_icd10tm = '0580915';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000585','','การตรวจหา bile acids/salts ในของเหลว อื่น ๆ มิได้จำแนก ','Bile acids/salts, other fluid NEC','1','0580915'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0580915');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา bile acids/salts ในอุจจาระ',lab_ncd_note = 'Bile acids/salts, stool' where lab_icd10tm = '0580966';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000586','','การตรวจหา bile acids/salts ในอุจจาระ','Bile acids/salts, stool','1','0580966'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0580966');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์การตรวจ แคลคูลัสทดสอบเดียวแคลคูลัส ',lab_ncd_note = 'Calculus analysis, single test, calculus' where lab_icd10tm = '0581067';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000587','','การวิเคราะห์การตรวจ แคลคูลัสทดสอบเดียวแคลคูลัส ','Calculus analysis, single test, calculus','1','0581067'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0581067');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ การวิเคราะห์ทดสอบแคลคูลัสหลายแคลคูลัส ',lab_ncd_note = 'Calculus analysis, multiple tests, calculus' where lab_icd10tm = '0581167';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000588','','การตรวจ การวิเคราะห์ทดสอบแคลคูลัสหลายแคลคูลัส ','Calculus analysis, multiple tests, calculus','1','0581167'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0581167');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาคาร์บอเนตในปัสสาวะ',lab_ncd_note = 'Carbonate, urine' where lab_icd10tm = '0581203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000589','','การตรวจหาคาร์บอเนตในปัสสาวะ','Carbonate, urine','1','0581203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0581203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาคาร์บอเนตในของเหลว อื่น ๆ มิได้จำแนก',lab_ncd_note = 'Carbonate, other fluid NEC' where lab_icd10tm = '0581215';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000590','','การตรวจหาคาร์บอเนตในของเหลว อื่น ๆ มิได้จำแนก','Carbonate, other fluid NEC','1','0581215'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0581215');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา carboxyhemoglobin ในเลือด',lab_ncd_note = 'Carboxyhemoglobin, whole blood' where lab_icd10tm = '0581301';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000591','','การตรวจหา carboxyhemoglobin ในเลือด','Carboxyhemoglobin, whole blood','1','0581301'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0581301');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา carcionembryonic antigen  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Carcionembryonic antigen, serum/plasma' where lab_icd10tm = '0581402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000592','','การตรวจหา carcionembryonic antigen  ในซีรั่ม / พลาสม่า','Carcionembryonic antigen, serum/plasma','1','0581402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0581402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา carcionembryonic antigen ในน้ำไขสันหลัง',lab_ncd_note = 'Carcionembryonic antigen, CSF' where lab_icd10tm = '0581404';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000593','','การตรวจหา carcionembryonic antigen ในน้ำไขสันหลัง','Carcionembryonic antigen, CSF','1','0581404'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0581404');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา carcionembryonic antigen  ในเนื้อเยื่ออื่นๆ  มิได้จำแนก',lab_ncd_note = 'Carcionembryonic antigen, tissue NEC' where lab_icd10tm = '0581450';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000594','','การตรวจหา carcionembryonic antigen  ในเนื้อเยื่ออื่นๆ  มิได้จำแนก','Carcionembryonic antigen, tissue NEC','1','0581450'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0581450');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา carnitine  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Carnitine, serum/plasma' where lab_icd10tm = '0581502';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000595','','การตรวจหา carnitine  ในซีรั่ม / พลาสม่า','Carnitine, serum/plasma','1','0581502'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0581502');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา carnitine ในปัสสาวะ',lab_ncd_note = 'Carnitine, urine' where lab_icd10tm = '0581503';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000596','','การตรวจหา carnitine ในปัสสาวะ','Carnitine, urine','1','0581503'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0581503');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาแคโรทีน  ในปัสสาวะ',lab_ncd_note = 'Carotene, urine' where lab_icd10tm = '0581601';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000597','','การตรวจหาแคโรทีน  ในปัสสาวะ','Carotene, urine','1','0581601'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0581601');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาแคโรทีน  ในเลือด',lab_ncd_note = 'Carotene, whole blood' where lab_icd10tm = '0581602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000598','','การตรวจหาแคโรทีน  ในเลือด','Carotene, whole blood','1','0581602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0581602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาแคโรทีน  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Carotene, serum/plasma' where lab_icd10tm = '0581702';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000599','','การตรวจหาแคโรทีน  ในซีรั่ม / พลาสม่า','Carotene, serum/plasma','1','0581702'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0581702');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา citrate  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Citrate, serum/plasma' where lab_icd10tm = '0581703';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000600','','การตรวจหา citrate  ในซีรั่ม / พลาสม่า','Citrate, serum/plasma','1','0581703'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0581703');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา citrate  ในปัสสาวะ',lab_ncd_note = 'Citrate, urine' where lab_icd10tm = '0581802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000601','','การตรวจหา citrate  ในปัสสาวะ','Citrate, urine','1','0581802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0581802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา creatine  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Creatine, serum/plasma' where lab_icd10tm = '0581803';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000602','','การตรวจหา creatine  ในซีรั่ม / พลาสม่า','Creatine, serum/plasma','1','0581803'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0581803');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา creatine  ในเม็ดเลือดแดง',lab_ncd_note = 'Creatine, RBC' where lab_icd10tm = '0581817';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000603','','การตรวจหา creatine  ในเม็ดเลือดแดง','Creatine, RBC','1','0581817'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0581817');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา creatinine  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Creatinine, serum/plasma' where lab_icd10tm = '0581902';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000604','','การตรวจหา creatinine  ในซีรั่ม / พลาสม่า','Creatinine, serum/plasma','1','0581902'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0581902');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา creatinine  ในปัสสาวะ',lab_ncd_note = 'Creatinine, urine' where lab_icd10tm = '0581903';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000605','','การตรวจหา creatinine  ในปัสสาวะ','Creatinine, urine','1','0581903'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0581903');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาค่า eGFR (ใช้สูตร CKD-EPI  formula)',lab_ncd_note = 'eGFR (CKD-EPI  formula)' where lab_icd10tm = '0581904';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000606','','การตรวจหาค่า eGFR (ใช้สูตร CKD-EPI  formula)','eGFR (CKD-EPI  formula)','1','0581904'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0581904');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา creatinine  ในของเหลวอื่นๆ  มิได้จำแนก',lab_ncd_note = 'Creatinine, other fluid NEC' where lab_icd10tm = '0581915';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000607','','การตรวจหา creatinine  ในของเหลวอื่นๆ  มิได้จำแนก','Creatinine, other fluid NEC','1','0581915'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0581915');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา cryofibrinogen ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Cryofibrinogen, serum/plasma' where lab_icd10tm = '0582002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000608','','การตรวจหา cryofibrinogen ในซีรั่ม / พลาสม่า','Cryofibrinogen, serum/plasma','1','0582002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0582002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา cryoglobulin ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Cryoglobulin, serum/plasma' where lab_icd10tm = '0582102';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000609','','การตรวจหา cryoglobulin ในซีรั่ม / พลาสม่า','Cryoglobulin, serum/plasma','1','0582102'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0582102');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา cyclic adenosine monophosphate  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Cyclic adenosine monophosphate, serum/plasma' where lab_icd10tm = '0582202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000610','','การตรวจหา cyclic adenosine monophosphate  ในซีรั่ม / พลาสม่า','Cyclic adenosine monophosphate, serum/plasma','1','0582202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0582202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา cyclic adenosine monophosphate  ในปัสสาวะ',lab_ncd_note = 'Cyclic adenosine monophosphate, urine' where lab_icd10tm = '0582203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000611','','การตรวจหา cyclic adenosine monophosphate  ในปัสสาวะ','Cyclic adenosine monophosphate, urine','1','0582203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0582203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา fibrinopeptide ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Fibrinopeptide, serum/plasma' where lab_icd10tm = '0582302';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000612','','การตรวจหา fibrinopeptide ในซีรั่ม / พลาสม่า','Fibrinopeptide, serum/plasma','1','0582302'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0582302');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา fibrinopeptide  ในของเหลวอื่น ๆ  มิได้จำแนก',lab_ncd_note = 'Fibrinopeptide, other fluid NEC' where lab_icd10tm = '0582315';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000613','','การตรวจหา fibrinopeptide  ในของเหลวอื่น ๆ  มิได้จำแนก','Fibrinopeptide, other fluid NEC','1','0582315'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0582315');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา fibronectin ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Fibronectin, serum/plasma' where lab_icd10tm = '0582402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000614','','การตรวจหา fibronectin ในซีรั่ม / พลาสม่า','Fibronectin, serum/plasma','1','0582402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0582402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา hippurate ในปัสสาวะ ',lab_ncd_note = 'Hippurate, urine' where lab_icd10tm = '0582503';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000615','','การตรวจหา hippurate ในปัสสาวะ ','Hippurate, urine','1','0582503'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0582503');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาไฮโดรเจนในลมหายใจ ',lab_ncd_note = 'Hydrogen, breath' where lab_icd10tm = '0582668';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000616','','การตรวจหาไฮโดรเจนในลมหายใจ ','Hydrogen, breath','1','0582668'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0582668');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา Iron binding capacity (ทั้งหมดหรือไม่อิ่มตัว) ในซีรั่ม, พลาสม่า',lab_ncd_note = 'Iron binding capacity (total or unsaturated), serum/plasma' where lab_icd10tm = '0582702';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000617','','การตรวจหา Iron binding capacity (ทั้งหมดหรือไม่อิ่มตัว) ในซีรั่ม, พลาสม่า','Iron binding capacity (total or unsaturated), serum/plasma','1','0582702'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0582702');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาอัตราส่วน lecithin / spingomyelin ในน้ำคร่ำ',lab_ncd_note = 'Lecithin/spingomyelin ratio, amniotic fluid' where lab_icd10tm = '0582805';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000618','','การตรวจหาอัตราส่วน lecithin / spingomyelin ในน้ำคร่ำ','Lecithin/spingomyelin ratio, amniotic fluid','1','0582805'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0582805');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา malonate ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Malonate, serum/plasma' where lab_icd10tm = '0582902';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000619','','การตรวจหา malonate ในซีรั่ม / พลาสม่า','Malonate, serum/plasma','1','0582902'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0582902');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาไนโตรเจนในเลือด ',lab_ncd_note = 'Nitrogen, whole blood' where lab_icd10tm = '0583001';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000620','','การตรวจหาไนโตรเจนในเลือด ','Nitrogen, whole blood','1','0583001'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0583001');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาไนโตรเจนในซีรั่ม / พลาสม่า',lab_ncd_note = 'Nitrogen, serum/plasma' where lab_icd10tm = '0583002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000621','','การตรวจหาไนโตรเจนในซีรั่ม / พลาสม่า','Nitrogen, serum/plasma','1','0583002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0583002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาไนโตรเจนในปัสสาวะ',lab_ncd_note = 'Nitrogen, urine' where lab_icd10tm = '0583003';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000622','','การตรวจหาไนโตรเจนในปัสสาวะ','Nitrogen, urine','1','0583003'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0583003');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาไนโตรเจนในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Nitrogen, other fluid NEC' where lab_icd10tm = '0583015';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000623','','การตรวจหาไนโตรเจนในของเหลวอื่นๆ มิได้จำแนก','Nitrogen, other fluid NEC','1','0583015'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0583015');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา occult blood ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Occult blood, other fluid NEC' where lab_icd10tm = '0583115';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000624','','การตรวจหา occult blood ในของเหลวอื่นๆ มิได้จำแนก','Occult blood, other fluid NEC','1','0583115'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0583115');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา occult blood ในอุจจาระ',lab_ncd_note = 'Occult blood,  stool' where lab_icd10tm = '0583166';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000625','','การตรวจหา occult blood ในอุจจาระ','Occult blood,  stool','1','0583166'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0583166');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา oncotic pressure  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Oncotic pressure, serum/plasma' where lab_icd10tm = '0583202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000626','','การตรวจหา oncotic pressure  ในซีรั่ม / พลาสม่า','Oncotic pressure, serum/plasma','1','0583202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0583202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา organic acids  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Organic acids, serum/plasma' where lab_icd10tm = '0583302';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000627','','การตรวจหา organic acids  ในซีรั่ม / พลาสม่า','Organic acids, serum/plasma','1','0583302'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0583302');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา organic acids  ในปัสสาวะ',lab_ncd_note = 'Organic acids, urine' where lab_icd10tm = '0583303';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000628','','การตรวจหา organic acids  ในปัสสาวะ','Organic acids, urine','1','0583303'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0583303');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา orotic acid  ในปัสสาวะ ',lab_ncd_note = 'Orotic acid, urine' where lab_icd10tm = '0583403';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000629','','การตรวจหา orotic acid  ในปัสสาวะ ','Orotic acid, urine','1','0583403'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0583403');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา osmolality  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Osmolality, serum/plasma' where lab_icd10tm = '0583502';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000630','','การตรวจหา osmolality  ในซีรั่ม / พลาสม่า','Osmolality, serum/plasma','1','0583502'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0583502');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา osmolality  ในปัสสาวะ',lab_ncd_note = 'Osmolality, urine' where lab_icd10tm = '0583503';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000631','','การตรวจหา osmolality  ในปัสสาวะ','Osmolality, urine','1','0583503'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0583503');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา osmolality  ในของเหลวอื่นๆ  มิได้จำแนก',lab_ncd_note = 'Osmolality, other fluid NEC' where lab_icd10tm = '0583515';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000632','','การตรวจหา osmolality  ในของเหลวอื่นๆ  มิได้จำแนก','Osmolality, other fluid NEC','1','0583515'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0583515');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา oximetry ในเลือด',lab_ncd_note = 'Oximetry, whole blood' where lab_icd10tm = '0583601';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000633','','การตรวจหา oximetry ในเลือด','Oximetry, whole blood','1','0583601'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0583601');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา pH ในเลือด ',lab_ncd_note = 'pH, whole blood' where lab_icd10tm = '0583701';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000634','','การตรวจหา pH ในเลือด ','pH, whole blood','1','0583701'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0583701');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา pH ในซีรั่ม / พลาสม่า',lab_ncd_note = 'pH, serum/plasma' where lab_icd10tm = '0583702';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000635','','การตรวจหา pH ในซีรั่ม / พลาสม่า','pH, serum/plasma','1','0583702'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0583702');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา pH ในของเหลว / อาหารจากกระเพาะอาหาร',lab_ncd_note = 'pH, gastric fluid/content' where lab_icd10tm = '0583706';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000636','','การตรวจหา pH ในของเหลว / อาหารจากกระเพาะอาหาร','pH, gastric fluid/content','1','0583706'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0583706');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา pH ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'pH, other fluid NEC' where lab_icd10tm = '0583715';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000637','','การตรวจหา pH ในของเหลวอื่นๆ มิได้จำแนก','pH, other fluid NEC','1','0583715'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0583715');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา pH ในอุจจาระ ',lab_ncd_note = 'pH, stool' where lab_icd10tm = '0583766';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000638','','การตรวจหา pH ในอุจจาระ ','pH, stool','1','0583766'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0583766');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา pCO2 ในเลือด',lab_ncd_note = 'pCO2, whole blood' where lab_icd10tm = '0583801';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000639','','การตรวจหา pCO2 ในเลือด','pCO2, whole blood','1','0583801'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0583801');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา pO2  ในเลือด ',lab_ncd_note = 'pO2, whole blood' where lab_icd10tm = '0583901';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000640','','การตรวจหา pO2  ในเลือด ','pO2, whole blood','1','0583901'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0583901');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา phenolphthalein  ในเลือด ',lab_ncd_note = 'Phenolphthalein, whole blood' where lab_icd10tm = '0584001';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000641','','การตรวจหา phenolphthalein  ในเลือด ','Phenolphthalein, whole blood','1','0584001'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0584001');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา phenolphthalein  ในปัสสาวะ',lab_ncd_note = 'Phenolphthalein,  urine' where lab_icd10tm = '0584003';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000642','','การตรวจหา phenolphthalein  ในปัสสาวะ','Phenolphthalein,  urine','1','0584003'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0584003');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา phenolphthalein  ในอุจจาระ',lab_ncd_note = 'Phenolphthalein, stool' where lab_icd10tm = '0584066';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000643','','การตรวจหา phenolphthalein  ในอุจจาระ','Phenolphthalein, stool','1','0584066'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0584066');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา phosphatidyl glycerol ในน้ำคร่ำ ',lab_ncd_note = 'Phosphatidyl glycerol, amniotic fluid' where lab_icd10tm = '0584105';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000644','','การตรวจหา phosphatidyl glycerol ในน้ำคร่ำ ','Phosphatidyl glycerol, amniotic fluid','1','0584105'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0584105');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบ pulmonary surfactant  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Prostatic specific antigen, serum/plasma' where lab_icd10tm = '0584202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000645','','การทดสอบ pulmonary surfactant  ในซีรั่ม / พลาสม่า','Prostatic specific antigen, serum/plasma','1','0584202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0584202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบ pulmonary surfactant  ในน้ำคร่ำ',lab_ncd_note = 'Pulmonary surfactant test, amniotic fluid' where lab_icd10tm = '0584305';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000646','','การทดสอบ pulmonary surfactant  ในน้ำคร่ำ','Pulmonary surfactant test, amniotic fluid','1','0584305'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0584305');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา succinylacetone  ในเลือด ',lab_ncd_note = 'Succinylacetone, whole blood' where lab_icd10tm = '0584401';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000647','','การตรวจหา succinylacetone  ในเลือด ','Succinylacetone, whole blood','1','0584401'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0584401');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา succinylacetone ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Succinylacetone, serum/plasma' where lab_icd10tm = '0584402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000648','','การตรวจหา succinylacetone ในซีรั่ม / พลาสม่า','Succinylacetone, serum/plasma','1','0584402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0584402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา succinylacetone ในปัสสาวะ',lab_ncd_note = 'Succinylacetone, urine' where lab_icd10tm = '0584403';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000649','','การตรวจหา succinylacetone ในปัสสาวะ','Succinylacetone, urine','1','0584403'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0584403');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาซัลเฟตในของเหลว อื่น ๆ มิได้จำแนก',lab_ncd_note = 'Sulfate, other fluid NEC' where lab_icd10tm = '0584515';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000650','','การตรวจหาซัลเฟตในของเหลว อื่น ๆ มิได้จำแนก','Sulfate, other fluid NEC','1','0584515'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0584515');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา thromboxane ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Thromboxane, serum/plasma' where lab_icd10tm = '0584602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000651','','การตรวจหา thromboxane ในซีรั่ม / พลาสม่า','Thromboxane, serum/plasma','1','0584602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0584602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา trypsinogen ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Trypsinogen, serum/plasma' where lab_icd10tm = '0584702';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000652','','การตรวจหา trypsinogen ในซีรั่ม / พลาสม่า','Trypsinogen, serum/plasma','1','0584702'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0584702');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายูเรตในเลือด ',lab_ncd_note = 'Urate, whole blood' where lab_icd10tm = '0584801';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000653','','การตรวจหายูเรตในเลือด ','Urate, whole blood','1','0584801'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0584801');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายูเรตในซีรั่ม / พลาสม่า',lab_ncd_note = 'Urate, serum/plasma' where lab_icd10tm = '0584802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000654','','การตรวจหายูเรตในซีรั่ม / พลาสม่า','Urate, serum/plasma','1','0584802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0584802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายูเรตในปัสสาวะ ',lab_ncd_note = 'Urate, urine' where lab_icd10tm = '0584803';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000655','','การตรวจหายูเรตในปัสสาวะ ','Urate, urine','1','0584803'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0584803');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายูเรตในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Urate, other fluid NEC' where lab_icd10tm = '0584815';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000656','','การตรวจหายูเรตในของเหลวอื่นๆ มิได้จำแนก','Urate, other fluid NEC','1','0584815'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0584815');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายูเรียในเลือด ',lab_ncd_note = 'Urea, whole blood' where lab_icd10tm = '0584901';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000657','','การตรวจหายูเรียในเลือด ','Urea, whole blood','1','0584901'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0584901');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายูเรียในซีรั่ม / พลาสม่า',lab_ncd_note = 'Urea, serum/plasma' where lab_icd10tm = '0584902';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000658','','การตรวจหายูเรียในซีรั่ม / พลาสม่า','Urea, serum/plasma','1','0584902'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0584902');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายูเรียในปัสสาวะ ',lab_ncd_note = 'Urea, urine' where lab_icd10tm = '0584903';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000659','','การตรวจหายูเรียในปัสสาวะ ','Urea, urine','1','0584903'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0584903');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายูเรียในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Urea, other fluid NEC' where lab_icd10tm = '0584915';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000660','','การตรวจหายูเรียในของเหลวอื่นๆ มิได้จำแนก','Urea, other fluid NEC','1','0584915'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0584915');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาไทรอกซีนทั้งหมด  ในเซรั่ม / พลาสม่า',lab_ncd_note = 'Thyroxine, total, serum/plasma (T4)' where lab_icd10tm = '0590202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000661','','การตรวจหาไทรอกซีนทั้งหมด  ในเซรั่ม / พลาสม่า','Thyroxine, total, serum/plasma (T4)','1','0590202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0590202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาไทรอกซีนอิสระ  ในเซรั่ม / พลาสม่า',lab_ncd_note = 'Thyroxine, free, serum/plasma (FT4)' where lab_icd10tm = '0590402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000662','','การตรวจหาไทรอกซีนอิสระ  ในเซรั่ม / พลาสม่า','Thyroxine, free, serum/plasma (FT4)','1','0590402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0590402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา triiodothyronine อิสระ  ในเซรั่ม / พลาสม่า',lab_ncd_note = 'Triiodothyronine, free, serum/plasma (FT3)' where lab_icd10tm = '0591202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000663','','การตรวจหา triiodothyronine อิสระ  ในเซรั่ม / พลาสม่า','Triiodothyronine, free, serum/plasma (FT3)','1','0591202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0591202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา triiodothyronine ย้อนกลับ  ในเซรั่ม / พลาสม่า',lab_ncd_note = 'Triiodothyronine, reverse, serum/plasma (rT3)' where lab_icd10tm = '0591402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000664','','การตรวจหา triiodothyronine ย้อนกลับ  ในเซรั่ม / พลาสม่า','Triiodothyronine, reverse, serum/plasma (rT3)','1','0591402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0591402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา triiodothyronine ทั้งหมด  ในเซรั่ม / พลาสม่า',lab_ncd_note = 'Triiodothyronine, total, serum/plasma (T3)' where lab_icd10tm = '0591602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000665','','การตรวจหา triiodothyronine ทั้งหมด  ในเซรั่ม / พลาสม่า','Triiodothyronine, total, serum/plasma (T3)','1','0591602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0591602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา triiodothyronine ดูดซึมเข้าเนื้อเยื่อ ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Triiodothyronine, uptake, serum/plasma' where lab_icd10tm = '0591802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000666','','การตรวจหา triiodothyronine ดูดซึมเข้าเนื้อเยื่อ ในซีรั่ม / พลาสม่า','Triiodothyronine, uptake, serum/plasma','1','0591802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0591802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา thyroid stimulating hormone  ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Thyroid stimulating hormone, serum/plasma (TSH)' where lab_icd10tm = '0592202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000667','','การตรวจหา thyroid stimulating hormone  ในซีรั่ม / พลาสม่า ','Thyroid stimulating hormone, serum/plasma (TSH)','1','0592202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0592202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา thyroglobulin  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Thyroglobulin, serum/plasma ' where lab_icd10tm = '0593202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000668','','การตรวจหา thyroglobulin  ในซีรั่ม / พลาสม่า','Thyroglobulin, serum/plasma ','1','0593202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0593202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา thyroid stimulating immunoglobulin  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Thyroid stimulating immunoglobulin, serum/plasma' where lab_icd10tm = '0593402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000669','','การตรวจหา thyroid stimulating immunoglobulin  ในซีรั่ม / พลาสม่า','Thyroid stimulating immunoglobulin, serum/plasma','1','0593402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0593402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา thyrotrophin binding inhibitory immunoglobulin ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Thyrotrophin binding inhibitory immunoglobulin, serum/plasma' where lab_icd10tm = '0593602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000670','','การตรวจหา thyrotrophin binding inhibitory immunoglobulin ในซีรั่ม / พลาสม่า','Thyrotrophin binding inhibitory immunoglobulin, serum/plasma','1','0593602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0593602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา thyroxine binding globulin ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Thyroxine binding globulin, serum/plasma' where lab_icd10tm = '0593802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000671','','การตรวจหา thyroxine binding globulin ในซีรั่ม / พลาสม่า ','Thyroxine binding globulin, serum/plasma','1','0593802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0593802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา 17 – hydroxyprogesterone ในซีรั่ม / พลาสม่า',lab_ncd_note = '17-Hydroxyprogesterone, serum/plasma' where lab_icd10tm = '0600202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000672','','การตรวจหา 17 – hydroxyprogesterone ในซีรั่ม / พลาสม่า','17-Hydroxyprogesterone, serum/plasma','1','0600202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0600202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา dihydrotestosterone ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Dihydrotestosterone, serum/plasma' where lab_icd10tm = '0600402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000673','','การตรวจหา dihydrotestosterone ในซีรั่ม / พลาสม่า','Dihydrotestosterone, serum/plasma','1','0600402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0600402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา estradiol ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Estradiol, serum/plasma' where lab_icd10tm = '0600602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000674','','การตรวจหา estradiol ในซีรั่ม / พลาสม่า','Estradiol, serum/plasma','1','0600602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0600602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา estradiol ในปัสสาวะ',lab_ncd_note = 'Estradiol, urine' where lab_icd10tm = '0600603';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000675','','การตรวจหา estradiol ในปัสสาวะ','Estradiol, urine','1','0600603'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0600603');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา estriol ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Estriol, serum/plasma' where lab_icd10tm = '0601002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000676','','การตรวจหา estriol ในซีรั่ม / พลาสม่า','Estriol, serum/plasma','1','0601002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0601002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา estriol ในปัสสาวะ',lab_ncd_note = 'Estriol, urine' where lab_icd10tm = '0601003';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000677','','การตรวจหา estriol ในปัสสาวะ','Estriol, urine','1','0601003'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0601003');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์หา estrogen receptor ในเนื้อเยื่ออื่น ๆ มิได้จำแนก',lab_ncd_note = 'Estrogen receptor assay, tissue NEC' where lab_icd10tm = '0601250';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000678','','การวิเคราะห์หา estrogen receptor ในเนื้อเยื่ออื่น ๆ มิได้จำแนก','Estrogen receptor assay, tissue NEC','1','0601250'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0601250');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา estrogen ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Estrogens, serum/plasma' where lab_icd10tm = '0601402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000679','','การตรวจหา estrogen ในซีรั่ม / พลาสม่า ','Estrogens, serum/plasma','1','0601402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0601402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา follicle stimulating hormone ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Follicle stimulating hormone, serum/plasma' where lab_icd10tm = '0601802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000680','','การตรวจหา follicle stimulating hormone ในซีรั่ม / พลาสม่า','Follicle stimulating hormone, serum/plasma','1','0601802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0601802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา gonadotropin releasing hormone ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Gonadotropin releasing hormone, serum/plasma' where lab_icd10tm = '0602002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000681','','การตรวจหา gonadotropin releasing hormone ในซีรั่ม / พลาสม่า','Gonadotropin releasing hormone, serum/plasma','1','0602002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0602002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรอง human chorionic gonadotropin ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Human chorionic gonadotropin screen, serum/plasma' where lab_icd10tm = '0602202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000682','','การตรวจคัดกรอง human chorionic gonadotropin ในซีรั่ม / พลาสม่า','Human chorionic gonadotropin screen, serum/plasma','1','0602202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0602202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรอง human chorionic gonadotropin ในปัสสาวะ',lab_ncd_note = 'Human chorionic gonadotropin screen, urine' where lab_icd10tm = '0602203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000683','','การตรวจคัดกรอง human chorionic gonadotropin ในปัสสาวะ','Human chorionic gonadotropin screen, urine','1','0602203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0602203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา human chorionic gonadotropin ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Human chorionic gonadotropin, serum/plasma' where lab_icd10tm = '0602402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000684','','การตรวจหา human chorionic gonadotropin ในซีรั่ม / พลาสม่า','Human chorionic gonadotropin, serum/plasma','1','0602402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0602402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา human chorionic gonadotropin ในปัสสาวะ',lab_ncd_note = 'Human chorionic gonadotropin, urine' where lab_icd10tm = '0602403';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000685','','การตรวจหา human chorionic gonadotropin ในปัสสาวะ','Human chorionic gonadotropin, urine','1','0602403'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0602403');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา human placental lactogen ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Human placental lactogen, serum/plasma' where lab_icd10tm = '0602602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000686','','การตรวจหา human placental lactogen ในซีรั่ม / พลาสม่า','Human placental lactogen, serum/plasma','1','0602602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0602602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา luteinizing hormone ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Luteinizing hormone, serum/plasma' where lab_icd10tm = '0603002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000687','','การตรวจหา luteinizing hormone ในซีรั่ม / พลาสม่า','Luteinizing hormone, serum/plasma','1','0603002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0603002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา pregnanediol ในปัสสาวะ',lab_ncd_note = 'Pregnanediol, urine' where lab_icd10tm = '0603203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000688','','การตรวจหา pregnanediol ในปัสสาวะ','Pregnanediol, urine','1','0603203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0603203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา pregnanetriol ในปัสสาวะ',lab_ncd_note = 'Pregnanetriol, urine' where lab_icd10tm = '0603403';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000689','','การตรวจหา pregnanetriol ในปัสสาวะ','Pregnanetriol, urine','1','0603403'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0603403');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา progesterone receptor ในเนื้อเยื่ออื่นๆ มิได้จำแนก',lab_ncd_note = 'Progesterone receptor assay, tissue NEC' where lab_icd10tm = '0603650';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000690','','การตรวจวิเคราะห์หา progesterone receptor ในเนื้อเยื่ออื่นๆ มิได้จำแนก','Progesterone receptor assay, tissue NEC','1','0603650'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0603650');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา progesterone ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Progesterone, serum/plasma' where lab_icd10tm = '0603802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000691','','การตรวจหา progesterone ในซีรั่ม / พลาสม่า','Progesterone, serum/plasma','1','0603802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0603802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา prolactin ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Prolactin, serum/plasma' where lab_icd10tm = '0604002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000692','','การตรวจหา prolactin ในซีรั่ม / พลาสม่า','Prolactin, serum/plasma','1','0604002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0604002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา sex hormone binding globulin ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Sex hormone binding globulin, serum/plasma' where lab_icd10tm = '0604202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000693','','การตรวจหา sex hormone binding globulin ในซีรั่ม / พลาสม่า','Sex hormone binding globulin, serum/plasma','1','0604202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0604202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา testosterone ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Testosterone, serum/plasma' where lab_icd10tm = '0604402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000694','','การตรวจหา testosterone ในซีรั่ม / พลาสม่า','Testosterone, serum/plasma','1','0604402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0604402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา testosterone ในปัสสาวะ ',lab_ncd_note = 'Testosterone, urine' where lab_icd10tm = '0604403';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000695','','การตรวจหา testosterone ในปัสสาวะ ','Testosterone, urine','1','0604403'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0604403');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'Re androgen index' where lab_icd10tm = '0604404';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000696','','-','Re androgen index','1','0604404'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0604404');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา Adrenocorticotropic ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Adrenocorticotropic hormone, serum/plasma' where lab_icd10tm = '0610602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000697','','การตรวจหา Adrenocorticotropic ในซีรั่ม / พลาสม่า','Adrenocorticotropic hormone, serum/plasma','1','0610602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0610602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา aldosterone ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Aldosterone, serum/plasma' where lab_icd10tm = '0610702';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000698','','การตรวจหา aldosterone ในซีรั่ม / พลาสม่า ','Aldosterone, serum/plasma','1','0610702'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0610702');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา androstanediol glucuronide ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Androstanediol glucuronide, serum/plasma' where lab_icd10tm = '0610902';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000699','','การตรวจหา androstanediol glucuronide ในซีรั่ม / พลาสม่า','Androstanediol glucuronide, serum/plasma','1','0610902'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0610902');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา androstenediol ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Androstenedione, serum/plasma' where lab_icd10tm = '0611102';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000700','','การตรวจหา androstenediol ในซีรั่ม / พลาสม่า','Androstenedione, serum/plasma','1','0611102'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0611102');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา androsterone ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Androsterone, serum/plasma' where lab_icd10tm = '0611202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000701','','การตรวจหา androsterone ในซีรั่ม / พลาสม่า','Androsterone, serum/plasma','1','0611202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0611202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา angiotensin ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Angiotensin, serum/plasma' where lab_icd10tm = '0611302';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000702','','การตรวจหา angiotensin ในซีรั่ม / พลาสม่า','Angiotensin, serum/plasma','1','0611302'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0611302');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา antidiuretic hormone ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Antidiuretic hormone, serum/plasma' where lab_icd10tm = '0611402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000703','','การตรวจหา antidiuretic hormone ในซีรั่ม / พลาสม่า','Antidiuretic hormone, serum/plasma','1','0611402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0611402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'ารตรวจหา atrial natriuretic polypeptide ในเลือด ',lab_ncd_note = 'Atrial natriuretic polypeptide, whole blood' where lab_icd10tm = '0611501';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000704','','ารตรวจหา atrial natriuretic polypeptide ในเลือด ','Atrial natriuretic polypeptide, whole blood','1','0611501'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0611501');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา atrial natriuretic polypeptide ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Atrial natriuretic polypeptide, serum/plasma' where lab_icd10tm = '0611502';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000705','','การตรวจหา atrial natriuretic polypeptide ในซีรั่ม / พลาสม่า','Atrial natriuretic polypeptide, serum/plasma','1','0611502'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0611502');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา Beta endorphin ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Beta endorphin, serum/plasma' where lab_icd10tm = '0611602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000706','','การตรวจหา Beta endorphin ในซีรั่ม / พลาสม่า','Beta endorphin, serum/plasma','1','0611602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0611602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา C-peptide ในซีรั่ม / พลาสม่า',lab_ncd_note = 'C-peptide assay, serum/plasma' where lab_icd10tm = '0611702';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000707','','การตรวจวิเคราะห์หา C-peptide ในซีรั่ม / พลาสม่า','C-peptide assay, serum/plasma','1','0611702'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0611702');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา calcitonin ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Calcitonin, serum/plasma' where lab_icd10tm = '0611802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000708','','การตรวจหา calcitonin ในซีรั่ม / พลาสม่า','Calcitonin, serum/plasma','1','0611802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0611802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา catecholamines ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Catecholamines, serum/plasma' where lab_icd10tm = '0611902';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000709','','การตรวจหา catecholamines ในซีรั่ม / พลาสม่า','Catecholamines, serum/plasma','1','0611902'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0611902');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา catecholamines ในปัสสาวะ',lab_ncd_note = 'Catecholamines, urine' where lab_icd10tm = '0611903';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000710','','การตรวจหา catecholamines ในปัสสาวะ','Catecholamines, urine','1','0611903'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0611903');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาเศษ catecholamines ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Catecholamines, fractionated, serum/plasma' where lab_icd10tm = '0612002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000711','','การตรวจหาเศษ catecholamines ในซีรั่ม / พลาสม่า','Catecholamines, fractionated, serum/plasma','1','0612002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0612002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาเศษ catecholamines ในปัสสาวะ',lab_ncd_note = 'Catecholamines, fractionated, urine' where lab_icd10tm = '0612003';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000712','','การตรวจหาเศษ catecholamines ในปัสสาวะ','Catecholamines, fractionated, urine','1','0612003'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0612003');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา cortisol ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Cortisol, serum/plasma' where lab_icd10tm = '0612202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000713','','การตรวจหา cortisol ในซีรั่ม / พลาสม่า','Cortisol, serum/plasma','1','0612202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0612202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา cortisol ในปัสสาวะ',lab_ncd_note = 'Cortisol, urine' where lab_icd10tm = '0612203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000714','','การตรวจหา cortisol ในปัสสาวะ','Cortisol, urine','1','0612203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0612203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา dehydroepiandrosterone ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Dehydroepiandrosterone, serum/plasma' where lab_icd10tm = '0612402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000715','','การตรวจหา dehydroepiandrosterone ในซีรั่ม / พลาสม่า','Dehydroepiandrosterone, serum/plasma','1','0612402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0612402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา dehydroepiandrosterone-sulfate ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Dehydroepiandrosterone-sulfate, serum/plasma' where lab_icd10tm = '0612502';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000716','','การตรวจหา dehydroepiandrosterone-sulfate ในซีรั่ม / พลาสม่า','Dehydroepiandrosterone-sulfate, serum/plasma','1','0612502'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0612502');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา erythropoietin ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Erythropoietin, serum/plasma' where lab_icd10tm = '0612602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000717','','การตรวจหา erythropoietin ในซีรั่ม / พลาสม่า','Erythropoietin, serum/plasma','1','0612602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0612602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา gastrin ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Gastrin, serum/plasma' where lab_icd10tm = '0612902';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000718','','การตรวจหา gastrin ในซีรั่ม / พลาสม่า','Gastrin, serum/plasma','1','0612902'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0612902');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา gastrin inhibitory polypeptide ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Gastrin inhibitory polypeptide, serum/plasma' where lab_icd10tm = '0613002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000719','','การตรวจหา gastrin inhibitory polypeptide ในซีรั่ม / พลาสม่า','Gastrin inhibitory polypeptide, serum/plasma','1','0613002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0613002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา gastrin releasing polypeptide ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Gastrin releasing polypeptide, serum/plasma' where lab_icd10tm = '0613102';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000720','','การตรวจหา gastrin releasing polypeptide ในซีรั่ม / พลาสม่า','Gastrin releasing polypeptide, serum/plasma','1','0613102'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0613102');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา glucagon ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Glucagon, serum/plasma' where lab_icd10tm = '0613202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000721','','การตรวจหา glucagon ในซีรั่ม / พลาสม่า','Glucagon, serum/plasma','1','0613202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0613202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา growth hormone ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Growth hormone, serum/plasma' where lab_icd10tm = '0613302';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000722','','การตรวจหา growth hormone ในซีรั่ม / พลาสม่า','Growth hormone, serum/plasma','1','0613302'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0613302');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา homovanillic acid ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Homovanillic acid, serum/plasma' where lab_icd10tm = '0613402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000723','','การตรวจหา homovanillic acid ในซีรั่ม / พลาสม่า','Homovanillic acid, serum/plasma','1','0613402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0613402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา homovanillic acid ในปัสสาวะ',lab_ncd_note = 'Homovanillic acid,  urine' where lab_icd10tm = '0613403';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000724','','การตรวจหา homovanillic acid ในปัสสาวะ','Homovanillic acid,  urine','1','0613403'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0613403');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา insulin-like growth factor ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Insulin-like growth factor, serum/plasma' where lab_icd10tm = '0613702';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000725','','การตรวจหา insulin-like growth factor ในซีรั่ม / พลาสม่า','Insulin-like growth factor, serum/plasma','1','0613702'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0613702');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาอินซูลินในซีรั่ม / พลาสม่า',lab_ncd_note = 'Insulin, serum/plasma' where lab_icd10tm = '0613902';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000726','','การตรวจหาอินซูลินในซีรั่ม / พลาสม่า','Insulin, serum/plasma','1','0613902'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0613902');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา metanephrine ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Metanephrine/Nor Metanephrine, serum/plasma' where lab_icd10tm = '0614002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000727','','การตรวจหา metanephrine ในซีรั่ม / พลาสม่า','Metanephrine/Nor Metanephrine, serum/plasma','1','0614002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0614002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา metanephrine ในปัสสาวะ',lab_ncd_note = 'Metanephrine, urine' where lab_icd10tm = '0614003';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000728','','การตรวจหา metanephrine ในปัสสาวะ','Metanephrine, urine','1','0614003'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0614003');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา oxytocin ในซีรั่ม/ พลาสม่า',lab_ncd_note = 'Oxytocin, serum/plasma' where lab_icd10tm = '0614202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000729','','การตรวจหา oxytocin ในซีรั่ม/ พลาสม่า','Oxytocin, serum/plasma','1','0614202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0614202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา pancreatic polypeptide ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Pancreatic polypeptide, serum/plasma' where lab_icd10tm = '0614302';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000730','','การตรวจหา pancreatic polypeptide ในซีรั่ม / พลาสม่า','Pancreatic polypeptide, serum/plasma','1','0614302'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0614302');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา parathyroid hormone ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Parathyroid hormone, serum/plasma' where lab_icd10tm = '0614402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000731','','การตรวจหา parathyroid hormone ในซีรั่ม / พลาสม่า','Parathyroid hormone, serum/plasma','1','0614402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0614402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา proinsulin ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Proinsulin, serum/plasma' where lab_icd10tm = '0614502';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000732','','การตรวจหา proinsulin ในซีรั่ม / พลาสม่า','Proinsulin, serum/plasma','1','0614502'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0614502');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา renin ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Renin, serum/plasma' where lab_icd10tm = '0614602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000733','','การตรวจหา renin ในซีรั่ม / พลาสม่า','Renin, serum/plasma','1','0614602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0614602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา secretin ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Secretin, serum/plasma' where lab_icd10tm = '0614702';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000734','','การตรวจหา secretin ในซีรั่ม / พลาสม่า','Secretin, serum/plasma','1','0614702'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0614702');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา serotonin ในเลือด',lab_ncd_note = 'Serotonin, whole blood' where lab_icd10tm = '0614801';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000735','','การตรวจหา serotonin ในเลือด','Serotonin, whole blood','1','0614801'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0614801');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา serotonin ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Serotonin, serum/plasma' where lab_icd10tm = '0614802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000736','','การตรวจหา serotonin ในซีรั่ม / พลาสม่า','Serotonin, serum/plasma','1','0614802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0614802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา somatostatin ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Somatostatin, serum/plasma' where lab_icd10tm = '0614902';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000737','','การตรวจหา somatostatin ในซีรั่ม / พลาสม่า','Somatostatin, serum/plasma','1','0614902'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0614902');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา vanillylmandelic acid ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Vanillylmandelic acid, serum/plasma' where lab_icd10tm = '0615002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000738','','การตรวจหา vanillylmandelic acid ในซีรั่ม / พลาสม่า','Vanillylmandelic acid, serum/plasma','1','0615002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0615002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา vanillylmandelic acid ในปัสสาวะ ',lab_ncd_note = 'Vanillylmandelic acid, urine' where lab_icd10tm = '0615003';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000739','','การตรวจหา vanillylmandelic acid ในปัสสาวะ ','Vanillylmandelic acid, urine','1','0615003'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0615003');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา vasoactive intestinal peptide ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Vasoactive intestinal peptide, serum/plasma' where lab_icd10tm = '0615102';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000740','','การตรวจหา vasoactive intestinal peptide ในซีรั่ม / พลาสม่า','Vasoactive intestinal peptide, serum/plasma','1','0615102'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0615102');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา melanocyte stimulating hormone ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Melanocyte stimulating hormone, serum/plasma' where lab_icd10tm = '0615202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000741','','การตรวจหา melanocyte stimulating hormone ในซีรั่ม / พลาสม่า','Melanocyte stimulating hormone, serum/plasma','1','0615202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0615202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจความสมบูรณ์ของเลือด โดยปราศจากการนับแยกด้วยเครื่องอัตโนมัติ ในเลือด ',lab_ncd_note = 'Complete blood count without automated differential, whole blood' where lab_icd10tm = '0620201';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000742','','การตรวจความสมบูรณ์ของเลือด โดยปราศจากการนับแยกด้วยเครื่องอัตโนมัติ ในเลือด ','Complete blood count without automated differential, whole blood','1','0620201'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0620201');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจความสมบูรณ์ของเลือด โดยปราศจากการนับแยกด้วยเครื่องอัตโนมัติ ในน้ำไขสันหลัง',lab_ncd_note = 'Complete blood count without automated differential, CSF' where lab_icd10tm = '0620204';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000743','','การตรวจความสมบูรณ์ของเลือด โดยปราศจากการนับแยกด้วยเครื่องอัตโนมัติ ในน้ำไขสันหลัง','Complete blood count without automated differential, CSF','1','0620204'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0620204');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจความสมบูรณ์ของเลือด โดยปราศจากการนับแยกด้วยเครื่องอัตโนมัติ ใน ของเหลว อื่น ๆ มิได้จำแนก',lab_ncd_note = 'Complete blood count without automated differential, other fluid NEC' where lab_icd10tm = '0620215';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000744','','การตรวจความสมบูรณ์ของเลือด โดยปราศจากการนับแยกด้วยเครื่องอัตโนมัติ ใน ของเหลว อื่น ๆ มิได้จำแนก','Complete blood count without automated differential, other fluid NEC','1','0620215'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0620215');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจความสมบูรณ์ของเลือด โดยการนับแยกด้วยเครื่องอัตโนมัติ ในเลือด ',lab_ncd_note = 'Complete blood count with automated differential, whole blood' where lab_icd10tm = '0620401';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000745','','การตรวจความสมบูรณ์ของเลือด โดยการนับแยกด้วยเครื่องอัตโนมัติ ในเลือด ','Complete blood count with automated differential, whole blood','1','0620401'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0620401');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจความสมบูรณ์ของเลือด โดยการนับแยกด้วยเครื่องอัตโนมัติ  ในน้ำไขสันหลัง ',lab_ncd_note = 'Complete blood count with automated differential, CSF' where lab_icd10tm = '0620404';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000746','','การตรวจความสมบูรณ์ของเลือด โดยการนับแยกด้วยเครื่องอัตโนมัติ  ในน้ำไขสันหลัง ','Complete blood count with automated differential, CSF','1','0620404'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0620404');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจความสมบูรณ์ของเลือด โดยการนับแยกด้วยเครื่องอัตโนมัติ  ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Complete blood count with automated differential, other fluid NEC' where lab_icd10tm = '0620415';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000747','','การตรวจความสมบูรณ์ของเลือด โดยการนับแยกด้วยเครื่องอัตโนมัติ  ในของเหลวอื่นๆ มิได้จำแนก','Complete blood count with automated differential, other fluid NEC','1','0620415'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0620415');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาค่าฮีมาโตคริตในเลือด ',lab_ncd_note = 'Hematocrit, whole blood' where lab_icd10tm = '0621201';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000748','','การตรวจหาค่าฮีมาโตคริตในเลือด ','Hematocrit, whole blood','1','0621201'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0621201');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาค่าฮีมาโตคริตในของเหลวอื่นๆ  มิได้จำแนก',lab_ncd_note = 'Hematocrit, other fluid NEC' where lab_icd10tm = '0621215';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000749','','การตรวจหาค่าฮีมาโตคริตในของเหลวอื่นๆ  มิได้จำแนก','Hematocrit, other fluid NEC','1','0621215'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0621215');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาค่าฮีโมโกลบินในเลือด',lab_ncd_note = 'Hemoglobin, whole blood' where lab_icd10tm = '0621401';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000750','','การตรวจหาค่าฮีโมโกลบินในเลือด','Hemoglobin, whole blood','1','0621401'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0621401');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาค่าฮีโมโกลบินในพลาสม่า',lab_ncd_note = 'Hemoglobin, plasma' where lab_icd10tm = '0621402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000751','','การตรวจหาค่าฮีโมโกลบินในพลาสม่า','Hemoglobin, plasma','1','0621402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0621402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาค่าฮีโมโกลบินในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Hemoglobin, other fluid NEC' where lab_icd10tm = '0621415';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000752','','การตรวจหาค่าฮีโมโกลบินในของเหลวอื่นๆ มิได้จำแนก','Hemoglobin, other fluid NEC','1','0621415'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0621415');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจนับจำนวน eosinophil ในเลือด ',lab_ncd_note = 'Eosinophil count, whole blood' where lab_icd10tm = '0621601';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000753','','การตรวจนับจำนวน eosinophil ในเลือด ','Eosinophil count, whole blood','1','0621601'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0621601');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจนับจำนวน eosinophil ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Eosinophil count, other fluid NEC' where lab_icd10tm = '0621615';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000754','','การตรวจนับจำนวน eosinophil ในของเหลวอื่นๆ มิได้จำแนก','Eosinophil count, other fluid NEC','1','0621615'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0621615');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจนับจำนวน reticulocyte ในเลือด',lab_ncd_note = 'Reticulocyte count, whole blood' where lab_icd10tm = '0621801';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000755','','การตรวจนับจำนวน reticulocyte ในเลือด','Reticulocyte count, whole blood','1','0621801'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0621801');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจนับจำนวนเม็ดเลือดขาวในเลือด',lab_ncd_note = 'White blood cell count, whole blood' where lab_icd10tm = '0622201';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000756','','การตรวจนับจำนวนเม็ดเลือดขาวในเลือด','White blood cell count, whole blood','1','0622201'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0622201');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจนับจำนวนเม็ดเลือดขาวในน้ำไขสันหลัง ',lab_ncd_note = 'White blood cell count, CSF' where lab_icd10tm = '0622204';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000757','','การตรวจนับจำนวนเม็ดเลือดขาวในน้ำไขสันหลัง ','White blood cell count, CSF','1','0622204'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0622204');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจนับจำนวนเม็ดเลือดขาวในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'White blood cell count, other fluid NEC' where lab_icd10tm = '0622215';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000758','','การตรวจนับจำนวนเม็ดเลือดขาวในของเหลวอื่นๆ มิได้จำแนก','White blood cell count, other fluid NEC','1','0622215'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0622215');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจนับจำนวนเม็ดเลือดขาวในไขกระดูก',lab_ncd_note = 'White blood cell count, bone marrow' where lab_icd10tm = '0622227';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000759','','การตรวจนับจำนวนเม็ดเลือดขาวในไขกระดูก','White blood cell count, bone marrow','1','0622227'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0622227');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจนับจำนวนเม็ดเลือดแดงในเลือด',lab_ncd_note = 'Red blood cell count, whole blood' where lab_icd10tm = '0622401';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000760','','การตรวจนับจำนวนเม็ดเลือดแดงในเลือด','Red blood cell count, whole blood','1','0622401'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0622401');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจนับจำนวนเม็ดเลือดแดงในน้ำไขสันหลัง',lab_ncd_note = 'Red blood cell count, CSF' where lab_icd10tm = '0622404';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000761','','การตรวจนับจำนวนเม็ดเลือดแดงในน้ำไขสันหลัง','Red blood cell count, CSF','1','0622404'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0622404');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจนับจำนวนเม็ดเลือดแดงในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Red blood cell count, other fluid NEC' where lab_icd10tm = '0622415';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000762','','การตรวจนับจำนวนเม็ดเลือดแดงในของเหลวอื่นๆ มิได้จำแนก','Red blood cell count, other fluid NEC','1','0622415'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0622415');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจนับจำนวนเกล็ดเลือดในเลือด',lab_ncd_note = 'Platelet count, whole blood' where lab_icd10tm = '0622601';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000763','','การตรวจนับจำนวนเกล็ดเลือดในเลือด','Platelet count, whole blood','1','0622601'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0622601');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจนับแยกเม็ดเลือดขาว (ด้วยมือ) ในเลือด ',lab_ncd_note = 'White blood cell differential (manual), whole blood' where lab_icd10tm = '0623001';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000764','','การตรวจนับแยกเม็ดเลือดขาว (ด้วยมือ) ในเลือด ','White blood cell differential (manual), whole blood','1','0623001'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0623001');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจนับแยกเม็ดเลือดขาว (ด้วยมือ) ในน้ำไขสันหลัง',lab_ncd_note = 'White blood cell differential (manual), CSF' where lab_icd10tm = '0623004';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000765','','การตรวจนับแยกเม็ดเลือดขาว (ด้วยมือ) ในน้ำไขสันหลัง','White blood cell differential (manual), CSF','1','0623004'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0623004');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจนับแยกเม็ดเลือดขาว (ด้วยมือ) ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'White blood cell differential (manual), other fluid NEC' where lab_icd10tm = '0623015';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000766','','การตรวจนับแยกเม็ดเลือดขาว (ด้วยมือ) ในของเหลวอื่นๆ มิได้จำแนก','White blood cell differential (manual), other fluid NEC','1','0623015'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0623015');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจนับแยกเม็ดเลือดขาว (ด้วยมือ) ในไขกระดูก',lab_ncd_note = 'white blood cell differential (manual), bone marrow' where lab_icd10tm = '0623027';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000767','','การตรวจนับแยกเม็ดเลือดขาว (ด้วยมือ) ในไขกระดูก','white blood cell differential (manual), bone marrow','1','0623027'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0623027');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจด้วยกล้องจุลทรรศน์ (เพื่อดูลักษณะรูปร่างและประมาณการ) ในเลือด ',lab_ncd_note = 'Microscopic examination (for morphology and estimates), whole blood' where lab_icd10tm = '0623401';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000768','','การตรวจด้วยกล้องจุลทรรศน์ (เพื่อดูลักษณะรูปร่างและประมาณการ) ในเลือด ','Microscopic examination (for morphology and estimates), whole blood','1','0623401'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0623401');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจด้วยกล้องจุลทรรศน์ (เพื่อดูลักษณะรูปร่างและประมาณการ) ในน้ำไขสันหลัง',lab_ncd_note = 'Microscopic examination (for morphology and estimates), CSF' where lab_icd10tm = '0623404';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000769','','การตรวจด้วยกล้องจุลทรรศน์ (เพื่อดูลักษณะรูปร่างและประมาณการ) ในน้ำไขสันหลัง','Microscopic examination (for morphology and estimates), CSF','1','0623404'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0623404');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจด้วยกล้องจุลทรรศน์ (เพื่อดูลักษณะรูปร่างและประมาณการ) ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Microscopic examination (for morphology and estimates),  other fluid NEC' where lab_icd10tm = '0623415';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000770','','การตรวจด้วยกล้องจุลทรรศน์ (เพื่อดูลักษณะรูปร่างและประมาณการ) ในของเหลวอื่นๆ มิได้จำแนก','Microscopic examination (for morphology and estimates),  other fluid NEC','1','0623415'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0623415');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจด้วยกล้องจุลทรรศน์ (เพื่อดูลักษณะรูปร่างและประมาณการ) ในไขกระดูก',lab_ncd_note = 'Microscopic examination (for morphology and estimates), bone marrow' where lab_icd10tm = '0623427';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000771','','การตรวจด้วยกล้องจุลทรรศน์ (เพื่อดูลักษณะรูปร่างและประมาณการ) ในไขกระดูก','Microscopic examination (for morphology and estimates), bone marrow','1','0623427'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0623427');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบ acidified serum lysis ในเลือด ',lab_ncd_note = 'Acidified serum lysis test, whole blood' where lab_icd10tm = '0630201';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000772','','การทดสอบ acidified serum lysis ในเลือด ','Acidified serum lysis test, whole blood','1','0630201'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0630201');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเตรียม buffy coat smear ในเลือด',lab_ncd_note = 'Buffy coat smear preparation, whole blood' where lab_icd10tm = '0630401';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000773','','การเตรียม buffy coat smear ในเลือด','Buffy coat smear preparation, whole blood','1','0630401'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0630401');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจสอบ buffy coat smear ในเลือด',lab_ncd_note = 'Buffy coat smear examination, whole blood' where lab_icd10tm = '0630601';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000774','','การตรวจสอบ buffy coat smear ในเลือด','Buffy coat smear examination, whole blood','1','0630601'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0630601');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบความเปราะของหลอดเลือดฝอยในเลือด',lab_ncd_note = 'Capillary fragility test, whole blood' where lab_icd10tm = '0630801';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000775','','การทดสอบความเปราะของหลอดเลือดฝอยในเลือด','Capillary fragility test, whole blood','1','0630801'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0630801');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหา fetal cells [เช่น kleihauer] ในเลือด',lab_ncd_note = 'Fetal cells screen [e.g. kleihauer], whole blood' where lab_icd10tm = '0631201';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000776','','การตรวจคัดกรองหา fetal cells [เช่น kleihauer] ในเลือด','Fetal cells screen [e.g. kleihauer], whole blood','1','0631201'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0631201');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหา fetal cells [เช่น kleihauer] ในของเหลวอื่นๆ  มิได้จำแนก',lab_ncd_note = 'Fetal cells screen [e.g. kleihauer], other fluid NEC' where lab_icd10tm = '0631215';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000777','','การตรวจคัดกรองหา fetal cells [เช่น kleihauer] ในของเหลวอื่นๆ  มิได้จำแนก','Fetal cells screen [e.g. kleihauer], other fluid NEC','1','0631215'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0631215');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหา fetal cells [เช่น rosette] ในเม็ดเลือดแดง',lab_ncd_note = 'Fetal/maternal hemorrhage screen [e.g. rosette], RBC' where lab_icd10tm = '0631417';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000778','','การตรวจคัดกรองหา fetal cells [เช่น rosette] ในเม็ดเลือดแดง','Fetal/maternal hemorrhage screen [e.g. rosette], RBC','1','0631417'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0631417');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจประเมิน Heinz body ในเลือด ',lab_ncd_note = 'Heinz body determination, whole blood' where lab_icd10tm = '0631601';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000779','','การตรวจประเมิน Heinz body ในเลือด ','Heinz body determination, whole blood','1','0631601'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0631601');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การย้อมพิเศษทางโลหิตวิทยาในเลือด ',lab_ncd_note = 'Hematology special stain, whole blood' where lab_icd10tm = '0632001';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000780','','การย้อมพิเศษทางโลหิตวิทยาในเลือด ','Hematology special stain, whole blood','1','0632001'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0632001');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การย้อมพิเศษทางโลหิตวิทยาในไขกระดูก',lab_ncd_note = 'Hematology special stain, bone marrow' where lab_icd10tm = '0632027';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000781','','การย้อมพิเศษทางโลหิตวิทยาในไขกระดูก','Hematology special stain, bone marrow','1','0632027'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0632027');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา hemoglobin A2 ในเลือด',lab_ncd_note = 'Hemoglobin A2, whole blood' where lab_icd10tm = '0632201';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000782','','การตรวจหา hemoglobin A2 ในเลือด','Hemoglobin A2, whole blood','1','0632201'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0632201');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ hemoglobin electrophoresis ในเลือด ',lab_ncd_note = 'Hemoglobin electrophoresis, whole blood' where lab_icd10tm = '0632401';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000783','','การตรวจ hemoglobin electrophoresis ในเลือด ','Hemoglobin electrophoresis, whole blood','1','0632401'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0632401');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา hemoglobin F ในเลือด',lab_ncd_note = 'Hemoglobin F, whole blood' where lab_icd10tm = '0632601';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000784','','การตรวจหา hemoglobin F ในเลือด','Hemoglobin F, whole blood','1','0632601'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0632601');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา hemoglobin H inclusions ในเลือด',lab_ncd_note = 'Hemoglobin H inclusions, whole blood' where lab_icd10tm = '0632801';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000785','','การตรวจหา hemoglobin H inclusions ในเลือด','Hemoglobin H inclusions, whole blood','1','0632801'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0632801');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหา hemoglobin S ในเลือด',lab_ncd_note = 'Hemoglobin S screen, whole blood' where lab_icd10tm = '0633001';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000786','','การตรวจคัดกรองหา hemoglobin S ในเลือด','Hemoglobin S screen, whole blood','1','0633001'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0633001');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา hemosiderin ในปัสสาวะ',lab_ncd_note = 'Hemosiderin, urine' where lab_icd10tm = '0634003';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000787','','การตรวจหา hemosiderin ในปัสสาวะ','Hemosiderin, urine','1','0634003'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0634003');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาคะแนน leukocyte alkaline phosphatase ในเลือด ',lab_ncd_note = 'Leukocyte alkaline phosphatase score, whole blood' where lab_icd10tm = '0634201';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000788','','การตรวจหาคะแนน leukocyte alkaline phosphatase ในเลือด ','Leukocyte alkaline phosphatase score, whole blood','1','0634201'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0634201');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาเชื้อมาลาเรียด้วยการทำฟิล์มโลหิตในเลือด',lab_ncd_note = 'Malaria smear, whole blood' where lab_icd10tm = '0634401';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000789','','การตรวจหาเชื้อมาลาเรียด้วยการทำฟิล์มโลหิตในเลือด','Malaria smear, whole blood','1','0634401'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0634401');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาเชื้อมาลาเรียด้วยการใช้ชุดตรวจสำเร็จรูป',lab_ncd_note = 'Malaria Antigen, rapid test ' where lab_icd10tm = '0634402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000790','','การตรวจหาเชื้อมาลาเรียด้วยการใช้ชุดตรวจสำเร็จรูป','Malaria Antigen, rapid test ','1','0634402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0634402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาเชื้อมาลาเรียด้วยวิธีชีวโมเลกุล',lab_ncd_note = 'Malaria molecular test' where lab_icd10tm = '0634403';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000791','','การตรวจหาเชื้อมาลาเรียด้วยวิธีชีวโมเลกุล','Malaria molecular test','1','0634403'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0634403');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาเชื้อ microfilaria ด้วยการทำฟิล์มโลหิตในเลือด',lab_ncd_note = 'Microfilaria smear, whole blood' where lab_icd10tm = '0634601';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000792','','การตรวจหาเชื้อ microfilaria ด้วยการทำฟิล์มโลหิตในเลือด','Microfilaria smear, whole blood','1','0634601'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0634601');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา mucin clot ในของเหลว อื่น ๆ มิได้จำแนก',lab_ncd_note = 'Mucin clot, other fluid NEC' where lab_icd10tm = '0634815';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000793','','การตรวจหา mucin clot ในของเหลว อื่น ๆ มิได้จำแนก','Mucin clot, other fluid NEC','1','0634815'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0634815');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา osmotic fragility ในเลือด',lab_ncd_note = 'Osmotic fragility, whole blood' where lab_icd10tm = '0635001';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000794','','การตรวจหา osmotic fragility ในเลือด','Osmotic fragility, whole blood','1','0635001'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0635001');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาอัตราการตกตะกอนในเลือด ',lab_ncd_note = 'Sedimentation rate, whole blood' where lab_icd10tm = '0635401';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000795','','การตรวจหาอัตราการตกตะกอนในเลือด ','Sedimentation rate, whole blood','1','0635401'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0635401');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา sucrose lysis ในเลือด',lab_ncd_note = 'Sucrose lysis, whole blood' where lab_icd10tm = '0635601';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000796','','การตรวจหา sucrose lysis ในเลือด','Sucrose lysis, whole blood','1','0635601'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0635601');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา sulfhemoglobin ในเลือด',lab_ncd_note = 'Sulfhemoglobin, whole blood' where lab_icd10tm = '0635801';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000797','','การตรวจหา sulfhemoglobin ในเลือด','Sulfhemoglobin, whole blood','1','0635801'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0635801');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา unstable hemoglobin ในเลือด',lab_ncd_note = 'Unstable hemoglobin, whole blood' where lab_icd10tm = '0636001';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000798','','การตรวจหา unstable hemoglobin ในเลือด','Unstable hemoglobin, whole blood','1','0636001'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0636001');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาความหนืดในเลือด',lab_ncd_note = 'Viscosity, whole blood' where lab_icd10tm = '0636201';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000799','','การตรวจหาความหนืดในเลือด','Viscosity, whole blood','1','0636201'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0636201');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาความหนืดในซีรั่ม / พลาสม่า',lab_ncd_note = 'Viscosity, serum/plasma' where lab_icd10tm = '0636202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000800','','การตรวจหาความหนืดในซีรั่ม / พลาสม่า','Viscosity, serum/plasma','1','0636202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0636202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวัดความสามารถในการเกิดปฏิกิริยาเคมีของ factor II ในพลาสม่า ',lab_ncd_note = 'Factor II activity, plasma' where lab_icd10tm = '0640202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000801','','การวัดความสามารถในการเกิดปฏิกิริยาเคมีของ factor II ในพลาสม่า ','Factor II activity, plasma','1','0640202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0640202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา factor II antigen ในพลาสม่า',lab_ncd_note = 'Factor II antigen assay, plasma' where lab_icd10tm = '0640402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000802','','การตรวจวิเคราะห์หา factor II antigen ในพลาสม่า','Factor II antigen assay, plasma','1','0640402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0640402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวัดความสามารถในการเกิดปฏิกิริยาเคมีของ factor V ในพลาสม่า',lab_ncd_note = 'Factor V activity, plasma' where lab_icd10tm = '0640602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000803','','การวัดความสามารถในการเกิดปฏิกิริยาเคมีของ factor V ในพลาสม่า','Factor V activity, plasma','1','0640602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0640602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา factor V antigen ในพลาสม่า',lab_ncd_note = 'Factor V antigen assay, plasma' where lab_icd10tm = '0640802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000804','','การตรวจวิเคราะห์หา factor V antigen ในพลาสม่า','Factor V antigen assay, plasma','1','0640802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0640802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวัดความสามารถในการเกิดปฏิกิริยาเคมีของ factor VII ในพลาสม่า',lab_ncd_note = 'Factor VII activity, plasma' where lab_icd10tm = '0641202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000805','','การวัดความสามารถในการเกิดปฏิกิริยาเคมีของ factor VII ในพลาสม่า','Factor VII activity, plasma','1','0641202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0641202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา factor VII antigen ในพลาสม่า',lab_ncd_note = 'Factor VII antigen assay, plasma' where lab_icd10tm = '0641402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000806','','การตรวจวิเคราะห์หา factor VII antigen ในพลาสม่า','Factor VII antigen assay, plasma','1','0641402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0641402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวัดความสามารถในการเกิดปฏิกิริยาเคมีของ factor VIII ในพลาสม่า',lab_ncd_note = 'Factor VIII activity, plasma' where lab_icd10tm = '0642202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000807','','การวัดความสามารถในการเกิดปฏิกิริยาเคมีของ factor VIII ในพลาสม่า','Factor VIII activity, plasma','1','0642202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0642202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา factor VIII Antigen ในพลาสม่า',lab_ncd_note = 'Factor VIII Antigen assay, plasma' where lab_icd10tm = '0642402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000808','','การตรวจวิเคราะห์หา factor VIII Antigen ในพลาสม่า','Factor VIII Antigen assay, plasma','1','0642402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0642402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวัดความสามารถในการเกิดปฏิกิริยาเคมีของ factor VIII ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Factor VIII antibody, serum/plasma' where lab_icd10tm = '0642602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000809','','การวัดความสามารถในการเกิดปฏิกิริยาเคมีของ factor VIII ในซีรั่ม / พลาสม่า','Factor VIII antibody, serum/plasma','1','0642602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0642602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา factor VIII multimer ในพลาสม่า',lab_ncd_note = 'Factor VIII multimer assay, plasma' where lab_icd10tm = '0642802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000810','','การตรวจวิเคราะห์หา factor VIII multimer ในพลาสม่า','Factor VIII multimer assay, plasma','1','0642802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0642802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา factor VIII multimer ในเกล็ดเลือด',lab_ncd_note = 'Factor VIII multimer assay, platelets' where lab_icd10tm = '0642818';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000811','','การตรวจวิเคราะห์หา factor VIII multimer ในเกล็ดเลือด','Factor VIII multimer assay, platelets','1','0642818'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0642818');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวัดความสามารถในการเกิดปฏิกิริยาเคมีของ factor IX ในพลาสม่า',lab_ncd_note = 'Factor IX activity, plasma' where lab_icd10tm = '0643202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000812','','การวัดความสามารถในการเกิดปฏิกิริยาเคมีของ factor IX ในพลาสม่า','Factor IX activity, plasma','1','0643202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0643202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา factor IX antigen ในพลาสม่า',lab_ncd_note = 'Factor IX antigen assay, plasma' where lab_icd10tm = '0643402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000813','','การตรวจวิเคราะห์หา factor IX antigen ในพลาสม่า','Factor IX antigen assay, plasma','1','0643402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0643402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวัดความสามารถในการเกิดปฏิกิริยาเคมีของ factor X ในพลาสม่า ',lab_ncd_note = 'Factor X activity, plasma' where lab_icd10tm = '0643602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000814','','การวัดความสามารถในการเกิดปฏิกิริยาเคมีของ factor X ในพลาสม่า ','Factor X activity, plasma','1','0643602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0643602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา factor X antigen ในพลาสม่า',lab_ncd_note = 'Factor X antigen assay, plasma' where lab_icd10tm = '0643802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000815','','การตรวจวิเคราะห์หา factor X antigen ในพลาสม่า','Factor X antigen assay, plasma','1','0643802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0643802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวัดความสามารถในการเกิดปฏิกิริยาเคมีของ factor XI ในพลาสม่า',lab_ncd_note = 'Factor XI activity, plasma' where lab_icd10tm = '0644202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000816','','การวัดความสามารถในการเกิดปฏิกิริยาเคมีของ factor XI ในพลาสม่า','Factor XI activity, plasma','1','0644202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0644202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา factor XI ในพลาสม่า',lab_ncd_note = 'Factor XI antigen assay, plasma' where lab_icd10tm = '0644402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000817','','การตรวจวิเคราะห์หา factor XI ในพลาสม่า','Factor XI antigen assay, plasma','1','0644402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0644402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวัดความสามารถในการเกิดปฏิกิริยาเคมีของ factor XII ในพลาสม่า',lab_ncd_note = 'Factor XII activity, plasma' where lab_icd10tm = '0644602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000818','','การวัดความสามารถในการเกิดปฏิกิริยาเคมีของ factor XII ในพลาสม่า','Factor XII activity, plasma','1','0644602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0644602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา factor XII antigen ในพลาสม่า',lab_ncd_note = 'Factor XII antigen assay, plasma' where lab_icd10tm = '0644802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000819','','การตรวจวิเคราะห์หา factor XII antigen ในพลาสม่า','Factor XII antigen assay, plasma','1','0644802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0644802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวัดความสามารถในการเกิดปฏิกิริยาเคมีของ factor XIII ในพลาสม่า',lab_ncd_note = 'Factor XIII activity, plasma' where lab_icd10tm = '0645202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000820','','การวัดความสามารถในการเกิดปฏิกิริยาเคมีของ factor XIII ในพลาสม่า','Factor XIII activity, plasma','1','0645202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0645202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา factor XIII antigen ในพลาสม่า',lab_ncd_note = 'Factor XIII antigen assay, plasma' where lab_icd10tm = '0645402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000821','','การตรวจวิเคราะห์หา factor XIII antigen ในพลาสม่า','Factor XIII antigen assay, plasma','1','0645402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0645402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา Fitzgerald factor ในพลาสม่า ',lab_ncd_note = 'Fitzgerald factor assay, plasma' where lab_icd10tm = '0646202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000822','','การตรวจวิเคราะห์หา Fitzgerald factor ในพลาสม่า ','Fitzgerald factor assay, plasma','1','0646202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0646202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา Fletcher factor พลาสม่า ',lab_ncd_note = 'Fletcher factor assay, plasma' where lab_icd10tm = '0646402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000823','','การตรวจวิเคราะห์หา Fletcher factor พลาสม่า ','Fletcher factor assay, plasma','1','0646402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0646402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา Ristocetin cofactor ในพลาสม่า',lab_ncd_note = 'Ristocetin cofactor assay, plasma' where lab_icd10tm = '0646602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000824','','การตรวจวิเคราะห์หา Ristocetin cofactor ในพลาสม่า','Ristocetin cofactor assay, plasma','1','0646602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0646602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา coagulation factor อื่นๆ มิได้จำแนก ในพลาสม่า',lab_ncd_note = 'Coagulation factor assay NEC, plasma' where lab_icd10tm = '0649002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000825','','การตรวจวิเคราะห์หา coagulation factor อื่นๆ มิได้จำแนก ในพลาสม่า','Coagulation factor assay NEC, plasma','1','0649002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0649002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา factor II inhibitor ในพลาสม่า',lab_ncd_note = 'Factor II inhibitor assay, plasma' where lab_icd10tm = '0650202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000826','','การตรวจวิเคราะห์หา factor II inhibitor ในพลาสม่า','Factor II inhibitor assay, plasma','1','0650202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0650202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา factor V inhibitor ในพลาสม่า',lab_ncd_note = 'Factor V inhibitor assay, plasma' where lab_icd10tm = '0650402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000827','','การตรวจวิเคราะห์หา factor V inhibitor ในพลาสม่า','Factor V inhibitor assay, plasma','1','0650402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0650402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา factor VII inhibitor ในพลาสม่า',lab_ncd_note = 'Factor VII inhibitor assay, plasma' where lab_icd10tm = '0650602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000828','','การตรวจวิเคราะห์หา factor VII inhibitor ในพลาสม่า','Factor VII inhibitor assay, plasma','1','0650602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0650602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา factor VIII inhibitor ในพลาสม่า',lab_ncd_note = 'Factor VIII inhibitor assay, plasma' where lab_icd10tm = '0650802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000829','','การตรวจวิเคราะห์หา factor VIII inhibitor ในพลาสม่า','Factor VIII inhibitor assay, plasma','1','0650802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0650802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา factor IX inhibitor ในพลาสม่า ',lab_ncd_note = 'Factor IX inhibitor assay, plasma' where lab_icd10tm = '0651202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000830','','การตรวจวิเคราะห์หา factor IX inhibitor ในพลาสม่า ','Factor IX inhibitor assay, plasma','1','0651202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0651202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา factor X inhibitor ในพลาสม่า',lab_ncd_note = 'Factor X inhibitor assay, plasma' where lab_icd10tm = '0651402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000831','','การตรวจวิเคราะห์หา factor X inhibitor ในพลาสม่า','Factor X inhibitor assay, plasma','1','0651402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0651402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา factor XI inhibitor ในพลาสม่า',lab_ncd_note = 'Factor XI inhibitor assay, plasma' where lab_icd10tm = '0651602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000832','','การตรวจวิเคราะห์หา factor XI inhibitor ในพลาสม่า','Factor XI inhibitor assay, plasma','1','0651602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0651602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา factor XII inhibitor ในพลาสม่า',lab_ncd_note = 'Factor XII inhibitor assay, plasma' where lab_icd10tm = '0651802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000833','','การตรวจวิเคราะห์หา factor XII inhibitor ในพลาสม่า','Factor XII inhibitor assay, plasma','1','0651802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0651802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา factor XIII inhibitor ในพลาสม่า',lab_ncd_note = 'Factor XIII inhibitor assay, plasma' where lab_icd10tm = '0652202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000834','','การตรวจวิเคราะห์หา factor XIII inhibitor ในพลาสม่า','Factor XIII inhibitor assay, plasma','1','0652202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0652202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา coagulation factor inhibitor อื่นๆ  มิได้จำแนก  ในพลาสม่า',lab_ncd_note = 'Coagulation factor inhibitor assay NEC, plasma' where lab_icd10tm = '0659002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000835','','การตรวจวิเคราะห์หา coagulation factor inhibitor อื่นๆ  มิได้จำแนก  ในพลาสม่า','Coagulation factor inhibitor assay NEC, plasma','1','0659002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0659002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวัดความสามารถในการเกิดปฏิกริยาเคมีของ alpha-2 antiplasmin ในพลาสม่า',lab_ncd_note = 'Alpha-2 antiplasmin activity, plasma' where lab_icd10tm = '0660202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000836','','การวัดความสามารถในการเกิดปฏิกริยาเคมีของ alpha-2 antiplasmin ในพลาสม่า','Alpha-2 antiplasmin activity, plasma','1','0660202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0660202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา alpha-2 antiplasmin antigen ในพลาสม่า',lab_ncd_note = 'Alpha-2 antiplasmin antigen, plasma' where lab_icd10tm = '0660402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000837','','การตรวจหา alpha-2 antiplasmin antigen ในพลาสม่า','Alpha-2 antiplasmin antigen, plasma','1','0660402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0660402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวัดความสามารถในการเกิดปฏิกิริยาเคมีของ antithrombin III ในพลาสม่า',lab_ncd_note = 'Antithrombin III activity, plasma' where lab_icd10tm = '0660602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000838','','การวัดความสามารถในการเกิดปฏิกิริยาเคมีของ antithrombin III ในพลาสม่า','Antithrombin III activity, plasma','1','0660602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0660602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา antithrombin III antigen ในพลาสม่า',lab_ncd_note = 'Antithrombin III antigen, plasma' where lab_icd10tm = '0660802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000839','','การตรวจหา antithrombin III antigen ในพลาสม่า','Antithrombin III antigen, plasma','1','0660802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0660802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาระยะเวลาที่ใช้ในการหยุดเลือดของเส้นเลือดในเลือด ',lab_ncd_note = 'Bleeding time, whole blood' where lab_icd10tm = '0661001';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000840','','การตรวจหาระยะเวลาที่ใช้ในการหยุดเลือดของเส้นเลือดในเลือด ','Bleeding time, whole blood','1','0661001'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0661001');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาระยะเวลาลิ่มเลือดสลายตัวในเลือด',lab_ncd_note = 'Clot lysis time, whole blood' where lab_icd10tm = '0661201';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000841','','การตรวจหาระยะเวลาลิ่มเลือดสลายตัวในเลือด','Clot lysis time, whole blood','1','0661201'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0661201');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาระยะเวลาลิ่มเลือดสลายตัวในพลาสม่า ',lab_ncd_note = 'Clot lysis time, plasma' where lab_icd10tm = '0661202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000842','','การตรวจหาระยะเวลาลิ่มเลือดสลายตัวในพลาสม่า ','Clot lysis time, plasma','1','0661202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0661202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาระยะเวลาลิ่มเลือดหดตัวในเลือด',lab_ncd_note = 'Clot retraction, whole blood' where lab_icd10tm = '0661401';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000843','','การตรวจหาระยะเวลาลิ่มเลือดหดตัวในเลือด','Clot retraction, whole blood','1','0661401'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0661401');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาระยะเวลาลิ่มเลือดเกาะตัวเป็นก้อนในเลือด',lab_ncd_note = 'Clotting time, whole blood' where lab_icd10tm = '0661601';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000844','','การตรวจหาระยะเวลาลิ่มเลือดเกาะตัวเป็นก้อนในเลือด','Clotting time, whole blood','1','0661601'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0661601');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาระยะเวลาลิ่มเลือดเกาะตัวเป็นก้อน  ในพลาสม่า',lab_ncd_note = 'Clotting time, plasma' where lab_icd10tm = '0661602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000845','','การตรวจหาระยะเวลาลิ่มเลือดเกาะตัวเป็นก้อน  ในพลาสม่า','Clotting time, plasma','1','0661602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0661602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาระยะเวลาเวลาในการเจือจางของพิษงูแมวเซา  ในพลาสม่า',lab_ncd_note = 'Dilute Russell viper venom time, plasma' where lab_icd10tm = '0661802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000846','','การตรวจหาระยะเวลาเวลาในการเจือจางของพิษงูแมวเซา  ในพลาสม่า','Dilute Russell viper venom time, plasma','1','0661802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0661802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา fibrin D-dimer ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Fibrin D-dimer assay, serum/plasma' where lab_icd10tm = '0662002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000847','','การตรวจวิเคราะห์หา fibrin D-dimer ในซีรั่ม / พลาสม่า','Fibrin D-dimer assay, serum/plasma','1','0662002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0662002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา fibrin D-dimer ในปัสสาวะ',lab_ncd_note = 'Fibrin D-dimer assay, urine' where lab_icd10tm = '0662003';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000848','','การตรวจวิเคราะห์หา fibrin D-dimer ในปัสสาวะ','Fibrin D-dimer assay, urine','1','0662003'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0662003');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา fibrin D-dimer ในของเหลวอื่นๆ มิได้จำแนก ',lab_ncd_note = 'Fibrin D-dimer assay, other fluid NEC' where lab_icd10tm = '0662015';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000849','','การตรวจวิเคราะห์หา fibrin D-dimer ในของเหลวอื่นๆ มิได้จำแนก ','Fibrin D-dimer assay, other fluid NEC','1','0662015'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0662015');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาผลิตภัณฑ์ย่อยสลายไฟบริน ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Fibrin degradation products, serum/plasma' where lab_icd10tm = '0662202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000850','','การตรวจหาผลิตภัณฑ์ย่อยสลายไฟบริน ในซีรั่ม / พลาสม่า','Fibrin degradation products, serum/plasma','1','0662202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0662202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาผลิตภัณฑ์จากการย่อยสลายไฟบริน  ในปัสสาวะ',lab_ncd_note = 'Fibrin degradation products, urine' where lab_icd10tm = '0662203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000851','','การตรวจหาผลิตภัณฑ์จากการย่อยสลายไฟบริน  ในปัสสาวะ','Fibrin degradation products, urine','1','0662203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0662203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาผลิตภัณฑ์จากการย่อยสลายไฟบริน  ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Fibrin degradation products, other fluid NEC' where lab_icd10tm = '0662215';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000852','','การตรวจหาผลิตภัณฑ์จากการย่อยสลายไฟบริน  ในของเหลวอื่นๆ มิได้จำแนก','Fibrin degradation products, other fluid NEC','1','0662215'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0662215');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา fibrinogen ในพลาสม่า',lab_ncd_note = 'Fibrinogen, plasma' where lab_icd10tm = '0662402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000853','','การตรวจหา fibrinogen ในพลาสม่า','Fibrinogen, plasma','1','0662402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0662402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา heparin ในพลาสม่า',lab_ncd_note = 'Heparin assay, plasma' where lab_icd10tm = '0662602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000854','','การตรวจวิเคราะห์หา heparin ในพลาสม่า','Heparin assay, plasma','1','0662602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0662602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา heparin cofactor ในพลาสม่า',lab_ncd_note = 'Heparin cofactor assay, plasma' where lab_icd10tm = '0662802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000855','','การตรวจวิเคราะห์หา heparin cofactor ในพลาสม่า','Heparin cofactor assay, plasma','1','0662802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0662802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา heparin neutralization ในพลาสม่า',lab_ncd_note = 'Heparin neutralization assay, plasma' where lab_icd10tm = '0663002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000856','','การตรวจวิเคราะห์หา heparin neutralization ในพลาสม่า','Heparin neutralization assay, plasma','1','0663002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0663002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา partial thromboplastin time ในเลือด',lab_ncd_note = 'Partial thromboplastin time, whole blood' where lab_icd10tm = '0663201';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000857','','การตรวจหา partial thromboplastin time ในเลือด','Partial thromboplastin time, whole blood','1','0663201'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0663201');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา partial thromboplastin time ในพลาสม่า',lab_ncd_note = 'Partial thromboplastin time, plasma' where lab_icd10tm = '0663202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000858','','การตรวจหา partial thromboplastin time ในพลาสม่า','Partial thromboplastin time, plasma','1','0663202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0663202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา plasmin ในพลาสม่า ',lab_ncd_note = 'Plasmin assay, plasma' where lab_icd10tm = '0663402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000859','','การตรวจวิเคราะห์หา plasmin ในพลาสม่า ','Plasmin assay, plasma','1','0663402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0663402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา plasmin inhibitor ในพลาสม่า',lab_ncd_note = 'Plasmin inhibitor assay, plasma' where lab_icd10tm = '0663602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000860','','การตรวจวิเคราะห์หา plasmin inhibitor ในพลาสม่า','Plasmin inhibitor assay, plasma','1','0663602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0663602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวัดความสามารถในการเกิดปฏิกริยาเคมีของ plasminogen ในพลาสม่า',lab_ncd_note = 'Plasminogen activity, plasma' where lab_icd10tm = '0663802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000861','','การวัดความสามารถในการเกิดปฏิกริยาเคมีของ plasminogen ในพลาสม่า','Plasminogen activity, plasma','1','0663802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0663802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา plasminogen antivator ในพลาสม่า',lab_ncd_note = 'Plasminogen antivator, plasma' where lab_icd10tm = '0664002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000862','','การตรวจหา plasminogen antivator ในพลาสม่า','Plasminogen antivator, plasma','1','0664002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0664002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา plasminogen antivator inhibitor ในพลาสม่า',lab_ncd_note = 'Plasminogen antivator inhibitor, plasma' where lab_icd10tm = '0664202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000863','','การตรวจหา plasminogen antivator inhibitor ในพลาสม่า','Plasminogen antivator inhibitor, plasma','1','0664202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0664202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบการรวมตัวของเกล็ดเลือดในเลือด',lab_ncd_note = 'Platelet aggregation test, whole blood' where lab_icd10tm = '0664401';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000864','','การทดสอบการรวมตัวของเกล็ดเลือดในเลือด','Platelet aggregation test, whole blood','1','0664401'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0664401');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบการรวมตัวของเกล็ดเลือดในพลาสม่า',lab_ncd_note = 'Platelet aggregation test,  plasma' where lab_icd10tm = '0664402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000865','','การทดสอบการรวมตัวของเกล็ดเลือดในพลาสม่า','Platelet aggregation test,  plasma','1','0664402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0664402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา platelet factor 3 ในพลาสม่า',lab_ncd_note = 'Platelet factor 3 assay, plasma' where lab_icd10tm = '0664602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000866','','การตรวจวิเคราะห์หา platelet factor 3 ในพลาสม่า','Platelet factor 3 assay, plasma','1','0664602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0664602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา platelet factor 4 ในพลาสม่า',lab_ncd_note = 'Platelet factor 4 assay, plasma' where lab_icd10tm = '0664802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000867','','การตรวจวิเคราะห์หา platelet factor 4 ในพลาสม่า','Platelet factor 4 assay, plasma','1','0664802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0664802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา platelet neutralization ในพลาสม่า',lab_ncd_note = 'Platelet neutralization assay, plasma' where lab_icd10tm = '0665002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000868','','การตรวจวิเคราะห์หา platelet neutralization ในพลาสม่า','Platelet neutralization assay, plasma','1','0665002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0665002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การศึกษาการหลั่งของเกล็ดเลือดในพลาสม่า',lab_ncd_note = 'Platelet secretion study, plasma' where lab_icd10tm = '0665202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000869','','การศึกษาการหลั่งของเกล็ดเลือดในพลาสม่า','Platelet secretion study, plasma','1','0665202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0665202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวัดความสามารถในการเกิดปฏิกิริยาเคมีของ protein C ในพลาสม่า ',lab_ncd_note = 'Protein C activity, plasma' where lab_icd10tm = '0666202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000870','','การวัดความสามารถในการเกิดปฏิกิริยาเคมีของ protein C ในพลาสม่า ','Protein C activity, plasma','1','0666202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0666202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา protein C antigen ในพลาสม่า',lab_ncd_note = 'Protein C antigen assay, plasma' where lab_icd10tm = '0666402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000871','','การตรวจวิเคราะห์หา protein C antigen ในพลาสม่า','Protein C antigen assay, plasma','1','0666402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0666402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวัดความสามารถในการเกิดปฏิกิริยาเคมีของ protein S ในพลาสม่า',lab_ncd_note = 'Protein S activity, plasma' where lab_icd10tm = '0666602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000872','','การวัดความสามารถในการเกิดปฏิกิริยาเคมีของ protein S ในพลาสม่า','Protein S activity, plasma','1','0666602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0666602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา protein S antigen ในพลาสม่า',lab_ncd_note = 'Protein S antigen assay, plasma' where lab_icd10tm = '0666802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000873','','การตรวจวิเคราะห์หา protein S antigen ในพลาสม่า','Protein S antigen assay, plasma','1','0666802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0666802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา prothrombin time ในเลือด',lab_ncd_note = 'Prothrombin time, whole blood' where lab_icd10tm = '0667201';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000874','','การตรวจหา prothrombin time ในเลือด','Prothrombin time, whole blood','1','0667201'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0667201');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา prothrombin time ในพลาสม่า',lab_ncd_note = 'Prothrombin time, plasma' where lab_icd10tm = '0667202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000875','','การตรวจหา prothrombin time ในพลาสม่า','Prothrombin time, plasma','1','0667202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0667202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา Reptilase time ในพลาสม่า',lab_ncd_note = 'Reptilase time, plasma' where lab_icd10tm = '0667402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000876','','การตรวจหา Reptilase time ในพลาสม่า','Reptilase time, plasma','1','0667402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0667402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา Thrombin time ในพลาสม่า',lab_ncd_note = 'Thrombin time, plasma' where lab_icd10tm = '0667602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000877','','การตรวจหา Thrombin time ในพลาสม่า','Thrombin time, plasma','1','0667602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0667602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ HLA A-B-C typing ในเลือด',lab_ncd_note = 'HLA A-B-C typing, whole blood' where lab_icd10tm = '0670201';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000878','','การตรวจ HLA A-B-C typing ในเลือด','HLA A-B-C typing, whole blood','1','0670201'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0670201');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ HLA A-B-C typing ในเม็ดเลือดขาว',lab_ncd_note = 'HLA A-B-C typing, WBC' where lab_icd10tm = '0670216';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000879','','การตรวจ HLA A-B-C typing ในเม็ดเลือดขาว','HLA A-B-C typing, WBC','1','0670216'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0670216');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ HLA A-B-C typing ในเนื้อเยื่ออื่นๆ มิได้จำแนก',lab_ncd_note = 'HLA A-B-C typing, tissue NEC' where lab_icd10tm = '0670250';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000880','','การตรวจ HLA A-B-C typing ในเนื้อเยื่ออื่นๆ มิได้จำแนก','HLA A-B-C typing, tissue NEC','1','0670250'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0670250');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ HLA-B27 typing ในเลือด',lab_ncd_note = 'HLA-B27 typing, whole blood' where lab_icd10tm = '0670401';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000881','','การตรวจ HLA-B27 typing ในเลือด','HLA-B27 typing, whole blood','1','0670401'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0670401');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ HLA-B27 typing ในเม็ดเลือดขาว',lab_ncd_note = 'HLA-B27 typing, WBC' where lab_icd10tm = '0670416';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000882','','การตรวจ HLA-B27 typing ในเม็ดเลือดขาว','HLA-B27 typing, WBC','1','0670416'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0670416');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ HLA-DR typing ในเลือด',lab_ncd_note = 'HLA-DR typing, whole blood' where lab_icd10tm = '0670601';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000883','','การตรวจ HLA-DR typing ในเลือด','HLA-DR typing, whole blood','1','0670601'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0670601');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ HLA-DR typing ในเซรั่ม / พลาสม่า',lab_ncd_note = 'HLA-DR typing, serum/plasma' where lab_icd10tm = '0670602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000884','','การตรวจ HLA-DR typing ในเซรั่ม / พลาสม่า','HLA-DR typing, serum/plasma','1','0670602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0670602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ HLA-DR typing ในเม็ดเลือดขาว',lab_ncd_note = 'HLA-DR typing, WBC' where lab_icd10tm = '0670616';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000885','','การตรวจ HLA-DR typing ในเม็ดเลือดขาว','HLA-DR typing, WBC','1','0670616'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0670616');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ HLA-DR typing ในเนื้อเยื่ออื่นๆ มิได้จำแนก',lab_ncd_note = 'HLA-DR typing, tissue NEC' where lab_icd10tm = '0670650';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000886','','การตรวจ HLA-DR typing ในเนื้อเยื่ออื่นๆ มิได้จำแนก','HLA-DR typing, tissue NEC','1','0670650'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0670650');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ HLA-DQ typing ในเลือด',lab_ncd_note = 'HLA-DQ typing, whole blood' where lab_icd10tm = '0670801';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000887','','การตรวจ HLA-DQ typing ในเลือด','HLA-DQ typing, whole blood','1','0670801'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0670801');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ HLA-DQ typing ในเซรั่ม, พลาสม่า',lab_ncd_note = 'HLA-DQ typing, serum/plasma' where lab_icd10tm = '0670802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000888','','การตรวจ HLA-DQ typing ในเซรั่ม, พลาสม่า','HLA-DQ typing, serum/plasma','1','0670802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0670802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ HLA-DQ typing ในเม็ดเลือดขาว',lab_ncd_note = 'HLA-DQ typing, WBC' where lab_icd10tm = '0670816';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000889','','การตรวจ HLA-DQ typing ในเม็ดเลือดขาว','HLA-DQ typing, WBC','1','0670816'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0670816');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ HLA-DQ typing ในเนื้อเยื่ออื่นๆ มิได้จำแนก',lab_ncd_note = 'HLA-DQ typing, tissue NEC' where lab_icd10tm = '0670850';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000890','','การตรวจ HLA-DQ typing ในเนื้อเยื่ออื่นๆ มิได้จำแนก','HLA-DQ typing, tissue NEC','1','0670850'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0670850');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ histocompatibility crossmatch ในเลือด',lab_ncd_note = 'Histocompatibility crossmatch, whole blood' where lab_icd10tm = '0671201';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000891','','การตรวจ histocompatibility crossmatch ในเลือด','Histocompatibility crossmatch, whole blood','1','0671201'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0671201');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ histocompatibility crossmatch ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Histocompatibility crossmatch, serum/plasma' where lab_icd10tm = '0671202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000892','','การตรวจ histocompatibility crossmatch ในซีรั่ม / พลาสม่า','Histocompatibility crossmatch, serum/plasma','1','0671202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0671202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ histocompatibility crossmatch ในเม็ดเลือดขาว',lab_ncd_note = 'Histocompatibility crossmatch, WBC' where lab_icd10tm = '0671216';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000893','','การตรวจ histocompatibility crossmatch ในเม็ดเลือดขาว','Histocompatibility crossmatch, WBC','1','0671216'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0671216');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ histocompatibility crossmatch ในเนื้อเยื่อ',lab_ncd_note = 'Histocompatibility crossmatch, tissue' where lab_icd10tm = '0671250';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000894','','การตรวจ histocompatibility crossmatch ในเนื้อเยื่อ','Histocompatibility crossmatch, tissue','1','0671250'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0671250');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหา histocompatibility antibody ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Histocompatibility antibody screen, serum/plasma' where lab_icd10tm = '0671602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000895','','การตรวจคัดกรองหา histocompatibility antibody ในซีรั่ม / พลาสม่า','Histocompatibility antibody screen, serum/plasma','1','0671602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0671602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหมู่เลือด ABO (reverse) ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Blood group typing - ABO (reverse), serum/plasma' where lab_icd10tm = '0680202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000896','','การตรวจหมู่เลือด ABO (reverse) ในซีรั่ม / พลาสม่า','Blood group typing - ABO (reverse), serum/plasma','1','0680202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0680202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหมู่เลือด ABO (forward) ในเม็ดเลือดแดง',lab_ncd_note = 'Blood group typing - ABO (forward), RBC' where lab_icd10tm = '0680217';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000897','','การตรวจหมู่เลือด ABO (forward) ในเม็ดเลือดแดง','Blood group typing - ABO (forward), RBC','1','0680217'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0680217');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหมู่เลือด Rh Phenotype ในเม็ดเลือดแดง',lab_ncd_note = 'Blood group typing - Rh Phenotype, RBC' where lab_icd10tm = '0680417';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000898','','การตรวจหมู่เลือด Rh Phenotype ในเม็ดเลือดแดง','Blood group typing - Rh Phenotype, RBC','1','0680417'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0680417');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหมู่เลือด Rh(D), RBC ในเม็ดเลือดแดง',lab_ncd_note = 'Blood group typing - Rh(D), RBC' where lab_icd10tm = '0680617';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000899','','การตรวจหมู่เลือด Rh(D), RBC ในเม็ดเลือดแดง','Blood group typing - Rh(D), RBC','1','0680617'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0680617');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การจัดหมู่เลือด RBC antigens อื่น ๆ มากกว่า ABO และ Rh ในเม็ดเลือดแดง',lab_ncd_note = 'Blood group typing - RBC antigens other than ABO and Rh, RBC' where lab_icd10tm = '0680817';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000900','','การจัดหมู่เลือด RBC antigens อื่น ๆ มากกว่า ABO และ Rh ในเม็ดเลือดแดง','Blood group typing - RBC antigens other than ABO and Rh, RBC','1','0680817'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0680817');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบ Antiglobulin โดยตรงในซีรั่ม / พลาสม่า',lab_ncd_note = 'Antiglobulin test - direct, serum/plasma' where lab_icd10tm = '0681017';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000901','','การทดสอบ Antiglobulin โดยตรงในซีรั่ม / พลาสม่า','Antiglobulin test - direct, serum/plasma','1','0681017'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0681017');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบ Antiglobulin ทางอ้อมในซีรั่ม / พลาสม่า',lab_ncd_note = 'Antiglobulin test - indirect, serum/plasma' where lab_icd10tm = '0681202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000902','','การทดสอบ Antiglobulin ทางอ้อมในซีรั่ม / พลาสม่า','Antiglobulin test - indirect, serum/plasma','1','0681202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0681202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาแอนติบอดี (คัดกรอง) ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Antibody detection (screen), serum/plasma' where lab_icd10tm = '0681402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000903','','การตรวจหาแอนติบอดี (คัดกรอง) ในซีรั่ม / พลาสม่า','Antibody detection (screen), serum/plasma','1','0681402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0681402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การระบุแอนติบอดีในซีรั่ม / พลาสม่า',lab_ncd_note = 'Antibody identification, serum/plasma' where lab_icd10tm = '0681602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000904','','การระบุแอนติบอดีในซีรั่ม / พลาสม่า','Antibody identification, serum/plasma','1','0681602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0681602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาความเข้มข้นของแอนติบอดี (ต่อแอนติเจน) ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Antibody titration (per antigen), serum/plasma' where lab_icd10tm = '0681802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000905','','การตรวจหาความเข้มข้นของแอนติบอดี (ต่อแอนติเจน) ในซีรั่ม / พลาสม่า','Antibody titration (per antigen), serum/plasma','1','0681802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0681802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจการชะของแอนติบอดีในเม็ดเลือดแดง',lab_ncd_note = 'Antibody elution, RBC' where lab_icd10tm = '0682017';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000906','','การตรวจการชะของแอนติบอดีในเม็ดเลือดแดง','Antibody elution, RBC','1','0682017'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0682017');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจการการดูดซึมของแอนติบอดี ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Antibody absorption, serum/plasma' where lab_icd10tm = '0682202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000907','','การตรวจการการดูดซึมของแอนติบอดี ในซีรั่ม / พลาสม่า','Antibody absorption, serum/plasma','1','0682202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0682202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจการการดูดซึมของแอนติบอดี ในเม็ดเลือดแดง',lab_ncd_note = 'Antibody absorption, RBC' where lab_icd10tm = '0682217';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000908','','การตรวจการการดูดซึมของแอนติบอดี ในเม็ดเลือดแดง','Antibody absorption, RBC','1','0682217'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0682217');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'เทคนิค Pre-warm ในเม็ดเลือดแดง',lab_ncd_note = 'Pre-warm technique, RBC' where lab_icd10tm = '0682417';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000909','','เทคนิค Pre-warm ในเม็ดเลือดแดง','Pre-warm technique, RBC','1','0682417'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0682417');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การบำบัดเซลล์ด้วยเอนไซม์ในเม็ดเลือดแดง',lab_ncd_note = 'Treatment of cells with enzyme, RBC' where lab_icd10tm = '0682617';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000910','','การบำบัดเซลล์ด้วยเอนไซม์ในเม็ดเลือดแดง','Treatment of cells with enzyme, RBC','1','0682617'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0682617');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'Crossmatch – เต็ม  ในแต่ละหน่วยของเลือด',lab_ncd_note = 'Crossmatch - full, each unit of blood' where lab_icd10tm = '0683201';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000911','','Crossmatch – เต็ม  ในแต่ละหน่วยของเลือด','Crossmatch - full, each unit of blood','1','0683201'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0683201');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'Crossmatch – สปินทันที  ในแต่ละหน่วยของเลือด ',lab_ncd_note = 'Crossmatch - immediate spin, each unit of blood' where lab_icd10tm = '0683401';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000912','','Crossmatch – สปินทันที  ในแต่ละหน่วยของเลือด ','Crossmatch - immediate spin, each unit of blood','1','0683401'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0683401');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'Cold agglutinin titer ในเซรั่ม / พลาสม่า ',lab_ncd_note = 'Cold agglutinin titer, serum/plasma' where lab_icd10tm = '0684202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000913','','Cold agglutinin titer ในเซรั่ม / พลาสม่า ','Cold agglutinin titer, serum/plasma','1','0684202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0684202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การศึกษา Autohemolysis ในซีรั่ม / พลาสมา',lab_ncd_note = 'Autohemolysis studies, serum/plasm' where lab_icd10tm = '0684402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000914','','การศึกษา Autohemolysis ในซีรั่ม / พลาสมา','Autohemolysis studies, serum/plasm','1','0684402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0684402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การศึกษา autohemolysis ในเม็ดเลือดแดง',lab_ncd_note = 'Autohemolysis studies, RBC' where lab_icd10tm = '0684417';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000915','','การศึกษา autohemolysis ในเม็ดเลือดแดง','Autohemolysis studies, RBC','1','0684417'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0684417');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา hemolysin ในเลือด ',lab_ncd_note = 'Hemolysin detection, whole blood' where lab_icd10tm = '0684601';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000916','','การตรวจหา hemolysin ในเลือด ','Hemolysin detection, whole blood','1','0684601'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0684601');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจสอบปฏิกิริยาตอบสนองจากการให้เลือด ',lab_ncd_note = 'Transfusion reaction investigation' where lab_icd10tm = '0687200';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000917','','การตรวจสอบปฏิกิริยาตอบสนองจากการให้เลือด ','Transfusion reaction investigation','1','0687200'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0687200');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเตรียม cryoprecipitate ในแต่ละหน่วย',lab_ncd_note = 'Cryoprecipitate preparation, each unit' where lab_icd10tm = '0690200';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000918','','การเตรียม cryoprecipitate ในแต่ละหน่วย','Cryoprecipitate preparation, each unit','1','0690200'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0690200');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การละลาย cryoprecipitate ในแต่ละหน่วย',lab_ncd_note = 'Cryoprecipitate thawing, each unit' where lab_icd10tm = '0690400';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000919','','การละลาย cryoprecipitate ในแต่ละหน่วย','Cryoprecipitate thawing, each unit','1','0690400'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0690400');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การรวม cryoprecipitate  รวมครั้งสุดท้าย (ไม่คำนึงถึงจำนวนของหน่วยเดิม)',lab_ncd_note = 'Cryoprecipitate pooling, final pool (regardless of number of original units)' where lab_icd10tm = '0690600';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000920','','การรวม cryoprecipitate  รวมครั้งสุดท้าย (ไม่คำนึงถึงจำนวนของหน่วยเดิม)','Cryoprecipitate pooling, final pool (regardless of number of original units)','1','0690600'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0690600');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเตรียมพลาสมาแช่แข็งในแต่ละหน่วย',lab_ncd_note = 'Frozen plasma preparation, each unit' where lab_icd10tm = '0691200';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000921','','การเตรียมพลาสมาแช่แข็งในแต่ละหน่วย','Frozen plasma preparation, each unit','1','0691200'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0691200');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การละลายพลาสมาแช่แข็งในแต่ละหน่วย ',lab_ncd_note = 'Frozen plasma thawing, each unit' where lab_icd10tm = '0691400';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000922','','การละลายพลาสมาแช่แข็งในแต่ละหน่วย ','Frozen plasma thawing, each unit','1','0691400'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0691400');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การแช่แข็งเลือดที่ถูกแช่แข็ง (เม็ดเลือดแดง หรือเลือด) ในแต่ละหน่วย',lab_ncd_note = 'Frozen  blood (RBC or whole blood) freezing, each unit' where lab_icd10tm = '0692200';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000923','','การแช่แข็งเลือดที่ถูกแช่แข็ง (เม็ดเลือดแดง หรือเลือด) ในแต่ละหน่วย','Frozen  blood (RBC or whole blood) freezing, each unit','1','0692200'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0692200');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การละลายเลือดที่ถูกแช่แข็ง (เม็ดเลือดแดง หรือเลือด) ในแต่ละหน่วย',lab_ncd_note = 'Frozen  blood (RBC or whole blood) thawing, each unit' where lab_icd10tm = '0692400';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000924','','การละลายเลือดที่ถูกแช่แข็ง (เม็ดเลือดแดง หรือเลือด) ในแต่ละหน่วย','Frozen  blood (RBC or whole blood) thawing, each unit','1','0692400'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0692400');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเตรียมเม็ดเลือดแดงบรรจุถุงในแต่ละหน่วย',lab_ncd_note = 'Packed RBC preparation, each unit' where lab_icd10tm = '0693200';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000925','','การเตรียมเม็ดเลือดแดงบรรจุถุงในแต่ละหน่วย','Packed RBC preparation, each unit','1','0693200'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0693200');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การล้างเม็ดเลือดแดง (สำหรับการถ่ายเลือด) ในแต่ละหน่วย ',lab_ncd_note = 'Washing RBC (for transfusion), each unit' where lab_icd10tm = '0693400';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000926','','การล้างเม็ดเลือดแดง (สำหรับการถ่ายเลือด) ในแต่ละหน่วย ','Washing RBC (for transfusion), each unit','1','0693400'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0693400');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การกรอง เม็ดเลือดแดง (สำหรับการถ่าย) ในแต่ละหน่วย',lab_ncd_note = 'Filtering RBC (for transfusion), each unit' where lab_icd10tm = '0693600';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000927','','การกรอง เม็ดเลือดแดง (สำหรับการถ่าย) ในแต่ละหน่วย','Filtering RBC (for transfusion), each unit','1','0693600'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0693600');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเตรียมเกล็ดเลือดเข้มข้นในแต่ละหน่วย',lab_ncd_note = 'Platelet concentrate preparation, each unit' where lab_icd10tm = '0694000';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000928','','การเตรียมเกล็ดเลือดเข้มข้นในแต่ละหน่วย','Platelet concentrate preparation, each unit','1','0694000'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0694000');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การรวมเกล็ดเลือดเข??้มข้น  รวมครั้งสุดท้าย (ไม่คำนึงถึงจำนวนของหน่วยเดิม)',lab_ncd_note = 'Platelet concentrate pooling, final pool (regardless of number of original units)' where lab_icd10tm = '0694200';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000929','','การรวมเกล็ดเลือดเข??้มข้น  รวมครั้งสุดท้าย (ไม่คำนึงถึงจำนวนของหน่วยเดิม)','Platelet concentrate pooling, final pool (regardless of number of original units)','1','0694200'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0694200');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การล้างเกล็ดเลือด (สำหรับฉีด) ในแต่ละหน่วย',lab_ncd_note = 'Washing platelet (for infusion), each unit' where lab_icd10tm = '0694400';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000930','','การล้างเกล็ดเลือด (สำหรับฉีด) ในแต่ละหน่วย','Washing platelet (for infusion), each unit','1','0694400'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0694400');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การกรองเกล็ดเลือด (สำหรับฉีด) ในแต่ละหน่วย',lab_ncd_note = 'Filtering platelet (for infusion), each unit' where lab_icd10tm = '0694600';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000931','','การกรองเกล็ดเลือด (สำหรับฉีด) ในแต่ละหน่วย','Filtering platelet (for infusion), each unit','1','0694600'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0694600');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเตรียมพลาสมาที่มีเกล็ดเลือดมากในแต่ละหน่วย',lab_ncd_note = 'Platelet rich plasma preparation, each unit' where lab_icd10tm = '0694800';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000932','','การเตรียมพลาสมาที่มีเกล็ดเลือดมากในแต่ละหน่วย','Platelet rich plasma preparation, each unit','1','0694800'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0694800');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การฉายรังสีผลิตภัณฑ์เลือดในแต่ละหน่วย',lab_ncd_note = 'Irradiation of blood products, each unit' where lab_icd10tm = '0695200';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000933','','การฉายรังสีผลิตภัณฑ์เลือดในแต่ละหน่วย','Irradiation of blood products, each unit','1','0695200'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0695200');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การแยกเลือดแต่ละหน่วยให้ลงตัว ในแต่ละหน่วย ',lab_ncd_note = 'Separation of blood unit into aliquots, each (aliquoted) unit' where lab_icd10tm = '0695400';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000934','','การแยกเลือดแต่ละหน่วยให้ลงตัว ในแต่ละหน่วย ','Separation of blood unit into aliquots, each (aliquoted) unit','1','0695400'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0695400');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'Reconstitution ของเลือดหน่วย [เช่น เพิ่มพลาสม่าเพื่อ RBC]  แต่ละหน่วย',lab_ncd_note = 'Reconstitution of blood unit [e.g. adding plasma to RBC], each unit' where lab_icd10tm = '0695600';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000935','','Reconstitution ของเลือดหน่วย [เช่น เพิ่มพลาสม่าเพื่อ RBC]  แต่ละหน่วย','Reconstitution of blood unit [e.g. adding plasma to RBC], each unit','1','0695600'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0695600');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'Reconstitution ผลิตภัณฑ์ fractionated  แต่ละหน่วย / ขวดเล็ก ',lab_ncd_note = 'Reconstitution of fractionated products, each unit/vial' where lab_icd10tm = '0695800';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000936','','Reconstitution ผลิตภัณฑ์ fractionated  แต่ละหน่วย / ขวดเล็ก ','Reconstitution of fractionated products, each unit/vial','1','0695800'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0695800');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การรวมกันของผลิตภัณฑ์เลือด  มิได้จำแนกที่ใด  รวมครั้งสุดท้าย (ไม่คำนึงถึงจำนวนของหน่วยเดิม)',lab_ncd_note = 'Pooling of blood products NEC, final pool (regardless of number of orginal units)' where lab_icd10tm = '0696000';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000937','','การรวมกันของผลิตภัณฑ์เลือด  มิได้จำแนกที่ใด  รวมครั้งสุดท้าย (ไม่คำนึงถึงจำนวนของหน่วยเดิม)','Pooling of blood products NEC, final pool (regardless of number of orginal units)','1','0696000'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0696000');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเพาะเลี้ยงเซลล์ต้นกำเนิด (ต่อการเก็บเกี่ยว)  เซลล์ต้นกำเนิด ',lab_ncd_note = 'Stem cell culture (per harvest), stem cells' where lab_icd10tm = '0697219';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000938','','การเพาะเลี้ยงเซลล์ต้นกำเนิด (ต่อการเก็บเกี่ยว)  เซลล์ต้นกำเนิด ','Stem cell culture (per harvest), stem cells','1','0697219'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0697219');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเพาะเลี้ยงเซลล์ต้นกำเนิด (ต่อการเก็บเกี่ยว)  ไขกระดูก ',lab_ncd_note = 'Stem cell culture (per harvest), bone marrow' where lab_icd10tm = '0697227';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000939','','การเพาะเลี้ยงเซลล์ต้นกำเนิด (ต่อการเก็บเกี่ยว)  ไขกระดูก ','Stem cell culture (per harvest), bone marrow','1','0697227'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0697227');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การประมวลผลสำหรับการเก็บรักษาเซลล์ต้นกำเนิด (ต่อการเก็บเกี่ยว)  ไขกระดูก',lab_ncd_note = 'Stem cell processing for cryopreservation (per harvest), bone marrow' where lab_icd10tm = '0697419';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000940','','การประมวลผลสำหรับการเก็บรักษาเซลล์ต้นกำเนิด (ต่อการเก็บเกี่ยว)  ไขกระดูก','Stem cell processing for cryopreservation (per harvest), bone marrow','1','0697419'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0697419');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'กระบวนการปลูกถ่ายไขกระดูก (ต่อการเก็บเกี่ยว)  ไขกระดูก ',lab_ncd_note = 'Bone marrow processing for transplant (per harvest), bone marrow' where lab_icd10tm = '0698227';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000941','','กระบวนการปลูกถ่ายไขกระดูก (ต่อการเก็บเกี่ยว)  ไขกระดูก ','Bone marrow processing for transplant (per harvest), bone marrow','1','0698227'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0698227');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'กระบวนการเก็บรักษาไขกระดูกแช่เย็น (ต่อการเก็บเกี่ยว)  ไขกระดูก',lab_ncd_note = 'Bone marrow processing for cryopreservation (per harvest), bone marrow' where lab_icd10tm = '0698427';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000942','','กระบวนการเก็บรักษาไขกระดูกแช่เย็น (ต่อการเก็บเกี่ยว)  ไขกระดูก','Bone marrow processing for cryopreservation (per harvest), bone marrow','1','0698427'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0698427');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'จัดเตรียมและการจัดการผลผลิต apheresis  ผู้บริจาคเพียงครั้งเดียว',lab_ncd_note = 'Preparation and handling of single donor apheresis products' where lab_icd10tm = '0698600';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000943','','จัดเตรียมและการจัดการผลผลิต apheresis  ผู้บริจาคเพียงครั้งเดียว','Preparation and handling of single donor apheresis products','1','0698600'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0698600');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบ allergen specific IgE antibody ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Allergen specific IgE antibody test, serum/plasma' where lab_icd10tm = '0700202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000944','','การทดสอบ allergen specific IgE antibody ในซีรั่ม / พลาสม่า ','Allergen specific IgE antibody test, serum/plasma','1','0700202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0700202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา antigen capture อื่นๆ มิได้ระบุ ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Antigen capture assay NOS, serum/plasma' where lab_icd10tm = '0700402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000945','','การตรวจวิเคราะห์หา antigen capture อื่นๆ มิได้ระบุ ในซีรั่ม / พลาสม่า','Antigen capture assay NOS, serum/plasma','1','0700402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0700402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา anti-streptolysin O titer ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Anti-streptolysin O titer, serum/plasma' where lab_icd10tm = '0700602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000946','','การตรวจหา anti-streptolysin O titer ในซีรั่ม / พลาสม่า','Anti-streptolysin O titer, serum/plasma','1','0700602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0700602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การศึกษา cell marker ในเนื้อเยื่ออื่นๆ มิได้จำแนก',lab_ncd_note = 'Cell marker studies, tissue NEC' where lab_icd10tm = '0700850';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000947','','การศึกษา cell marker ในเนื้อเยื่ออื่นๆ มิได้จำแนก','Cell marker studies, tissue NEC','1','0700850'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0700850');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา cytokine ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Cytokine assay, serum/plasma' where lab_icd10tm = '0701002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000948','','การตรวจวิเคราะห์หา cytokine ในซีรั่ม / พลาสม่า','Cytokine assay, serum/plasma','1','0701002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0701002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา cytotoxic ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Cytotoxic assay, serum/plasma' where lab_icd10tm = '0701202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000949','','การตรวจวิเคราะห์หา cytotoxic ในซีรั่ม / พลาสม่า','Cytotoxic assay, serum/plasma','1','0701202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0701202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา febrile agglutination ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Febrile agglutination, serum/plasma' where lab_icd10tm = '0701402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000950','','การตรวจหา febrile agglutination ในซีรั่ม / พลาสม่า','Febrile agglutination, serum/plasma','1','0701402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0701402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา Heterophile antibody ในเซรั่ม / พลาสม่า',lab_ncd_note = 'Heterophile antibody, serum/plasma' where lab_icd10tm = '0701602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000951','','การตรวจหา Heterophile antibody ในเซรั่ม / พลาสม่า','Heterophile antibody, serum/plasma','1','0701602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0701602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา Immune complex ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Immune complex assay, serum/plasma' where lab_icd10tm = '0701802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000952','','การตรวจวิเคราะห์หา Immune complex ในซีรั่ม / พลาสม่า','Immune complex assay, serum/plasma','1','0701802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0701802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา Kappa light chains และ lambda light chains ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Kappa and lambda light chains, serum/plasma' where lab_icd10tm = '0702002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000953','','การตรวจหา Kappa light chains และ lambda light chains ในซีรั่ม / พลาสม่า','Kappa and lambda light chains, serum/plasma','1','0702002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0702002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา Kappa light chains และ lambda light chains ในปัสสาวะ ',lab_ncd_note = 'Kappa and lambda light chains, urine' where lab_icd10tm = '0702003';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000954','','การตรวจหา Kappa light chains และ lambda light chains ในปัสสาวะ ','Kappa and lambda light chains, urine','1','0702003'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0702003');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา Kappa light chains และ lambda light chains ในเม็ดเลือดขาว',lab_ncd_note = 'Kappa and lambda light chains, WBC' where lab_icd10tm = '0702016';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000955','','การตรวจหา Kappa light chains และ lambda light chains ในเม็ดเลือดขาว','Kappa and lambda light chains, WBC','1','0702016'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0702016');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา Kappa light chains และ lambda light chains ในเนื้อเยื่ออื่นๆ มิได้จำแนก',lab_ncd_note = 'Kappa and lambda light chains, tissue NEC' where lab_icd10tm = '0702050';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000956','','การตรวจหา Kappa light chains และ lambda light chains ในเนื้อเยื่ออื่นๆ มิได้จำแนก','Kappa and lambda light chains, tissue NEC','1','0702050'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0702050');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวัดความสามารถในการเกิดปฏิกิริยาเคมีของ Leukocyte chemotactic ในเลือด ',lab_ncd_note = 'Leukocyte chemotactic activity, whole blood' where lab_icd10tm = '0702201';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000957','','การวัดความสามารถในการเกิดปฏิกิริยาเคมีของ Leukocyte chemotactic ในเลือด ','Leukocyte chemotactic activity, whole blood','1','0702201'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0702201');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวัดความสามารถในการเกิดปฏิกิริยาเคมีของ Leukocyte phagocytic ในเลือด',lab_ncd_note = 'Leukocyte phagocytic activity, whole blood' where lab_icd10tm = '0702401';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000958','','การวัดความสามารถในการเกิดปฏิกิริยาเคมีของ Leukocyte phagocytic ในเลือด','Leukocyte phagocytic activity, whole blood','1','0702401'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0702401');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ leukocyte phenotyping ในเม็ดเลือดขาว ',lab_ncd_note = 'Leukocyte phenotyping, WBC' where lab_icd10tm = '0702616';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000959','','การตรวจ leukocyte phenotyping ในเม็ดเลือดขาว ','Leukocyte phenotyping, WBC','1','0702616'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0702616');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจประเมิน lymphocyte B – cell ในเลือด',lab_ncd_note = 'Lymphocyte B-cell evaluation, whole blood' where lab_icd10tm = '0702801';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000960','','การตรวจประเมิน lymphocyte B – cell ในเลือด','Lymphocyte B-cell evaluation, whole blood','1','0702801'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0702801');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจประเมิน lymphocyte T-cell ในเลือด',lab_ncd_note = 'Lymphocyte T-cell evaluation, whole blood' where lab_icd10tm = '0703001';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000961','','การตรวจประเมิน lymphocyte T-cell ในเลือด','Lymphocyte T-cell evaluation, whole blood','1','0703001'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0703001');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การศึกษาการกระตุ้นเม็ดเลือดขาวในเซรั่ม / พลาสม่า',lab_ncd_note = 'Lymphocyte stimulation studies, serum/plasma' where lab_icd10tm = '0703002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000962','','การศึกษาการกระตุ้นเม็ดเลือดขาวในเซรั่ม / พลาสม่า','Lymphocyte stimulation studies, serum/plasma','1','0703002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0703002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การศึกษาการกระตุ้นเม็ดเลือดขาวในเม็ดเลือดขาว',lab_ncd_note = 'Lymphocyte stimulation studies, WBC' where lab_icd10tm = '0703216';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000963','','การศึกษาการกระตุ้นเม็ดเลือดขาวในเม็ดเลือดขาว','Lymphocyte stimulation studies, WBC','1','0703216'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0703216');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา lymphocyte surface immunoglobulins ในเลือด',lab_ncd_note = 'Lymphocyte surface immunoglobulins, whole blood' where lab_icd10tm = '0703401';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000964','','การตรวจหา lymphocyte surface immunoglobulins ในเลือด','Lymphocyte surface immunoglobulins, whole blood','1','0703401'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0703401');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา lymphocyte transformation อื่นๆ มิได้ระบุ ในเลือด  ',lab_ncd_note = 'Lymphocyte transformation assay NOS, whole blood' where lab_icd10tm = '0703601';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000965','','การตรวจวิเคราะห์หา lymphocyte transformation อื่นๆ มิได้ระบุ ในเลือด  ','Lymphocyte transformation assay NOS, whole blood','1','0703601'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0703601');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา lymphocyte transformation ร่วมกับการเพาะเลี้ยง mitogen ในเลือด',lab_ncd_note = 'Lymphocyte transformation assay with mitogen culture, whole blood' where lab_icd10tm = '0703801';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000966','','การตรวจวิเคราะห์หา lymphocyte transformation ร่วมกับการเพาะเลี้ยง mitogen ในเลือด','Lymphocyte transformation assay with mitogen culture, whole blood','1','0703801'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0703801');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา lymphokine ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Lymphokine assay, serum/plasma' where lab_icd10tm = '0704002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000967','','การตรวจวิเคราะห์หา lymphokine ในซีรั่ม / พลาสม่า','Lymphokine assay, serum/plasma','1','0704002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0704002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา mixed lymphocyte culture ในเลือด',lab_ncd_note = 'Mixed lymphocyte culture assay, whole blood' where lab_icd10tm = '0704201';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000968','','การตรวจวิเคราะห์หา mixed lymphocyte culture ในเลือด','Mixed lymphocyte culture assay, whole blood','1','0704201'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0704201');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา mononuclear cell ในเลือด',lab_ncd_note = 'Mononuclear cell assay, whole blood' where lab_icd10tm = '0704401';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000969','','การตรวจวิเคราะห์หา mononuclear cell ในเลือด','Mononuclear cell assay, whole blood','1','0704401'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0704401');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจวิเคราะห์หา natural killer cell ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Natural killer cell assay, serum/plasma' where lab_icd10tm = '0704602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000970','','การตรวจวิเคราะห์หา natural killer cell ในซีรั่ม / พลาสม่า','Natural killer cell assay, serum/plasma','1','0704602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0704602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบ nitroblue tetrazolium ในซี??รั่ม / พลาสม่า',lab_ncd_note = 'Nitroblue tetrazolium test, serum/plasma' where lab_icd10tm = '0704802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000971','','การทดสอบ nitroblue tetrazolium ในซี??รั่ม / พลาสม่า','Nitroblue tetrazolium test, serum/plasma','1','0704802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0704802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา platelet associated immunoglobulins ในเลือด ',lab_ncd_note = 'Platelet associated immunoglobulins, whole blood' where lab_icd10tm = '0705001';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000972','','การตรวจหา platelet associated immunoglobulins ในเลือด ','Platelet associated immunoglobulins, whole blood','1','0705001'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0705001');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา platelet associated immunoglobulins ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Platelet associated immunoglobulins, serum/plasma' where lab_icd10tm = '0705002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000973','','การตรวจหา platelet associated immunoglobulins ในซีรั่ม / พลาสม่า','Platelet associated immunoglobulins, serum/plasma','1','0705002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0705002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา rheumatoid factor ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Rheumatoid factor, serum/plasma' where lab_icd10tm = '0705602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000974','','การตรวจหา rheumatoid factor ในซีรั่ม / พลาสม่า','Rheumatoid factor, serum/plasma','1','0705602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0705602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา rheumatoid factor ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Rheumatoid factor, other fluid NEC' where lab_icd10tm = '0705615';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000975','','การตรวจหา rheumatoid factor ในของเหลวอื่นๆ มิได้จำแนก','Rheumatoid factor, other fluid NEC','1','0705615'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0705615');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา acetylcholine receptor antibody ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Acetylcholine receptor antibody, serum/plasma' where lab_icd10tm = '0710202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000976','','การตรวจหา acetylcholine receptor antibody ในซีรั่ม / พลาสม่า','Acetylcholine receptor antibody, serum/plasma','1','0710202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0710202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา adrenal cortex antibody ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Adrenal cortex antibody, serum/plasma' where lab_icd10tm = '0710302';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000977','','การตรวจหา adrenal cortex antibody ในซีรั่ม / พลาสม่า','Adrenal cortex antibody, serum/plasma','1','0710302'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0710302');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา antinuclear antibody ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Antinuclear antibody, serum/plasma' where lab_icd10tm = '0710402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000978','','การตรวจหา antinuclear antibody ในซีรั่ม / พลาสม่า','Antinuclear antibody, serum/plasma','1','0710402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0710402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา antinuclear antibody ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Antinuclear antibody, other fluid NEC' where lab_icd10tm = '0710415';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000979','','การตรวจหา antinuclear antibody ในของเหลวอื่นๆ มิได้จำแนก','Antinuclear antibody, other fluid NEC','1','0710415'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0710415');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา cancer antigen 125 ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Cancer antigen 125, serum/plasma' where lab_icd10tm = '0710702';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000980','','การตรวจหา cancer antigen 125 ในซีรั่ม / พลาสม่า','Cancer antigen 125, serum/plasma','1','0710702'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0710702');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา cancer antigen 125 ในเนื้อเยื่ออื่นๆ มิได้จำแนก ',lab_ncd_note = 'Cancer antigen 125, tissue NEC' where lab_icd10tm = '0710750';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000981','','การตรวจหา cancer antigen 125 ในเนื้อเยื่ออื่นๆ มิได้จำแนก ','Cancer antigen 125, tissue NEC','1','0710750'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0710750');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา cancer antigen 15-3 ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Cancer antigen 15-3, serum/plasma' where lab_icd10tm = '0710802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000982','','การตรวจหา cancer antigen 15-3 ในซีรั่ม / พลาสม่า','Cancer antigen 15-3, serum/plasma','1','0710802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0710802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา cancer antigen 19-9 ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Cancer antigen 19-9, serum/plasma' where lab_icd10tm = '0710902';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000983','','การตรวจหา cancer antigen 19-9 ในซีรั่ม / พลาสม่า','Cancer antigen 19-9, serum/plasma','1','0710902'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0710902');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา cancer antigen อื่นๆ มิได้จำแนก ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Cancer antigen NEC, serum/plasma' where lab_icd10tm = '0711002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000984','','การตรวจหา cancer antigen อื่นๆ มิได้จำแนก ในซีรั่ม / พลาสม่า','Cancer antigen NEC, serum/plasma','1','0711002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0711002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา cardiolipin antibody ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Cardiolipin antibody, serum/plasma' where lab_icd10tm = '0711202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000985','','การตรวจหา cardiolipin antibody ในซีรั่ม / พลาสม่า','Cardiolipin antibody, serum/plasma','1','0711202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0711202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา DNA antibody ในซีรั่ม / พลาสม่า',lab_ncd_note = 'DNA antibody, serum/plasma' where lab_icd10tm = '0711802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000986','','การตรวจหา DNA antibody ในซีรั่ม / พลาสม่า','DNA antibody, serum/plasma','1','0711802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0711802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา extractable nuclear antibody อื่นๆ มิได้จำแนก ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Extractable nuclear antibody NEC, serum/plasma' where lab_icd10tm = '0712002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000987','','การตรวจหา extractable nuclear antibody อื่นๆ มิได้จำแนก ในซีรั่ม / พลาสม่า','Extractable nuclear antibody NEC, serum/plasma','1','0712002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0712002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา glomerular basement membrane antibody ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Glomerular basement membrane antibody, serum/plasma' where lab_icd10tm = '0712102';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000988','','การตรวจหา glomerular basement membrane antibody ในซีรั่ม / พลาสม่า','Glomerular basement membrane antibody, serum/plasma','1','0712102'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0712102');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา Histone antibody ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Histone antibody, serum/plasma' where lab_icd10tm = '0712202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000989','','การตรวจหา Histone antibody ในซีรั่ม / พลาสม่า','Histone antibody, serum/plasma','1','0712202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0712202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา HLA antibody ในซีรั่ม / พลาสม่า',lab_ncd_note = 'HLA antibody, serum/plasma' where lab_icd10tm = '0712302';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000990','','การตรวจหา HLA antibody ในซีรั่ม / พลาสม่า','HLA antibody, serum/plasma','1','0712302'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0712302');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา HLA antibody ในปัสสาวะ',lab_ncd_note = 'HLA antibody, urine' where lab_icd10tm = '0712303';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000991','','การตรวจหา HLA antibody ในปัสสาวะ','HLA antibody, urine','1','0712303'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0712303');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา HLA antibody ในเม็ดเลือดขาว',lab_ncd_note = 'HLA antibody,  WBC' where lab_icd10tm = '0712316';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000992','','การตรวจหา HLA antibody ในเม็ดเลือดขาว','HLA antibody,  WBC','1','0712316'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0712316');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา HLA antibody ในเกล็ดเลือด',lab_ncd_note = 'HLA antibody, platelets' where lab_icd10tm = '0712318';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000993','','การตรวจหา HLA antibody ในเกล็ดเลือด','HLA antibody, platelets','1','0712318'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0712318');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา insulin antibody ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Insulin antibody, serum/plasma' where lab_icd10tm = '0712702';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000994','','การตรวจหา insulin antibody ในซีรั่ม / พลาสม่า','Insulin antibody, serum/plasma','1','0712702'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0712702');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา intrinsic factor antibody ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Intrinsic factor antibody, serum/plasma' where lab_icd10tm = '0712802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000995','','การตรวจหา intrinsic factor antibody ในซีรั่ม / พลาสม่า','Intrinsic factor antibody, serum/plasma','1','0712802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0712802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา Jo-1 extractable nuclear antibody ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Jo-1 extractable nuclear antibody, serum/plasma' where lab_icd10tm = '0712902';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000996','','การตรวจหา Jo-1 extractable nuclear antibody ในซีรั่ม / พลาสม่า','Jo-1 extractable nuclear antibody, serum/plasma','1','0712902'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0712902');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา La (SSB) antibody ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'La (SSB) antibody, serum/plasma' where lab_icd10tm = '0713102';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000997','','การตรวจหา La (SSB) antibody ในซีรั่ม / พลาสม่า ','La (SSB) antibody, serum/plasma','1','0713102'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0713102');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา lung basement membrane antibody ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Lung basement membrane antibody, serum/plasma' where lab_icd10tm = '0713302';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000998','','การตรวจหา lung basement membrane antibody ในซีรั่ม / พลาสม่า','Lung basement membrane antibody, serum/plasma','1','0713302'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0713302');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา microsomal antibody ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Microsomal antibody, serum/plasma' where lab_icd10tm = '0713402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000000999','','การตรวจหา microsomal antibody ในซีรั่ม / พลาสม่า','Microsomal antibody, serum/plasma','1','0713402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0713402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา mitochondrial antibody ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Mitochondrial antibody, serum/plasma' where lab_icd10tm = '0713502';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001000','','การตรวจหา mitochondrial antibody ในซีรั่ม / พลาสม่า','Mitochondrial antibody, serum/plasma','1','0713502'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0713502');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา monocyte antibody ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Monocyte antibody, serum/plasma' where lab_icd10tm = '0713602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001001','','การตรวจหา monocyte antibody ในซีรั่ม / พลาสม่า','Monocyte antibody, serum/plasma','1','0713602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0713602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา monocyte antibody ในเม็ดเลือดขาว',lab_ncd_note = 'Monocyte antibody, WBC' where lab_icd10tm = '0713616';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001002','','การตรวจหา monocyte antibody ในเม็ดเลือดขาว','Monocyte antibody, WBC','1','0713616'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0713616');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา monocyte antibody ในไขกระดูก',lab_ncd_note = 'Monocyte antibody, bone marrow' where lab_icd10tm = '0713627';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001003','','การตรวจหา monocyte antibody ในไขกระดูก','Monocyte antibody, bone marrow','1','0713627'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0713627');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา monocyte antibody ในต่อมน้ำเหลือง ',lab_ncd_note = 'Monocyte antibody, lymph node' where lab_icd10tm = '0713628';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001004','','การตรวจหา monocyte antibody ในต่อมน้ำเหลือง ','Monocyte antibody, lymph node','1','0713628'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0713628');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา monocyte antibody ในเนื้อเยื่ออื่นๆ มิได้จำแนก',lab_ncd_note = 'Monocyte antibody, tissue NEC' where lab_icd10tm = '0713650';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001005','','การตรวจหา monocyte antibody ในเนื้อเยื่ออื่นๆ มิได้จำแนก','Monocyte antibody, tissue NEC','1','0713650'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0713650');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา mouse antibody ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Mouse antibody, serum/plasma' where lab_icd10tm = '0713802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001006','','การตรวจหา mouse antibody ในซีรั่ม / พลาสม่า','Mouse antibody, serum/plasma','1','0713802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0713802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา myelin basic protein antibody ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Myelin basic protein antibody, serum/plasma' where lab_icd10tm = '0714202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001007','','การตรวจหา myelin basic protein antibody ในซีรั่ม / พลาสม่า','Myelin basic protein antibody, serum/plasma','1','0714202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0714202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา myocardium antibody ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Myocardium antibody, serum/plasma' where lab_icd10tm = '0714302';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001008','','การตรวจหา myocardium antibody ในซีรั่ม / พลาสม่า','Myocardium antibody, serum/plasma','1','0714302'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0714302');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา nerve/neuronal antibody ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Nerve/neuronal antibody, serum/plasma' where lab_icd10tm = '0714402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001009','','การตรวจหา nerve/neuronal antibody ในซีรั่ม / พลาสม่า','Nerve/neuronal antibody, serum/plasma','1','0714402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0714402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา neutrophil cytoplasmic antibody ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Neutrophil cytoplasmic antibody, serum/plasma' where lab_icd10tm = '0714502';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001010','','การตรวจหา neutrophil cytoplasmic antibody ในซีรั่ม / พลาสม่า','Neutrophil cytoplasmic antibody, serum/plasma','1','0714502'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0714502');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา OKT3 antibody ในซีรั่ม / พลาสม่า',lab_ncd_note = 'OKT3 antibody, serum/plasma' where lab_icd10tm = '0714602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001011','','การตรวจหา OKT3 antibody ในซีรั่ม / พลาสม่า','OKT3 antibody, serum/plasma','1','0714602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0714602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา ovarian antibody  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Ovarian antibody, serum/plasma' where lab_icd10tm = '0714702';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001012','','การตรวจหา ovarian antibody  ในซีรั่ม / พลาสม่า','Ovarian antibody, serum/plasma','1','0714702'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0714702');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา pancreatic islet cell antibody ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Pancreatic islet cell antibody, serum/plasma' where lab_icd10tm = '0714802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001013','','การตรวจหา pancreatic islet cell antibody ในซีรั่ม / พลาสม่า','Pancreatic islet cell antibody, serum/plasma','1','0714802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0714802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา Parietal cell antibody ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Parietal cell antibody, serum/plasma' where lab_icd10tm = '0714902';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001014','','การตรวจหา Parietal cell antibody ในซีรั่ม / พลาสม่า','Parietal cell antibody, serum/plasma','1','0714902'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0714902');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา Pemphigus/pemphigoid antibody ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Pemphigus/pemphigoid antibody, serum/plasma' where lab_icd10tm = '0715102';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001015','','การตรวจหา Pemphigus/pemphigoid antibody ในซีรั่ม / พลาสม่า','Pemphigus/pemphigoid antibody, serum/plasma','1','0715102'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0715102');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา Platelet antibody ในเลือด',lab_ncd_note = 'Platelet antibody, whole blood' where lab_icd10tm = '0715301';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001016','','การตรวจหา Platelet antibody ในเลือด','Platelet antibody, whole blood','1','0715301'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0715301');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา Platelet antibody ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Platelet antibody, serum/plasma' where lab_icd10tm = '0715302';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001017','','การตรวจหา Platelet antibody ในซีรั่ม / พลาสม่า','Platelet antibody, serum/plasma','1','0715302'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0715302');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา Proliferating cell nuclear antibody ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Proliferating cell nuclear antibody, serum/plasma' where lab_icd10tm = '0715502';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001018','','การตรวจหา Proliferating cell nuclear antibody ในซีรั่ม / พลาสม่า','Proliferating cell nuclear antibody, serum/plasma','1','0715502'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0715502');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา Reticulin antibody ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Reticulin antibody, serum/plasma' where lab_icd10tm = '0715702';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001019','','การตรวจหา Reticulin antibody ในซีรั่ม / พลาสม่า','Reticulin antibody, serum/plasma','1','0715702'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0715702');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา Ribonucleoprotein extractable nuclear antibody  ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Ribonucleoprotein extractable nuclear antibody, serum/plasma' where lab_icd10tm = '0715802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001020','','การตรวจหา Ribonucleoprotein extractable nuclear antibody  ในซีรั่ม / พลาสม่า','Ribonucleoprotein extractable nuclear antibody, serum/plasma','1','0715802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0715802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา Ro (SSA) antibody ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Ro (SSA) antibody, serum/plasma' where lab_icd10tm = '0715902';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001021','','การตรวจหา Ro (SSA) antibody ในซีรั่ม / พลาสม่า','Ro (SSA) antibody, serum/plasma','1','0715902'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0715902');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา salivary gland antibody ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Salivary gland antibody, serum/plasma' where lab_icd10tm = '0716102';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001022','','การตรวจหา salivary gland antibody ในซีรั่ม / พลาสม่า','Salivary gland antibody, serum/plasma','1','0716102'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0716102');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา Scl 70 extractable nuclear antibody ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Scl 70 extractable nuclear antibody, serum/plasma' where lab_icd10tm = '0716202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001023','','การตรวจหา Scl 70 extractable nuclear antibody ในซีรั่ม / พลาสม่า','Scl 70 extractable nuclear antibody, serum/plasma','1','0716202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0716202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา skin antibody ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Skin antibody, serum/plasma' where lab_icd10tm = '0716302';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001024','','การตรวจหา skin antibody ในซีรั่ม / พลาสม่า','Skin antibody, serum/plasma','1','0716302'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0716302');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา Smith extractable nuclear antibody ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Smith extractable nuclear antibody, serum/plasma' where lab_icd10tm = '0716402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001025','','การตรวจหา Smith extractable nuclear antibody ในซีรั่ม / พลาสม่า','Smith extractable nuclear antibody, serum/plasma','1','0716402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0716402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา smooth muscle antibody ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Smooth muscle antibody, serum/plasma' where lab_icd10tm = '0716502';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001026','','การตรวจหา smooth muscle antibody ในซีรั่ม / พลาสม่า','Smooth muscle antibody, serum/plasma','1','0716502'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0716502');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา sperm antibody ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Sperm antibody, serum/plasma' where lab_icd10tm = '0716602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001027','','การตรวจหา sperm antibody ในซีรั่ม / พลาสม่า','Sperm antibody, serum/plasma','1','0716602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0716602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา striated muscle antibody ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Striated muscle antibody, serum/plasma' where lab_icd10tm = '0716702';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001028','','การตรวจหา striated muscle antibody ในซีรั่ม / พลาสม่า','Striated muscle antibody, serum/plasma','1','0716702'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0716702');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา thyroglobulin antibody ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Thyroglobulin antibody, serum/plasma' where lab_icd10tm = '0717102';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001029','','การตรวจหา thyroglobulin antibody ในซีรั่ม / พลาสม่า','Thyroglobulin antibody, serum/plasma','1','0717102'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0717102');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา thyroglobulin antibody ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Thyroglobulin antibody, lymph node' where lab_icd10tm = '0717128';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001030','','การตรวจหา thyroglobulin antibody ในซีรั่ม / พลาสม่า','Thyroglobulin antibody, lymph node','1','0717128'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0717128');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา thyroid stimulating hormone antibody ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Thyroid stimulating hormone antibody, serum/plasma' where lab_icd10tm = '0717202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001031','','การตรวจหา thyroid stimulating hormone antibody ในซีรั่ม / พลาสม่า','Thyroid stimulating hormone antibody, serum/plasma','1','0717202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0717202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา thyroid stimulating hormone receptor antibody ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Thyroid stimulating hormone receptor antibody, serum/plasma' where lab_icd10tm = '0717302';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001032','','การตรวจหา thyroid stimulating hormone receptor antibody ในซีรั่ม / พลาสม่า','Thyroid stimulating hormone receptor antibody, serum/plasma','1','0717302'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0717302');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา Yo antibody',lab_ncd_note = 'Yo antibody' where lab_icd10tm = '0717502';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001033','','การตรวจหา Yo antibody','Yo antibody','1','0717502'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0717502');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา tissue-specific antibody อื่นๆ  มิได้จำแนก  ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Tissue-specific antibody NEC, any specimen type' where lab_icd10tm = '0719099';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001034','','การตรวจหา tissue-specific antibody อื่นๆ  มิได้จำแนก  ในสิ่งส่งตรวจชนิดต่างๆ','Tissue-specific antibody NEC, any specimen type','1','0719099'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0719099');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบแอนติบอดี สำหรับ Legionella ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Antibody test for legionella, any specimen type' where lab_icd10tm = '0720299';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001035','','การทดสอบแอนติบอดี สำหรับ Legionella ในสิ่งส่งตรวจชนิดต่างๆ','Antibody test for legionella, any specimen type','1','0720299'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0720299');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบแอนติบอดี สำหรับ Lyme disease ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Antibody test for Lyme disease, any specimen type' where lab_icd10tm = '0720499';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001036','','การทดสอบแอนติบอดี สำหรับ Lyme disease ในสิ่งส่งตรวจชนิดต่างๆ','Antibody test for Lyme disease, any specimen type','1','0720499'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0720499');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบแอนติบอดี สำหรับ meningococcus ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Antibody test for meningococcus, any specimen type' where lab_icd10tm = '0720699';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001037','','การทดสอบแอนติบอดี สำหรับ meningococcus ในสิ่งส่งตรวจชนิดต่างๆ','Antibody test for meningococcus, any specimen type','1','0720699'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0720699');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'Antibody test for treponema, any specimen type (VDRL)' where lab_icd10tm = '0721297';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001038','','-','Antibody test for treponema, any specimen type (VDRL)','1','0721297'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0721297');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'Rapid plasma reagin, serum/plasma (RPR)' where lab_icd10tm = '0721298';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001039','','-','Rapid plasma reagin, serum/plasma (RPR)','1','0721298'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0721298');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบแอนติบอดี สำหรับ treponema ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Antibody test for treponema, any specimen type (TPHA)' where lab_icd10tm = '0721299';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001040','','การทดสอบแอนติบอดี สำหรับ treponema ในสิ่งส่งตรวจชนิดต่างๆ','Antibody test for treponema, any specimen type (TPHA)','1','0721299'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0721299');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบแอนติบอดี สำหรับแบคทีเรียในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Antibody test for bacteria NEC, any specimen type' where lab_icd10tm = '0725099';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001041','','การทดสอบแอนติบอดี สำหรับแบคทีเรียในสิ่งส่งตรวจชนิดต่างๆ','Antibody test for bacteria NEC, any specimen type','1','0725099'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0725099');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบแอนติเจน สำหรับ chlamydia ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Antigen test for chlamydia, any specimen type' where lab_icd10tm = '0726099';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001042','','การทดสอบแอนติเจน สำหรับ chlamydia ในสิ่งส่งตรวจชนิดต่างๆ','Antigen test for chlamydia, any specimen type','1','0726099'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0726099');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบแอนติเจน สำหรับ streptococcus A ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Antigen test for streptococcus A, any specimen type' where lab_icd10tm = '0727299';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001043','','การทดสอบแอนติเจน สำหรับ streptococcus A ในสิ่งส่งตรวจชนิดต่างๆ','Antigen test for streptococcus A, any specimen type','1','0727299'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0727299');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบแอนติเจน สำหรับ streptococcus B ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Antigen test for streptococcus B, any specimen type' where lab_icd10tm = '0727499';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001044','','การทดสอบแอนติเจน สำหรับ streptococcus B ในสิ่งส่งตรวจชนิดต่างๆ','Antigen test for streptococcus B, any specimen type','1','0727499'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0727499');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบแอนติเจน สำหรับแบคทีเรียอื่นๆ มิได้จำแนก ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Antigen test for bacteria NEC, any specimen type' where lab_icd10tm = '0729099';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001045','','การทดสอบแอนติเจน สำหรับแบคทีเรียอื่นๆ มิได้จำแนก ในสิ่งส่งตรวจชนิดต่างๆ','Antigen test for bacteria NEC, any specimen type','1','0729099'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0729099');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบแอนติบอดี สำหรับเชื้อราอื่นๆ  มิได้จำแนก  ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Antibody test for fungus NEC, any specimen type' where lab_icd10tm = '0735099';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001046','','การทดสอบแอนติบอดี สำหรับเชื้อราอื่นๆ  มิได้จำแนก  ในสิ่งส่งตรวจชนิดต่างๆ','Antibody test for fungus NEC, any specimen type','1','0735099'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0735099');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบแอนติเจนสำหรับเชื้อราอื่นๆ  มิได้จำแนก  ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Antigen test for fungus NEC, any specimen type' where lab_icd10tm = '0739099';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001047','','การทดสอบแอนติเจนสำหรับเชื้อราอื่นๆ  มิได้จำแนก  ในสิ่งส่งตรวจชนิดต่างๆ','Antigen test for fungus NEC, any specimen type','1','0739099'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0739099');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบแอนติบอดี สำหรับ cytomegalovirus (CMV) ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Antibody test for cytomegalovirus (CMV), any specimen type' where lab_icd10tm = '0740299';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001048','','การทดสอบแอนติบอดี สำหรับ cytomegalovirus (CMV) ในสิ่งส่งตรวจชนิดต่างๆ','Antibody test for cytomegalovirus (CMV), any specimen type','1','0740299'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0740299');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบแอนติบอดี สำหรับ Epstein-Barr virus ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Antibody test for Epstein-Barr virus, any specimen type' where lab_icd10tm = '0740899';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001049','','การทดสอบแอนติบอดี สำหรับ Epstein-Barr virus ในสิ่งส่งตรวจชนิดต่างๆ','Antibody test for Epstein-Barr virus, any specimen type','1','0740899'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0740899');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบแอนติบอดี สำหรับ hepatitis A ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Antibody test for hepatitis A, any specimen type' where lab_icd10tm = '0741299';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001050','','การทดสอบแอนติบอดี สำหรับ hepatitis A ในสิ่งส่งตรวจชนิดต่างๆ','Antibody test for hepatitis A, any specimen type','1','0741299'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0741299');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'Antibody test for hepatitis B (anti HBc) ' where lab_icd10tm = '0741498';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001051','','-','Antibody test for hepatitis B (anti HBc) ','1','0741498'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0741498');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบแอนติบอดี สำหรับ hepatitis B (surface) ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Antibody test for hepatitis B (surface), any specimen type' where lab_icd10tm = '0741499';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001052','','การทดสอบแอนติบอดี สำหรับ hepatitis B (surface) ในสิ่งส่งตรวจชนิดต่างๆ','Antibody test for hepatitis B (surface), any specimen type','1','0741499'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0741499');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบแอนติบอดี สำหรับ hepatitis C virus ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Antibody test for hepatitis C virus, any specimen type' where lab_icd10tm = '0741699';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001053','','การทดสอบแอนติบอดี สำหรับ hepatitis C virus ในสิ่งส่งตรวจชนิดต่างๆ','Antibody test for hepatitis C virus, any specimen type','1','0741699'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0741699');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบแอนติบอดี สำหรับ hepatitis virus อื่นๆ  มิได้จำแนก  ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Antibody test for hepatitis virus NEC, any specimen type' where lab_icd10tm = '0741899';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001054','','การทดสอบแอนติบอดี สำหรับ hepatitis virus อื่นๆ  มิได้จำแนก  ในสิ่งส่งตรวจชนิดต่างๆ','Antibody test for hepatitis virus NEC, any specimen type','1','0741899'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0741899');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบแอนติบอดี สำหรับ herpes simplex virus ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Antibody test for herpes simplex virus , any specimen type' where lab_icd10tm = '0742299';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001055','','การทดสอบแอนติบอดี สำหรับ herpes simplex virus ในสิ่งส่งตรวจชนิดต่างๆ','Antibody test for herpes simplex virus , any specimen type','1','0742299'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0742299');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบแอนติบอดี สำหรับ influenza virus ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Antibody test for influenza virus , any specimen type' where lab_icd10tm = '0742499';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001056','','การทดสอบแอนติบอดี สำหรับ influenza virus ในสิ่งส่งตรวจชนิดต่างๆ','Antibody test for influenza virus , any specimen type','1','0742499'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0742499');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบแอนติบอดี สำหรับ measles virus ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Antibody test for measles virus , any specimen type' where lab_icd10tm = '0742699';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001057','','การทดสอบแอนติบอดี สำหรับ measles virus ในสิ่งส่งตรวจชนิดต่างๆ','Antibody test for measles virus , any specimen type','1','0742699'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0742699');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบแอนติบอดี สำหรับ human immunodeficiency virus (HIV)  ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Antibody test for human immunodeficiency virus (HIV) , any specimen type' where lab_icd10tm = '0743299';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001058','','การทดสอบแอนติบอดี สำหรับ human immunodeficiency virus (HIV)  ในสิ่งส่งตรวจชนิดต่างๆ','Antibody test for human immunodeficiency virus (HIV) , any specimen type','1','0743299'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0743299');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบแอนติบอดี สำหรับ human T cell lymphotropic retrovirus ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Antibody test for human T cell lymphotropic retrovirus, any specimen type' where lab_icd10tm = '0743499';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001059','','การทดสอบแอนติบอดี สำหรับ human T cell lymphotropic retrovirus ในสิ่งส่งตรวจชนิดต่างๆ','Antibody test for human T cell lymphotropic retrovirus, any specimen type','1','0743499'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0743499');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบแอนติบอดี สำหรับ rabies ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Antibody test for rabies , any specimen type' where lab_icd10tm = '0744299';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001060','','การทดสอบแอนติบอดี สำหรับ rabies ในสิ่งส่งตรวจชนิดต่างๆ','Antibody test for rabies , any specimen type','1','0744299'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0744299');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบแอนติบอดี สำหรับ respiratory syncytial virus ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Antibody test for respiratory syncytial virus , any specimen type' where lab_icd10tm = '0744499';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001061','','การทดสอบแอนติบอดี สำหรับ respiratory syncytial virus ในสิ่งส่งตรวจชนิดต่างๆ','Antibody test for respiratory syncytial virus , any specimen type','1','0744499'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0744499');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบแอนติบอดี สำหรับ rubella ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Antibody test for rubella, any specimen type' where lab_icd10tm = '0744699';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001062','','การทดสอบแอนติบอดี สำหรับ rubella ในสิ่งส่งตรวจชนิดต่างๆ','Antibody test for rubella, any specimen type','1','0744699'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0744699');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา Genotype ของเชื้อไวรัส HPV',lab_ncd_note = 'HPV Genotyping test' where lab_icd10tm = '0744798';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001063','','การตรวจหา Genotype ของเชื้อไวรัส HPV','HPV Genotyping test','1','0744798'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0744798');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'HPV Antigen test' where lab_icd10tm = '0744799';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001064','','-','HPV Antigen test','1','0744799'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0744799');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบแอนติบอดี สำหรับ varicella zoster ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Antibody test for varicella zoster, any specimen type' where lab_icd10tm = '0744899';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001065','','การทดสอบแอนติบอดี สำหรับ varicella zoster ในสิ่งส่งตรวจชนิดต่างๆ','Antibody test for varicella zoster, any specimen type','1','0744899'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0744899');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบแอนติบอดี สำหรับไวรัสอื่นๆ มิได้จำแนก ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Antibody test for virus NEC, any specimen type' where lab_icd10tm = '0745099';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001066','','การทดสอบแอนติบอดี สำหรับไวรัสอื่นๆ มิได้จำแนก ในสิ่งส่งตรวจชนิดต่างๆ','Antibody test for virus NEC, any specimen type','1','0745099'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0745099');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบแอนติเจน สำหรับ hepatitis B (surface) ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Antigen test for hepatitis B (surface), any specimen type' where lab_icd10tm = '0746299';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001067','','การทดสอบแอนติเจน สำหรับ hepatitis B (surface) ในสิ่งส่งตรวจชนิดต่างๆ','Antigen test for hepatitis B (surface), any specimen type','1','0746299'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0746299');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบแอนติเจน สำหรับ hepatitis virus อื่นๆ  มิได้จำแนก  ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Antigen test for hepatitis virus NEC, any specimen type' where lab_icd10tm = '0749099';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001068','','การทดสอบแอนติเจน สำหรับ hepatitis virus อื่นๆ  มิได้จำแนก  ในสิ่งส่งตรวจชนิดต่างๆ','Antigen test for hepatitis virus NEC, any specimen type','1','0749099'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0749099');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'HIV DNA' where lab_icd10tm = '0749100';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001069','','-','HIV DNA','1','0749100'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0749100');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'HSV DNA' where lab_icd10tm = '0749101';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001070','','-','HSV DNA','1','0749101'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0749101');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'Dengue RNA' where lab_icd10tm = '0749102';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001071','','-','Dengue RNA','1','0749102'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0749102');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'HCV RNA test' where lab_icd10tm = '0749103';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001072','','-','HCV RNA test','1','0749103'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0749103');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'Molecular test for T. Vaginalis' where lab_icd10tm = '0749110';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001073','','-','Molecular test for T. Vaginalis','1','0749110'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0749110');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'Molecular test for CT/NG' where lab_icd10tm = '0749111';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001074','','-','Molecular test for CT/NG','1','0749111'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0749111');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'Molecular test for H. ducreyi' where lab_icd10tm = '0749112';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001075','','-','Molecular test for H. ducreyi','1','0749112'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0749112');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'Dengue NS1 antigen' where lab_icd10tm = '0749200';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001076','','-','Dengue NS1 antigen','1','0749200'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0749200');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'Dengue Ig G' where lab_icd10tm = '0749201';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001077','','-','Dengue Ig G','1','0749201'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0749201');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'Dengue Ig M' where lab_icd10tm = '0749202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001078','','-','Dengue Ig M','1','0749202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0749202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'Anti HDV IgM' where lab_icd10tm = '0749210';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001079','','-','Anti HDV IgM','1','0749210'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0749210');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'HIV viral load' where lab_icd10tm = '0749300';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001080','','-','HIV viral load','1','0749300'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0749300');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'CMV viral load' where lab_icd10tm = '0749301';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001081','','-','CMV viral load','1','0749301'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0749301');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'HCV viral load' where lab_icd10tm = '0749302';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001082','','-','HCV viral load','1','0749302'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0749302');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'HBV viral load' where lab_icd10tm = '0749303';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001083','','-','HBV viral load','1','0749303'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0749303');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบแอนติบอดี สำหรับ toxoplasmosis ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Antibody test for toxoplasmosis, any specimen type' where lab_icd10tm = '0750299';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001084','','การทดสอบแอนติบอดี สำหรับ toxoplasmosis ในสิ่งส่งตรวจชนิดต่างๆ','Antibody test for toxoplasmosis, any specimen type','1','0750299'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0750299');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบแอนติบอดี สำหรับ parasite อื่นๆ  มิได้จำแนก  ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Antibody test for parasite NEC, any specimen type' where lab_icd10tm = '0755099';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001085','','การทดสอบแอนติบอดี สำหรับ parasite อื่นๆ  มิได้จำแนก  ในสิ่งส่งตรวจชนิดต่างๆ','Antibody test for parasite NEC, any specimen type','1','0755099'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0755099');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การทดสอบแอนติเจน สำหรับ parasite อื่นๆ  มิได้จำแนก  ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Antigen test for parasite NEC, any specimen type' where lab_icd10tm = '0759099';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001086','','การทดสอบแอนติเจน สำหรับ parasite อื่นๆ  มิได้จำแนก  ในสิ่งส่งตรวจชนิดต่างๆ','Antigen test for parasite NEC, any specimen type','1','0759099'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0759099');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเพาะเลี้ยงเชื้อแบคทีเรียในเลือด',lab_ncd_note = 'Bacterial culture, whole blood' where lab_icd10tm = '0760201';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001087','','การเพาะเลี้ยงเชื้อแบคทีเรียในเลือด','Bacterial culture, whole blood','1','0760201'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0760201');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเพาะเลี้ยงเชื้อแบคทีเรียในปัสสาวะ',lab_ncd_note = 'Bacterial culture, urine' where lab_icd10tm = '0760203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001088','','การเพาะเลี้ยงเชื้อแบคทีเรียในปัสสาวะ','Bacterial culture, urine','1','0760203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0760203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเพาะเลี้ยงเชื้อแบคทีเรียในน้ำไขสันหลัง ',lab_ncd_note = 'Bacterial culture, CSF' where lab_icd10tm = '0760204';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001089','','การเพาะเลี้ยงเชื้อแบคทีเรียในน้ำไขสันหลัง ','Bacterial culture, CSF','1','0760204'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0760204');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเพาะเลี้ยงเชื้อแบคทีเรียในเสมหะ',lab_ncd_note = 'Bacterial culture, sputum' where lab_icd10tm = '0760209';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001090','','การเพาะเลี้ยงเชื้อแบคทีเรียในเสมหะ','Bacterial culture, sputum','1','0760209'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0760209');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเพาะเลี้ยงเชื้อแบคทีเรียในของเหลวจากข้อ',lab_ncd_note = 'Bacterial culture, synovial fluid' where lab_icd10tm = '0760210';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001091','','การเพาะเลี้ยงเชื้อแบคทีเรียในของเหลวจากข้อ','Bacterial culture, synovial fluid','1','0760210'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0760210');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเพาะเลี้ยงเชื้อแบคทีเรียในของเสียที่ถูกปล่อยออกมา',lab_ncd_note = 'Bacterial culture, effluents' where lab_icd10tm = '0760211';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001092','','การเพาะเลี้ยงเชื้อแบคทีเรียในของเสียที่ถูกปล่อยออกมา','Bacterial culture, effluents','1','0760211'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0760211');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเพาะเลี้ยงเชื้อแบคทีเรียในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Bacterial culture, other fluid NEC' where lab_icd10tm = '0760215';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001093','','การเพาะเลี้ยงเชื้อแบคทีเรียในของเหลวอื่นๆ มิได้จำแนก','Bacterial culture, other fluid NEC','1','0760215'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0760215');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเพาะเลี้ยงเชื้อแบคทีเรียในไขกระดูก',lab_ncd_note = 'Bacterial culture, bone marrow' where lab_icd10tm = '0760227';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001094','','การเพาะเลี้ยงเชื้อแบคทีเรียในไขกระดูก','Bacterial culture, bone marrow','1','0760227'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0760227');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเพาะเลี้ยงเชื้อแบคทีเรียในเนื้อเยื่ออื่นๆ มิได้จำแนก',lab_ncd_note = 'Bacterial culture, tissue NEC' where lab_icd10tm = '0760250';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001095','','การเพาะเลี้ยงเชื้อแบคทีเรียในเนื้อเยื่ออื่นๆ มิได้จำแนก','Bacterial culture, tissue NEC','1','0760250'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0760250');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเพาะเลี้ยงเชื้อแบคทีเรียในสิ่งส่งตรวจที่ป้ายจากอวัยวะเพศ ',lab_ncd_note = 'Bacterial culture, genital (swab)' where lab_icd10tm = '0760251';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001096','','การเพาะเลี้ยงเชื้อแบคทีเรียในสิ่งส่งตรวจที่ป้ายจากอวัยวะเพศ ','Bacterial culture, genital (swab)','1','0760251'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0760251');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเพาะเลี้ยงเชื้อแบคทีเรียในสิ่งส่งตรวจที่ป้ายจาก หู / ตา / ปาก ',lab_ncd_note = 'Bacterial culture, ear/eye/mouth  (swab)' where lab_icd10tm = '0760252';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001097','','การเพาะเลี้ยงเชื้อแบคทีเรียในสิ่งส่งตรวจที่ป้ายจาก หู / ตา / ปาก ','Bacterial culture, ear/eye/mouth  (swab)','1','0760252'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0760252');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเพาะเลี้ยงเชื้อแบคทีเรียในสิ่งส่งตรวจที่ป้ายจากแผลตื้นๆ',lab_ncd_note = 'Bacterial culture, superficial wound (swab)' where lab_icd10tm = '0760253';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001098','','การเพาะเลี้ยงเชื้อแบคทีเรียในสิ่งส่งตรวจที่ป้ายจากแผลตื้นๆ','Bacterial culture, superficial wound (swab)','1','0760253'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0760253');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเพาะเลี้ยงเชื้อแบคทีเรียในสิ่งส่งตรวจที่ป้ายจากบาดแผลลึก',lab_ncd_note = 'Bacterial culture, deep wound  (swab)' where lab_icd10tm = '0760254';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001099','','การเพาะเลี้ยงเชื้อแบคทีเรียในสิ่งส่งตรวจที่ป้ายจากบาดแผลลึก','Bacterial culture, deep wound  (swab)','1','0760254'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0760254');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเพาะเลี้ยงเชื้อแบคทีเรียในสิ่งส่งตรวจที่ป้ายจากจมูก',lab_ncd_note = 'Bacterial culture, nose  (swab)' where lab_icd10tm = '0760255';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001100','','การเพาะเลี้ยงเชื้อแบคทีเรียในสิ่งส่งตรวจที่ป้ายจากจมูก','Bacterial culture, nose  (swab)','1','0760255'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0760255');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเพาะเลี้ยงเชื้อแบคทีเรียในสิ่งส่งตรวจที่ป้ายจากคอ',lab_ncd_note = 'Bacterial culture, throat (swab)' where lab_icd10tm = '0760256';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001101','','การเพาะเลี้ยงเชื้อแบคทีเรียในสิ่งส่งตรวจที่ป้ายจากคอ','Bacterial culture, throat (swab)','1','0760256'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0760256');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเพาะเลี้ยงเชื้อแบคทีเรียในอุจจาระ',lab_ncd_note = 'Bacterial culture, stool' where lab_icd10tm = '0760266';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001102','','การเพาะเลี้ยงเชื้อแบคทีเรียในอุจจาระ','Bacterial culture, stool','1','0760266'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0760266');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเพาะเลี้ยงเชื้อแบคทีเรียในอาหาร',lab_ncd_note = 'Bacterial culture, food' where lab_icd10tm = '0760269';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001103','','การเพาะเลี้ยงเชื้อแบคทีเรียในอาหาร','Bacterial culture, food','1','0760269'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0760269');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเพาะเลี้ยงเชื้อแบคทีเรียในสิ่งแปลกปลอมอื่นๆ มิได้จำแนก ',lab_ncd_note = 'Bacterial culture, foreign body NEC' where lab_icd10tm = '0760273';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001104','','การเพาะเลี้ยงเชื้อแบคทีเรียในสิ่งแปลกปลอมอื่นๆ มิได้จำแนก ','Bacterial culture, foreign body NEC','1','0760273'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0760273');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเพาะเลี้ยงเชื้อแบคทีเรียในน้ำ',lab_ncd_note = 'Bacterial culture, water' where lab_icd10tm = '0760283';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001105','','การเพาะเลี้ยงเชื้อแบคทีเรียในน้ำ','Bacterial culture, water','1','0760283'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0760283');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเพาะเลี้ยงเชื้อแบคทีเรียในตัวอย่างจากสิ่งแวดล้อม ',lab_ncd_note = 'Bacterial culture, environmental sample' where lab_icd10tm = '0760284';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001106','','การเพาะเลี้ยงเชื้อแบคทีเรียในตัวอย่างจากสิ่งแวดล้อม ','Bacterial culture, environmental sample','1','0760284'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0760284');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเพาะเลี้ยงเชื้อแบคทีเรียในสิ่งส่งตรวจชนิดอื่นๆ มิได้จำแนก',lab_ncd_note = 'Bacterial culture, specimen type NEC' where lab_icd10tm = '0760290';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001107','','การเพาะเลี้ยงเชื้อแบคทีเรียในสิ่งส่งตรวจชนิดอื่นๆ มิได้จำแนก','Bacterial culture, specimen type NEC','1','0760290'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0760290');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเพาะเลี้ยงเชื้อ chlamydial ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Chlamydial culture, any specimen type' where lab_icd10tm = '0770299';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001108','','การเพาะเลี้ยงเชื้อ chlamydial ในสิ่งส่งตรวจชนิดต่างๆ','Chlamydial culture, any specimen type','1','0770299'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0770299');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเพาะเลี้ยงเชื้อราในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Fungal culture, any specimen type' where lab_icd10tm = '0771299';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001109','','การเพาะเลี้ยงเชื้อราในสิ่งส่งตรวจชนิดต่างๆ','Fungal culture, any specimen type','1','0771299'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0771299');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเพาะเลี้ยงเชื้อ mycobacteria ในเสมหะ',lab_ncd_note = 'Mycobacteria culture, sputum' where lab_icd10tm = '0772209';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001110','','การเพาะเลี้ยงเชื้อ mycobacteria ในเสมหะ','Mycobacteria culture, sputum','1','0772209'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0772209');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเพาะเลี้ยงเชื้อ mycobacteria ในสิ่งส่งตรวจชนิดอื่นๆ  มิได้จำแนก ',lab_ncd_note = 'Mycobacteria culture, specimen type NEC' where lab_icd10tm = '0772290';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001111','','การเพาะเลี้ยงเชื้อ mycobacteria ในสิ่งส่งตรวจชนิดอื่นๆ  มิได้จำแนก ','Mycobacteria culture, specimen type NEC','1','0772290'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0772290');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเพาะเลี้ยงเชื้อ mycobacteria ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Mycoplasma culture, any specimen type' where lab_icd10tm = '0773299';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001112','','การเพาะเลี้ยงเชื้อ mycobacteria ในสิ่งส่งตรวจชนิดต่างๆ','Mycoplasma culture, any specimen type','1','0773299'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0773299');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเพาะเลี้ยงเชื้อปรสิตในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Parasite culture, any specimen type' where lab_icd10tm = '0774299';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001113','','การเพาะเลี้ยงเชื้อปรสิตในสิ่งส่งตรวจชนิดต่างๆ','Parasite culture, any specimen type','1','0774299'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0774299');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเพาะเลี้ยงเชื้อไวรัสในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Viral culture, any specimen type' where lab_icd10tm = '0775299';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001114','','การเพาะเลี้ยงเชื้อไวรัสในสิ่งส่งตรวจชนิดต่างๆ','Viral culture, any specimen type','1','0775299'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0775299');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจระบุเชื้อแบคทีเรียในเลือด',lab_ncd_note = 'Bacterial identification, whole blood' where lab_icd10tm = '0780201';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001115','','การตรวจระบุเชื้อแบคทีเรียในเลือด','Bacterial identification, whole blood','1','0780201'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0780201');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจระบุเชื้อแบคทีเรียในปัสสาวะ',lab_ncd_note = 'Bacterial identification, urine' where lab_icd10tm = '0780203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001116','','การตรวจระบุเชื้อแบคทีเรียในปัสสาวะ','Bacterial identification, urine','1','0780203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0780203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจระบุเชื้อแบคทีเรียในน้ำไขสันหลัง ',lab_ncd_note = 'Bacterial identification, CSF' where lab_icd10tm = '0780204';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001117','','การตรวจระบุเชื้อแบคทีเรียในน้ำไขสันหลัง ','Bacterial identification, CSF','1','0780204'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0780204');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจระบุเชื้อแบคทีเรียในเสมหะ ',lab_ncd_note = 'Bacterial identification, sputum' where lab_icd10tm = '0780209';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001118','','การตรวจระบุเชื้อแบคทีเรียในเสมหะ ','Bacterial identification, sputum','1','0780209'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0780209');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจระบุเชื้อแบคทีเรียในของเหลวจากข้อ',lab_ncd_note = 'Bacterial identification, synovial fluid ' where lab_icd10tm = '0780210';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001119','','การตรวจระบุเชื้อแบคทีเรียในของเหลวจากข้อ','Bacterial identification, synovial fluid ','1','0780210'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0780210');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจระบุเชื้อแบคทีเรียในของเสียที่ปล่อยออกจากร่างกาย',lab_ncd_note = 'Bacterial identification, effluents ' where lab_icd10tm = '0780211';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001120','','การตรวจระบุเชื้อแบคทีเรียในของเสียที่ปล่อยออกจากร่างกาย','Bacterial identification, effluents ','1','0780211'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0780211');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจระบุเชื้อแบคทีเรียในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Bacterial identification, other fluid NEC' where lab_icd10tm = '0780215';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001121','','การตรวจระบุเชื้อแบคทีเรียในของเหลวอื่นๆ มิได้จำแนก','Bacterial identification, other fluid NEC','1','0780215'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0780215');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจระบุเชื้อแบคทีเรียในไขกระดูก',lab_ncd_note = 'Bacterial identification, bone marrow' where lab_icd10tm = '0780227';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001122','','การตรวจระบุเชื้อแบคทีเรียในไขกระดูก','Bacterial identification, bone marrow','1','0780227'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0780227');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจระบุเชื้อแบคทีเรียในเนื้อเยื่ออื่นๆ มิได้จำแนก',lab_ncd_note = 'Bacterial identification, tissue NEC' where lab_icd10tm = '0780250';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001123','','การตรวจระบุเชื้อแบคทีเรียในเนื้อเยื่ออื่นๆ มิได้จำแนก','Bacterial identification, tissue NEC','1','0780250'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0780250');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจระบุเชื้อแบคทีเรียในสิ่งส่งตรวจที่ป้ายจากอวัยวะเพศ',lab_ncd_note = 'Bacterial identification, genital (swab)' where lab_icd10tm = '0780251';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001124','','การตรวจระบุเชื้อแบคทีเรียในสิ่งส่งตรวจที่ป้ายจากอวัยวะเพศ','Bacterial identification, genital (swab)','1','0780251'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0780251');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจระบุเชื้อแบคทีเรียในสิ่งส่งตรวจที่ป้ายจากหู / ตา / ปาก',lab_ncd_note = 'Bacterial identification, ear/eye/mouth (swab)' where lab_icd10tm = '0780252';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001125','','การตรวจระบุเชื้อแบคทีเรียในสิ่งส่งตรวจที่ป้ายจากหู / ตา / ปาก','Bacterial identification, ear/eye/mouth (swab)','1','0780252'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0780252');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจระบุเชื้อแบคทีเรียในสิ่งส่งตรวจที่ป้ายจากแผลตื้นๆ',lab_ncd_note = 'Bacterial identification, superficial wound (swab)' where lab_icd10tm = '0780253';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001126','','การตรวจระบุเชื้อแบคทีเรียในสิ่งส่งตรวจที่ป้ายจากแผลตื้นๆ','Bacterial identification, superficial wound (swab)','1','0780253'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0780253');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจระบุเชื้อแบคทีเรียในสิ่งส่งตรวจที่ป้ายจากแผลลึก',lab_ncd_note = 'Bacterial identification, deep wound (swab)' where lab_icd10tm = '0780254';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001127','','การตรวจระบุเชื้อแบคทีเรียในสิ่งส่งตรวจที่ป้ายจากแผลลึก','Bacterial identification, deep wound (swab)','1','0780254'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0780254');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจระบุเชื้อแบคทีเรียในสิ่งส่งตรวจที่ป้ายจากจมูก',lab_ncd_note = 'Bacterial identification, nose (swab)' where lab_icd10tm = '0780255';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001128','','การตรวจระบุเชื้อแบคทีเรียในสิ่งส่งตรวจที่ป้ายจากจมูก','Bacterial identification, nose (swab)','1','0780255'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0780255');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจระบุเชื้อแบคทีเรียในสิ่งส่งตรวจที่ป้ายจากภายในลำคอ',lab_ncd_note = 'Bacterial identification, throat (swab)' where lab_icd10tm = '0780256';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001129','','การตรวจระบุเชื้อแบคทีเรียในสิ่งส่งตรวจที่ป้ายจากภายในลำคอ','Bacterial identification, throat (swab)','1','0780256'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0780256');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'Bacterial identification, urethra (swab)' where lab_icd10tm = '0780257';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001130','','-','Bacterial identification, urethra (swab)','1','0780257'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0780257');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจระบุเชื้อแบคทีเรียในอุจจาระ',lab_ncd_note = 'Bacterial identification, stool' where lab_icd10tm = '0780266';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001131','','การตรวจระบุเชื้อแบคทีเรียในอุจจาระ','Bacterial identification, stool','1','0780266'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0780266');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจระบุเชื้อแบคทีเรียในอาหาร',lab_ncd_note = 'Bacterial identification, food' where lab_icd10tm = '0780269';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001132','','การตรวจระบุเชื้อแบคทีเรียในอาหาร','Bacterial identification, food','1','0780269'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0780269');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจระบุเชื้อแบคทีเรียในสิ่งส่งตรวจที่ทำการแยกแล้ว',lab_ncd_note = 'Bacterial identification, isolate' where lab_icd10tm = '0780270';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001133','','การตรวจระบุเชื้อแบคทีเรียในสิ่งส่งตรวจที่ทำการแยกแล้ว','Bacterial identification, isolate','1','0780270'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0780270');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจระบุเชื้อแบคทีเรียในสิ่งแปลกปลอมจากร่างกายอื่นๆ  มิได้จำแนก',lab_ncd_note = 'Bacterial identification, foreign body NEC' where lab_icd10tm = '0780273';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001134','','การตรวจระบุเชื้อแบคทีเรียในสิ่งแปลกปลอมจากร่างกายอื่นๆ  มิได้จำแนก','Bacterial identification, foreign body NEC','1','0780273'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0780273');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจระบุเชื้อแบคทีเรียในน้ำ ',lab_ncd_note = 'Bacterial identification, water' where lab_icd10tm = '0780283';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001135','','การตรวจระบุเชื้อแบคทีเรียในน้ำ ','Bacterial identification, water','1','0780283'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0780283');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจระบุเชื้อแบคทีเรียในตัวอย่างจากสิ่งแวดล้อม',lab_ncd_note = 'Bacterial identification, environmental sample' where lab_icd10tm = '0780284';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001136','','การตรวจระบุเชื้อแบคทีเรียในตัวอย่างจากสิ่งแวดล้อม','Bacterial identification, environmental sample','1','0780284'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0780284');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจระบุเชื้อแบคทีเรียในสิ่งส่งตรวจชนิดอื่นๆ มิได้จำแนก',lab_ncd_note = 'Bacterial identification, specimen type NEC' where lab_icd10tm = '0780290';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001137','','การตรวจระบุเชื้อแบคทีเรียในสิ่งส่งตรวจชนิดอื่นๆ มิได้จำแนก','Bacterial identification, specimen type NEC','1','0780290'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0780290');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจระบุเชื้อ chlamydial ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Chlamydial identification, any specimen type' where lab_icd10tm = '0790299';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001138','','การตรวจระบุเชื้อ chlamydial ในสิ่งส่งตรวจชนิดต่างๆ','Chlamydial identification, any specimen type','1','0790299'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0790299');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจระบุเชื้อราในสิ่งส่งตรวจที่ทำการแยกแล้ว',lab_ncd_note = 'Fungal identification, isolate' where lab_icd10tm = '0791270';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001139','','การตรวจระบุเชื้อราในสิ่งส่งตรวจที่ทำการแยกแล้ว','Fungal identification, isolate','1','0791270'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0791270');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจระบุเชื้อราในสิ่งส่งตรวจชนิดอื่นๆ มิได้จำแนก',lab_ncd_note = 'Fungal identification, specimen type NEC' where lab_icd10tm = '0791290';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001140','','การตรวจระบุเชื้อราในสิ่งส่งตรวจชนิดอื่นๆ มิได้จำแนก','Fungal identification, specimen type NEC','1','0791290'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0791290');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจระบุเชื้อ mycobacteria ในสิ่งส่งตรวจที่ทำการแยกแล้ว',lab_ncd_note = 'Mycobacteria identification, isolate' where lab_icd10tm = '0792270';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001141','','การตรวจระบุเชื้อ mycobacteria ในสิ่งส่งตรวจที่ทำการแยกแล้ว','Mycobacteria identification, isolate','1','0792270'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0792270');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจระบุเชื้อ mycobacteria ในสิ่งส่งตรวจชนิดอื่นๆ มิได้จำแนก',lab_ncd_note = 'Mycobacteria identification, specimen type NEC' where lab_icd10tm = '0792290';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001142','','การตรวจระบุเชื้อ mycobacteria ในสิ่งส่งตรวจชนิดอื่นๆ มิได้จำแนก','Mycobacteria identification, specimen type NEC','1','0792290'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0792290');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจระบุเชื้อ mycobacteria ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Mycoplasma identification, any specimen type' where lab_icd10tm = '0793299';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001143','','การตรวจระบุเชื้อ mycobacteria ในสิ่งส่งตรวจชนิดต่างๆ','Mycoplasma identification, any specimen type','1','0793299'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0793299');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'Parasite identification, vagianal (swab)' where lab_icd10tm = '0794298';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001144','','-','Parasite identification, vagianal (swab)','1','0794298'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0794298');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจระบุ เชื้อปรสิต ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Parasite identification, any specimen type' where lab_icd10tm = '0794299';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001145','','การตรวจระบุ เชื้อปรสิต ในสิ่งส่งตรวจชนิดต่างๆ','Parasite identification, any specimen type','1','0794299'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0794299');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจระบุเชื้อไวรัสในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Viral identification, any specimen type' where lab_icd10tm = '0795299';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001146','','การตรวจระบุเชื้อไวรัสในสิ่งส่งตรวจชนิดต่างๆ','Viral identification, any specimen type','1','0795299'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0795299');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การหาภูมิไวรับของเชื้อแบคทีเรียในเลือด',lab_ncd_note = 'Bacterial susceptibility, whole blood' where lab_icd10tm = '0800201';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001147','','การหาภูมิไวรับของเชื้อแบคทีเรียในเลือด','Bacterial susceptibility, whole blood','1','0800201'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0800201');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การหาภูมิไวรับของเชื้อแบคทีเรียในปัสสาวะ',lab_ncd_note = 'Bacterial susceptibility, urine' where lab_icd10tm = '0800203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001148','','การหาภูมิไวรับของเชื้อแบคทีเรียในปัสสาวะ','Bacterial susceptibility, urine','1','0800203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0800203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การหาภูมิไวรับของเชื้อแบคทีเรียในน้ำไขสันหลัง',lab_ncd_note = 'Bacterial susceptibility, CSF' where lab_icd10tm = '0800204';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001149','','การหาภูมิไวรับของเชื้อแบคทีเรียในน้ำไขสันหลัง','Bacterial susceptibility, CSF','1','0800204'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0800204');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การหาภูมิไวรับของเชื้อแบคทีเรียในเสมหะ',lab_ncd_note = 'Bacterial susceptibility, sputum' where lab_icd10tm = '0800209';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001150','','การหาภูมิไวรับของเชื้อแบคทีเรียในเสมหะ','Bacterial susceptibility, sputum','1','0800209'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0800209');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเพาะเลี้ยงเชื้อเชื้อแบคทีเรียในของเหลวจากข้อ ',lab_ncd_note = 'Bacterial susceptibility, synovial fluid ' where lab_icd10tm = '0800210';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001151','','การเพาะเลี้ยงเชื้อเชื้อแบคทีเรียในของเหลวจากข้อ ','Bacterial susceptibility, synovial fluid ','1','0800210'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0800210');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การเพาะเลี้ยงเชื้อแบคทีเรียในของเสียที่ปล่อยออกจากร่างกาย',lab_ncd_note = 'Bacterial susceptibility, effluents ' where lab_icd10tm = '0800211';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001152','','การเพาะเลี้ยงเชื้อแบคทีเรียในของเสียที่ปล่อยออกจากร่างกาย','Bacterial susceptibility, effluents ','1','0800211'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0800211');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การหาภูมิไวรับของเชื้อแบคทีเรียในของเหลวอื่น ๆ มิได้จำแนก',lab_ncd_note = 'Bacterial susceptibility, other fluid NEC' where lab_icd10tm = '0800215';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001153','','การหาภูมิไวรับของเชื้อแบคทีเรียในของเหลวอื่น ๆ มิได้จำแนก','Bacterial susceptibility, other fluid NEC','1','0800215'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0800215');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การหาภูมิไวรับของเชื้อแบคทีเรียในไขกระดูก',lab_ncd_note = 'Bacterial susceptibility, bone marrow' where lab_icd10tm = '0800227';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001154','','การหาภูมิไวรับของเชื้อแบคทีเรียในไขกระดูก','Bacterial susceptibility, bone marrow','1','0800227'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0800227');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การหาภูมิไวรับของเชื้อแบคทีเรียในเนื้อเยื่อ อื่น ๆ มิได้จำแนก',lab_ncd_note = 'Bacterial susceptibility, tissue NEC' where lab_icd10tm = '0800250';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001155','','การหาภูมิไวรับของเชื้อแบคทีเรียในเนื้อเยื่อ อื่น ๆ มิได้จำแนก','Bacterial susceptibility, tissue NEC','1','0800250'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0800250');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การหาภูมิไวรับของเชื้อแบคทีเรียในสิ่งส่งตรวจที่ป้ายจากอวัยวะเพศ',lab_ncd_note = 'Bacterial susceptibility, genital (swab)' where lab_icd10tm = '0800251';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001156','','การหาภูมิไวรับของเชื้อแบคทีเรียในสิ่งส่งตรวจที่ป้ายจากอวัยวะเพศ','Bacterial susceptibility, genital (swab)','1','0800251'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0800251');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การหาภูมิไวรับของเชื้อแบคทีเรียในสิ่งส่งตรวจที่ป้ายจากหู / ตา / ปาก ',lab_ncd_note = 'Bacterial susceptibility, ear/eye/mouth (swab)' where lab_icd10tm = '0800252';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001157','','การหาภูมิไวรับของเชื้อแบคทีเรียในสิ่งส่งตรวจที่ป้ายจากหู / ตา / ปาก ','Bacterial susceptibility, ear/eye/mouth (swab)','1','0800252'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0800252');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การหาภูมิไวรับของเชื้อแบคทีเรียในสิ่งส่งตรวจที่ป้ายจากแผลตื้นๆ',lab_ncd_note = 'Bacterial susceptibility, superficial wound (swab)' where lab_icd10tm = '0800253';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001158','','การหาภูมิไวรับของเชื้อแบคทีเรียในสิ่งส่งตรวจที่ป้ายจากแผลตื้นๆ','Bacterial susceptibility, superficial wound (swab)','1','0800253'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0800253');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การหาภูมิไวรับของเชื้อแบคทีเรียในสิ่งส่งตรวจที่ป้ายจากแผลลึก',lab_ncd_note = 'Bacterial susceptibility, deep wound (swab)' where lab_icd10tm = '0800254';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001159','','การหาภูมิไวรับของเชื้อแบคทีเรียในสิ่งส่งตรวจที่ป้ายจากแผลลึก','Bacterial susceptibility, deep wound (swab)','1','0800254'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0800254');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การหาภูมิไวรับของเชื้อแบคทีเรียในสิ่งส่งตรวจที่ป้ายจากจมูก',lab_ncd_note = 'Bacterial susceptibility, nose (swab)' where lab_icd10tm = '0800255';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001160','','การหาภูมิไวรับของเชื้อแบคทีเรียในสิ่งส่งตรวจที่ป้ายจากจมูก','Bacterial susceptibility, nose (swab)','1','0800255'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0800255');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การหาภูมิไวรับของเชื้อแบคทีเรียในสิ่งส่งตรวจที่ป้ายจากภายในลำคอ',lab_ncd_note = 'Bacterial susceptibility, throat (swab)' where lab_icd10tm = '0800256';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001161','','การหาภูมิไวรับของเชื้อแบคทีเรียในสิ่งส่งตรวจที่ป้ายจากภายในลำคอ','Bacterial susceptibility, throat (swab)','1','0800256'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0800256');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การหาภูมิไวรับของเชื้อแบคทีเรียในอุจจาระ',lab_ncd_note = 'Bacterial susceptibility, stool' where lab_icd10tm = '0800266';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001162','','การหาภูมิไวรับของเชื้อแบคทีเรียในอุจจาระ','Bacterial susceptibility, stool','1','0800266'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0800266');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การหาภูมิไวรับของเชื้อแบคทีเรียในอาหาร',lab_ncd_note = 'Bacterial susceptibility, food' where lab_icd10tm = '0800269';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001163','','การหาภูมิไวรับของเชื้อแบคทีเรียในอาหาร','Bacterial susceptibility, food','1','0800269'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0800269');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การหาภูมิไวรับของเชื้อแบคทีเรียในสิ่งส่งตรวจที่ทำการแยกแล้ว',lab_ncd_note = 'Bacterial susceptibility, isolate' where lab_icd10tm = '0800270';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001164','','การหาภูมิไวรับของเชื้อแบคทีเรียในสิ่งส่งตรวจที่ทำการแยกแล้ว','Bacterial susceptibility, isolate','1','0800270'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0800270');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การหาภูมิไวรับของเชื้อแบคทีเรียในสิ่งแปลกปลอมจากร่างกายอื่นๆ  มิได้จำแนก',lab_ncd_note = 'Bacterial susceptibility, foreign body NEC' where lab_icd10tm = '0800273';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001165','','การหาภูมิไวรับของเชื้อแบคทีเรียในสิ่งแปลกปลอมจากร่างกายอื่นๆ  มิได้จำแนก','Bacterial susceptibility, foreign body NEC','1','0800273'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0800273');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจระบุเชื้อแบคทีเรียในน้ำ ',lab_ncd_note = 'Bacterial susceptibility, water' where lab_icd10tm = '0800283';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001166','','การตรวจระบุเชื้อแบคทีเรียในน้ำ ','Bacterial susceptibility, water','1','0800283'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0800283');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจระบุเชื้อแบคทีเรียในตัวอย่างจากสิ่งแวดล้อม',lab_ncd_note = 'Bacterial susceptibility, environmental sample' where lab_icd10tm = '0800284';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001167','','การตรวจระบุเชื้อแบคทีเรียในตัวอย่างจากสิ่งแวดล้อม','Bacterial susceptibility, environmental sample','1','0800284'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0800284');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การหาภูมิไวรับของเชื้อแบคทีเรียในสิ่งส่งตรวจชนิดอื่นๆ  มิได้จำแนก',lab_ncd_note = 'Bacterial susceptibility, specimen type NEC' where lab_icd10tm = '0800290';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001168','','การหาภูมิไวรับของเชื้อแบคทีเรียในสิ่งส่งตรวจชนิดอื่นๆ  มิได้จำแนก','Bacterial susceptibility, specimen type NEC','1','0800290'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0800290');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = '-',lab_ncd_note = 'Tuberculous susceptibility sputum' where lab_icd10tm = '0800300';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001169','','-','Tuberculous susceptibility sputum','1','0800300'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0800300');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การหาภูมิไวรับของเชื้อ chlamydial ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Chlamydial susceptibility, any specimen type' where lab_icd10tm = '0810299';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001170','','การหาภูมิไวรับของเชื้อ chlamydial ในสิ่งส่งตรวจชนิดต่างๆ','Chlamydial susceptibility, any specimen type','1','0810299'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0810299');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การหาภูมิไวรับของเชื้อราในสิ่งส่งตรวจที่ทำการแยกแล้ว',lab_ncd_note = 'Fungal susceptibility, isolate' where lab_icd10tm = '0811270';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001171','','การหาภูมิไวรับของเชื้อราในสิ่งส่งตรวจที่ทำการแยกแล้ว','Fungal susceptibility, isolate','1','0811270'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0811270');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การหาภูมิไวรับของเชื้อราในสิ่งส่งตรวจชนิดอื่นๆ  มิได้จำแนก',lab_ncd_note = 'fungal susceptibility, specimen type NEC' where lab_icd10tm = '0811290';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001172','','การหาภูมิไวรับของเชื้อราในสิ่งส่งตรวจชนิดอื่นๆ  มิได้จำแนก','fungal susceptibility, specimen type NEC','1','0811290'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0811290');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การหาภูมิไวรับของเชื้อ mycobacteria ในสิ่งส่งตรวจที่ทำการแยกแล้ว',lab_ncd_note = 'Mycobacteria susceptibility, isolate' where lab_icd10tm = '0812270';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001173','','การหาภูมิไวรับของเชื้อ mycobacteria ในสิ่งส่งตรวจที่ทำการแยกแล้ว','Mycobacteria susceptibility, isolate','1','0812270'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0812270');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การหาภูมิไวรับของเชื้อ mycobacteria ในสิ่งส่งตรวจชนิดอื่นๆ  มิได้จำแนก',lab_ncd_note = 'Mycobacteria susceptibility, specimen type NEC' where lab_icd10tm = '0812290';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001174','','การหาภูมิไวรับของเชื้อ mycobacteria ในสิ่งส่งตรวจชนิดอื่นๆ  มิได้จำแนก','Mycobacteria susceptibility, specimen type NEC','1','0812290'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0812290');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การหาภูมิไวรับของเชื้อ mycobacteria ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Mycoplasma susceptibility, any specimen type ' where lab_icd10tm = '0813299';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001175','','การหาภูมิไวรับของเชื้อ mycobacteria ในสิ่งส่งตรวจชนิดต่างๆ','Mycoplasma susceptibility, any specimen type ','1','0813299'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0813299');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การหาภูมิไวรับของ parasite ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Parasite susceptibility, any specimen type' where lab_icd10tm = '0814299';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001176','','การหาภูมิไวรับของ parasite ในสิ่งส่งตรวจชนิดต่างๆ','Parasite susceptibility, any specimen type','1','0814299'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0814299');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การหาภูมิไวรับของเชื้อไวรัสในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Viral susceptibility any specimen type' where lab_icd10tm = '0815299';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001177','','การหาภูมิไวรับของเชื้อไวรัสในสิ่งส่งตรวจชนิดต่างๆ','Viral susceptibility any specimen type','1','0815299'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0815299');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์หาพิษ botulism ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Botulism toxin assay, any specimen type' where lab_icd10tm = '0820299';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001178','','การวิเคราะห์หาพิษ botulism ในสิ่งส่งตรวจชนิดต่างๆ','Botulism toxin assay, any specimen type','1','0820299'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0820299');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์หาพิษ Clostridium difficile ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Clostridium difficile toxin assay, any specimen type' where lab_icd10tm = '0820699';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001179','','การวิเคราะห์หาพิษ Clostridium difficile ในสิ่งส่งตรวจชนิดต่างๆ','Clostridium difficile toxin assay, any specimen type','1','0820699'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0820699');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์หาพิษ Diphteria/tetanus antitoxin ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Diphteria/tetanus antitoxin assay, any specimen type' where lab_icd10tm = '0821299';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001180','','การวิเคราะห์หาพิษ Diphteria/tetanus antitoxin ในสิ่งส่งตรวจชนิดต่างๆ','Diphteria/tetanus antitoxin assay, any specimen type','1','0821299'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0821299');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์หาพิษ E.coli 0157 ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'E.coli 0157 toxin assay, any specimen type' where lab_icd10tm = '0821499';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001181','','การวิเคราะห์หาพิษ E.coli 0157 ในสิ่งส่งตรวจชนิดต่างๆ','E.coli 0157 toxin assay, any specimen type','1','0821499'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0821499');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์หาพิษ Pertussis ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Pertussis toxin assay, any specimen type' where lab_icd10tm = '0821699';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001182','','การวิเคราะห์หาพิษ Pertussis ในสิ่งส่งตรวจชนิดต่างๆ','Pertussis toxin assay, any specimen type','1','0821699'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0821699');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์หา Toxin/anti-toxin อื่นๆ  มิได้จำแนก ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Toxin/anti-toxin assay NEC, any specimen type' where lab_icd10tm = '0829099';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001183','','การวิเคราะห์หา Toxin/anti-toxin อื่นๆ  มิได้จำแนก ในสิ่งส่งตรวจชนิดต่างๆ','Toxin/anti-toxin assay NEC, any specimen type','1','0829099'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0829099');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจโรคช่องคลอดอักเสบด้วยกล้องจุลทรรศน์ ในสิ่งส่งตรวจที่ป้ายจากอวัยวะเพศ',lab_ncd_note = 'Microscopic examination for vaginitis, genital (swab/smear)' where lab_icd10tm = '0830251';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001184','','การตรวจโรคช่องคลอดอักเสบด้วยกล้องจุลทรรศน์ ในสิ่งส่งตรวจที่ป้ายจากอวัยวะเพศ','Microscopic examination for vaginitis, genital (swab/smear)','1','0830251'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0830251');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจ pneumocystis carinii ด้วยกล้องจุลทรรศน์ ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Microscopic examination for pneumocystis carinii, any specimen type' where lab_icd10tm = '0830499';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001185','','การตรวจ pneumocystis carinii ด้วยกล้องจุลทรรศน์ ในสิ่งส่งตรวจชนิดต่างๆ','Microscopic examination for pneumocystis carinii, any specimen type','1','0830499'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0830499');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจเชื้อปรสิตด้วยกล้องจุลทรรศน์  ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Microscopic examination for parasite, any specimen type' where lab_icd10tm = '0830699';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001186','','การตรวจเชื้อปรสิตด้วยกล้องจุลทรรศน์  ในสิ่งส่งตรวจชนิดต่างๆ','Microscopic examination for parasite, any specimen type','1','0830699'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0830699');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจเชื้อปรสิตด้วยด้วยตาเปล่า ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Macroscopic examination for parasite, any specimen type' where lab_icd10tm = '0831099';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001187','','การตรวจเชื้อปรสิตด้วยด้วยตาเปล่า ในสิ่งส่งตรวจชนิดต่างๆ','Macroscopic examination for parasite, any specimen type','1','0831099'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0831099');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจเชื้อแบคทีเรียด้วยกล้องจุลทรรศน์ ในสิ่งส่งตรวจที่ทำการแยกแล้ว',lab_ncd_note = 'Microscopic examination for bacteria, isolate' where lab_icd10tm = '0831270';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001188','','การตรวจเชื้อแบคทีเรียด้วยกล้องจุลทรรศน์ ในสิ่งส่งตรวจที่ทำการแยกแล้ว','Microscopic examination for bacteria, isolate','1','0831270'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0831270');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจเชื้อแบคทีเรียด้วยกล้องจุลทรรศน์ ในสิ่งส่งตรวจที่ป้ายมากับแผ่นสไลด์',lab_ncd_note = 'Microscopic examination for bacteria, slide/smear' where lab_icd10tm = '0831271';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001189','','การตรวจเชื้อแบคทีเรียด้วยกล้องจุลทรรศน์ ในสิ่งส่งตรวจที่ป้ายมากับแผ่นสไลด์','Microscopic examination for bacteria, slide/smear','1','0831271'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0831271');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจเชื้อแบคทีเรียด้วยกล้องจุลทรรศน์ ในสิ่งส่งตรวจชนิดอื่นๆ  มิได้จำแนก (ตรวจสอบโดยตรง) ',lab_ncd_note = 'Microscopic examination for bacteria, specimen type NEC (direct exam)' where lab_icd10tm = '0831290';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001190','','การตรวจเชื้อแบคทีเรียด้วยกล้องจุลทรรศน์ ในสิ่งส่งตรวจชนิดอื่นๆ  มิได้จำแนก (ตรวจสอบโดยตรง) ','Microscopic examination for bacteria, specimen type NEC (direct exam)','1','0831290'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0831290');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจสิ่งมีชีวิตอื่นๆ มิได้จำแนก ด้วยกล้องจุลทรรศน์ ในสิ่งส่งตรวจที่ทำการแยกแล้ว',lab_ncd_note = 'Microscopic examination for organism NEC, isolate ' where lab_icd10tm = '0831470';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001191','','การตรวจสิ่งมีชีวิตอื่นๆ มิได้จำแนก ด้วยกล้องจุลทรรศน์ ในสิ่งส่งตรวจที่ทำการแยกแล้ว','Microscopic examination for organism NEC, isolate ','1','0831470'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0831470');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจสิ่งมีชีวิตอื่นๆ มิได้จำแนก ด้วยกล้องจุลทรรศน์ ในสิ่งส่งตรวจที่ป้ายมากับแผ่นสไลด์',lab_ncd_note = 'Microscopic examination for organism NEC,slide/smear' where lab_icd10tm = '0831471';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001192','','การตรวจสิ่งมีชีวิตอื่นๆ มิได้จำแนก ด้วยกล้องจุลทรรศน์ ในสิ่งส่งตรวจที่ป้ายมากับแผ่นสไลด์','Microscopic examination for organism NEC,slide/smear','1','0831471'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0831471');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจสิ่งมีชีวิตอื่นๆ มิได้จำแนก ด้วยกล้องจุลทรรศน์ ในสิ่งส่งตรวจชนิดอื่นๆ  มิได้จำแนก  (ตรวจสอบโดยตรง) ',lab_ncd_note = 'Microscopic examination for organism NEC, specimen type NEC (direct exam)' where lab_icd10tm = '0831490';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001193','','การตรวจสิ่งมีชีวิตอื่นๆ มิได้จำแนก ด้วยกล้องจุลทรรศน์ ในสิ่งส่งตรวจชนิดอื่นๆ  มิได้จำแนก  (ตรวจสอบโดยตรง) ','Microscopic examination for organism NEC, specimen type NEC (direct exam)','1','0831490'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0831490');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาไข่และเชื้อปรสิตในอุจจาระ',lab_ncd_note = 'Ova and parasites, stool' where lab_icd10tm = '0834266';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001194','','การตรวจหาไข่และเชื้อปรสิตในอุจจาระ','Ova and parasites, stool','1','0834266'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0834266');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาไข่และเชื้อปรสิตในสิ่งส่งตรวจชนิดอื่นๆ  มิได้จำแนก',lab_ncd_note = 'Ova and parasites, specimen type NEC' where lab_icd10tm = '0834290';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001195','','การตรวจหาไข่และเชื้อปรสิตในสิ่งส่งตรวจชนิดอื่นๆ  มิได้จำแนก','Ova and parasites, specimen type NEC','1','0834290'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0834290');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจนับเชื้อแบคทีเรียในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Bacterial count, any specimen type' where lab_icd10tm = '0837699';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001196','','การตรวจนับเชื้อแบคทีเรียในสิ่งส่งตรวจชนิดต่างๆ','Bacterial count, any specimen type','1','0837699'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0837699');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา acetaminophen ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Acetaminophen, serum/plasma' where lab_icd10tm = '0840202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001197','','การตรวจหายา acetaminophen ในซีรั่ม / พลาสม่า','Acetaminophen, serum/plasma','1','0840202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0840202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา amikacin ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Amikacin, serum/plasma' where lab_icd10tm = '0840402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001198','','การตรวจหายา amikacin ในซีรั่ม / พลาสม่า','Amikacin, serum/plasma','1','0840402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0840402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา amikacin ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Amikacin, other fluid NEC' where lab_icd10tm = '0840415';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001199','','การตรวจหายา amikacin ในของเหลวอื่นๆ มิได้จำแนก','Amikacin, other fluid NEC','1','0840415'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0840415');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา carbamazepine อิสระในซีรั่ม / พลาสม่า',lab_ncd_note = 'Carbamazepine, free, serum/plasma' where lab_icd10tm = '0840602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001200','','การตรวจหายา carbamazepine อิสระในซีรั่ม / พลาสม่า','Carbamazepine, free, serum/plasma','1','0840602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0840602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา carbamazepine ทั้งหมดในซีรั่ม / พลาสม่า',lab_ncd_note = 'Carbamazepine, total, serum/plasma' where lab_icd10tm = '0840802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001201','','การตรวจหายา carbamazepine ทั้งหมดในซีรั่ม / พลาสม่า','Carbamazepine, total, serum/plasma','1','0840802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0840802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา chloramphenicol ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Chloramphenicol, serum/plasma' where lab_icd10tm = '0841002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001202','','การตรวจหายา chloramphenicol ในซีรั่ม / พลาสม่า ','Chloramphenicol, serum/plasma','1','0841002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0841002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา cyclosporine ในเลือด ',lab_ncd_note = 'Cyclosporine, whole blood' where lab_icd10tm = '0841201';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001203','','การตรวจหายา cyclosporine ในเลือด ','Cyclosporine, whole blood','1','0841201'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0841201');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา cyclosporine ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Cyclosporine, serum/plasma' where lab_icd10tm = '0841202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001204','','การตรวจหายา cyclosporine ในซีรั่ม / พลาสม่า','Cyclosporine, serum/plasma','1','0841202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0841202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา digitoxin ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Digitoxin, serum/plasma' where lab_icd10tm = '0841402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001205','','การตรวจหายา digitoxin ในซีรั่ม / พลาสม่า','Digitoxin, serum/plasma','1','0841402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0841402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา digoxin ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Digoxin, serum/plasma' where lab_icd10tm = '0841602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001206','','การตรวจหายา digoxin ในซีรั่ม / พลาสม่า','Digoxin, serum/plasma','1','0841602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0841602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา digoxin ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Digoxin, other fluid NEC' where lab_icd10tm = '0841615';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001207','','การตรวจหายา digoxin ในของเหลวอื่นๆ มิได้จำแนก','Digoxin, other fluid NEC','1','0841615'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0841615');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา digoxin ในเนื้อเยื่ออื่นๆ มิได้จำแนกยาดิจ๊อกซิน',lab_ncd_note = 'Digoxin, tissue NEC' where lab_icd10tm = '0841650';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001208','','การตรวจหายา digoxin ในเนื้อเยื่ออื่นๆ มิได้จำแนกยาดิจ๊อกซิน','Digoxin, tissue NEC','1','0841650'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0841650');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา disopyramide ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Disopyramide, serum/plasma' where lab_icd10tm = '0841802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001209','','การตรวจหายา disopyramide ในซีรั่ม / พลาสม่า','Disopyramide, serum/plasma','1','0841802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0841802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา ethosuximide ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Ethosuximide, serum/plasma' where lab_icd10tm = '0842002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001210','','การตรวจหายา ethosuximide ในซีรั่ม / พลาสม่า','Ethosuximide, serum/plasma','1','0842002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0842002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา gentamicin ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Gentamicin, serum/plasma' where lab_icd10tm = '0842202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001211','','การตรวจหายา gentamicin ในซีรั่ม / พลาสม่า','Gentamicin, serum/plasma','1','0842202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0842202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา gentamicin ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Gentamicin, other fluid NEC' where lab_icd10tm = '0842215';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001212','','การตรวจหายา gentamicin ในของเหลวอื่นๆ มิได้จำแนก','Gentamicin, other fluid NEC','1','0842215'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0842215');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา kanamycin ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Kanamycin, serum/plasma' where lab_icd10tm = '0842402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001213','','การตรวจหายา kanamycin ในซีรั่ม / พลาสม่า','Kanamycin, serum/plasma','1','0842402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0842402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา lidocaine ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Lidocaine, serum/plasma' where lab_icd10tm = '0842602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001214','','การตรวจหายา lidocaine ในซีรั่ม / พลาสม่า','Lidocaine, serum/plasma','1','0842602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0842602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา lithium ในเลือด',lab_ncd_note = 'Lithium, whole blood' where lab_icd10tm = '0842801';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001215','','การตรวจหายา lithium ในเลือด','Lithium, whole blood','1','0842801'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0842801');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา lithium ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Lithium, serum/plasma' where lab_icd10tm = '0842802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001216','','การตรวจหายา lithium ในซีรั่ม / พลาสม่า','Lithium, serum/plasma','1','0842802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0842802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา maprotiline ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Maprotiline, serum/plasma' where lab_icd10tm = '0843002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001217','','การตรวจหายา maprotiline ในซีรั่ม / พลาสม่า','Maprotiline, serum/plasma','1','0843002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0843002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา mephenytoin ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Mephenytoin, serum/plasma' where lab_icd10tm = '0843202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001218','','การตรวจหายา mephenytoin ในซีรั่ม / พลาสม่า','Mephenytoin, serum/plasma','1','0843202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0843202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา methotrexate ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Methotrexate, serum/plasma' where lab_icd10tm = '0843402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001219','','การตรวจหายา methotrexate ในซีรั่ม / พลาสม่า','Methotrexate, serum/plasma','1','0843402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0843402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา methsuximide ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Methsuximide, serum/plasma' where lab_icd10tm = '0843602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001220','','การตรวจหายา methsuximide ในซีรั่ม / พลาสม่า','Methsuximide, serum/plasma','1','0843602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0843602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา N-acetyl procainamide ในซีรั่ม / พลาสม่า',lab_ncd_note = 'N-acetyl procainamide, serum/plasma' where lab_icd10tm = '0843802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001221','','การตรวจหายา N-acetyl procainamide ในซีรั่ม / พลาสม่า','N-acetyl procainamide, serum/plasma','1','0843802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0843802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา netilmicin ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Netilmicin, serum/plasma' where lab_icd10tm = '0844002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001222','','การตรวจหายา netilmicin ในซีรั่ม / พลาสม่า','Netilmicin, serum/plasma','1','0844002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0844002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา phenobarbital อิสระในซีรั่ม / พลาสม่า',lab_ncd_note = 'Phenobarbital, free, serum/plasma' where lab_icd10tm = '0844202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001223','','การตรวจหายา phenobarbital อิสระในซีรั่ม / พลาสม่า','Phenobarbital, free, serum/plasma','1','0844202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0844202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา phenobarbital ทั้งหมดในซีรั่ม / พลาสม่า',lab_ncd_note = 'Phenobarbital, total, serum/plasma' where lab_icd10tm = '0844402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001224','','การตรวจหายา phenobarbital ทั้งหมดในซีรั่ม / พลาสม่า','Phenobarbital, total, serum/plasma','1','0844402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0844402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา phenytoin อิสระในซีรั่ม / พลาสม่า',lab_ncd_note = 'Phenytoin, free, serum/plasma' where lab_icd10tm = '0844602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001225','','การตรวจหายา phenytoin อิสระในซีรั่ม / พลาสม่า','Phenytoin, free, serum/plasma','1','0844602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0844602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา phenytoin ทั้งหมดในซีรั่ม / พลาสม่า',lab_ncd_note = 'Phenytoin, total, serum/plasma' where lab_icd10tm = '0844802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001226','','การตรวจหายา phenytoin ทั้งหมดในซีรั่ม / พลาสม่า','Phenytoin, total, serum/plasma','1','0844802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0844802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา primidone ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Primidone, serum/plasma' where lab_icd10tm = '0845002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001227','','การตรวจหายา primidone ในซีรั่ม / พลาสม่า','Primidone, serum/plasma','1','0845002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0845002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา Procainamide ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Procainamide serum/plasma' where lab_icd10tm = '0845202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001228','','การตรวจหายา Procainamide ในซีรั่ม / พลาสม่า','Procainamide serum/plasma','1','0845202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0845202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา propranolol ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Propranolol, serum/plasma' where lab_icd10tm = '0845402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001229','','การตรวจหายา propranolol ในซีรั่ม / พลาสม่า','Propranolol, serum/plasma','1','0845402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0845402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา quinidine ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Quinidine, serum/plasma' where lab_icd10tm = '0845602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001230','','การตรวจหายา quinidine ในซีรั่ม / พลาสม่า','Quinidine, serum/plasma','1','0845602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0845602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา salicylate ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Salicylate, serum/plasma' where lab_icd10tm = '0845802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001231','','การตรวจหายา salicylate ในซีรั่ม / พลาสม่า','Salicylate, serum/plasma','1','0845802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0845802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา theophylline ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Theophylline, serum/plasma' where lab_icd10tm = '0846002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001232','','การตรวจหายา theophylline ในซีรั่ม / พลาสม่า','Theophylline, serum/plasma','1','0846002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0846002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา tobramycin ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Tobramycin, serum/plasma' where lab_icd10tm = '0846202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001233','','การตรวจหายา tobramycin ในซีรั่ม / พลาสม่า','Tobramycin, serum/plasma','1','0846202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0846202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา tobramycin ในของเหลวอื่น ๆ มิได้จำแนก',lab_ncd_note = 'Tobramycin, other fluid NEC' where lab_icd10tm = '0846215';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001234','','การตรวจหายา tobramycin ในของเหลวอื่น ๆ มิได้จำแนก','Tobramycin, other fluid NEC','1','0846215'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0846215');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา valproate อิสระในซีรั่ม / พลาสม่า',lab_ncd_note = 'Valproate, free, serum/plasma' where lab_icd10tm = '0846402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001235','','การตรวจหายา valproate อิสระในซีรั่ม / พลาสม่า','Valproate, free, serum/plasma','1','0846402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0846402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา valproate ทั้งหมดในซีรั่ม / พลาสม่า',lab_ncd_note = 'Valproate, total, serum/plasma' where lab_icd10tm = '0846602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001236','','การตรวจหายา valproate ทั้งหมดในซีรั่ม / พลาสม่า','Valproate, total, serum/plasma','1','0846602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0846602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา vancomycin ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Vancomycin, serum/plasma' where lab_icd10tm = '0846802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001237','','การตรวจหายา vancomycin ในซีรั่ม / พลาสม่า','Vancomycin, serum/plasma','1','0846802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0846802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหายา vancomycin ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Vancomycin, other fluid NEC' where lab_icd10tm = '0846815';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001238','','การตรวจหายา vancomycin ในของเหลวอื่นๆ มิได้จำแนก','Vancomycin, other fluid NEC','1','0846815'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0846815');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์หายาที่ใช้ในการรักษาโรคอื่นๆ  มิได้ระบุ  จากสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Therapeutic drug assay NEC, any specimen type' where lab_icd10tm = '0849099';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001239','','การวิเคราะห์หายาที่ใช้ในการรักษาโรคอื่นๆ  มิได้ระบุ  จากสิ่งส่งตรวจชนิดต่างๆ','Therapeutic drug assay NEC, any specimen type','1','0849099'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0849099');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาอลูมิเนียมในเลือด ',lab_ncd_note = 'Aluminum, whole blood' where lab_icd10tm = '0850201';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001240','','การตรวจหาอลูมิเนียมในเลือด ','Aluminum, whole blood','1','0850201'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0850201');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาอลูมิเนียมในซีรั่ม / พลาสม่า',lab_ncd_note = 'Aluminum, serum/plasma' where lab_icd10tm = '0850202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001241','','การตรวจหาอลูมิเนียมในซีรั่ม / พลาสม่า','Aluminum, serum/plasma','1','0850202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0850202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาอลูมิเนียมในปัสสาวะ',lab_ncd_note = 'Aluminum, urine' where lab_icd10tm = '0850203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001242','','การตรวจหาอลูมิเนียมในปัสสาวะ','Aluminum, urine','1','0850203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0850203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาอลูมิเนียมในของเหลวอื่น ๆ มิได้จำแนก',lab_ncd_note = 'Aluminum, other fluid NEC' where lab_icd10tm = '0850215';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001243','','การตรวจหาอลูมิเนียมในของเหลวอื่น ๆ มิได้จำแนก','Aluminum, other fluid NEC','1','0850215'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0850215');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาพลวงในเลือด',lab_ncd_note = 'Antimony, whole blood' where lab_icd10tm = '0850401';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001244','','การตรวจหาพลวงในเลือด','Antimony, whole blood','1','0850401'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0850401');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาพลวงในซีรั่ม / พลาสม่า',lab_ncd_note = 'Antimony, serum/plasma' where lab_icd10tm = '0850402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001245','','การตรวจหาพลวงในซีรั่ม / พลาสม่า','Antimony, serum/plasma','1','0850402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0850402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาพลวงในปัสสาวะ',lab_ncd_note = 'Antimony, urine' where lab_icd10tm = '0850403';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001246','','การตรวจหาพลวงในปัสสาวะ','Antimony, urine','1','0850403'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0850403');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา arsenic ในเลือด',lab_ncd_note = 'Arsenic, whole blood' where lab_icd10tm = '0850601';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001247','','การตรวจหา arsenic ในเลือด','Arsenic, whole blood','1','0850601'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0850601');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา arsenic ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Arsenic, serum/plasma' where lab_icd10tm = '0850602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001248','','การตรวจหา arsenic ในซีรั่ม / พลาสม่า','Arsenic, serum/plasma','1','0850602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0850602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา arsenic ในปัสสาวะ ',lab_ncd_note = 'Arsenic, urine' where lab_icd10tm = '0850603';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001249','','การตรวจหา arsenic ในปัสสาวะ ','Arsenic, urine','1','0850603'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0850603');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา arsenic ในของเหลวอื่นๆ  มิได้จำแนก',lab_ncd_note = 'Arsenic, other fluid NEC' where lab_icd10tm = '0850615';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001250','','การตรวจหา arsenic ในของเหลวอื่นๆ  มิได้จำแนก','Arsenic, other fluid NEC','1','0850615'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0850615');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา arsenic ในเนื้อเยื่ออื่นๆ  มิได้จำแนก',lab_ncd_note = 'Arsenic, tissue NEC' where lab_icd10tm = '0850650';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001251','','การตรวจหา arsenic ในเนื้อเยื่ออื่นๆ  มิได้จำแนก','Arsenic, tissue NEC','1','0850650'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0850650');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาโบรอนในเลือด ',lab_ncd_note = 'Boron, whole blood' where lab_icd10tm = '0850801';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001252','','การตรวจหาโบรอนในเลือด ','Boron, whole blood','1','0850801'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0850801');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาโบรอนในซีรั่ม / พลาสม่า',lab_ncd_note = 'Boron, serum/plasma' where lab_icd10tm = '0850802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001253','','การตรวจหาโบรอนในซีรั่ม / พลาสม่า','Boron, serum/plasma','1','0850802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0850802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาโบรอนในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Boron, other fluid NEC' where lab_icd10tm = '0850815';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001254','','การตรวจหาโบรอนในของเหลวอื่นๆ มิได้จำแนก','Boron, other fluid NEC','1','0850815'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0850815');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาโบรไมด์ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Bromide, serum/plasma' where lab_icd10tm = '0851002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001255','','การตรวจหาโบรไมด์ในซีรั่ม / พลาสม่า','Bromide, serum/plasma','1','0851002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0851002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาโบรไมด์ในปัสสาวะ ',lab_ncd_note = 'Bromide, urine' where lab_icd10tm = '0851003';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001256','','การตรวจหาโบรไมด์ในปัสสาวะ ','Bromide, urine','1','0851003'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0851003');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาแคดเมียมในเลือด',lab_ncd_note = 'Cadmium, whole blood' where lab_icd10tm = '0851201';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001257','','การตรวจหาแคดเมียมในเลือด','Cadmium, whole blood','1','0851201'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0851201');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาแคดเมียมในปัสสาวะ ',lab_ncd_note = 'Cadmium, urine' where lab_icd10tm = '0851203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001258','','การตรวจหาแคดเมียมในปัสสาวะ ','Cadmium, urine','1','0851203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0851203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาแคดเมียมในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Cadmium, other fluid NEC' where lab_icd10tm = '0851215';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001259','','การตรวจหาแคดเมียมในของเหลวอื่นๆ มิได้จำแนก','Cadmium, other fluid NEC','1','0851215'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0851215');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาซีเซียมในเลือด',lab_ncd_note = 'Cesium, whole blood' where lab_icd10tm = '0851401';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001260','','การตรวจหาซีเซียมในเลือด','Cesium, whole blood','1','0851401'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0851401');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาซีเซียมในซีรั่ม / พลาสม่า',lab_ncd_note = 'Cesium, serum/plasma' where lab_icd10tm = '0851402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001261','','การตรวจหาซีเซียมในซีรั่ม / พลาสม่า','Cesium, serum/plasma','1','0851402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0851402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาซีเซียมในปัสสาวะ',lab_ncd_note = 'Cesium, urine' where lab_icd10tm = '0851403';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001262','','การตรวจหาซีเซียมในปัสสาวะ','Cesium, urine','1','0851403'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0851403');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาซีเซียมในเม็ดเลือดแดง',lab_ncd_note = 'Cesium, RBC' where lab_icd10tm = '0851417';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001263','','การตรวจหาซีเซียมในเม็ดเลือดแดง','Cesium, RBC','1','0851417'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0851417');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาทองคำในเลือด ',lab_ncd_note = 'Gold, whole blood' where lab_icd10tm = '0851601';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001264','','การตรวจหาทองคำในเลือด ','Gold, whole blood','1','0851601'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0851601');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาทองคำในซีรั่ม / พลาสม่า',lab_ncd_note = 'Gold, serum/plasma' where lab_icd10tm = '0851602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001265','','การตรวจหาทองคำในซีรั่ม / พลาสม่า','Gold, serum/plasma','1','0851602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0851602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาทองคำในปัสสาวะ',lab_ncd_note = 'Gold, urine' where lab_icd10tm = '0851603';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001266','','การตรวจหาทองคำในปัสสาวะ','Gold, urine','1','0851603'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0851603');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาทองคำในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Gold, other fluid NEC' where lab_icd10tm = '0851615';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001267','','การตรวจหาทองคำในของเหลวอื่นๆ มิได้จำแนก','Gold, other fluid NEC','1','0851615'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0851615');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาตะกั่วในเลือด',lab_ncd_note = 'Lead, whole blood' where lab_icd10tm = '0851801';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001268','','การตรวจหาตะกั่วในเลือด','Lead, whole blood','1','0851801'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0851801');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาตะกั่วในซีรั่ม / พลาสม่า',lab_ncd_note = 'Lead, serum/plasma' where lab_icd10tm = '0851802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001269','','การตรวจหาตะกั่วในซีรั่ม / พลาสม่า','Lead, serum/plasma','1','0851802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0851802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาตะกั่วในปัสสาวะ',lab_ncd_note = 'Lead, urine' where lab_icd10tm = '0851803';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001270','','การตรวจหาตะกั่วในปัสสาวะ','Lead, urine','1','0851803'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0851803');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาตะกั่วในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Lead, other fluid NEC' where lab_icd10tm = '0851815';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001271','','การตรวจหาตะกั่วในของเหลวอื่นๆ มิได้จำแนก','Lead, other fluid NEC','1','0851815'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0851815');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาตะกั่วในเม็ดเลือดแดง',lab_ncd_note = 'Lead, RBC' where lab_icd10tm = '0851817';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001272','','การตรวจหาตะกั่วในเม็ดเลือดแดง','Lead, RBC','1','0851817'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0851817');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาตะกั่วในเนื้อเยื่ออื่นๆ มิได้จำแนก',lab_ncd_note = 'Lead, tissue NEC' where lab_icd10tm = '0851850';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001273','','การตรวจหาตะกั่วในเนื้อเยื่ออื่นๆ มิได้จำแนก','Lead, tissue NEC','1','0851850'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0851850');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา mercury ในเลือด ',lab_ncd_note = 'Mercury, whole blood' where lab_icd10tm = '0852001';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001274','','การตรวจหา mercury ในเลือด ','Mercury, whole blood','1','0852001'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0852001');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา mercury ในเซรั่ม, พลาสม่า',lab_ncd_note = 'Mercury, serum/plasma' where lab_icd10tm = '0852002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001275','','การตรวจหา mercury ในเซรั่ม, พลาสม่า','Mercury, serum/plasma','1','0852002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0852002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา mercury ในปัสสาวะ ',lab_ncd_note = 'Mercury, urine' where lab_icd10tm = '0852003';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001276','','การตรวจหา mercury ในปัสสาวะ ','Mercury, urine','1','0852003'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0852003');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา mercury ในของเหลวอื่นๆ  มิได้จำแนก',lab_ncd_note = 'Mercury, other fluid NEC' where lab_icd10tm = '0852015';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001277','','การตรวจหา mercury ในของเหลวอื่นๆ  มิได้จำแนก','Mercury, other fluid NEC','1','0852015'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0852015');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา mercury ในเนื้อเยื่ออื่นๆ มิได้จำแนก',lab_ncd_note = 'Mercury, tissue NEC' where lab_icd10tm = '0852050';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001278','','การตรวจหา mercury ในเนื้อเยื่ออื่นๆ มิได้จำแนก','Mercury, tissue NEC','1','0852050'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0852050');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา silver ในเลือด',lab_ncd_note = 'Silver, whole blood' where lab_icd10tm = '0852201';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001279','','การตรวจหา silver ในเลือด','Silver, whole blood','1','0852201'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0852201');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา silver ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Silver, serum/plasma' where lab_icd10tm = '0852202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001280','','การตรวจหา silver ในซีรั่ม / พลาสม่า','Silver, serum/plasma','1','0852202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0852202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา silver ในปัสสาวะ',lab_ncd_note = 'Silver, urine' where lab_icd10tm = '0852203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001281','','การตรวจหา silver ในปัสสาวะ','Silver, urine','1','0852203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0852203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา silver ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Silver, other fluid NEC' where lab_icd10tm = '0852215';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001282','','การตรวจหา silver ในของเหลวอื่นๆ มิได้จำแนก','Silver, other fluid NEC','1','0852215'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0852215');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาโลหะอื่นๆ มิได้จำแนก จากสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Metal NEC, any specimen type' where lab_icd10tm = '0859099';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001283','','การตรวจหาโลหะอื่นๆ มิได้จำแนก จากสิ่งส่งตรวจชนิดต่างๆ','Metal NEC, any specimen type','1','0859099'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0859099');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา alprazolam ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Alprazolam, serum/plasma' where lab_icd10tm = '0860202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001284','','การตรวจหา alprazolam ในซีรั่ม / พลาสม่า','Alprazolam, serum/plasma','1','0860202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0860202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา alprazolam ในปัสสาวะ',lab_ncd_note = 'Alprazolam, urine' where lab_icd10tm = '0860203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001285','','การตรวจหา alprazolam ในปัสสาวะ','Alprazolam, urine','1','0860203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0860203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา amiodarone ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Amiodarone, serum/plasma' where lab_icd10tm = '0860402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001286','','การตรวจหา amiodarone ในซีรั่ม / พลาสม่า','Amiodarone, serum/plasma','1','0860402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0860402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา barbiturates ในเลือด',lab_ncd_note = 'Barbiturates, whole blood' where lab_icd10tm = '0860601';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001287','','การตรวจหา barbiturates ในเลือด','Barbiturates, whole blood','1','0860601'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0860601');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา barbiturates ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Barbiturates, serum/plasma' where lab_icd10tm = '0860602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001288','','การตรวจหา barbiturates ในซีรั่ม / พลาสม่า','Barbiturates, serum/plasma','1','0860602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0860602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา barbiturates ในปัสสาวะ',lab_ncd_note = 'Barbiturates, urine' where lab_icd10tm = '0860603';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001289','','การตรวจหา barbiturates ในปัสสาวะ','Barbiturates, urine','1','0860603'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0860603');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา barbiturates ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Barbiturates, other fluid NEC' where lab_icd10tm = '0860615';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001290','','การตรวจหา barbiturates ในของเหลวอื่นๆ มิได้จำแนก','Barbiturates, other fluid NEC','1','0860615'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0860615');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา benzodiapines ในเลือด ',lab_ncd_note = 'Benzodiapines, whole blood' where lab_icd10tm = '0860801';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001291','','การตรวจหา benzodiapines ในเลือด ','Benzodiapines, whole blood','1','0860801'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0860801');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา benzodiapines ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Benzodiapines, serum/plasma' where lab_icd10tm = '0860802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001292','','การตรวจหา benzodiapines ในซีรั่ม / พลาสม่า','Benzodiapines, serum/plasma','1','0860802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0860802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา benzodiapines ในปัสสาวะ',lab_ncd_note = 'Benzodiapines, urine' where lab_icd10tm = '0860803';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001293','','การตรวจหา benzodiapines ในปัสสาวะ','Benzodiapines, urine','1','0860803'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0860803');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา benzodiapines ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Benzodiapines, other fluid NEC' where lab_icd10tm = '0860815';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001294','','การตรวจหา benzodiapines ในของเหลวอื่นๆ มิได้จำแนก','Benzodiapines, other fluid NEC','1','0860815'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0860815');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาคาเฟอีนในซีรั่ม / พลาสม่า',lab_ncd_note = 'Caffeine, serum/plasma' where lab_icd10tm = '0861002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001295','','การตรวจหาคาเฟอีนในซีรั่ม / พลาสม่า','Caffeine, serum/plasma','1','0861002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0861002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาคาเฟอีนในปัสสาวะ',lab_ncd_note = 'Caffeine, urine' where lab_icd10tm = '0861003';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001296','','การตรวจหาคาเฟอีนในปัสสาวะ','Caffeine, urine','1','0861003'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0861003');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา cannabis/cannabinoid ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Cannabis/cannabinoid, serum/plasma' where lab_icd10tm = '0861202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001297','','การตรวจหา cannabis/cannabinoid ในซีรั่ม / พลาสม่า','Cannabis/cannabinoid, serum/plasma','1','0861202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0861202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา cannabis/cannabinoid ในปัสสาวะ',lab_ncd_note = 'Cannabis/cannabinoid, urine' where lab_icd10tm = '0861203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001298','','การตรวจหา cannabis/cannabinoid ในปัสสาวะ','Cannabis/cannabinoid, urine','1','0861203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0861203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา cannabis/cannabinoid ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Cannabis/cannabinoid, other fluid NEC' where lab_icd10tm = '0861215';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001299','','การตรวจหา cannabis/cannabinoid ในของเหลวอื่นๆ มิได้จำแนก','Cannabis/cannabinoid, other fluid NEC','1','0861215'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0861215');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาโคเคน (ผลิตผลของกระบวนการสันดาป) ในปัสสาวะ ',lab_ncd_note = 'Cocaine(metabolites), urine' where lab_icd10tm = '0861403';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001300','','การตรวจหาโคเคน (ผลิตผลของกระบวนการสันดาป) ในปัสสาวะ ','Cocaine(metabolites), urine','1','0861403'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0861403');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาโคเคน (ผลิตผลของกระบวนการสันดาป) ในของเหลวอื่น ๆ มิได้จำแนก',lab_ncd_note = 'Cocaine(metabolites), other fluid NEC' where lab_icd10tm = '0861415';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001301','','การตรวจหาโคเคน (ผลิตผลของกระบวนการสันดาป) ในของเหลวอื่น ๆ มิได้จำแนก','Cocaine(metabolites), other fluid NEC','1','0861415'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0861415');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา codeine ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Codeine, serum/plasma' where lab_icd10tm = '0861602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001302','','การตรวจหา codeine ในซีรั่ม / พลาสม่า','Codeine, serum/plasma','1','0861602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0861602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา codeine/morphine ในปัสสาวะ',lab_ncd_note = 'Codeine/morphine, urine' where lab_icd10tm = '0861803';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001303','','การตรวจหา codeine/morphine ในปัสสาวะ','Codeine/morphine, urine','1','0861803'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0861803');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา haloperidol ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Haloperidol, serum/plasma' where lab_icd10tm = '0862002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001304','','การตรวจหา haloperidol ในซีรั่ม / พลาสม่า','Haloperidol, serum/plasma','1','0862002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0862002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา haloperidol ในปัสสาวะ',lab_ncd_note = 'Haloperidol, urine' where lab_icd10tm = '0862203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001305','','การตรวจหา haloperidol ในปัสสาวะ','Haloperidol, urine','1','0862203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0862203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา ibuprofen ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Ibuprofen, serum/plasma' where lab_icd10tm = '0862402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001306','','การตรวจหา ibuprofen ในซีรั่ม / พลาสม่า','Ibuprofen, serum/plasma','1','0862402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0862402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา lysergic acid diethylamide ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Lysergic acid diethylamide, serum/plasma' where lab_icd10tm = '0862602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001307','','การตรวจหา lysergic acid diethylamide ในซีรั่ม / พลาสม่า','Lysergic acid diethylamide, serum/plasma','1','0862602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0862602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา lysergic acid diethylamide ในปัสสาวะ',lab_ncd_note = 'Lysergic acid diethylamide, urine' where lab_icd10tm = '0862603';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001308','','การตรวจหา lysergic acid diethylamide ในปัสสาวะ','Lysergic acid diethylamide, urine','1','0862603'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0862603');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา meperidine ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Meperidine, serum/plasma' where lab_icd10tm = '0862802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001309','','การตรวจหา meperidine ในซีรั่ม / พลาสม่า','Meperidine, serum/plasma','1','0862802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0862802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา meperidine ในปัสสาวะ',lab_ncd_note = 'Meperidine, urine' where lab_icd10tm = '0862803';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001310','','การตรวจหา meperidine ในปัสสาวะ','Meperidine, urine','1','0862803'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0862803');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา methadone ในเลือด',lab_ncd_note = 'Methadone, whole blood' where lab_icd10tm = '0863001';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001311','','การตรวจหา methadone ในเลือด','Methadone, whole blood','1','0863001'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0863001');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา methadone ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Methadone, serum/plasma' where lab_icd10tm = '0863002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001312','','การตรวจหา methadone ในซีรั่ม / พลาสม่า','Methadone, serum/plasma','1','0863002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0863002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา methadone ในปัสสาวะ ',lab_ncd_note = 'Methadone, urine' where lab_icd10tm = '0863003';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001313','','การตรวจหา methadone ในปัสสาวะ ','Methadone, urine','1','0863003'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0863003');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา methadone ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Methadone, other fluid NEC' where lab_icd10tm = '0863015';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001314','','การตรวจหา methadone ในของเหลวอื่นๆ มิได้จำแนก','Methadone, other fluid NEC','1','0863015'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0863015');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา methaqualone ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Methaqualone, serum/plasma' where lab_icd10tm = '0863202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001315','','การตรวจหา methaqualone ในซีรั่ม / พลาสม่า','Methaqualone, serum/plasma','1','0863202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0863202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา methaqualone ในปัสสาวะ ',lab_ncd_note = 'Methaqualone, urine' where lab_icd10tm = '0863203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001316','','การตรวจหา methaqualone ในปัสสาวะ ','Methaqualone, urine','1','0863203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0863203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา methyprylon ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Methyprylon, serum/plasma' where lab_icd10tm = '0863402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001317','','การตรวจหา methyprylon ในซีรั่ม / พลาสม่า','Methyprylon, serum/plasma','1','0863402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0863402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา methyprylon ในปัสสาวะ ',lab_ncd_note = 'Methyprylon, urine' where lab_icd10tm = '0863403';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001318','','การตรวจหา methyprylon ในปัสสาวะ ','Methyprylon, urine','1','0863403'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0863403');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา มอร์ฟีนในซีรั่ม / พลาสม่า',lab_ncd_note = 'Morphine, serum/plasma' where lab_icd10tm = '0863602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001319','','การตรวจหา มอร์ฟีนในซีรั่ม / พลาสม่า','Morphine, serum/plasma','1','0863602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0863602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา มอร์ฟีนในปัสสาวะ ',lab_ncd_note = 'Morphine, urine' where lab_icd10tm = '0863603';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001320','','การตรวจหา มอร์ฟีนในปัสสาวะ ','Morphine, urine','1','0863603'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0863603');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา phencyclidine ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Phencyclidine, serum/plasma' where lab_icd10tm = '0863802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001321','','การตรวจหา phencyclidine ในซีรั่ม / พลาสม่า','Phencyclidine, serum/plasma','1','0863802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0863802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา phencyclidine ในปัสสาวะ',lab_ncd_note = 'Phencyclidine, urine' where lab_icd10tm = '0863803';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001322','','การตรวจหา phencyclidine ในปัสสาวะ','Phencyclidine, urine','1','0863803'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0863803');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา phenothiazine ในเลือด',lab_ncd_note = 'Phenothiazine, whole blood' where lab_icd10tm = '0864001';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001323','','การตรวจหา phenothiazine ในเลือด','Phenothiazine, whole blood','1','0864001'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0864001');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา phenothiazine ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Phenothiazine, serum/plasma' where lab_icd10tm = '0864002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001324','','การตรวจหา phenothiazine ในซีรั่ม / พลาสม่า','Phenothiazine, serum/plasma','1','0864002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0864002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา phenothiazine ในปัสสาวะ',lab_ncd_note = 'Phenothiazine, urine' where lab_icd10tm = '0864003';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001325','','การตรวจหา phenothiazine ในปัสสาวะ','Phenothiazine, urine','1','0864003'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0864003');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา phenothiazine ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Phenothiazine, other fluid NEC' where lab_icd10tm = '0864015';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001326','','การตรวจหา phenothiazine ในของเหลวอื่นๆ มิได้จำแนก','Phenothiazine, other fluid NEC','1','0864015'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0864015');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาควินินในซีรั่ม / พลาสม่า',lab_ncd_note = 'Quinine, serum/plasma' where lab_icd10tm = '0864202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001327','','การตรวจหาควินินในซีรั่ม / พลาสม่า','Quinine, serum/plasma','1','0864202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0864202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาควินินในปัสสาวะ',lab_ncd_note = 'Quinine, urine' where lab_icd10tm = '0864203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001328','','การตรวจหาควินินในปัสสาวะ','Quinine, urine','1','0864203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0864203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา tricyclic antidepressant ในเลือด ',lab_ncd_note = 'Tricyclic antidepressant, whole blood' where lab_icd10tm = '0864401';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001329','','การตรวจหา tricyclic antidepressant ในเลือด ','Tricyclic antidepressant, whole blood','1','0864401'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0864401');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา tricyclic antidepressant ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Tricyclic antidepressant, serum/plasma' where lab_icd10tm = '0864402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001330','','การตรวจหา tricyclic antidepressant ในซีรั่ม / พลาสม่า','Tricyclic antidepressant, serum/plasma','1','0864402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0864402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา tricyclic antidepressant ในปัสสาวะ',lab_ncd_note = 'Tricyclic antidepressant, urine' where lab_icd10tm = '0864403';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001331','','การตรวจหา tricyclic antidepressant ในปัสสาวะ','Tricyclic antidepressant, urine','1','0864403'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0864403');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา tricyclic antidepressant ในของเหลวอื่นๆ  มิได้จำแนก',lab_ncd_note = 'Tricyclic antidepressant, other fluid NEC' where lab_icd10tm = '0864415';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001332','','การตรวจหา tricyclic antidepressant ในของเหลวอื่นๆ  มิได้จำแนก','Tricyclic antidepressant, other fluid NEC','1','0864415'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0864415');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา warfarin ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Warfarin, serum/plasma' where lab_icd10tm = '0864602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001333','','การตรวจหา warfarin ในซีรั่ม / พลาสม่า','Warfarin, serum/plasma','1','0864602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0864602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์ตรวจหายาและสารเสพติดอื่นๆ  มิได้ระบุ  ในสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Drug assay NEC, any specimen type' where lab_icd10tm = '0869099';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001334','','การวิเคราะห์ตรวจหายาและสารเสพติดอื่นๆ  มิได้ระบุ  ในสิ่งส่งตรวจชนิดต่างๆ','Drug assay NEC, any specimen type','1','0869099'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0869099');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหา acetaminophen ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Acetaminophen screen, serum/plasma' where lab_icd10tm = '0870202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001335','','การตรวจคัดกรองหา acetaminophen ในซีรั่ม / พลาสม่า','Acetaminophen screen, serum/plasma','1','0870202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0870202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหา acetaminophen ในปัสสาวะ ',lab_ncd_note = 'Acetaminophen screen, urine' where lab_icd10tm = '0870203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001336','','การตรวจคัดกรองหา acetaminophen ในปัสสาวะ ','Acetaminophen screen, urine','1','0870203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0870203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหา acetaminophen ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Acetaminophen screen, other fluid NEC' where lab_icd10tm = '0870215';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001337','','การตรวจคัดกรองหา acetaminophen ในของเหลวอื่นๆ มิได้จำแนก','Acetaminophen screen, other fluid NEC','1','0870215'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0870215');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหา amphetamine ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Amphetamine screen, serum/plasma' where lab_icd10tm = '0870402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001338','','การตรวจคัดกรองหา amphetamine ในซีรั่ม / พลาสม่า','Amphetamine screen, serum/plasma','1','0870402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0870402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหา amphetamine ในปัสสาวะ ',lab_ncd_note = 'Amphetamine screen, urine' where lab_icd10tm = '0870403';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001339','','การตรวจคัดกรองหา amphetamine ในปัสสาวะ ','Amphetamine screen, urine','1','0870403'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0870403');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหา amphetamine ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Amphetamine screen, other fluid NEC' where lab_icd10tm = '0870415';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001340','','การตรวจคัดกรองหา amphetamine ในของเหลวอื่นๆ มิได้จำแนก','Amphetamine screen, other fluid NEC','1','0870415'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0870415');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหา barbiturates ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Barbiturates screen, serum/plasma' where lab_icd10tm = '0870602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001341','','การตรวจคัดกรองหา barbiturates ในซีรั่ม / พลาสม่า','Barbiturates screen, serum/plasma','1','0870602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0870602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหา barbiturates ในปัสสาวะ ',lab_ncd_note = 'Barbiturates screen, urine' where lab_icd10tm = '0870603';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001342','','การตรวจคัดกรองหา barbiturates ในปัสสาวะ ','Barbiturates screen, urine','1','0870603'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0870603');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหา barbiturates ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Barbiturates screen, other fluid NEC' where lab_icd10tm = '0870615';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001343','','การตรวจคัดกรองหา barbiturates ในของเหลวอื่นๆ มิได้จำแนก','Barbiturates screen, other fluid NEC','1','0870615'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0870615');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหา benzodiazepines ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Benzodiazepines screen, serum/plasma' where lab_icd10tm = '0870802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001344','','การตรวจคัดกรองหา benzodiazepines ในซีรั่ม / พลาสม่า ','Benzodiazepines screen, serum/plasma','1','0870802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0870802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหา benzodiazepines ในปัสสาวะ ',lab_ncd_note = 'Benzodiazepines screen, urine' where lab_icd10tm = '0870803';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001345','','การตรวจคัดกรองหา benzodiazepines ในปัสสาวะ ','Benzodiazepines screen, urine','1','0870803'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0870803');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหา benzodiazepines ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Benzodiazepines screen, other fluid NEC' where lab_icd10tm = '0870815';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001346','','การตรวจคัดกรองหา benzodiazepines ในของเหลวอื่นๆ มิได้จำแนก','Benzodiazepines screen, other fluid NEC','1','0870815'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0870815');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหา cannabis/cannabinoid ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Cannabis/cannabinoid screen, serum/plasma' where lab_icd10tm = '0871002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001347','','การตรวจคัดกรองหา cannabis/cannabinoid ในซีรั่ม / พลาสม่า ','Cannabis/cannabinoid screen, serum/plasma','1','0871002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0871002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหา cannabis/cannabinoid ในปัสสาวะ ',lab_ncd_note = 'Cannabis/cannabinoid screen, urine' where lab_icd10tm = '0871003';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001348','','การตรวจคัดกรองหา cannabis/cannabinoid ในปัสสาวะ ','Cannabis/cannabinoid screen, urine','1','0871003'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0871003');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหา cannabis/cannabinoid ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Cannabis/cannabinoid screen, other fluid NEC' where lab_icd10tm = '0871015';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001349','','การตรวจคัดกรองหา cannabis/cannabinoid ในของเหลวอื่นๆ มิได้จำแนก','Cannabis/cannabinoid screen, other fluid NEC','1','0871015'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0871015');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหาโคเคน (ผลิตผลของกระบวนการสันดาป) ในปัสสาวะ ',lab_ncd_note = 'Cocaine(metabolites) screen, urine' where lab_icd10tm = '0871203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001350','','การตรวจคัดกรองหาโคเคน (ผลิตผลของกระบวนการสันดาป) ในปัสสาวะ ','Cocaine(metabolites) screen, urine','1','0871203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0871203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหาโคเคน (ผลิตผลของกระบวนการสันดาป) ในของเหลวอื่นๆ  มิได้จำแนก',lab_ncd_note = 'Cocaine(metabolites) screen, other fluid NEC' where lab_icd10tm = '0871215';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001351','','การตรวจคัดกรองหาโคเคน (ผลิตผลของกระบวนการสันดาป) ในของเหลวอื่นๆ  มิได้จำแนก','Cocaine(metabolites) screen, other fluid NEC','1','0871215'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0871215');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหา lysergic acid diethylamide ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Lysergic acid diethylamide screen, serum/plasma' where lab_icd10tm = '0871402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001352','','การตรวจคัดกรองหา lysergic acid diethylamide ในซีรั่ม / พลาสม่า ','Lysergic acid diethylamide screen, serum/plasma','1','0871402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0871402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหา lysergic acid diethylamide ในปัสสาวะ',lab_ncd_note = 'Lysergic acid diethylamide screen, urine' where lab_icd10tm = '0871403';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001353','','การตรวจคัดกรองหา lysergic acid diethylamide ในปัสสาวะ','Lysergic acid diethylamide screen, urine','1','0871403'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0871403');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหามอร์ฟีนในซีรั่ม / พลาสม่า',lab_ncd_note = 'Morphine screen, serum/plasma' where lab_icd10tm = '0871602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001354','','การตรวจคัดกรองหามอร์ฟีนในซีรั่ม / พลาสม่า','Morphine screen, serum/plasma','1','0871602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0871602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหามอร์ฟีนในปัสสาวะ ',lab_ncd_note = 'Morphine screen, urine' where lab_icd10tm = '0871603';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001355','','การตรวจคัดกรองหามอร์ฟีนในปัสสาวะ ','Morphine screen, urine','1','0871603'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0871603');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหา opiate ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Opiate screen, serum/plasma' where lab_icd10tm = '0871802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001356','','การตรวจคัดกรองหา opiate ในซีรั่ม / พลาสม่า ','Opiate screen, serum/plasma','1','0871802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0871802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหา opiate ในปัสสาวะ ',lab_ncd_note = 'Opiate screen, urine' where lab_icd10tm = '0871803';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001357','','การตรวจคัดกรองหา opiate ในปัสสาวะ ','Opiate screen, urine','1','0871803'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0871803');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหา Opiate ในของเหลวอื่น ๆ มิได้จำแนก',lab_ncd_note = 'Opiate screen, other fluid NEC' where lab_icd10tm = '0871815';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001358','','การตรวจคัดกรองหา Opiate ในของเหลวอื่น ๆ มิได้จำแนก','Opiate screen, other fluid NEC','1','0871815'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0871815');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหา phencyclidine ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Phencyclidine screen, serum/plasma' where lab_icd10tm = '0872002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001359','','การตรวจคัดกรองหา phencyclidine ในซีรั่ม / พลาสม่า','Phencyclidine screen, serum/plasma','1','0872002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0872002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหา phencyclidine ในปัสสาวะ ',lab_ncd_note = 'Phencyclidine screen, urine' where lab_icd10tm = '0872003';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001360','','การตรวจคัดกรองหา phencyclidine ในปัสสาวะ ','Phencyclidine screen, urine','1','0872003'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0872003');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหา phenothiazine ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Phenothiazine screen, serum/plasma' where lab_icd10tm = '0872202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001361','','การตรวจคัดกรองหา phenothiazine ในซีรั่ม / พลาสม่า','Phenothiazine screen, serum/plasma','1','0872202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0872202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหา phenothiazine ในปัสสาวะ ',lab_ncd_note = 'Phenothiazine screen, urine' where lab_icd10tm = '0872203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001362','','การตรวจคัดกรองหา phenothiazine ในปัสสาวะ ','Phenothiazine screen, urine','1','0872203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0872203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหา phenothiazine ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Phenothiazine screen, other fluid NEC' where lab_icd10tm = '0872215';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001363','','การตรวจคัดกรองหา phenothiazine ในของเหลวอื่นๆ มิได้จำแนก','Phenothiazine screen, other fluid NEC','1','0872215'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0872215');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหาซาลิไซเลตในซีรั่ม / พลาสม่า',lab_ncd_note = 'Salicylate screen, serum/plasma' where lab_icd10tm = '0872402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001364','','การตรวจคัดกรองหาซาลิไซเลตในซีรั่ม / พลาสม่า','Salicylate screen, serum/plasma','1','0872402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0872402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหาซาลิไซเลตในปัสสาวะ',lab_ncd_note = 'Salicylate screen, urine' where lab_icd10tm = '0872403';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001365','','การตรวจคัดกรองหาซาลิไซเลตในปัสสาวะ','Salicylate screen, urine','1','0872403'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0872403');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหาซาลิไซเลตในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Salicylate screen, other fluid NEC' where lab_icd10tm = '0872415';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001366','','การตรวจคัดกรองหาซาลิไซเลตในของเหลวอื่นๆ มิได้จำแนก','Salicylate screen, other fluid NEC','1','0872415'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0872415');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหา tricyclic antidepressants ในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Tricyclic antidepressants screen, serum/plasma' where lab_icd10tm = '0872602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001367','','การตรวจคัดกรองหา tricyclic antidepressants ในซีรั่ม / พลาสม่า ','Tricyclic antidepressants screen, serum/plasma','1','0872602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0872602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหา tricyclic antidepressants ในปัสสาวะ ',lab_ncd_note = 'Tricyclic antidepressants screen, urine' where lab_icd10tm = '0872603';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001368','','การตรวจคัดกรองหา tricyclic antidepressants ในปัสสาวะ ','Tricyclic antidepressants screen, urine','1','0872603'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0872603');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหา tricyclic antidepressants ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Tricyclic antidepressants screen, other fluid NEC' where lab_icd10tm = '0872615';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001369','','การตรวจคัดกรองหา tricyclic antidepressants ในของเหลวอื่นๆ มิได้จำแนก','Tricyclic antidepressants screen, other fluid NEC','1','0872615'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0872615');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหายาอื่นๆ มิได้ระบุ ตัวเดียว ในพลาสม่า',lab_ncd_note = 'Drug screen NOS, single drug, serum/plasma' where lab_icd10tm = '0878002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001370','','การตรวจคัดกรองหายาอื่นๆ มิได้ระบุ ตัวเดียว ในพลาสม่า','Drug screen NOS, single drug, serum/plasma','1','0878002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0878002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหายาอื่นๆ มิได้ระบุ ตัวเดียว ในปัสสาวะ ',lab_ncd_note = 'Drug screen NOS, single drug, urine' where lab_icd10tm = '0878003';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001371','','การตรวจคัดกรองหายาอื่นๆ มิได้ระบุ ตัวเดียว ในปัสสาวะ ','Drug screen NOS, single drug, urine','1','0878003'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0878003');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหายาอื่นๆ มิได้ระบุ ตัวเดียว ในของเหลวอื่น ๆ มิได้จำแนก',lab_ncd_note = 'Drug screen NOS, single drug, other fluid NEC' where lab_icd10tm = '0878015';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001372','','การตรวจคัดกรองหายาอื่นๆ มิได้ระบุ ตัวเดียว ในของเหลวอื่น ๆ มิได้จำแนก','Drug screen NOS, single drug, other fluid NEC','1','0878015'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0878015');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหายาอื่นๆ มิได้ระบุ ตัวเดียว ในเซรั่ม / พลาสม่า',lab_ncd_note = 'Drug screen NOS, single drug, serum/plasma' where lab_icd10tm = '0878202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001373','','การตรวจคัดกรองหายาอื่นๆ มิได้ระบุ ตัวเดียว ในเซรั่ม / พลาสม่า','Drug screen NOS, single drug, serum/plasma','1','0878202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0878202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหายาอื่นๆ มิได้ระบุ หลายตัว ในปัสสาวะ',lab_ncd_note = 'Drug screen NOS, multiple drugs, urine' where lab_icd10tm = '0878203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001374','','การตรวจคัดกรองหายาอื่นๆ มิได้ระบุ หลายตัว ในปัสสาวะ','Drug screen NOS, multiple drugs, urine','1','0878203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0878203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหายาและสารเสพติดอื่นๆ มิได้ระบุ หลายตัว ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Drug screen NOS, multiple drugs, other fluid NEC' where lab_icd10tm = '0878215';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001375','','การตรวจคัดกรองหายาและสารเสพติดอื่นๆ มิได้ระบุ หลายตัว ในของเหลวอื่นๆ มิได้จำแนก','Drug screen NOS, multiple drugs, other fluid NEC','1','0878215'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0878215');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา acetaldehyde ในเลือด',lab_ncd_note = 'Acetaldehyde, whole blood ' where lab_icd10tm = '0880201';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001376','','การตรวจหา acetaldehyde ในเลือด','Acetaldehyde, whole blood ','1','0880201'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0880201');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา acetaldehyde ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Acetaldehyde, serum/plasma' where lab_icd10tm = '0880202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001377','','การตรวจหา acetaldehyde ในซีรั่ม / พลาสม่า','Acetaldehyde, serum/plasma','1','0880202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0880202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา acetone ในเลือด ',lab_ncd_note = 'Acetone, whole blood ' where lab_icd10tm = '0880401';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001378','','การตรวจหา acetone ในเลือด ','Acetone, whole blood ','1','0880401'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0880401');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา acetone ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Acetone, serum/plasma' where lab_icd10tm = '0880402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001379','','การตรวจหา acetone ในซีรั่ม / พลาสม่า','Acetone, serum/plasma','1','0880402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0880402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา acetone ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Acetone, other fluid NEC' where lab_icd10tm = '0880415';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001380','','การตรวจหา acetone ในของเหลวอื่นๆ มิได้จำแนก','Acetone, other fluid NEC','1','0880415'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0880415');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา alcohol panel อื่นๆ มิได้ระบุ ในเลือด',lab_ncd_note = 'Alcohol panel NOS, whole blood' where lab_icd10tm = '0880601';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001381','','การตรวจหา alcohol panel อื่นๆ มิได้ระบุ ในเลือด','Alcohol panel NOS, whole blood','1','0880601'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0880601');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา alcohol panel อื่นๆ มิได้ระบุในซีรั่ม / พลาสม่า',lab_ncd_note = 'Alcohol panel NOS, serum/plasma' where lab_icd10tm = '0880602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001382','','การตรวจหา alcohol panel อื่นๆ มิได้ระบุในซีรั่ม / พลาสม่า','Alcohol panel NOS, serum/plasma','1','0880602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0880602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา alcohol panel อื่นๆ มิได้ระบุ ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Alcohol panel NOS, other fluid NEC' where lab_icd10tm = '0880615';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001383','','การตรวจหา alcohol panel อื่นๆ มิได้ระบุ ในของเหลวอื่นๆ มิได้จำแนก','Alcohol panel NOS, other fluid NEC','1','0880615'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0880615');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาคุณภาพสารเบนซีนในซีรั่ม / พลาสม่า',lab_ncd_note = 'Benzene, qualitative, serum/plasma' where lab_icd10tm = '0880802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001384','','การตรวจหาคุณภาพสารเบนซีนในซีรั่ม / พลาสม่า','Benzene, qualitative, serum/plasma','1','0880802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0880802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสารเบนซีนในซีรั่ม / พลาสม่า',lab_ncd_note = 'Benzene, serum/plasma' where lab_icd10tm = '0881002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001385','','การตรวจหาสารเบนซีนในซีรั่ม / พลาสม่า','Benzene, serum/plasma','1','0881002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0881002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา carbon tetrachloride ในเลือด ',lab_ncd_note = 'Carbon tetrachloride, whole blood' where lab_icd10tm = '0881201';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001386','','การตรวจหา carbon tetrachloride ในเลือด ','Carbon tetrachloride, whole blood','1','0881201'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0881201');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา carbon tetrachloride ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Carbon tetrachloride, serum/plasma' where lab_icd10tm = '0881202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001387','','การตรวจหา carbon tetrachloride ในซีรั่ม / พลาสม่า','Carbon tetrachloride, serum/plasma','1','0881202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0881202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา carbon tetrachloride ในปัสสาวะ ',lab_ncd_note = 'Carbon tetrachloride, urine' where lab_icd10tm = '0881203';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001388','','การตรวจหา carbon tetrachloride ในปัสสาวะ ','Carbon tetrachloride, urine','1','0881203'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0881203');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา carbon tetrachloride ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Carbon tetrachloride, other fluid NEC' where lab_icd10tm = '0881215';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001389','','การตรวจหา carbon tetrachloride ในของเหลวอื่นๆ มิได้จำแนก','Carbon tetrachloride, other fluid NEC','1','0881215'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0881215');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา chloral hydrate ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Chloral hydrate, serum/plasma' where lab_icd10tm = '0881402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001390','','การตรวจหา chloral hydrate ในซีรั่ม / พลาสม่า','Chloral hydrate, serum/plasma','1','0881402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0881402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา chloral hydrate ในปัสสาวะ',lab_ncd_note = 'Chloral hydrate, urine' where lab_icd10tm = '0881403';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001391','','การตรวจหา chloral hydrate ในปัสสาวะ','Chloral hydrate, urine','1','0881403'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0881403');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา chloral hydrate ในของเหลวอื่น ๆ มิได้จำแนก',lab_ncd_note = 'Chloral hydrate, other fluid NEC' where lab_icd10tm = '0881415';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001392','','การตรวจหา chloral hydrate ในของเหลวอื่น ๆ มิได้จำแนก','Chloral hydrate, other fluid NEC','1','0881415'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0881415');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาเอทานอลในเลือด',lab_ncd_note = 'Ethanol, whole blood' where lab_icd10tm = '0881601';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001393','','การตรวจหาเอทานอลในเลือด','Ethanol, whole blood','1','0881601'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0881601');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาเอทานอลในซีรั่ม / พลาสม่า',lab_ncd_note = 'Ethanol, serum/plasma' where lab_icd10tm = '0881602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001394','','การตรวจหาเอทานอลในซีรั่ม / พลาสม่า','Ethanol, serum/plasma','1','0881602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0881602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาเอทานอลในปัสสาวะ',lab_ncd_note = 'Ethanol, urine' where lab_icd10tm = '0881603';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001395','','การตรวจหาเอทานอลในปัสสาวะ','Ethanol, urine','1','0881603'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0881603');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาเอทานอลในของเหลวอื่น ๆ มิได้จำแนก',lab_ncd_note = 'Ethanol, other fluid NEC' where lab_icd10tm = '0881615';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001396','','การตรวจหาเอทานอลในของเหลวอื่น ๆ มิได้จำแนก','Ethanol, other fluid NEC','1','0881615'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0881615');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาเอทิลีนไกลคอลในเลือด ',lab_ncd_note = 'Ethylene glycol, whole blood' where lab_icd10tm = '0881801';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001397','','การตรวจหาเอทิลีนไกลคอลในเลือด ','Ethylene glycol, whole blood','1','0881801'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0881801');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาเอทิลีนไกลคอลในซีรั่ม / พลาสม่า',lab_ncd_note = 'Ethylene glycol, serum/plasma' where lab_icd10tm = '0881802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001398','','การตรวจหาเอทิลีนไกลคอลในซีรั่ม / พลาสม่า','Ethylene glycol, serum/plasma','1','0881802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0881802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาเอทิลีนไกลคอลในของเหลวอื่น ๆ มิได้จำแนก',lab_ncd_note = 'Ethylene glycol, other fluid NEC' where lab_icd10tm = '0881815';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001399','','การตรวจหาเอทิลีนไกลคอลในของเหลวอื่น ๆ มิได้จำแนก','Ethylene glycol, other fluid NEC','1','0881815'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0881815');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา formaldehyde ในเลือด',lab_ncd_note = 'Formaldehyde, whole blood' where lab_icd10tm = '0882001';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001400','','การตรวจหา formaldehyde ในเลือด','Formaldehyde, whole blood','1','0882001'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0882001');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา formaldehyde ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Formaldehyde, serum/plasma' where lab_icd10tm = '0882002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001401','','การตรวจหา formaldehyde ในซีรั่ม / พลาสม่า','Formaldehyde, serum/plasma','1','0882002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0882002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา formaldehyde ในปัสสาวะ',lab_ncd_note = 'Formaldehyde, urine' where lab_icd10tm = '0882003';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001402','','การตรวจหา formaldehyde ในปัสสาวะ','Formaldehyde, urine','1','0882003'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0882003');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหา formaldehyde ในของเหลวอื่นๆ มิได้จำแนก',lab_ncd_note = 'Formaldehyde, other fluid NEC' where lab_icd10tm = '0882015';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001403','','การตรวจหา formaldehyde ในของเหลวอื่นๆ มิได้จำแนก','Formaldehyde, other fluid NEC','1','0882015'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0882015');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาไอโซโพรพานอลในเลือด',lab_ncd_note = 'Isopropanol, whole blood' where lab_icd10tm = '0882201';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001404','','การตรวจหาไอโซโพรพานอลในเลือด','Isopropanol, whole blood','1','0882201'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0882201');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาไอโซโพรพานอลในซีรั่ม / พลาสม่า',lab_ncd_note = 'Isopropanol, serum/plasma' where lab_icd10tm = '0882202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001405','','การตรวจหาไอโซโพรพานอลในซีรั่ม / พลาสม่า','Isopropanol, serum/plasma','1','0882202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0882202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาไอโซโพรพานอลในของเหลว อื่น ๆ มิได้จำแนก',lab_ncd_note = 'Isopropanol, other fluid NEC' where lab_icd10tm = '0882215';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001406','','การตรวจหาไอโซโพรพานอลในของเหลว อื่น ๆ มิได้จำแนก','Isopropanol, other fluid NEC','1','0882215'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0882215');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาคุณภาพเมทานอลในซีรั่ม / พลาสม่า ',lab_ncd_note = 'Methanol, qualitative, serum/plasma' where lab_icd10tm = '0882402';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001407','','การตรวจหาคุณภาพเมทานอลในซีรั่ม / พลาสม่า ','Methanol, qualitative, serum/plasma','1','0882402'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0882402');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาคุณภาพเมทานอลในปัสสาวะ',lab_ncd_note = 'Methanol, qualitative, urine' where lab_icd10tm = '0882403';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001408','','การตรวจหาคุณภาพเมทานอลในปัสสาวะ','Methanol, qualitative, urine','1','0882403'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0882403');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาเมธานอลในซีรั่ม / พลาสม่า',lab_ncd_note = 'Methanol, serum/plasma' where lab_icd10tm = '0882602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001409','','การตรวจหาเมธานอลในซีรั่ม / พลาสม่า','Methanol, serum/plasma','1','0882602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0882602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาเมทานอลในปัสสาวะ',lab_ncd_note = 'Methanol, urine' where lab_icd10tm = '0882603';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001410','','การตรวจหาเมทานอลในปัสสาวะ','Methanol, urine','1','0882603'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0882603');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาโทลูอีนในซีรั่ม / พลาสม่า',lab_ncd_note = 'Toluene, serum/plasma' where lab_icd10tm = '0882802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001411','','การตรวจหาโทลูอีนในซีรั่ม / พลาสม่า','Toluene, serum/plasma','1','0882802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0882802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาไซลีนในซีรั่ม / พลาสม่า',lab_ncd_note = 'Xylene, serum/plasma' where lab_icd10tm = '0883002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001412','','การตรวจหาไซลีนในซีรั่ม / พลาสม่า','Xylene, serum/plasma','1','0883002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0883002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การวิเคราะห์สารระเหยอื่นๆ มิได้จำแนก จากสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Volatile assay NEC, any specimen type' where lab_icd10tm = '0889099';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001413','','การวิเคราะห์สารระเหยอื่นๆ มิได้จำแนก จากสิ่งส่งตรวจชนิดต่างๆ','Volatile assay NEC, any specimen type','1','0889099'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0889099');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหาคาร์บอนมอนอกไซด์ในเม็ดเลือดแดง',lab_ncd_note = 'Carbon monoxide screen, RBC' where lab_icd10tm = '0890217';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001414','','การตรวจคัดกรองหาคาร์บอนมอนอกไซด์ในเม็ดเลือดแดง','Carbon monoxide screen, RBC','1','0890217'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0890217');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาคาร์บอนมอนอกไซด์ในเม็ดเลือดแดง',lab_ncd_note = 'Carbon monoxide, RBC' where lab_icd10tm = '0890417';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001415','','การตรวจหาคาร์บอนมอนอกไซด์ในเม็ดเลือดแดง','Carbon monoxide, RBC','1','0890417'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0890417');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสารไซยาไนด์ในเลือด',lab_ncd_note = 'Cyanide, whole blood' where lab_icd10tm = '0890601';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001416','','การตรวจหาสารไซยาไนด์ในเลือด','Cyanide, whole blood','1','0890601'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0890601');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสารไซยาไนด์ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Cyanide, serum/plasma' where lab_icd10tm = '0890602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001417','','การตรวจหาสารไซยาไนด์ในซีรั่ม / พลาสม่า','Cyanide, serum/plasma','1','0890602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0890602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสารไซยาไนด์ในปัสสาวะ',lab_ncd_note = 'Cyanide, urine' where lab_icd10tm = '0890603';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001418','','การตรวจหาสารไซยาไนด์ในปัสสาวะ','Cyanide, urine','1','0890603'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0890603');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสารไซยาไนด์ในเนื้อเยื่ออื่นๆ มิได้จำแนก',lab_ncd_note = 'Cyanide, tissue NEC' where lab_icd10tm = '0890650';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001419','','การตรวจหาสารไซยาไนด์ในเนื้อเยื่ออื่นๆ มิได้จำแนก','Cyanide, tissue NEC','1','0890650'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0890650');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสาร formate ในเลือด',lab_ncd_note = 'Formate, whole blood' where lab_icd10tm = '0890801';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001420','','การตรวจหาสาร formate ในเลือด','Formate, whole blood','1','0890801'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0890801');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสาร formate ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Formate, serum/plasma' where lab_icd10tm = '0890802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001421','','การตรวจหาสาร formate ในซีรั่ม / พลาสม่า','Formate, serum/plasma','1','0890802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0890802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสาร paraquat ในเลือด ',lab_ncd_note = 'Paraquat, whole blood' where lab_icd10tm = '0891001';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001422','','การตรวจหาสาร paraquat ในเลือด ','Paraquat, whole blood','1','0891001'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0891001');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสาร paraquat ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Paraquat, serum/plasma' where lab_icd10tm = '0891002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001423','','การตรวจหาสาร paraquat ในซีรั่ม / พลาสม่า','Paraquat, serum/plasma','1','0891002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0891002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสารกำจัดศัตรูพืชในเลือด ',lab_ncd_note = 'Pesticides, whole blood' where lab_icd10tm = '0891201';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001424','','การตรวจหาสารกำจัดศัตรูพืชในเลือด ','Pesticides, whole blood','1','0891201'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0891201');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสารกำจัดศัตรูพืชในซีรั่ม / พลาสม่า',lab_ncd_note = 'Pesticides, serum/plasma' where lab_icd10tm = '0891202';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001425','','การตรวจหาสารกำจัดศัตรูพืชในซีรั่ม / พลาสม่า','Pesticides, serum/plasma','1','0891202'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0891202');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสารฟีนอลในเลือด',lab_ncd_note = 'Phenol, whole blood' where lab_icd10tm = '0891401';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001426','','การตรวจหาสารฟีนอลในเลือด','Phenol, whole blood','1','0891401'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0891401');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสารฟีนอลในปัสสาวะ',lab_ncd_note = 'Phenol, urine' where lab_icd10tm = '0891403';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001427','','การตรวจหาสารฟีนอลในปัสสาวะ','Phenol, urine','1','0891403'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0891403');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสาร polychlorinated biphenol ซีรั่ม / พลาสม่า',lab_ncd_note = 'Polychlorinated biphenol, serum/plasma' where lab_icd10tm = '0891602';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001428','','การตรวจหาสาร polychlorinated biphenol ซีรั่ม / พลาสม่า','Polychlorinated biphenol, serum/plasma','1','0891602'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0891602');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสาร polychlorinated biphenol ในปัสสาวะ ',lab_ncd_note = 'Polychlorinated biphenol, urine' where lab_icd10tm = '0891603';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001429','','การตรวจหาสาร polychlorinated biphenol ในปัสสาวะ ','Polychlorinated biphenol, urine','1','0891603'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0891603');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหาสาร strychnine ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Strychnine screen, serum/plasma' where lab_icd10tm = '0891802';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001430','','การตรวจคัดกรองหาสาร strychnine ในซีรั่ม / พลาสม่า','Strychnine screen, serum/plasma','1','0891802'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0891802');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจคัดกรองหาสาร strychnine ในปัสสาวะ',lab_ncd_note = 'Strychnine screen, urine' where lab_icd10tm = '0891803';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001431','','การตรวจคัดกรองหาสาร strychnine ในปัสสาวะ','Strychnine screen, urine','1','0891803'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0891803');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสาร strychnine ในซีรั่ม / พลาสม่า',lab_ncd_note = 'Strychnine, serum/plasma' where lab_icd10tm = '0892002';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001432','','การตรวจหาสาร strychnine ในซีรั่ม / พลาสม่า','Strychnine, serum/plasma','1','0892002'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0892002');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจหาสาร strychnine ในปัสสาวะ',lab_ncd_note = 'Strychnine, urine' where lab_icd10tm = '0892003';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001433','','การตรวจหาสาร strychnine ในปัสสาวะ','Strychnine, urine','1','0892003'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0892003');
 COMMIT;
BEGIN; 
 UPDATE b_item_lab_ncd_std SET item_lab_ncd_std_description = 'การตรวจทางพิษวิทยา มิได้จำแนกจากสิ่งส่งตรวจชนิดต่างๆ',lab_ncd_note = 'Toxicology - substance NEC, any specimen type' where lab_icd10tm = '0899099';
 INSERT INTO b_item_lab_ncd_std SELECT 'ncd201900000001434','','การตรวจทางพิษวิทยา มิได้จำแนกจากสิ่งส่งตรวจชนิดต่างๆ','Toxicology - substance NEC, any specimen type','1','0899099'
 WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std where lab_icd10tm = '0899099');
 COMMIT;


-- update db version
INSERT INTO s_version VALUES ('9701000000088', '88', 'Hospital OS, Community Edition', '3.9.55', '3.35.0', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_55.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.55');
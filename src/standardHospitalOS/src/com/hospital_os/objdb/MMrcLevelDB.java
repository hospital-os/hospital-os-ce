/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.MMrcLevel;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sompr
 */
public class MMrcLevelDB {

    public ConnectionInf theConnectionInf;

    public MMrcLevelDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public List<MMrcLevel> selectAll() throws Exception {
        String sql = "select * from f_mmrc_level";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            return executeQuery(ePQuery);
        }
    }

    public MMrcLevel selectByPK(String pkId) throws Exception {
        String sql = "select * from f_mmrc_level \n"
                + " where f_mmrc_level_id = ?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, pkId);
            List<MMrcLevel> v = executeQuery(ePQuery);
            return (MMrcLevel) (v.isEmpty() ? null : v.get(0));
        }
    }

    public List<MMrcLevel> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<MMrcLevel> list = new ArrayList<>();
        try (ResultSet rs = preparedStatement.executeQuery()) {
            while (rs.next()) {
                MMrcLevel p = new MMrcLevel();
                p.setObjectId(rs.getString("f_mmrc_level_id"));
                p.description = rs.getString("description");
                list.add(p);
            }
            return list;
        }
    }
}

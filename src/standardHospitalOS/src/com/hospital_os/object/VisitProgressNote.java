/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;
import java.util.Date;

/**
 *
 * @author tanakrit
 */
public class VisitProgressNote extends Persistent {

    public String t_visit_progress_note_id = "";
    public String t_visit_id = "";
    public String subjective = "";
    public String assessment = "";
    public String objective = "";
    public String plan = "";
    public String active = "";
    public Date record_date_time;
    public String user_record_id = "";
    public Date update_date_time;
    public String user_update_id = "";
    public Date cancel_date_time;
    public String user_cancel_id = "";

}

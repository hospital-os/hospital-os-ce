/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.object;

/**
 *
 * @author Somprasong
 */
public class ServiceResponse {

    public final static int SUCCESS = 1;
    public final static int FAIL = 2;
    private int responseCode = FAIL;
    private String responseMessage = "";
    private Object responseData;

    public ServiceResponse() {
    }

    public ServiceResponse(int responseCode, String responseMessage) {
        this.responseCode = responseCode;
        this.responseMessage = responseMessage;
    }

    public ServiceResponse(int responseCode, String responseMessage, Object responseData) {
        this.responseCode = responseCode;
        this.responseMessage = responseMessage;
        this.responseData = responseData;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public Object getResponseData() {
        return responseData;
    }

    public void setResponseData(Object responseData) {
        this.responseData = responseData;
    }
}

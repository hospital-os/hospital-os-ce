/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.utility;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Somprasong
 */
public class HosStdTableModel extends AbstractTableModel {

    private final String[] columns;
    private List<HosStdDatasource> datas = new ArrayList<HosStdDatasource>();

    public HosStdTableModel(String[] columns) {
        this.columns = columns;
    }

    public HosStdDatasource getRow(int row) {
        return datas.get(row);
    }

    @Override
    public boolean isCellEditable(int row, int col) {
////            if (col == 0) {
////                return true;
////            } else {
        return false;
//            }
    }

    @Override
    public int getRowCount() {
        return datas.size();
    }

    @Override
    public int getColumnCount() {
        return columns.length;
    }

    @Override
    public String getColumnName(int col) {
        return columns[col];
    }

    @Override
    public Object getValueAt(int row, int col) {
        HosStdDatasource objs = datas.get(row);
        for (int i = 0; i < objs.size(); i++) {
            Object object = objs.get(i);
            if (i == col) {
                return object;
            }
        }
        return "";
    }

    @SuppressWarnings("UseOfObsoleteCollectionType")
    public List<HosStdDatasource> getData() {
        return datas;
    }

    public void setDatas(List<HosStdDatasource> datas) {
        this.datas.clear();
        this.datas.addAll(datas);
    }

    public void addData(HosStdDatasource objects) {
        datas.add(objects);
    }

    @Override
    public void setValueAt(Object value, int row, int col) {
        HosStdDatasource objs = datas.get(row);
        for (int i = 0; i < objs.size(); i++) {
            if (i == col) {
                objs.set(i, value);
                break;
            }
        }
        fireTableCellUpdated(row, col);

    }

    public void clearTable() {
        getData().clear();
        fireTableDataChanged();
    }
}

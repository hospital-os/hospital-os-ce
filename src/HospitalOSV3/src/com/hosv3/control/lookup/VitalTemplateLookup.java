/*
 * VitalTemplateLookup.java
 *
 * Created on 20 �ѹ��¹ 2548, 9:48 �.
 */
package com.hosv3.control.lookup;

//import com.henbe.connection.*;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.LookupControlInf;
import com.hosv3.control.LookupControl;

/**
 * @not deprecated because use henbe package bad readData
 *
 * @author kingland
 */
public class VitalTemplateLookup implements LookupControlInf {

    private LookupControl theLookup;

    /**
     * Creates a new instance of VitalTemplateLookup
     */
    public VitalTemplateLookup(LookupControl lookup) {
        theLookup = lookup;
    }

    @Override
    @SuppressWarnings("UseOfObsoleteCollectionType")
    public java.util.Vector listData(String str) {
        if (str == null || str.equals("") || str.equals("%")) {
            return null;
        } else {
            return theLookup.listVitalTemplate(str, "0");
        }
    }

    @Override
    public CommonInf readHosData(String str) {
        return null;
    }
}

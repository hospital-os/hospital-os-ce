/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class ItemSupply extends Persistent {

    private static final long serialVersionUID = 1L;
    public String b_item_id;
    public String item_supply_printable = "0";
    public String print_mar_type = "0";
    public String supplement_label = "";
    public String user_record_id;
    public String record_date_time;
    public String user_update_id;
    public String update_date_time;
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.utility;

import java.util.Comparator;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class ComplexDataSource implements Comparable<ComplexDataSource>, Comparator<ComplexDataSource> {

    private Object id;
    private Object[] values;
    private int index;

    public ComplexDataSource() {
    }

    public ComplexDataSource(Object id, Object[] values) {
        this(id, values, 0);
    }

    public ComplexDataSource(Object id, Object[] values, int index) {
        this.id = id;
        this.values = values;
        this.index = index;
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public Object[] getValues() {
        return values;
    }

    public void setValues(Object[] values) {
        this.values = values;
    }

    public Object getValue(int index) {
        return values[index];
    }

    public void setValue(int index, Object value) {
        this.values[index] = value;
    }

    @Override
    public String toString() {
        return (String) this.values[index];
    }

    @Override
    public int compareTo(ComplexDataSource o) {
        Object checkId;
        if (o.getId() instanceof Object[]) {
            Object[] objects = (Object[]) o.getId();
            checkId = objects[0];
        } else {
            checkId = o.getId();
        }
        Object thisId;
        if (this.getId() instanceof Object[]) {
            Object[] objects = (Object[]) this.getId();
            thisId = objects[0];
        } else {
            thisId = this.getId();
        }
        //ascending order
        return String.valueOf(thisId).compareTo(String.valueOf(checkId));
        //descending order
        //return checkId.compareTo(thisId);
    }

    @Override
    public int compare(ComplexDataSource o1, ComplexDataSource o2) {
        return String.valueOf(o1.getId()).compareTo(String.valueOf(o2.getId()));
    }
}

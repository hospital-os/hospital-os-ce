/*
 * PanelOrder.java
 *
 * Created on 29 �ѹ��¹ 2546, 9:32 �.
 */
package com.hosv3.gui.panel.detail;

import com.hospital_os.object.Active;
import com.hospital_os.object.CategoryGroup;
import com.hospital_os.object.CategoryGroupItem;
import com.hospital_os.object.Drug;
import com.hospital_os.object.DrugDoseShortcut;
import com.hospital_os.object.DrugFrequency;
import com.hospital_os.object.Employee;
import com.hospital_os.object.Item;
import com.hospital_os.object.ItemPrice;
import com.hospital_os.object.ItemSupply;
import com.hospital_os.object.LabAtkProduct;
import com.hospital_os.object.OrderItem;
import com.hospital_os.object.OrderItemDrug;
import com.hospital_os.object.OrderLabSecret;
import com.hospital_os.object.OrderStatus;
import com.hospital_os.object.VisitType;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hospital_os.utility.ComboFix;
import com.hospital_os.utility.ComboboxModel;
import com.hospital_os.utility.Gutil;
import com.hosv3.control.HosControl;
import com.hosv3.control.lookup.DoseUomLookup;
import com.hosv3.control.lookup.DrugDoseShortcutLookup;
import com.hosv3.control.lookup.DrugFrequencyLookup;
import com.hosv3.control.lookup.DrugInstructionLookup;
import com.hosv3.gui.panel.transaction.inf.InfPanelOrder;
import com.hosv3.object.DrugFrequency2;
import com.hosv3.object.DrugInstruction2;
import com.hosv3.object.HosObject;
import com.hosv3.object.Uom2;
import com.hosv3.utility.Constant;
import com.hosv3.utility.GuiLang;
import com.hosv3.utility.InfSelectList;
import java.awt.CardLayout;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.JTextComponent;
import sd.comp.textfield.DoubleDocument;

/**
 *
 * @author Surachai Thowong
 * @modify amp
 * @modify pu
 *
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class PDOrder extends javax.swing.JPanel implements InfPanelOrder,
        InfSelectList {

    protected HosObject theHO;
    protected HosControl theHC;
    protected UpdateStatus theUS;
    public final static String CARD_BLANK = "CARD_BLANK";
    public final static String CARD_DRUG = "CARD_DRUG";
    public final static String CARD_DRUGS = "CARD_DRUGS";
    static final long serialVersionUID = 0;
    public final static String CARD_SERVICE = "CARD_SERVICE";
    public final static String CARD_SERVICE_DF = "CARD_SERVICE_DF";
    public final static String CARD_SERVICE_OPERATOR = "CARD_SERVICE_OPERATOR";
    public final static String CARD_SUPPLY = "CARD_SUPPLY";
    public final static String CARD_LAB = "CARD_LAB";
    public boolean show_cancel_order = true;
    /**
     * ��㹡�õ�Ǩ�ͺ��� Double-Click
     */
    /*
     * �Ըա������
     */
    protected OrderItem theOrderItem;
    protected OrderItemDrug theOrderItemDrug;
    protected DrugDoseShortcut theDrugDoseShortcut;
    protected OrderLabSecret theOrderLabSecret;
    protected ComboFix theComboFix = null;
    protected ComboFix theEmployeeComboFix = null;
    /**
     * Creates new form PanelOrder
     */
    protected CardLayout layoutOrder;
    protected CardLayout layoutDrug;
    protected Item theItem;
    protected OrderItem[] orderItems;

    private final DocumentListener orderDayDL = new DocumentListener() {
        @Override
        public void insertUpdate(DocumentEvent e) {
            calculateAmountByDate(theOrderItem, theOrderItemDrug, true);
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            calculateAmountByDate(theOrderItem, theOrderItemDrug, true);
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            calculateAmountByDate(theOrderItem, theOrderItemDrug, true);
        }
    };

    public PDOrder() {
        initComponents();
        setLanguage(null);
        layoutDrug = (CardLayout) jPanelSPU.getLayout();
        layoutOrder = (CardLayout) panelOrder.getLayout();
        setEnableCardBlank(false);
    }

    public void setControl(HosControl hc, UpdateStatus us) {
        theHC = hc;
        theHO = hc.theHO;
        theUS = us;
        jComboBoxInstruction.setControl(null, new DrugInstructionLookup(hc.theLookupControl), new DrugInstruction2());
        jComboBoxFrequency.setControl(null, new DrugFrequencyLookup(hc.theLookupControl), new DrugFrequency2());
        jComboBoxUse.setControl(null, new DoseUomLookup(hc.theLookupControl), new Uom2());
        jComboBoxDrugDoseShortcut.setControl(null, new DrugDoseShortcutLookup(hc.theLookupControl), new DrugDoseShortcut());
        jComboBoxDrugDoseShortcut.refresh();

        List vc = theHC.theLookupControl.listLabOrderCause();
        ComboboxModel.initComboBox(jcbLabOrderCause, vc);
        ComboFix na = new ComboFix();
        na.code = null;
        na.name = "����к�";
        jcbLabOrderCause.insertItemAt((Object) na, 0);
        jcbLabOrderCause.setSelectedIndex(0);

        List<CommonInf> v = theHC.theLookupControl.listLabAtkProductByKeyword("");
        ComboboxModel.initComboBox(cbLabAtkProduct, v);
        LabAtkProduct undefine = new LabAtkProduct();
        undefine.setObjectId(null);
        undefine.lab_atk_device_name = "����к�";
        cbLabAtkProduct.insertItemAt((Object) undefine, 0);
        cbLabAtkProduct.setSelectedIndex(0);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jScrollPane6 = new javax.swing.JScrollPane();
        jPanel12 = new javax.swing.JPanel();
        panelOrder = new javax.swing.JPanel();
        jPanelDrug = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jCheckBoxSpecialUsage = new javax.swing.JCheckBox();
        jPanel2 = new javax.swing.JPanel();
        jLabel25 = new javax.swing.JLabel();
        lblPregnancyCategory = new javax.swing.JLabel();
        jCheckBoxDrugStat = new javax.swing.JCheckBox();
        jPanelSPU = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jLabelDoseShortcut = new javax.swing.JLabel();
        jComboBoxDrugDoseShortcut = new com.hosv3.gui.component.HosComboBox();
        btnRefreshDoseShortcut = new javax.swing.JButton();
        jComboBoxInstruction = new com.hosv3.gui.component.HosComboBox();
        jComboBoxFrequency = new com.hosv3.gui.component.HosComboBox();
        jComboBoxUse = new com.hosv3.gui.component.HosComboBox();
        jTextFieldUseUnit = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jScrollPane11 = new javax.swing.JScrollPane();
        jTextAreaTextUsage = new javax.swing.JTextArea();
        jPanel5 = new javax.swing.JPanel();
        jLabelDrugInstruction = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextFieldCaution = new javax.swing.JTextArea();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTextFieldDescription = new javax.swing.JTextArea();
        jPanel9 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jButtonReturnDrug = new javax.swing.JButton();
        jTextFieldQtyReturn = new com.hospital_os.utility.DoubleTextField();
        jLabelReturnDrug = new javax.swing.JLabel();
        jButtonSaveOrderDrug = new javax.swing.JButton();
        txtQtyUnit = new javax.swing.JTextField();
        jLabelQtyUnit = new javax.swing.JLabel();
        panelPrintStickerQty = new javax.swing.JPanel();
        txtStickerQty = new sd.comp.jcalendar.JSpinField();
        lblSticker = new javax.swing.JLabel();
        lblStickerUnit = new javax.swing.JLabel();
        DayForUse2 = new javax.swing.JLabel();
        jTextFieldTotalDay = new com.hospital_os.utility.DoubleTextField();
        jLabelQtyUnit1 = new javax.swing.JLabel();
        jPanelLab = new javax.swing.JPanel();
        jButtonSaveLab = new javax.swing.JButton();
        jcbLabOrderCause = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        panelCodeLabSecret = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTextFieldCodeLabSecret = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        cbLabUrgent = new javax.swing.JCheckBox();
        jLabelUrgent2 = new javax.swing.JLabel();
        jLabelNote5 = new javax.swing.JLabel();
        jTextFieldCautionLab = new javax.swing.JTextField();
        jPanel10 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        cbLabAtkProduct = new javax.swing.JComboBox();
        jPanelSupply = new javax.swing.JPanel();
        jLabelQty = new javax.swing.JLabel();
        jTextFieldQty = new com.hospital_os.utility.DoubleTextField();
        jLabelQty1 = new javax.swing.JLabel();
        jButtonSaveOrderSupply = new javax.swing.JButton();
        jTextFieldCautionSupply = new javax.swing.JTextField();
        jLabelNote1 = new javax.swing.JLabel();
        jLabelNote4 = new javax.swing.JLabel();
        txtSupplySN = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtSupplement = new javax.swing.JTextArea();
        jPanelBlank = new javax.swing.JPanel();
        jLabelCause = new javax.swing.JLabel();
        jLabelEmployee = new javax.swing.JLabel();
        jLabelEmployee1 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jLabelCause1 = new javax.swing.JTextArea();
        jLabelNote3 = new javax.swing.JLabel();
        jTextFieldCautionBlank = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        cbBlankUrgent = new javax.swing.JCheckBox();
        jLabelUrgent = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jPanelService = new javax.swing.JPanel();
        jLabelServicePrice = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jTextFieldServicePrice = new com.hospital_os.utility.DoubleTextField();
        jTextFieldServiceQty = new com.hospital_os.utility.DoubleTextField();
        jLabel61 = new javax.swing.JLabel();
        jTextFieldCautionService = new javax.swing.JTextField();
        jLabelNote2 = new javax.swing.JLabel();
        jButtonSaveOrderService = new javax.swing.JButton();
        jPanelDrugs = new javax.swing.JPanel();
        jPanel11 = new javax.swing.JPanel();
        DayForUse1 = new javax.swing.JLabel();
        txtUseDrugTotalDays = new com.hospital_os.utility.DoubleTextField();
        jLabel9 = new javax.swing.JLabel();
        btnSaveOrderDrugs = new javax.swing.JButton();
        panelEditPrice = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        doubleTextFieldCustomPrice = new com.hospital_os.utility.DoubleTextField();
        jLabel48 = new javax.swing.JLabel();
        btnUpdatePrice = new javax.swing.JButton();
        jPanel13 = new javax.swing.JPanel();

        addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                formFocusGained(evt);
            }
        });
        setLayout(new java.awt.BorderLayout());

        jPanel12.setLayout(new java.awt.GridBagLayout());

        panelOrder.setLayout(new java.awt.CardLayout());

        jPanelDrug.setLayout(new java.awt.GridBagLayout());

        jPanel1.setLayout(new java.awt.GridBagLayout());

        jCheckBoxSpecialUsage.setFont(jCheckBoxSpecialUsage.getFont());
        jCheckBoxSpecialUsage.setText("�Ը���Ẻ�����");
        jCheckBoxSpecialUsage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxSpecialUsageActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(jCheckBoxSpecialUsage, gridBagConstraints);

        jPanel2.setLayout(new java.awt.GridBagLayout());

        jLabel25.setFont(jLabel25.getFont());
        jLabel25.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel25.setText("Pregnancy Category:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(jLabel25, gridBagConstraints);

        lblPregnancyCategory.setFont(lblPregnancyCategory.getFont().deriveFont(lblPregnancyCategory.getFont().getStyle() | java.awt.Font.BOLD));
        lblPregnancyCategory.setForeground(new java.awt.Color(0, 0, 255));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(lblPregnancyCategory, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 0, 0);
        jPanel1.add(jPanel2, gridBagConstraints);

        jCheckBoxDrugStat.setFont(jCheckBoxDrugStat.getFont());
        jCheckBoxDrugStat.setText("STAT");
        jCheckBoxDrugStat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxDrugStatActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 10, 2, 2);
        jPanel1.add(jCheckBoxDrugStat, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelDrug.add(jPanel1, gridBagConstraints);

        jPanelSPU.setLayout(new java.awt.CardLayout());

        jPanel7.setLayout(new java.awt.GridBagLayout());

        jPanel8.setLayout(new java.awt.GridBagLayout());

        jLabelDoseShortcut.setFont(jLabelDoseShortcut.getFont());
        jLabelDoseShortcut.setText("��Ǫ��� ���Ẻ Dose ��� ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel8.add(jLabelDoseShortcut, gridBagConstraints);

        jComboBoxDrugDoseShortcut.setEnableNA(true);
        jComboBoxDrugDoseShortcut.setFont(jComboBoxDrugDoseShortcut.getFont());
        jComboBoxDrugDoseShortcut.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jComboBoxDrugDoseShortcutMouseReleased(evt);
            }
        });
        jComboBoxDrugDoseShortcut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxDrugDoseShortcutActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel8.add(jComboBoxDrugDoseShortcut, gridBagConstraints);

        btnRefreshDoseShortcut.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/refresh_24.png"))); // NOI18N
        btnRefreshDoseShortcut.setMaximumSize(new java.awt.Dimension(26, 26));
        btnRefreshDoseShortcut.setMinimumSize(new java.awt.Dimension(26, 26));
        btnRefreshDoseShortcut.setPreferredSize(new java.awt.Dimension(26, 26));
        btnRefreshDoseShortcut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshDoseShortcutActionPerformed(evt);
            }
        });
        jPanel8.add(btnRefreshDoseShortcut, new java.awt.GridBagConstraints());

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.weightx = 1.0;
        jPanel7.add(jPanel8, gridBagConstraints);

        jComboBoxInstruction.setFont(jComboBoxInstruction.getFont());
        jComboBoxInstruction.setMaximumSize(new java.awt.Dimension(400, 26));
        jComboBoxInstruction.setMinimumSize(new java.awt.Dimension(360, 26));
        jComboBoxInstruction.setPreferredSize(new java.awt.Dimension(400, 26));
        jComboBoxInstruction.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jComboBoxInstructionFocusGained(evt);
            }
        });
        jComboBoxInstruction.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jComboBoxInstructionMouseReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.weightx = 0.3;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel7.add(jComboBoxInstruction, gridBagConstraints);

        jComboBoxFrequency.setFont(jComboBoxFrequency.getFont());
        jComboBoxFrequency.setMaximumSize(new java.awt.Dimension(400, 26));
        jComboBoxFrequency.setMinimumSize(new java.awt.Dimension(360, 26));
        jComboBoxFrequency.setPreferredSize(new java.awt.Dimension(400, 26));
        jComboBoxFrequency.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jComboBoxFrequencyFocusGained(evt);
            }
        });
        jComboBoxFrequency.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jComboBoxFrequencyMouseReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.weightx = 0.3;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel7.add(jComboBoxFrequency, gridBagConstraints);

        jComboBoxUse.setFont(jComboBoxUse.getFont());
        jComboBoxUse.setMaximumSize(new java.awt.Dimension(200, 26));
        jComboBoxUse.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jComboBoxUseFocusGained(evt);
            }
        });
        jComboBoxUse.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jComboBoxUseMouseReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel7.add(jComboBoxUse, gridBagConstraints);

        jTextFieldUseUnit.setFont(jTextFieldUseUnit.getFont());
        jTextFieldUseUnit.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldUseUnit.setMinimumSize(new java.awt.Dimension(40, 24));
        jTextFieldUseUnit.setPreferredSize(new java.awt.Dimension(40, 24));
        jTextFieldUseUnit.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextFieldUseUnitFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextFieldUseUnitFocusLost(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel7.add(jTextFieldUseUnit, gridBagConstraints);

        jLabel11.setFont(jLabel11.getFont());
        jLabel11.setText("Dose");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel7.add(jLabel11, gridBagConstraints);

        jLabel12.setFont(jLabel12.getFont());
        jLabel12.setText("˹���");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel7.add(jLabel12, gridBagConstraints);

        jLabel14.setFont(jLabel14.getFont());
        jLabel14.setText("�������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel7.add(jLabel14, gridBagConstraints);

        jLabel15.setFont(jLabel15.getFont());
        jLabel15.setText("�Ը���");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel7.add(jLabel15, gridBagConstraints);

        jPanelSPU.add(jPanel7, "Normal");

        jPanel6.setMinimumSize(new java.awt.Dimension(156, 40));
        jPanel6.setPreferredSize(new java.awt.Dimension(156, 40));
        jPanel6.setLayout(new java.awt.GridBagLayout());

        jScrollPane11.setMinimumSize(new java.awt.Dimension(22, 45));
        jScrollPane11.setPreferredSize(new java.awt.Dimension(22, 45));

        jTextAreaTextUsage.setFont(jTextAreaTextUsage.getFont());
        jTextAreaTextUsage.setLineWrap(true);
        jScrollPane11.setViewportView(jTextAreaTextUsage);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel6.add(jScrollPane11, gridBagConstraints);

        jPanelSPU.add(jPanel6, "Special");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelDrug.add(jPanelSPU, gridBagConstraints);

        jPanel5.setLayout(new java.awt.GridBagLayout());

        jLabelDrugInstruction.setFont(jLabelDrugInstruction.getFont());
        jLabelDrugInstruction.setText("��͸Ժ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 10, 2, 2);
        jPanel5.add(jLabelDrugInstruction, gridBagConstraints);

        jLabel3.setFont(jLabel3.getFont());
        jLabel3.setText("����͹");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jLabel3, gridBagConstraints);

        jTextFieldCaution.setColumns(20);
        jTextFieldCaution.setFont(jTextFieldCaution.getFont());
        jTextFieldCaution.setRows(3);
        jScrollPane1.setViewportView(jTextFieldCaution);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 0.5;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jScrollPane1, gridBagConstraints);

        jTextFieldDescription.setColumns(20);
        jTextFieldDescription.setFont(jTextFieldDescription.getFont());
        jTextFieldDescription.setRows(3);
        jScrollPane4.setViewportView(jTextFieldDescription);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 0.5;
        gridBagConstraints.insets = new java.awt.Insets(2, 10, 2, 2);
        jPanel5.add(jScrollPane4, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelDrug.add(jPanel5, gridBagConstraints);

        jPanel9.setLayout(new java.awt.GridBagLayout());

        jLabel2.setFont(jLabel2.getFont());
        jLabel2.setText("�ӹǹ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 10, 2, 2);
        jPanel9.add(jLabel2, gridBagConstraints);

        jButtonReturnDrug.setFont(jButtonReturnDrug.getFont());
        jButtonReturnDrug.setText("�׹��");
        jButtonReturnDrug.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonReturnDrugActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel9.add(jButtonReturnDrug, gridBagConstraints);

        jTextFieldQtyReturn.setColumns(3);
        jTextFieldQtyReturn.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldQtyReturn.setText("0");
        jTextFieldQtyReturn.setFont(jTextFieldQtyReturn.getFont());
        jTextFieldQtyReturn.setMinimumSize(new java.awt.Dimension(50, 22));
        jTextFieldQtyReturn.setPreferredSize(new java.awt.Dimension(50, 22));
        jTextFieldQtyReturn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldQtyReturnActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 9;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel9.add(jTextFieldQtyReturn, gridBagConstraints);

        jLabelReturnDrug.setFont(jLabelReturnDrug.getFont());
        jLabelReturnDrug.setText("�ӹǹ���׹");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 10, 2, 2);
        jPanel9.add(jLabelReturnDrug, gridBagConstraints);

        jButtonSaveOrderDrug.setFont(jButtonSaveOrderDrug.getFont());
        jButtonSaveOrderDrug.setText("�ѹ�֡");
        jButtonSaveOrderDrug.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSaveOrderDrugActionPerformed(evt);
            }
        });
        jButtonSaveOrderDrug.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jButtonSaveOrderDrugKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel9.add(jButtonSaveOrderDrug, gridBagConstraints);

        txtQtyUnit.setDocument(new DoubleDocument());
        txtQtyUnit.setFont(txtQtyUnit.getFont());
        txtQtyUnit.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtQtyUnit.setMinimumSize(new java.awt.Dimension(50, 22));
        txtQtyUnit.setPreferredSize(new java.awt.Dimension(50, 22));
        txtQtyUnit.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtQtyUnitFocusGained(evt);
            }
        });
        txtQtyUnit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtQtyUnitActionPerformed(evt);
            }
        });
        txtQtyUnit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtQtyUnitKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel9.add(txtQtyUnit, gridBagConstraints);

        jLabelQtyUnit.setFont(jLabelQtyUnit.getFont());
        jLabelQtyUnit.setText("˹��¨���");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel9.add(jLabelQtyUnit, gridBagConstraints);

        panelPrintStickerQty.setLayout(new java.awt.GridBagLayout());

        txtStickerQty.setMinimum(1);
        txtStickerQty.setMinimumSize(new java.awt.Dimension(50, 22));
        txtStickerQty.setPreferredSize(new java.awt.Dimension(50, 22));
        txtStickerQty.setValue(1);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelPrintStickerQty.add(txtStickerQty, gridBagConstraints);

        lblSticker.setFont(lblSticker.getFont());
        lblSticker.setText("��ҡ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 7, 2, 2);
        panelPrintStickerQty.add(lblSticker, gridBagConstraints);

        lblStickerUnit.setFont(lblStickerUnit.getFont());
        lblStickerUnit.setText("�");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelPrintStickerQty.add(lblStickerUnit, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(2, 10, 2, 2);
        jPanel9.add(panelPrintStickerQty, gridBagConstraints);

        DayForUse2.setFont(DayForUse2.getFont());
        DayForUse2.setText("��������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel9.add(DayForUse2, gridBagConstraints);

        jTextFieldTotalDay.setColumns(3);
        jTextFieldTotalDay.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldTotalDay.setFont(jTextFieldTotalDay.getFont());
        jTextFieldTotalDay.setMinimumSize(new java.awt.Dimension(50, 22));
        jTextFieldTotalDay.setPreferredSize(new java.awt.Dimension(50, 22));
        jTextFieldTotalDay.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextFieldTotalDayFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel9.add(jTextFieldTotalDay, gridBagConstraints);

        jLabelQtyUnit1.setFont(jLabelQtyUnit1.getFont());
        jLabelQtyUnit1.setText("�ѹ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel9.add(jLabelQtyUnit1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        gridBagConstraints.weightx = 1.0;
        jPanelDrug.add(jPanel9, gridBagConstraints);

        panelOrder.add(jPanelDrug, "CARD_DRUG");

        jPanelLab.setLayout(new java.awt.GridBagLayout());

        jButtonSaveLab.setFont(jButtonSaveLab.getFont());
        jButtonSaveLab.setText("�ѹ�֡");
        jButtonSaveLab.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSaveLabActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelLab.add(jButtonSaveLab, gridBagConstraints);

        jcbLabOrderCause.setFont(jcbLabOrderCause.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelLab.add(jcbLabOrderCause, gridBagConstraints);

        jLabel7.setFont(jLabel7.getFont());
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("�˵ؼš����� Lab");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelLab.add(jLabel7, gridBagConstraints);

        panelCodeLabSecret.setLayout(new java.awt.GridBagLayout());

        jLabel1.setFont(jLabel1.getFont());
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("��������觵�Ǩ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        panelCodeLabSecret.add(jLabel1, gridBagConstraints);

        jTextFieldCodeLabSecret.setFont(jTextFieldCodeLabSecret.getFont());
        jTextFieldCodeLabSecret.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        jTextFieldCodeLabSecret.setMinimumSize(new java.awt.Dimension(100, 24));
        jTextFieldCodeLabSecret.setPreferredSize(new java.awt.Dimension(100, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 19, 5, 5);
        panelCodeLabSecret.add(jTextFieldCodeLabSecret, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        jPanelLab.add(panelCodeLabSecret, gridBagConstraints);

        jPanel4.setLayout(new java.awt.GridBagLayout());

        cbLabUrgent.setFont(cbLabUrgent.getFont());
        cbLabUrgent.setText("��觴�ǹ");
        cbLabUrgent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbLabUrgentActionPerformed(evt);
            }
        });
        jPanel4.add(cbLabUrgent, new java.awt.GridBagConstraints());

        jLabelUrgent2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelUrgent2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/urgeny.png"))); // NOI18N
        jLabelUrgent2.setPreferredSize(new java.awt.Dimension(16, 16));
        jPanel4.add(jLabelUrgent2, new java.awt.GridBagConstraints());

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(8, 0, 0, 0);
        jPanelLab.add(jPanel4, gridBagConstraints);

        jLabelNote5.setFont(jLabelNote5.getFont());
        jLabelNote5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabelNote5.setText("�����˵�");
        jLabelNote5.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelLab.add(jLabelNote5, gridBagConstraints);

        jTextFieldCautionLab.setFont(jTextFieldCautionLab.getFont());
        jTextFieldCautionLab.setMinimumSize(new java.awt.Dimension(250, 26));
        jTextFieldCautionLab.setPreferredSize(new java.awt.Dimension(250, 26));
        jTextFieldCautionLab.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldCautionLabActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelLab.add(jTextFieldCautionLab, gridBagConstraints);

        jPanel10.setLayout(new java.awt.GridBagLayout());

        jLabel10.setFont(jLabel10.getFont());
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel10.setText("�Ţ����Ե�ѳ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel10.add(jLabel10, gridBagConstraints);

        cbLabAtkProduct.setEditable(true);
        cbLabAtkProduct.setFont(cbLabAtkProduct.getFont());
        cbLabAtkProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbLabAtkProductActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 14, 5, 5);
        jPanel10.add(cbLabAtkProduct, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        jPanelLab.add(jPanel10, gridBagConstraints);

        panelOrder.add(jPanelLab, "CARD_LAB");

        jPanelSupply.setRequestFocusEnabled(false);
        jPanelSupply.setLayout(new java.awt.GridBagLayout());

        jLabelQty.setFont(jLabelQty.getFont());
        jLabelQty.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabelQty.setText("�ӹǹ��ʴ� ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelSupply.add(jLabelQty, gridBagConstraints);

        jTextFieldQty.setColumns(5);
        jTextFieldQty.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldQty.setText("1");
        jTextFieldQty.setFont(jTextFieldQty.getFont());
        jTextFieldQty.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextFieldQtyFocusLost(evt);
            }
        });
        jTextFieldQty.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldQtyKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelSupply.add(jTextFieldQty, gridBagConstraints);

        jLabelQty1.setFont(jLabelQty1.getFont());
        jLabelQty1.setText("���/�ѹ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelSupply.add(jLabelQty1, gridBagConstraints);

        jButtonSaveOrderSupply.setFont(jButtonSaveOrderSupply.getFont());
        jButtonSaveOrderSupply.setText("�ѹ�֡");
        jButtonSaveOrderSupply.setMargin(new java.awt.Insets(2, 7, 2, 7));
        jButtonSaveOrderSupply.setMaximumSize(new java.awt.Dimension(65, 25));
        jButtonSaveOrderSupply.setMinimumSize(new java.awt.Dimension(65, 25));
        jButtonSaveOrderSupply.setPreferredSize(new java.awt.Dimension(65, 25));
        jButtonSaveOrderSupply.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSaveOrderSupplyActionPerformed(evt);
            }
        });
        jButtonSaveOrderSupply.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jButtonSaveOrderSupplyKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelSupply.add(jButtonSaveOrderSupply, gridBagConstraints);

        jTextFieldCautionSupply.setFont(jTextFieldCautionSupply.getFont());
        jTextFieldCautionSupply.setMinimumSize(new java.awt.Dimension(250, 21));
        jTextFieldCautionSupply.setPreferredSize(new java.awt.Dimension(250, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelSupply.add(jTextFieldCautionSupply, gridBagConstraints);

        jLabelNote1.setFont(jLabelNote1.getFont());
        jLabelNote1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabelNote1.setText("�����˵�");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelSupply.add(jLabelNote1, gridBagConstraints);

        jLabelNote4.setFont(jLabelNote4.getFont());
        jLabelNote4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabelNote4.setText("�����Ţ�ػ�ó� (S/N)");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelSupply.add(jLabelNote4, gridBagConstraints);

        txtSupplySN.setFont(txtSupplySN.getFont());
        txtSupplySN.setMinimumSize(new java.awt.Dimension(250, 21));
        txtSupplySN.setPreferredSize(new java.awt.Dimension(250, 21));
        txtSupplySN.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtSupplySNFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelSupply.add(txtSupplySN, gridBagConstraints);

        jLabel4.setFont(jLabel4.getFont());
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("��������´�����");
        jLabel4.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelSupply.add(jLabel4, gridBagConstraints);

        txtSupplement.setColumns(20);
        txtSupplement.setRows(5);
        txtSupplement.setMinimumSize(new java.awt.Dimension(220, 80));
        jScrollPane2.setViewportView(txtSupplement);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelSupply.add(jScrollPane2, gridBagConstraints);

        panelOrder.add(jPanelSupply, "CARD_SUPPLY");

        jPanelBlank.setLayout(new java.awt.GridBagLayout());

        jLabelCause.setFont(jLabelCause.getFont());
        jLabelCause.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabelCause.setText("���˵ء��¡��ԡ");
        jLabelCause.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelBlank.add(jLabelCause, gridBagConstraints);

        jLabelEmployee.setFont(jLabelEmployee.getFont());
        jLabelEmployee.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabelEmployee.setText("���¡��ԡ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelBlank.add(jLabelEmployee, gridBagConstraints);

        jLabelEmployee1.setFont(jLabelEmployee1.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelBlank.add(jLabelEmployee1, gridBagConstraints);

        jLabelCause1.setEditable(false);
        jLabelCause1.setFont(jLabelCause1.getFont());
        jLabelCause1.setLineWrap(true);
        jLabelCause1.setWrapStyleWord(true);
        jLabelCause1.setEnabled(false);
        jScrollPane3.setViewportView(jLabelCause1);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weighty = 0.5;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelBlank.add(jScrollPane3, gridBagConstraints);

        jLabelNote3.setFont(jLabelNote3.getFont());
        jLabelNote3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabelNote3.setText("�����˵�");
        jLabelNote3.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelBlank.add(jLabelNote3, gridBagConstraints);

        jTextFieldCautionBlank.setFont(jTextFieldCautionBlank.getFont());
        jTextFieldCautionBlank.setMinimumSize(new java.awt.Dimension(250, 21));
        jTextFieldCautionBlank.setPreferredSize(new java.awt.Dimension(250, 21));
        jTextFieldCautionBlank.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldCautionBlankActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelBlank.add(jTextFieldCautionBlank, gridBagConstraints);

        cbBlankUrgent.setFont(cbBlankUrgent.getFont());
        cbBlankUrgent.setText("��觴�ǹ");
        cbBlankUrgent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbBlankUrgentActionPerformed(evt);
            }
        });
        jPanel3.add(cbBlankUrgent);

        jLabelUrgent.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelUrgent.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/urgeny.png"))); // NOI18N
        jLabelUrgent.setPreferredSize(new java.awt.Dimension(16, 16));
        jPanel3.add(jLabelUrgent);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(10, 0, 0, 0);
        jPanelBlank.add(jPanel3, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanelBlank.add(jSeparator1, gridBagConstraints);

        panelOrder.add(jPanelBlank, "CARD_BLANK");

        jPanelService.setLayout(new java.awt.GridBagLayout());

        jLabelServicePrice.setFont(jLabelServicePrice.getFont());
        jLabelServicePrice.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabelServicePrice.setText("�ҤҤ�Һ�ԡ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelService.add(jLabelServicePrice, gridBagConstraints);

        jLabel5.setFont(jLabel5.getFont());
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("�ӹǹ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelService.add(jLabel5, gridBagConstraints);

        jLabel6.setFont(jLabel6.getFont());
        jLabel6.setText("����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelService.add(jLabel6, gridBagConstraints);

        jTextFieldServicePrice.setColumns(5);
        jTextFieldServicePrice.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldServicePrice.setText("0");
        jTextFieldServicePrice.setFont(jTextFieldServicePrice.getFont());
        jTextFieldServicePrice.setMinimumSize(new java.awt.Dimension(61, 22));
        jTextFieldServicePrice.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldServicePriceKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelService.add(jTextFieldServicePrice, gridBagConstraints);

        jTextFieldServiceQty.setColumns(5);
        jTextFieldServiceQty.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldServiceQty.setText("1");
        jTextFieldServiceQty.setFont(jTextFieldServiceQty.getFont());
        jTextFieldServiceQty.setMinimumSize(new java.awt.Dimension(61, 22));
        jTextFieldServiceQty.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextFieldServiceQtyFocusLost(evt);
            }
        });
        jTextFieldServiceQty.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldServiceQtyKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelService.add(jTextFieldServiceQty, gridBagConstraints);

        jLabel61.setFont(jLabel61.getFont());
        jLabel61.setText("�ҷ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelService.add(jLabel61, gridBagConstraints);

        jTextFieldCautionService.setFont(jTextFieldCautionService.getFont());
        jTextFieldCautionService.setMinimumSize(new java.awt.Dimension(250, 21));
        jTextFieldCautionService.setPreferredSize(new java.awt.Dimension(250, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelService.add(jTextFieldCautionService, gridBagConstraints);

        jLabelNote2.setFont(jLabelNote2.getFont());
        jLabelNote2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabelNote2.setText("�����˵�");
        jLabelNote2.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelService.add(jLabelNote2, gridBagConstraints);

        jButtonSaveOrderService.setFont(jButtonSaveOrderService.getFont());
        jButtonSaveOrderService.setText("Save");
        jButtonSaveOrderService.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSaveOrderServiceActionPerformed(evt);
            }
        });
        jButtonSaveOrderService.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jButtonSaveOrderServiceKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelService.add(jButtonSaveOrderService, gridBagConstraints);

        panelOrder.add(jPanelService, "CARD_SERVICE");

        jPanelDrugs.setLayout(new java.awt.GridBagLayout());

        jPanel11.setLayout(new java.awt.GridBagLayout());

        DayForUse1.setFont(DayForUse1.getFont());
        DayForUse1.setText("��������㹡������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel11.add(DayForUse1, gridBagConstraints);

        txtUseDrugTotalDays.setColumns(3);
        txtUseDrugTotalDays.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtUseDrugTotalDays.setFont(txtUseDrugTotalDays.getFont());
        txtUseDrugTotalDays.setMinimumSize(new java.awt.Dimension(50, 25));
        txtUseDrugTotalDays.setPreferredSize(new java.awt.Dimension(50, 25));
        txtUseDrugTotalDays.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtUseDrugTotalDaysFocusGained(evt);
            }
        });
        txtUseDrugTotalDays.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUseDrugTotalDaysActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel11.add(txtUseDrugTotalDays, gridBagConstraints);

        jLabel9.setFont(jLabel9.getFont());
        jLabel9.setText("�ѹ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel11.add(jLabel9, gridBagConstraints);

        btnSaveOrderDrugs.setFont(btnSaveOrderDrugs.getFont());
        btnSaveOrderDrugs.setText("�ѹ�֡");
        btnSaveOrderDrugs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveOrderDrugsActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel11.add(btnSaveOrderDrugs, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanelDrugs.add(jPanel11, gridBagConstraints);

        panelOrder.add(jPanelDrugs, "CARD_DRUGS");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weighty = 1.0;
        jPanel12.add(panelOrder, gridBagConstraints);

        panelEditPrice.setLayout(new java.awt.GridBagLayout());

        jLabel13.setFont(jLabel13.getFont());
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel13.setText("�ҤҢ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        panelEditPrice.add(jLabel13, gridBagConstraints);

        doubleTextFieldCustomPrice.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        doubleTextFieldCustomPrice.setText("0");
        doubleTextFieldCustomPrice.setFont(doubleTextFieldCustomPrice.getFont());
        doubleTextFieldCustomPrice.setMinimumSize(new java.awt.Dimension(60, 21));
        doubleTextFieldCustomPrice.setPreferredSize(new java.awt.Dimension(60, 21));
        doubleTextFieldCustomPrice.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                doubleTextFieldCustomPriceFocusGained(evt);
            }
        });
        doubleTextFieldCustomPrice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                doubleTextFieldCustomPriceActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        panelEditPrice.add(doubleTextFieldCustomPrice, gridBagConstraints);

        jLabel48.setFont(jLabel48.getFont());
        jLabel48.setText("�ҷ (���˹���)");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        panelEditPrice.add(jLabel48, gridBagConstraints);

        btnUpdatePrice.setText("�ѹ�֡");
        btnUpdatePrice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdatePriceActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        panelEditPrice.add(btnUpdatePrice, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.weightx = 1.0;
        jPanel12.add(panelEditPrice, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        gridBagConstraints.weightx = 1.0;
        jPanel12.add(jPanel13, gridBagConstraints);

        jScrollPane6.setViewportView(jPanel12);

        add(jScrollPane6, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void jComboBoxFrequencyMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jComboBoxFrequencyMouseReleased
        JTextComponent editor = (JTextComponent) jComboBoxFrequency.getEditor().getEditorComponent();
        editor.setSelectionStart(0);
        editor.setSelectionStart(editor.getText().length());
    }//GEN-LAST:event_jComboBoxFrequencyMouseReleased

    private void jComboBoxUseMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jComboBoxUseMouseReleased
        JTextComponent editor = (JTextComponent) jComboBoxUse.getEditor().getEditorComponent();
        editor.setSelectionStart(0);
        editor.setSelectionStart(editor.getText().length());
    }//GEN-LAST:event_jComboBoxUseMouseReleased

    private void jComboBoxInstructionMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jComboBoxInstructionMouseReleased
        JTextComponent editor = (JTextComponent) jComboBoxInstruction.getEditor().getEditorComponent();
        editor.setSelectionStart(0);
        editor.setSelectionStart(editor.getText().length());
    }//GEN-LAST:event_jComboBoxInstructionMouseReleased

    private void jComboBoxDrugDoseShortcutMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jComboBoxDrugDoseShortcutMouseReleased
        JTextComponent editor = (JTextComponent) jComboBoxDrugDoseShortcut.getEditor().getEditorComponent();
        editor.setSelectionStart(0);
        editor.setSelectionStart(editor.getText().length());
    }//GEN-LAST:event_jComboBoxDrugDoseShortcutMouseReleased

    private void jTextFieldCautionBlankActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldCautionBlankActionPerformed
        this.saveOrderItem();
    }//GEN-LAST:event_jTextFieldCautionBlankActionPerformed

    private void jTextFieldUseUnitFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextFieldUseUnitFocusGained
        jTextFieldUseUnit.selectAll();
    }//GEN-LAST:event_jTextFieldUseUnitFocusGained

    private void jTextFieldUseUnitFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextFieldUseUnitFocusLost
        calculateAmountByDate(theOrderItem, theOrderItemDrug, true);
    }//GEN-LAST:event_jTextFieldUseUnitFocusLost

    private void formFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_formFocusGained
        this.jComboBoxDrugDoseShortcut.requestFocus();
    }//GEN-LAST:event_formFocusGained

    private void jTextFieldTotalDayFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextFieldTotalDayFocusGained
        jTextFieldTotalDay.selectAll();
    }//GEN-LAST:event_jTextFieldTotalDayFocusGained

    private void jComboBoxFrequencyFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jComboBoxFrequencyFocusGained
    }//GEN-LAST:event_jComboBoxFrequencyFocusGained

    private void jComboBoxUseFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jComboBoxUseFocusGained
    }//GEN-LAST:event_jComboBoxUseFocusGained

    private void jComboBoxInstructionFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jComboBoxInstructionFocusGained
    }//GEN-LAST:event_jComboBoxInstructionFocusGained

    private void txtQtyUnitFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtQtyUnitFocusGained
        txtQtyUnit.selectAll();
    }//GEN-LAST:event_txtQtyUnitFocusGained

    private void jTextFieldQtyReturnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldQtyReturnActionPerformed
        doReturnDrugAction();
    }//GEN-LAST:event_jTextFieldQtyReturnActionPerformed

    private void jComboBoxDrugDoseShortcutActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jComboBoxDrugDoseShortcutActionPerformed
    {//GEN-HEADEREND:event_jComboBoxDrugDoseShortcutActionPerformed
        String code = Gutil.getGuiData(jComboBoxDrugDoseShortcut);
        theDrugDoseShortcut = theHC.theLookupControl.readDrugDoseShortcutByCode(code);
        setDrugDoseShortcut(theDrugDoseShortcut);
    }//GEN-LAST:event_jComboBoxDrugDoseShortcutActionPerformed

    private void jButtonSaveLabActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonSaveLabActionPerformed
    {//GEN-HEADEREND:event_jButtonSaveLabActionPerformed
        if (panelCodeLabSecret.isVisible() && jTextFieldCodeLabSecret.isEnabled()) {
            theOrderLabSecret.specimen_code = jTextFieldCodeLabSecret.getText();
        }
        saveOrderItem();
    }//GEN-LAST:event_jButtonSaveLabActionPerformed

    private void txtQtyUnitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtQtyUnitActionPerformed
        jButtonSaveOrderDrugActionPerformed(null);
    }//GEN-LAST:event_txtQtyUnitActionPerformed

    private void jButtonSaveOrderDrugKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButtonSaveOrderDrugKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            jButtonSaveOrderDrugActionPerformed(null);
        }
    }//GEN-LAST:event_jButtonSaveOrderDrugKeyReleased

    private void jButtonSaveOrderSupplyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSaveOrderSupplyActionPerformed
        if (jTextFieldQty.getText().isEmpty()) {
            theUS.setStatus("��سҡ�͡�ӹǹ�Ǫ�ѳ���͹�ӡ�úѹ�֡", UpdateStatus.WARNING);
            return;
        }
        double qty = Double.parseDouble(jTextFieldQty.getText());
        if (qty < 1) {
            theUS.setStatus("��سҡ�͡�ӹǹ�Ǫ�ѳ�����ҡ���� 0 ��͹�ӡ�úѹ�֡", UpdateStatus.WARNING);
            return;
        }
        saveOrderItem();
    }//GEN-LAST:event_jButtonSaveOrderSupplyActionPerformed

    private void jCheckBoxSpecialUsageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxSpecialUsageActionPerformed
        if (jCheckBoxSpecialUsage.isSelected()) {
            layoutDrug.show(jPanelSPU, "Special");
            jTextAreaTextUsage.requestFocus();
        } else {
            layoutDrug.show(jPanelSPU, "Normal");
            this.jComboBoxInstruction.requestFocus();
        }
    }//GEN-LAST:event_jCheckBoxSpecialUsageActionPerformed

    private void jButtonSaveOrderSupplyKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButtonSaveOrderSupplyKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_UP) {
            jTextFieldQty.requestFocus();
        }

    }//GEN-LAST:event_jButtonSaveOrderSupplyKeyReleased

    private void jTextFieldServicePriceKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldServicePriceKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_DOWN) {
            jTextFieldServiceQty.requestFocus();
        }
    }//GEN-LAST:event_jTextFieldServicePriceKeyReleased

    private void jButtonSaveOrderServiceKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButtonSaveOrderServiceKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_UP) {
            jTextFieldServiceQty.requestFocus();
        }
    }//GEN-LAST:event_jButtonSaveOrderServiceKeyReleased

    private void jTextFieldQtyKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldQtyKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_DOWN) {
            jButtonSaveOrderSupply.requestFocus();
        }
    }//GEN-LAST:event_jTextFieldQtyKeyReleased

    private void jTextFieldServiceQtyKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldServiceQtyKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_DOWN) {
            jButtonSaveOrderService.requestFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_UP) {
            jTextFieldServicePrice.requestFocus();
        }
    }//GEN-LAST:event_jTextFieldServiceQtyKeyReleased

    private void jTextFieldServiceQtyFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextFieldServiceQtyFocusLost

    }//GEN-LAST:event_jTextFieldServiceQtyFocusLost

    private void jTextFieldQtyFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextFieldQtyFocusLost

    }//GEN-LAST:event_jTextFieldQtyFocusLost

    private void jButtonSaveOrderServiceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSaveOrderServiceActionPerformed
        if (jTextFieldServicePrice.getText().isEmpty()) {
            theUS.setStatus("��سҡ�͡�ҤҤ�Һ�ԡ�á�͹�ӡ�úѹ�֡", UpdateStatus.WARNING);
            return;
        }
        if (jTextFieldServiceQty.getText().isEmpty()) {
            theUS.setStatus("��سҡ�͡�ӹǹ����㹡������ԡ�á�͹�ӡ�úѹ�֡", UpdateStatus.WARNING);
            return;
        }
        double qty = Double.parseDouble(jTextFieldServiceQty.getText());
        if (qty < 1) {
            theUS.setStatus("��سҡ�͡�ӹǹ����㹡������ԡ�÷���ҡ����0��͹�ӡ�úѹ�֡", UpdateStatus.WARNING);
            return;
        }
        saveOrderItem();
    }//GEN-LAST:event_jButtonSaveOrderServiceActionPerformed

    private void jButtonReturnDrugActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonReturnDrugActionPerformed
        this.doReturnDrugAction();
    }//GEN-LAST:event_jButtonReturnDrugActionPerformed

    private void jButtonSaveOrderDrugActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSaveOrderDrugActionPerformed
        if (jCheckBoxSpecialUsage.isSelected() && jTextAreaTextUsage.getText().isEmpty()) {
            theUS.setStatus("��سҡ�͡�Ըա�����繾���ɡ�͹�ӡ�úѹ�֡", UpdateStatus.WARNING);
            return;
        }
        saveOrderItem();
    }//GEN-LAST:event_jButtonSaveOrderDrugActionPerformed

    private void txtSupplySNFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtSupplySNFocusGained
        txtSupplySN.selectAll();
    }//GEN-LAST:event_txtSupplySNFocusGained

    private void txtUseDrugTotalDaysFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtUseDrugTotalDaysFocusGained
        txtUseDrugTotalDays.selectAll();
    }//GEN-LAST:event_txtUseDrugTotalDaysFocusGained

    private void btnSaveOrderDrugsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveOrderDrugsActionPerformed
        this.doSaveOrderItemDrugs();
    }//GEN-LAST:event_btnSaveOrderDrugsActionPerformed

    private void txtUseDrugTotalDaysActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUseDrugTotalDaysActionPerformed
        btnSaveOrderDrugs.doClick();
    }//GEN-LAST:event_txtUseDrugTotalDaysActionPerformed

    private void txtQtyUnitKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtQtyUnitKeyReleased
        if (theOrderItemDrug != null
                && theOrderItemDrug.printing.equals("1")
                && theOrderItemDrug.print_equal_quantity.equals("1")) {
            txtStickerQty.setValue(txtQtyUnit.getText().isEmpty() ? 1
                    : ((Double) Math.ceil(Double.parseDouble(txtQtyUnit.getText()))).intValue());
        }
    }//GEN-LAST:event_txtQtyUnitKeyReleased

    private void doubleTextFieldCustomPriceFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_doubleTextFieldCustomPriceFocusGained
        doubleTextFieldCustomPrice.selectAll();
    }//GEN-LAST:event_doubleTextFieldCustomPriceFocusGained

    private void doubleTextFieldCustomPriceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_doubleTextFieldCustomPriceActionPerformed
        btnUpdatePrice.doClick();
    }//GEN-LAST:event_doubleTextFieldCustomPriceActionPerformed

    private void btnUpdatePriceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdatePriceActionPerformed
        this.doUpdateCustomPrice();
    }//GEN-LAST:event_btnUpdatePriceActionPerformed

    private void jCheckBoxDrugStatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxDrugStatActionPerformed
        this.doUpdateOrderStatus();
    }//GEN-LAST:event_jCheckBoxDrugStatActionPerformed

    private void cbBlankUrgentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbBlankUrgentActionPerformed
        this.doUpdateOrderStatus();
    }//GEN-LAST:event_cbBlankUrgentActionPerformed

    private void cbLabUrgentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbLabUrgentActionPerformed
        this.doUpdateOrderStatus();
    }//GEN-LAST:event_cbLabUrgentActionPerformed

    private void jTextFieldCautionLabActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldCautionLabActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldCautionLabActionPerformed

    private void cbLabAtkProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbLabAtkProductActionPerformed
        if (evt.getActionCommand().equals("comboBoxEdited")) {
            String keyword = String.valueOf(cbLabAtkProduct.getSelectedItem());
            if (cbLabAtkProduct.getSelectedIndex() >= 0) {
                CommonInf commonInf = (CommonInf) cbLabAtkProduct.getSelectedItem();
                if (commonInf.getName().trim().equals(keyword.trim())) {
                    return;
                }
            }
            List<CommonInf> v = theHC.theLookupControl.listLabAtkProductByKeyword(keyword.trim());
            if (v != null && !v.isEmpty() && v.size() > 1) {
                LabAtkProduct undefine = new LabAtkProduct();
                undefine.setObjectId(null);
                undefine.lab_atk_device_name = "����к�";
                if (keyword.trim().isEmpty()) {
                    v.add(0, undefine);
                }
            }
            if (v == null) {
                v = new ArrayList<>();
            }
            if (!v.isEmpty()) {
                ComboboxModel.initComboBox(cbLabAtkProduct, v);
            }
        }
    }//GEN-LAST:event_cbLabAtkProductActionPerformed

    private void btnRefreshDoseShortcutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshDoseShortcutActionPerformed
        jComboBoxDrugDoseShortcut.refresh();
    }//GEN-LAST:event_btnRefreshDoseShortcutActionPerformed

    /**
     * ��˹���������´�ͧ������� �����¡�� Dose ��ͷ�����͡�ҡ ComboBox
     *
     * @return
     * @Author pu
     * @Date 07/08/2549
     */
    @Override
    public boolean saveOrderItem() {
        return theHC.theOrderControl.saveOrderItem(getOrderItem(), getOrderItemDrug(), theOrderLabSecret);
    }

    private void doSaveOrderItemDrugs() {
        if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(this, "�׹�ѹ�к���������㹡�����ҡѺ�ء��¡�÷�����͡ ���������", "�׹�ѹ�к���������㹡������", JOptionPane.YES_NO_OPTION)) {
            int countUnsave = 0;
            String unSaveOrderName = "";
            double day = Double.parseDouble(this.txtUseDrugTotalDays.getText().isEmpty() ? "0" : this.txtUseDrugTotalDays.getText());
            if (day == 0d) {
                theUS.setStatus("�к���������㹡������ ����ͧ�����ա����", UpdateStatus.WARNING);
                txtUseDrugTotalDays.requestFocus();
            }
            for (OrderItem orderItem : orderItems) {
                if (orderItem.isDrug() && orderItem.status.equals(OrderStatus.NOT_VERTIFY)) {
                    OrderItemDrug orderItemDrug = orderItem.theOrderItemDrug;
                    if (orderItemDrug == null) {
                        orderItemDrug = theHC.theOrderControl.readOrderItemDrugByOid(orderItem.getObjectId());
                    }
                    DrugFrequency df = theHC.theSetupControl.listDrugFrequencyByPK(orderItemDrug.frequency);
                    double freq = Double.parseDouble(df.factor == null || df.factor.isEmpty() ? "0" : df.factor);
                    if (freq == 0d) {
                        continue;
                    }
                    double use_qty = Constant.toDouble(orderItemDrug.dose);
                    if (use_qty == 0d) {
                        continue;
                    }
                    double qty = freq * use_qty * day;
                    orderItem.qty = Constant.getShowDoubleString(qty);
                    if (!theHC.theOrderControl.saveOrderItem(orderItem, orderItemDrug, null)) {
                        countUnsave++;
                        unSaveOrderName += (unSaveOrderName.isEmpty() ? "" : ", ") + orderItem.common_name;
                    }
                }
            }
            if (countUnsave > 0) {
                theUS.setStatus("�������ö�ѹ�֡��ӹǹ " + countUnsave + " ��¡�� : " + unSaveOrderName, UpdateStatus.WARNING);
            } else {
                theUS.setStatus("�ѹ�֡��������㹡�����ҵ���ӹǹ�ѹ �����", UpdateStatus.COMPLETE);
            }
        }
    }

    private void setDrugDoseShortcut(DrugDoseShortcut ddsc) {
        theDrugDoseShortcut = ddsc;
        if (theDrugDoseShortcut == null) {
            return;
        }
        Gutil.setGuiData(jComboBoxInstruction, theDrugDoseShortcut.drug_instruction_id);
        Gutil.setGuiData(jComboBoxFrequency, theDrugDoseShortcut.drug_frequency_id);
        jTextFieldUseUnit.setText(Constant.getShowDoubleString(theDrugDoseShortcut.qty));
        Gutil.setGuiData(jComboBoxUse, theDrugDoseShortcut.drug_uom_id);
    }

    private double calculateAmountByDate(OrderItem oi, OrderItemDrug oid, boolean alert) {
        if (alert && !oid.purch_uom.equals(oid.use_uom)) {
            theUS.setStatus(Constant.getTextBundle("˹����ҷ������Ш��µ�ҧ�ѹ") + " "
                    + Constant.getTextBundle("�������ö�ӹǳ�ӹǹ����"), UpdateStatus.WARNING);
            return 0d;
        }
        DrugFrequency df = (DrugFrequency) jComboBoxFrequency.getSelectedItem();
        try {
            double freq = Double.parseDouble(df.factor == null || df.factor.isEmpty() ? "0" : df.factor);
            double use_qty = Double.parseDouble(this.jTextFieldUseUnit.getText().isEmpty() ? "0" : this.jTextFieldUseUnit.getText());
            double day = Double.parseDouble(this.jTextFieldTotalDay.getText().isEmpty() ? "0" : this.jTextFieldTotalDay.getText());
            if (day == 0) {
                return 0d;
            }
            double total = freq * use_qty * day;
            this.txtQtyUnit.setText(Constant.getShowDoubleString(total));
            if (theOrderItemDrug != null
                    && theOrderItemDrug.printing.equals("1")
                    && theOrderItemDrug.print_equal_quantity.equals("1")) {
                txtStickerQty.setValue(((Double) Math.ceil(total)).intValue());
            }
            return total;
        } catch (Exception ex) {
            if (alert) {
                theUS.setStatus(Constant.getTextBundle("��äӹǳ�ӹǹ���Դ�����Դ��Ҵ"), UpdateStatus.WARNING);
            }
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
            return 0d;
        }
    }

    public static int countWaitDispense(Vector theOrderItemV) {
        int queueDespense = 0;
        for (int i = 0; i < theOrderItemV.size(); i++) {
            OrderItem theOrderItemVs = (OrderItem) theOrderItemV.get(i);
            if (theOrderItemVs.category_group.equals("1")
                    && theOrderItemVs.charge_complete.equals(Active.isEnable())
                    && theOrderItemVs.dispense != null
                    && !theOrderItemVs.dispense.isEmpty()) {
                queueDespense += 1;
            }
        }
        return queueDespense;
    }

    /*
     * �ӡ�ô֧��Ҩҡ keyword �����繤�� string input String keyword output
     * String (��Ңͧ string)
     */
    @Override
    public OrderItem getOrderItem() {
        if (theOrderItem.isService()) {
            if (!theOrderItem.isEditedPrice() && theOrderItem.isEditablePrice()) {
                if (!jTextFieldServicePrice.getText().equals(theOrderItem.price)) {
                    theOrderItem.order_edited_price = "1";
                }
            }
            theOrderItem.qty = jTextFieldServiceQty.getText();
            theOrderItem.price = jTextFieldServicePrice.getText();
            theOrderItem.note = this.jTextFieldCautionService.getText();
        } else if (theOrderItem.isDrug()) {
            theOrderItem.qty = txtQtyUnit.getText();
        } else if (theOrderItem.isSupply()) {
            theOrderItem.qty = jTextFieldQty.getText();
            theOrderItem.note = this.jTextFieldCautionSupply.getText();
            theOrderItem.serial_number = this.txtSupplySN.getText();
            theOrderItem.supplement_label = this.txtSupplement.getText();
        } else if (theOrderItem.isLab()) {
            theOrderItem.b_lab_order_cause_id = ComboboxModel.getCodeComboBox(jcbLabOrderCause);
            theOrderItem.order_urgent_status = this.cbLabUrgent.isSelected() ? "1" : "0";
            theOrderItem.note = this.jTextFieldCautionLab.getText();
            theOrderItem.f_lab_atk_product_id = ComboboxModel.getCodeComboBox(cbLabAtkProduct);
        } else {
            theOrderItem.order_urgent_status = this.cbBlankUrgent.isSelected() ? "1" : "0";
            theOrderItem.note = this.jTextFieldCautionBlank.getText();
        }
        return theOrderItem;
    }

    @Override
    public OrderItemDrug getOrderItemDrug() {
        if (theOrderItemDrug == null) {
            return null;
        }
        /*
         * �����ǹ�ͧ dose
         */
        if (jCheckBoxSpecialUsage.isSelected()) {/*
             * dose Ẻ�����
             */
            theOrderItemDrug.usage_special = "1";
            theOrderItemDrug.usage_text = jTextAreaTextUsage.getText();
            theOrderItemDrug.instruction = "1510000000001";
            theOrderItemDrug.frequency = "1500000000001";
            theOrderItemDrug.dose = "";
            theOrderItemDrug.use_uom = "2520000000001";
        } else {/*
             * dose ����
             */
            theOrderItemDrug.usage_special = "0";
            theOrderItemDrug.usage_text = "";
            theOrderItemDrug.instruction = jComboBoxInstruction.getText();
            theOrderItemDrug.frequency = jComboBoxFrequency.getText();
            theOrderItemDrug.dose = jTextFieldUseUnit.getText();
            theOrderItemDrug.use_uom = jComboBoxUse.getText();
            theOrderItemDrug.status = theOrderItem != null ? theOrderItem.status : "0";

            Uom2 uom = theHC.theLookupControl.readUomById(theOrderItemDrug.use_uom);
            DrugFrequency2 freq = theHC.theLookupControl.readDrugFrequencyById(theOrderItemDrug.frequency);
            if (uom != null && freq != null) {
                theOrderItemDrug.generateDoseShort(theOrderItemDrug.dose, uom.uom_id, freq.drug_frequency_id);
            }
        }
        theOrderItemDrug.caution = jTextFieldCaution.getText();
        theOrderItemDrug.description = jTextFieldDescription.getText();

        theOrderItemDrug.drug_stat_status = jCheckBoxDrugStat.isSelected() ? "1" : "0";
        theOrderItemDrug.drug_sticker_quantity = theOrderItemDrug.printing.equals("1")
                ? txtStickerQty.getValue() : 1;
        return theOrderItemDrug;
    }

    /*
     * �繡�����͡��¡�� item ������������ʴ���������´�ͧ item �����Ф��
     * default order
     *
     */
    @Override
    public boolean setItem(Item item) {
        theItem = item;
        if (item == null) {
            theOrderItem = null;
            layoutOrder.show(panelOrder, CARD_BLANK);
            return false;
        }
        String date_time = theHO.date_time;
        setBorder(new TitledBorder(null, item.common_name, javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));
        CategoryGroupItem cgi = theHC.theLookupControl.readCategoryGroupItemById(item.item_group_code_category);
        Drug drug = cgi.category_group_code.equals(CategoryGroup.isDrug())
                ? theHC.theOrderControl.listDrugByItem(item.getObjectId())
                : null;
        ItemSupply supply = cgi.category_group_code.equals(CategoryGroup.isSupply())
                ? theHC.theSetupControl.readItemSupplyByItemId(item.getObjectId())
                : null;
        ItemPrice ip = theHC.theOrderControl.readItemPriceByItem(item.getObjectId());
        theOrderItem = theHO.initOrderItem(theHO.theVisit.visit_type, item, cgi, ip, date_time, drug, supply);
        OrderItemDrug oid = null;
        if (drug != null) {
            Uom2 uom = theHC.theLookupControl.readUomById(drug.use_uom);
            DrugFrequency2 freq = theHC.theLookupControl.readDrugFrequencyById(drug.frequency);
            oid = theHO.initOrderItemDrug(drug, uom, freq);
        }
        if (cgi.category_group_code.equals(CategoryGroup.isDrug())) {
            jComboBoxDrugDoseShortcut.refresh();
            layoutOrder.show(panelOrder, CARD_DRUG);
            if (drug == null) {
                theUS.setStatus(Constant.getTextBundle("��辺��������´�������") + " "
                        + Constant.getTextBundle("��سҵ�Ǩ�ͺ��¡���ҹ���˹�� Admin"), UpdateStatus.WARNING);
            }
        }
        if (cgi.category_group_code.equals(CategoryGroup.isSupply())) {
            layoutOrder.show(panelOrder, CARD_SUPPLY);
        }
        setObject(theOrderItem, oid, null);
        if (drug != null && !drug.usage_text.isEmpty() && !drug.usage_text.equals("null")) {
            jCheckBoxSpecialUsage.setSelected(true);
            jTextAreaTextUsage.setText(drug.usage_text);
            jCheckBoxSpecialUsageActionPerformed(null);
        }
        return true;
    }

    /*
     * �������¡�âͧ�� �Ըա������ ��Ҩҡ theDrug �� ����� Object
     */
    @Override
    public boolean setOrderItem(OrderItem oi) {
        this.theOrderItem = oi == null ? null : (OrderItem) oi.clone();
        this.orderItems = null;
        OrderItemDrug oid = null;
        OrderLabSecret ols = null;
        if (oi != null) {
            String suffixName = oi.getProperty("suffix_name") == null ? "" : (String) oi.getProperty("suffix_name");
            setBorder(new TitledBorder(null, oi.common_name + suffixName, javax.swing.border.TitledBorder.CENTER, TitledBorder.DEFAULT_POSITION));
            if (oi.isDrug()) {
                oid = oi.theOrderItemDrug;
                if (oid == null) {
                    oid = theHC.theOrderControl.readOrderItemDrugByOid(oi.getObjectId());
                }
            }
            if (oi.isLab() && oi.secret.equals("1")) {
                ols = theHC.theOrderControl.readOrderLabSecret(oi.getObjectId());
            }
        } else {
            setBorder(new TitledBorder(""));
        }
        setObject(oi, oid, ols);
        return true;
    }

    @Override
    public boolean setOrderItems(OrderItem... orderItems) {
        setOrderItem(null);
        txtUseDrugTotalDays.setText("");
        this.orderItems = orderItems;
        int drugCount = 0;
        String drugName = "";
        for (OrderItem orderItem : orderItems) {
            if (orderItem.isDrug()) {
                drugCount++;
                drugName += (drugName.isEmpty() ? "" : ", ") + orderItem.common_name;
            }
        }
        if (drugCount > 0) {
            layoutOrder.show(panelOrder, CARD_DRUGS);
        }
        txtUseDrugTotalDays.requestFocus();
        return true;
    }

    protected void setObject(OrderItem oi, OrderItemDrug oid, OrderLabSecret ois) {
        theOrderLabSecret = ois;
        boolean is_lock = false;
        setEnableCardBlank(false);
        layoutOrder.show(panelOrder, CARD_BLANK);
        this.jTextFieldCautionBlank.setText("");
        this.jTextFieldCautionBlank.setVisible(false);
        setEnabledUrgent(false);
        this.jLabelNote3.setVisible(false);
        if (oi == null) {
            theItem = null;
            return;
        }
        theItem = theHC.theLookupControl.readItemById(oi.item_code);

        this.jTextFieldCautionBlank.setVisible(true);
        this.jLabelNote3.setVisible(true);

        boolean is_charge = oi.charge_complete.equals(Active.isEnable());
        boolean is_cancel = oi.status.equals(OrderStatus.DIS_CONTINUE);
        boolean is_dispense = oi.status.equals(OrderStatus.DISPENSE);
        boolean isVerify = !oi.status.equals(OrderStatus.NOT_VERTIFY);

        if (theHO.theVisit != null) {
            is_lock = theHO.theVisit.locking.equals("1")
                    && !theHO.theVisit.lock_user.equals(theHO.theEmployee.getObjectId());
        }
        setEnabledOther(!is_dispense && !is_lock && !is_cancel && !is_charge);
        setEnabledDrug(!is_lock && !is_cancel);
        setEnabledUrgent(isVerify && (theOrderItem.isLab() || theOrderItem.isXray() || theOrderItem.isDrug()));
        ////lab//////////////////////////////////////////////////\
        if (oi.isLab()) {
            if (oi.status.equals(OrderStatus.DIS_CONTINUE)) {
                layoutOrder.show(panelOrder, CARD_BLANK);
                setEnableCardBlank(true);
                if (oi.cause_cancel_resultlab.isEmpty()) {
                    jLabelCause1.setText("");
                    theUS.setStatus("��سҵ�Ǩ�ͺ���˵ء��¡��ԡ�Ż�ա����㹰ҹ������", UpdateStatus.WARNING);
                } else {
                    jLabelCause1.setText(oi.cause_cancel_resultlab);
                }
                Employee em = theHC.theLookupControl.readEmployeeById(oi.discontinue);
                if (em != null) {
                    jLabelEmployee1.setText(em.person.person_firstname + " " + em.person.person_lastname);
                } else {
                    jLabelEmployee1.setText(Constant.getTextBundle("�����ҹ�ҧ���١ź�͡�ҡ�ҹ������"));
                }
            } else {
                List<CommonInf> v = theHC.theLookupControl.listLabAtkProductByKeyword("");
                ComboboxModel.initComboBox(cbLabAtkProduct, v);
                LabAtkProduct undefine = new LabAtkProduct();
                undefine.setObjectId(null);
                undefine.lab_atk_device_name = "����к�";
                cbLabAtkProduct.insertItemAt((Object) undefine, 0);
                panelCodeLabSecret.setVisible(false);
                //amp:02/03/2549 ����������觵�Ǩ㹡ó��� Lab ���Դ
                if ("1".equals(theOrderItem.secret)) {
                    this.theOrderLabSecret = new OrderLabSecret();
                    jTextFieldCodeLabSecret.setText("");
                    if (ois != null) {
                        if ("3".equals(theHO.theEmployee.authentication_id)
                                || (oi.vertifier != null && oi.vertifier.equals(theHO.theEmployee.getObjectId()))
                                || oi.order_user.equals(theHO.theEmployee.getObjectId())) {
                            jTextFieldCodeLabSecret.setText(ois.specimen_code);
                            jTextFieldCodeLabSecret.setEnabled(true);
                        } else {
                            jTextFieldCodeLabSecret.setText("*********");
                            jTextFieldCodeLabSecret.setEnabled(true);
                        }
                    }
                    panelCodeLabSecret.setVisible(true);
                }
                ComboboxModel.setCodeComboBox(jcbLabOrderCause, oi.b_lab_order_cause_id);
                ComboboxModel.setCodeComboBox(cbLabAtkProduct, oi.f_lab_atk_product_id);
                layoutOrder.show(panelOrder, CARD_LAB);
            }
            this.cbLabUrgent.setSelected(!theOrderItem.order_urgent_status.equals("0"));
            this.jTextFieldCautionLab.setText(oi.note);
        }
        ////xray//////////////////////////////////////////////////
        if (oi.isXray()) {
            layoutOrder.show(panelOrder, CARD_BLANK);
            this.cbBlankUrgent.setSelected(!theOrderItem.order_urgent_status.equals("0"));
            this.jTextFieldCautionBlank.setText(oi.note);
        }
        ////supply//////////////////////////////////////////////////
        if (oi.isSupply()) {
            layoutOrder.show(panelOrder, CARD_SUPPLY);
            jTextFieldQty.setText(Constant.getShowDoubleString(oi.qty));
            ////////////////////////////////////////////////
            jTextFieldQty.setEditable(!is_charge && !is_lock);
            this.txtSupplySN.setText(oi.serial_number == null ? "" : oi.serial_number);
            this.jTextFieldCautionSupply.setText(oi.note);
            txtSupplement.setText(oi.supplement_label);
        }
        ////service//////////////////////////////////////////////////
        if (oi.isService()) {
            layoutOrder.show(panelOrder, CARD_SERVICE);
            jTextFieldServicePrice.setText(oi.price);
            jTextFieldServicePrice.setEditable(oi.isEditablePrice());
            jTextFieldServiceQty.setText(Constant.getShowDoubleString(oi.qty));
            this.jTextFieldCautionService.setText(oi.note);
        }
        if (oi.isPackage()) {
            layoutOrder.show(panelOrder, CARD_BLANK);
            this.cbBlankUrgent.setSelected(false);
            this.jTextFieldCautionBlank.setText(oi.note);
        }
        ////drug//////////////////////////////////////////////////
        if (oi.isDrug()) {
            jComboBoxDrugDoseShortcut.refresh();
            layoutOrder.show(panelOrder, CARD_DRUG);
            setOrderItemDrug(oid);
            String qty = Constant.getShowDoubleString(oi.qty);
            txtQtyUnit.setText(qty);
            // set default for first time
            if (oi.getObjectId() == null
                    && oid.printing.equals("1")
                    && oid.print_equal_quantity.equals("1")) {
                txtStickerQty.setValue(((Double) Math.ceil(Double.parseDouble(qty))).intValue());
            }
            //������ҷ��׹��е�ͧ   ʶҹШ��� ��� �繼������//////////////////
            boolean is_return = oi.status.equals(OrderStatus.DISPENSE)
                    && theHO.theVisit.visit_type.equals(VisitType.IPD);
            jTextFieldQtyReturn.setVisible(is_return);
            jLabelReturnDrug.setVisible(is_return);
            jButtonReturnDrug.setVisible(is_return);
            ///////////////////////////////////////////////////////////
        } else {
            theOrderItemDrug = null;
        }
        panelEditPrice.setVisible(false);
        if (oi.getObjectId() != null && !oi.isService() && oi.isEditablePrice()) {
            panelEditPrice.setVisible(true);
            doubleTextFieldCustomPrice.setText(theOrderItem.price);
        }
    }

    private void setOrderItemDrug(OrderItemDrug oid) {
        theOrderItemDrug = oid;
        if (oid == null) {
            theUS.setStatus("�������������´�����š�������", UpdateStatus.WARNING);
            return;
        }
        if (oid.usage_special.equals(Active.isEnable())) {
            layoutDrug.show(jPanelSPU, "Special");
            jCheckBoxSpecialUsage.setSelected(true);
            jTextAreaTextUsage.setText(oid.usage_text);
            jComboBoxInstruction.setText("");
            jComboBoxFrequency.setText("");
            jTextFieldUseUnit.setText("");
            jComboBoxUse.setText("");
        } else {
            layoutDrug.show(jPanelSPU, "Normal");
            if (Constant.toDouble(oid.dose) == 0) {
                oid.dose = "";
            }
            jCheckBoxSpecialUsage.setSelected(false);
            jComboBoxInstruction.setText(oid.instruction);
            jComboBoxFrequency.setText(oid.frequency);
            jTextFieldUseUnit.setText(Constant.getShowDoubleString(oid.dose));
            jComboBoxUse.setText(oid.use_uom);
            jTextAreaTextUsage.setText("");
        }
        jTextFieldCaution.setText(oid.caution.trim());
        jTextFieldDescription.setText(oid.description.trim());
        jCheckBoxDrugStat.setSelected(!oid.drug_stat_status.equals("0"));
        lblPregnancyCategory.setText(oid.pregnancy_category.equals("") ? "N/A" : oid.pregnancy_category);
        Uom2 uom2 = theHC.theLookupControl.readUomById(oid.purch_uom);
        jLabelQtyUnit.setText(uom2 != null ? uom2.description : "");
        jTextFieldTotalDay.getDocument().removeDocumentListener(orderDayDL);
        jTextFieldTotalDay.setText("");
        jTextFieldTotalDay.getDocument().addDocumentListener(orderDayDL);
        jTextFieldQtyReturn.setText("");
        txtStickerQty.setValue(oid.drug_sticker_quantity < 1 ? 1 : oid.drug_sticker_quantity);
        panelPrintStickerQty.setVisible(oid.printing.equals("1"));
    }

    private void setEnabledDrug(boolean b) { // pu : 25/07/2549 : ��� Dose ���� ���������ö��� Dose ����
        jButtonSaveOrderDrug.setEnabled(b);
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel DayForUse1;
    private javax.swing.JLabel DayForUse2;
    private javax.swing.JButton btnRefreshDoseShortcut;
    public javax.swing.JButton btnSaveOrderDrugs;
    private javax.swing.JButton btnUpdatePrice;
    private javax.swing.JCheckBox cbBlankUrgent;
    protected javax.swing.JComboBox cbLabAtkProduct;
    private javax.swing.JCheckBox cbLabUrgent;
    private com.hospital_os.utility.DoubleTextField doubleTextFieldCustomPrice;
    private javax.swing.JButton jButtonReturnDrug;
    private javax.swing.JButton jButtonSaveLab;
    public javax.swing.JButton jButtonSaveOrderDrug;
    public javax.swing.JButton jButtonSaveOrderService;
    public javax.swing.JButton jButtonSaveOrderSupply;
    private javax.swing.JCheckBox jCheckBoxDrugStat;
    private javax.swing.JCheckBox jCheckBoxSpecialUsage;
    private com.hosv3.gui.component.HosComboBox jComboBoxDrugDoseShortcut;
    private com.hosv3.gui.component.HosComboBox jComboBoxFrequency;
    private com.hosv3.gui.component.HosComboBox jComboBoxInstruction;
    private com.hosv3.gui.component.HosComboBox jComboBoxUse;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel61;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelCause;
    private javax.swing.JTextArea jLabelCause1;
    private javax.swing.JLabel jLabelDoseShortcut;
    private javax.swing.JLabel jLabelDrugInstruction;
    private javax.swing.JLabel jLabelEmployee;
    private javax.swing.JLabel jLabelEmployee1;
    private javax.swing.JLabel jLabelNote1;
    private javax.swing.JLabel jLabelNote2;
    private javax.swing.JLabel jLabelNote3;
    private javax.swing.JLabel jLabelNote4;
    private javax.swing.JLabel jLabelNote5;
    private javax.swing.JLabel jLabelQty;
    private javax.swing.JLabel jLabelQty1;
    private javax.swing.JLabel jLabelQtyUnit;
    private javax.swing.JLabel jLabelQtyUnit1;
    private javax.swing.JLabel jLabelReturnDrug;
    private javax.swing.JLabel jLabelServicePrice;
    private javax.swing.JLabel jLabelUrgent;
    private javax.swing.JLabel jLabelUrgent2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JPanel jPanelBlank;
    private javax.swing.JPanel jPanelDrug;
    private javax.swing.JPanel jPanelDrugs;
    private javax.swing.JPanel jPanelLab;
    private javax.swing.JPanel jPanelSPU;
    private javax.swing.JPanel jPanelService;
    private javax.swing.JPanel jPanelSupply;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextArea jTextAreaTextUsage;
    private javax.swing.JTextArea jTextFieldCaution;
    private javax.swing.JTextField jTextFieldCautionBlank;
    private javax.swing.JTextField jTextFieldCautionLab;
    private javax.swing.JTextField jTextFieldCautionService;
    private javax.swing.JTextField jTextFieldCautionSupply;
    private javax.swing.JTextField jTextFieldCodeLabSecret;
    private javax.swing.JTextArea jTextFieldDescription;
    private com.hospital_os.utility.DoubleTextField jTextFieldQty;
    protected com.hospital_os.utility.DoubleTextField jTextFieldQtyReturn;
    private com.hospital_os.utility.DoubleTextField jTextFieldServicePrice;
    private com.hospital_os.utility.DoubleTextField jTextFieldServiceQty;
    private com.hospital_os.utility.DoubleTextField jTextFieldTotalDay;
    private javax.swing.JTextField jTextFieldUseUnit;
    protected javax.swing.JComboBox jcbLabOrderCause;
    private javax.swing.JLabel lblPregnancyCategory;
    private javax.swing.JLabel lblSticker;
    private javax.swing.JLabel lblStickerUnit;
    private javax.swing.JPanel panelCodeLabSecret;
    private javax.swing.JPanel panelEditPrice;
    private javax.swing.JPanel panelOrder;
    private javax.swing.JPanel panelPrintStickerQty;
    private javax.swing.JTextField txtQtyUnit;
    private sd.comp.jcalendar.JSpinField txtStickerQty;
    private javax.swing.JTextArea txtSupplement;
    private javax.swing.JTextField txtSupplySN;
    private com.hospital_os.utility.DoubleTextField txtUseDrugTotalDays;
    // End of variables declaration//GEN-END:variables

    @Override
    public void setEnabled(boolean b) {
        setEnabledOther(b);
        setEnabledDrug(b);
    }

    public void setEnabledOther(boolean b) {
        jButtonSaveOrderSupply.setEnabled(b);
        jTextFieldServicePrice.setEnabled(b);
        jTextFieldServiceQty.setEnabled(b);
        jButtonSaveOrderService.setEnabled(b);
        jTextFieldTotalDay.setEnabled(b);
        jTextFieldCodeLabSecret.setEnabled(b);
        jButtonSaveLab.setEnabled(b);
        jTextFieldCautionService.setEditable(b);
        txtQtyUnit.setEnabled(b);
        txtStickerQty.setEnabled(b);
    }

    private void setLanguage(String msg) {
        GuiLang.setLanguage(jButtonReturnDrug);
        GuiLang.setLanguage(jButtonSaveOrderDrug);
        GuiLang.setLanguage(jButtonSaveOrderService);
        GuiLang.setLanguage(jButtonSaveOrderSupply);
        GuiLang.setLanguage(jCheckBoxSpecialUsage);
        GuiLang.setLanguage(jLabel2);
        GuiLang.setLanguage(jLabel3);
        GuiLang.setLanguage(jLabel5);
        GuiLang.setLanguage(jLabel6);
        GuiLang.setLanguage(jLabel61);
        GuiLang.setLanguage(jLabelDoseShortcut);
        GuiLang.setLanguage(jLabelDrugInstruction);
        GuiLang.setLanguage(jLabelNote1);
        GuiLang.setLanguage(jLabelNote2);
        GuiLang.setLanguage(jLabelNote3);
        GuiLang.setLanguage(jLabelQty);
        GuiLang.setLanguage(jLabelQtyUnit);
        GuiLang.setLanguage(jLabelReturnDrug);
        GuiLang.setLanguage(jLabelServicePrice);
        GuiLang.setLanguage(this.jLabel1);
        GuiLang.setLanguage(this.jLabelQty1);

    }

    private void setEnableCardBlank(boolean a) {
        jLabelCause1.setVisible(a);
        this.jScrollPane3.setVisible(a);
        jLabelCause.setVisible(a);
        jLabelEmployee.setVisible(a);
        jLabelEmployee1.setVisible(a);
    }

    @Override
    public Item getItem() {
        return theItem;
    }

    public void setDrug(Drug drug) {
        theOrderItem.qty = drug.qty;
        theOrderItem.print_mar_type = drug.print_mar_type;

        Uom2 uom = theHC.theLookupControl.readUomById(drug.use_uom);
        DrugFrequency2 freq = theHC.theLookupControl.readDrugFrequencyById(drug.frequency);
        OrderItemDrug oid = theHO.initOrderItemDrug(drug, uom, freq);
        setObject(theOrderItem, oid, null);
    }

    @Override
    public int selectList(String id) {
        Drug drug = theHC.theOrderControl.readDrugById(id);
        setDrug(drug);
        return 1;
    }

    protected void doReturnDrugAction() {
        theHC.theOrderControl.saveReturnDrug(theOrderItem, jTextFieldQtyReturn.getText());
    }
    private static final Logger LOG = Logger.getLogger(PDOrder.class.getName());

    private void doUpdateCustomPrice() {
        theOrderItem.price = doubleTextFieldCustomPrice.getText();
        theHC.theOrderControl.saveOrderItemCustomPrice(theOrderItem);
    }

    private void doUpdateOrderStatus() {
        if (theOrderItem.isLab() || theOrderItem.isXray()) {
            String status = getJCheckBoxUrgent().isSelected() ? "1" : "0";
            theHC.theOrderControl.updateOrderItemUrgentStatus(null, status, theOrderItem, theHO.theVisit);
        } else if (theOrderItem.isDrug()) {
            theHC.theOrderControl.updateOrderItemUrgentStatus(getOrderItemDrug(), null, theOrderItem, theHO.theVisit);
        }
    }

    public void setEditableSupplyQty(boolean isEditable) {
        jTextFieldQty.setEditable(isEditable);
    }

    public void setEditableDrugQty(boolean isEditable) {
        jTextFieldTotalDay.setEditable(isEditable);
        txtQtyUnit.setEditable(isEditable);
    }

    private void setEnabledUrgent(boolean isVisitble) {
        jPanel4.setVisible(isVisitble);
        jPanel3.setVisible(isVisitble);
        jCheckBoxDrugStat.setVisible(isVisitble);
    }

    private javax.swing.JCheckBox getJCheckBoxUrgent() {
        if (jPanelLab.isVisible()) {
            return cbLabUrgent;
        } else {
            return cbBlankUrgent;
        }
    }

}

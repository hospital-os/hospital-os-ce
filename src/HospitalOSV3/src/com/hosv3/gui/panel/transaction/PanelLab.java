/*
 * PanelFpWoman.java
 *
 * Created on 19 ���Ҥ� 2546, 17:07 �.
 */
package com.hosv3.gui.panel.transaction;

import com.hospital_os.object.*;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hospital_os.utility.*;
import com.hosv3.control.HosControl;
import com.hosv3.gui.dialog.DialogLabReferOut;
import com.hosv3.gui.dialog.HosDialog;
import com.hosv3.object.HosObject;
import com.hosv3.subject.HosSubject;
import com.hosv3.usecase.transaction.ManageBillingResp;
import com.hosv3.usecase.transaction.ManageLabXrayResp;
import com.hosv3.usecase.transaction.ManageOrderResp;
import com.hosv3.usecase.transaction.ManagePatientResp;
import com.hosv3.usecase.transaction.ManageVisitResp;
import com.hosv3.utility.Constant;
import com.hosv3.utility.DateUtil;
import com.hosv3.utility.GuiLang;
import com.hosv3.utility.TableRenderer;
import java.awt.event.KeyEvent;
import java.util.Hashtable;
import java.util.Vector;

/**
 *
 * @author Administrator
 */
@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public class PanelLab extends javax.swing.JPanel
        implements ManageVisitResp,
        ManageOrderResp,
        ManageLabXrayResp,
        ManagePatientResp,
        ManageBillingResp {

    static final long serialVersionUID = 0;
    HosControl theHC;
    HosObject theHO;
    UpdateStatus theUS;
    HosSubject theHS;
    HosDialog theHD;
    public static int ROW_HIGH = 22;
    private Patient thePatient;
    private Visit theVisit;
    private Vector vOrderItemLab = new Vector();
//    private Vector vResultLab;
    private Vector visitHistoryV = new Vector();
//    private Vector vResultLabHistory = new Vector();
//    private Vector vLabResultItem = new Vector();
//    private int selectdetail = -1;
//    private boolean isSaveRePort;
    /**
     * �ʴ���� refer out ��������
     */
    private CelRendererLabReferOut celRendererLabReferOut = new CelRendererLabReferOut();
//    CelRendererLabReport celRendererLabReport = new CelRendererLabReport();
    private CellRendererDayOrder cellRendererDayOrder;
    private CellRendererHos dateRender = new CellRendererHos(CellRendererHos.DATE_TIME);
    private String[] col_jTableListOrder = {"����", "��觴�ǹ", "���ҷ�����", "OUT", "ʶҹ�"};
    private String[] col_jTableListOrderReport = {"����", "�觵�Ǩ�Ż", "�觼��Ż"};
    //String[] col_jTableDetailOrderLab = {"��¡��","�ѹ������","����","��","˹���","��һ���"};
    private String[] col_jTableDetailOrderLab = {"��¡��", "����", "��", "˹���", "��һ���"};
    private String[] jTableXrayHistory_col = {"Vn", "�ѹ���"};
//    private static final int FIELD_RESULT_INDEX = 2;
    private CellRendererHos vnRender = new CellRendererHos(CellRendererHos.VN);
    private final CellRendererHos UrgentStatus = new CellRendererHos(CellRendererHos.URGENY_STATUS);

    public PanelLab() {
        initComponents();
        jTableListOrderLab.requestFocus();
        setEnabled(false);
        //this.jButtonExecute1.setVisible(false);
    }

    public void setWrite(boolean b) {
        panelResultLabCur.setWrite(b);
    }

    public void setControl(HosControl hc, UpdateStatus us) {
        theUS = us;
        theHC = hc;
        theHO = hc.theHO;
        theHS = hc.theHS;
        theVisit = theHO.theVisit;
        thePatient = theHO.thePatient;
        vnRender = new CellRendererHos(CellRendererHos.VN, theHC.theLookupControl.getSequenceDataVN().pattern);
        theHS.theOrderSubject.attachManageOrder(this);
        theHS.theVisitSubject.attachManageVisit(this);
        theHS.theResultSubject.attachManageLab(this);
        theHS.thePatientSubject.attachManagePatient(this);
        theHS.theBillingSubject.attachManageBilling(this);
        cellRendererDayOrder = new CellRendererDayOrder(true);
//        jComboBoxEmployee.setControl(new EmployeeLookup(theHC.theSetupControl,theHC.theLookupControl),false);
        ComboboxModel.initComboBox(jComboBoxEmployee, theHC.theLookupControl.listLab());
        this.panelResultLabCur.setControl(theHC);
        this.panelResultLabHis.setControl(theHC);
        this.panelResultLabCur.setDateVisible(false);
        this.panelResultLabHis.setDateVisible(false);
        this.panelResultLabCur.setTableOrder(this.jTableListOrderLab);
        setLanguage(null);

        // set visible button
        btnViewLISResult.setVisible(theHC.theLookupControl.readOption().enable_lis_result_viewer.equals("1")
                && theHC.theHO.theGActionAuthV.isReadTabLabViewLISResultFile());
    }

    public void setDialog(HosDialog hd) {
        theHD = hd;
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel5 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jButtonAddOrderItemLab = new javax.swing.JButton();
        jButtonDelOrderItemLab = new javax.swing.JButton();
        jButtonReferOut = new javax.swing.JButton();
        jButtonCancelReferOut = new javax.swing.JButton();
        jComboBoxEmployee = new com.hosv3.gui.component.HosComboBox();
        jButtonExecute = new javax.swing.JButton();
        jButtonSortResult = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTableListOrderLab = new com.hosv3.gui.component.HJTableSort();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTextAreaNote = new javax.swing.JTextArea();
        jPanel1 = new javax.swing.JPanel();
        jButtonDelectQueueLab = new javax.swing.JButton();
        jButtonRemainLab = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jButtonSelectAll = new javax.swing.JButton();
        btnViewLISResult = new javax.swing.JButton();
        jButtonSaveLab = new javax.swing.JButton();
        jButtonPrintResultLab = new javax.swing.JButton();
        jButtonSendResult = new javax.swing.JButton();
        jButtonPrintResultLabHistory = new javax.swing.JButton();
        panelResultLabCur = new com.hosv3.gui.component.PanelResultLab();
        jPanel6 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jRadioButtonSelectAll = new javax.swing.JCheckBox();
        jButtonSearchHistory = new javax.swing.JButton();
        jPanel20 = new javax.swing.JPanel();
        jLabelTo1 = new javax.swing.JLabel();
        dateComboBoxHistoryStart = new com.hospital_os.utility.DateComboBox();
        dateComboBoxHistoryEnd = new com.hospital_os.utility.DateComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableHistory = new com.hosv3.gui.component.HJTableSort();
        panelResultLabHis = new com.hosv3.gui.component.PanelResultLab();

        setLayout(new java.awt.GridBagLayout());

        jTabbedPane1.setDoubleBuffered(true);
        jTabbedPane1.setFont(jTabbedPane1.getFont());
        jTabbedPane1.setMinimumSize(new java.awt.Dimension(480, 428));
        jTabbedPane1.setPreferredSize(new java.awt.Dimension(480, 496));
        jTabbedPane1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jTabbedPane1StateChanged(evt);
            }
        });

        jPanel5.setLayout(new java.awt.GridBagLayout());

        jPanel9.setLayout(new java.awt.GridBagLayout());

        jPanel2.setLayout(new java.awt.GridBagLayout());

        jButtonAddOrderItemLab.setFont(jButtonAddOrderItemLab.getFont());
        jButtonAddOrderItemLab.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/plus16.gif"))); // NOI18N
        jButtonAddOrderItemLab.setToolTipText("������¡���Ż");
        jButtonAddOrderItemLab.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonAddOrderItemLab.setMaximumSize(new java.awt.Dimension(26, 26));
        jButtonAddOrderItemLab.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonAddOrderItemLab.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonAddOrderItemLab.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAddOrderItemLabActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(jButtonAddOrderItemLab, gridBagConstraints);

        jButtonDelOrderItemLab.setFont(jButtonDelOrderItemLab.getFont());
        jButtonDelOrderItemLab.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/minus16.gif"))); // NOI18N
        jButtonDelOrderItemLab.setToolTipText("ź��¡���Ż");
        jButtonDelOrderItemLab.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonDelOrderItemLab.setMaximumSize(new java.awt.Dimension(26, 26));
        jButtonDelOrderItemLab.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonDelOrderItemLab.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonDelOrderItemLab.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDelOrderItemLabActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(jButtonDelOrderItemLab, gridBagConstraints);

        jButtonReferOut.setFont(jButtonReferOut.getFont());
        jButtonReferOut.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/refer_out.gif"))); // NOI18N
        jButtonReferOut.setToolTipText("�觵�Ǩ�Ż");
        jButtonReferOut.setMargin(new java.awt.Insets(2, 2, 2, 2));
        jButtonReferOut.setMaximumSize(new java.awt.Dimension(63, 26));
        jButtonReferOut.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonReferOut.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonReferOut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonReferOutActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(jButtonReferOut, gridBagConstraints);

        jButtonCancelReferOut.setFont(jButtonCancelReferOut.getFont());
        jButtonCancelReferOut.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/no_refer_out.gif"))); // NOI18N
        jButtonCancelReferOut.setToolTipText("¡��ԡ����觵�Ǩ�Ż");
        jButtonCancelReferOut.setMargin(new java.awt.Insets(2, 2, 2, 2));
        jButtonCancelReferOut.setMaximumSize(new java.awt.Dimension(63, 26));
        jButtonCancelReferOut.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonCancelReferOut.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonCancelReferOut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelReferOutActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(jButtonCancelReferOut, gridBagConstraints);

        jComboBoxEmployee.setDoubleBuffered(true);
        jComboBoxEmployee.setFont(jComboBoxEmployee.getFont());
        jComboBoxEmployee.setMinimumSize(new java.awt.Dimension(123, 30));
        jComboBoxEmployee.setPreferredSize(new java.awt.Dimension(123, 30));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(jComboBoxEmployee, gridBagConstraints);

        jButtonExecute.setFont(jButtonExecute.getFont());
        jButtonExecute.setText("���Թ���");
        jButtonExecute.setMargin(new java.awt.Insets(2, 7, 2, 7));
        jButtonExecute.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonExecuteActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(jButtonExecute, gridBagConstraints);

        jButtonSortResult.setFont(jButtonSortResult.getFont());
        jButtonSortResult.setText("�Ѵ���Ż");
        jButtonSortResult.setMargin(new java.awt.Insets(2, 7, 2, 7));
        jButtonSortResult.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSortResultActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(jButtonSortResult, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 0);
        jPanel9.add(jPanel2, gridBagConstraints);

        jScrollPane2.setMinimumSize(new java.awt.Dimension(224, 404));
        jScrollPane2.setPreferredSize(new java.awt.Dimension(224, 404));

        jTableListOrderLab.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTableListOrderLab.setFillsViewportHeight(true);
        jTableListOrderLab.setFont(jTableListOrderLab.getFont());
        jTableListOrderLab.setRowHeight(30);
        jTableListOrderLab.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTableListOrderLabMouseReleased(evt);
            }
        });
        jScrollPane2.setViewportView(jTableListOrderLab);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel9.add(jScrollPane2, gridBagConstraints);

        jScrollPane3.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane3.setMinimumSize(new java.awt.Dimension(100, 61));
        jScrollPane3.setPreferredSize(new java.awt.Dimension(100, 61));

        jTextAreaNote.setFont(jTextAreaNote.getFont());
        jTextAreaNote.setLineWrap(true);
        jTextAreaNote.setWrapStyleWord(true);
        jTextAreaNote.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextAreaNoteKeyReleased(evt);
            }
        });
        jScrollPane3.setViewportView(jTextAreaNote);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel9.add(jScrollPane3, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(11, 11, 0, 0);
        jPanel5.add(jPanel9, gridBagConstraints);

        jPanel1.setLayout(new java.awt.GridBagLayout());

        jButtonDelectQueueLab.setFont(jButtonDelectQueueLab.getFont());
        jButtonDelectQueueLab.setText("ź���");
        jButtonDelectQueueLab.setToolTipText("ź����Ż");
        jButtonDelectQueueLab.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDelectQueueLabActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(jButtonDelectQueueLab, gridBagConstraints);

        jButtonRemainLab.setFont(jButtonRemainLab.getFont());
        jButtonRemainLab.setText("��ҧ��");
        jButtonRemainLab.setToolTipText("��ҧ���Ż");
        jButtonRemainLab.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonRemainLabActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(jButtonRemainLab, gridBagConstraints);

        jPanel3.setLayout(new java.awt.GridBagLayout());

        jButtonSelectAll.setFont(jButtonSelectAll.getFont());
        jButtonSelectAll.setText("������");
        jButtonSelectAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSelectAllActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jButtonSelectAll, gridBagConstraints);

        btnViewLISResult.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/folderlab@24.png"))); // NOI18N
        btnViewLISResult.setMaximumSize(new java.awt.Dimension(28, 28));
        btnViewLISResult.setMinimumSize(new java.awt.Dimension(28, 28));
        btnViewLISResult.setPreferredSize(new java.awt.Dimension(28, 28));
        btnViewLISResult.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewLISResultActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(btnViewLISResult, gridBagConstraints);

        jButtonSaveLab.setFont(jButtonSaveLab.getFont());
        jButtonSaveLab.setText("�ѹ�֡");
        jButtonSaveLab.setToolTipText("�ѹ�֡���Ż");
        jButtonSaveLab.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSaveLabActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jButtonSaveLab, gridBagConstraints);

        jButtonPrintResultLab.setFont(jButtonPrintResultLab.getFont());
        jButtonPrintResultLab.setText("���������");
        jButtonPrintResultLab.setToolTipText("�������ź");
        jButtonPrintResultLab.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPrintResultLabActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jButtonPrintResultLab, gridBagConstraints);

        jButtonSendResult.setFont(jButtonSendResult.getFont());
        jButtonSendResult.setText("�觼�");
        jButtonSendResult.setToolTipText("�觼��Ż");
        jButtonSendResult.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSendResultActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jButtonSendResult, gridBagConstraints);

        jButtonPrintResultLabHistory.setText("�����������͹��ѧ");
        jButtonPrintResultLabHistory.setToolTipText("�����������͹��ѧ");
        jButtonPrintResultLabHistory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPrintResultLabHistoryActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jButtonPrintResultLabHistory, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        jPanel1.add(jPanel3, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(5, 11, 11, 11);
        jPanel5.add(jPanel1, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(11, 5, 0, 11);
        jPanel5.add(panelResultLabCur, gridBagConstraints);

        jTabbedPane1.addTab("��¡��", jPanel5);

        jPanel6.setLayout(new java.awt.GridBagLayout());

        jPanel7.setMinimumSize(new java.awt.Dimension(220, 438));
        jPanel7.setPreferredSize(new java.awt.Dimension(220, 438));
        jPanel7.setLayout(new java.awt.GridBagLayout());

        jRadioButtonSelectAll.setFont(jRadioButtonSelectAll.getFont());
        jRadioButtonSelectAll.setSelected(true);
        jRadioButtonSelectAll.setText("������");
        jRadioButtonSelectAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonSelectAllActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 0);
        jPanel7.add(jRadioButtonSelectAll, gridBagConstraints);

        jButtonSearchHistory.setFont(jButtonSearchHistory.getFont());
        jButtonSearchHistory.setText("����");
        jButtonSearchHistory.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonSearchHistory.setMaximumSize(new java.awt.Dimension(50, 24));
        jButtonSearchHistory.setMinimumSize(new java.awt.Dimension(50, 24));
        jButtonSearchHistory.setPreferredSize(new java.awt.Dimension(50, 24));
        jButtonSearchHistory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSearchHistoryActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 15, 0, 0);
        jPanel7.add(jButtonSearchHistory, gridBagConstraints);

        jPanel20.setLayout(new java.awt.GridBagLayout());

        jLabelTo1.setFont(jLabelTo1.getFont());
        jLabelTo1.setText("to");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 5);
        jPanel20.add(jLabelTo1, gridBagConstraints);

        dateComboBoxHistoryStart.setEnabled(false);
        dateComboBoxHistoryStart.setFont(dateComboBoxHistoryStart.getFont());
        dateComboBoxHistoryStart.setMinimumSize(new java.awt.Dimension(94, 24));
        dateComboBoxHistoryStart.setPreferredSize(new java.awt.Dimension(94, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 5);
        jPanel20.add(dateComboBoxHistoryStart, gridBagConstraints);

        dateComboBoxHistoryEnd.setEnabled(false);
        dateComboBoxHistoryEnd.setFont(dateComboBoxHistoryEnd.getFont());
        dateComboBoxHistoryEnd.setMinimumSize(new java.awt.Dimension(94, 24));
        dateComboBoxHistoryEnd.setPreferredSize(new java.awt.Dimension(94, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 3);
        jPanel20.add(dateComboBoxHistoryEnd, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel7.add(jPanel20, gridBagConstraints);

        jTableHistory.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTableHistory.setFillsViewportHeight(true);
        jTableHistory.setFont(jTableHistory.getFont());
        jTableHistory.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTableHistoryMouseReleased(evt);
            }
        });
        jScrollPane1.setViewportView(jTableHistory);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 0);
        jPanel7.add(jScrollPane1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.insets = new java.awt.Insets(11, 11, 11, 0);
        jPanel6.add(jPanel7, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(11, 11, 11, 11);
        jPanel6.add(panelResultLabHis, gridBagConstraints);

        jTabbedPane1.addTab("����ѵ�", jPanel6);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        add(jTabbedPane1, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonSortResultActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSortResultActionPerformed
        int[] a = jTableListOrderLab.getSelectedRows();
        theHC.theOrderControl.generateResultLab(vOrderItemLab, a);
        panelResultLabCur.setResultLabV(theHO.theVisit, vOrderItemLab);
    }//GEN-LAST:event_jButtonSortResultActionPerformed

    private void jTextAreaNoteKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextAreaNoteKeyReleased

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            theHC.theVitalControl.saveDxNote(jTextAreaNote.getText());
        }
    }//GEN-LAST:event_jTextAreaNoteKeyReleased

    private void jButtonSelectAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSelectAllActionPerformed
        jTableListOrderLab.selectAll();
        panelResultLabCur.setOrderSelectedAll();
    }//GEN-LAST:event_jButtonSelectAllActionPerformed

    private void jTableListOrderLabMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableListOrderLabMouseReleased
        if (jTableListOrderLab.getSelectedRows().length == 0) {
            panelResultLabCur.setOrderSelected(null);
            return;
        }
        if (jTableListOrderLab.getSelectedRows().length
                == jTableListOrderLab.getRowCount()) {
            panelResultLabCur.setOrderSelectedAll();
            return;
        }
        int row = jTableListOrderLab.getSelectedRow();
        OrderItem oi = (OrderItem) vOrderItemLab.get(row);
        this.panelResultLabCur.setOrderSelected(oi);

    }//GEN-LAST:event_jTableListOrderLabMouseReleased

    private void jButtonSendResultActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSendResultActionPerformed
        int[] a = jTableListOrderLab.getSelectedRows();
        panelResultLabCur.setResultLabV(theHO.theVisit, vOrderItemLab);
        theHC.theResultControl.sendDataResultLab(vOrderItemLab, this.panelResultLabCur.getResultLabV(), a);
    }//GEN-LAST:event_jButtonSendResultActionPerformed

    private void jButtonExecuteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonExecuteActionPerformed
        int[] a = jTableListOrderLab.getSelectedRows();
        String eid = this.jComboBoxEmployee.getText();
        theHC.theOrderControl.executeOrderItem(vOrderItemLab, a, eid, true);
    }//GEN-LAST:event_jButtonExecuteActionPerformed

    private void jButtonDelectQueueLabActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDelectQueueLabActionPerformed
        if (theHO.theVisit == null || theHO.theVisit.getObjectId() == null) {
            theUS.setStatus("�ѧ������͡������", UpdateStatus.WARNING);
            return;
        }
        if (!theUS.confirmBox("�س��ͧ��÷���ź������������ ?", UpdateStatus.WARNING)) {
            return;
        }
        theHC.theResultControl.deleteQueueLabByVisitID(theHO.theVisit.getObjectId());
    }//GEN-LAST:event_jButtonDelectQueueLabActionPerformed

    private void jTableHistoryMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableHistoryMouseReleased
        int select = jTableHistory.getSelectedRow();
        Visit visit = (Visit) visitHistoryV.get(select);
        setHisResultLabV(visit);
    }//GEN-LAST:event_jTableHistoryMouseReleased

    private void jRadioButtonSelectAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonSelectAllActionPerformed
        if (jRadioButtonSelectAll.isSelected()) {
            dateComboBoxHistoryStart.setEnabled(false);
            dateComboBoxHistoryEnd.setEnabled(false);
        } else {
            dateComboBoxHistoryStart.setEnabled(true);
            dateComboBoxHistoryEnd.setEnabled(true);
        }
    }//GEN-LAST:event_jRadioButtonSelectAllActionPerformed

    private void jButtonSearchHistoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSearchHistoryActionPerformed
        if (theHO.thePatient == null) {
            theUS.setStatus("��س����͡�����¡�͹���һ���ѵ�", UpdateStatus.WARNING);
            return;
        }
        String from = dateComboBoxHistoryStart.getText();
        String to = dateComboBoxHistoryEnd.getText();
        boolean all = jRadioButtonSelectAll.isSelected();
        visitHistoryV = theHC.theVisitControl.listVisitLabByDatePid(all, from, to, theHO.thePatient.getObjectId());
        setVisitHistoryV(visitHistoryV);
    }//GEN-LAST:event_jButtonSearchHistoryActionPerformed

    private void jTabbedPane1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jTabbedPane1StateChanged
    }//GEN-LAST:event_jTabbedPane1StateChanged

    private void jButtonRemainLabActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRemainLabActionPerformed
        int[] select = jTableListOrderLab.getSelectedRows();
        theHC.theResultControl.saveRemainResultLab(vOrderItemLab, select);
    }//GEN-LAST:event_jButtonRemainLabActionPerformed

    private void jButtonSaveLabActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSaveLabActionPerformed
        theHC.theResultControl.saveResultLab(vOrderItemLab, panelResultLabCur.getResultLabV(), theUS, jTableListOrderLab.getSelectedRows());
    }//GEN-LAST:event_jButtonSaveLabActionPerformed

    private void jButtonPrintResultLabActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPrintResultLabActionPerformed
        // ���¡ Dialog �ͧ������͡��¡�� Lab ���о����
        if (theHO.theVisit == null || theHO.theVisit.getObjectId() == null) {
            theUS.setStatus("�ѧ������͡������", UpdateStatus.WARNING);
            return;
        }
        theHD.showDialogSelectLabPrint(vOrderItemLab, theVisit, thePatient, panelResultLabCur.getResultLabV());
    }//GEN-LAST:event_jButtonPrintResultLabActionPerformed

    private void jButtonCancelReferOutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelReferOutActionPerformed
        int[] a = jTableListOrderLab.getSelectedRows();
        if (a.length == 0) {
            theUS.setStatus("�ѧ��������͡��¡�� Order", UpdateStatus.WARNING);
            return;
        }
        theHC.theOrderControl.referOutLab(vOrderItemLab, a, false);
    }//GEN-LAST:event_jButtonCancelReferOutActionPerformed

    private void jButtonReferOutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonReferOutActionPerformed
        if (jTableListOrderLab.getSelectedRow() == -1) {
            theUS.setStatus("�ѧ��������͡��¡�� Order", UpdateStatus.WARNING);
            return;
        }
        if (jTableListOrderLab.getSelectedRows().length == 0) {
            return;
        }
        int[] a = jTableListOrderLab.getSelectedRows();
        for (int i = 0; i < a.length; i++) {
            if (((OrderItem) vOrderItemLab.get(a[i])).status.equals(OrderStatus.REPORT)) {
                theUS.setStatus("��¡���Ż����§ҹ�������������ö�觵�Ǩ��", UpdateStatus.WARNING);
                return;
            }
        }
        theHC.theOrderControl.referOutLab(vOrderItemLab, a, true);
        DialogLabReferOut.showDialog(theHC, theUS, vOrderItemLab, a);
    }//GEN-LAST:event_jButtonReferOutActionPerformed

    private void jButtonDelOrderItemLabActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDelOrderItemLabActionPerformed
        int[] row = jTableListOrderLab.getSelectedRows();
        if (row.length == 0) {
            theUS.setStatus("�ѧ��������͡��¡�� Order", UpdateStatus.WARNING);
            return;
        }
        for (int i = 0; i < row.length; i++) {
            OrderItem oi = (OrderItem) vOrderItemLab.get(row[i]);
            if (!oi.vertifier.isEmpty() && !theHO.theEmployee.getObjectId().equals(oi.vertifier)) {
                theUS.setStatus("�������ö¡��ԡ��¡�� Order �������Ҩҡ�ش��ԡ�������", UpdateStatus.WARNING);
                return;
            }
        }
        Vector<OrderItem> v_selected = new Vector<OrderItem>();
        for (int i = 0; i < row.length; i++) {
            v_selected.add((OrderItem) vOrderItemLab.get(row[i]));
        }
        theHC.theOrderControl.cancelOrderItem(v_selected);
    }//GEN-LAST:event_jButtonDelOrderItemLabActionPerformed

    private void jButtonAddOrderItemLabActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAddOrderItemLabActionPerformed
        if (theHO.thePatient == null) {
            theUS.setStatus("�ѧ������͡������", UpdateStatus.WARNING);
            return;
        }
        if (theHO.theVisit == null || theHO.theVisit.getObjectId() == null) {
            theUS.setStatus("�ѧ������͡������", UpdateStatus.WARNING);
            return;
        }
        if (theVisit.is_discharge_money.equals("1")) {
            theUS.setStatus("�����¨�˹��·ҧ����Թ���� �������ö������¡���Ż��", UpdateStatus.WARNING);
            return;
        }
        if (theHO.isLockingByOther()) {
            theUS.setStatus("�����¶١���˹�Ҥ��������ԡ������ �������ö������¡���Ż��", UpdateStatus.WARNING);
            return;
        }
        if (!"".equals(this.theHO.orderSecret)) {
            theUS.setStatus("���������Ż���Դ �������ö������¡���Ż��", UpdateStatus.WARNING);
            return;
        }
        theHD.showDialogOrderItemLabByLab(vOrderItemLab, theVisit);
    }//GEN-LAST:event_jButtonAddOrderItemLabActionPerformed

    private void btnViewLISResultActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewLISResultActionPerformed
        theHD.showDialogLISResult();
    }//GEN-LAST:event_btnViewLISResultActionPerformed

    private void jButtonPrintResultLabHistoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPrintResultLabHistoryActionPerformed
        theHD.showDialogHistoryLab(theHO.theVisit);
    }//GEN-LAST:event_jButtonPrintResultLabHistoryActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnViewLISResult;
    private com.hospital_os.utility.DateComboBox dateComboBoxHistoryEnd;
    private com.hospital_os.utility.DateComboBox dateComboBoxHistoryStart;
    private javax.swing.JButton jButtonAddOrderItemLab;
    private javax.swing.JButton jButtonCancelReferOut;
    private javax.swing.JButton jButtonDelOrderItemLab;
    private javax.swing.JButton jButtonDelectQueueLab;
    private javax.swing.JButton jButtonExecute;
    private javax.swing.JButton jButtonPrintResultLab;
    private javax.swing.JButton jButtonPrintResultLabHistory;
    private javax.swing.JButton jButtonReferOut;
    private javax.swing.JButton jButtonRemainLab;
    private javax.swing.JButton jButtonSaveLab;
    private javax.swing.JButton jButtonSearchHistory;
    private javax.swing.JButton jButtonSelectAll;
    private javax.swing.JButton jButtonSendResult;
    private javax.swing.JButton jButtonSortResult;
    private com.hosv3.gui.component.HosComboBox jComboBoxEmployee;
    private javax.swing.JLabel jLabelTo1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JCheckBox jRadioButtonSelectAll;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTabbedPane jTabbedPane1;
    private com.hosv3.gui.component.HJTableSort jTableHistory;
    private com.hosv3.gui.component.HJTableSort jTableListOrderLab;
    private javax.swing.JTextArea jTextAreaNote;
    private com.hosv3.gui.component.PanelResultLab panelResultLabCur;
    private com.hosv3.gui.component.PanelResultLab panelResultLabHis;
    // End of variables declaration//GEN-END:variables

    /**
     * ૵������ҧ�
     */
    @Override
    public void setEnabled(boolean b) {
        jButtonRemainLab.setEnabled(b);
        this.jButtonSendResult.setEnabled(b);
        jButtonDelectQueueLab.setEnabled(b);
        jButtonSaveLab.setEnabled(b);
        jButtonReferOut.setEnabled(b);
        jButtonDelOrderItemLab.setEnabled(b);
        jButtonCancelReferOut.setEnabled(b);
        jButtonPrintResultLab.setEnabled(b);
        jButtonPrintResultLabHistory.setEnabled(b);
        jButtonAddOrderItemLab.setEnabled(b);
        jButtonExecute.setEnabled(b);
    }

    //��㹡�� refresh ��¡�� order ����ա������¹�ŧ
    //���ռ�����ҧ order �˹�Ҩ� lab ��ҹ��
    private void setOrderItemLabV(Visit visit) {
        if (visit == null) {
            setOrderItemLabV(new Vector());
            return;
        }
        Vector vc = theHC.theOrderControl.listOrderLabByVNAndSecret(visit.getObjectId());
        setOrderItemLabV(vc);
    }

    private void setHisResultLabV(Visit visit) {
        if (visit == null) {
            this.panelResultLabHis.setResultLabV(new Vector());
            return;
        }
        Vector vc = theHC.theOrderControl.listOrderLabByVNAndSecret(visit.getObjectId());
        this.panelResultLabHis.setResultLabV(visit, vc);
    }

    private void setResultLabV(Vector order_v) {
        //henbe:18/03/2549
        this.panelResultLabCur.setResultLabV(theHO.theVisit, order_v);
    }

    ////////////////////////////////////////////////////////////////////////////
    /**
     * ૵Visit 㹵��ҧ History
     */
    private void setVisitHistoryV(Vector v) {
        this.visitHistoryV = v;
        TaBleModel tm;
        if (v == null || v.isEmpty()) {
            tm = new TaBleModel(jTableXrayHistory_col, 0);
        } else {
            tm = new TaBleModel(jTableXrayHistory_col, v.size());
            int size = v.size();
            for (int i = 0; i < size; i++) {
                Visit vi = (Visit) v.get(i);
                tm.setValueAt(vi.vn, i, 0);
                tm.setValueAt(DateUtil.getDateFromText(vi.begin_visit_time), i, 1);
            }
        }
        jTableHistory.setModel(tm);
        jTableHistory.getColumnModel().getColumn(0).setPreferredWidth(15);
        jTableHistory.getColumnModel().getColumn(0).setCellRenderer(TableRenderer.getRendererCenter());
        jTableHistory.getColumnModel().getColumn(0).setCellRenderer(vnRender);
        jTableHistory.getColumnModel().getColumn(1).setCellRenderer(dateRender);
    }

    /**
     * ���ҧ���ҧ㹡���ʴ� LabOrderItem ����ѧ����ա����§ҹ��
     */
    private void setOrderItemLabV(Vector order_v) {
        vOrderItemLab = order_v;
        TaBleModel tm;
        if (vOrderItemLab == null || vOrderItemLab.isEmpty()) {
            tm = new TaBleModel(col_jTableListOrder, 0);
            jTableListOrderLab.setModel(tm);
            return;
        }
        tm = new TaBleModel(col_jTableListOrder, vOrderItemLab.size());
        int vOrderItemLab_size = 0;
        if (vOrderItemLab != null) {
            vOrderItemLab_size = vOrderItemLab.size();
        }
        for (int i = 0; i < vOrderItemLab_size; i++) {
            OrderItem oi = (OrderItem) vOrderItemLab.get(i);
            boolean isUrgent = oi.order_urgent_status.equals("1");
            Hashtable ht = new Hashtable();
            ht.put("OrderItem", oi);
            ht.put("String", "");
            String ttt = theHC.theOrderControl.getTTTRenderDayOrder(oi, "");
            ht.put("display_string", ttt);
            tm.setValueAt(ht, i, 0);
            if (isUrgent) {
                tm.setValueAt(ListTransfer.SHOW_URGENT, i, 1);
            }
            String day_verify = DateUtil.getDateToStringShort(DateUtil.getDateFromText(oi.vertify_time), true);
            tm.setValueAt(day_verify, i, 2);
            tm.setValueAt(oi.refer_out, i, 3);
            if (oi.status.equals(OrderStatus.VERTIFY)) {
                tm.setValueAt(Constant.getTextBundle("�׹�ѹ"), i, 4);
            } else if (oi.status.equals(OrderStatus.EXECUTE)) {
                tm.setValueAt(Constant.getTextBundle("���Թ���"), i, 4);
            } else if (oi.status.equals(OrderStatus.REPORT)) {
                tm.setValueAt(Constant.getTextBundle("��§ҹ��"), i, 4);
            } else if (oi.status.equals(OrderStatus.REMAIN)) {
                tm.setValueAt(Constant.getTextBundle("��ҧ��"), i, 4);
            }
        }
        //�繡�á�˹���� table ����¡�÷��١���͡����͹���� set vector �����������
        //��Шзӡ������Ң����Ź���������������Ҷ����������ǡ�����͡��¡���á᷹
        int current_row = -1;
        if (jTableListOrderLab.getSelectedRows().length == 1) {
            current_row = jTableListOrderLab.getSelectedRow();
        }
        boolean select_all = jTableListOrderLab.getRowCount() > 0
                && jTableListOrderLab.getSelectedRows().length == jTableListOrderLab.getRowCount();

        jTableListOrderLab.setModel(tm);

        jTableListOrderLab.setRowHeight(ROW_HIGH);
        //jTableListOrderLab.selectAll();
        jTableListOrderLab.getColumnModel().getColumn(0).setPreferredWidth(100); // name
        jTableListOrderLab.getColumnModel().getColumn(0).setCellRenderer(cellRendererDayOrder);
        jTableListOrderLab.getColumnModel().getColumn(1).setPreferredWidth(15); // UrgentStatus
        jTableListOrderLab.getColumnModel().getColumn(1).setCellRenderer(UrgentStatus);
        jTableListOrderLab.getColumnModel().getColumn(2).setPreferredWidth(65); // name
        jTableListOrderLab.getColumnModel().getColumn(3).setPreferredWidth(30); // referout
        jTableListOrderLab.getColumnModel().getColumn(3).setCellRenderer(celRendererLabReferOut);
        jTableListOrderLab.getColumnModel().getColumn(4).setPreferredWidth(35);
        if (current_row != -1 && current_row < jTableListOrderLab.getRowCount()) {
            jTableListOrderLab.setRowSelectionInterval(current_row, current_row);
            jTableListOrderLabMouseReleased(null);
        } else if (select_all) {
            jTableListOrderLab.selectAll();
            panelResultLabCur.setOrderSelectedAll();
        } else {
            jTableListOrderLab.clearSelection();
            panelResultLabCur.setOrderSelected(null);
        }

    }

    /**
     * ��Ǩ�ͺ���Visit ���Ƿӡ��૵������ҧ��
     */
    private boolean setVisit(Visit v) {
        jTabbedPane1.setSelectedIndex(0);
        theVisit = v;
        if (theVisit == null) {
            clearGui();
            setEnabled(false);
            return false;
        }
        if (theVisit != null) {
            this.jTextAreaNote.setText(theVisit.diagnosis_note);
        }
        setEnabled(true);
        if (theHO.theEmployee.authentication_id.equals(Authentication.LAB)) {
            ComboboxModel.setCodeComboBox(jComboBoxEmployee, theHO.theEmployee.getObjectId());
        }
        return true;
    }

    public void setLanguage(String msg) {
        GuiLang.setLanguage(this);
        GuiLang.setLanguage(jTabbedPane1);
        GuiLang.setLanguage(col_jTableListOrder);
        GuiLang.setLanguage(col_jTableListOrderReport);
        GuiLang.setLanguage(col_jTableDetailOrderLab);
        GuiLang.setLanguage(jTableXrayHistory_col);
        GuiLang.setLanguage(jButtonSendResult);
        GuiLang.setLanguage(jButtonRemainLab);
        GuiLang.setLanguage(jButtonExecute);
        GuiLang.setLanguage(jButtonDelectQueueLab);
        GuiLang.setLanguage(jButtonPrintResultLab);
        GuiLang.setLanguage(jButtonCancelReferOut);
        GuiLang.setLanguage(jButtonSaveLab);
        GuiLang.setLanguage(jButtonReferOut);
        GuiLang.setLanguage(jButtonAddOrderItemLab);
        GuiLang.setLanguage(jButtonDelOrderItemLab);
        GuiLang.setLanguage(jButtonSearchHistory);
        GuiLang.setLanguage(jRadioButtonSelectAll);
        GuiLang.setLanguage(jButtonSortResult);
        GuiLang.setLanguage(jButtonSelectAll);
    }

    /**
     * ��觡��蹹��зӧҹ�ѵ��ѵ�������ա�����¡��������ʴ������ż�����
     *
     * @param str
     * @param status ʶҹ� str ��ͤ���
     * @return-
     * @author modify by kingland
     * @date 16/08/2549
     */
    @Override
    public void notifyReadVisit(String str, int status) {
        //����������š�͹�ӡ����������������
        String old_patient = "";
        if (theVisit != null) {
            old_patient = theVisit.patient_id;
        }

        setVisit(theHO.theVisit);
        setOrderItemLabV(theHO.theVisit);
        setResultLabV(vOrderItemLab);
        jTableListOrderLab.requestFocus();

        if (theVisit != null
                && theHO.theVisit != null
                && !old_patient.equals(theHO.theVisit.patient_id)) {
            Vector vc = theHC.theVisitControl.listVisitLabByDatePid(
                    true, "", "", theHO.thePatient.getObjectId());
            setVisitHistoryV(vc);
        }
    }

    @Override
    public void notifyCheckDoctorTreament(String str, int status) {
    }

    @Override
    public void notifyManagePatientLabReferIn(String str, int status) {
        setEnabled(true);
    }

    @Override
    public void notifyObservVisit(String str, int status) {
    }

    @Override
    public void notifyUnlockVisit(String str, int status) {
        clearGui();
    }

    @Override
    public void notifyVisitPatient(String str, int status) {
    }

    @Override
    public void notifyAdmitVisit(String str, int status) {
    }

    @Override
    public void notifyReceiveReturnDrug(String str, int status) {
    }

    @Override
    public void notifyCancelOrderItem(String str, int status) {
        if (theHO.theVisit != null) {
            setVisit(theHO.theVisit);
            setOrderItemLabV(theHO.theVisit);
            setResultLabV(vOrderItemLab);
            jTableListOrderLab.requestFocus();
        }
    }

    @Override
    public void notifyCheckAutoOrder(String str, int status) {
    }

    @Override
    public void notifyDoctorOffDrug(String DoctorId, int status) {
    }

    @Override
    public void notifySaveOrderItem(String str, int status) {
        setOrderItemLabV(theHO.theVisit);
    }

    @Override
    public void notifySaveOrderRequest(String str, int status) {
    }

    @Override
    public void notifyDropVisit(String str, int status) {
    }

    @Override
    public void notifySendVisit(String str, int status) {
    }

    @Override
    public void notifyDischargeFinancial(String str, int status) {
    }

    @Override
    public void notifyReverseFinancial(String str, int status) {
    }

    @Override
    public void notifyReverseDoctor(String str, int status) {
    }

    @Override
    public void notifySaveLabResult(String str, int status) {
        setOrderItemLabV(theHO.theVisit);
        setResultLabV(vOrderItemLab);
    }

    @Override
    public void notifySaveFilmXray(String str, int status) {
    }

    @Override
    public void notifyDeleteVisitPayment(String str, int status) {
    }

    @Override
    public void notifyDeleteFilmXray(String str, int status) {
    }

    @Override
    public void notifyDeleteXrayPosition(String str, int status) {
    }

    @Override
    public void notifySaveResultXray(String str, int status) {
    }

    @Override
    public void notifySaveXrayPosition(String str, int status) {
    }

    @Override
    public void notifyXrayReportComplete(String str, int status) {
    }

    @Override
    public void notifyDeleteLabOrder(String msg, int status) {
        setOrderItemLabV(theHO.theVisit);
        setResultLabV(vOrderItemLab);
        jTableListOrderLab.requestFocus();
    }

    @Override
    public void notifyDeleteLabResult(String str, int status) {
        setOrderItemLabV(theHO.theVisit);
        setResultLabV(vOrderItemLab);
    }

    @Override
    public void notifyExecuteOrderItem(String str, int status) {
        setOrderItemLabV(theHO.theVisit);
        setResultLabV(vOrderItemLab);
    }

    @Override
    public void notifyContinueOrderItem(String str, int status) {
    }

    @Override
    public void notifyDispenseOrderItem(String str, int status) {
    }

    @Override
    public void notifySaveOrderItemInLab(String str, int status) {
        setOrderItemLabV(theHO.theVisit);
        setResultLabV(vOrderItemLab);
    }

    @Override
    public void notifyReferOutLab(String msg, int status) {
        setOrderItemLabV(theHO.theVisit);
    }

    @Override
    public void notifyDischargeDoctor(String str, int status) {
    }

    @Override
    public void notifyRemainDoctorDischarge(String str, int status) {
    }

    @Override
    public void notifySendVisitBackWard(String str, int status) {
    }

    @Override
    public void notifySaveReturnDrug(String str, int status) {
    }

    @Override
    public void notifyReportResultLab(String str, int status) {
        clearGui();
    }

    @Override
    public void notifyDeleteResultXray(String str, int status) {
    }

    @Override
    public void notifyVerifyOrderItem(String str, int status) {
        setOrderItemLabV(theHO.theVisit);
        setResultLabV(vOrderItemLab);
        jTableListOrderLab.requestFocus();
    }

    @Override
    public void notifyAddLabReferOut(String str, int status) {
    }

    @Override
    public void notifyAddLabReferIn(String str, int status) {
    }

    @Override
    public void notifyReverseAdmit(String str, int status) {
    }

    @Override
    public void notifySaveRemainLabResult(String str, int status) {
        setOrderItemLabV(theHO.theVisit);
    }

    @Override
    public void notifySendResultLab(String str, int status) {
        setVisit(theHO.theVisit);
        setOrderItemLabV(theHO.theVisit);
        setResultLabV(vOrderItemLab);
    }

    public void clearGui() {
        setOrderItemLabV(new Vector());
        this.panelResultLabCur.setResultLabV(new Vector());
        this.panelResultLabHis.setResultLabV(new Vector());
        setVisitHistoryV(new Vector());
        setEnabled(false);
        if (theHO.theEmployee.authentication_id.equals(Authentication.LAB)) {
            ComboboxModel.setCodeComboBox(jComboBoxEmployee, theHO.theEmployee.getObjectId());
        }
//        vResultLabHistory = null;
//        vResultLab = null;
    }

    @Override
    public void notifyDeleteQueueLab(String str, int status) {
        clearGui();
    }

    @Override
    public void notifySavePatient(String str, int status) {
    }

    @Override
    public void notifyResetPatient(String str, int status) {
    }

    @Override
    public void notifyDeletePatient(String str, int status) {
    }

    @Override
    public void notifyReadPatient(String str, int status) {
        //����������š�͹�ӡ����������������
        setVisit(theHO.theVisit);
        setOrderItemLabV(theHO.theVisit);
        setResultLabV(new Vector());
        if (theHO.thePatient != null) {
            Vector vc = theHC.theVisitControl.listVisitLabByDatePid(
                    true, "", "", theHO.thePatient.getObjectId());
            setVisitHistoryV(vc);
        }
    }

    @Override
    public void notifySaveAppointment(String str, int status) {
    }

    @Override
    public void notifyManageDrugAllergy(String str, int status) {
    }

    @Override
    public void notifySavePatientPayment(String str, int status) {
    }

    @Override
    public void notifyDeletePatientPayment(String str, int status) {
    }

    @Override
    public void notifyReadFamily(String str, int status) {
    }

    @Override
    public void notifyAdmitLeaveHome(String str, int status) {
    }

    @Override
    public void notifySaveInformIntolerance(Object... object) {
        //
    }

    @Override
    public void notifyCancelBillingInvoice(String str, int status) {
    }

    @Override
    public void notifyCancelBill(String str, int status) {
    }

    @Override
    public void notifyCalculateAllBillingInvoice(String str, int status) {
    }

    @Override
    public void notifyPatientPaidMoney(String str, int status) {
    }

    @Override
    public void notifyBillingInvoice(String str, int status) {
        setOrderItemLabV(theHO.theVisit);
        setResultLabV(vOrderItemLab);
        jTableListOrderLab.requestFocus();
    }

    @Override
    public void notifyUpdatePatientPictureProfile(Object obj) {
    }
}

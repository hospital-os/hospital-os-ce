/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class Language extends Persistent {

    public String description = "";
    public String active = "1";

    @Override
    public String toString() {
        return description;

    }
}
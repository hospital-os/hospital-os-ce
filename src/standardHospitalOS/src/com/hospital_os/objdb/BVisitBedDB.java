/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.BVisitBed;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class BVisitBedDB {

    public ConnectionInf connectionInf;
    final public String tableId = "852";

    public BVisitBedDB(ConnectionInf db) {
        connectionInf = db;
    }

    public int insert(BVisitBed obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO b_visit_bed(\n"
                    + "            b_visit_bed_id, b_visit_ward_id, sequences, bed_number,\n"
                    + "            active, user_record,user_modify)\n"
                    + "    VALUES (?, ?, ?, ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.getGenID(tableId));
            preparedStatement.setString(index++, obj.b_visit_ward_id);
            preparedStatement.setInt(index++, obj.sequences);
            preparedStatement.setString(index++, obj.bed_number);
//            preparedStatement.setArray(index++, connectionInf.getConnection().createArrayOf("varchar", obj.continue_b_item_ids));
            preparedStatement.setString(index++, obj.active);
            preparedStatement.setString(index++, obj.user_record);
            preparedStatement.setString(index++, obj.user_modify);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int updateInfo(BVisitBed obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE b_visit_bed\n"
                    + "   SET sequences=?, \n"
                    + "       bed_number=?, active=?, modify_date_time=current_timestamp, user_modify=?\n"
                    + " WHERE b_visit_bed_id=?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setInt(index++, obj.sequences);
            preparedStatement.setString(index++, obj.bed_number);
            preparedStatement.setString(index++, obj.active);
            preparedStatement.setString(index++, obj.user_modify);
            preparedStatement.setString(index++, obj.getObjectId());
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int updateOrder(BVisitBed obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE b_visit_bed\n"
                    + "   SET continue_b_item_ids=?, modify_date_time=current_timestamp, user_modify=?\n"
                    + " WHERE b_visit_bed_id=?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setArray(index++, connectionInf.getConnection().createArrayOf("varchar", obj.continue_b_item_ids));
            preparedStatement.setString(index++, obj.user_modify);
            preparedStatement.setString(index++, obj.getObjectId());
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int updateSeq(BVisitBed obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "UPDATE b_visit_bed\n"
                    + "   SET sequences= ?\n"
                    + " WHERE b_visit_bed_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setInt(index++, obj.sequences);
            preparedStatement.setString(index++, obj.getObjectId());
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(BVisitBed obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM b_visit_bed\n");
            sql.append(" WHERE b_visit_bed_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public BVisitBed selectById(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_visit_bed\n"
                    + "where b_visit_bed_id = ?\n";
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, id);
            List<BVisitBed> executeQuery = executeQuery(preparedStatement);
            return executeQuery.isEmpty() ? null : executeQuery.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<BVisitBed> listIntervalSeq(String wardId, int seqStart, int seqEnd) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * \n"
                    + "from b_visit_bed\n"
                    + "where\n"
                    + "        b_visit_bed.active = '1'\n"
                    + "        and b_visit_bed.b_visit_ward_id = ?\n"
                    + "        and sequences >= ? and sequences <= ? \n"
                    + "order by\n"
                    + "        b_visit_bed.sequences asc";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, wardId);
            preparedStatement.setInt(2, seqStart);
            preparedStatement.setInt(3, seqEnd);
            List<BVisitBed> list = executeQuery(preparedStatement);
            return list;
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<BVisitBed> selectByWardId(String wardId, boolean isShowAll) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_visit_bed \n"
                    + "where b_visit_ward_id = ? ";
            if (!isShowAll) {
                sql += " and active = '1' ";
            }
            sql += "order by sequences";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, wardId);
            List<BVisitBed> list = executeQuery(preparedStatement);
            return list;
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<BVisitBed> listAvailabalByWardId(String wardId) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * \n"
                    + "from b_visit_bed\n"
                    + "where\n"
                    + "        b_visit_bed.active = '1'\n"
                    + "        and b_visit_bed.b_visit_ward_id = ?\n"
                    + "        and b_visit_bed.b_visit_bed_id not in (select\n"
                    + "         t_visit_bed.b_visit_bed_id from t_visit_bed where t_visit_bed.active = '1' and t_visit_bed.current_bed = '1')\n"
                    + "order by\n"
                    + "        b_visit_bed.sequences asc";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, wardId);
            List<BVisitBed> list = executeQuery(preparedStatement);
            return list;
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<BVisitBed> listByActiveSeq(int seq, String wardId, String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_visit_bed\n"
                    + "where b_visit_bed.active = '1'\n"
                    + "and b_visit_bed.b_visit_ward_id = ?\n"
                    + "and sequences  = ?\n";
            if (id != null && !id.isEmpty()) {
                sql += "and b_visit_bed_id  <> ?\n";
            }
            sql += "order by bed_number";
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, wardId);
            preparedStatement.setInt(index++, seq);
            if (id != null && !id.isEmpty()) {
                preparedStatement.setString(index++, id);
            }
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<BVisitBed> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<BVisitBed> list = new ArrayList<BVisitBed>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                BVisitBed obj = new BVisitBed();
                obj.setObjectId(rs.getString("b_visit_bed_id"));
                obj.sequences = rs.getInt("sequences");
                obj.bed_number = rs.getString("bed_number");
                obj.b_visit_ward_id = rs.getString("b_visit_ward_id");
                obj.continue_b_item_ids = (String[]) rs.getArray("continue_b_item_ids").getArray();
                obj.active = rs.getString("active");
                obj.record_date_time = rs.getTimestamp("record_date_time");
                obj.user_record = rs.getString("user_record");
                obj.modify_date_time = rs.getTimestamp("modify_date_time");
                obj.user_modify = rs.getString("user_modify");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }
}

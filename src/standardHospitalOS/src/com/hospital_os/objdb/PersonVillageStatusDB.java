/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.PersonVillageStatus;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */@SuppressWarnings("ClassWithoutLogger")
public class PersonVillageStatusDB {
    private final ConnectionInf connectionInf;

    public PersonVillageStatusDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public List<PersonVillageStatus> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_person_village_status";
            preparedStatement = connectionInf.ePQuery(sql);
            
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<PersonVillageStatus> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<PersonVillageStatus> list = new ArrayList<PersonVillageStatus>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                PersonVillageStatus obj = new PersonVillageStatus();
                obj.setObjectId(rs.getString("f_person_village_status_id"));
                obj.description = rs.getString("person_village_status_description");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (PersonVillageStatus obj : list()) {
            list.add((CommonInf) obj);            
        }
        return list;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.OrderReturn;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tanakrit
 */
public class OrderReturnDB {

    public ConnectionInf theConnectionInf;
    final public String idTable = "959";

    public OrderReturnDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public int insert(OrderReturn p) throws Exception {
        String sql = "INSERT INTO t_order_return( \n"
                + "               t_order_return_id, t_visit_id, t_order_id, b_item_id, f_item_group_id, \n"
                + "               order_common_name, dispense_qty, return_qty, receive_qty, \n"
                + "               user_return_id, receiver_date_time, user_receiver_id) \n"
                + "        VALUES(?, ?, ?, ?, ?, \n"
                + "               ?, ?, ?, ?, \n"
                + "               ?, ?, ?) \n";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, p.getGenID(idTable));
            ePQuery.setString(index++, p.t_visit_id);
            ePQuery.setString(index++, p.t_order_id);
            ePQuery.setString(index++, p.b_item_id);
            ePQuery.setString(index++, p.f_item_group_id);

            ePQuery.setString(index++, p.order_common_name);
            ePQuery.setDouble(index++, Double.parseDouble(p.dispense_qty.isEmpty() ? "0" : p.dispense_qty));
            ePQuery.setDouble(index++, Double.parseDouble(p.return_qty.isEmpty() ? "0" : p.return_qty));

            if (p.receive_qty.isEmpty()) {
                ePQuery.setNull(index++, java.sql.Types.NULL);
            } else {
                ePQuery.setDouble(index++, Double.parseDouble(p.receive_qty));
            }

            ePQuery.setString(index++, p.user_return_id);
            ePQuery.setTimestamp(index++, p.receiver_date_time != null ? new java.sql.Timestamp(p.receiver_date_time.getTime()) : null);
            ePQuery.setString(index++, p.user_receiver_id);
            return ePQuery.executeUpdate();
        }
    }

    public int update(OrderReturn p) throws Exception {
        String sql = "UPDATE t_order_return \n"
                + "      SET t_visit_id=?, t_order_id=?, b_item_id=?, f_item_group_id=?, "
                + "          order_common_name=?, dispense_qty=?, return_qty=?, receive_qty=? \n"
                + "        , return_date_time=current_timestamp, user_return_id=?, receiver_date_time=?, user_receiver_id=? \n"
                + "WHERE t_order_return_id = ?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;

            ePQuery.setString(index++, p.t_visit_id);
            ePQuery.setString(index++, p.t_order_id);
            ePQuery.setString(index++, p.b_item_id);
            ePQuery.setString(index++, p.f_item_group_id);
            ePQuery.setString(index++, p.order_common_name);
            ePQuery.setDouble(index++, Double.parseDouble(p.dispense_qty.isEmpty() ? "0" : p.dispense_qty));
            ePQuery.setDouble(index++, Double.parseDouble(p.return_qty.isEmpty() ? "0" : p.return_qty));
            if (p.receive_qty.isEmpty()) {
                ePQuery.setNull(index++, java.sql.Types.NULL);
            } else {
                ePQuery.setDouble(index++, Double.parseDouble(p.receive_qty));
            }
            ePQuery.setString(index++, p.user_return_id);
            ePQuery.setTimestamp(index++, p.receiver_date_time != null ? new java.sql.Timestamp(p.receiver_date_time.getTime()) : null);
            ePQuery.setString(index++, p.user_receiver_id);
            ePQuery.setString(index++, p.getObjectId());
            return ePQuery.executeUpdate();
        }
    }

    public int delete(OrderReturn o) throws Exception {
        String sql = "delete from t_order_return where t_order_return_id =?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            ePQuery.setString(1, o.getObjectId());
            return ePQuery.executeUpdate();
        }
    }

    public OrderReturn selectNotReceiveByOrderItemId(String orderItemId) throws Exception {
        String sql = "select * from t_order_return where t_order_id = ? and receive_qty ='0' ";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            ePQuery.setString(1, orderItemId);
            List<OrderReturn> list = executeQuery(ePQuery);
            return list.isEmpty() ? null : list.get(0);
        }
    }

    public List<OrderReturn> selectOrderReturnByVId(String visitId) throws Exception {
        String sql = "select * from t_order_return where t_visit_id = ?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            ePQuery.setString(1, visitId);
            return executeQuery(ePQuery);
        }
    }

    public OrderReturn selectByOrderItemId(String orderItemId) throws Exception {
        String sql = "select * from t_order_return where t_order_id = ?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            ePQuery.setString(1, orderItemId);
            List<OrderReturn> list = executeQuery(ePQuery);
            return list.isEmpty() ? null : list.get(0);
        }
    }

    public OrderReturn selectByPK(String pk) throws Exception {
        String sql = "select * from t_order_return where t_order_return_id = ?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            ePQuery.setString(1, pk);
            List<OrderReturn> list = executeQuery(ePQuery);
            return list.isEmpty() ? null : list.get(0);
        }
    }

    public List<OrderReturn> queryOIRD(String orderItemId) throws Exception {
        String sql = "select * from t_order_return where t_order_id = ?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            ePQuery.setString(1, orderItemId);
            return executeQuery(ePQuery);
        }
    }

    public List<OrderReturn> executeQuery(PreparedStatement preparedStatement) throws Exception {
        OrderReturn p;
        List<OrderReturn> list = new ArrayList<>();
        try (ResultSet rs = preparedStatement.executeQuery()) {
            while (rs.next()) {
                p = new OrderReturn();
                p.setObjectId(rs.getString("t_order_return_id"));
                p.t_visit_id = rs.getString("t_visit_id");
                p.t_order_id = rs.getString("t_order_id");
                p.b_item_id = rs.getString("b_item_id");
                p.f_item_group_id = rs.getString("f_item_group_id");
                p.order_common_name = rs.getString("order_common_name");
                p.dispense_qty = String.valueOf(rs.getDouble("dispense_qty"));
                p.return_qty = String.valueOf(rs.getDouble("return_qty"));
                p.receive_qty = String.valueOf(rs.getDouble("receive_qty")).equals("0.0")
                        ? ""
                        : String.valueOf(rs.getDouble("receive_qty"));
                p.receiver_date_time = rs.getTimestamp("receiver_date_time");
                p.return_date_time = rs.getTimestamp("return_date_time");
                p.user_return_id = rs.getString("user_return_id");
                p.user_receiver_id = rs.getString("user_receiver_id");
                list.add(p);
            }
            return list;
        }
    }
}

/*
 * AppointmentStatus.java
 *
 * Created on 19 �ѹ��¹ 2548, 23:02 �.
 */
package com.hospital_os.object;
//import com.hospital_os.utility.*;

/**
 *
 * @author Administrator
 */
public abstract class AppointmentStatus {

    public static String WAIT = "0";
    public static String COMPLETE = "1";
    public static String MISS = "2";
    public static String CANCEL = "3";
    public static String BEFORE = "4";
    public static String AFTER = "5";
    public static String CONFIRM = "6";
    //amp:9/8/2549
    public static String WAIT_STR = "�͡�ùѴ";
    public static String COMPLETE_STR = "�ҵ���Ѵ";
    public static String MISS_STR = "�Դ�Ѵ";
    public static String CANCEL_STR = "¡��ԡ�Ѵ";
    public static String BEFORE_STR = "�ҡ�͹�Ѵ";
    public static String AFTER_STR = "����ѧ�Ѵ";
    public static String CONFIRM_STR = "�׹�ѹ��ùѴ";
    public static String WAIT_FN = "/com/hosv3/gui/images/appointment_wait.png";
    public static String COMPLETE_FN = "/com/hosv3/gui/images/appointment_complete.png";
    public static String MISS_FN = "/com/hosv3/gui/images/appointment_miss.png";
    public static String CANCEL_FN = "/com/hosv3/gui/images/appointment_cancel.png";
    public static String BEFORE_FN = "/com/hosv3/gui/images/appointment_before.png";
    public static String AFTER_FN = "/com/hosv3/gui/images/appointment_after.png";
    public static String CONFIRM_FN = "/com/hosv3/gui/images/appointment_confirm.png";

    /**
     * Creates a new instance of AppointmentStatus
     */
    //amp:9/8/2549
    public static String getString(String code) {
        if (code.equals(WAIT)) {
            return (WAIT_STR);
        } else if (code.equals(COMPLETE)) {
            return (COMPLETE_STR);
        } else if (code.equals(MISS)) {
            return (MISS_STR);
        } else if (code.equals(CANCEL)) {
            return (CANCEL_STR);
        } else if (code.equals(BEFORE)) {
            return (BEFORE_STR);
        } else if (code.equals(AFTER)) {
            return (AFTER_STR);
        } else if (code.equals(CONFIRM)) {
            return (AFTER_STR);
        } else {
            return "";
        }
    }
}

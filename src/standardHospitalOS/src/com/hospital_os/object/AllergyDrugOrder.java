/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
public class AllergyDrugOrder extends Persistent {

    public String t_patient_drug_allergy_id = "";
    public String t_order_id = "";
    public String allergy_drug_order_cause = "";
    public String user_record = "";
    public String record_date_time = "";
    public String user_modify = "";
    public String modify_date_time = "";
    public String active = "";
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;
import java.util.Date;

/**
 *
 * @author Somprasong
 */
public class InsuranceClaimBilling extends Persistent {

    public String b_finance_insurance_company_id = "";
    public double total_debt = 0.0d;
    public double total_discount = 0.0d;
    public double total_vat = 0.0d;
    public double total_tax = 0.0d;
    public double total_payment = 0.0d;
    public String f_payment_type_id = "";
    public String b_bank_info_id = "";
    public String bank_branch_name = "";
    public String account_number = "";
    public Date payment_date = new Date();
    public String record_datetime = "";
    public String user_record_id = "";
}

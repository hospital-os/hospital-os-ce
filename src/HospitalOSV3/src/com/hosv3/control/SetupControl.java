/*
 * SetupControl.java *
 * Created on 11 ���Ҥ� 2546, 15:36 �.
 */
package com.hosv3.control;

import com.hosos.util.crypto.Base64;
import com.hosos.util.crypto.MD5;
import com.hosos.util.general.RandomString;
import com.hospital_os.object.*;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hospital_os.utility.ComboFix;
import com.hospital_os.utility.ComplexDataSource;
import com.hospital_os.utility.Constant;
import com.hospital_os.utility.CryptPassword;
import com.hospital_os.utility.DataSource;
import com.hospital_os.utility.Gutil;
import com.hosv3.control.thread.SolveDepNameSNamePID;
import com.hosv3.control.thread.SolveDeprecatedVNThread;
import com.hosv3.control.thread.SolveEmptyFamilyThread;
import com.hosv3.control.thread.SolveEmptyHCISThread;
import com.hosv3.control.thread.SolveHNPatternThread;
import com.hosv3.object.HosObject;
import com.hosv3.object.LabMapping;
import com.hosv3.object.LookupObject;
import com.hosv3.object.OfficeInCup;
import com.hosv3.object.ReceiptBookSeq;
import com.hosv3.object.ReceiptSequance;
import com.hosv3.subject.HosSubject;
import com.hosv3.utility.Config;
import com.hosv3.utility.DateUtil;
import com.hosv3.utility.ResourceBundle;
import com.pcu.object.DoseEpiSet;
import com.pcu.object.EpiSet;
import com.pcu.object.EpiSetGroup;
import com.pcu.object.Home;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import sd.comp.idandcombobox.KeyValue;
import sd.util.datetime.DateTimeUtil;

/**
 *
 * @author tong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class SetupControl {

    private static final Logger LOG = Logger.getLogger(BillingControl.class.getName());
    protected ConnectionInf theConnectionInf;
    protected HosDB theHosDB;
    protected HosObject theHO;
    protected LookupObject theLO;
    protected HosSubject theHS;
    UpdateStatus theUS;
    LookupControl theLookupControl;
    PatientControl thePatientControl;
    SolveEmptyFamilyThread sef = new SolveEmptyFamilyThread();
    SolveEmptyHCISThread solveEmpHCIS = new SolveEmptyHCISThread();
    SolveHNPatternThread solveHNPat = new SolveHNPatternThread();
    SolveDeprecatedVNThread solveDepVN = new SolveDeprecatedVNThread();
    SolveDepNameSNamePID solveDepPID = new SolveDepNameSNamePID();
    SystemControl theSystemControl;
    private SetupControlCleanTransection theCleanTrans;
    private HosControl hosControl;

    public SetupControl(ConnectionInf con, HosObject ho, HosDB hdb, HosSubject hs, LookupObject lo) {
        theConnectionInf = con;
        theHosDB = hdb;
        theHS = hs;
        theHO = ho;
        theLO = lo;
        theCleanTrans = new SetupControlCleanTransection(theConnectionInf, theHosDB);
    }

    public void setSystemControl(SystemControl systemControl) {
        theSystemControl = systemControl;
    }

    public int deleteAuthentitcation(Authentication authentication) {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ResultSet rs = theConnectionInf.eQuery("select count(*) "
                    + "from b_employee where f_employee_authentication_id = '"
                    + authentication.getObjectId() + "'");
            int count = 0;
            if (rs.next()) {
                count = rs.getInt(1);
            }
            rs.close();
            if (count > 0) {
                theUS.setStatus("�������Է�Զ١���� User �����˹�������������öź�͡��", UpdateStatus.WARNING);
                return 2;
            }
            if (authentication.getObjectId().equals("1")
                    || authentication.getObjectId().equals("2")
                    || authentication.getObjectId().equals("3")
                    || authentication.getObjectId().equals("4")
                    || authentication.getObjectId().equals("5")
                    || authentication.getObjectId().equals("6")
                    || authentication.getObjectId().equals("7")
                    || authentication.getObjectId().equals("8")
                    || authentication.getObjectId().equals("9")
                    || authentication.getObjectId().equals("10")
                    || authentication.getObjectId().equals("11")
                    || authentication.getObjectId().equals("12")
                    || authentication.getObjectId().equals("13")) {
                theUS.setStatus("�������Է�ԡ����ҹ��鹰ҹ�������ö¡��ԡ��", UpdateStatus.WARNING);
                return 3;
            }
            if (!theUS.confirmBox("�ѹ�ѹ���ź�������Է�ԡ����ҹ�����", UpdateStatus.WARNING)) {
                return 1;
            }
            theHosDB.theAuthenticationDB.delete(authentication);
            theHosDB.theGActionAuthDB.deleteByAid(authentication.getObjectId());
            theConnectionInf.getConnection().commit();
            theUS.setStatus("���¡��ԡ�������Է�ԡ����ҹ�������", UpdateStatus.COMPLETE);
            return 0;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            return 4;
        } finally {
            theConnectionInf.close();
        }

    }

    public void setUpdateStatus(UpdateStatus us) {
        theUS = us;
    }

    public void setDepControl(LookupControl lc, PatientControl pc) {
        theLookupControl = lc;
        thePatientControl = pc;
    }

    public int saveOfficeInCup(Office off) {
        int ret = -1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            OfficeInCup oic = HosObject.initOfficeInCup(off);
            theUS.setStatus("�ѹ�֡ʶҹ��Һ���ࢵ�������", UpdateStatus.COMPLETE);
            ret = theHosDB.theOfficeInCupDB.insert(oic);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus("�ѹ�֡ʶҹ��Һ���ࢵ�Դ��Ҵ", UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        return ret;
    }

    public int saveWelfareInCup(Office off) {
        int ret = -1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            OfficeInCup oic = HosObject.initOfficeInCup(off);
            theUS.setStatus("�ѹ�֡ʶҹ��Һ���ࢵ�������", UpdateStatus.COMPLETE);
            ret = theHosDB.theWelfareInCupDB.insert(oic);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus("�ѹ�֡ʶҹ��Һ���ࢵ�Դ��Ҵ", UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        return ret;
    }

    public int deleteOfficeInCup(OfficeInCup oic) {
        if (oic == null || oic.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return 0;
        }
        if (!theUS.confirmBox(ResourceBundle.getBundleText("�׹�ѹ����ź��¡�ù��")
                + " "
                + ResourceBundle.getBundleText("�Ҩ�Դ�š�з��Ѻ������㹰ҹ��������"), UpdateStatus.WARNING)) {
            return 0;
        }
        int ret = -1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theUS.setStatus("źʶҹ��Һ���ࢵ�������", UpdateStatus.COMPLETE);
            ret = theHosDB.theOfficeInCupDB.delete(oic);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(ResourceBundle.getBundleText("źʶҹ��Һ���ࢵ") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        return ret;
    }

    public int deleteWelfareInCup(OfficeInCup oic) {
        if (oic == null || oic.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return 0;
        }
        if (!theUS.confirmBox(ResourceBundle.getBundleText("�׹�ѹ����ź��¡�ù��")
                + " "
                + ResourceBundle.getBundleText("�Ҩ�Դ�š�з��Ѻ������㹰ҹ��������"), UpdateStatus.WARNING)) {
            return 0;
        }
        int ret = -1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theUS.setStatus("źʶҹ��Һ���ࢵ�������", UpdateStatus.COMPLETE);
            ret = theHosDB.theWelfareInCupDB.delete(oic);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(ResourceBundle.getBundleText("źʶҹ��Һ���ࢵ") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        return ret;
    }

    public int deleteOfficeInCup(Office oic) {
        if (oic == null || oic.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return 0;
        }
        if (!theUS.confirmBox(ResourceBundle.getBundleText("�׹�ѹ����ź��¡�ù��")
                + " "
                + ResourceBundle.getBundleText("�Ҩ�Դ�š�з��Ѻ������㹰ҹ��������"), UpdateStatus.WARNING)) {
            return 0;
        }
        int ret = -1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theUS.setStatus("źʶҹ��Һ���ࢵ�������", UpdateStatus.COMPLETE);
            ret = theHosDB.theOfficeInCupDB.deleteByOffId(oic.getObjectId());
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(ResourceBundle.getBundleText("źʶҹ��Һ���ࢵ") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        return ret;
    }

    public Vector listBillingGroupItemAll(String pk, String active) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theBillingGroupItemDB.selectAllByName(pk, active);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public int saveBilling(BillingGroupItem billing) {
        int ans = 0;
        if ((billing.billing_group_item_id.isEmpty())
                || (billing.description.isEmpty())) {
            theUS.setStatus("�������ö�ѹ�֡�������繤����ҧ��", UpdateStatus.WARNING);
            return ans;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            BillingGroupItem bt = theHosDB.theBillingGroupItemDB.selectByCode(billing.billing_group_item_id);
            if (bt != null && billing.getObjectId() == null) {
                theUS.setStatus("�������ö�ѹ�֡���ʫ����", UpdateStatus.WARNING);
                return ans;
            }
            Vector btv = theHosDB.theBillingGroupItemDB.selectEqName(billing.description, "1");
            if (billing.getObjectId() == null) {
                if (!btv.isEmpty()) {
                    theUS.setStatus(ResourceBundle.getBundleText("�������ö�ѹ�֡����")
                            + " "
                            + ResourceBundle.getBundleText("��������´")
                            + " "
                            + ResourceBundle.getBundleText("����բ����ū�ӡѹ��"), UpdateStatus.WARNING);
                    return ans;
                }
            } else {
                if (btv.size() > 1) {
                    theUS.setStatus(ResourceBundle.getBundleText("�������ö�ѹ�֡����")
                            + " "
                            + ResourceBundle.getBundleText("��������´")
                            + " "
                            + ResourceBundle.getBundleText("����բ����ū�ӡѹ��"), UpdateStatus.WARNING);
                    return ans;
                }
                if (btv.size() == 1) {
                    BillingGroupItem bgi = (BillingGroupItem) btv.get(0);
                    if (!bgi.getObjectId().equals(billing.getObjectId())) {
                        theUS.setStatus(ResourceBundle.getBundleText("�������ö�ѹ�֡����") + " "
                                + ResourceBundle.getBundleText("��������´") + " "
                                + ResourceBundle.getBundleText("����բ����ū�ӡѹ��"), UpdateStatus.WARNING);
                        return ans;
                    }
                }
            }
            ////////////////////////////////////////////////////////////////////////
            if (billing.active.equals("0")) {
                Vector v = theHosDB.theItemDB.selectByBgi(billing.getObjectId(), "1");
                if (v != null && !v.isEmpty()) {
                    theUS.setStatus(ResourceBundle.getBundleText("�������¡�ù������¡�õ�Ǩ�ѡ������") + " "
                            + ResourceBundle.getBundleText("��س�¡��ԡ��¡�õ�Ǩ�ѡ������ҹ�鹡�͹"), UpdateStatus.WARNING);
                    return ans;
                }
            }
            ////////////////////////////////////////////////////////////////////////
            if (billing.getObjectId() == null) {
                theHosDB.theBillingGroupItemDB.insert(billing);
                theUS.setStatus(ResourceBundle.getBundleText("��úѹ�֡�������¡�������") + " "
                        + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            } else {
                theHosDB.theBillingGroupItemDB.update(billing);
                theUS.setStatus(ResourceBundle.getBundleText("�����䢡������¡�������") + " "
                        + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            }
            ans = 1;
            theLO.theBillingGroupItem = theHosDB.theBillingGroupItemDB.selectBgiAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(ResourceBundle.getBundleText("��úѹ�֡�������¡�������") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public int deleteBilling(BillingGroupItem billing) {
        int ans = 0;
        if (theLookupControl.readOption().life.equals(Active.isEnable())) {
            theUS.setStatus(ResourceBundle.getBundleText("�к����������ҹ����")
                    + " "
                    + ResourceBundle.getBundleText("���ź�������������ö����"), UpdateStatus.WARNING);
            return ans;
        }
        if (billing == null || billing.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return ans;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector v = theHosDB.theItemDB.selectByBgi(billing.getObjectId());
            if (v != null && !v.isEmpty()) {
                theUS.setStatus("�������¡�ù������¡�õ�Ǩ�ѡ�������������öź��", UpdateStatus.WARNING);
                return ans;
            }
            boolean confirm = theUS.confirmBox(ResourceBundle.getBundleText("�׹�ѹ����ź��¡�ù��")
                    + " "
                    + ResourceBundle.getBundleText("�Ҩ�Դ�š�з��Ѻ������㹰ҹ��������"), UpdateStatus.WARNING);
            if (!confirm) {
                return ans;
            }
            ans = theHosDB.theBillingGroupItemDB.delete(billing);
            theLO.theBillingGroupItem = theHosDB.theBillingGroupItemDB.selectBgiAll();
            theConnectionInf.getConnection().commit();
            theUS.setStatus(ResourceBundle.getBundleText("���ź�������¡�õ�Ǩ�ѡ�ҷ�����͡") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(ResourceBundle.getBundleText("���ź�������¡�õ�Ǩ�ѡ�ҷ�����͡") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.COMPLETE);
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public int saveClinic(Clinic theClinic) {
        int ans = 0;
        if ((theClinic.clinic_id.isEmpty()) || (theClinic.name.isEmpty())) {
            theUS.setStatus("�������ö�ѹ�֡�������繤����ҧ��", UpdateStatus.WARNING);
            return ans;
        }
        Clinic cn = listClinicByCode(theClinic.clinic_id);
        if (cn != null && theClinic.getObjectId() == null) {
            theUS.setStatus("�������ö�ѹ�֡���ʫ����", UpdateStatus.WARNING);
            return ans;
        }
        if (!theUS.confirmBox(ResourceBundle.getBundleText("�����䢢����Ż������ä (clinic)")
                + " "
                + ResourceBundle.getBundleText("���ռšѺ����͡��§ҹ�ҧ���")
                + " "
                + ResourceBundle.getBundleText("�׹�ѹ������"), UpdateStatus.WARNING)) {
            return ans;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (theClinic.getObjectId() == null) {
                theHosDB.theClinicDB.insert(theClinic);
                theUS.setStatus(ResourceBundle.getBundleText("�ѹ�֡��ª��ͤ�Թԡ")
                        + " "
                        + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            } else {
                theHosDB.theClinicDB.update(theClinic);
                theUS.setStatus(ResourceBundle.getBundleText("�����ª��ͤ�Թԡ")
                        + " "
                        + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            }
            ans = 1;
            theLO.theClinic = theHosDB.theClinicDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public int saveGActionAuth(Vector v, Authentication authen) {
        int ans = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Authentication au = theHosDB.theAuthenticationDB.selectByPK(authen.getObjectId());
            if (au != null) {
                theHosDB.theAuthenticationDB.update(authen);
            } else {
                theHosDB.theAuthenticationDB.insert(authen);
            }
            ans = theHosDB.theGActionAuthDB.insertByAid(v, authen.getObjectId());
            theUS.setStatus("��úѹ�֡�Է�ԡ����ҹ�������", UpdateStatus.COMPLETE);
            theConnectionInf.getConnection().commit();
            theUS.setStatus(ResourceBundle.getBundleText("��úѹ�֡�Է�ԡ����ҹ") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(ResourceBundle.getBundleText("��úѹ�֡�Է�ԡ����ҹ") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);

        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public Vector listClinicAll(String pk, String active) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theClinicDB.selectAllByName("%" + pk + "%", active);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public int deleteClinic(Clinic clinic) {
        if (clinic == null || clinic.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return 0;
        }
        if (clinic.getObjectId().startsWith("02500000")) {
            theUS.setStatus(ResourceBundle.getBundleText("�������öź�����Ż������ä�ͧ�к���") + " "
                    + ResourceBundle.getBundleText("��س����� inActive"), UpdateStatus.WARNING);
            return -1;
        }
        if (!theUS.confirmBox(ResourceBundle.getBundleText("�׹�ѹ����ź��¡�ù��") + " "
                + ResourceBundle.getBundleText("�Ҩ�Դ�š�з��Ѻ������㹰ҹ��������"), UpdateStatus.WARNING)) {
            return -1;
        }
        int ans = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theClinicDB.delete(clinic);
            theConnectionInf.getConnection().commit();
            theUS.setStatus(ResourceBundle.getBundleText("ź��ª��ͤ�Թԡ") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public int deleteDrugDosePrint(DrugDosePrint o) {
        int ans = 0;
        if (theLookupControl.readOption().life.equals(Active.isEnable())) {
            theUS.setStatus(ResourceBundle.getBundleText("�к����������ҹ����") + " "
                    + ResourceBundle.getBundleText("���ź�������������ö����"), UpdateStatus.WARNING);
            ans = 0;
        }
        if (o == null || o.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return ans;
        }
        if (!theUS.confirmBox(ResourceBundle.getBundleText("�׹�ѹ����ź��¡�ù��") + " "
                + ResourceBundle.getBundleText("�Ҩ�Դ�š�з��Ѻ������㹰ҹ��������"), UpdateStatus.WARNING)) {
            return 0;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ans = theHosDB.theDrugDosePrintDB.delete(o);
            theLO.theDrugDosePrint = theHosDB.theDrugDosePrintDB.selectByKeyWord("", Active.isEnable());
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    /**
     * @param labresultgroup
     * @param vd
     * @param row
     * @Author : sumo
     * @date : 15/03/2549
     * @see : ź�����ż��Ż
     * �ͧ�����Ū�Դ�ͧ�����§ҹ���Ż������͡,ᶺ�ͧ�����ż��Ż������͡,Vector
     * �ͧ�����ż��Ż������͡
     * @return : int
     */
    public int deleteLabResutlDetail(LabResultGroup labresultgroup, int[] row, Vector vd) {
        int ans = 0;
        if (row.length == 0) {
            theUS.setStatus("��س����͡��������´���Ż����ͧ��è�ź��͹�ӡ�á�����ź", UpdateStatus.WARNING);
            return ans;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector v = theHosDB.theLabResultItemDB.selectByLabResultGroup(labresultgroup.getObjectId());
            if (v != null && !v.isEmpty()) {
                theUS.setStatus(ResourceBundle.getBundleText("�������öź��������´���Ż�����") + " "
                        + ResourceBundle.getBundleText("���ͧ�ҡ�ա�ùӪ�Դ�ͧ�����§ҹ���Ż����������"), UpdateStatus.WARNING);
                return ans;
            }
            if (!theUS.confirmBox(ResourceBundle.getBundleText("�׹�ѹ����ź��¡�ù��") + " "
                    + ResourceBundle.getBundleText("�Ҩ�Դ�š�з��Ѻ������㹰ҹ��������"), UpdateStatus.WARNING)) {
                return 0;
            }
            for (int i = row.length - 1; i >= 0; i--) {
                LabResultDetail lrd = (LabResultDetail) vd.get(row[i]);
                theHosDB.theLabResultDetailDB.delete(lrd);
                theUS.setStatus(ResourceBundle.getBundleText("ź��������´���Ż") + " "
                        + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
                vd.remove(row[i]);
            }
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "ź��������´���Ż");
            ans = 1;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, "ź��������´���Ż");
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    /**
     * @Author : sumo
     * @date : 16/03/2549
     * @see : ź�����Ū�Դ�ͧ�����§ҹ���Ż
     * @param o �ͧ�����Ū�Դ�ͧ�����§ҹ���Ż������͡
     * @return int
     */
    public int deleteLabResultGroup(LabResultGroup o) {
        int ans = 0;
        if (theLookupControl.readOption().life.equals(Active.isEnable())) {
            theUS.setStatus(ResourceBundle.getBundleText("�к����������ҹ����") + " "
                    + ResourceBundle.getBundleText("���ź�������������ö����"), UpdateStatus.WARNING);
            return ans;
        }
        if (o == null || o.getObjectId() == null) {
            theUS.setStatus("��س����͡��Դ�ͧ�����§ҹ���Ż����ͧ��è�ź��͹�ӡ�á�����ź", UpdateStatus.WARNING);
            return ans;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector v = theHosDB.theLabResultItemDB.selectByLabResultGroup(o.getObjectId());
            if (v != null && !v.isEmpty()) {
                theUS.setStatus(ResourceBundle.getBundleText("�������öź��Դ�ͧ�����§ҹ���Ż�����") + " "
                        + ResourceBundle.getBundleText("���ͧ�ҡ�ա�ùӪ�Դ�ͧ�����§ҹ���Ż����������"), UpdateStatus.WARNING);
                return ans;
            }
            boolean confirm = theUS.confirmBox(ResourceBundle.getBundleText("�׹�ѹ����ź��¡�ù��") + " "
                    + ResourceBundle.getBundleText("�Ҩ�Դ�š�з��Ѻ������㹰ҹ��������"), UpdateStatus.WARNING);
            if (!confirm) {
                return ans;
            }
            theHosDB.theLabResultGroupDB.delete(o);
            theHosDB.theLabResultDetailDB.delete(o);
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "¡��ԡ���ź��Դ��§ҹ���Ż");
            ans = 1;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, "¡��ԡ���ź��Դ��§ҹ���Ż");
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public int saveDrugDosePrint(DrugDosePrint o) {
        try {
            double qty = Double.parseDouble(o.item_drug_dose_value);
            if (qty > 1) {
                theUS.setStatus("��سҡ�͡�ӹǹ㹪�ǧ 0-1", UpdateStatus.WARNING);
                return 0;
            }
        } catch (Exception ex) {
            theUS.setStatus("��سҡ�͡�ӹǹ����繵���Ţ���١��ͧ", UpdateStatus.WARNING);
            return 0;
        }
        if (o.item_drug_dose_value.isEmpty()
                || o.item_drug_dose_description.isEmpty()) {
            theUS.setStatus("�������ö�ѹ�֡�����Ũӹǹ��Т�ͤ���������繤����ҧ��", UpdateStatus.WARNING);
            return 0;
        }
        DrugDosePrint di = theLookupControl.readDrugDosePrintByValue(o.item_drug_dose_value);
        if (di != null && o.getObjectId() == null) {
            theUS.setStatus("�������ö�ѹ�֡��Ҩӹǹ����ӡѹ��", UpdateStatus.WARNING);
            return 0;
        }
        if (di != null && o.getObjectId() != null && !di.getObjectId().equals(o.getObjectId())) {
            theUS.setStatus("�������ö�ѹ�֡��Ҩӹǹ����ӡѹ��", UpdateStatus.WARNING);
            return 0;
        }

        int ans = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (o.getObjectId() == null) {
                ans = theHosDB.theDrugDosePrintDB.insert(o);
            } else {
                ans = theHosDB.theDrugDosePrintDB.update(o);
            }
            theLO.theDrugDosePrint = theHosDB.theDrugDosePrintDB.selectByKeyWord("", Active.isEnable());
            theConnectionInf.getConnection().commit();
            theUS.setStatus(ResourceBundle.getBundleText("�ѹ�֡�ӹǹ�ҷ��������Ѻ��þ����") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    protected int intCountQuery(String sql) throws Exception {
        java.sql.ResultSet rs = theConnectionInf.eQuery(sql);
        int record_count = 0;
        if (rs.next()) {
            record_count = rs.getInt(1);
        }
        return record_count;
    }

    public int deleteDrugFrequency(DrugFrequency drugfrequency) {
        int ans = 0;
        if (theLookupControl.readOption().life.equals(Active.isEnable())) {
            theUS.setStatus(ResourceBundle.getBundleText("�к����������ҹ����") + " "
                    + ResourceBundle.getBundleText("���ź�������������ö����"), UpdateStatus.WARNING);
            return ans;
        }
        if (drugfrequency == null || drugfrequency.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return ans;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String sql_count = "select count(*) from t_order_drug where b_item_drug_frequency_id = '"
                    + drugfrequency.getObjectId() + "'";
            if (intCountQuery(sql_count) > 0) {
                theUS.setStatus(ResourceBundle.getBundleText("�к����ա��������Ź������") + " "
                        + ResourceBundle.getBundleText("���ź�������������ö����"), UpdateStatus.WARNING);
                return ans;
            }
            sql_count = "select count(*) from b_item_drug where b_item_drug_frequency_id = '"
                    + drugfrequency.getObjectId() + "'";
            if (intCountQuery(sql_count) > 0) {
                theUS.setStatus(ResourceBundle.getBundleText("��¡�ù����١������Ѻ��¡��������") + " "
                        + ResourceBundle.getBundleText("���ź�������������ö����"), UpdateStatus.WARNING);
                return ans;
            }
            boolean confirm = theUS.confirmBox(ResourceBundle.getBundleText("�׹�ѹ����ź��¡�ù��") + " "
                    + ResourceBundle.getBundleText("�Ҩ�Դ�š�з��Ѻ������㹰ҹ��������"), UpdateStatus.WARNING);
            if (confirm) {
                ans = theHosDB.theDrugFrequencyDB.delete(drugfrequency);
            }
            theConnectionInf.getConnection().commit();
            theUS.setStatus(ResourceBundle.getBundleText("ź��ǧ���ҷ������") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public Vector listDrugFrequencyAll(String pk, String active) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theDrugFrequencyDB.selectAllByName(pk, active);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public int saveDrugFrequency(DrugFrequency drugfrequency) {
        int ans = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (drugfrequency.drug_frequency_id.isEmpty() || drugfrequency.description.isEmpty()) {
                theUS.setStatus("�������ö�ѹ�֡�������繤����ҧ��", UpdateStatus.WARNING);
                return 0;
            }
            try {
                double factor = Double.parseDouble(drugfrequency.factor);
                if (factor <= 0) {
                    theUS.setStatus("��سҡ�͡�ӹǹ ˹���/�ѹ ����ҡ���� 0", UpdateStatus.WARNING);
                    return 0;
                }
            } catch (Exception ex) {
                theUS.setStatus("��سҡ�͡�ӹǹ ˹���/�ѹ �繵���Ţ", UpdateStatus.WARNING);
                return 0;
            }

            DrugFrequency df = theHosDB.theDrugFrequencyDB.selectByCode(
                    drugfrequency.drug_frequency_id.trim());

            if (df != null && !df.getObjectId().equals(drugfrequency.getObjectId())) {
                theUS.setStatus("���ʫ�ӡ�سҡ�͡��������", UpdateStatus.WARNING);
                return -1;
            }
            if (drugfrequency.getObjectId() == null) {
                ans = theHosDB.theDrugFrequencyDB.insert(drugfrequency);
            } else {
                ans = theHosDB.theDrugFrequencyDB.update(drugfrequency);
            }

            theLO.theDrugFrequency = theHosDB.theDrugFrequencyDB.selectAll();
            theConnectionInf.getConnection().commit();
            theUS.setStatus(ResourceBundle.getBundleText("�ѹ�֡��ǧ���ҡ������") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }
    //code name active

    public Vector listDrugInstructionByCNA(String pk, String active) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theDrugInstructionDB.selectByCNA(pk, active);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public int saveDrugInstruction(DrugInstruction druginstruction) {
        int ans = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (druginstruction.getObjectId() == null) {
                ans = theHosDB.theDrugInstructionDB.insert(druginstruction);
            } else {
                ans = theHosDB.theDrugInstructionDB.update(druginstruction);
            }

            theLO.theDrugInstruction = theHosDB.theDrugInstructionDB.selectAll();
            theConnectionInf.getConnection().commit();
            theUS.setStatus("�ѹ�֡�����", UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            theUS.setStatus("�ѹ�֡�Դ��Ҵ", UpdateStatus.ERROR);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public int deleteDrugInstruction(DrugInstruction druginstruction) {
        int ans = 0;
        if (theLookupControl.readOption().life.equals(Active.isEnable())) {
            theUS.setStatus(com.hosv3.utility.Constant.getTextBundle("�к����������ҹ����") + " "
                    + com.hosv3.utility.Constant.getTextBundle("���ź�������������ö����"), UpdateStatus.WARNING);
            return ans;
        }
        if (druginstruction == null || druginstruction.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return ans;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String sql_count = "select count(*) from t_order_drug where b_item_drug_instruction_id = '"
                    + druginstruction.getObjectId() + "'";
            if (intCountQuery(sql_count) > 0) {
                theUS.setStatus(com.hosv3.utility.Constant.getTextBundle("�к����ա��������Ź������") + " "
                        + com.hosv3.utility.Constant.getTextBundle("���ź�������������ö����"), UpdateStatus.WARNING);
                throw new Exception("cn");
            }
            sql_count = "select count(*) from b_item_drug where b_item_drug_instruction_id = '"
                    + druginstruction.getObjectId() + "'";
            if (intCountQuery(sql_count) > 0) {
                theUS.setStatus(com.hosv3.utility.Constant.getTextBundle("��¡�ù����١������Ѻ��¡��������") + " "
                        + com.hosv3.utility.Constant.getTextBundle("���ź�������������ö����"), UpdateStatus.WARNING);
                throw new Exception("cn");
            }
            boolean confirm = theUS.confirmBox(com.hosv3.utility.Constant.getTextBundle("�׹�ѹ����ź��¡�ù��") + " "
                    + com.hosv3.utility.Constant.getTextBundle("�Ҩ�Դ�š�з��Ѻ������㹰ҹ��������"), UpdateStatus.WARNING);
            if (!confirm) {
                throw new Exception("cn");
            }

            ans = theHosDB.theDrugInstructionDB.delete(druginstruction);
            theConnectionInf.getConnection().commit();
            theUS.setStatus("���ź�����������", UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            if (!(ex.getMessage() != null && ex.getMessage().equals("cn"))) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                theUS.setStatus("���ź�����żԴ��Ҵ", UpdateStatus.ERROR);
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public int editDrugInstruction(DrugInstruction druginstruction) {
        int ans = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ans = theHosDB.theDrugInstructionDB.update(druginstruction);
            theConnectionInf.getConnection().commit();
            theUS.setStatus("�ѹ�֡�����", UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            theUS.setStatus("�ѹ�֡�Դ��Ҵ", UpdateStatus.ERROR);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    /**
     * ������¡�� Dose �����ҧ��� ���������˹�� setup
     *
     * @param key �� String ����纤ӷ���ͧ��ä���
     * @param active �� String �����ʶҹТͧ��¡�� Dose ���
     * @return Vector �������¡�� Dose ��ͷ�����
     * @date 04/08/2006
     * @Author Pu
     */
    public Vector listDrugDoseShortcutAll(String key, String active) {
        Vector vc = new Vector();

        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theDrugDoseShortcutDB.selectByCNA(key, active);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * ������¡�� Dose �����ҧ��� �ҡ primary key
     *
     * @param pk �� String ����� primary key �ͧ��¡�� dose
     * ��ͷ���ͧ��ä���
     * @return DrugDoseShortcut �������¡�� Dose ��ͷ�����
     * @date 04/08/2006
     * @Author Pu
     */
    public DrugDoseShortcut listDrugDoseShortcutByPk(String pk) {
        DrugDoseShortcut drugdose = new DrugDoseShortcut();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            drugdose = theHosDB.theDrugDoseShortcutDB.selectByPK(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return drugdose;
    }

    /**
     * ź��¡�� Dose ��� �͡�ҡ�ҹ������
     *
     * @param doseshortcut �� Object �������¡�� Dose ��ͷ���ͧ���ź
     * @return int �����ʶҹС��ź ����� 0 �ʴ������������
     * @date 04/08/2006
     * @Author Pu
     */
    public int deleteDrugDoseShortcutByPk(DrugDoseShortcut doseshortcut) {
        int ans = 0;
        if (theLookupControl.readOption().life.equals(Active.isEnable())) {
            theUS.setStatus(ResourceBundle.getBundleText("�к����������ҹ����") + " "
                    + ResourceBundle.getBundleText("���ź�������������ö����"), UpdateStatus.WARNING);
            return ans;
        }
        if (doseshortcut == null || doseshortcut.getObjectId() == null) {
            theUS.setStatus(ResourceBundle.getBundleText("��س����͡��¡�÷���ͧ���ź"), UpdateStatus.WARNING);
            return ans;
        }
        if (!theUS.confirmBox(ResourceBundle.getBundleText("�׹�ѹ����ź��¡�ù��") + " "
                + ResourceBundle.getBundleText("�Ҩ�Դ�š�з��Ѻ������㹰ҹ��������"), UpdateStatus.WARNING)) {
            return 0;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theDrugDoseShortcutDB.delete(doseshortcut);
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "ź��¡�� Dose ���");
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, "ź��¡�� Dose ���");
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    /**
     * �ѹ�֡��¡�� Dose ���
     *
     * @param doseshortcut �� Object �ͧ��¡�� Dose ��ͷ���ͧ��úѹ�֡
     * @return int
     * @date 04/08/2006
     * @Author Pu
     */
    public int saveDrugDoseShortcut(DrugDoseShortcut doseshortcut) {
        int ans = 0;
        if (doseshortcut.code.isEmpty() || doseshortcut.description.isEmpty()) {
            theUS.setStatus("�������ö�ѹ�֡�������繤����ҧ��", UpdateStatus.WARNING);
            return ans;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            DrugDoseShortcut doseshort = theHosDB.theDrugDoseShortcutDB.selectByCode(doseshortcut.code);
            if (doseshort != null && doseshortcut.getObjectId() == null) {
                theUS.setStatus("�������ö�ѹ�֡���ʫ����", UpdateStatus.WARNING);
                return ans;
            }
            if (doseshort != null && doseshortcut.getObjectId() != null
                    && !doseshortcut.getObjectId().equals(doseshort.getObjectId())) {
                theUS.setStatus("�������ö�ѹ�֡���ʫ����", UpdateStatus.WARNING);
                return ans;
            }
            if (doseshortcut.getObjectId() == null) {
                ans = theHosDB.theDrugDoseShortcutDB.insert(doseshortcut);
            } else {
                ans = theHosDB.theDrugDoseShortcutDB.update(doseshortcut);
            }
            theUS.setStatus(ResourceBundle.getBundleText("��úѹ�֡��¡�õ�Ǩ�ѡ��") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            theConnectionInf.getConnection().commit();
            return ans;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(ResourceBundle.getBundleText("��úѹ�֡��¡�õ�Ǩ�ѡ��") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
            return 0;
        } finally {
            theConnectionInf.close();
        }
    }

    public int saveDxTemplate(DxTemplate dxtemplate) {
        int ans = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (dxtemplate.guide_after_dx.length() > 255) {
                theUS.setStatus(ResourceBundle.getBundleText("���й���ѧ��Ǩ") + " "
                        + ResourceBundle.getBundleText("�ըӹǹ���ҡ�Թ�") + " "
                        + ResourceBundle.getBundleText("��س�Ŵ��ŧ����"), UpdateStatus.WARNING);
                return ans;
            }
            if (dxtemplate.description == null || dxtemplate.description.length() == 0) {
                theUS.setStatus("��سҡ�͡ Dx", UpdateStatus.WARNING);
                return ans;
            }
            DxTemplate dxtemp = theHosDB.theDxTemplate2DB.selectByName(dxtemplate.description);
            if (dxtemp != null && dxtemplate.getObjectId() == null) {
                theUS.setStatus(ResourceBundle.getBundleText("Dx:") + " "
                        + dxtemplate.description + " "
                        + ResourceBundle.getBundleText("������㹰ҹ������"), UpdateStatus.WARNING);
                return ans;
            }
            if (theHO.vItemDx == null) {
                theHO.vItemDx = new Vector();
            }
            if (theHO.vItemRiskDx == null) {
                theHO.vItemRiskDx = new ArrayList<>();
            }
            int size = (theHO.vItemDx.size() >= theHO.vItemRiskDx.size()) ? theHO.vItemDx.size() : theHO.vItemRiskDx.size();
            if (dxtemplate.getObjectId() == null) {
                theHosDB.theDxTemplate2DB.insert(dxtemplate);
                //pu:09/08/2549 :������¡�� Item �ͧ Dx
                for (int i = 0; i < size; i++) {
                    if (i < theHO.vItemDx.size()) {
                        DxTemplateMapItem itemdx = ((DxTemplateMapItem) theHO.vItemDx.get(i));
                        itemdx.template_dx_id = dxtemplate.getObjectId();
                        theHosDB.theDxTemplateMapItemDB.insert(itemdx);
                    }
                    if (i < theHO.vItemRiskDx.size()) {
                        DxTemplateMapItemRisk itemriskdx = ((DxTemplateMapItemRisk) theHO.vItemRiskDx.get(i));
                        itemriskdx.b_template_dx_id = dxtemplate.getObjectId();
                        itemriskdx.user_record_id = theHO.theEmployee.getObjectId();
                        theHosDB.theDxTemplateMapItemRiskDB.insert(itemriskdx);
                    }
                }
            } else {
                theHosDB.theDxTemplate2DB.update(dxtemplate);
                //pu:09/08/2549 ������¡�� Item �ͧ Dx
                for (int i = 0; i < size; i++) {
                    if (i < theHO.vItemDx.size()) {
                        DxTemplateMapItem itemdx = ((DxTemplateMapItem) theHO.vItemDx.get(i));
                        if (itemdx.getObjectId() == null) {
                            itemdx.template_dx_id = dxtemplate.getObjectId();
                            theHosDB.theDxTemplateMapItemDB.insert(itemdx);
                        }
                    }
                    if (i < theHO.vItemRiskDx.size()) {
                        DxTemplateMapItemRisk itemriskdx = ((DxTemplateMapItemRisk) theHO.vItemRiskDx.get(i));
                        if (itemriskdx.getObjectId() == null) {
                            itemriskdx.b_template_dx_id = dxtemplate.getObjectId();
                            itemriskdx.user_record_id = theHO.theEmployee.getObjectId();
                            theHosDB.theDxTemplateMapItemRiskDB.insert(itemriskdx);
                        }
                    }
                }
            }
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, "��úѹ�֡��¡�� Dx ��辺����");
            return 1;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT, "��úѹ�֡��¡�� Dx ��辺����");
            return ans;
        } finally {
            theConnectionInf.close();
        }
    }

    public int deleteDxTemplate(DxTemplate dxtemplate) {
        int ans = 0;
        if (dxtemplate == null || dxtemplate.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return ans;
        }
        boolean confirm = theUS.confirmBox(ResourceBundle.getBundleText("�׹�ѹ����ź��¡�ù��") + " "
                + ResourceBundle.getBundleText("�Ҩ�Դ�š�з��Ѻ������㹰ҹ��������"), UpdateStatus.WARNING);
        if (!confirm) {
            return ans;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theDxTemplateMapItemDB.deleteItemDxByDxTemplate(dxtemplate.getObjectId());
            theHO.vItemDx = null;
            theHosDB.theDxTemplate2DB.delete(dxtemplate);
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "���ź��¡�� Dx ��辺����");
            ans = 1;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, "���ź��¡�� Dx ��辺����");
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public DxTemplate readDxTemplateByName(String dxtemplate) {
        DxTemplate DxTemplate = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            DxTemplate = theHosDB.theDxTemplateDB.selectByName(dxtemplate);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return DxTemplate;
    }

    public int deleteEmployee(Employee employee) {
        int ans = 0;
        if (theLookupControl.readOption().life.equals(Active.isEnable())) {
            theUS.setStatus(ResourceBundle.getBundleText("�к����������ҹ����") + " "
                    + ResourceBundle.getBundleText("���ź�������������ö����"), UpdateStatus.WARNING);
            return ans;
        }
        if (employee == null || employee.getObjectId() == null) {
            theUS.setStatus(ResourceBundle.getBundleText("��س����͡��¡�÷���ͧ���ź"), UpdateStatus.WARNING);
            return ans;
        }
        if (!theUS.confirmBox(ResourceBundle.getBundleText("�׹�ѹ����ź��¡�ù��") + " "
                + ResourceBundle.getBundleText("�Ҩ�Դ�š�з��Ѻ������㹰ҹ��������"), UpdateStatus.WARNING)) {
            return 0;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theEmployeeDB.delete(employee);
            theLO.theEmployee = theHosDB.theEmployeeDB.selectActive("1");

            theConnectionInf.getConnection().commit();
            theUS.setStatus(ResourceBundle.getBundleText("ź�����ҹ") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public int saveEmployee(Employee employee) {
        int ans = 0;
        if (employee.employee_id.isEmpty() || employee.person.person_firstname.isEmpty()
                || employee.person.person_lastname.isEmpty()) {

            theUS.setStatus(ResourceBundle.getBundleText("��سҡ�͡������") + " "
                    + ResourceBundle.getBundleText("���ͷ�����ԡ��") + " "
                    + ResourceBundle.getBundleText("����") + " "
                    + ResourceBundle.getBundleText("���ʡ��"), UpdateStatus.WARNING);
            return -1;
        }
        if (employee.person.person_pid == null || employee.person.person_pid.isEmpty()) {
            theUS.setStatus("��سҡ�͡�������Ţ�ѵû�ЪҪ�", UpdateStatus.WARNING);
            return -1;
        }
        Employee em2 = theLookupControl.readEmployeeByUsername(employee.employee_id, "1");
        if (em2 != null && !em2.getObjectId().equals(employee.getObjectId())) {
            theUS.setStatus(ResourceBundle.getBundleText("�������ö�ѹ�֡") + " "
                    + ResourceBundle.getBundleText("���ͷ�����ԡ�ë����"), UpdateStatus.WARNING);
            return -1;
        }
        if (em2 != null && !em2.getObjectId().equals(employee.getObjectId())) {
            theUS.setStatus(ResourceBundle.getBundleText("�������ö�ѹ�֡") + " "
                    + ResourceBundle.getBundleText("���ͷ�����ԡ�ë����"), UpdateStatus.WARNING);
            return -1;
        }
        if (!employee.employee_no.isEmpty()) {
            // Somprasong 041111 ������Ǩ���੾��ᾷ�����ͧ��͡ 6 ��ѡ
            if (employee.authentication_id.equals("3") && employee.employee_no.length() > 6) {
                theUS.setStatus("��سҡ�͡�����Ţ㺻�Сͺ�ԪҪվ�繵���Ţ����Թ 6 ��ѡ", UpdateStatus.WARNING);
                return -1;
            }
        }
        boolean is_user = theHO.theEmployee.getObjectId().equals(employee.getObjectId());
        boolean is_admin = theHO.theEmployee.authentication_id.equals(Authentication.ADMIN);
        //��������� authentication_id_tmp ������������������ա������¹�ŧ�Է���ͧ���ͧ ��ͧ��� Admin �繼������¹�Է�ԡ����ҹ���

        if (!theHO.theEmployee.authentication_id.equals(Authentication.ADMIN)) {
            if (!theHO.theEmployee.authentication_id.equals(employee.authentication_id)) {
                theUS.setStatus(ResourceBundle.getBundleText("���ѹ�֡������Է����������¹�Է�ԡ����ҹ�ͧ���ͧ��") + " "
                        + ResourceBundle.getBundleText("��سҵԴ��� Admin"), UpdateStatus.WARNING);
                return 3;
            }
        }
        if (!is_user && !is_admin) {
            theUS.setStatus("���ѹ�֡������Է�����кѹ�֡�����Ţͧ����餹���", UpdateStatus.WARNING);
            return -1;
        }

        if (employee.employee_no.trim().isEmpty()
                && (employee.f_provider_type_id.equals(ProviderType.PHYSICIAN_ID) || employee.f_provider_type_id.equals(ProviderType.DENTIST_ID)
                || employee.f_provider_type_id.equals(ProviderType.NURSE_ID) || employee.f_provider_type_id.equals(ProviderType.PHARMACIST_ID)
                || employee.f_provider_type_id.equals(ProviderType.PHYSIOTHERAPIST_ID))) {
            theUS.setStatus("�������ؤ�ҡ�ᾷ�� �ѹ�ᾷ�� ��Һ�� ���Ѫ�� ��йѡ����Ҿ �������Ţ���㺻�Сͺ�ԪҪվ �����繤����ҧ", UpdateStatus.WARNING);
            return -1;
        }

        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            // 3.9.29
            Person tempPerson = theHosDB.thePersonDB.selectByPID(employee.person.person_pid);
            if (tempPerson != null && !tempPerson.getObjectId().equals(employee.person.getObjectId())) {
                String msg = String.format("��Ǩ�������Ţ�ѵû�Ъҡë�ӡѺ %s\n��ͧ���������Ţͧ�ؤ�Ź��᷹������ �������",
                        tempPerson.person_firstname + " " + tempPerson.person_lastname);
                int ret = JOptionPane.showConfirmDialog(null, msg, "����͹", JOptionPane.YES_NO_OPTION);
                if (ret == JOptionPane.YES_OPTION) {
                    employee.person = tempPerson;
                } else {
                    theUS.setStatus("�������ö�ѹ�֡ �����Ţ�ѵû�Ъҡë����", UpdateStatus.WARNING);
                    throw new Exception("cc");
                }
            }
            // person
            if (employee.person.getObjectId() == null) {
                employee.person.user_record_id = this.theHO.theEmployee.getObjectId();
                employee.person.record_date_time = this.theHO.date_time;
                employee.person.user_modify_id = this.theHO.theEmployee.getObjectId();
                employee.person.modify_date_time = this.theHO.date_time;
                Home home = hosControl.thePatientControl.intReadHome(null, null, null);
                employee.person.t_health_home_id = home.getObjectId();
                theHosDB.thePersonDB.insert(employee.person);
            } else {
                employee.person.user_modify_id = this.theHO.theEmployee.getObjectId();
                employee.person.modify_date_time = this.theHO.date_time;
                theHosDB.thePersonDB.update(employee.person);
            }
            // employee
            if (employee.getObjectId() == null) {
                employee.record_date_time = this.theHO.date_time;
                employee.update_date_time = this.theHO.date_time;
                theHosDB.theEmployeeDB.insert(employee);
            } else {
                employee.update_date_time = this.theHO.date_time;
                theHosDB.theEmployeeDB.update(employee);
            }
            // update lastest info to all employee of this person
            Vector<Employee> employees = theHosDB.theEmployeeDB.selectEmployeeByPersonId(employee.person.getObjectId());
            for (Employee tempEmp : employees) {
                if (!tempEmp.getObjectId().equals(employee.getObjectId())) {
                    tempEmp.employee_no = employee.employee_no;
                    tempEmp.employee_number_issue_date = employee.employee_number_issue_date;
                    tempEmp.f_provider_council_code_id = employee.f_provider_council_code_id;
                    tempEmp.f_provider_type_id = employee.f_provider_type_id;
                    tempEmp.start_date = employee.start_date;
                    tempEmp.out_date = employee.out_date;
                    tempEmp.move_from = employee.move_from;
                    tempEmp.move_to = employee.move_to;
                    theHosDB.theEmployeeDB.update(tempEmp);
                }
            }
            //�����ᾷ��������¹�ش��ԡ�èзӡ�����ҧᾷ�� � �ش��ԡ�÷������仴���
            if (employee.authentication_id.equals(Authentication.DOCTOR)) {
                ServicePoint sp = new ServicePoint();
                sp.setObjectId(employee.default_service_id);
                intAddEmployeeInServicePoint(sp, employee);
            }
            theLO.theEmployee = theHosDB.theEmployeeDB.selectActive("1");
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            ans = 1;
        } catch (Exception ex) {
            if (!ex.getMessage().equals("cc")) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                hosControl.updateTransactionFail(HosControl.TRANSACTION_DELETE);
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            ans = -1;
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    /*
     * �� rule ����������������������ѹ�֡ rule ��ѹ�֡ŧ authen
     *
     */
    public Vector listEmployeeAll(String pk, String active, String authen) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theEmployeeDB.selectAll(pk.trim(), active, authen);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listEmployeeSetup(String keyword, String active, String authen) {
        return listEmployeeSetup(keyword, active, authen, true);
    }

    public Vector listEmployeeSetup(String keyword, String active, String authen, boolean checkAuthen) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (!checkAuthen || theHO.theEmployee.authentication_id.equals(Authentication.ADMIN)) {
                vc = theHosDB.theEmployeeDB.selectAll(keyword.trim(), active, authen);
            } else {
                vc = new Vector();
                Employee emp = theLookupControl.readEmployeeByUsername(theHO.theEmployee.employee_id);
                vc.add(emp);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public int editServicePoint(ServicePoint servicepoint) {
        int ans = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theServicePointDB.update(servicepoint);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public Vector listServicePointByName(String pk, String active) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theServicePointDB.selectAllByName(pk, active);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public ServicePoint listServicePointByPk(String pk) {
        ServicePoint sp = new ServicePoint();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            sp = theHosDB.theServicePointDB.selectByPK(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return sp;
    }

    /*
     * henbe
     */
    public int saveServicePoint(ServicePoint servicepoint) {
        return saveServicePoint(servicepoint, null);
    }

    /*
     * henbe
     */
    public int saveServicePoint(ServicePoint servicepoint, Vector vdoctor) {
        if (servicepoint.service_point_id.isEmpty()
                || servicepoint.name.isEmpty()) {
            theUS.setStatus("�������ö�ѹ�֡�������繤����ҧ��", UpdateStatus.WARNING);
            return -1;
        }
        int ans = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (!servicepoint.active.equals(Active.isEnable())) {
                Vector vc = theHosDB.theQueueTransferDB.listTransferVisitQueueByServicePoint(servicepoint.getObjectId(), "", "");
                if (vc != null && !vc.isEmpty()) {
                    theUS.setStatus(ResourceBundle.getBundleText("�ռ���������㹨ش��ԡ��") + " "
                            + servicepoint.name + " "
                            + ResourceBundle.getBundleText("�������öź�ش��ԡ�ù����"), UpdateStatus.WARNING);
                    servicepoint.active = Active.isEnable();
                    return -1;
                }
            }
            ServicePoint sp = theHosDB.theServicePointDB.selectByCode(servicepoint.service_point_id);
            if (sp != null) {
                if (servicepoint.getObjectId() == null) {
                    theUS.setStatus("�������ö�ѹ�֡���ʨش��ԡ�ë����", UpdateStatus.WARNING);
                    return -1;
                }
                if (servicepoint.getObjectId() != null
                        && !sp.getObjectId().equals(servicepoint.getObjectId())) {
                    theUS.setStatus("�������ö�ѹ�֡���ʨش��ԡ�ë����", UpdateStatus.WARNING);
                    return -1;
                }
            }
            if (servicepoint.getObjectId() == null) {
                ans = theHosDB.theServicePointDB.insert(servicepoint);
                theUS.setStatus(ResourceBundle.getBundleText("�ѹ�֡�ش��ԡ�ü����¹͡") + " "
                        + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            } else {
                ans = theHosDB.theServicePointDB.update(servicepoint);
                theUS.setStatus(ResourceBundle.getBundleText("��䢨ش��ԡ�ü����¹͡") + " "
                        + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            }

            if (vdoctor != null) {
                theHosDB.theServicePointDoctorDB.deleteByServicePointID(servicepoint.getObjectId());
                //}
                if (servicepoint.service_type_id.equals("3")) {
                    for (int i = 0; i < vdoctor.size(); i++) {
                        Employee emp = (Employee) vdoctor.get(i);
                        //check �����ᾷ���ӡѺ���������ºѹ�֡������������
                        boolean is_exist = false;
                        for (int j = i + 1; j < vdoctor.size(); j++) {
                            Employee emp1 = (Employee) vdoctor.get(j);
                            if (emp1.getObjectId().equals(emp.getObjectId())) {
                                is_exist = true;
                                break;
                            }
                        }
                        if (!is_exist) {
                            ServicePointDoctor spd = new ServicePointDoctor();
                            spd.doctor_id = emp.getObjectId();
                            spd.service_point_key_id = servicepoint.getObjectId();
                            theHosDB.theServicePointDoctorDB.insert(spd);
                            theUS.setStatus(ResourceBundle.getBundleText("�ѹ�֡�ش��ԡ�ü����¹͡") + " "
                                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
                        }
                    }
                }
            }
            theLO.vServicePoint = theHosDB.theServicePointDB.selectAll();
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            ans = -1;
        } finally {
            try {
                if (!theConnectionInf.getConnection().getAutoCommit()) {
                    theConnectionInf.getConnection().commit();
                }
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theConnectionInf.close();
        }
        return ans;
    }

    public int deleteServicePoint(ServicePoint servicepoint) {
        int ans = 0;
        if (theLookupControl.readOption().life.equals(Active.isEnable())) {
            LOG.log(Level.INFO, "{0} {1}", new Object[]{ResourceBundle.getBundleText("�к����������ҹ����"), ResourceBundle.getBundleText("���ź�������������ö����")});
            theUS.setStatus(ResourceBundle.getBundleText("�к����������ҹ����") + " "
                    + ResourceBundle.getBundleText("���ź�������������ö����"), UpdateStatus.WARNING);
            return ans;
        }
        if (servicepoint == null || servicepoint.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return ans;
        }
        if (servicepoint.active.equals("1")) {
            theUS.setStatus(ResourceBundle.getBundleText("�к����������ҹ����") + " "
                    + ResourceBundle.getBundleText("���ź�������������ö����"), UpdateStatus.WARNING);
            ans = 0;
            return ans;
        }
        if (servicepoint.getObjectId().equals("2403071862616")) {
            theUS.setStatus("���͹حҵ���ź�ش��ԡ��PCU", UpdateStatus.WARNING);
            ans = 0;
            return ans;
        }
        boolean confirm = theUS.confirmBox(ResourceBundle.getBundleText("�׹�ѹ����ź��¡�ù��") + " "
                + ResourceBundle.getBundleText("�Ҩ�Դ�š�з��Ѻ������㹰ҹ��������"), UpdateStatus.WARNING);
        if (!confirm) {
            return ans;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ans = theHosDB.theServicePointDoctorDB.deleteByServicePointID(servicepoint.getObjectId());
            ans += theHosDB.theServicePointDB.delete(servicepoint);
            theLO.vServicePoint = theHosDB.theServicePointDB.selectAll();
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_DELETE);
            ans = 0;
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE);
        }
        return ans;
    }

    public int saveServicePointDoctor(ServicePointDoctor servicepointdoctor) {
        int ans = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ans = theHosDB.theServicePointDoctorDB.insert(servicepointdoctor);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public int deleteServicePointDoctor(Vector vEmp, int[] rows, ServicePoint sp) {
        int ans = 0;
        if (rows.length == 0) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return ans;
        }
        boolean confirm = theUS.confirmBox(ResourceBundle.getBundleText("�׹�ѹ����ź�ؤ��ҡ÷�����͡") + " "
                + ResourceBundle.getBundleText("�͡�ҡ�ش��ԡ��"), UpdateStatus.WARNING);
        if (!confirm) {
            return -1;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (int i = 0; i < rows.length; i++) {
                Employee ep = (Employee) vEmp.get(rows[i]);
                if (ep.default_service_id.equals(sp.getObjectId())) {
                    theUS.setStatus(ResourceBundle.getBundleText("�������öź�����㹨ش��ԡ����") + " "
                            + ResourceBundle.getBundleText("���ͧ�ҡ�����ҹ�ըش��ԡ�ù��ش��ԡ����ѡ"), UpdateStatus.WARNING);
                    continue;
                }
                ans = theHosDB.theServicePointDoctorDB.deleteDoctorByServicePointID(ep.getObjectId(), sp.getObjectId());
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE);
        }
        return ans;
    }

    public Vector listCategoryGroupItemByName(String pk, String active) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theCategoryGroupItemDB.selectAllByName(pk, active);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public int saveCategoryGroupItem(CategoryGroupItem category) {
        return saveCategoryGroupItem(category, "");
    }

    public int saveCategoryGroupItem(CategoryGroupItem category, String old_cat) {
        int ans = 0;
        if ((category.category_group_item_id.isEmpty()) || (category.description.isEmpty())) {
            theUS.setStatus("�������ö�ѹ�֡������������Ъ����繤����ҧ��", UpdateStatus.WARNING);
            return ans;
        }
        if (!old_cat.isEmpty() && !old_cat.equals(category.category_group_code)) {
            theUS.confirmBox(ResourceBundle.getBundleText("�������¹�ŧ�������ѡ�ͧ��¡�õ�Ǩ�ѡ�Ҩ��ռš�з��Ѻ��¡�õ�Ǩ�ѡ��㹡�������"), UpdateStatus.WARNING);
            return ans;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            CategoryGroupItem ct = theHosDB.theCategoryGroupItemDB.selectByCode(category.category_group_item_id);
            if (ct != null && category.getObjectId() == null) {
                theUS.setStatus("�������ö�ѹ�֡���ʫ����", UpdateStatus.WARNING);
                return ans;
            }
            Vector ctv = theHosDB.theCategoryGroupItemDB.selectEqName(category.description, "1");
            if (category.getObjectId() == null) {
                if (!ctv.isEmpty()) {
                    theUS.setStatus(ResourceBundle.getBundleText("�������ö�ѹ�֡����") + " "
                            + ResourceBundle.getBundleText("��������´") + " "
                            + ResourceBundle.getBundleText("����բ����ū�ӡѹ��"), UpdateStatus.WARNING);
                    return ans;
                }
            } else {
                if (ctv.size() > 1) {
                    theUS.setStatus(ResourceBundle.getBundleText("�������ö�ѹ�֡����") + " "
                            + ResourceBundle.getBundleText("��������´") + " "
                            + ResourceBundle.getBundleText("����բ����ū�ӡѹ��"), UpdateStatus.WARNING);
                    return ans;
                }
                if (ctv.size() == 1) {
                    CategoryGroupItem bgi = (CategoryGroupItem) ctv.get(0);
                    if (!bgi.getObjectId().equals(category.getObjectId())) {
                        theUS.setStatus(ResourceBundle.getBundleText("�������ö�ѹ�֡����") + " "
                                + ResourceBundle.getBundleText("��������´") + " "
                                + ResourceBundle.getBundleText("����բ����ū�ӡѹ��"), UpdateStatus.WARNING);
                        return ans;
                    }
                }

            }
            ////////////////////////////////////////////////////////////////////////
            if (category.active.equals("0")) {
                Vector v = theHosDB.theItemDB.selectByCgi(category.getObjectId(), "1");
                if (v != null && !v.isEmpty()) {
                    theUS.setStatus("�������¡�ù������¡�õ�Ǩ�ѡ�������س�¡��ԡ��¡�õ�Ǩ�ѡ������ҹ�鹡�͹", UpdateStatus.WARNING);
                    return ans;
                }
            }
            ////////////////////////////////////////////////////////////////////////
            if (category.getObjectId() == null) {
                ans = theHosDB.theCategoryGroupItemDB.insert(category);
                theUS.setStatus(ResourceBundle.getBundleText("��úѹ�֡�������¡�õ�Ǩ�ѡ��") + " "
                        + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            } else {
                ans = theHosDB.theCategoryGroupItemDB.update(category);
                theUS.setStatus(ResourceBundle.getBundleText("�����䢡������¡�õ�Ǩ�ѡ��") + " "
                        + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            }
            theLO.theCategoryGroupItem = theHosDB.theCategoryGroupItemDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(ResourceBundle.getBundleText("��úѹ�֡�������¡�õ�Ǩ�ѡ��") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public int deleteCategoryGroupItem(CategoryGroupItem category) {
        int ans = 0;
        if (theLookupControl.readOption().life.equals(Active.isEnable())) {
            theUS.setStatus(ResourceBundle.getBundleText("�к����������ҹ����") + " "
                    + ResourceBundle.getBundleText("���ź�������������ö����"), UpdateStatus.WARNING);
            return ans;
        }
        if (category == null || category.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return ans;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector v = theHosDB.theItemDB.selectByCgi(category.getObjectId());
            if (v != null && !v.isEmpty()) {
                theUS.setStatus("�������¡�ù������¡�õ�Ǩ�ѡ�������������öź��", UpdateStatus.WARNING);
                return ans;
            }
            if (!theUS.confirmBox(ResourceBundle.getBundleText("�׹�ѹ����ź��¡�ù��") + " "
                    + ResourceBundle.getBundleText("�Ҩ�Դ�š�з��Ѻ������㹰ҹ��������"), UpdateStatus.WARNING)) {
                return ans;
            }
            ans = theHosDB.theCategoryGroupItemDB.delete(category);
            theLO.theCategoryGroupItem = null;
            theConnectionInf.getConnection().commit();
            theUS.setStatus(ResourceBundle.getBundleText("���ź�������¡�õ�Ǩ�ѡ��") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(ResourceBundle.getBundleText("���ź�������¡�õ�Ǩ�ѡ��") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    /**
     * @Author : sumo
     * @date : 05/06/2549
     * @see : list ��������¡�á�����ҵðҹ��������
     * @param pk �ͧ�Ӥ�
     * @param active
     * @return : Object �ͧ��������¡�á�����ҵðҹ���ç�Ѻ������
     */
    public Vector listItem16GroupByName(String pk, String active) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theItem16GroupDB.selectAllByName(pk, active);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * @Author : sumo
     * @date : 05/06/2549
     * @see : �ѹ�֡��������¡�á�����ҵðҹ
     * @param standard �ͧ��������¡�á�����ҵðҹ��ͧ��úѹ�֡
     */
    public void saveItem16Group(Item16Group standard) {
        if ((standard.item_16_group_id.isEmpty()) && (standard.description.isEmpty())) {
            theUS.setStatus("��س��к�������Ъ�����¡�� 16 ������ҵðҹ��͹�ӡ�úѹ�֡", UpdateStatus.WARNING);
            return;
        }
        if (standard.item_16_group_id.isEmpty()) {
            theUS.setStatus("��س��к�������¡�� 16 ������ҵðҹ��͹�ӡ�úѹ�֡", UpdateStatus.WARNING);
            return;
        }
        if ((standard.description.isEmpty())) {
            theUS.setStatus("��س��кت�����¡�� 16 ������ҵðҹ��͹�ӡ�úѹ�֡", UpdateStatus.WARNING);
            return;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Item16Group ct = theHosDB.theItem16GroupDB.selectByCode(standard.item_16_group_id);
            if (ct != null && standard.getObjectId() == null) {
                theUS.setStatus(ResourceBundle.getBundleText("�������ö�ѹ�֡���ʫ����") + " "
                        + ResourceBundle.getBundleText("��سҵ�Ǩ�ͺ�����ա����"), UpdateStatus.WARNING);
                return;
            }
            Vector ctv = theHosDB.theItem16GroupDB.selectEqName(standard.description, "1");
            if (standard.getObjectId() == null) {
                if (!ctv.isEmpty()) {
                    theUS.setStatus(ResourceBundle.getBundleText("�������ö�ѹ�֡����") + " "
                            + ResourceBundle.getBundleText("��������´") + " "
                            + ResourceBundle.getBundleText("����բ����ū�ӡѹ��"), UpdateStatus.WARNING);
                    return;
                }
            } else {
                if (ctv.size() > 1) {
                    theUS.setStatus(ResourceBundle.getBundleText("�������ö�ѹ�֡����") + " "
                            + ResourceBundle.getBundleText("��������´") + " "
                            + ResourceBundle.getBundleText("����բ����ū�ӡѹ��"), UpdateStatus.WARNING);
                    return;
                }
                if (ctv.size() == 1) {
                    Item16Group bgi = (Item16Group) ctv.get(0);
                    if (!bgi.getObjectId().equals(standard.getObjectId())) {
                        theUS.setStatus(ResourceBundle.getBundleText("�������ö�ѹ�֡����") + " "
                                + ResourceBundle.getBundleText("��������´") + " "
                                + ResourceBundle.getBundleText("����բ����ū�ӡѹ��"), UpdateStatus.WARNING);
                        return;
                    }
                }
            }
            ////////////////////////////////////////////////////////////////////////
            if (standard.active.equals("0")) {
                Vector v = theHosDB.theItemDB.selectBy16gi(standard.getObjectId(), "1");
                if (v != null && !v.isEmpty()) {
                    theUS.setStatus("�������¡�ù������¡�õ�Ǩ�ѡ�������س�¡��ԡ��¡�õ�Ǩ�ѡ������ҹ�鹡�͹", UpdateStatus.WARNING);
                    return;
                }
            }
            ////////////////////////////////////////////////////////////////////////
            if (standard.item_16_group_id.endsWith("_OH")) {
                Vector v_oh = theHosDB.theItem16GroupDB.selectAllByName("_OH", "1");
                if (v_oh != null && !v_oh.isEmpty()) {
                    for (int i = 0; i < v_oh.size(); i++) {
                        Item16Group i16g = (Item16Group) v_oh.get(i);
                        if (!i16g.getObjectId().equals(standard.getObjectId())) {
                            theUS.setStatus(ResourceBundle.getBundleText("������ҡ�Ѻ��ҹ�е�ͧ����§�����������ҹ��") + " "
                                    + ResourceBundle.getBundleText("��������������") + " "
                                    + i16g.description, UpdateStatus.WARNING);
                            return;
                        }
                    }
                }
            }
            ////////////////////////////////////////////////////////////////////////
            if (standard.getObjectId() == null) {
                theHosDB.theItem16GroupDB.insert(standard);
                theUS.setStatus(ResourceBundle.getBundleText("��úѹ�֡��¡�� 16 ������ҵðҹ") + " "
                        + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            } else {
                theHosDB.theItem16GroupDB.update(standard);
                theUS.setStatus(ResourceBundle.getBundleText("��������¡�� 16 ������ҵðҹ") + " "
                        + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            }
            theLO.vItem16Group = theHosDB.theItem16GroupDB.selectAll();
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);

        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        }
    }

    /**
     * @Author : sumo
     * @date : 05/06/2549
     * @see : ź��������¡�á�����ҵðҹ������͡
     * @param standard �ͧ��������¡�á�����ҵðҹ������͡
     * @return int �ӹǹ�����ŷ��ź�
     */
    public int deleteItem16Group(Item16Group standard) {
        int ans = 0;
        if (theLookupControl.readOption().life.equals(Active.isEnable())) {
            theUS.setStatus(ResourceBundle.getBundleText("�к����������ҹ����") + " "
                    + ResourceBundle.getBundleText("���ź�������������ö����"), UpdateStatus.WARNING);
            return ans;
        }
        if (standard == null || standard.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return ans;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector v = theHosDB.theItemDB.selectByCgi(standard.getObjectId());
            if (v != null && !v.isEmpty()) {
                theUS.setStatus("�������¡�ù������¡�õ�Ǩ�ѡ�������������öź��", UpdateStatus.WARNING);
                return ans;
            }
            boolean confirm = theUS.confirmBox(ResourceBundle.getBundleText("�׹�ѹ����ź��¡�ù��") + " "
                    + ResourceBundle.getBundleText("�Ҩ�Դ�š�з��Ѻ������㹰ҹ��������"), UpdateStatus.WARNING);
            if (!confirm) {
                return ans;
            }

            ans = theHosDB.theItem16GroupDB.delete(standard);
            theLO.vItem16Group = theHosDB.theItem16GroupDB.selectAll();
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_DELETE, "��������¡�� 16 ������ҵðҹ������͡");
            return ans;
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE, "��������¡�� 16 ������ҵðҹ������͡");
        }
        return ans;
    }

    public int saveOption(Option option, boolean isAutoResetYear, int day, int month) {
        int ans = 0;
        if (option.cancel_receipt.equals(Active.isEnable()) && option.passwd_cancel_receipt.isEmpty()) {
            theUS.setStatus("���ʻŴ��ͤ", UpdateStatus.WARNING);
            return -1;
        }
        if (option.cancel_discharge_finance_nextday.equals(Active.isEnable()) && option.passwd_cancel_discharge_finance_nextday.isEmpty()) {
            theUS.setStatus("���ʻŴ��ͤ", UpdateStatus.WARNING);
            return -1;
        }
        if (option.Cancel_Receipt_Time.isEmpty() || option.Cancel_Receipt_Time.equals(ans)) {
            theUS.setStatus("��˹�����¡��ԡ�����", UpdateStatus.WARNING);
            return -1;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Option checkPasswd = theHosDB.theOptionDB.select();
            if (!checkPasswd.passwd_cancel_receipt.equals(option.passwd_cancel_receipt.trim())) {
                option.passwd_cancel_receipt = CryptPassword.encryptText(option.passwd_cancel_receipt.trim());
            }
            if (!checkPasswd.passwd_cancel_discharge_finance_nextday.equals(option.passwd_cancel_discharge_finance_nextday.trim())) {
                option.passwd_cancel_discharge_finance_nextday = CryptPassword.encryptText(option.passwd_cancel_discharge_finance_nextday.trim());
            }
            if (!checkPasswd.passwd_cancel_receipt_payment_gateway.equals(option.passwd_cancel_receipt_payment_gateway.trim())) {
                option.passwd_cancel_receipt_payment_gateway = CryptPassword.encryptText(option.passwd_cancel_receipt_payment_gateway.trim());
            }
            theHosDB.theOptionDB.insert(option);
            theLO.theOption = option;
            if (isAutoResetYear) {
                java.util.Date currentDate = null;
                ResultSet resultSetDate = theConnectionInf.eQuery("select current_date");
                if (resultSetDate.next()) {
                    currentDate = resultSetDate.getDate(1);
                }
                Calendar tempCalendar = Calendar.getInstance();
                tempCalendar.setTime(currentDate);
                tempCalendar.set(Calendar.MONTH, month);
                tempCalendar.set(Calendar.DAY_OF_MONTH, day);
                tempCalendar.set(Calendar.HOUR_OF_DAY, 0);
                tempCalendar.set(Calendar.MINUTE, 0);
                tempCalendar.set(Calendar.SECOND, 0);
                tempCalendar.set(Calendar.MILLISECOND, 0);
                Date resetDate = tempCalendar.getTime();
                if (resetDate.after(currentDate)) {
                    tempCalendar.add(Calendar.YEAR, -1);
                    resetDate = tempCalendar.getTime();
                }
                theHosDB.theVisitYearDB.updateAutoResetYear(day, month, resetDate);
            }
            theConnectionInf.getConnection().commit();
            ans = 1;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(ResourceBundle.getBundleText("��úѹ�֡�������к�") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
            ans = -1;
        } finally {
            theConnectionInf.close();
        }
        if (ans == 1) {
            theHS.theSetupSubject.notifyRefreshComboBox();
            theUS.setStatus(ResourceBundle.getBundleText("��úѹ�֡�������к�") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        }
        return ans;
    }

    public Vector listServicePointDoctor(String pk) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theServicePointDoctorDB.selectBySerciveID(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public int deleteWard(Ward ward) {
        int ans = 0;
        if (ward == null || ward.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return ans;
        }
        if (!theUS.confirmBox(ResourceBundle.getBundleText("�׹�ѹ����ź��¡�ù��") + " "
                + ResourceBundle.getBundleText("�Ҩ�Դ�š�з��Ѻ������㹰ҹ��������"), UpdateStatus.WARNING)) {
            return 0;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);

            ans = theHosDB.theWardDB.delete(ward);
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_DELETE);
            ans = -1;
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE);
        }
        return ans;
    }

    public Vector listWardByName(String pk, String active) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theWardDB.selectAllByName(pk, active);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public int saveWard(Ward ward) {
        if (ward.ward_id.isEmpty() || ward.description.isEmpty() || ward.b_service_point_id.isEmpty()) {
            theUS.setStatus("�������ö�ѹ�֡�������繤����ҧ��", UpdateStatus.WARNING);
            return -1;
        }
        if (ward.ward_id.length() > 4) {
            if (!theUS.confirmBox(ResourceBundle.getBundleText("���������Թ 4 ��ѡ�Ҩ�ռšѺ��§ҹ IPD 12 ����Դ��Ҵ") + " "
                    + ResourceBundle.getBundleText("�׹�ѹ��úѹ�֡"), UpdateStatus.WARNING)) {
                return -1;
            }
        }
        int ans = 0;
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Ward wd = theHosDB.theWardDB.selectByCode(ward.ward_id);
            if (wd != null && ward.getObjectId() == null) {
                theUS.setStatus("�������ö�ѹ�֡���ʫ����", UpdateStatus.WARNING);
                return -1;
            }
            if (!ward.active.equals(Active.isEnable())) {
                Vector vListWard = theHosDB.theListTransferDB.listQueueVisitInWard(ward.getObjectId());
                if (vListWard != null && !vListWard.isEmpty()) {
                    theUS.setStatus(ResourceBundle.getBundleText("�ѧ�ռ��������������") + " "
                            + ResourceBundle.getBundleText("��س����¼������������蹡�͹"), UpdateStatus.WARNING);
                    ward.active = Active.isEnable();
                    return -1;
                }
            }
            if (ward.getObjectId() == null) {
                ans = theHosDB.theWardDB.insert(ward);
                theUS.setStatus(ResourceBundle.getBundleText("��úѹ�֡����") + " "
                        + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            } else {
                ans = theHosDB.theWardDB.update(ward);
                theUS.setStatus(ResourceBundle.getBundleText("����������") + " "
                        + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            }
            theLO.theWard = theHosDB.theWardDB.selectAll();
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            ans = -1;
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        }
        return ans;
    }

    public int deletePayer(Payer payer) {
        int ans = 0;
        if (theLookupControl.readOption().life.equals(Active.isEnable())) {
            theUS.setStatus(ResourceBundle.getBundleText("�к����������ҹ����") + " "
                    + ResourceBundle.getBundleText("���ź�������������ö����"), UpdateStatus.WARNING);
            return ans;
        }
        if (payer == null || payer.getObjectId() == null) {
            theUS.setStatus(ResourceBundle.getBundleText("��س����͡��¡�÷���ͧ���ź"), UpdateStatus.WARNING);
            return ans;
        }
        if (!theUS.confirmBox(ResourceBundle.getBundleText("�׹�ѹ����ź��¡�ù��") + " "
                + ResourceBundle.getBundleText("�Ҩ�Դ�š�з��Ѻ������㹰ҹ��������"), UpdateStatus.WARNING)) {
            return 0;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.thePayerDB.delete(payer);
            theConnectionInf.getConnection().commit();
            theUS.setStatus(ResourceBundle.getBundleText("ź�������Թ") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public int editPayer(Payer payer) {
        int ans = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.thePayerDB.update(payer);
            theConnectionInf.getConnection().commit();
            theUS.setStatus(ResourceBundle.getBundleText("��䢼������Թ") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public Vector listPayer(String pk, String active) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.thePayerDB.selectAllByName(pk, active);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public int savePayer(Payer payer) {
        int ans = 0;
        if ((payer.payer_id.isEmpty()) || (payer.description.isEmpty())) {
            theUS.setStatus("�������ö�ѹ�֡�������繤����ҧ��", UpdateStatus.WARNING);
            return ans;
        }
        Payer py = listPayerByCode(payer.payer_id);
        if (py != null && payer.getObjectId() == null) {
            theUS.setStatus("�������ö�ѹ�֡���ʫ����", UpdateStatus.WARNING);
            return ans;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (payer.getObjectId() == null) {
                theHosDB.thePayerDB.insert(payer);
            } else {
                theHosDB.thePayerDB.update(payer);
            }
            theLO.thePayer = theHosDB.thePayerDB.selectAll();
            ans = 1;
            theConnectionInf.getConnection().commit();
            theUS.setStatus(ResourceBundle.getBundleText("�ѹ�֡�������Թ") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public Vector listUOM(String pk, String active) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theUomDB.selectAllByName(pk, active);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public int saveUOM2(Uom uom, Vector vprint) {
        for (int i = 0; i < vprint.size(); i++) {
            DrugDosePrint o = (DrugDosePrint) vprint.get(i);
            try {
                double qty = Double.parseDouble(o.item_drug_dose_value);
                if (qty > 1) {
                    theUS.setStatus("��سҡ�͡�ӹǹ㹪�ǧ 0-1", UpdateStatus.WARNING);
                    return 0;
                }
            } catch (Exception ex) {
                theUS.setStatus("��سҡ�͡�ӹǹ����繵���Ţ���١��ͧ", UpdateStatus.WARNING);
                return 0;
            }
            if (o.item_drug_dose_value.isEmpty()
                    || o.item_drug_dose_description.isEmpty()) {
                theUS.setStatus("�������ö�ѹ�֡�����Ũӹǹ��Т�ͤ���������繤����ҧ��", UpdateStatus.WARNING);
                return 0;
            }
        }

        int ans = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (uom.getObjectId() == null) {
                ans = theHosDB.theUomDB.insert(uom);
            } else {
                ans = theHosDB.theUomDB.update(uom);
            }
            theHosDB.theDrugDosePrintDB.delete(uom.getObjectId());
            theHosDB.theDrugDosePrintDB.insertV(vprint);

            theHosDB.theDrugDoseMapUomDB.delete(uom.getObjectId());
            theHosDB.theDrugDoseMapUomDB.insertV(vprint, uom);

            theLO.theUom = theHosDB.theUomDB.selectAll();

            theConnectionInf.getConnection().commit();
            theUS.setStatus(ResourceBundle.getBundleText("�ѹ�֡˹�����") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public int saveUOM(Uom uom) {
        int ans = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (uom.getObjectId() == null) {
                ans = theHosDB.theUomDB.insert(uom);
            } else {
                ans = theHosDB.theUomDB.update(uom);
            }
            theLO.theUom = theHosDB.theUomDB.selectAll();
            theConnectionInf.getConnection().commit();
            theUS.setStatus(ResourceBundle.getBundleText("�ѹ�֡˹�����") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public int deleteUOM(Uom uom) {
        int ans = 0;
        if (theLookupControl.readOption().life.equals(Active.isEnable())) {
            theUS.setStatus(ResourceBundle.getBundleText("�к����������ҹ����") + " "
                    + ResourceBundle.getBundleText("���ź�������������ö����"), UpdateStatus.WARNING);
            return ans;
        }
        if (uom == null || uom.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return ans;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String sql_count1 = "select count(*) from t_order_drug where b_item_drug_uom_id_use = '"
                    + uom.getObjectId() + "'";
            String sql_count2 = "select count(*) from t_order_drug where b_item_drug_uom_id_purch = '"
                    + uom.getObjectId() + "'";
            if (intCountQuery(sql_count1) > 0 || intCountQuery(sql_count2) > 0) {
                theUS.setStatus(ResourceBundle.getBundleText("�к����ա��������Ź������") + " "
                        + ResourceBundle.getBundleText("���ź�������������ö����"), UpdateStatus.WARNING);
                return ans;
            }
            sql_count1 = "select count(*) from b_item_drug where item_drug_use_uom = '"
                    + uom.getObjectId() + "'";
            sql_count2 = "select count(*) from b_item_drug where item_drug_purch_uom = '"
                    + uom.getObjectId() + "'";
            if (intCountQuery(sql_count1) > 0 || intCountQuery(sql_count2) > 0) {
                theUS.setStatus(ResourceBundle.getBundleText("��¡�ù����١������Ѻ��¡��������") + " "
                        + ResourceBundle.getBundleText("���ź�������������ö����"), UpdateStatus.WARNING);
                return ans;
            }

            boolean confirm = theUS.confirmBox(ResourceBundle.getBundleText("�׹�ѹ����ź��¡�ù��") + " "
                    + ResourceBundle.getBundleText("�Ҩ�Դ�š�з��Ѻ������㹰ҹ��������"), UpdateStatus.WARNING);
            if (!confirm) {
                return ans;
            }

            ans = theHosDB.theUomDB.delete(uom);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public int deleteVitalTemplate(VitalTemplate vitaltemplate) {
        int ans = 0;
        if (vitaltemplate == null || vitaltemplate.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return ans;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theVitalTemplateDB.delete(vitaltemplate);
            theConnectionInf.getConnection().commit();
            theUS.setStatus(ResourceBundle.getBundleText("ź��¡�õ�Ǫ����ҡ�����ͧ��") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public int editVitalTemplate(VitalTemplate vitaltemplate) {
        int ans = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theVitalTemplateDB.update(vitaltemplate);
            theConnectionInf.getConnection().commit();
            theUS.setStatus(ResourceBundle.getBundleText("��������¡�õ�Ǫ����ҡ�����ͧ��") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(ResourceBundle.getBundleText("��úѹ�֡��¡�õ�Ǫ����ҡ�����ͧ��") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public int saveVitalTemplate(VitalTemplate vitaltemplate) {
        int ans = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theVitalTemplateDB.insert(vitaltemplate);
            theConnectionInf.getConnection().commit();
            theUS.setStatus(ResourceBundle.getBundleText("��úѹ�֡��¡�õ�Ǫ����ҡ�����ͧ��") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(ResourceBundle.getBundleText("��úѹ�֡��¡�õ�Ǫ����ҡ�����ͧ��") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public Vector listVitalTemplate(String pk, String point) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if ((point.equals(Active.isDisable())) || ("0".equals(point))) {
                vc = theHosDB.theVitalTemplateDB.selectAllByName(pk);
            } else {
                vc = theHosDB.theVitalTemplateDB.selectAllByServicePoint(pk, point);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listLabResultDetailByResultType(String resulttypeid) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (!resulttypeid.isEmpty()) {
                vc = theHosDB.theLabResultDetailDB.selectByResulteCode(resulttypeid);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector intListLabResultDetail(String resulttypeid) {
        Vector vc = new Vector();
        try {
            vc = theHosDB.theLabResultDetailDB.selectCByResulteCode(resulttypeid);
            if (vc.isEmpty()) {
                vc.add(new ComboFix("-", "-"));
            }
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
        }
        return vc;
    }

    public Vector listLabCResultDetailByResultType(String resulttypeid) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = intListLabResultDetail(resulttypeid);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public int saveICD9(ICD9 icd9) {
        int ans = 0;
        if ((icd9.icd9_id.isEmpty()) || (icd9.description.isEmpty())) {
            theUS.setStatus("�������ö�ѹ�֡�������繤����ҧ��", UpdateStatus.WARNING);
            return ans;
        }
        ICD9 i9 = listIcd9ByCode(icd9.icd9_id);
        if (i9 != null && icd9.getObjectId() == null) {
            theUS.setStatus("�������ö�ѹ�֡���ʫ����", UpdateStatus.WARNING);
            return ans;
        }
        if (i9 != null && icd9.getObjectId() != null
                && !icd9.getObjectId().equals(i9.getObjectId())) {
            theUS.setStatus("�������ö�ѹ�֡���ʫ����", UpdateStatus.WARNING);
            return ans;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (icd9.getObjectId() == null) {
                theHosDB.theICD9DB.insert(icd9);
                theUS.setStatus(ResourceBundle.getBundleText("��úѹ�֡�����ѵ����") + " "
                        + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            } else {
                theHosDB.theICD9DB.update(icd9);
                theUS.setStatus(ResourceBundle.getBundleText("�����������ѵ����") + " "
                        + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT, "�����ѵ����");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, "�����ѵ����");
        }
        return ans;
    }

    public int saveICD10(ICD10 icd10) {
        int ans = 0;
        if ((icd10.icd10_id.isEmpty())
                || (icd10.description.isEmpty())
                || (icd10.generate_code.isEmpty())) {
            theUS.setStatus("�������ö�ѹ�֡�������繤����ҧ��", UpdateStatus.WARNING);
            return ans;
        }
        ICD10 i10 = listIcd10ByCode(icd10.icd10_id, 1);
        if (i10 != null && icd10.getObjectId() == null) {
            theUS.setStatus("�������ö�ѹ�֡���ʫ����", UpdateStatus.WARNING);
            return ans;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (icd10.getObjectId() == null) {
                theHosDB.theICD10DB.insert(icd10);
            } else {
                theHosDB.theICD10DB.update(icd10);
            }
            ans = 1;
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        }
        return ans;
    }

    public int editICD9(ICD9 icd9) {
        int ans = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ans = theHosDB.theICD9DB.update(icd9);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public int editICD10(ICD10 icd10) {
        int ans = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ans = theHosDB.theICD10DB.update(icd10);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public int deleteICD9(ICD9 icd9) {
        int ans = 0;
        if (theLookupControl.readOption().life.equals(Active.isEnable())) {
            theUS.setStatus(ResourceBundle.getBundleText("�к����������ҹ����") + " "
                    + ResourceBundle.getBundleText("���ź�������������ö����"), UpdateStatus.WARNING);
            return ans;
        }
        if (icd9 == null || icd9.getObjectId() == null) {
            theUS.setStatus(ResourceBundle.getBundleText("��س����͡��¡�÷���ͧ���ź"), UpdateStatus.WARNING);
            return ans;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ans = theHosDB.theICD9DB.delete(icd9);
            theConnectionInf.getConnection().commit();
            theUS.setStatus(ResourceBundle.getBundleText("���ź�����������ѵ����") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(ResourceBundle.getBundleText("���ź�����������ѵ����") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public int deleteICD10(ICD10 icd10) {
        int ans = 0;
        if (theLookupControl.readOption().life.equals(Active.isEnable())) {
            theUS.setStatus(ResourceBundle.getBundleText("�к����������ҹ����") + " "
                    + ResourceBundle.getBundleText("���ź�������������ö����"), UpdateStatus.WARNING);
            return ans;
        }
        if (icd10 == null || icd10.getObjectId() == null) {
            theUS.setStatus(ResourceBundle.getBundleText("��س����͡��¡�÷���ͧ���ź"), UpdateStatus.WARNING);
            return ans;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ans = theHosDB.theICD10DB.delete(icd10);
            theConnectionInf.getConnection().commit();
            theUS.setStatus(ResourceBundle.getBundleText("���ź�����ä") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(ResourceBundle.getBundleText("���ź�����ä") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public Vector listICD9All(String pk) {
        return listICD9All(pk, 1);
    }

    public Vector listICD9All(String pk, int active) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theICD9DB.selectByIdName(pk, active);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listItemByGroup(String itemGpID, String itemname, String active) {
        return listItemByGroup(itemGpID, itemname, active, false, false);
    }

    public Vector listItemByGroup(String itemGpID, String itemname, String active, boolean begin_with, boolean isCanOrder) {
        return listItemByGroup(itemGpID, itemname, active, begin_with, ItemType.isSingle(), isCanOrder);
    }

    public Vector listItemByGroup(String itemGpID, String itemname, String active, boolean begin_with, String itemType, boolean isCanOrder) {
        Vector vc = new Vector();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theItemDB.selectByItemGroup(itemGpID, itemname, active, begin_with, itemType, isCanOrder);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listAllLab() {
        Vector vc = new Vector();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theItemDB.selectAllLab();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public String getLabType(String b_item_id) {
        String type = "2";
        try {
            String sql = "select * from b_item_lab_group where b_item_id = '" + b_item_id + "'";
            ResultSet rs = theConnectionInf.eQuery(sql);
            while (rs.next()) {
                type = "1";
            }
            rs.close();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
        }
        return type;
    }

    public Vector listItemAndPrice(String plan, String itemGpID, String itemname, String active, boolean begin_with) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String sql = " select b_item.b_item_id"
                    + ",item_common_name"
                    + ",item_trade_name"
                    + ",item_price_active_date"
                    + ",b_item_price.item_price_number"
                    + ",item_price"
                    + ",item_price_ipd"
                    + ",item_price_cost"
                    + " from b_item"
                    + " left join (select b_item_id,max(item_price_active_date) as d from b_item_price group by b_item_id) as ip on b_item.b_item_id = ip.b_item_id"
                    + " left join b_item_price on (substr(ip.d,1,10) = substr(b_item_price.item_price_active_date,1,10) and b_item_price.b_item_id = b_item.b_item_id ";

            if (plan.length() != 0) {
                sql += " and item_price_number = '" + plan + "'";
            } else {
                sql += " and length(item_price_number) < 10";
            }

            sql += ") where item_active = '1'";

            if (itemname.length() != 0) {
                itemname = Gutil.CheckReservedWords(itemname);
                if (!begin_with) {
                    itemname = "%" + itemname;
                }
                sql += " and ( UPPER(item_common_name) like UPPER('" + itemname + "%')  "
                        + " or UPPER(item_nick_name) like UPPER('" + itemname + "%') "
                        + " or UPPER(item_trade_name) like UPPER('" + itemname + "%') "
                        + " or UPPER(item_number) like UPPER('" + itemname + "%'))  ";
            }

            if (itemGpID.length() != 0) {
                sql += " and b_item_subgroup_id = '" + itemGpID + "' ";
            }

            sql += " order by item_common_name";
            ResultSet rs = theConnectionInf.eQuery(sql);
            while (rs.next()) {
                String[] array = new String[8];
                array[0] = rs.getString(1);
                array[1] = rs.getString(2);
                array[2] = rs.getString(3);
                array[3] = rs.getString(4);
                array[4] = rs.getString(5);
                array[5] = Constant.getShowDoubleString(rs.getDouble(6));
                array[6] = Constant.getShowDoubleString(rs.getDouble(7));
                array[7] = Constant.getShowDoubleString(rs.getDouble(8));
                vc.add(array);
            }
            theConnectionInf.getConnection().commit();
            return vc;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            return null;
        } finally {
            theConnectionInf.close();
        }
    }

    public boolean saveItemPrice(String plan, String date, ItemPrice... itemPrices) {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            java.sql.ResultSet rs = theConnectionInf.eQuery("select CURRENT_TIME");
            String cur_time = "";
            if (rs.next()) {
                cur_time = rs.getString(1);
            }
            rs.close();
            for (ItemPrice itemPrice : itemPrices) {
                itemPrice.item_price_id = plan;
                itemPrice.active_date = date + "," + cur_time.substring(0, 8);
                itemPrice.user_record = theHO.theEmployee.getObjectId();
                theHosDB.theItemPriceDB.insert(itemPrice);
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        }
        return isComplete;
    }

    public int saveItem(Item item, ItemPrice ip, List<ItemPackage> vItemPackage) {
        return saveItem(item, ip, null, null, null, null, false, null, null, null, vItemPackage);
    }

    public int saveItem(Item item, ItemPrice ip, Drug drug, LabResultItem lri,
            LabGroup labgroup, Vector labset, boolean is_group, ItemService service) {
        return saveItem(item, ip, drug, lri, labgroup, labset, is_group, service, null);
    }

    public int saveItem(Item item, ItemPrice ip, Drug drug, LabResultItem lri,
            LabGroup labgroup, Vector labset, boolean is_group, ItemService service,
            ItemSupply itemSupply) {
        return saveItem(item, ip, drug, lri, labgroup, labset, is_group, service, itemSupply, null, null);
    }

    public int saveItem(Item item, ItemPrice ip, Drug drug, LabResultItem lri,
            LabGroup labgroup, Vector labset, boolean is_group, ItemService service,
            ItemSupply itemSupply, BItemXray itemXray) {
        return saveItem(item, ip, drug, lri, labgroup, labset, is_group, service, itemSupply, itemXray, null);
    }

    public int saveItem(Item item, ItemPrice ip, Drug drug, LabResultItem lri,
            LabGroup labgroup, Vector labset, boolean is_group, ItemService service,
            ItemSupply itemSupply, BItemXray itemXray, List<ItemPackage> vItemPackage) {
        int ans = 0;
        if (item.item_id.isEmpty() || item.common_name.isEmpty()) {
            theUS.setStatus("�������ö�ѹ�֡�������繤����ҧ��", UpdateStatus.WARNING);
            return ans;
        }
        if (item.getObjectId() == null && ip == null) {
            theUS.setStatus("��سҡ�͡�Ҥҵ�駵ѹ����Ѻ��¡�õ�Ǩ�ѡ�ҹ��", UpdateStatus.WARNING);
            return ans;
        }
        if (item.item_lab_duration_day.isEmpty()) {
            theUS.setStatus("�������ö�ѹ�֡������������繤����ҧ��", UpdateStatus.WARNING);
            return ans;
        }
        if (ip != null) {
            if (ip.price < 0) {
                theUS.setStatus("��سҺѹ�֡�Ҥ� OPD ����ҡ����������ҡѺ�ٹ��", UpdateStatus.WARNING);
                return ans;
            }
            if (ip.price_ipd < 0) {
                theUS.setStatus("��سҺѹ�֡�Ҥ� IPD ����ҡ����������ҡѺ�ٹ��", UpdateStatus.WARNING);
                return ans;
            }
            if (ip.price > 1000000) {
                theUS.setStatus("��سҺѹ�֡�Ҥ� OPD ��������", UpdateStatus.WARNING);
                return ans;
            }
            if (ip.price_ipd > 1000000) {
                theUS.setStatus("��سҺѹ�֡�Ҥ� IPD ��������", UpdateStatus.WARNING);
                return ans;
            }
            if (ip.item_share_doctor < 0) {
                theUS.setStatus("��سҺѹ�֡��ǹ�觤��ᾷ�� ����ҡ����������ҡѺ�ٹ��", UpdateStatus.WARNING);
                return ans;
            }
            if (ip.item_editable_price.equals("1")) {
                if (ip.item_limit_price_min < 0) {
                    theUS.setStatus("��سҺѹ�֡��ǧ�Ҥҵ���ش ����ҡ����������ҡѺ�ٹ��", UpdateStatus.WARNING);
                    return ans;
                }
                if (ip.item_limit_price_max <= ip.item_limit_price_min) {
                    theUS.setStatus("��سҺѹ�֡��ǧ�Ҥ��٧�ش ����ҡ���Ҫ�ǧ�Ҥҵ���ش", UpdateStatus.WARNING);
                    return ans;
                }
            }
        }
        boolean isNew = item.getObjectId() == null;
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Item it = theHosDB.theItemDB.selectById(item.item_id);
            if (it != null && item.getObjectId() == null) {
                theUS.setStatus("�������ö�ѹ�֡���ʫ����", UpdateStatus.WARNING);
                throw new Exception("cn");
            }
            if (it != null && item.getObjectId() != null
                    && !item.getObjectId().equals(it.getObjectId())) {
                theUS.setStatus("�������ö�ѹ�֡���ʫ����", UpdateStatus.WARNING);
                throw new Exception("cn");
            }
            // ���Ŵ���㹡�õ�Ǩ�ͺ��������ӡ�èѴ���§���������ѧ
            if (!item.secret.equals("1")) {
                item.secret = "sort";
            }
            // ��ҵ�����������¡�� 㹢�з�� active ���� �������¹ active �� 2
            if (item.active.equals("1") && item.item_not_use_to_order.equals("1")) {
                item.active = "2";
            }
            // �����
            CategoryGroupItem cgiNew = theLookupControl.readCategoryGroupItemById(item.item_group_code_category);
            // b_item
            item.user_modify = hosControl.theHO.theEmployee.getObjectId();
            if (item.getObjectId() == null) {
                item.user_record = hosControl.theHO.theEmployee.getObjectId();
                ans = theHosDB.theItemDB.insert(item);

                if (item.item_picture != null) {
                    theHosDB.theItemPictureDB.insert(item, theHO.theEmployee.getObjectId());
                }
            } else {
                // ��Ǩ�ͺ����� item ��������¡����������� ��������ź�����š��������͡��͹
                Item itemBeforeUpdate = theHosDB.theItemDB.selectByPK(item.getObjectId());
                CategoryGroupItem cgiBeforeUpdate = theLookupControl.readCategoryGroupItemById(itemBeforeUpdate.item_group_code_category);
                if (cgiBeforeUpdate != null && !cgiBeforeUpdate.category_group_code.equals(cgiNew.category_group_code)) {
                    if (cgiBeforeUpdate.category_group_code.equals(CategoryGroup.isLab())) {
                        LabResultItem labResultItem = theHosDB.theLabResultItemDB.selectByItemID(itemBeforeUpdate.getObjectId());
                        if (labResultItem != null) {
                            theHosDB.theLabResultItemGenderDB.deleteByBItemLabResultId(labResultItem.getObjectId());
                            theHosDB.theLabResultItemAgeDB.deleteByBItemLabResultId(labResultItem.getObjectId());
                            theHosDB.theLabResultItemTextDB.deleteByBItemLabResultId(labResultItem.getObjectId());
                            theHosDB.theLabResultItemDB.delete(labResultItem);
                        } else {
                            theHosDB.theLabSetDB.deleteByItemId(itemBeforeUpdate.getObjectId());
                            theHosDB.theLabGroupDB.deleteByItid(itemBeforeUpdate.getObjectId());
                        }
                    } else if (cgiBeforeUpdate.category_group_code.equals(CategoryGroup.isDrug())) {
                        theHosDB.theDrugDB.delete(itemBeforeUpdate.getObjectId());
                    } else if (cgiBeforeUpdate.category_group_code.equals(CategoryGroup.isService())
                            || cgiBeforeUpdate.category_group_code.equals(CategoryGroup.isDental())) {
                        theHosDB.theItemServiceDB.deleteByItemId(itemBeforeUpdate.getObjectId());
                    } else if (cgiBeforeUpdate.category_group_code.equals(CategoryGroup.isSupply())) {
                        theHosDB.theItemSupplyDB.delete(itemBeforeUpdate.getObjectId());
                    } else if (cgiBeforeUpdate.category_group_code.equals(CategoryGroup.isXray())) {
                        theHosDB.theBItemXrayDB.deleteByItemId(itemBeforeUpdate.getObjectId());
                    }
                }
                // update
                ans = theHosDB.theItemDB.update(item);
            }

            Vector vItemPrice = theHosDB.theItemPriceDB.selectByItem(item.getObjectId());
            boolean checkPrice = false;
            for (Object object : vItemPrice) {
                ItemPrice itemPrice = (ItemPrice) object;
                if (itemPrice.item_price_id == null || itemPrice.item_price_id.isEmpty()) {
                    checkPrice = true;
                }
            }
            if (!checkPrice && (ip == null || !ip.item_price_id.isEmpty())) {
                theUS.setStatus("��ͧ�ա�á�˹��Ҥҷ��������к��Է�ԡ���ѡ��", UpdateStatus.WARNING);
                throw new Exception("cn");
            }
            // item price
            if (ip != null) {
                ip.item_id = item.getObjectId();
                ip.user_record = hosControl.theHO.theEmployee.getObjectId();
                ans = theHosDB.theItemPriceDB.insert(ip);
            }
            // �����
            // 1. LAB
            if (cgiNew.category_group_code.equals(CategoryGroup.isLab())) {
                if (labset != null && !labset.isEmpty()) {
                    for (int i = 0, size = labset.size(); i < size; i++) {
                        LabSet ls = (LabSet) labset.get(i);
                        if (ls.item_id.equals(item.getObjectId())) {
                            theUS.setStatus("��س����͡��¡���ź���·���������¡�÷����ѧ���͡����", UpdateStatus.WARNING);
                            throw new Exception("cn");
                        }
                    }
                }
                // Check is secret lab?
                //amp:25/02/2549 ź item ��軡�Դ � b_item_lab_group
                if ("1".equals(item.secret)) {
                    Vector vLabGroup = theHosDB.theLabSetDB.selectByItemId(item.getObjectId());
                    if (vLabGroup != null) {
                        if (theUS.confirmBox(ResourceBundle.getBundleText("�Ż��ǹ������¡���Ż������Ż�ش")
                                + "\n" + ResourceBundle.getBundleText("�ҡ��ͧ��úѹ�֡���Ż���Դ")
                                + "\n" + ResourceBundle.getBundleText("�������ź�Ż��¡�ù���͡�ҡ�Ż�ش������"), UpdateStatus.WARNING)) {
                            theUS.setStatus("�������ѧ���١���", UpdateStatus.WARNING);
                            throw new Exception("cn");
                        } else {
                            ans = theHosDB.theLabSetDB.deleteByItemId(item.getObjectId());
                        }
                    }
                }
                // Check is LabDetail
                // tuk:20/07/2549 ��������������� insert ���� update �óշ������¡�� order lab ��ҹ��
                if (!is_group) {
                    lri.item_id = item.getObjectId();
                    if (lri.name.isEmpty()) {
                        lri.name = item.common_name;
                    }
                    if (lri.getObjectId() == null) {
                        theHosDB.theLabResultItemDB.insert(lri);
                    } else {
                        theHosDB.theLabResultItemDB.update(lri);
                    }
                    // set normal range delete old data and insert new
                    theHosDB.theLabResultItemGenderDB.deleteByBItemLabResultId(lri.getObjectId());
                    theHosDB.theLabResultItemAgeDB.deleteByBItemLabResultId(lri.getObjectId());
                    theHosDB.theLabResultItemTextDB.deleteByBItemLabResultId(lri.getObjectId());
                    if (lri.normalRanges != null && lri.normalRanges.length > 0) {
                        if (lri.normal_range_type.equals("1")) {
                            LabResultItemGender itemGender = (LabResultItemGender) lri.normalRanges[0];
                            itemGender.b_item_lab_result_id = lri.getObjectId();
                            theHosDB.theLabResultItemGenderDB.insert(itemGender);
                        } else if (lri.normal_range_type.equals("2")) {
                            for (Object object : lri.normalRanges) {
                                LabResultItemAge itemAge = (LabResultItemAge) object;
                                itemAge.b_item_lab_result_id = lri.getObjectId();
                                theHosDB.theLabResultItemAgeDB.insert(itemAge);
                            }
                        } else if (lri.normal_range_type.equals("3")) {
                            for (Object object : lri.normalRanges) {
                                LabResultItemText itemText = (LabResultItemText) object;
                                itemText.b_item_lab_result_id = lri.getObjectId();
                                theHosDB.theLabResultItemTextDB.insert(itemText);
                            }
                        }
                    }
                } else {//Is LabGroup?
                    theHosDB.theLabGroupDB.deleteByItid(item.getObjectId());
                    if (labgroup != null) {
                        theHosDB.theLabSetDB.deleteByLgid(labgroup.getObjectId());
                    }
                    if (labset != null && !labset.isEmpty()) {
                        labgroup.item_id = item.getObjectId();
                        theHosDB.theLabGroupDB.insert(labgroup);
                        for (int i = 0, size = labset.size(); i < size; i++) {
                            LabSet ls = (LabSet) labset.get(i);
                            ls.lab_group_id = labgroup.getObjectId();
                            try {
                                Integer.parseInt(ls.item_name);
                            } catch (Exception ex) {
                                ls.item_name = String.valueOf(i);
                            }
                            if (ls.item_name.length() == 1) {
                                ls.item_name = "0" + ls.item_name;
                            }
                            theHosDB.theLabSetDB.insert(ls);
                        }
                    }
                }
            } // 2. DRUG
            else if (cgiNew.category_group_code.equals(CategoryGroup.isDrug())) {
                //�ѭ�Ҥ�Ͱҹ�������ѹ�红������� double �����������ö�ѹ�֡�繤����ҧ��
                if (!drug.dose.trim().isEmpty()) {
                    try {
                        Double.parseDouble(drug.dose);
                    } catch (Exception ex) {
                        theUS.setStatus("��س��кػ���ҳ�����(᷺��������´) ����繨ӹǹ����Ţ", UpdateStatus.WARNING);
                        throw new Exception("cn");
                    }
                }
                if (!drug.qty.trim().isEmpty()) {
                    try {
                        Double.parseDouble(drug.qty);
                    } catch (Exception ex) {
                        theUS.setStatus("��س��кػ���ҳ������(᷺��������´) ����繨ӹǹ����Ţ", UpdateStatus.WARNING);
                        throw new Exception("cn");
                    }
                }
                if (drug.drug_strength.trim().isEmpty()) {
                    theUS.setStatus("��س��кؤ����ç (Strength) �᷺��������´", UpdateStatus.WARNING);
                    throw new Exception("cn");
                }
                //amp:13/03/2549 ��Ǩ�ͺ item ��� update ������������ ������������ź dose �ҷ������
                //���ͧ�ҡ�ա�����¡�����ҡ���������繡�������
                if (drug.getObjectId() == null) {
                    drug.item_id = item.getObjectId();
                    drug.drug_id = item.item_id;
                    theHosDB.theDrugDB.insert(drug);
                } else {
                    theHosDB.theDrugDB.update(drug);
                }
            } // 3. SERVICE or DENTAL
            else if (cgiNew.category_group_code.equals(CategoryGroup.isService())
                    || cgiNew.category_group_code.equals(CategoryGroup.isDental())) {
                if (service != null && !service.icd9_code.isEmpty()) {
                    if (service.getObjectId() == null) {
                        service.record_date_time = this.theHO.date_time;
                        theHosDB.theItemServiceDB.insert(service);
                    } else {
                        theHosDB.theItemServiceDB.update(service);
                    }
                } else {
                    theHosDB.theItemServiceDB.inactiveByItemID(item.getObjectId());
                }
            } // 4. SUPPLY
            else if (cgiNew.category_group_code.equals(CategoryGroup.isSupply())) {
                if (itemSupply != null) {
                    itemSupply.update_date_time = this.theHO.date_time;
                    itemSupply.user_update_id = this.theHO.theEmployee.getObjectId();
                    if (itemSupply.getObjectId() != null) {
                        theHosDB.theItemSupplyDB.update(itemSupply);
                    } else {
                        itemSupply.b_item_id = item.getObjectId();
                        itemSupply.record_date_time = this.theHO.date_time;
                        itemSupply.user_record_id = this.theHO.theEmployee.getObjectId();
                        theHosDB.theItemSupplyDB.insert(itemSupply);
                    }
                }
            } else if (cgiNew.category_group_code.equals(CategoryGroup.isXray())) {
                if (itemXray != null) {
                    itemXray.user_update_id = this.theHO.theEmployee.getObjectId();
                    if (itemXray.getObjectId() != null) {
                        theHosDB.theBItemXrayDB.update(itemXray);
                    } else {
                        itemXray.b_item_id = item.getObjectId();
                        itemXray.user_record_id = this.theHO.theEmployee.getObjectId();
                        theHosDB.theBItemXrayDB.insert(itemXray);
                    }
                }
            } else if (cgiNew.category_group_code.equals(CategoryGroup.isPackage())) {
                if (vItemPackage != null && !vItemPackage.isEmpty()) {
                    for (ItemPackage itemPackage : vItemPackage) {
                        itemPackage.b_item_id = item.getObjectId();
                        if (itemPackage.getObjectId() != null) {
                            theHosDB.theItemPackageDB.update(itemPackage);
                        } else {
                            theHosDB.theItemPackageDB.insert(itemPackage);
                        }
                    }
                }
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
            ans = 1;
        } catch (Exception ex) {
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            if (!(ex.getMessage() != null && ex.getMessage().equals("cn"))) {
                hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT, "��¡�õ�Ǩ�ѡ��");
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
            }
            if (isNew) {
                item.setObjectId(null);
            }
            ans = 0;
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, "��¡�õ�Ǩ�ѡ��");
        }
        return ans;
    }

    public String readMaxItemCodeByCat(String cat) {
        String string = "";
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Item item = theHosDB.theItemDB.selectMaxByCgi(cat);
            if (item != null) {
                string = item.item_id;
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return string;
    }

    public Item listItemByPk(String pk) {
        Item item = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            item = theHosDB.theItemDB.selectByPK(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return item;
    }

    public int deleteItemByPk(String pk) {
        int ans = 0;
        if (theLookupControl.readOption().life.equals(Active.isEnable())) {
            theUS.setStatus(ResourceBundle.getBundleText("�к����������ҹ����") + " "
                    + ResourceBundle.getBundleText("���ź�������������ö����"), UpdateStatus.WARNING);
            return ans;
        }
        if (!theUS.confirmBox(ResourceBundle.getBundleText("�׹�ѹ����ź��¡�ù��") + " "
                + ResourceBundle.getBundleText("�Ҩ�Դ�š�з��Ѻ������㹰ҹ��������"), UpdateStatus.WARNING)) {
            return 0;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            int count = theHosDB.theOrderItemDB.countItem(pk);
            if (count > 0) {
                theUS.setStatus(ResourceBundle.getBundleText("�������öź��¡�õ�Ǩ�ѡ�ҹ����") + " "
                        + ResourceBundle.getBundleText("�������ա�����������������"), UpdateStatus.WARNING);
                throw new Exception("cancel");
            }
            if (pk != null && !pk.substring(3).startsWith(theLO.theSite.off_id)) {
                theUS.setStatus(ResourceBundle.getBundleText("��¡�õ�Ǩ�ѡ�ҹ���繢ͧ�ҹ���������") + " "
                        + ResourceBundle.getBundleText("�������öź��") + " "
                        + ResourceBundle.getBundleText("��س� set active ᷹"), UpdateStatus.WARNING);
                throw new Exception("cancel");
            }

            theHosDB.theItemPackageDB.delete(pk);
            theHosDB.theItemDB.delete(pk);
            theHosDB.theItemPriceDB.delete(pk);
            theHosDB.theDrugDB.delete(pk);
            theHosDB.theLabSetDB.delete(pk);
            theHosDB.theLabGroupDB.delete(pk);
            theHosDB.theLabResultItemDB.delete(pk);
            theHosDB.theItemSupplyDB.delete(pk);
            theHosDB.theItemServiceDB.inactiveByItemID(pk);

            theConnectionInf.getConnection().commit();
            theUS.setStatus(ResourceBundle.getBundleText("���ź��¡�õ�Ǩ�ѡ��") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            return 1;
        } catch (Exception ex) {
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            if (!ex.getMessage().equals("cancel")) {
                LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
                theUS.setStatus(ResourceBundle.getBundleText("���ź��¡�õ�Ǩ�ѡ��") + " "
                        + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
            }
            return 0;
        } finally {
            theConnectionInf.close();
        }
    }

    public int deleteItemPrice(ItemPrice itemprice) {
        int ans = 0;
        if (itemprice == null || itemprice.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return ans;
        }
        if (theLookupControl.readOption().life.equals(Active.isEnable())) {
            theUS.setStatus(ResourceBundle.getBundleText("�к����������ҹ����") + " "
                    + ResourceBundle.getBundleText("���ź�������������ö����"), UpdateStatus.WARNING);
            return ans;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ans = theHosDB.theItemPriceDB.delete(itemprice);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public Vector listItemPrice(String pkItem_id) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theItemPriceDB.selectByItem(pkItem_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Drug readDrug(String pkItem_id) {
        Drug drug = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            drug = theHosDB.theDrugDB.selectByItem(pkItem_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return drug;
    }

    public Vector<Drug> readDrugV(String pkItem_id) {
        Vector<Drug> vector = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vector = theHosDB.theDrugDB.selectByItemV(pkItem_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vector;
    }

    public int deleteLabResultItem(LabResultItem lri) {
        int ans = 0;
        if (lri == null || lri.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return ans;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ans = theHosDB.theLabResultItemDB.delete(lri);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public int deleteLabResultItem(Vector vlri, int[] rows) {
        int ans = 0;
        if (rows.length == 0) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return ans;
        }
        if (!theUS.confirmBox(ResourceBundle.getBundleText("�׹�ѹ����ź��¡�ù��") + " "
                + ResourceBundle.getBundleText("�Ҩ�Դ�š�з��Ѻ������㹰ҹ��������"), UpdateStatus.WARNING)) {
            return 0;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (int i = rows.length - 1; i >= 0; i--) {
                LabResultItem lri = (LabResultItem) vlri.get(rows[i]);
                if (lri.getObjectId() != null) {
                    ans = theHosDB.theLabResultItemDB.delete(lri);
                }
                vlri.remove(rows[i]);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public LabResultItem readLabResultItem(String item_id) {
        LabResultItem item = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector vc = theHosDB.theLabResultItemDB.selectByItem(item_id);
            if (vc != null && !vc.isEmpty()) {
                item = (LabResultItem) vc.get(0);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return item;
    }

//    /**
//     * @deprecated �����
//     */
//    public int editLabResultItem(Vector labresult) {
//        int ans = 0;
//        theConnectionInf.open();
//        try {
//            for (int i = 0; i < labresult.size(); i++) {
//                theHosDB.theLabResultItemDB.update((LabResultItem) labresult.get(i));
//            }
//        } catch (Exception ex) {
//            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
//
//        }
//        theConnectionInf.close();
//        return ans;
//    }
    public int saveLabResultItem(LabResultItem lri) {
        int ans = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (lri.getObjectId() == null) {
                theHosDB.theLabResultItemDB.insert(lri);
            } else {
                theHosDB.theLabResultItemDB.update(lri);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

//    /**
//     * @deprecated unused Somprasong 28-06-2012
//     * @param labresultitem
//     * @param item_id
//     * @return
//     */
//    public int saveLabResultItem(Vector labresultitem, String item_id) {
//        int ans = 0;
//        theConnectionInf.open();
//        try {
//            theConnectionInf.getConnection().setAutoCommit(false);
//            for (int i = 0; i < labresultitem.size(); i++) {
//                if (((LabResultItem) labresultitem.get(i)).getObjectId() == null) {
//                    LabResultItem lri = (LabResultItem) labresultitem.get(i);
//                    lri.item_id = item_id;
//                    theHosDB.theLabResultItemDB.insert((LabResultItem) labresultitem.get(i));
//                }
//            }
//            theConnectionInf.getConnection().commit();
//        } catch (Exception ex) {
//            LOG.log(java.util.logging.Level.SEVERE, null, ex);
//            try {
//                theConnectionInf.getConnection().rollback();
//            } catch (SQLException ex1) {
//                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
//            }
//        } finally {
//            theConnectionInf.close();
//        }
//        return ans;
//    }
    public Vector listSearchItemDrug(String pk) {
        return listSearchItem(pk, 3, CategoryGroup.isDrug());
    }

    /**
     * ��㹡���� Item ����ͧ��õ���������¡��
     *
     * @param pk
     * @param sh
     * @param cgc
     * @return
     */
    public Vector listSearchItem(String pk, int sh, String cgc) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (sh == 2) {
                Vector categorygroup = theHosDB.theCategoryGroupItemDB.selectByCategoryGroupCode(cgc);
                for (int i = 0; categorygroup != null && categorygroup.size() > i; i++) {
                    CategoryGroupItem cat = (CategoryGroupItem) categorygroup.get(i);
                    Vector tempp = theHosDB.theItemDB.selectByItemGroup(cat.getObjectId(), pk, Active.isEnable());
                    if (tempp != null) {
                        for (int n = 0; n < tempp.size(); n++) {
                            vc.add(tempp.get(n));
                        }
                    }
                }
            }
            if (sh == 3) {
                Vector categorygroup = theHosDB.theCategoryGroupItemDB.selectByCategoryGroupCode(
                        CategoryGroup.isDrug());
                for (int i = 0; categorygroup != null && categorygroup.size() > i; i++) {
                    CategoryGroupItem cat = (CategoryGroupItem) categorygroup.get(i);
                    Vector tempp = theHosDB.theItemDB.selectByItemGroup(cat.getObjectId(), pk, Active.isEnable());
                    if (tempp != null && !tempp.isEmpty()) {
                        for (int n = 0; n < tempp.size(); n++) {
                            vc.add(tempp.get(n));
                        }
                    }
                }
            }
            if (sh == 4) {
                Vector categorygroup = theHosDB.theCategoryGroupItemDB.selectByCategoryGroupCode(
                        CategoryGroup.isXray());
                for (int i = 0; categorygroup != null && categorygroup.size() > i; i++) {
                    CategoryGroupItem cat = (CategoryGroupItem) categorygroup.get(i);
                    Vector tempp = theHosDB.theItemDB.selectByItemGroup(cat.getObjectId(), pk, Active.isEnable());
                    if (tempp != null && !tempp.isEmpty()) {
                        for (int n = 0; n < tempp.size(); n++) {
                            vc.add(tempp.get(n));
                        }
                    }
                }
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * @Author : amp
     * @date : 27/02/2549
     * @see : ������¡���Ż�������� �Ż���Դ
     * @param pk �Ӥ�
     * @return Vector ��¡�� Item �Ż�����軡�Դ
     */
    public Vector listSearchItemLabNotSecret(String pk) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector categorygroup = theHosDB.theCategoryGroupItemDB.selectByCategoryGroupCode(CategoryGroup.isLab());
            for (int i = 0; categorygroup != null && categorygroup.size() > i; i++) {
                CategoryGroupItem cat = (CategoryGroupItem) categorygroup.get(i);
                Vector tempp = theHosDB.theItemDB.selectItemLabNotSecret(cat.getObjectId(), pk);
                if (tempp != null) {
                    for (int n = 0; n < tempp.size(); n++) {
                        vc.add(tempp.get(n));
                    }
                }
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public int saveAutoReportBug(AutoReportBug autoReportBug) {
        String objectid = null;
        int ans = 0;
        if (autoReportBug.smtp_host.trim().isEmpty()) {
            theUS.setStatus("��سҡ�͡ SMTP SERVER", UpdateStatus.WARNING);
            return 3;
        }
        if (autoReportBug.user_name.trim().isEmpty()) {
            theUS.setStatus(ResourceBundle.getBundleText("��سҡ�͡") + " "
                    + ResourceBundle.getBundleText("���ͼ����"), UpdateStatus.WARNING);
            return 4;
        }
        if (autoReportBug.password.trim().isEmpty()) {
            theUS.setStatus(ResourceBundle.getBundleText("��سҡ�͡") + " "
                    + ResourceBundle.getBundleText("���ʼ�ҹ"), UpdateStatus.WARNING);
            return 5;
        }
        if (autoReportBug.mail_from.trim().isEmpty()) {
            theUS.setStatus(ResourceBundle.getBundleText("��سҡ�͡") + " "
                    + ResourceBundle.getBundleText("����Ũҡ"), UpdateStatus.WARNING);
            return 6;
        }
        if (autoReportBug.mail_from.trim().isEmpty()) {
            theUS.setStatus(ResourceBundle.getBundleText("��سҡ�͡") + " "
                    + ResourceBundle.getBundleText("�������ѧ"), UpdateStatus.WARNING);
            return 7;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (autoReportBug.getObjectId() == null) {
                ans = theHosDB.theAutoReportBugDB.insert(autoReportBug);
            } else {
                ans = theHosDB.theAutoReportBugDB.update(autoReportBug);
            }
            this.theLookupControl.readAutoReportBug();
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        }
        return ans;
    }

    public int saveSite(Site site) {
        int ans = 0;
        if (site.off_id.trim().isEmpty()) {
            theUS.setStatus("��سҡ�͡����ʶҹ��Һ��", UpdateStatus.WARNING);
            return ans;
        }
        if (site.off_name.trim().isEmpty()) {
            theUS.setStatus("��سҡ�͡�������ʶҹ��Һ�", UpdateStatus.WARNING);
            return ans;
        }
        if (site.full_name.trim().isEmpty()) {
            theUS.setStatus("��سҡ�͡�������ʶҹ��Һ��", UpdateStatus.WARNING);
            return ans;
        }
        if (site.admin.trim().isEmpty()) {
            theUS.setStatus("��سҡ�͡���ͼ������к�", UpdateStatus.WARNING);
            return ans;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (site.getObjectId() == null) {
                ans = theHosDB.theSiteDB.insert(site);
            } else {
                ans = theHosDB.theSiteDB.update(site);
            }
            theLO.theSite = site;
            theHO.initSite();
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(ResourceBundle.getBundleText("��úѹ�֡������ʶҹ��Һ��") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.COMPLETE);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theUS.setStatus(ResourceBundle.getBundleText("��úѹ�֡������ʶҹ��Һ��") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        }
        return ans;
    }

    public int editSite(Site site) {
        int ans = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theSiteDB.update(site);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public Plan getPlanById(String id) {
        Plan plan = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            plan = theHosDB.thePlanDB.selectByPK(id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return plan;
    }

    public Vector listPlan(String pk, String active) {
        Vector vector = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vector = theHosDB.thePlanDB.selectByCNA(pk, active);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vector;
    }

    /**
     * @Author : sumo
     * @date : 20/03/2549
     * @see : �ѹ�֡�������Է�ԡ���ѡ��
     * @param plan Object �ͧ�������Է�ԡ���ѡ��
     * @return : int
     */
    public int savePlan(Plan plan) {
        int ans = 0;
        if (plan.plan_id.isEmpty()) {
            theUS.setStatus("��سҡ�͡�����Է�ԡ���ѡ�ҡ�͹�ӡ�á������ѹ�֡", UpdateStatus.WARNING);
            return 0;
        }
        if (plan.description.isEmpty()) {
            theUS.setStatus("��سҡ�͡�����Է�ԡ���ѡ�ҡ�͹�ӡ�á������ѹ�֡", UpdateStatus.WARNING);
            return 0;
        }
        if (plan.pttype.isEmpty()) {
            theUS.setStatus("��سҡ�͡ PT Type ��͹�ӡ�á������ѹ�֡", UpdateStatus.WARNING);
            return 0;
        }

        try {
            Double.parseDouble(plan.money_limit);
        } catch (Exception ex) {
            theUS.setStatus("��س��к�ǧ�Թ�繵���Ţ", UpdateStatus.WARNING);
            return 0;
        }
        try {
            Double.parseDouble(plan.sort_index);
        } catch (Exception ex) {
            theUS.setStatus("��س��к��ӴѺ������§�繵���Ţ ����� 2 ��ѡ", UpdateStatus.WARNING);
            return 0;
        }
        if (plan.sort_index.length() != 2) {
            theUS.setStatus("��س��к��ӴѺ������§�繵���Ţ ����� 2 ��ѡ", UpdateStatus.WARNING);
            return 0;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Plan xr = theHosDB.thePlanDB.selectByCode(plan.plan_id);
            if (xr != null && plan.getObjectId() == null) {
                theUS.setStatus(ResourceBundle.getBundleText("�������ö�ѹ�֡�����Է�ԡ���ѡ�ҫ����") + " "
                        + ResourceBundle.getBundleText("��سҵ�Ǩ�ͺ�����Է�ԡ���ѡ���ա����"), UpdateStatus.WARNING);
                throw new Exception(ResourceBundle.getBundleText("�������ö�ѹ�֡�����Է�ԡ���ѡ�ҫ����") + " "
                        + ResourceBundle.getBundleText("��سҵ�Ǩ�ͺ�����Է�ԡ���ѡ���ա����"));
            }
            if (xr != null && plan.getObjectId() != null
                    && !plan.getObjectId().equals(xr.getObjectId())) {
                theUS.setStatus(ResourceBundle.getBundleText("�������ö�ѹ�֡�����Է�ԡ���ѡ�ҫ����") + " "
                        + ResourceBundle.getBundleText("��سҵ�Ǩ�ͺ�����Է�ԡ���ѡ���ա����"), UpdateStatus.WARNING);
                throw new Exception(ResourceBundle.getBundleText("�������ö�ѹ�֡�����Է�ԡ���ѡ�ҫ����") + " "
                        + ResourceBundle.getBundleText("��سҵ�Ǩ�ͺ�����Է�ԡ���ѡ���ա����"));
            }
            if (plan.getObjectId() == null) {
                theHosDB.thePlanDB.insert(plan);
            } else {
                theHosDB.thePlanDB.update(plan);
            }
            theLO.thePlanActive = theHosDB.thePlanDB.selectByCNA("", Active.isEnable());
            ans = 1;
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            ans = 0;
        } finally {
            theConnectionInf.close();
        }
        if (ans == 1) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        }
        return ans;
    }

    public int deletePlan(Plan plan) {
        int ans = 0;
        if (plan == null || plan.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return ans;
        }
//        if (plan == null) {
//            theUS.setStatus("��س����͡�Է�ԡ���ѡ�ҡ�͹�ӡ�á�����ź", UpdateStatus.WARNING);
//            return ans;
//        }
        if (plan.getObjectId().startsWith("21200000")) {
            theUS.setStatus(ResourceBundle.getBundleText("�������öź�������Է�Ԣͧ�к���") + " "
                    + ResourceBundle.getBundleText("��س����� inActive"), UpdateStatus.WARNING);
            return -1;
        }
        theConnectionInf.open();
        try {
            Vector vp_used = theHosDB.thePaymentDB.selectByPlanId(plan.getObjectId());
            if (!vp_used.isEmpty()) {
                theUS.setStatus("�ա����ҹ�Է�Թ��㹡���Ѻ��ԡ�������������öź��", UpdateStatus.WARNING);
                throw new Exception("�ա����ҹ�Է�Թ��㹡���Ѻ��ԡ�������������öź��");
            }
            Vector pp_used = theHosDB.thePatientPaymentDB.selectByPlanId(plan.getObjectId());
            if (!pp_used.isEmpty()) {
                theUS.setStatus("�ա�á�˹��Է�Թ�����Ѻ�����������������öź��", UpdateStatus.WARNING);
                throw new Exception("�ա�á�˹��Է�Թ�����Ѻ�����������������öź��");
            }
            if (!theUS.confirmBox(ResourceBundle.getBundleText("�׹�ѹ����ź��¡�ù��") + " "
                    + ResourceBundle.getBundleText("�Ҩ�Դ�š�з��Ѻ������㹰ҹ��������"), UpdateStatus.WARNING)) {
                ans = -1;
                throw new Exception();

            }
            theHosDB.thePlanDB.delete(plan);
            theLO.thePlanActive = theHosDB.thePlanDB.selectByCNA("", Active.isEnable());
            theConnectionInf.getConnection().commit();
            ans = 1;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_DELETE, ex);
        } finally {
            theConnectionInf.close();
        }
        if (ans == 1) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE);
        }
        return ans;
    }

    public int editPlan(Plan plan) {
        int ans = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.thePlanDB.update(plan);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public Vector listContractGroupByNameId(String search) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theContractDB.selectByNameId(search);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * @Author : sumo
     * @date : 15/03/2549
     * @see : �ѹ�֡��������¡����ǹŴ
     * @param contract Object �ͧ��������¡����ǹŴ
     * @return : int
     */
    public int saveContract(Contract contract) {
        int ans = 0;
        if (contract.contract_id.isEmpty()) {
            theUS.setStatus("��س��к����ʡ�͹�ӡ�úѹ�֡", UpdateStatus.WARNING);
            return ans;
        }
        if (contract.description.isEmpty()) {
            theUS.setStatus("��س��кت�����ǹŴ��͹�ӡ�úѹ�֡", UpdateStatus.WARNING);
            return ans;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Contract xr = theHosDB.theContractDB.selectByCode(contract.contract_id);
            if (xr != null && contract.getObjectId() == null) {
                theUS.setStatus(ResourceBundle.getBundleText("�������ö�ѹ�֡���ʫ����") + " "
                        + ResourceBundle.getBundleText("��سҵ�Ǩ�ͺ�����ա����"), UpdateStatus.WARNING);
                throw new Exception();
            }
            if (xr != null && contract.getObjectId() != null
                    && !contract.getObjectId().equals(xr.getObjectId())) {
                theUS.setStatus(ResourceBundle.getBundleText("�������ö�ѹ�֡���ʫ����") + " "
                        + ResourceBundle.getBundleText("��سҵ�Ǩ�ͺ�����ա����"), UpdateStatus.WARNING);
                throw new Exception();
            }
            if (contract.getObjectId() != null) {
                theHosDB.theContractDB.update(contract);
                theUS.setStatus(ResourceBundle.getBundleText("��䢢�������¡����ǹŴ") + " "
                        + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            } else {
                theHosDB.theContractDB.insert(contract);
                Vector cData = HosObject.initContractAdjustV(contract, theLookupControl.listCategoryGroupItem());
                for (int i = 0, size = cData.size(); i < size; i++) {
                    ContractAdjust ca = (ContractAdjust) cData.get(i);
                    ans = theHosDB.theContractAdjustDB.insert(ca);
                }
                theUS.setStatus(ResourceBundle.getBundleText("�ѹ�֡�������¡����ǹŴ") + " "
                        + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            }
            theLO.theContract = theHosDB.theContractDB.selectAll();
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_DELETE, ex);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        }
        return ans;
    }

    /**
     * @Author : sumo
     * @date : 15/03/2549
     * @see : ź��������¡����ǹŴ
     * @param contract Object �ͧ��������¡����ǹŴ������͡
     * @param row �Ƿ�����͡
     * @return int
     */
    public int deleteContract(Contract contract, int row) {
        int ans = 0;
        if (theLookupControl.readOption().life.equals(Active.isEnable())) {
            theUS.setStatus(ResourceBundle.getBundleText("�к����������ҹ����") + " "
                    + ResourceBundle.getBundleText("���ź�������������ö����"), UpdateStatus.WARNING);
            return ans;
        }
        if (contract == null || contract.getObjectId() == null) {
            theUS.setStatus(ResourceBundle.getBundleText("��س����͡��¡�÷���ͧ���ź"), UpdateStatus.WARNING);
            return ans;
        }
        if (row == -1) {
            theUS.setStatus("��س����͡��¡����ǹŴ��͹�ӡ�á�����ź", UpdateStatus.WARNING);
            return ans;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector vc = theHosDB.thePlanDB.selectByContract(contract.getObjectId());
            if (vc != null && !vc.isEmpty()) {
                theUS.setStatus(ResourceBundle.getBundleText("�������öź��ǹŴ��¡�ù����") + " "
                        + ResourceBundle.getBundleText("���ͧ�ҡ���Է�ԡ���ѡ�ҷ������ǹŴ�������"), UpdateStatus.WARNING);
                throw new Exception();
            }
            boolean confirm = theUS.confirmBox(ResourceBundle.getBundleText("�׹�ѹ����ź��¡�ù��") + " "
                    + ResourceBundle.getBundleText("�Ҩ�Դ�š�з��Ѻ������㹰ҹ��������"), UpdateStatus.WARNING);
            if (!confirm) {
                throw new Exception();
            }
            theHosDB.theContractDB.delete(contract);
            theHosDB.theContractAdjustDB.deleteByCid(contract.getObjectId());
            theLO.theContract = theHosDB.theContractDB.selectAll();
            theConnectionInf.getConnection().commit();
            isComplete = true;
            ans = 1;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_DELETE);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE);
        }
        return ans;
    }

    public Vector listContractAdjust(String search) {
        Vector list = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            list = theHosDB.theContractAdjustDB.selectByCid(search);
            String sql = " select * from b_item_subgroup "
                    + " where b_item_subgroup_id not in "
                    + "   (select b_item_subgroup_id from b_contract_condition where b_contract_id = '" + search + "')"
                    + " and item_subgroup_active = '1'";
            Vector subgroupV = theHosDB.theCategoryGroupItemDB.eQuery(sql);
            Vector cData = HosObject.initContractAdjustV(search, subgroupV);
            for (int i = 0; i < cData.size(); i++) {
                list.add(cData.get(i));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public int editContractAdjust(ContractAdjust contractadjust) {
        int ans = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (contractadjust.getObjectId() != null) {
                theHosDB.theContractAdjustDB.update(contractadjust);
            } else {
                theHosDB.theContractAdjustDB.insert(contractadjust);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public int saveContractAdjust(Vector cData) {
        int ans = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (cData == null) {
                cData = new Vector();
            }
            for (int i = 0, size = cData.size(); i < size; i++) {
                ContractAdjust ca = (ContractAdjust) cData.get(i);
                ans = theHosDB.theContractAdjustDB.insert(ca);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public int saveContractAdjust(ContractAdjust contractadjust) {
        int ans = 0;
        if (contractadjust.adjustment.isEmpty()) {
            theUS.setStatus("��س��к���ǹŴ㹪�ǧ 0-100 ��ҹ��", UpdateStatus.WARNING);
            return 0;
        }
        double a = Double.parseDouble(contractadjust.adjustment);
        if (a < 0 || a > 100) {
            theUS.setStatus("��ǹŴ��ͧ����㹪�ǧ 0-100 ��ҹ��", UpdateStatus.WARNING);
            return 0;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (contractadjust.getObjectId() == null) {
                ans = theHosDB.theContractAdjustDB.insert(contractadjust);
            } else {
                ans = theHosDB.theContractAdjustDB.update(contractadjust);
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            ans = 0;
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        }
        return ans;
    }

    /**
     * @author henbe ���Ҥҷ����������¡����
     * @param vCA
     * @param rows
     * @param adjust
     * @return
     */
    public int saveContractAdjust(Vector vCA, int[] rows, String adjust) {
        int ans = 0;
        if (adjust.isEmpty()) {
            theUS.setStatus("��س��к���ǹŴ㹪�ǧ 0-100 ��ҹ��", UpdateStatus.WARNING);
            return 0;
        }
        double a = Double.parseDouble(adjust);
        if (a < 0 || a > 100) {
            theUS.setStatus("��ǹŴ��ͧ����㹪�ǧ 0-100 ��ҹ��", UpdateStatus.WARNING);
            return 0;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (int i = 0; i < rows.length; i++) {
                ContractAdjust contractadjust = (ContractAdjust) vCA.get(rows[i]);
                contractadjust.adjustment = adjust;
                ans = theHosDB.theContractAdjustDB.updateAdjust(contractadjust);
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            theUS.setStatus(ResourceBundle.getBundleText("��úѹ�֡��ǹŴ") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
            ans = 0;
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theUS.setStatus(ResourceBundle.getBundleText("��úѹ�֡��ǹŴ") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        }
        return ans;
    }

    /**
     * @Author : sumo
     * @date : 15/03/2549
     * @see : ź��������¡�á���� Order �����㹡�÷���ǹŴ�ҡ���ҧ
     * @param contractadjust Vector �ͧ��������¡�á���� Order ������͡
     * @param rows int[] �ͧ�Ƿ�����͡
     * @return : int
     */
    public int deleteContractAdjust(Vector contractadjust, int[] rows) {
        int ans = 0;
        if (rows.length == 0) {
            theUS.setStatus("��س����͡��¡����ǹŴ", UpdateStatus.WARNING);
            return ans;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (int i = rows.length - 1; i >= 0; i--) {
                ContractAdjust contract = (ContractAdjust) contractadjust.get(rows[i]);
                theHosDB.theContractAdjustDB.delete(contract);
                contractadjust.remove(rows[i]);
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;

            ans = 1;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_DELETE);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE);
        }
        return ans;
    }

    public Office getOfficeById(String name) {
        return this.theLookupControl.getOfficeById(name);
    }

    public Vector listOfficeByName(String name) {
        return this.theLookupControl.listOfficeByName(name);
    }

    public void saveLabGroupLabSet(LabGroup labgroup, Vector labset) {

        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (labgroup.getObjectId() == null) {
                theHosDB.theLabGroupDB.insert(labgroup);
                if (labset.size() > 0) {
                    for (int i = 0; i < labset.size(); i++) {
                        if (((LabSet) labset.get(i)).getObjectId() == null) {
                            ((LabSet) labset.get(i)).lab_group_id = labgroup.getObjectId();
                            theHosDB.theLabSetDB.insert(((LabSet) labset.get(i)));
                        } else {
                            theHosDB.theLabSetDB.update((LabSet) labset.get(i));
                        }
                    }
                }
            } else {
                theHosDB.theLabGroupDB.update(labgroup);
                if (labset.size() > 0) {
                    for (int i = 0; i < labset.size(); i++) {
                        if (((LabSet) labset.get(i)).getObjectId() == null) {
                            ((LabSet) labset.get(i)).lab_group_id = labgroup.getObjectId();
                            theHosDB.theLabSetDB.insert(((LabSet) labset.get(i)));
                        } else {
                            theHosDB.theLabSetDB.update((LabSet) labset.get(i));
                        }
                    }
                }
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
    }

    public Vector listLabSetByLabGroupID(String labgroup_id) {
        Vector v = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            v = theHosDB.theLabSetDB.selectByLabGroupID(labgroup_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return v;
    }

    public Vector listLabSetByItemId(String item_id) {
        Vector v = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            v = intListLabSetByItemId(item_id, "1");
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return v;
    }

    public Vector intListLabSetByItemId(String item_id, String sort) throws Exception {
        Vector v = new Vector();
        String sql = " select b_item_lab_group.*"
                + " ,item_detail.item_common_name as item_name"
                + " ,case when item_detail_sub.item_lab_set_description is null"
                + "  then '0' else '1' end as has_sub"
                + " from b_item_lab_group "
                + " left join b_item_lab_set on b_item_lab_set.b_item_lab_set_id = b_item_lab_group.b_item_lab_set_id"
                + " left join b_item as item_detail on item_detail.b_item_id = b_item_lab_group.b_item_id"
                + " left join b_item_lab_set as item_detail_sub on item_detail_sub.b_item_id = item_detail.b_item_id"
                + " where b_item_lab_set.b_item_id = '" + item_id + "'";
        if (sort.equals("sort")) {
            sql += " order by item_lab_group_item_name";
        }

        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            LabSet ls = theHosDB.theLabSetDB.rs2Object(rs);
            //ls.item_common_name =
            ls.has_sub = rs.getString("has_sub");
            ls.item_common_name = rs.getString("item_name");
            v.add(ls);
        }
        rs.close();
        return v;
    }

    public LabGroup listLabGroupByItem(String item_id) {
        LabGroup lg = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            lg = theHosDB.theLabGroupDB.selectByItemID(item_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return lg;
    }

    public void deleteLabSet(Vector vlabset, int[] rows) {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (int i = rows.length - 1; i >= 0; i--) {
                LabSet lri = (LabSet) vlabset.get(rows[i]);
                if (lri.getObjectId() != null) {
                    theHosDB.theLabSetDB.delete(lri);
                }
                vlabset.remove(rows[i]);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
    }

    public Vector listEmployeeOr(String keyword) {
        Vector vector = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vector = theHosDB.theEmployeeDB.selectAllByName(keyword, Active.isEnable());
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vector;
    }

    public ICD9 listIcd9ByPk(String pk) {
        ICD9 icd9 = null;
        if (pk.isEmpty()) {
            return null;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            icd9 = theHosDB.theICD9DB.selectByPK(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return icd9;
    }

    public ICD9 listIcd9ById(String pk) {
        ICD9 icd9 = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            icd9 = theHosDB.theICD9DB.selectById(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return icd9;
    }

    public Vector listLabResultGroup(String pk) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (pk.isEmpty()) {
                vc = theHosDB.theLabResultGroupDB.selectLabResultGroupAll();
            } else {
                vc = theHosDB.theLabResultGroupDB.selectByCode(pk);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * @Author : sumo
     * @date : 14/03/2549
     * @see : ������¡���Ҫش
     * @param pk String �ͧ�Ӥ鹢ͧ�����Ҫش
     * @param doctor String ���ʢͧᾷ��
     * @return : Vector �ͧ������¡���Ҫش
     */
    public Vector listDrugSetGroup(String pk, String doctor) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            //�ó�����кؤӤ��������к�ᾷ��
            if (pk.isEmpty() && doctor.isEmpty()) {
                vc = theHosDB.theDrugSetGroupDB.selectAll();
            }
            //�ó�����кؤӤ����к�ᾷ��
            if (pk.isEmpty() && !doctor.isEmpty()) {
                vc = theHosDB.theDrugSetGroupDB.selectByDoctor(doctor);
            } //�ó��кؤӤ��������к�ᾷ�� ���� �кط�駤Ӥ����ᾷ��
            else {
                vc = theHosDB.theDrugSetGroupDB.selectByNameAndOrDoctor("%" + pk + "%", doctor);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listDrugSetByGroup(String pk) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theDrugSetDB.eQuery("select b_item_set.* from b_item_set"
                    + " inner join b_item on "
                    + "     (b_item.b_item_id = b_item_set.b_item_id and b_item.item_active = '1')"
                    + " where b_item_group_id = '" + pk + "'");
//                    + " order by b_item.item_common_name");
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * @Author : sumo
     * @date : 20/03/2549
     * @see : ź�������¡���Ҫش
     * @param drugsetgroup Object �ͧ�����š������¡���Ҫش������͡
     * @return : int
     */
    public int deleteDrugSetGroup(DrugSetGroup drugsetgroup) {
        int ans = 0;
        if (drugsetgroup == null || drugsetgroup.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return ans;
        }
//        if (drugsetgroup == null) {
//            theUS.setStatus("��س����͡�ش��õ�Ǩ�ѡ�ҡ�͹�ӡ�á�����ź", UpdateStatus.WARNING);
//            return ans;
//        }
        boolean confirm = theUS.confirmBox(ResourceBundle.getBundleText("�׹�ѹ����ź��¡�ù��") + " "
                + ResourceBundle.getBundleText("�Ҩ�Դ�š�з��Ѻ������㹰ҹ��������"), UpdateStatus.WARNING);
        if (!confirm) {
            return ans;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theDrugSetGroupDB.delete(drugsetgroup);
            theHosDB.theDrugSetDB.deleteByGroup(drugsetgroup.getObjectId());
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_DELETE);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE);
        }
        return ans;
    }

    public int deleteDrugSetByGroup(String pk) {
        int ans = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theDrugSetDB.deleteByGroup(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public int deleteDrugSetReq(DrugSet drugset) {
        int ans = 0;
        if (drugset == null || drugset.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return ans;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theDrugSetDB.delete(drugset);
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_DELETE);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE);
        }
        return ans;
    }

    /**
     * @Author : sumo
     * @date : 15/03/2549
     * @see : �ѹ�֡��������¡����¡�êش��Ǩ�ѡ��
     * @param drugSetGroup Object �ͧ��¡�êش��Ǩ�ѡ��
     * @param drugSet Vector �ͧ��¡�� Order ����������¡�êش��Ǩ�ѡ��
     * @return : void
     */
    public int saveDrugSetGroup(DrugSetGroup drugSetGroup, Vector drugSet) {
        if (drugSetGroup.descroption.isEmpty()) {
            theUS.setStatus("��س��кت��ͪش��͹�ӡ�úѹ�֡", UpdateStatus.WARNING);
            return 0;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            DrugSetGroup xr = theHosDB.theDrugSetGroupDB.selectByNameAndDoctor(drugSetGroup.descroption, drugSetGroup.oner_drug);
            if (xr != null && drugSetGroup.getObjectId() == null) {
                theUS.setStatus(ResourceBundle.getBundleText("�������ö�ѹ�֡���ͪش�������") + " "
                        + ResourceBundle.getBundleText("��سҵ�Ǩ�ͺ���ͪش�ա����"), UpdateStatus.WARNING);
                throw new Exception();
            }
            if (xr != null && drugSetGroup.getObjectId() != null
                    && !drugSetGroup.getObjectId().equals(xr.getObjectId())) {
                theUS.setStatus(ResourceBundle.getBundleText("�������ö�ѹ�֡���ͪش�������") + " "
                        + ResourceBundle.getBundleText("��سҵ�Ǩ�ͺ���ͪش�ա����"), UpdateStatus.WARNING);
                throw new Exception();
            }

            if (drugSetGroup.getObjectId() == null) {
                theHosDB.theDrugSetGroupDB.insert(drugSetGroup);
            } else {
                theHosDB.theDrugSetGroupDB.update(drugSetGroup);
            }
            //�ó�����բ����� Order �ش��õ�Ǩ�ѡ�Ҩ���������
            if (drugSet != null && !drugSet.isEmpty()) {
                saveDrugSet(drugSet, drugSetGroup.getObjectId());
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        }
        return isComplete ? 1 : 0;
    }

    /**
     * @Author : sumo
     * @date : 20/03/2549
     * @see : �ѹ�֡��Դ��§ҹ���Ż
     * @param labResultGroup Object �ͧ�����Ū�Դ��§ҹ���Ż
     * @return : int
     *
     * if(vd.size() != 0){ for(int i = 0;i<vd.size();i++) { LabResultDetail ld =
     * (LabResultDetail) vd.get(i);
     * theHC.theSetupControl.saveLabResuleDetail(ld); } }
     *
     */
    public int saveLabResuleType(LabResultGroup labResultGroup) {
        return saveLabResuleType(labResultGroup, null);
    }

    public int saveLabResuleType(LabResultGroup labResultGroup, Vector vLRD) {
        int ans = 0;
        if (labResultGroup.number.isEmpty()) {
            theUS.setStatus("��سҡ�͡���ʪش��͹�ӡ�á������ѹ�֡", UpdateStatus.WARNING);
            return ans;
        }
        if (labResultGroup.name.isEmpty()) {
            theUS.setStatus("��سҡ�͡���ͪش��͹�ӡ�á������ѹ�֡", UpdateStatus.WARNING);
            return ans;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            LabResultGroup lg = theHosDB.theLabResultGroupDB.selectByCode1(labResultGroup.number);
            if (lg != null && labResultGroup.getObjectId() == null) {
                theUS.setStatus(ResourceBundle.getBundleText("�������ö�ѹ�֡���ʪش�����") + " "
                        + ResourceBundle.getBundleText("��سҵ�Ǩ�ͺ���ʪش�ա����"), UpdateStatus.WARNING);
                throw new Exception();
            }
            if (lg != null && labResultGroup.getObjectId() != null
                    && !labResultGroup.getObjectId().equals(lg.getObjectId())) {
                theUS.setStatus(ResourceBundle.getBundleText("�������ö�ѹ�֡���ʪش�����") + " "
                        + ResourceBundle.getBundleText("��سҵ�Ǩ�ͺ���ʪش�ա����"), UpdateStatus.WARNING);
                throw new Exception();
            }
            if (labResultGroup.getObjectId() == null) {
                theHosDB.theLabResultGroupDB.insert(labResultGroup);
            } else {
                theHosDB.theLabResultGroupDB.update(labResultGroup);
            }

            if (vLRD != null && !vLRD.isEmpty()) {
                for (int i = 0; i < vLRD.size(); i++) {
                    LabResultDetail ld = (LabResultDetail) vLRD.get(i);
                    ld.result_id = labResultGroup.getObjectId();
                    saveLabResuleDetail(ld);
                }
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
            ans = 1;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        }
        return ans;
    }

    /**
     * @Author : sumo
     * @date : 20/03/2549
     * @see : �ѹ�֡��������´�ͧ���Ż
     * @param labResultDetail Object �ͧ��������������´�ͧ���Ż
     * @return : int
     */
    public int saveLabResuleDetail(LabResultDetail labResultDetail) {
        int ans = 0;
        if (labResultDetail == null) {
            theUS.setStatus("��س��к���������´�ͧ���Ż��͹�ӡ�á������ѹ�֡", UpdateStatus.WARNING);
            return ans;
        }
        if (labResultDetail.value.trim().isEmpty()) {
            theUS.setStatus("��س��кؤ�ҷ�����������ҧ��͹�ӡ�á������ѹ�֡", UpdateStatus.WARNING);
            return ans;
        }
        labResultDetail.value = labResultDetail.value.trim();
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            LabResultDetail ld = theHosDB.theLabResultDetailDB.selectByResultGid(labResultDetail.value, labResultDetail.result_id);
            if (ld != null && labResultDetail.getObjectId() == null) {
                theUS.setStatus(ResourceBundle.getBundleText("�������ö��������´�ͧ���Ż�ѹ�֡��") + " "
                        + ResourceBundle.getBundleText("��سҵ�Ǩ�ͺ��������´�ͧ���Ż�ա����"), UpdateStatus.WARNING);
                throw new Exception();
            }
            if (ld != null && labResultDetail.getObjectId() != null
                    && !labResultDetail.getObjectId().equals(ld.getObjectId())) {
                theUS.setStatus(ResourceBundle.getBundleText("�������ö��������´�ͧ���Ż�ѹ�֡��") + " "
                        + ResourceBundle.getBundleText("��سҵ�Ǩ�ͺ��������´�ͧ���Ż�ա����"), UpdateStatus.WARNING);
                throw new Exception();
            }
            if (labResultDetail.getObjectId() == null) {
                theHosDB.theLabResultDetailDB.insert(labResultDetail);
                theUS.setStatus(ResourceBundle.getBundleText("��úѹ�֡��Դ��§ҹ���Ż") + " "
                        + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            } else {
                theHosDB.theLabResultDetailDB.update(labResultDetail);
                theUS.setStatus(ResourceBundle.getBundleText("�����䢪�Դ��§ҹ���Ż") + " "
                        + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        }
        return ans;
    }

    public int saveDrugSet(Vector drugSet, String dsgId) {
        int ans = 0;

        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (drugSet == null) {
                theUS.setStatus("����բ������ҷ��зӡ�úѹ�֡", UpdateStatus.COMPLETE);
                return 0;
            }
            for (int i = 0; i < drugSet.size(); i++) {
                DrugSet ds = (DrugSet) drugSet.get(i);
                ds.drug_set_group_key_id = dsgId;
                if (ds.getObjectId() == null) {
                    theHosDB.theDrugSetDB.insert(ds);
                } else {
                    theHosDB.theDrugSetDB.update(ds);
                }
            }
            theUS.setStatus(ResourceBundle.getBundleText("�ѹ�֡��¡���Ҫش") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public int saveDoseDrugSet(Vector doseDrugSet) {
        int ans = 0;

        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (doseDrugSet != null) {
                for (int i = 0; i < doseDrugSet.size(); i++) {
                    if (doseDrugSet.get(i) != null) {
                        if (((DoseDrugSet) doseDrugSet.get(i)).getObjectId() == null) {
                            theHosDB.theDoseDrugSetDB.insert((DoseDrugSet) doseDrugSet.get(i));
                        } else {
                            theHosDB.theDoseDrugSetDB.update((DoseDrugSet) doseDrugSet.get(i));
                        }
                    }
                }

            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public DoseDrugSet listDoseDrugSet(String key_drugset) {
        DoseDrugSet dds = new DoseDrugSet();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            dds = theHosDB.theDoseDrugSetDB.selectByKeyDrugSet(key_drugset);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return dds;
    }

    /**
     * @Author : sumo
     * @date : 20/03/2549
     * @see : ź��¡�� Order ��Ҫش
     * @param doseDrugSet Object �ͧ������ź��¡�� Order ��Ҫش������͡
     * @return : int
     */
    public int deleteDoseDrugSet(DoseDrugSet doseDrugSet) {
        int ans = 0;
        if (doseDrugSet == null || doseDrugSet.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return ans;
        }
//        if (doseDrugSet == null) {
//            theUS.setStatus("��س����͡��¡�� Order ��͹�ӡ�á�����ź", UpdateStatus.WARNING);
//            return ans;
//        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theDoseDrugSetDB.delete(doseDrugSet);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_DELETE);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE);
        }
        return isComplete ? 1 : 0;
    }

    public void saveOffice(Office theOffice) {
        if ((theOffice.pk_field.isEmpty()) || (theOffice.name.isEmpty())) {
            theUS.setStatus("�������ö�ѹ�֡�������繤����ҧ��", UpdateStatus.WARNING);
            return;
        }
        Office off = theLookupControl.readHospitalByCode(theOffice.pk_field);
        if (off != null) {
            boolean ret = theUS.confirmBox(ResourceBundle.getBundleText("�׹�ѹ�����䢢����Ţͧʶҹ��Һ������") + " " + theOffice.pk_field, UpdateStatus.WARNING);
            if (!ret) {
                return;
            }
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (off == null) {
                theHosDB.theOfficeDB.insert(theOffice);
                theUS.setStatus(ResourceBundle.getBundleText("�ѹ�֡ʶҹ��Һ��") + " "
                        + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            } else {
                theHosDB.theOfficeDB.update(theOffice);
                theUS.setStatus(ResourceBundle.getBundleText("���ʶҹ��Һ��") + " "
                        + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        }
    }

    public int deleteOffice(Office office) {
        int ans = 0;
        if (theLookupControl.readOption().life.equals(Active.isEnable())) {
            theUS.setStatus(ResourceBundle.getBundleText("�к����������ҹ����") + " "
                    + ResourceBundle.getBundleText("���ź�������������ö����"), UpdateStatus.WARNING);
            return ans;
        }
        if (office == null || office.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return ans;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theOfficeDB.delete(office);
            theConnectionInf.getConnection().commit();
            theUS.setStatus(ResourceBundle.getBundleText("źʶҹ��Һ��") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(ResourceBundle.getBundleText("źʶҹ��Һ��") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public int savePassword(Employee employee) {
        int ans = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theEmployeeDB.updatePassword(employee);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public boolean checkDxTemplate(String dx) {
        boolean result = true;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            DxTemplate answer = theHosDB.theDxTemplate2DB.selectByPK(dx);
            theConnectionInf.close();
            if (answer == null) {
                result = false;
            } else {
                result = true;
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    /**
     * �ѹ�֡�������¡���ä������ѧ ������� ICD10
     * �������㹡�����ä������ѧ���С����
     *
     * @param groupchronic �� Object �ͧ������ä������ѧ
     * @param vIcd10AllGroup ������ ICD10 ����������� �ͧ������ä������ѧ
     * @param vIcd10SpecifyCode ������ ICD10 ����������੾��
     * �ͧ������ä������ѧ
     * @return
     * @author pu
     * @date 15/09/2008
     * @UC hv3uc_241 saveGroupChronic
     */
    public int saveGroupChronicICD10(GroupChronic groupchronic, Vector vIcd10AllGroup, Vector vIcd10SpecifyCode) {
        int ans = 0;
        if (groupchronic == null) {
            theUS.setStatus("��س��к����ʡ�����ä������ѧ", UpdateStatus.WARNING);
            return ans;
        }
        if (groupchronic.group_chronic_id.isEmpty()) {
            theUS.setStatus("��س��к����ʡ�����ä������ѧ", UpdateStatus.WARNING);
            return ans;
        }
        if (groupchronic.description_th.isEmpty() || groupchronic.description_en.isEmpty()) {
            theUS.setStatus("��س��кت��͡�����ä������ѧ", UpdateStatus.WARNING);
            return ans;
        }

        if ((vIcd10AllGroup == null || vIcd10AllGroup.isEmpty()) && (vIcd10SpecifyCode == null || vIcd10SpecifyCode.isEmpty())) {
            theUS.setStatus("��س��к����� ICD10 ����Ѻ������ä������ѧ", UpdateStatus.WARNING);
            return ans;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            GroupChronic cr = theHosDB.theGroupChronicDB.selectByCode(groupchronic.group_chronic_id);
            if (cr != null) {
                if (groupchronic.getObjectId() == null || !groupchronic.getObjectId().equals(cr.getObjectId())) {
                    theUS.setStatus(ResourceBundle.getBundleText("���ʡ�����ä������ѧ���") + " "
                            + ResourceBundle.getBundleText("�������ö�ѹ�֡��"), UpdateStatus.WARNING);
                    throw new Exception("duplicate");
                }
            }
            //pu:�����������¡���ä������ѧ
            if (groupchronic.getObjectId() == null) {
                ans = intSaveGroupChronic(groupchronic, vIcd10AllGroup, vIcd10SpecifyCode);
            } //pu:�Ѿവ�������¡���ä������ѧ
            else {
                ans = intUpdateGroupChronic(groupchronic, vIcd10AllGroup, vIcd10SpecifyCode);
            }
            theLO.theGroupChronic = theHosDB.theGroupChronicDB.selectAll();
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            if (!"duplicate".equals(ex.getMessage())) {
                LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
                hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            }
        } finally {
            this.theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        }
        return ans;
    }

    private int intSaveGroupChronic(GroupChronic groupchronic, Vector vIcd10AllGroup, Vector vIcd10SpecifyCode) throws Exception {
        int ans = theHosDB.theGroupChronicDB.insert(groupchronic);
        if (ans > 0) {
            ans = intSaveIcd10Group(groupchronic, vIcd10AllGroup);
            if (vIcd10SpecifyCode == null || vIcd10SpecifyCode.isEmpty()) {
                return ans;
            }
            for (int i = 0, size = vIcd10SpecifyCode.size(); i < size; i++) {
                GroupIcd10 icdgc = ((GroupIcd10) vIcd10SpecifyCode.get(i));
                ans = intSaveIcd10SpecifyCode(groupchronic, icdgc);
            }
        }
        return ans;
    }

    private int intUpdateGroupChronic(GroupChronic groupchronic, Vector vIcd10AllGroup, Vector vIcd10SpecifyCode) throws Exception {
        int ans = theHosDB.theGroupChronicDB.update(groupchronic);
        if (ans > 0) {
            ans = intSaveIcd10Group(groupchronic, vIcd10AllGroup);
            if (vIcd10SpecifyCode == null || vIcd10SpecifyCode.isEmpty()) {
                return ans;
            }
            for (int i = 0, size = vIcd10SpecifyCode.size(); i < size; i++) {
                GroupIcd10 icdgc = ((GroupIcd10) vIcd10SpecifyCode.get(i));
                ans = intSaveIcd10SpecifyCode(groupchronic, icdgc);
            }
        }
        return ans;
    }

    private int intSaveIcd10Group(GroupChronic groupchronic, Vector vIcd10AllGroup) throws Exception {
        int ans = 0;
        //pu:��Ǩ�ͺ��� ����� ICD10 ����������� �������������㹵��ҧ b_group_icd10
        if (vIcd10AllGroup == null || vIcd10AllGroup.isEmpty()) {
            return ans;
        }
        group_loop:
        for (int i = 0, size = vIcd10AllGroup.size(); i < size; i++) {
            GroupIcd10 icdgc = ((GroupIcd10) vIcd10AllGroup.get(i));
            if (icdgc.getObjectId() == null) {
                //���ҡ�������� ICD10 �������������㹵��ҧ b_group_icd10
                GroupIcd10 groupicd10 = theHosDB.theGroupIcd10DB.selectByIcdCode(icdgc.icdcode.substring(0, 3));
                //���������ա�������� ICD10 �������� b_group_icd10 �������� �� N/A 仡�͹
                if (groupicd10 == null) {
                    icdgc.group504 = "99";
                    icdgc.group505 = "99";
                    icdgc.group506 = "99";
                    icdgc.group_504_id = GroupIcd10.NA_504;
                    icdgc.group_505_id = GroupIcd10.NA_505;
                    icdgc.group_506_id = GroupIcd10.NA_506;
                    icdgc.group_disease = "";
                    icdgc.group_chronic_id = groupchronic.getObjectId();
                    icdgc.groupchronic = groupchronic.group_chronic_id;
                    ans = theHosDB.theGroupIcd10DB.insert(icdgc);
                } else {//������ա�������� ICD10 �������� b_group_icd10 ��������� ���Ҩҡ���ʡ���� ICD10 ��������
                    groupicd10.group_chronic_id = groupchronic.getObjectId();
                    groupicd10.groupchronic = groupchronic.group_chronic_id;
                    ans = theHosDB.theGroupIcd10DB.update(groupicd10);
                }

                //pu:�������ʷ���������鹵鹴��¡���� ICD10
//                Vector vc = theHosDB.theICD10DB.selectByIdGroup(icdgc.icdcode.substring(0, 3), "", "", "", true);
                Vector vc = theHosDB.theICD10DB.selectByIdGroup(icdgc.icdcode.substring(0, 3), true, "", true);
                //pu: �ѹ�֡���� ICD10 㹵��ҧ b_group_icd10
                if (vc == null || vc.isEmpty()) {
                    continue group_loop;
                }
                icd10_loop:
                for (int j = 0, sizev = vc.size(); j < sizev; j++) {
                    ICD10 icd10 = (ICD10) vc.get(j);
                    GroupIcd10 gicd10 = new GroupIcd10();
                    gicd10.icdcode = icd10.icd10_id;
                    ans = intSaveIcd10SpecifyCode(groupchronic, gicd10);
                }

            } else { //������� b_group_icd10
                icdgc.group_chronic_id = groupchronic.getObjectId();
                icdgc.groupchronic = groupchronic.group_chronic_id;
                ans = theHosDB.theGroupIcd10DB.update(icdgc);
            }
        }
        return ans;
    }

    private int intSaveIcd10SpecifyCode(GroupChronic groupchronic, GroupIcd10 icdgc) throws Exception {
        int ans;
        //�ѧ���������� b_group_icd10
        if (icdgc.getObjectId() == null) {
            //������ա�����ͧ���� ICD10 �������� b_group_icd10 �������
            GroupIcd10 groupicd10 = theHosDB.theGroupIcd10DB.selectByIcdCode(icdgc.icdcode.substring(0, 3));
            //���������ա�������� ICD10 �������� b_group_icd10 �������� �� N/A 仡�͹
            if (groupicd10 == null) {
                icdgc.group504 = "99";
                icdgc.group505 = "99";
                icdgc.group506 = "99";
                icdgc.group_504_id = GroupIcd10.NA_504;
                icdgc.group_505_id = GroupIcd10.NA_505;
                icdgc.group_506_id = GroupIcd10.NA_506;
                icdgc.group_disease = "";
                icdgc.group_chronic_id = groupchronic.getObjectId();
                icdgc.groupchronic = groupchronic.group_chronic_id;
                ans = theHosDB.theGroupIcd10DB.insert(icdgc);
            } else {//������ա�������� ICD10 �������� b_group_icd10 ��������� ���Ҩҡ���ʡ���� ICD10 ��������
                icdgc.group504 = groupicd10.group504;
                icdgc.group505 = groupicd10.group505;
                icdgc.group506 = groupicd10.group506;
                icdgc.group_504_id = groupicd10.group_504_id;
                icdgc.group_505_id = groupicd10.group_505_id;
                icdgc.group_506_id = groupicd10.group_506_id;
                icdgc.group_disease = groupicd10.group_disease;
                icdgc.other = groupicd10.other;
                icdgc.group_chronic_id = groupchronic.getObjectId();
                icdgc.groupchronic = groupchronic.group_chronic_id;
                ans = theHosDB.theGroupIcd10DB.insert(icdgc);
            }
        } else { //������� b_group_icd10
            icdgc.group_chronic_id = groupchronic.getObjectId();
            icdgc.groupchronic = groupchronic.group_chronic_id;
            ans = theHosDB.theGroupIcd10DB.update(icdgc);
        }
        return ans;
    }

    public void deleteICD10GroupChronic(int[] rows, Vector vIcd10GroupChronic, boolean group) {
        if (rows.length == 0) {
            theUS.setStatus("��س����͡ ICD10 ����ͧ���ź", UpdateStatus.WARNING);
            return;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            int size = rows.length;
            if (group) {
                for (int i = size - 1; i >= 0; i--) {
                    GroupIcd10 icdgc = (GroupIcd10) vIcd10GroupChronic.get(rows[i]);
                    //pu:�����¡�� ICD10 �¶١�ѹ�֡ŧ�ҹ������ ����Ѿവ������㹿�Ŵ�������ǡѺ chronic �繤����ҧ
                    if (icdgc.getObjectId() != null) {
                        icdgc.group_chronic_id = GroupIcd10.NA_CHRONIC;
                        icdgc.groupchronic = "99";
                        theHosDB.theGroupIcd10DB.update(icdgc);
                        theHO.vICD10GCGroup.remove(icdgc);
                    } else {
                        theHO.vICD10GCGroup.remove(icdgc);
                    }
                }
            } else {
                for (int i = size - 1; i >= 0; i--) {
                    GroupIcd10 icdgc = (GroupIcd10) vIcd10GroupChronic.get(rows[i]);
                    //pu:�����¡�� ICD10 �¶١�ѹ�֡ŧ�ҹ������ ����Ѿവ������㹿�Ŵ�������ǡѺ chronic �繤����ҧ
                    if (icdgc.getObjectId() != null) {
                        icdgc.group_chronic_id = GroupIcd10.NA_CHRONIC;
                        icdgc.groupchronic = "99";
                        theHosDB.theGroupIcd10DB.update(icdgc);
                        theHO.vICD10GCSpecifyCode.remove(icdgc);
                    } else {
                        theHO.vICD10GCSpecifyCode.remove(icdgc);
                    }
                }
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT, "���� ICD10");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theICD10GroupChronicSubject.notifySetTableICD10GCGroup("", UpdateStatus.NORMAL);
            theHS.theICD10GroupChronicSubject.notifySetTableICD10GCSpecifyCode("", UpdateStatus.NORMAL);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, "���� ICD10");
        }

    }

    public int saveGroupChronic(GroupChronic groupchronic) {
        int ans = 0;
        if ((groupchronic.group_chronic_id.isEmpty()) || ((groupchronic.description_en.isEmpty()) || (groupchronic.description_th.isEmpty()))) {
            theUS.setStatus("�������ö�ѹ�֡�������繤����ҧ��", UpdateStatus.WARNING);
            return ans;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            GroupChronic cr = theHosDB.theGroupChronicDB.selectByCode(groupchronic.group_chronic_id);
            if (cr != null) {
                if (groupchronic.getObjectId() == null || !groupchronic.getObjectId().equals(cr.getObjectId())) {
                    theUS.setStatus("�������ö�ѹ�֡���ʫ����", UpdateStatus.WARNING);
                    throw new Exception();
                }
            }
            if (groupchronic.getObjectId() == null) {
                ans = theHosDB.theGroupChronicDB.insert(groupchronic);
            } else {
                ans = theHosDB.theGroupChronicDB.update(groupchronic);
            }
            theLO.theGroupChronic = theHosDB.theGroupChronicDB.selectAll();
            theConnectionInf.getConnection().commit();
            theUS.setStatus(ResourceBundle.getBundleText("��úѹ�֡������") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(ResourceBundle.getBundleText("��úѹ�֡������") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    /**
     * ������¡�á�����ä������ѧ�������㹰ҹ������
     *
     * @param pk �� String �ͧ ���͡�����ä������ѧ����ͧ��ä���
     * @param active �� String ����Ѻ�к�ʶҹТͧ���������ͧ��ä���
     * @return Vector �ͧ Object GroupChronic
     * @author pu
     * @date 15/09/2008
     */
    public Vector listGroupChronicAll(String pk, String active) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theGroupChronicDB.selectByNameActive(pk, active);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * �������� ICD10 ��������������� ���������ä������ѧ�������
     * ���������� Vector �������� HosObject
     *
     * @param gc �� Object �ͧ GroupChronic ������ä������ѧ
     * @author pu
     * @date 16/09/2008
     */
    public void listICD10GroupChronicGroup(GroupChronic gc) {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHO.vICD10GCGroup = theHosDB.theGroupIcd10DB.selectGruopChronicByICD10ID(gc.getObjectId(), true);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theICD10GroupChronicSubject.notifySetTableICD10GCGroup("", UpdateStatus.NORMAL);
        }
    }

    /**
     * �������� ICD10 ��������������� ���������ä������ѧ�������
     * ���Ǥ׹��ҡ�Ѻ�� Vector
     *
     * @param gc �� Object �ͧ GroupSurveil ������ä������ѧ
     * @return Vector �ͧ Oject GroupChronic
     * @author pu
     * @date 15/09/2008
     */
    public Vector listICD10GroupChronic(GroupChronic gc) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theICD10GroupChronicDB.selectByChronicID(gc.getObjectId(), gc.active, ICD10GroupChronic.GROUP_TYPE);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * �������� ICD10 �����������о�� ���������ä������ѧ�������
     * ���Ǥ׹��ҡ�Ѻ�� Vector
     *
     * @param gc �� Object �ͧ GroupChronic ������ä������ѧ
     * @author pu
     * @date 15/09/2008
     */
    public void listICD10GroupChronicSpecifyCode(GroupChronic gc) {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHO.vICD10GCSpecifyCode = theHosDB.theGroupIcd10DB.selectGruopChronicByICD10ID(gc.getObjectId(), false);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theICD10GroupChronicSubject.notifySetTableICD10GCSpecifyCode("", UpdateStatus.NORMAL);
        }
    }

    /**
     * �������� ICD10 �����������ä������ѧ ���������ä������ѧ�������
     * ���Ǥ׹��ҡ�Ѻ�� Vector
     *
     * @param gc �� Object �ͧ GroupChronic ������ä������ѧ
     * @return Vector �ͧ Oject 10GroupChronic
     * @author pu
     * @date 15/09/2008
     */
    public Vector listICD10SpecifyCodeChronic(GroupChronic gc) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theICD10GroupChronicDB.selectByChronicID(gc.getObjectId(), gc.active, ICD10GroupChronic.CODE_TYPE);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public StringBuffer getStringICD10Chronic(GroupChronic gc) {
        StringBuffer icd_chronic = new StringBuffer();
        Hashtable hash = new Hashtable();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector vcGroupChronic = theHosDB.theGroupIcd10DB.selectByChronicID(gc.getObjectId());
            if (vcGroupChronic != null && !vcGroupChronic.isEmpty()) {
                int size = vcGroupChronic.size();
                loop:
                for (int i = 0; i < size; i++) {
                    String icdcode = ((GroupIcd10) vcGroupChronic.get(i)).icdcode;
                    if (i == (size - 1)) {
                        icd_chronic.append(icdcode);
                    } else {
                        icd_chronic.append(icdcode).append(",");
                    }
                }
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return icd_chronic;
    }
    //@deprecated pu : ¡��ԡ���ź ���� inactive ᷹

    public int deleteGroupChronic(GroupChronic groupchronic) {
        int ans = 0;
        if (groupchronic == null || groupchronic.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return ans;
        }
        if (!theUS.confirmBox(ResourceBundle.getBundleText("�׹�ѹ����ź��¡�ù��") + " "
                + ResourceBundle.getBundleText("�Ҩ�Դ�š�з��Ѻ������㹰ҹ��������"), UpdateStatus.WARNING)) {
            return 0;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ans = theHosDB.theGroupChronicDB.delete(groupchronic);
            theLO.theGroupChronic = theHosDB.theGroupChronicDB.selectAll();
            theUS.setStatus(ResourceBundle.getBundleText("ź��¡���ä������ѧ") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    /**
     * ������¡�á�����ä������ѧ�������㹰ҹ������
     *
     * @param pk �� String �ͧ ���͡�����ä������ѧ����ͧ��ä���
     * @param active �� String ����Ѻ�к�ʶҹТͧ���������ͧ��ä���
     * @return Vector �ͧ Object GroupSurveil
     * @author pu
     * @date 10/09/2008
     */
    public Vector listGroupSurveilAll(String pk, String active) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theGroupSurveilDB.selectByNameActive(pk, active);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * �������� ICD10 ��������������� ���������ä������ѧ�������
     * ���������� Vector �������� HosObject
     *
     * @param gs �� Object �ͧ GroupSurveil ������ä������ѧ
     * @author pu
     * @date 16/09/2008
     */
    public void listICD10GroupSurveilGroup(GroupSurveil gs) {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHO.vICD10GSGroup = theHosDB.theICD10GroupSurveilDB.selectBySurveilID(gs.getObjectId(), gs.active, ICD10GroupSurveil.GROUP_TYPE);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {

            theHS.theICD10GroupSurveilSubject.notifySetTableICD10GSGroup("", UpdateStatus.NORMAL);
        }
    }

    /**
     * �������� ICD10 ��������������� ���������ä������ѧ�������
     * ���Ǥ׹��ҡ�Ѻ�� Vector
     *
     * @param gs �� Object �ͧ GroupSurveil ������ä������ѧ
     * @return Vector �ͧ Oject ICD10GroupSurveil
     * @author pu
     * @date 16/09/2008
     */
    public Vector listICD10GroupSurveil(GroupSurveil gs) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theICD10GroupSurveilDB.selectBySurveilID(gs.getObjectId(), gs.active, ICD10GroupSurveil.GROUP_TYPE);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * �������� ICD10 ����������੾�� ���������ä������ѧ�������
     * ���������� Vector �������� HosObject
     *
     * @param gs �� Object �ͧ GroupSurveil ������ä������ѧ
     * @author pu
     * @date 16/09/2008
     */
    public void listICD10GroupSurveilSpecifyCode(GroupSurveil gs) {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theICD10GroupSurveilSubject.notifySetTableICD10GSSpecifyCode("", UpdateStatus.NORMAL);
        }
    }

    /**
     * �������� ICD10 �����������о�� ���������ä������ѧ�������
     * ���Ǥ׹��ҡ�Ѻ�� Vector
     *
     * @param gs �� Object �ͧ GroupSurveil ������ä������ѧ
     * @return Vector �ͧ Oject ICD10GroupSurveil
     * @author pu
     * @date 16/09/2008
     */
    public Vector listICD10SpecifyCodeSurveil(GroupSurveil gs) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theICD10GroupSurveilDB.selectBySurveilID(gs.getObjectId(), gs.active, ICD10GroupSurveil.CODE_TYPE);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * �ѹ�֡�������¡���ä������ѧ 506 ������� ICD10
     * �������㹡����������ѧ���С����
     *
     * @param groupsurveil �� Object �ͧ������ä������ѧ
     * @param vIcd10AllGroup ������ ICD10 ����������� �ͧ������ä������ѧ
     * @param vIcd10SpecifyCode ������ ICD10 ����������੾��
     * �ͧ������ä������ѧ
     * @return
     * @author pu
     * @date 16/09/2008
     * @UC hv3uc_267 saveICD10GroupSurveil
     */
    public int saveGroupSurveilICD10(GroupSurveil groupsurveil, Vector vIcd10AllGroup, Vector vIcd10SpecifyCode) {
        int ans = 0;
        if (groupsurveil.group_surveil_id.isEmpty()) {
            theUS.setStatus("��س��к����ʡ�����ä������ѧ", UpdateStatus.WARNING);
            return ans;
        }

        if (groupsurveil.description_th.isEmpty() || groupsurveil.description_en.isEmpty()) {
            theUS.setStatus("��س��кت��͡�����ä������ѧ", UpdateStatus.WARNING);
            return ans;
        }

        if ((vIcd10AllGroup == null || vIcd10AllGroup.isEmpty()) && (vIcd10SpecifyCode == null || vIcd10SpecifyCode.isEmpty())) {
            theUS.setStatus("��س��к����� ICD10 ����Ѻ������ä������ѧ", UpdateStatus.WARNING);
            return ans;
        }

        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            GroupSurveil cr = theHosDB.theGroupSurveilDB.selectByCode(groupsurveil.group_surveil_id);
            if (cr != null) {
                if (groupsurveil.getObjectId() == null || !groupsurveil.getObjectId().equals(cr.getObjectId())) {
                    theUS.setStatus(ResourceBundle.getBundleText("���ʡ�����ä������ѧ���") + " "
                            + ResourceBundle.getBundleText("�������ö�ѹ�֡��"), UpdateStatus.WARNING);
                    throw new Exception();
                }
            }

            //pu:����������ä������ѧ
            if (groupsurveil.getObjectId() == null) {
                ans = theHosDB.theGroupSurveilDB.insert(groupsurveil);
                if (ans > 0) {
                    //pu:��Ǩ�ͺ��� ����� ICD10 ����������� �������������㹵��ҧ b_icd10_group_surveil Ẻ GROUP_TYPE
                    if (vIcd10AllGroup != null && !vIcd10AllGroup.isEmpty()) {
                        for (int i = 0, size = vIcd10AllGroup.size(); i < size; i++) {
                            ICD10GroupSurveil icdgs = ((ICD10GroupSurveil) vIcd10AllGroup.get(i));
                            icdgs.group_surveil_id = groupsurveil.getObjectId();
                            icdgs.icd10_surveil_type = ICD10GroupSurveil.GROUP_TYPE;
                            icdgs.active = groupsurveil.active;
                            ans = theHosDB.theICD10GroupSurveilDB.insert(icdgs);
                        }
                    }
                    //pu:��Ǩ�ͺ��� ����� ICD10 ����������੾�� �������������㹵��ҧ b_icd10_group_surveil Ẻ CODE_TYPE
                    if (vIcd10SpecifyCode != null && !vIcd10SpecifyCode.isEmpty()) {
                        for (int i = 0, size = vIcd10SpecifyCode.size(); i < size; i++) {
                            ICD10GroupSurveil icdgs = ((ICD10GroupSurveil) vIcd10AllGroup.get(i));
                            icdgs.group_surveil_id = groupsurveil.getObjectId();
                            icdgs.icd10_surveil_type = ICD10GroupSurveil.CODE_TYPE;
                            icdgs.active = groupsurveil.active;
                            ans = theHosDB.theICD10GroupSurveilDB.insert(icdgs);
                        }
                    }
                }
            } //pu:�Ѿവ�������¡���ä������ѧ
            else {
                ans = theHosDB.theGroupSurveilDB.update(groupsurveil);
                if (ans > 0) {
                    //pu:��Ǩ�ͺ��� ����� ICD10 ����������� �������������㹵��ҧ b_icd10_group_surveil Ẻ GROUP_TYPE
                    if (vIcd10AllGroup != null && !vIcd10AllGroup.isEmpty()) {
                        for (int i = 0, size = vIcd10AllGroup.size(); i < size; i++) {
                            ICD10GroupSurveil icdgs = ((ICD10GroupSurveil) vIcd10AllGroup.get(i));
                            if (icdgs.getObjectId() == null) {
                                icdgs.group_surveil_id = groupsurveil.getObjectId();
                                icdgs.icd10_surveil_type = ICD10GroupSurveil.GROUP_TYPE;
                                icdgs.active = groupsurveil.active;
                                ans = theHosDB.theICD10GroupSurveilDB.insert(icdgs);
                            } else {
                                icdgs.group_surveil_id = groupsurveil.getObjectId();
                                icdgs.icd10_surveil_type = ICD10GroupSurveil.GROUP_TYPE;
                                icdgs.active = groupsurveil.active;
                                ans = theHosDB.theICD10GroupSurveilDB.update(icdgs);
                            }
                        }
                    }
                    //pu:��Ǩ�ͺ��� ����� ICD10 ����������੾�� �������������㹵��ҧ b_icd10_group_surveil Ẻ CODE_TYPE
                    if (vIcd10SpecifyCode != null && !vIcd10SpecifyCode.isEmpty()) {
                        for (int i = 0, size = vIcd10SpecifyCode.size(); i < size; i++) {
                            ICD10GroupSurveil icdgs = ((ICD10GroupSurveil) vIcd10AllGroup.get(i));
                            if (icdgs.getObjectId() == null) {
                                icdgs.group_surveil_id = groupsurveil.getObjectId();
                                icdgs.icd10_surveil_type = ICD10GroupSurveil.CODE_TYPE;
                                icdgs.active = groupsurveil.active;
                                ans = theHosDB.theICD10GroupSurveilDB.insert(icdgs);
                            } else {
                                icdgs.group_surveil_id = groupsurveil.getObjectId();
                                icdgs.icd10_surveil_type = ICD10GroupSurveil.CODE_TYPE;
                                icdgs.active = groupsurveil.active;
                                ans = theHosDB.theICD10GroupSurveilDB.update(icdgs);
                            }
                        }
                    }
                }
            }
            theLO.theGroupSurveil = theHosDB.theGroupSurveilDB.selectAll();
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT, "�ѹ�֡��¡�á�����ä������ѧ");
        } finally {
            this.theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, "�ѹ�֡��¡�á�����ä������ѧ");
        }
        return ans;
    }

    /**
     * ź��¡�� ICD10 � Vector ��͹��㹵��ҧ
     *
     * @param rows �� �ӹǹ�Ǣͧ��¡�� ICD10 ����ͧ���ź
     * @param vIcd10GroupSurveil
     * @param type �繻������ͧ���� ICD10
     * @author Pu
     * @date 16/09/2008
     */
    public void deleteICD10GroupSurveil(int[] rows, Vector vIcd10GroupSurveil, String type) {
        if (rows.length == 0) {
            theUS.setStatus("��س����͡ ICD10 ����ͧ���ź", UpdateStatus.WARNING);
            return;
        }

        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            int size = rows.length;
            if (type.equals(ICD10GroupSurveil.GROUP_TYPE)) {
                for (int i = size - 1; i >= 0; i--) {
                    ICD10GroupSurveil icdgs = (ICD10GroupSurveil) vIcd10GroupSurveil.get(rows[i]);
                    //pu:�����¡�� ICD10 �¶١�ѹ�֡ŧ�ҹ������
                    if (icdgs.getObjectId() != null) {
                        theHosDB.theICD10GroupSurveilDB.delete(icdgs);
                        theHO.vICD10GSGroup.remove(icdgs);
                    } else {
                        theHO.vICD10GSGroup.remove(icdgs);
                    }
                }

            } else {
                for (int i = size - 1; i >= 0; i--) {
                    ICD10GroupSurveil icdgs = (ICD10GroupSurveil) vIcd10GroupSurveil.get(rows[i]);
                    //pu:�����¡�� ICD10 �¶١�ѹ�֡ŧ�ҹ������
                    if (icdgs.getObjectId() != null) {
                        theHosDB.theICD10GroupSurveilDB.delete(icdgs);
                        theHO.vICD10GSSpecifyCode.remove(icdgs);

                    } else {
                        theHO.vICD10GSSpecifyCode.remove(icdgs);
                    }
                }
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_DELETE, "���� ICD10");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theICD10GroupSurveilSubject.notifySetTableICD10GSGroup("", UpdateStatus.NORMAL);
            theHS.theICD10GroupSurveilSubject.notifySetTableICD10GSSpecifyCode("", UpdateStatus.NORMAL);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE, "���� ICD10");
        }
    }

    /**
     * ������ ICD10 �ͧ����� Surveil �������� StringBuffer
     * ����Ѻ�ʴ���㹤������ͧ���ҧ
     *
     * @param gs �� Object �ͧ GroupSurveil ������ä������ѧ
     * @return StringBuffer ��������� ICD10 �������ͧ������ä������ѧ
     * @author pu
     * @date 16/09/2008
     */
    public StringBuffer getStringICD10Surveil(GroupSurveil gs) {
        StringBuffer icd_surveil = new StringBuffer();

        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector vcGroup = theHosDB.theICD10GroupSurveilDB.selectBySurveilID(gs.getObjectId(), gs.active, ICD10GroupSurveil.GROUP_TYPE);
            Vector vcCode = theHosDB.theICD10GroupSurveilDB.selectBySurveilID(gs.getObjectId(), gs.active, ICD10GroupSurveil.CODE_TYPE);
            if (vcGroup != null && !vcGroup.isEmpty()) {
                for (int i = 0; i < vcGroup.size(); i++) {
                    if ((vcGroup.size() - i) == 1 && (vcCode == null || vcCode.isEmpty())) {
                        icd_surveil.append(((ICD10GroupSurveil) vcGroup.get(i)).icd_number);
                    } else {
                        icd_surveil.append(((ICD10GroupSurveil) vcGroup.get(i)).icd_number).append(",");
                    }
                }
            }
            if (vcCode != null && !vcCode.isEmpty()) {
                for (int i = 0; i < vcCode.size(); i++) {
                    if ((vcCode.size() - i) == 1) {
                        icd_surveil.append(((ICD10GroupSurveil) vcCode.get(i)).icd_number);
                    } else {
                        icd_surveil.append(((ICD10GroupSurveil) vcCode.get(i)).icd_number).append(",");
                    }
                }
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return icd_surveil;
    }

    public Vector listVitalTemplateByName(String pk) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theVitalTemplateDB.selectAllByName(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listGroupIcd10All(String pk) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theGroupIcd10DB.selectByIcd(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /*
     * 18/04/2549 jao
     */
    public Vector listGroupIcd10ByDisease(String pk) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theGroupIcd10DB.selctByDisease(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public int saveGroupIcd10(GroupIcd10 gicd10) {
        int ans = 0;
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (gicd10.getObjectId() == null) {
                ans = theHosDB.theGroupIcd10DB.insert(gicd10);
            } else {
                ans = theHosDB.theGroupIcd10DB.update(gicd10);
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT, "�ѹ�֡�����š�èѴ����������ä");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, "�ѹ�֡�����š�èѴ����������ä");
        }
        return ans;
    }

    public GroupChronic listGroupChronicByCode(String pk) {
        GroupChronic gc = new GroupChronic();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            gc = theHosDB.theGroupChronicDB.selectByCode(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return gc;
    }

    public Item listItemByItemIdReq(String itemid) {
        Item m = new Item();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            m = theHosDB.theItemDB.selectById(itemid);

            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return m;
    }

    public boolean saveAutoOrderItemReq(AutoOrderItem autoorderitem) {
        if ((autoorderitem.auto_order_item_id.isEmpty()) || (autoorderitem.item_id.isEmpty())) {
            theUS.setStatus("�������ö�ѹ�֡�������繤����ҧ��", UpdateStatus.WARNING);
            return false;
        }
        if (autoorderitem.mon_time_start.compareTo(autoorderitem.mon_time_stop) > 0) {
            theUS.setStatus(ResourceBundle.getBundleText("���ҷ���͡�ͧ�ѹ�ѹ���-�ء��Դ��Ҵ") + " "
                    + ResourceBundle.getBundleText("��سҵ�Ǩ�ͺ�ش���������Шش����ش�ͧ����"), UpdateStatus.WARNING);
            return false;
        }
        if (autoorderitem.sat_time_start.compareTo(autoorderitem.sat_time_stop) > 0) {
            theUS.setStatus(ResourceBundle.getBundleText("���ҷ���͡�ͧ�ѹ�����Դ��Ҵ") + " "
                    + ResourceBundle.getBundleText("��سҵ�Ǩ�ͺ�ش���������Шش����ش�ͧ����"), UpdateStatus.WARNING);
            return false;
        }
        if (autoorderitem.sun_time_start.compareTo(autoorderitem.sun_time_stop) > 0) {
            theUS.setStatus(ResourceBundle.getBundleText("���ҷ���͡�ͧ�ѹ�ҷԵ��Դ��Ҵ") + " "
                    + ResourceBundle.getBundleText("��سҵ�Ǩ�ͺ�ش���������Шش����ش�ͧ����"), UpdateStatus.WARNING);
            return false;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (autoorderitem.getObjectId() == null) {
                theHosDB.theAutoOrderItemDB.insert(autoorderitem);
                theUS.setStatus(ResourceBundle.getBundleText("�ѹ�֡��¡�õ�Ǩ�ѡ���ѵ��ѵ�") + " "
                        + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            } else {
                theHosDB.theAutoOrderItemDB.update(autoorderitem);
                theUS.setStatus(ResourceBundle.getBundleText("�����¡�õ�Ǩ�ѡ���ѵ��ѵ�") + " "
                        + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        }
        return isComplete;
    }

    public Vector listAutoOrderItemReq(String pk) {
        if (pk == null) {
            return null;
        }
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theAutoOrderItemDB.selectAutoOrderByName(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;

    }

    public int deleteAutoOrderItemReq(AutoOrderItem auto) {
        int ans = 0;
        if (auto == null || auto.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return ans;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ans = theHosDB.theAutoOrderItemDB.delete(auto);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE, "ź��¡�õ�Ǩ�ѡ���ѵ��ѵ�");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE, "ź��¡�õ�Ǩ�ѡ���ѵ��ѵ�");
        }
        return ans;
    }

    public int deleteQueueVisit(QueueVisit queuevisit) {
        int ans = 0;
        if (theLookupControl.readOption().life.equals(Active.isEnable())) {
            theUS.setStatus(ResourceBundle.getBundleText("�к����������ҹ����") + " "
                    + ResourceBundle.getBundleText("���ź�������������ö����"), UpdateStatus.WARNING);
            return ans;
        }
        if (queuevisit == null || queuevisit.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return ans;
        }
        if (!theUS.confirmBox(ResourceBundle.getBundleText("�׹�ѹ����ź��¡�ù��") + " "
                + ResourceBundle.getBundleText("�Ҩ�Դ�š�з��Ѻ������㹰ҹ��������"), UpdateStatus.WARNING)) {
            return 0;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ans = theHosDB.theQueueVisitDB.delete(queuevisit);
            theConnectionInf.getConnection().commit();
            theUS.setStatus(ResourceBundle.getBundleText("ź���") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(ResourceBundle.getBundleText("ź���") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);

        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public int saveQueueVisit(QueueVisit queuevisit) {
        int ans = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (queuevisit.getObjectId() == null) {
                ans = theHosDB.theQueueVisitDB.insert(queuevisit);
                theUS.setStatus(ResourceBundle.getBundleText("�ѹ�֡���") + " "
                        + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            } else {
                ans = theHosDB.theQueueVisitDB.update(queuevisit);
                theUS.setStatus(ResourceBundle.getBundleText("��䢤��") + " "
                        + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(ResourceBundle.getBundleText("�ѹ�֡���") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public ContractAdjust listContractAdjustByIdItem(String groupItem, String contractid) {
        ContractAdjust ca = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ca = theHosDB.theContractAdjustDB.queryByCGaContractID(groupItem, contractid);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ca;
    }

    public int deleteXrayLeteral(XRayLeteral xrayleteral) {
        int ans = 0;
        if (theLookupControl.readOption().life.equals(Active.isEnable())) {
            theUS.setStatus(ResourceBundle.getBundleText("�к����������ҹ����") + " "
                    + ResourceBundle.getBundleText("���ź�������������ö����"), UpdateStatus.WARNING);
            return ans;
        }
        if (xrayleteral == null || xrayleteral.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return ans;
        }
        if (!theUS.confirmBox(ResourceBundle.getBundleText("�׹�ѹ����ź��¡�ù��") + " "
                + ResourceBundle.getBundleText("�Ҩ�Դ�š�з��Ѻ������㹰ҹ��������"), UpdateStatus.WARNING)) {
            return 0;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ans = theHosDB.theXRayLeteralDB.delete(xrayleteral);
            theUS.setStatus(ResourceBundle.getBundleText("ź��ҹ�ͧ��é�� X-Ray") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public Vector listXrayLeteralBySearch(String search, String active) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theXRayLeteralDB.selectAllByName(search, active);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listXrayFilmSizeBySearch(String search) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theFilmSizeDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public int saveXrayLeteral(XRayLeteral xrayleteral) {
        int ans = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (xrayleteral.getObjectId() == null) {
                ans = theHosDB.theXRayLeteralDB.insert(xrayleteral);
            } else {
                ans = theHosDB.theXRayLeteralDB.update(xrayleteral);
            }
            theLO.theXrayLeteral = theHosDB.theXRayLeteralDB.selectAll();
            theUS.setStatus(ResourceBundle.getBundleText("�ѹ�֡��ҹ�ͧ��é�� X-Ray") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public int saveXrayFilmSize(FilmSize theXrayFilmSize) {
        int ans = 0;
        if (theXrayFilmSize.filmsize.isEmpty()) {
            theUS.setStatus("��س��к����ʢͧ��Ҵ����� Xray ��͹�ӡ�á������ѹ�֡", UpdateStatus.WARNING);
            return ans;
        }
        if (theXrayFilmSize.description.isEmpty()) {
            theUS.setStatus("��س��к���������´�ͧ��Ҵ����� Xray ��͹�ӡ�á������ѹ�֡", UpdateStatus.WARNING);
            return ans;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            FilmSize xr = theHosDB.theFilmSizeDB.selectByCode(theXrayFilmSize.filmsize);
            if (xr != null && theXrayFilmSize.getObjectId() == null) {
                theUS.setStatus("�������ö�ѹ�֡���ʫ����", UpdateStatus.WARNING);
                throw new Exception();
            }
            if (xr != null && theXrayFilmSize.getObjectId() != null
                    && !theXrayFilmSize.getObjectId().equals(xr.getObjectId())) {
                theUS.setStatus("�������ö�ѹ�֡���ʫ����", UpdateStatus.WARNING);
                throw new Exception();
            }
            if (theXrayFilmSize.getObjectId() == null) {
                theHosDB.theFilmSizeDB.insert(theXrayFilmSize);
            } else {
                theHosDB.theFilmSizeDB.update(theXrayFilmSize);
            }
            theLO.theFilmSize = theHosDB.theFilmSizeDB.selectAll();
            theConnectionInf.getConnection().commit();
            isComplete = true;
            ans = 1;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(ResourceBundle.getBundleText("��úѹ�֡������ͧ��é�� X-Ray") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theUS.setStatus(ResourceBundle.getBundleText("��úѹ�֡������ͧ��é�� X-Ray") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        }
        return ans;
    }

    public int deleteXrayPosition(XRayPosition xrayPosition) {
        int ans = 0;
        if (theLookupControl.readOption().life.equals(Active.isEnable())) {
            theUS.setStatus(ResourceBundle.getBundleText("�к����������ҹ����") + " "
                    + ResourceBundle.getBundleText("���ź�������������ö����"), UpdateStatus.WARNING);
            return ans;
        }
        if (xrayPosition == null || xrayPosition.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return ans;
        }
        if (!theUS.confirmBox(ResourceBundle.getBundleText("�׹�ѹ����ź��¡�ù��") + " "
                + ResourceBundle.getBundleText("�Ҩ�Դ�š�з��Ѻ������㹰ҹ��������"), UpdateStatus.WARNING)) {
            return 0;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ans = theHosDB.theXRayPositionDB.delete(xrayPosition);
            theUS.setStatus(ResourceBundle.getBundleText("ź��Ңͧ��é�� X-Ray") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public int deleteFilmSize(FilmSize x) {
        int ans = 0;
        if (theLookupControl.readOption().life.equals(Active.isEnable())) {
            theUS.setStatus(ResourceBundle.getBundleText("�к����������ҹ����") + " "
                    + ResourceBundle.getBundleText("���ź�������������ö����"), UpdateStatus.WARNING);
            return ans;
        }
        if (x == null || x.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return ans;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ans = theHosDB.theFilmSizeDB.delete(x);
            theUS.setStatus(ResourceBundle.getBundleText("ź������ͧ��é�� X-Ray") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public Vector listXrayPositionBySearch(String search, String active) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theXRayPositionDB.selectAllByName(search, active);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public int saveXrayPosition(XRayPosition xrayPosition) {
        int ans = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (xrayPosition.getObjectId() == null) {
                ans = theHosDB.theXRayPositionDB.insert(xrayPosition);
            } else {
                ans = theHosDB.theXRayPositionDB.update(xrayPosition);
            }
            theLO.theXrayPosition = theHosDB.theXRayPositionDB.selectAll();
            theUS.setStatus(ResourceBundle.getBundleText("�ѹ�֡��Ңͧ��é�� X-Ray") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public int deleteServicePointDoctorBySPoint(String servicepoint) {
        int ans = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ans = theHosDB.theServicePointDoctorDB.deleteByServicePointID(servicepoint);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public ServicePointDoctor listServicePointDoctorByEmPoint(String emid, String pointid) {
        ServicePointDoctor theServicePointDoctor = new ServicePointDoctor();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theServicePointDoctor = theHosDB.theServicePointDoctorDB.selectByEmIdPointId(emid, pointid);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theServicePointDoctor;
    }

    public Contract listContractGroupByCode(String code) {
        Contract theContract = new Contract();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theContract = theHosDB.theContractDB.selectByCode(code);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theContract;
    }

    public Ward listWardByCode(String code) {
        Ward theWard = new Ward();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theWard = theHosDB.theWardDB.selectByCode(code);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theWard;
    }

    public Ward readWardByPk(String pk) {
        Ward theWard = new Ward();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theWard = theHosDB.theWardDB.selectByPK(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theWard;
    }

    public ServicePoint listServicePointByCode(String code) {
        ServicePoint theServicePoint = new ServicePoint();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theServicePoint = theHosDB.theServicePointDB.selectByCode(code);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theServicePoint;
    }

    public Clinic listClinicByCode(String code) {
        Clinic theClinic = new Clinic();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theClinic = theHosDB.theClinicDB.selectByCode(code);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theClinic;
    }

    public CategoryGroupItem listCategoryGroupItemByCode(String code) {
        CategoryGroupItem theCategoryGroupItem = new CategoryGroupItem();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theCategoryGroupItem = theHosDB.theCategoryGroupItemDB.selectByCode(code);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theCategoryGroupItem;
    }

    /**
     * @Author : sumo
     * @date : 05/06/2549
     * @see : list ��������¡�á�����ҵðҹ���ʴ�� combo
     * @param code String �ͧ�Ӥ�
     * @return : Object �ͧ��������¡�á�����ҵðҹ���ç�Ѻ������
     */
    public Item16Group listItem16GroupByCode(String code) {
        Item16Group theItem16Group = new Item16Group();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theItem16Group = theHosDB.theItem16GroupDB.selectByCode(code);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theItem16Group;
    }

    public BillingGroupItem listBillingGroupItemByCode(String code) {
        BillingGroupItem theBillingGroupItem = new BillingGroupItem();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theBillingGroupItem = theHosDB.theBillingGroupItemDB.selectByCode(code);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theBillingGroupItem;
    }

    public Payer listPayerByCode(String code) {
        Payer thePayer = new Payer();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            thePayer = theHosDB.thePayerDB.selectByCode(code);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return thePayer;
    }

    public VitalTemplate listVitalTemplateByCodeSP(String code, String sp) {
        VitalTemplate theVitalTemplate = new VitalTemplate();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theVitalTemplate = theHosDB.theVitalTemplateDB.selectByCodeServicePoint(code, sp);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theVitalTemplate;
    }

    public XRayLeteral listXrayLeteralByCode(String code) {
        XRayLeteral theXRayLeteral = new XRayLeteral();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theXRayLeteral = theHosDB.theXRayLeteralDB.selectByCode(code);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theXRayLeteral;
    }

    public FilmSize listXrayFilmSizeByCode(String code) {
        FilmSize theFilmSize = new FilmSize();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theFilmSize = theHosDB.theFilmSizeDB.selectByCode(code);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theFilmSize;
    }

    public XRayPosition listXrayPositionByCode(String code) {
        XRayPosition theXRayPosition = new XRayPosition();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theXRayPosition = theHosDB.theXRayPositionDB.selectByCode(code);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theXRayPosition;
    }

    public DrugFrequency listDrugFrequencyByCode(String code) {
        DrugFrequency theDrugFrequency = new DrugFrequency();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theDrugFrequency = theHosDB.theDrugFrequencyDB.selectByCode(code);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theDrugFrequency;
    }

    /**
     * ���Ҥ������ͧ�� ��� Primary key
     *
     * @param pk �� String ����� primary key �ͧ�������ͧ�ҷ���ͧ��ä���
     * @return DrugFrequency �� Object �ͧ�������ͧ��
     * @Author Pu
     * @Date 04/08/2006
     */
    public DrugFrequency listDrugFrequencyByPK(String pk) {
        DrugFrequency theDrugFrequency = new DrugFrequency();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theDrugFrequency = theHosDB.theDrugFrequencyDB.selectByPK(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theDrugFrequency;
    }

    public DrugInstruction listDrugInstructionByCode(String code) {
        DrugInstruction theDrugInstruction = new DrugInstruction();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theDrugInstruction = theHosDB.theDrugInstructionDB.selectByCode(code);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theDrugInstruction;
    }

    /**
     * �����Ըա������ ��� Primary key
     *
     * @param pk �� String ����� primary key �ͧ�Ը������ͧ��ä���
     * @return DrugInstruction �� Object �ͧ�Ըա������
     * @Author Pu
     * @Date 04/08/2006
     */
    public DrugInstruction listDrugInstructionByPK(String pk) {
        DrugInstruction theDrugInstruction = new DrugInstruction();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theDrugInstruction = theHosDB.theDrugInstructionDB.selectByPK(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theDrugInstruction;
    }

    public AutoOrderItem listAutoOrderItemByCode(String code) {
        AutoOrderItem theAutoOrderItem = new AutoOrderItem();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theAutoOrderItem = theHosDB.theAutoOrderItemDB.selectByCode(code);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theAutoOrderItem;
    }

    public Uom listUOMByCode(String code) {
        Uom theUom = new Uom();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theUom = theHosDB.theUomDB.selectByCode(code);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theUom;
    }

    public Uom listUOMByKeyId(String pk) {
        Uom theUom = new Uom();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theUom = theHosDB.theUomDB.selectByPK(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theUom;
    }

    public ICD10 listIcd10ByCode(String code, int active) {
        ICD10 theICD10 = new ICD10();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theICD10 = theHosDB.theICD10DB.selectByCode(code, active);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theICD10;
    }

    public ICD9 listIcd9ByCode(String code) {
        ICD9 theICD9 = new ICD9();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theICD9 = theHosDB.theICD9DB.selectByCode(code);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theICD9;
    }

    public GroupChronic listGroupChronicByPk(String pk) {
        GroupChronic theGroupChronic = new GroupChronic();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theGroupChronic = theHosDB.theGroupChronicDB.selectByPK(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theGroupChronic;
    }

    /**
     * @param pk
     * @return
     * @author amp
     * @date 18/04/2549
     */
    public com.pcu.object.Disease listDiseaseByPk(String pk) {
        com.pcu.object.Disease theDisease = new com.pcu.object.Disease();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theDisease = theHosDB.theDiseaseDB.selectByPK(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theDisease;
    }

    public Employee listEmployeeByUsername(String usr) {
        Employee theEmployee2 = new Employee();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theEmployee2 = theLookupControl.readEmployeeByUsername(usr);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theEmployee2;
    }

    public int deleteSQLTemplate(SQLTemplate sqlttemplate) {
        int result = 0;
        if (sqlttemplate == null || sqlttemplate.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return result;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theSQLTemplateDB.delete(sqlttemplate);
            theLO.theSQLTemplate = theHosDB.theSQLTemplateDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    public SQLTemplate listSQLTemplateByCode(String code) {
        SQLTemplate sqltemplate = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            sqltemplate = (SQLTemplate) theHosDB.theSQLTemplateDB.listByCode(code);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return sqltemplate;
    }

    public int saveSQLTemplate(SQLTemplate sqlttemplate) {
        int result = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (sqlttemplate.getObjectId() == null) {
                result = theHosDB.theSQLTemplateDB.insert(sqlttemplate);
            } else {
                result = theHosDB.theSQLTemplateDB.update(sqlttemplate);
            }
            theLO.theSQLTemplate = theHosDB.theSQLTemplateDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    public Vector listSQLTemplateBySearch(String search, String status) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theSQLTemplateDB.listBySearch(search, status);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public boolean checkSQL(String sql) {
        boolean result = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theCheckSQLDB.checkSQL(sql);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    public boolean checkSQLParam(String sql) {
        boolean result = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theCheckSQLDB.checkSQLParam(sql);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    public SQLTemplate readSQLTemplateByID(String key_id) {
        SQLTemplate sqltemplate = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            sqltemplate = (SQLTemplate) theHosDB.theSQLTemplateDB.selectByPK(key_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return sqltemplate;
    }

    public String querySQL(String sql) {
        boolean isComplete = false;
        theConnectionInf.open();
        String ret = "";
        String[] columnname = null;
        Vector vString = null;
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            java.sql.ResultSet rs = theHosDB.theCheckSQLDB.querySQL(sql);
            int column;

            if (rs != null) {
                java.sql.ResultSetMetaData metadata = rs.getMetaData();
                column = metadata.getColumnCount();
                columnname = new String[column];
                String[] rowdata;
                vString = new Vector();
                for (int i = 0; i < column; i++) {
                    columnname[i] = metadata.getColumnLabel(i + 1);
                }
                while (rs.next()) {
                    rowdata = new String[column];
                    for (int i = 0; i < column; i++) {
                        if (rs.getObject(i + 1) != null) {
                            rowdata[i] = rs.getString(i + 1);
                        }
                    }
                    vString.add(rowdata);
                }
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;

            ret = "";
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(ex.getMessage(), UpdateStatus.ERROR);
            ret = ex.getMessage();
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theSetupSubject.notifyquerySQL(columnname, vString);
        }
        return ret;
    }

    public int deleteSQLTemplateParam(SQLTemplateParam sqlttemplate) {
        int result = 0;
        if (sqlttemplate == null || sqlttemplate.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return result;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theSQLTemplateParamDB.delete(sqlttemplate);
            theLO.theSQLTemplateParam = theHosDB.theSQLTemplateParamDB.selectAll();

            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    public Vector listSQLTemplateParamAll(String search) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theSQLTemplateParamDB.selectAll();

            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public SQLTemplateParam listSQLTemplateParamByCode(String code) {
        SQLTemplateParam sqltemplateparam = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            sqltemplateparam = (SQLTemplateParam) theHosDB.theSQLTemplateParamDB.listByCode(code);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return sqltemplateparam;
    }

    public Vector listSQLTemplateParamBySearch(String search, String status) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theSQLTemplateParamDB.listBySearch(search, status);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public SQLTemplateParam readSQLTemplateParamByID(String key_id) {
        SQLTemplateParam sqltemplateParam = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            sqltemplateParam = (SQLTemplateParam) theHosDB.theSQLTemplateParamDB.selectByPK(key_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return sqltemplateParam;
    }

    public int saveSQLTemplateParam(SQLTemplateParam sql) {
        int result = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (sql.getObjectId() == null) {
                result = theHosDB.theSQLTemplateParamDB.insert(sql);
            } else {
                result = theHosDB.theSQLTemplateParamDB.update(sql);
            }
            theLO.theSQLTemplateParam = theHosDB.theSQLTemplateParamDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    public String querySQLParam(String sql, String startdate, String enddate) {
        String ret = "";
        boolean isComplete = false;
        theConnectionInf.open();
        String[] columnname = null;
        Vector vString = null;
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String tmp_sql = sql;
            int cur_index = 0;
            int num_qt = 0;
            while (cur_index != -1) {
                cur_index++;
                cur_index = tmp_sql.indexOf('?', cur_index);
                num_qt += 1;
            }
            num_qt -= 1;     //�ٻ�Թ�� 1 �ͺ
            java.sql.PreparedStatement ps = theConnectionInf.ePQuery(sql);
            //PreparedStatement ps = getConnection().prepareStatement(sql);
            ps.setString(1, startdate);
            ps.setString(2, enddate);
            if (num_qt > 2) {
                for (int i = 3; i <= num_qt; i++) {
                    ps.setString(i, startdate);
                    i++;
                    ps.setString(i, enddate);
                }
            }
            java.sql.ResultSet rs = ps.executeQuery();
            int column;
            if (rs != null) {
                java.sql.ResultSetMetaData metadata = rs.getMetaData();
                column = metadata.getColumnCount();
                columnname = new String[column];
                String[] rowdata;
                vString = new Vector();
                for (int i = 0; i < column; i++) {
                    columnname[i] = metadata.getColumnLabel(i + 1);

                }
                while (rs.next()) {
                    rowdata = new String[column];
                    for (int i = 0; i < column; i++) {
                        if (rs.getObject(i + 1) != null) {
                            rowdata[i] = rs.getString(i + 1);

                        }
                    }
                    vString.add(rowdata);
                }
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            ret = ex.getMessage();
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theSetupSubject.notifyquerySQLParam(columnname, vString);
        }
        return ret;
    }

    public String readAddressByFullname(String full_id) {
        String fullid = new String();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            FAddress address = theHosDB.theFAddressDB.selectByPK(full_id);
            if (address != null) {
                fullid = address.description;
            } else {
                fullid = "";
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return fullid;
    }

    public Vector listEpiSetGroup(String pk) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (pk.isEmpty()) {
                vc = theHosDB.theEpiSetGroupDB.selectAll();
            } else {
                vc = theHosDB.theEpiSetGroupDB.selectByName(pk);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public EpiSetGroup readEpiSetGroup(String epi_id) {
        EpiSetGroup ds = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ds = theHosDB.theEpiSetGroupDB.selectByPK(epi_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ds;
    }

    public Vector listEpiSetByGroup(String pk) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theEpiSetDB.selectByGroup(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

//    /**
//     * @deprecated ���� edit ����������������¹��� save ᷹
//     */
//    public int editEpiSetGroup(EpiSetGroup drugsetgroup) {
//        int ans = 0;
//        theConnectionInf.open();
//        try {
//            if (drugsetgroup.getObjectId() != null) {
//                theHosDB.theEpiSetGroupDB.update(drugsetgroup);
//            } else {
//                theHosDB.theEpiSetGroupDB.insert(drugsetgroup);
//            }
//        } catch (Exception ex) {
//            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
//        }
//        theConnectionInf.close();
//        return ans;
//    }
    public int deleteEpiSetGroup(EpiSetGroup drugsetgroup) {
        int ans = 0;
        if (drugsetgroup == null || drugsetgroup.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return ans;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theEpiSetGroupDB.delete(drugsetgroup);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public int deleteEpiSetByGroup(String pk) {
        int ans = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theEpiSetDB.deleteByGroup(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public int deleteEpiSetReq(EpiSet drugset) {
        int ans = 0;
        if (drugset == null || drugset.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return ans;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theEpiSetDB.delete(drugset);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public int editEpiSet(EpiSet drugset) {
        int ans = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);

            if (drugset.getObjectId() != null) {
                theHosDB.theEpiSetDB.update(drugset);
            } else {
                theHosDB.theEpiSetDB.insert(drugset);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public int saveEpiSetGroup(EpiSetGroup drugSetGroup) {
        int ans = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theEpiSetGroupDB.insert(drugSetGroup);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public int saveEpiSet(Vector drugSet) {
        int ans = 0;
        if (drugSet == null) {
            return ans;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (int i = 0; i < drugSet.size(); i++) {
                if (((EpiSet) drugSet.get(i)).getObjectId() == null) {
                    theHosDB.theEpiSetDB.insert((EpiSet) drugSet.get(i));
                } else {
                    theHosDB.theEpiSetDB.update((EpiSet) drugSet.get(i));
                }
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public int saveDoseEpiSet(Vector doseEpiSet) {
        int ans = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (doseEpiSet != null) {
                for (int i = 0; i < doseEpiSet.size(); i++) {
                    if (doseEpiSet.get(i) != null) {
                        if (((DoseEpiSet) doseEpiSet.get(i)).getObjectId() == null) {
                            theHosDB.theDoseEpiSetDB.insert((DoseEpiSet) doseEpiSet.get(i));
                        } else {
                            theHosDB.theDoseEpiSetDB.update((DoseEpiSet) doseEpiSet.get(i));
                        }
                    }
                }

            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public DoseEpiSet listDoseEpiSet(String key_drugset) {
        DoseEpiSet dds = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            dds = theHosDB.theDoseEpiSetDB.selectByKeyEpiSet(key_drugset);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return dds;
    }

    public int deleteDoseEpiSet(DoseEpiSet doseEpiSet) {
        int ans = 0;
        if (doseEpiSet == null || doseEpiSet.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return ans;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theDoseEpiSetDB.delete(doseEpiSet);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public Vector listGuideByDescription(String des) {
        Vector vector = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vector = theHosDB.theGuideDB.selectAllByName(des);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vector;
    }

    public int saveGuide(Guide guideAfterDx) {
        int ans = 0;
        if (guideAfterDx.description.length() > 255) {
            theUS.setStatus(ResourceBundle.getBundleText("���й���ѧ��Ǩ�դ�������ҡ�Թ�") + " "
                    + ResourceBundle.getBundleText("��س�Ŵ��ŧ����"), UpdateStatus.WARNING);
            return ans;
        }

        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (guideAfterDx.getObjectId() == null) {
                theHosDB.theGuideDB.insert(guideAfterDx);
            } else {
                theHosDB.theGuideDB.update(guideAfterDx);
            }
            theUS.setStatus(ResourceBundle.getBundleText("�ѹ�֡��Ǫ��¤��й���ѧ��Ǩ") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(ResourceBundle.getBundleText("�ѹ�֡��Ǫ��¤��й���ѧ��Ǩ") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public int deleteGuide(Guide guideAfterDx) {
        int ans = 0;
        if (theLookupControl.readOption().life.equals(Active.isEnable())) {
            theUS.setStatus(ResourceBundle.getBundleText("�к����������ҹ����") + " "
                    + ResourceBundle.getBundleText("���ź�������������ö����"), UpdateStatus.WARNING);
            return ans;
        }
        if (guideAfterDx == null || guideAfterDx.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return ans;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ans = theHosDB.theGuideDB.delete(guideAfterDx);
            theUS.setStatus(ResourceBundle.getBundleText("ź��Ǫ��¤��й���ѧ��Ǩ") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    /**
     * Function tong 2/1/48
     *
     * @param data
     * @return
     */
    public Vector listSequenceDataAll(String data) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theSequenceDataDB.selectBySearch(data);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * Function amp 14/6/48
     *
     * @param data
     * @return
     */
    public Vector listNCDGroupAll(String data) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theNCDGroupDB.selectBySearch(data);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * @henbe 210606
     * @param sp
     * @param employee
     * @return
     */
    public boolean addEmployeeInServicePoint(ServicePoint sp, Employee employee) {
        boolean ret = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ret = intAddEmployeeInServicePoint(sp, employee);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ret;
    }

    /**
     * @henbe 210606
     * @param sp
     * @param employee
     * @return
     * @throws Exception
     */
    public boolean intAddEmployeeInServicePoint(ServicePoint sp, Employee employee) throws Exception {
        Vector sdv = theHosDB.theServicePointDoctorDB.selectBySerciveID(sp.getObjectId());
        for (int i = 0; sdv != null && i < sdv.size(); i++) {
            ServicePointDoctor sd = (ServicePointDoctor) sdv.get(i);
            if (sd.doctor_id.equals(employee.getObjectId())) {
                theUS.setStatus("��ᾷ��㹨ش��ԡ�ôѧ����������������ö�����ա��", UpdateStatus.WARNING);
                return false;
            }
        }
        ServicePointDoctor sd = new ServicePointDoctor();
        sd.doctor_id = employee.getObjectId();
        sd.service_point_key_id = sp.getObjectId();
        theHosDB.theServicePointDoctorDB.insert(sd);
        return true;
    }

    /**
     * Function tong 2/1/48
     *
     * @param sequenceData
     * @param vn_seq
     * @return
     */
    public boolean intCheckSaveSeqVN(SequenceData sequenceData, int vn_seq) {
        String vn_slastest = "";
        String max_value = "";
        try {
            max_value = theHosDB.theVisitDB.selectMaxVN();
            int year_index = sequenceData.pattern.indexOf('y');
            if (year_index != 0) {
                theUS.setStatus("��س��кص���á�ͧ�ٻẺ����繤�һ���ҹ��", UpdateStatus.WARNING);
                return false;
            }
            if (max_value != null) {
                if (year_index != -1) {
                    vn_slastest = max_value.substring(year_index + 1);
                }
                int vn_lastest = Integer.parseInt(vn_slastest);
                if (vn_lastest >= vn_seq) {
                    boolean res = theUS.confirmBox(ResourceBundle.getBundleText("���Ţ VN ����ش���") + " " + max_value
                            + " " + ResourceBundle.getBundleText("�׹�ѹ��úѹ�֡"), UpdateStatus.WARNING);
                    if (!res) {
                        return false;
                    }
                }
            }
            return true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            boolean res = theUS.confirmBox(ResourceBundle.getBundleText("���Ţ VN ����ش��������Ţ")
                    + " " + max_value + " " + " "
                    + ResourceBundle.getBundleText("�������ö��Ǩ�ͺ����ش������")
                    + " "
                    + ResourceBundle.getBundleText("�׹�ѹ��úѹ�֡"), UpdateStatus.WARNING);
            if (!res) {
                return false;
            }
            return true;
        }
    }

    /**
     * Function tong 2/1/48
     *
     * @param sequenceData
     * @param vn_seq
     * @return
     * @throws Exception
     */
    public boolean intCheckSaveSeqHCIS(SequenceData sequenceData, int vn_seq) throws Exception {
        if (sequenceData.pattern.length() != 6) {
            theUS.setStatus("�ӴѺ�Ţ HN_HCIS ��ͧ���Ţ 6 ��ѡ�����͡��§ҹ 18 �����ҹ��", UpdateStatus.WARNING);
            return false;
        }
        String vn_slastest = theHosDB.theFamilyDB.selectMaxHCIS();
        int year_index = sequenceData.pattern.lastIndexOf('y');
        try {
            if (vn_slastest != null) {
                if (year_index != -1) {
                    vn_slastest = vn_slastest.substring(year_index + 1);
                }
                int vn_lastest = Integer.parseInt(vn_slastest);
                if (vn_lastest >= vn_seq) {
                    boolean res = theUS.confirmBox(ResourceBundle.getBundleText("���Ţ HCIS ����ش���") + " " + vn_lastest
                            + " " + ResourceBundle.getBundleText("�׹�ѹ��úѹ�֡"), UpdateStatus.WARNING);
                    if (!res) {
                        return false;
                    }
                }
            }
            return true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            boolean res = theUS.confirmBox(ResourceBundle.getBundleText("���Ţ HCIS ����ش��������Ţ")
                    + " " + vn_slastest
                    + " " + " "
                    + ResourceBundle.getBundleText("�������ö��Ǩ�ͺ����ش������")
                    + " "
                    + ResourceBundle.getBundleText("�׹�ѹ��úѹ�֡"), UpdateStatus.WARNING);
            if (!res) {
                return false;
            }
            return true;
        }
    }

    /**
     * Function tong 2/1/48
     *
     * @param sequenceData
     * @param vn_seq
     * @return
     * @throws Exception
     */
    public boolean intCheckSaveSeqAN(SequenceData sequenceData, int vn_seq) throws Exception {
        String vn_slastest = theHosDB.theVisitDB.selectMaxAN();
        int year_index = sequenceData.pattern.indexOf('y');
        if (year_index != 0) {
            theUS.setStatus("��س��кص���á�ͧ�ٻẺ����繤�һ���ҹ��", UpdateStatus.WARNING);
            return false;
        }
        try {
            if (vn_slastest != null) {
                if (year_index != -1) {
                    vn_slastest = vn_slastest.substring(year_index + 1);
                }
                int vn_lastest = Integer.parseInt(vn_slastest);
                if (vn_lastest >= vn_seq) {
                    boolean res = theUS.confirmBox(ResourceBundle.getBundleText("���Ţ AN ����ش���") + " " + vn_lastest
                            + " " + ResourceBundle.getBundleText("�׹�ѹ��úѹ�֡"), UpdateStatus.WARNING);
                    if (!res) {
                        return false;
                    }
                }
            }
            return true;
        } catch (Exception ex) {
//            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            boolean res = theUS.confirmBox(ResourceBundle.getBundleText("���Ţ AN ����ش��������Ţ")
                    + " " + vn_slastest
                    + " " + " "
                    + ResourceBundle.getBundleText("�������ö��Ǩ�ͺ����ش������")
                    + " "
                    + ResourceBundle.getBundleText("�׹�ѹ��úѹ�֡"), UpdateStatus.WARNING);
            if (!res) {
                return false;
            }
            return true;
        }
    }

    /**
     * Function tong 2/1/48
     *
     * @param sequenceData
     * @param vn_seq
     * @return
     * @throws Exception
     */
    public boolean intCheckSaveSeqHN(SequenceData sequenceData, int vn_seq) throws Exception {
        String vn_slastest = theHosDB.thePatientDB.selectMaxHN();
        int year_index = sequenceData.pattern.lastIndexOf('y');
        try {
            if (vn_slastest != null) {
                if (year_index != -1) {
                    vn_slastest = vn_slastest.substring(year_index + 1);
                }
                int vn_lastest = Integer.parseInt(vn_slastest);
                if (vn_lastest >= vn_seq) {
                    boolean res = theUS.confirmBox(ResourceBundle.getBundleText("���Ţ HN ����ش���") + " " + vn_lastest
                            + " " + ResourceBundle.getBundleText("�׹�ѹ��úѹ�֡"), UpdateStatus.WARNING);
                    if (!res) {
                        return false;
                    }
                }
            }
            return true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            boolean res = theUS.confirmBox(ResourceBundle.getBundleText("���Ţ HN ����ش��������Ţ")
                    + " " + vn_slastest
                    + " " + " "
                    + ResourceBundle.getBundleText("�������ö��Ǩ�ͺ����ش������")
                    + " "
                    + ResourceBundle.getBundleText("�׹�ѹ��úѹ�֡"), UpdateStatus.WARNING);
            if (!res) {
                return false;
            }
            return true;
        }
    }

    /**
     * Function tong 2/1/48
     *
     * @param sequenceData
     * @return
     */
    public int saveSequenceData(SequenceData sequenceData) {
        int ret = 0;
        if (sequenceData.value.isEmpty() || sequenceData.name.isEmpty()) {
            theUS.setStatus("��سҺѹ�֡�����ŷ�����������ҧ", UpdateStatus.WARNING);
            return 0;
        }
        int seqNo;
        try {
            seqNo = Integer.parseInt(sequenceData.value);
        } catch (Exception ex) {
            theUS.setStatus("��س��к��Ţ Sequence �繵���Ţ", UpdateStatus.WARNING);
            return ret;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (sequenceData.getObjectId().equals("vn")) {
                if (!intCheckSaveSeqVN(sequenceData, seqNo)) {
                    return 0;
                }
            } else if (sequenceData.getObjectId().equals("hn_hcis")) {
                if (!intCheckSaveSeqHCIS(sequenceData, seqNo)) {
                    return 0;
                }
            } else if (sequenceData.getObjectId().equals("an")) {
                if (!intCheckSaveSeqAN(sequenceData, seqNo)) {
                    return 0;
                }
            } else if (sequenceData.getObjectId().equals("hn")) {
                if (!intCheckSaveSeqHN(sequenceData, seqNo)) {
                    return 0;
                }
            } else if (sequenceData.getObjectId().equals("rn")) {
                if (!intCheckSaveSeqRN(sequenceData, seqNo)) {
                    return 0;
                }
            } else if (sequenceData.getObjectId().equals("rfin")) {
                if (!intCheckSaveSeqRFIN(sequenceData, seqNo)) {
                    return 0;
                }
            } else if (sequenceData.getObjectId().equals("rfon")) {
                if (!intCheckSaveSeqRFON(sequenceData, seqNo)) {
                    return 0;
                }
            } else if (sequenceData.getObjectId().equals("xn")) {
                if (!intCheckSaveSeqXN(sequenceData, seqNo)) {
                    return 0;
                }
            } else if (sequenceData.getObjectId().equals("mc")
                    || sequenceData.getObjectId().equals("mc_w")
                    || sequenceData.getObjectId().equals("mc_a")) {
                if (!intCheckSaveSeqMedCer(sequenceData, seqNo)) {
                    return 0;
                }
            }
            if (sequenceData.getObjectId().equals("an")) {
                sequenceData.pattern = "1" + sequenceData.pattern;
            } else if (sequenceData.getObjectId().equals("vn")) {
                sequenceData.pattern = "0" + sequenceData.pattern;
            }

            ret = theHosDB.theSequenceDataDB.update(sequenceData);

            theUS.setStatus(ResourceBundle.getBundleText("��úѹ�֡�Ţ Sequence") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(ResourceBundle.getBundleText("��úѹ�֡�Ţ Sequence") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        return ret;
    }

    /**
     * function ��Ǩ�ͺ��Ҩش��ԡ�ù������ͧ��Ǩ�������
     *
     * @param service_id
     * @return
     */
    public boolean checkServiceIsTreat(String service_id) {
        boolean result = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theServicePointDB.checkServiceIsTreat(service_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    /**
     * noom 17/12/2548
     *
     * @param pk
     * @return
     */
    public DrugDosePrint listDrugDosePrintByPk(String pk) {
        DrugDosePrint theDrugDosePrint = new DrugDosePrint();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theDrugDosePrint = theHosDB.theDrugDosePrintDB.selectByPK(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theDrugDosePrint;
    }

    public int saveDrugDoseMapUom(DrugDoseMapUom theDrugDoseMapUom) {
        int ans = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theDrugDoseMapUom2DB.insert(theDrugDoseMapUom);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public int deleteDrugDoseMapUom(DrugDoseMapUom theDrugDoseMapUom) {
        int ans = 0;
        if (theDrugDoseMapUom == null || theDrugDoseMapUom.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return ans;
        }
        theConnectionInf.open();
        try {
            theHosDB.theDrugDoseMapUom2DB.delete(theDrugDoseMapUom);
            theUS.setStatus(ResourceBundle.getBundleText("ź�ӹǹ��") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
        }
        theConnectionInf.close();
        return ans;
    }

    public DrugDoseMapUom readDrugDoseMapUomFromDrugUomAndDoseId(String drug_uom_id, String drug_dose_id) {
        DrugDoseMapUom theDrugDoseMapUom = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theDrugDoseMapUom = theHosDB.theDrugDoseMapUom2DB.selectByDrugUomAndDoseId(drug_uom_id, drug_dose_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theDrugDoseMapUom;
    }

    public DrugDosePrint readDrugDosePrintByPk(String pk) {
        DrugDosePrint theDrugDosePrint = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theDrugDosePrint = theHosDB.theDrugDosePrintDB.selectByPK(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theDrugDosePrint;
    }

    public Vector listDrugDoseMapUomByDrugUomId(String pkDrugUom_id) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theDrugDoseMapUom2DB.selectByDrugUomId(pkDrugUom_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public int deleteDrugDoseMapUomFromDrugUomId(String drug_uom_id) {
        int ans = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theDrugDoseMapUom2DB.deleteFromDrugUomId(drug_uom_id);
            theUS.setStatus(ResourceBundle.getBundleText("ź˹�����") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public int deleteDrugDoseMapUomFromDrugDoseId(String drug_dose_id) {
        int ans = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theDrugDoseMapUom2DB.deleteFromDrugDoseId(drug_dose_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    /**
     * @Author : amp
     * @date : 13/03/2549
     * @see : ��������ա���� item ���㹵��ҧ order �����ѧ
     * @param item_id
     * @return : false=����ա���� , true=�ա��������
     */
    public boolean readOrderItemByItemId(String item_id) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theOrderItemDB.selectOrderItemByItemId(item_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc != null ? true : false;
    }

    /**
     * @Author: amp
     * @date: 14/03/2549
     * @see: �������ҵðҹ
     * @param pk �Ӥ�
     * @param active
     * @return ���ҵðҹ
     */
    public Vector listDrugStandardByCodeName(String pk, String active) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theDrugStandardDB.selectByCodeName(pk, active);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * @param drugStandard
     * @return
     * @Author amp
     * @date 14/03/2549
     * @see ź���ҵðҹ
     */
    public int deleteDrugStandard(DrugStandard drugStandard) {
        int ans = 0;
        if (theLookupControl.readOption().life.equals(Active.isEnable())) {
            theUS.setStatus(ResourceBundle.getBundleText("�к����������ҹ����") + " "
                    + ResourceBundle.getBundleText("���ź�������������ö����"), UpdateStatus.WARNING);
            return ans;
        }
        if (drugStandard == null || drugStandard.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return ans;
        }
        if (!drugStandard.getObjectId().substring(3).startsWith(theLO.theSite.off_id)) {
            theUS.setStatus(ResourceBundle.getBundleText("��¡�õ�Ǩ�ѡ�ҹ���繢ͧ�ҹ���������") + " "
                    + ResourceBundle.getBundleText("�������öź��") + " "
                    + ResourceBundle.getBundleText("��س� set active ᷹"), UpdateStatus.WARNING);
            return 0;
        }
        if (!theUS.confirmBox(ResourceBundle.getBundleText("�׹�ѹ����ź��¡�ù��") + " "
                + ResourceBundle.getBundleText("�Ҩ�Դ�š�з��Ѻ������㹰ҹ��������"), UpdateStatus.WARNING)) {
            return 0;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theDrugStandardDB.delete(drugStandard);
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_DELETE);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE);
        }
        return ans;
    }

    /**
     * @Author amp
     * @date 14/03/2549
     * @see �������ҵðҹ
     * @param number ����
     * @return ���ҵðҹ
     */
    public DrugStandard listDrugStandardByNumber(String number) {
        DrugStandard theDrugStandard = new DrugStandard();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theDrugStandard = theHosDB.theDrugStandardDB.selectByNumber(number);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theDrugStandard;
    }

    /**
     * @Author amp
     * @date 14/03/2549
     * @see �ѹ�֡���ҵðҹ
     * @param drugStandard ���ҵðҹ
     * @return
     */
    public int saveDrugStandard(DrugStandard drugStandard) {
        if ("".equals(drugStandard.number) || "".equals(drugStandard.description)) {
            theUS.setStatus("��س��к���������Ъ������ҵðҹ", UpdateStatus.WARNING);
            return 0;
        }
        DrugStandard drs = listDrugStandardByNumber(drugStandard.number);
        if (drs != null && drugStandard.getObjectId() == null) {
            theUS.setStatus("�������ö�ѹ�֡���ʫ����", UpdateStatus.WARNING);
            return 0;
        }
        if (drs != null && drugStandard.getObjectId() != null
                && !drugStandard.getObjectId().equals(drs.getObjectId())) {
            theUS.setStatus("�������ö�ѹ�֡���ʫ����", UpdateStatus.WARNING);
            return 0;
        }
        int ans = 0;
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (drugStandard.getObjectId() == null) {
                theHosDB.theDrugStandardDB.insert(drugStandard);
                theUS.setStatus(ResourceBundle.getBundleText("��úѹ�֡���ҵðҹ") + " "
                        + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
                ans = 1;
            } else {
                theHosDB.theDrugStandardDB.update(drugStandard);
                theUS.setStatus(ResourceBundle.getBundleText("���������ҵðҹ") + " "
                        + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
                ans = 1;
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        }
        return ans;
    }

    /**
     * @Author amp
     * @date 16/03/2549
     * @see �ѹ�֡ map ���ҵðҹ�Ѻ Item
     * @param vDrugStandardMapItem Vector �ͧ DrugStandardMapItem
     * @return
     */
    public int saveDrugStandardMapItem(Vector vDrugStandardMapItem) {
        int ans = 0;
        DrugStandardMapItem theDrugStandardMapItem;
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            DrugStandardMapItem drugStandardMapItem0 = (DrugStandardMapItem) vDrugStandardMapItem.get(0);
            Vector vOriginal = theHosDB.theDrugInteractionDB.selectByOriginal(drugStandardMapItem0.drug_standard_id);
            Vector vInteraction = theHosDB.theDrugInteractionDB.selectByInteraction(drugStandardMapItem0.drug_standard_id);

            for (int i = 0; i < vDrugStandardMapItem.size(); i++) {
                DrugStandardMapItem drugStandardMapItem = (DrugStandardMapItem) vDrugStandardMapItem.get(i);
                theDrugStandardMapItem = theHosDB.theDrugStandardMapItemDB.selectByDrugStandardAndItem(drugStandardMapItem.drug_standard_id, drugStandardMapItem.item_id);

                if (theDrugStandardMapItem == null) {
                    // ��ҵ�ͧ��� 1: 1 ������ comment �͡������
//                    DrugStandardMapItem dsmi = theHosDB.theDrugStandardMapItemDB.selectByItem(drugStandardMapItem.item_id);
//                    if (dsmi == null) {
                    theHosDB.theDrugStandardMapItemDB.insert(drugStandardMapItem);
                    ans = 1;
                    //�ҷ���ջ�ԡ����ҡѹ
                    if (vOriginal == null) {
                        if (vInteraction != null) {
                            for (int j = 0, sizej = vInteraction.size(); j < sizej; j++) {
                                DrugInteraction drugInteraction = (DrugInteraction) vInteraction.get(j);
                                //�� Item �ͧ�ҵ�駵� �ͧ standrad(�ҷ���ջ�ԡ����Ҵ���) ���� ǹ�ٻ add
                                Vector vItemOriginal = theHosDB.theDrugStandardMapItemDB.selectItemByStandard(drugInteraction.drug_standard_original_id);
                                if (vItemOriginal != null) {
                                    for (int k = 0, sizek = vItemOriginal.size(); k < sizek; k++) {
                                        DrugInteractionDetail drugInteractionDetail = new DrugInteractionDetail();
                                        drugInteractionDetail.drug_interaction_id = drugInteraction.getObjectId();
                                        drugInteractionDetail.original_id = ((DrugStandardMapItem) vItemOriginal.get(k)).item_id;
                                        drugInteractionDetail.interaction_id = drugStandardMapItem.item_id;
                                        theHosDB.theDrugInteractionDetailDB.insert(drugInteractionDetail);
                                    }
                                }
                            }
                        }
                    } else {
                        for (int x = 0, sizex = vOriginal.size(); x < sizex; x++) {
                            DrugInteraction drugInteraction = (DrugInteraction) vOriginal.get(x);
                            //�� Item �ͧ�ҵ�駵� �ͧ standrad(�ҷ���ջ�ԡ����Ҵ���) ���� ǹ�ٻ add
                            Vector vItemInteraction = theHosDB.theDrugStandardMapItemDB.selectItemByStandard(drugInteraction.drug_standard_interaction_id);
                            if (vItemInteraction != null) {
                                for (int k = 0, sizek = vItemInteraction.size(); k < sizek; k++) {
                                    DrugInteractionDetail drugInteractionDetail = new DrugInteractionDetail();
                                    drugInteractionDetail.drug_interaction_id = drugInteraction.getObjectId();
                                    drugInteractionDetail.original_id = drugStandardMapItem.item_id;
                                    drugInteractionDetail.interaction_id = ((DrugStandardMapItem) vItemInteraction.get(k)).item_id;
                                    theHosDB.theDrugInteractionDetailDB.insert(drugInteractionDetail);
                                }
                            }
                        }
                    }
//                    } else {
//                        boolean rename = false;
//                        if ("".equals(dsmi.drug_standard_id)) {
//                            rename = true;
//                        }
//                        if (!drugStandardMapItem.drug_standard_id.equals(dsmi.drug_standard_id)) {
//                            rename = true;
//                            theHosDB.theDrugInteractionDetailDB.deleteOriginalItem(dsmi.item_id);
//                            theHosDB.theDrugInteractionDetailDB.deleteInteractionItem(dsmi.item_id);
//                        }
//
//                        dsmi.drug_standard_id = drugStandardMapItem.drug_standard_id;
//                        theHosDB.theDrugStandardMapItemDB.update(dsmi);
//                        ans = 1;
//
//                        if (rename) {
//                            if (vOriginal == null) {
//                                if (vInteraction != null) {
//                                    for (int j = 0, sizej = vInteraction.size(); j < sizej; j++) {
//                                        DrugInteraction drugInteraction = (DrugInteraction) vInteraction.get(j);
//                                        //�� Item �ͧ�ҵ�駵� �ͧ standrad(�ҷ���ջ�ԡ����Ҵ���) ���� ǹ�ٻ add
//                                        Vector vItemOriginal = theHosDB.theDrugStandardMapItemDB.selectItemByStandard(drugInteraction.drug_standard_original_id);
//                                        if (vItemOriginal != null) {
//                                            for (int k = 0, sizek = vItemOriginal.size(); k < sizek; k++) {
//                                                DrugInteractionDetail drugInteractionDetail = new DrugInteractionDetail();
//                                                drugInteractionDetail.drug_interaction_id = drugInteraction.getObjectId();
//                                                drugInteractionDetail.original_id = ((DrugStandardMapItem) vItemOriginal.get(k)).item_id;
//                                                drugInteractionDetail.interaction_id = dsmi.item_id;
//                                                theHosDB.theDrugInteractionDetailDB.insert(drugInteractionDetail);
//                                            }
//                                        }
//                                    }
//                                }
//                            } else {
//                                for (int x = 0, sizex = vOriginal.size(); x < sizex; x++) {
//                                    DrugInteraction drugInteraction = (DrugInteraction) vOriginal.get(x);
//                                    //�� Item �ͧ�ҵ�駵� �ͧ standrad(�ҷ���ջ�ԡ����Ҵ���) ���� ǹ�ٻ add
//                                    Vector vItemInteraction = theHosDB.theDrugStandardMapItemDB.selectItemByStandard(drugInteraction.drug_standard_interaction_id);
//                                    if (vItemInteraction != null) {
//                                        for (int k = 0, sizek = vItemInteraction.size(); k < sizek; k++) {
//                                            DrugInteractionDetail drugInteractionDetail = new DrugInteractionDetail();
//                                            drugInteractionDetail.drug_interaction_id = drugInteraction.getObjectId();
//                                            drugInteractionDetail.original_id = dsmi.item_id;
//                                            drugInteractionDetail.interaction_id = ((DrugStandardMapItem) vItemInteraction.get(k)).item_id;
//                                            theHosDB.theDrugInteractionDetailDB.insert(drugInteractionDetail);
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
                }
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        }
        return ans;
    }

    /**
     * @Author: amp
     * @date: 16/03/2549
     * @see: �������ҵðҹ��� Map �Ѻ Item
     * @param description ���� item
     * @param active
     * @return Vector �ͧ DrugStandardMapItem
     */
    public Vector listDrugStandardMapItem(String description, String active) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theDrugStandardMapItemDB.listDrugStandardMapItem(description, active);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * @Author : amp
     * @date : 17/03/2549
     * @see : ź��¡�� map ���ҵðҹ�Ѻ item
     * @param vDrugStandardMapItem Vector �ͧ DrugStandardMapItem
     * @param row �Ƿ���ź
     */
    public void deleteDrugStandardMapItem(Vector vDrugStandardMapItem, int[] row) {
        if (row.length == 0) {
            theUS.setStatus("�ѧ����բ�����", UpdateStatus.WARNING);
            return;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (int i = row.length - 1; i >= 0; i--) {
                DrugStandardMapItem dsmi = (DrugStandardMapItem) vDrugStandardMapItem.get(row[i]);
                if (dsmi.getObjectId() != null) {
                    theHosDB.theDrugStandardMapItemDB.delete(dsmi);
                    //amp:22/03/2549
                    theHosDB.theDrugInteractionDetailDB.deleteOriginalItem(dsmi.item_id);
                    theHosDB.theDrugInteractionDetailDB.deleteInteractionItem(dsmi.item_id);

                }
                vDrugStandardMapItem.remove(row[i]);
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        }
    }

    /**
     * @Author amp
     * @date 20/03/2549
     * @see ��Ǩ�ͺ������ҵðҹ���ǹ�� Map Item �����ѧ
     * @param drugStandard ���ҵðҹ
     */
    public void readDrugStandardMapItem(DrugStandard drugStandard) {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            DrugStandardMapItem theDrugStandardMapItem = theHosDB.theDrugStandardMapItemDB.selectByStandard(drugStandard.getObjectId());
            if (theDrugStandardMapItem == null) {
                theUS.setStatus(drugStandard.description + " " + ResourceBundle.getBundleText("�ѧ�����Ѻ���Ѻ��¡�õ�Ǩ�ѡ��"), UpdateStatus.WARNING);
            } else {
                theUS.setStatus("", UpdateStatus.NORMAL);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
    }

    /**
     * @Author amp
     * @date 20/03/2549
     * @see ��Ǩ�ͺ������ҵðҹ���ǹ�� Map Item �����ѧ
     * @param vDrugInteraction Vector �ͧ�ҷ���ջ�ԡ����Ҵ���
     * @note ����������������Ѻ�͹ saveDrugStandardMapItem
     * �����ѹ�е�ͧ�Դ��� database ������ description �ա����
     */
    public void readDrugStandardMapItem(Vector vDrugInteraction) {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String drug_not_map_item = "";
            for (int i = 0, size = vDrugInteraction.size(); i < size; i++) {
                DrugStandard drugStandard = (DrugStandard) vDrugInteraction.get(i);
                DrugStandardMapItem theDrugStandardMapItem = theHosDB.theDrugStandardMapItemDB.selectByStandard(drugStandard.getObjectId());
                if (theDrugStandardMapItem == null) {
                    if ("".equals(drug_not_map_item)) {
                        drug_not_map_item = drugStandard.description;
                    } else {
                        drug_not_map_item = drug_not_map_item + ", " + drugStandard.description;
                    }
                }
            }
            if ("".equals(drug_not_map_item)) {
                theUS.setStatus("", UpdateStatus.NORMAL);
            } else {
                theUS.setStatus(ResourceBundle.getBundleText("��") + drug_not_map_item + ResourceBundle.getBundleText("�ѧ�����Ѻ���Ѻ��¡�õ�Ǩ�ѡ��"), UpdateStatus.WARNING);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
    }

    /**
     * @Author amp
     * @date 20/03/2549
     * @see �ҷ���ջ�ԡ����ҡѹ
     * @param vDrugInteraction Vector �ͧ �ҷ���ջ�ԡ����ҡѹ
     * @return
     */
    public int saveDrugInteraction(Vector vDrugInteraction) {
        int ans = 0;
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            DrugInteraction theDrugInteraction;
            Vector vItemOriginal;
            Vector vItemInteraction;
            for (int i = 0, size = vDrugInteraction.size(); i < size; i++) {
                DrugInteraction drugInteraction = (DrugInteraction) vDrugInteraction.get(i);
                if (DrugInteractionType.isDrug().equals(drugInteraction.type)) {
                    //��Ǩ�ͺẺ�ç���
                    theDrugInteraction = theHosDB.theDrugInteractionDB.selectByOriginalAndInteraction(
                            drugInteraction.drug_standard_original_id,
                            drugInteraction.drug_standard_interaction_id);
                    if (theDrugInteraction == null) {
                        // disable this feature from gitlab #56
//                        //��Ǩ�ͺẺ��Ѻ��ҹ ,��ҹ㹷�������¶֧ ��ҹ�ҵ�駵� �Ѻ��ҹ�ҷ���ջ�ԡ����Ҵ���
//                        theDrugInteraction = theHosDB.theDrugInteractionDB.selectByOriginalAndInteraction(
//                                drugInteraction.drug_standard_interaction_id,
//                                drugInteraction.drug_standard_original_id);
//                        if (theDrugInteraction == null) {
                        theHosDB.theDrugInteractionDB.insert(drugInteraction);
                        vItemOriginal = theHosDB.theDrugStandardMapItemDB.selectItemByStandard(drugInteraction.drug_standard_original_id);
                        vItemInteraction = theHosDB.theDrugStandardMapItemDB.selectItemByStandard(drugInteraction.drug_standard_interaction_id);
                        if (vItemOriginal != null && vItemInteraction != null) {
                            for (int j = 0, sizej = vItemOriginal.size(); j < sizej; j++) {
                                for (int k = 0, sizek = vItemInteraction.size(); k < sizek; k++) {
                                    DrugInteractionDetail drugInteractionDetail = new DrugInteractionDetail();
                                    drugInteractionDetail.drug_interaction_id = drugInteraction.getObjectId();
                                    drugInteractionDetail.original_id = ((DrugStandardMapItem) vItemOriginal.get(j)).item_id;
                                    drugInteractionDetail.interaction_id = ((DrugStandardMapItem) vItemInteraction.get(k)).item_id;
                                    theHosDB.theDrugInteractionDetailDB.insert(drugInteractionDetail);
                                }
                            }
                        }
//                        } else {
//                            //�֧����Ѻ��ҹ��������ҹ����������������
//                            drugInteraction.setObjectId(theDrugInteraction.getObjectId());
//                            drugInteraction.drug_standard_original_id = theDrugInteraction.drug_standard_original_id;
//                            drugInteraction.drug_standard_interaction_id = theDrugInteraction.drug_standard_interaction_id;
//                            theHosDB.theDrugInteractionDB.update(drugInteraction);
//                        }
                    } else {
                        drugInteraction.setObjectId(theDrugInteraction.getObjectId());
                        theHosDB.theDrugInteractionDB.update(drugInteraction);
                    }
                }
                if (DrugInteractionType.isBloodPresure().equals(drugInteraction.type)) {
                    theDrugInteraction = theHosDB.theDrugInteractionDB.selectByOriginal(
                            drugInteraction.drug_standard_original_id,
                            DrugInteractionType.isBloodPresure());
                    if (theDrugInteraction == null) {
                        theHosDB.theDrugInteractionDB.insert(drugInteraction);
                    } else {
                        drugInteraction.setObjectId(theDrugInteraction.getObjectId());
                        theHosDB.theDrugInteractionDB.update(drugInteraction);
                    }
                }
                if (DrugInteractionType.isPregnant().equals(drugInteraction.type)) {
                    theDrugInteraction = theHosDB.theDrugInteractionDB.selectByOriginal(
                            drugInteraction.drug_standard_original_id,
                            DrugInteractionType.isPregnant());
                    if (theDrugInteraction == null) {
                        theHosDB.theDrugInteractionDB.insert(drugInteraction);
                    } else {
                        drugInteraction.setObjectId(theDrugInteraction.getObjectId());
                        theHosDB.theDrugInteractionDB.update(drugInteraction);
                    }
                }
                ans = 1;
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        }
        return ans;
    }

    /**
     * @Author: amp
     * @date: 21/03/2549
     * @see: �����ҷ���ջ�ԡ����ҡѹ
     * @param description ���� item, active
     * @return Vector �ͧ DrugInteraction
     */
    public Vector listDrugInteraction(String description) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theDrugInteractionDB.listDrugInteraction(description);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * @Author : amp
     * @date : 21/03/2549
     * @see : ź��¡���ҷ���ջ�ԡ����ҡѹ
     * @param vDrugInteraction vector �ͧ DrugInteraction
     * @param rows �Ƿ���ź
     */
    public void deleteDrugInteraction(Vector vDrugInteraction, int[] rows) {
        if (rows.length == 0) {
            theUS.setStatus("�ѧ����բ�����", UpdateStatus.WARNING);
            return;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (int i = rows.length - 1; i >= 0; i--) {
                DrugInteraction di = (DrugInteraction) vDrugInteraction.get(rows[i]);
                if (di.getObjectId() != null) {
                    theHosDB.theDrugInteractionDB.delete(di);
                    theHosDB.theDrugInteractionDetailDB.deleteByDrugInteractionId(di.getObjectId());
                    theUS.setStatus(ResourceBundle.getBundleText("ź�š�èѺ������ҵðҹ����ջ�ԡ����ҡѹ") + " "
                            + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
                }
                vDrugInteraction.remove(rows[i]);
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        }
    }

    // @not deprecated henbe bad function pattern
    public ItemService readItemSeviceByItem(String item) {
        ItemService is = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector v = theHosDB.theItemServiceDB.selectByItem(item);
            if (!v.isEmpty()) {
                is = (ItemService) v.get(0);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return is;
    }

    /**
     * ���� Icd9 � Text field ��͹ save ŧ�ҹ������ sumo
     *
     * @param icd9
     */
    public void addICD9(ICD9 icd9) {
        if (icd9 == null) {
            return;
        }
        if (theHO.vIcd9 == null) {
            theHO.vIcd9 = new Vector();
        }
        boolean same = false;
        //��Ǩ�ͺ��ҫ�ӡѺ�����������������
        for (int i = 0, size = theHO.vIcd9.size(); i < size; i++) {
            ICD9 i9 = (ICD9) theHO.vIcd9.get(i);
            if (i9.getObjectId().equals(icd9.getObjectId())) {
                same = true;
            }
        }
        if (!same) {
            theHO.vIcd9.add(icd9.icd9_id);
        }
    }

    /**
     * ������¡�� Item � Vector ��͹��㹵��ҧ
     *
     * @param item �� Object �ͧ��¡�� Item
     * @author Pu
     * @date 09/08/2549
     */
    public void addItemDx(Item item) {
        if (item == null) {
            return;
        }
        DxTemplateMapItem itemDx = new DxTemplateMapItem();
        itemDx.item_id = item.getObjectId();
        itemDx.code = item.item_id;
        itemDx.description = item.common_name;

        if (theHO.vItemDx == null) {
            theHO.vItemDx = new Vector();
        }
        boolean same = false;
        //��Ǩ�ͺ��ҫ�ӡѺ�����������������
        for (int i = 0, size = theHO.vItemDx.size(); i < size; i++) {
            DxTemplateMapItem itdx = (DxTemplateMapItem) theHO.vItemDx.get(i);
            if (itemDx.item_id.equals(itdx.item_id)) {
                same = true;
            }
        }
        if (!same) {
            theHO.vItemDx.add(itemDx);
        }
        theHS.theItemDxSubject.notifySetTableItemDx("�ʴ���¡�� ItemDx 㹵��ҧ", UpdateStatus.NORMAL);
    }

    /**
     * ź��¡�� Item � Vector ��͹��㹵��ҧ
     *
     * @param rows �� �ӹǹ�Ǣͧ��¡�� Item ����ͧ���ź
     * @author Pu
     * @date 09/08/2549
     */
    public void deleteItemDx(int[] rows) {
        try {
            if (rows.length != 0) {
                theConnectionInf.open();
                int size = rows.length;
                for (int i = size - 1; i >= 0; i--) {
                    DxTemplateMapItem itemDx = (DxTemplateMapItem) theHO.vItemDx.get(rows[i]);
                    //�����¡�� Item �ѧ����¶١�ѹ�֡ŧ�ҹ������
                    if (itemDx.getObjectId() != null) {
                        theHosDB.theDxTemplateMapItemDB.delete(itemDx);
                        theHO.vItemDx.remove(itemDx);
                    } else {
                        theHO.vItemDx.remove(itemDx);
                    }
                    theUS.setStatus(ResourceBundle.getBundleText("ź��¡����觵�Ǩ�ͧ Dx") + " "
                            + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
                }
            }
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
        } finally {
            theConnectionInf.close();
        }
        theHS.theItemDxSubject.notifySetTableItemDx(ResourceBundle.getBundleText("�ʴ���¡�� ItemDx 㹵��ҧ"), UpdateStatus.NORMAL);
    }

    /**
     * ������¡�� Item �����͡੾�� Item �ͧ Dx ���١���͡
     *
     * @param pk �� String ����� Primary key �ͧ Dx ���١���͡
     * @author Pu
     * @date 09/08/2549
     */
    public void listItemDxByDxTemplate(String pk) {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHO.vItemDx = theHosDB.theDxTemplateMapItemDB.specialQueryItemDx(pk);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theItemDxSubject.notifySetTableItemDx(ResourceBundle.getBundleText("�ʴ���¡�� ItemDx 㹵��ҧ"), UpdateStatus.NORMAL);
        }

    }

    public void addItemRiskDx(Item item) {
        if (item == null) {
            return;
        }
        DxTemplateMapItemRisk itemDx = new DxTemplateMapItemRisk();
        itemDx.b_item_id = item.getObjectId();
        itemDx.code = item.item_id;
        itemDx.description = item.common_name;

        if (theHO.vItemRiskDx == null) {
            theHO.vItemRiskDx = new ArrayList<>();
        }
        boolean same = false;
        //��Ǩ�ͺ��ҫ�ӡѺ�����������������
        for (int i = 0, size = theHO.vItemRiskDx.size(); i < size; i++) {
            DxTemplateMapItemRisk itdx = (DxTemplateMapItemRisk) theHO.vItemRiskDx.get(i);
            if (itemDx.b_item_id.equals(itdx.b_item_id)) {
                same = true;
            }
        }
        if (!same) {
            theHO.vItemRiskDx.add(itemDx);
        }
        theHS.theItemDxSubject.notifySetTableItemDx("�ʴ���¡�� ItemDx 㹵��ҧ", UpdateStatus.NORMAL);
    }

    public void deleteItemRiskDx(int[] rows) {
        try {
            if (rows.length != 0) {
                theConnectionInf.open();
                int size = rows.length;
                for (int i = size - 1; i >= 0; i--) {
                    DxTemplateMapItemRisk itemDx = (DxTemplateMapItemRisk) theHO.vItemRiskDx.get(rows[i]);
                    //�����¡�� Item �ѧ����¶١�ѹ�֡ŧ�ҹ������
                    if (itemDx.getObjectId() != null) {
                        theHosDB.theDxTemplateMapItemRiskDB.delete(itemDx);
                        theHO.vItemRiskDx.remove(itemDx);
                    } else {
                        theHO.vItemRiskDx.remove(itemDx);
                    }
                    theUS.setStatus(ResourceBundle.getBundleText("ź��¡����觵�Ǩ�ͧ Dx") + " "
                            + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
                }
            }
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
        } finally {
            theConnectionInf.close();
        }
        theHS.theItemDxSubject.notifySetTableItemDx(ResourceBundle.getBundleText("�ʴ���¡�� ItemDx 㹵��ҧ"), UpdateStatus.NORMAL);
    }

    public void listItemRiskDxByDxTemplate(String pk) {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHO.vItemRiskDx = theHosDB.theDxTemplateMapItemRiskDB.specialQueryItemDx(pk);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theItemDxSubject.notifySetTableItemDx(ResourceBundle.getBundleText("�ʴ���¡�� ItemDx 㹵��ҧ"), UpdateStatus.NORMAL);
        }
    }

    /*
     * �����ش��ԡ�� � Text field ��͹ save ŧ�ҹ������ sumo
     */
    public void addServicePoint(ServicePoint theSp) {
        if (theSp == null) {
            return;
        }
        if (theHO.vServicePoint == null) {
            theHO.vServicePoint = new Vector();
        }
    }

    /**
     * @Author amp
     * @date 11/04/2549
     * @see ź��������ҧ���
     * @param bodyOrgan
     * @return
     */
    public int deleteBodyOrgan(BodyOrgan bodyOrgan) {
        int ans = 0;
        if (bodyOrgan == null || bodyOrgan.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return ans;
        }
        boolean confirm = theUS.confirmBox(ResourceBundle.getBundleText("�׹�ѹ����ź��¡�ù��") + " "
                + ResourceBundle.getBundleText("�Ҩ�Դ�š�з��Ѻ������㹰ҹ��������"), UpdateStatus.WARNING);
        if (!confirm) {
            return 0;
        }

        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ans = theHosDB.theBodyOrganDB.delete(bodyOrgan);
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            ans = 0;
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        }
        return ans;
    }

    /**
     * @Author amp
     * @date 14/06/2549
     * @see ź�ä NCD
     * @param ncdGroup
     * @return
     */
    public int deleteNCDGroup(NCDGroup ncdGroup) {
        int ans = 0;
        if (theLookupControl.readOption().life.equals(Active.isEnable())) {
            theUS.setStatus(ResourceBundle.getBundleText("�к����������ҹ����") + " "
                    + ResourceBundle.getBundleText("���ź�������������ö����"), UpdateStatus.WARNING);
            return ans;
        }
        if (ncdGroup == null || ncdGroup.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return ans;
        }
        if (!theUS.confirmBox(ResourceBundle.getBundleText("�׹�ѹ����ź��¡�ù��") + " "
                + ResourceBundle.getBundleText("�Ҩ�Դ�š�з��Ѻ������㹰ҹ��������"), UpdateStatus.WARNING)) {
            return 0;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theNCDGroupDB.delete(ncdGroup);
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_DELETE);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE);
        }
        return ans;
    }

    /**
     * @Author: amp
     * @date: 11/04/2549
     * @see: �������ҵðҹ
     * @param pk �Ӥ�
     * @param active
     * @return ��������ҧ���
     */
    public Vector listBodyOrgan(String pk, String active) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theBodyOrganDB.selectByCodeName(pk, active);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * @Author amp
     * @date 11/04/2549
     * @see �ѹ�֡��������ҧ���
     * @param bodyOrgan ��������ҧ���
     * @return
     */
    public int saveBodyOrgan(BodyOrgan bodyOrgan) {
        if ("".equals(bodyOrgan.number) || "".equals(bodyOrgan.description)) {
            theUS.setStatus("�������ö�ѹ�֡�������繤����ҧ��", UpdateStatus.WARNING);
            return 0;
        }
        if (bodyOrgan.number.length() == 1) {
            bodyOrgan.number = "0" + String.valueOf(Integer.parseInt(bodyOrgan.number));
        }
        BodyOrgan bo = listBodyOrganByNumber(bodyOrgan.number);

        if (bo != null && bodyOrgan.getObjectId() == null) {
            theUS.setStatus("�������ö�ѹ�֡���ʫ����", UpdateStatus.WARNING);
            return 0;
        }
        if (bo != null && bodyOrgan.getObjectId() != null
                && !bodyOrgan.getObjectId().equals(bo.getObjectId())) {
            theUS.setStatus("�������ö�ѹ�֡���ʫ����", UpdateStatus.WARNING);
            return 0;
        }
        int ans = 0;
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (bodyOrgan.getObjectId() == null) {
                theHosDB.theBodyOrganDB.insert(bodyOrgan);
                theUS.setStatus(ResourceBundle.getBundleText("��úѹ�֡��������ҧ���") + " "
                        + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
                ans = 1;
            } else {
                theHosDB.theBodyOrganDB.update(bodyOrgan);
                theUS.setStatus(ResourceBundle.getBundleText("��������������ҧ���") + " "
                        + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
                ans = 1;
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        }
        return ans;
    }

    /**
     * @Author amp
     * @date 11/04/2549
     * @see ������������ҧ���
     * @param number ����
     * @return ��������ҧ���
     */
    public BodyOrgan listBodyOrganByNumber(String number) {
        BodyOrgan theBodyOrgan = new BodyOrgan();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theBodyOrgan = theHosDB.theBodyOrganDB.selectByNumber(number);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theBodyOrgan;
    }

    /**
     * @Author amp
     * @date 26/04/2549
     * @see �ѹ�֡��èѺ����ҵðҹ�дѺ����ҡ��Ẻ����ѺẺ���
     * @param nutritionNew Object �ͧ NutritionType Ẻ����
     * @param vNutritionOld Vector �ͧ Nutrition ���
     * @param row_old Row �ͧ NutritionType ��ҷ�����͡
     * @return
     */
    public int saveNutritionMap(NutritionType nutritionNew, Vector vNutritionOld, int[] row_old) {
        int ans = 0;
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            NutritionType nutritionOld;
            NutritionTypeMap nutritionTypeMap;
            for (int i = 0, size = row_old.length; i < size; i++) {
                nutritionOld = (NutritionType) vNutritionOld.get(row_old[i]);
                nutritionTypeMap = theHosDB.theNutritionTypeMapDB.selectByNutritionOld(nutritionOld.getObjectId());
                if (nutritionTypeMap == null) {
                    nutritionTypeMap = new NutritionTypeMap();
                    nutritionTypeMap.nutrition_new = nutritionNew.getObjectId();
                    nutritionTypeMap.nutrition_old = nutritionOld.getObjectId();
                    theHosDB.theNutritionTypeMapDB.insert(nutritionTypeMap);
                    theUS.setStatus(ResourceBundle.getBundleText("�ѹ�֡��èѺ����дѺ����ҡ��") + " "
                            + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
                } else {
                    nutritionTypeMap.nutrition_new = nutritionNew.getObjectId();
                    theHosDB.theNutritionTypeMapDB.update(nutritionTypeMap);
                    theUS.setStatus(ResourceBundle.getBundleText("��䢡�èѺ����дѺ����ҡ��") + " "
                            + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
                }
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, "�ʴ�����ѵ��ä NCD");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "�ʴ�����ѵ��ä NCD");
        }
        return ans;
    }

    /**
     * @Author: amp
     * @date: 26/04/2549
     * @see: �����дѺ����ҡ�÷��Ѻ���ѹ����
     * @return Vector �ͧ NutritionMap
     */
    public Vector listNutritionMap() {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theNutritionTypeMapDB.listNutritionMap();
            theLO.vNutritionTypeMap = vc;
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * @Author amp
     * @date 14/06/2549
     * @see �ѹ�֡�ä NCD
     * @param ncdGroup �ä NCD
     * @return
     */
    public int saveNCDGroup(NCDGroup ncdGroup) {
        if ("".equals(ncdGroup.number)) {
            theUS.setStatus("�������ö�ѹ�֡�����繤����ҧ��", UpdateStatus.WARNING);
            return 0;
        }
        if ("".equals(ncdGroup.description)) {
            theUS.setStatus("�������ö�ѹ�֡�����繤����ҧ��", UpdateStatus.WARNING);
            return 0;
        }
        if ("".equals(ncdGroup.value)) {
            theUS.setStatus("��س��кؤ�ҡ�͹�ӡ�úѹ�֡", UpdateStatus.WARNING);
            return 0;
        }
        NCDGroup ncdg = readNCDGroupByNumber(ncdGroup.number);

        if (ncdg != null && ncdGroup.getObjectId() == null) {
            theUS.setStatus("�������ö�ѹ�֡���ʫ����", UpdateStatus.WARNING);
            return 0;
        }
        if (ncdg != null && ncdGroup.getObjectId() != null
                && !ncdGroup.getObjectId().equals(ncdg.getObjectId())) {
            theUS.setStatus("�������ö�ѹ�֡���ʫ����", UpdateStatus.WARNING);
            return 0;
        }
        int ans = 0;
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (ncdGroup.getObjectId() == null) {
                theHosDB.theNCDGroupDB.insert(ncdGroup);
                theUS.setStatus(ResourceBundle.getBundleText("��úѹ�֡�ä NCD") + " "
                        + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
                ans = 1;
            } else {
                theHosDB.theNCDGroupDB.update(ncdGroup);
                theUS.setStatus(ResourceBundle.getBundleText("�������ä NCD") + " "
                        + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
                ans = 1;
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        }
        return ans;
    }

    /**
     * @Author amp
     * @date 14/06/2549
     * @see �����ä NCD
     * @param number ����
     * @return �ä NCD
     */
    public NCDGroup readNCDGroupByNumber(String number) {
        NCDGroup theNCDGroup = new NCDGroup();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theNCDGroup = theHosDB.theNCDGroupDB.selectByNumber(number);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theNCDGroup;
    }

    /**
     * @param pk
     * @return
     * @author amp
     * @date 19/06/2549
     */
    public NCDGroup readNCDGroupByPk(String pk) {
        NCDGroup theNCDGroup = new NCDGroup();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theNCDGroup = theHosDB.theNCDGroupDB.selectByPK(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theNCDGroup;
    }

    /**
     * @param theUS
     * @return
     * @author henbe ��ѭ�� �鹼����ª�� �¡�����ҧ index family_id in table
     * t_patient
     *
     * CREATE INDEX visit_doctor_discharge_datetime ON
     * public.t_visit(visit_staff_doctor_discharge_date_time)
     *
     * CREATE INDEX t_helath_family_patient_pid ON
     * public.t_health_family(patient_pid) CREATE INDEX t_patient_pid ON
     * public.t_patient(patient_pid)
     *
     *
     */
    public int createIndex(UpdateStatus theUS) {
        if (!theUS.confirmBox("�׹�ѹ������ҧ Index �������", UpdateStatus.WARNING)) {
            return 0;
        }
        int total = 0;
        theConnectionInf.open();
        try {
            total += theConnectionInf.eUpdate("CREATE INDEX t_health_family_key"
                    + " ON public.t_patient(t_health_family_id)");
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
        }
        try {
            total += theConnectionInf.eUpdate("CREATE INDEX visit_doctor_discharge_datetime "
                    + " ON public.t_visit(visit_staff_doctor_discharge_date_time)");
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
        }
        try {
            total += theConnectionInf.eUpdate("CREATE INDEX family_health_family_active"
                    + " ON public.t_health_family(health_family_active)");
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
        }

        try {
            total += theConnectionInf.eUpdate("CREATE INDEX order_drug_active "
                    + "ON public.t_order_drug(order_drug_active)");
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
        }

        theUS.setStatus(ResourceBundle.getBundleText("������ҧ t_health_family_key �������") + " "
                + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS") + " " + total + " "
                + ResourceBundle.getBundleText("��¡��"), UpdateStatus.COMPLETE);
        theConnectionInf.close();
        return total;
    }

    public int deleteGroupIcd10(GroupIcd10 theGroupIcd10) {
        if (theGroupIcd10 == null || theGroupIcd10.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return 0;
        }
        if (theGroupIcd10.getObjectId() != null && theGroupIcd10.getObjectId().length() < 15) {
            theUS.setStatus("����������ͧ�к��������öź��", UpdateStatus.WARNING);
            return 0;
        }
        int ret = 0;
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ret = theHosDB.theGroupIcd10DB.delete(theGroupIcd10);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, "ź�����š�èѴ����������ä");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theUS.setStatus(ResourceBundle.getBundleText("���ź�����š�èѴ����������ä") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        }
        return ret;
    }

    public void cleanTrans() {
        if (!theUS.confirmBox(ResourceBundle.getBundleText("�׹�ѹ��ä�ҧ�ѹ�֡������ѹ��������") + " ("
                + ResourceBundle.getBundleText("����͢����ѹ�зӡ�ä�ҧ�ѹ�֡�����ѹ��͹˹��") + ")", UpdateStatus.WARNING)) {
            return;
        }
        int ret = theCleanTrans.con(2);
        theUS.setStatus(ResourceBundle.getBundleText("��ä�ҧ�ѹ�֡������ѹ��������") + " " + ret + " "
                + ResourceBundle.getBundleText("��¡��"), UpdateStatus.COMPLETE);
    }

    public void resetYear() {
        if (!theUS.confirmBox(ResourceBundle.getBundleText("�׹�ѹ������������Ţ�ӴѺ������") + " ("
                + ResourceBundle.getBundleText("����͢�鹻��������Ţ���к���лջѨ�غѹᵡ��ҧ�ѹ") + ")", UpdateStatus.WARNING)) {
            return;
        }
        int ret = theCleanTrans.con(3);
        if (ret == 0) {
            theUS.setStatus("������������Ţ�ӴѺ����������ͧ�ҡ���к���лջѨ�غѹ�繤�����ǡѹ����", UpdateStatus.WARNING);
        } else {
            theUS.setStatus(ResourceBundle.getBundleText("������������Ţ�ӴѺ����͢�����") + " ("
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        }
    }

    /**
     * henbe comment 100253 ton ��ѧ�ѹ����ѹ����������Ƿ����֧��ͧ�����
     *
     * @param item_id
     * @param sort
     * @return
     */
    public Vector listLabSetByItemId(String item_id, String sort) {
        Vector vRet = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            //�ӡ�èѴ���§���������ͧ͢��¡���Ż���¹���
            vRet = intListLabSetByItemId(item_id, sort);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vRet;
    }
//
//    private boolean intCheckSaveSeqDFN(SequenceData sequenceData, int vn_seq) throws Exception {
//        String vn_slastest = theHosDB.theFamilyDB.selectMaxHCIS();
//        int year_index = sequenceData.pattern.lastIndexOf("y");
//        try {
//            if (vn_slastest != null) {
//                if (year_index != -1) {
//                    vn_slastest = vn_slastest.substring(year_index + 1);
//                }
//                int vn_lastest = Integer.parseInt(vn_slastest);
//                if (vn_lastest >= vn_seq) {
//                    boolean res = theUS.confirmBox(ResourceBundle.getBundleText("���Ţ DrugFund ����ش���") + " " + vn_lastest
//                            + " " + ResourceBundle.getBundleText("�׹�ѹ��úѹ�֡"), UpdateStatus.WARNING);
//                    if (!res) {
//                        return false;
//                    }
//                }
//            }
//            return true;
//        } catch (Exception ex) {
//            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
//            boolean res = theUS.confirmBox(ResourceBundle.getBundleText("���Ţ DrugFund ����ش��������Ţ")
//                    + " " + vn_slastest
//                    + " " + " "
//                    + ResourceBundle.getBundleText("�������ö��Ǩ�ͺ����ش������")
//                    + " "
//                    + ResourceBundle.getBundleText("�׹�ѹ��úѹ�֡"), UpdateStatus.WARNING);
//            if (!res) {
//                return false;
//            }
//            return true;
//        }
//    }

    private boolean intCheckSaveSeqXN(SequenceData sequenceData, int vn_seq) throws Exception {
        String vn_slastest = theHosDB.thePatientDB.selectMaxXN(sequenceData.pattern);
        int year_index = sequenceData.pattern.lastIndexOf('y');
        try {
            if (vn_slastest != null) {
                if (year_index != -1) {
                    vn_slastest = vn_slastest.substring(year_index + 1);
                }
                int vn_lastest = Integer.parseInt(vn_slastest);
                if (vn_lastest >= vn_seq) {
                    boolean res = theUS.confirmBox(ResourceBundle.getBundleText("���Ţ XN ����ش���") + " " + vn_lastest
                            + " " + ResourceBundle.getBundleText("�׹�ѹ��úѹ�֡"), UpdateStatus.WARNING);
                    if (!res) {
                        return false;
                    }
                }
            }
            return true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            boolean res = theUS.confirmBox(ResourceBundle.getBundleText("���Ţ XN ����ش��������Ţ")
                    + " " + vn_slastest
                    + " " + " "
                    + ResourceBundle.getBundleText("�������ö��Ǩ�ͺ����ش������")
                    + " "
                    + ResourceBundle.getBundleText("�׹�ѹ��úѹ�֡"), UpdateStatus.WARNING);
            if (!res) {
                return false;
            }
            return true;
        }
    }

    private boolean intCheckSaveSeqRFON(SequenceData sequenceData, int vn_seq) throws Exception {
        String vn_slastest = theHosDB.theReferDB.selectMaxOut(sequenceData.pattern);
        int year_index = sequenceData.pattern.lastIndexOf('y');
        try {
            if (vn_slastest != null) {
                if (year_index != -1) {
                    vn_slastest = vn_slastest.substring(year_index + 1);
                }
                int vn_lastest = Integer.parseInt(vn_slastest);
                if (vn_lastest >= vn_seq) {
                    boolean res = theUS.confirmBox(ResourceBundle.getBundleText("���Ţ ReferOut ����ش���") + " " + vn_lastest
                            + " " + ResourceBundle.getBundleText("�׹�ѹ��úѹ�֡"), UpdateStatus.WARNING);
                    if (!res) {
                        return false;
                    }
                }
            }
            return true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            boolean res = theUS.confirmBox(ResourceBundle.getBundleText("���Ţ ReferOut ����ش��������Ţ")
                    + " " + vn_slastest
                    + " " + " "
                    + ResourceBundle.getBundleText("�������ö��Ǩ�ͺ����ش������")
                    + " "
                    + ResourceBundle.getBundleText("�׹�ѹ��úѹ�֡"), UpdateStatus.WARNING);
            if (!res) {
                return false;
            }
            return true;
        }
    }

    private boolean intCheckSaveSeqRFIN(SequenceData sequenceData, int vn_seq) throws Exception {
        String vn_slastest = theHosDB.theReferDB.selectMaxIn(sequenceData.pattern);
        int year_index = sequenceData.pattern.lastIndexOf('y');
        try {
            if (vn_slastest != null) {
                if (year_index != -1) {
                    vn_slastest = vn_slastest.substring(year_index + 1);
                }
                int vn_lastest = Integer.parseInt(vn_slastest);
                if (vn_lastest >= vn_seq) {
                    boolean res = theUS.confirmBox(ResourceBundle.getBundleText("���Ţ ReferIn ����ش���") + " " + vn_lastest
                            + " " + ResourceBundle.getBundleText("�׹�ѹ��úѹ�֡"), UpdateStatus.WARNING);
                    if (!res) {
                        return false;
                    }
                }
            }
            return true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            boolean res = theUS.confirmBox(ResourceBundle.getBundleText("���Ţ ReferIn ����ش��������Ţ")
                    + " " + vn_slastest
                    + " " + " "
                    + ResourceBundle.getBundleText("�������ö��Ǩ�ͺ����ش������")
                    + " "
                    + ResourceBundle.getBundleText("�׹�ѹ��úѹ�֡"), UpdateStatus.WARNING);
            if (!res) {
                return false;
            }
            return true;
        }
    }

    private boolean intCheckSaveSeqRN(SequenceData sequenceData, int vn_seq) throws Exception {
        String vn_slastest = theHosDB.theReceiptDB.selectMaxRN();
        int year_index = sequenceData.pattern.lastIndexOf('y');
        try {
            if (vn_slastest != null) {
                if (year_index != -1) {
                    vn_slastest = vn_slastest.substring(year_index + 1);
                }
                try {
                    int vn_lastest = Integer.parseInt(vn_slastest);
                    if (vn_lastest >= vn_seq) {
                        boolean res = theUS.confirmBox(ResourceBundle.getBundleText("���Ţ����� ����ش���") + " " + vn_lastest
                                + " " + ResourceBundle.getBundleText("�׹�ѹ��úѹ�֡"), UpdateStatus.WARNING);
                        if (!res) {
                            return false;
                        }
                    }
                } catch (Exception ex) {
                    LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
                }
            }
            return true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            boolean res = theUS.confirmBox(ResourceBundle.getBundleText("���Ţ����� ����ش��������Ţ")
                    + " " + vn_slastest
                    + " " + " "
                    + ResourceBundle.getBundleText("�������ö��Ǩ�ͺ����ش������")
                    + " "
                    + ResourceBundle.getBundleText("�׹�ѹ��úѹ�֡"), UpdateStatus.WARNING);
            if (!res) {
                return false;
            }
            return true;
        }
    }

    private boolean intCheckSaveSeqMedCer(SequenceData sequenceData, int seqNo) throws Exception {
        int typeId = sequenceData.getObjectId().equals("mc") ? 1
                : sequenceData.getObjectId().equals("mc_w") ? 2
                : 3;
        String lastestNo = theHosDB.theMedicalCertificateDB.selectMaxNoByType(typeId);
        int year_index = sequenceData.pattern.lastIndexOf('y');
        try {
            if (lastestNo != null) {
                if (year_index != -1) {
                    lastestNo = lastestNo.substring(year_index + 1);
                }
                int lastest = Integer.parseInt(lastestNo);
                if (lastest >= seqNo) {
                    boolean res = theUS.confirmBox(ResourceBundle.getBundleText("���Ţ����ش���") + " " + lastest
                            + " " + ResourceBundle.getBundleText("�׹�ѹ��úѹ�֡"), UpdateStatus.WARNING);
                    if (!res) {
                        return false;
                    }
                }
            }
            return true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            boolean res = theUS.confirmBox(ResourceBundle.getBundleText("���Ţ����ش��������Ţ")
                    + " " + lastestNo
                    + " " + " "
                    + ResourceBundle.getBundleText("�������ö��Ǩ�ͺ����ش������")
                    + " "
                    + ResourceBundle.getBundleText("�׹�ѹ��úѹ�֡"), UpdateStatus.WARNING);
            if (!res) {
                return false;
            }
            return true;
        }
    }

    /**
     * ������¡�� ICD10 ����������� ICD10 � Vector ��͹��㹵��ҧ
     * GroupChronic
     *
     * @param icd10 �� Object �ͧ��¡�� ICD10
     *
     */
    public void addICD10GCGroup(ICD10 icd10) {
        GroupIcd10 icdgc = new GroupIcd10();
        if (icd10 == null) {
            return;
        }
        icdgc.icdcode = icd10.icd10_id;
        if (theHO.vICD10GCGroup == null) {
            theHO.vICD10GCGroup = new Vector();
        }
        if (theHO.vICD10GCSpecifyCode == null) {
            theHO.vICD10GCSpecifyCode = new Vector();
        }
        boolean same = false;
        //��Ǩ�ͺ��ҫ�ӡѺ���ʡ���������������������
        for (int i = 0, size = theHO.vICD10GCGroup.size(); i < size; i++) {
            GroupIcd10 icdgcc = (GroupIcd10) theHO.vICD10GCGroup.get(i);
            if (icdgc.icdcode.equals(icdgcc.icdcode)) {
                same = true;
            }
        }
        if (!same) {
            theHO.vICD10GCGroup.add(icdgc);
        }
        theHS.theICD10GroupChronicSubject.notifySetTableICD10GCGroup("�ʴ���¡�� ICD10Group 㹵��ҧ", UpdateStatus.NORMAL);
    }

    /**
     * ������¡�� ICD10 ���������� ICD10 � Vector ��͹��㹵��ҧ GroupChronic
     *
     * @param icd10 �� Object �ͧ��¡�� ICD10
     *
     */
    public void addICD10GCSpecifyCode(ICD10 icd10) {
        GroupIcd10 icdgc = new GroupIcd10();
        if (icd10 == null) {
            return;
        }
        icdgc.icdcode = icd10.icd10_id;
        if (theHO.vICD10GCSpecifyCode == null) {
            theHO.vICD10GCSpecifyCode = new Vector();
        }
        if (theHO.vICD10GCGroup == null) {
            theHO.vICD10GCGroup = new Vector();
        }
        boolean same = false;
        //��Ǩ�ͺ��ҫ�ӡѺ����੾�з����������������
        for (int i = 0, size = theHO.vICD10GCSpecifyCode.size(); i < size; i++) {
            GroupIcd10 icdgcc = (GroupIcd10) theHO.vICD10GCSpecifyCode.get(i);
            if (icdgc.icdcode.equals(icdgcc.icdcode)) {
                same = true;
            }
        }
        if (theHO.vICD10GCGroup != null && !theHO.vICD10GCGroup.isEmpty()) {
            //��Ǩ�ͺ��ҫ�ӡѺ���ʡ���������������������
            for (int i = 0, size = theHO.vICD10GCGroup.size(); i < size; i++) {
                GroupIcd10 icdgcg = (GroupIcd10) theHO.vICD10GCGroup.get(i);
                if (icdgc.icdcode.equals(icdgcg.icdcode)) {
                    same = true;
                }
            }
        }
        if (!same) {
            theHO.vICD10GCSpecifyCode.add(icdgc);
        }
        theHS.theICD10GroupChronicSubject.notifySetTableICD10GCSpecifyCode("�ʴ���¡�� ICD10SpecifyCode 㹵��ҧ", UpdateStatus.NORMAL);
    }

    /**
     * ������¡�� ICD10 ����������� ICD10 � Vector ��͹��㹵��ҧ
     * ICD10GroupSurveil
     *
     * @param icd10 �� Object �ͧ��¡�� ICD10
     * @author Pu
     * @date 10/09/2551
     */
    public void addICD10GSGroup(ICD10 icd10) {
        ICD10GroupSurveil icdgs = new ICD10GroupSurveil();
        if (icd10 == null) {
            return;
        }
        icdgs.icd10_id = icd10.getObjectId();
        icdgs.icd_number = icd10.icd10_id;
        icdgs.icd10_surveil_type = ICD10GroupSurveil.GROUP_TYPE;
        if (theHO.vICD10GSGroup == null) {
            theHO.vICD10GSGroup = new Vector();
        }
        if (theHO.vICD10GSSpecifyCode == null) {
            theHO.vICD10GSSpecifyCode = new Vector();
        }
        boolean same = false;
        //��Ǩ�ͺ��ҫ�ӡѺ�����������������
        for (int i = 0, size = theHO.vICD10GSGroup.size(); i < size; i++) {
            ICD10GroupSurveil icdgsc = (ICD10GroupSurveil) theHO.vICD10GSGroup.get(i);
            if (icdgs.icd10_id.equals(icdgsc.icd10_id)) {
                same = true;
            }
        }
        //��Ǩ�ͺ��ҫ�ӡѺ����੾�з����������������
        if (theHO.vICD10GSSpecifyCode != null || !theHO.vICD10GSSpecifyCode.isEmpty()) {
            for (int i = 0, size = theHO.vICD10GSSpecifyCode.size(); i < size; i++) {
                ICD10GroupSurveil icdgss = (ICD10GroupSurveil) theHO.vICD10GSSpecifyCode.get(i);
                if (icdgs.icd_number.equals(icdgss.icd_number.substring(0, 3))) {
                    same = true;
                }
            }
        }
        if (!same) {
            theHO.vICD10GSGroup.add(icdgs);
        }
        theHS.theICD10GroupSurveilSubject.notifySetTableICD10GSGroup("�ʴ���¡�� ICD10Group 㹵��ҧ", UpdateStatus.NORMAL);
    }

    /**
     * ������¡�� ICD10 ���������� ICD10 � Vector ��͹��㹵��ҧ
     * ICD10GroupSurveil
     *
     * @param icd10 �� Object �ͧ��¡�� ICD10
     * @author Pu
     * @date 10/09/2551
     */
    public void addICD10GSSpecifyCode(ICD10 icd10) {
        ICD10GroupSurveil icdgs = new ICD10GroupSurveil();
        if (icd10 == null) {
            return;
        }
        icdgs.icd10_id = icd10.getObjectId();
        icdgs.icd_number = icd10.icd10_id;
        icdgs.icd10_surveil_type = ICD10GroupSurveil.CODE_TYPE;

        if (theHO.vICD10GSSpecifyCode == null) {
            theHO.vICD10GSSpecifyCode = new Vector();
        }
        if (theHO.vICD10GSGroup == null) {
            theHO.vICD10GSGroup = new Vector();
        }
        boolean same = false;
        //��Ǩ�ͺ��ҫ�ӡѺ�����������������
        for (int i = 0, size = theHO.vICD10GSSpecifyCode.size(); i < size; i++) {
            ICD10GroupSurveil icdgsc = (ICD10GroupSurveil) theHO.vICD10GSSpecifyCode.get(i);
            if (icdgs.icd10_id.equals(icdgsc.icd10_id)) {
                same = true;
            }
        }
        if (theHO.vICD10GSGroup != null && !theHO.vICD10GSGroup.isEmpty()) {
            //��Ǩ�ͺ��ҫ�ӡѺ���ʡ���������������������
            for (int i = 0, size = theHO.vICD10GSGroup.size(); i < size; i++) {
                ICD10GroupSurveil icdgsc = (ICD10GroupSurveil) theHO.vICD10GSGroup.get(i);
                if (icdgs.icd_number.substring(0, 3).equals(icdgsc.icd_number)) {
                    same = true;
                }
            }
        }
        if (!same) {
            theHO.vICD10GSSpecifyCode.add(icdgs);
        }
        theHS.theICD10GroupSurveilSubject.notifySetTableICD10GSSpecifyCode("�ʴ���¡�� ICD10SpecifyCode 㹵��ҧ", UpdateStatus.NORMAL);
    }

    public Vector listPatientRemain(boolean begin_with, String name, String date_from, String date_to) {
        Vector vc = new Vector();
        name = Gutil.CheckReservedWords(name);
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String sql = " select "
                    + "t_billing.t_patient_id"
                    + ",t_patient.patient_hn"
                    + ",patient_firstname"
                    + ",patient_lastname"
                    + ",sum(billing_remain) as remain"
                    + ",max(billing_billing_date_time)"
                    + " from t_billing "
                    + " inner join t_patient on t_billing.t_patient_id = t_patient.t_patient_id"
                    + " where billing_remain>0 and t_billing.billing_active = '1'";
            if (name.length() != 0) {
                if (!begin_with) {
                    name = "%" + name;
                }
                sql += " and (patient_hn like '" + name + "%'"
                        + " or patient_firstname like '" + name + "%'"
                        + " or patient_lastname like '" + name + "%')";
            }
            if (date_from != null && date_to != null) {
                sql += " and substr(billing_billing_date_time,1,10) >= '" + date_from + "'"
                        + " and substr(billing_billing_date_time,1,10) <= '" + date_to + "'";
            }
            sql += " group by t_patient.patient_hn,patient_firstname,patient_lastname,t_billing.t_patient_id"
                    + " order by patient_hn limit 5000";

            ResultSet rs = theConnectionInf.eQuery(sql);
            while (rs.next()) {
                String[] array = new String[6];
                array[0] = rs.getString(1);
                array[1] = rs.getString(2);
                array[2] = rs.getString(3);
                array[3] = rs.getString(4);
                array[4] = rs.getString(5);
                array[5] = rs.getString(6);
                vc.add(array);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public void saveRemainZero(Vector vItem, int[] row) {
        boolean isComplete = false;
        theConnectionInf.open();
        int ret = 0;
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (int i = 0; i < row.length; i++) {
                String[] data = (String[]) vItem.get(row[i]);
                String patient_id = data[0];
                String sql = " update t_billing set billing_remain = 0 where t_patient_id = '" + patient_id + "'";
                ret += theConnectionInf.eUpdate(sql);
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(ResourceBundle.getBundleText("���ᷧ˹���٭���Ѻ������")
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theUS.setStatus("���ᷧ˹���٭���Ѻ�����¨ӹǹ" + ret + " ��¡���������", UpdateStatus.COMPLETE);
        }
    }

    public void saveDxTemplate(String string) {
        saveDxTemplate(new DxTemplate(string));
    }

    public void saveOptionDetail(OptionDetail optionDetail) {
        if (optionDetail == null) {
            return;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            int ret = theHosDB.theOptionDetailDB.update(optionDetail);
            if (ret == 0) {
                theHosDB.theOptionDetailDB.insert(optionDetail);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
    }

    public int saveItemDrug(Item item, Drug drug) {
        //�ѭ�Ҥ�Ͱҹ�������ѹ�红������� double �����������ö�ѹ�֡�繤����ҧ��
        if (drug.dose.trim().length() != 0) {
            try {
                Double.parseDouble(drug.dose);
            } catch (Exception ex) {
                theUS.setStatus("��س��кػ���ҳ������繨ӹǹ����Ţ", UpdateStatus.WARNING);
                return 1;
            }
        }
        if (drug.qty.trim().length() != 0) {
            try {
                Double.parseDouble(drug.qty);
            } catch (Exception ex) {
                theUS.setStatus("��س��кػ���ҳ�������繨ӹǹ����Ţ", UpdateStatus.WARNING);
                return 2;
            }
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (drug.getObjectId() == null) {
                drug.item_id = item.getObjectId();
                theHosDB.theDrugDB.insert(drug);
            } else {
                theHosDB.theDrugDB.update(drug);
            }
            theConnectionInf.getConnection().commit();
            return 0;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            return -1;
        } finally {
            theConnectionInf.close();
        }
    }

    public int deleteDrug(Drug drug) {
        int ret = -1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector v = theHosDB.theDrugDB.selectByItemV(drug.item_id);
            if (v.size() >= 1) {
                theUS.setStatus("�������öź�������Ҿ�鹰ҹ�ͧ��¡�õ�Ǩ�ѡ����", UpdateStatus.WARNING);
                ret = 1;
                throw new Exception("�������öź�������Ҿ�鹰ҹ�ͧ��¡�õ�Ǩ�ѡ����");
            }
            theHosDB.theDrugDB.delete(drug);
            ret = 0;
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ret;
    }

    public Vector listReceiptSequance() {
        Vector v = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            v = this.theHosDB.theReceiptSequanceDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return v;
    }

    public Vector listReceiptSequance(String txt) {
        Vector v = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            v = this.theHosDB.theReceiptSequanceDB.select(txt);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return v;
    }

    public ReceiptSequance getReceiptSequance(String id) {
        ReceiptSequance rs = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            rs = this.theHosDB.theReceiptSequanceDB.selectByID(id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return rs;
    }

    public int saveReceiptSequance(ReceiptSequance receiptSequance) {
        int res = 0;
        if (receiptSequance.receipt_sequence_ip.trim().isEmpty()) {
            return 4;
        }
        if (receiptSequance.receipt_sequence_pattern.trim().isEmpty()) {
            return 5;
        }
        if (receiptSequance.receipt_sequence_value.trim().isEmpty()) {
            return 6;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ReceiptSequance rs = this.theHosDB.theReceiptSequanceDB.selectBySP2(receiptSequance);
            if (rs != null) {
                res = 7;
                throw new Exception();
            }
            // Somprasong 28102011 ������õ�Ǩ�ͺ�������������ӡѺ ip ����������
            rs = this.theHosDB.theReceiptSequanceDB.selectByBookNumber(receiptSequance);
            if (rs != null) {
                JOptionPane.showMessageDialog(null, "�������ö��������� " + rs.book_no + " ��\n"
                        + "���ͧ�ҡ���������١�����ǡѺ IP Address : " + rs.receipt_sequence_ip, "����͹", JOptionPane.WARNING_MESSAGE);
                res = 8;
                throw new Exception();
            }
            // ������Ǩ�ͺ�Ţ����������ҫ������
            int sd_value = 1;
            try {
                sd_value = Integer.parseInt(receiptSequance.receipt_sequence_value);
            } catch (Exception e) {
            }
            String receiptNo = theHosDB.theReceiptSequanceDB.getSeqPattern(receiptSequance.receipt_sequence_pattern, sd_value);
            if (theHosDB.theReceiptDB.selectByReceiptNo(receiptNo) != null) {
                int ret = JOptionPane.showConfirmDialog(null,
                        "��Ǩ�ͺ���ӴѺ�Ţ�������稫�� �ҡ�ٻẺ����˹�\n��ͧ�������������Ѻ�繤������ش����ѵ��ѵ� �������\n***�Ҩ�����ҹҹ***",
                        "�׹�ѹ",
                        JOptionPane.OK_CANCEL_OPTION);
                if (ret == JOptionPane.OK_OPTION) {
                    do {
                        receiptNo = theHosDB.theReceiptSequanceDB.getSeqPattern(receiptSequance.receipt_sequence_pattern, ++sd_value);
                    } while (theHosDB.theReceiptDB.selectByReceiptNo(receiptNo) != null);
                    receiptSequance.receipt_sequence_value = String.valueOf(sd_value);
                } else {
                    throw new Exception("dup");
                }
            }
            if (receiptSequance.getObjectId() == null) {
                this.theHosDB.theReceiptSequanceDB.insert(receiptSequance);
            } else {
                this.theHosDB.theReceiptSequanceDB.update(receiptSequance);
            }
            // Somprasong 28102011 ���͡�úѹ�֡������ŧ���ҧ book_seq ��Ң����������
            ReceiptBookSeq bookSeq = theHosDB.theReceiptBookSeqDB.selectByBookNumber(Integer.parseInt(receiptSequance.book_no));
            if (bookSeq == null) {
                int lastNumber = theHosDB.theReceiptDB.findLastNumberFromBookNo(receiptSequance.book_no);
                bookSeq = new ReceiptBookSeq();
                bookSeq.book_number = Integer.parseInt(receiptSequance.book_no);
                bookSeq.current_seq = lastNumber == 0 ? Integer.parseInt(receiptSequance.begin_no) - 1 : lastNumber;
                theHosDB.theReceiptBookSeqDB.insert(bookSeq);
            }
            theConnectionInf.getConnection().commit();
            this.theUS.setStatus("�ѹ�֡�������������", UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            if (!ex.getMessage().equals("dup")) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                this.theUS.setStatus("�ѹ�֡�����żԴ��Ҵ", UpdateStatus.ERROR);
            }
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public int deleteLabMapping(String b_item_id) {
        int res = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            res = this.theHosDB.theLabMappingDB.deleteByItem(b_item_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public int saveLabMapping(LabMapping labMapping) {
        int res = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (labMapping.getObjectId() == null) {
                res = this.theHosDB.theLabMappingDB.insert(labMapping);
            } else {
                res = this.theHosDB.theLabMappingDB.update(labMapping);
            }
            this.theUS.setStatus("�ѹ�֡�������������", UpdateStatus.COMPLETE);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            this.theUS.setStatus("�ѹ�֡�����żԴ��Ҵ", UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public LabMapping getLabMappingByItem(String b_item_id) {
        LabMapping lm = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            lm = this.theHosDB.theLabMappingDB.getByItem(b_item_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return lm;
    }

    public Vector listLabMappingByItem(String b_item_id) {
        Vector v = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            v = this.theHosDB.theLabMappingDB.selectByItem(b_item_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return v;
    }

    public int deleteReceiptSequance(ReceiptSequance receiptSequance) {
        int res = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            res = this.theHosDB.theReceiptSequanceDB.delete(receiptSequance);
            this.theUS.setStatus("�ѹ�֡�������������", UpdateStatus.COMPLETE);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            this.theUS.setStatus("�ѹ�֡�����żԴ��Ҵ", UpdateStatus.COMPLETE);
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public List<DataSource> listItemDrugMapG6PD(String keyword) {
        List<DataSource> list = new ArrayList<DataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> listByKeyword = this.theHosDB.theItemDrugMapG6pdDB.listByKeyword(keyword);
            for (Object[] objects : listByKeyword) {
                list.add(new DataSource(String.valueOf(objects[0]), String.valueOf(objects[1])));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public List<DataSource> listItemNotMapDrugMapG6PD(String keyword) {
        List<DataSource> list = new ArrayList<DataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> listByKeyword = this.theHosDB.theItemDrugMapG6pdDB.listNotMapByKeyword(keyword);
            for (Object[] objects : listByKeyword) {
                list.add(new DataSource(String.valueOf(objects[0]), String.valueOf(objects[1])));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public int saveItemDrugMapG6PD(ItemDrugMapG6PD... drs) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (ItemDrugMapG6PD itemDrugMapG6PD : drs) {
                this.theHosDB.theItemDrugMapG6pdDB.insert(itemDrugMapG6PD);
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public int deleteItemDrugMapG6PD(ItemDrugMapG6PD... dfs) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (ItemDrugMapG6PD itemDrugMapG6PD : dfs) {
                this.theHosDB.theItemDrugMapG6pdDB.delete(itemDrugMapG6PD);
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public List<ComplexDataSource> listMapItemDrugStdMapItem(String keyword) {
        List<ComplexDataSource> list = new ArrayList<ComplexDataSource>();
        return list;
    }

    public List<DataSource> listNotMapItemDrugStdMapItem() {
        List<DataSource> list = new ArrayList<DataSource>();
        return list;
    }

    public List<ItemEdStandard> listItemEdStandard() {
        List<ItemEdStandard> list = new ArrayList<ItemEdStandard>();
        return list;
    }

    public int saveItemDrugStdMapItem(ItemDrugStdMapItem drs) {
        int res = 0;
        return res;
    }

    public int deleteItemDrugStdMapItem(ItemDrugStdMapItem dfs) {
        int res = 0;
        return res;
    }

    public int doImportOldDrugAllergyData(String pharmaId) {
        // check column
        try {
            theConnectionInf.eQuery("select flag from t_patient_drug_allergy_backup");
        } catch (Exception ex) {
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(SetupControl.class.getName()).log(Level.SEVERE, null, ex1);
            }

            theConnectionInf.open();
            try {
                theConnectionInf.getConnection().setAutoCommit(false);
                theConnectionInf.eUpdate("ALTER TABLE t_patient_drug_allergy_backup "
                        + "ADD COLUMN flag character varying(1) NOT NULL DEFAULT '0'");
                theConnectionInf.getConnection().commit();
            } catch (Exception e) {
                this.theUS.setStatus("�������ö���ҧ Column flag ��", UpdateStatus.ERROR);
                LOG.log(java.util.logging.Level.SEVERE, null, e);
                try {
                    theConnectionInf.getConnection().rollback();
                } catch (SQLException ex1) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex1);
                }
                return 0;
            } finally {
                theConnectionInf.close();
            }
        }
        // process
        theConnectionInf.open();
        try {
            int count = 0;
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector<PatientDrugAllergy> pdas = theHosDB.thePatientDrugAllergyDB.seclectUnImported(pharmaId);
            for (PatientDrugAllergy pda : pdas) {
                ResultSet rs = theConnectionInf.eQuery("select t_patient_drug_allergy_id from t_patient_drug_allergy where t_patient_id = '" + pda.t_patient_id + "' "
                        + "and b_item_drug_standard_id = '" + pda.b_item_drug_standard_id + "' and active = '1' ");
                if (!rs.next()) {
                    theHosDB.thePatientDrugAllergyDB.insert(pda);
                    theHosDB.thePatientDrugAllergyDB.updateImported(pda);
                    count++;
                }
            }
            theConnectionInf.getConnection().commit();
            if (count == 0) {
                this.theUS.setStatus("�������¡�ù����", UpdateStatus.WARNING);
            } else {
                this.theUS.setStatus("������������ (�ӹǹ " + count + " ��¡��)", UpdateStatus.COMPLETE);
            }
            return 1;
        } catch (Exception ex) {
            this.theUS.setStatus("����ҼԴ��Ҵ", UpdateStatus.ERROR);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            return 0;
        } finally {
            theConnectionInf.close();
        }
    }

    public List<DataSource> listUnmapNED(String keyword) {
        List<DataSource> list = new ArrayList<DataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> unmappeds = this.theHosDB.theMapNedDB.listUnmap(keyword);
            for (Object[] objects : unmappeds) {
                list.add(new DataSource(String.valueOf(objects[0]), String.valueOf(objects[1])));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public List<DataSource> listMappedNED(String keyword) {
        List<DataSource> list = new ArrayList<DataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<MapNed> mapNeds = this.theHosDB.theMapNedDB.listMapped(keyword);
            for (MapNed mapNed : mapNeds) {
                list.add(new DataSource(mapNed.getObjectId(), mapNed.item_subgroup_description));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public List<DataSource> listMapNEDContractPlans(String mapId) {
        List<DataSource> list = new ArrayList<DataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> objs = theHosDB.theMapNedDB.listMapContractPlans(mapId);
            for (Object[] objects : objs) {
                list.add(new DataSource(String.valueOf(objects[0]), String.valueOf(objects[1])));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public int saveMapNed(MapNed... mapNeds) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (MapNed mapNed : mapNeds) {
                mapNed.user_update_id = theHO.theEmployee.getObjectId();
                mapNed.update_date_time = theHO.date_time;
                mapNed.user_record_id = theHO.theEmployee.getObjectId();
                mapNed.record_date_time = theHO.date_time;
                this.theHosDB.theMapNedDB.insert(mapNed);
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public int updateMapNed(MapNed mapNed) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLookupControl.intReadDateTime();
            mapNed.user_update_id = theHO.theEmployee.getObjectId();
            mapNed.update_date_time = theHO.date_time;
            theHosDB.theMapNedDB.update(mapNed);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public int deleteMapNed(MapNed... mapNeds) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (MapNed mapNed : mapNeds) {
                this.theHosDB.theMapNedDB.delete(mapNed.getObjectId());
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public List<DataSource> listDUEItem(String keyword, String active) {
        List<DataSource> list = new ArrayList<DataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<DueType> dueTypes = this.theHosDB.theDueTypeDB.listAll(keyword, active);
            for (DueType dueType : dueTypes) {
                list.add(new DataSource(dueType.getObjectId(), dueType.due_name));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public DueType getDueTypeById(String id) {
        DueType dt = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            dt = this.theHosDB.theDueTypeDB.selectById(id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return dt;
    }

    public int saveOrUpdateDueType(DueType dueType) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLookupControl.intReadDateTime();

            dueType.user_update_id = theHO.theEmployee.getObjectId();
            dueType.update_date_time = theHO.date_time;
            if (dueType.getObjectId() == null) {
                dueType.user_record_id = theHO.theEmployee.getObjectId();
                dueType.record_date_time = theHO.date_time;
                theHosDB.theDueTypeDB.insert(dueType);
            } else {
                if (dueType.active.equals("0")) {
                    // count used
                    List<MapDue> mapDues = theHosDB.theMapDueDB.listByDueTypeId(dueType.getObjectId());
                    if (!mapDues.isEmpty()) {
                        String msg = "��Դ��û����Թ������ҹ�� �١�Ѻ���Ѻ��¡�õ�Ǩ�ѡ������ �ӹǹ %d ��¡��\n"
                                + "�ҡ�׹�ѹ���¡��ԡ ��¡�÷��Ѻ������ж١ź������ �׹�ѹ���¡��ԡ �������";
                        int ret = JOptionPane.showConfirmDialog(null,
                                String.format(msg, mapDues.size()),
                                "�׹�ѹ���¡��ԡ",
                                JOptionPane.YES_NO_OPTION);
                        // delete all mapped
                        if (ret == JOptionPane.YES_OPTION) {
                            for (MapDue mapDue : mapDues) {
                                theHosDB.theMapDueDB.delete(mapDue);
                            }
                        } else {
                            throw new Exception("Cancel by user.");
                        }
                    }
                }
                theHosDB.theDueTypeDB.updateActive(dueType);
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            if (!ex.getMessage().equals("Cancel by user.")) {
                hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public List<DataSource> listDUEDetailsByDueId(String id) {
        List<DataSource> list = new ArrayList<DataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<DueTypeDetail> dueTypes = this.theHosDB.theDueTypeDetailDB.listByDueTypeId(id);
            for (DueTypeDetail dueType : dueTypes) {
                list.add(new DataSource(dueType.getObjectId(), dueType.description));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public int addDueTypeDetail(DueTypeDetail dueTypeDetail) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLookupControl.intReadDateTime();
            dueTypeDetail.active = "1";
            dueTypeDetail.user_record_id = theHO.theEmployee.getObjectId();
            dueTypeDetail.record_date_time = theHO.date_time;
            dueTypeDetail.user_update_id = theHO.theEmployee.getObjectId();
            dueTypeDetail.update_date_time = theHO.date_time;
            theHosDB.theDueTypeDetailDB.insert(dueTypeDetail);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public int deleteDueTypeDetail(DueTypeDetail... dueTypeDetails) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (DueTypeDetail dueTypeDetail : dueTypeDetails) {
                dueTypeDetail.active = "0";
                dueTypeDetail.user_update_id = theHO.theEmployee.getObjectId();
                dueTypeDetail.update_date_time = theHO.date_time;
                this.theHosDB.theDueTypeDetailDB.updateActive(dueTypeDetail);
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public Vector listDUEItem(String keyword) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<DueType> dueTypes = this.theHosDB.theDueTypeDB.listAll(keyword, "1");
            for (DueType dueType : dueTypes) {
                vc.add(dueType);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public List<DataSource> listItemBySubgroupId(String grpId, String keyword, boolean begin) {
        keyword = Gutil.CheckReservedWords(keyword);
        List<DataSource> list = new ArrayList<DataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            StringBuilder sql = new StringBuilder();
            sql.append("select * ");
            sql.append("from b_item ");
            sql.append("where ");
            sql.append("b_item.item_active='1' ");
            if (!grpId.isEmpty()) {
                sql.append("and b_item.b_item_subgroup_id = '");
                sql.append(grpId);
                sql.append("' ");
            }
            sql.append("and (");
            sql.append("b_item.item_number ilike '");
            sql.append(begin ? "" : "%");
            sql.append(keyword);
            sql.append("%' ");
            sql.append("or b_item.item_common_name ilike '");
            sql.append(begin ? "" : "%");
            sql.append(keyword);
            sql.append("%' ");
            sql.append("or b_item.item_nick_name ilike '");
            sql.append(begin ? "" : "%");
            sql.append(keyword);
            sql.append("%' ");
            sql.append("or b_item.item_trade_name ilike '");
            sql.append(begin ? "" : "%");
            sql.append(keyword);
            sql.append("%'");
            sql.append(") order by b_item.item_common_name");
            Vector vc = theHosDB.theItemDB.eQuery(sql.toString());
            for (Object object : vc) {
                Item item = (Item) object;
                list.add(new DataSource(item.getObjectId(), item.common_name));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public MapDue getMapDueByItemId(String id) {
        MapDue mapDue = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            mapDue = this.theHosDB.theMapDueDB.selectByItemId(id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return mapDue;
    }

    public List<DataSource> listMapDUEContractPlans(String mapId) {
        List<DataSource> list = new ArrayList<DataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> objs = theHosDB.theMapDueDB.listMapContractPlans(mapId);
            for (Object[] objects : objs) {
                list.add(new DataSource(String.valueOf(objects[0]), String.valueOf(objects[1])));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public int saveOrUpdateMapDue(MapDue mapDue) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLookupControl.intReadDateTime();

            mapDue.user_update_id = theHO.theEmployee.getObjectId();
            mapDue.update_date_time = theHO.date_time;
            if (mapDue.getObjectId() == null) {
                mapDue.user_record_id = theHO.theEmployee.getObjectId();
                mapDue.record_date_time = theHO.date_time;
                theHosDB.theMapDueDB.insert(mapDue);
            } else {
                theHosDB.theMapDueDB.update(mapDue);
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public ItemSupply readItemSupplyByItemId(String itemId) {
        ItemSupply is = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            is = theHosDB.theItemSupplyDB.selectByItemId(itemId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return is;
    }

    public BItemXray readItemXrayByItemId(String itemId) {
        BItemXray is = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            is = theHosDB.theBItemXrayDB.selectByItemId(itemId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return is;
    }

    public List<DataSource> listDrugDosageForm(String keyword, String active) {
        List<DataSource> list = new ArrayList<DataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector<DrugDosageForm> vc = this.theHosDB.theDrugDosageFormDB.selectByKeyword(keyword, active);
            for (DrugDosageForm obj : vc) {
                list.add(new DataSource(obj.getObjectId(), obj.form_name));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public DrugDosageForm getDrugDosageFormById(String id) {
        DrugDosageForm dt = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            dt = this.theHosDB.theDrugDosageFormDB.selectByPK(id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return dt;
    }

    public int saveOrUpdateDrugDosageForm(DrugDosageForm setupObj) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (setupObj.getObjectId() == null) {
                DrugDosageForm dupObj = theHosDB.theDrugDosageFormDB.selectByName(setupObj.form_name);
                if (dupObj != null) {
                    this.theUS.setStatus("�������ö�ѹ�֡�� ���ͧ�ҡ�ժ��͹����������", UpdateStatus.WARNING);
                    throw new Exception("Cancel by user.");
                }
            }
            theLookupControl.intReadDateTime();
            setupObj.user_update_id = theHO.theEmployee.getObjectId();
            setupObj.update_date_time = theHO.date_time;
            if (setupObj.getObjectId() == null) {
                setupObj.user_record_id = theHO.theEmployee.getObjectId();
                setupObj.record_date_time = theHO.date_time;
                theHosDB.theDrugDosageFormDB.insert(setupObj);
            } else {
                theHosDB.theDrugDosageFormDB.update(setupObj);
            }
            theLO.vDrugDosageForm = theHosDB.theDrugDosageFormDB.selectAll();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            if (!ex.getMessage().equals("Cancel by user.")) {
                hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public void setHosControl(HosControl theHC) {
        this.hosControl = theHC;
    }

    public List<ComplexDataSource> listMapDrugUnit(String keyword, boolean onlyUnmap) {
        List<ComplexDataSource> list = new ArrayList<ComplexDataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> datas = this.theHosDB.theMapDrugUnitDB.listMapData(keyword, onlyUnmap);
            for (Object[] objects : datas) {
                list.add(new ComplexDataSource(
                        new DataSource(
                                objects[0] == null ? null : String.valueOf(objects[0]),
                                objects[3] == null ? null : String.valueOf(objects[3])),
                        new Object[]{
                            objects[1] == null ? "" : String.valueOf(objects[1]),
                            objects[2] == null ? "" : String.valueOf(objects[2])}));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public List<DataSource> listDrugUnitStd(String keyword) {
        List<DataSource> list = new ArrayList<DataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<RpDrugUnit> list1 = this.theHosDB.theRpDrugUnitDB.list(keyword);
            for (RpDrugUnit objects : list1) {
                list.add(new DataSource(objects.getObjectId(), objects.description));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public int deleteMapDrugUnit(String... ids) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (String id : ids) {
                if (id != null) {
                    this.theHosDB.theMapDrugUnitDB.delete(id);
                }
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public int saveMapDrugUnit(MapDrugUnit... maps) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (MapDrugUnit map : maps) {
                if (map.getObjectId() == null) {
                    this.theHosDB.theMapDrugUnitDB.insert(map);
                } else {
                    this.theHosDB.theMapDrugUnitDB.update(map);
                }
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public int saveOrUpdateSpecimen(Specimen specimen) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Specimen selectByCode = theHosDB.theSpecimenDB.selectByCode(specimen.code);
            if (selectByCode != null && (specimen.getObjectId() == null ? selectByCode.getObjectId() != null : !specimen.getObjectId().equals(selectByCode.getObjectId()))) {
                this.theUS.setStatus("�������ö�ѹ�֡���ʫ���� �ô��Ǩ�ͺ ��д��Թ��������ա����", UpdateStatus.WARNING);
                throw new Exception("cancel");
            }
            theLookupControl.intReadDateTime();
            specimen.user_modify = theHO.theEmployee.getObjectId();
            specimen.modify_datetime = theHO.date_time;
            if (specimen.getObjectId() == null) {
                specimen.user_record = theHO.theEmployee.getObjectId();
                specimen.record_datetime = theHO.date_time;
                theHosDB.theSpecimenDB.insert(specimen);
            } else {
                theHosDB.theSpecimenDB.update(specimen);
            }
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        } catch (Exception ex) {
            if (!ex.getMessage().equals("cancel")) {
                hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public int deleteSpecimen(Specimen specimen) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector vc = theHosDB.theItemDB.selectBySpecimenId(specimen.getObjectId());
            if (vc.isEmpty()) {
                theHosDB.theSpecimenDB.delete(specimen);
            } else {
                theLookupControl.intReadDateTime();
                specimen.user_modify = theHO.theEmployee.getObjectId();
                specimen.modify_datetime = theHO.date_time;
                theHosDB.theSpecimenDB.inactive(specimen);
            }
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public Vector listSpecimen(String keyword, String active) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Specimen> specimens = this.theHosDB.theSpecimenDB.listByKeyword(keyword, active);
            for (Specimen specimen : specimens) {
                vc.add(specimen);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public int saveOrUpdateLabOrderCause(LabOrderCause labOrderCause) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            LabOrderCause selectByCode = theHosDB.theLabOrderCauseDB.selectByCode(labOrderCause.code);
            if (selectByCode != null && (labOrderCause.getObjectId() == null ? selectByCode.getObjectId() != null : !labOrderCause.getObjectId().equals(selectByCode.getObjectId()))) {
                this.theUS.setStatus("�������ö�ѹ�֡���ʫ���� �ô��Ǩ�ͺ ��д��Թ��������ա����", UpdateStatus.WARNING);
                throw new Exception("cancel");
            }
            theLookupControl.intReadDateTime();
            labOrderCause.user_update_id = theHO.theEmployee.getObjectId();
            if (labOrderCause.getObjectId() == null) {
                labOrderCause.user_record_id = theHO.theEmployee.getObjectId();
                theHosDB.theLabOrderCauseDB.insert(labOrderCause);
            } else {
                theHosDB.theLabOrderCauseDB.update(labOrderCause);
            }
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        } catch (Exception ex) {
            if (!ex.getMessage().equals("cancel")) {
                hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public int deleteLabOrderCause(LabOrderCause labOrderCause) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            int count = theHosDB.theOrderItemDB.countByLabOrderCauseId(labOrderCause.getObjectId());
            if (count == 0) {
                theHosDB.theLabOrderCauseDB.delete(labOrderCause);
            } else {
                labOrderCause.user_update_id = theHO.theEmployee.getObjectId();
                theHosDB.theLabOrderCauseDB.inactive(labOrderCause);
            }
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public Vector listLabOrderCause(String keyword, String active) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<LabOrderCause> list = this.theHosDB.theLabOrderCauseDB.listByKeyword(keyword, active);
            for (LabOrderCause obj : list) {
                vc.add(obj);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public LabResultItemGender getLabResultItemGenderByLabResultItemId(String id) {
        LabResultItemGender itemGender = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            itemGender = this.theHosDB.theLabResultItemGenderDB.selectByBItemLabResultId(id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return itemGender;
    }

    public List<LabResultItemAge> listLabResultItemAgeByLabResultItemId(String id) {
        List<LabResultItemAge> itemAges = new ArrayList<LabResultItemAge>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            itemAges.addAll(this.theHosDB.theLabResultItemAgeDB.selectByBItemLabResultId(id));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return itemAges;
    }

    public List<LabResultItemText> listLabResultItemTextByLabResultItemId(String id) {
        List<LabResultItemText> itemTexts = new ArrayList<LabResultItemText>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            itemTexts.addAll(this.theHosDB.theLabResultItemTextDB.selectByBItemLabResultId(id));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return itemTexts;
    }

    public List<DataSource> listItemMapDoctorFee(String keyword) {
        List<DataSource> list = new ArrayList<DataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> listByKeyword = this.theHosDB.theItemMapDoctorFeeDB.listByKeyword(keyword);
            for (Object[] objects : listByKeyword) {
                list.add(new DataSource(String.valueOf(objects[0]),
                        String.valueOf(objects[1] + " - " + String.valueOf(objects[2]))));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public List<DataSource> listItemNotMapDoctorFee(String keyword) {
        List<DataSource> list = new ArrayList<DataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> listByKeyword = this.theHosDB.theItemMapDoctorFeeDB.listNotMapByKeyword(keyword);
            for (Object[] objects : listByKeyword) {
                list.add(new DataSource(String.valueOf(objects[0]),
                        String.valueOf(objects[1] + " - " + String.valueOf(objects[2]))));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public int saveItemMapDoctorFee(ItemMapDoctorFee... imdfs) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (ItemMapDoctorFee imdf : imdfs) {
                this.theHosDB.theItemMapDoctorFeeDB.insert(imdf);
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public int deleteItemMapDoctorFee(ItemMapDoctorFee... imdfs) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (ItemMapDoctorFee imdf : imdfs) {
                this.theHosDB.theItemMapDoctorFeeDB.delete(imdf);
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public int saveOrUpdateBankInfo(BankInfo bankInfo) {
        if (bankInfo.code.isEmpty()) {
            this.theUS.setStatus("��س��к����ʡ�͹�ѹ�֡", UpdateStatus.WARNING);
            return 0;
        }
        if (bankInfo.description.isEmpty()) {
            this.theUS.setStatus("��س��кت��͡�͹�ѹ�֡", UpdateStatus.WARNING);
            return 0;
        }
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            BankInfo selectByCode = theHosDB.theBankInfoDB.selectByCode(bankInfo.code);
            if (selectByCode != null && (bankInfo.getObjectId() == null ? selectByCode.getObjectId() != null : !bankInfo.getObjectId().equals(selectByCode.getObjectId()))) {
                this.theUS.setStatus("�������ö�ѹ�֡���ʫ���� �ô��Ǩ�ͺ ��д��Թ��������ա����", UpdateStatus.WARNING);
                throw new Exception("cancel");
            }
            theLookupControl.intReadDateTime();
            bankInfo.user_modify = theHO.theEmployee.getObjectId();
            bankInfo.modify_datetime = theHO.date_time;
            if (bankInfo.getObjectId() == null) {
                bankInfo.user_record = theHO.theEmployee.getObjectId();
                bankInfo.record_datetime = theHO.date_time;
                theHosDB.theBankInfoDB.insert(bankInfo);
            } else {
                theHosDB.theBankInfoDB.update(bankInfo);
            }
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        } catch (Exception ex) {
            if (!ex.getMessage().equals("cancel")) {
                hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public int deleteBankInfo(BankInfo bankInfo) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLookupControl.intReadDateTime();
            bankInfo.user_modify = theHO.theEmployee.getObjectId();
            bankInfo.modify_datetime = theHO.date_time;
            theHosDB.theBankInfoDB.inactive(bankInfo);
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public Vector listBankInfo(String keyword, String active) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<BankInfo> bankInfos = this.theHosDB.theBankInfoDB.listByKeyword(keyword, active);
            for (BankInfo bankInfo : bankInfos) {
                vc.add(bankInfo);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public int saveOrUpdateFinanceInsuranceCompany(FinanceInsuranceCompany financeInsuranceCompany) {
        if (financeInsuranceCompany.code.isEmpty()) {
            this.theUS.setStatus("��س��к����ʡ�͹�ѹ�֡", UpdateStatus.WARNING);
            return 0;
        }
        if (financeInsuranceCompany.company_name.isEmpty()) {
            this.theUS.setStatus("��س��кت��ͺ���ѷ��͹�ѹ�֡", UpdateStatus.WARNING);
            return 0;
        }
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            FinanceInsuranceCompany selectByCode = theHosDB.theFinanceInsuranceCompanyDB.selectByCode(financeInsuranceCompany.code);
            if (selectByCode != null && (financeInsuranceCompany.getObjectId() == null ? selectByCode.getObjectId() != null : !financeInsuranceCompany.getObjectId().equals(selectByCode.getObjectId()))) {
                this.theUS.setStatus("�������ö�ѹ�֡���ʫ���� �ô��Ǩ�ͺ ��д��Թ��������ա����", UpdateStatus.WARNING);
                throw new Exception("cancel");
            }
            theLookupControl.intReadDateTime();
            financeInsuranceCompany.user_update_id = theHO.theEmployee.getObjectId();
            financeInsuranceCompany.update_datetime = theHO.date_time;
            if (financeInsuranceCompany.getObjectId() == null) {
                financeInsuranceCompany.user_record_id = theHO.theEmployee.getObjectId();
                financeInsuranceCompany.record_datetime = theHO.date_time;
                theHosDB.theFinanceInsuranceCompanyDB.insert(financeInsuranceCompany);
            } else {
                theHosDB.theFinanceInsuranceCompanyDB.update(financeInsuranceCompany);
            }
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        } catch (Exception ex) {
            if (!ex.getMessage().equals("cancel")) {
                hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public Vector listFinanceInsuranceCompany(String keyword, String active) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<FinanceInsuranceCompany> list = this.theHosDB.theFinanceInsuranceCompanyDB.listByKeyword(keyword, active);
            for (FinanceInsuranceCompany obj : list) {
                vc.add(obj);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public int saveOrUpdateFinanceInsuranceDiscounts(FinanceInsurancePlan financeInsuranceDiscounts) {
        if (financeInsuranceDiscounts.code.isEmpty()) {
            this.theUS.setStatus("��س��к����ʡ�͹�ѹ�֡", UpdateStatus.WARNING);
            return 0;
        }
        if (financeInsuranceDiscounts.description.isEmpty()) {
            this.theUS.setStatus("��س��кت���Ἱ��͹�ѹ�֡", UpdateStatus.WARNING);
            return 0;
        }
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            FinanceInsurancePlan selectByCode = theHosDB.theFinanceInsurancePlanDB.selectByCode(financeInsuranceDiscounts.code);
            if (selectByCode != null && (financeInsuranceDiscounts.getObjectId() == null ? selectByCode.getObjectId() != null : !financeInsuranceDiscounts.getObjectId().equals(selectByCode.getObjectId()))) {
                this.theUS.setStatus("�������ö�ѹ�֡���ʫ���� �ô��Ǩ�ͺ ��д��Թ��������ա����", UpdateStatus.WARNING);
                throw new Exception("cancel");
            }
            theLookupControl.intReadDateTime();
            financeInsuranceDiscounts.user_update_id = theHO.theEmployee.getObjectId();
            financeInsuranceDiscounts.update_datetime = theHO.date_time;
            if (financeInsuranceDiscounts.getObjectId() == null) {
                financeInsuranceDiscounts.user_record_id = theHO.theEmployee.getObjectId();
                financeInsuranceDiscounts.record_datetime = theHO.date_time;
                theHosDB.theFinanceInsurancePlanDB.insert(financeInsuranceDiscounts);
            } else {
                theHosDB.theFinanceInsurancePlanDB.update(financeInsuranceDiscounts);
            }
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        } catch (Exception ex) {
            if (!ex.getMessage().equals("cancel")) {
                hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public Vector listFinanceInsuranceDiscounts(String keyword, String active) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<FinanceInsurancePlan> list = this.theHosDB.theFinanceInsurancePlanDB.listByKeyword(keyword, active);
            for (FinanceInsurancePlan obj : list) {
                vc.add(obj);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public List<ComplexDataSource> listMapInsuranceDiscountCondition(String insuranceDiscountId) {
        List<ComplexDataSource> list = new ArrayList<ComplexDataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> datas = this.theHosDB.theFinanceInsuranceDiscountsConditionDB.listMap(insuranceDiscountId);
            for (Object[] objects : datas) {
                list.add(new ComplexDataSource(
                        //                        new DataSource(
                        objects[0] == null ? null : String.valueOf(objects[0]),
                        //                        objects[3] == null ? null : String.valueOf(objects[3])),
                        new Object[]{
                            objects[1] == null ? "" : String.valueOf(objects[1]),
                            objects[2] == null ? "" : String.valueOf(objects[2])}));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public int saveOrUpdateInsuranceDiscountCondition(FinanceInsuranceDiscountsCondition setupObj) {
        if (setupObj == null) {
            return 0;
        }
        if (setupObj.condition_adjustment.isEmpty()) {
            theUS.setStatus("��س��к���ǹŴ㹪�ǧ 0-100 % ��ҹ��", UpdateStatus.WARNING);
            return 0;
        }
        double a = Double.parseDouble(setupObj.condition_adjustment);
        if (a < 0 || a > 100) {
            theUS.setStatus("��ǹŴ��ͧ����㹪�ǧ 0-100 % ��ҹ��", UpdateStatus.WARNING);
            return 0;
        }
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            FinanceInsuranceDiscountsCondition find
                    = theHosDB.theFinanceInsuranceDiscountsConditionDB.selectByDiscountIdAndItemBillingSubgroupId(
                            setupObj.b_finance_insurance_plan_id,
                            setupObj.b_item_billing_subgroup_id);
            if (find != null) {
                find.condition_adjustment = setupObj.condition_adjustment;
                theHosDB.theFinanceInsuranceDiscountsConditionDB.update(find);
            } else {
                theHosDB.theFinanceInsuranceDiscountsConditionDB.insert(setupObj);
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            if (!ex.getMessage().equals("Cancel by user.")) {
                hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public int cancelInsuranceDiscountCondition(FinanceInsuranceDiscountsCondition setupObj) {
        if (setupObj == null) {
            return 0;
        }
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);

            theHosDB.theFinanceInsuranceDiscountsConditionDB.deleteByDiscountIdAndItemBillingSubgroupId(
                    setupObj.b_finance_insurance_plan_id,
                    setupObj.b_item_billing_subgroup_id);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_DELETE);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public MapFinanceInsurance getMapFinanceInsurance() {
        MapFinanceInsurance obj = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            obj = this.theHosDB.theMapFinanceInsuranceDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return obj;
    }

    public int saveMapFinanceInsurance(MapFinanceInsurance... isccs) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (MapFinanceInsurance iscc : isccs) {
                this.theHosDB.theMapFinanceInsuranceDB.insert(iscc);
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public int deleteMapFinanceInsurance(MapFinanceInsurance... isccs) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (MapFinanceInsurance iscc : isccs) {
                this.theHosDB.theMapFinanceInsuranceDB.delete(iscc);
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_DELETE);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public List<DataSource> listMapFinanceInsurance(String keyword) {
        List<DataSource> list = new ArrayList<>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> listByKeyword = this.theHosDB.theMapFinanceInsuranceDB.listMapByKeyword(keyword);
            for (Object[] objects : listByKeyword) {
                list.add(new DataSource(String.valueOf(objects[0]), String.valueOf(objects[1])));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public List<DataSource> listNotMapFinanceInsurance(String keyword) {
        List<DataSource> list = new ArrayList<>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> listByKeyword = this.theHosDB.theMapFinanceInsuranceDB.listNotMapByKeyword(keyword);
            for (Object[] objects : listByKeyword) {
                list.add(new DataSource(String.valueOf(objects[0]), String.valueOf(objects[1])));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public List<MapFinanceInsurance> listMapFinanceInsurance() {
        List<MapFinanceInsurance> list = new ArrayList<>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            list = this.theHosDB.theMapFinanceInsuranceDB.listAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public int saveOrUpdateMapFinanceInsurance(MapFinanceInsurance mapFinanceInsurance) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);

            if (mapFinanceInsurance.getObjectId() == null) {
                theHosDB.theMapFinanceInsuranceDB.insert(mapFinanceInsurance);
            } else {
                theHosDB.theMapFinanceInsuranceDB.update(mapFinanceInsurance);
            }
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        } catch (Exception ex) {
            if (!ex.getMessage().equals("cancel")) {
                hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public int deleteMapFinanceInsurance(MapFinanceInsurance mapFinanceInsurance) {
        if (mapFinanceInsurance == null || mapFinanceInsurance.getObjectId() == null) {
            return 0;
        }
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theMapFinanceInsuranceDB.delete(mapFinanceInsurance);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_DELETE);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public List<DataSource> listNotMapItemSubgroupCheckCalculate(String keyword) {
        List<DataSource> list = new ArrayList<DataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> listByKeyword = this.theHosDB.theItemSubgroupCheckCalculateDB.listNotMapByKeyword(keyword);
            for (Object[] objects : listByKeyword) {
                list.add(new DataSource(String.valueOf(objects[0]), String.valueOf(objects[1])));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public List<DataSource> listMapItemSubgroupCheckCalculate(String keyword) {
        List<DataSource> list = new ArrayList<DataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> listByKeyword = this.theHosDB.theItemSubgroupCheckCalculateDB.listByKeyword(keyword);
            for (Object[] objects : listByKeyword) {
                list.add(new DataSource(String.valueOf(objects[0]), String.valueOf(objects[1])));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public int saveItemSubgroupCheckCalculate(ItemSubgroupCheckCalculate... isccs) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (ItemSubgroupCheckCalculate iscc : isccs) {
                this.theHosDB.theItemSubgroupCheckCalculateDB.insert(iscc);
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public int deleteItemSubgroupCheckCalculate(ItemSubgroupCheckCalculate... isccs) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (ItemSubgroupCheckCalculate iscc : isccs) {
                this.theHosDB.theItemSubgroupCheckCalculateDB.delete(iscc);
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public List<DataSource> listUserMapDeleteNotify(String keyword) {
        List<DataSource> list = new ArrayList<DataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> listByKeyword = this.theHosDB.theMapUserDeleteNotifyDB.listByKeyword(keyword);
            for (Object[] objects : listByKeyword) {
                list.add(new DataSource(String.valueOf(objects[0]), String.valueOf(objects[1])));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public List<DataSource> listUserNotMapDeleteNotify(String keyword) {
        List<DataSource> list = new ArrayList<DataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> listByKeyword = this.theHosDB.theMapUserDeleteNotifyDB.listNotMapByKeyword(keyword);
            for (Object[] objects : listByKeyword) {
                list.add(new DataSource(String.valueOf(objects[0]), String.valueOf(objects[1])));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public int saveUserMapDeleteNotify(MapUserDeleteNotify... mudns) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (MapUserDeleteNotify mudn : mudns) {
                this.theHosDB.theMapUserDeleteNotifyDB.insert(mudn);
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public int deleteUserMapDeleteNotify(MapUserDeleteNotify... mudns) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (MapUserDeleteNotify mudn : mudns) {
                this.theHosDB.theMapUserDeleteNotifyDB.delete(mudn);
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public List<Printing> listPrinting() {
        List<Printing> list = new ArrayList<Printing>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            list.addAll(this.theHosDB.thePrintingDB.list());
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public int updatePrinting(Printing printing) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            this.theHosDB.thePrintingDB.update(printing);
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public Vector listLanguage(String keyword, String active) {
        Vector<Language> list = new Vector<Language>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            list.addAll(this.theHosDB.theLanguageDB.listByKeyword(keyword, active));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public List<DataSource> listLanguageMappingNation(String languageId) {
        List<DataSource> list = new ArrayList<DataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> listByKeyword = this.theHosDB.theLanguageMappingDB.listMap(languageId);
            for (Object[] objects : listByKeyword) {
                list.add(new DataSource(String.valueOf(objects[0]), String.valueOf(objects[1])));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public int deleteLanguageMapping(LanguageMapping... lms) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (LanguageMapping lm : lms) {
                this.theHosDB.theLanguageMappingDB.delete(lm);
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public int addLanguageMapping(List<LanguageMapping> lms) {
        int ret = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (LanguageMapping lm : lms) {
                LanguageMapping obj = theHosDB.theLanguageMappingDB.getByLanguageIdAndNationId(lm.b_language_id, lm.f_patient_nation_id);
                if (obj != null) {
                    continue;
                }
                theHosDB.theLanguageMappingDB.insert(lm);
            }
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            ret = 1;
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            Logger.getLogger(VisitControl.class.getName()).log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(VisitControl.class.getName()).log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ret;
    }

    public int saveOrUpdateLanguage(Language language) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (language.getObjectId() == null) {
                theHosDB.theLanguageDB.insert(language);
            } else {
                theHosDB.theLanguageDB.update(language);
            }
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public List<DataSource> listMappedPrintingOtherLanguage(String praintigId) {
        List<DataSource> list = new ArrayList<DataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> listByKeyword = this.theHosDB.thePrintingOtherLanguageDB.listMap(praintigId);
            for (Object[] objects : listByKeyword) {
                list.add(new DataSource(String.valueOf(objects[0]), String.valueOf(objects[1])));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public PrintingOtherLanguage getPrintingOtherLanguageById(String id) {
        PrintingOtherLanguage pol = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            pol = this.theHosDB.thePrintingOtherLanguageDB.select(id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return pol;
    }

    public int deletePrintingOtherLanguage(PrintingOtherLanguage pol) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            this.theHosDB.thePrintingOtherLanguageDB.delete(pol);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public KeyValue[] getUnmapPrintingOtherLanguage(String printingId) {
        KeyValue[] keyvalues = new KeyValue[]{};
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> listNotifyType = theHosDB.thePrintingOtherLanguageDB.listUnmap(printingId);
            keyvalues = new KeyValue[listNotifyType.size()];
            for (int i = 0; i < keyvalues.length; i++) {
                Object[] get = listNotifyType.get(i);
                keyvalues[i] = new KeyValue((String) get[0], (String) get[1]);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return keyvalues;
    }

    public int saveOrUpdatePrintingOtherLanguage(PrintingOtherLanguage pol) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (pol.getObjectId() == null) {
                theHosDB.thePrintingOtherLanguageDB.insert(pol);
            } else {
                theHosDB.thePrintingOtherLanguageDB.update(pol);
            }
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    //
    public List<DataSource> listMappedItemDrugFrequencyOtherLanguage(String freqId) {
        List<DataSource> list = new ArrayList<DataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> listByKeyword = this.theHosDB.theItemDrugFrequencyOtherLanguageDB.listMap(freqId);
            for (Object[] objects : listByKeyword) {
                list.add(new DataSource(String.valueOf(objects[0]), String.valueOf(objects[1])));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public ItemDrugFrequencyOtherLanguage getItemDrugFrequencyOtherLanguageById(String id) {
        ItemDrugFrequencyOtherLanguage pol = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            pol = this.theHosDB.theItemDrugFrequencyOtherLanguageDB.select(id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return pol;
    }

    public int deleteItemDrugFrequencyOtherLanguage(ItemDrugFrequencyOtherLanguage pol) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            this.theHosDB.theItemDrugFrequencyOtherLanguageDB.delete(pol);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public KeyValue[] getUnmapItemDrugFrequencyOtherLanguage(String printingId) {
        KeyValue[] keyvalues = new KeyValue[]{};
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> listNotifyType = theHosDB.theItemDrugFrequencyOtherLanguageDB.listUnmap(printingId);
            keyvalues = new KeyValue[listNotifyType.size()];
            for (int i = 0; i < keyvalues.length; i++) {
                Object[] get = listNotifyType.get(i);
                keyvalues[i] = new KeyValue((String) get[0], (String) get[1]);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return keyvalues;
    }

    public int saveOrUpdateItemDrugFrequencyOtherLanguage(ItemDrugFrequencyOtherLanguage pol) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (pol.getObjectId() == null) {
                theHosDB.theItemDrugFrequencyOtherLanguageDB.insert(pol);
            } else {
                theHosDB.theItemDrugFrequencyOtherLanguageDB.update(pol);
            }
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    // instruction
    public List<DataSource> listMappedItemDrugInstructionOtherLanguage(String instructionId) {
        List<DataSource> list = new ArrayList<DataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> listByKeyword = this.theHosDB.theItemDrugInstructionOtherLanguageDB.listMap(instructionId);
            for (Object[] objects : listByKeyword) {
                list.add(new DataSource(String.valueOf(objects[0]), String.valueOf(objects[1])));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public ItemDrugInstructionOtherLanguage getItemDrugInstructionOtherLanguageById(String id) {
        ItemDrugInstructionOtherLanguage pol = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            pol = this.theHosDB.theItemDrugInstructionOtherLanguageDB.select(id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return pol;
    }

    public int deleteItemDrugInstructionOtherLanguage(ItemDrugInstructionOtherLanguage idiol) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            this.theHosDB.theItemDrugInstructionOtherLanguageDB.delete(idiol);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public KeyValue[] getUnmapItemDrugInstructionOtherLanguage(String instructionId) {
        KeyValue[] keyvalues = new KeyValue[]{};
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> listNotifyType = theHosDB.theItemDrugInstructionOtherLanguageDB.listUnmap(instructionId);
            keyvalues = new KeyValue[listNotifyType.size()];
            for (int i = 0; i < keyvalues.length; i++) {
                Object[] get = listNotifyType.get(i);
                keyvalues[i] = new KeyValue((String) get[0], (String) get[1]);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return keyvalues;
    }

    public int saveOrUpdateItemDrugInstructionOtherLanguage(ItemDrugInstructionOtherLanguage idiol) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (idiol.getObjectId() == null) {
                theHosDB.theItemDrugInstructionOtherLanguageDB.insert(idiol);
            } else {
                theHosDB.theItemDrugInstructionOtherLanguageDB.update(idiol);
            }
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    // uom
    public List<DataSource> listMappedItemDrugUOMOtherLanguage(String instructionId) {
        List<DataSource> list = new ArrayList<DataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> listByKeyword = this.theHosDB.theItemDrugUOMOtherLanguageDB.listMap(instructionId);
            for (Object[] objects : listByKeyword) {
                list.add(new DataSource(String.valueOf(objects[0]), String.valueOf(objects[1])));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public ItemDrugUOMOtherLanguage getItemDrugUOMOtherLanguageById(String id) {
        ItemDrugUOMOtherLanguage pol = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            pol = this.theHosDB.theItemDrugUOMOtherLanguageDB.select(id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return pol;
    }

    public int deleteItemDrugUOMOtherLanguage(ItemDrugUOMOtherLanguage idiol) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            this.theHosDB.theItemDrugUOMOtherLanguageDB.delete(idiol);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public KeyValue[] getUnmapItemDrugUOMOtherLanguage(String instructionId) {
        KeyValue[] keyvalues = new KeyValue[]{};
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> listNotifyType = theHosDB.theItemDrugUOMOtherLanguageDB.listUnmap(instructionId);
            keyvalues = new KeyValue[listNotifyType.size()];
            for (int i = 0; i < keyvalues.length; i++) {
                Object[] get = listNotifyType.get(i);
                keyvalues[i] = new KeyValue((String) get[0], (String) get[1]);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return keyvalues;
    }

    public int saveOrUpdateItemDrugUOMOtherLanguage(ItemDrugUOMOtherLanguage idiol) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (idiol.getObjectId() == null) {
                theHosDB.theItemDrugUOMOtherLanguageDB.insert(idiol);
            } else {
                theHosDB.theItemDrugUOMOtherLanguageDB.update(idiol);
            }
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    // item drug
    public List<DataSource> listMappedItemDrugOtherLanguage(String itemDrugId) {
        List<DataSource> list = new ArrayList<DataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> listByKeyword = this.theHosDB.theItemDrugOtherLanguageDB.listMap(itemDrugId);
            for (Object[] objects : listByKeyword) {
                list.add(new DataSource(String.valueOf(objects[0]), String.valueOf(objects[1])));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public ItemDrugOtherLanguage getItemDrugOtherLanguageById(String id) {
        ItemDrugOtherLanguage pol = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            pol = this.theHosDB.theItemDrugOtherLanguageDB.select(id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return pol;
    }

    public int deleteItemDrugOtherLanguage(ItemDrugOtherLanguage idiol) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            this.theHosDB.theItemDrugOtherLanguageDB.delete(idiol);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public KeyValue[] getUnmapItemDrugOtherLanguage(String instructionId) {
        KeyValue[] keyvalues = new KeyValue[]{};
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> listNotifyType = theHosDB.theItemDrugOtherLanguageDB.listUnmap(instructionId);
            keyvalues = new KeyValue[listNotifyType.size()];
            for (int i = 0; i < keyvalues.length; i++) {
                Object[] get = listNotifyType.get(i);
                keyvalues[i] = new KeyValue((String) get[0], (String) get[1]);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return keyvalues;
    }

    public int saveOrUpdateItemDrugOtherLanguage(ItemDrugOtherLanguage idiol) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (idiol.getObjectId() == null) {
                theHosDB.theItemDrugOtherLanguageDB.insert(idiol);
            } else {
                theHosDB.theItemDrugOtherLanguageDB.update(idiol);
            }
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public List<DataSource> listDrugFavorite(String doctorId, String clinicId) {
        List<DataSource> ret = new ArrayList<DataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<DrugFavorite> list = this.theHosDB.theItemFavoriteDB.selectByDoctorAndClinic(doctorId, clinicId);
            for (DrugFavorite itemFavorite : list) {
                ret.add(new DataSource(itemFavorite, itemFavorite.itemName));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ret;
    }

    public boolean addNewItemFavorite(List<DrugFavorite> favorites) {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (DrugFavorite itemFavorite : favorites) {
                DrugFavorite selectByDoctorAndClinicAndItem = theHosDB.theItemFavoriteDB.selectByDoctorAndClinicAndItem(itemFavorite.doctor_id,
                        itemFavorite.b_visit_clinic_id, itemFavorite.b_item_id);
                if (selectByDoctorAndClinicAndItem != null) {
                    continue;
                }
                Drug drug = theHosDB.theDrugDB.selectByItem(itemFavorite.b_item_id);
                if (drug != null) {
                    itemFavorite.usage_special = drug.usage_special;
                    itemFavorite.usage_text = drug.usage_text;
                    itemFavorite.caution = drug.caution;
                    itemFavorite.description = drug.description;
                    itemFavorite.instruction_id = drug.instruction;
                    itemFavorite.dose = drug.dose;
                    itemFavorite.use_uom_id = drug.use_uom;
                    itemFavorite.frequency_id = drug.frequency;
                    itemFavorite.qty = drug.qty;
                    itemFavorite.purch_uom_id = drug.purch_uom;
                }
                theHosDB.theItemFavoriteDB.insert(itemFavorite);
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            theUS.setStatus(("�ѹ�֡�����żԴ��Ҵ"), UpdateStatus.ERROR);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theUS.setStatus(("�ѹ�֡�����������"), UpdateStatus.COMPLETE);
        }
        return isComplete;
    }

    public int updateDrugFavorite(DrugFavorite itemFavorite) {
        int ans = 0;
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theItemFavoriteDB.update(itemFavorite);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            theUS.setStatus(("�ѹ�֡�����żԴ��Ҵ"), UpdateStatus.ERROR);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theUS.setStatus(("�ѹ�֡�����������"), UpdateStatus.COMPLETE);
        }
        return ans;
    }

    public int deleteDrugFavorite(DrugFavorite... itemFavorites) {
        int ans = 0;
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (DrugFavorite itemFavorite : itemFavorites) {
                ans += theHosDB.theItemFavoriteDB.delete(itemFavorite);
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            theUS.setStatus(("ź�����żԴ��Ҵ"), UpdateStatus.ERROR);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            ans = 0;
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theUS.setStatus(("ź�����������"), UpdateStatus.COMPLETE);
        }
        return ans;
    }

    public Vector listCreditCardType(String keyword, String active) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<CreditCardType> ccts = this.theHosDB.theCreditCardTypeDB.listByKeyword(keyword, active);
            for (CreditCardType cct : ccts) {
                vc.add(cct);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public int saveOrUpdateCreditCardType(CreditCardType creditCardType) {
        if (creditCardType.code.isEmpty()) {
            this.theUS.setStatus("��س��к����ʡ�͹�ѹ�֡", UpdateStatus.WARNING);
            return 0;
        }
        if (creditCardType.description.isEmpty()) {
            this.theUS.setStatus("��س��кت��͡�͹�ѹ�֡", UpdateStatus.WARNING);
            return 0;
        }
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            CreditCardType selectByCode = theHosDB.theCreditCardTypeDB.selectByCode(creditCardType.code);
            if (selectByCode != null && (creditCardType.getObjectId() == null ? selectByCode.getObjectId() != null : !creditCardType.getObjectId().equals(selectByCode.getObjectId()))) {
                this.theUS.setStatus("�������ö�ѹ�֡���ʫ���� �ô��Ǩ�ͺ ��д��Թ��������ա����", UpdateStatus.WARNING);
                throw new Exception("cancel");
            }
            theLookupControl.intReadDateTime();
            creditCardType.user_modify = theHO.theEmployee.getObjectId();
            creditCardType.modify_datetime = theHO.date_time;
            if (creditCardType.getObjectId() == null) {
                creditCardType.user_record = theHO.theEmployee.getObjectId();
                creditCardType.record_datetime = theHO.date_time;
                theHosDB.theCreditCardTypeDB.insert(creditCardType);
            } else {
                theHosDB.theCreditCardTypeDB.update(creditCardType);
            }
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        } catch (Exception ex) {
            if (!ex.getMessage().equals("cancel")) {
                hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public int deleteCreditCardType(CreditCardType creditCardType) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLookupControl.intReadDateTime();
            creditCardType.user_modify = theHO.theEmployee.getObjectId();
            creditCardType.modify_datetime = theHO.date_time;
            theHosDB.theCreditCardTypeDB.inactive(creditCardType);
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INACTIVE);
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INACTIVE);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public List<ComplexDataSource> listCreditCardInfoByBankId(String bankInfoId) {
        List<ComplexDataSource> list = new ArrayList<ComplexDataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<CreditCardInfo> ccis = this.theHosDB.theCreditCardInfoDB.listByBankInfoId(bankInfoId);
            for (CreditCardInfo cci : ccis) {
                list.add(new ComplexDataSource(cci, new String[]{cci.creditCardType, cci.pattern_number}));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public int deleteCreditCardInfo(CreditCardInfo... creditCardInfos) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (CreditCardInfo creditCardInfo : creditCardInfos) {
                theHosDB.theCreditCardInfoDB.delete(creditCardInfo);
            }
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE);
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_DELETE);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public int saveCreditCardInfo(CreditCardInfo cardInfo) {
        if (cardInfo.pattern_number == null || cardInfo.pattern_number.length() != 6) {
            this.theUS.setStatus("��س��к������Ţ�ѵ� 6 ����á", UpdateStatus.WARNING);
            return 0;
        }
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            CreditCardInfo cardInfo1 = theHosDB.theCreditCardInfoDB.selectByPatternNumber(cardInfo.pattern_number);
            if (cardInfo1 != null && (cardInfo.getObjectId() == null ? cardInfo1.getObjectId() != null : !cardInfo.getObjectId().equals(cardInfo1.getObjectId()))) {
                this.theUS.setStatus("�������ö�ѹ�֡�����Ţ�ѵ� 6 ����á����� (��ӡѺ�ѵ� " + cardInfo1.creditCardType + " ��Ҥ�� " + cardInfo1.bankName + ")", UpdateStatus.WARNING);
                throw new Exception("cancel");
            }
            theLookupControl.intReadDateTime();
            if (cardInfo.getObjectId() == null) {
                theHosDB.theCreditCardInfoDB.insert(cardInfo);
            } else {
                theHosDB.theCreditCardInfoDB.update(cardInfo);
            }
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        } catch (Exception ex) {
            if (!ex.getMessage().equals("cancel")) {
                hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public boolean IsNewRecceipt(String month, String day) {
        Calendar cal = Calendar.getInstance();
        int CalDay = cal.get(Calendar.DAY_OF_MONTH);
        int CalMonth = cal.get(Calendar.MONTH) + 1;
        int initday = Integer.valueOf(day);
        int initMonth = Integer.valueOf(month);
        if (CalDay == initday && CalMonth == initMonth) {
            return true;
        } else {
            return false;
        }
    }

    public List<ComplexDataSource> listServiceLimit() {
        List<ComplexDataSource> ret = new ArrayList<ComplexDataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<ServiceLimit> list = this.theHosDB.theServiceLimitDB.list();
            for (ServiceLimit serviceLimit : list) {
                ret.add(new ComplexDataSource(serviceLimit, new Object[]{
                    serviceLimit.time_start,
                    serviceLimit.time_end,
                    serviceLimit.limit_appointment,
                    serviceLimit.limit_walkin
                }));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ret;
    }

    public boolean addNewServiceLimit(ServiceLimit serviceLimit) {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<ServiceLimit> listByIntervalTime = theHosDB.theServiceLimitDB.listByIntervalTime(serviceLimit.time_start, serviceLimit.time_end);
            // new
            if (serviceLimit.getObjectId() == null && listByIntervalTime.isEmpty()) {
                theHosDB.theServiceLimitDB.insert(serviceLimit);
            } else {
                boolean isUpdated = false;
                for (ServiceLimit serviceLimit1 : listByIntervalTime) {
                    // update
                    if (serviceLimit1.getObjectId().equals(serviceLimit.getObjectId())) {
                        theHosDB.theServiceLimitDB.update(serviceLimit);
                        isUpdated = true;
                        break;
                    }
                }
                if (!isUpdated) {
                    JOptionPane.showMessageDialog(null, "�������ö�ѹ�֡��ǧ���ҫ�ӡѹ��", "����͹", JOptionPane.WARNING_MESSAGE);
                    throw new Exception("cn");
                }
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            if (!(ex.getMessage() != null && ex.getMessage().equals("cn"))) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                theUS.setStatus(("�ѹ�֡�����żԴ��Ҵ"), UpdateStatus.ERROR);
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theUS.setStatus(("�ѹ�֡�����������"), UpdateStatus.COMPLETE);
        }
        return isComplete;
    }

    public int deleteServiceLimit(ServiceLimit... serviceLimits) {
        int ans = 0;
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (ServiceLimit serviceLimit : serviceLimits) {
                ans += theHosDB.theServiceLimitDB.delete(serviceLimit);
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            theUS.setStatus(("ź�����żԴ��Ҵ"), UpdateStatus.ERROR);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            ans = 0;
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theUS.setStatus(("ź�����������"), UpdateStatus.COMPLETE);
        }
        return ans;
    }

    public List<ComplexDataSource> listServiceLimitClinic() {
        List<ComplexDataSource> ret = new ArrayList<ComplexDataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<ServiceLimitClinic> list = this.theHosDB.theServiceLimitClinicDB.list();
            for (ServiceLimitClinic slc : list) {
                ret.add(new ComplexDataSource(slc, new Object[]{
                    slc.clinicName,
                    slc.time_start,
                    slc.time_end,
                    slc.limit_appointment,
                    slc.limit_walkin
                }));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ret;
    }

    public boolean addNewServiceLimitClinic(ServiceLimitClinic slc) {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<ServiceLimitClinic> listByIntervalTime = theHosDB.theServiceLimitClinicDB.listByClinicIdAndIntervalTime(slc.b_visit_clinic_id, slc.time_start, slc.time_end);
            // new
            if (slc.getObjectId() == null && listByIntervalTime.isEmpty()) {
                theHosDB.theServiceLimitClinicDB.insert(slc);
            } else {
                boolean isUpdated = false;
                for (ServiceLimitClinic slc1 : listByIntervalTime) {
                    // update
                    if (slc1.getObjectId().equals(slc.getObjectId())) {
                        theHosDB.theServiceLimitClinicDB.update(slc);
                        isUpdated = true;
                        break;
                    }
                }
                if (!isUpdated) {
                    JOptionPane.showMessageDialog(null, "�������ö�ѹ�֡��ǧ���ҫ�ӡѹ��㹤�Թԡ���ǡѹ", "����͹", JOptionPane.WARNING_MESSAGE);
                    throw new Exception("cn");
                }
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            if (!(ex.getMessage() != null && ex.getMessage().equals("cn"))) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                theUS.setStatus(("�ѹ�֡�����żԴ��Ҵ"), UpdateStatus.ERROR);
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theUS.setStatus(("�ѹ�֡�����������"), UpdateStatus.COMPLETE);
        }
        return isComplete;
    }

    public int deleteServiceLimitClinic(ServiceLimitClinic... slcs) {
        int ans = 0;
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (ServiceLimitClinic serviceLimitClinic : slcs) {
                ans += theHosDB.theServiceLimitClinicDB.delete(serviceLimitClinic);
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            theUS.setStatus(("ź�����żԴ��Ҵ"), UpdateStatus.ERROR);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            ans = 0;
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theUS.setStatus(("ź�����������"), UpdateStatus.COMPLETE);
        }
        return ans;
    }

    public void refreshPersonFromEmpolyee(Employee employee) {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Person person = theHosDB.thePersonDB.select(employee.t_person_id);
            if (person != null) {
                employee.person = person;
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
    }

    public VisitYear getCurrentVisitYear() {
        VisitYear visitYear = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            visitYear = theHosDB.theVisitYearDB.getVisitYear();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return visitYear;
    }

    public List<DataSource> listNotMapPlanGovOffical(String keyword) {
        List<DataSource> list = new ArrayList<DataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> listByKeyword = this.theHosDB.thePlanGovOfficalDB.listNotMapByKeyword(keyword);
            for (Object[] objects : listByKeyword) {
                list.add(new DataSource(String.valueOf(objects[0]), String.valueOf(objects[1])));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public List<DataSource> listMapPlanGovOffical(String keyword) {
        List<DataSource> list = new ArrayList<DataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> listByKeyword = this.theHosDB.thePlanGovOfficalDB.listByKeyword(keyword);
            for (Object[] objects : listByKeyword) {
                list.add(new DataSource(String.valueOf(objects[0]), String.valueOf(objects[1])));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public int savePlanGovOffical(PlanGovOffical... isccs) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (PlanGovOffical iscc : isccs) {
                this.theHosDB.thePlanGovOfficalDB.insert(iscc);
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public int deletePlanGovOffical(PlanGovOffical... isccs) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (PlanGovOffical iscc : isccs) {
                this.theHosDB.thePlanGovOfficalDB.delete(iscc);
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_DELETE);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public List<DataSource> listNotMapPlanSocialsec(String keyword) {
        List<DataSource> list = new ArrayList<DataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> listByKeyword = this.theHosDB.thePlanSocialsecDB.listNotMapByKeyword(keyword);
            for (Object[] objects : listByKeyword) {
                list.add(new DataSource(String.valueOf(objects[0]), String.valueOf(objects[1])));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public List<DataSource> listMapPlanSocialsec(String keyword) {
        List<DataSource> list = new ArrayList<DataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> listByKeyword = this.theHosDB.thePlanSocialsecDB.listByKeyword(keyword);
            for (Object[] objects : listByKeyword) {
                list.add(new DataSource(String.valueOf(objects[0]), String.valueOf(objects[1])));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public int savePlanSocialsec(PlanSocialsec... isccs) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (PlanSocialsec iscc : isccs) {
                this.theHosDB.thePlanSocialsecDB.insert(iscc);
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public int deletePlanSocialsec(PlanSocialsec... isccs) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (PlanSocialsec iscc : isccs) {
                this.theHosDB.thePlanSocialsecDB.delete(iscc);
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_DELETE);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public List<DataSource> listNotMapPlanClaimPrice(String keyword) {
        List<DataSource> list = new ArrayList<DataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> listByKeyword = this.theHosDB.thePlanClaimPriceDB.listNotMapByKeyword(keyword);
            for (Object[] objects : listByKeyword) {
                list.add(new DataSource(String.valueOf(objects[0]), String.valueOf(objects[1])));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public List<DataSource> listMapPlanClaimPrice(String keyword) {
        List<DataSource> list = new ArrayList<DataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> listByKeyword = this.theHosDB.thePlanClaimPriceDB.listByKeyword(keyword);
            for (Object[] objects : listByKeyword) {
                list.add(new DataSource(String.valueOf(objects[0]), String.valueOf(objects[1])));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public int savePlanClaimPrice(PlanClaimPrice... isccs) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (PlanClaimPrice iscc : isccs) {
                this.theHosDB.thePlanClaimPriceDB.insert(iscc);
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public int deletePlanClaimPrice(PlanClaimPrice... isccs) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (PlanClaimPrice iscc : isccs) {
                this.theHosDB.thePlanClaimPriceDB.delete(iscc);
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_DELETE);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public List<BVisitBed> listBedInfoByWardIdAndActive(String wardId, boolean isShowAll) {
        List<BVisitBed> bvbs = new ArrayList<BVisitBed>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            bvbs.addAll(this.theHosDB.theBVisitBedDB.selectByWardId(wardId, isShowAll));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return bvbs;
    }

    public int saveOrUpdateBVisitBedInfo(BVisitBed bvb) {
        int ans = 0;
        if ((bvb.sequences <= 0) || (bvb.bed_number.equals(""))) {
            theUS.setStatus("�������ö�ѹ�֡�������繤����ҧ �����繤�� 0 ��", UpdateStatus.WARNING);
            return ans;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<BVisitBed> listByActiveSeq = theHosDB.theBVisitBedDB.listByActiveSeq(bvb.sequences, bvb.b_visit_ward_id, bvb.getObjectId());
            if (!listByActiveSeq.isEmpty()) {
                int showConfirmDialog = JOptionPane.showConfirmDialog(theUS.getJFrame(), "�Ţ�ӴѺ��ӡѺ�Ţ��� �׹�ѹ�������¹�ŧ����������", "��͹", JOptionPane.OK_CANCEL_OPTION);
                if (showConfirmDialog == JOptionPane.CANCEL_OPTION) {
                    theUS.setStatus("¡��ԡ�¼����ҹ", UpdateStatus.WARNING);
                    throw new Exception("cn");
                } else {
                    BVisitBed old = theHosDB.theBVisitBedDB.selectById(bvb.getObjectId());
                    int oldSeq = old.sequences;
                    old.sequences = old.sequences * -1;
                    theHosDB.theBVisitBedDB.updateSeq(old);
                    if (oldSeq < bvb.sequences) {
                        List<BVisitBed> listIntervalSeq = theHosDB.theBVisitBedDB.listIntervalSeq(bvb.b_visit_ward_id, oldSeq, bvb.sequences);
                        for (BVisitBed temp : listIntervalSeq) {
                            temp.sequences = temp.sequences - 1;
                            theHosDB.theBVisitBedDB.updateSeq(temp);
                        }
                    } else {
                        List<BVisitBed> listIntervalSeq = theHosDB.theBVisitBedDB.listIntervalSeq(bvb.b_visit_ward_id, bvb.sequences, oldSeq);
                        for (int i = listIntervalSeq.size() - 1; i >= 0; i--) {
                            BVisitBed temp = listIntervalSeq.get(i);
                            temp.sequences = temp.sequences + 1;
                            theHosDB.theBVisitBedDB.updateSeq(temp);
                        }
                    }
                }
            }
            bvb.user_modify = theHO.theEmployee.getObjectId();
            if (bvb.getObjectId() == null) {
                bvb.user_record = theHO.theEmployee.getObjectId();
                theHosDB.theBVisitBedDB.insert(bvb);
            } else {
                theHosDB.theBVisitBedDB.updateInfo(bvb);
            }
            ans = 1;
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        } catch (Exception ex) {
            if (ex.getMessage() != null && !ex.getMessage().equals("cn")) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public Vector<Item> listItemByItemIds(String[] itemIds) {
        Vector<Item> items = new Vector<Item>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            items.addAll(this.theHosDB.theItemDB.selectByItemIds(itemIds));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return items;
    }

    public int updateBVisitBedOrder(BVisitBed bvb) {
        int ans = 0;
        if (bvb == null || bvb.getObjectId() == null || bvb.getObjectId().isEmpty()) {
            theUS.setStatus("��سҺѹ�֡��������§��͹", UpdateStatus.WARNING);
            return ans;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            bvb.user_modify = theHO.theEmployee.getObjectId();
            theHosDB.theBVisitBedDB.updateOrder(bvb);
            ans = 1;
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_UPDATE);
        } catch (Exception ex) {
            if (ex.getMessage() != null && !ex.getMessage().equals("cn")) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                hosControl.updateTransactionFail(HosControl.TRANSACTION_UPDATE);
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public List<ComboFix> listItemSubGroupOfDrugAndSupply() {
        List<ComboFix> list = new ArrayList<ComboFix>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String sql = "select"
                    + " b_item_subgroup.b_item_subgroup_id as b_item_subgroup_id"
                    + " ,b_item_subgroup.item_subgroup_description as item_subgroup_description"
                    + " from b_item_subgroup"
                    + " where"
                    + " b_item_subgroup.f_item_group_id in ('1','4')"
                    + " and b_item_subgroup.item_subgroup_active = '1'"
                    + " order by"
                    + " b_item_subgroup.f_item_group_id asc"
                    + " ,b_item_subgroup.item_subgroup_description asc";
            ResultSet rs = theConnectionInf.eQuery(sql);
            while (rs.next()) {
                ComboFix comboFix = new ComboFix();
                comboFix.code = rs.getString("b_item_subgroup_id");
                comboFix.name = rs.getString("item_subgroup_description");
                list.add(comboFix);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public List<ComplexDataSource> listMapDrugSupplyWithProductCategoryDS(String keyword, String grpId, String type) {
        List<ComplexDataSource> list = new ArrayList<ComplexDataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<MapProductCategory> listByKeyword = this.theHosDB.theMapProductCategoryDB.list(keyword, grpId, type);
            for (MapProductCategory mpc : listByKeyword) {
                list.add(new ComplexDataSource(
                        mpc,
                        new Object[]{mpc.item_name, mpc.product_category_name}));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public int saveMapDrugSupplyWithProductCategory(MapProductCategory... mpcs) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (MapProductCategory mpc : mpcs) {
                if (mpc.getObjectId() != null) {
                    this.theHosDB.theMapProductCategoryDB.delete(mpc.getObjectId());
                }
                this.theHosDB.theMapProductCategoryDB.insert(mpc);
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public int deleteMapDrugSupplyWithProductCategory(MapProductCategory... mpcs) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (MapProductCategory mpc : mpcs) {
                this.theHosDB.theMapProductCategoryDB.delete(mpc.getObjectId());
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_DELETE);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public List<ItemManufacturer> listItemManufacturerByKeywordAndActive(String keyword, String active) {
        List<ItemManufacturer> list = new ArrayList<ItemManufacturer>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            list.addAll(this.theHosDB.theItemManufacturerDB.listByKeyword(keyword, active));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public int saveOrUpdateItemManufacturer(ItemManufacturer im) {
        int ans = 0;
        if (im.item_manufacturer_number.equals("")) {
            theUS.setStatus("�������ö�ѹ�֡�����繤����ҧ��", UpdateStatus.WARNING);
            return ans;
        }
        if (im.item_manufacturer_description.equals("")) {
            theUS.setStatus("�������ö�ѹ�֡�����繤����ҧ��", UpdateStatus.WARNING);
            return ans;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<ItemManufacturer> list = theHosDB.theItemManufacturerDB.listByCode(im.item_manufacturer_number);
            if (!list.isEmpty()) {
                if (im.getObjectId() == null
                        || (list.size() == 1 && !list.get(0).getObjectId().equals(im.getObjectId()))
                        || list.size() > 1) {
                    theUS.setStatus("�������ö�ѹ�֡���ʫ����", UpdateStatus.WARNING);
                    throw new Exception("cn");
                }
            }
            im.user_modify = theHO.theEmployee.getObjectId();
            if (im.getObjectId() == null) {
                im.user_record = theHO.theEmployee.getObjectId();
                theHosDB.theItemManufacturerDB.insert(im);
            } else {
                theHosDB.theItemManufacturerDB.update(im);
            }
            ans = 1;
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        } catch (Exception ex) {
            if (ex.getMessage() != null && !ex.getMessage().equals("cn")) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public List<ItemDistributor> listItemDistributorByKeywordAndActive(String keyword, String active) {
        List<ItemDistributor> list = new ArrayList<ItemDistributor>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            list.addAll(this.theHosDB.theItemDistributorDB.listByKeyword(keyword, active));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public int saveOrUpdateItemDistributor(ItemDistributor im) {
        int ans = 0;
        if (im.item_distributor_number.equals("")) {
            theUS.setStatus("�������ö�ѹ�֡�����繤����ҧ��", UpdateStatus.WARNING);
            return ans;
        }
        if (im.item_distributor_description.equals("")) {
            theUS.setStatus("�������ö�ѹ�֡�����繤����ҧ��", UpdateStatus.WARNING);
            return ans;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<ItemDistributor> list = theHosDB.theItemDistributorDB.listByCode(im.item_distributor_number);
            if (!list.isEmpty()) {
                if (im.getObjectId() == null
                        || (list.size() == 1 && !list.get(0).getObjectId().equals(im.getObjectId()))
                        || list.size() > 1) {
                    theUS.setStatus("�������ö�ѹ�֡���ʫ����", UpdateStatus.WARNING);
                    throw new Exception("cn");
                }
            }
            im.user_modify = theHO.theEmployee.getObjectId();
            if (im.getObjectId() == null) {
                im.user_record = theHO.theEmployee.getObjectId();
                theHosDB.theItemDistributorDB.insert(im);
            } else {
                theHosDB.theItemDistributorDB.update(im);
            }
            ans = 1;
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        } catch (Exception ex) {
            if (ex.getMessage() != null && !ex.getMessage().equals("cn")) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public List<ComplexDataSource> listMapDrugTMTDS(String keyword, String grpId, String type) {
        List<ComplexDataSource> list = new ArrayList<ComplexDataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<MapDrugTMT> listByKeyword = this.theHosDB.theMapDrugTMTDB.list(keyword, grpId, type);
            for (MapDrugTMT mapDrugTMT : listByKeyword) {
                list.add(new ComplexDataSource(
                        mapDrugTMT,
                        new Object[]{mapDrugTMT.item_name, mapDrugTMT.fsn}));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public int saveMapDrugTMT(MapDrugTMT... mapDrugTMTs) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (MapDrugTMT mapDrugTMT : mapDrugTMTs) {
                if (mapDrugTMT.getObjectId() != null) {
                    this.theHosDB.theMapDrugTMTDB.delete(mapDrugTMT.getObjectId());
                }
                this.theHosDB.theMapDrugTMTDB.insert(mapDrugTMT);
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public int deleteMapDrugTMT(MapDrugTMT... mapDrugTMTs) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (MapDrugTMT mapDrugTMT : mapDrugTMTs) {
                this.theHosDB.theMapDrugTMTDB.delete(mapDrugTMT.getObjectId());
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_DELETE);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public List<ComboFix> listTMTManufacturer(String keyword) {
        List<ComboFix> list = new ArrayList<ComboFix>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            list.addAll(theHosDB.theDrugTMTDB.getComboboxDatasourceManufacturer(keyword));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public List<ComplexDataSource> listDrugTMTDS(String keyword, String manufacturer) {
        List<ComplexDataSource> list = new ArrayList<ComplexDataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<DrugTMT> listByKeyword = this.theHosDB.theDrugTMTDB.listByKeyword(keyword, manufacturer);
            for (DrugTMT drugTMT : listByKeyword) {
                list.add(new ComplexDataSource(
                        drugTMT,
                        new Object[]{drugTMT.getObjectId(), drugTMT.fsn}));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    /**
     * Drug code 24
     */
    public List<ComplexDataSource> listMapDrugCode24DS(String keyword, String grpId, String type) {
        List<ComplexDataSource> list = new ArrayList<ComplexDataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<MapDrugCode24> listByKeyword = this.theHosDB.theMapDrugCode24DB.list(keyword, grpId, type);
            for (MapDrugCode24 mapDrugCode24 : listByKeyword) {
                list.add(new ComplexDataSource(
                        mapDrugCode24,
                        new Object[]{mapDrugCode24.item_name, mapDrugCode24.item_name24 == null || mapDrugCode24.item_name24.isEmpty() ? "" : mapDrugCode24.item_name24}));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public int saveMapDrugCode24(MapDrugCode24... mapDrugCode24s) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (MapDrugCode24 mapDrugCode24 : mapDrugCode24s) {
                if (mapDrugCode24.getObjectId() != null) {
                    this.theHosDB.theMapDrugCode24DB.delete(mapDrugCode24.getObjectId());
                }
                this.theHosDB.theMapDrugCode24DB.insert(mapDrugCode24);
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public int deleteMapDrugCode24(MapDrugCode24... mapDrugCode24s) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (MapDrugCode24 mapDrugCode24 : mapDrugCode24s) {
                this.theHosDB.theMapDrugCode24DB.delete(mapDrugCode24.getObjectId());
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_DELETE);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public List<ComboFix> listDrugCode24Company(String keyword) {
        List<ComboFix> list = new ArrayList<ComboFix>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            list.addAll(theHosDB.theDrugCode24DB.getComboboxDatasourceCompany(keyword));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public List<ComplexDataSource> listDrugCode24DS(String keyword, String manufacturer) {
        List<ComplexDataSource> list = new ArrayList<ComplexDataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<DrugCode24> listByKeyword = this.theHosDB.theDrugCode24DB.listByKeyword(keyword, manufacturer);
            for (DrugCode24 drugTMT : listByKeyword) {
                String value = drugTMT.regno + " " + drugTMT.itemname
                        + "^" + drugTMT.tradename;
                if (drugTMT.itemname.lastIndexOf(", ") != -1) {
                    value += "\n" + drugTMT.itemname.substring(drugTMT.itemname.lastIndexOf(", ") + 2);
                }
                value += "\n" + drugTMT.regno
                        + "\n" + drugTMT.company;
                list.add(new ComplexDataSource(
                        drugTMT,
                        new Object[]{value}));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public Object[] checkMapTMTAndCode24(String itemId) {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String sql = "select \n"
                    + "b_map_drug_tmt.b_map_drug_tmt_id as tmt \n"
                    + ", b_nhso_map_drug.b_nhso_map_drug_id as code24 \n"
                    + "from b_item\n"
                    + "left join b_map_drug_tmt on b_map_drug_tmt.b_item_id = b_item.b_item_id\n"
                    + "left join b_nhso_map_drug  on b_nhso_map_drug.b_item_id = b_item.b_item_id\n"
                    + "where b_item.b_item_id = '" + itemId + "'";
            List<Object[]> eComplexQuery = theConnectionInf.eComplexQuery(sql);
            theConnectionInf.getConnection().commit();
            return eComplexQuery.isEmpty() ? new Object[]{"", ""} : eComplexQuery.get(0);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            return new Object[]{"", ""};
        } finally {
            theConnectionInf.close();
        }
    }

    public int saveOrUpdateDrugCode24(DrugCode24 drugCode24) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (drugCode24.getObjectId() == null) {
                DrugCode24 selectByCode24 = theHosDB.theDrugCode24DB.selectByCode24(drugCode24.drugcode24);
                if (selectByCode24 != null) {
                    theUS.setStatus("�������ö�ѹ�֡�� ���ͧ�ҡ���� 24 ��ѡ���", UpdateStatus.WARNING);
                    throw new Exception("cn");
                }
                theHosDB.theDrugCode24DB.insert(drugCode24);
            } else {
                theHosDB.theDrugCode24DB.update(drugCode24);
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            if (!(ex.getMessage() != null && ex.getMessage().equalsIgnoreCase("cn"))) {
                hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public Object[] getLastestImportMapDrugTMT() {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String sql = "select * from t_log_import_drug_tmt order by record_date_time desc limit 1";
            List<Object[]> eComplexQuery = theConnectionInf.eComplexQuery(sql);
            theConnectionInf.getConnection().commit();
            return eComplexQuery.isEmpty() ? null : eComplexQuery.get(0);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            return null;
        } finally {
            theConnectionInf.close();
        }
    }

    public Object[] getLastestExportMapDrugTMT() {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String sql = "select * from t_log_export_drug_tmt order by record_date_time desc limit 1";
            List<Object[]> eComplexQuery = theConnectionInf.eComplexQuery(sql);
            theConnectionInf.getConnection().commit();
            return eComplexQuery.isEmpty() ? null : eComplexQuery.get(0);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            return null;
        } finally {
            theConnectionInf.close();
        }
    }

    public boolean exportMapDrugTMT(String exportDir, String startDate, String endDate, boolean isXls, boolean isCsv) {
        theUS.setStatus("Exporting...", UpdateStatus.WARNING);
        boolean result = false;
        String sqlFile = Config.RESOURCE_DIR + File.separator + "sql" + File.separator + "core" + File.separator + "drug_tmt" + File.separator + "export_matched.sql";
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Map<String, String> params = new HashMap<String, String>();
            params.put("start_date", startDate);
            params.put("end_date", endDate);
            Map<String, Object> queryFromFile = theConnectionInf.queryFromFile(new File(sqlFile), params);
            String[] columnNames = (String[]) queryFromFile.get("cols");
            List<Object[]> rows = (List<Object[]>) queryFromFile.get("rows");
            if (rows.isEmpty()) {
                theUS.setStatus("��辺�����ŵ����ǧ���ҷ�����͡", UpdateStatus.WARNING);
                throw new Exception("cn");
            }
            String sql = "select * from t_log_export_drug_tmt order by record_date_time desc limit 1";
            List<Object[]> lastExport = theConnectionInf.eComplexQuery(sql);
            String seq = "001";
            if (!lastExport.isEmpty()) {
                String lastFileName = String.valueOf(lastExport.get(0)[2]);
                if (lastFileName != null && !lastFileName.isEmpty()) {
                    DecimalFormat formatter = new DecimalFormat("000");
                    seq = formatter.format(Integer.parseInt(lastFileName.substring(lastFileName.length() - 3)) + 1);
                }
            }
            String subDir = DateTimeUtil.getString(new Date(), "yyyyMMdd", DateTimeUtil.LOCALE_TH);
            String exportFileName = theHO.theSite.off_id + "Drug" + "N" + seq;
            // insert log
            sql = "INSERT INTO t_log_export_drug_tmt(\n"
                    + "            file_export_path, file_export_name, \n"
                    + "            file_export_extension, start_date, end_date, user_record_id)\n"
                    + "    VALUES (?, ?, ?, ?, ?, ?)";
            PreparedStatement ps1 = theConnectionInf.ePQuery(sql);
            ps1.setString(1, exportDir + File.separator + subDir);
            ps1.setString(2, exportFileName);
            String extensions = isXls ? "xls" : "";
            extensions += isCsv ? (extensions.isEmpty() ? "" : ",") + "csv" : "";
            ps1.setArray(3, theConnectionInf.getConnection().createArrayOf("varchar", extensions.split(",")));
            ps1.setString(4, startDate);
            ps1.setString(5, endDate);
            ps1.setString(6, theHO.theEmployee.getObjectId());
            ps1.executeUpdate();
            ps1.close();

            sql = "select max(t_log_export_drug_tmt_id) as t_log_export_drug_tmt_id from t_log_export_drug_tmt";
            List<Object[]> eComplexQuery = theConnectionInf.eComplexQuery(sql);
            int t_log_export_drug_tmt_id = (Integer) eComplexQuery.get(0)[0];

            // insert log
            for (Object[] objects : rows) {
                sql = "INSERT INTO t_log_export_drug_catalog(\n"
                        + "            b_item_id, t_log_export_drug_tmt_id, \n"
                        + "            hospdrugcode, productcat, tmtid, specprep, genericname, tradename, \n"
                        + "            dsfcode, dosageform, strength, content, unitprice, distributor, \n"
                        + "            manufacturer, ised, ndc24, packsize, packprice, updateflag, datechange, \n"
                        + "            dateupdate, dateeffective)\n"
                        + "    VALUES (?, ?, \n"
                        + "            ?, ?, ?, ?, ?, ?, \n"
                        + "            ?, ?, ?, ?, ?, ?, \n"
                        + "            ?, ?, ?, ?, ?, ?, ?, \n"
                        + "            ?, ?)";
                PreparedStatement ps2 = theConnectionInf.ePQuery(sql);
                int index = 1;
                ps2.setString(index++, String.valueOf(objects[0]));
                ps2.setInt(index++, t_log_export_drug_tmt_id);
                ps2.setString(index++, String.valueOf(objects[1]));
                ps2.setString(index++, String.valueOf(objects[2]));
                ps2.setString(index++, String.valueOf(objects[3]));
                ps2.setString(index++, String.valueOf(objects[4]));
                ps2.setString(index++, String.valueOf(objects[5]));
                ps2.setString(index++, String.valueOf(objects[6]));
                ps2.setString(index++, String.valueOf(objects[7]));
                ps2.setString(index++, String.valueOf(objects[8]));
                ps2.setString(index++, String.valueOf(objects[9]));
                ps2.setString(index++, String.valueOf(objects[10]));
                ps2.setString(index++, String.valueOf(objects[11]));
                ps2.setString(index++, String.valueOf(objects[12]));
                ps2.setString(index++, String.valueOf(objects[13]));
                ps2.setString(index++, String.valueOf(objects[14]));
                ps2.setString(index++, String.valueOf(objects[15]));
                ps2.setString(index++, String.valueOf(objects[16]));
                ps2.setString(index++, String.valueOf(objects[17]));
                ps2.setString(index++, String.valueOf(objects[18]));
                ps2.setString(index++, String.valueOf(objects[19]));
                ps2.setString(index++, String.valueOf(objects[20]));
                ps2.setString(index++, String.valueOf(objects[21]));
                ps2.executeUpdate();
                ps2.close();
            }
            if (isCsv) {
                File csvDir = new File(exportDir + File.separator + subDir);
                if (!csvDir.exists()) {
                    csvDir.mkdirs();
                }
                File csvFile = new File(csvDir.getAbsoluteFile(), exportFileName + ".csv");
                FileOutputStream fileOutputStream = new FileOutputStream(csvFile);
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream, "UTF-8");
                BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
                PrintWriter printWriter = new PrintWriter(bufferedWriter);
                for (int i = 1; i < columnNames.length; i++) {
                    String columnName = columnNames[i];
                    printWriter.append(columnName);
                    if (i == columnNames.length - 1) {
                        printWriter.append('\n');
                    } else {
                        printWriter.append('|');
                    }
                }
                for (Object[] objects : rows) {
                    for (int i = 1; i < objects.length; i++) {
                        String data = String.valueOf(objects[i]);
                        printWriter.append(data);
                        if (i == columnNames.length - 1) {
                            printWriter.append('\n');
                        } else {
                            printWriter.append('|');
                        }
                    }
                }
                printWriter.flush();
                printWriter.close();
                bufferedWriter.close();
                outputStreamWriter.close();
                fileOutputStream.close();
            }
            if (isXls) {
                File xlsDir = new File(exportDir + File.separator + subDir);
                if (!xlsDir.exists()) {
                    xlsDir.mkdirs();
                }
                File xlsFile = new File(xlsDir.getAbsoluteFile(), exportFileName + ".xls");
                FileOutputStream fileOut = new FileOutputStream(xlsFile);
                HSSFWorkbook workbook = new HSSFWorkbook();
                HSSFSheet worksheet = workbook.createSheet(exportFileName);
                // Column name row index 0
                HSSFRow rowColumnName = worksheet.createRow(0);
                for (int i = 0; i < columnNames.length; i++) {
                    String columnName = columnNames[i];
                    HSSFCell cell = rowColumnName.createCell(i, CellType.STRING);
                    cell.setCellValue(columnName);
                    HSSFCellStyle cellStyle = workbook.createCellStyle();
                    cellStyle.setAlignment(HorizontalAlignment.CENTER);
                    HSSFFont hSSFFont = workbook.createFont();
                    hSSFFont.setBold(true);
                    cellStyle.setFont(hSSFFont);
                    cell.setCellStyle(cellStyle);
                }
                // data start at row index 1
                for (int x = 0; x < rows.size(); x++) {
                    Object[] objects = rows.get(x);
                    HSSFRow row = worksheet.createRow(x + 1);
                    for (int i = 0; i < objects.length; i++) {
                        String data = String.valueOf(objects[i]);
                        HSSFCell cell = row.createCell(i, CellType.STRING);
                        cell.setCellValue(data);
                    }
                }
                workbook.write(fileOut);
                fileOut.flush();
                fileOut.close();
            }
            theConnectionInf.getConnection().commit();
            theUS.setStatus("Export Complete!", UpdateStatus.COMPLETE);
            result = true;
        } catch (Exception ex) {
            if (!(ex.getMessage() != null && ex.getMessage().equals("cn"))) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                theUS.setStatus("Export Error!", UpdateStatus.ERROR);
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }

        return result;
    }

    public List<ComplexDataSource> listDayOff(boolean isSelectAll, Date startDate) {
        List<ComplexDataSource> list = new ArrayList<ComplexDataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<BDayOff> dayOffs = this.theHosDB.theBDayOffDB.listDayOff(isSelectAll, startDate);
            for (BDayOff dayOff : dayOffs) {
                list.add(new ComplexDataSource(dayOff, new Object[]{
                    dayOff.dayoff_date, dayOff.dayoff_name, dayOff.dayoff_description
                }));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public int saveOrUpdateDayOff(BDayOff dayOff) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            BDayOff selectByDate = theHosDB.theBDayOffDB.selectByDate(dayOff.dayoff_date);
            if (selectByDate != null
                    && (!selectByDate.getObjectId().equals(dayOff.getObjectId()))) {
                theUS.setStatus("�������ö�ѹ�֡�ѹ��ش��ӡѹ��", UpdateStatus.WARNING);
                throw new Exception("cn");
            }
            dayOff.user_modify = theHO.theEmployee.getObjectId();
            if (dayOff.getObjectId() == null) {
                dayOff.user_record = theHO.theEmployee.getObjectId();
                theHosDB.theBDayOffDB.insert(dayOff);
            } else {
                theHosDB.theBDayOffDB.update(dayOff);
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            if (!(ex.getMessage() != null && ex.getMessage().equals("cn"))) {
                hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public int deleteDayOff(BDayOff... dayOffs) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (BDayOff dayOff : dayOffs) {
                this.theHosDB.theBDayOffDB.delete(dayOff);
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public Vector listXrayModalityBySearch(String keyword, String active) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc.addAll(theHosDB.theBModalityDB.listByKeyword(keyword, active.equals("1")));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public int saveXrayModality(BModality bModality) {
        int ans = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (bModality.getObjectId() == null) {
                ans = theHosDB.theBModalityDB.insert(bModality);
            } else {
                ans = theHosDB.theBModalityDB.update(bModality);
            }
            if (theLO.theXrayModality == null) {
                theLO.theXrayModality = new ArrayList<CommonInf>();
            } else {
                theLO.theXrayModality.clear();
            }
            theLO.theXrayModality.addAll(theHosDB.theBModalityDB.selectAll());
            theUS.setStatus("�ѹ�֡ Modality "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public int deleteXrayModality(BModality bModality) {
        int ans = 0;
        if (theLookupControl.readOption().life.equals(Active.isEnable())) {
            theUS.setStatus(ResourceBundle.getBundleText("�к����������ҹ����") + " "
                    + ResourceBundle.getBundleText("���ź�������������ö����"), UpdateStatus.WARNING);
            return ans;
        }
        if (bModality == null || bModality.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return ans;
        }
        if (!theUS.confirmBox(ResourceBundle.getBundleText("�׹�ѹ����ź��¡�ù��") + " "
                + ResourceBundle.getBundleText("�Ҩ�Դ�š�з��Ѻ������㹰ҹ��������"), UpdateStatus.WARNING)) {
            return 0;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ans = theHosDB.theBModalityDB.delete(bModality);
            theUS.setStatus(ResourceBundle.getBundleText("ź Modality") + " "
                    + com.hosv3.utility.ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public RestfulUrl getRestfulUrl(String id) {
        RestfulUrl restfulUrl = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            restfulUrl = theHosDB.theRestfulUrlDB.selectById(id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return restfulUrl;
    }

    public List<RestfulUrl> listRestfulUrls(String keyword) {
        List<RestfulUrl> list = new ArrayList<RestfulUrl>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            list.addAll(theHosDB.theRestfulUrlDB.list(keyword));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public boolean saveOrUpdateRestfulUrl(RestfulUrl restfulUrl) {
        boolean isSuccessed = false;
        if (restfulUrl.restful_code == null || restfulUrl.restful_code.isEmpty()) {
            theUS.setStatus("���������繤����ҧ", UpdateStatus.WARNING);
            return false;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            RestfulUrl selectByCode = theHosDB.theRestfulUrlDB.selectByCode(restfulUrl.restful_code);
            if (selectByCode == null
                    || (selectByCode.getObjectId().equals(restfulUrl.getObjectId()))) {
                restfulUrl.user_update_id = theHO.theEmployee.getObjectId();
                if (restfulUrl.getObjectId() == null) {
                    theHosDB.theRestfulUrlDB.insert(restfulUrl);
                } else {
                    theHosDB.theRestfulUrlDB.update(restfulUrl);
                }
            } else {
                theUS.setStatus("���ʫ���������ö�ѹ�֡��", UpdateStatus.WARNING);
                throw new Exception("cn");
            }
            theConnectionInf.getConnection().commit();
            isSuccessed = true;
        } catch (Exception ex) {
            if (ex != null && !ex.getMessage().equals("cn")) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                theUS.setStatus("��úѹ�֡������ �Դ��Ҵ", UpdateStatus.ERROR);
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isSuccessed) {
            theUS.setStatus("��úѹ�֡������ �����", UpdateStatus.COMPLETE);
        }

        return isSuccessed;
    }

    public boolean deleteRestfulUrl(RestfulUrl restfulUrl) {
        boolean isSuccessed = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theRestfulUrlDB.delete(restfulUrl);
            theConnectionInf.getConnection().commit();
            isSuccessed = true;
        } catch (Exception ex) {
            if (ex != null && !ex.getMessage().equals("cn")) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                theUS.setStatus("��úѹ�֡������ �Դ��Ҵ", UpdateStatus.ERROR);
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isSuccessed) {
            theUS.setStatus("��úѹ�֡������ �����", UpdateStatus.COMPLETE);
        }

        return isSuccessed;
    }

    public List<DataSource> listUnmapOrderDoctor(String keyword) {
        List<DataSource> list = new ArrayList<DataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> unmappeds = this.theHosDB.theMapOrderDoctorDB.listUnmap(keyword);
            for (Object[] objects : unmappeds) {
                list.add(new DataSource(String.valueOf(objects[0]), String.valueOf(objects[1])));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public List<DataSource> listMappedOrderDoctor(String keyword) {
        List<DataSource> list = new ArrayList<DataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<MapOrderDoctor> mapOrderDoctors = this.theHosDB.theMapOrderDoctorDB.listMapped(keyword);
            for (MapOrderDoctor mapOrderDoctor : mapOrderDoctors) {
                list.add(new DataSource(mapOrderDoctor.getObjectId(), mapOrderDoctor.item_subgroup_description));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public int saveMapOrderDoctor(MapOrderDoctor... mapOrderDoctors) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (MapOrderDoctor mapOrderDoctor : mapOrderDoctors) {
                mapOrderDoctor.user_update_id = theHO.theEmployee.getObjectId();
                mapOrderDoctor.update_date_time = theHO.date_time;
                mapOrderDoctor.user_record_id = theHO.theEmployee.getObjectId();
                mapOrderDoctor.record_date_time = theHO.date_time;
                this.theHosDB.theMapOrderDoctorDB.insert(mapOrderDoctor);
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public int deleteMapOrderDoctor(MapOrderDoctor... mapOrderDoctors) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (MapOrderDoctor mapOrderDoctor : mapOrderDoctors) {
                this.theHosDB.theMapOrderDoctorDB.delete(mapOrderDoctor.getObjectId());
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public Vector<BOpinionRecommendation> listOpinionRecommendation(String des) {
        Vector<BOpinionRecommendation> vector = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<BOpinionRecommendation> list = theHosDB.theBOpinionRecommendationDB.listByKeyword(des);
            vector = new Vector(list);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vector;
    }

    public int saveOpinionRecommendation(BOpinionRecommendation obj) {
        int ans = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            obj.user_update_id = theHO.theEmployee.getObjectId();
            if (obj.getObjectId() == null) {
                obj.user_record_id = theHO.theEmployee.getObjectId();
                theHosDB.theBOpinionRecommendationDB.insert(obj);
            } else {
                theHosDB.theBOpinionRecommendationDB.update(obj);
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public int deleteOpinionRecommendation(BOpinionRecommendation obj) {
        int ans = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ans = theHosDB.theBOpinionRecommendationDB.delete(obj);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_DELETE);
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public List<DataSource> listUnmapWarningInformIntolerance(String keyword) {
        List<DataSource> list = new ArrayList<DataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> unmappeds = this.theHosDB.theMapWarningInformIntoleranceDB.listUnmap(keyword);
            for (Object[] objects : unmappeds) {
                list.add(new DataSource(String.valueOf(objects[0]), String.valueOf(objects[1])));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public List<DataSource> listMappedWarningInformIntolerance(String keyword) {
        List<DataSource> list = new ArrayList<DataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<MapWarningInformIntolerance> mapWarningInformIntolerances = this.theHosDB.theMapWarningInformIntoleranceDB.listMapped(keyword);
            for (MapWarningInformIntolerance mapWarningInformIntolerance : mapWarningInformIntolerances) {
                list.add(new DataSource(mapWarningInformIntolerance.getObjectId(), mapWarningInformIntolerance.authenticationDescription));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public int saveMapWarningInformIntolerance(MapWarningInformIntolerance... mapWarningInformIntolerances) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (MapWarningInformIntolerance mapWarningInformIntolerance : mapWarningInformIntolerances) {
                mapWarningInformIntolerance.user_record_id = theHO.theEmployee.getObjectId();
                this.theHosDB.theMapWarningInformIntoleranceDB.insert(mapWarningInformIntolerance);
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public int deleteMapWarningInformIntolerance(MapWarningInformIntolerance... mapWarningInformIntolerances) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (MapWarningInformIntolerance mapWarningInformIntolerance : mapWarningInformIntolerances) {
                this.theHosDB.theMapWarningInformIntoleranceDB.delete(mapWarningInformIntolerance.getObjectId());
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public List<DataSource> listUnmapWarningDrugAllergy(String keyword) {
        List<DataSource> list = new ArrayList<DataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> unmappeds = this.theHosDB.theMapWarningDrugAllergyDB.listUnmap(keyword);
            for (Object[] objects : unmappeds) {
                list.add(new DataSource(String.valueOf(objects[0]), String.valueOf(objects[1])));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public List<DataSource> listMappedWarningDrugAllergy(String keyword) {
        List<DataSource> list = new ArrayList<DataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<MapWarningDrugAllergy> mapWarningDrugAllergys = this.theHosDB.theMapWarningDrugAllergyDB.listMapped(keyword);
            for (MapWarningDrugAllergy mapWarningDrugAllergy : mapWarningDrugAllergys) {
                list.add(new DataSource(mapWarningDrugAllergy.getObjectId(), mapWarningDrugAllergy.authenticationDescription));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public int saveMapWarningDrugAllergy(MapWarningDrugAllergy... mapWarningDrugAllergys) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (MapWarningDrugAllergy mapWarningDrugAllergy : mapWarningDrugAllergys) {
                mapWarningDrugAllergy.user_record_id = theHO.theEmployee.getObjectId();
                this.theHosDB.theMapWarningDrugAllergyDB.insert(mapWarningDrugAllergy);
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public int deleteMapWarningDrugAllergy(MapWarningDrugAllergy... mapWarningDrugAllergys) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (MapWarningDrugAllergy mapWarningDrugAllergy : mapWarningDrugAllergys) {
                this.theHosDB.theMapWarningDrugAllergyDB.delete(mapWarningDrugAllergy.getObjectId());
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public Vector listBTariff(String keyword, String active) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<BTariff> ccts = this.theHosDB.theBTariffDB.listByKeyword(keyword, active);
            for (BTariff cct : ccts) {
                vc.add(cct);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public int saveOrUpdateBTariff(BTariff creditCardType) {
        if (creditCardType.code.isEmpty()) {
            this.theUS.setStatus("��س��к����ʡ�͹�ѹ�֡", UpdateStatus.WARNING);
            return 0;
        }
        if (creditCardType.description.isEmpty()) {
            this.theUS.setStatus("��س��кت��͡�͹�ѹ�֡", UpdateStatus.WARNING);
            return 0;
        }
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            BTariff selectByCode = theHosDB.theBTariffDB.selectByCode(creditCardType.code);
            if (selectByCode != null && (creditCardType.getObjectId() == null ? selectByCode.getObjectId() != null : !creditCardType.getObjectId().equals(selectByCode.getObjectId()))) {
                this.theUS.setStatus("�������ö�ѹ�֡���ʫ���� �ô��Ǩ�ͺ ��д��Թ��������ա����", UpdateStatus.WARNING);
                throw new Exception("cancel");
            }
            theLookupControl.intReadDateTime();
            creditCardType.user_update_id = theHO.theEmployee.getObjectId();
            if (creditCardType.getObjectId() == null) {
                creditCardType.user_record_id = theHO.theEmployee.getObjectId();
                theHosDB.theBTariffDB.insert(creditCardType);
            } else {
                theHosDB.theBTariffDB.update(creditCardType);
            }
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        } catch (Exception ex) {
            if (!ex.getMessage().equals("cancel")) {
                hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public int deleteBTariff(BTariff creditCardType) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLookupControl.intReadDateTime();
            creditCardType.user_update_id = theHO.theEmployee.getObjectId();
            theHosDB.theBTariffDB.inactive(creditCardType);
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INACTIVE);
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INACTIVE);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public List<ComplexDataSource> listMapLabTMLTDS(String keyword, String grpId, String type) {
        List<ComplexDataSource> list = new ArrayList<>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<MapLabTMLT> listByKeyword = this.theHosDB.theMapLabTMLTDB.list(keyword, grpId, type);
            for (MapLabTMLT mapLabTMLT : listByKeyword) {
                list.add(new ComplexDataSource(
                        mapLabTMLT,
                        new Object[]{mapLabTMLT.item_name, mapLabTMLT.fsn}));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public List<ComplexDataSource> listLabTMLTDS(String keyword) {
        List<ComplexDataSource> list = new ArrayList<>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<LabTMLT> listByKeyword = this.theHosDB.theLabTMLTDB.listByKeyword(keyword);
            for (LabTMLT labTMLT : listByKeyword) {
                list.add(new ComplexDataSource(
                        labTMLT,
                        new Object[]{labTMLT.getObjectId(), labTMLT.tmlt_name}));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public List<ComboFix> listItemSubGroupOfLab() {
        List<ComboFix> list = new ArrayList<ComboFix>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String sql = "select"
                    + " b_item_subgroup.b_item_subgroup_id as b_item_subgroup_id"
                    + " ,b_item_subgroup.item_subgroup_description as item_subgroup_description"
                    + " from b_item_subgroup"
                    + " where"
                    + " b_item_subgroup.f_item_group_id in ('2')"
                    + " and b_item_subgroup.item_subgroup_active = '1'"
                    + " order by"
                    + " b_item_subgroup.f_item_group_id asc"
                    + " ,b_item_subgroup.item_subgroup_description asc";
            ResultSet rs = theConnectionInf.eQuery(sql);
            while (rs.next()) {
                ComboFix comboFix = new ComboFix();
                comboFix.code = rs.getString("b_item_subgroup_id");
                comboFix.name = rs.getString("item_subgroup_description");
                list.add(comboFix);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public int saveMapLabTMLT(MapLabTMLT... mapLabTMLTs) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (MapLabTMLT mapLabTMLT : mapLabTMLTs) {
                if (mapLabTMLT.getObjectId() != null) {
                    this.theHosDB.theMapLabTMLTDB.delete(mapLabTMLT.getObjectId());
                }
                this.theHosDB.theMapLabTMLTDB.insert(mapLabTMLT);
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public int deleteMapLabTMLT(MapLabTMLT... mapLabTMLTs) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (MapLabTMLT mapLabTMLT : mapLabTMLTs) {
                this.theHosDB.theMapLabTMLTDB.delete(mapLabTMLT.getObjectId());
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_DELETE);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public Object[] getLastestImportMapLabTMLT() {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String sql = "select * from t_log_import_lab_tmlt order by record_date_time desc limit 1";
            List<Object[]> eComplexQuery = theConnectionInf.eComplexQuery(sql);
            theConnectionInf.getConnection().commit();
            return eComplexQuery.isEmpty() ? null : eComplexQuery.get(0);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            return null;
        } finally {
            theConnectionInf.close();
        }
    }

    public Object[] getLastestExportMapLabTMLT() {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String sql = "select * from t_log_export_lab_tmlt order by record_date_time desc limit 1";
            List<Object[]> eComplexQuery = theConnectionInf.eComplexQuery(sql);
            theConnectionInf.getConnection().commit();
            return eComplexQuery.isEmpty() ? null : eComplexQuery.get(0);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            return null;
        } finally {
            theConnectionInf.close();
        }
    }

    public boolean exportMapLabTMLT(String exportDir, String startDate, String endDate) {
        theUS.setStatus("Exporting...", UpdateStatus.WARNING);
        boolean result = false;
        String sqlFile = Config.RESOURCE_DIR + File.separator + "sql" + File.separator + "core" + File.separator + "lab_tmlt" + File.separator + "export_tmlt_mapping.sql";
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Map<String, String> params = new HashMap<>();
            params.put("start_date", DateUtil.getStrChageTH2EN(startDate));
            params.put("end_date", DateUtil.getStrChageTH2EN(endDate));
            Map<String, Object> queryFromFile = theConnectionInf.queryFromFile(new File(sqlFile), params);
            String[] columnNames = (String[]) queryFromFile.get("cols");
            List<Object[]> rows = (List<Object[]>) queryFromFile.get("rows");
            if (rows.isEmpty()) {
                theUS.setStatus("��辺�����ŵ����ǧ���ҷ�����͡", UpdateStatus.WARNING);
                throw new Exception("cn");
            }
            String sql = "select * from t_log_export_lab_tmlt order by record_date_time desc limit 1";
            List<Object[]> lastExport = theConnectionInf.eComplexQuery(sql);
            String seq = "001";
            if (!lastExport.isEmpty()) {
                String lastFileName = String.valueOf(lastExport.get(0)[2]);
                if (lastFileName != null && !lastFileName.isEmpty()) {
                    DecimalFormat formatter = new DecimalFormat("000");
                    seq = formatter.format(Integer.parseInt(lastFileName.substring(lastFileName.length() - 3)) + 1);
                }
            }
            String subDir = DateUtil.getString(new Date(), "yyyyMMdd", DateUtil.LOCALE_TH);
            String exportFileName = theHO.theSite.off_id + "Lab" + "N" + seq;
            // insert log
            sql = "INSERT INTO t_log_export_lab_tmlt(\n"
                    + "            file_export_path, file_export_name, \n"
                    + "            file_export_extension, start_date, end_date, user_record_id)\n"
                    + "    VALUES (?, ?, ?, ?, ?, ?)";
            try (PreparedStatement ps1 = theConnectionInf.ePQuery(sql)) {
                ps1.setString(1, exportDir + File.separator + subDir);
                ps1.setString(2, exportFileName);
                String extensions = "xls";
                ps1.setArray(3, theConnectionInf.getConnection().createArrayOf("varchar", extensions.split(",")));
                ps1.setString(4, DateUtil.getStrChageTH2EN(startDate));
                ps1.setString(5, DateUtil.getStrChageTH2EN(endDate));
                ps1.setString(6, theHO.theEmployee.getObjectId());
                ps1.executeUpdate();
            }

            File xlsDir = new File(exportDir + File.separator + subDir);
            if (!xlsDir.exists()) {
                xlsDir.mkdirs();
            }
            File xlsFile = new File(xlsDir.getAbsoluteFile(), exportFileName + ".xls");
            try (FileOutputStream fileOut = new FileOutputStream(xlsFile)) {
                HSSFWorkbook workbook = new HSSFWorkbook();
                HSSFSheet worksheet = workbook.createSheet(exportFileName);
                // Column name row index 0
                HSSFRow rowColumnName = worksheet.createRow(0);
                for (int i = 0; i < columnNames.length; i++) {
                    String columnName = columnNames[i];
                    HSSFCell cell = rowColumnName.createCell(i, CellType.STRING);
                    cell.setCellValue(columnName);
                    HSSFCellStyle cellStyle = workbook.createCellStyle();
                    cellStyle.setAlignment(HorizontalAlignment.CENTER);
                    HSSFFont hSSFFont = workbook.createFont();
                    hSSFFont.setBold(true);
                    cellStyle.setFont(hSSFFont);
                    cell.setCellStyle(cellStyle);
                }
                // data start at row index 1
                for (int x = 0; x < rows.size(); x++) {
                    Object[] objects = rows.get(x);
                    HSSFRow row = worksheet.createRow(x + 1);
                    for (int i = 0; i < objects.length; i++) {
                        String data = String.valueOf(objects[i]);
                        HSSFCell cell = row.createCell(i, CellType.STRING);
                        cell.setCellValue(data);
                    }
                }
                workbook.write(fileOut);
                fileOut.flush();
            }
            theConnectionInf.getConnection().commit();
            theUS.setStatus("Export Complete!", UpdateStatus.COMPLETE);
            result = true;
        } catch (Exception ex) {
            if (!(ex.getMessage() != null && ex.getMessage().equals("cn"))) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                theUS.setStatus("Export Error!", UpdateStatus.ERROR);
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }

        return result;
    }

    public int saveOrUpdateBVisitRangeAge(BVisitRangeAge bVisitRangeAge) {
        if (bVisitRangeAge.code.isEmpty()) {
            this.theUS.setStatus("��س��к����ʡ�͹�ѹ�֡", UpdateStatus.WARNING);
            return 0;
        }
        if (bVisitRangeAge.description.isEmpty()) {
            this.theUS.setStatus("��س��кت��͡�͹�ѹ�֡", UpdateStatus.WARNING);
            return 0;
        }
        if (bVisitRangeAge.begin_age.isEmpty() || bVisitRangeAge.end_age.isEmpty()) {
            this.theUS.setStatus("�����Ū�ǧ���� �����繤����ҧ", UpdateStatus.WARNING);
            return 0;
        }
        if (Integer.parseInt(bVisitRangeAge.end_age) < Integer.parseInt(bVisitRangeAge.begin_age)) {
            this.theUS.setStatus("��ǧ��������ش�е�ͧ�ҡ���Ҫ�ǧ�����������", UpdateStatus.WARNING);
            return 0;
        }

        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<BVisitRangeAge> theBVisitRangeAges = theHosDB.theBVisitRangeAgeDB.listAll();
            for (BVisitRangeAge temp : theBVisitRangeAges) {
                if (temp == null) {
                    break;
                }
                if (bVisitRangeAge.getObjectId() == null && bVisitRangeAge.code.equals(temp.code)) {
                    this.theUS.setStatus("�������ö�ѹ�֡���ʫ���� �ô��Ǩ�ͺ ��д��Թ��������ա����", UpdateStatus.WARNING);
                    throw new Exception("cancel");
                }
                if (temp.getObjectId().equals(bVisitRangeAge.getObjectId())) {
                    continue;
                }
                int tStart = Integer.parseInt(temp.begin_age);
                int start = Integer.parseInt(bVisitRangeAge.begin_age);
                int tEnd = Integer.parseInt(temp.end_age);
                int end = Integer.parseInt(bVisitRangeAge.end_age);
                if (start <= tStart && tEnd <= end) {
                    theUS.setStatus("��������ǧ��¡������Ѻ��ԡ�� �ժ�ǧ���ط���͹�ѹ", UpdateStatus.WARNING);
                    return 0;
                } else if ((start <= tStart && tStart <= end) && (tStart <= end && end < tEnd)) {
                    theUS.setStatus("��������ǧ��¡������Ѻ��ԡ�� �ժ�ǧ���ط���͹�ѹ", UpdateStatus.WARNING);
                    return 0;
                } else if ((tStart <= start && start <= tEnd) && (start <= tEnd && tEnd <= end)) {
                    theUS.setStatus("��������ǧ��¡������Ѻ��ԡ�� �ժ�ǧ���ط���͹�ѹ", UpdateStatus.WARNING);
                    return 0;
                } else if (tStart <= start && end <= tEnd) {
                    theUS.setStatus("��������ǧ��¡������Ѻ��ԡ�� �ժ�ǧ���ط���͹�ѹ", UpdateStatus.WARNING);
                    return 0;
                }
            }
            theLookupControl.intReadDateTime();
            bVisitRangeAge.user_update_id = theHO.theEmployee.getObjectId();
            if (bVisitRangeAge.getObjectId() == null) {
                bVisitRangeAge.user_record_id = theHO.theEmployee.getObjectId();
                theHosDB.theBVisitRangeAgeDB.insert(bVisitRangeAge);
            } else {
                theHosDB.theBVisitRangeAgeDB.update(bVisitRangeAge);
            }

            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        } catch (Exception ex) {
            if (!ex.getMessage().equals("cancel")) {
                hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public int inactiveBVisitRangeAge(BVisitRangeAge bVisitRangeAge) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLookupControl.intReadDateTime();
            bVisitRangeAge.user_update_id = theHO.theEmployee.getObjectId();
            theHosDB.theBVisitRangeAgeDB.inactive(bVisitRangeAge);
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INACTIVE);
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INACTIVE);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public Vector<BVisitRangeAge> listBVisitRangeAge(String keyword, String active) {
        Vector<BVisitRangeAge> list = new Vector<>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            list.addAll(this.theHosDB.theBVisitRangeAgeDB.listByKeyword(keyword, active));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public List<NewsPewsLineToken> listLineNotifyTokenByServicepoint(String id, boolean isOPD) {
        List<NewsPewsLineToken> vc = new ArrayList<>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theNewsPewsLineTokenDB.selectByServicepoint(id, isOPD);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public int saveOrUpdateLineNotifyToken(NewsPewsLineToken newsPewsLineToken) {
        if (newsPewsLineToken.line_notify_token.isEmpty()) {
            this.theUS.setStatus("��س��к� Line Token ��͹�ѹ�֡", UpdateStatus.WARNING);
            return 0;
        }
        if (newsPewsLineToken.line_notify_name.isEmpty()) {
            this.theUS.setStatus("��س��кت��͡�͹�ѹ�֡", UpdateStatus.WARNING);
            return 0;
        }
        if (newsPewsLineToken.b_service_point_id == null && newsPewsLineToken.b_visit_ward_id == null) {
            this.theUS.setStatus("��س����͡�ش��ԡ��", UpdateStatus.WARNING);
            return 0;
        }
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            NewsPewsLineToken selectByToken = theHosDB.theNewsPewsLineTokenDB.selectByToken(newsPewsLineToken.line_notify_token, newsPewsLineToken.b_service_point_id);
            if (selectByToken != null && (newsPewsLineToken.getObjectId() != null ? !selectByToken.getObjectId().equals(newsPewsLineToken.getObjectId()) : true)) {
                this.theUS.setStatus("�������ö�ѹ�֡ Line Token ����� �ô��Ǩ�ͺ ��д��Թ��������ա����", UpdateStatus.WARNING);
                return 0;
            }
            theLookupControl.intReadDateTime();
            newsPewsLineToken.user_update_id = theHO.theEmployee.getObjectId();
            if (newsPewsLineToken.getObjectId() == null) {
                newsPewsLineToken.user_record_id = theHO.theEmployee.getObjectId();
                theHosDB.theNewsPewsLineTokenDB.insert(newsPewsLineToken);
            } else {
                theHosDB.theNewsPewsLineTokenDB.update(newsPewsLineToken);
            }
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public int deleteLineNotifyToken(NewsPewsLineToken lineNotifyToken) {
        if (lineNotifyToken == null || lineNotifyToken.getObjectId() == null) {
            return 0;
        }
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theNewsPewsLineTokenDB.delete(lineNotifyToken);
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public List<NewsPewsScore> listAllNewsPewsScore(boolean isNews) {
        List<NewsPewsScore> vc = new ArrayList<>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theNewsPewsScoreDB.selectByType(isNews ? "NEWS" : "PEWS");
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public int saveOrUpdateNewsPewsScore(NewsPewsScore theNewsPewsScore) {
        if (theNewsPewsScore.hours == 0 && theNewsPewsScore.mins == 0) {
            this.theUS.setStatus("��س��кبӹǹ����������͹ҷ� ��͹�ѹ�֡", UpdateStatus.WARNING);
            return 0;
        }
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            NewsPewsScore selectByScore = theHosDB.theNewsPewsScoreDB.selectByScoreAndType(
                    theNewsPewsScore.score, theNewsPewsScore.newspews_type);
            if (selectByScore != null && (theNewsPewsScore.getObjectId() != null ? !selectByScore.getObjectId().equals(theNewsPewsScore.getObjectId()) : true)) {
                this.theUS.setStatus("�������ö�ѹ�֡ score ����� �ô��Ǩ�ͺ ��д��Թ��������ա����", UpdateStatus.WARNING);
                return 0;
            }
            theLookupControl.intReadDateTime();
            theNewsPewsScore.user_update_id = theHO.theEmployee.getObjectId();
            if (theNewsPewsScore.getObjectId() == null) {
                theNewsPewsScore.user_record_id = theHO.theEmployee.getObjectId();
                theHosDB.theNewsPewsScoreDB.insert(theNewsPewsScore);
            } else {
                theHosDB.theNewsPewsScoreDB.update(theNewsPewsScore);
            }
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus("�ѹ�֡�Դ��Ҵ", UpdateStatus.ERROR);
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public int deleteNewsPewsScore(NewsPewsScore newsPewsScore) {
        if (newsPewsScore == null || newsPewsScore.getObjectId() == null) {
            return 0;
        }
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theNewsPewsScoreDB.delete(newsPewsScore);
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public List<NewsPewsMessagePattern> listAllNewsPewsMessagePattern(boolean isNews) {
        List<NewsPewsMessagePattern> vc = new ArrayList<>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theNewsPewsMessagePatternDB.selectByType(isNews ? "NEWS" : "PEWS");
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public int saveOrUpdateNewsPewsMessagePattern(NewsPewsMessagePattern theNewsPewsMessagePattern) {
        if (theNewsPewsMessagePattern.message_pattern.trim().isEmpty()) {
            return 0;
        }
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);

            theLookupControl.intReadDateTime();
            theNewsPewsMessagePattern.user_update_id = theHO.theEmployee.getObjectId();
            if (theNewsPewsMessagePattern.getObjectId() == null) {
                theNewsPewsMessagePattern.user_record_id = theHO.theEmployee.getObjectId();
                theHosDB.theNewsPewsMessagePatternDB.insert(theNewsPewsMessagePattern);
            } else {
                theHosDB.theNewsPewsMessagePatternDB.update(theNewsPewsMessagePattern);
            }
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus("�ѹ�֡�Դ��Ҵ", UpdateStatus.ERROR);
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public Vector<ModuleLicense> listModuleLicense(String keyword) {
        Vector<ModuleLicense> vc = new Vector<>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<ModuleLicense> ccts = this.theHosDB.theModuleLicenseDB.list(keyword);
            for (ModuleLicense cct : ccts) {
                vc.add(cct);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public int upsertModuleLicense(ModuleLicense m) {
        if (m.license.isEmpty()) {
            this.theUS.setStatus("��س��к� license ��͹�ѹ�֡", UpdateStatus.WARNING);
            return 0;
        }
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLookupControl.intReadDateTime();
            m.user_record_id = theHO.theEmployee.getObjectId();
            theHosDB.theModuleLicenseDB.upsert(m);
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        } catch (Exception ex) {
            if (!ex.getMessage().equals("cancel")) {
                hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public int deleteModuleLicense(String id) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theModuleLicenseDB.delete(id);
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INACTIVE);
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INACTIVE);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public Vector listItemPackage(String pkItem_id, boolean acive) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc.addAll(theHosDB.theItemPackageDB.selectByItem(pkItem_id, acive ? "1" : "0"));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public int deleteItemPackage(ItemPackage itemPackage) {
        int ans = 0;
        if (itemPackage == null || itemPackage.getObjectId() == null) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return ans;
        }
        if (theLookupControl.readOption().life.equals(Active.isEnable())) {
            theUS.setStatus(ResourceBundle.getBundleText("�к����������ҹ����") + " "
                    + ResourceBundle.getBundleText("���ź�������������ö����"), UpdateStatus.WARNING);
            return ans;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ans = theHosDB.theItemPackageDB.delete(itemPackage);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public List<BUserApikey> listAllApiKeyByUserId(String userId) {
        List<BUserApikey> vc = new ArrayList<>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = this.theHosDB.theBUserApikeyDB.listByUserId(userId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public String createBUserApikey(String appName, String userId) {
        String apiKey = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            BUserApikey bua = new BUserApikey();
            bua.created_by = theHO.theEmployee.getObjectId();
            bua.b_employee_id = userId;

            UUID uuid = UUID.randomUUID();
            String prefix = RandomString.alphanumericString(10);
            String hashed = MD5.hash(uuid.toString());
            String b64 = Base64.convertStringToBase64(uuid.toString());

//            Hash hash = Password.hash(uuid.toString()).withBcrypt();
            bua.app_name = appName;
            bua.api_key = prefix + "." + hashed;
            theHosDB.theBUserApikeyDB.insert(bua);
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            apiKey = prefix + "." + b64;
        } catch (Exception ex) {
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
        } finally {
            theConnectionInf.close();
        }
        return apiKey;
    }

    public int deleteBUserApikey(String id) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theBUserApikeyDB.delete(id);
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INACTIVE);
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INACTIVE);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public List<ComplexDataSource> listServiceLimitServicePoint() {
        List<ComplexDataSource> ret = new ArrayList<>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<ServiceLimitServicePoint> list = this.theHosDB.theServiceLimitServicePointDB.list();
            for (ServiceLimitServicePoint slc : list) {
                ret.add(new ComplexDataSource(slc, new Object[]{
                    slc.serviceName,
                    slc.time_start,
                    slc.time_end,
                    slc.limit_appointment,
                    slc.limit_walkin
                }));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ret;
    }

    public boolean addNewServiceLimitServicePoint(ServiceLimitServicePoint slc) {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<ServiceLimitServicePoint> listByIntervalTime = theHosDB.theServiceLimitServicePointDB.listByServicePointIdAndIntervalTime(slc.b_service_point_id, slc.time_start, slc.time_end);
            // new
            if (slc.getObjectId() == null && listByIntervalTime.isEmpty()) {
                ServiceLimitServicePoint checkFromDb = theHosDB.theServiceLimitServicePointDB.selectByServicePointId(slc.b_service_point_id);
                if (checkFromDb != null) {
                    JOptionPane.showMessageDialog(null, "�������ö�ѹ�֡��ǧ���������� 㹨ش��ԡ�����ǡѹ", "����͹", JOptionPane.WARNING_MESSAGE);
                    throw new Exception("cn");
                }
                theHosDB.theServiceLimitServicePointDB.insert(slc);
            } else {
                boolean isUpdated = false;
                for (ServiceLimitServicePoint slc1 : listByIntervalTime) {
                    // update
                    if (slc1.getObjectId().equals(slc.getObjectId())) {
                        theHosDB.theServiceLimitServicePointDB.update(slc);
                        isUpdated = true;
                        break;
                    }
                }
                if (!isUpdated) {
                    JOptionPane.showMessageDialog(null, "�������ö�ѹ�֡��ǧ���ҫ�ӡѹ��㹨ش��ԡ�����ǡѹ", "����͹", JOptionPane.WARNING_MESSAGE);
                    throw new Exception("cn");
                }
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            if (!(ex.getMessage() != null && ex.getMessage().equals("cn"))) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                theUS.setStatus(("�ѹ�֡�����żԴ��Ҵ"), UpdateStatus.ERROR);
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theUS.setStatus(("�ѹ�֡�����������"), UpdateStatus.COMPLETE);
        }
        return isComplete;
    }

    public int deleteServiceLimitServicePoint(ServiceLimitServicePoint... slcs) {
        int ans = 0;
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (ServiceLimitServicePoint serviceLimitServicePoint : slcs) {
                ans += theHosDB.theServiceLimitServicePointDB.delete(serviceLimitServicePoint);
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            theUS.setStatus(("ź�����żԴ��Ҵ"), UpdateStatus.ERROR);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            ans = 0;
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theUS.setStatus(("ź�����������"), UpdateStatus.COMPLETE);
        }
        return ans;
    }

    public byte[] selectItemPictureByItemId(String itemId) {
        byte[] img = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            img = theHosDB.theItemPictureDB.selectItemPictureByItemId(itemId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return img;
    }

    public int updateItemPicture(Item theItem) {
        int ret = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (theHosDB.theItemPictureDB.countByItemId(theItem.getObjectId()) > 0) {
                ret = theHosDB.theItemPictureDB.delete(theItem);
            }
            if (theItem.item_picture != null) {
                ret = theHosDB.theItemPictureDB.insert(theItem, theHO.theEmployee.getObjectId());
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ret;
    }
}

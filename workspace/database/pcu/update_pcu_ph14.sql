--issues#714
-- เพิ่มเก็บข้อมูล b_hstock_item_id ในรายการวัคซีน กรณีที่เปิดใช้งานระบบคลัง
ALTER TABLE b_health_epi_item ADD IF NOT EXISTS b_hstock_item_id VARCHAR(255) DEFAULT NULL;  

-- update db version
INSERT INTO s_health_version VALUES ('9710000000014','14','PCU, Community Edition','1.40.0','1.8.0', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

insert into s_script_update_log values ('pcu','update_pcu_ph14.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'update PCU ph13 -> ph14');
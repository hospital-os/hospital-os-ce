/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;
import java.util.Date;

/**
 *
 * @author tanakrit
 */
public class NewsPewsMessagePattern extends Persistent {

    public String message_pattern = "";
    public boolean is_opd;
    public String newspews_type;
    public String user_record_id;
    public Date record_date_time;
    public String user_update_id;
    public Date update_date_time;
}

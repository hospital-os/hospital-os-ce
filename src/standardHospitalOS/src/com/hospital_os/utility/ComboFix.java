/*
 * ComboFix.java
 *
 * Created on 2 ���Ҥ� 2546, 17:04 �.
 */
package com.hospital_os.utility;

import com.hospital_os.usecase.connection.*;

/**
 *
 * @author Administrator
 */
@SuppressWarnings("ClassWithoutLogger")
public class ComboFix implements CommonInf {

    public String code;
    public String name;
    public String other;

    /**
     * Creates a new instance of FixHospital
     */
    public ComboFix() {
        code = new String();
        name = new String();
        other = new String();
    }

    public ComboFix(String code, String str) {
        this.code = code;
        this.name = str;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getName() {
        return name;
    }

    public String getOther() {
        return other;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class Printing extends Persistent {

    public static String PRINT_OPD_CARD = "1";
    public static String PRINT_RECEIPT = "2";
    public static String PRINT_APPOINTMENT = "3";
    public static String PRINT_INDEX_XRAY = "4";
    public static String PRINT_DRUG_RX = "5";
    public static String PRINT_DRUG_STICKER = "6";
    public static String PRINT_RESULT_LAB = "7";
    public static String PRINT_REFER = "8";
    public static String PRINT_APPOINTMENT_LIST = "9";
    public static String PRINT_MEDCER = "10";
    public static String PRINT_MEDCER_WORK = "11";
    public static String PRINT_MEDCER_ALIEN = "12";

    public String description = "";
    public String default_jrxml = "";
    public String user_record = "";
    public String enable = "1";
    public String enable_other_language = "0";
    public String enable_choose_language = "0";

    public static Printing getPrinting(String type, List<Printing> printings) {
        for (Printing printing : printings) {
            if (type.equals(printing.getObjectId())) {
                return printing;
            }
        }
        return null;
    }
}

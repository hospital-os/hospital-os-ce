/*
 * ItemLookup.java
 *
 * Created on 8 �ԧ�Ҥ� 2549, 15:20 �.
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
package com.hosv3.control.lookup;

import com.hospital_os.object.Item;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.LookupControlInf;
import com.hosv3.control.LookupControl;
import com.hosv3.control.SetupControl;
import com.hosv3.utility.connection.ExecuteControlInf;

/**
 *
 * @not deprecated because use henbe package and bad read Data
 *
 * @author henbe
 */
public class ItemLookup implements LookupControlInf, ExecuteControlInf {

    boolean is_lookup = true;
    private LookupControl theLookup;
    private SetupControl theEC;

    /**
     * Creates a new instance of ItemLookup
     */
    public ItemLookup(LookupControl lookup) {
        theLookup = lookup;
    }

    public ItemLookup(SetupControl lookup) {
        is_lookup = false;
        theEC = lookup;
    }

    @Override
    public boolean execute(Object str) {
        theEC.addItemDx((Item) str);
        return true;
    }

    @Override
    @SuppressWarnings("UseOfObsoleteCollectionType")
    public java.util.Vector listData(String str) {
        if (str == null || str.equals("") || str.equals("%")) {
            return null;
        } else {
            return theLookup.listItemByName(str);
        }
    }

    public CommonInf readData(String str) {
        return null;
    }

    @Override
    public CommonInf readHosData(String str) {
        return null;
    }
}

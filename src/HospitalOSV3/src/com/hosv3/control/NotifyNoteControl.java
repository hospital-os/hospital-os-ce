/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.control;

import com.hospital_os.object.Authentication;
import com.hospital_os.object.Employee;
import com.hospital_os.object.NotifyNote;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hosv3.object.HosObject;
import com.hosv3.subject.HosSubject;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Logger;
import sd.comp.idandcombobox.KeyValue;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class NotifyNoteControl {

    protected ConnectionInf theConnectionInf;
    protected HosDB theHosDB;
    protected HosObject theHO;
    protected HosSubject theHS;
    protected UpdateStatus theUS;
    protected LookupControl theLookupControl;
    private HosControl hosControl;

    /**
     * Creates a new instance of LookupControl
     */
    public NotifyNoteControl(ConnectionInf con, HosObject ho, HosDB hdb, HosSubject hs, LookupControl theLookupControl) {
        theConnectionInf = con;
        theHosDB = hdb;
        theHS = hs;
        theHO = ho;
        this.theLookupControl = theLookupControl;
    }

    public void setHosControl(HosControl hosControl) {
        this.hosControl = hosControl;
    }

    public void setUpdateStatus(UpdateStatus theUS) {
        this.theUS = theUS;
    }

    @SuppressWarnings("UseOfObsoleteCollectionType")
    public List<NotifyNote> listNotifyNote(String hn) {
        List<NotifyNote> list = new ArrayList<NotifyNote>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            list.addAll(theHosDB.theNotifyNoteDB.listByHn(hn));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    @SuppressWarnings("UseOfObsoleteCollectionType")
    public List<NotifyNote> listNotifyNote2(String hn) {
        List<NotifyNote> list = new ArrayList<NotifyNote>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            list.addAll(theHosDB.theNotifyNoteDB.listByHn2(hn));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public NotifyNote getNotifyNote(String id) {
        NotifyNote notifyNote = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            notifyNote = theHosDB.theNotifyNoteDB.getById(id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return notifyNote;
    }

    public int saveOrUpdateNotifyNote(NotifyNote obj) {
        int iRes = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String date_time = theLookupControl.intReadDateTime();
            if (obj.getObjectId() == null) {
                obj.patient_hn = theHO.thePatient.hn;
                obj.visit_id_rec = theHO.theVisit == null ? "" : theHO.theVisit.getObjectId();
                obj.rec_staff = theHO.theEmployee.getObjectId();
                obj.rec_datetime = date_time;
                obj.active = "1";
                theHosDB.theNotifyNoteDB.insert(obj);
            } else {
                obj.mod_datetime = date_time;
                theHosDB.theNotifyNoteDB.update(obj);
            }
            theConnectionInf.getConnection().commit();
            iRes = 1;
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
        } finally {
            theConnectionInf.close();
        }
        return iRes;
    }

    public int deleteNotifyNote(NotifyNote obj) {
        int iRes = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String date_time = theLookupControl.intReadDateTime();
            obj.del_datetime = date_time;
            obj.active = "0";
            theHosDB.theNotifyNoteDB.inactive(obj);
            theConnectionInf.getConnection().commit();
            iRes = 1;
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INACTIVE);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INACTIVE);
        } finally {
            theConnectionInf.close();
        }
        return iRes;
    }

    public KeyValue[] getNotifyType() {
        KeyValue[] keyvalues = new KeyValue[]{};
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> listNotifyType = theHosDB.theNotifyNoteDB.listNotifyType();
            keyvalues = new KeyValue[listNotifyType.size()];
            for (int i = 0; i < keyvalues.length; i++) {
                Object[] get = listNotifyType.get(i);
                keyvalues[i] = new KeyValue((String) get[0], (String) get[1]);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return keyvalues;
    }

    public List<NotifyNote> listNotifyNoteViewOnly(String hn, String visitId, int mode, String empId, String authenID) {
        List<NotifyNote> list = new ArrayList<NotifyNote>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            list.addAll(theHosDB.theNotifyNoteDB.listByHnAbdVisitId(hn, visitId, mode, empId, authenID));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public List<Employee> getRemover(String... ids) {
        List<Employee> list = new ArrayList<Employee>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector<Employee> vc = theHosDB.theEmployeeDB.selectByIds(ids);
            for (Employee employee : vc) {
                list.add(employee);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public List<Authentication> getAuthen(String... ids) {
        List<Authentication> vc = new ArrayList<>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (String id : ids) {
                Authentication at = theHosDB.theAuthenticationDB.selectByPK(id);
                vc.add(at);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public List<Employee> listDefaultDeleteUser() {
        List<Employee> list = new ArrayList<Employee>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector<Employee> vc = theHosDB.theMapUserDeleteNotifyDB.listEmployee();
            for (Employee employee : vc) {
                list.add(employee);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }
    private static final Logger LOG = Logger.getLogger(NotifyNoteControl.class.getName());
}

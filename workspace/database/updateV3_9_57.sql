-- update for kiosk miss in 3.9.53
update t_visit set f_visit_opd_discharge_status_id = '' where f_visit_opd_discharge_status_id is null;
update t_visit set f_visit_ipd_discharge_type_id = '' where f_visit_ipd_discharge_type_id is null;
update t_visit set f_visit_ipd_discharge_status_id = '' where f_visit_ipd_discharge_status_id is null;

ALTER TABLE t_patient_past_vaccine ALTER COLUMN t_patient_id TYPE varchar(30) USING t_patient_id::varchar;

ALTER TABLE public.t_visit ALTER COLUMN f_visit_opd_discharge_status_id SET DEFAULT ''::character varying;
ALTER TABLE public.t_visit ALTER COLUMN f_visit_ipd_discharge_type_id SET DEFAULT ''::character varying;
ALTER TABLE public.t_visit ALTER COLUMN f_visit_ipd_discharge_status_id SET DEFAULT ''::character varying;
ALTER TABLE public.t_visit ALTER COLUMN f_visit_opd_discharge_status_id SET NOT NULL;
ALTER TABLE public.t_visit ALTER COLUMN f_visit_ipd_discharge_type_id SET NOT NULL;
ALTER TABLE public.t_visit ALTER COLUMN f_visit_ipd_discharge_status_id SET NOT NULL;

update t_result_lab set result_lab_value = '' where result_lab_value is null;

ALTER TABLE public.t_result_lab ALTER COLUMN result_lab_value SET DEFAULT ''::character varying;
ALTER TABLE t_result_lab ALTER COLUMN result_lab_value SET NOT NULL;

insert into f_gui_action (f_gui_action_id, gui_action_name) VALUES ('0422', 'ประวัติใบรับรองแพทย์');
insert into f_gui_action (f_gui_action_id, gui_action_name) VALUES ('2605', 'ออกใบรับรองแพทย์');
insert into f_gui_action (f_gui_action_id, gui_action_name) VALUES ('5413', 'สรุปความเห็นแพทย์');

CREATE TABLE public.b_opinion_recommendation (
	id varchar NOT NULL,
	code varchar NOT NULL,
	description varchar NOT NULL,
	active varchar NOT NULL DEFAULT '1',
        record_datetime              timestamp without time zone NOT NULL DEFAULT current_timestamp,
        user_record_id               character varying(30)   NOT NULL,
        update_datetime              timestamp without time zone NOT NULL DEFAULT current_timestamp,
        user_update_id               character varying(30)   NOT NULL,
	CONSTRAINT b_opinion_recommendation_pk PRIMARY KEY (id)
);
CREATE INDEX b_opinion_recommendation_code_idx ON public.b_opinion_recommendation (code);
CREATE INDEX b_opinion_recommendation_description_idx ON public.b_opinion_recommendation (description);
CREATE INDEX b_opinion_recommendation_active_idx ON public.b_opinion_recommendation (active);


DROP TABLE IF EXISTS t_medical_certificate;
DROP TABLE IF EXISTS f_medical_certificate_type;

CREATE TABLE public.f_medical_certificate_type (
	id int NOT NULL,
	description varchar(255) NOT NULL,
	CONSTRAINT f_medical_certificate_type_id_pk PRIMARY KEY (id)
);

INSERT INTO public.f_medical_certificate_type (id,description)
	VALUES (1,'ใบรับรองความเจ็บป่วย');
INSERT INTO public.f_medical_certificate_type (id,description)
	VALUES (2,'ใบรับรองสุขภาพ');
INSERT INTO public.f_medical_certificate_type (id,description)
	VALUES (3,'ใบรับรองแพทย์ต่างด้าว');

CREATE TABLE public.t_medical_certificate (
	id serial NOT NULL,
	hn varchar(10) NOT NULL,
	vn varchar(10) NOT NULL,
	doctor_id varchar(30) NOT NULL,
	f_medical_certificate_type_id int4 NOT NULL,
	medical_certificate_no varchar(30) NOT NULL,
	issue_date date NOT NULL DEFAULT CURRENT_DATE,
	record_datetime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	user_record_id varchar(30) NOT NULL,
	update_datetime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	user_update_id varchar(30) NOT NULL,
	cancel_datetime timestamp NULL,
	user_cancel_id varchar(30) NULL,
	active bool NOT NULL DEFAULT true,
	details jsonb NOT NULL,
        print_count varchar NOT NULL DEFAULT 0,
	CONSTRAINT t_medical_certificate_pk PRIMARY KEY (id),
	CONSTRAINT t_medical_certificate_fk FOREIGN KEY (f_medical_certificate_type_id) REFERENCES f_medical_certificate_type(id)
);

CREATE INDEX t_medical_certificate_hn_idx ON public.t_medical_certificate USING btree (hn);
CREATE UNIQUE INDEX t_medical_certificate_medical_certificate_no_idx ON public.t_medical_certificate USING btree (medical_certificate_no);
CREATE INDEX t_medical_certificate_vn_idx ON public.t_medical_certificate USING btree (vn);
CREATE INDEX t_medical_certificate_active_idx ON public.t_medical_certificate USING btree (active);

INSERT INTO public.b_sequence_data (b_sequence_data_id,sequence_data_description,sequence_data_pattern,sequence_data_value,sequence_data_active)
	VALUES ('mc','Medical Certificate Number','MCyy00000','1','1');
INSERT INTO public.b_sequence_data (b_sequence_data_id,sequence_data_description,sequence_data_pattern,sequence_data_value,sequence_data_active)
	VALUES ('mc_w','Medical Certificate Work Number','MCWyy00000','1','1');
INSERT INTO public.b_sequence_data (b_sequence_data_id,sequence_data_description,sequence_data_pattern,sequence_data_value,sequence_data_active)
	VALUES ('mc_a','Medical Certificate Alien Number','MCAyy00000','1','1');

insert into b_printing values ('10', 'ใบพิมพ์ใบรับรองความเจ็บป่วย', 'medical_certificate.jrxml');
insert into b_printing values ('11', 'ใบพิมพ์ใบรับรองสุขภาพ', 'medical_certificate_work.jrxml');
insert into b_printing values ('12', 'ใบพิมพ์ใบรับรองแพทย์ต่างด้าว', 'medical_certificate_alien.jrxml');

CREATE OR REPLACE FUNCTION public.latest_vitalsign(visit_id text)
 RETURNS TABLE(t_visit_id text, weight text, height text, bmi text, temp text, bp text, map text, spo2 text, pulse text, rr text)
 LANGUAGE sql
AS $function$
        select 
        visit_id as t_visit_id
		,(array_remove(string_to_array(string_agg(ds.visit_vital_sign_weight, ','), ','), ''))[1] as weight
		,(array_remove(string_to_array(string_agg(ds.visit_vital_sign_height, ','), ','), ''))[1] as height
		,(array_remove(string_to_array(string_agg(ds.visit_vital_sign_bmi, ','), ','), ''))[1] as bmi
		,(array_remove(string_to_array(string_agg(ds.visit_vital_sign_temperature, ','), ','), ''))[1] as temp
		,(array_remove(string_to_array(string_agg(ds.visit_vital_sign_blood_presure, ','), ','), ''))[1] as bp
		,(array_remove(string_to_array(string_agg(ds.visit_vital_sign_map, ','), ','), ''))[1] as map
		,(array_remove(string_to_array(string_agg(ds.visit_vital_sign_spo2, ','), ','), ''))[1] as spo2
		,(array_remove(string_to_array(string_agg(ds.visit_vital_sign_heart_rate, ','), ','), ''))[1] as pulse
		,(array_remove(string_to_array(string_agg(ds.visit_vital_sign_respiratory_rate, ','), ','), ''))[1] as rr
        from (
                select * 
                from t_visit_vital_sign
                where t_visit_id = visit_id
                and visit_vital_sign_active  = '1'
                order by text_to_timestamp( visit_vital_sign_check_date || ',' ||  visit_vital_sign_check_time ) desc
        ) as ds	

$function$
;

CREATE OR REPLACE VIEW public.visit_medical_certificate_view
AS select 
t_visit.t_visit_id as t_visit_id
, t_patient.patient_hn as hn
, t_visit.visit_vn as vn
, case when f_patient_prefix.f_patient_prefix_id = null then '' else f_patient_prefix.patient_prefix_description end ||
t_patient.patient_firstname || ' '  ||t_patient.patient_lastname as name
, case when t_person_foreigner.t_person_foreigner_id = null 
then t_patient.patient_firstname_eng || ' '  ||t_patient.patient_lastname_eng 
else t_person_foreigner.passport_fname || ' '  ||t_person_foreigner.passport_lname
end as name_eng
, t_patient.patient_pid as pid
, t_person_foreigner.passport_no as passport
, text_to_timestamp(t_patient.patient_birthday)::date as dob
, (case when t_patient.patient_house is null or t_patient.patient_house = '' then '' else 'เลขที่ ' || t_patient.patient_house end) || ' ' ||
(case when t_patient.patient_moo is null or t_patient.patient_moo = '' then '' else 'หมู่ที่ ' || t_patient.patient_moo end) || ' ' ||
(case when t_patient.patient_road is null or t_patient.patient_road = '' then '' else 'ถนน' || t_patient.patient_road end) || ' ' ||
(case when tambol.f_address_id is null then '' else 'ตำบล/แขวง ' || tambol.address_description end) || ' ' ||
(case when amphur.f_address_id is null then '' else 'อำเภอ/เขต ' || amphur.address_description end) || ' ' ||
(case when changwat.f_address_id is null then '' else 'จังหวัด' || changwat.address_description end) || ' ' ||
(case when t_patient.patient_postcode is null or t_patient.patient_postcode = '' then '' else 'รหัสไปรษณีย์ ' || t_patient.patient_postcode end) as address
, t_patient.patient_phone_number as phone
, t_patient.patient_patient_mobile_phone as mobile
, f_patient_nation.patient_nation_description as nationality
, f_patient_occupation.patient_occupation_description as occupation
, t_health_family.skin_color
, vs.weight
, vs.height
, vs.bp
, vs.pulse
, array_to_string(
 array(select t_patient_personal_disease.patient_personal_disease_description from t_patient_personal_disease where t_patient_personal_disease.t_patient_id = t_patient.t_patient_id)
, ',') as disease
, t_person_foreigner.employer_name
, (case when t_person_foreigner.employer_address_house is null or t_person_foreigner.employer_address_house = '' then '' else 'เลขที่ ' || t_person_foreigner.employer_address_house end) || ' ' ||
(case when t_person_foreigner.employer_address_moo is null or t_person_foreigner.employer_address_moo = '' then '' else 'หมู่ที่ ' || t_person_foreigner.employer_address_moo end) || ' ' ||
(case when t_person_foreigner.employer_address_road is null or t_person_foreigner.employer_address_road = '' then '' else 'ถนน' || t_person_foreigner.employer_address_road end) || ' ' ||
(case when employer_tambol.f_address_id is null then '' else 'ตำบล/แขวง ' || employer_tambol.address_description end) || ' ' ||
(case when employer_amphur.f_address_id is null then '' else 'อำเภอ/เขต ' || employer_amphur.address_description end) || ' ' ||
(case when employer_changwat.f_address_id is null then '' else 'จังหวัด' || employer_changwat.address_description end) || ' ' ||
(case when t_person_foreigner.employer_address_postcode = null or t_person_foreigner.employer_address_postcode = '' then '' else 'รหัสไปรษณีย์ ' || t_person_foreigner.employer_address_postcode end) as employer_address
, t_person_foreigner.employer_contact_phone_number as employer_phone
, t_person_foreigner.employer_contact_mobile_phone_number as employer_mobile

from t_visit
inner join t_patient on t_patient.t_patient_id = t_visit.t_patient_id
inner join t_health_family on t_health_family.t_health_family_id = t_patient.t_health_family_id
left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_patient.f_patient_prefix_id
left join f_address as tambol on tambol.f_address_id = t_patient.patient_tambon
left join f_address as amphur on amphur.f_address_id = t_patient.patient_amphur
left join f_address as changwat on changwat.f_address_id = t_patient.patient_changwat
left join f_patient_nation on f_patient_nation.f_patient_nation_id = t_patient.f_patient_nation_id
left join f_patient_occupation on f_patient_occupation.f_patient_occupation_id = t_patient.f_patient_occupation_id
left join t_person_foreigner on t_person_foreigner.t_person_id = t_patient.t_person_id
left join f_address as employer_tambol on employer_tambol.f_address_id = t_person_foreigner.employer_address_tambon
left join f_address as employer_amphur on employer_amphur.f_address_id = t_person_foreigner.employer_address_amphur
left join f_address as employer_changwat on employer_changwat.f_address_id = t_person_foreigner.employer_address_changwat
left join latest_vitalsign(t_visit.t_visit_id) as vs on vs.t_visit_id = t_visit.t_visit_id;

CREATE OR REPLACE FUNCTION public.timestamp_to_text(timestamp)
 RETURNS text
 LANGUAGE plpgsql
 IMMUTABLE
AS $function$
BEGIN
        return ((to_char($1,'YYYY')::int)+543)::text||to_char($1,'-MM-DD,HH24:MI:SS');
exception when others then
        return null;
END;$function$
;

-- update db version
INSERT INTO s_version VALUES ('9701000000090', '90', 'Hospital OS, Community Edition', '3.9.57', '3.37.0', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_57.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.57');
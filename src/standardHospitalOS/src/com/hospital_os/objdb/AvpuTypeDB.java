/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.AvpuType;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tanakrit
 */
public class AvpuTypeDB {

    public ConnectionInf theConnectionInf;

    public AvpuTypeDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public List<AvpuType> selectAll() throws Exception {
        String sql = "select * from f_avpu_type";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            return executeQuery(ePQuery);
        }
    }

    public AvpuType selectByPK(String pkId) throws Exception {
        String sql = "select * from f_avpu_type \n"
                + " where f_avpu_type_id = ?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, pkId);
            List<AvpuType> v = executeQuery(ePQuery);
            return (AvpuType) (v.isEmpty() ? null : v.get(0));
        }
    }

    public List<AvpuType> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<AvpuType> list = new ArrayList<>();
        try (ResultSet rs = preparedStatement.executeQuery()) {
            while (rs.next()) {
                AvpuType p = new AvpuType();
                p.setObjectId(rs.getString("f_avpu_type_id"));
                p.description = rs.getString("description");
                p.avpu_score = rs.getInt("avpu_score");
                list.add(p);
            }
            return list;
        }
    }
}

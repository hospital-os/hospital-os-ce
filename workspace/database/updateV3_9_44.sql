update f_gui_action set gui_action_name = 'กำหนด path รูปแบบอักษร' where f_gui_action_id = '1003';
update f_gui_action set gui_action_name = 'กำหนด path งานพิมพ์' where f_gui_action_id = '1004';

DROP TRIGGER update_result_xray_complete ON t_result_xray;

DROP FUNCTION update_result_xray_complete_datetime();

CREATE TABLE f_xray_result_summary
(
  f_xray_result_summary_id character varying(1) NOT NULL,
  description text NOT NULL,
  CONSTRAINT f_xray_result_summary_pkey PRIMARY KEY (f_xray_result_summary_id)
); 

insert into f_xray_result_summary (f_xray_result_summary_id, description)
values ('0', 'ผิดปกติ');
insert into f_xray_result_summary (f_xray_result_summary_id, description)
values ('1', 'ปกติ');
insert into f_xray_result_summary (f_xray_result_summary_id, description)
values ('9', 'ไม่ระบุ');

ALTER TABLE t_result_xray ADD COLUMN f_xray_result_summary_id character varying(1) NOT NUlL default '9';

-- PCU Module
alter table t_health_anc add column is_anc_place_other character varying(1) NOT NUlL default '0';
update t_health_anc set is_anc_place_other = '1' 
where 
health_anc_notice is not null and
health_anc_notice <> '' and
substring(health_anc_notice, 0, 5) = '0000';

alter table t_health_anc add column anc_place_hcode character varying(5) NOT NUlL default '00000';
update t_health_anc set anc_place_hcode = b_site.b_visit_office_id
from b_site
where 
health_anc_notice is  null or
health_anc_notice = '' or
substring(health_anc_notice, 0, 5) <> '0000';

update t_health_anc set health_anc_notice = substring(health_anc_notice, 5)
where 
health_anc_notice is not null and
health_anc_notice <> '' and
substring(health_anc_notice, 0, 5) = '0000';

-- update db version
INSERT INTO s_version VALUES ('9701000000077', '77', 'Hospital OS, Community Edition', '3.9.44', '3.27.291215', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_44.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.44');
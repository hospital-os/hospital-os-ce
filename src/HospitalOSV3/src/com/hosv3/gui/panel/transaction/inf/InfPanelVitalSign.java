/*
 * InfPanelObject.java
 *
 * Created on 8 ���Ҥ� 2549, 10:26 �.
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
package com.hosv3.gui.panel.transaction.inf;

import com.hospital_os.object.VitalSign;

/**
 *
 * @author Administrator
 */
public interface InfPanelVitalSign {

    public void setVitalSign(VitalSign vs);

    public VitalSign getVitalSign();

    public boolean saveVitalSign();
}

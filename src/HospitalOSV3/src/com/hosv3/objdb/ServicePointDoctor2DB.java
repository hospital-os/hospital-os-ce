package com.hosv3.objdb;

import com.hospital_os.objdb.ServicePointDoctorDB;
import com.hospital_os.object.ServicePointDoctor;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.util.Vector;

@SuppressWarnings("UseOfObsoleteCollectionType")
public class ServicePointDoctor2DB extends ServicePointDoctorDB {

    public ServicePointDoctor2DB(ConnectionInf db) {
        super(db);
    }

    public ServicePointDoctor selectByDoctor(String dc) throws Exception {
        String sql = "select * from " + dbObj.table
                + " where " + dbObj.doctor_id
                + " = '" + dc + "'";
        Vector vc = eQuery(sql);
        if (vc.isEmpty()) {
            return null;
        } else {
            return (ServicePointDoctor) vc.get(0);
        }
    }
}

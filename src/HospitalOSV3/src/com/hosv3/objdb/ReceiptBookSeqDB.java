/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.objdb;

import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.Gutil;
import com.hosv3.object.ReceiptBookSeq;
import java.sql.ResultSet;
import java.util.Vector;

@SuppressWarnings("UseOfObsoleteCollectionType")
public class ReceiptBookSeqDB {

    public ConnectionInf theConnectionInf;
    public ReceiptBookSeq dbObj;

    public ReceiptBookSeqDB(ConnectionInf db) {
        theConnectionInf = db;
        dbObj = new ReceiptBookSeq();
        initConfig();
    }

    private boolean initConfig() {
        dbObj.table = "t_billing_receipt_book_seq";
        dbObj.pk_field = "book_number";
        return true;
    }

    public int insert(ReceiptBookSeq p) throws Exception {
        String sql = "insert into " + dbObj.table + " ("
                + dbObj.pk_field
                + " , current_seq"
                + " ) values ("
                + p.book_number
                + "," + p.current_seq
                + ")";
        sql = Gutil.convertSQLToMySQL(sql, theConnectionInf.gettypeDatabase());
        return theConnectionInf.eUpdate(sql);
    }

    public int update(ReceiptBookSeq o) throws Exception {
        String sql = "update " + dbObj.table + " set ";
        String field = ""
                + "', current_seq =" + o.current_seq
                + " where " + dbObj.pk_field + "=" + o.book_number + "";
        sql = Gutil.convertSQLToMySQL(sql + field.substring(2), theConnectionInf.gettypeDatabase());
        return theConnectionInf.eUpdate(sql);
    }

    public int delete(ReceiptBookSeq of) throws Exception {
        String sql = "delete from " + dbObj.table
                + " where " + dbObj.pk_field
                + " = " + of.book_number + "";
        return theConnectionInf.eUpdate(sql);
    }

    public Vector selectAll() throws Exception {
        String sql = "select * from " + dbObj.table;
        return eQuery(sql);
    }

    public ReceiptBookSeq selectByBookNumber(int bookno) throws Exception {
        String sql = "select * from " + dbObj.table + " where " + dbObj.pk_field + " = " + bookno;
        Vector eQuery = eQuery(sql);
        return (ReceiptBookSeq) (eQuery.isEmpty() ? null : eQuery.get(0));
    }

    public Vector eQuery(String sql) throws Exception {
        ReceiptBookSeq p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            p = new ReceiptBookSeq();
            p.setObjectId(String.valueOf(rs.getInt(dbObj.pk_field)));
            p.book_number = rs.getInt(dbObj.pk_field);
            p.current_seq = rs.getInt("current_seq");
            p.update_ts = rs.getDate("update_ts");
            list.add(p);
        }
        rs.close();
        return list;
    }
}
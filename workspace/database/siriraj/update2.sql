ALTER TABLE s_siriraj_version ADD CONSTRAINT s_siriraj_version_pkey PRIMARY KEY (s_siriraj_version_id);

CREATE TABLE b_map_group_code
(
  b_map_group_code_id character varying(255) NOT NULL,
  b_item_id character varying(255) NOT NULL,
  f_group_code_id character varying(255),
  CONSTRAINT b_map_group_code_pkey PRIMARY KEY (b_map_group_code_id),
  CONSTRAINT b_map_group_code_unique1 UNIQUE (b_item_id)
);

CREATE TABLE b_siriraj_medical_bill
(
  b_siriraj_medical_bill_id character varying(255) NOT NULL,
  ip_address character varying(15) NOT NULL,
  format character varying(255) NOT NULL,
  bookno integer NOT NULL,
  begin_no integer NOT NULL,
  end_no integer NOT NULL,
  sequence_value integer NOT NULL,
  active character varying(1) NOT NULL,
  CONSTRAINT b_siriraj_medical_bill_pkey PRIMARY KEY (b_siriraj_medical_bill_id)
);

CREATE TABLE b_siriraj_direct_draw_map_plan (
  b_siriraj_direct_draw_map_plan_id	varchar(255) NOT NULL,
  b_contract_plans_id               	varchar(255) NOT NULL,
  CONSTRAINT b_siriraj_direct_draw_map_plan_pkey PRIMARY KEY (b_siriraj_direct_draw_map_plan_id),
  CONSTRAINT b_siriraj_direct_draw_map_plan_unique1 UNIQUE (b_contract_plans_id)
);

CREATE TABLE t_siriraj_medical_bill(
  t_siriraj_medical_bill_id character varying(255) NOT NULL,
  medical_bill_no character varying(255) NOT NULL,
  bookno integer NOT NULL,
  "number" integer NOT NULL,
  medical_bill_datetime character varying(255) NOT NULL,
  medical_bill_count integer NOT NULL,
  t_billing_invoice_id character varying(255) NOT NULL,
  active character varying(1) NOT NULL,
  staff_record character varying(255) NOT NULL,
  record_datetime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  staff_update character varying(255) NOT NULL,
  update_datetime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  CONSTRAINT t_siriraj_medical_bill_pkey PRIMARY KEY (t_siriraj_medical_bill_id)
);

CREATE TABLE f_group_code (
  f_group_code_id	varchar(255) NULL,
  group_name    	varchar(255) NULL,
  CONSTRAINT f_group_code_pkey PRIMARY KEY (f_group_code_id)
);

INSERT INTO f_group_code VALUES ('T001','(�ԡ��)��Ἱ�»�������� 1');
INSERT INTO f_group_code VALUES ('T011','(�ԡ�����)��Ἱ�»�������� 1');
INSERT INTO f_group_code VALUES ('T002','(�ԡ��)��Ἱ�»�������� 2');
INSERT INTO f_group_code VALUES ('T012','(�ԡ�����)��Ἱ�»�������� 2');
INSERT INTO f_group_code VALUES ('T003','(�ԡ��)��Ἱ�»�������� 3');
INSERT INTO f_group_code VALUES ('T013','(�ԡ�����)��Ἱ�»�������� 3');
INSERT INTO f_group_code VALUES ('T004','(�ԡ��)��Ἱ�»�������� 4');
INSERT INTO f_group_code VALUES ('T014','(�ԡ�����)��Ἱ�»�������� 4');
INSERT INTO f_group_code VALUES ('T021','(�ԡ��)��õ�Ǩ �ԹԨ��� �����觡���ѡ�Ҵ�ҹ���ᾷ��Ἱ��');
INSERT INTO f_group_code VALUES ('T041','(�ԡ�����)��õ�Ǩ �ԹԨ��� �����觡���ѡ�Ҵ�ҹ���ᾷ��Ἱ��');
INSERT INTO f_group_code VALUES ('T022','(�ԡ��)��ùǴ��Ẻ�Ҫ�ӹѡ�������Ф���ع�����͡�úӺѴ�ѡ���ä �ҷ� �ä����Դ�����Դ���Ԣͧ��������� ������ ��д١ ��͵�� ���ʹ�� (45-50 �ҷ�)');
INSERT INTO f_group_code VALUES ('T042','(�ԡ�����)��ùǴ��Ẻ�Ҫ�ӹѡ�������Ф���ع�����͡�úӺѴ�ѡ���ä �ҷ� �ä����Դ�����Դ���Ԣͧ��������� ������ ��д١ ��͵�� ���ʹ�� (45-50 �ҷ�)');
INSERT INTO f_group_code VALUES ('T023','(�ԡ��)��ùǴ��Ẻ�Ҫ�ӹѡ�������Ф���ع�����͡�úӺѴ�ѡ���ä �ҷ� �ä����Դ�����Դ���Ԣͧ��������� ������ ��д١ ��͵�� ���ʹ�� (100-110 �ҷ�)');
INSERT INTO f_group_code VALUES ('T043','(�ԡ�����)��ùǴ��Ẻ�Ҫ�ӹѡ�������Ф���ع�����͡�úӺѴ�ѡ���ä �ҷ� �ä����Դ�����Դ���Ԣͧ��������� ������ ��д١ ��͵�� ���ʹ�� (100-110 �ҷ�)');
INSERT INTO f_group_code VALUES ('T024','(�ԡ��)��ùǴ��Ẻ�Ҫ�ӹѡ�������Ф���ع�����͡�ÿ�鹿����ö�Ҿ �ҷ� �ä����ġ�� �ä����ҵ �ä�ѹ�Ժҵ (45-50 �ҷ�)');
INSERT INTO f_group_code VALUES ('T044','(�ԡ�����)��ùǴ��Ẻ�Ҫ�ӹѡ�������Ф���ع�����͡�ÿ�鹿����ö�Ҿ �ҷ� �ä����ġ�� �ä����ҵ �ä�ѹ�Ժҵ (45-50 �ҷ�)');
INSERT INTO f_group_code VALUES ('T025','(�ԡ��)��ùǴ��Ẻ�Ҫ�ӹѡ�������Ф���ع�����͡�ÿ�鹿����ö�Ҿ �ҷ� �ä����ġ�� �ä����ҵ �ä�ѹ�Ժҵ (100-110 �ҷ�)');
INSERT INTO f_group_code VALUES ('T045','(�ԡ�����)��ùǴ��Ẻ�Ҫ�ӹѡ�������Ф���ع�����͡�ÿ�鹿����ö�Ҿ �ҷ� �ä����ġ�� �ä����ҵ �ä�ѹ�Ժҵ (100-110 �ҷ�)');
INSERT INTO f_group_code VALUES ('T026','(�ԡ��)���ͺ�͹����ع�����͡�úӺѴ�ѡ���ä���/���Ϳ�鹿��ä�ͺ�״ ����������');
INSERT INTO f_group_code VALUES ('T046','(�ԡ�����)���ͺ�͹����ع�����͡�úӺѴ�ѡ���ä���/���Ϳ�鹿��ä�ͺ�״ ����������');
INSERT INTO f_group_code VALUES ('T027','(�ԡ��)��ùǴ��Ẻ�Ҫ�ӹѡ���͡�úӺѴ�ѡ���ä �ҷ� �ä����Դ�����Դ���Ԣͧ��������� ������ ��д١ ��͵�� ���ʹ��');
INSERT INTO f_group_code VALUES ('T047','(�ԡ�����)��ùǴ��Ẻ�Ҫ�ӹѡ���͡�úӺѴ�ѡ���ä �ҷ� �ä����Դ�����Դ���Ԣͧ��������� ������ ��д١ ��͵�� ���ʹ��');
INSERT INTO f_group_code VALUES ('T028','(�ԡ��)��ùǴ��Ẻ�Ҫ�ӹѡ���͡�ÿ�鹿����ö�Ҿ �ҷ� �ä����ġ�� �ä����ҵ �ä�ѹ�Ժҵ');
INSERT INTO f_group_code VALUES ('T048','(�ԡ�����)��ùǴ��Ẻ�Ҫ�ӹѡ���͡�ÿ�鹿����ö�Ҿ �ҷ� �ä����ġ�� �ä����ҵ �ä�ѹ�Ժҵ');
INSERT INTO f_group_code VALUES ('T029','(�ԡ��)��û�Ф���ع�����͡�úӺѴ�ѡ���ä �ҷ� �ä����Դ�����Դ���Ԣͧ��������� ������ ��д١ ��͵�� ���ʹ�� �ä����ġ�� �ä����ҵ �ä�ѹ�Ժҵ (����ա�ùǴ) (25-30 �ҷ�)');
INSERT INTO f_group_code VALUES ('T049','(�ԡ�����)��û�Ф���ع�����͡�úӺѴ�ѡ���ä �ҷ� �ä����Դ�����Դ���Ԣͧ��������� ������ ��д١ ��͵�� ���ʹ�� �ä����ġ�� �ä����ҵ �ä�ѹ�Ժҵ (����ա�ùǴ) (25-30 �ҷ�)');
INSERT INTO f_group_code VALUES ('T030','(�ԡ��)��û�Ф���ع�����͡�úӺѴ�ѡ���ä �ҷ� �ä����Դ�����Դ���Ԣͧ��������� ������ ��д١ ��͵�� ���ʹ�� �ä����ġ�� �ä����ҵ �ä�ѹ�Ժҵ (����ա�ùǴ) (45-50 �ҷ�)');
INSERT INTO f_group_code VALUES ('T050','(�ԡ�����)��û�Ф���ع�����͡�úӺѴ�ѡ���ä �ҷ� �ä����Դ�����Դ���Ԣͧ��������� ������ ��д١ ��͵�� ���ʹ�� �ä����ġ�� �ä����ҵ �ä�ѹ�Ժҵ (����ա�ùǴ) (45-50 �ҷ�)');
INSERT INTO f_group_code VALUES ('T061','(�ԡ�����)��ô���˭ԧ��ѧ��ʹ (�Ѻ�������� �Ǵ��Ẻ�Ҫ�ӹѡ���ͺ�͹����ع�� (��¤���)');
INSERT INTO f_group_code VALUES ('T062','(�ԡ�����)��ô���˭ԧ��ѧ��ʹ (�Ѻ�������� �Ǵ��Ẻ�Ҫ�ӹѡ���ͺ�͹����ع�� (�Ѵ�ش)');

INSERT INTO s_siriraj_version VALUES ('9750000000002', '2', 'Siriraj, Community Edition', '2.4.310811', '1.1.310811', to_char(current_timestamp, 'YYYY-MM-DD HH24:MI:SS'));

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.Address;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class AddressDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "836";

    public AddressDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public int insert(Address obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "INSERT INTO t_address (\n"
                    + "            t_address_id, f_address_housetype_id, \n"
                    + "            organization_name, house_no, building, floor, room_no,\n"
                    + "            village_no, village, soi, sub_soi, road, sub_district_id,\n"
                    + "            district_id, province_id, postal_code, country_id,\n"
                    + "            is_use_location, latitude, longitude, address1, address2,\n"
                    + "            city, state,address_en,\n"
                    + "            active, user_record_id, user_update_id)\n"
                    + "    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, obj.getGenID(tableId));
            preparedStatement.setString(index++, obj.f_address_housetype_id);
            preparedStatement.setString(index++, obj.organization_name);
            preparedStatement.setString(index++, obj.house_no);
            preparedStatement.setString(index++, obj.building);
            preparedStatement.setString(index++, obj.floor);
            preparedStatement.setString(index++, obj.room_no);
            preparedStatement.setString(index++, obj.village_no);
            preparedStatement.setString(index++, obj.village);
            preparedStatement.setString(index++, obj.soi);
            preparedStatement.setString(index++, obj.sub_soi);
            preparedStatement.setString(index++, obj.road);
            preparedStatement.setString(index++, obj.sub_district_id);
            preparedStatement.setString(index++, obj.district_id);
            preparedStatement.setString(index++, obj.province_id);
            preparedStatement.setString(index++, obj.postal_code);
            preparedStatement.setString(index++, obj.country_id);
            preparedStatement.setBoolean(index++, obj.is_use_location);
            if (obj.is_use_location) {
                preparedStatement.setDouble(index++, obj.latitude);
                preparedStatement.setDouble(index++, obj.longitude);
            } else {
                preparedStatement.setNull(index++, java.sql.Types.DOUBLE);
                preparedStatement.setNull(index++, java.sql.Types.DOUBLE);
            }
            preparedStatement.setString(index++, obj.address1);
            preparedStatement.setString(index++, obj.address2);
            preparedStatement.setString(index++, obj.city);
            preparedStatement.setString(index++, obj.state);
            preparedStatement.setString(index++, obj.address_en);
            preparedStatement.setString(index++, obj.active);
            preparedStatement.setString(index++, obj.user_record_id);
            preparedStatement.setString(index++, obj.user_update_id);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            obj.setObjectId(null);
            throw ex;
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(Address obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "UPDATE t_address\n"
                    + "      SET f_address_housetype_id=?, \n"
                    + "          organization_name=?, house_no=?, building=?, \n"
                    + "          floor=?, room_no=?, village_no=?, village=?,  \n"
                    + "          soi=?, sub_soi=?, road=?, sub_district_id=?, district_id=?, province_id=?,  \n"
                    + "          postal_code=?, country_id=?, is_use_location=?, latitude=?, longitude=?,  \n"
                    + "          address1=?, address2=?, city=?, state=?, address_en=?,  \n"
                    + "          active=?, user_update_id=?, update_date_time= current_timestamp\n"
                    + "    WHERE t_address_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, obj.f_address_housetype_id);
            preparedStatement.setString(index++, obj.organization_name);
            preparedStatement.setString(index++, obj.house_no);
            preparedStatement.setString(index++, obj.building);
            preparedStatement.setString(index++, obj.floor);
            preparedStatement.setString(index++, obj.room_no);
            preparedStatement.setString(index++, obj.village_no);
            preparedStatement.setString(index++, obj.village);
            preparedStatement.setString(index++, obj.soi);
            preparedStatement.setString(index++, obj.sub_soi);
            preparedStatement.setString(index++, obj.road);
            preparedStatement.setString(index++, obj.sub_district_id);
            preparedStatement.setString(index++, obj.district_id);
            preparedStatement.setString(index++, obj.province_id);
            preparedStatement.setString(index++, obj.postal_code);
            preparedStatement.setString(index++, obj.country_id);
            preparedStatement.setBoolean(index++, obj.is_use_location);
            if (obj.is_use_location) {
                preparedStatement.setDouble(index++, obj.latitude);
                preparedStatement.setDouble(index++, obj.longitude);
            } else {
                preparedStatement.setNull(index++, java.sql.Types.DOUBLE);
                preparedStatement.setNull(index++, java.sql.Types.DOUBLE);
            }
            preparedStatement.setString(index++, obj.address1);
            preparedStatement.setString(index++, obj.address2);
            preparedStatement.setString(index++, obj.city);
            preparedStatement.setString(index++, obj.state);
            preparedStatement.setString(index++, obj.address_en);
            preparedStatement.setString(index++, obj.active);
            preparedStatement.setString(index++, obj.user_update_id);
            preparedStatement.setString(index++, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int inactive(Address obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {

            String sql = "UPDATE t_address\n"
                    + "   SET active=?, user_update_id=?, update_date_time=current_timestamp\n"
                    + " WHERE t_address_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, "0");
            preparedStatement.setString(index++, obj.user_update_id);
            preparedStatement.setString(index++, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "DELETE FROM t_address WHERE t_address_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, id);
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public Address select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select t_address.* \n"
                    + ", case when  f_address_housetype.f_address_housetype_id is null then '' else f_address_housetype.address_housetype_description end as housetype\n"
                    + ", case when tambol.f_address_id is null then '' else tambol.address_description end as sub_district\n"
                    + ", case when amphur.f_address_id is null then '' else amphur.address_description end as district\n"
                    + ", case when changwat.f_address_id is null then '' else changwat.address_description end as provice\n"
                    + ", case when f_country.f_country_id is null then '' else f_country.common_name_th end as country\n"
                    + "from t_address\n"
                    + "left join f_address_housetype on f_address_housetype.f_address_housetype_id = t_address.f_address_housetype_id\n"
                    + "left join f_address as tambol on tambol.f_address_id = t_address.sub_district_id\n"
                    + "left join f_address as amphur on amphur.f_address_id = t_address.district_id\n"
                    + "left join f_address as changwat on changwat.f_address_id = t_address.province_id\n"
                    + "left join f_country on f_country.f_country_id = t_address.country_id\n"
                    + "where t_address_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<Address> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public Address selectByHouseNoVillNoSubDisNo(String houseNo, String villNo, String subDisId) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from t_address where house_no = ? and village_no = ? and district_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, houseNo);
            preparedStatement.setString(1, villNo);
            preparedStatement.setString(1, subDisId);
            List<Address> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<Address> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<Address> list = new ArrayList<>();
        try (ResultSet rs = preparedStatement.executeQuery()) {
            while (rs.next()) {
                Address obj = new Address();
                obj.setObjectId(rs.getString("t_address_id"));
                obj.f_address_housetype_id = rs.getString("f_address_housetype_id");
                obj.organization_name = rs.getString("organization_name");
                obj.house_no = rs.getString("house_no");
                obj.building = rs.getString("building");
                obj.floor = rs.getString("floor");
                obj.room_no = rs.getString("room_no");
                obj.village_no = rs.getString("village_no");
                obj.village = rs.getString("village");
                obj.soi = rs.getString("soi");
                obj.sub_soi = rs.getString("sub_soi");
                obj.road = rs.getString("road");
                obj.sub_district_id = rs.getString("sub_district_id");
                obj.district_id = rs.getString("district_id");
                obj.province_id = rs.getString("province_id");
                obj.postal_code = rs.getString("postal_code");
                obj.country_id = rs.getString("country_id");
                obj.is_use_location = rs.getBoolean("is_use_location");
                obj.latitude = rs.getDouble("latitude");
                obj.longitude = rs.getDouble("longitude");
                obj.address1 = rs.getString("address1");
                obj.address2 = rs.getString("address2");
                obj.city = rs.getString("city");
                obj.state = rs.getString("state");
                obj.address_en = rs.getString("address_en");
                obj.active = rs.getString("active");
                obj.user_record_id = rs.getString("user_record_id");
                obj.record_date_time = rs.getTimestamp("record_date_time");
                obj.user_update_id = rs.getString("user_update_id");
                obj.update_date_time = rs.getTimestamp("update_date_time");
                obj.user_cancel_id = rs.getString("user_cancel_id");
                obj.cancel_date_time = rs.getTimestamp("cancel_date_time");
                try {
                    obj.houseTypeName = rs.getString("housetype");
                } catch (SQLException ex) {
                }
                try {
                    obj.subDistrictName = rs.getString("sub_district");
                } catch (SQLException ex) {
                }
                try {
                    obj.districtName = rs.getString("district");
                } catch (SQLException ex) {
                }
                try {
                    obj.provinceName = rs.getString("provice");
                } catch (SQLException ex) {
                }
                try {
                    obj.countryName = rs.getString("country");
                } catch (SQLException ex) {
                }
                list.add(obj);
            }
            return list;
        }
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.AllergyType;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.ComboFix;
import java.sql.ResultSet;
import java.util.Vector;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class AllergyTypeDB {

    private final ConnectionInf theConnectionInf;

    public AllergyTypeDB(ConnectionInf db) {
        theConnectionInf = db;
    }
    public AllergyType selectById(String id) throws Exception {
        String sql = "select * from f_allergy_type where f_allergy_type_id = '%s'";
        Vector eQuery = eQuery(String.format(sql, id));
        return (AllergyType) (eQuery.isEmpty() ? null : eQuery.get(0));
    }
    
    public Vector<AllergyType> listAll() throws Exception {
        String sql = "select * from f_allergy_type";
        Vector eQuery = eQuery(sql);
        return eQuery;
    }

    public Vector eQuery(String sql) throws Exception {
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        while (rs.next()) {
            AllergyType p = new AllergyType();
            p.setObjectId(rs.getString("f_allergy_type_id"));
            p.description = rs.getString("allergy_type_description");
            list.add(p);
        }
        rs.close();
        return list;
    }

    public Vector<ComboFix> getComboboxDatasources() throws Exception {
        Vector<ComboFix> list = new Vector<ComboFix>();
        Vector<AllergyType> listAll = listAll();
        for (AllergyType obj : listAll) {
            list.add(new ComboFix(obj.getObjectId(), obj.description));
        }
        return list;
    }
}

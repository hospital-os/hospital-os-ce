/*
 * Guide.java
 *
 * Created on 4 �ԧ�Ҥ� 2549, 16:39 �.
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;
import java.util.Vector;

/**
 *
 * @author sumo
 */
@SuppressWarnings("UseOfObsoleteCollectionType") 
public class Guide extends Persistent {

    public String number = "";
    public String description = "";
    public String active = "1";

    /** Creates a new instance of Guide */
    public Guide() {
    }

    @Override
    public String toString() {
        return description;
    }

    public static String toString(Vector vGuide) {
//        Constant.println("vGuide==null" + vGuide == null);
        if (vGuide == null) {
            return "";
        }
//        Constant.println("vGuide.size()" + vGuide.size());
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < vGuide.size(); i++) {
            Guide guide = (Guide) vGuide.get(i);
            sb.append(guide.toString());
            sb.append(" ");
        }
        return sb.toString();
    }
}

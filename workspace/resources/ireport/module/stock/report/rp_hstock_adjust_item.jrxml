<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="รายงานการปรับยอดยาและเวชภัณฑ์" language="groovy" pageWidth="842" pageHeight="595" orientation="Landscape" whenNoDataType="NoDataSection" columnWidth="802" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="8fe1a022-0357-4ed8-a81f-efc255491623">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<property name="net.sf.jasperreports.export.xls.exclude.origin.band.1" value="pageHeader"/>
	<property name="net.sf.jasperreports.export.xls.exclude.origin.band.2" value="pageFooter"/>
	<property name="net.sf.jasperreports.export.xls.exclude.origin.band.3" value="lastPageFooter"/>
	<property name="net.sf.jasperreports.export.xls.ignore.cell.background" value="true"/>
	<property name="net.sf.jasperreports.export.xls.white.page.background" value="false"/>
	<property name="net.sf.jasperreports.export.xls.ignore.graphics" value="true"/>
	<property name="net.sf.jasperreports.export.xls.collapse.row.span" value="false"/>
	<property name="net.sf.jasperreports.export.xls.wrap.text" value="true"/>
	<property name="net.sf.jasperreports.export.xls.column.width.ratio" value="1.10f"/>
	<property name="net.sf.jasperreports.export.xls.auto.fit.column" value="true"/>
	<property name="net.sf.jasperreports.export.xls.detect.cell.type" value="true"/>
	<property name="net.sf.jasperreports.export.xls.remove.empty.space.between.rows" value="true"/>
	<property name="net.sf.jasperreports.export.xls.remove.empty.space.between.columns" value="true"/>
	<property name="net.sf.jasperreports.export.xls.ignore.page.margins" value="true"/>
	<property name="net.sf.jasperreports.export.xls.one.page.per.sheet" value="false"/>
	<property name="net.sf.jasperreports.export.xls.exclude.origin.keep.first.band.1" value="columnHeader"/>
	<property name="net.sf.jasperreports.export.xls.sheet.names.first" value="รายงานการปรับยอดยาและเวชภัณฑ์"/>
	<parameter name="start_date_time" class="java.sql.Timestamp">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="end_date_time" class="java.sql.Timestamp">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="adjust_ids" class="java.util.Collection">
		<defaultValueExpression><![CDATA[[3,5,7,8,9,10]]]></defaultValueExpression>
	</parameter>
	<parameter name="hstock_id" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[select
        b_hstock.hstock_code
        ,b_hstock.hstock_name
        ,b_item.item_common_name
        ,b_item_drug_uom.item_drug_uom_description as drug_uom
        ,t_hstock_mgnt.lot_number
        ,t_hstock_card.update_datetime as card_update_datetime
        ,f_hstock_adjust_type.adjust_type
        ,t_hstock_card.previous_qty_lot
        ,t_hstock_card.qty
        ,t_hstock_card.update_qty_lot
        ,(t_hstock_card.qty* t_hstock_card.small_unit_cost) as unit_cost

        ,f_patient_prefix.patient_prefix_description||t_person.person_firstname||'  '||t_person.person_lastname as user_update
        ,t_hstock_card.f_hstock_adjust_type_id
        ,t_hstock_card.expire_date_adjust
        ,t_hstock_card.reason_adjust


from t_hstock_card inner join t_hstock_mgnt on t_hstock_card.t_hstock_mgnt_id = t_hstock_mgnt.t_hstock_mgnt_id
        inner join b_hstock_item on b_hstock_item.b_hstock_item_id = t_hstock_mgnt.b_hstock_item_id
	and b_hstock_item.active ='1'
        inner join b_hstock on b_hstock_item.b_hstock_id = b_hstock.b_hstock_id
        inner join b_item on b_hstock_item.b_item_id = b_item.b_item_id
        left join b_item_drug_uom on t_hstock_mgnt.b_item_drug_uom_small_id = b_item_drug_uom.b_item_drug_uom_id
        inner join f_hstock_adjust_type on t_hstock_card.f_hstock_adjust_type_id = f_hstock_adjust_type.f_hstock_adjust_type_id

        left join b_employee on t_hstock_card.user_update_id = b_employee.b_employee_id
        left join t_person on b_employee.t_person_id = t_person.t_person_id
        left join f_patient_prefix on t_person.f_prefix_id = f_patient_prefix.f_patient_prefix_id

where
         t_hstock_card.f_hstock_adjust_type_id in ('3','5','7','8','9','10')
        and t_hstock_card.update_datetime between $P{start_date_time} and $P{end_date_time}
      	and $X{IN,t_hstock_card.f_hstock_adjust_type_id ,adjust_ids}
	    and b_hstock_item.b_hstock_id = $P{hstock_id}

order by
        b_item.item_common_name asc
        ,card_update_datetime asc]]>
	</queryString>
	<field name="hstock_code" class="java.lang.String"/>
	<field name="hstock_name" class="java.lang.String"/>
	<field name="item_common_name" class="java.lang.String"/>
	<field name="drug_uom" class="java.lang.String"/>
	<field name="lot_number" class="java.lang.String"/>
	<field name="card_update_datetime" class="java.sql.Timestamp"/>
	<field name="adjust_type" class="java.lang.String"/>
	<field name="previous_qty_lot" class="java.lang.Number"/>
	<field name="qty" class="java.lang.Number"/>
	<field name="update_qty_lot" class="java.lang.Number"/>
	<field name="unit_cost" class="java.lang.Double"/>
	<field name="user_update" class="java.lang.String"/>
	<field name="f_hstock_adjust_type_id" class="java.lang.String"/>
	<field name="expire_date_adjust" class="java.sql.Date"/>
	<field name="reason_adjust" class="java.lang.String"/>
	<group name="detail" keepTogether="true">
		<groupHeader>
			<band/>
		</groupHeader>
		<groupFooter>
			<band/>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<pageHeader>
		<band height="98" splitType="Stretch">
			<textField>
				<reportElement uuid="0200b55e-35d7-45a3-ab55-a6e003f407b3" x="55" y="50" width="160" height="20"/>
				<textElement verticalAlignment="Top">
					<font fontName="TH SarabunPSK" size="16" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA[new java.text.SimpleDateFormat("d MMMM yyyy เวลา HH:mm", new java.util.Locale("th", "TH")).format($P{start_date_time})
+ " น."]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement uuid="45a626ff-66ce-4c8e-bc8d-cef4444e04fb" x="0" y="0" width="802" height="30"/>
				<textElement textAlignment="Center">
					<font fontName="TH SarabunPSK" size="22" isBold="true"/>
				</textElement>
				<text><![CDATA[รายงานการปรับยอดยาและเวชภัณฑ์]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="1d81e9cc-ea4f-4306-aa8d-aa861b6d152b" x="0" y="70" width="90" height="20"/>
				<box>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Top">
					<font fontName="TH SarabunPSK" size="16" isBold="true"/>
				</textElement>
				<text><![CDATA[เหตุผลที่ปรับยอด :]]></text>
			</staticText>
			<textField>
				<reportElement uuid="01c3540f-07e4-4fa1-b525-605c63a5a9c3" x="417" y="70" width="20" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font size="14"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{adjust_ids}.contains("8") ?  "" : ""]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement uuid="1d81e9cc-ea4f-4306-aa8d-aa861b6d152b" x="0" y="50" width="55" height="20"/>
				<box>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Top">
					<font fontName="TH SarabunPSK" size="16" isBold="true"/>
				</textElement>
				<text><![CDATA[ตั้งแต่วันที่ :]]></text>
			</staticText>
			<textField>
				<reportElement uuid="0200b55e-35d7-45a3-ab55-a6e003f407b3" x="33" y="30" width="522" height="20"/>
				<textElement verticalAlignment="Top">
					<font fontName="TH SarabunPSK" size="16" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{hstock_code}+" "+$F{hstock_name}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="01c3540f-07e4-4fa1-b525-605c63a5a9c3" x="186" y="70" width="20" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font size="14"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{adjust_ids}.contains("5") ?  "" : ""]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement uuid="1d81e9cc-ea4f-4306-aa8d-aa861b6d152b" x="206" y="70" width="101" height="20"/>
				<box>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Top">
					<font fontName="TH SarabunPSK" size="16" isBold="true"/>
				</textElement>
				<text><![CDATA[ปรับเพิ่มเหตุผลอื่น ๆ
]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="1d81e9cc-ea4f-4306-aa8d-aa861b6d152b" x="525" y="70" width="43" height="20"/>
				<box>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Top">
					<font fontName="TH SarabunPSK" size="16" isBold="true"/>
				</textElement>
				<text><![CDATA[หมดอายุ]]></text>
			</staticText>
			<textField>
				<reportElement uuid="01c3540f-07e4-4fa1-b525-605c63a5a9c3" x="91" y="70" width="20" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font size="14"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{adjust_ids}.contains("3") ?  "" : ""]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement uuid="1d81e9cc-ea4f-4306-aa8d-aa861b6d152b" x="437" y="70" width="43" height="20"/>
				<box>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Top">
					<font fontName="TH SarabunPSK" size="16" isBold="true"/>
				</textElement>
				<text><![CDATA[สูญหาย]]></text>
			</staticText>
			<textField>
				<reportElement uuid="01c3540f-07e4-4fa1-b525-605c63a5a9c3" x="608" y="70" width="20" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font size="14"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{adjust_ids}.contains("10") ?  "" : ""]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="0200b55e-35d7-45a3-ab55-a6e003f407b3" x="270" y="50" width="160" height="20"/>
				<textElement verticalAlignment="Top">
					<font fontName="TH SarabunPSK" size="16" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA[new java.text.SimpleDateFormat("d MMMM yyyy เวลา HH:mm", new java.util.Locale("th", "TH")).format($P{end_date_time})
+ " น."]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement uuid="1d81e9cc-ea4f-4306-aa8d-aa861b6d152b" x="111" y="70" width="59" height="20"/>
				<box>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Top">
					<font fontName="TH SarabunPSK" size="16" isBold="true"/>
				</textElement>
				<text><![CDATA[ปรับเพิ่ม
]]></text>
			</staticText>
			<textField>
				<reportElement uuid="01c3540f-07e4-4fa1-b525-605c63a5a9c3" x="505" y="70" width="20" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font size="14"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{adjust_ids}.contains("9") ?  "" : ""]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement uuid="1d81e9cc-ea4f-4306-aa8d-aa861b6d152b" x="218" y="50" width="52" height="20"/>
				<box>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Top">
					<font fontName="TH SarabunPSK" size="16" isBold="true"/>
				</textElement>
				<text><![CDATA[ถึงวันที่ :]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="1d81e9cc-ea4f-4306-aa8d-aa861b6d152b" x="628" y="70" width="97" height="20"/>
				<box>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Top">
					<font fontName="TH SarabunPSK" size="16" isBold="true"/>
				</textElement>
				<text><![CDATA[ปรับลดเหตุผลอื่น ๆ]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="1d81e9cc-ea4f-4306-aa8d-aa861b6d152b" x="0" y="30" width="33" height="20"/>
				<box>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Top">
					<font fontName="TH SarabunPSK" size="16" isBold="true"/>
				</textElement>
				<text><![CDATA[คลัง :]]></text>
			</staticText>
			<textField>
				<reportElement uuid="01c3540f-07e4-4fa1-b525-605c63a5a9c3" x="328" y="70" width="20" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font size="14"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{adjust_ids}.contains("7") ?  "" : ""]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement uuid="1d81e9cc-ea4f-4306-aa8d-aa861b6d152b" x="348" y="70" width="43" height="20"/>
				<box>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Top">
					<font fontName="TH SarabunPSK" size="16" isBold="true"/>
				</textElement>
				<text><![CDATA[เสียหาย]]></text>
			</staticText>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="18" splitType="Stretch">
			<staticText>
				<reportElement uuid="ab4ad71e-3af9-40b7-9238-36337f3c3a42" x="0" y="0" width="30" height="18"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center">
					<font fontName="TH SarabunPSK" size="14" isBold="true"/>
				</textElement>
				<text><![CDATA[ลำดับ]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="00ea863a-0971-4908-b5d1-f6f0a2d608a4" x="30" y="0" width="150" height="18"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center">
					<font fontName="TH SarabunPSK" size="14" isBold="true"/>
				</textElement>
				<text><![CDATA[รายการ]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="475835b2-e16e-470d-90d6-4f50304c5cf8" x="180" y="0" width="45" height="18"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center">
					<font fontName="TH SarabunPSK" size="14" isBold="true"/>
				</textElement>
				<text><![CDATA[หน่วย]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="ee786a61-307e-4683-881b-e897a9bfacca" x="225" y="0" width="60" height="18"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center">
					<font fontName="TH SarabunPSK" size="14" isBold="true"/>
				</textElement>
				<text><![CDATA[Lot No.]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="e72166b0-ee94-4b7f-891f-248b8d746f26" x="285" y="0" width="60" height="18"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center">
					<font fontName="TH SarabunPSK" size="14" isBold="true"/>
				</textElement>
				<text><![CDATA[วันที่ปรับยอด]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="cb398063-d4a4-4163-b809-7da14b4cd6dd" x="345" y="0" width="85" height="18"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center">
					<font fontName="TH SarabunPSK" size="14" isBold="true"/>
				</textElement>
				<text><![CDATA[เหตุผลที่ปรับ]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="b652277c-809a-47ef-9408-c322151f5393" x="430" y="0" width="60" height="18"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center">
					<font fontName="TH SarabunPSK" size="14" isBold="true"/>
				</textElement>
				<text><![CDATA[ก่อนปรับ]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="e04000dc-5ae8-4038-aff3-a55ef218d103" x="490" y="0" width="60" height="18"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center">
					<font fontName="TH SarabunPSK" size="14" isBold="true"/>
				</textElement>
				<text><![CDATA[จำนวนที่ปรับ]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="b3d3ba4c-ff8a-493f-8c50-04941340a0aa" x="550" y="0" width="60" height="18"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center">
					<font fontName="TH SarabunPSK" size="14" isBold="true"/>
				</textElement>
				<text><![CDATA[หลังปรับ]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="fb5035e5-497e-4313-8e42-ac510b946e42" x="610" y="0" width="55" height="18"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center">
					<font fontName="TH SarabunPSK" size="14" isBold="true"/>
				</textElement>
				<text><![CDATA[มูลค่า]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="1c45a13b-3f70-4dc1-8755-c920342d9ae0" x="665" y="0" width="88" height="18"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center">
					<font fontName="TH SarabunPSK" size="14" isBold="true"/>
				</textElement>
				<text><![CDATA[ผู้ปรับยอด]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="2b96f57e-cef4-405e-b5a0-18bd9e22e83f" x="753" y="0" width="49" height="18"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center">
					<font fontName="TH SarabunPSK" size="14" isBold="true"/>
				</textElement>
				<text><![CDATA[หมายเหตุ]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="18" splitType="Prevent">
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="c382a3cc-0b67-4f7c-9e77-ed96bf1c90b3" positionType="Float" stretchType="RelativeToBandHeight" x="180" y="0" width="45" height="18"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center">
					<font fontName="TH SarabunPSK" size="14"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{drug_uom}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0." isBlankWhenNull="true">
				<reportElement uuid="0370f158-737c-45c2-861f-9f47b51b2578" positionType="Float" stretchType="RelativeToBandHeight" x="0" y="0" width="30" height="18"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="TH SarabunPSK" size="14"/>
					<paragraph rightIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{REPORT_COUNT}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="bef05dfd-ee05-400f-9f98-73915a1cdbe0" positionType="Float" stretchType="RelativeToBandHeight" x="30" y="0" width="150" height="18"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement>
					<font fontName="TH SarabunPSK" size="14"/>
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{item_common_name}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="e6c66812-783c-4968-826d-0bc08786a3be" positionType="Float" stretchType="RelativeToBandHeight" x="225" y="0" width="60" height="18"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center">
					<font fontName="TH SarabunPSK" size="14"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{lot_number}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="df3ca34c-2930-47ab-ab8f-f54a219e3f83" positionType="Float" stretchType="RelativeToBandHeight" x="285" y="0" width="60" height="18"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center">
					<font fontName="TH SarabunPSK" size="14"/>
				</textElement>
				<textFieldExpression><![CDATA[new java.text.SimpleDateFormat("dd/MM/yyyy", new java.util.Locale("th", "TH")).format($F{card_update_datetime})]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="fd0b84b8-89ea-46ec-83e8-ba75a8c7ed32" positionType="Float" stretchType="RelativeToBandHeight" x="345" y="0" width="85" height="18"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement>
					<font fontName="TH SarabunPSK" size="14"/>
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{adjust_type}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement uuid="e422f570-e8b4-4fbf-9592-d56fd78253e5" positionType="Float" stretchType="RelativeToBandHeight" x="430" y="0" width="60" height="18"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="TH SarabunPSK" size="14"/>
					<paragraph rightIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{previous_qty_lot}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement uuid="536b6a7e-8644-4b1e-ae4b-a436ed010649" positionType="Float" stretchType="RelativeToBandHeight" x="490" y="0" width="60" height="18"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="TH SarabunPSK" size="14"/>
					<paragraph rightIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{qty}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement uuid="272ba02b-9c45-477e-b054-40b019130e5a" positionType="Float" stretchType="RelativeToBandHeight" x="550" y="0" width="60" height="18"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="TH SarabunPSK" size="14"/>
					<paragraph rightIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{update_qty_lot}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement uuid="31ae0b07-0b8d-48ae-a5b4-0e8c19ccbbc4" positionType="Float" stretchType="RelativeToBandHeight" x="610" y="0" width="55" height="18"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="TH SarabunPSK" size="14"/>
					<paragraph rightIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{unit_cost}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="96597017-f1bb-431e-be6f-685e1ad863cc" positionType="Float" stretchType="RelativeToBandHeight" x="665" y="0" width="88" height="18"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement>
					<font fontName="TH SarabunPSK" size="14"/>
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{user_update}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="a57cc3e6-2421-4c47-b0fd-b119796c6b08" positionType="Float" stretchType="RelativeToBandHeight" x="753" y="0" width="49" height="18"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement>
					<font fontName="TH SarabunPSK" size="14"/>
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{f_hstock_adjust_type_id}== "9"
?"หมดอายุวันที่ "+ new java.text.SimpleDateFormat("dd/MM/yyyy", new java.util.Locale("th", "TH")).format($F{expire_date_adjust})
: $F{reason_adjust}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<pageFooter>
		<band height="20">
			<textField>
				<reportElement uuid="64a1e208-dc2e-4241-9b70-2e2e8250b7b2" x="712" y="0" width="60" height="20"/>
				<textElement textAlignment="Right">
					<font fontName="TH SarabunPSK" size="12"/>
				</textElement>
				<textFieldExpression><![CDATA["หน้า "+$V{PAGE_NUMBER}+" / "]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement uuid="4be7aed8-66b7-4199-ab49-112dd128f13a" x="772" y="0" width="30" height="20"/>
				<textElement>
					<font fontName="TH SarabunPSK" size="12"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="0200b55e-35d7-45a3-ab55-a6e003f407b3" x="0" y="0" width="188" height="20"/>
				<textElement verticalAlignment="Middle">
					<font fontName="TH SarabunPSK" size="12"/>
				</textElement>
				<textFieldExpression><![CDATA["พิมพ์วันที่ "+new java.text.SimpleDateFormat("dd MMMM yyyy เวลา HH:mm", new java.util.Locale("th", "TH")).format(new Date())
+" น."]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<noData>
		<band height="555">
			<staticText>
				<reportElement uuid="45a626ff-66ce-4c8e-bc8d-cef4444e04fb" x="0" y="70" width="802" height="69" forecolor="#FF0000"/>
				<textElement textAlignment="Center">
					<font fontName="TH SarabunPSK" size="26" isBold="true"/>
				</textElement>
				<text><![CDATA[ไม่พบข้อมูลของใบพิมพ์
รายงานการปรับยอดยาและเวชภัณฑ์
]]></text>
			</staticText>
		</band>
	</noData>
</jasperReport>

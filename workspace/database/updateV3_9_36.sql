-- ขยายขนาด passport เป็น 10 หลัก
ALTER TABLE t_health_family ALTER COLUMN passport_no TYPE character varying(10);

-- เพิ่มจำกัปริมาณที่จ่ายที่รายการยา
ALTER TABLE b_item_drug ADD COLUMN limit_amount_day integer NOT NULL default 0;

-- เพิ่มวันที่ออกใบประกอบใน employee
ALTER TABLE b_employee ADD COLUMN employee_number_issue_date date;

-- ล้มแฟ้ม
insert into f_gui_action (f_gui_action_id, gui_action_name) VALUES ('0421', 'ข้อมูลการล้มแฟ้ม');

-- drug favorite
insert into f_gui_action (f_gui_action_id, gui_action_name) VALUES ('5412', 'รายการตรวจรักษาที่ใช้บ่อย');

CREATE TABLE b_item_drug_favorite(
    b_item_drug_favorite_id    character varying(255)   NOT NULL,
    doctor_id             character varying(255)   NOT NULL,
    b_visit_clinic_id     character varying(255)   NOT NULL,
    b_item_id             character varying(255)   NOT NULL,
    usage_special         character varying(1) NOT NULL default '0', -- No 0, Yes 1
    usage_text            text,
    caution               text,
    caution_en            text,
    description           text,
    description_en        text,
    instruction_id        character varying(255)   NOT NULL,
    dose                  character varying(10)   NOT NULL,
    use_uom_id            character varying(255)   NOT NULL,
    frequency_id          character varying(255)   NOT NULL,
    qty                   character varying(10)   NOT NULL,
    purch_uom_id          character varying(255)   NOT NULL,
CONSTRAINT b_item_drug_favorite_pkey PRIMARY KEY (b_item_drug_favorite_id)
);

CREATE TABLE b_credit_card_type(
    b_credit_card_type_id    character varying(255)   NOT NULL,
    code                     character varying(255)   NOT NULL,
    description              character varying(255)   NOT NULL,
    active                   character varying(1)   NOT NULL DEFAULT '1',
    user_record       character varying(255),
    record_datetime   character varying(19),
    user_modify       character varying(255),
    modify_datetime   character varying(19),
CONSTRAINT b_credit_card_type_pkey PRIMARY KEY (b_credit_card_type_id),
CONSTRAINT b_credit_card_type_unique1 UNIQUE (code)
);

CREATE TABLE b_credit_card_info(
    b_credit_card_info_id    character varying(255)   NOT NULL,
    b_bank_info_id           character varying(255)   NOT NULL,
    b_credit_card_type_id    character varying(255)   NOT NULL,
    pattern_number           character varying(6)   NOT NULL,
CONSTRAINT b_credit_card_info_pkey PRIMARY KEY (b_credit_card_info_id),
CONSTRAINT b_credit_card_info_unique1 UNIQUE (pattern_number)
);

-- การเงิน
ALTER TABLE f_payment_type ADD COLUMN limit_account_number integer   NOT NULL default 0;
ALTER TABLE f_payment_type ADD COLUMN is_auto_check_bank character varying(1)   NOT NULL default '0';
ALTER TABLE f_payment_type ADD COLUMN require_card_type character varying(1)   NOT NULL default '0';
ALTER TABLE f_payment_type ADD COLUMN require_payment_date character varying(1)   NOT NULL default '0';
update f_payment_type set require_account_name = '1' , require_card_type = '1', limit_account_number = 16 , is_auto_check_bank = '1' where f_payment_type_id = '2';
update f_payment_type set require_payment_date = '1' where f_payment_type_id = '3';
update f_payment_type set require_payment_date = '1' , require_account_no = '0' where f_payment_type_id = '4';
ALTER TABLE f_payment_type ADD COLUMN lbl_bank_info character varying(255) default '';
ALTER TABLE f_payment_type ADD COLUMN lbl_account_name character varying(255) default '';
ALTER TABLE f_payment_type ADD COLUMN lbl_account_no character varying(255) default '';
ALTER TABLE f_payment_type ADD COLUMN lbl_card_type character varying(255) default '';
ALTER TABLE f_payment_type ADD COLUMN lbl_payment_date character varying(255) default '';
update f_payment_type set lbl_bank_info = 'ธนาคาร', lbl_account_name = 'ชื่อบนบัตรเครดิต', lbl_account_no = 'หมายบัตรเครดิต', lbl_card_type='ประเภทบัตรเครดิต' where f_payment_type_id = '2';
update f_payment_type set lbl_bank_info = 'ธนาคาร', lbl_account_no = 'เลขที่เช็ค', lbl_payment_date='วันที่ออกเช็ค' where f_payment_type_id = '3';
update f_payment_type set lbl_bank_info = 'ธนาคาร', lbl_payment_date='วันที่โอนเงิน' where f_payment_type_id = '4';

INSERT INTO f_payment_type (f_payment_type_id, description, require_bank_info
, require_account_name, require_account_no, require_card_type, is_auto_check_bank, limit_account_number
, lbl_bank_info, lbl_account_name, lbl_account_no, lbl_card_type)
VALUES ('5', 'บัตรเดบิต', '1','1','1','1','1',16,'ธนาคาร','ชื่อบนบัตรเดบิต','หมายบัตรเดบิต','ประเภทบัตรเดบิต');

ALTER TABLE t_billing_receipt ADD COLUMN b_bank_info_id character varying(255);
ALTER TABLE t_billing_receipt ADD COLUMN b_credit_card_type_id character varying(255);
ALTER TABLE t_billing_receipt ADD COLUMN account_name character varying(255);
ALTER TABLE t_billing_receipt ADD COLUMN account_number character varying(255);
ALTER TABLE t_billing_receipt ADD COLUMN transaction_date date;

-- DROP UNUSED COLUMN
ALTER TABLE b_employee DROP COLUMN employee_firstname;
ALTER TABLE b_employee DROP COLUMN employee_lastname;

-- ระบุว่าเป็นแพทย์ประจำหรือไม่ (ค่าตั้งต้น คือป็นแพทย์ประจำ)
ALTER TABLE b_employee ADD COLUMN is_main_doctor character varying(1)   NOT NULL default '1';

-- Appointment copy from CU
ALTER TABLE t_patient_appointment RENAME COLUMN patient_appointment_clinic TO patient_appointment_servicepoint;
ALTER TABLE t_patient_appointment ADD COLUMN appointment_confirm_date character varying(10);
ALTER TABLE t_patient_appointment ADD COLUMN change_appointment_cause text;
ALTER TABLE t_patient_appointment ADD COLUMN visit_id_make_appointment character varying(255) NULL;
ALTER TABLE t_patient_appointment ADD COLUMN patient_appointment_clinic character varying(255);

ALTER TABLE b_visit_clinic ADD COLUMN enable_sametime_appointment character varying(1) NOT NULL default '1';

INSERT INTO b_visit_clinic 
(b_visit_clinic_id
,visit_clinic_number
,visit_clinic_description
,f_service_group_id
,visit_clinic_active
,enable_sametime_appointment
)
select '999', '999', 'อื่นๆ', '8', '1', '1'
WHERE  NOT EXISTS (
select b_visit_clinic_id from b_visit_clinic where b_visit_clinic.visit_clinic_description = 'อื่นๆ'
);

update t_patient_appointment set patient_appointment_clinic = b_visit_clinic_id from b_visit_clinic where b_visit_clinic.visit_clinic_description = 'อื่นๆ';

ALTER TABLE t_patient_appointment
   ALTER COLUMN patient_appointment_clinic SET NOT NULL;

update public.t_patient_appointment set patient_appointment_time = patient_appointment_end_time where patient_appointment_time = '' or patient_appointment_time is null;
update public.t_patient_appointment set patient_appointment_time = '08:00' where patient_appointment_time = '' or patient_appointment_time is null;
update public.t_patient_appointment set patient_appointment_end_time = patient_appointment_time where patient_appointment_end_time = '' or patient_appointment_end_time is null;

-- fix apoointment order from drug set
ALTER TABLE t_patient_appointment_order ADD COLUMN b_item_set_id character varying(255) NULL;

-- จำกัดจำนวนนัด
insert into f_gui_action (f_gui_action_id, gui_action_name) VALUES ('0409', 'จำกัดจำนวนนัด');

CREATE TABLE b_service_limit(
    b_service_limit_id    character varying(255)   NOT NULL,
    time_start            character varying(5)   NOT NULL default '00:00',
    time_end              character varying(5)   NOT NULL default '00:00',
    limit_appointment     integer NOT NULL default 0,
    limit_walkin          integer NOT NULL default 0,
CONSTRAINT b_service_limit_pkey PRIMARY KEY (b_service_limit_id)
);

CREATE TABLE b_service_limit_clinic(
    b_service_limit_clinic_id    character varying(255)   NOT NULL,
    b_visit_clinic_id     character varying(255)   NOT NULL,
    time_start            character varying(5)   NOT NULL default '00:00',
    time_end              character varying(5)   NOT NULL default '00:00',
    limit_appointment     integer NOT NULL default 0,
    limit_walkin          integer NOT NULL default 0,
CONSTRAINT b_service_limit_clinic_pkey PRIMARY KEY (b_service_limit_clinic_id)
);
-- alter table
ALTER TABLE b_employee ADD COLUMN b_visit_clinic_id character varying(255) NULL;

-- เพิ่มสถานะการนัด
INSERT INTO f_appointment_status (f_appointment_status_id, appointment_status_name, appointment_status_note) VALUES 
('6', 'ยืนยันการนัด', 'ยืนยันการนัดตามวันเวลาที่นัด');
-- END Appointment copy from CU

-- copy ยืมคืน OPD Card from CU
-- Borrow OPD Card
alter table b_service_point add column alert_send_opdcard  character varying(1) NOT NULL default '0';

-- internal
CREATE TABLE t_borrow_opdcard_internal(
    t_borrow_opdcard_internal_id    character varying(255)   NOT NULL,
    t_patient_id                    character varying(255)   NOT NULL,
    t_visit_id                      character varying(255)   NOT NULL,
    status                      character varying(1) NOT NULL default '0', -- No 0, Yes 1
    takeout_datetime             timestamp without time zone NOT NULL DEFAULT current_timestamp,
    user_takeout_id              character varying(255)   NOT NULL,
    return_datetime             timestamp without time zone,
    user_return_id              character varying(255),
CONSTRAINT t_borrow_opdcard_internal_pkey PRIMARY KEY (t_borrow_opdcard_internal_id),
CONSTRAINT t_borrow_opdcard_internal_unique1 UNIQUE (t_patient_id, t_visit_id)
);

CREATE TABLE t_borrow_opdcard_internal_tracking(
    t_borrow_opdcard_internal_tracking_id character varying(255)   NOT NULL,
    t_borrow_opdcard_internal_id    character varying(255)   NOT NULL,
    b_service_point_sender_id       character varying(255)   NOT NULL, -- b_service_point
    sender_id                       character varying(255)   NOT NULL, -- b_employee
    send_datetime                   timestamp without time zone NOT NULL DEFAULT current_timestamp,
    b_service_point_receiver_id     character varying(255)   NOT NULL, -- b_service_point
CONSTRAINT t_borrow_opdcard_internal_tracking_pkey PRIMARY KEY (t_borrow_opdcard_internal_tracking_id)
);
-- End of Borrow OPDCard from CU

ALTER TABLE t_patient_payment ADD COLUMN patient_payment_regular_hospital character varying(5) DEFAULT '';
ALTER TABLE t_visit_payment ADD COLUMN visit_payment_regular_hospital character varying(5) DEFAULT '';

-- update timesamp of receipt's book number 
ALTER TABLE t_billing_receipt_book_seq ADD COLUMN update_ts timestamp without time zone default current_timestamp;

CREATE FUNCTION "public"."set_update_ts" () RETURNS trigger AS'
    BEGIN
        NEW.update_ts = NOW();
        RETURN NEW;
    END;
    'LANGUAGE 'plpgsql' IMMUTABLE CALLED ON NULL INPUT SECURITY INVOKER;

CREATE TRIGGER "trg_set_update_ts" BEFORE INSERT OR UPDATE
    ON t_billing_receipt_book_seq FOR EACH ROW
    EXECUTE PROCEDURE "public"."set_update_ts"();

-- reset year
ALTER TABLE t_visit_year ADD COLUMN auto_reset_on_day integer NULL;
ALTER TABLE t_visit_year ADD COLUMN auto_reset_on_month integer NULL;
ALTER TABLE t_visit_year ADD COLUMN auto_reset_date date NULL;

--Update t_patient_payment.patient_payment_money_limit
update t_patient_payment 
set patient_payment_money_limit = substr(patient_payment_money_limit,1,position('-' in t_patient_payment.patient_payment_money_limit)-1)
where patient_payment_money_limit ilike '%-%';

--Update t_diag_icd9.diag_icd9_staff_doctor
update t_diag_icd9 set diag_icd9_staff_doctor = doctor.staff_doctor
from (
select 
        t_diag_icd9.t_diag_icd9_id
        ,b_employee.b_employee_id
        ,t_diag_icd10.diag_icd10_staff_doctor
        ,t_visit_service.visit_service_staff_doctor
        , case when t_diag_icd10.diag_icd10_staff_doctor is not null and t_visit_service.visit_service_staff_doctor is not null
                    then t_diag_icd10.diag_icd10_staff_doctor
                 when t_diag_icd10.diag_icd10_staff_doctor is not null and t_visit_service.visit_service_staff_doctor is null
                    then t_diag_icd10.diag_icd10_staff_doctor
                 when t_diag_icd10.diag_icd10_staff_doctor is null and t_visit_service.visit_service_staff_doctor is not null
                    then t_visit_service.visit_service_staff_doctor
                    else t_diag_icd9.diag_icd9_staff_doctor end as staff_doctor

from t_diag_icd9 left join 
                (select 
                            t_diag_icd10.diag_icd10_vn
                            ,t_diag_icd10.diag_icd10_staff_doctor        
                    from t_diag_icd10 inner join (
                    select 
                            t_diag_icd10.diag_icd10_vn as diag_icd10_vn
                            ,min(t_diag_icd10.diag_icd10_record_date_time) as diag_icd10_record_date_time
                    from t_diag_icd10 
                    where 
                            t_diag_icd10.diag_icd10_active = '1'
                            and t_diag_icd10.f_diag_icd10_type_id = '1'
                    group by
                            t_diag_icd10.diag_icd10_vn) as min_diag_icd10
                    on t_diag_icd10.diag_icd10_vn = min_diag_icd10.diag_icd10_vn 
                        and t_diag_icd10.diag_icd10_record_date_time = min_diag_icd10.diag_icd10_record_date_time ) as t_diag_icd10
        on t_diag_icd9.diag_icd9_vn = t_diag_icd10.diag_icd10_vn

        left join (select
                                t_visit_service.t_visit_id
                                ,t_visit_service.visit_service_staff_doctor
                        from t_visit_service inner join (
                        select
                                t_visit_service.t_visit_id as t_visit_id
                                ,max(t_visit_service.assign_date_time) as assign_date_time
                        from t_visit_service
                        where
                                visit_service_staff_doctor <> ''
                        group by
                                t_visit_service.t_visit_id ) as max_service_doctor
                        on t_visit_service.t_visit_id = max_service_doctor.t_visit_id
                            and t_visit_service.assign_date_time = max_service_doctor.assign_date_time) as t_visit_service
        on t_diag_icd9.diag_icd9_vn =  t_visit_service.t_visit_id

        inner join b_employee on t_diag_icd9.diag_icd9_staff_doctor  = b_employee.b_employee_id
                                           and  b_employee.f_employee_authentication_id not in  ('3','98','99')
where 
        t_diag_icd9.diag_icd9_active = '1'

) as doctor
where
        t_diag_icd9.t_diag_icd9_id =  doctor.t_diag_icd9_id;

-- Fixed BUG #983
update f_item_lab_type set name = 'จุลทรรศน์ศาสตร์(Microscopy)' where id = '2';

-- For LIS
ALTER TABLE t_lis_ln
  ADD COLUMN exec_computer_name character varying(255) NOT NULL DEFAULT 'Hospital-OS';
ALTER TABLE t_lis_ln
  ADD COLUMN exec_computer_ip inet NOT NULL DEFAULT '127.0.0.1';

-- update db version
INSERT INTO s_version VALUES ('9701000000069', '69', 'Hospital OS, Community Edition', '3.9.36', '3.22.151113', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_36.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.36');
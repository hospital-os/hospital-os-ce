CREATE TABLE b_receipt_sequence (
b_receipt_sequence_id varchar(255) NOT NULL
, receipt_sequence_description varchar(255)
, receipt_sequence_service_point varchar(255)
, receipt_sequence_pattern varchar(255)
, receipt_sequence_value varchar(255)
, receipt_sequence_ip varchar(255)
, receipt_sequence_active varchar(255) DEFAULT '0'::character varying 
, PRIMARY KEY (b_receipt_sequence_id)
);

INSERT INTO s_version VALUES ('9701000000045', '45', 'Hospital OS, Community Edition', '3.9.12', '3.18.240311', '2554-03-24 15:28:00');

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_12.sql',(select current_date) || ','|| (select current_time),'ปรับแก้สำหรับ hospitalOS3.9.12');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.usecase.transaction;

/**
 *
 * @author Somprasong
 */
public interface ManageAppointment {

    public void notifySaveAppointment(String str, int status);

    public void notifyDeleteAppointment(String str, int status);
}

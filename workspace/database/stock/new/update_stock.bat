set PATH=%PATH%;C:\Program Files\PostgreSQL\8.4\bin
set DB=kkrachan
set HOST=192.168.1.8
pg_dump -U  postgres  -h %HOST% -i  -F p -a --column-inserts -v  -t b_employee  %DB%  >  b_employee.sql
pg_dump -U  postgres  -h %HOST% -i   -F p -a --column-inserts -v  -t b_item  %DB%  >  b_item.sql
pg_dump -U  postgres  -h %HOST% -i  -F p -a --column-inserts -v  -t b_item_price  %DB%  >  b_item_price.sql
pg_dump -U  postgres  -h %HOST% -i  -F p -a --column-inserts -v  -t b_service_point  %DB%  >  b_service_point.sql
pg_dump -U  postgres  -h %HOST% -i   -F p -a --column-inserts -v  -t b_visit_ward  %DB%  > b_visit_ward.sql
pg_dump -U  postgres  -h %HOST% -i   -F p -a --column-inserts -v  -t b_item_drug_uom  %DB%  >  b_item_drug_uom.sql
pg_dump -U  postgres  -h %HOST% -i   -F p -a --column-inserts -v  -t b_site  %DB%  >  b_site.sql
pg_dump -U  postgres  -h %HOST% -i   -F p -a --column-inserts -v  -t f_address  %DB%  > f_address.sql
psql -U postgres  -h %HOST% -f deleteTable.sql %DB% 
psql -U postgres  -h %HOST% -f b_item.sql %DB%
psql -U postgres  -h %HOST% -f b_item_price.sql %DB%
psql -U postgres  -h %HOST% -f b_service_point.sql %DB%
psql -U postgres  -h %HOST% -f b_visit_ward.sql %DB%
psql -U postgres  -h %HOST% -f b_item_drug_uom.sql %DB%
psql -U postgres  -h %HOST% -f b_site.sql %DB%
psql -U postgres  -h %HOST% -f f_address.sql %DB%
psql -U postgres  -h %HOST% -f b_employee.sql %DB%
del b_employee.sql
del b_item.sql
del b_item_price.sql
del b_service_point.sql
del b_visit_ward.sql
del b_item_drug_uom.sql
del b_site.sql
del f_address.sql
pause
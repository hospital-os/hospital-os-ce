/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
public class DrugCode24 extends Persistent implements CommonInf {

    public String itemname = "";
    public String regno = "";
    public String tradename = "";
    public String company = "";
    public String drugcode24 = "";

    @Override
    public String getCode() {
        return getObjectId();
    }

    @Override
    public String getName() {
        return itemname;
    }

    @Override
    public String toString() {
        return getName();
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
public class EyesExam extends Persistent {

    public String t_visit_id = "";
    public String left_eyes_result = "";
    public String right_eyes_result = "";
    public String left_eyes_diagnosis = "";
    public String right_eyes_diagnosis = "";
    public String left_eyes_csme = "";
    public String right_eyes_csme = "";
    public String left_eyes_opticnerve = "";
    public String right_eyes_opticnerve = "";
    public String left_eyes_opticnerve_detail = "";
    public String right_eyes_opticnerve_detail = "";
    public String left_eyes_haemorrhage = "";
    public String right_eyes_haemorrhage = "";
    public String left_eyes_exudates = "";
    public String right_eyes_exudates = "";
    public String left_eyes_bg = "";
    public String right_eyes_bg = "";
    public String left_eyes_macula_edema = "";
    public String right_eyes_macula_edema = "";
    public String other_diseases_found = "";
    public String other_diseases_cataract = "";
    public String other_diseases_glaucoma = "";
    public String other_diseases_pterygium = "";
    public String other_diseases_other = "";
    public String other_diseases_other_detail = "";
    public String doctor_recommend = "";
    public String eyes_management = "";
    public String appointment_date = "";
    public String appointment_place = "";
    public String doctor_diag = "";
    public String screen_date = "";
    public String color_vision_result = "9";
    public String color_vision_test_1_result = "";
    public String color_vision_test_2_result = "";
    public String color_vision_test_3_result = "";
    public String color_vision_test_4_result = "";
    public String color_vision_test_5_result = "";
    public String color_vision_test_6_result = "";
    public String color_vision_test_7_result = "";
    public String color_vision_test_8_result = "";
    public String color_vision_test_9_result = "";
    public String color_vision_test_10_result = "";
    public String color_vision_test_11_result = "";
    public String color_vision_test_12_result = "";
    public String color_vision_test_13_result = "";
    public String color_vision_test_14_result = "";
    public String color_vision_test_15_result = "";
    public String color_vision_test_16_result = "";
    public String color_vision_test_17_result = "";
    public String color_vision_test_18_result = "";
    public String color_vision_test_19_result = "";
    public String color_vision_test_20_result = "";
    public String color_vision_test_21_result = "";
    public String color_vision_test_22_result = "";
    public String color_vision_test_23_result = "";
    public String color_vision_test_24_result = "";
    public String color_vision_test_user_id = "";
    public String active = "1";
    public String record_date_time = "";
    public String user_record_id = "";
    public String update_date_time = "";
    public String user_update_id = "";
}

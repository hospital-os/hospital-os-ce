/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.Naranjo;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.ResultSet;
import java.util.Vector;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class NaranjoDB {

    private final ConnectionInf theConnectionInf;
    private final String tableId = "990";

    public NaranjoDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public int insert(Naranjo obj) throws Exception {
        String sql = "INSERT INTO t_naranjo( "
                + "t_naranjo_id, t_patient_drug_allergy_id, naranjo_result1, "
                + "naranjo_result2, naranjo_result3, "
                + "naranjo_result4, naranjo_result5, "
                + "naranjo_result6, naranjo_result7, "
                + "naranjo_result8, naranjo_result9, "
                + "naranjo_result10, naranjo_total, "
                + "user_record, record_date_time, user_modify, modify_date_time, active) "
                + "VALUES ('%s', '%s', %d, "
                + "%d, %d, "
                + "%d, %d, "
                + "%d, %d, "
                + "%d, %d, "
                + "%d, %d, "
                + "'%s', '%s', '%s', '%s', '%s')";
        sql = String.format(sql, obj.getGenID(tableId), obj.t_patient_drug_allergy_id,
                obj.naranjo_result1,
                obj.naranjo_result2,
                obj.naranjo_result3,
                obj.naranjo_result4,
                obj.naranjo_result5,
                obj.naranjo_result6,
                obj.naranjo_result7,
                obj.naranjo_result8,
                obj.naranjo_result9,
                obj.naranjo_result10,
                obj.naranjo_total,
                obj.user_record, obj.record_date_time,
                obj.user_modify, obj.modify_date_time, "1");
        return theConnectionInf.eUpdate(sql);
    }

    public int update(Naranjo obj) throws Exception {
        String sql = "UPDATE t_naranjo "
                + "SET naranjo_result1=%d, "
                + "naranjo_result2=%d, naranjo_result3=%d, "
                + "naranjo_result4=%d, naranjo_result5=%d, "
                + "naranjo_result6=%d, naranjo_result7=%d, "
                + "naranjo_result8=%d, naranjo_result9=%d, "
                + "naranjo_result10=%d, naranjo_total=%d, "
                + "user_modify='%s', modify_date_time='%s', active='%s' "
                + "WHERE t_naranjo_id='%s' ";
        sql = String.format(sql, 
                obj.naranjo_result1,
                obj.naranjo_result2,
                obj.naranjo_result3,
                obj.naranjo_result4,
                obj.naranjo_result5,
                obj.naranjo_result6,
                obj.naranjo_result7,
                obj.naranjo_result8,
                obj.naranjo_result9,
                obj.naranjo_result10,
                obj.naranjo_total,
                obj.user_modify, obj.modify_date_time, obj.active, obj.getObjectId());
        return theConnectionInf.eUpdate(sql);
    }

    public Naranjo selectById(String id) throws Exception {
        String sql = "select * from t_naranjo where t_naranjo_id = '%s'";
        Vector eQuery = eQuery(String.format(sql, id));
        return (Naranjo) (eQuery.isEmpty() ? null : eQuery.get(0));
    }
    
    public Naranjo selectByPDAId(String pdaId) throws Exception {
        String sql = "select * from t_naranjo where t_patient_drug_allergy_id = '%s'";
        Vector eQuery = eQuery(String.format(sql, pdaId));
        return (Naranjo) (eQuery.isEmpty() ? null : eQuery.get(0));
    }

    public Vector eQuery(String sql) throws Exception {
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        while (rs.next()) {
            Naranjo p = new Naranjo();
            p.setObjectId(rs.getString("t_naranjo_id"));
            p.t_patient_drug_allergy_id = rs.getString("t_patient_drug_allergy_id");
            p.naranjo_result1 = rs.getInt("naranjo_result1");
            p.naranjo_result2 = rs.getInt("naranjo_result2");
            p.naranjo_result3 = rs.getInt("naranjo_result3");
            p.naranjo_result4 = rs.getInt("naranjo_result4");
            p.naranjo_result5 = rs.getInt("naranjo_result5");
            p.naranjo_result6 = rs.getInt("naranjo_result6");
            p.naranjo_result7 = rs.getInt("naranjo_result7");
            p.naranjo_result8 = rs.getInt("naranjo_result8");
            p.naranjo_result9 = rs.getInt("naranjo_result9");
            p.naranjo_result10 = rs.getInt("naranjo_result10");
            p.naranjo_total = rs.getInt("naranjo_total");
            p.user_record = rs.getString("user_record");
            p.record_date_time = rs.getString("record_date_time");
            p.user_modify = rs.getString("user_modify");
            p.modify_date_time = rs.getString("modify_date_time");
            p.active = rs.getString("active");
            list.add(p);
        }
        rs.close();
        return list;
    }

}

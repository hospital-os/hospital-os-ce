/*
 * DrugDoseShortcutLookup.java
 *
 * Created on 3 �ԧ�Ҥ� 2549, 15:47 �.
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
package com.hosv3.control.lookup;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.LookupControlInf;
import com.hosv3.control.LookupControl;

/**
 *
 * @author henbe
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class DrugDoseShortcutLookup implements LookupControlInf {

    private LookupControl thePC;

    /**
     * Creates a new instance of DrugDoseShortcut
     */
    public DrugDoseShortcutLookup(LookupControl pc) {
        thePC = pc;
    }

    @Override
    public java.util.Vector listData(String str) {
        return thePC.listDrugDoseShortcutByName(str);
    }

    public CommonInf readData(String pk) {
        return thePC.readDrugDoseShortcutByCode(pk);
    }

    @Override
    public CommonInf readHosData(String pk) {
        return thePC.readDrugDoseShortcutByCode(pk);
    }
}

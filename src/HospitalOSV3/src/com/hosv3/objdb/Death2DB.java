package com.hosv3.objdb;

import com.hospital_os.objdb.DeathDB;
import com.hospital_os.object.Death;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.util.Vector;

@SuppressWarnings("UseOfObsoleteCollectionType")
public class Death2DB extends DeathDB {

    public Death2DB(ConnectionInf db) {
        super(db);
    }

    public Death selectByPatientId(String patient_id) throws Exception {
        return selectByPtid(patient_id);
    }

    @Override
    public Death selectByPtid(String patient_id) throws Exception {
        return selectByPtid(patient_id, "1");
    }

    public Death selectByPtid(String patient_id, String active) throws Exception {
        String sql = "select * from " + dbObj.table
                + " where " + dbObj.patient_id
                + " = '" + patient_id + "'";
        if (!active.equals("")) {
            sql += " and " + dbObj.active + " = '" + active + "'";
        }

        Vector v = eQuery(sql);
        if (v.isEmpty()) {
            return null;
        } else {
            return (Death) v.get(0);
        }
    }
    //////////////////////////////////////////////////////////////////////////////
}

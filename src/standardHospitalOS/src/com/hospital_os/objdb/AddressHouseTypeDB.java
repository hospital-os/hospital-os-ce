/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.AddressHouseType;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class AddressHouseTypeDB {

    private final ConnectionInf connectionInf;

    public AddressHouseTypeDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public List<AddressHouseType> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_address_housetype";
            preparedStatement = connectionInf.ePQuery(sql);
            
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<AddressHouseType> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<AddressHouseType> list = new ArrayList<AddressHouseType>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                AddressHouseType obj = new AddressHouseType();
                obj.setObjectId(rs.getString("f_address_housetype_id"));
                obj.description = rs.getString("address_housetype_description");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (AddressHouseType obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }
}

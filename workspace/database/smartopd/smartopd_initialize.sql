ALTER TABLE t_result_xray ADD COLUMN request_smartopd character varying(1) default '0';

CREATE TABLE s_smartopd_version (
    s_smartopd_version_id                    varchar(255) NOT NULL,
    version_smartopd_number            	varchar(255) NULL,
    version_smartopd_description       	varchar(255) NULL,
    version_smartopd_application_number	varchar(255) NULL,
    version_smartopd_database_number   	varchar(255) NULL,
    version_smartopd_update_time       	varchar(255) NULL
    );

ALTER TABLE s_smartopd_version
	ADD CONSTRAINT s_smartopd_version_pkey
	PRIMARY KEY (s_smartopd_version_id);


INSERT INTO s_smartopd_version VALUES ('9780000000001', '1', 'SmartOPD Module', '1.0.0', '1.0.0', (select current_date) || ','|| (select current_time));

INSERT INTO s_script_update_log values ('SmartOPD_Module','smartopd_initialize.sql',(select current_date) || ','|| (select current_time),'Initialize SmartOPD Module');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.utility.search;

import com.hospital_os.utility.DataSource;
import com.hospital_os.utility.TableModelDataSource;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class DialogSearchData extends javax.swing.JDialog {

    private DialogSearchDatasource datasource;
    private final TableModelDataSource tabelModelItem = new TableModelDataSource("��¡��");
    private final ArrayList<DataSource> choosed = new ArrayList<DataSource>();

    /**
     * Creates new form DialogSearchData
     *
     * @param parent
     * @param modal
     */
    public DialogSearchData(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        clearUI();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        cbActive = new javax.swing.JCheckBox();
        txtSearch = new org.jdesktop.swingx.JXSearchField();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableResult = new javax.swing.JTable();
        btnChoose = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("����");
        getContentPane().setLayout(new java.awt.GridBagLayout());

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("���͹䢡�ä���"));
        jPanel1.setLayout(new java.awt.GridBagLayout());

        jLabel1.setText("�Ӥ���");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(jLabel1, gridBagConstraints);

        cbActive.setText("Active");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(cbActive, gridBagConstraints);

        txtSearch.setFont(txtSearch.getFont());
        txtSearch.setLayoutStyle(org.jdesktop.swingx.JXSearchField.LayoutStyle.MAC);
        txtSearch.setPrompt("Keyword"); // NOI18N
        txtSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSearchActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(txtSearch, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        getContentPane().add(jPanel1, gridBagConstraints);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("���Ѿ���ä���"));
        jPanel2.setLayout(new java.awt.GridBagLayout());

        tableResult.setFont(tableResult.getFont().deriveFont(tableResult.getFont().getSize()+1f));
        tableResult.setModel(tabelModelItem);
        tableResult.setFillsViewportHeight(true);
        tableResult.setRowHeight(18);
        tableResult.setSelectionMode(javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        tableResult.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableResultMouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tableResultMouseReleased(evt);
            }
        });
        tableResult.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tableResultKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tableResult);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(jScrollPane1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        getContentPane().add(jPanel2, gridBagConstraints);

        btnChoose.setText("���͡");
        btnChoose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChooseActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LAST_LINE_END;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        getContentPane().add(btnChoose, gridBagConstraints);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSearchActionPerformed
        this.doSearch();
    }//GEN-LAST:event_txtSearchActionPerformed

    private void btnChooseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChooseActionPerformed
        this.doChoose();
    }//GEN-LAST:event_btnChooseActionPerformed

    private void tableResultKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tableResultKeyReleased
        btnChoose.setEnabled(tableResult.getSelectedRowCount() > 0);
    }//GEN-LAST:event_tableResultKeyReleased

    private void tableResultMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableResultMouseReleased
        btnChoose.setEnabled(tableResult.getSelectedRowCount() > 0);
    }//GEN-LAST:event_tableResultMouseReleased

    private void tableResultMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableResultMouseClicked
        if (evt.getClickCount() == 2
                && tableResult.getSelectionModel().getSelectionMode()
                == javax.swing.ListSelectionModel.SINGLE_SELECTION) {
            btnChoose.doClick();
        }
    }//GEN-LAST:event_tableResultMouseClicked
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnChoose;
    private javax.swing.JCheckBox cbActive;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tableResult;
    private org.jdesktop.swingx.JXSearchField txtSearch;
    // End of variables declaration//GEN-END:variables

    public void setDatasource(DialogSearchDatasource datasource, boolean isShowActive, boolean isSingleSelection) {
        this.datasource = datasource;
        cbActive.setVisible(isShowActive);
        tableResult.setSelectionMode(isSingleSelection ? javax.swing.ListSelectionModel.SINGLE_SELECTION
                : javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    }

    private void doSearch() {
        tabelModelItem.clearTable();
        List<DataSource> results = this.datasource.search(
                txtSearch.getText().trim(),
                cbActive.isVisible() ? cbActive.isSelected() ? "1" : "0" : "1");
        tabelModelItem.setDataSources(results);
        tabelModelItem.fireTableDataChanged();
        btnChoose.setEnabled(tableResult.getSelectedRowCount() > 0);
    }

    public void clearUI() {
        choosed.clear();
        txtSearch.setText("");
        tabelModelItem.clearTable();
        btnChoose.setEnabled(false);
    }

    public List<DataSource> openDialog() {
        choosed.clear();
        this.setSize(400, 400);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        return choosed;
    }

    private void doChoose() {
        choosed.clear();
        choosed.addAll(tabelModelItem.getSelectedDataSources(tableResult.getSelectedRows()));
        this.dispose();
    }

    public void setSearchText(String txt) {
        txtSearch.setText(txt);
        this.doSearch();
    }
}

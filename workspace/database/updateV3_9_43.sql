alter table b_site add column is_hospital_wing boolean NOT NULL default false;

update b_site set is_hospital_wing = true where b_visit_office_id in (
'11482' ,
'11521' ,
'14965' ,
'14966' ,
'14161' ,
'14928' ,
'11514' ,
'11500' ,
'14862' ,
'11495' ,
'11499' ,
'11507' ,
'11518' ,
'11528' ,
'14967' ,
'14968' ,
'11490' , 
'11476');

ALTER TABLE t_diag_icd10 ADD COLUMN icd10_accident character varying(1) NOT NUlL default '0';

update t_diag_icd10 set icd10_accident = 
(select icd10_accident from b_icd10 where b_icd10.icd10_number = t_diag_icd10.diag_icd10_number);

update f_tab set tab_description = 'อาการเจ็บป่วย' where f_tab_id = '3';
update f_tab set tab_description = 'แลป' where f_tab_id = '7';
update f_tab set tab_description = 'เอ็กซเรย์' where f_tab_id = '8';

DELETE FROM b_item_lab_set
WHERE b_item_lab_set.b_item_id 
IN (select
        b_item_lab_set.b_item_id        
from b_item_lab_set inner join b_item as lab_set on lab_set.b_item_id = b_item_lab_set.b_item_id 
        inner join b_item_subgroup on  lab_set.b_item_subgroup_id = b_item_subgroup.b_item_subgroup_id
        inner join f_item_group on b_item_subgroup.f_item_group_id = f_item_group.f_item_group_id
where
        f_item_group.f_item_group_id <> '2');

CREATE TABLE f_person_jobtype (
    f_person_jobtype_id         character varying(2)   NOT NULL,
    description                 text   NOT NULL,
CONSTRAINT f_person_jobtype_pkey PRIMARY KEY (f_person_jobtype_id)
);
INSERT INTO f_person_jobtype VALUES ('0','ไม่ระบุ');
INSERT INTO f_person_jobtype VALUES ('1','ผสมเคมีบำบัด');
INSERT INTO f_person_jobtype VALUES ('2','รังสี');
INSERT INTO f_person_jobtype VALUES ('3','เล่นดนตรี');
INSERT INTO f_person_jobtype VALUES ('4','งานเกี่ยวกับอากาศยาน');
INSERT INTO f_person_jobtype VALUES ('5','จราจร');
INSERT INTO f_person_jobtype VALUES ('6','สรรพาวุธ');
INSERT INTO f_person_jobtype VALUES ('7','ซ่อมเครื่องยนต์');
INSERT INTO f_person_jobtype VALUES ('8','ดับเพลิง');
INSERT INTO f_person_jobtype VALUES ('9','กลึงโลหะ');
INSERT INTO f_person_jobtype VALUES ('10','งานที่มีการสัมผัสเสียงดัง');
INSERT INTO f_person_jobtype VALUES ('11','งานแท่นพิมพ์ โรงพิมพ์');
INSERT INTO f_person_jobtype VALUES ('12','งานบัดกรี');
INSERT INTO f_person_jobtype VALUES ('13','เติมน้ำกลั่น/ น้ำกรด แบตเตอรี่');
INSERT INTO f_person_jobtype VALUES ('14','งานพ่นสี ทาสี ลอกสี');
INSERT INTO f_person_jobtype VALUES ('15','เติมน้ำมันอากาศยาน');
INSERT INTO f_person_jobtype VALUES ('16','งานโรเนียว');
INSERT INTO f_person_jobtype VALUES ('17','งานพ่นทราย ลอกสี');
INSERT INTO f_person_jobtype VALUES ('18','งานไฟเบอร์กลาส');
INSERT INTO f_person_jobtype VALUES ('19','บุผ้าใยแก้ว');
INSERT INTO f_person_jobtype VALUES ('20','งานไม้(เลื่อย,ไส)');

ALTER TABLE t_health_family ADD COLUMN f_person_jobtype_id character varying(2) default '0';
ALTER TABLE t_person ADD COLUMN f_person_jobtype_id character varying(2) default '0';


insert into f_gui_action (f_gui_action_id, gui_action_name) VALUES ('5808', 'ตั้งค่าการเชื่อมต่อ RESTFul');
insert into f_gui_action (f_gui_action_id, gui_action_name) VALUES ('2403', 'แสดงภาพผู้ป่วย');
insert into f_gui_action (f_gui_action_id, gui_action_name) VALUES ('2404', 'เพิ่ม/ลบ ไฟลภาพผู้ป่วย');

ALTER TABLE t_patient ADD COLUMN picture_profile bytea default null;

CREATE TABLE b_restful_url(
    b_restful_url_id 	character varying(50),
    restful_code        text,           
    restful_url			character varying(255) NOT NUlL,
    user_update_id        	CHARACTER VARYING(255) NOT NULL,
    update_datetime       	timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,	
    CONSTRAINT b_restful_url_pk PRIMARY KEY (b_restful_url_id),
    CONSTRAINT b_restful_url_restful_code_key UNIQUE (restful_code)
);


-- update db version
INSERT INTO s_version VALUES ('9701000000076', '76', 'Hospital OS, Community Edition', '3.9.43', '3.27.220915', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_43.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.43');
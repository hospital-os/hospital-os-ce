/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.DueTypeDetail;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.Gutil;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class DueTypeDetailDB {

    private final ConnectionInf theConnectionInf;
    private final String idtable = "984";

    public DueTypeDetailDB(ConnectionInf theConnectionInf) {
        this.theConnectionInf = theConnectionInf;
    }

    public int insert(DueTypeDetail o) throws Exception {
        String sql = "INSERT INTO b_due_type_detail (b_due_type_detail_id, b_due_type_id, "
                + "description, active, user_record_id, record_date_time, user_update_id, update_date_time) "
                + "VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')";
        return theConnectionInf.eUpdate(String.format(sql,
                o.getGenID(idtable),
                o.b_due_type_id,
                Gutil.CheckReservedWords(o.description),
                o.active,
                o.user_record_id,
                o.record_date_time,
                o.user_update_id,
                o.update_date_time));
    }

    public int updateActive(DueTypeDetail o) throws Exception {
        String sql = "UPDATE b_due_type_detail SET active = '%s', user_update_id = '%s', update_date_time = '%s' "
                + "WHERE b_due_type_detail_id = '%s'";
        return theConnectionInf.eUpdate(String.format(sql,
                o.active,
                o.user_update_id,
                o.update_date_time,
                o.getObjectId()));
    }

    public DueTypeDetail selectById(String id) throws Exception {
        String sql = "select * from b_due_type_detail where b_due_type_detail_id = '%s'";
        List<DueTypeDetail> list = eQuery(String.format(sql, id));
        return (DueTypeDetail) (list.isEmpty() ? null : list.get(0));
    }

    public List<DueTypeDetail> listByDueTypeId(String id) throws Exception {
        String sql = "select * from b_due_type_detail where b_due_type_id = '%s' and active = '1'";
        List<DueTypeDetail> list = eQuery(String.format(sql, id));
        return list;
    }

    public List<DueTypeDetail> eQuery(String sql) throws Exception {
        List<DueTypeDetail> list = new ArrayList<DueTypeDetail>();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        while (rs.next()) {
            DueTypeDetail p = new DueTypeDetail();
            p.setObjectId(rs.getString("b_due_type_detail_id"));
            p.b_due_type_id = rs.getString("b_due_type_id");
            p.description = rs.getString("description");
            p.active = rs.getString("active");
            p.user_record_id = rs.getString("user_record_id");
            p.record_date_time = rs.getString("record_date_time");
            p.user_update_id = rs.getString("user_update_id");
            p.update_date_time = rs.getString("update_date_time");
            list.add(p);
        }
        rs.close();
        return list;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.gui.dialog;

import com.hosos.util.datetime.DateUtil;
import com.hospital_os.object.ServiceLimit;
import com.hospital_os.object.ServiceLimitServicePoint;
import com.hospital_os.utility.CellRendererHos;
import com.hospital_os.utility.ComboboxModel;
import com.hospital_os.utility.Gutil;
import com.hospital_os.utility.TableModelComplexDataSource;
import com.hosv3.control.HosControl;
import java.awt.CardLayout;
import java.util.List;
import java.util.Vector;
import javax.swing.JOptionPane;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class DialogAppointmentLimit extends javax.swing.JFrame {

    private HosControl theHC;
    private TableModelComplexDataSource tmcdsHos = new TableModelComplexDataSource(new String[]{
        "���������", "��������ش", "�ӹǹ�Ѵ"//, "�ӹǹ Walk-in"
    });
    private TableModelComplexDataSource tmcdsServicePoint = new TableModelComplexDataSource(new String[]{
        "�ش��ԡ��", "���������", "��������ش", "�ӹǹ�Ѵ"//, "�ӹǹ Walk-in"
    });
    private ServiceLimit sl;
    private ServiceLimitServicePoint slc;

    /**
     * Creates new form DialogAppointmentLimit
     */
    public DialogAppointmentLimit() {
        initComponents();
        this.setIconImage(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/appointment_limit.png")).getImage());
        CellRendererHos cellRendererHosCenter = new CellRendererHos(CellRendererHos.ALIGNMENT_CENTER);
        CellRendererHos cellRendererHosRight = new CellRendererHos(CellRendererHos.ALIGNMENT_RIGHT);
        tableListHospital.getColumnModel().getColumn(0).setCellRenderer(cellRendererHosCenter);
        tableListHospital.getColumnModel().getColumn(1).setCellRenderer(cellRendererHosCenter);

        tableListHospital.getColumnModel().getColumn(2).setCellRenderer(cellRendererHosRight);
//        tableListHospital.getColumnModel().getColumn(3).setCellRenderer(cellRendererHosRight);

        tableListServicePoint.getColumnModel().getColumn(1).setCellRenderer(cellRendererHosCenter);
        tableListServicePoint.getColumnModel().getColumn(2).setCellRenderer(cellRendererHosCenter);

        tableListServicePoint.getColumnModel().getColumn(3).setCellRenderer(cellRendererHosRight);
//        tableListClinic.getColumnModel().getColumn(4).setCellRenderer(cellRendererHosRight);

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        panelCard = new javax.swing.JPanel();
        panelShow = new javax.swing.JPanel();
        panelHospital = new javax.swing.JPanel();
        panelLeft = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableListHospital = new javax.swing.JTable();
        panelHospitalButton = new javax.swing.JPanel();
        btnHospitalAdd = new javax.swing.JButton();
        btnHospitalRemove = new javax.swing.JButton();
        btnHospitalEdit = new javax.swing.JButton();
        panelRight = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tableListServicePoint = new javax.swing.JTable();
        panelClinicButton = new javax.swing.JPanel();
        btnServicePointAdd = new javax.swing.JButton();
        btnServicePoinEdit = new javax.swing.JButton();
        btnServicePoinRemove = new javax.swing.JButton();
        panelEdit = new javax.swing.JPanel();
        lblServicePoint = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtStartTime = new com.hospital_os.utility.TimeTextField();
        txtEndTime = new com.hospital_os.utility.TimeTextField();
        limitApp = new sd.comp.jcalendar.JSpinField();
        limitWalkin = new sd.comp.jcalendar.JSpinField();
        jLabel5 = new javax.swing.JLabel();
        cbServicePoint = new javax.swing.JComboBox();
        jPanel1 = new javax.swing.JPanel();
        btnHospitalCancel = new javax.swing.JButton();
        btnHospitalSave = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("�ӡѴ�ӹǹ�Ѵ");

        panelCard.setLayout(new java.awt.CardLayout());

        panelShow.setLayout(new java.awt.GridBagLayout());

        panelHospital.setLayout(new java.awt.GridBagLayout());
        panelShow.add(panelHospital, new java.awt.GridBagConstraints());

        panelLeft.setBorder(javax.swing.BorderFactory.createTitledBorder("�ӹǹ/ʶҹ��Һ��"));
        panelLeft.setLayout(new java.awt.GridBagLayout());

        tableListHospital.setFont(tableListHospital.getFont());
        tableListHospital.setModel(tmcdsHos);
        tableListHospital.setFillsViewportHeight(true);
        tableListHospital.setRowHeight(20);
        tableListHospital.setSelectionMode(javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        tableListHospital.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tableListHospitalMouseReleased(evt);
            }
        });
        tableListHospital.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tableListHospitalKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tableListHospital);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelLeft.add(jScrollPane1, gridBagConstraints);

        panelHospitalButton.setLayout(new java.awt.GridBagLayout());

        btnHospitalAdd.setFont(btnHospitalAdd.getFont().deriveFont(btnHospitalAdd.getFont().getStyle() | java.awt.Font.BOLD));
        btnHospitalAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/add.png"))); // NOI18N
        btnHospitalAdd.setMinimumSize(new java.awt.Dimension(30, 30));
        btnHospitalAdd.setPreferredSize(new java.awt.Dimension(30, 30));
        btnHospitalAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHospitalAddActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelHospitalButton.add(btnHospitalAdd, gridBagConstraints);

        btnHospitalRemove.setFont(btnHospitalRemove.getFont().deriveFont(btnHospitalRemove.getFont().getStyle() | java.awt.Font.BOLD));
        btnHospitalRemove.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/delete.png"))); // NOI18N
        btnHospitalRemove.setMinimumSize(new java.awt.Dimension(30, 30));
        btnHospitalRemove.setPreferredSize(new java.awt.Dimension(30, 30));
        btnHospitalRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHospitalRemoveActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelHospitalButton.add(btnHospitalRemove, gridBagConstraints);

        btnHospitalEdit.setFont(btnHospitalEdit.getFont().deriveFont(btnHospitalEdit.getFont().getStyle() | java.awt.Font.BOLD));
        btnHospitalEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/edit.png"))); // NOI18N
        btnHospitalEdit.setMinimumSize(new java.awt.Dimension(30, 30));
        btnHospitalEdit.setPreferredSize(new java.awt.Dimension(30, 30));
        btnHospitalEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHospitalEditActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelHospitalButton.add(btnHospitalEdit, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        panelLeft.add(panelHospitalButton, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 0.6;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelShow.add(panelLeft, gridBagConstraints);

        panelRight.setBorder(javax.swing.BorderFactory.createTitledBorder("�ӹǹ/�ش��ԡ��"));
        panelRight.setLayout(new java.awt.GridBagLayout());

        tableListServicePoint.setFont(tableListServicePoint.getFont());
        tableListServicePoint.setModel(tmcdsServicePoint);
        tableListServicePoint.setFillsViewportHeight(true);
        tableListServicePoint.setRowHeight(20);
        tableListServicePoint.setSelectionMode(javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        tableListServicePoint.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tableListServicePointMouseReleased(evt);
            }
        });
        tableListServicePoint.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tableListServicePointKeyReleased(evt);
            }
        });
        jScrollPane2.setViewportView(tableListServicePoint);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelRight.add(jScrollPane2, gridBagConstraints);

        panelClinicButton.setLayout(new java.awt.GridBagLayout());

        btnServicePointAdd.setFont(btnServicePointAdd.getFont().deriveFont(btnServicePointAdd.getFont().getStyle() | java.awt.Font.BOLD));
        btnServicePointAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/add.png"))); // NOI18N
        btnServicePointAdd.setMinimumSize(new java.awt.Dimension(30, 30));
        btnServicePointAdd.setPreferredSize(new java.awt.Dimension(30, 30));
        btnServicePointAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnServicePointAddActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelClinicButton.add(btnServicePointAdd, gridBagConstraints);

        btnServicePoinEdit.setFont(btnServicePoinEdit.getFont().deriveFont(btnServicePoinEdit.getFont().getStyle() | java.awt.Font.BOLD));
        btnServicePoinEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/delete.png"))); // NOI18N
        btnServicePoinEdit.setMinimumSize(new java.awt.Dimension(30, 30));
        btnServicePoinEdit.setPreferredSize(new java.awt.Dimension(30, 30));
        btnServicePoinEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnServicePoinEditActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelClinicButton.add(btnServicePoinEdit, gridBagConstraints);

        btnServicePoinRemove.setFont(btnServicePoinRemove.getFont().deriveFont(btnServicePoinRemove.getFont().getStyle() | java.awt.Font.BOLD));
        btnServicePoinRemove.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/edit.png"))); // NOI18N
        btnServicePoinRemove.setMinimumSize(new java.awt.Dimension(30, 30));
        btnServicePoinRemove.setPreferredSize(new java.awt.Dimension(30, 30));
        btnServicePoinRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnServicePoinRemoveActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelClinicButton.add(btnServicePoinRemove, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        panelRight.add(panelClinicButton, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 0.6;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelShow.add(panelRight, gridBagConstraints);

        panelCard.add(panelShow, "show");

        panelEdit.setBorder(javax.swing.BorderFactory.createTitledBorder("��������´��èӡѴ�ӹǹ"));
        panelEdit.setLayout(new java.awt.GridBagLayout());

        lblServicePoint.setFont(lblServicePoint.getFont().deriveFont(lblServicePoint.getFont().getStyle() | java.awt.Font.BOLD));
        lblServicePoint.setText("�ش��ԡ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        panelEdit.add(lblServicePoint, gridBagConstraints);

        jLabel2.setFont(jLabel2.getFont().deriveFont(jLabel2.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel2.setText("��������ش");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        panelEdit.add(jLabel2, gridBagConstraints);

        jLabel3.setFont(jLabel3.getFont().deriveFont(jLabel3.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel3.setText("�ӹǹ�Ѵ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        panelEdit.add(jLabel3, gridBagConstraints);

        jLabel4.setFont(jLabel4.getFont().deriveFont(jLabel4.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel4.setText("�ӹǹ Walk-in");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        panelEdit.add(jLabel4, gridBagConstraints);

        txtStartTime.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtStartTime.setToolTipText("");
        txtStartTime.setFont(txtStartTime.getFont());
        txtStartTime.setMinimumSize(new java.awt.Dimension(70, 24));
        txtStartTime.setName("txtStartTime"); // NOI18N
        txtStartTime.setPreferredSize(new java.awt.Dimension(70, 24));
        txtStartTime.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtStartTimeFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        panelEdit.add(txtStartTime, gridBagConstraints);

        txtEndTime.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtEndTime.setToolTipText("");
        txtEndTime.setFont(txtEndTime.getFont());
        txtEndTime.setMinimumSize(new java.awt.Dimension(70, 24));
        txtEndTime.setName("timeTextFieldTimeAppointment"); // NOI18N
        txtEndTime.setPreferredSize(new java.awt.Dimension(70, 24));
        txtEndTime.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtEndTimeFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        panelEdit.add(txtEndTime, gridBagConstraints);

        limitApp.setMinimumSize(new java.awt.Dimension(70, 24));
        limitApp.setPreferredSize(new java.awt.Dimension(70, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        panelEdit.add(limitApp, gridBagConstraints);

        limitWalkin.setMinimumSize(new java.awt.Dimension(70, 24));
        limitWalkin.setPreferredSize(new java.awt.Dimension(70, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        panelEdit.add(limitWalkin, gridBagConstraints);

        jLabel5.setFont(jLabel5.getFont().deriveFont(jLabel5.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel5.setText("���������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        panelEdit.add(jLabel5, gridBagConstraints);

        cbServicePoint.setEditable(true);
        cbServicePoint.setFont(cbServicePoint.getFont());
        cbServicePoint.setMinimumSize(new java.awt.Dimension(150, 24));
        cbServicePoint.setPreferredSize(new java.awt.Dimension(150, 24));
        cbServicePoint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbServicePointActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        panelEdit.add(cbServicePoint, gridBagConstraints);

        jPanel1.setLayout(new java.awt.GridBagLayout());

        btnHospitalCancel.setFont(btnHospitalCancel.getFont().deriveFont(btnHospitalCancel.getFont().getStyle() | java.awt.Font.BOLD));
        btnHospitalCancel.setText("¡��ԡ");
        btnHospitalCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHospitalCancelActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(btnHospitalCancel, gridBagConstraints);

        btnHospitalSave.setFont(btnHospitalSave.getFont().deriveFont(btnHospitalSave.getFont().getStyle() | java.awt.Font.BOLD));
        btnHospitalSave.setText("�ѹ�֡");
        btnHospitalSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHospitalSaveActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(btnHospitalSave, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        panelEdit.add(jPanel1, gridBagConstraints);

        panelCard.add(panelEdit, "edit");

        getContentPane().add(panelCard, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cbServicePointActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbServicePointActionPerformed
        if (evt.getActionCommand().equals("comboBoxEdited")) {
            String keyword = String.valueOf(cbServicePoint.getSelectedItem());
            Vector v = theHC.theLookupControl.listServicePointOPD(keyword);
            if (v == null) {
                v = new Vector();
            }
            if (!v.isEmpty()) {
                ComboboxModel.initComboBox(cbServicePoint, v);
            }
        }
    }//GEN-LAST:event_cbServicePointActionPerformed

    private void tableListServicePointKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tableListServicePointKeyReleased
        this.doSelectedServicePoint();
    }//GEN-LAST:event_tableListServicePointKeyReleased

    private void tableListServicePointMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableListServicePointMouseReleased
        this.doSelectedServicePoint();
    }//GEN-LAST:event_tableListServicePointMouseReleased

    private void btnHospitalAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHospitalAddActionPerformed
        this.doAddServiceLimit();
    }//GEN-LAST:event_btnHospitalAddActionPerformed

    private void btnServicePointAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnServicePointAddActionPerformed
        this.doAddServiceLimitServicePoint();
    }//GEN-LAST:event_btnServicePointAddActionPerformed

    private void btnHospitalEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHospitalEditActionPerformed
        this.doEditServiceLimit();
    }//GEN-LAST:event_btnHospitalEditActionPerformed

    private void btnServicePoinRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnServicePoinRemoveActionPerformed
        this.doEditServiceLimitServicePoint();
    }//GEN-LAST:event_btnServicePoinRemoveActionPerformed

    private void btnHospitalRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHospitalRemoveActionPerformed
        this.doDeleteServiceLimit();
    }//GEN-LAST:event_btnHospitalRemoveActionPerformed

    private void btnServicePoinEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnServicePoinEditActionPerformed
        this.doDeleteServiceLimitServicePoint();
    }//GEN-LAST:event_btnServicePoinEditActionPerformed

    private void btnHospitalSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHospitalSaveActionPerformed
        this.doSave();
    }//GEN-LAST:event_btnHospitalSaveActionPerformed

    private void tableListHospitalKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tableListHospitalKeyReleased
        this.doSelectedHos();
    }//GEN-LAST:event_tableListHospitalKeyReleased

    private void tableListHospitalMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableListHospitalMouseReleased
        this.doSelectedHos();
    }//GEN-LAST:event_tableListHospitalMouseReleased

    private void btnHospitalCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHospitalCancelActionPerformed
        sl = null;
        slc = null;
        switchCard("show");
    }//GEN-LAST:event_btnHospitalCancelActionPerformed

    private void txtStartTimeFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtStartTimeFocusGained
        txtStartTime.selectAll();
    }//GEN-LAST:event_txtStartTimeFocusGained

    private void txtEndTimeFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtEndTimeFocusGained
        txtEndTime.selectAll();
    }//GEN-LAST:event_txtEndTimeFocusGained

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnHospitalAdd;
    private javax.swing.JButton btnHospitalCancel;
    private javax.swing.JButton btnHospitalEdit;
    private javax.swing.JButton btnHospitalRemove;
    private javax.swing.JButton btnHospitalSave;
    private javax.swing.JButton btnServicePoinEdit;
    private javax.swing.JButton btnServicePoinRemove;
    private javax.swing.JButton btnServicePointAdd;
    private javax.swing.JComboBox cbServicePoint;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblServicePoint;
    private sd.comp.jcalendar.JSpinField limitApp;
    private sd.comp.jcalendar.JSpinField limitWalkin;
    private javax.swing.JPanel panelCard;
    private javax.swing.JPanel panelClinicButton;
    private javax.swing.JPanel panelEdit;
    private javax.swing.JPanel panelHospital;
    private javax.swing.JPanel panelHospitalButton;
    private javax.swing.JPanel panelLeft;
    private javax.swing.JPanel panelRight;
    private javax.swing.JPanel panelShow;
    private javax.swing.JTable tableListHospital;
    private javax.swing.JTable tableListServicePoint;
    private com.hospital_os.utility.TimeTextField txtEndTime;
    private com.hospital_os.utility.TimeTextField txtStartTime;
    // End of variables declaration//GEN-END:variables

    public void setControl(HosControl theHC) {
        this.theHC = theHC;
        ComboboxModel.initComboBox(this.cbServicePoint, this.theHC.theLookupControl.listServicePointOPD(""));
    }

    public void showDialog() {
        this.loadServiceLimit();
        this.loadServiceLimitServicePoint();
        switchCard("show");
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void switchCard(String cardName) {
        CardLayout cl = (CardLayout) panelCard.getLayout();
        cl.show(panelCard, cardName);
    }

    private void loadServiceLimit() {
        tmcdsHos.clearTable();
        tmcdsHos.setComplexDataSources(theHC.theSetupControl.listServiceLimit());
        tmcdsHos.fireTableDataChanged();
        btnHospitalEdit.setEnabled(false);
        btnHospitalRemove.setEnabled(false);
    }

    private void loadServiceLimitServicePoint() {
        tmcdsServicePoint.clearTable();
        tmcdsServicePoint.setComplexDataSources(theHC.theSetupControl.listServiceLimitServicePoint());
        tmcdsServicePoint.fireTableDataChanged();
        btnServicePoinRemove.setEnabled(false);
        btnServicePoinEdit.setEnabled(false);
    }

    private void doSelectedHos() {
        btnHospitalEdit.setEnabled(tableListHospital.getSelectedRowCount() > 0);
        btnHospitalRemove.setEnabled(tableListHospital.getSelectedRowCount() > 0);
    }

    private void doSelectedServicePoint() {
        btnServicePoinRemove.setEnabled(tableListServicePoint.getSelectedRowCount() > 0);
        btnServicePoinEdit.setEnabled(tableListServicePoint.getSelectedRowCount() > 0);
    }

    private void doAddServiceLimit() {
        setEditData(new ServiceLimit());
    }

    private void doAddServiceLimitServicePoint() {
        setEditData(new ServiceLimitServicePoint());
    }

    private void setEditData(Object object) {
        sl = null;
        slc = null;
        if (object instanceof ServiceLimit) {
            lblServicePoint.setVisible(false);
            cbServicePoint.setVisible(false);
            sl = (ServiceLimit) object;
            if (sl.time_start == null || sl.time_start.isEmpty()) {
                txtStartTime.resetTime();
            } else {
                txtStartTime.setText(sl.time_start);
            }
            if (sl.time_end == null || sl.time_end.isEmpty()) {
                txtEndTime.resetTime();
            } else {
                txtEndTime.setText(sl.time_end);
            }
            limitApp.setValue(sl.limit_appointment);
            limitWalkin.setValue(sl.limit_walkin);
        } else {
            ComboboxModel.initComboBox(cbServicePoint, this.theHC.theLookupControl.listServicePointOPD(""));
            lblServicePoint.setVisible(true);
            cbServicePoint.setVisible(true);
            slc = (ServiceLimitServicePoint) object;
            Gutil.setGuiData(cbServicePoint, slc.b_service_point_id);
            if (slc.time_start == null) {
                txtStartTime.resetTime();
            } else {
                txtStartTime.setDate(slc.time_start);
            }
            if (slc.time_end == null) {
                txtEndTime.resetTime();
            } else {
                txtEndTime.setDate(slc.time_end);
            }
            limitApp.setValue(slc.limit_appointment);
            limitWalkin.setValue(slc.limit_walkin);
        }
        jLabel4.setVisible(false);
        limitWalkin.setVisible(false);
        switchCard("edit");
    }

    private void doEditServiceLimit() {
        setEditData(tmcdsHos.getSelectedId(tableListHospital.getSelectedRow()));
    }

    private void doEditServiceLimitServicePoint() {
        setEditData(tmcdsServicePoint.getSelectedId(tableListServicePoint.getSelectedRow()));
    }

    private void doDeleteServiceLimit() {
        List<Object> checkedIds = tmcdsHos.getSelectedIds(tableListHospital.getSelectedRows());
        if (checkedIds.size() > 0) {
            if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(null,
                    "�׹�ѹ���ź��¡�÷�����͡", "�׹�ѹ���ź", JOptionPane.YES_NO_OPTION)) {
                int ret = theHC.theSetupControl.deleteServiceLimit(checkedIds.toArray(new ServiceLimit[checkedIds.size()]));
                if (ret > 0) {
                    loadServiceLimit();
                }
            }
        }
    }

    private void doDeleteServiceLimitServicePoint() {
        List<Object> checkedIds = tmcdsServicePoint.getSelectedIds(tableListServicePoint.getSelectedRows());
        if (checkedIds.size() > 0) {
            if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(null,
                    "�׹�ѹ���ź��¡�÷�����͡", "�׹�ѹ���ź", JOptionPane.YES_NO_OPTION)) {
                int ret = theHC.theSetupControl.deleteServiceLimitServicePoint(checkedIds.toArray(new ServiceLimitServicePoint[checkedIds.size()]));
                if (ret > 0) {
                    loadServiceLimitServicePoint();
                }
            }
        }
    }

    private void doSave() {
        if (sl != null) {
            sl.time_start = txtStartTime.getText();
            sl.time_end = txtEndTime.getText();
            sl.limit_appointment = limitApp.getValue();
            sl.limit_walkin = limitWalkin.getValue();
            if (theHC.theSetupControl.addNewServiceLimit(sl)) {
                loadServiceLimit();
                switchCard("show");
            }
        } else {
            slc.b_service_point_id = Gutil.getGuiData(cbServicePoint);
            slc.time_start = DateUtil.getTimeFromText(txtStartTime.getTextTime());
            slc.time_end = DateUtil.getTimeFromText(txtEndTime.getTextTime());
            slc.limit_appointment = limitApp.getValue();
            slc.limit_walkin = limitWalkin.getValue();
            if (theHC.theSetupControl.addNewServiceLimitServicePoint(slc)) {
                loadServiceLimitServicePoint();
                switchCard("show");
            }
        }
    }
}

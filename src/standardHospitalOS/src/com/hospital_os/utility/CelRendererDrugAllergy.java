/*
 * CelRendererDrugAllergy.java
 *
 * Created on 3 ����Ҥ� 2547, 10:38 �.
 */
package com.hospital_os.utility;

import java.awt.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.table.*;

/**
 *
 * @author tong
 */

public class CelRendererDrugAllergy implements TableCellRenderer {

    @Override
    public Component getTableCellRendererComponent(JTable table,
            Object value, boolean isSelected, boolean hasFocus,
            int row, int column) {
        try {
            if (value != null && ((String) value).equals("1")) {
                return patient_drugallergy;
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }

        return patient_nodrugallergy;
    }
    private JLabel patient_drugallergy = new JLabel(new ImageIcon(getClass().getResource(Gutil.getTextBundleImage("DRUGALLERGY")))); /**/

    private JLabel patient_nodrugallergy = new JLabel(new ImageIcon(getClass().getResource(Gutil.getTextBundleImage("NO_DRUGALLERGY"))));
    private static final Logger LOG = Logger.getLogger(CelRendererDrugAllergy.class.getName());
}

/*
 * PostpartumStatusResult.java
 *
 * Created on 20 �Զع�¹ 2548, 13:50 �.
 */
package com.pcu.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author amp
 */
@SuppressWarnings("ClassWithoutLogger")
public class PostpartumStatusResult extends Persistent {

    private static final long serialVersionUID = 1L;
    public String description = "";

    /**
     * Creates a new instance of PostpartumStatusResult
     */
    public PostpartumStatusResult() {
    }
}

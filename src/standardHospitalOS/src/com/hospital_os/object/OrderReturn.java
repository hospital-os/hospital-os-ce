/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;
import java.util.Date;

/**
 *
 * @author tanakrit
 */
public class OrderReturn extends Persistent {

    public String t_order_return_id;
    public String t_visit_id;
    public String t_order_id;
    public String b_item_id;
    public String f_item_group_id;
    public String order_common_name;
    public String dispense_qty = "0";
    public String return_qty = "0";
    public String receive_qty = "0";
    public Date return_date_time;
    public String user_return_id;
    public Date receiver_date_time;
    public String user_receiver_id;

}

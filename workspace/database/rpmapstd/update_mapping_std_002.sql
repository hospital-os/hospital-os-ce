CREATE TABLE IF NOT EXISTS r_aipn65_billgrcs (   
    id              CHARACTER VARYING(30) NOT NULL,
    description     TEXT NOT NULL,  
    CONSTRAINT r_aipn65_billgrcs_pk PRIMARY KEY (id)
);

INSERT INTO r_aipn65_billgrcs (id, description)
VALUES ('01','ค่าห้องค่าอาหาร')
    ,('02','อวัยวะเทียมและอุปกรณ์ในการบำบัดฯ')
    ,('03','ยาสารอาหารทางเส้นเลือดใช้ที่รพ.')
    ,('04','ยาสารอาหารทางเส้นเลือดใช้ที่บ้าน')
    ,('05','เวชภัณฑ์ที่มิใช่ยา')
    ,('06','บริการโลหิตและส่วนประกอบของโลหิต')
    ,('07','การตรวจทางเทคนิคการแพทย์และพยาธิฯ')
    ,('08','การตรวจวินิจฉัยและรักษาทางรังสีวิทยา')
    ,('09','การตรวจวินิจฉัยโดยวิธีพิเศษอื่น ๆ')
    ,('10','อุปกรณ์ของใช้และเครื่องมือทางการแพทย์')
    ,('11','การทําหัตถการและวิสัญญี')
    ,('12','บริการทางการพยาบาล')
    ,('13','บริการทางทันตกรรม')
    ,('14','บริการกายภาพบําบัดและเวชกรรมฟื้นฟู')
    ,('15','บริการฝังเข็ม/การบําบัดผู้ประกอบโรคศิลป์')
    ,('16','ห้องผ่าตัดและห้องคลอด')
    ,('17','ค่าธรรมเนียมบุคลากรทางการแพทย์')
    ,('88','ค่าบริการอื่น ๆ')
    ,('90','ไม่แยกหมวด (รวมทุกหมวด)')
    ,('91','ค่าธรรมเนียมพิเศษ (Surcharge)')
ON CONFLICT (id)
DO NOTHING;

CREATE TABLE IF NOT EXISTS b_map_aipn65_billgrcs (
    b_map_aipn65_billgrcs_id    CHARACTER VARYING(50) NOT NULL,
    r_aipn65_billgrcs_id        CHARACTER VARYING(50) NOT NULL,
    b_item_id                   CHARACTER VARYING(50) NOT NULL,  
    CONSTRAINT b_map_aipn65_billgrcs_pk PRIMARY KEY (b_map_aipn65_billgrcs_id),
    CONSTRAINT b_map_aipn65_billgrcs_un1 UNIQUE (r_aipn65_billgrcs_id,b_item_id)
);

INSERT INTO s_mapping_std VALUES ('2', '2', 'ReportMapStd Module', '1.2.0', '1.1.0', (select current_date) || ','|| (select current_time));

INSERT INTO s_script_update_log values ('ReportMapStd_Module','update_mapping_std_002.sql',(select current_date) || ','|| (select current_time),'Update ReportMapStd Module');

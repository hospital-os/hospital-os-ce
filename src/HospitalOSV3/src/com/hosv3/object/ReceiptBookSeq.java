/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.object;

import com.hospital_os.usecase.connection.Persistent;
import java.util.Date;

/**
 *
 * @author Somprasong 28102011
 */
public class ReceiptBookSeq extends Persistent {

    public int book_number;
    public int current_seq;
    public Date update_ts;
}

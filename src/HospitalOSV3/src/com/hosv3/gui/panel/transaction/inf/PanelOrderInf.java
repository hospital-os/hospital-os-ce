/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.gui.panel.transaction.inf;

import com.hospital_os.object.Item;
import com.hospital_os.object.ItemPrice;
import com.hospital_os.object.OrderItem;
import com.hospital_os.object.OrderItemDrug;
import com.hospital_os.object.Payment;
import com.hospital_os.object.Visit;
import java.beans.PropertyChangeListener;
import java.util.Vector;

/**
 *
 * @author Somprasong
 */
public interface PanelOrderInf {

    public void initComboBox();

    @SuppressWarnings("UseOfObsoleteCollectionType")
    public Vector getOrderItemV();

    public Item getItem();

    public OrderItemDrug getOrderItemDrug();

    public ItemPrice getItemPrice(OrderItem oi, Payment payment);

    public void showDialogStockReceiveDrug();

    public void showDialogOrderSet(Visit theVisit);

    public void showDialogOrderSet(PropertyChangeListener listener);
}

/*
 * OrderControl.java
 * henbe modify
 * Created on 17 ���Ҥ� 2546, 17:08 �.
 */
package com.hosv3.control;

import com.hospital_os.objdb.specialQuery.SpecialQueryOrderDrugDB;
import com.hospital_os.object.*;
import com.hospital_os.object.specialQuery.SpecialQueryDrugInteraction;
import com.hospital_os.object.specialQuery.SpecialQueryOrderHistoryDrug;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hospital_os.utility.Constant;
import com.hosv3.gui.dialog.DAskDoctorClinic;
import com.hosv3.gui.dialog.DAskDoctorOrder;
import com.hosv3.gui.dialog.DialogConfirmCancelWithCause;
import com.hosv3.gui.dialog.DialogPasswd;
import com.hosv3.gui.dialog.DialogWarningDrugAllergy;
import com.hosv3.object.DrugFrequency2;
import com.hosv3.object.HosObject;
import com.hosv3.object.QueueDispense2;
import com.hosv3.object.QueueLab2;
import com.hosv3.object.Uom2;
import com.hosv3.object.squery.SpecialQueryPatientDrugAllergy;
import com.hosv3.subject.HosSubject;
import com.hosv3.utility.DateUtil;
import com.hosv3.utility.DialogCauseCancelResultLab;
import com.hosv3.utility.DialogDrugInteraction;
import com.hosv3.utility.HosStdDatasource;
import com.pcu.object.Family;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author tong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class OrderControl {

    private static final Logger LOG = Logger.getLogger(BillingControl.class.getName());
    public static final int ReadLabResultItem_Call_LIMIT = 200;
    int result = 0;
    protected ConnectionInf theConnectionInf;
    protected HosDB theHosDB;
    protected HosObject theHO;
    protected HosSubject theHS;
    protected UpdateStatus theUS;
    protected DiagnosisControl theDiagnosisControl;
    protected SetupControl theSetupControl;
    protected VisitControl theVisitControl;
    protected LookupControl theLookupControl;
    protected String employeeSetContinue;
    protected SystemControl theSystemControl;
    protected DAskDoctorClinic theDialogUseDoctor;
    protected HosControl hosControl;

    /**
     * Creates a new instance of LookupControl
     *
     * @param con
     * @param ho
     * @param hdb
     * @param hs
     */
    public OrderControl(ConnectionInf con, HosObject ho, HosDB hdb, HosSubject hs) {
        theConnectionInf = con;
        theHosDB = hdb;
        theHS = hs;
        theHO = ho;
    }

    /*
     * public void setDepControl(VisitControl lc){
     */
    public void setDepControl(DiagnosisControl dc, VisitControl vc, LookupControl lc, SetupControl sc) {
        theVisitControl = vc;
        theLookupControl = lc;
        theDiagnosisControl = dc;
        theSetupControl = sc;
    }

    public void setSystemControl(SystemControl systemControl) {
        theSystemControl = systemControl;
    }

    public void setHosControl(HosControl hc) {
        hosControl = hc;
    }

    /**
     * @param item
     * @param patient_id
     * @return
     * @throws Exception
     *
     */
    protected boolean intCheckDrugAllergy(String item, String patient_id) throws Exception {
        return intListPatientDrugAllergy(patient_id, item) != null;
    }

    protected SpecialQueryPatientDrugAllergy intListPatientDrugAllergy(String patientId, String itemId) throws Exception {
        SpecialQueryPatientDrugAllergy sqpda = theHosDB.theSpecialQueryPatientDrugAllergyDB.selectByPatientIdItemId(patientId, itemId);
        // find item
        if (sqpda != null) {
            Vector<Item> items = theHosDB.theSpecialQueryItem2DB.listItemByStdId(sqpda.item_drug_standard_id);
            sqpda.items.addAll(items);
        }
        return sqpda;
    }

    /**
     * @Author amp
     * @date 23/03/2549
     * @see ��Ǩ�ͺ�ҷ����ѧ������� interaction �Ѻ�ҷ���������� ����
     * �����ѹ ���͵�駤������������
     * @param theOrderItem ���������
     * @return String �ͧ����͹
     * @throws Exception
     */
    protected String intCheckDrugInteraction(OrderItem theOrderItem) throws Exception {
        if (!theLookupControl.readOption().isUseDrugInteract()) {
            return "";
        }
        /*
         * amp:6/6/2549:������ա�û�Ѻ����¹�������� Method ���(��Ǩ�ͺ˹��
         * Transaction) ��س����仴� Method
         * OrderControl.intCheckItemDrugInteraction (����繡�õ�Ǩ�ͺ˹��
         * Setup)����
         */
        theHO.vOrderDrugInteraction = null;
        DrugStandardMapItem drugStandardMapItem = theHosDB.theDrugStandardMapItemDB.selectByItem(theOrderItem.item_code);
        StringBuilder drug_standard = new StringBuilder();
        boolean have_interaction = false;
        if (drugStandardMapItem == null || "".equals(drugStandardMapItem.drug_standard_id)) {
            return drug_standard.toString();
        }
        theHO.vOrderDrugInteraction = new Vector();
        drug_standard.append(theOrderItem.common_name).append(" ").append(("�ջ�ԡ����Ҵѧ���")).append(" \n");
        String sql = "select b_item_drug_interaction.*"
                + " ,stda1.item_drug_standard_description as item_drug_standard_description "
                + " ,stdb1.item_drug_standard_description as interaction_description "
                + " ,a1.b_item_id as standard_item "
                + " ,b1.b_item_id as interaction_item "
                + " from b_item_drug_interaction"
                + " inner join b_item_drug_standard_map_item "
                + "    as a1 on a1.b_item_drug_standard_id = b_item_drug_interaction.drug_standard_original_id"
                + " inner join b_item_drug_standard_map_item as "
                + "    b1 on b1.b_item_drug_standard_id = b_item_drug_interaction.drug_standard_interaction_id"
                + " INNER JOIN b_item_drug_standard as"
                + "    stda1 on stda1.b_item_drug_standard_id = a1.b_item_drug_standard_id  "
                + " INNER JOIN b_item_drug_standard as"
                + "    stdb1 on stdb1.b_item_drug_standard_id = b1.b_item_drug_standard_id "
                + " where a1.b_item_id = '" + theOrderItem.item_code + "'";
        Vector v = new Vector();
        ResultSet rs = theHosDB.theDrugInteractionDB.theConnectionInf.eQuery(sql);
        while (rs.next()) {
            DrugInteraction p = theHosDB.theDrugInteractionDB.rs2Object(rs);
            p.standard_item = rs.getString("standard_item");
            p.interaction_item = rs.getString("interaction_item");
            p.drug_standard_original_description = rs.getString("item_drug_standard_description");
            p.drug_standard_interaction_description = rs.getString("interaction_description");
            v.add(p);
        }
        rs.close();
        for (int j = 0; j < v.size(); j++) {
            DrugInteraction di = (DrugInteraction) v.get(j);
            for (int k = 0; k < theHO.vOrderItem.size(); k++) {
                OrderItem oi = (OrderItem) theHO.vOrderItem.get(k);
                if (di.interaction_item.equals(oi.item_code)) {
                    have_interaction = true;
                    drug_standard.append("\n ").append(("��ԡ����ҡѺ :")).append(" ").append(di.drug_standard_interaction_description);
                    drug_standard.append("\n ").append(("�дѺ�����ç :")).append("  ").append(di.force);
                    drug_standard.append("\n ").append(("��ԡ����ҷ����Դ :")).append(" ").append(di.act);
                    drug_standard.append("\n ").append(("�Ը���� :")).append(" ").append(di.repair).append("\n");
                    OrderDrugInteraction odi = theHO.initOrderDrugInteraction(
                            di, oi.getObjectId(), false);
                    theHO.vOrderDrugInteraction.addElement(odi);
                    break;
                }
            }
        }

        if (v.isEmpty()) {//��Ѻ���ǵ�Ǩ�ͺ
            sql = "select b_item_drug_interaction.*"
                    + " ,stda1.item_drug_standard_description as interaction_description "
                    + " ,stdb1.item_drug_standard_description as item_drug_standard_description "
                    + " ,a1.b_item_id as interaction_item "
                    + " ,b1.b_item_id as standard_item "
                    + " from b_item_drug_interaction"
                    + " inner join b_item_drug_standard_map_item "
                    + "    as a1 on a1.b_item_drug_standard_id = b_item_drug_interaction.drug_standard_original_id"
                    + " inner join b_item_drug_standard_map_item as "
                    + "    b1 on b1.b_item_drug_standard_id = b_item_drug_interaction.drug_standard_interaction_id"
                    + " INNER JOIN b_item_drug_standard as"
                    + "    stda1 on stda1.b_item_drug_standard_id = a1.b_item_drug_standard_id  "
                    + " INNER JOIN b_item_drug_standard as"
                    + "    stdb1 on stdb1.b_item_drug_standard_id = b1.b_item_drug_standard_id "
                    + " where b1.b_item_id = '" + theOrderItem.item_code + "'";
            v = new Vector();
            rs = theHosDB.theDrugInteractionDB.theConnectionInf.eQuery(sql);
            while (rs.next()) {
                DrugInteraction p = theHosDB.theDrugInteractionDB.rs2Object(rs);
                p.standard_item = rs.getString("standard_item");
                p.interaction_item = rs.getString("interaction_item");
                p.drug_standard_original_description = rs.getString("item_drug_standard_description");
                p.drug_standard_interaction_description = rs.getString("interaction_description");
                v.add(p);
            }
            rs.close();

            for (int j = 0; j < v.size(); j++) {
                DrugInteraction di = (DrugInteraction) v.get(j);
                for (int k = 0; k < theHO.vOrderItem.size(); k++) {
                    OrderItem oi = (OrderItem) theHO.vOrderItem.get(k);
                    String inter_des = di.drug_standard_interaction_description;
                    String inter_id = di.drug_standard_interaction_id;
                    di.drug_standard_interaction_description = di.drug_standard_original_description;
                    di.drug_standard_interaction_id = di.drug_standard_original_id;
                    di.drug_standard_original_description = inter_des;
                    di.drug_standard_original_id = inter_id;
                    if (di.interaction_item.equals(oi.item_code)) {
                        have_interaction = true;
                        drug_standard.append("\n ").append(("��ԡ����ҡѺ :")).append(" ").append(di.drug_standard_interaction_description);
                        drug_standard.append("\n ").append(("�дѺ�����ç :")).append("  ").append(di.force);
                        drug_standard.append("\n ").append(("��ԡ����ҷ����Դ :")).append(" ").append(di.act);
                        drug_standard.append("\n ").append(("�Ը���� :")).append(" ").append(di.repair).append("\n");
                        OrderDrugInteraction odi = theHO.initOrderDrugInteraction(
                                di, oi.getObjectId(), false);
                        theHO.vOrderDrugInteraction.addElement(odi);
                        break;
                    }
                }
            }
        }
        //��Ǩ�ͺ�Ѻ��駤����
        if ("1".equals(theHO.theVisit.pregnant)) {
            DrugInteraction di = theHosDB.theDrugInteractionDB.readPregnantInteraction(drugStandardMapItem.drug_standard_id);
            if (di != null) {
                have_interaction = true;
                drug_standard.append("\n ").append(("��ԡ����ҡѺ��õ�駤����"));
                drug_standard.append("\n ").append(("�дѺ�����ç :")).append(" ").append(di.force);
                drug_standard.append("\n ").append(("��ԡ����ҷ����Դ :")).append(" ").append(di.act);
                drug_standard.append("\n ").append(("�Ը���� :")).append(" ").append(di.repair).append("\n");
                OrderDrugInteraction odi = theHO.initOrderDrugInteraction(di, "", false);
                theHO.vOrderDrugInteraction.addElement(odi);
            }
        }
        if (have_interaction == false) {
            drug_standard.delete(0, drug_standard.length());
        }
        theOrderItem.vOrderDrugInteraction = theHO.vOrderDrugInteraction;
        theOrderItem.sDrugInteraction = drug_standard.toString();
        return drug_standard.toString();
    }

    /**
     * @param item
     * @return
     * @not deprecated henbe unused use intReadItemPriceByItemID() instead
     *
     * @author pu
     * @date 14/08/2549
     */
    public ItemPrice readItemPriceByItem(String item) {
        ItemPrice ip = new ItemPrice();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ip = intReadItemPriceByItem(item);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ip;
    }

    /**
     *
     * search final price of item in current datetime DentalModule Used this
     * function
     *
     * @param item Item Object
     * @return ItemPrice
     */
    public ItemPrice intReadItemPriceByItemID(String item) {
        try {
            return intReadItemPriceByItem(item);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            return new ItemPrice();
        }
    }

    public ItemPrice intReadItemPriceByItem(String item) throws Exception {
        Payment pm = new Payment();
        if (theHO.vVisitPayment != null && theHO.vVisitPayment.size() > 0) {
            pm = (Payment) theHO.vVisitPayment.get(0);
        }
        return intReadItemPriceByItem(item, pm);
    }

    public ItemPrice intReadItemPriceByItem(String itemId, Payment payment) throws Exception {
        Vector v = theHosDB.theItemPriceDB.selectByItem(itemId);
        if (v == null || v.isEmpty()) {
            throw new Exception("Price Not Found in item");
        }
        Vector<ItemPrice> vFinal = new Vector<>();
        for (int i = 0; i < v.size(); i++) {
            ItemPrice ip = (ItemPrice) v.get(i);
            // ����ѹ������Ҥ� ���ѹ�͹Ҥ� ��� remove �͡�ҡ vector ���
            if (DateUtil.countDateDiff(ip.active_date, theHO.date_time) > 0) {
                continue;
            }
            if (ip.item_price_id == null || ip.item_price_id.isEmpty()) {
                vFinal.add(ip);
                continue;
            }

            if (ip.item_price_id.equals(payment.plan_kid)) {
                return ip;
            }
        }
        if (vFinal.isEmpty()) {
            throw new Exception("Price Not Found in item (" + itemId + ").");
        }
        for (int i = 0; i < vFinal.size(); i++) {
            ItemPrice ip = (ItemPrice) vFinal.get(i);
            if (ip.b_tariff_id != null && ip.b_tariff_id.equals(payment.b_tariff_id)) {
                return ip;
            }
        }
        return (ItemPrice) vFinal.get(0);
    }

    public ItemPrice intReadItemPriceByItem(OrderItem oi) throws Exception {
        return intReadItemPriceByItem(oi.item_code);
    }

    public ItemPrice intReadItemPriceByItem(OrderItem oi, Payment payment) throws Exception {
        return intReadItemPriceByItem(oi.item_code, payment);
    }

    public Vector listOrderXrayReportedByVN(String vn) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theOrderItemDB.selectOrderItemReportedByVNCG(vn, CategoryGroup.isXray(), false);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listOrderXrayReportedAndCompleteByVN(String vn) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theOrderItemDB.selectOrderItemReportedCompletByVNCG(vn, CategoryGroup.isXray());
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listOrderXrayReportedInComplete(String visit_id) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theOrderItemDB.selectExeByVidCat(visit_id, CategoryGroup.isXray());
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public java.util.Vector listResultXraySizeByRxid(String resultXrayID) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (resultXrayID != null) {
                vc = theHosDB.theResultXraySizeDB.selectByResultXRayID(resultXrayID);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public boolean intCheckSaveXrayResult(ResultXRay theResultXRay, Vector vResultXraySize, Vector vResultXrayPosition) {
        if (theHO.theVisit == null) {
            theUS.setStatus(("��س����͡�����·������㹡�кǹ���"), UpdateStatus.WARNING);
            return false;
        }
        if (theHO.theVisit.visit_status.equals(VisitStatus.isOutProcess())) {
            theUS.setStatus(("�����¨���кǹ��������������ö��䢢�������"), UpdateStatus.WARNING);
            return false;
        }
        if (theResultXRay == null) {
            theUS.setStatus(("�ѧ��������͡��¡����硫�����"), UpdateStatus.WARNING);
            return false;
        }
        if (vResultXraySize == null || vResultXraySize.isEmpty()) {
            theUS.setStatus(("��辺�����Ţͧ�������硫�����"), UpdateStatus.WARNING);
            return false;
        }
        if (theResultXRay.result_complete.equals("1")) {
            theUS.setStatus(("���˹�ҷ����ӡ���觼�����") + " "
                    + ("�������ö�����"), UpdateStatus.WARNING);
            return false;
        }
        return true;
    }
    ///////////////////////////////////////////////////////////////////////////

    /**
     * @param rx
     * @param oi
     * @not deprecated ʧ���������� comment ���Ƿ��������áѹ��
     *
     * @author henbe
     * @parameter result xray, order item
     */
    public void saveXrayResult(ResultXRay rx, OrderItem oi) {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String date_time = theLookupControl.intReadDateTime();
            rx.record_time = date_time;
            rx.reporter = theHO.theEmployee.getObjectId();
            if (rx.getObjectId() == null) {
                //�ѹ�֡ŧ�ҹ�����Ţͧ�š�� Xray
                theHosDB.theResultXRayDB.insert(rx);
            } else {
                //�Ѿവŧ�ҹ������
                theHosDB.theResultXRayDB.update(rx);
            }
            //�ѹ�֡ orderItem �繴��Թ��� �����ʶҹ��� �׹�ѹ
            if (oi.status.equals(OrderStatus.VERTIFY)) {
                // insert into t_order_xray for pacs
                intInsertOrderXray(oi, theHO.theEmployee.getObjectId(), date_time);
                oi.executed_time = date_time;
                oi.executer = theHO.theEmployee.getObjectId();
                oi.status = OrderStatus.EXECUTE;
                theHosDB.theOrderItemDB.update(oi);
            }
            theHO.theXObject = rx;
            theHO.vXObject = null;
            theHO.vxo_index = null;
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT, "����硫����");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theResultSubject.notifySaveResultXray(("��úѹ�֡�š����硫����") + " "
                    + ("�������"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, "����硫����");
        }
    }

    /**
     * ��㹡�úѹ�֡੾���Ҥ���ҹ��
     *
     * @param oi �� Object �ͧ OrderItem ����բ����ŵ�ҧ�ú
     * @author padungrat(tong)
     */
    public void saveOrderXrayPrice(OrderItem oi) {
        if (oi == null || oi.getObjectId() == null) {
            theUS.setStatus(("�������¡�� Order Xray"), UpdateStatus.ERROR);
            return;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theOrderItemDB.updateXrayPrice(oi);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(("��úѹ�֡�Ҥ�") + " "
                    + ("�Դ��Ҵ"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theResultSubject.notifySaveFilmXray(("��úѹ�֡�Ҥҿ������硫����") + " "
                    + ("�������"), UpdateStatus.COMPLETE);
        }
    }

    public ResultXraySize saveResultXraySize(ResultXraySize resultXraySize, OrderItem oi, ResultXRay theResultXRay) {
        return saveResultXraySize(resultXraySize, oi, theResultXRay, null);
    }

    public ResultXraySize saveResultXraySize(ResultXraySize resultXraySize, OrderItem oi, ResultXRay theResultXRay, Vector vRxp) {
        if (theHO.theVisit == null) {
            theUS.setStatus(("�������ѧ����� visit"), UpdateStatus.WARNING);
            return null;
        }
        if (theHO.theVisit.visit_status.equals(VisitStatus.isOutProcess())) {
            theUS.setStatus(("�����¨���кǹ��������������ö��䢢�������"), UpdateStatus.WARNING);
            return null;
        }
        if (theResultXRay == null) {
            theUS.setStatus(("��������͡��¡����硫�����"), UpdateStatus.WARNING);
            return null;
        }
        if (theResultXRay.getObjectId() == null) {
            theUS.setStatus(("��س����͡��¡����硫�����"), UpdateStatus.WARNING);
            return null;
        }
        if (theResultXRay.result_complete.equals("1")) {
            theUS.setStatus(("���˹�ҷ����ӡ���觼�����") + " "
                    + ("�������ö�����"), UpdateStatus.WARNING);
            return null;
        }
        if (resultXraySize.num_film.isEmpty()
                || Integer.parseInt(resultXraySize.num_film) == 0) {
            theUS.setStatus(("��س��кبӹǹ�����"), UpdateStatus.WARNING);
            return null;
        }
        if (resultXraySize.damaging_film.isEmpty()) {
            theUS.setStatus(("��س��кبӹǹ���������"), UpdateStatus.WARNING);
            return null;
        }
        if (Integer.parseInt(resultXraySize.damaging_film)
                > Integer.parseInt(resultXraySize.num_film)) {
            theUS.setStatus(("�ӹǹ��������µ�ͧ���¡��Ҩӹǹ����������"), UpdateStatus.WARNING);
            return null;
        }
        if (resultXraySize.price.isEmpty()) {
            theUS.setStatus(("��س��к��Ҥҿ����"), UpdateStatus.WARNING);
            return null;
        }
        if (theResultXRay.result_complete.equals(Active.isEnable())) {
            theUS.setStatus(("�š����硫��������§ҹ�������������ö�����������硫������"), UpdateStatus.WARNING);
            return null;
        }
        if (vRxp == null || vRxp.isEmpty()) {
            theUS.setStatus(("�������ö�ѹ�֡���-��ҹ�繤����ҧ��"), UpdateStatus.WARNING);
            return null;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            resultXraySize.result_xray_id = theResultXRay.getObjectId();
            resultXraySize.order_item_id = oi.getObjectId();
            if (resultXraySize.getObjectId() == null) {
                theHosDB.theResultXraySizeDB.insert(resultXraySize);
            } else {
                theHosDB.theResultXraySizeDB.update(resultXraySize);
            }
//            theHosDB.theResultXrayPositionDB.deleteAllResultXrayPositionbyXraySizeID(resultXraySize.getObjectId());
            String activeIds = "";
            for (int i = 0; i < vRxp.size(); i++) {
                ResultXrayPosition resultXrayPosition = (ResultXrayPosition) vRxp.get(i);
                resultXrayPosition.active = "1";
                if (resultXrayPosition.getObjectId() == null) {
                    resultXrayPosition.order_result_xray_id = theResultXRay.getObjectId();
                    resultXrayPosition.xray_result_size_id = resultXraySize.getObjectId();
                    resultXrayPosition.order_item_id = oi.getObjectId();
                    theHosDB.theResultXrayPositionDB.insert(resultXrayPosition);
                } else {
                    theHosDB.theResultXrayPositionDB.update(resultXrayPosition);
                }
                activeIds += (activeIds.isEmpty() ? "" : ",") + "'" + resultXrayPosition.getObjectId() + "'";
            }
            if (activeIds.isEmpty()) {
                theHosDB.theResultXrayPositionDB.deleteAllResultXrayPositionbyXraySizeID(resultXraySize.getObjectId());
            } else {
                theHosDB.theResultXrayPositionDB.inactiveByActiveIds(resultXraySize.getObjectId(), activeIds);
            }
            if (resultXraySize.add_order.equals(Active.isEnable())) {
                // update order price = item_price + all film price
                Vector vResultXraySize = theHosDB.theResultXraySizeDB.selectByResultXRayID(theResultXRay.getObjectId());
                double filmPrice = 0d;
                for (Object object : vResultXraySize) {
                    ResultXraySize rxs = (ResultXraySize) object;
                    filmPrice += (Double.parseDouble(rxs.num_film) - Double.parseDouble(rxs.damaging_film)) * Double.parseDouble(rxs.price);
                }
                double itemPrice = Double.parseDouble(oi.price) - oi.order_xray_film_price;
                oi.price = Constant.doubleToDBString(itemPrice + filmPrice);
                oi.order_xray_film_price = filmPrice;
                theHosDB.theOrderItemDB.updateXrayPrice(oi);
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT, "�ӹǹ����� Xray �����");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theResultSubject.notifySaveFilmXray(("��úѹ�֡�����ſ������硫����") + " "
                    + ("�������"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, "�ӹǹ����� Xray �����");
        }
        return resultXraySize;
    }

    /**
     * �ӡ�úѹ�֡������� �����żš�� Xray ��� updateStatus OrderItem �����
     * ��§ҹ�� ��кѹ�֡��������������ŧ ���ҧ Film size
     *
     * @param rx
     * @param oi
     * @param vResultXrayPosition
     * @param resultXRayFilmSize
     */
    public void saveDataResultXrayReq(ResultXRay rx, OrderItem oi, Vector resultXRayFilmSize, java.util.Vector vResultXrayPosition) {
        double priceFilmXray = 0.00;
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            //�繼� Xray ����
            if ((rx.getObjectId() == null)) {
                //�ѹ�֡ŧ�ҹ�����Ţͧ�š�� Xray
                //���� ������ �����Ţͧ���ҧ order_result_xray_size
                int resultinsert = theHosDB.theResultXRayDB.insert(rx);
                if ((resultinsert == 1)) {
                    ResultXraySize theResultXraySize;
                    if ((resultXRayFilmSize != null)) {
                        priceFilmXray = 0.00;
                        for (int i = 0; i < resultXRayFilmSize.size(); i++) {
                            theResultXraySize = (ResultXraySize) resultXRayFilmSize.get(i);
                            theResultXraySize.result_xray_id = rx.getObjectId();
                            priceFilmXray += (Double.parseDouble(theResultXraySize.price)
                                    * Double.parseDouble(theResultXraySize.num_film));
                            // insert
                            if ((theResultXraySize.getObjectId() == null)) {
                                theResultXraySize.result_xray_id = rx.getObjectId();
                            } else {
                                //updateStatus
                                theHosDB.theResultXraySizeDB.update(theResultXraySize);
                            }
                            //tong
                            ResultXrayPosition theResultXrayPosition;
                            Vector vc = listResultXrayPositionByResultXraySizeID(theResultXraySize.getObjectId());
                            theConnectionInf.open();
                            if ((vc != null)) {
                                for (int j = 0; j < vc.size(); j++) {
                                    theResultXrayPosition = (ResultXrayPosition) vc.get(j);
                                    theResultXrayPosition.order_result_xray_id = rx.getObjectId();
                                    if ((theResultXrayPosition.getObjectId() == null)) {
                                        theHosDB.theResultXrayPositionDB.insert(theResultXrayPosition);
                                    } else {
                                        theHosDB.theResultXrayPositionDB.update(theResultXrayPosition);
                                    }
                                }
                            } else {//㹡óշ���ա�� updateStatus position ����� active �� 0
                                Vector vPosition = theHosDB.theResultXrayPositionDB.selectByResultXRaySizeIDNotActive(theResultXraySize.getObjectId());
                                if ((vPosition != null)) {
                                    for (int k = 0; k < vPosition.size(); k++) {
                                        theResultXrayPosition = (ResultXrayPosition) vPosition.get(k);
                                        theResultXrayPosition.order_result_xray_id = rx.getObjectId();
                                        if ((theResultXrayPosition.getObjectId() == null)) {
                                            theHosDB.theResultXrayPositionDB.insert(theResultXrayPosition);
                                        } else {
                                            theHosDB.theResultXrayPositionDB.update(theResultXrayPosition);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                priceFilmXray = Double.parseDouble(oi.price) + priceFilmXray;
                oi.price = String.valueOf(priceFilmXray);
                theHosDB.theOrderItemDB.update(oi);
                //theHosDB.theOrderItemDB.updatePrice(oi);
            } else {   //updateStatus ŧ�ҹ������
                theHosDB.theResultXRayDB.update(rx);
                if ((resultXRayFilmSize != null)) {
                    ResultXraySize theResultXraySize;
                    priceFilmXray = 0.00;
                    for (int i = 0; i < resultXRayFilmSize.size(); i++) {
                        theResultXraySize = (ResultXraySize) resultXRayFilmSize.get(i);
                        theResultXraySize.result_xray_id = rx.getObjectId();
                        priceFilmXray += Double.parseDouble(theResultXraySize.price);
                        if (theResultXraySize.getObjectId() == null) {
                            theHosDB.theResultXraySizeDB.insert(theResultXraySize);
                        } else {
                            theHosDB.theResultXraySizeDB.update(theResultXraySize);
                        }
                        //tong
                        ResultXrayPosition theResultXrayPosition;
                        Vector vc = listResultXrayPositionByResultXraySizeID(theResultXraySize.getObjectId());
                        theConnectionInf.open();
                        if ((vc != null)) {
                            for (int j = 0; j < vc.size(); j++) {
                                theResultXrayPosition = (ResultXrayPosition) vc.get(j);
                                theResultXrayPosition.order_result_xray_id = rx.getObjectId();
                                if (theResultXrayPosition.getObjectId() == null) {
                                    theHosDB.theResultXrayPositionDB.insert(theResultXrayPosition);
                                } else {
                                    theHosDB.theResultXrayPositionDB.update(theResultXrayPosition);
                                }
                            }
                        } else {//㹡óշ���ա�� updateStatus position ����� active �� 0
                            Vector vPosition = theHosDB.theResultXrayPositionDB.selectByResultXRaySizeIDNotActive(theResultXraySize.getObjectId());
                            if ((vPosition != null)) {
                                for (int k = 0; k < vPosition.size(); k++) {
                                    theResultXrayPosition = (ResultXrayPosition) vPosition.get(k);
                                    theResultXrayPosition.order_result_xray_id = rx.getObjectId();
                                    if ((theResultXrayPosition.getObjectId() == null)) {
                                        do {
                                        } while (theHosDB.theResultXrayPositionDB.insert(theResultXrayPosition) == 0);
                                    } else {
                                        theHosDB.theResultXrayPositionDB.update(theResultXrayPosition);
                                    }
                                }
                            }
                        }
                    }
                }
                priceFilmXray = Double.parseDouble(oi.price) + priceFilmXray;
                oi.price = String.valueOf(priceFilmXray);
                theHosDB.theOrderItemDB.update(oi);
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(("��úѹ�֡�š����硫����") + " "
                    + ("�Դ��Ҵ"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theResultSubject.notifySaveResultXray(("��úѹ�֡�š����硫����") + " "
                    + ("�������"), UpdateStatus.COMPLETE);
        }
    }

    /**
     * ��㹵�Ǩ�ͺ����� �ش�ź�������
     *
     * @param oi �� Object OrderItem
     * @param oi_out �� Vector �ͧ Object OrderItem
     * @return
     * @throws Exception
     * @author Pongtorn(Henbe)
     * @date 17/03/49,18:42
     */
    protected boolean isLabGroup(OrderItem oi, Vector oi_out) throws Exception {
        Vector lgv = theHosDB.theLabGroupDB.selectAll();
        for (int j = 0; j < lgv.size(); j++) {
            LabGroup lg = (LabGroup) lgv.get(j);
            if (lg.item_id.equals(oi.item_code)) {
                if (oi_out != null) {
                    oi_out.add(oi);
                }
                return true;
            }
        }
        return false;
    }

    /**
     * ��㹡�����ҧ�����š����� Lab ����ͧ Lab
     *
     * @param oi �� Object OrderItem
     * @return
     * @throws Exception
     * @author Pongtorn(Henbe)
     * @date 17/03/49,18:42
     */
    protected int generateResultLab(OrderItem oi) throws Exception {
        int ret = 0;
        Vector labResultItems = new Vector();
        intReadLabResultItem(labResultItems, oi.item_code, true);
        Patient patient = hosControl.theHosDB.thePatientDB.selectByPK(oi.hn);
        Family family = patient == null ? null : patient.getFamily();
        for (int i = 0, size = labResultItems.size(); i < size; i++) {
            LabResultItem lri = (LabResultItem) labResultItems.get(i);
            ResultLab rl = new ResultLab();
            rl.name = lri.name;
            rl.result = "";
            rl.unit = lri.unit;
            rl.order_item_id = oi.getObjectId();
            rl.item_id = lri.item_id;
            rl.active = Active.isEnable();
            rl.result_complete = Active.isDisable();
            rl.visit_id = theHO.theVisit.getObjectId();
            rl.result_type_id = lri.resultType;
            rl.normal_range_type = lri.normal_range_type;
            switch (Integer.parseInt(lri.normal_range_type)) {
                case 1:
                    // gender
                    LabResultItemGender itemGender = hosControl.theSetupControl.getLabResultItemGenderByLabResultItemId(
                            lri.getObjectId());
                    if (itemGender != null) {
                        if (family != null && "1".equals(family.f_sex_id)) {
                            rl.min = itemGender.male_result_min == null ? "" : itemGender.male_result_min;
                            rl.max = itemGender.male_result_max == null ? "" : itemGender.male_result_max;
                            rl.critical_min = itemGender.male_result_critical_min == null ? "" : itemGender.male_result_critical_min;
                            rl.critical_max = itemGender.male_result_critical_max == null ? "" : itemGender.male_result_critical_max;
                        } else if (family != null && "2".equals(family.f_sex_id)) {
                            rl.min = itemGender.female_result_min == null ? "" : itemGender.female_result_min;
                            rl.max = itemGender.female_result_max == null ? "" : itemGender.female_result_max;
                            rl.critical_min = itemGender.female_result_critical_min == null ? "" : itemGender.female_result_critical_min;
                            rl.critical_max = itemGender.female_result_critical_max == null ? "" : itemGender.female_result_critical_max;
                        } else {
                            rl.min = "";
                            rl.max = "";
                            rl.critical_min = "";
                            rl.critical_max = "";
                        }
                    }
                    break;
                case 2:
                    if (family == null) {
                        rl.max = "";
                        rl.min = "";
                        rl.critical_min = "";
                        rl.critical_max = "";
                    } else {
                        // ages
                        List<LabResultItemAge> list = hosControl.theSetupControl.listLabResultItemAgeByLabResultItemId(lri.getObjectId());
                        int age = DateUtil.calculateAgeInt(family.patient_birthday, hosControl.theHO.date_time);
                        for (LabResultItemAge itemAge : list) {
                            int ageMin = Integer.parseInt(itemAge.age_min);
                            int ageMax = Integer.parseInt(itemAge.age_max);
                            if (ageMin <= age && age <= ageMax) {
                                if ("1".equals(family.f_sex_id)) {
                                    rl.min = itemAge.male_min == null ? "" : itemAge.male_min;
                                    rl.max = itemAge.male_max == null ? "" : itemAge.male_max;
                                    rl.critical_min = itemAge.male_critical_min == null ? "" : itemAge.male_critical_min;
                                    rl.critical_max = itemAge.male_critical_max == null ? "" : itemAge.male_critical_max;
                                } else if ("2".equals(family.f_sex_id)) {
                                    rl.min = itemAge.female_min == null ? "" : itemAge.female_min;
                                    rl.max = itemAge.female_max == null ? "" : itemAge.female_max;
                                    rl.critical_min = itemAge.female_critical_min == null ? "" : itemAge.female_critical_min;
                                    rl.critical_max = itemAge.female_critical_max == null ? "" : itemAge.female_critical_max;
                                } else {
                                    rl.min = "";
                                    rl.max = "";
                                    rl.critical_min = "";
                                    rl.critical_max = "";
                                }
                            }
                        }
                    }
                    break;
                case 3:
                    // texts
                    rl.min = "";
                    rl.max = "";
                    rl.critical_min = "";
                    rl.critical_max = "";
                    List<LabResultItemText> lrits = hosControl.theSetupControl.listLabResultItemTextByLabResultItemId(
                            lri.getObjectId());
                    rl.default_values = new String[lrits.size()];
                    for (int j = 0; j < rl.default_values.length; j++) {
                        rl.default_values[j] = lrits.get(j).result_text;
                    }
                    break;
                case 4:
                    String sql = lri.default_value + " where t_visit_id = '" + oi.visit_id + "'";
                    if (sql.startsWith("select")) {
                        ResultSet rs = theConnectionInf.eQuery(sql);
                        if (rs.next()) {
                            rl.min = rs.getString("min");
                            rl.max = rs.getString("max");
                            rl.critical_min = rs.getString("critical_min");
                            rl.critical_max = rs.getString("critical_max");
                        }
                    }
                    break;
                default:
                    rl.min = "";
                    rl.max = "";
                    rl.critical_min = "";
                    rl.critical_max = "";
                    break;
            }
            rl.result_group_id = lri.labresult_id;
            rl.index = String.valueOf(i);
            if (rl.index.length() == 1) {
                rl.index = "0" + rl.index;
            }
            rl.lab_result_item_id = lri.getObjectId();
            String sql = "update t_result_lab"
                    + " set result_lab_index = '" + rl.index + "' "
                    + " where t_order_id = '" + rl.order_item_id + "' "
                    + " and b_item_lab_result_id = '" + rl.lab_result_item_id + "'";
            if (theConnectionInf.eUpdate(sql) == 0) {
                ret += theHosDB.theResultLabDB.insert(rl);
            }
        }
        return ret;
    }

    private DAskDoctorOrder dAskDoctorOrder = null;

    /**
     * ��㹡���׹�ѹ��¡�� Order ��зӡ�úѹ�֡�����������駹��� Update ��¡��
     * Order ���� ��¡�� orderitemdrug �� �����¡�� Order Item Drug �� null
     * ��кѹ�֡੾�� ��¡�� order ��ҹ�� ��������� OrderItem ,
     * OrderItemDrug
     *
     * @param orderItems �� Object OrderItem
     * @param select_row
     * @return
     * @author Pongtorn(Henbe)
     * @date 17/03/49,18:42
     */
    public boolean verifyOrderItem(Vector orderItems, int[] select_row) {
        if (select_row == null) {
            select_row = new int[orderItems.size()];
            for (int i = 0; i < select_row.length; i++) {
                select_row[i] = i;
            }
        }
        if (select_row.length == 0) {
            theUS.setStatus("�ѧ��������͡��¡�õ�Ǩ�ѡ��", UpdateStatus.WARNING);
            return false;
        }
        Vector ois = new Vector();
        for (int i = 0; i < select_row.length; i++) {
            OrderItem oi = (OrderItem) orderItems.get(select_row[i]);
            ois.add(oi);
        }
        return verifyOrderItem(ois);
    }

    public boolean verifyOrderItem(Vector<OrderItem> orderItems) {
        if (theHO.theVisit == null) {
            theUS.setStatus("��س����͡�����·������㹡�кǹ���", UpdateStatus.WARNING);
            return false;
        }
        if (theHO.theVisit.visit_status.equals(VisitStatus.isOutProcess())) {
            theUS.setStatus("�����¨���кǹ��������������ö��䢢�������", UpdateStatus.WARNING);
            return false;
        }
        if (!HosObject.isOrderStatus(orderItems, OrderStatus.NOT_VERTIFY)) {
            theUS.setStatus("����¡�õ�Ǩ�ѡ�ҷ�������ʶҹз���������ö�׹�ѹ��", UpdateStatus.WARNING);
            return false;
        }
        if (theLookupControl.readOption().verify.equals(Active.isEnable())) {
            boolean retb = DialogPasswd.showDialog(theHO, theUS, theHO.theEmployee.password);
            if (!retb) {
                theUS.setStatus(("���ʼ�ҹ���١��ͧ"), UpdateStatus.WARNING);
                return false;
            }
        }

        boolean isOrderDoctor = "1".equals(hosControl.theLookupControl.readOption(false).enable_order_doctor);
        if (isOrderDoctor && dAskDoctorOrder == null) {
            dAskDoctorOrder = new DAskDoctorOrder(theUS, theLookupControl);
        }
        Map<String, Object> resultMap = null;

        List<OrderItem> ordersToVerify = new ArrayList<OrderItem>();

        orderItems = addOrderChildByParent(orderItems);

        for (OrderItem theOrderItem : orderItems) {
            if (!theOrderItem.status.equals(OrderStatus.NOT_VERTIFY)
                    || (!theOrderItem.status.equals(OrderStatus.NOT_VERTIFY) && theOrderItem.isChildPackage())) {
                continue;
            }
            // ��Ǩ�ͺ��¡�� ä� ᾷ��
            if (isOrderDoctor) {
                if (isMappedOrderDoctorByOrderId(theOrderItem.getObjectId())) {
                    // �������㹡���� ä� ᾷ�� ���������ᾷ���� login ����
                    if (theHO.theEmployee.authentication_id.equals(Authentication.DOCTOR)
                            || theHO.theEmployee.authentication_id.equals(Authentication.DENT)
                            || theHO.theEmployee.authentication_id.equals(Authentication.THAI_DOCTOR)) {
                        theOrderItem.order_by_doctor = theHO.theEmployee.getObjectId();
                        theOrderItem.order_by_doctor_type = "0";
                    } else {
                        if (resultMap == null) {
                            resultMap = dAskDoctorOrder.showDialog(theOrderItem.common_name);
                            // if cancel dialog skip this order item to verify
                            if (resultMap == null) {
                                continue;
                            }
                        }
                        theOrderItem.order_by_doctor = (String) resultMap.get(DAskDoctorOrder.DOCTOR_ID);
                        theOrderItem.order_by_doctor_type = (String) resultMap.get(DAskDoctorOrder.ORDER_TYPE);
                        if (!(Boolean) resultMap.get(DAskDoctorOrder.USE_ALL)) {
                            resultMap = null;
                        }
                    }
                }
            }
            ordersToVerify.add(theOrderItem);
        }

        if (ordersToVerify.isEmpty()) {
            return false;
        }

        StringBuilder sb = new StringBuilder();
        theConnectionInf.open();
        boolean returnResult = false;
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String date_time = theLookupControl.intReadDateTime();
            for (OrderItem orderItem : ordersToVerify) {
                boolean ret = intVerifyOrderItem(orderItem, date_time, theHO.theEmployee.getObjectId());
                if (ret) {
                    sb.append(orderItem.getObjectId()).append("|");
                }
            }
            // QueueLabStatus
            theHO.theListTransfer = theHosDB.theQueueTransferDB.select2ByVisitID(theHO.theVisit.getObjectId());
            HashMap<String, String> status = OrderControl.checkLabAndXrayStatus(orderItems);
            if (theHO.theListTransfer != null) {
                theHO.theListTransfer.labstatus = status.get("lab");
                theHO.theListTransfer.xraystatus = status.get("xray");
                theHosDB.theQueueTransferDB.update(theHO.theListTransfer);
            }
            theHO.theVisit.queue_lab_status = status.get("lab");
            theHO.theVisit.queue_xray_status = status.get("xray");
            theHosDB.theVisitDB.updateLabAndXrayStatus(theHO.theVisit);
            // Update ������Ѻ theHO.vDiagIcd9 sumo 05/09/2549
            theHO.vOrderItem = theHosDB.theOrderItemDB.selectByVidTypeCancel(theHO.theVisit.getObjectId(), "", false);
            theHO.vDiagIcd9 = theHosDB.theDiagIcd9DB.selectByVid(theHO.theVisit.getObjectId(), Active.isEnable());

            theHO.theXObject = null;
            theHO.vXObject = orderItems;
            theConnectionInf.getConnection().commit();
            returnResult = true;
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.UNKONW, "�׹�ѹ��¡�� Order");
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        // ��Һѹ�֡����稤��� notify 仺͡
        if (returnResult) {
            theHS.theOrderSubject.notifyVerifyOrderItem(sb.toString().isEmpty() ? "" : sb.toString().substring(0, sb.toString().length() - 1), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "�׹�ѹ��¡�� Order");
        }
        return returnResult;
    }

    public boolean intVerifyOrderItem(OrderItem theOrderItem, String date_time, String eid) throws Exception {
        return intVerifyOrderItem(theOrderItem, date_time, eid, true);
    }

    public boolean intVerifyOrderItem(OrderItem theOrderItem, String date_time, String eid, boolean isCheckDefaultPrice) throws Exception {
        if (theLookupControl.readOption().isUseDrugInteract()) {
            String drug_interaction = intCheckDrugInteraction(theOrderItem);
            if (!"".equals(drug_interaction)) {
                int ask1 = DialogDrugInteraction.showDialog(theUS, drug_interaction);
                if (ask1 != 1) {
                    return false;
                }
            }
        }
        //������͡��੾����¡�÷���ѧ����׹�ѹ��ҹ�鹶������������ͧ������
        theOrderItem.vertifier = eid;
        theOrderItem.vertify_time = date_time;
        theOrderItem.status = OrderStatus.VERTIFY;
        CategoryGroupItem selectServiceById = hosControl.theHosDB.theCategoryGroupItemDB.selectServiceById(theOrderItem.item_group_code_category);

        // Somprasong 18-03-2012 update price
        // 13-06-2012 for module dental can edit price
        if (isCheckDefaultPrice && selectServiceById == null && !theOrderItem.isChildPackage()) {
            ItemPrice itemPrice = intReadItemPriceByItem(theOrderItem);
            theOrderItem.price = theOrderItem.isEditedPrice() // �������Ҥ�����������ͧ�Ѿഷ����Է��
                    ? theOrderItem.price
                    : Constant.doubleToDBString(theOrderItem.isOPDPrice()
                            ? itemPrice.price
                            : itemPrice.price_ipd);
            theOrderItem.order_cost = Constant.doubleToDBString(itemPrice.price_cost);
            theOrderItem.order_share_doctor = itemPrice.item_share_doctor;
            theOrderItem.order_share_hospital = itemPrice.item_share_hospital;
            theOrderItem.order_editable_price = itemPrice.item_editable_price;
            theOrderItem.order_limit_price_min = itemPrice.item_limit_price_min;
            theOrderItem.order_limit_price_max = itemPrice.item_limit_price_max;
            theOrderItem.use_price_claim = itemPrice.use_price_claim;
            theOrderItem.order_price_claim = itemPrice.item_price_claim;
        }
        theHosDB.theOrderItemDB.update(theOrderItem);
        //���������ҡ����ѹ�֡ŧ�������
        //�������¡�� �ź ���ѹ�֡�ŷѹ��������ֹ�ѹ��¡��
        if (theOrderItem.isLab()) {
            intSaveQueueOfOrder(theOrderItem, true, date_time);//����Ż
            generateResultLab(theOrderItem);
        }
        //�������¡�� xray ���ѹ�֡�ŷѹ��������ֹ�ѹ��¡��
        if (theOrderItem.isXray()) {
            theHosDB.theVisitDB.updateXrayStatus("1", theHO.theVisit.getObjectId());
            intSaveQueueOfOrder(theOrderItem, true, date_time);//��� xray
            ResultXRay rx = theHO.initResultXRay(theOrderItem.getObjectId());
            theHosDB.theResultXRayDB.insert(rx);
        }
        //�������¡���Ҩ��ա�úѹ�֡��������´�ͧ�ҷء���駷���ա������¹ʶҹ�����
        if (theOrderItem.isDrug()) {
            OrderItemDrug oid = theHosDB.theOrderItemDrugDB.selectByOrderItemID(theOrderItem.getObjectId());
            oid.modify = eid;
            oid.modify_time = date_time;
            theHosDB.theOrderItemDrugDB.inActive(oid);
            oid.setObjectId(null);
            oid.active = Active.isEnable();
            oid.status = OrderStatus.VERTIFY;
            theHosDB.theOrderItemDrugDB.insert(oid);
            ////Drug Interaction
            //amp:27/03/2549 �����纻���ѵԡ������ҷ���ջ�ԡ����Ҵ���
            if (theLookupControl.readOption().isUseDrugInteract()
                    && theHO.vOrderDrugInteraction != null) {
                for (int i = 0, size = theHO.vOrderDrugInteraction.size(); i < size; i++) {
                    OrderDrugInteraction theOdi = (OrderDrugInteraction) theHO.vOrderDrugInteraction.get(i);
                    theOdi.order_item_id = theOrderItem.getObjectId();
                    if (theHosDB.theOrderDrugInteractionDB.selectByItemAndInteractionItem(theOdi.order_item_id, theOdi.interaction_item_id).isEmpty()) {
                        theHosDB.theOrderDrugInteractionDB.insert(theOdi);
                        OrderDrugInteraction theOdi2 = new OrderDrugInteraction(theOdi);
                        theOdi2.order_item_id = theOdi.interaction_item_id;
                        theOdi2.order_item_drug_standard_id = theOdi.interaction_item_drug_standard_id;
                        theOdi2.order_item_drug_standard_description = theOdi.interaction_item_drug_standard_description;
                        theOdi2.interaction_item_id = theOdi.order_item_id;
                        theOdi2.interaction_item_drug_standard_id = theOdi.order_item_drug_standard_id;
                        theOdi2.interaction_item_drug_standard_description = theOdi.order_item_drug_standard_description;
                        theHosDB.theOrderDrugInteractionDB.insert(theOdi2);
                    }
                }
            }
        }
        // �ѹ�֡ DiagICD9 �ѵ��ѵ� 㹡óշ���繼����¹͡����Դ Option �˹�� Setup sumo 05/09/2549
        // ��Ǩ�ͺ��� Order �������׹�ѹ�繷ѹ��������ͤ�Һ�ԡ��������� sumo 05/09/2549
        // ��Ǩ�ͺ��� Order ����׹�ѹ�ա�èѺ���Ѻ DiagIcd9 ����������� sumo 05/09/2549
        // list ������ DiagIcd9 �����ѹ�֡������ Visit �Ѩ�غѹ��������� henbe 13/09/2549
        // ���� Icd9 ���Ѻ���������� DB �������
        // ��ء case ���¡��� ipd ���� opd
        if (theOrderItem.isService()
                || theOrderItem.isDental()) {
            if (theLookupControl.readOption().auto_diag_icd9.equals(Active.isEnable())) {
                Vector vItem_service = theHosDB.theItemServiceDB.selectByItem(theOrderItem.item_code);
                if (!vItem_service.isEmpty()) {
                    ItemService is = (ItemService) vItem_service.get(0);
                    if (!is.icd9_code.isEmpty()) {
                        // Somprasong 211011 ������������������ѧ �������ͧŧ auto
                        DiagIcd9 selectIcd9 = theHosDB.theDiagIcd9DB.selectIcd9(is.icd9_code, theOrderItem.visit_id);
                        if (selectIcd9 == null) {
                            ICD9 icd9 = theHosDB.theICD9DB.selectById(is.icd9_code);
                            if (icd9 != null) {
                                DiagDoctorClinic dc = theDialogUseDoctor.showDialog("3".equals(theHO.theEmployee.authentication_id) ? theHO.theEmployee.getObjectId() : null);
                                DiagIcd9 theDiagIcd9 = theHO.initDiagIcd9(icd9, dc);
                                theDiagnosisControl.intSaveDiagIcd9(null, theDiagIcd9, theHO.vDiagIcd9);
                            }
                        }
                    }
                }
            }
        }
        return true;
    }

    /**
     * �ӡ��ź��§ҹ����硫�����
     *
     * @param rx
     * @param oi
     */
    public void deleteResultXray(ResultXRay rx, OrderItem oi) {
        double priceFilmXray = 0.00;
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            oi.status = OrderStatus.VERTIFY;
            theHosDB.theOrderItemDB.update(oi);
            Vector vResultXraysize = new Vector();
            if (rx != null && rx.getObjectId() != null) {
                theHosDB.theResultXRayDB.update(rx);
                vResultXraysize.addAll(theHosDB.theResultXraySizeDB.selectByResultXRayID(rx.getObjectId()));
            }
            if (vResultXraysize.isEmpty()) {
                theUS.setStatus(("��س����͡��¡�ü���硫�������ͧ���ź"), UpdateStatus.ERROR);
                return;
            }
            for (int i = 0; i < vResultXraysize.size(); i++) {
                ResultXraySize rxz = (ResultXraySize) vResultXraysize.get(i);
                Vector vResultXrayPosition = theHosDB.theResultXrayPositionDB.selectByResultXRaySizeID(rxz.getObjectId());
                priceFilmXray += (Double.parseDouble(rxz.price)
                        * Double.parseDouble(rxz.num_film));
                for (int j = 0; j < vResultXrayPosition.size(); j++) {
                    ResultXrayPosition rxp = (ResultXrayPosition) vResultXrayPosition.get(j);
                    rxp.active = Active.isDisable();
                    theHosDB.theResultXrayPositionDB.update(rxp);
                }
                rxz.active = Active.isDisable();
                theHosDB.theResultXraySizeDB.update(rxz);
            }
            /////////////////////////////////////////////////////////////////
            //�ѹ�֡orderItem
            if (priceFilmXray > 0) {
                priceFilmXray = Double.parseDouble(oi.price) - priceFilmXray;
                oi.price = String.valueOf(priceFilmXray);
                theHosDB.theOrderItemDB.update(oi);
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            theUS.setStatus(("���ź����硫����") + " "
                    + ("�Դ��Ҵ"), UpdateStatus.ERROR);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theResultSubject.notifyDeleteResultXray(("���ź����硫�����") + " "
                    + ("������"), UpdateStatus.COMPLETE);
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    /**
     * �зӡ������¹�š����§ҹ ��Ҵ�蹿����
     *
     * @param rxs
     * @param rx
     * @param oi
     * @return
     */
    public int deleteResultXraySizeByKeyID(ResultXraySize rxs, ResultXRay rx, OrderItem oi) {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            rxs.active = Active.isDisable();
            OrderItem theOrderItem = theHosDB.theOrderItemDB.selectByPK(rxs.order_item_id);
            //��䢨ӹǹ�Ҥ�ŧ
            if ((theOrderItem != null)) {
                double filmPrice = !rxs.add_order.equals("1") ? 0.0d : (Double.parseDouble(rxs.num_film) - Double.parseDouble(rxs.damaging_film)) * Double.parseDouble(rxs.price);
                double price = Double.parseDouble(oi.price) - filmPrice;
                oi.price = Constant.doubleToDBString(price);
                oi.order_xray_film_price = oi.order_xray_film_price - filmPrice;
                theHosDB.theOrderItemDB.update(oi);
            }
            if ((rxs.result_xray_id != null) && (!"".equals(rxs.result_xray_id))
                    && (!"null".equals(rxs.result_xray_id)) && rx.result_complete.equals(Active.isEnable())) {
                theHosDB.theResultXraySizeDB.update(rxs);
            } else {
                theHosDB.theResultXraySizeDB.delete(rxs);
            }
            Vector vcposition = theHosDB.theResultXrayPositionDB.selectByResultXRaySizeID(rxs.getObjectId());
            if ((vcposition != null)) {
                for (int i = 0, size = vcposition.size(); i < size; i++) {
                    ResultXrayPosition theResultXrayPosition = (ResultXrayPosition) vcposition.get(i);
                    theResultXrayPosition.active = Active.isDisable();
                    if ((theResultXrayPosition.order_result_xray_id != null)
                            && (!theResultXrayPosition.order_result_xray_id.isEmpty())
                            && (!theResultXrayPosition.order_result_xray_id.equals("null"))
                            && rx.result_complete.equals(Active.isEnable())) {
                        theHosDB.theResultXrayPositionDB.update(theResultXrayPosition);
                    } else {
                        theHosDB.theResultXrayPositionDB.delete(theResultXrayPosition);
                    }
                }
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_DELETE, "��������´�ͧ�����");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theResultSubject.notifyDeleteFilmXray(("���ź�����") + " "
                    + ("�������"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE, "��������´�ͧ�����");
        }
        return 0;
    }

    /**
     * ���¡������͵�ͧ����觼�Xray
     *
     * @param xrayreported
     */
    public void saveXrayReportComplete(Vector xrayreported) {
        if (theHO.theVisit == null) {
            theUS.setStatus(("�ѧ��������͡������"), UpdateStatus.WARNING);
            return;
        }
        if (theHO.theVisit.visit_status.equals(VisitStatus.isOutProcess())) {
            theUS.setStatus(("�����¨���кǹ��������������ö��䢢�������"), UpdateStatus.WARNING);
            return;
        }
        if (xrayreported == null) {
            theUS.setStatus(("��辺��¡����硫������зӡ����"), UpdateStatus.WARNING);
            return;
        }
        boolean isComplete = false;
        String visitID = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector vXray = theHosDB.theOrderItemDB.selectXrayByVid(theHO.theVisit.getObjectId());
            if (vXray != null && xrayreported.size() < vXray.size()) {
                boolean confirm = theUS.confirmBox(("�պҧ��¡�÷���ѧ�����ѹ�֡��") + " "
                        + ("�׹�ѹ������"), UpdateStatus.WARNING);
                if (!confirm) {
                    throw new Exception("Cancel");
                }
            }
            for (int i = 0, size = xrayreported.size(); i < size; i++) {
                OrderItem theOrderItem = (OrderItem) xrayreported.get(i);
                ResultXRay theResultXRay = theHosDB.theResultXRayDB.selectOrderItemByVNItemId(theOrderItem.getObjectId(), theOrderItem.visit_id);
                if (theResultXRay != null && theResultXRay.result_complete.equals(Active.isDisable())) {
                    theResultXRay.result_complete = Active.isEnable();
                    theResultXRay.reporter = theHO.theEmployee.getObjectId();
                    theResultXRay.pacs_status = "0"; // reset to 0 for flag to pacs update this report too.
                    theHosDB.theResultXRayDB.update(theResultXRay);
                }
                theOrderItem.status = OrderStatus.REPORT; // �������§ҹ���������͡� �觼�
                theOrderItem.order_complete = Active.isEnable();
                theOrderItem.reporter = theHO.theEmployee.getObjectId();
                theOrderItem.reported_time = theLookupControl.getTextCurrentDateTime();
                theHosDB.theOrderItemDB.update(theOrderItem);
            }
            theHosDB.theQueueXrayDB.deleteByVisitID(theHO.theVisit.getObjectId());
            visitID = theHO.theVisit.getObjectId();
            refreshXrayStatus(theHO.theVisit.getObjectId());
            theVisitControl.intUnlockVisit(theHO.theVisit);
            theHO.clearFamily();
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            if (ex.getMessage().equals("Cancel")) {
                theUS.setStatus("¡��ԡ�¼����", UpdateStatus.WARNING);
            } else {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                hosControl.updateTransactionFail(HosControl.UNKONW, "�觼š����硫����");
            }

        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theResultSubject.notifyXrayReportComplete(visitID, UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "�觼š����硫����");
        }
    }

    public boolean receiveReturnDrug(OrderItemReceiveDrug oird) {
        if (!"".equals(oird.qty_receive)) {
            double receive;
            try {
                receive = Double.parseDouble(oird.qty_receive);
            } catch (Exception ex) {
                theUS.setStatus("�ӹǹ����Ѻ�׹��ͧ�繵���Ţ", UpdateStatus.WARNING);
                return false;
            }
            if (receive > Double.parseDouble(oird.qty_return)) {
                theUS.setStatus("�ӹǹ����Ѻ�׹��ͧ����ҡ���Ҩӹǹ���׹��", UpdateStatus.WARNING);
                return false;
            }
        } else {
            theUS.setStatus(("������кبӹǹ���ǡ�سҡ����� Enter �������������Ѻ�����š�͹������ �ѹ�֡"), UpdateStatus.WARNING);
            return false;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theOrderItemReceiveDrugDB.update(oird);
            theHO.vOrderItemReceiveDrug = theHosDB.theOrderItemReceiveDrugDB.selectOIRDByVId(theHO.theVisit.getObjectId());

            theHO.theXObject = oird;
            theHO.vXObject = null;
            theHO.vxo_index = null;
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT, "��ä׹��");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theOrderSubject.notifyReceiveReturnDrug(
                    "��ä׹�Ңͧ�������������", UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, "��ä׹��");
        }
        return isComplete;
    }

    /*
     * ��úѹ�֡��ä׹�Ҩкѹ�֡�ӹǹ�������������ѧ�ҡ�׹����
     * ��úѹ�֡��ä׹�ШӨӹǹ�����¤����á���������������Ҩ������������Ѻ�׹���������
     */
    public void saveReturnDrug(OrderItem oi, String strReturnQty) {
        if ("".equals(strReturnQty)) {
            theUS.setStatus("�������ö�׹�� ������кبӹǹ��", UpdateStatus.WARNING);
            return;
        }
        if (oi != null && oi.charge_complete.equals(Active.isEnable())) {
            theUS.setStatus("�������ö�׹���� ���ͧ�ҡ�Ҵѧ�������ӡ�äԴ�Թ����", UpdateStatus.WARNING);
            return;
        }
        double retQty = Double.parseDouble(strReturnQty);
        double dispQty = Double.parseDouble(oi.qty);
        if (retQty > dispQty) {
            theUS.setStatus("�������ö�׹���� ���ͧ�ҡ�ӹǹ���׹�ҡ���Ҩӹǹ�����¨�ԧ", UpdateStatus.WARNING);
            return;
        }
        if (retQty == 0) {
            theUS.setStatus("�������ö�׹���� �ӹǹ�ҷ����µ�ͧ�ҡ���� 0", UpdateStatus.WARNING);
            return;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String date_time = theLookupControl.intReadDateTime();
            OrderItemReceiveDrug oird = theHosDB.theOrderItemReceiveDrugDB.selectNotReceiveByOrderItemId(oi.getObjectId());

            if (oird != null) {
                double oird_qty_return = Double.parseDouble(oird.qty_return);
                oird.qty_return = String.valueOf(oird_qty_return + retQty);
                theHosDB.theOrderItemReceiveDrugDB.update(oird);
            } else {
                oird = new OrderItemReceiveDrug();
                oird.order_item_id = oi.getObjectId();
                oird.visit_id = oi.visit_id;
                oird.item_id = oi.item_code;
                oird.common_name = oi.common_name;
                oird.receiver = "";
                oird.qty_receive = "";
                oird.time_receive = "";
                oird.qty_return = String.valueOf(retQty);
                oird.returner = theHO.theEmployee.getObjectId();
                oird.time_return = date_time;
                oird.qty_dispense = oi.qty;
                theHosDB.theOrderItemReceiveDrugDB.insert(oird);
            }

            oi.qty = String.valueOf(dispQty - retQty);
            theHosDB.theOrderItemDB.update(oi);

            //amp:5/6/2549:������ա�ä׹�Ҩ������ 0 ��ͧ��ҷ���ջ�ԡ����ҡѺ�ҷ���ա�ä׹�Ҩ�����͡
            if (theLookupControl.readOption().isUseDrugInteract()
                    && oi.qty.trim().length() != 0
                    && Double.parseDouble(oi.qty) == 0.0) {
                theHosDB.theOrderDrugInteractionDB.updateByOrderItemId(oi.getObjectId());
                theHosDB.theOrderDrugInteractionDB.updateByInteractionItemId(oi.getObjectId());
            }

            theHO.vOrderItemReceiveDrug = theHosDB.theOrderItemReceiveDrugDB.selectOIRDByVId(theHO.theVisit.getObjectId());

            theHO.theXObject = oird;
            theHO.vXObject = null;
            theHO.vxo_index = null;
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus("��ä׹�Ңͧ������ �Դ��Ҵ", UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theOrderSubject.notifySaveReturnDrug("��ä׹�Ңͧ������ �������", UpdateStatus.COMPLETE);
        }
    }

    public boolean changeReturnDrug(OrderItemReceiveDrug oird, String strReturnQty) {
        if ("".equals(strReturnQty)) {
            theUS.setStatus("�������ö��䢡�ä׹�� ������кبӹǹ��", UpdateStatus.WARNING);
            return false;
        }
        double retQty = Double.parseDouble(strReturnQty);
        double dispQty = Double.parseDouble(oird.qty_dispense);
        if (retQty > dispQty) {
            theUS.setStatus("�������ö��䢡�ä׹���� ���ͧ�ҡ�ӹǹ���׹�ҡ���Ҩӹǹ�����¨�ԧ", UpdateStatus.WARNING);
            return false;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);

            OrderItem oi = theHosDB.theOrderItemDB.selectByPK(oird.order_item_id);
            if (oi != null && oi.charge_complete.equals(Active.isEnable())) {
                theUS.setStatus("�������ö��䢡�ä׹���� ���ͧ�ҡ�Ҵѧ�������ӡ�äԴ�Թ����", UpdateStatus.WARNING);
                throw new Exception("cn");
            }

            if (retQty == 0) {
                theHosDB.theOrderItemReceiveDrugDB.delete(oird);
            } else {
                oird.qty_return = strReturnQty;
                theHosDB.theOrderItemReceiveDrugDB.update(oird);
            }

            oi.qty = String.valueOf(dispQty - retQty);
            theHosDB.theOrderItemDB.update(oi);

            //amp:5/6/2549:������ա�ä׹�Ҩ������ 0 ��ͧ��ҷ���ջ�ԡ����ҡѺ�ҷ���ա�ä׹�Ҩ�����͡
            if (theLookupControl.readOption().isUseDrugInteract()
                    && oi.qty.trim().length() != 0
                    && Double.parseDouble(oi.qty) == 0.0) {
                theHosDB.theOrderDrugInteractionDB.updateByOrderItemId(oi.getObjectId());
                theHosDB.theOrderDrugInteractionDB.updateByInteractionItemId(oi.getObjectId());
            }

            theHO.vOrderItemReceiveDrug = theHosDB.theOrderItemReceiveDrugDB.selectOIRDByVId(theHO.theVisit.getObjectId());

            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            if (!ex.getMessage().equals("cn")) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                theUS.setStatus("��䢡�ä׹�Ңͧ������ �Դ��Ҵ", UpdateStatus.ERROR);
            }

        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theOrderSubject.notifySaveReturnDrug("��䢡�ä׹�Ңͧ������ �������", UpdateStatus.COMPLETE);
        }
        return isComplete;
    }

    /*
     * ¡��ԡ�������ҵ�����ͧ henbe incomplete wait for notify
     * cancelOrderContinue
     *
     */
    public boolean cancelOrderDrugContinue(Vector orderitem, int[] select_row, Employee emp) {
        boolean is_continue = false;
        for (int i = 0; i < select_row.length; i++) {
            OrderItem oi = (OrderItem) orderitem.get(select_row[i]);
            if (oi.continue_order.equals(Active.isEnable())) {
                is_continue = true;
            }
        }
        if (!is_continue) {
            theUS.setStatus(("�������ö��� Off ���� ���ͧ�ҡ��辺��¡���ҵ�����ͧ"), UpdateStatus.WARNING);
            return false;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String date_time = theLookupControl.intReadDateTime();
            for (int i = 0; i < select_row.length; i++) {
                int row = select_row[i];
                OrderItem theOrderItem = (OrderItem) orderitem.get(row);
                if (theOrderItem.continue_order.equals(Active.isEnable())) {
                    theOrderItem.continue_order = "0";
                    theHosDB.theOrderItemDB.update(theOrderItem);
                    //theHosDB.theOrderItemDB.updateNS(theOrderItem);
                    /////////////////////////////////////////////////////////////
                    OrderContinue oc = theHosDB.theOrderContinueDB.selectByOrid(
                            theOrderItem.getObjectId());
                    oc.date_off = date_time;
                    //���������˹�ҷ���衴����¡��ԡ�ҵ�����ͧ sumo--21/3/2549
                    oc.user_off = theHO.theEmployee.getObjectId();
                    oc.doctor_set_off = emp.getObjectId();
                    theHosDB.theOrderContinueDB.update(oc);
                }
            }
            theHO.vXObject = orderitem;
            theHO.vxo_index = select_row;
            theHO.theXObject = null;
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, "¡��ԡ�������ҵ�����ͧ");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theOrderSubject.notifyContinueOrderItem(("���¡��ԡ�������ҵ�����ͧ") + " "
                    + ("�������"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "¡��ԡ�������ҵ�����ͧ");
        }
        return isComplete;
    }

    /**
     * ����ҵ�����ͧ
     * �ҡ��¡�õ�����ͧ��������������Ǩҡ��������ҵ�����ͧ
     * addOrderContinue
     *
     * @param theVisit
     * @not deprecated henbe use intSaveOrderItem instead this
     * theHosDB.theOrderItemDB.insert(
     */
    public void orderDrugContinue(Visit theVisit) {
        boolean isComplete = false;
        theConnectionInf.open();
        Vector<OrderItem> ois = new Vector<OrderItem>();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String date_time = theLookupControl.intReadDateTime();
            Vector vOC = theHosDB.theOrderContinueDB.selectByVidDo(theVisit.getObjectId(), "");
            if (vOC == null || vOC.isEmpty()) {
                theUS.setStatus(("�������¡���ҵ�����ͧ"), UpdateStatus.WARNING);
            } else {
                theHO.is_order = false;//amp:5/6/2549 �����������͹�������� DrugInteraction �ء���駷���ա������Ҫش
                for (int i = 0, size = vOC.size(); i < size; i++) {
                    OrderContinue oc = (OrderContinue) vOC.get(i);
                    OrderItem oi = theHosDB.theOrderItemDB.selectByPK(oc.order_item_id);
                    OrderItemDrug oid = theHosDB.theOrderItemDrugDB.selectByOrderItemID(oi.getObjectId());
                    oi.setObjectId(null);
                    ItemPrice itemPrice = intReadItemPriceByItem(oi);
                    oi.order_price_type = theVisit.isVisitTypeIPD() ? "1" : "0"; // opd = 0, ipd = 1
                    oi.price = Constant.doubleToDBString(
                            oi.isOPDPrice()
                            ? itemPrice.price : itemPrice.price_ipd);
                    oi.order_cost = Constant.doubleToDBString(itemPrice.price_cost);
                    oi.order_share_doctor = itemPrice.item_share_doctor;
                    oi.order_share_hospital = itemPrice.item_share_hospital;
                    oi.order_editable_price = itemPrice.item_editable_price;
                    oi.order_limit_price_min = itemPrice.item_limit_price_min;
                    oi.order_limit_price_max = itemPrice.item_limit_price_max;
                    oi.use_price_claim = itemPrice.use_price_claim;
                    oi.order_price_claim = itemPrice.item_price_claim;
                    oi.order_edited_price = "0";
                    oi.order_xray_film_price = 0;
                    oi.continue_order = "0";
                    oi.status = OrderStatus.NOT_VERTIFY;
                    oi.charge_complete = Active.isDisable();
                    oi.discontinue = "";
                    oi.discontinue_time = "";
                    oi.dispense = "";
                    oi.dispense_time = "";
                    oi.vertifier = "";//theHO.theEmployee.getObjectId();
                    oi.vertify_time = "";//date_time;
                    oi.executer = theHO.theEmployee.getObjectId();
                    oi.executed_time = date_time;
                    oi.visit_id = theVisit.getObjectId();
                    oi.order_user = theHO.theEmployee.getObjectId();
                    oi.order_time = date_time;
                    oi.clinic_code = theHO.theServicePoint.getObjectId();
                    if (oid != null && oi.isDrug()) {
                        oid.setObjectId(null);
                        oid.order_item_id = null;
                        oid.status = OrderStatus.NOT_VERTIFY;
                        if (oid.dose_short == null || oid.dose_short.isEmpty()) {
                            Uom2 uom = theLookupControl.readUomById(oid.use_uom);
                            DrugFrequency2 freq = theLookupControl.readDrugFrequencyById(oid.frequency);
                            if (uom != null && freq != null) {
                                oid.generateDoseShort(oid.dose, uom.uom_id, freq.drug_frequency_id);
                            }
                        }
                    }
                    //amp:5/6/2549
                    intSaveOrderItem(oi, oid, date_time);
                    ois.add(oi);
                }
                theHO.theXObject = null;
                theHO.vXObject = vOC;
                theHO.vxo_index = null;
                isComplete = true;
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            theUS.setStatus(("�������ҵ�����ͧ��������") + " "
                    + ("�Դ��Ҵ"), UpdateStatus.ERROR);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            try {
                if (!theConnectionInf.getConnection().getAutoCommit()) {
                    theConnectionInf.getConnection().commit();
                }
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theOrderSubject.notifySaveOrderItem(("�������ҵ�����ͧ��������") + " "
                    + ("�������"), UpdateStatus.COMPLETE);
            // auto verify
            verifyOrderItem(ois, null);
        }
    }

    public int deleteOrderItemLabReferOut(OrderItemLabreferout orderItemLabreferout) {
        int ans = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ans = theHosDB.theOrderItemLabreferoutDB.delete(orderItemLabreferout);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public void saveAboutLabReferOut(FormLabreferout formLabreferOut, Vector orderItemLabreferout, UpdateStatus us) {
        if (formLabreferOut == null) {
            us.setStatus(("�����żԴ��Ҵ��سҵ�Ǩ�ͺ�����ա���� ���͡����������ա����"), UpdateStatus.WARNING);
            return;
        }
        if (formLabreferOut.form_name == null
                || formLabreferOut.form_name.isEmpty()) {
            us.setStatus(("��س��кت���Ẻ���������觵�Ǩ"), UpdateStatus.WARNING);
            return;
        }
        if ((formLabreferOut.site_id == null
                || formLabreferOut.site_id.isEmpty())
                && (formLabreferOut.other_place == null
                || formLabreferOut.other_place.isEmpty())) {
            us.setStatus(("��س����͡ʶҹ��Һ�ŷ���觵�Ǩ"), UpdateStatus.WARNING);
            return;
        }
        if (orderItemLabreferout == null || orderItemLabreferout.isEmpty()) {
            us.setStatus(("�ѧ����բ�������¡���Ż����ͧ����觵�Ǩ"), UpdateStatus.WARNING);
            return;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            //��Ǩ�ͺ����� ��úѹ�֡ŧ�ҹ�����������ѧ
            if ((formLabreferOut.getObjectId() == null)) {
                theHosDB.theFormLabreferoutDB.insert(formLabreferOut);
            } else {
                theHosDB.theFormLabreferoutDB.update(formLabreferOut);
            }
            //�Ẻ������� lab ���� referout
            for (int i = 0; i < orderItemLabreferout.size(); i++) {
                OrderItemLabreferout lro = (OrderItemLabreferout) orderItemLabreferout.get(i);
                if (lro.getObjectId() == null) {
                    lro.form_labreferout_id = formLabreferOut.getObjectId();
                    theHosDB.theOrderItemLabreferoutDB.insert(lro);
                } else {
                    theHosDB.theOrderItemLabreferoutDB.update(lro);
                }
            }
            theConnectionInf.getConnection().commit();
            us.setStatus(("��úѹ�֡��¡���Ż����ͧ����觵�Ǩ") + " "
                    + ("�������"), UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            us.setStatus(("��úѹ�֡��¡���Ż����ͧ����觵�Ǩ") + " "
                    + ("�Դ��Ҵ"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
    }

    /**
     * ��� order_result_xray_id �դ���� null �зӡ��ź�͡�ҡ�ҹ���������
     * ���������¡�ù���ѧ�����١�ѹ�֡
     *
     * @param resultXrayPosition
     * @return
     */
    public int deleteResultXrayPosition(ResultXrayPosition resultXrayPosition) {
        int ans = 0;
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            resultXrayPosition.active = Active.isDisable();
            if ((resultXrayPosition.order_result_xray_id != null)
                    && !resultXrayPosition.order_result_xray_id.isEmpty()
                    && !resultXrayPosition.order_result_xray_id.equals("null")) {
                ans = theHosDB.theResultXrayPositionDB.update(resultXrayPosition);
            } else {
                ans = theHosDB.theResultXrayPositionDB.delete(resultXrayPosition);
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(("���ź�����Ŵ�ҹXray") + " "
                    + ("�Դ��Ҵ"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theResultSubject.notifyDeleteXrayPosition(("���ź�����Ŵ�ҹXray") + " "
                    + ("�������"), UpdateStatus.COMPLETE);
        }
        return ans;
    }

    /**
     * ��㹡�úѹ�֡�����š����� Xray ����ͧ Xray �������ҧ������ҹ��
     * ������ա�� �����¡�� order ������ѹ�Ҵ
     *
     * @param orderItem �� Vector �ͧ Object OrderItem
     * @param visit �� Object �ͧ Visit
     * @author padungrat(tong)
     * @date 13/03/49,10:42
     *
     * @not deprecated henbe use intSaveOrderItem instead this
     * theHosDB.theOrderItemDB.insert(
     */
    public void saveOrderItemInXRay(Vector orderItem, Visit visit) {
        if ((orderItem == null)) {
            theUS.setStatus(("�ѧ��������͡��¡�� Order"), UpdateStatus.WARNING);
            return;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (int i = 0; i < orderItem.size(); i++) {
                OrderItem oi = (OrderItem) orderItem.get(i);
                if (oi.getObjectId() == null) {
                    intSaveOrderItem(oi, null, theHO.date_time);
                    ResultXRay rx = new ResultXRay();
                    rx.xn = theHO.thePatient.xn;
                    rx.xray_point = "";
                    rx.description = "";
                    rx.hn = theHO.thePatient.getObjectId();
                    rx.vn = theHO.theVisit.getObjectId();
                    rx.record_time = theLookupControl.intReadDateTime();
                    rx.order_item_id = oi.getObjectId();
                    rx.reporter = "";
                    rx.active = Active.isEnable();
                    theHosDB.theResultXRayDB.insert(rx);
                }
            }
            theHO.vXObject = orderItem;
            theHO.vxo_index = null;
            theHO.theXObject = null;
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, "������¡�� Order");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theOrderSubject.notifySaveOrderItem(("���������¡��Order") + " "
                    + ("�������"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "������¡�� Order");
        }
    }

    public Vector queryStaffDoctorVerifyOrder(String visit_id) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theSpecialEmployeeDB.queryStaffDoctorVerifyOrder(visit_id);
            if ((vc != null)) {
                if ((vc.isEmpty())) {
                    vc = null;
                }
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listDoctorByVerifyOrder(String visit_id) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String sql = "SELECT * "
                    + " FROM b_employee INNER JOIN t_order ON b_employee.b_employee_id = t_order.order_staff_verify"
                    + " WHERE "
                    + "     ((t_order.f_order_status_id)<>'" + OrderStatus.DIS_CONTINUE + "'"
                    + "   And (t_order.f_order_status_id)<>'" + OrderStatus.NOT_VERTIFY + "')"
                    + " AND ((b_employee.f_employee_authentication_id)='" + Authentication.DOCTOR + "') "
                    + " AND ((t_order.t_visit_id)='" + visit_id + "')";
            vc = theHosDB.theEmployeeDB.eQuery(sql);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public vOrderSpecial queryOrderItemAndOrderItemDrugByVisitID(String visit_id) {
        SpecialQueryOrderDrugDB theSpecialQueryOrderDrugDB = new SpecialQueryOrderDrugDB(theConnectionInf);
        vOrderSpecial theVOrderSpecial = new vOrderSpecial();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theVOrderSpecial = theSpecialQueryOrderDrugDB.queryOrderItemAndOrderItemDrugByVisitID(visit_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theVOrderSpecial;
    }

    /*
     * function public void updateOrderFromPanelOrder(OrderItem
     * orderItem,OrderItemDrug orderitemdrug)
     */
    public boolean saveOrderItem(OrderItem theOrderItem, OrderItemDrug orderitemdrug) {
        //pu : 20/07/2549
        if (this.theHO.thePatient != null || this.theHO.theVisit != null) {
            return saveOrderItem(theOrderItem, orderitemdrug, null);
        }
        return false;
    }

    public boolean saveOrderItem(OrderItem theOrderItem, OrderItemDrug orderitemdrug, OrderLabSecret ols) {
        theConnectionInf.open();
        boolean ret = false;
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String date_time = theLookupControl.intReadDateTime();
            boolean isInsert = (theOrderItem.getObjectId() == null);
            if (!checkLabDuration(theOrderItem, theHO.theVisit)) {
                return false;
            }
            boolean b = intSaveOrderItem(theOrderItem, orderitemdrug, date_time, ols);
            if (!b) {
                return false;
            }
            if (isInsert && theOrderItem.isPackage()) {
                List<ItemPackage> list = theHosDB.theItemPackageDB.selectByItem(theOrderItem.item_code, "1");
                for (int i = 0; i < list.size(); i++) {
                    ItemPackage theItemPackage = list.get(i);
                    Item item = theHosDB.theItemDB.selectByPK(theItemPackage.b_item_sub_id);
                    CategoryGroupItem cgi = theLookupControl.readCategoryGroupItemById(item.item_group_code_category);
                    Drug drug = cgi.category_group_code.equals(CategoryGroup.isDrug())
                            ? theHosDB.theDrugDB.selectByItem(item.getObjectId())
                            : null;
                    ItemSupply supply = cgi.category_group_code.equals(CategoryGroup.isSupply())
                            ? theHosDB.theItemSupplyDB.selectByItemId(item.getObjectId())
                            : null;

                    OrderItemDrug oid = null;
                    if (drug != null) {
                        Uom2 uom = theLookupControl.readUomById(drug.use_uom);
                        DrugFrequency2 freq = theLookupControl.readDrugFrequencyById(drug.frequency);
                        oid = theHO.initOrderItemDrug(drug, uom, freq);
                    }
                    OrderItem subOrderItem = theHO.initOrderItem(theHO.theVisit.visit_type, item, cgi, null, date_time, drug, supply);
                    subOrderItem.f_item_type_id = "2";
                    subOrderItem.qty = String.valueOf(theItemPackage.item_qty_purch);

                    OrderLabSecret ois = null;
                    if (subOrderItem.secret.equals("1")) {
                        ois = new OrderLabSecret();
                        String m = JOptionPane.showInputDialog(theUS.getJFrame(), "��¡����� : " + item.getName() + " \n��������觵�Ǩ", "��������觵�Ǩ", JOptionPane.OK_CANCEL_OPTION);
                        if (m == null || m.isEmpty()) {
                            theUS.setStatus("��سҡ�͡��������觵�Ǩ", UpdateStatus.WARNING);
                            throw new Exception("cn");
                        }
                        ois.specimen_code = m;
                    }

                    b = intSaveOrderItem(subOrderItem, oid, date_time, ois);
                    if (!b) {
                        throw new Exception("cn");
                    }
                    OrderPackage theOrderPackage = new OrderPackage();
                    theOrderPackage.t_order_id = theOrderItem.getObjectId();
                    theOrderPackage.t_order_sub_id = subOrderItem.getObjectId();
                    theOrderPackage.order_seq = theItemPackage.item_seq;
                    theHosDB.theOrderPackageDB.insert(theOrderPackage);
                }
            }
            theHO.vXObject = null;
            theHO.vxo_index = null;
            theHO.theXObject = theOrderItem;
            theHO.vOrderItem = theHosDB.theOrderItemDB.selectByVidTypeCancel(theHO.theVisit.getObjectId(), "", false);
            theConnectionInf.getConnection().commit();
            ret = true;
        } catch (Exception ex) {
            if (ex != null && ex.getMessage() != null && !ex.getMessage().equals("cn")) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT, "��¡�õ�Ǩ�ѡ��");
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (ret) {
            theHS.theOrderSubject.notifySaveOrderItem(("��úѹ�֡��¡�õ�Ǩ�ѡ��") + " "
                    + ("�������"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, "��¡�õ�Ǩ�ѡ��");
        }
        return ret;
    }

    public boolean saveOrderItemCustomPrice(OrderItem theOrderItem) {
        double price = Double.parseDouble(theOrderItem.price);
        if (!(theOrderItem.order_limit_price_min == 0.0d && theOrderItem.order_limit_price_max == 0.0d)
                && (price < theOrderItem.order_limit_price_min || price > theOrderItem.order_limit_price_max)) {
            JOptionPane.showMessageDialog(null,
                    "�Ҥҵ�ͧ����㹪�ǧ " + theOrderItem.order_limit_price_min + " - " + theOrderItem.order_limit_price_max + " �ҷ",
                    "��͹", JOptionPane.WARNING_MESSAGE);
            return false;
        }
        theConnectionInf.open();
        boolean ret = false;
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theOrderItemDB.updateCustomPrice(theOrderItem);

            theHO.vXObject = null;
            theHO.vxo_index = null;
            theHO.theXObject = theOrderItem;
            theHO.vOrderItem = theHosDB.theOrderItemDB.selectByVidTypeCancel(theHO.theVisit.getObjectId(), "", false);
            theConnectionInf.getConnection().commit();
            ret = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_UPDATE);

        } finally {
            theConnectionInf.close();
        }
        if (ret) {
            theHS.theOrderSubject.notifySaveOrderItem(("��úѹ�֡��¡�õ�Ǩ�ѡ��") + " "
                    + ("�������"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_UPDATE);
        }
        return ret;
    }

    public static boolean confirmRepeatOrder(Vector vOrderItem, OrderItem theOrderItem, UpdateStatus theUS) {
        boolean confirm = true;
        for (int i = 0; vOrderItem != null && i < vOrderItem.size(); i++) {
            OrderItem oi = (OrderItem) vOrderItem.get(i);
            if (oi.item_code.equals(theOrderItem.item_code)
                    && !oi.status.equals(OrderStatus.DIS_CONTINUE)) {
                if (theOrderItem.getObjectId() == null) //amp:02/03/2549 ���Сó��Ż���Դ �����������觵�Ǩ�ѹ�е�ͧ���� Code �ç���
                {
                    confirm = theUS.confirmBox(("��¡�õ�Ǩ�ѡ��") + " " + theOrderItem.common_name
                            + " " + ("��ӡѹ�Ѻ��¡������������������") + " "
                            + ("�׹�ѹ�����觫��"), UpdateStatus.WARNING);
                    break;
                }
            }
        }
        return confirm;
    }

    /**
     * @deprecate henbe unused
     *
     * @param theOrderItem
     * @param orderitemdrug
     * @param date_time
     * @return
     * @throws Exception
     */
    public boolean intSaveOrderItem(OrderItem theOrderItem, OrderItemDrug orderitemdrug, String date_time) throws Exception {
        return intSaveOrderItem(theOrderItem, orderitemdrug, date_time, null);
    }

    /**
     *
     * @param theOrderItem
     * @param orderitemdrug
     * @param date_time
     * @param ols
     * @return
     * @throws Exception
     */
    public boolean intSaveOrderItem(OrderItem theOrderItem, OrderItemDrug orderitemdrug, String date_time, OrderLabSecret ols) throws Exception {
        boolean ret = intUDSaveOrderItem(theOrderItem, orderitemdrug, date_time, ols, theHO.thePatient, theHO.theVisit);
        return ret;
    }

    /**
     * �ѧ������º�����ջѭ������ͧ dependency �ҧ��ǹ
     *
     * @param theOrderItem
     * @param orderitemdrug
     * @param date_time
     * @param ols
     * @param patient
     * @param visit
     * @return
     * @throws Exception
     */
    public boolean intUDSaveOrderItem(OrderItem theOrderItem,
            OrderItemDrug orderitemdrug, String date_time, OrderLabSecret ols,
            Patient patient, Visit visit) throws Exception {
        if (visit == null) {
            theUS.setStatus(("��س����͡������㹡�кǹ������������¡�õ�Ǩ"), UpdateStatus.WARNING);
            return false;
        }
        if (visit.visit_status.equals(VisitStatus.isOutProcess())) {
            theUS.setStatus(("�����¨���кǹ��������������ö��䢢�������"), UpdateStatus.WARNING);
            return false;
        }
        //��Ǩ�ͺ��ҩѹ�繤���͡�����������
        if (HosObject.isLockingByOther(visit, theHO.theEmployee.getObjectId())) {
            theUS.setStatus(("�س�������͡�����������������ö��зӡ���� �Ѻ�����¤������"), UpdateStatus.WARNING);
            return false;
        }
        BillingGroupItem bgi = theLookupControl.readBillingGroupItemById(theOrderItem.item_group_code_billing);
        if (bgi == null) {
            theUS.setStatus("�������¡������稢ͧ��¡�õ�Ǩ����ջѭ�� ��سҡ�˹���Ңͧ����������", UpdateStatus.WARNING);
            return false;
        }
        // Lab
        if (theOrderItem.isLab()) {
            if (theOrderItem.secret.equals("1") && ols != null && ols.specimen_code.isEmpty()) {
                theUS.setStatus(("��سҡ�͡��������觵�Ǩ"), UpdateStatus.WARNING);
                return false;
            }
            if (theOrderItem.getObjectId() != null && theOrderItem.secret.equals("1")) {
                theUS.setStatus(("��������觵�Ǩ�������ö�����") + " "
                        + ("��س�¡��ԡ��¡�������������"), UpdateStatus.WARNING);
                return false;
            }
            if (!confirmRepeatOrder(theHO.vOrderItem, theOrderItem, theUS)) {
                return false;
            }
        }
        // Xray
        if (theOrderItem.isXray()) {
            if (!confirmRepeatOrder(theHO.vOrderItem, theOrderItem, theUS)) {
                return false;
            }
        }
        // Drug
        SpecialQueryPatientDrugAllergy sqpda = null;
        if (theOrderItem.isDrug()) {
            if (orderitemdrug == null) {
                theUS.setStatus("��¡���ҷ���������բ����� ��سҵ�Ǩ�ͺ�ҹ������", UpdateStatus.WARNING);
                return false;
            }

            if (orderitemdrug.dose_short == null || orderitemdrug.dose_short.isEmpty()) {
                Uom2 uom = theLookupControl.readUomById(orderitemdrug.use_uom);
                DrugFrequency2 freq = theLookupControl.readDrugFrequencyById(orderitemdrug.frequency);
                if (uom != null && freq != null) {
                    orderitemdrug.generateDoseShort(orderitemdrug.dose, uom.uom_id, freq.drug_frequency_id);
                }
            }

            try {
                Double.parseDouble(theOrderItem.qty);
            } catch (NumberFormatException ex) {
                theUS.setStatus(("��س��кػ���ҳ�������繵���Ţ"), UpdateStatus.WARNING);
                return false;
            }
            Drug drug = theHosDB.theDrugDB.selectByItem(theOrderItem.item_code);
            if (orderitemdrug.usage_special.equals("0")) {
                if (orderitemdrug.purch_uom.equals(orderitemdrug.use_uom)
                        && Double.parseDouble(theOrderItem.qty) != 0) {
                    try {
                        double dose_used = Double.parseDouble(orderitemdrug.dose);
                        double qty_used = Double.parseDouble(theOrderItem.qty);
                        if (dose_used > qty_used) {
                            theUS.setStatus("����ҳ������ҡ���һ���ҳ������ ��سҵ�Ǩ�ͺ�������ա����", UpdateStatus.WARNING);
                            return false;
                        }
                    } catch (NumberFormatException ex) {
                    }
                }
                // check limit (if use special dose can not use this formula)
                if (drug != null && drug.limit_amount_day > 0) {
                    double usePerTime = Double.parseDouble(orderitemdrug.dose.isEmpty() ? "0.0d" : orderitemdrug.dose);
                    DrugFrequency df = theHosDB.theDrugFrequencyDB.selectByPK(orderitemdrug.frequency);
                    int timePerDay = Integer.parseInt(df.factor == null || df.factor.isEmpty() ? "0" : df.factor);
                    double amount = Double.parseDouble(theOrderItem.qty.isEmpty() ? "0.0d" : theOrderItem.qty);
                    double totalDay = amount / (usePerTime * timePerDay);
                    if (Double.parseDouble(String.valueOf(drug.limit_amount_day)) < totalDay) {
                        JOptionPane.showMessageDialog(theUS.getJFrame(), "�������ö����� \"" + theOrderItem.common_name + " \"�Թ�ӹǹ�ѹ����˹��� (�٧�ش " + drug.limit_amount_day + " �ѹ)", "��͹", JOptionPane.WARNING_MESSAGE);
                        return false;
                    }
                }
            }

            //DrugAllergy & G-6 PD
            //amp:27/03/2549 ���Ͷ���繡�� updateStatus ����ͧ�����͹�ա
            if (theOrderItem.getObjectId() == null) {
                // Drug interaction
                if (theLookupControl.readOption().isUseDrugInteractDeny()) {
                    List<SpecialQueryDrugInteraction> drugInteractions
                            = theHosDB.theSpecialQueryDrugInteractionDB.checkDrugInteraction(
                                    visit.getObjectId(), theOrderItem.item_code);

                    if (!drugInteractions.isEmpty()) {
                        StringBuilder message = new StringBuilder();
                        message.append("<html><body>");
                        message.append("<font color=blue size=+1>").append(theOrderItem.common_name).append("</font>");
                        drugInteractions.forEach((SpecialQueryDrugInteraction di) -> {
//                            message.append("\n- " + di.interactionName + "\n" + di.interactionAction);
                            message.append("<br><br><b>�ջ�ԡ����ҡѺ:</b> <font color=red><b>").append(di.interactionName).append("</b></font>");
                            message.append("<br>&nbsp;&nbsp;&nbsp;<b>�дѺ�����ç:</b> ").append(di.interactionLevel);
                            message.append("<br>&nbsp;&nbsp;&nbsp;<b>��ԡ����ҷ����Դ:</b> ").append(di.interactionAction);
                            message.append("<br>&nbsp;&nbsp;&nbsp;<b>�Ը����:</b> ").append(di.interactionFix);
                        });
                        message.append("</body></html>");
                        DialogDrugInteraction.showDialogWarning(theUS, message.toString());
//                        JOptionPane.showMessageDialog(theUS.getJFrame(),
//                                "�������ö����� \"" + theOrderItem.common_name
//                                + "\" ���ͧ�ҡ�ջ�ԡ���ҡѺ" + message.toString(),
//                                "��͹", JOptionPane.WARNING_MESSAGE);
                        return false;
                    }
                }

                // DrugAllergy
                sqpda = intListPatientDrugAllergy(patient.getObjectId(), theOrderItem.item_code);
                if (sqpda != null) {
                    DialogWarningDrugAllergy dwda = new DialogWarningDrugAllergy(null, true);
                    dwda.setControl(hosControl);
                    boolean res1 = dwda.openConfirmDialog(sqpda);
                    if (!res1) {
                        return false;
                    }
                    theOrderItem.drug_allergy = "1";
                    sqpda.allergy_drug_order_cause = dwda.getStrNote();
                }
                // G-6-PD
                if (theHO.theG6pd != null && "1".equals(theHO.theG6pd.g_6_pd)) {
                    if (null != theHosDB.theItemDrugMapG6pdDB.selectByItemId(theOrderItem.item_code)) {
                        DialogConfirmCancelWithCause dccwc = new DialogConfirmCancelWithCause(null, true);
                        if (1 == dccwc.showDialog("��͹!", "�ҹ���ռŵ�ͼ����� G-6-PD ��ͧ����׹�ѹ�������� �������\n\n\n", false)) {
                            theOrderItem.drug_allergy = "2";
                        } else {
                            return false;
                        }
                    }
                }
                if (theHO.vDxTemplate != null) {
                    DxTemplate Dx = theHosDB.theDxTemplateDB.selectByItem(theOrderItem.item_code, visit.getObjectId());
                    if (Dx != null) {
                        DialogConfirmCancelWithCause dccwc = new DialogConfirmCancelWithCause(null, true);
                        String text = Dx.thaidescription.isEmpty() ? Dx.description : Dx.thaidescription;
                        if (1 == dccwc.showDialog("��͹!", "<html>�ҹ���ռŵ�ͼ����� <font color='red'>" + text + "</font> ��ͧ����׹�ѹ�������� ������� \n\n\n</html>", true)) {
                            theOrderItem.order_dx_item_risk = "1";
                            theOrderItem.order_dx_item_risk_reason = dccwc.getCause();
                        } else {
                            return false;
                        }
                    }
                }
            }
            if (drug.indication != null && !drug.indication.trim().isEmpty()) {
                JOptionPane.showMessageDialog(theUS.getJFrame(), drug.indication, "��ͺ觪��������", JOptionPane.WARNING_MESSAGE);
            }
        }
        //Item Price
        try {
            if (!theOrderItem.isChildPackage()) {
                double price = Double.parseDouble(theOrderItem.price);
                if (price == 0) {
                    boolean order = theUS.confirmBox(("��¡����觵�Ǩ����ա�á�˹��Ҥ��� 0")
                            + ("�ô��˹��Ҥҷ����͹��͹") + " "
                            + ("�׹�ѹ������ ?"), UpdateStatus.WARNING);
                    if (!order) {
                        return false;
                    }
                }
                if (theOrderItem.isEditablePrice()
                        && !(theOrderItem.order_limit_price_min == 0.0d && theOrderItem.order_limit_price_max == 0.0d)
                        && (price < theOrderItem.order_limit_price_min
                        || price > theOrderItem.order_limit_price_max)) {
                    JOptionPane.showMessageDialog(null,
                            "�Ҥҵ�ͧ����㹪�ǧ " + theOrderItem.order_limit_price_min + " - " + theOrderItem.order_limit_price_max + " �ҷ",
                            "��͹", JOptionPane.WARNING_MESSAGE);
                    return false;
                }
            }
        } catch (NumberFormatException ex) {
            boolean order = theUS.confirmBox(("��¡����觵�Ǩ����ա�á�˹��Ҥ�") + " "
                    + ("�Դ��Ҵ") + " (" + theOrderItem.price
                    + ") " + ("�ô��˹��Ҥҷ����͹��͹"), UpdateStatus.WARNING);
            if (!order) {
                return false;
            }
        }

        for (int i = 0; i < theHO.vVisitPayment.size(); i++) {
            Payment pm = (Payment) theHO.vVisitPayment.get(i);
            OptionDetail od = theHO.theLO.getOptionDetail("warning_order_by_plan_cat" + pm.plan_kid);
            String[] option = od.note.split(",");
            if (option.length >= 3) {
                boolean res;
                if (theOrderItem.item_group_code_category.equals(option[1])) {
                    boolean ret = theUS.confirmBox(option[2], UpdateStatus.WARNING);
                    if (ret) {
                    } else {
                        return false;
                    }
                } else {
                    for (String option1 : option) {
                        if (option1.indexOf('^') > 0) {
                            String[] tmp = option1.split("\\^");
                            if (tmp[0].equals(theOrderItem.item_code)) {
                                res = theUS.confirmBox(option[2], UpdateStatus.WARNING);
                                if (res) {
                                } else {
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
        }
        //Processing
        if (theOrderItem.getObjectId() == null) {
            if (theOrderItem.order_price_type == null || theOrderItem.order_price_type.isEmpty()) {
                theOrderItem.order_price_type = visit.isVisitTypeIPD() ? "1" : "0"; // opd = 0, ipd = 1
            }
            if (theOrderItem.price.isEmpty()) {
                Payment pm = (Payment) theHO.vVisitPayment.get(0);
                ItemPrice itemPrice = this.intReadItemPriceByItem(theOrderItem.item_code, pm);
                theOrderItem.price = Constant.doubleToDBString(
                        theOrderItem.isOPDPrice()
                        ? itemPrice.price : itemPrice.price_ipd);
                theOrderItem.order_cost = Constant.doubleToDBString(itemPrice.price_cost);
                theOrderItem.order_share_doctor = itemPrice.item_share_doctor;
                theOrderItem.order_share_hospital = itemPrice.item_share_hospital;
                theOrderItem.order_editable_price = itemPrice.item_editable_price;
                theOrderItem.order_limit_price_min = itemPrice.item_limit_price_min;
                theOrderItem.order_limit_price_max = itemPrice.item_limit_price_max;
                theOrderItem.use_price_claim = itemPrice.use_price_claim;
                theOrderItem.order_price_claim = itemPrice.item_price_claim;
            }

            theOrderItem.clinic_code = theHO.theServicePoint.getObjectId();
            theOrderItem.hn = patient.getObjectId();
            theOrderItem.visit_id = visit.getObjectId();
            theOrderItem.order_user = theHO.theEmployee.getObjectId();
            if (theOrderItem.order_time == null || theOrderItem.order_time.isEmpty()) {
                theOrderItem.order_time = date_time;
            }
            if (theOrderItem.isDrug() || theOrderItem.isSupply()) {
                MapDrugTMT mapDrugTMT = theHosDB.theMapDrugTMTDB.selectByItemId(theOrderItem.item_code);
                if (mapDrugTMT != null) {
                    theOrderItem.tpucode = mapDrugTMT.b_drug_tmt_tpucode;
                }
                MapDrugCode24 mapDrugCode24 = theHosDB.theMapDrugCode24DB.selectByItemId(theOrderItem.item_code);
                if (mapDrugCode24 != null) {
                    theOrderItem.drugcode24 = mapDrugCode24.drugcode24;
                }
            }
            theHosDB.theOrderItemDB.insert(theOrderItem);
            //Drug
            if (theOrderItem.isDrug()) {
                OrderItemDrug oid = orderitemdrug;
                oid.order_item_id = theOrderItem.getObjectId();
                oid.active = Active.isEnable();
                oid.modify = theHO.theEmployee.getObjectId();
                oid.modify_time = date_time;
                theHosDB.theOrderItemDrugDB.insert(oid);
                if ("1".equals(theOrderItem.drug_allergy) && sqpda != null) {
                    AllergyDrugOrder ado = new AllergyDrugOrder();
                    ado.t_order_id = theOrderItem.getObjectId();
                    ado.t_patient_drug_allergy_id = sqpda.patient_drug_allergy_id;
                    ado.allergy_drug_order_cause = sqpda.allergy_drug_order_cause;
                    ado.user_record = theHO.theEmployee.getObjectId();
                    ado.record_date_time = date_time;
                    ado.user_modify = theHO.theEmployee.getObjectId();
                    ado.modify_date_time = date_time;
                    theHosDB.theAllergyDrugOrderDB.insert(ado);
                }
            }
            //lab
            if (theOrderItem.isLab() && ols != null && theOrderItem.secret.equals(Active.isEnable())) {
                ols.order_id = theOrderItem.getObjectId();
                theHosDB.theOrderLabSecretDB.insert(ols);
            }
        } else {
            theHosDB.theOrderItemDB.update(theOrderItem);
            if (orderitemdrug != null)//੾����¡�÷��������ҹ��
            {
                theHosDB.theOrderItemDrugDB.update(orderitemdrug);
            }
        }
        return true;
    }

//    /**
//     * @author tong
//     * @date 25/05/48
//     * @param Object �ͧ Visit, Vector �ͧ Object Order ��� ��Դ�ͧ Dialog
//     * typeDialog �� 1 = DialogOrderSet (��¡���Ҫش) �� 2 =
//     * DialogListOrderSet (��¡������͹���駷������ ��� VN) �� 3 =
//     * DialogListOrderSet (��¡������͹�ѹ������� ��� �ѹ)
//     * @return void ��㹡�úѹ�֡�����¡�� order item ����Ѻ�Ҩҡ Dialog �Ҫش
//     * , ��¡������͹���駷������ ��� ��¡������͹�ѹ������� ���������Ѻ
//     * panelOrder �¼�ҹ notifysaveOrderItemFromDialogOrder �����˵�
//     * ����ͧ������� �ѧ�����Ѵ������� ��ͧ�͡�á�˹� GUI �ͧ Dialog message
//     * ��͹
//     */
    public void saveOrderItemFromDrugSet(Vector vItem, Vector vDrugSet, int[] rows) {
        if (vItem == null || vItem.isEmpty()) {
            theUS.setStatus(("�������¡�õ�Ǩ�ѡ�ҷ��ӡ������"), UpdateStatus.WARNING);
            return;
        }
        if (rows.length == 0) {
            theUS.setStatus(("�������¡�õ�Ǩ�ѡ�ҷ��ӡ������"), UpdateStatus.WARNING);
            return;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        boolean haveWaring = false;
        String item_name = "";
        StringBuilder sb = new StringBuilder();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String date_time = theLookupControl.intReadDateTime();
            //ǹ�ٻ���ͺѹ�֡�Ҫش�������ö�ѹ�֡��
            theHO.is_order = false;//amp:5/6/2549 �����������͹�������� DrugInteraction �ء���駷���ա������Ҫش
            Calendar cal = DateUtil.getCalendar(date_time);
            for (int i = 0; i < rows.length; i++) {
                Item item = (Item) vItem.get(rows[i]);
                if (item.active.equals("0") || item.active.equals("2")) {
                    sb.append(item.common_name).append("\n");
                    continue;
                }
                //init order item
                CategoryGroupItem cgi = theLookupControl.readCategoryGroupItemById(
                        item.item_group_code_category);
                ItemPrice iPrice = intReadItemPriceByItemID(item.getObjectId());
                if (iPrice == null) {
                    item_name = item_name + " " + item.common_name;
                    continue;
                }
                Drug drug = cgi.category_group_code.equals(CategoryGroup.isDrug())
                        ? theHosDB.theDrugDB.selectByItem(item.getObjectId())
                        : null;
                ItemSupply supply = cgi.category_group_code.equals(CategoryGroup.isSupply())
                        ? theHosDB.theItemSupplyDB.selectByItemId(item.getObjectId())
                        : null;
                String rec_date_time = DateUtil.addSecond(cal, i);
                OrderItem oi = theHO.initOrderItem(theHO.theVisit.visit_type, item, cgi, iPrice, rec_date_time, drug, supply);
                OrderItemDrug oid = null;
                //������������ҹ detail �ͧ�����觪ش��鹴���
                if (oi.isDrug()) {
                    //��Ҿ������������¡�ù������ dose �ͧ�ѹ�͡�Ҵ���
                    DrugSet ds = null;
                    for (int j = 0; j < vDrugSet.size(); j++) {
                        DrugSet ds1 = (DrugSet) vDrugSet.get(j);
                        if (ds1.item_code.equals(oi.item_code)) {
                            ds = ds1;
                            break;
                        }
                    }
                    //��Ҿ������������¡�ù������ dose �ͧ�ѹ�͡�Ҵ���
                    if (ds != null) {
                        DoseDrugSet doseDrugSet = theHosDB.theDoseDrugSetDB.selectByKeyDrugSet(ds.getObjectId());
                        if (doseDrugSet != null) {
                            Uom2 uom = theLookupControl.readUomById(doseDrugSet.use_uom);
                            DrugFrequency2 freq = theLookupControl.readDrugFrequencyById(doseDrugSet.frequency);
                            oid = theHO.initOrderItemDrug(doseDrugSet, uom, freq);
                            oi.qty = doseDrugSet.qty;
                        }
                    }
                    //�������¡��������� vDrugSet �������Ҩҡ default �ͧ�ҵ�ǹ��
                    if (oid == null) {
                        Uom2 uom = theLookupControl.readUomById(drug.use_uom);
                        DrugFrequency2 freq = theLookupControl.readDrugFrequencyById(drug.frequency);
                        oid = theHO.initOrderItemDrug(drug, uom, freq);
                    }
                }
                if (oi.qty.equals("0")) {
                    oi.qty = "1";
                }
                intSaveOrderItem(oi, oid, date_time);
            }
            //tuk: 27/07/2549 ������ʶҹСóշ������¡�õ�Ǩ�ѡ�ҷ��������Ҥ�
            theHO.vXObject = vItem;
            theHO.vxo_index = rows;
            theHO.theXObject = null;
            if (item_name.isEmpty() && sb.toString().trim().isEmpty()) {
                theHO.vOrderItem = theHosDB.theOrderItemDB.selectByVidTypeCancel(theHO.theVisit.getObjectId(), "", false);
            } else {
                haveWaring = true;
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            theUS.setStatus(("��úѹ�֡�ش��õ�Ǩ�ѡ��") + " "
                    + ("�Դ��Ҵ"), UpdateStatus.ERROR);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            if (haveWaring) {
                if (sb.toString() != null && !sb.toString().trim().isEmpty()) {
                    theHS.theOrderSubject.notifySaveOrderItem("����¡�õ�Ǩ�ѡ�ҷ�� inactive ����������� ����", UpdateStatus.WARNING);

                    JOptionPane.showMessageDialog(null, "�к��������ö�����¡�õ�Ǩ�ѡ�ҷ�� inactive ����������� ������ �ѧ���\n"
                            + sb.toString(), "����¡�õ�Ǩ�ѡ�ҷ�� inactive ����������� ����", JOptionPane.WARNING_MESSAGE);
                } else {
                    theHS.theOrderSubject.notifySaveOrderItem(("��辺�ҤҢͧ��¡�õ�Ǩ�ѡ��") + " " + item_name
                            + " " + ("�ô�駼������к�"), UpdateStatus.WARNING);
                }
            } else {
                theHS.theOrderSubject.notifySaveOrderItem(("��úѹ�֡�ش��õ�Ǩ�ѡ��")
                        + ("�������"), UpdateStatus.COMPLETE);
            }
        }
    }

    /**
     *
     * @param pt
     * @param v
     * @param pm
     * @param date_time
     * @return
     * @throws Exception
     */
    protected boolean intCheckAutoOrder(Patient pt, Visit v, Vector pm, String date_time) throws Exception {
        Vector vAutoOrder = theHosDB.theAutoOrderItemDB.selectAllAutoOrder();
        if (vAutoOrder == null) {
            return false;
        }
        // check �Է�ԡ���ѡ��
        for (int i = 0; i < vAutoOrder.size(); i++) {
            AutoOrderItem auOrder = (AutoOrderItem) vAutoOrder.get(i);
            for (int n = 0; n < pm.size(); n++) {
                Payment p = (Payment) pm.get(n);
                if (auOrder.plan_id.equals(p.plan_kid)) {
                    intCheckDayAutoOrder(pt, v, auOrder, date_time);
                }
            }
            if (auOrder.plan_id.isEmpty()) {
                intCheckDayAutoOrder(pt, v, auOrder, date_time);
            }
        }
        return true;
    }

    protected boolean intCheckDayAutoOrder(Patient pt, Visit v, AutoOrderItem aoi, String date_time) throws Exception {
        String curr_date = date_time.substring(0, date_time.indexOf(','));
        String curr_time = date_time.substring(date_time.indexOf(',') + 1);
        Date date = DateUtil.getDateFromText(curr_date);

        BDayOff dayOff = theHosDB.theBDayOffDB.selectByDate(date);
        if (dayOff != null) {
            if (aoi.holiday.equals("1")) {
                if (aoi.holiday_alltime.equals("1")) {
                    return intSaveAutoOrderItem(pt, v, aoi, date_time);
                } else {
                    boolean ts = curr_time.compareTo(aoi.holiday_time_start) > 0;
                    boolean tf = curr_time.compareTo(aoi.holiday_time_stop) < 0;
                    if (ts == true && tf == true) {
                        return intSaveAutoOrderItem(pt, v, aoi, date_time);
                    }
                }
            }
            return false;
        }
        // if not day off
        String day = DateUtil.isDay(date);
        if (day.equals("1") && aoi.sunday.equals("1")) {
            if (aoi.sun_alltime.equals("1")) {
                return intSaveAutoOrderItem(pt, v, aoi, date_time);
            } else {
                boolean ts = curr_time.compareTo(aoi.sun_time_start) > 0;
                boolean tf = curr_time.compareTo(aoi.sun_time_stop) < 0;
                if (ts == true && tf == true) {
                    return intSaveAutoOrderItem(pt, v, aoi, date_time);
                }
            }
        }
        if (day.equals("7") && aoi.saturday.equals("1")) {
            if (aoi.sat_alltime.equals(Active.isEnable())) {
                return intSaveAutoOrderItem(pt, v, aoi, date_time);
            } else {
                boolean ts = curr_time.compareTo(aoi.sat_time_start) > 0;
                boolean tf = curr_time.compareTo(aoi.sat_time_stop) < 0;
                if (ts == true && tf == true) {
                    return intSaveAutoOrderItem(pt, v, aoi, date_time);
                }
            }
        }
        if (!day.equals("1") && !day.equals("7")) {
            if (aoi.monday.equals(Active.isEnable()) && aoi.mon_alltime.equals(Active.isEnable())) {
                return intSaveAutoOrderItem(pt, v, aoi, date_time);
            } else {
                boolean ts = curr_time.compareTo(aoi.mon_time_start) > 0;
                boolean tf = curr_time.compareTo(aoi.mon_time_stop) < 0;
                if (ts == true && tf == true) {
                    return intSaveAutoOrderItem(pt, v, aoi, date_time);
                }
            }
        }
        return false;
    }

    /**
     *
     * @param pt
     * @param v
     * @param a
     * @param date_time
     * @return
     * @throws Exception
     */
    protected boolean intSaveAutoOrderItem(Patient pt, Visit v, AutoOrderItem a, String date_time) throws Exception {
        Item item = theHosDB.theItemDB.selectByPK(a.item_id);
        if (item == null || item.active.equals("0") || item.active.equals("2")) {
            return false;
        }
        CategoryGroupItem cgi = theHosDB.theCategoryGroupItemDB.selectByPK(item.item_group_code_category);
        if (cgi == null) {
            return false;
        }
        if (a.b_hstock_item_id != null && !a.b_hstock_item_id.isEmpty()) {
            item.setProperty("b_hstock_item_id", a.b_hstock_item_id);
        }
        Drug drug = cgi.category_group_code.equals(CategoryGroup.isDrug())
                ? theHosDB.theDrugDB.selectByItem(item.getObjectId())
                : null;
        ItemSupply supply = cgi.category_group_code.equals(CategoryGroup.isSupply())
                ? theHosDB.theItemSupplyDB.selectByItemId(item.getObjectId())
                : null;
        ItemPrice ip = intReadItemPriceByItemID(a.item_id);
        OrderItem or = theHO.initOrderItem(v.visit_type, item, cgi, ip, theHO.date_time, drug, supply);
        OrderItemDrug oid = null;
        if (drug != null) {
            Uom2 uom = theLookupControl.readUomById(drug.use_uom);
            DrugFrequency2 freq = theLookupControl.readDrugFrequencyById(drug.frequency);
            oid = theHO.initOrderItemDrug(drug, uom, freq);
        }
        intUDSaveOrderItem(or, oid, theHO.date_time, null, pt, v);
        return true;
    }

    /**
     * @param patient
     * @param v
     * @param ap
     * @param date_time
     * @throws Exception
     * @Author : amp
     * @date : 25/02/2549
     * @see : �ѹ�֡ order ��������ǧ˹��
     * @not deprecated ����ա�����¡��ҹẺ reused
     */
    protected void intCheckAppointmentOrder(Patient patient, Visit v, String date_time, Appointment ap) throws Exception {
        Vector vAppointmentOrder = theHosDB.theAppointmentOrderDB.selectByPatientAndAppointment(
                patient.getObjectId(), ap.getObjectId());
        ap.vn = v.vn;
        ap.visit_id = v.getObjectId();
        theHosDB.theAppointmentDB.update(ap);
        //insert order
        /////////////////////////////////////////////////////////////////////////////////////
        for (int i = 0, size = vAppointmentOrder.size(); i < size; i++) {
            AppointmentOrder apor = (AppointmentOrder) vAppointmentOrder.get(i);
            Item item = theHosDB.theItemDB.selectByPK(apor.item_id);
            if (item.active.equals("0") || item.active.equals("2")) {
                continue; // Feature #595
            }
            CategoryGroupItem cgi = theHosDB.theCategoryGroupItemDB.selectByPK(item.item_group_code_category);
            ItemPrice ip = intReadItemPriceByItemID(apor.item_id);
            Drug drug = cgi.category_group_code.equals(CategoryGroup.isDrug())
                    ? theHosDB.theDrugDB.selectByItem(item.getObjectId())
                    : null;
            ItemSupply supply = cgi.category_group_code.equals(CategoryGroup.isSupply())
                    ? theHosDB.theItemSupplyDB.selectByItemId(item.getObjectId())
                    : null;
            OrderItem or = theHO.initOrderItem(v.visit_type, item, cgi, ip, date_time, drug, supply);

            // ������Ǩ�ͺ����� drug ������� �������¡�˹� qty ��� OrderItemDrug
            OrderItemDrug oid = null;
            if (or.isDrug()) {
                if (apor.b_item_set_id != null && !apor.b_item_set_id.isEmpty()) {
                    DoseDrugSet doseDrugSet = theHosDB.theDoseDrugSetDB.selectByKeyDrugSet(apor.b_item_set_id);
                    if (doseDrugSet != null) {
                        Uom2 uom = theLookupControl.readUomById(doseDrugSet.use_uom);
                        DrugFrequency2 freq = theLookupControl.readDrugFrequencyById(doseDrugSet.frequency);
                        oid = theHO.initOrderItemDrug(doseDrugSet, uom, freq);
                        or.qty = doseDrugSet.qty;
                    }
                }
                if (oid == null) {
                    if (drug != null) {
                        or.qty = drug.qty;
                        Uom2 uom = theLookupControl.readUomById(drug.use_uom);
                        DrugFrequency2 freq = theLookupControl.readDrugFrequencyById(drug.frequency);
                        oid = theHO.initOrderItemDrug(drug, uom, freq);
                    }
                }
            }
            if (or.qty.equals("0")) {
                or.qty = "1";
            }
            Map<String, Object> properties = apor.getProperties();
            for (String key : properties.keySet()) {
                or.setProperty(key, properties.get(key));
            }
            // Must ordered before verify
            if (intSaveOrderItem(or, oid, date_time)) {
                // ������ѧ ������Ǫ�ѳ������ͧ verify
                if (or.getProperty("b_hstock_item_id") != null
                        && (or.isDrug() || or.isSupply())) {
                    continue;
                }
                // if item is due or ned not auto verify
                // check due
                if ("1".equals(hosControl.theLookupControl.readOption(false).enable_due)) {
                    if (isMappedDueByOrderId(or.getObjectId())) {
                        continue;
                    }
                }
                // check ned
                if ("1".equals(hosControl.theLookupControl.readOption(false).enable_ned)) {
                    if (isMappedNedByOrderId(or.getObjectId())) {
                        continue;
                    }
                }
                if ("1".equals(hosControl.theLookupControl.readOption(false).enable_order_doctor)) {
                    // �������㹡���� ä� ᾷ�� ���������ᾷ����Ѵ���
                    if (ap.doctor_code != null && !ap.doctor_code.isEmpty()) {
                        if (intIsMappedOrderDoctorByOrderId(or.getObjectId())) {
                            or.order_by_doctor = ap.doctor_code;
                            or.order_by_doctor_type = "0";
                        }
                    }
                }
                theHO.vOrderItem = theHosDB.theOrderItemDB.selectByVidTypeCancel(theHO.theVisit.getObjectId(), "", false);
                intVerifyOrderItem(or, date_time, ap.appointmenter);
            }
        }
        theHO.vOrderItem = theHosDB.theOrderItemDB.selectByVidTypeCancel(theHO.theVisit.getObjectId(), "", false);
        //////////////////////////////////////////////////////////////
        // QueueLabStatus
        theHO.theListTransfer = theHosDB.theQueueTransferDB.select2ByVisitID(theHO.theVisit.getObjectId());
        HashMap<String, String> status = OrderControl.checkLabAndXrayStatus(theHO.vOrderItem);
        if (theHO.theListTransfer != null) {
            theHO.theListTransfer.labstatus = status.get("lab");
            theHO.theListTransfer.xraystatus = status.get("xray");
            theHosDB.theQueueTransferDB.update(theHO.theListTransfer);
        }
        theHO.theVisit.queue_lab_status = status.get("lab");
        theHO.theVisit.queue_xray_status = status.get("xray");
        theHosDB.theVisitDB.updateLabAndXrayStatus(theHO.theVisit);
    }

    /*
     * inc is verify or cancel add or delete
     */
    protected boolean intSaveQueueOfOrder(int qty) throws Exception {
        return intSaveQueueOfOrder(null, true, theHO.date_time, theHO.theVisit, theHO.thePatient, qty, CategoryGroup.isDrug());
    }

    protected boolean intSaveQueueOfOrder(OrderItem oi, boolean inc, String date_time) throws Exception {
        return intSaveQueueOfOrder(oi, inc, date_time, theHO.theVisit, theHO.thePatient, -1, oi.category_group);
    }

    /**
     * ��÷ӧҹ�����������Ѻ�Ż��������������������§ҹ���ͧ
     * ��������Ţ��Ǩ��ҡ���һ��ԡ�����Ҩ��ź�ҡ�ѧ�ѹ�����
     *
     * ���ͧ�ҡ����ҡ��§ҹ���µ�ǡ��ͧ���¡�ѧ�ѹ������¤���
     * ����ԧ���Ǥ�è��Ҩҡ��ùѺ�����ش���·����������ѧ������Ż�ա�������
     *
     * @param oi
     * @param inc
     * @param date_time
     * @param visit
     * @param patient
     * @param qty
     * @param catG
     * @return
     * @throws java.lang.Exception
     */
    protected boolean intSaveQueueOfOrder(OrderItem oi, boolean inc, String date_time, Visit visit, Patient patient, int qty, String catG) throws Exception {
        // lab order
        if (catG.equals(CategoryGroup.isLab())) {
            QueueLab2 ql;
            if ("1".equals(oi.secret)) {
                ql = theHosDB.theQueueLabDB.select2ByVisitIDAndOrderId(visit.getObjectId(), oi.getObjectId());
            } else {
                ql = theHosDB.theQueueLabDB.select2ByVisitIDAndOrderId(visit.getObjectId(), "");
            }
            if (ql == null) {
                ql = new QueueLab2();
                ql.visit_id = visit.getObjectId();
                ql.patient_id = visit.patient_id;
                ql.number_order = "0";
                ql.remain = "0";
                if ("1".equals(oi.secret)) {
                    ql.order_id = oi.getObjectId();
                    ql.secret_code = theHosDB.theOrderLabSecretDB.selectByOrderId(ql.order_id).specimen_code;
                } else {
                    ql.order_id = "";
                    ql.secret_code = "";
                }
            }
            ql.assign_time = date_time;
            ql.last_service = theHO.theServicePoint.getObjectId();
            int queue = Integer.parseInt(ql.number_order);
            if (queue < 0) {
                queue = 0;
            }
            if (inc) {
                queue++;
            } else {
                queue--;
            }
            ql.number_order = String.valueOf(queue);
            if (ql.getObjectId() == null) {
                theHosDB.theQueueLabDB.insert(ql);
            }
            if (ql.getObjectId() != null && queue > 0) {
                theHosDB.theQueueLabDB.update(ql);
            }
            if (ql.getObjectId() != null && queue <= 0) {
                theHosDB.theQueueLabDB.delete(ql);
            }
        }
        /*
         * Xray
         */
        if (catG.equals(CategoryGroup.isXray())) {
            QueueXray ql = theHosDB.theQueueXrayDB.selectByVisitID(visit.getObjectId());
            if ((ql == null)) {
                ql = new QueueXray();
                ql.visit_id = visit.getObjectId();
                ql.patient_id = visit.patient_id;
                ql.number_order = "0";
            }
            ql.assign_time = date_time;
            ql.last_service = theHO.theServicePoint.getObjectId();
            int queue = Integer.parseInt(ql.number_order);
            if (queue < 0) {
                queue = 0;
            }
            if (inc) {
                queue++;
            } else {
                queue--;
            }
            ql.number_order = String.valueOf(queue);
            if (ql.getObjectId() == null) {
                theHosDB.theQueueXrayDB.insert(ql);
            }
            if (ql.getObjectId() != null && queue > 0) {
                theHosDB.theQueueXrayDB.update(ql);
            }
            if (ql.getObjectId() != null && queue == 0) {
                theHosDB.theQueueXrayDB.delete(ql);
            }
        }
        if (catG.equals(CategoryGroup.isDrug()) || catG.equals(CategoryGroup.isSupply())) {
            QueueDispense2 ql = theHosDB.theQueueDispense2DB.selectByVisitID(visit.getObjectId());
            if ((ql == null)) {
                ql = new QueueDispense2();
                ql.visit_id = visit.getObjectId();
                ql.patient_id = visit.patient_id;
                ql.number_order = "0";
                ql.active = Active.isEnable();
                ql.vn = visit.vn;
                ql.hn = patient.hn;
                ql.prename = patient.f_prefix_id;
                ql.firstname = patient.patient_name;
                ql.lastname = patient.patient_last_name;
            }
            ql.assign_time = date_time;
            ql.last_service = theHO.theServicePoint.getObjectId();
            ServicePoint sp = theHosDB.theServicePointDB.selectByPK(ql.last_service);
            ql.service_point_name = sp.name;
            ql.servicePointColor = sp.service_point_color;
            // Somprasong 10072012 add visit queue
            if (theHO.theListTransfer != null) {
                ql.visit_queue_name = theHO.theListTransfer.description;
                ql.visit_queue_color = theHO.theListTransfer.color;
                ql.visit_queue_number = theHO.theListTransfer.queue;
                ql.arrived_datetime = theHO.theListTransfer.arrived_datetime;
                ql.arrived_status = theHO.theListTransfer.arrived_status;
            }
            int numberOfOrder = Integer.parseInt(ql.number_order);
            if (qty >= 0) {
                numberOfOrder = qty;
            } else {
                if (numberOfOrder < 0) {
                    numberOfOrder = 0;
                }
                if (inc) {
                    numberOfOrder++;
                } else {
                    numberOfOrder--;
                }
            }

            ql.number_order = String.valueOf(numberOfOrder);
            if (ql.getObjectId() == null && numberOfOrder > 0) {
                theHosDB.theQueueDispense2DB.insert(ql);
            }
            if (ql.getObjectId() != null && numberOfOrder > 0) {
                theHosDB.theQueueDispense2DB.update(ql);
            }
            if (ql.getObjectId() != null && numberOfOrder <= 0) {
                theHosDB.theQueueDispense2DB.delete(ql);
                return false;
            }
        }
        return true;
    }

    public void setUpdateStatus(UpdateStatus us) {
        theUS = us;
        theDialogUseDoctor = new DAskDoctorClinic(theUS, theLookupControl);
    }

    public boolean cancelOrderItem(Vector<OrderItem> orderitems) {
        if (theHO.thePatient == null) {
            theUS.setStatus(("�ѧ������͡������"), UpdateStatus.WARNING);
            return false;
        }
        if (theHO.theVisit == null) {
            theUS.setStatus(("��س����͡�����·������㹡�кǹ���"), UpdateStatus.WARNING);
            return false;
        }
        if (theHO.theVisit.visit_status.equals(VisitStatus.isOutProcess())) {
            theUS.setStatus(("�����¨���кǹ��������������ö��䢢�������"), UpdateStatus.WARNING);
            return false;
        }
        if (orderitems == null || orderitems.isEmpty()) {
            theUS.setStatus(("�ѧ��������͡��¡�õ�Ǩ�ѡ��"), UpdateStatus.WARNING);
            return false;
        }
        if (!theUS.confirmBox(("�׹�ѹ���¡��ԡ��¡�õ�Ǩ�ѡ��"), UpdateStatus.WARNING)) {
            theUS.setStatus("¡��ԡ���¡��ԡ��¡�õ�Ǩ�ѡ��", UpdateStatus.WARNING);
            return false;
        }
        if (theLookupControl.readOption().discontinue.equals(Active.isEnable())) {
            boolean retb = DialogPasswd.showDialog(theHO, theUS, theHO.theEmployee.password);
            if (!retb) {
                theUS.setStatus(("���ʼ�ҹ���١��ͧ"), UpdateStatus.WARNING);
                return false;
            }
        }

        for (OrderItem oi : orderitems) {
            // ��ҹ��äԴ�Թ����
            if (oi.charge_complete.equals(Active.isEnable())) {
                theUS.setStatus("����¡�õ�Ǩ�ѡ�ҷ��Դ�Թ�����������ö¡��ԡ��", UpdateStatus.WARNING);
                return false;
            }
            if (oi.continue_order.equals(Active.isEnable())) {
                theUS.setStatus("����¡�õ�Ǩ�ѡ�ҷ����觵�����ͧ ��ͧ¡��ԡ�����觵�����ͧ��͹ �֧������ö¡��ԡ��", UpdateStatus.WARNING);
                return false;
            }
            if (oi.status.equals(OrderStatus.DIS_CONTINUE)) {
                theUS.setStatus("����¡�õ�Ǩ�ѡ�ҷ��١¡��ԡ���� �������ö¡��ԡ��", UpdateStatus.WARNING);
                return false;
            }
            if (oi.vertifier != null && !oi.status.equals(OrderStatus.NOT_VERTIFY)) {
                if (!oi.vertifier.equals(theHO.theEmployee.getObjectId())) {
                    if (!theHO.theGActionAuthV.isWritePermissionCancelOrder()) {
                        theUS.setStatus("������Է��¡��ԡ��¡�õ�Ǩ�ѡ�ҷ������¼���餹�����", UpdateStatus.WARNING);
                        return false;
                    }
                }
            }
        }

        boolean returnResult = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            doCheckOrderParentPackage(orderitems);
            boolean res = intCancelOrderItem(orderitems);
            if (!res) {
                throw new Exception();
            }
            doAfterCancelOrderItem(orderitems);
            theHO.theXObject = null;
            theConnectionInf.getConnection().commit();
            returnResult = true;
        } catch (Exception ex) {
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            hosControl.updateTransactionFail(HosControl.UNKONW, "¡��ԡ��¡�� Order");
        } finally {
            theConnectionInf.close();
        }
        if (returnResult) {
            theHS.theOrderSubject.notifyCancelOrderItem("���¡��ԡ��¡�õ�Ǩ�ѡ�� �������", UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "¡��ԡ��¡�� Order");
        }
        return returnResult;
    }

    protected void doAfterCancelOrderItem(Vector<OrderItem> orderItems) throws Exception {
    }

    public void intCancelOrderItemNotVerify(OrderItem oi) throws Exception {
        theHosDB.theOrderItemDrugDB.deleteByOrderItemId(oi.getObjectId());
        if (theLookupControl.readOption().isUseDrugInteract()) {
            theHosDB.theOrderDrugInteractionDB.deleteByOrderItemId(oi.getObjectId());
            theHosDB.theOrderDrugInteractionDB.deleteByInteractionItemId(oi.getObjectId());
        }
        theHosDB.theOrderItemDB.delete(oi);
    }

    public void doCancelOrderPackage(OrderItem oi) {
        theConnectionInf.open();
        try {

            theConnectionInf.getConnection().setAutoCommit(false);
            if (oi.status.equals(OrderStatus.NOT_VERTIFY)) {
                theHosDB.theOrderPackageDB.deleteByOrderId(oi.getObjectId(), !oi.isParentPackage());
            } else {
                theHosDB.theOrderPackageDB.inActiveByOrderId(oi.getObjectId(), !oi.isParentPackage());
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
        } finally {
            theConnectionInf.close();
        }
    }

    public void intCancelOrderItem(OrderItem oi) throws Exception {
        oi.status = OrderStatus.DIS_CONTINUE;
        oi.discontinue = theHO.theEmployee.getObjectId();
        oi.discontinue_time = theHO.date_time;
        if (theLookupControl.readOption().isUseDrugInteract()) {
            theHosDB.theOrderDrugInteractionDB.updateByOrderItemId(oi.getObjectId());
            theHosDB.theOrderDrugInteractionDB.updateByInteractionItemId(oi.getObjectId());
        }
        theHosDB.theOrderItemDB.update(oi);
    }

    /**
     * Module Dental �� //pu
     *
     * @param orderitem
     * @param select_row
     * @return
     * @throws Exception
     */
    public boolean intCancelOrderItem(Vector<OrderItem> orderItems) throws Exception {
        //������á�͡���˵ء��¡��ԡ��¡���Ż sumo -- 20/03/2549
        for (OrderItem oi : orderItems) {
            int ret = 0;
            if (oi.isLab() && !oi.status.equals(OrderStatus.NOT_VERTIFY)) {
                ret = DialogCauseCancelResultLab.showDialog(theUS, orderItems);
            }
            if (ret == 1) {
                break;
            }
            if (ret == 2) {
                theUS.setStatus("���¡��ԡ��¡���Ż �١¡��ԡ�¼����ҹ", UpdateStatus.WARNING);
                return false;
            }
        }
        // Somprasong add for LIS 23-09-2010
        List<OrderItem> orderLabs = new ArrayList<OrderItem>();
        List<OrderItem> orderXrays = new ArrayList<>();
        for (int i = orderItems.size() - 1; i >= 0; i--) {
            OrderItem oi = (OrderItem) orderItems.get(i);
            // ʶҹ� ����׹�ѹ
            if (oi.status.equals(OrderStatus.NOT_VERTIFY)) {
                intCancelOrderItemNotVerify(oi);
                orderItems.remove(i);
            } else {
                // ʶҹ��׹�ѹ,ʶҹд��Թ���,����
                intCancelOrderItem(oi);
                // Somprasong add for LIS Module 23-09-2010
                if (oi.isLab()) {
                    orderLabs.add(oi);
                    theHosDB.theResultLabDB.inactiveByOrderId(oi.getObjectId());
                } // �ѹ�֡ DiagICD9 �ѵ��ѵ� 㹡óշ���繼����¹͡����Դ Option �˹�� Setup sumo 05/09/2549
                // ��Ǩ�ͺ��� Order �������׹�ѹ�繷ѹ��������ͤ�Һ�ԡ��������� sumo 05/09/2549
                // ��Ǩ�ͺ��� Order ����׹�ѹ�ա�èѺ���Ѻ DiagIcd9 ����������� sumo 05/09/2549
                // list ������ DiagIcd9 �����ѹ�֡������ Visit �Ѩ�غѹ��������� henbe 13/09/2549
                else if (oi.isService()
                        || oi.isDental()) {
                    if (theLookupControl.readOption().auto_diag_icd9.equals(Active.isEnable())) {
                        // Somprasong 091111 ������õ�Ǩ�ͺ��� icd9 ��� map ����Ѻ order ������¡��ԡ�������ա������� ������¡��ԡ
                        Vector selectOrderItemMapIcd9 = theHosDB.theOrderItemDB.selectOrderItemMapIcd9(oi.visit_id, oi.item_code);
                        if (selectOrderItemMapIcd9.size() > 0) {
                            continue;
                        }
                        Vector vItem_service = theHosDB.theItemServiceDB.selectByItem(oi.item_code);
                        if (!vItem_service.isEmpty()) {
                            ItemService is = (ItemService) vItem_service.get(0);
                            if (!is.icd9_code.isEmpty()) {
                                DiagIcd9 dxicd9 = theHosDB.theDiagIcd9DB.selectIcd9(is.icd9_code, theHO.theVisit.getObjectId());
                                theDiagnosisControl.intDeleteDiagnosisIcd9(dxicd9, null);
                            }
                        }
                    }
                } else if (oi.isXray()) {
                    theHosDB.theOrderXrayDB.cancel(oi.getObjectId(),
                            oi.discontinue,
                            DateUtil.convertStringToDate(oi.discontinue_time, "yyyy-MM-dd,HH:mm:ss", DateUtil.LOCALE_TH));
                    orderXrays.add(oi);
                }
                intSaveQueueOfOrder(oi, false, theHO.date_time);
                theVisitControl.updateVisitUrgentStatus(theHO.theVisit, oi);
            }
        }
        theHO.vOrderItem = theHosDB.theOrderItemDB.selectByVidTypeCancel(theHO.theVisit.getObjectId(), "", false);
        theHO.theListTransfer = theHosDB.theQueueTransferDB.select2ByVisitID(theHO.theVisit.getObjectId());
        HashMap<String, String> status = OrderControl.checkLabAndXrayStatus(theHO.vOrderItem);
        if (theHO.theListTransfer != null) {
            theHO.theListTransfer.labstatus = status.get("lab");
            theHO.theListTransfer.xraystatus = status.get("xray");
            theHosDB.theQueueTransferDB.update(theHO.theListTransfer);
        }
        theHO.theVisit.queue_lab_status = status.get("lab");
        theHO.theVisit.queue_xray_status = status.get("xray");
        theHosDB.theVisitDB.updateLabAndXrayStatus(theHO.theVisit);
        // Somprasong 091111 update �������
        theHO.vDiagIcd9 = theHosDB.theDiagIcd9DB.selectByVid(theHO.theVisit.getObjectId(), Active.isEnable());
        // Somprasong add for LIS 23-09-2010
        this.intCancelLisOrder(orderLabs, theHO.theEmployee.getObjectId());
        // Somprasong 130213 update �������ź��¡���Ż�͡�ҡ �Ż refer out �ҡ�ء form
        theHosDB.theOrderItemLabreferoutDB.deleteFromOrderIds(orderLabs.toArray(new OrderItem[orderLabs.size()]));
        if (!orderXrays.isEmpty()) {
            checkXRayOrderStatus(theHO.theVisit, theHO.vOrderItem);
        }
        return true;
    }

    public static String checkLabStatus(Vector orderitem) {
        /**
         * ***************** ********************************************
         */
        /*
         * ��Ǩ�ͺ����ѧ���Ż����ѧ����§ҹ��������� ¡��ԡ���ź
         */
        int total_ver = 0;
        int total_rep = 0;
        int total_rem = 0;
        int total = 0;
        for (int i = 0, size = orderitem.size(); i < size; i++) {
            OrderItem oi = (OrderItem) orderitem.get(i);
            if (oi.isLab()
                    && !oi.status.equals(OrderStatus.DIS_CONTINUE)
                    && !oi.status.equals(OrderStatus.NOT_VERTIFY) //            && oi.secret.equals(Active.isDisable()) add by neung
                    ) {

                total += 1;
                if (oi.status.equals(OrderStatus.VERTIFY)
                        || oi.status.equals(OrderStatus.EXECUTE)) {
                    total_ver += 1;
                } else if (oi.status.equals(OrderStatus.REPORT)) {
                    total_rep += 1;
                } else if (oi.status.equals(OrderStatus.REMAIN)) {
                    total_rem += 1;
                }
            }
        }
        String status = QueueLabStatus.NOLAB;
        //������Ż
        if (total == 0) {
            status = QueueLabStatus.NOLAB;
        } //��ŧ���Ż �ѧ��§ҹ�����ú �ѧ����¡�÷���׹�ѹ��д��Թ�������
        else if (total_ver > 0 && total_rep == 0) {
            status = QueueLabStatus.WAIT;
        } //��§ҹ�źҧ��ǹ ¡�����¡�÷����ŧ��
        else if (total_rep > 0 && total_rep < total && total_ver > 0) {
            status = QueueLabStatus.SOMEREPORT;
        } //��ҧ�Źҹ ¡�����¡�÷����ŧ��
        else if (total_rem > 0 && total_ver == 0) {
            status = QueueLabStatus.REMAIN;
        } //��§ҹ��������Ƿ�����
        else if (total > 0 && total_rep == total) {
            status = QueueLabStatus.REPORT;
        }
        return status;
    }

    public static HashMap<String, String> checkLabAndXrayStatus(Vector<OrderItem> orderitem) {
        int lab_total_ver = 0, lab_total_rep = 0, lab_total_rem = 0, lab_total = 0;
        int total = 0, total_ver = 0, total_rep = 0;
        for (int i = 0, size = orderitem.size(); i < size; i++) {
            OrderItem oi = (OrderItem) orderitem.get(i);
            if (oi.isLab()
                    && !oi.status.equals(OrderStatus.DIS_CONTINUE)
                    && !oi.status.equals(OrderStatus.NOT_VERTIFY)) {
                lab_total += 1;
                if (oi.status.equals(OrderStatus.VERTIFY)
                        || oi.status.equals(OrderStatus.EXECUTE)) {
                    lab_total_ver += 1;
                } else if (oi.status.equals(OrderStatus.REPORT)) {
                    lab_total_rep += 1;
                } else if (oi.status.equals(OrderStatus.REMAIN)) {
                    lab_total_rem += 1;
                }
            } else if (oi.isXray()
                    && !oi.status.equals(OrderStatus.DIS_CONTINUE)
                    && !oi.status.equals(OrderStatus.NOT_VERTIFY)) {
                total += 1;
                if (oi.status.equals(OrderStatus.VERTIFY)
                        || oi.status.equals(OrderStatus.EXECUTE)) {
                    total_ver += 1;
                } else if (oi.status.equals(OrderStatus.REPORT)) {
                    total_rep += 1;
                }
            }
        }
        HashMap<String, String> vStatus = new HashMap<>();
        String status = QueueLabStatus.NOLAB;
        if (lab_total == 0) {
            status = QueueLabStatus.NOLAB;
        } //��ŧ���Ż �ѧ��§ҹ�����ú �ѧ����¡�÷���׹�ѹ��д��Թ�������
        else if (lab_total_ver > 0 && lab_total_rep == 0) {
            status = QueueLabStatus.WAIT;
        } //��§ҹ�źҧ��ǹ ¡�����¡�÷����ŧ��
        else if (lab_total_rep > 0 && lab_total_rep < lab_total && lab_total_ver > 0) {
            status = QueueLabStatus.SOMEREPORT;
        } //��ҧ�Źҹ ¡�����¡�÷����ŧ��
        else if (lab_total_rem > 0 && lab_total_ver == 0) {
            status = QueueLabStatus.REMAIN;
        } //��§ҹ��������Ƿ�����
        else if (lab_total > 0 && lab_total_rep == lab_total) {
            status = QueueLabStatus.REPORT;
        }
        vStatus.put("lab", status);
        String xray_status = QueueXrayStatus.NOXRAY;
        if (total == 0) {
            xray_status = QueueXrayStatus.NOXRAY;
        } else if (total_ver > 0 && total_rep == 0) {
            xray_status = QueueXrayStatus.WAIT;
        } else if (total > 0 && total_rep == total) {
            xray_status = QueueXrayStatus.REPORT;
        }
        vStatus.put("xray", xray_status);
        return vStatus;
    }

    /*
     * ��������ҷ��������������
     */
    boolean dispense_confirm = false;

    protected boolean isCanDispenseOrder(OrderItem oi) throws Exception {
        dispense_confirm = false;
        if (!(oi.isDrug() || oi.isSupply())) {
            /*
             * �����¡�ù������
             */
            theUS.setStatus(("��¡�� Order ������͡�ҧ��¡�������������Ǫ�ѳ���觨��������"), UpdateStatus.WARNING);
            return false;
        }
        if (!oi.status.equals(OrderStatus.EXECUTE)) {
            /*
             * �繼����¹͡
             */
            theUS.setStatus(("��¡���ҷ����������ʶҹ� '���Թ���' ���������ö������"), UpdateStatus.WARNING);
            return false;
        }
        if (oi.status.equals(OrderStatus.DISPENSE)) {
            /*
             * �繼����¹͡
             */
            theUS.setStatus(("��¡������ʶҹ�� ���� ����"), UpdateStatus.WARNING);
            return false;
        }
        if (theHO.theVisit != null) {
            boolean allergy = intCheckDrugAllergy(oi.item_code, theHO.thePatient.getObjectId());
            boolean un_billing = theHO.theVisit.visit_type.equals(VisitType.OPD)
                    && oi.charge_complete.equals(Active.isDisable());
            String warning = ("��¡�õ�Ǩ") + " " + oi.common_name + " ";
            if (allergy) {
                oi.drug_allergy = "1";
                warning += " " + ("��������");
            }
            if (un_billing && allergy) {
                warning += " " + ("���");
            }
            if (un_billing) {
                warning += " " + ("�ѧ�����Դ�Թ");
            }
            if ((allergy || un_billing) && !dispense_confirm) {
                boolean res1 = theUS.confirmBox(warning + " " + ("�׹�ѹ��è��� ?"), UpdateStatus.WARNING);
                if (!res1) {
                    return false;
                }
                dispense_confirm = true;
            }
        }
        return true;
    }

    /*
     * public void dispenseOrderItem(Vector orderitem,int[] select_row){
     */
    public void dispenseOrderItems(Vector vvisit, int[] select_row) {
        if (select_row.length == 0) {
            theUS.setStatus("�ѧ��������͡�����¨ҡ��Ǩ�����", UpdateStatus.WARNING);
            return;
        }
        if (theLookupControl.readOption().dispense.equals(Active.isEnable())) {
            boolean retb = DialogPasswd.showDialog(theHO, theUS, theHO.theEmployee.password);
            if (!retb) {
                theUS.setStatus("���ʼ�ҹ���١��ͧ", UpdateStatus.WARNING);
                return;
            }
        }
        int coutTotalDispened = 0;
        boolean isComplete = false;
        boolean isUnlockCurrentVisit = false;
        List<String> listNotExecute = new ArrayList<String>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String date_time = theLookupControl.intReadDateTime();
            for (int j = 0; j < select_row.length; j++) {
                QueueDispense2 qd = (QueueDispense2) vvisit.get(select_row[j]);
                Vector orderitem = theHosDB.theOrderItemDB.selectByVisitId(qd.visit_id);
                int countDispensed = 0;
                int countNotExecute = 0;
                for (int i = 0; i < orderitem.size(); i++) {
                    /*
                     * �������¹͡����ö������ҡ�͹�Դ�Թ��
                     */
                    OrderItem oi = (OrderItem) orderitem.get(i);
                    if ((oi.isDrug() || oi.isSupply())
                            && (oi.status.equals(OrderStatus.NOT_VERTIFY)
                            || oi.status.equals(OrderStatus.VERTIFY))) {
                        countNotExecute++;
                    }
                    if (!isCanDispenseOrder(oi)) {
                        continue;
                    }
                    oi.status = OrderStatus.DISPENSE;
                    oi.dispense = theHO.theEmployee.getObjectId();
                    oi.dispense_time = date_time;
                    theHosDB.theOrderItemDB.update(oi);
                    countDispensed++;
                    if (oi.isDrug()) {
                        OrderItemDrug oid = theHosDB.theOrderItemDrugDB.selectByOrderItemID(oi.getObjectId());
                        oid.modify = theHO.theEmployee.getObjectId();
                        oid.modify_time = date_time;
                        theHosDB.theOrderItemDrugDB.inActive(oid);
                        oid.setObjectId(null);
                        oid.active = Active.isEnable();
                        oid.status = OrderStatus.DISPENSE;
                        theHosDB.theOrderItemDrugDB.insert(oid);
                    }
                }
                if (countDispensed >= 0 && countNotExecute == 0) {
                    theHosDB.theQueueDispense2DB.deleteByVisitID(qd.visit_id);
                    if (theHO.theVisit != null && theHO.theVisit.getObjectId().equals(qd.visit_id)) {
                        isUnlockCurrentVisit = true;
                    }
                }
                if (countNotExecute > 0) {
                    listNotExecute.add(String.format("VN %s ���� %s �ӹǹ %d ��¡��", qd.vn, qd.firstname + " " + qd.lastname, countNotExecute));
                }
            }
            theHO.vXObject = vvisit;
            theHO.vxo_index = select_row;
            theHO.theXObject = null;
            theHO.select_visit_dispense_stock = null;
            theHO.theQueueDispense2V = null;
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, "�����觨�����¡�õ�Ǩ�ѡ��");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            if (!listNotExecute.isEmpty()) {
                String msg = "�������ö��觨�����ҧ��¡�����ͧ�ҡʶҹ�����繴��Թ��� �ѧ���\n";
                for (String str : listNotExecute) {
                    msg += str + "\n";
                }
                JOptionPane.showMessageDialog(theUS.getJFrame(), msg, "����͹", JOptionPane.WARNING_MESSAGE);
            }
            String status = "�����觨�����¡�õ�Ǩ�ѡ���������";
            if (coutTotalDispened > 0) {
                status = "�����觨�����¡�õ�Ǩ�ѡ��������� ��ҧ��¡�õ�Ǩ�ѡ���������ö�Ѵʵ�͡�� ��سҵ�Ǩ�ͺ�ӹǹ����ѧ";
                theHS.theOrderSubject.notifyDispenseOrderItem(status, UpdateStatus.WARNING);
            } else {
                theHS.theOrderSubject.notifyDispenseOrderItem(status, UpdateStatus.COMPLETE);
            }
            if (isUnlockCurrentVisit) {
                theVisitControl.unlockVisit();
            }
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "�����觨�����¡�õ�Ǩ�ѡ��");
        }
    }

    /*
     * public void dispenseOrderItem(Vector orderitem,int[] select_row){
     */
    public void dispenseOrderItem(Vector orderitem, int[] select_row) {
        boolean is_remain = true;
        boolean isComplete = false;
        if (theHO.theVisit == null) {
            theUS.setStatus(("��س����͡�����·������㹡�кǹ���"), UpdateStatus.WARNING);
            return;
        }
        if (select_row.length == 0) {
            theUS.setStatus(("�ѧ��������͡��¡�� Order"), UpdateStatus.WARNING);
            return;
        }
        if (theLookupControl.readOption().dispense.equals(Active.isEnable())) {
            boolean retb = DialogPasswd.showDialog(theHO, theUS, theHO.theEmployee.password);
            if (!retb) {
                theUS.setStatus(("���ʼ�ҹ���١��ͧ"), UpdateStatus.WARNING);
                return;
            }
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String date_time = theLookupControl.intReadDateTime();
            dispense_confirm = false;
            int work = 0;
            for (int i = 0; i < select_row.length; i++) {
                /*
                 * �������¹͡����ö������ҡ�͹�Դ�Թ��
                 */
                OrderItem oi = (OrderItem) orderitem.get(select_row[i]);
                if (!isCanDispenseOrder(oi)) {
                    continue;
                }
                work++;
                oi.status = OrderStatus.DISPENSE;
                oi.dispense = theHO.theEmployee.getObjectId();
                oi.dispense_time = date_time;
                theHosDB.theOrderItemDB.update(oi);
                // ��㹡��ź �͡�ҡ queue
                is_remain = intSaveQueueOfOrder(oi, false, date_time);//������/�Ǫ�ѳ��
                if (oi.isDrug()) {
                    OrderItemDrug oid = theHosDB.theOrderItemDrugDB.selectByOrderItemID(oi.getObjectId());
                    oid.modify = theHO.theEmployee.getObjectId();
                    oid.modify_time = date_time;
                    theHosDB.theOrderItemDrugDB.inActive(oid);
                    oid.setObjectId(null);
                    oid.active = Active.isEnable();
                    oid.status = OrderStatus.DISPENSE;
                    theHosDB.theOrderItemDrugDB.insert(oid);
                }
            }
            if (work > 0) {
                int drug_charge = 0;
                int drug_dispense = 0;
                for (int i = 0; i < theHO.vOrderItem.size(); i++) {
                    OrderItem oi = (OrderItem) theHO.vOrderItem.get(i);
                    if (oi.isDrug() || oi.isSupply()) {
                        if (oi.charge_complete.equals("1")) {
                            drug_charge++;
                            if (oi.status.equals(OrderStatus.DISPENSE)) {
                                drug_dispense++;
                            }
                        }
                    }
                }

                if (drug_dispense >= drug_charge && "1".equals(this.theHO.theVisit.is_discharge_money)) {
                    is_remain = false;
                    theHosDB.theQueueDispense2DB.deleteByVisitID(theHO.theVisit.getObjectId());
                }
                // reset
                theHO.vOrderItem = theHosDB.theOrderItemDB.selectByVidTypeCancel(theHO.theVisit.getObjectId(), "", false);
                theHO.vXObject = orderitem;
                theHO.vxo_index = select_row;
                theHO.theXObject = null;
                theHO.select_visit_dispense_stock = null;
                theHO.theQueueDispense2V = null;
                isComplete = true;
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, "��觨�����¡�õ�Ǩ�ѡ��");
            isComplete = false;
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            if (!is_remain) {//�����¨�˹��·ҧ����Թ���ǡ����Ŵ��͡������
                theVisitControl.unlockVisit(theHO.theVisit);
            } else {
                theHS.theOrderSubject.notifyDispenseOrderItem("�����觨�����¡�õ�Ǩ�ѡ�� �������", UpdateStatus.COMPLETE);
            }
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "��觨�����¡�õ�Ǩ�ѡ��");
        }
    }

    public boolean generateResultLab(Vector orderitem, int[] select_row) {
        theConnectionInf.open();
        try {
            for (int i = 0; i < select_row.length; i++) {
                OrderItem oi = (OrderItem) orderitem.get(select_row[i]);
                ////////////////////////////////////////////////////////////
                if (oi.isLab()) {
                    //���˵ؼźҧ���ҧ����ͧ�ѹ����͹���ͤ�����ʹ���㹡���׹�ѹ��¡��
                    try {
                        generateResultLab(oi);
                    } catch (Exception ex) {
                        theUS.setStatus(ex.getMessage(), UpdateStatus.ERROR);
                        LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
                    }
                }
            }
            return true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            return false;
        } finally {
            theConnectionInf.close();
        }

    }
    boolean execute_xray = false;

    /*
     * @deprecated �ѧ�Ҵ��������ó�ç����ö����Ҩо����
     * ʵԡ�����Ҵ���������� public void executeOrderItem(Vector
     * orderitem,int[] select_row){ QueueListDispense Order
     * ��ǹ��÷ӧҹ��鹤�è�������㹿ѧ�ѹ������������� control ����ó�
     */
    public List<OrderItem> executeOrderItem(Vector orderitem, int[] select_row, String eid) {
        return executeOrderItem(orderitem, select_row, eid, false);
    }

    public List<OrderItem> executeOrderItem(Vector orderitem, int[] select_row, String eid, boolean isFromTabLab) {
        List<OrderItem> verifies = new ArrayList<OrderItem>();
        if (theHO.theVisit == null) {
            theUS.setStatus(("��س����͡�����·������㹡�кǹ���"), UpdateStatus.WARNING);
            return verifies;
        }
        if (!isFromTabLab && theHO.theVisit.visit_status.equals(VisitStatus.isOutProcess())) {
            theUS.setStatus(("�����¨���кǹ��������������ö��䢢�������"), UpdateStatus.WARNING);
            return verifies;
        }
        if (select_row.length == 0) {
            theUS.setStatus(("�ѧ��������͡��¡�õ�Ǩ�ѡ��"), UpdateStatus.WARNING);
            return verifies;
        }
        if (theLookupControl.readOption().execute.equals(Active.isEnable())) {
            boolean retb = DialogPasswd.showDialog(theHO, theUS, theHO.theEmployee.password);
            if (!retb) {
                theUS.setStatus(("���ʼ�ҹ���١��ͧ"), UpdateStatus.WARNING);
                return verifies;
            }
        }

        if (!isFromTabLab) {
            orderitem = addOrderChildByParent(orderitem);
        }

        for (int i = 0; i < (isFromTabLab ? select_row.length : orderitem.size()); i++) {
            OrderItem oi = (OrderItem) orderitem.get(isFromTabLab ? select_row[i] : i);
            if (oi.status.equals(OrderStatus.VERTIFY)) {
                //������Է��㹡�ô��Թ�����¡���ź������˹�ҷ���ź��ҹ��
                if (oi.isLab()) {
                    if (theHO.theEmployee.authentication_id.equals(Authentication.LAB)
                            || theHO.theEmployee.authentication_id.equals(Authentication.HEALTH)
                            || theHO.theEmployee.authentication_id.equals(Authentication.ONE)
                            || theHO.theEmployee.authentication_id.equals(Authentication.NURSE)
                            || theHO.theEmployee.authentication_id.equals(Authentication.DOCTOR)
                            || theHO.theEmployee.authentication_id.equals(Authentication.IPD)) {
                        verifies.add(oi);
                    } else {
                        theUS.setStatus(("������Է��㹡�ô��Թ�����¡���ź������˹�ҷ���ź��ҹ��"), UpdateStatus.WARNING);
                    }
                } else if (oi.isXray() && !isFromTabLab) {
                    verifies.add(oi);
                } else if ((oi.isDrug()
                        || oi.isService()
                        || oi.isDental()//amp:18/02/2549 ����������� Order �ҧ�ѹ���������ö���Թ�����
                        || oi.isSupply()) && !isFromTabLab) {
                    verifies.add(oi);
                } else if (!isFromTabLab) {
                    verifies.add(oi);
                }
            }
        }
        if (verifies.isEmpty()) {
            theUS.setStatus(("��¡�÷�����͡�������¡�÷���׹�ѹ���� ����������Է���㹡�ô��Թ��¡�÷�����͡"), UpdateStatus.WARNING);
            return verifies;
        }
        theConnectionInf.open();
        // Somprasong add for LIS 23-09-2010
        List<OrderItem> orderLabs = new ArrayList<OrderItem>();
        // Somprasong end add for LIS 23-09-2010
        boolean returnResult = false;
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String date_time = theLookupControl.intReadDateTime();
            for (int i = 0; i < verifies.size(); i++) {
                execute_xray = false;
                OrderItem oi = (OrderItem) verifies.get(i);
                if (oi.status.equals(OrderStatus.VERTIFY)
                        || oi.status.equals(OrderStatus.REMAIN)) {
                    if (oi.isDrug()) {
                        OrderItemDrug oid = theHosDB.theOrderItemDrugDB.selectByOrderItemID(oi.getObjectId());
                        oid.modify = theHO.theEmployee.getObjectId();
                        if (eid.isEmpty()) {
                            oid.modify = theHO.theEmployee.getObjectId();
                        }
                        oid.modify_time = date_time;
                        theHosDB.theOrderItemDrugDB.inActive(oid);
                        oid.setObjectId(null);
                        oid.active = Active.isEnable();
                        oid.status = OrderStatus.EXECUTE;
                        theHosDB.theOrderItemDrugDB.insert(oid);
                    }
                    if (oi.isXray()) {
                        if (!theHO.theEmployee.authentication_id.equals(Authentication.XRAY) && !execute_xray) {
                            boolean ret = theUS.confirmBox(("��ͧ��ô��Թ�����¡�� Xray :")
                                    + " " + oi.common_name + " " + ("������� ?"), UpdateStatus.WARNING);
                            if (!ret) {
                                continue;
                            }
                            execute_xray = true;
                        }
                        oi.order_complete = Active.isEnable();
                        // sumo:23/02/2549 ᾷ����觴��Թ��ö������� xn ��ͧ���� xn ��� ��м����Թ��������ʢͧᾷ�줹������
                        if (theHO.thePatient.xn.isEmpty() && theHO.thePatient.xn.trim().length() == 0) {
                            theHO.thePatient.xn = theHosDB.theSequenceDataDB.updateSequence("xn", true);
                            theHosDB.thePatientDB.updateXN(theHO.thePatient);
                        }
                        ResultXRay theResultXRay = theHosDB.theResultXRayDB.selectOrderItemByVNItemId(oi.getObjectId(), oi.visit_id);
                        if (theResultXRay.xn.isEmpty() && theResultXRay.xn.trim().length() == 0) {
                            theResultXRay.xn = theHO.thePatient.xn;
                        }
                        theResultXRay.reporter = eid;
                        if (eid.isEmpty()) {
                            theResultXRay.reporter = theHO.theEmployee.getObjectId();
                        }
                        theResultXRay.record_time = theHO.date_time;
                        theResultXRay.xray_time = theHO.date_time.substring(11, 16);
                        theHosDB.theResultXRayDB.update(theResultXRay);
                        //tuk: 01/08/2549 �������ա��ź ��� xray ������ա�ô��Թ�������ء��¡��
                        intSaveQueueOfOrder(oi, false, date_time);//���Թ�����¡�� xray
                        // insert into t_order_xray for pacs
                        intInsertOrderXray(oi, eid, date_time);
                    }
                    oi.status = OrderStatus.EXECUTE;
                    oi.executer = eid.isEmpty() ? theHO.theEmployee.getObjectId() : eid;
                    oi.executed_time = date_time;
                    theHosDB.theOrderItemDB.update(oi);
                    // Somprasong add for LIS 23-09-2010
                    if (oi.isLab()) {
                        orderLabs.add(oi);
                    }
                    // Somprasong end add for LIS 23-09-2010
                }
            }
            theHO.vOrderItem = theHosDB.theOrderItemDB.selectByVidTypeCancel(theHO.theVisit.getObjectId(), "", false);

            theHO.vXObject = orderitem;
            theHO.vxo_index = select_row;
            theHO.theXObject = null;
            // Somprasong add for LIS 23-09-2010
            this.genLabNumber(orderLabs, theHO.theVisit.getObjectId(),
                    date_time,
                    theHO.theEmployee.getObjectId(),
                    theHO.theServicePoint.getObjectId());
            // Somprasong end add for LIS 23-09-2010
            this.checkXRayOrderStatus(theHO.theVisit, theHO.vOrderItem);
            theConnectionInf.getConnection().commit();
            returnResult = true;
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.UNKONW, "��觴��Թ��� ��¡�õ�Ǩ�ѡ��");
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (returnResult) {
            if (!orderLabs.isEmpty()) {
                theHS.theOrderSubject.notifyExecuteOrderItem(("��ô��Թ��õ�Ǩ�ѡ��") + " "
                        + ("�������") + " has_lab ", UpdateStatus.COMPLETE);
            } else {
                theHS.theOrderSubject.notifyExecuteOrderItem(("��ô��Թ��õ�Ǩ�ѡ��") + " "
                        + ("�������"), UpdateStatus.COMPLETE);
            }
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "��觴��Թ��� ��¡�õ�Ǩ�ѡ��");
        } else {
            verifies.clear();
        }
        return verifies;
    }

    public void intInsertOrderXray(OrderItem oi, String empId, String dbDateTime) throws Exception {
        OrderXray orderXray = new OrderXray();
        orderXray.t_visit_id = oi.visit_id;
        orderXray.t_order_id = oi.getObjectId();
        BItemXray itemXray = theHosDB.theBItemXrayDB.selectByItemId(oi.item_code);
        orderXray.b_modality_id = itemXray.b_modality_id;
        orderXray.accession_number = theHosDB.theOrderXrayDB.getAccessionNumber(theHO.theVisit.getObjectId(), theHO.theVisit.vn);
        orderXray.priority = theHO.theVisit.emergency.equals("3") ? "E"
                : (theHO.theVisit.visit_type.equals("0") ? "O" : "I");
        orderXray.order_status = "1";
        orderXray.pacs_status = "0";
        if (oi.order_user != null && !oi.order_user.isEmpty()) {
            Employee emp = theLookupControl.readEmployeeById(oi.order_user);
            if (emp.authentication_id.equals("3")) {
                orderXray.doctor_id = emp.getObjectId();
            }
        }
        if (oi.order_time != null && !oi.order_time.isEmpty()) {
            orderXray.order_datetime = DateUtil.convertStringToDate(oi.order_time, "yyyy-MM-dd,HH:mm:ss", DateUtil.LOCALE_TH);
        }
        orderXray.executor_id = empId.isEmpty() ? theHO.theEmployee.getObjectId() : empId;
        orderXray.execute_datetime = DateUtil.convertStringToDate(dbDateTime, "yyyy-MM-dd,HH:mm:ss", DateUtil.LOCALE_TH);
        theHosDB.theOrderXrayDB.insert(orderXray);
    }

    public boolean continueOrderItem(Vector orderitem, int[] row, Employee emp) {
        employeeSetContinue = "";
        if (emp != null) {
            employeeSetContinue = emp.getObjectId();
        }
        return continueOrderItem(orderitem, row);
    }

    /*
     *
     * �繡�� updateStatus order item �����Ẻ continue
     */
    public boolean continueOrderItem(Vector orderitem, int[] row) {
        if (theHO.theVisit == null) {
            theUS.setStatus(("�������ѧ����� Visit"), UpdateStatus.WARNING);
            return false;
        }
        if (theHO.theVisit.visit_status.equals(VisitStatus.isOutProcess())) {
            theUS.setStatus(("�����¨���кǹ��������������ö��䢢�������"), UpdateStatus.WARNING);
            return false;
        }
        if (row.length == 0) {
            theUS.setStatus(("�ѧ��������͡��¡�� Order"), UpdateStatus.WARNING);
            return false;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String date_time = theLookupControl.intReadDateTime();
            for (int i = 0; i < row.length; i++) {
                OrderItem oi = (OrderItem) orderitem.get(row[i]);
                if (oi.isChildPackage()) {
                    continue;
                }
                if (Integer.parseInt(oi.status) == 0) {
                    theUS.setStatus(("�������ö���������¡����觵�����ͧ��") + " "
                            + ("���ͧ�ҡ��¡���ѧ������׹�ѹ"), UpdateStatus.WARNING);
                    return false;
                }
                if (Integer.parseInt(oi.status) == 3) {
                    theUS.setStatus(("�������ö���������¡����觵�����ͧ��") + " "
                            + ("���ͧ�ҡ��¡�ö١¡��ԡ�����"), UpdateStatus.WARNING);
                    return false;
                }
                OrderContinue oc = theHosDB.theOrderContinueDB.selectByOrid(oi.getObjectId());
                if (oc != null) {
                    theUS.setStatus(("��¡�õ�Ǩ�������� off ���� �������ҵ�����ͧ��������") + " "
                            + ("��س����͡��¡������"), UpdateStatus.WARNING);
                    return false;
                }
                ///////////////////////////////////////////////////////////////
                //��Ǩ�ͺ�������¡���ҵ�����ͧ����ӡѹ�������
                int size = orderitem.size();
                boolean drugconrepeated = false;
                for (int j = 0; j < size; j++) {
                    OrderItem oitemp = (OrderItem) orderitem.get(j);
                    if (oitemp.item_code.equals(oi.item_code)
                            && oitemp.continue_order.equals(Active.isEnable())) {
                        theUS.setStatus(("��¡��") + " " + oi.common_name + " "
                                + ("�������ö�����¡�õ�����ͧ��") + " "
                                + ("���ͧ�ҡ����¡����觵�����ͧ����ӡѹ"), UpdateStatus.WARNING);
                        drugconrepeated = true;
                        break;
                    }
                }
                if (drugconrepeated == true) {
                    return false;
                }
                ///////////////////////////////////////////////////////////////
                //��Ǩ�ͺ�����¡������¡�õ�����ͧ���������ѧ
                if (!oi.continue_order.equals(Active.isEnable()) && !drugconrepeated) {
                    oi.continue_order = "1";
                    theHosDB.theOrderItemDB.update(oi);
                    //theHosDB.theOrderItemDB.updateNS(oi);
                    //////////////////////////////////////////////////
                    OrderContinue orderContinue = new OrderContinue();
                    orderContinue.date_continue = date_time;
                    orderContinue.user_continue = theHO.theEmployee.getObjectId();
                    orderContinue.visit_id = oi.visit_id;
                    orderContinue.order_item_id = oi.getObjectId();
                    orderContinue.date_off = "";
                    //���������˹�ҷ���衴����¡��ԡ�ҵ�����ͧ sumo--21/3/2549
                    orderContinue.doctor_set_continue = employeeSetContinue;
                    Item item = theHosDB.theItemDB.selectByPK(oi.item_code);
                    orderContinue.common_name = item != null ? item.common_name : oi.common_name;
                    orderContinue.doctor_set_off = "";
                    theHosDB.theOrderContinueDB.insert(orderContinue);
                }
            }
            theHO.vOrderItem = theHosDB.theOrderItemDB.selectByVidTypeCancel(theHO.theVisit.getObjectId(), "", false);
            theHO.vXObject = orderitem;
            theHO.vxo_index = row;
            theHO.theXObject = null;
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, "��觵�Ǩ��¡��Ẻ������ͧ");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theOrderSubject.notifyContinueOrderItem(("�����觵�Ǩ��¡��Ẻ������ͧ") + " "
                    + ("�������"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "��觵�Ǩ��¡��Ẻ������ͧ");
        }
        return isComplete;
    }

    public Vector listResultXrayPositionByResultXraySizeID(String xraysize_id) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theResultXrayPositionDB.selectByResultXRaySizeID(xraysize_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector readOrderLabByVNItemId(String itemid, String vn) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if ((itemid != null)) {
                vc = theHosDB.theResultLabDB.selectOrderItemByVNItemId(itemid, vn);
            } else {
                vc = theHosDB.theResultLabDB.selectOrderItemByVisit_ID(vn);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /*
     * printing
     */
    public Vector listOrderXrayByVN(String vn) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            //comment by tong ����������¹ function ��÷ӧҹ����
            vc = theHosDB.theOrderItemDB.selectOrderItemByVNCGForOption(
                    vn, CategoryGroup.isXray(), false, false);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listOrderXrayByVNForPrint(String vn, boolean showreport, boolean useshowexecute) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            //comment by tong ����������¹ function ��÷ӧҹ����
            vc = theHosDB.theOrderItemDB.selectOrderItemByVNCGForOption(
                    vn, CategoryGroup.isXray(), showreport, useshowexecute);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * ���� ��¡�� lab �ҡ���ҧ order ⴹ vn �� visit_id
     *
     * @param vn
     * @return
     */
    public Vector listOrderLabByVN(String vn) {
        return listOrderLabByVN(vn, true, true, false);
    }

    public Vector listOrderLabByVN(String vn, boolean report, boolean remain, boolean charge) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theOrderItemDB.selectOrderItemByVNCG(vn, CategoryGroup.isLab(), report, remain, charge);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     *
     * @Author: amp
     * @date : 07/03/2549
     * @see: ���͡ order �Ż
     * @return
     * @param vn
     */
    public Vector listOrderLabByVNAndSecret(String vn) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if ("".equals(theHO.orderSecret)) {
                vc = theHosDB.theOrderItemDB.selectOrderItemByVNAndOrderNotSecret(
                        vn, CategoryGroup.isLab(), true, true, false);
            } else {
                vc = theHosDB.theOrderItemDB.selectOrderItemByPk(theHO.orderSecret);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * ������¡���Ż������Ż��ػ��ҹ��
     *
     * @param vOrderItem
     * @return
     */
    public Vector listOrderLabGroup(Vector vOrderItem) {
        Vector vc = new Vector();
        Vector vLookup = theLookupControl.listLabSet();
        if (vLookup == null) {
            return vc;
        }
        if (vOrderItem != null) {
            int size = vOrderItem.size();
            int lsize = vLookup.size();
            OrderItem oi;
            for (int i = 0; i < size; i++) {
                oi = (OrderItem) vOrderItem.get(i);
                for (int j = 0; j < lsize; j++) {
                    LabGroup lg = (LabGroup) vLookup.get(j);
                    if (lg.item_id.equals(oi.item_code)) {
                        vc.add(oi);
                    }
                }
            }
        }
        return vc;
    }

    /**
     * ������¡���Ż������Ż����
     *
     * @param vOrderItem
     * @return
     */
    public Vector listOrderLabDetail(Vector vOrderItem) {
        Vector vc = new Vector();
        Vector vLookup = theLookupControl.listLabSet();
        if (vLookup == null) {
            return vc;
        }
        if (vOrderItem == null) {
            return vc;
        }

        int size = vOrderItem.size();
        int lsize = vLookup.size();
        for (int i = 0; i < size; i++) {
            boolean boo = false;
            OrderItem oi = (OrderItem) vOrderItem.get(i);
            for (int j = 0; j < lsize; j++) {
                LabGroup lg = (LabGroup) vLookup.get(j);
                if (lg.item_id.equals(oi.item_code)) {
                    boo = true;
                    break;
                }
            }
            if (boo == false) {
                vc.add(oi);
            }
        }
        return vc;
    }

    /*
     * printing
     */
    public Vector listOrderServiceByVN(String vn) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theOrderItemDB.selectOrderItemServicePrintByVisitId(vn);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /*
     * printing
     */
    public OrderItemDrug getOrderItemDrugByOrderItemId(String orderItemId) {
        OrderItemDrug orderItemDrug = new OrderItemDrug();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            orderItemDrug = theHosDB.theOrderItemDrugDB.selectByOrderItemID(orderItemId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return orderItemDrug;
    }

    public OrderItemDrug readOrderItemDrugByOid(String id) {
        OrderItemDrug oid = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            oid = theHosDB.theOrderItemDrugDB.selectByOrderItemID(id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return oid;
    }

    /*
     * printing
     */
    public Vector listOrderItemOrderByItemGroup(String visit_id) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theOrderItemDB.selectByVisitIdOrderByItemGroup(visit_id);

            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /*
     * printing
     */
    public Vector listOrderItemOrderByBillingGroup(String visit_id) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theOrderItemDB.selectByVisitIdOrderByBillingGroup(visit_id);

            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    protected int intDeleteAllResultXrayPositionbyXraySizeID(String xraysize_id) {
        result = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theResultXrayPositionDB.deleteAllResultXrayPositionbyXraySizeID(xraysize_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    public Vector listOrderContinueByVisitID(String visitId) {
        theHO.objectid = visitId;
        Vector vc = null;
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theOrderContinueDB.selectOCByVId(visitId);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, "�ٻ���ѵ��ҵ�����ͧ");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "�ٻ���ѵ��ҵ�����ͧ");
        }
        return vc;
    }

    public Vector listLabReferOutByVId(String vId, String search) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theOrderItemDB.selectLROByVId(vId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listOrderHistoryByNameGroupPatientId(String name, String group, String patientId) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theOrderItemDB.selectOrderItemByNameGroupPatientId(name, group, patientId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listOrderHistoryByNameDatePatientId(String name, String dateFrom, String dateTo, String patientId) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theOrderItemDB.selectOrderItemByNameDatePatientId(name, dateFrom, dateTo, patientId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listOrderHistoryByNamePatientId(String name, String patientId) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theOrderItemDB.listOrderHistoryByNamePatientId(name, patientId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listOrderHistoryByGroupDatePatientId(String group, String dateFrom, String dateTo, String patientId) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theOrderItemDB.selectOrderItemByGroupDatePatientId(
                    group, dateFrom, dateTo, patientId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listOrderHistoryByGroupPatientId(String group, String patientId) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theOrderItemDB.selectOrderItemByGroupPatientId(group, patientId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listOrderHistoryByDatePatientId(String dateFrom, String dateTo, String patientId) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theOrderItemDB.selectOrderItemByDatePatientId(dateFrom, dateTo, patientId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listOrderHistoryByPatientId(String patientId) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theOrderItemDB.listOrderHistoryByPatientId(patientId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public SpecialQueryOrderHistoryDrug selectOrderItemDrugByOrderItemId(String orderItemId) {
        SpecialQueryOrderHistoryDrug theSpecialQueryOrderHistoryDrug = new SpecialQueryOrderHistoryDrug();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theSpecialQueryOrderHistoryDrug = theHosDB.theSpecialQueryOrderHistoryDrugDB.selectByOrderItemID(orderItemId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theSpecialQueryOrderHistoryDrug;
    }
    ///////////////////////////////////////////////////////////////////////////

    /**
     * ��� ���������ö������¡�� Item ���� Group
     *
     * @param pkGroup
     * @return
     */
    public Vector listItemFromItemSetByItemGroupId(String pkGroup) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String sql = "select b_item.* \n"
                    + "from b_item_set \n"
                    + "inner join b_item on (b_item.b_item_id = b_item_set.b_item_id and b_item.item_active = '1') \n"
                    + "where b_item_group_id = '" + pkGroup + "'";
            vc = theHosDB.theItemDB.eQuery(sql);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listDrugSetByGroup(String pk) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theDrugSetDB.selectByGroup(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * ��㹡�ä������¡�� �ش��觵�Ǩ�ѡ�� �¡�ä���ҵ�� ���ͪش��觵�Ǩ�ѡ��
     * ���� ��Ңͧ �� choose �� True ���ҵ������ �� false ���ҵ��
     * �ش��觵�Ǩ�ѡ�� ���� ���ҵ������
     *
     * @param key
     * @param owner
     * @param is_owner
     * @return
     */
    public Vector listDrugSetGroupBySearch(String key, String owner, boolean is_owner) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            if (!is_owner) {
                owner = "%";
            }
            vc = theHosDB.theDrugSetGroupDB.selectByKeyOwner(key, owner);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * @param vOrderItem
     * @param new_order
     * @Author : henbe pongtorn
     * @date : 20/03/2549
     * @see : ��úѹ�֡ order Ẻ���µ�Ǿ�����ѹ ������кش�����ҵ�ͧ��� new
     * Order ������������
     * @parame : Vector vOrderItem boolean
     * @return : Null
     * �ջѭ������ͧ��ô֧�����Ũҡ�ҹ������˹ѡ�ҡ�е�ͧ��Ѻ������
     */
    public boolean saveVOrderItem(Vector vOrderItem, boolean new_order) {
        boolean isComplete = false;
        if (vOrderItem == null || vOrderItem.isEmpty()) {
            theUS.setStatus(("�������¡�õ�Ǩ�ѡ�ҷ��ӡ������"), UpdateStatus.WARNING);
            return false;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            int count = 0;
            String date_time = theLookupControl.intReadDateTime();
            theHO.is_order = false;
            Calendar cal = DateUtil.getCalendar(theHO.date_time);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < vOrderItem.size(); i++) {
                OrderItem oi = (OrderItem) vOrderItem.get(i);
                Item item = theHosDB.theItemDB.selectByPK(oi.item_code);
                if (item.active.equals("0") || item.active.equals("2")) {
                    sb.append(item.common_name).append("\n");
                    continue;
                }
                //��ǹ��¡������ ��˹�����繤����ҧ ���� �� 0
                oi.common_name = item.common_name; // ������¡�ù���ա����䢪�������
                oi.status = OrderStatus.NOT_VERTIFY;
                oi.continue_order = "0";
                oi.charge_complete = Active.isDisable();
                oi.secret = "0";
                oi.discontinue = "";
                oi.discontinue_time = "";
                oi.dispense = "";
                oi.dispense_time = "";
                oi.vertifier = "";
                oi.vertify_time = "";
                oi.executer = "";
                oi.executed_time = "";
                oi.visit_id = theHO.theVisit.getObjectId();
                oi.hn = theHO.theVisit.patient_id;
                oi.order_user = theHO.theEmployee.getObjectId();
                String rec_date_time = DateUtil.addSecond(cal, i);
                oi.order_time = rec_date_time;
                oi.clinic_code = theHO.theServicePoint.getObjectId();
                oi.order_price_type = theHO.theVisit.isVisitTypeIPD() ? "1" : "0"; // opd = 0, ipd = 1
                ItemPrice itemPrice = intReadItemPriceByItemID(oi.item_code);
                if (itemPrice != null) {
                    oi.price = Constant.doubleToDBString(
                            oi.isOPDPrice()
                            ? itemPrice.price : itemPrice.price_ipd);
                    oi.order_cost = Constant.doubleToDBString(itemPrice.price_cost);
                    oi.order_share_doctor = itemPrice.item_share_doctor;
                    oi.order_share_hospital = itemPrice.item_share_hospital;
                    oi.order_editable_price = itemPrice.item_editable_price;
                    oi.order_limit_price_min = itemPrice.item_limit_price_min;
                    oi.order_limit_price_max = itemPrice.item_limit_price_max;
                    oi.use_price_claim = itemPrice.use_price_claim;
                    oi.order_price_claim = itemPrice.item_price_claim;
                }

                oi.item_group_code_category = item.item_group_code_category;
                oi.item_group_code_billing = item.item_group_code_billing;

                // ����¡�á���� Order
                CategoryGroupItem cat = theLookupControl.readCategoryGroupItemById(oi.item_group_code_category);
                OrderItemDrug oid = null;
                if (cat.category_group_code.equals(Active.isEnable())) {
                    oid = theHosDB.theOrderItemDrugDB.selectByOrderItemID(oi.getObjectId());
                    oid.setObjectId(null);
                    oid.status = OrderStatus.NOT_VERTIFY;
                    if (oid.dose_short == null || oid.dose_short.isEmpty()) {
                        Uom2 uom = theLookupControl.readUomById(oid.use_uom);
                        DrugFrequency2 freq = theLookupControl.readDrugFrequencyById(oid.frequency);
                        if (uom != null && freq != null) {
                            oid.generateDoseShort(oid.dose, uom.uom_id, freq.drug_frequency_id);
                        }
                    }
                }
                if (new_order) {
                    oi.setObjectId(null);
                }
                boolean b = intSaveOrderItem(oi, oid, date_time, null);
                if (b) {
                    count++;
                }
            }
            if (sb.toString() != null && !sb.toString().trim().isEmpty()) {
                JOptionPane.showMessageDialog(null, "�к��������ö�����¡���������� inactive ����������� ������ �ѧ���\n"
                        + sb.toString(), "����¡���������� inactive ����������� ����", JOptionPane.WARNING_MESSAGE);
            }
            if (count == 0) {
                theUS.setStatus(("�������¡�÷��зӡ�úѹ�֡"), UpdateStatus.WARNING);
                throw new Exception("cn");
            }
            theHO.vXObject = vOrderItem;
            theHO.vxo_index = null;
            theHO.theXObject = null;
            theHO.vOrderItem = theHosDB.theOrderItemDB.selectByVidTypeCancel(theHO.theVisit.getObjectId(), "", false);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            if (!(ex.getMessage() != null && ex.getMessage().equals("cn"))) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                hosControl.updateTransactionFail(HosControl.UNKONW, "��úѹ�֡��¡�õ�Ǩ�ѡ��");
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theOrderSubject.notifySaveOrderItem(("��úѹ�֡��¡�õ�Ǩ�ѡ��") + " "
                    + ("�������"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "��úѹ�֡��¡�õ�Ǩ�ѡ��");
        }
        return isComplete;
    }

    public Vector listOrderItemByRange(boolean all, String dateFrom, String dateTo, String type, boolean show_cancel) {
        if (theHO.theVisit == null) {
            return null;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String visit_id = theHO.theVisit.getObjectId();
            if (all) {
                dateFrom = "";
                dateTo = "";
            }
            theHO.vOrderItem = theHosDB.theOrderItemDB.selectByDateTypeCancel(dateFrom, dateTo, visit_id, type, show_cancel);
            for (int i = 0; i < theHO.vOrderItem.size(); i++) {
                OrderItem oi = (OrderItem) theHO.vOrderItem.get(i);
                if (oi.isDrug()) {
                    oi.sDrugInteraction = intReadOrderDrugInteraction(oi);
                    if (theLookupControl.readOption().isShowDose()) {
                        oi.theOrderItemDrug = theHosDB.theOrderItemDrugDB.selectByOrderItemID(oi.getObjectId());
                    }
                }
            }
            theConnectionInf.getConnection().commit();
            return theHO.vOrderItem;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            return null;
        } finally {
            theConnectionInf.close();
        }
    }

    public void updateOrderItemPriceByFirstPlan(Visit theVisit, Vector<Payment> vVisitPayment, Vector<OrderItem> vOrderItem) {
        if (theVisit == null || vVisitPayment == null || vOrderItem == null) {
            return;
        }
        Payment payment = (Payment) vVisitPayment.get(0);
        if (payment == null) {
            return;
        }
        boolean ret = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ret = intUpdateOrderItemPriceByFirstPlan(theVisit, payment, vOrderItem);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (ret) {
            theHS.theOrderSubject.notifySaveOrderItem(Constant.getTextBundle("��úѹ�֡��¡�õ�Ǩ�ѡ��") + " "
                    + Constant.getTextBundle("�������"), UpdateStatus.COMPLETE);
        }
    }

    public boolean intUpdateOrderItemPriceByFirstPlan(Visit theVisit, Payment payment, Vector<OrderItem> vOrderItem) throws Exception {
        if (theVisit == null || payment == null || vOrderItem == null) {
            return false;
        }

        for (OrderItem orderItem : vOrderItem) {
            if (orderItem.charge_complete.equals("0")) {
                orderItem.order_price_type = theVisit.isVisitTypeIPD() ? "1" : "0"; // opd = 0, ipd = 1
                ItemPrice ip = this.intReadItemPriceByItem(orderItem, payment);
                orderItem.price = orderItem.isEditedPrice() // �������Ҥ�����������ͧ�Ѿഷ����Է��
                        ? orderItem.price
                        : Constant.doubleToDBString(orderItem.isOPDPrice()
                                ? ip.price
                                : ip.price_ipd);
                orderItem.order_cost = Constant.doubleToDBString(ip.price_cost);
                orderItem.order_share_doctor = ip.item_share_doctor;
                orderItem.order_share_hospital = ip.item_share_hospital;
                orderItem.order_editable_price = ip.item_editable_price;
                orderItem.order_limit_price_min = ip.item_limit_price_min;
                orderItem.order_limit_price_max = ip.item_limit_price_max;
                orderItem.use_price_claim = ip.use_price_claim;
                orderItem.order_price_claim = ip.item_price_claim;
                theHosDB.theOrderItemDB.update(orderItem);
            }
        }
        return true;
    }

    /**
     * Somprasong add 120810
     *
     * @param grpId
     * @param keyword
     * @param begin
     * @return
     */
    public Vector listItem(String grpId, String keyword, boolean begin) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            StringBuilder sql = new StringBuilder();
            sql.append("select * ");
            sql.append("from b_item ");
            sql.append("where ");
            sql.append("b_item.item_active='1' ");
            if (!grpId.isEmpty()) {
                sql.append("and b_item.b_item_subgroup_id = ? ");
            }
            sql.append("and (");
            sql.append("b_item.item_number ilike ? ");
            sql.append("or b_item.item_common_name ilike ? ");
            sql.append("or b_item.item_nick_name ilike ? ");
            sql.append("or b_item.item_trade_name ilike ?");
            sql.append(")  and (item_package_use_hstock is null\n"
                    + "    or item_package_use_hstock = '0') "
                    + "order by b_item.item_common_name");
            try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql.toString())) {
                int index = 1;
                if (!grpId.isEmpty()) {
                    ePQuery.setString(index++, grpId);
                }
                ePQuery.setString(index++, (begin ? "" : "%") + keyword + "%");
                ePQuery.setString(index++, (begin ? "" : "%") + keyword + "%");
                ePQuery.setString(index++, (begin ? "" : "%") + keyword + "%");
                ePQuery.setString(index++, (begin ? "" : "%") + keyword + "%");
                vc = theHosDB.theItemDB.eQuery(ePQuery.toString());
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * @Author: amp
     * @date: 25/7/2549
     * @see: ������¡�� Item ��¡����Ż���Դ
     * @param itemGrId
     * @param itemname
     * @param begin
     * @return
     */
    public Vector listItemByGroup(String itemGrId, String itemname, boolean begin) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            //��ҤӤ���Ẻ ��˹�Ҵ��¡�����ͧ��� % ˹�ҤӤ� ����������������� % ����
            itemname = itemname.trim() + "%";
            if (!begin) {
                itemname = "%" + itemname;
            }
            itemname = "%" + itemname.trim() + "%";
            vc = theHosDB.theItemDB.selectItemLabNotSecret(itemGrId, itemname);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * @Author: amp
     * @date: 25/7/2549
     * @see: ������¡�� Item ��¡����Ż���Դ
     * @param itemGrId
     * @param itemname
     * @return
     */
    public Vector listItemByGroup(String itemGrId, String itemname) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            itemname = "%" + itemname.trim() + "%";
            vc = theHosDB.theItemDB.selectItemLabNotSecret(itemGrId, itemname);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * ��¡�á�Ѻ��ҹ
     *
     * @param v
     * @param row
     * @param req
     * @return
     * @author Padungrat(tong)
     * @date 01/05/2549,11:33
     */
    public boolean saveOrderHome(Vector v, int[] row, boolean req) {
        if (row.length == 0) {
            theUS.setStatus(("��س����͡��¡�÷���ͧ��á�˹��������¡�á�Ѻ��ҹ"), UpdateStatus.WARNING);
            return false;
        }
        boolean is_outpc = theHO.theVisit.visit_status.equals(VisitStatus.isOutProcess());
        boolean is_lock = theHO.theVisit.locking.equals(Active.isEnable())
                && !theHO.theVisit.lock_user.equals(theHO.theEmployee.getObjectId());
        if (is_outpc || is_lock) {
            theUS.setStatus(("�����Ŷ١��͡�¼���餹���") + " "
                    + ("�������ö��䢢�������"), UpdateStatus.WARNING);
            return false;
        }
        for (int i = 0; i < row.length; i++) {
            OrderItem orderitem = (OrderItem) v.get(row[i]);
            if (orderitem.status.equals(Active.isDisable())) {
                theUS.setStatus(("����¡�õ�Ǩ�ѡ�������ʶҹ�����׹�ѹ"), UpdateStatus.WARNING);
                return false;
            }
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (int i = 0; i < row.length; i++) {
                OrderItem orderitem = (OrderItem) v.get(row[i]);
                //��Ң͵�Ǩ��ӡ�����¡��������ѹ�� ����¡��ԡ��â͵�Ǩ���ͧ��ҡ��������ѹ��Ѻ��
                //��෤�Ԥ��ä��ҹԴ�֧�ç����ä鹡�����ҡ�Ѻ��ҹ����������㹡�ä����
                //���� _OH �繤�����ѡ㹡�ä���
                if (req) {
                    orderitem.order_home = "1";
                    Item16Group ig = theHosDB.theItem16GroupDB.selectOrderHome();
                    // tuk: 15/08/2549  �� null �óշ�� 16 ������������ number ����Сͺ���� _OH
                    if (ig != null) {
                        orderitem.item_16_group = ig.getObjectId();
                    }
                } else {
                    orderitem.order_home = "0";
                    Item it = theHosDB.theItemDB.selectByPK(orderitem.item_code);
                    orderitem.item_16_group = it.item_16_group;
                }
                theHosDB.theOrderItemDB.update(orderitem);
                //theHosDB.theOrderItemDB.updateOrderHome(orderitem);
            }
            theHO.theXObject = null;
            theHO.vXObject = v;
            theHO.vxo_index = null;
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, "�ѹ�֡�����¡�á�Ѻ��ҹ");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theOrderSubject.notifySaveOrderRequest(("�ѹ�֡�����¡�á�Ѻ��ҹ") + " "
                    + ("�������"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "�ѹ�֡�����¡�á�Ѻ��ҹ");
        }
        return isComplete;
    }

    public boolean saveOrderRequest(Vector v, int[] row, boolean req) {
        if (row.length == 0) {
            theUS.setStatus(("��س����͡��¡�÷���ͧ��â͵�Ǩ"), UpdateStatus.WARNING);
            return false;
        }
        boolean is_outpc = theHO.theVisit.visit_status.equals(VisitStatus.isOutProcess());
        boolean is_lock = theHO.theVisit.locking.equals(Active.isEnable())
                && !theHO.theVisit.lock_user.equals(theHO.theEmployee.getObjectId());
        if (is_outpc || is_lock) {
            theUS.setStatus(("�����Ŷ١��͡�¼���餹���") + " "
                    + ("�������ö��䢢�������"), UpdateStatus.WARNING);
            return false;
        }
        for (int i = 0; i < row.length; i++) {
            OrderItem orderitem = (OrderItem) v.get(row[i]);
            if (orderitem.charge_complete.equals(Active.isEnable())) {
                theUS.setStatus(("����¡�õ�Ǩ�ѡ�ҷ������Թ�����������ö����¹�ŧ�����Ţ͵�Ǩ��"), UpdateStatus.WARNING);
                return false;
            }
            //���������Ǩ�ͺ�͡��ҵ�ͧ��Ǩ�ͺ 2 ��鹷�� Gui ��� control
            if (orderitem.status.equals(OrderStatus.DIS_CONTINUE)) {
                theUS.setStatus(("����¡�õ�Ǩ�ѡ�ҷ��¡��ԡ�����������ö����¹�ŧ�����Ţ͵�Ǩ��"), UpdateStatus.WARNING);
                return false;
            }
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (int i = 0; i < row.length; i++) {
                OrderItem orderitem = (OrderItem) v.get(row[i]);
                if (req) {
                    orderitem.request = "1";
                } else {
                    orderitem.request = "0";
                }
                theHosDB.theOrderItemDB.update(orderitem);
                //theHosDB.theOrderItemDB.updateNS(orderitem);
            }
            theHO.theXObject = null;
            theHO.vXObject = v;
            theHO.vxo_index = null;
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, "�ѹ�֡��â͵�Ǩ");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theOrderSubject.notifySaveOrderRequest(("�ѹ�֡��â͵�Ǩ") + " "
                    + ("�������"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "�ѹ�֡��â͵�Ǩ");
        }
        return isComplete;
    }

    public Drug listDrugByItem(String item) {
        Drug drug = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (theHO.theFamily != null && (theHO.theFamily.race_id != null && !theHO.theFamily.race_id.isEmpty())) {
                drug = theHosDB.theDrugDB.selectByItem(item, theHO.theFamily.race_id);
            } else {
                drug = theHosDB.theDrugDB.selectByItem(item);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return drug;
    }

    /*
     * print data
     */
    public String readDoctorPrintDrug(String visit_id) {
        String doctor_drug = "";
        result = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            doctor_drug = theHosDB.theSpecialEmployeeDB.queryByEmployeeName(visit_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return doctor_drug;
    }

    /**
     * @Author : sumo
     * @date : 01/06/2549
     * @see : ��û�Ѻ��ا��¡���Ż(�觵�Ǩ)
     * @param vOrderItemLab Vector �ͧ�������Ż������͡
     * @param select_row Array �Ǣͧ�����ż��Ż������͡,
     * @param refer boolean ������繡��¡��ԡ�����觵�Ǩ
     */
    public void referOutLab(Vector vOrderItemLab, int[] select_row, boolean refer) {
        boolean isUpdate = false;
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (int i = 0; i < select_row.length; i++) {
                OrderItem oi = (OrderItem) vOrderItemLab.get(select_row[i]);
                Vector vLRO = theHosDB.theOrderItemLabreferoutDB.selectByOrderId(oi.getObjectId());
                boolean can_unrefer = vLRO.isEmpty();
                if (!refer && !can_unrefer) {
                    theUS.setStatus(("�ա�úѹ�֡������ŧẺ���������觵�Ǩ�Ż��ѧ�͡�ç��Һ������") + " "
                            + ("�������ö¡��ԡ��"), UpdateStatus.WARNING);
                    throw new Exception("cn:�ա�úѹ�֡������ŧẺ���������觵�Ǩ�Ż��ѧ�͡�ç��Һ������ �������ö¡��ԡ��");
                }
                if (!refer && oi.refer_out.equals("0")) {
                    //���¡��ԡ����觵�Ǩ�ͧ��¡����纷��������觵�Ǩ ������ͧ�Ѿവ:aut
                    continue;
                }
                if (!refer) {
                    oi.refer_out = "0";
                } else {
                    oi.refer_out = "1";
                }
                //theHosDB.theOrderItemDB.updateRefer(oi);
                theHosDB.theOrderItemDB.update(oi);
                isUpdate = true;
            }

            if (!isUpdate) {
                theUS.setStatus("��¡������������ʶҹ��觵�Ǩ", UpdateStatus.WARNING);
                throw new Exception("cn:��¡������������ʶҹ��觵�Ǩ");
            }
            theHO.theXObject = null;
            theHO.vXObject = vOrderItemLab;
            theHO.vxo_index = select_row;
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            if (!ex.getMessage().startsWith("cn")) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                if (refer == false) {
                    hosControl.updateTransactionFail(HosControl.UNKONW, "¡��ԡ�觵�Ǩ�Ż");
                }
                if (refer == true) {
                    hosControl.updateTransactionFail(HosControl.UNKONW, "�觵�Ǩ�Ż");
                }
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theOrderSubject.notifyReferOutLab(("�����䢢����š���觵�Ǩ�ź") + " "
                    + ("�������"), UpdateStatus.COMPLETE);
            if (refer == false) {
                hosControl.updateTransactionSuccess(HosControl.UNKONW, "¡��ԡ�觵�Ǩ�Ż");
            }
            if (refer == true) {
                hosControl.updateTransactionSuccess(HosControl.UNKONW, "�觵�Ǩ�Ż");
            }
        }
    }

    public Vector listOrderItemByDate(String v) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theOrderItemDB.selectOrderItemByDate(v);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * ��੾�����¡�âͧ visit ����ա���׹�ѹ ���Թ��� ���� ��§ҹ�� ��ҧ��
     * ¡��� ����׹�ѹ ¡��ԡ
     *
     * @param visit_id
     * @return
     */
    public Vector listOrderItemByVidActive(String visit_id) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theOrderItemDB.selectOrderItemByVidActive(visit_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listOrderItemByVid(String v) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theOrderItemDB.selectOrderItemByVisitID(v);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listOrderItemByDay(String Date, String visit_id) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theOrderItemDB.selectOrderItemByDay(Date, visit_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listOrderItemReceiveDrugByVisitID(String visitId) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theOrderItemReceiveDrugDB.selectOIRDByVId(visitId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listFormLabReferOutByVID(String vId) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theFormLabreferoutDB.selectFLROByVId(vId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listLabReferOutByFLROID(String FLROID) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theOrderItemLabreferoutDB.selectLROByFLROID(FLROID);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public int deleteFormLabReferOut(FormLabreferout formLabreferout, Vector vLabreferout, UpdateStatus theUS) {
        int ans = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ans = theHosDB.theFormLabreferoutDB.delete(formLabreferout);
            for (int i = 0; vLabreferout != null && i < vLabreferout.size(); i++) {
                OrderItemLabreferout olro = (OrderItemLabreferout) vLabreferout.get(i);
                theHosDB.theOrderItemLabreferoutDB.delete(olro);
            }
            theUS.setStatus(("���ź��¡���觵�Ǩ�Ż�͡�ç��Һ��") + " "
                    + ("�������"), UpdateStatus.COMPLETE);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(("���ź��¡���觵�Ǩ�Ż�͡�ç��Һ��") + " "
                    + ("�Դ��Ҵ"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        return ans;
    }

    public Vector listOrderHistoryByNameGroupDatePatientId(String name, boolean is_group, String group, boolean is_date, String dateFrom, String dateTo, String patientId) {
        theHO.objectid = patientId;
        Vector vc = null;
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (is_group) {
                if (!name.isEmpty() && is_date) {
                    vc = theHosDB.theOrderItemDB.selectOrderItemByNameGroupDatePatientId(name, group, dateFrom, dateTo, patientId);
                } else if (!name.isEmpty() && !is_date) {
                    vc = theHosDB.theOrderItemDB.selectOrderItemByNameGroupPatientId(name, group, patientId);
                } else if (name.isEmpty() && !is_date) {
                    vc = theHosDB.theOrderItemDB.selectOrderItemByGroupPatientId(group, patientId);
                } else if (name.isEmpty() && is_date) {
                    vc = theHosDB.theOrderItemDB.selectOrderItemByGroupDatePatientId(group, dateFrom, dateTo, patientId);
                } else if (name.isEmpty() && is_date) {
                    vc = theHosDB.theOrderItemDB.selectOrderItemByGroupDatePatientId(group, dateFrom, dateTo, patientId);
                }
            } else {
                if (!name.isEmpty() && !is_date) {
                    vc = theHosDB.theOrderItemDB.listOrderHistoryByNamePatientId(name, patientId);
                } else if (name.isEmpty() && !is_date) {
                    vc = theHosDB.theOrderItemDB.listOrderHistoryByPatientId(patientId);
                } else if (name.isEmpty() && is_date) {
                    vc = theHosDB.theOrderItemDB.selectOrderItemByDatePatientId(dateFrom, dateTo, patientId);
                } else if (name.isEmpty() && is_date) {
                    vc = theHosDB.theOrderItemDB.selectOrderItemByNameDatePatientId(name, dateFrom, dateTo, patientId);
                }
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, "�ٻ���ѵ���¡�õ�Ǩ");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "�ٻ���ѵ���¡�õ�Ǩ");
        }
        return vc;
    }

    public OrderItem readOrderItemById(String orderItem_id) {
        OrderItem orderitem = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            orderitem = theHosDB.theOrderItemDB.selectByPK(orderItem_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return orderitem;
    }

    public Vector listOrderXrayAllByVN(String vid) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theOrderItemDB.selectOrderItemAllVnCG(vid, CategoryGroup.isXray());
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public ResultXRay readOrderXrayByVNItemId(String itemid, String vn) {
        ResultXRay theResultXRay = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theResultXRay = theHosDB.theResultXRayDB.selectOrderItemByVNItemId(itemid, vn);
            if (theResultXRay != null) {
                theHO.objectid = theResultXRay.getObjectId();
            }
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "�ټ���硫����");
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, "�ټ���硫����");
        } finally {
            theConnectionInf.close();
        }
        return theResultXRay;
    }
    /////////////////////////////////////////////////////////////////////////////

    /**
     * ��㹡�õ�Ǩ�ͺ�������¡���Ż����˹��ҧ����ѧ����յ����
     *
     * @param item_id
     * @return
     */
    public Vector listLabSetByItemID(String item_id) {
        Vector vLabset = null;
        LabGroup theLabGroup = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector vLookup = this.theLookupControl.listLabSet();
            int labgroupSize = vLookup.size();
            for (int i = 0; i < labgroupSize; i++) {
                LabGroup lg = (LabGroup) vLookup.get(i);
                if (lg.item_id.equals(item_id)) {
                    theLabGroup = lg;
                    break;
                }
            }
            if ((theLabGroup != null)) {
                vLabset = theHosDB.theLabSetDB.selectByLabGroupID(theLabGroup.getObjectId());
            } else {
                vLabset = null;
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vLabset;
    }

    public Vector listLabResultItem(Vector v) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (v != null && !v.isEmpty()) {
                vc = theHosDB.theSpecialQueryLabResultItem.listLabResultItemByItemID(v);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }
    //OrderContro
    int readLabResultItem_Call = 0;
    //�ӿѧ�ѹ������ͻ�ͧ�ѹ��� recursive �Թ�����繵�ǹѺ������¡����ͧ����ͺ����
    //�ҡ���¡�ҡ gui ��е�ͧ���¡ reset�� true �����������ҷ��йѺ

    public Vector readLabResultItem(String item_id) {
        Vector v = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            intReadLabResultItem(v, item_id, true);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            theUS.setStatus(ex.getMessage(), UpdateStatus.ERROR);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return v;
    }

    /**
     * �ѧ��鹹���ա�� limit ����������������ʴ����������ͻ�ͧ�ѹ��� overflow
     * �ͧ�����
     *
     * @param labresultitem
     * @param item_id
     * @param reset
     * @return
     * @throws Exception
     */
    protected boolean intReadLabResultItem(Vector labresultitem, String item_id, boolean reset) throws Exception {
        if (reset) {
            readLabResultItem_Call = 0;
        }
        //����ʴ���������´�ͧ���ź�Դ��Ҵ�ô��Ǩ�ͺ �������Դ error �����ѹ��з�����
        readLabResultItem_Call++;
        if (readLabResultItem_Call > ReadLabResultItem_Call_LIMIT) {
            theUS.setStatus(("����ʴ���������´�ͧ���ź�Դ��Ҵ�ô��Ǩ�ͺ ��¡���ź�ҡ˹�� Setup"), UpdateStatus.WARNING);
            return false;
        }

        Item item = theHosDB.theItemDB.selectByPK(item_id);
        Vector labset = theSetupControl.intListLabSetByItemId(item_id, item.secret);
        //�óշ�����Ż����
        if (labset.isEmpty()) {
            LabResultItem theLabResultItem = theHosDB.theLabResultItemDB.selectByItemID(item_id);
            if (theLabResultItem != null && !theLabResultItem.name.trim().isEmpty()) {
                labresultitem.add(theLabResultItem);
            }
        } //�óշ�����Ż�ش
        else {
            for (int i = 0; i < labset.size(); i++) {
                LabSet lset = (LabSet) labset.get(i);
                Vector labset1 = theSetupControl.intListLabSetByItemId(lset.item_id, item.secret);

                if (labset1.isEmpty()) {
                    LabResultItem theLabResultItem = theHosDB.theLabResultItemDB.selectByItemID(lset.item_id);
                    if (theLabResultItem != null && !theLabResultItem.name.trim().isEmpty()) {
                        labresultitem.add(theLabResultItem);
                    }
                } else {
                    for (int j = 0; j < labset1.size(); j++) {
                        LabSet lset1 = (LabSet) labset1.get(j);
                        LabResultItem theLabResultItem = theHosDB.theLabResultItemDB.selectByItemID(lset1.item_id);
                        if (theLabResultItem != null && !theLabResultItem.name.trim().isEmpty()) {
                            labresultitem.add(theLabResultItem);
                        }
                    }
                }
            }
        }
        return true;
    }

    public ContractAdjust readContractAdjustByCGaCT(String category_group_id, String contract_id) {
        if (contract_id == null) {
            return null;
        }
        ContractAdjust contractAdjust = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            contractAdjust = theHosDB.theContractAdjustDB.queryByCGaContractID(category_group_id, contract_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return contractAdjust;
    }

    /**
     * @Author : amp
     * @date : 02/03/2549
     * @see : ����������觵�Ǩ�ͧ�Ż���Դ�� order_id
     * @param order_id
     * @return OrderLabSecret
     */
    public OrderLabSecret readOrderLabSecret(String order_id) {
        OrderLabSecret theOrderLabSecret = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theOrderLabSecret = theHosDB.theOrderLabSecretDB.selectByOrderId(order_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theOrderLabSecret;
    }

    /**
     * �ѧ��ѹ㹡��ź��¡�� Xray ��ҹ��
     *
     * @param orderItem �� Object �ͧ OrderItem
     * @author padungrat(tong)
     * @date 13/03/49,10:37
     */
    public void deleteOrderItemXRayByXRay(OrderItem orderItem) {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theOrderItemDB.delete(orderItem);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
    }

    /**
     * @Author : amp
     * @date : 31/03/2549
     * @see : �� orderDrugInteraction
     * @param ord
     * @return String
     * @throws Exception
     */
    public String intReadOrderDrugInteraction(OrderItem ord) throws Exception {
        if (!theLookupControl.readOption().isUseDrugInteract()) {
            return "";
        }
        if (!ord.isDrug()) {
            return "";
        }
        String drug_interaction = "<br><br>" + ("�ջ�ԡ����Ҵѧ���");

        Vector vOrderDrugInteraction = theHosDB.theOrderDrugInteractionDB.selectByOrderItem(ord.getObjectId());
        boolean is_interaction = false;
        String standard_old = "";
        for (int i = 0, size = vOrderDrugInteraction.size(); i < size; i++) {
            if (i == 0) {
                is_interaction = true;
            }

            OrderDrugInteraction odi = (OrderDrugInteraction) vOrderDrugInteraction.get(i);
            if (odi.interaction_type.equals(DrugInteractionType.isDrug())
                    && !standard_old.equals(odi.interaction_item_drug_standard_id)) {
                drug_interaction += "<br><br>" + ("��ԡ����ҡѺ") + " : " + odi.interaction_item_drug_standard_description;
                drug_interaction += "&nbsp;&nbsp;&nbsp;" + ("�дѺ�����ç") + " : " + odi.interaction_force;
                drug_interaction += "<br>" + ("��ԡ����ҷ����Դ") + " : " + odi.interaction_act;
                drug_interaction += "<br>" + ("�Ը����") + " : " + odi.interaction_repair;
                standard_old = odi.interaction_item_drug_standard_id;
            }
            if (odi.interaction_type.equals(DrugInteractionType.isBloodPresure())) {
                drug_interaction += "<br><br>" + ("��ԡ����ҡѺ�����ѹ���") + " : " + odi.interaction_blood_presure;
                drug_interaction += "&nbsp;&nbsp;&nbsp;" + ("�дѺ�����ç") + " : " + odi.interaction_force;
                drug_interaction += "<br>" + ("��ԡ����ҷ����Դ") + " : " + odi.interaction_act;
                drug_interaction += "<br>" + ("�Ը����") + " : " + odi.interaction_repair;
            }
            if (odi.interaction_type.equals(DrugInteractionType.isPregnant())) {
                drug_interaction += "<br><br>" + ("��ԡ����ҡѺ��õ�駤����");
                drug_interaction += "&nbsp;&nbsp;&nbsp;" + ("�дѺ�����ç") + " : " + odi.interaction_force;
                drug_interaction += "<br>" + ("��ԡ����ҷ����Դ") + " : " + odi.interaction_act;
                drug_interaction += "<br>" + ("�Ը����") + " : " + odi.interaction_repair;
            }
        }
        if (!is_interaction) {
            drug_interaction = "";
        }
        ord.sDrugInteraction = drug_interaction;
        return drug_interaction;
    }

    /**
     * //henbe call Class ����� renderer ����÷������¡ control
     * �������������ա�� render ˹�Ҩ����� //�ѹ������¡�Դ��� control
     * ����ء���� �蹡�� alt tab ���¡�зӧҹ㹿ѧ�ѹ�������
     *
     *
     * @param oi
     * @param drug_interaction
     * @return
     */
    public String getTTTRenderDayOrder(OrderItem oi, String drug_interaction) {
        String day_verify = DateUtil.getDateToString(DateUtil.getDateFromText(oi.vertify_time), true);
        String staff_verify = "";
        String staff_execute = "";
        String staff_dispense = "";
        String staff_discontinue = "";
        String staff_report = "";
        if (oi.vertifier != null && !oi.vertifier.isEmpty()) {
            staff_verify = theLookupControl.readEmployeeNameById(oi.vertifier);
        }
        if (oi.executer != null && !oi.executer.isEmpty()) {
            staff_execute = theLookupControl.readEmployeeNameById(oi.executer);
        }
        if (oi.reporter != null && !oi.reporter.isEmpty()) {
            staff_report = theLookupControl.readEmployeeNameById(oi.reporter);
        }
        if (oi.dispense != null && !oi.dispense.isEmpty()) {
            staff_dispense = theLookupControl.readEmployeeNameById(oi.dispense);
        }
        if (oi.discontinue != null && !oi.discontinue.isEmpty()) {
            staff_discontinue = theLookupControl.readEmployeeNameById(oi.discontinue);
        }

        String ttt = ("����") + " : " + oi.common_name;
        if (!day_verify.isEmpty()) {
            ttt = ttt + "<br>" + HosObject.DATE_VERIFY + " : " + day_verify;
        }
        if (!staff_verify.isEmpty()) {
            ttt = ttt + "<br>" + HosObject.VERIFIER + " : " + staff_verify;
        }
        if (!staff_execute.isEmpty()) {
            ttt = ttt + "<br>" + HosObject.EXECUTER + " : " + staff_execute;
        }
        if (!staff_report.isEmpty()) {
            ttt = ttt + "<br>" + HosObject.REPORTOR + " : " + staff_report;
        }
        if (!staff_dispense.isEmpty()) {
            ttt = ttt + "<br>" + HosObject.DISPENSER + " : " + staff_dispense;
        }
        if (!staff_discontinue.isEmpty()) {
            ttt = ttt + "<br>" + HosObject.CANCEL + " : " + staff_discontinue;
        }
        if ("1".equals(oi.drug_allergy)) {
            if (theLookupControl.readOption().drug_standard_allergy.equals("1")) {
                ttt = ttt + "<br><br>" + HosObject.ALLERGY + ("�ҡ�������ǡѺ") + " : " + oi.common_name;
            } else {
                ttt = ttt + "<br><br>" + HosObject.ALLERGY + " : " + oi.common_name;
            }
        }

        if (!"".equals(drug_interaction)) {
            ttt += drug_interaction;
        }
        return ttt;
    }

    /**
     * ���� ��¡�� lab �ҡ���ҧ order ⴹ vn �� visit_id
     *
     * @param visit_id
     * @return
     */
    public Vector listOrderLabExeRepByVid(String visit_id) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theOrderItemDB.selectLabExeRep(visit_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listOrderLabInProcess(String visit_id) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theOrderItemDB.selectByVidCatStatus(visit_id,
                    CategoryGroup.isLab(),
                    OrderStatus.EXECUTE,
                    OrderStatus.REMAIN,
                    OrderStatus.REPORT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * @Author: sumo
     * @date : 05/09/2549
     * @see: ���͡ order ������ʢͧ Item_Id �Ѻ Visit_Id
     * @return Vector �ͧ��¡�� Order ��������
     * @param item_id
     * @param visit_id
     */
    public Vector listOrderItemByItemIdAndVid(String item_id, String visit_id) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theOrderItemDB.selectOrderItemByItemIdAndVid(item_id, visit_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listEVitalSign(String patient_id) {
        Vector vVital = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            java.sql.ResultSet rs = theConnectionInf.eQuery(
                    "select record_date||','|| record_time"
                    + " ,visit_vital_sign_weight"
                    + " ,visit_vital_sign_height "
                    + " ,visit_vital_sign_bmi "
                    + " ,visit_vital_sign_temperature"
                    + " ,visit_vital_sign_blood_presure"
                    + " ,visit_vital_sign_heart_rate"
                    + " ,visit_vital_sign_respiratory_rate"
                    + " ,t_visit_vital_sign.visit_vital_sign_waistline_inch, visit_vital_sign_hips_inch "
                    + " from t_visit_vital_sign "
                    + " where t_visit_vital_sign.t_patient_id = '" + patient_id + "' and t_visit_vital_sign.visit_vital_sign_active ='1' "
                    + " order by record_date desc, record_time desc");
            while (rs.next()) {
                String[] data = new String[10];
                for (int i = 0; i < data.length; i++) {
                    data[i] = rs.getString(i + 1);
                }
                vVital.add(data);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vVital;
    }

    public Vector listELab(String patient_id) {
        Vector vLab = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            java.sql.ResultSet rs = theConnectionInf.eQuery(
                    " select record_date_time "
                    + " ,t_order.order_common_name"
                    + " ,result_lab_name"
                    + " ,result_lab_value"
                    + "|| ' ' || result_lab_unit "
                    + " ,result_lab_min ||'-'||result_lab_max"
                    + " from t_result_lab "
                    + " inner join t_order on t_order.t_order_id = t_result_lab.t_order_id "
                    + " inner join t_visit on t_order.t_visit_id = t_visit.t_visit_id"
                    + " where t_visit.t_patient_id = '" + patient_id + "' "
                    + " and t_order.f_order_status_id = '4'");
            while (rs.next()) {
                String[] data = new String[5];
                for (int i = 0; i < data.length; i++) {
                    data[i] = rs.getString(i + 1);
                }
                vLab.add(data);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vLab;
    }

    public Vector listEXray(String patient_id) {
        Vector vXray = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            java.sql.ResultSet rs = theConnectionInf.eQuery(
                    " select order_date_time"
                    + " ,order_common_name"
                    + " ,result_xray_description "
                    + " ,t_order.t_order_id"
                    + " from t_order"
                    + " inner join t_result_xray on t_order.t_order_id = t_result_xray.t_order_id "
                    + " where f_item_group_id = '" + CategoryGroup.isXray() + "'"
                    + " and t_order.t_patient_id = '" + patient_id + "'"
                    + " and t_order.f_order_status_id = '4'");
            while (rs.next()) {
                String[] data = new String[4];
                for (int i = 0; i < data.length; i++) {
                    data[i] = rs.getString(i + 1);
                }
                vXray.add(data);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vXray;
    }

    public List<OrderItem> executeOrderItem(Vector vOrderItemLab, int[] a) {
        return executeOrderItem(vOrderItemLab, a, "");
    }

    public Vector listDrugV(String item_id) {
        Vector vret = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector vdrug = theHosDB.theDrugDB.selectByItemV(item_id);

            for (int i = 0; i < vdrug.size(); i++) {
                Drug drug = (Drug) vdrug.get(i);
                vret.add(new String[]{drug.getObjectId(), drug.description});
                theConnectionInf.getConnection().commit();
            }
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vret;
    }

    public Drug readDrugById(String drug_id) {
        Drug drug = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            drug = theHosDB.theDrugDB.selectByPK(drug_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return drug;
    }

    /**
     * Somprasong use for gen lab number for lis module
     *
     * @param orderLabs
     */
    private void genLabNumber(List<OrderItem> orderLabs, String visitId,
            String execDatetime, String execStaffId,
            String servicePointId) throws Exception {
        if (orderLabs.isEmpty()) {
            return;
        }
        String labNumber = theHosDB.theLisLnDB.getLabNumber(visitId, execDatetime,
                execStaffId, servicePointId);
        for (OrderItem orderLab : orderLabs) {
            LisOrder lisOrder = new LisOrder();
            lisOrder.ln = labNumber;
            lisOrder.order_id = orderLab.getObjectId();
            lisOrder.status = "1";
            lisOrder.exec_staff = execStaffId;
            theHosDB.theLisOrderDB.insert(lisOrder);
        }
    }

    protected void intCancelLisOrder(List<OrderItem> orderLabs, String cancelStaff) throws Exception {
        if (orderLabs.isEmpty()) {
            return;
        }
        for (OrderItem orderLab : orderLabs) {
            LisOrder lisOrder = theHosDB.theLisOrderDB.getLisOrderbyOrderId(orderLab.getObjectId());
            if (lisOrder == null) {
                continue;
            }
            lisOrder.status = "0";
            lisOrder.cancel_staff = cancelStaff;
            theHosDB.theLisOrderDB.updateCancel(lisOrder);
        }
    }

    public String getLabNumber(String objectId) {
        String labNobyOrderId = "";
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            labNobyOrderId = theHosDB.theLisOrderDB.getLabNobyOrderId(objectId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return labNobyOrderId;
    }

    public Vector listSpecialResultLabByPatientId() {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date curDate = new Date();
            Date flagDate = new Date(curDate.getYear(), 10, 1);
            String startDate = dateFormat.format(flagDate);
            String endDate = dateFormat.format(curDate);
            if (curDate.before(flagDate)) {
                Date date = new Date(curDate.getYear() - 1, 10, 1);
                startDate = dateFormat.format(date);
                endDate = dateFormat.format(curDate);
            }
            vc = theHosDB.theSpecialQueryResultLabDB.queryLabAfbTbByPatientID(theHO.thePatient.getObjectId(), startDate, endDate);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public List<HosStdDatasource> listMatchOrderDueByOrderIds(String visitId, String... ids) {
        List<HosStdDatasource> datasources = new ArrayList<HosStdDatasource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> list = (theHosDB.theMapDueDB.listMatchOrderByOrderIds(visitId, ids));
            for (Object[] objects : list) {
                HosStdDatasource datasource = new HosStdDatasource();
                datasource.addMapKey("t_order_id", String.valueOf(objects[0]));
                datasource.addMapKey("map_due_type", String.valueOf(objects[1]));
                datasource.addMapKey("b_due_type_id", String.valueOf(objects[2]));
                datasource.add(datasources.size() + 1);
                for (int i = 3; i < objects.length; i++) {
                    datasource.add(String.valueOf(objects[i]));
                }
                datasources.add(datasource);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return datasources;
    }

    public List<HosStdDatasource> listMatchOrderNedByOrderIds(String visitId, String... ids) {
        List<HosStdDatasource> datasources = new ArrayList<HosStdDatasource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> list = theHosDB.theMapNedDB.listMatchOrderByOrderIds(visitId, ids);
            for (Object[] objects : list) {
                HosStdDatasource datasource = new HosStdDatasource();
                datasource.addMapKey("t_order_id", String.valueOf(objects[0]));
                datasource.add(datasources.size() + 1);
                for (int i = 1; i < objects.length; i++) {
                    datasource.add(String.valueOf(objects[i]));
                }
                datasources.add(datasource);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return datasources;
    }

    public List<OrderNed> listOrderNedByOrderId(String orderId) {
        List<OrderNed> orderNeds = new ArrayList<OrderNed>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            orderNeds.addAll(theHosDB.theOrderNedDB.listByOrderId(orderId));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return orderNeds;
    }

    public List<OrderDue> listOrderDueByOrderId(String orderId) {
        List<OrderDue> orderDues = new ArrayList<OrderDue>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            orderDues.addAll(theHosDB.theOrderDueDB.listByOrderId(orderId));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return orderDues;
    }

    public int saveOrderNed(String orderId, OrderNed... orderNeds) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLookupControl.intReadDateTime();
            theHosDB.theOrderNedDB.deleteAllByOrderId(orderId);
            for (OrderNed orderNed : orderNeds) {
                orderNed.user_record_id = theHO.theEmployee.getObjectId();
                orderNed.record_date_time = theHO.date_time;
                theHosDB.theOrderNedDB.insert(orderNed);
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public int saveOrderDue(String orderId, OrderDue... orderDues) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLookupControl.intReadDateTime();
            theHosDB.theOrderDueDB.deleteAllByOrderId(orderId);
            for (OrderDue orderNed : orderDues) {
                orderNed.user_record_id = theHO.theEmployee.getObjectId();
                orderNed.record_date_time = theHO.date_time;
                theHosDB.theOrderDueDB.insert(orderNed);
            }
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public List<DueTypeDetail> listDueDetailByDueTypeId(String id) {
        List<DueTypeDetail> orderDues = new ArrayList<DueTypeDetail>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            orderDues.addAll(theHosDB.theDueTypeDetailDB.listByDueTypeId(id));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return orderDues;
    }

    public boolean isMappedDueByOrderId(String orderId) {
        boolean res = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            res = theHosDB.theMapDueDB.isMappedDueByOrderId(orderId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public boolean isMappedNedByOrderId(String orderId) {
        boolean res = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            res = theHosDB.theMapNedDB.isMappedNedByOrderId(orderId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public boolean isMappedOrderDoctorByOrderId(String orderId) {
        boolean res = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            res = intIsMappedOrderDoctorByOrderId(orderId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public boolean intIsMappedOrderDoctorByOrderId(String orderId) throws Exception {
        return theHosDB.theMapOrderDoctorDB.isMappedOrderDoctorByOrderId(orderId);
    }

    public boolean saveOrderItemFromDrugFavorite(DrugFavorite... itemFavorites) {
        if (itemFavorites == null || itemFavorites.length == 0) {
            theUS.setStatus(("�������¡���ҷ��ӡ������"), UpdateStatus.WARNING);
            return false;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        boolean haveWaring = false;
        String item_name = "";
        StringBuilder sb = new StringBuilder();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String date_time = theLookupControl.intReadDateTime();
            //ǹ�ٻ���ͺѹ�֡�Ҫش�������ö�ѹ�֡��
            theHO.is_order = false;//amp:5/6/2549 �����������͹�������� DrugInteraction �ء���駷���ա������Ҫش
            for (DrugFavorite itemFavorite : itemFavorites) {
                Item item = theHosDB.theItemDB.selectByPK(itemFavorite.b_item_id);
                if (item.active.equals("0") || item.active.equals("2")) {
                    sb.append(item.common_name).append("\n");
                    continue;
                }
                if (itemFavorite.b_hstock_item_id != null && !itemFavorite.b_hstock_item_id.isEmpty()) {
                    item.setProperty("b_hstock_item_id", itemFavorite.b_hstock_item_id);
                }
                //init order item
                CategoryGroupItem cgi = theLookupControl.readCategoryGroupItemById(
                        item.item_group_code_category);
                ItemPrice iPrice = intReadItemPriceByItemID(item.getObjectId());
                if (iPrice == null) {
                    item_name = item_name + " " + item.common_name;
                    continue;
                }
                Drug drug = cgi.category_group_code.equals(CategoryGroup.isDrug())
                        ? theHosDB.theDrugDB.selectByItem(item.getObjectId())
                        : null;
                ItemSupply supply = cgi.category_group_code.equals(CategoryGroup.isSupply())
                        ? theHosDB.theItemSupplyDB.selectByItemId(item.getObjectId())
                        : null;
                OrderItem oi = theHO.initOrderItem(theHO.theVisit.visit_type, item, cgi, iPrice, date_time, drug, supply);
                OrderItemDrug oid = new OrderItemDrug();
                oid.instruction = itemFavorite.instruction_id;
                oid.printing = drug.printting;
                oid.purch_uom = itemFavorite.purch_uom_id;
                oid.usage_special = itemFavorite.usage_special;
                oid.usage_text = itemFavorite.usage_text;
                oid.use_uom = itemFavorite.use_uom_id;
                oid.frequency = itemFavorite.frequency_id;
                oid.caution = itemFavorite.caution;
                oid.description = itemFavorite.description;
                oid.dose = itemFavorite.dose;
                oid.item_id = itemFavorite.b_item_id;
                oi.qty = itemFavorite.qty;
                if (oi.qty.isEmpty() || oi.qty.equals("0")) {
                    oi.qty = "1";
                }
                Uom2 uom = theLookupControl.readUomById(oid.use_uom);
                DrugFrequency2 freq = theLookupControl.readDrugFrequencyById(oid.frequency);
                if (uom != null && freq != null) {
                    oid.generateDoseShort(oid.dose, uom.uom_id, freq.drug_frequency_id);
                }
                intSaveOrderItem(oi, oid, date_time);
            }
            if (item_name.isEmpty() && sb.toString().trim().isEmpty()) {
                theHO.vOrderItem = theHosDB.theOrderItemDB.selectByVidTypeCancel(theHO.theVisit.getObjectId(), "", false);
            } else {
                haveWaring = true;
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            theUS.setStatus("��úѹ�֡ �Դ��Ҵ", UpdateStatus.ERROR);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            if (haveWaring) {
                if (sb.toString() != null && !sb.toString().trim().isEmpty()) {
                    theHS.theOrderSubject.notifySaveOrderItem("����¡�õ�Ǩ�ѡ�ҷ�� inactive ����������� ����", UpdateStatus.WARNING);

                    JOptionPane.showMessageDialog(null, "�к��������ö�����¡�õ�Ǩ�ѡ�ҷ�� inactive ����������� ������ �ѧ���\n"
                            + sb.toString(), "����¡�õ�Ǩ�ѡ�ҷ�� inactive ����������� ����", JOptionPane.WARNING_MESSAGE);
                } else {
                    theHS.theOrderSubject.notifySaveOrderItem("��辺�ҤҢͧ��¡�õ�Ǩ�ѡ�� " + item_name
                            + "  �ô�駼������к�", UpdateStatus.WARNING);
                }
            } else {
                theHS.theOrderSubject.notifySaveOrderItem("��úѹ�֡ �������", UpdateStatus.COMPLETE);
            }
        }
        return isComplete;
    }

    private void checkXRayOrderStatus(Visit theVisit, Vector<OrderItem> vOrderItem) throws Exception {
        int countXray = 0;
        for (OrderItem oi : vOrderItem) {
            if (oi.isXray()) {
                countXray++;
            }
        }
        if (countXray == 0) {
            theHosDB.theQueueXrayDB.deleteByVisitID(theVisit.getObjectId());
        }
    }

    public int updateOrderItemUrgentStatus(OrderItemDrug orderItemDrug, String urgentStatus, OrderItem oi, Visit visit) {
        if (oi == null || visit == null) {
            return 0;
        }
        int ret = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (oi.isDrug()) {
                theHosDB.theOrderItemDrugDB.updateDrugStat(orderItemDrug);
            } else {
                theHosDB.theOrderItemDB.updateOrderUrgentStatus(urgentStatus, oi.getObjectId());
            }
            theVisitControl.updateVisitUrgentStatus(visit, oi);
            ret = 1;
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            ret = 0;
        } finally {
            theConnectionInf.close();
        }
        if (ret != 0) {
            theHS.theOrderSubject.notifySaveOrderItem(("��úѹ�֡ʶҹ���觴�ǹ") + " " + ("�������"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.UNKONW, "��úѹ�֡��¡�õ�Ǩ�ѡ��");
        }
        return ret;
    }

    public DxTemplate readDxTemplateByItemID(OrderItem OrderItem, Visit Visit) {
        if (OrderItem == null) {
            return null;
        }
        DxTemplate dxt = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            dxt = theHosDB.theDxTemplateDB.selectByItem(OrderItem.item_code, Visit.getObjectId());
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return dxt;
    }

    protected String refreshXrayStatus(String visit_id) throws Exception {
        Vector<OrderItem> orderItems = theHosDB.theOrderItemDB.selectByVisitId(visit_id);
        String status = OrderControl.checkLabAndXrayStatus(orderItems).get("xray");
        if (theHO.theListTransfer != null) {
            theHO.theListTransfer.xraystatus = status;
            theHosDB.theQueueTransferDB.update(theHO.theListTransfer);
        }
        theHO.theVisit.queue_xray_status = status;
        theHosDB.theVisitDB.updateLabAndXrayStatus(theHO.theVisit);
        if (status.equals(QueueXrayStatus.REPORT)) {
            theHosDB.theQueueXrayDB.deleteByVisitID(theHO.theVisit.getObjectId());
        }
        return status;
    }

    public boolean checkLabDuration(OrderItem item, Visit visit) {
        if (item == null || visit == null) {
            return false;
        }
        theConnectionInf.open();
        boolean confirm = false;
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Object[] object = theHosDB.theOrderItemDB.selectLabDuration(item.item_code, visit.getObjectId());
            if (object != null) {
                String itemName = String.valueOf(object[7]);
                String durationDay = String.valueOf(object[3]);
                String orderLast = DateUtil.convertDateToString((Date) object[1], "dd/MM/yyyy", DateUtil.LOCALE_TH);
                confirm = theUS.confirmBox(("<html>��¡�õ�Ǩ�ѡ�� " + itemName)
                        + " " + ("��ӡѹ�Ѻ��¡���������������������� <b>" + durationDay + "</b> �ѹ <br>")
                        + " " + ("<b>(�ѹ����������ش " + orderLast + ")</b> �׹�ѹ�����觫��</html>"), UpdateStatus.WARNING);
            } else {
                confirm = true;
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return confirm;
    }

    public void doCheckOrderParentPackage(Vector<OrderItem> orderitems) throws Exception {
        Vector<OrderItem> vOrderItemClone = (Vector<OrderItem>) orderitems.clone();
        for (OrderItem orderitem : vOrderItemClone) {
            if (orderitem.isParentPackage()) {
                List<OrderItem> list = theHosDB.theOrderItemDB.selectByOrderIdJoinOrderPackage(orderitem.getObjectId());
                orderitems.addAll(list);
            }
            doCancelOrderPackage(orderitem);
        }
    }

    public Vector<OrderItem> addOrderChildByParent(Vector<OrderItem> orderitems) {
        Vector<OrderItem> list = new Vector<>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (OrderItem orderitem : orderitems) {
                if (!orderitem.isParentPackage()) {
                    if (!orderitem.isChildPackage()) {
                        list.add(orderitem);
                    }
                    continue;
                }
                list.add(orderitem);
                Vector<OrderItem> child = theHosDB.theOrderItemDB.selectByOrderIdJoinOrderPackage(orderitem.getObjectId());
                list.addAll(child);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

}

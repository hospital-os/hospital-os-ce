package com.hospital_os.utility;

import com.hospital_os.usecase.connection.ConnectionInf;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConnectionDBMgr implements ConnectionInf {

    DBConnection theDBConnection;
    String url;
    String user;
    String pass;
    String dri;
    boolean multiConnection = true;
    static boolean isClose = false;
    public int typeDatabase = 0;

    /**
     * @roseuid 3F73FB550305
     */
    /**
     * �������ź�͡���
     */
    public ConnectionDBMgr() {
        this.theDBConnection = new DBConnection();
        url = "jdbc:postgresql://192.168.1.1:5432/test_tong";
        user = "postgres";
        pass = "grespost";
        /*dri = "org.postgresql.Driver";*/

 /*
         url = "jdbc:microsoft:sqlserver://tong:1433;User=sa;Password=tong123;DatabaseName=tong_test";
         user = "sa";
         pass = "tong123";
         dri = "com.microsoft.jdbc.sqlserver.SQLServerDriver";
         */
        this.theDBConnection.create(url, user, pass, dri);
        this.theDBConnection.open("");
    }

    public ConnectionDBMgr(String url, String uname, String passw, int typeDatabase) {
        this.url = url;
        this.user = uname;
        this.pass = passw;
        /*String di = "org.postgresql.Driver";*/
        this.typeDatabase = typeDatabase;
        String di = "com.microsoft.jdbc.sqlserver.SQLServerDriver";
        this.theDBConnection = new DBConnection();
        this.theDBConnection.create(url, uname, passw, di);
        /*       this.theDBConnection.create("jdbc:mysql://localhost/hospital_osv","tong","");*/
        if (di.equals("")) {
            this.theDBConnection.open("org.postgresql.Driver");
        } else {
            this.theDBConnection.open(di);
        }
    }

    public ConnectionDBMgr(String di, String url, String uname, String passw, int typeDatabase) {
        this.url = url;
        this.user = uname;
        this.pass = passw;
        this.typeDatabase = typeDatabase;
        this.theDBConnection = new DBConnection();
        this.theDBConnection.create(url, uname, passw, di);
        this.theDBConnection.open(di);
    }

    @Override
    public boolean nbegin() {
//       if(this.theDBConnection.execSQLUpdate("BEGIN")!= 0)
//            return true;
//        else
        return false;
    }

    @Override
    public boolean open() {
        /**
         * ��� isClose �� false ���� ������¤������ ���ӧҹ������� ����ͧ
         * ����ҡ �� true ���� �ʴ���ҵ�ͧ�������Դ connection ��ҧ���
         * ���Ǥ���
         */
        //henbe comment 120806
        //if(!this.isClose)
        //  if(multiConnection)
        return this.theDBConnection.open("");
        //return false;
    }

    @Override
    public boolean begin() {
        /* this.theDBConnection.open(url);*/

 /*   this.theDBConnection.open("");   */
 /*   if(this.theDBConnection.execSQLUpdate("BEGIN")!= 0)*/
        return true;
        /*   else*/
 /*           return false;*/

    }

    @Override
    public boolean rollback() {
        /*   if(this.theDBConnection.execSQLUpdate("ROLLBACK") != 0)*/
        return true;
        /*   else*/
 /*      return false;*/

    }

    @Override
    public boolean commit() {
        /*HENBE_APR*/
 /*  if(this.theDBConnection.execSQLUpdate("COMMIT") != 0)*/
        return true;
        /*   else*/
 /*        return false;*/

    }

    @Override
    public int eUpdate(String sql) throws Exception {
        LOG.info(sql);
        return this.theDBConnection.execSQLUpdate(sql);
    }

    @Override
    public java.sql.ResultSet eQuery(String sql) throws Exception {
        LOG.info(sql);
        return this.theDBConnection.execSQLQuery(sql);

    }

    @Override
    public List<Object[]> eComplexQuery(String sql) throws Exception {
        LOG.info(sql);
        return this.theDBConnection.execComplexSQLQuery(sql);

    }

    @Override
    public java.sql.PreparedStatement ePQuery(String sql) {
        return this.theDBConnection.execPSQLQuery(sql);

    }

    @Override
    public boolean close() {
        if (multiConnection) {
            return this.theDBConnection.close();
        } else {
            return true;
        }

    }

    @Override
    public boolean connect(String driver, String url) {
        try {
            this.theDBConnection.create("jdbc:postgresql://192.168.1.1:5432/test_tong", "postgres", "grespost", "org.postgresql.Driver");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
            return false;
        }

        return true;
    }

    /* for postgres*/

    public static boolean checkConnection(String url, String username, String password, int typedatabase) {
        boolean res = false;
        try {
            /* LOG.info("");*/
            switch (typedatabase) {
                case 0:
                    Class.forName("org.postgresql.Driver");/*DriverManager.registerDriver(new org.postgresql.Driver());   */
                    break;
                case 1:
                    Class.forName("com.microsoft.jdbc.sqlserver.SQLServerDriver");/*DriverManager.registerDriver(new com.microsoft.jdbc.sqlserver.SQLServerDriver()); */
                    break;
                case 2:
                    Class.forName("org.gjt.mm.mysql.Driver");/*DriverManager.registerDriver(new org.gjt.mm.mysql.Driver()); */
                    break;
            }


            /* connect to database*/

 /*    LOG.info(url + " " + username + " " + password );*/
            Connection connection = DriverManager.getConnection(url, username, password);
            if (connection != null) {
                /*LOG.info("true"); */
                res = true;
            } else {
                /*  LOG.info("false");*/
                javax.swing.JOptionPane.showMessageDialog(new javax.swing.JFrame(), "�������ö�Դ��Ͱҹ�������� �Ҩ������ Server �ջѭ�� ���� ����� Driver �ͧ Database", "Error(����ͼԴ��Ҵ)", javax.swing.JOptionPane.OK_OPTION);

            }
        } catch (Exception ex) {
            javax.swing.JOptionPane.showMessageDialog(new javax.swing.JFrame(), "�������ö�Դ��Ͱҹ�������� �Ҩ������ Server �ջѭ�� ���� ����� Driver �ͧ Database", "Error(����ͼԴ��Ҵ)", javax.swing.JOptionPane.OK_OPTION);

            /*/ LOG.log(Level.SEVERE, ex.getMessage(), ex);*/
        }
        return res;
    }

    /*for SQL Server*/

    public static boolean checkConnectionSQLServer(String url, String username, String password) {
        boolean res = false;
        try {
            // DriverManager.registerDriver(new com.microsoft.jdbc.sqlserver.SQLServerDriver());      /* connect to database*/
            Connection connection = DriverManager.getConnection(url, username, password);
            if (connection != null) {
                res = true;
            }
        } catch (Exception ex) {
            /*LOG.log(Level.SEVERE, ex.getMessage(), ex);*/
        }
        return res;
    }

    @Override
    public int gettypeDatabase() {
        return typeDatabase;
    }

    @Override
    public java.sql.Connection getConnection() {
        return theDBConnection.getConnection();
    }

    /*HENBE_APR*/

    @Override
    public boolean exit() {
        return this.theDBConnection.close();
    }

    @Override
    public void MultiConnection(boolean choose) {
        this.multiConnection = choose;
    }

    /**
     * ������͵�ͧ������Դ Connection true is close --- false is not close
     */
    @Override
    public boolean close(boolean isclose) {

        if (isclose) {
            isClose = false;
            return this.close();
        } else {
            return false;
        }

    }

    /**
     * ������͵�ͧ�������Դ Connection true is open connection ---- false is
     * open-close connection
     */
    @Override
    public boolean open(boolean isclose) {
        isClose = isclose;
        return this.open();

    }

    @Override
    public java.util.Properties getProperties() {
        java.util.Properties data = new java.util.Properties();
        data.setProperty("USER", user);
        data.setProperty("PASSWORD", pass);
        data.setProperty("URL", url);
        if (this.multiConnection) {
            data.setProperty("MULTICONNECTION", "1");
        } else {
            data.setProperty("MULTICONNECTION", "0");
        }
        return data;
    }

    @Override
    public ConnectionInf getClone() {
        return new ConnectionDBMgr(this.url, this.user, this.pass, this.typeDatabase);
    }

    @Override
    public String getURL() {
        return url;
    }

    @Override
    public String getUsername() {
        return user;
    }
    private static final Logger LOG = Logger.getLogger(ConnectionDBMgr.class.getName());

    @Override
    public Map<String, Object> queryFromFile(File file, Map<String, String> params) throws Exception {
        return null;
    }

    @Override
    public List<Map<String, Object>> eComplexQueryWithColumn(String sql) throws Exception {
        LOG.info(sql);
        return this.theDBConnection.execComplexSQLQueryWithColumn(sql);
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.gui.dialog;

import com.hospital_os.object.Authentication;
import com.hospital_os.object.Employee;
import com.hospital_os.utility.ComboboxModel;
import com.hosv3.control.HosControl;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class DialogEmployeeList extends javax.swing.JDialog {

    private static final long serialVersionUID = 1L;
    private final UserTableModel model = new UserTableModel();
    private HosControl theHC;
    private boolean isOK;

    /**
     * Creates new form DialogEmployeeList
     *
     * @param parent
     * @param modal
     */
    public DialogEmployeeList(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        comboRole = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();
        searchText = new org.jdesktop.swingx.JXSearchField();
        btnOK = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(new java.awt.GridBagLayout());

        comboRole.setFont(comboRole.getFont());
        comboRole.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboRoleActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        getContentPane().add(comboRole, gridBagConstraints);

        table.setFont(table.getFont());
        table.setModel(model);
        table.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tableMouseReleased(evt);
            }
        });
        table.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tableKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(table);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        getContentPane().add(jScrollPane1, gridBagConstraints);

        searchText.setFont(searchText.getFont());
        searchText.setLayoutStyle(org.jdesktop.swingx.JXSearchField.LayoutStyle.MAC);
        searchText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchTextActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        getContentPane().add(searchText, gridBagConstraints);

        btnOK.setFont(btnOK.getFont());
        btnOK.setText("��ŧ");
        btnOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOKActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        getContentPane().add(btnOK, gridBagConstraints);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void searchTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchTextActionPerformed
        this.doSearch();
    }//GEN-LAST:event_searchTextActionPerformed

    private void comboRoleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboRoleActionPerformed
        this.doSearch();
    }//GEN-LAST:event_comboRoleActionPerformed

    private void tableMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableMouseReleased
        this.btnOK.setEnabled(table.getRowCount() > 0 && table.getSelectedRowCount() > 0);
    }//GEN-LAST:event_tableMouseReleased

    private void tableKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tableKeyReleased
        this.btnOK.setEnabled(table.getRowCount() > 0 && table.getSelectedRowCount() > 0);
    }//GEN-LAST:event_tableKeyReleased

    private void btnOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOKActionPerformed
        isOK = true;
        this.dispose();
    }//GEN-LAST:event_btnOKActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnOK;
    private javax.swing.JComboBox comboRole;
    private javax.swing.JScrollPane jScrollPane1;
    private org.jdesktop.swingx.JXSearchField searchText;
    private javax.swing.JTable table;
    // End of variables declaration//GEN-END:variables

    public List<Employee> openDialog() {
        this.isOK = false;
        searchText.setText("");
        // default one stop service
        if (comboRole.getItemCount() > 0) {
            for (int i = 0; i < comboRole.getItemCount(); i++) {
                Authentication authentication = (Authentication) comboRole.getItemAt(i);
                if (authentication.getCode().equals(Authentication.ONE)) {
                    comboRole.setSelectedIndex(i);
                    break;
                }
            }
        }
        this.doSearch();
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        return getSelectedList();
    }

    @SuppressWarnings("UseOfObsoleteCollectionType")
    public void setControl(HosControl hc) {
        this.theHC = hc;
        Vector listAuthentication = new Vector();
        Authentication authentication = new Authentication();
        authentication.setObjectId("");
        authentication.description = "�ء�Է�������ҹ";
        listAuthentication.add(0, authentication);
        Vector list = theHC.theLookupControl.listAuthentication();
        if (list != null && !list.isEmpty()) {
            listAuthentication.addAll(list);
        }
        ComboboxModel.initComboBox(comboRole, listAuthentication);
    }

    private void doSearch() {
        if (theHC != null) {
            model.clearTable();
            String authen = ((Authentication) comboRole.getSelectedItem()).getCode();
            @SuppressWarnings("UseOfObsoleteCollectionType")
            Vector<Employee> employees = theHC.theSetupControl.listEmployeeSetup(searchText.getText().trim(), "1", authen.isEmpty() ? null : authen, false);
            for (Employee employee : employees) {
                model.addData(employee);
            }
            model.fireTableDataChanged();
        }
    }

    private List<Employee> getSelectedList() {
        List<Employee> list = new ArrayList<Employee>();
        if (isOK && table.getRowCount() > 0 && table.getSelectedRowCount() > 0) {
            for (int index : table.getSelectedRows()) {
                Employee em = model.getRow(index);
                list.add(model.getRow(index));
            }
        }
        return list;
    }

    private class UserTableModel extends AbstractTableModel {

        private static final long serialVersionUID = 1L;
        private String[] columns = {
            "�����ҹ",
            "�Է�������ҹ"};
        private List<Employee> data = new ArrayList<Employee>();

        public Employee getRow(int row) {
            return data.get(row);
        }

        @Override
        public boolean isCellEditable(int row, int col) {
////            if (col == 0) {
////                return true;
////            } else {
            return false;
//            }
        }

        @Override
        public int getRowCount() {
            return data.size();
        }

        @Override
        public int getColumnCount() {
            return columns.length;
        }

        @Override
        public String getColumnName(int col) {
            return columns[col];
        }

        @Override
        public Object getValueAt(int row, int col) {
            Employee obj = data.get(row);
            switch (col) {
                case 0:
                    return obj.person.person_firstname + " " + obj.person.person_lastname;
                case 1:
                    return theHC.theLookupControl.readAuthenticationById(obj.authentication_id, false);
                default:
                    return obj;
            }
        }

        @SuppressWarnings("UseOfObsoleteCollectionType")
        public List<Employee> getData() {
            return data;
        }

        public void setDatas(List<Employee> datas) {
            data.clear();
            data.addAll(datas);
        }

        public void addData(Employee objects) {
            data.add(objects);
        }

        @Override
        public void setValueAt(Object value, int row, int col) {
//            Employee user = data.get(row);
//            switch(col){
//                case 0:
//                    user.setUserName(String.valueOf(value));break;
//                case 1:
//                    user.getRoleId().getRoleName();
//                break;case 2:
//                    user.getFirstName() + user.getLastName();
//                break;default:
//                    user = (Employee) value;
//                    break;
//            }            
            fireTableCellUpdated(row, col);

        }

        public void clearTable() {
            getData().clear();
            fireTableDataChanged();
        }
    }
}

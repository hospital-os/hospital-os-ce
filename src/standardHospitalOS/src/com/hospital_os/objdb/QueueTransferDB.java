/*
 * QueueTransferDB.java
 *
 * Created on 1 ����Ҥ� 2548, 13:54 �.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.*;
import com.hospital_os.usecase.connection.*;
import com.hospital_os.utility.*;
import java.sql.*;
import java.util.*;

/**
 *
 * @author tong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class QueueTransferDB {

    /**
     * Creates a new instance of QueueTransferDB
     */
    public ConnectionInf theConnectionInf;
    public ListTransfer dbObj;
    final public String idtable = "272";/*"195";*/

    private String SQL = "";

    public QueueTransferDB(ConnectionInf db) {

        theConnectionInf = db;
        dbObj = new ListTransfer();
        initConfig();
    }

    private boolean initConfig() {
        dbObj.table = "t_visit_queue_transfer";
        dbObj.pk_field = "t_visit_queue_transfer_id";

        dbObj.assign_time = "assign_date_time";
        dbObj.description = "visit_queue_setup_description";
        dbObj.fname = "patient_firstname";
        dbObj.hn = "visit_hn";
        dbObj.lname = "patient_lastname";
        dbObj.locking = "visit_locking";
        dbObj.name = "service_point_description";
        dbObj.color = "visit_queue_setup_queue_color";
        dbObj.patient_id = "t_patient_id";
        dbObj.doctor = "visit_service_staff_doctor";
        dbObj.visit_id = "t_visit_id";
        dbObj.vn = "visit_vn";
        dbObj.queue = "visit_queue_map_queue";
        dbObj.visit_type = "f_visit_type_id";
        dbObj.servicepoint_id = "b_service_point_id";
        dbObj.patient_allergy = "patient_drugallergy";
        dbObj.sex = "f_sex_id";
        dbObj.prefix = "f_patient_prefix_id";
        dbObj.labstatus = "visit_queue_transfer_lab_status";
        dbObj.xraystatus = "visit_queue_transfer_xray_status";

        return true;
    }

    public int insert(ListTransfer o) throws Exception {
        String sql = "";
        ListTransfer p = o;
        p.generateOID(idtable);
        sql = "insert into " + dbObj.table + " ("
                + dbObj.pk_field + " ,"
                + dbObj.assign_time + " ,"
                + dbObj.description + " ,"
                + dbObj.fname + " ,"
                + dbObj.hn + " ,"
                + dbObj.lname + " ,"
                + dbObj.locking + " ,"
                + dbObj.name + " ,"
                + dbObj.color + " ,"
                + dbObj.patient_id + " ,"
                + dbObj.doctor + " ,"
                + dbObj.visit_id + " ,"
                + dbObj.vn + " ,"
                + dbObj.queue + " ,"
                + dbObj.visit_type + " ,"
                + dbObj.servicepoint_id + " ,"
                + dbObj.sex + " ,"
                + dbObj.prefix + " ,"
                + dbObj.patient_allergy + ", "
                + dbObj.labstatus + ", "
                + dbObj.xraystatus
                + " ) values ('"
                + p.getObjectId() + "','"
                + p.assign_time + "','"
                + p.description + "','"
                + p.fname + "','"
                + p.hn + "','"
                + p.lname + "','"
                + p.locking + "','"
                + p.name + "','"
                + p.color + "','"
                + p.patient_id + "','"
                + p.doctor + "','"
                + p.visit_id + "','"
                + p.vn + "','"
                + p.queue + "','"
                + p.visit_type + "','"
                + p.servicepoint_id + "','"
                + p.sex + "','"
                + p.prefix + "','"
                + p.patient_allergy + "','"
                + p.labstatus + "','"
                + p.xraystatus
                + "')";

        sql = Gutil.convertSQLToMySQL(sql, theConnectionInf.gettypeDatabase());
        return theConnectionInf.eUpdate(sql);

    }

    public int delete(ListTransfer o) throws Exception {
        String sql = "delete from " + dbObj.table
                + " where " + dbObj.pk_field + "='" + o.getObjectId() + "'";
        return theConnectionInf.eUpdate(sql);
    }

    public int deleteByVisitID(String visit_id) throws Exception {
        String sql = "delete from " + dbObj.table
                + " where " + dbObj.visit_id + "='" + visit_id + "'";
        return theConnectionInf.eUpdate(sql);
    }

    public int updateMapQueueTransferByVisitID(ListTransfer o) throws Exception {
        String sql = "UPDATE " + dbObj.table + ""
                + " set " + dbObj.color + " = '" + o.color + "'"
                + " , " + dbObj.description + " = '" + o.description + "'"
                + " , " + dbObj.queue + " = '" + o.queue + "'"
                + " where " + dbObj.visit_id + "='" + o.visit_id + "'";

        return theConnectionInf.eUpdate(sql);
    }

    public int updateTransferPatientAllergy(String patient_id) throws Exception {
        String sql = "UPDATE " + dbObj.table + ""
                + " set " + dbObj.patient_allergy + " = '1'"
                + " where " + dbObj.patient_id + "='" + patient_id + "'";
        return theConnectionInf.eUpdate(sql);
    }

    /**
     * Update lock
     */
    public int updateLock(ListTransfer listTransfer) throws Exception {
        String sql = "UPDATE " + dbObj.table + ""
                + " set " + dbObj.locking + " = '" + listTransfer.locking + "'"
                + " where " + dbObj.pk_field + "='" + listTransfer.getObjectId() + "'";
        return theConnectionInf.eUpdate(sql);
    }

    /**
     * Update lock
     */
    public int updateLockByVisitID(String visit_id, String lock) throws Exception {
        String sql = "UPDATE " + dbObj.table + ""
                + " set " + dbObj.locking + " = '" + lock + "'"
                + " where " + dbObj.visit_id + "='" + visit_id + "'";

        return theConnectionInf.eUpdate(sql);
    }

    /**
     * Update transfer field Update : visit_locking : service_point_description
     * : assign_date_time : b_service_point_id : visit_service_staff_doctor
     */
    public int updateServiceTransfer(ListTransfer listTransfer) throws Exception {
        String sql = "UPDATE " + dbObj.table + ""
                + " set " + dbObj.locking + " = '" + listTransfer.locking + "'"
                + " , " + dbObj.assign_time + " = '" + listTransfer.assign_time + "'"
                + " , " + dbObj.servicepoint_id + " = '" + listTransfer.servicepoint_id + "'"
                + " , " + dbObj.doctor + " = '" + listTransfer.doctor + "'"
                + " , " + dbObj.visit_id + " = '" + listTransfer.visit_id + "'"
                + " , " + dbObj.name + " = '" + listTransfer.name + "'"
                + " where " + dbObj.pk_field + "='" + listTransfer.getObjectId() + "'";
        return theConnectionInf.eUpdate(sql);
    }

    /**
     * Update Drug Allergy when save DrugAllergy
     */
    public int updateDrugAllergy(ListTransfer listTransfer) throws Exception {
        String sql = "UPDATE " + dbObj.table + ""
                + " set " + dbObj.patient_allergy + " = '" + listTransfer.patient_allergy + "'"
                + " where " + dbObj.pk_field + "='" + listTransfer.getObjectId() + "'";
        return theConnectionInf.eUpdate(sql);
    }

    /**
     * Update sex when save Patient
     */
    public int updatesexByVisitID(String visit_id, String sex, String prefix) throws Exception {
        String sql = "UPDATE " + dbObj.table + ""
                + " set " + dbObj.sex + " = '" + sex + "'"
                + " , " + dbObj.prefix + " = '" + prefix + "'"
                + " where " + dbObj.visit_id + "='" + visit_id + "'";
        return theConnectionInf.eUpdate(sql);
    }

    /**
     * ��㹡�� list �����·������㹨ش��ԡ�� �¨��觢��������
     * service_point_id = key ��ѡ�ͧ���ҧ �¨��� key ��ѡ�ͧ���ҧ
     * service_point employee_id_doctor = key ��ѡ�ͧ���ҧ �¨��� key
     * ��ѡ�ͧ���ҧ Employee ੾�Шش��ԡ�÷������ͧ��Ǩ choose =
     * ���͡����繼������ ���� �����¹͡
     */
    public Vector listTransferVisitQueueByServicePoint(String servicePointId, String doctorId, String visitType) throws Exception {
        String sql = "select t_visit_queue_transfer.* \n"
                + "         ,t_visit.xray_urgent_status \n"
                + "         ,t_visit.lab_urgent_status \n"
                + "         ,t_visit.drug_stat_status \n"
                + "         ,t_visit.f_emergency_status_id \n"
                + "         ,b_visit_range_age.description as range_age\n"
                + "         ,t_visit.visit_vital_sign_score"
                + "         ,t_visit.visit_vital_sign_notify_datetime"
                + "     from t_visit_queue_transfer \n"
                + "     inner join t_visit on t_visit.t_visit_id  =t_visit_queue_transfer.t_visit_id \n"
                + "     left join b_visit_range_age on t_visit.b_visit_range_age_id = b_visit_range_age.b_visit_range_age_id \n"
                + "          and b_visit_range_age.active = '1' \n"
                + "     where true \n";
        if (servicePointId != null && !servicePointId.trim().isEmpty()) {
            sql = sql + "and t_visit_queue_transfer.b_service_point_id = ?\n";
        }
        if (doctorId != null && !doctorId.trim().isEmpty()) {
            sql = sql + "and t_visit_queue_transfer.visit_service_staff_doctor = ?\n";
        }
        if (visitType != null && !visitType.isEmpty() && (visitType.equals(VisitType.IPD) || visitType.equals(VisitType.OPD))) {
            sql = sql + "and t_visit_queue_transfer.f_visit_type_id = ?\n";
        }
        sql = sql + " order by t_visit_queue_transfer.arrived_datetime\n"
                + "    ,t_visit_queue_transfer.assign_date_time";

        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;

            if (servicePointId != null && !servicePointId.trim().isEmpty()) {
                ePQuery.setString(index++, servicePointId);
            }
            if (doctorId != null && !doctorId.trim().isEmpty()) {
                ePQuery.setString(index++, doctorId == null
                        || doctorId.isEmpty()
                        || doctorId.equals("")
                        || doctorId.equals("null") ? null : doctorId);
            }
            if (visitType != null && !visitType.isEmpty() && (visitType.equals(VisitType.IPD) || visitType.equals(VisitType.OPD))) {
                ePQuery.setString(index++, visitType);
            }
            return veQuery(ePQuery.toString());
        }
    }

    public Vector veQuery(String sql) throws Exception {
        ListTransfer p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            p = new ListTransfer();
            p.setObjectId(rs.getString(dbObj.pk_field));

            p.assign_time = rs.getString(dbObj.assign_time);
            p.description = rs.getString(dbObj.description);
            p.fname = rs.getString(dbObj.fname);
            p.hn = rs.getString(dbObj.hn);
            p.lname = rs.getString(dbObj.lname);
            p.locking = rs.getString(dbObj.locking);
            p.name = rs.getString(dbObj.name);
            p.patient_id = rs.getString(dbObj.patient_id);
            p.doctor = rs.getString(dbObj.doctor);
            p.color = rs.getString(dbObj.color);
            p.visit_id = rs.getString(dbObj.visit_id);
            p.vn = rs.getString(dbObj.vn);
            p.visit_type = rs.getString(dbObj.visit_type);
            p.queue = rs.getString(dbObj.queue);
            p.servicepoint_id = rs.getString(dbObj.servicepoint_id);
            p.patient_allergy = rs.getString(dbObj.patient_allergy);
            p.sex = rs.getString(dbObj.sex);
            p.prefix = rs.getString(dbObj.prefix);
            p.labstatus = rs.getString("visit_queue_transfer_lab_status");
            try {
                p.lab_urgent_status = rs.getString("lab_urgent_status");
                p.xray_urgent_status = rs.getString("xray_urgent_status");
                p.drug_stat_status = rs.getString("drug_stat_status");
            } catch (SQLException e) {
            }
            try {
                p.emergency_status = rs.getString("f_emergency_status_id");
            } catch (SQLException e) {
            }
            p.xraystatus = rs.getString("visit_queue_transfer_xray_status");
            p.arrived_datetime = rs.getTimestamp("arrived_datetime");
            try {
                p.range_age = rs.getString("range_age");
            } catch (SQLException e) {
            }
            p.arrived_status = String.valueOf(rs.getInt("arrived_status"));
            try {
                p.visit_vital_sign_score = rs.getString("visit_vital_sign_score");
                p.visit_vital_sign_notify_datetime = rs.getTimestamp("visit_vital_sign_notify_datetime");
                p.visit_vital_sign_newspews_type = rs.getString("visit_vital_sign_newspews_type");
            } catch (SQLException e) {
            }
            list.add(p);
        }

        rs.close();
        return list;
    }

    public ListTransfer selectByVisitID(String visitId) throws Exception {
        SQL = "";
        SQL = "select * from " + dbObj.table
                + " where " + dbObj.visit_id + "='" + visitId + "'";

        Vector v = veQuery(SQL);

        if (v.isEmpty()) {
            return null;
        } else {
            return (ListTransfer) v.get(0);
        }
    }

    public ListTransfer selectByPatientID(String patientId) throws Exception {
        SQL = "";
        SQL = "select * from " + dbObj.table
                + " where " + dbObj.patient_id + "='" + patientId + "'";

        Vector v = veQuery(SQL);

        if (v.isEmpty()) {
            return null;
        } else {
            return (ListTransfer) v.get(0);
        }
    }

}

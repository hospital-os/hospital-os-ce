/*
 * ListVisitByTransfer.java
 *
 * Created on 17 ���Ҥ� 2546, 18:21 �.
 */
package com.hosv3.gui.panel.transaction;

import com.hospital_os.object.Authentication;
import com.hospital_os.object.ListTransfer;
import com.hospital_os.object.ServicePoint;
import com.hospital_os.object.ServiceType;
import com.hospital_os.object.Visit;
import com.hospital_os.object.VisitType;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hospital_os.utility.Audio;
import com.hospital_os.utility.CelRendererTranfer;
import com.hospital_os.utility.CellRendererHos;
import com.hospital_os.utility.ComboboxModel;
import com.hospital_os.utility.Gutil;
import com.hospital_os.utility.TaBleModel;
import com.hosv3.control.HosControl;
import com.hosv3.gui.component.CellRendererColor;
import com.hosv3.gui.dialog.HosDialog;
import com.hosv3.object.HosObject;
import com.hosv3.object.QueueDispense2;
import com.hosv3.subject.HosSubject;
import com.hosv3.usecase.transaction.ManageLabXrayResp;
import com.hosv3.usecase.transaction.ManageOrderResp;
import com.hosv3.usecase.transaction.ManagePatientResp;
import com.hosv3.usecase.transaction.ManageQueueResp;
import com.hosv3.usecase.transaction.ManageVisitResp;
import com.hosv3.utility.Constant;
import com.hosv3.utility.DateUtil;
import com.hosv3.utility.GuiLang;
import com.hosv3.utility.TableRenderer;
import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.util.Vector;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.Timer;

/**
 *
 * @author Administrator
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class PanelListVisitByTransfer extends javax.swing.JPanel
        implements ManageVisitResp, ManagePatientResp,
        ManageOrderResp, ManageLabXrayResp, ManageQueueResp,
        ComponentListener {

    private static final long serialVersionUID = 1L;
    HosObject theHO;
    HosControl theHC;
    HosSubject theHS;
    UpdateStatus theUS;
    HosDialog theHD;
    /**
     * ��㹡�äǺ�����÷ӧҹ�����ѹ�ͧ GUI
     */
    Visit theVisit;
    Vector theTransfer;
    Vector theQueueDispense2V;
    /**
     * �繡���ʴ� �բͧ ��ö١��͡
     */
    private final CelRendererTranfer cellRendererTranfer = new CelRendererTranfer();
    private CellRendererHos vnRender = new CellRendererHos(CellRendererHos.VN);
    private CellRendererHos hnRender = new CellRendererHos(CellRendererHos.HN);
    private final CellRendererHos dateRender = new CellRendererHos(CellRendererHos.DATE_TIME);
    private final CellRendererHos allergyRender = new CellRendererHos(CellRendererHos.ALLERGY);
    private final CellRendererHos labRender = new CellRendererHos(CellRendererHos.LAB_STATUS);
    private final CellRendererHos urgenyRender = new CellRendererHos(CellRendererHos.URGENY_STATUS);
    private final CellRendererHos esiRender = new CellRendererHos(CellRendererHos.ESI_STATUS);
    private final CellRendererHos rangeAgeRender = new CellRendererHos(CellRendererHos.RANGE_AGE);
    private final CellRendererHos xrayRender = new CellRendererHos(CellRendererHos.XRAY_STATUS);
    private final CellRendererHos queueRender = new CellRendererHos(CellRendererHos.HIGHLIGHT_QUEUE);
    private final CellRendererHos scoreRender = new CellRendererHos(CellRendererHos.NEWS_PEWS_SCORE);
    private final CellRendererColor theCellRendererColor = new CellRendererColor(true);
    Vector vListTransfer = new Vector();  // ᷹ theTransfer
    Vector vListRemain = new Vector();
    int ref = 0;
    boolean used_queue = false;
    String[] col_sp_queue = {"�ӴѺ", "��͡", "����", "ESI", "��觴�ǹ", "������", "HN", "VN", "����-ʡ��", "����", "�Ҷ֧����", "�ش��ԡ��", "NEWS/PEWS Score", "�Ţ���", "���", "�Ż", "�͡�����"};
    String[] col_sp = {"�ӴѺ", "��͡", "����", "ESI", "��觴�ǹ", "������", "HN", "VN", "����-ʡ��", "����", "�Ҷ֧����", "�ش��ԡ��", "NEWS/PEWS Score", "�Ż", "�͡�����"};
    String[] col_lab = {"�ӴѺ", "��͡", "����", "ESI", "��觴�ǹ", "������", "HN", "VN", "����-ʡ��", "����", "�Ҷ֧����", "�Ţ���", "���", "Lab", "��������觵�Ǩ", "X-ray"};
    String[] col_dispense = {"�ӴѺ", "������", "HN", "VN", "����-ʡ��", "�ش��ԡ���ش����", "�ըش��ԡ���ش����", "����������ͨ�����", "�Ҷ֧����", "�ӹǹ", "���"};
    String[] col_sp_dx = {"�ӴѺ", "��͡", "����", "HN", "VN", "����-ʡ��", "����", "Dx", "Lab", "���"};
    //��㹡���ʴ��ӴѺ�Ţ���㹵��ҧ Transaction
    int countrowTransaction = 0;
    //��㹡�õ�Ǩ�ͺ ����ʴ��Ţͧ combobox �ͧ ᾷ��
    String authen;
    Timer theTimer;
    //�ӹǹ������㹨ش��ԡ��
    int countpatient = 0;
    private Vector vQueueStat;

    /**
     * Creates new form ListVisitByTransfer
     */
    public PanelListVisitByTransfer() {
        initComponents();
        setLanguage(null);
        jTableQueueOpd.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jTableQueueDispense.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    }

    public void setControl(HosControl hc, UpdateStatus us) {
        theHC = hc;
        theHO = hc.theHO;
        theHS = hc.theHS;
        theUS = us;
        vnRender = new CellRendererHos(CellRendererHos.VN, theHC.theLookupControl.getSequenceDataVN().pattern);
        hnRender = new CellRendererHos(CellRendererHos.HN, theHC.theLookupControl.getSequenceDataHN().pattern);
        theHS.theVisitSubject.attachManageVisit(this);
        theHS.thePatientSubject.attachManagePatient(this);
        theHS.theOrderSubject.attachManageOrder(this);
        theHS.theResultSubject.attachManageLab(this);
        theHS.theResultSubject.attachManageXray(this);
        theHS.theQueueSubject.attachManageQueue(this);
        addComponentListener(this);
        theHC.theHP.aPanelListVisit.addComponentListener(this);
        setUserAuth();
//        setLanguage(null);

        theTimer = new Timer(Constant.TIME_REFRESH, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                if (!theHO.theEmployee.authentication_id.equals(Authentication.STAT)) {
                    refreshButton();
                }
            }
        });
        theTimer.start();
        setQueueStatV(new Vector());
    }

    @Override
    public void setEnabled(boolean enable) {
        jTableQueueDispense.setEnabled(enable);
        jTableQueueOpd.setEnabled(enable);
    }

    public void setDialog(HosDialog hd) {
        theHD = hd;
    }

    private void setLanguage(String str) {
        GuiLang.setLanguage(jButtonDoctorDischarge);
        GuiLang.setLanguage(jCheckBoxSearchByDate);
        GuiLang.setLanguage(jLabelTo);
        GuiLang.setLanguage(jToggleButtonStat);
        GuiLang.setLanguage(buttonGroupPatient);
        GuiLang.setLanguage(jComboBoxDoctor);
        GuiLang.setLanguage(jComboBoxSpoint);
        GuiLang.setLanguage(jLabelDoctor);
        GuiLang.setLanguage(jLabelPname);
        GuiLang.setLanguage(jPanelTop);
        GuiLang.setLanguage(jPanelTop1);
        GuiLang.setLanguage(jRadioButtonAll);
        GuiLang.setLanguage(jRadioButtonIPD);
        GuiLang.setLanguage(jRadioButtonOPD);
        GuiLang.setLanguage(jScrollPane);
        GuiLang.setLanguage(jScrollPane1);
        GuiLang.setLanguage(jTableQueueOpd);
        GuiLang.setLanguage(jTableQueueDispense);
        GuiLang.setLanguage(jButton1);
        GuiLang.setLanguage(jToggleButtonSound);
    }

    protected void initComboBox() {
        setUserAuth();
    }

    protected void setUserAuth() {
        authen = theHO.theEmployee.authentication_id;
        used_queue = theHC.theLookupControl.readOption().inqueuevisit.equals("1");
        this.jPanelRadioPttype.setVisible(theHO.theGActionAuthV.isReadMenuIpd());
        jPanelQDispense.setVisible(false);
        ComboboxModel.initComboBox(jComboBoxSpoint, theHC.theLookupControl.listServicePointwithOutXL());
        jComboBoxSpoint.insertItemAt(HosObject.getServicePointUD(), 0);
        chooseServicePoint(theHO.theServicePoint);
        jPanelStatSearch.setVisible(false);
        jPanelTop1.setVisible(true);

        if (authen.equals(Authentication.PHARM)
                || authen.equals(Authentication.ONE)
                || authen.equals(Authentication.LAB)
                || authen.equals(Authentication.HEALTH)) {
            jPanelQDispense.setVisible(true);
        }

        if (authen.equals(Authentication.XRAY)
                || authen.equals(Authentication.LAB)
                || authen.equals(Authentication.STAT)) {
            jComboBoxSpoint.setEnabled(false);
            jComboBoxDoctor.setEnabled(false);
        }

        if (authen.equals(Authentication.STAT)) {
            jRadioButtonAll.setSelected(true);
            this.jToggleButtonStat.setSelected(true);
            this.jToggleButtonStatActionPerformed(null);
            jPanelTop1.setVisible(false);
            jTableQueueOpd.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        } else {
            jPanelTop1.setVisible(true);
            jTableQueueOpd.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        }
        //refreshButton();
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroupPatient = new javax.swing.ButtonGroup();
        jPanelTop = new javax.swing.JPanel();
        jPanelTop1 = new javax.swing.JPanel();
        jLabelPname = new javax.swing.JLabel();
        jLabelDoctor = new javax.swing.JLabel();
        jComboBoxDoctor = new javax.swing.JComboBox();
        jComboBoxSpoint = new javax.swing.JComboBox();
        jPanelRadioPttype = new javax.swing.JPanel();
        jRadioButtonOPD = new javax.swing.JRadioButton();
        jRadioButtonAll = new javax.swing.JRadioButton();
        jRadioButtonIPD = new javax.swing.JRadioButton();
        jButton1 = new javax.swing.JButton();
        jToggleButtonSound = new javax.swing.JToggleButton();
        jToggleButtonStat = new javax.swing.JToggleButton();
        jPanelStatSearch = new javax.swing.JPanel();
        dateComboBoxStart = new com.hospital_os.utility.DateComboBox();
        jLabelTo = new javax.swing.JLabel();
        dateComboBoxEnd = new com.hospital_os.utility.DateComboBox();
        jCheckBoxSearchByDate = new javax.swing.JCheckBox();
        jButtonDoctorDischarge = new javax.swing.JButton();
        jToggleButtonQueue = new javax.swing.JToggleButton();
        jPanelCard = new javax.swing.JPanel();
        jSplitPane1 = new javax.swing.JSplitPane();
        jPanelCenter = new javax.swing.JPanel();
        jScrollPane = new javax.swing.JScrollPane();
        jTableQueueOpd = new com.hosv3.gui.component.HJTableSort();
        jPanelQDispense = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableQueueDispense = new com.hosv3.gui.component.HJTableSort();
        jButtonDispense = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTableQueueStat = new com.hosv3.gui.component.HJTableSort();

        setLayout(new java.awt.GridBagLayout());

        jPanelTop.setLayout(new java.awt.GridBagLayout());

        jPanelTop1.setLayout(new java.awt.GridBagLayout());

        jLabelPname.setFont(jLabelPname.getFont());
        jLabelPname.setText("�ش��ԡ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelTop1.add(jLabelPname, gridBagConstraints);

        jLabelDoctor.setFont(jLabelDoctor.getFont());
        jLabelDoctor.setText("ᾷ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelTop1.add(jLabelDoctor, gridBagConstraints);

        jComboBoxDoctor.setFont(jComboBoxDoctor.getFont());
        jComboBoxDoctor.setMinimumSize(new java.awt.Dimension(66, 30));
        jComboBoxDoctor.setPreferredSize(new java.awt.Dimension(66, 30));
        jComboBoxDoctor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxDoctorActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelTop1.add(jComboBoxDoctor, gridBagConstraints);

        jComboBoxSpoint.setFont(jComboBoxSpoint.getFont());
        jComboBoxSpoint.setMinimumSize(new java.awt.Dimension(66, 30));
        jComboBoxSpoint.setPreferredSize(new java.awt.Dimension(66, 30));
        jComboBoxSpoint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxSpointActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelTop1.add(jComboBoxSpoint, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanelTop.add(jPanelTop1, gridBagConstraints);

        jPanelRadioPttype.setLayout(new java.awt.GridBagLayout());

        buttonGroupPatient.add(jRadioButtonOPD);
        jRadioButtonOPD.setFont(jRadioButtonOPD.getFont());
        jRadioButtonOPD.setSelected(true);
        jRadioButtonOPD.setText("OPD");
        jRadioButtonOPD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonOPDActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelRadioPttype.add(jRadioButtonOPD, gridBagConstraints);

        buttonGroupPatient.add(jRadioButtonAll);
        jRadioButtonAll.setFont(jRadioButtonAll.getFont());
        jRadioButtonAll.setText("ALL");
        jRadioButtonAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonAllActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelRadioPttype.add(jRadioButtonAll, gridBagConstraints);

        buttonGroupPatient.add(jRadioButtonIPD);
        jRadioButtonIPD.setFont(jRadioButtonIPD.getFont());
        jRadioButtonIPD.setText("IPD");
        jRadioButtonIPD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonIPDActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelRadioPttype.add(jRadioButtonIPD, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        jPanelTop.add(jPanelRadioPttype, gridBagConstraints);

        jButton1.setFont(jButton1.getFont());
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/Refresh.png"))); // NOI18N
        jButton1.setToolTipText("���¡�٢���������");
        jButton1.setMinimumSize(new java.awt.Dimension(26, 26));
        jButton1.setPreferredSize(new java.awt.Dimension(26, 26));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelTop.add(jButton1, gridBagConstraints);

        jToggleButtonSound.setFont(jToggleButtonSound.getFont());
        jToggleButtonSound.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/play_sound_off.gif"))); // NOI18N
        jToggleButtonSound.setToolTipText("�Դ/�Դ���§ ");
        jToggleButtonSound.setMaximumSize(new java.awt.Dimension(27, 27));
        jToggleButtonSound.setMinimumSize(new java.awt.Dimension(26, 26));
        jToggleButtonSound.setPreferredSize(new java.awt.Dimension(26, 26));
        jToggleButtonSound.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButtonSoundActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelTop.add(jToggleButtonSound, gridBagConstraints);

        jToggleButtonStat.setFont(jToggleButtonStat.getFont());
        jToggleButtonStat.setText("ʶԵ�");
        jToggleButtonStat.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jToggleButtonStat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButtonStatActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelTop.add(jToggleButtonStat, gridBagConstraints);

        jPanelStatSearch.setLayout(new java.awt.GridBagLayout());

        dateComboBoxStart.setFont(dateComboBoxStart.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelStatSearch.add(dateComboBoxStart, gridBagConstraints);

        jLabelTo.setFont(jLabelTo.getFont());
        jLabelTo.setText("�֧");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelStatSearch.add(jLabelTo, gridBagConstraints);

        dateComboBoxEnd.setFont(dateComboBoxEnd.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelStatSearch.add(dateComboBoxEnd, gridBagConstraints);

        jCheckBoxSearchByDate.setFont(jCheckBoxSearchByDate.getFont());
        jCheckBoxSearchByDate.setSelected(true);
        jCheckBoxSearchByDate.setText("�ѹ���");
        jCheckBoxSearchByDate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxSearchByDateActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelStatSearch.add(jCheckBoxSearchByDate, gridBagConstraints);

        jButtonDoctorDischarge.setFont(jButtonDoctorDischarge.getFont());
        jButtonDoctorDischarge.setText("��˹���");
        jButtonDoctorDischarge.setToolTipText("�����¡�÷�����͡");
        jButtonDoctorDischarge.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDoctorDischargeActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelStatSearch.add(jButtonDoctorDischarge, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        jPanelTop.add(jPanelStatSearch, gridBagConstraints);

        jToggleButtonQueue.setFont(jToggleButtonQueue.getFont().deriveFont(jToggleButtonQueue.getFont().getStyle() | java.awt.Font.BOLD, jToggleButtonQueue.getFont().getSize()+2));
        jToggleButtonQueue.setForeground(java.awt.Color.blue);
        jToggleButtonQueue.setText("Q");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelTop.add(jToggleButtonQueue, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(jPanelTop, gridBagConstraints);

        jPanelCard.setLayout(new java.awt.CardLayout());

        jSplitPane1.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        jSplitPane1.setResizeWeight(1.0);
        jSplitPane1.setOneTouchExpandable(true);

        jPanelCenter.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "������㹨ش��ԡ��", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP));
        jPanelCenter.setMinimumSize(new java.awt.Dimension(150, 47));
        jPanelCenter.setPreferredSize(new java.awt.Dimension(150, 47));
        jPanelCenter.setLayout(new java.awt.GridBagLayout());

        jScrollPane.setPreferredSize(new java.awt.Dimension(453, 403));

        jTableQueueOpd.setFillsViewportHeight(true);
        jTableQueueOpd.setFont(jTableQueueOpd.getFont());
        jTableQueueOpd.setRowHeight(50);
        jTableQueueOpd.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTableQueueOpdMouseReleased(evt);
            }
        });
        jTableQueueOpd.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTableQueueOpdKeyReleased(evt);
            }
        });
        jScrollPane.setViewportView(jTableQueueOpd);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanelCenter.add(jScrollPane, gridBagConstraints);

        jSplitPane1.setLeftComponent(jPanelCenter);

        jPanelQDispense.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "�������ͨ�����", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));
        jPanelQDispense.setMinimumSize(new java.awt.Dimension(150, 150));
        jPanelQDispense.setPreferredSize(new java.awt.Dimension(150, 150));
        jPanelQDispense.setLayout(new java.awt.GridBagLayout());

        jScrollPane1.setPreferredSize(new java.awt.Dimension(453, 403));

        jTableQueueDispense.setFillsViewportHeight(true);
        jTableQueueDispense.setFont(jTableQueueDispense.getFont());
        jTableQueueDispense.setRowHeight(50);
        jTableQueueDispense.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTableQueueDispenseMouseReleased(evt);
            }
        });
        jTableQueueDispense.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTableQueueDispenseKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(jTableQueueDispense);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelQDispense.add(jScrollPane1, gridBagConstraints);

        jButtonDispense.setFont(jButtonDispense.getFont());
        jButtonDispense.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/dispense.gif"))); // NOI18N
        jButtonDispense.setToolTipText(GuiLang.setLanguage("�����ҷ�������¤�"));
        jButtonDispense.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonDispense.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonDispense.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDispenseActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelQDispense.add(jButtonDispense, gridBagConstraints);

        jSplitPane1.setRightComponent(jPanelQDispense);

        jPanelCard.add(jSplitPane1, "card_opd");

        jPanel3.setLayout(new java.awt.GridBagLayout());

        jScrollPane2.setPreferredSize(new java.awt.Dimension(453, 403));

        jTableQueueStat.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null}
            },
            new String [] {
                "Title 1"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jTableQueueStat.setFillsViewportHeight(true);
        jTableQueueStat.setFont(jTableQueueStat.getFont());
        jTableQueueStat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTableQueueStatKeyReleased(evt);
            }
        });
        jTableQueueStat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTableQueueStatMouseReleased(evt);
            }
        });
        jScrollPane2.setViewportView(jTableQueueStat);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(3, 0, 0, 0);
        jPanel3.add(jScrollPane2, gridBagConstraints);

        jPanelCard.add(jPanel3, "card_stat");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        add(jPanelCard, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonDispenseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDispenseActionPerformed
        this.doDispenseAction();
    }//GEN-LAST:event_jButtonDispenseActionPerformed

    private void jTableQueueStatKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTableQueueStatKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_UP || evt.getKeyCode() == KeyEvent.VK_DOWN) {
            jTableQueueStatMouseReleased(null);
        }
    }//GEN-LAST:event_jTableQueueStatKeyReleased

    private void jTableQueueStatMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableQueueStatMouseReleased
//�ó� stat ���͡���������¤����ͨ�˹�����§��������
        int[] rows = jTableQueueStat.getSelectedRows();
        if (rows.length > 1) {
            return;
        }
        int row = this.jTableQueueStat.getSelectedRow();
        String[] t = (String[]) vQueueStat.get(row);
        theHC.theVisitControl.readVisitPatientByVid(t[t.length - 1], "0", null);
    }//GEN-LAST:event_jTableQueueStatMouseReleased

    private void jToggleButtonStatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButtonStatActionPerformed
        CardLayout cl = (CardLayout) jPanelCard.getLayout();
        jPanelStatSearch.setVisible(jToggleButtonStat.isSelected());
        jPanelTop1.setVisible(!jToggleButtonStat.isSelected());
        if (this.jToggleButtonStat.isSelected()) {
            cl.show(jPanelCard, "card_stat");
        } else {
            cl.show(jPanelCard, "card_opd");
        }
    }//GEN-LAST:event_jToggleButtonStatActionPerformed

    private void jButtonDoctorDischargeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDoctorDischargeActionPerformed
        Visit visit = new Visit();
        visit.visit_type = jRadioButtonIPD.isSelected() ? VisitType.IPD : VisitType.OPD;
        if (theHD.showDialogDischarge(visit)) {
            int[] select = jTableQueueStat.getSelectedRows();
            theHC.theVisitControl.dischargeDoctor(visit, vQueueStat, select);
        }
    }//GEN-LAST:event_jButtonDoctorDischargeActionPerformed

    private void jCheckBoxSearchByDateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxSearchByDateActionPerformed
        dateComboBoxEnd.setEnabled(jCheckBoxSearchByDate.isSelected());
        dateComboBoxStart.setEnabled(jCheckBoxSearchByDate.isSelected());
        this.refreshButton();
    }//GEN-LAST:event_jCheckBoxSearchByDateActionPerformed

    private void jToggleButtonSoundActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButtonSoundActionPerformed
//Audio.getAudio("config/sound/new_patient.wav").play();
        if (jToggleButtonSound.isSelected()) {
            jToggleButtonSound.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/play_sound.gif")));
        } else {
            jToggleButtonSound.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/play_sound_off.gif")));
        }
        theHC.theSystemControl.setSoundEnabled(jToggleButtonSound.isSelected());

    }//GEN-LAST:event_jToggleButtonSoundActionPerformed

    /**
     * ��㹵�Ǩ�ͺ����� �ش��ԡ�÷����ᾷ���������
     *
     * @param sp
     * @author Pongtorn(Henbe)
     * @date 20/03/49,18:42
     */
    protected void chooseServicePoint(ServicePoint sp) {
        jComboBoxDoctor.setVisible(false);
        jLabelDoctor.setVisible(false);
        if (sp == null) {
            return;
        }
        ComboboxModel.setCodeComboBox(jComboBoxSpoint, sp.getObjectId());
        if (!sp.service_type_id.equals(ServiceType.DIAG)) {
            return;
        }
        Vector v = theHC.theLookupControl.listDoctorSP(sp.getObjectId());
        if (v != null) {
            jComboBoxDoctor.setVisible(true);
            jLabelDoctor.setVisible(true);
            ComboboxModel.initComboBox(jComboBoxDoctor, v);
            jComboBoxDoctor.insertItemAt(HosObject.getEmployeeUD(), 0);
            jComboBoxDoctor.setSelectedIndex(0);
            if (authen.equals(Authentication.DOCTOR)) {
                Gutil.setGuiData(jComboBoxDoctor, theHO.theEmployee.getObjectId());
            }
        }
    }

    private void jComboBoxSpointActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxSpointActionPerformed
        ServicePoint sp = (ServicePoint) jComboBoxSpoint.getSelectedItem();
        chooseServicePoint(sp);
        refreshButton();
    }//GEN-LAST:event_jComboBoxSpointActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        refreshButton();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jTableQueueDispenseKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTableQueueDispenseKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_UP || evt.getKeyCode() == KeyEvent.VK_DOWN) {
            jTableQueueDispenseMouseReleased(null);
        }
    }//GEN-LAST:event_jTableQueueDispenseKeyReleased

    private void jComboBoxDoctorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxDoctorActionPerformed
        refreshButton();
    }//GEN-LAST:event_jComboBoxDoctorActionPerformed

       private void jRadioButtonIPDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonIPDActionPerformed
        refreshButton();
        jButtonDoctorDischarge.setEnabled(!jRadioButtonAll.isSelected());
    }//GEN-LAST:event_jRadioButtonIPDActionPerformed

    private void jRadioButtonOPDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonOPDActionPerformed
        refreshButton();
        jButtonDoctorDischarge.setEnabled(!jRadioButtonAll.isSelected());
    }//GEN-LAST:event_jRadioButtonOPDActionPerformed

    private void jRadioButtonAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonAllActionPerformed
        refreshButton();
        jButtonDoctorDischarge.setEnabled(!jRadioButtonAll.isSelected());
    }//GEN-LAST:event_jRadioButtonAllActionPerformed

    private void jTableQueueDispenseMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableQueueDispenseMouseReleased
        this.jTableQueueOpd.clearSelection();
        int[] rows = jTableQueueDispense.getSelectedRows();
        int row = -1;
        if (rows.length == 1) {
            row = rows[0];
        }
        if (row == -1) {
            return;
        }
        if (evt.getClickCount() == 2) {
            jTableQueueDispense.setEnabled(false);
            //����ͨ�����
            if ((theQueueDispense2V != null && theQueueDispense2V.size() > 0)
                    && theQueueDispense2V.get(row) instanceof QueueDispense2) {
                QueueDispense2 spd = (QueueDispense2) theQueueDispense2V.get(row);
                theHC.theVisitControl.readVisitPatientByVid(spd.visit_id);
            }
            //��Ǥ�ҧ���ź
            if ((vListRemain != null && vListRemain.size() > 0)
                    && vListRemain.get(row) instanceof ListTransfer) {
                //amp:09/03/2549 ���ж�Ҽ��������Ż���Դ ��ͧ��͹���� �֧��ͧ�յ�ǵ�Ǩ�ͺ
                ListTransfer t = (ListTransfer) vListRemain.get(row);
                theHC.theVisitControl.readVisitPatientByVid(t.visit_id, "1", t);
            }
            jTableQueueDispense.setEnabled(true);
            jTableQueueDispense.setRowSelectionInterval(row, row);
        }
    }//GEN-LAST:event_jTableQueueDispenseMouseReleased

    private void jTableQueueOpdKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTableQueueOpdKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_UP || evt.getKeyCode() == KeyEvent.VK_DOWN) {
            jTableQueueOpdMouseReleased(null);
        }
        if (evt.getKeyCode() == KeyEvent.VK_DELETE) {
            if (theHO.theEmployee.authentication_id.equals(Authentication.LAB)) {
                theHC.theVisitControl.deleteQueueLab(true);
            } else {
                theHC.theVisitControl.deleteQueueTransfer(vListTransfer, jTableQueueOpd.getSelectedRows());
            }
        }
    }//GEN-LAST:event_jTableQueueOpdKeyReleased


    /*
     * //��˹�����ʴ� default panel ��Ǩ�ͺ�Է�Ԣͧ�����ҹ
     */
    private void jTableQueueOpdMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableQueueOpdMouseReleased
        jTableQueueDispense.clearSelection();
        //�óռ�������仨����͡������������� 1 ����ҹ��
        int row = jTableQueueOpd.getSelectedRow();
        if (row == -1) {
            return;
        }
        ListTransfer listTransfer = (ListTransfer) vListTransfer.get(row);
        if (this.jToggleButtonQueue.isSelected()) {
            if (evt.getClickCount() >= 2) {
                String ret = JOptionPane.showInputDialog(theUS.getJFrame(), "�����Ţ���");
                if (ret != null && !ret.isEmpty()) {
                    if (theHC.theVisitControl.saveQueueValue(listTransfer, ret)) {
                        jButton1.doClick();
                    }
                }
            } else {
                theUS.setStatus("��س� double click �Ţ��Ƿ���ͧ������ ���͡����� Q ���͡�Ѻ����͡����", UpdateStatus.WARNING);
            }
        } else {
            theHC.theVisitControl.readVisitPatientByVid(listTransfer.visit_id, "0", listTransfer);
        }
    }//GEN-LAST:event_jTableQueueOpdMouseReleased


    /*
     * //��Ǩ�ͺ��Ҩش��ԡ�ù����ᾷ���������Ҷ����
     * ��������͡�ش���������ͧ��Ǩ //��� default �繪���ᾷ�� login ���
     * date 1/03/48 build 8
     *
     */
    protected void refreshButton() {
        setCursor(Constant.CUR_DEFAULT);
        String d = "";
        if (jComboBoxDoctor.isEnabled() && jComboBoxDoctor.isVisible()) {
            d = Gutil.getGuiData(jComboBoxDoctor);
        }

        String s = Gutil.getGuiData(jComboBoxSpoint);
        String visitType = jPanelRadioPttype.isVisible() ? "" : "0";
        if (jPanelRadioPttype.isVisible()) {
            if (jRadioButtonOPD.isSelected()) {
                visitType = "0";//opd
            } else if (jRadioButtonIPD.isSelected()) {
                visitType = "1";//ipd
            }
        }
        int old_qty = 0;
        if (vListTransfer != null) {
            old_qty = vListTransfer.size();
        }
        ////////////////////////////////////////////////////////////
        String date_from = dateComboBoxStart.getText();
        String date_to = dateComboBoxEnd.getText();
        if (!jCheckBoxSearchByDate.isSelected()) {
            date_from = "";
            date_to = "";
        }
        if (this.jToggleButtonStat.isSelected()) {
            Vector vt = theHC.theVisitControl.listQueueICD(visitType, date_from, date_to);
            setQueueStatV(vt);
        } else {
            if (authen.equals(Authentication.XRAY)) {
                vListTransfer = theHC.theVisitControl.listQueueXray(visitType);
                setListTransferV(vListTransfer);
            } else if (authen.equals(Authentication.LAB)) {
                vListTransfer = theHC.theVisitControl.listQueueLab(visitType);
                setListTransferLab(vListTransfer);
                vListRemain = theHC.theVisitControl.listRemainQueueLab(visitType);
                setListTransferLabRemain(vListRemain);
            } else {
                //����ª��ͼ����·������㹨ش��ԡ�÷���˹�
                vListTransfer = theHC.theVisitControl.listTransferByServicePoint(s, d, visitType);
                setListTransferV(vListTransfer);
            }
            if (authen.equals(Authentication.PHARM)
                    || authen.equals(Authentication.ONE)
                    || authen.equals(Authentication.HEALTH)) {
                // Drug and one stop service
                theQueueDispense2V = theHC.theVisitControl.listVisitInQueueDispense2(visitType);
                setQueueDispense2V(theQueueDispense2V);
            }
        }
        if (vListTransfer != null) {
//            Constant.println("________________________ if(vListTransfer!=null)");
            int new_qty = vListTransfer.size();
            if (new_qty > old_qty && theHC.theSystemControl.isSoundEnabled()) {
//                Constant.println("________________________PlaySound");
                Audio.getAudio("config/sound/new_patient.wav").play();
            }
        }

    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroupPatient;
    private com.hospital_os.utility.DateComboBox dateComboBoxEnd;
    private com.hospital_os.utility.DateComboBox dateComboBoxStart;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButtonDispense;
    private javax.swing.JButton jButtonDoctorDischarge;
    private javax.swing.JCheckBox jCheckBoxSearchByDate;
    private javax.swing.JComboBox jComboBoxDoctor;
    private javax.swing.JComboBox jComboBoxSpoint;
    private javax.swing.JLabel jLabelDoctor;
    private javax.swing.JLabel jLabelPname;
    private javax.swing.JLabel jLabelTo;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanelCard;
    private javax.swing.JPanel jPanelCenter;
    private javax.swing.JPanel jPanelQDispense;
    private javax.swing.JPanel jPanelRadioPttype;
    private javax.swing.JPanel jPanelStatSearch;
    private javax.swing.JPanel jPanelTop;
    private javax.swing.JPanel jPanelTop1;
    private javax.swing.JRadioButton jRadioButtonAll;
    private javax.swing.JRadioButton jRadioButtonIPD;
    private javax.swing.JRadioButton jRadioButtonOPD;
    private javax.swing.JScrollPane jScrollPane;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSplitPane jSplitPane1;
    private com.hosv3.gui.component.HJTableSort jTableQueueDispense;
    private com.hosv3.gui.component.HJTableSort jTableQueueOpd;
    private com.hosv3.gui.component.HJTableSort jTableQueueStat;
    private javax.swing.JToggleButton jToggleButtonQueue;
    private javax.swing.JToggleButton jToggleButtonSound;
    private javax.swing.JToggleButton jToggleButtonStat;
    // End of variables declaration//GEN-END:variables

    //����Ѻ���ҧ�ͨ�����
    private void setQueueDispense2V(Vector vc) {
        theQueueDispense2V = vc;
        countrowTransaction = 0;
        TaBleModel tm;
        if (vc != null) {
            tm = new TaBleModel(col_dispense, vc.size());
            for (int i = 0, size = vc.size(); i < size; i++) {
                countrowTransaction += 1;
                QueueDispense2 pd = (QueueDispense2) vc.get(i);
                if (pd != null) {
                    if (pd.arrived_status == null) {
                        pd.arrived_status = "0";
                    }
                    String name = theHC.theLookupControl.getPatientName(pd);

                    tm.setValueAt(countrowTransaction + "|" + pd.arrived_status, i, 0);
                    tm.setValueAt(pd.range_age, i, 1);
                    tm.setValueAt(pd.hn, i, 2);
                    tm.setValueAt(pd.vn, i, 3);
                    tm.setValueAt(name, i, 4);
                    tm.setValueAt(pd.service_point_name, i, 5);
                    // Somprasong 31102011 �����ըش��ԡ������ش
                    tm.setValueAt(pd.servicePointColor
                            + "|" + "" //+ pd.service_point_name
                            ,
                             i, 6);
                    try {
                        tm.setValueAt(DateUtil.getDateFromText(pd.assign_time), i, 7);
                    } catch (Exception ex) {
                        tm.setValueAt(pd.assign_time, i, 7);
                    }
                    tm.setValueAt(pd.arrived_datetime, i, 8);
                    tm.setValueAt(new Integer(pd.number_order), i, 9);
                    // Somprasong 10072012 �������
                    tm.setValueAt(pd.visit_queue_color
                            + "|" + pd.visit_queue_name
                            + "|" + pd.visit_queue_number, i, 10);
                }
            }
        } else {
            tm = new TaBleModel(col_dispense, 0);
        }
        setQuantityInQueue("�ӹǹ���������Ѻ��", jPanelQDispense, vc);
        //String[] col_dispense = {"HN","VN","����-ʡ��","�ش��ԡ���ش����","����������ͨ�����","�ӹǹ"};
        jTableQueueDispense.setModel(tm);
        jTableQueueDispense.getColumnModel().getColumn(0).setMaxWidth(50); // �ӴѺ
        jTableQueueDispense.getColumnModel().getColumn(0).setCellRenderer(queueRender);
        jTableQueueDispense.getColumnModel().getColumn(1).setPreferredWidth(50);
        jTableQueueDispense.getColumnModel().getColumn(1).setCellRenderer(rangeAgeRender);
        jTableQueueDispense.getColumnModel().getColumn(2).setPreferredWidth(60); // hn
        if (theHC.theLO.theOption.unused_pattern.equals("0")) {
            jTableQueueDispense.getColumnModel().getColumn(2).setCellRenderer(hnRender);
        } else {
            jTableQueueDispense.getColumnModel().getColumn(2).setCellRenderer(new CellRendererHos(CellRendererHos.HN));
        }
        jTableQueueDispense.getColumnModel().getColumn(3).setPreferredWidth(80); // vn
        if (theHC.theLO.theOption.unused_pattern.equals("0")) {
            jTableQueueDispense.getColumnModel().getColumn(3).setCellRenderer(vnRender);
        } else {
            jTableQueueDispense.getColumnModel().getColumn(3).setCellRenderer(new CellRendererHos(CellRendererHos.VN));
        }
        jTableQueueDispense.getColumnModel().getColumn(4).setPreferredWidth(300); // name
        jTableQueueDispense.getColumnModel().getColumn(5).setPreferredWidth(120); // service point
        jTableQueueDispense.getColumnModel().getColumn(5).setCellRenderer(TableRenderer.getRendererCenter());
        // SOmprasong 31102011 �����ըش��ԡ��
        jTableQueueDispense.getColumnModel().getColumn(6).setPreferredWidth(50); // service point color
        jTableQueueDispense.getColumnModel().getColumn(6).setCellRenderer(theCellRendererColor); //8
        jTableQueueDispense.getColumnModel().getColumn(7).setPreferredWidth(160); // wait time
        jTableQueueDispense.getColumnModel().getColumn(7).setCellRenderer(dateRender);
        jTableQueueDispense.getColumnModel().getColumn(8).setPreferredWidth(160); // arrive time
        jTableQueueDispense.getColumnModel().getColumn(8).setCellRenderer(dateRender);
        jTableQueueDispense.getColumnModel().getColumn(9).setMaxWidth(100); // �ӹǹ
        jTableQueueDispense.getColumnModel().getColumn(9).setCellRenderer(TableRenderer.getRendererRight());
        jTableQueueDispense.getColumnModel().getColumn(10).setPreferredWidth(120);
        jTableQueueDispense.getColumnModel().getColumn(10).setCellRenderer(theCellRendererColor);

    }

    private void setTableListTransfer(boolean show) {
        jTableQueueOpd.getColumnModel().getColumn(0).setMaxWidth(50); // �ӴѺ
        jTableQueueOpd.getColumnModel().getColumn(0).setCellRenderer(queueRender);
        jTableQueueOpd.getColumnModel().getColumn(1).setMaxWidth(50); // ��͡
        jTableQueueOpd.getColumnModel().getColumn(1).setCellRenderer(cellRendererTranfer);
        jTableQueueOpd.getColumnModel().getColumn(2).setMaxWidth(50); // ����
        jTableQueueOpd.getColumnModel().getColumn(2).setCellRenderer(allergyRender);
        jTableQueueOpd.getColumnModel().getColumn(3).setMaxWidth(50);
        jTableQueueOpd.getColumnModel().getColumn(3).setCellRenderer(esiRender);
        jTableQueueOpd.getColumnModel().getColumn(4).setMaxWidth(50);
        jTableQueueOpd.getColumnModel().getColumn(4).setCellRenderer(urgenyRender);
        jTableQueueOpd.getColumnModel().getColumn(5).setMaxWidth(80);
        jTableQueueOpd.getColumnModel().getColumn(5).setCellRenderer(rangeAgeRender);
        jTableQueueOpd.getColumnModel().getColumn(6).setPreferredWidth(115); // HN 1
        if (theHC.theLO.theOption.unused_pattern.equals("0")) {
            jTableQueueOpd.getColumnModel().getColumn(6).setCellRenderer(hnRender);
        } else {
            jTableQueueOpd.getColumnModel().getColumn(6).setCellRenderer(new CellRendererHos(CellRendererHos.HN));
        }
        jTableQueueOpd.getColumnModel().getColumn(7).setPreferredWidth(110); // VN 2
        if (theHC.theLO.theOption.unused_pattern.equals("0")) {
            jTableQueueOpd.getColumnModel().getColumn(7).setCellRenderer(vnRender);
        } else {
            jTableQueueOpd.getColumnModel().getColumn(7).setCellRenderer(new CellRendererHos(CellRendererHos.VN));
        }
        jTableQueueOpd.getColumnModel().getColumn(8).setPreferredWidth(200); // ���� 3
        jTableQueueOpd.getColumnModel().getColumn(9).setPreferredWidth(180); // ���� 5
        jTableQueueOpd.getColumnModel().getColumn(9).setCellRenderer(dateRender);
        jTableQueueOpd.getColumnModel().getColumn(10).setPreferredWidth(150); // ���� 5
        jTableQueueOpd.getColumnModel().getColumn(10).setCellRenderer(dateRender);
        //show ��Ѻ�������������价������� lab ���� xray
        if (show) {
            jTableQueueOpd.getColumnModel().getColumn(11).setPreferredWidth(150); // �ش��ԡ�� 6
            jTableQueueOpd.getColumnModel().getColumn(11).setCellRenderer(TableRenderer.getRendererCenter());
            //used_queue ��Ѻ option ���͡�������������� ��Ǽ�����
            if (used_queue) {
                jTableQueueOpd.getColumnModel().getColumn(12).setPreferredWidth(200);
                jTableQueueOpd.getColumnModel().getColumn(12).setCellRenderer(scoreRender);
                jTableQueueOpd.getColumnModel().getColumn(13).setMaxWidth(80); // �Ţ��� 7
                jTableQueueOpd.getColumnModel().getColumn(13).setCellRenderer(TableRenderer.getRendererRight());
                jTableQueueOpd.getColumnModel().getColumn(14).setPreferredWidth(120); // ��� 8
                jTableQueueOpd.getColumnModel().getColumn(14).setCellRenderer(theCellRendererColor); //8
                jTableQueueOpd.getColumnModel().getColumn(15).setCellRenderer(labRender);
                jTableQueueOpd.getColumnModel().getColumn(15).setMaxWidth(50);
                jTableQueueOpd.getColumnModel().getColumn(16).setCellRenderer(xrayRender);
                jTableQueueOpd.getColumnModel().getColumn(16).setMaxWidth(50);
            } else {
                jTableQueueOpd.getColumnModel().getColumn(12).setPreferredWidth(200);
                jTableQueueOpd.getColumnModel().getColumn(12).setCellRenderer(scoreRender);
                jTableQueueOpd.getColumnModel().getColumn(13).setCellRenderer(labRender);
                jTableQueueOpd.getColumnModel().getColumn(13).setMaxWidth(50);
                jTableQueueOpd.getColumnModel().getColumn(14).setCellRenderer(xrayRender);
                jTableQueueOpd.getColumnModel().getColumn(14).setMaxWidth(50);
            }
        } else {// lab & xray
            jTableQueueOpd.getColumnModel().getColumn(11).setPreferredWidth(50); // �Ţ���
            jTableQueueOpd.getColumnModel().getColumn(11).setCellRenderer(TableRenderer.getRendererRight());
            jTableQueueOpd.getColumnModel().getColumn(12).setPreferredWidth(120);// ���
            jTableQueueOpd.getColumnModel().getColumn(12).setCellRenderer(theCellRendererColor);
            jTableQueueOpd.getColumnModel().getColumn(13).setCellRenderer(labRender);
            jTableQueueOpd.getColumnModel().getColumn(13).setMaxWidth(50);
            if (theHO.theEmployee.authentication_id.equals(Authentication.XRAY)) {
                jTableQueueOpd.getColumnModel().getColumn(14).setPreferredWidth(0);
                jTableQueueOpd.getColumnModel().getColumn(14).setMinWidth(0);
                jTableQueueOpd.getColumnModel().getColumn(14).setMaxWidth(0);
                jTableQueueOpd.getColumnModel().getColumn(14).setWidth(0);
            }
            jTableQueueOpd.getColumnModel().getColumn(15).setCellRenderer(xrayRender);
            jTableQueueOpd.getColumnModel().getColumn(15).setMaxWidth(50);
        }
    }

    /**
     * ����Ѻ���ҧ��ҧ���Ż
     */
    private void setListTransferLabRemain(Vector vc) {
//        Vector vPrefix = theHC.theLookupControl.listPrefix(); // Somprasong comment not use 191011
        if (vc == null) {
            vc = new Vector();
        }
        countrowTransaction = 0;
        TaBleModel tm = new TaBleModel(col_lab, vc.size());
        for (int i = 0; i < vc.size() && vc != null; i++) {
            countrowTransaction += 1;
            ListTransfer listTransfer = (ListTransfer) vc.get(i);
            if (listTransfer.queue == null) {
                listTransfer.queue = "0";
                listTransfer.color = "r=255,g=255,b=255";
                listTransfer.description = ("");
            }
            boolean isStat = listTransfer.drug_stat_status.equals("1");
            boolean isUrgent = listTransfer.xray_urgent_status.equals("1") || listTransfer.lab_urgent_status.equals("1");
            if ("".equals(listTransfer.specimen_code)) //amp:06/03/2549 �Ż��軡�Դ
            {
                String name = theHC.theLookupControl.getPatientName(listTransfer);

                tm.setValueAt(countrowTransaction, i, 0);
                tm.setValueAt(listTransfer.locking, i, 1);
                tm.setValueAt(listTransfer.patient_allergy, i, 2);

                tm.setValueAt(listTransfer.emergency_status, i, 3);
                if (isStat && isUrgent) {
                    tm.setValueAt(ListTransfer.SHOW_ALL, i, 4);
                } else if (isStat) {
                    tm.setValueAt(ListTransfer.SHOW_STAT, i, 4);
                } else if (isUrgent) {
                    tm.setValueAt(ListTransfer.SHOW_URGENT, i, 4);
                }
                tm.setValueAt(listTransfer.range_age, i, 5);
                tm.setValueAt(listTransfer.hn, i, 6);
                tm.setValueAt(listTransfer.vn, i, 7);
                tm.setValueAt(name, i, 8);
                tm.setValueAt(listTransfer.assign_time, i, 9);
                tm.setValueAt(listTransfer.arrived_datetime, i, 10);
                if (listTransfer.queue != null && !listTransfer.queue.isEmpty()) {
                    tm.setValueAt(new Integer(listTransfer.queue), i, 11);
                } else {
                    tm.setValueAt("", i, 11);
                }
                tm.setValueAt(listTransfer.color + "|" + listTransfer.description + "|" + listTransfer.queue, i, 12);
                tm.setValueAt(listTransfer.labstatus, i, 13);
                tm.setValueAt(listTransfer.specimen_code, i, 14);
                tm.setValueAt(listTransfer.xraystatus, i, 15);
            } else {//�Ż���Դ
                tm.setValueAt(countrowTransaction, i, 0);
                tm.setValueAt("********", i, 6);
                tm.setValueAt("********", i, 7);
                tm.setValueAt("*** ***** *****", i, 8);
                tm.setValueAt(listTransfer.color + "|" + listTransfer.description + "|" + listTransfer.queue, i, 12);
                tm.setValueAt(listTransfer.labstatus, i, 13);
                tm.setValueAt(listTransfer.specimen_code, i, 14);
                tm.setValueAt(listTransfer.xraystatus, i, 15);
            }
        }
        //String[] col_dispense = {"HN","VN","����-ʡ��","�ش��ԡ���ش����","����������ͨ�����","�ӹǹ"};
        jTableQueueDispense.setModel(tm);
        setQuantityInQueue("��ҧ���Ż", jPanelQDispense, vc);
        jTableQueueDispense.getColumnModel().getColumn(0).setMaxWidth(50); // �ӴѺ
        jTableQueueDispense.getColumnModel().getColumn(0).setCellRenderer(TableRenderer.getRendererRight());
        jTableQueueDispense.getColumnModel().getColumn(1).setMaxWidth(50); // ��͡
        jTableQueueDispense.getColumnModel().getColumn(1).setCellRenderer(cellRendererTranfer);
        jTableQueueDispense.getColumnModel().getColumn(2).setMaxWidth(50); // ����
        jTableQueueDispense.getColumnModel().getColumn(2).setCellRenderer(allergyRender);
        jTableQueueDispense.getColumnModel().getColumn(3).setMaxWidth(50);
        jTableQueueDispense.getColumnModel().getColumn(3).setCellRenderer(esiRender);
        jTableQueueDispense.getColumnModel().getColumn(4).setMaxWidth(50);
        jTableQueueDispense.getColumnModel().getColumn(4).setCellRenderer(urgenyRender);
        jTableQueueDispense.getColumnModel().getColumn(5).setMaxWidth(80);
        jTableQueueDispense.getColumnModel().getColumn(5).setCellRenderer(rangeAgeRender);
        jTableQueueDispense.getColumnModel().getColumn(6).setPreferredWidth(90); // HN 1
        if (theHC.theLO.theOption.unused_pattern.equals("0")) {
            jTableQueueDispense.getColumnModel().getColumn(6).setCellRenderer(hnRender);
        } else {
            jTableQueueDispense.getColumnModel().getColumn(6).setCellRenderer(new CellRendererHos(CellRendererHos.HN));
        }
        jTableQueueDispense.getColumnModel().getColumn(7).setPreferredWidth(110); // VN 2
        if (theHC.theLO.theOption.unused_pattern.equals("0")) {
            jTableQueueDispense.getColumnModel().getColumn(7).setCellRenderer(vnRender);
        } else {
            jTableQueueDispense.getColumnModel().getColumn(7).setCellRenderer(new CellRendererHos(CellRendererHos.VN));
        }
        jTableQueueDispense.getColumnModel().getColumn(8).setPreferredWidth(200); // ���� 3
        jTableQueueDispense.getColumnModel().getColumn(9).setPreferredWidth(180); // ���� 5
        jTableQueueDispense.getColumnModel().getColumn(9).setCellRenderer(dateRender);
        jTableQueueDispense.getColumnModel().getColumn(10).setPreferredWidth(180); // ���� 5
        jTableQueueDispense.getColumnModel().getColumn(10).setCellRenderer(dateRender);
        jTableQueueDispense.getColumnModel().getColumn(11).setPreferredWidth(80); // �Ţ��� 7
        jTableQueueDispense.getColumnModel().getColumn(11).setCellRenderer(TableRenderer.getRendererRight());
        jTableQueueDispense.getColumnModel().getColumn(12).setPreferredWidth(120); // ���
        jTableQueueDispense.getColumnModel().getColumn(12).setCellRenderer(theCellRendererColor);
        jTableQueueDispense.getColumnModel().getColumn(13).setCellRenderer(labRender);
        jTableQueueDispense.getColumnModel().getColumn(13).setMaxWidth(50);
        jTableQueueDispense.getColumnModel().getColumn(15).setCellRenderer(xrayRender);
        jTableQueueDispense.getColumnModel().getColumn(15).setMaxWidth(50);
    }

    private void setQueueStatV(Vector vqueue) {
        vQueueStat = vqueue;
        TaBleModel tm = new TaBleModel(col_sp_dx, vqueue.size());

        for (int i = 0; i < vqueue.size(); i++)//row
        {
            String[] str = (String[]) vqueue.get(i);
            for (int j = 0; j < str.length - 1; j++)//column
            {
                tm.setValueAt(str[j], i, j);
                if (j == 6) {
                    tm.setValueAt(DateUtil.getDateFromText(str[j]), i, j);
                } else if (j == 9) {
                    tm.setValueAt(str[j] + "|" + str[j + 1], i, j);
                    j++;
                }
            }
        }
        jTableQueueStat.setModel(tm);
        jTableQueueStat.getColumnModel().getColumn(0).setMaxWidth(50); // �ӴѺ
        jTableQueueStat.getColumnModel().getColumn(0).setCellRenderer(TableRenderer.getRendererRight());
        jTableQueueStat.getColumnModel().getColumn(1).setMaxWidth(50); // ��͡
        jTableQueueStat.getColumnModel().getColumn(1).setCellRenderer(cellRendererTranfer);
        jTableQueueStat.getColumnModel().getColumn(2).setMaxWidth(50); // ����
        jTableQueueStat.getColumnModel().getColumn(2).setCellRenderer(allergyRender);
        jTableQueueStat.getColumnModel().getColumn(3).setPreferredWidth(90); // HN 1
        if (theHC.theLO.theOption.unused_pattern.equals("0")) {
            jTableQueueStat.getColumnModel().getColumn(3).setCellRenderer(hnRender);
        } else {
            jTableQueueStat.getColumnModel().getColumn(3).setCellRenderer(new CellRendererHos(CellRendererHos.HN));
        }
        jTableQueueStat.getColumnModel().getColumn(4).setPreferredWidth(90); // VN 2
        if (theHC.theLO.theOption.unused_pattern.equals("0")) {
            jTableQueueStat.getColumnModel().getColumn(4).setCellRenderer(vnRender);
        } else {
            jTableQueueStat.getColumnModel().getColumn(4).setCellRenderer(new CellRendererHos(CellRendererHos.VN));
        }
        jTableQueueStat.getColumnModel().getColumn(5).setPreferredWidth(180); // ���� 3
        jTableQueueStat.getColumnModel().getColumn(6).setPreferredWidth(180); // ���� 5
        jTableQueueStat.getColumnModel().getColumn(6).setCellRenderer(dateRender);
        jTableQueueStat.getColumnModel().getColumn(7).setPreferredWidth(180); // ���� 3
        jTableQueueStat.getColumnModel().getColumn(8).setMaxWidth(50); // ��͡
        jTableQueueStat.getColumnModel().getColumn(8).setCellRenderer(labRender);
        jTableQueueStat.getColumnModel().getColumn(9).setPreferredWidth(120); // ���
        jTableQueueStat.getColumnModel().getColumn(9).setCellRenderer(theCellRendererColor); //8
        countpatient = vQueueStat.size();

        if (vQueueStat.isEmpty()) {
            jScrollPane2.setBorder(new javax.swing.border.TitledBorder(null, Constant.getTextBundle("����ռ�����㹤��"), javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP));
        } else {
            jScrollPane2.setBorder(new javax.swing.border.TitledBorder(null, Constant.getTextBundle("�ӹǹ��������ŧ�����ä")
                    + "  " + countpatient + "  " + Constant.getTextBundle("��"), javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP));
        }
    }

    //xxxxxx
    private void setListTransferV(Vector vc) {
//        Vector vPrefix = theHC.theLookupControl.listPrefix(); // Somprasong comment not use 191011
        countrowTransaction = 0;
        if (vc == null) {
            vc = new Vector();
        }
        TaBleModel tm;

        if (used_queue)//�ʴ��ش��ԡ�� ����ʴ����
        {
            tm = new TaBleModel(col_sp_queue, vc.size());
        } else //�ʴ��ش��ԡ�� ������ʴ����
        {
            tm = new TaBleModel(col_sp, vc.size());
        }

        boolean checkvalue = true;
        for (int i = 0; i < vc.size(); i++) {
            countrowTransaction += 1;
            ListTransfer listTransfer = (ListTransfer) vc.get(i);
            if (listTransfer.queue == null && listTransfer.visit_type.equalsIgnoreCase(VisitType.OPD)) {
                listTransfer.queue = "0";
                listTransfer.color = "r=255,g=255,b=255";
                listTransfer.description = Constant.getTextBundle("�������");
            }
            if (listTransfer.arrived_status == null) {
                listTransfer.arrived_status = "0";
            }
            String name = theHC.theLookupControl.getPatientName(listTransfer);
            String newsPewsScore = "";
            if (listTransfer.visit_vital_sign_score != null) {
                newsPewsScore = listTransfer.visit_vital_sign_newspews_type + " Score: " + listTransfer.visit_vital_sign_score;
            }
            if (listTransfer.visit_vital_sign_notify_datetime != null) {
                newsPewsScore += "|" + listTransfer.visit_vital_sign_notify_datetime;
            }
            tm.setValueAt(countrowTransaction + "|" + listTransfer.arrived_status, i, 0);
            tm.setValueAt(listTransfer.locking, i, 1);
            tm.setValueAt(listTransfer.patient_allergy, i, 2);

            tm.setValueAt(listTransfer.emergency_status, i, 3);
            boolean isStat = listTransfer.drug_stat_status.equals("1");
            boolean isUrgent = listTransfer.xray_urgent_status.equals("1") || listTransfer.lab_urgent_status.equals("1");
            if (isStat && isUrgent) {
                tm.setValueAt(ListTransfer.SHOW_ALL, i, 4);
            } else if (isStat) {
                tm.setValueAt(ListTransfer.SHOW_STAT, i, 4);
            } else if (isUrgent) {
                tm.setValueAt(ListTransfer.SHOW_URGENT, i, 4);
            }
            tm.setValueAt(listTransfer.range_age, i, 5);
            tm.setValueAt(listTransfer.hn, i, 6);
            tm.setValueAt(listTransfer.vn, i, 7);
            tm.setValueAt(name, i, 8);
            tm.setValueAt(listTransfer.assign_time, i, 9);
            tm.setValueAt(listTransfer.arrived_datetime, i, 10);
            tm.setValueAt(listTransfer.name, i, 11);
            if (!used_queue) {
                tm.setValueAt(newsPewsScore, i, 12);
                if (listTransfer.labstatus == null) {
                    listTransfer.labstatus = "0";
                }
                tm.setValueAt(listTransfer.labstatus, i, 13);
                tm.setValueAt(listTransfer.xraystatus, i, 14);
            } else {
                tm.setValueAt(newsPewsScore, i, 12);
                if (listTransfer.queue != null && !listTransfer.queue.isEmpty()) {
                    tm.setValueAt(new Integer(listTransfer.queue), i, 13);
                } else {
                    tm.setValueAt("", i, 13);
                }
                tm.setValueAt(listTransfer.color
                        + "|" + listTransfer.description
                        + "|" + listTransfer.queue,
                        i, 14);
                if (listTransfer.labstatus == null) {
                    listTransfer.labstatus = "0";
                }
                tm.setValueAt(listTransfer.labstatus, i, 15);
                tm.setValueAt(listTransfer.xraystatus, i, 16);
                tm.setEditingCol(13);
            }
        }
        if (checkvalue) {
            countpatient = vc.size();
        }

        jTableQueueOpd.setModel(tm);
        setTableListTransfer(true);
        setQuantityInQueue("�ӹǹ������㹨ش��ԡ��", jPanelCenter, vc);
    }

    /**
     * @Author: amp
     * @date : 06/03/2549
     * @see: �ʴ�����Ż
     */
    private void setListTransferLab(Vector vc) {
        TaBleModel tm;
        countrowTransaction = 0;
        if (vc == null || vc.isEmpty()) {
            tm = new TaBleModel(col_lab, 0);
        } else {
            tm = new TaBleModel(col_lab, vc.size());
            boolean checkvalue = true;
            for (int i = 0; i < vc.size(); i++) {
                countrowTransaction += 1;
                ListTransfer listTransfer = (ListTransfer) vc.get(i);
                if (listTransfer.queue == null) {
                    listTransfer.queue = "0";
                    listTransfer.color = "r=255,g=255,b=255";
                    listTransfer.description = ("");
                }
                if (listTransfer.arrived_status == null) {
                    listTransfer.arrived_status = "0";
                }
                boolean isStat = listTransfer.drug_stat_status.equals("1");
                boolean isUrgent = listTransfer.xray_urgent_status.equals("1") || listTransfer.lab_urgent_status.equals("1");
                if ("".equals(listTransfer.specimen_code)) {
                    String name = theHC.theLookupControl.getPatientName(listTransfer);

                    tm.setValueAt(countrowTransaction + "|" + listTransfer.arrived_status, i, 0);
                    tm.setValueAt(listTransfer.locking, i, 1);
                    tm.setValueAt(listTransfer.patient_allergy, i, 2);

                    tm.setValueAt(listTransfer.emergency_status, i, 3);
                    if (isStat && isUrgent) {
                        tm.setValueAt(ListTransfer.SHOW_ALL, i, 4);
                    } else if (isStat) {
                        tm.setValueAt(ListTransfer.SHOW_STAT, i, 4);
                    } else if (isUrgent) {
                        tm.setValueAt(ListTransfer.SHOW_URGENT, i, 4);
                    }
                    tm.setValueAt(listTransfer.range_age, i, 5);
                    tm.setValueAt(listTransfer.hn, i, 6);
                    tm.setValueAt(listTransfer.vn, i, 7);
                    tm.setValueAt(name, i, 8);
                    tm.setValueAt(listTransfer.assign_time, i, 9);
                    tm.setValueAt(listTransfer.arrived_datetime, i, 10);
                    if (listTransfer.queue != null && !listTransfer.queue.isEmpty()) {
                        tm.setValueAt(new Integer(listTransfer.queue), i, 11);
                    } else {
                        tm.setValueAt("", i, 11);
                    }
                    tm.setValueAt(listTransfer.color
                            + "|" + listTransfer.description
                            + "|" + listTransfer.queue, i, 12);
                    tm.setValueAt(listTransfer.labstatus, i, 13);
                    tm.setValueAt(listTransfer.specimen_code, i, 14);
                    tm.setValueAt(listTransfer.xraystatus, i, 15);
                } else {
                    tm.setValueAt(countrowTransaction + "|" + listTransfer.arrived_status, i, 0);
                    tm.setValueAt("********", i, 6);
                    tm.setValueAt("********", i, 7);
                    tm.setValueAt("*** ***** *****", i, 8);
                    if (listTransfer.queue != null && !listTransfer.queue.isEmpty()) {
                        tm.setValueAt(new Integer(listTransfer.queue), i, 11);
                    } else {
                        tm.setValueAt("", i, 11);
                    }
                    tm.setValueAt(listTransfer.color
                            + "|" + listTransfer.description
                            + "|" + listTransfer.queue, i, 12);
                    tm.setValueAt(listTransfer.labstatus, i, 13);
                    tm.setValueAt(listTransfer.specimen_code, i, 14);
                    tm.setValueAt(listTransfer.xraystatus, i, 15);
                }
            }
            if (checkvalue) {
                countpatient = vc.size();
            }
        }
        jTableQueueOpd.setModel(tm);
        setTableListTransfer(false);
        setQuantityInQueue("�ӹǹ��������ŧ���Ż", jPanelCenter, vc);
    }

    private void setQuantityInQueue(String queue_name, JPanel jp, Vector vc) {
        int iCountPatient = 0;
        if (vc != null) {
            iCountPatient = vc.size();
        }
        if (iCountPatient == 0) {
            jp.setBorder(new javax.swing.border.TitledBorder(null, Constant.getTextBundle("����ռ�����㹤��"), javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP));

        } else {
            jp.setBorder(new javax.swing.border.TitledBorder(null, Constant.getTextBundle(queue_name)
                    + "  " + iCountPatient + "  " + Constant.getTextBundle("��"), javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP));
        }
    }

    @Override
    public void notifyAdmitVisit(String str, int status) {
        refreshButton();
    }

    @Override
    public void notifyReadVisit(String str, int status) {
        if (!theHO.theEmployee.authentication_id.equals(Authentication.STAT)) {
            refreshButton();
        }
    }

    @Override
    public void notifyUnlockVisit(String str, int status) {
        if (!theHO.theEmployee.authentication_id.equals(Authentication.STAT)) {
            refreshButton();
        }
    }

    @Override
    public void notifyVisitPatient(String str, int status) {
//        Constant.println("PanelListTransfer__notifyVisitPatient");
        refreshButton();
    }

    @Override
    public void notifyDropVisit(String str, int status) {
        refreshButton();
    }

    @Override
    public void notifySendVisit(String str, int status) {
        refreshButton();
    }

    @Override
    public void notifyManageDrugAllergy(String str, int status) {
        refreshButton();
    }

    @Override
    public void notifyDischargeDoctor(String str, int status) {
        if (theHO.theEmployee.authentication_id.equals(Authentication.STAT)) {
            refreshButton();
        }
    }

    @Override
    public void notifyDischargeFinancial(String str, int status) {
        refreshButton();
    }

    @Override
    public void notifyReverseFinancial(String str, int status) {
        refreshButton();
    }

    @Override
    public void notifyReverseDoctor(String str, int status) {
        refreshButton();
    }

    @Override
    public void notifyCheckDoctorTreament(String str, int status) {
    }

    @Override
    public void notifyObservVisit(String str, int status) {
    }

    @Override
    public void notifyDeleteVisitPayment(String str, int status) {
    }

    @Override
    public void notifyDeletePatientPayment(String str, int status) {
    }

    @Override
    public void notifySavePatientPayment(String str, int status) {
    }

    @Override
    public void notifyCancelOrderItem(String str, int status) {
    }

    @Override
    public void notifyCheckAutoOrder(String str, int status) {
    }

    @Override
    public void notifyContinueOrderItem(String str, int status) {
    }

    @Override
    public void notifyDispenseOrderItem(String str, int status) {
        refreshButton();
    }

    @Override
    public void notifyDoctorOffDrug(String DoctorId, int status) {
    }

    @Override
    public void notifyExecuteOrderItem(String str, int status) {
        refreshButton();
    }

    @Override
    public void notifyReferOutLab(String msg, int status) {
    }

    @Override
    public void notifySaveOrderItem(String str, int status) {
    }

    @Override
    public void notifySaveOrderItemInLab(String str, int status) {
    }

    @Override
    public void notifyReceiveReturnDrug(String str, int status) {
    }

    @Override
    public void notifyVerifyOrderItem(String str, int status) {
        refreshButton();
    }

    @Override
    public void notifyDeletePatient(String str, int status) {
    }

    @Override
    public void notifyReadPatient(String str, int status) {
    }

    @Override
    public void notifyReadFamily(String str, int status) {
        refreshButton();
    }

    @Override
    public void notifySavePatient(String str, int status) {
    }

    @Override
    public void notifyRemainDoctorDischarge(String str, int status) {
        refreshButton();
    }

    @Override
    public void notifySendVisitBackWard(String str, int status) {
        refreshButton();
    }

    @Override
    public void notifySaveReturnDrug(String str, int status) {
    }

    @Override
    public void notifyDeleteFilmXray(String str, int status) {
    }

    @Override
    public void notifyDeleteLabOrder(String str, int status) {
    }

    @Override
    public void notifyDeleteLabResult(String str, int status) {
    }

    @Override
    public void notifyDeleteXrayPosition(String str, int status) {
    }

    @Override
    public void notifyManagePatientLabReferIn(String str, int status) {
    }

    @Override
    public void notifyReportResultLab(String str, int status) {
        refreshButton();
    }

    @Override
    public void notifySaveFilmXray(String str, int status) {
    }

    @Override
    public void notifySaveLabResult(String str, int status) {
        //refreshButton();
    }

    @Override
    public void notifySaveResultXray(String str, int status) {
    }

    @Override
    public void notifySaveXrayPosition(String str, int status) {
    }

    @Override
    public void notifyXrayReportComplete(String str, int status) {
        refreshButton();
    }

    @Override
    public void notifyDeleteResultXray(String str, int status) {
    }

    @Override
    public void notifySaveOrderRequest(String str, int status) {
    }

    @Override
    public void notifySaveAppointment(String str, int status) {
    }

    @Override
    public void notifyAddLabReferOut(String str, int status) {
    }

    @Override
    public void notifyAddLabReferIn(String str, int status) {
    }

    @Override
    public void notifyReverseAdmit(String str, int status) {
        refreshButton();
        theHC.theVisitControl.readVisitPatientByVid(theHO.theVisit.getObjectId());
    }

    @Override
    public void notifyResetPatient(String str, int status) {
        refreshButton();
    }

    @Override
    public void notifySaveRemainLabResult(String str, int status) {
        refreshButton();
    }

    @Override
    public void notifySendResultLab(String str, int status) {
        refreshButton();
    }

    @Override
    public void notifyDeleteQueueLab(String str, int status) {
        refreshButton();
    }

    @Override
    public void notifyAdmitLeaveHome(String str, int status) {
    }

    @Override
    public void notifySaveInformIntolerance(Object... object) {
    }
    private static final Logger LOG = Logger.getLogger(PanelListVisitByTransfer.class.getName());

    @Override
    public void componentResized(ComponentEvent e) {
    }

    @Override
    public void componentMoved(ComponentEvent e) {
    }

    @Override
    public void componentShown(ComponentEvent e) {
        theTimer.start();
        refreshButton();
//        if(!theTimer.isRunning()){
//            theTimer.start();
//        }
    }

    @Override
    public void componentHidden(ComponentEvent e) {
        theTimer.stop();
//        if(theTimer.isRunning()){
//            theTimer.stop();
//        }
    }

    protected void doDispenseAction() {
        int[] rows = this.jTableQueueDispense.getSelectedRows();
        if (rows.length == 0) {
            theUS.setStatus("�ѧ��������͡�����¨ҡ��Ǩ�����", UpdateStatus.WARNING);
            return;
        }
        if (theHC.theHP.aPanelOrder_Stock != null) {
            theHO.select_visit_dispense_stock = rows;
            theHO.theQueueDispense2V = theQueueDispense2V == null ? null : (Vector) theQueueDispense2V.clone();
            theHS.theOrderSubject.notifyDispenseOrderItem("�����觨�����¡�õ�Ǩ�ѡ�� �������", UpdateStatus.COMPLETE);
        } else {
            theHC.theOrderControl.dispenseOrderItems(theQueueDispense2V, rows);
        }
    }

    @Override
    public void notifyUpdatePatientPictureProfile(Object obj) {

    }

    @Override
    public void notifyUpdateQueue(String str, int status) {
        refreshButton();
    }
}

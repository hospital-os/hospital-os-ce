-- payment bank info
CREATE TABLE b_bank_info (
    b_bank_info_id    character varying(255)   NOT NULL,
    code              character varying(255)   NOT NULL,
    description       character varying(255)   NOT NULL,
    active            character varying(1)     NOT NULL,
    user_record       character varying(255),
    record_datetime   character varying(19),
    user_modify       character varying(255),
    modify_datetime   character varying(19),
CONSTRAINT b_bank_info_pkey PRIMARY KEY (b_bank_info_id)
);
CREATE UNIQUE INDEX b_bank_info_index1
   ON b_bank_info (code ASC NULLS LAST);

-- doctor fee
CREATE TABLE b_map_item_df (
    b_map_item_df_id    character varying(255)   NOT NULL,
    b_item_id              character varying(255)   NOT NULL,
CONSTRAINT b_map_item_df_pkey PRIMARY KEY (b_map_item_df_id)
);
CREATE UNIQUE INDEX b_map_item_df_index1
   ON b_map_item_df (b_item_id ASC NULLS LAST);

-- payment type
CREATE TABLE f_payment_type (
    f_payment_type_id    character varying(2)   NOT NULL,
    description         character varying(255)   NOT NULL,
    require_bank_info   character varying(1)   NOT NULL default '0',
    require_account_name   character varying(1)   NOT NULL default '0',
    require_account_no   character varying(1)   NOT NULL default '0',
CONSTRAINT f_payment_type_pkey PRIMARY KEY (f_payment_type_id)
);
CREATE UNIQUE INDEX f_payment_type_index1
   ON f_payment_type (description ASC NULLS LAST);

INSERT INTO f_payment_type VALUES ('1', 'เงินสด', '0', '0', '0');
INSERT INTO f_payment_type VALUES ('2', 'บัตรเครดิต', '1', '0', '1');
INSERT INTO f_payment_type VALUES ('3', 'เช็ค', '1', '0', '1');
INSERT INTO f_payment_type VALUES ('4', 'โอน', '1', '0', '1');

drop table f_billing_receipt_model;

ALTER TABLE t_billing_receipt RENAME f_billing_receipt_model_id  TO f_payment_type_id;

-- special discount 
-- CREATE TABLE t_billing_special_discount(
--  t_billing_special_discount_id  Character varying(255) NOT NULL,
--  t_billing_id                   Character varying(255) NOT NULL,
--  discount                       double precision NOT NULL default 0.0,
--  discount_cause                 text,
--  CONSTRAINT t_billing_special_discount_pkey PRIMARY KEY (t_billing_special_discount_id)
-- );
-- CREATE UNIQUE INDEX t_billing_special_discount_index1
--    ON t_billing_special_discount (t_billing_id ASC NULLS LAST);
ALTER TABLE t_billing ADD COLUMN special_discount double precision NOT NULL default 0.0;
ALTER TABLE t_billing ADD COLUMN special_discount_cause text;

-- discount by subgroup
-- CREATE TABLE t_billing_subgroup_discount(
--  t_billing_subgroup_discount_id  Character varying(255) NOT NULL,
--  t_billing_id                   Character varying(255) NOT NULL,
--  t_visit_id                     Character varying(255) NOT NULL,
--  t_billing_invoice_billing_subgroup_id   Character varying(255) NOT NULL,
--  discount_percentage            double precision NOT NULL default 0.0,
--  discount                       double precision NOT NULL default 0.0,
--  CONSTRAINT t_billing_subgroup_discount_pkey PRIMARY KEY (t_billing_subgroup_discount_id)
-- );
-- CREATE INDEX t_billing_subgroup_discount_index1
--    ON t_billing_subgroup_discount (t_billing_id ASC NULLS LAST);
-- CREATE INDEX t_billing_subgroup_discount_index2
--    ON t_billing_subgroup_discount (t_visit_id ASC NULLS LAST);
-- CREATE INDEX t_billing_subgroup_discount_index3
--    ON t_billing_subgroup_discount (t_billing_invoice_billing_subgroup_id ASC NULLS LAST);
ALTER TABLE t_billing_invoice_billing_subgroup ADD COLUMN discount_percentage double precision NOT NULL default 0.0;
ALTER TABLE t_billing_invoice_billing_subgroup ADD COLUMN discount double precision NOT NULL default 0.0;
ALTER TABLE t_billing_invoice_billing_subgroup ADD COLUMN final_patient_share double precision;

update t_billing_invoice_billing_subgroup set final_patient_share = billing_invoice_billing_subgroup_patient_share;

-- receipt
ALTER TABLE t_billing_receipt ADD COLUMN print_name_type character varying(1)   NOT NULL default '1';
ALTER TABLE t_billing_receipt ADD COLUMN print_name character varying(255)   NOT NULL default '';

-- setup ui
insert into f_gui_action (f_gui_action_id, gui_action_name) VALUES ('5704', 'ผู้ป่วยค้างชำระ');
insert into f_gui_action (f_gui_action_id, gui_action_name) VALUES ('5705', 'ธนาคาร');

-- fixed bug #146
update t_visit set service_location = '1' where service_location is null or service_location = '';

--Drop column in t_health_family_planing	  
ALTER TABLE t_health_family_planing DROP COLUMN health_family_planing_breast_exam;
ALTER TABLE t_health_family_planing DROP COLUMN health_family_planing_breast_exam_notice;
ALTER TABLE t_health_family_planing DROP COLUMN health_family_planing_cervix_exam;
ALTER TABLE t_health_family_planing DROP COLUMN health_family_planing_cervix_exam_notice;
ALTER TABLE t_health_family_planing DROP COLUMN health_family_planing_cervix_method;
ALTER TABLE t_health_cancer RENAME COLUMN health_cancer_pregnant_exam TO health_cancer_cervix_method;

-- insurance
CREATE TABLE b_finance_insurance_company (
    b_finance_insurance_company_id  character varying(255)   NOT NULL,
    code                            character varying(255)   NOT NULL,
    company_name                    character varying(255)   NOT NULL,
    company_address                 character varying(255)   NULL,
    company_telephone               character varying(255)   NULL,
    company_fax                     character varying(255)   NULL,
    alert_repay                     character varying(1)     NOT NULL,
    alert_repay_day                 character varying(255)   NULL,
    active                          character varying(1)     NOT NULL,
    record_datetime                 character varying(19)    NOT NULL,
    update_datetime                 character varying(19)    NULL,
    user_record_id                  character varying(255)   NOT NULL,
    user_update_id                  character varying(255)   NULL,
CONSTRAINT b_finance_insurance_company_pkey PRIMARY KEY (b_finance_insurance_company_id)
);

CREATE TABLE b_finance_insurance_discounts (
    b_finance_insurance_discounts_id    character varying(255)   NOT NULL,
    code                                character varying(255)   NOT NULL,
    description                         character varying(255)   NOT NULL,
    b_contract_plans_id                 character varying(255)   NOT NULL,
    b_finance_insurance_company_id      character varying(255)   NOT NULL,
    active                              character varying(1)     NOT NULL,
    record_datetime                     character varying(19)    NOT NULL,
    update_datetime                     character varying(19)    NULL,
    user_record_id                      character varying(255)   NOT NULL,
    user_update_id                      character varying(255)   NULL,
CONSTRAINT b_finance_insurance_discounts_pkey PRIMARY KEY (b_finance_insurance_discounts_id)
);


CREATE TABLE b_finance_insurance_discounts_condition  (
    b_finance_insurance_discounts_condition_id    character varying(255)   NOT NULL,
    b_finance_insurance_discounts_id              character varying(255)   NOT NULL,
    b_item_subgroup_id                            character varying(255)   NOT NULL,
    condition_adjustment                          character varying(255)   NOT NULL,
CONSTRAINT b_finance_insurance_discounts_condition_pkey PRIMARY KEY (b_finance_insurance_discounts_condition_id)
);

INSERT INTO b_sequence_data VALUES ('insurance', 'insurance number', 'A000000', '1', '1');

CREATE TABLE t_finance_code  (
    t_finance_code_id               character varying(255)   NOT NULL,
    t_payment_id                    character varying(255)   NOT NULL,
    claim_code                      character varying(255)   NOT NULL,
    b_finance_insurance_company_id  character varying(255)   NOT NULL,
    t_visit_id                      character varying(255)   NOT NULL,
    claim_code_datetime             character varying(19)    NOT NULL,
    status_insurance_approve        character varying(1)     NOT NULL,
    reason_not_approve              text                     NULL,
    record_datetime                 character varying(19)    NOT NULL,
    user_record_id                  character varying(255)   NOT NULL,
    alert_datetime                  character varying(19)    NULL,
    status_insurance_receive        character varying(1)     NOT NULL,
CONSTRAINT t_finance_code_pkey PRIMARY KEY (t_finance_code_id)
);

CREATE TABLE t_finance_invoice_subgroup  (
    t_finance_invoice_subgroup_id                character varying(255)   NOT NULL,
    t_finance_code_id                            character varying(255)   NOT NULL,
    t_patient_id                                 character varying(255)   NOT NULL,
    t_visit_id                                   character varying(255)   NOT NULL,
    b_item_subgroup_id                           character varying(255)   NOT NULL,
    t_payment_id                                 character varying(255)   NOT NULL,
    subgroup_total                               character varying(255)   NOT NULL,
    b_finance_insurance_discounts_id             character varying(255)   NOT NULL,
    insurance_discounts                          character varying(255)   NOT NULL,
    subgroup_draw                                character varying(255)   NOT NULL,
    subgroup_remain                              character varying(255)   NOT NULL,
    record_datetime                              character varying(19)    NOT NULL,
    user_record_id                               character varying(255)   NOT NULL,
CONSTRAINT t_finance_invoice_subgroup_pkey PRIMARY KEY (t_finance_invoice_subgroup_id)
);


CREATE TABLE t_finance_paid_life_insurance  (
    t_finance_paid_life_insurance_id                character varying(255)   NOT NULL,
    b_finance_insurance_company_id                  character varying(255)   NOT NULL,
    b_contract_plans_id                             character varying(255)   NOT NULL,
    t_visit_id                                      character varying(255)   NOT NULL,
    outstanding                                     character varying(255)   NOT NULL,
    t_finance_code_id                               character varying(255)   NOT NULL,
    withdraw                                        character varying(255)   NOT NULL,
    record_datetime                                 character varying(19)    NOT NULL,
    user_record_id                                  character varying(255)   NOT NULL,
 CONSTRAINT t_finance_paid_life_insurance_pkey PRIMARY KEY (t_finance_paid_life_insurance_id)
);


CREATE TABLE t_finance_paid_life_insurance_bill  (
    t_finance_paid_life_insurance_bill_id                character varying(255)   NOT NULL,
    b_finance_insurance_company_id                       character varying(255)   NOT NULL,
    b_contract_plans_id                                  character varying(255)   NOT NULL,
    balance                                              character varying(255)   NOT NULL,
    taxes                                                character varying(255)   NOT NULL,
    paid                                                 character varying(255)   NOT NULL,
    vat                                                  character varying(255)   NOT NULL,
    branch_name                                          character varying(19)    NOT NULL,
    account_number                                       character varying(255)   NOT NULL,
    payment_datetime                                     character varying(19)    NOT NULL,
    f_payment_type_id                                    character varying(1)     NOT NULL,
    b_bank_info_id                                       character varying(255)   NOT NULL, 
    record_datetime                                      character varying(19)    NOT NULL,
    user_record_id                                       character varying(255)   NOT NULL,
CONSTRAINT t_finance_paid_life_insurance_bill_pkey PRIMARY KEY (t_finance_paid_life_insurance_bill_id)
);

CREATE TABLE t_finance_paid_life_insurance_receipt  (
    t_finance_paid_life_insurance_receipt_id               character varying(255)   NOT NULL,
    t_finance_paid_life_insurance_id                       character varying(255)   NOT NULL,
    t_finance_paid_life_insurance_bill_id                  character varying(255)   NOT NULL,
CONSTRAINT t_finance_paid_life_insurance_receipt_pkey PRIMARY KEY (t_finance_paid_life_insurance_receipt_id)
);

--fixed bug f_comservice
update t_health_community_service set  f_comservice_id = (case when f_comservice_id = '000037' then '000032'
        when f_comservice_id = '000038' then '000033'
        when f_comservice_id = '000039' then '000034'
        when f_comservice_id = '000040' then '000035'
        when f_comservice_id = '000041' then '000036'   
end)

where
f_comservice_id in ('000037','000038','000039','000040','000041');

delete from f_comservice where f_comservice_id in ('000037','000038','000039','000040','000041');

-- setup
insert into f_gui_action (f_gui_action_id, gui_action_name) VALUES ('6000', 'ประกันชีวิต');
insert into f_gui_action (f_gui_action_id, gui_action_name) VALUES ('6001', 'รายการบริษัทประกัน');
insert into f_gui_action (f_gui_action_id, gui_action_name) VALUES ('6002', 'รายการส่วนลดประกัน');

-- update db version
INSERT INTO s_version VALUES ('9701000000065', '65', 'Hospital OS, Community Edition', '3.9.32', '3.21.240413', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_32.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.32');
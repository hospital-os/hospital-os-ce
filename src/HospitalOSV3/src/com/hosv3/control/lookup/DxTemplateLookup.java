/*
 * VitalTemplateLookup.java
 *
 * Created on 20 �ѹ��¹ 2548, 9:48 �.
 */
package com.hosv3.control.lookup;

import com.hospital_os.object.DxTemplate;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.LookupControlInf;
import com.hosv3.control.LookupControl;
import com.hosv3.control.VitalControl;
import com.hosv3.utility.connection.ExecuteControlInf;

/**
 *
 * @not deprecated because use henbe package and bad read data
 *
 * @author kingland
 */
public class DxTemplateLookup implements LookupControlInf, ExecuteControlInf {

    boolean is_lookup = true;
    private LookupControl theLookup;
    private VitalControl theEC;

    /**
     * Creates a new instance of VitalTemplateLookup
     */
    public DxTemplateLookup(LookupControl lookup) {
        theLookup = lookup;
    }

    public DxTemplateLookup(VitalControl lookup) {
        is_lookup = false;
        theEC = lookup;
    }

    public DxTemplateLookup(LookupControl lookup, VitalControl econ) {
        is_lookup = false;
        theLookup = lookup;
        theEC = econ;
    }

    @Override
    @SuppressWarnings("UseOfObsoleteCollectionType")
    public java.util.Vector listData(String str) {
        if (str == null || str.equals("") || str.equals("%")) {
            return null;
        } else {
            return theLookup.listDxTemplateByName(str);
        }
    }

    @Override
    public CommonInf readHosData(String str) {
        return null;
    }

    @Override
    public boolean execute(Object str) {
        theEC.addDxTemplate((DxTemplate) str);
        return true;
    }
}

package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

@SuppressWarnings("ClassWithoutLogger")
public class DrugInstruction extends Persistent {

    private static final long serialVersionUID = 1L;

    public String drug_instruction_id;
    public String description;
    public String active;
    public String edqm_route_code;

    /**
     * @roseuid 3F658BBB036E
     */
    public DrugInstruction() {
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.utility;

import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hospital_os.utility.LongTask;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * <b>Title:</b> Splash<br> <b>Description:</b><blockquote> Splash screen for
 * program hospital-os project</blockquote> <b>Copyright:</b> Copyright (c)
 * 2001<br> <b>Company:</b> 4th Tier<br>
 *
 * @author Surachai Thowong, 13/01/2002
 * @version 1.0
 * @author henbe
 */
public class Splash extends JFrame {

    private static final Logger LOG = Logger.getLogger(Splash.class.getName());
    public static final long serialVersionUID = 0;
    private BorderLayout borderLayout1 = new BorderLayout();
    private JLabel jLabelSplash = new JLabel();
    private PanelImage thePI = new PanelImage();

    /**
     * @roseuid 3C328572015E
     */
    public Splash() {
        try {
            init();
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    public void showDialog(UpdateStatus theUS) {
        JDialog jd = new JDialog(theUS.getJFrame(), true);
        jd.getContentPane().add(thePI);
        jd.setSize(500, 300);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation((screenSize.width - getSize().width) / 2, (screenSize.height - getSize().height) / 2);
        jd.setVisible(true);
    }

    private void init() throws Exception {
        this.setUndecorated(true);
        this.getContentPane().setLayout(borderLayout1);
        this.getContentPane().add(thePI, BorderLayout.CENTER);
        jLabelSplash.setToolTipText(Constant.getTextBundle("��س����ѡ����"));
        thePI.setLayout(new java.awt.GridBagLayout());
        java.awt.GridBagConstraints gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 10, 10);
        thePI.add(jLabelSplash, gridBagConstraints);
        /////////
        javax.swing.JProgressBar progressBar;
        LongTask task;
        task = new LongTask();
        progressBar = new javax.swing.JProgressBar(0, task.getLengthOfTask());
        progressBar.setValue(0);
        progressBar.setIndeterminate(true);
        task.go();
        progressBar.setMaximumSize(new java.awt.Dimension(5, 7));
        progressBar.setPreferredSize(new java.awt.Dimension(220, 7));
        progressBar.setStringPainted(true); //get space for the string
        progressBar.setString("");          //but don't paint it
        progressBar.setForeground(new Color(255, 153, 51));
        progressBar.setBackground(new Color(255, 255, 255));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        thePI.add(progressBar, gridBagConstraints);
        setSize(500, 300);
        //Center the window
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public static String getNews(ConnectionInf con) {
        try {
            con.open();
            ResultSet rs = con.eQuery("select text from b_site_news order by date_time desc limit 1");
            if (rs.next()) {
                return rs.getString(1);
            }
            return "<html><body><h2>���й� �����������ʶҹ��Һ��</h2></body></html>";
        } catch (Exception ex) {
            return "<html><body><h2>���й� �����������ʶҹ��Һ��</h2></body></html>";
        } finally {
            con.close();
        }
    }

    public void setText(String str) {
        this.jLabelSplash.setText(str);
    }

    public void showNews(ConnectionInf con_inf) {
        String news = Splash.getNews(con_inf);
        if (news != null && !news.isEmpty()) {
            this.thePI.setImage(Config.SPLASH_FILE_BG);
            this.setText(news);
        }
    }

    public JPanel getPanel() {
        return this.thePI;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author tanakrit
 */
public class NewsPewsNotifyDB {

    public ConnectionInf theConnectionInf;
    final public String idtable = "983";

    public NewsPewsNotifyDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public int delete(String vn, String serviceId) throws Exception {
        String sql = "UPDATE t_newspews_notify \n"
                + "      SET status = 'CN' \n"
                + "    where vn=? and service_id=? ";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, vn);
            ePQuery.setString(index++, serviceId);
            return ePQuery.executeUpdate();
        }
    }

    public int deleteAll(String vn) throws Exception {
        String sql = "UPDATE t_newspews_notify \n"
                + "      SET status = 'CN' \n"
                + "    where vn=?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, vn);
            return ePQuery.executeUpdate();
        }
    }

    public String calculate_newpewsType(String patientId) throws Exception {
        String sql = "select calculate_newspews_type(calculate_agegroup(?)) as newspews_type";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, patientId);
            String type = "";
            try (ResultSet rs = ePQuery.executeQuery()) {
                while (rs.next()) {
                    type = rs.getString("newspews_type");
                }
            }
            return type;
        }
    }
}

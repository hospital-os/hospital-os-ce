/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.VisitServiceType;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class VisitServiceTypeDB {

    private final ConnectionInf connectionInf;

    public VisitServiceTypeDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public List<VisitServiceType> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_visit_service_type";
            preparedStatement = connectionInf.ePQuery(sql);
            
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<VisitServiceType> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<VisitServiceType> list = new ArrayList<VisitServiceType>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                VisitServiceType obj = new VisitServiceType();
                obj.setObjectId(rs.getString("f_visit_service_type_id"));
                obj.description = rs.getString("visit_service_type_description");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (VisitServiceType obj : list()) {
            list.add((CommonInf) obj);            
        }
        return list;
    }
}

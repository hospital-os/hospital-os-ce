-- ใบพิมพ์
CREATE TABLE b_printing (
    b_printing_id    character varying(255)   NOT NULL,
    description      character varying(255)   NOT NULL,
    default_jrxml    character varying(255)   NOT NULL,
    enable           character varying(1)   NOT NULL default '1',
    enable_other_language character varying(1)   NOT NULL default '0',
CONSTRAINT b_printing_pkey PRIMARY KEY (b_printing_id)
);

insert into b_printing (b_printing_id, description, default_jrxml, enable, enable_other_language) VALUES ('1', 'ใบพิมพ์ OPD Card', 'OPD_Card_con.jrxml', '0', '0');
insert into b_printing (b_printing_id, description, default_jrxml, enable, enable_other_language) VALUES ('2', 'ใบพิมพ์ใบเสร็จ', 'receipt_con.jrxml', '0', '0');
insert into b_printing (b_printing_id, description, default_jrxml, enable, enable_other_language) VALUES ('3', 'ใบพิมพ์ใบนัด', 'appointment_con.jrxml', '0', '0');
insert into b_printing (b_printing_id, description, default_jrxml, enable, enable_other_language) VALUES ('4', 'ใบพิมพ์ Index Xray', 'x_ray_card_con.jrxml', '0', '0');
insert into b_printing (b_printing_id, description, default_jrxml, enable, enable_other_language) VALUES ('5', 'ใบพิมพ์ใบสั่งยา', 'drugRx_con.jrxml', '0', '0');
insert into b_printing (b_printing_id, description, default_jrxml, enable, enable_other_language) VALUES ('6', 'ใบพิมพ์สติ๊กเกอร์ยา', 'Drug_Sticker_con.jrxml', '0', '0');
insert into b_printing (b_printing_id, description, default_jrxml, enable, enable_other_language) VALUES ('7', 'ใบพิมพ์ผลแลป', 'resultLab_con.jrxml', '0', '0');
insert into b_printing (b_printing_id, description, default_jrxml, enable, enable_other_language) VALUES ('8', 'ใบพิมพ์ใบ Refer', 'refer_con.jrxml', '0', '0');

update b_printing set enable = b_option_detail.option_detail_name
from b_option_detail where b_option_detail.b_option_detail_id = 'print_opdcard_con' and b_printing.b_printing_id = '1';

update b_printing set enable = b_option_detail.option_detail_name
from b_option_detail where b_option_detail.b_option_detail_id = 'print_receipt_con' and b_printing.b_printing_id = '2';

update b_printing set enable = b_option_detail.option_detail_name
from b_option_detail where b_option_detail.b_option_detail_id = 'print_appoint_con' and b_printing.b_printing_id = '3';

update b_printing set enable = b_option_detail.option_detail_name
from b_option_detail where b_option_detail.b_option_detail_id = 'print_xraycard_con' and b_printing.b_printing_id = '4';

update b_printing set enable = b_option_detail.option_detail_name
from b_option_detail where b_option_detail.b_option_detail_id = 'print_drugrx_con' and b_printing.b_printing_id = '5';

update b_printing set enable = b_option_detail.option_detail_name
from b_option_detail where b_option_detail.b_option_detail_id = 'print_sticker_con' and b_printing.b_printing_id = '6';

update b_printing set enable = b_option_detail.option_detail_name
from b_option_detail where b_option_detail.b_option_detail_id = 'print_result_lab_con' and b_printing.b_printing_id = '7';

update b_printing set enable = b_option_detail.option_detail_name
from b_option_detail where b_option_detail.b_option_detail_id = 'print_refer_con' and b_printing.b_printing_id = '8';

CREATE TABLE b_language (
    b_language_id    character varying(255)   NOT NULL,
    description           character varying(255)   NOT NULL,
    active              character varying(1)   NOT NULL default '1',
CONSTRAINT b_language_pkey PRIMARY KEY (b_language_id)
);

CREATE TABLE b_language_mapping (
    b_language_mapping_id character varying(255)   NOT NULL,
    b_language_id         character varying(255)   NOT NULL,
    f_patient_nation_id   character varying(255)   NOT NULL,
CONSTRAINT b_language_mapping_pkey PRIMARY KEY (b_language_mapping_id)
);
CREATE INDEX b_language_mapping_index1
   ON b_language_mapping (b_language_id ASC NULLS LAST);
CREATE INDEX b_language_mapping_index2
   ON b_language_mapping (f_patient_nation_id ASC NULLS LAST);

CREATE TABLE b_printing_other_language (
    b_printing_other_language_id    character varying(255)   NOT NULL,
    b_printing_id           character varying(255)   NOT NULL,
    b_language_id           character varying(255)   NOT NULL,
    jrxml                   character varying(255)   NOT NULL,
CONSTRAINT b_printing_other_language_pkey PRIMARY KEY (b_printing_other_language_id)
);
CREATE INDEX b_printing_other_language_index1
   ON b_printing_other_language (b_printing_id ASC NULLS LAST);

-- b_item_drug
CREATE TABLE b_item_drug_other_language (
    b_item_drug_other_language_id    character varying(255)   NOT NULL,
    b_item_drug_id           character varying(255)   NOT NULL,
    b_language_id           character varying(255)   NOT NULL,
    caution                 text,
    description             text,
CONSTRAINT b_item_drug_other_language_pkey PRIMARY KEY (b_item_drug_other_language_id)
);
CREATE INDEX b_item_drug_other_language_index1
   ON b_item_drug_other_language (b_item_drug_id ASC NULLS LAST);

--b_item_drug_frequency
CREATE TABLE b_item_drug_frequency_other_language (
    b_item_drug_frequency_other_language_id    character varying(255)   NOT NULL,
    b_item_drug_frequency_id           character varying(255)   NOT NULL,
    b_language_id           character varying(255)   NOT NULL,
    description             text,
CONSTRAINT b_item_drug_frequency_other_language_pkey PRIMARY KEY (b_item_drug_frequency_other_language_id)
);
CREATE INDEX b_item_drug_frequency_other_language_index1
   ON b_item_drug_frequency_other_language (b_item_drug_frequency_id ASC NULLS LAST);

--b_item_drug_instruction
CREATE TABLE b_item_drug_instruction_other_language (
    b_item_drug_instruction_other_language_id    character varying(255)   NOT NULL,
    b_item_drug_instruction_id           character varying(255)   NOT NULL,
    b_language_id           character varying(255)   NOT NULL,
    description             text,
CONSTRAINT b_item_drug_instruction_other_language_pkey PRIMARY KEY (b_item_drug_instruction_other_language_id)
);
CREATE INDEX b_item_drug_instruction_other_language_index1
   ON b_item_drug_instruction_other_language (b_item_drug_instruction_id ASC NULLS LAST);

--b_item_drug_uom
CREATE TABLE b_item_drug_uom_other_language (
    b_item_drug_uom_other_language_id    character varying(255)   NOT NULL,
    b_item_drug_uom_id           character varying(255)   NOT NULL,
    b_language_id           character varying(255)   NOT NULL,
    description             text,
CONSTRAINT b_item_drug_uom_other_language_pkey PRIMARY KEY (b_item_drug_uom_other_language_id)
);
CREATE INDEX b_item_drug_uom_other_language_index1
   ON b_item_drug_uom_other_language (b_item_drug_uom_id ASC NULLS LAST);

insert into f_gui_action
      (f_gui_action_id, gui_action_name) VALUES ('5104', 'ตั้งค่าใบพิมพ์แบบ Query');

insert into f_gui_action
      (f_gui_action_id, gui_action_name) VALUES ('5105', 'ตั้งค่าภาษา');

-- update db version
INSERT INTO s_version VALUES ('9701000000068', '68', 'Hospital OS, Community Edition', '3.9.35', '3.22.141013', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_35.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.35');
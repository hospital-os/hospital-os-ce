ALTER TYPE public.authen_froms ADD VALUE 'web';
ALTER TYPE public.authen_froms ADD VALUE 'manual';

ALTER TABLE public.t_nhso_authencode ADD delete_date_time timestamp with time zone NULL DEFAULT null;
ALTER TABLE public.t_nhso_authencode ADD user_delete_id varchar NULL;

ALTER TABLE public.t_nhso_authencode ALTER COLUMN mobile DROP NOT NULL;
ALTER TABLE public.t_nhso_authencode ALTER COLUMN correlation_id DROP NOT NULL;
ALTER TABLE public.t_nhso_authencode ALTER COLUMN created_date DROP NOT NULL;

ALTER TABLE public.t_nhso_authencode ADD CONSTRAINT t_nhso_authencode_un UNIQUE (claim_type,claim_code);

INSERT INTO s_nhso_authencode_version (version_app, version_db, description)VALUES ('1.1.0', '1.0.1', 'NHSO Authencode Module');

INSERT INTO s_script_update_log values ('NHSO_Authencode_Module','update_authen_code_002.sql',(select current_date) || ','|| (select current_time),'Initialize NHSO Authencode Module');
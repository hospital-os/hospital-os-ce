/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.Audiometry;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class AudiometryDB {

    public ConnectionInf theConnectionInf;
    final public String idtable = "998";

    public AudiometryDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public int insert(Audiometry o) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "INSERT INTO t_audiometry("
                    + "t_audiometry_id, t_visit_id, left_result, right_result, "
                    + "user_test_id, active, user_record_id, user_update_id)"
                    + "VALUES (?, ?, ?, ?, "
                    + "?, ?, ?, ?)";
            preparedStatement = theConnectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, o.getGenID(idtable));
            preparedStatement.setString(index++, o.t_visit_id);
            preparedStatement.setString(index++, o.left_result);
            preparedStatement.setString(index++, o.right_result);
            preparedStatement.setString(index++, o.user_test_id);
            preparedStatement.setString(index++, o.active);
            preparedStatement.setString(index++, o.user_record_id);
            preparedStatement.setString(index++, o.user_update_id);

            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(Audiometry o) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "UPDATE t_audiometry "
                    + "SET left_result=?, right_result=?,  "
                    + "user_test_id=?, update_date_time=current_timestamp,user_update_id=? "
                    + "WHERE t_audiometry_id=?";
            preparedStatement = theConnectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, o.left_result);
            preparedStatement.setString(index++, o.right_result);
            preparedStatement.setString(index++, o.user_test_id);
            preparedStatement.setString(index++, o.user_update_id);
            preparedStatement.setString(index++, o.getObjectId());

            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }

    }

    public int delete(Audiometry o) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "UPDATE t_audiometry "
                    + "SET active='0',update_date_time=current_timestamp,user_update_id=? "
                    + "WHERE t_audiometry_id=?";
            preparedStatement = theConnectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, o.user_update_id);
            preparedStatement.setString(2, o.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public Audiometry findByVisitId(String visitId) throws Exception {
        String sql = "select * from t_audiometry where t_visit_id = ? and active = '1'";

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = theConnectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, visitId);
            List<Audiometry> list = eQuery(preparedStatement.toString());
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    private List<Audiometry> eQuery(String sql) throws Exception {
        List<Audiometry> list = new ArrayList<Audiometry>();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            Audiometry o = new Audiometry();
            o.setObjectId(rs.getString("t_audiometry_id"));
            o.t_visit_id = rs.getString("t_visit_id");
            o.left_result = rs.getString("left_result");
            o.right_result = rs.getString("right_result");
            o.user_test_id = rs.getString("user_test_id");
            o.active = rs.getString("active");
            o.record_date_time = rs.getTimestamp("record_date_time");
            o.user_record_id = rs.getString("user_record_id");
            o.update_date_time = rs.getTimestamp("update_date_time");
            o.user_update_id = rs.getString("user_update_id");
            list.add(o);
        }
        rs.close();
        return list;
    }
}

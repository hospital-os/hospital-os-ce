/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.PatientPastVaccine;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.Gutil;
import java.sql.ResultSet;
import java.util.Vector;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class PatientPastVaccineDB {

    private final ConnectionInf theConnectionInf;
    private final String tableId = "994";

    public PatientPastVaccineDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public int insert(PatientPastVaccine obj) throws Exception {
        String sql = "INSERT INTO t_patient_past_vaccine( "
                + "t_patient_past_vaccine_id, t_patient_id, patient_past_vaccine_complete,  "
                + "patient_past_vaccine_no_comlete_name, user_record, record_date_time,  "
                + "user_modify, modify_date_time, active) "
                + "VALUES ('%s', '%s', '%s',  "
                + "'%s', '%s', '%s',  "
                + "'%s', '%s', '%s')";
        sql = String.format(sql, obj.getGenID(tableId), obj.t_patient_id, obj.patient_past_vaccine_complete,
                Gutil.CheckReservedWords(obj.patient_past_vaccine_no_comlete_name), obj.user_record,
                obj.record_date_time, obj.user_modify, obj.modify_date_time, "1");
        return theConnectionInf.eUpdate(sql);
    }

    public int update(PatientPastVaccine obj) throws Exception {
        String sql = "UPDATE t_patient_past_vaccine "
                + "SET patient_past_vaccine_complete='%s',  "
                + "patient_past_vaccine_no_comlete_name='%s',  "
                + "user_modify='%s', modify_date_time='%s' "
                + "WHERE t_patient_past_vaccine_id='%s' ";
        sql = String.format(sql, obj.patient_past_vaccine_complete,
                Gutil.CheckReservedWords(obj.patient_past_vaccine_no_comlete_name),
                obj.user_modify, obj.modify_date_time, obj.getObjectId());
        return theConnectionInf.eUpdate(sql);
    }

    public PatientPastVaccine selectById(String id) throws Exception {
        String sql = "select * from t_patient_past_vaccine where t_patient_past_vaccine_id = '%s'";
        Vector eQuery = eQuery(String.format(sql, id));
        return (PatientPastVaccine) (eQuery.isEmpty() ? null : eQuery.get(0));
    }

    public PatientPastVaccine selectByPatientId(String patient_id) throws Exception {
        String sql = "select * from t_patient_past_vaccine where t_patient_id = '%s'";
        Vector eQuery = eQuery(String.format(sql, patient_id));
        return (PatientPastVaccine) (eQuery.isEmpty() ? null : eQuery.get(0));
    }

    public Vector eQuery(String sql) throws Exception {
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        while (rs.next()) {
            PatientPastVaccine p = new PatientPastVaccine();
            p.setObjectId(rs.getString("t_patient_past_vaccine_id"));
            p.t_patient_id = rs.getString("t_patient_id");
            p.patient_past_vaccine_complete = rs.getString("patient_past_vaccine_complete");
            p.patient_past_vaccine_no_comlete_name = rs.getString("patient_past_vaccine_no_comlete_name");
            p.user_record = rs.getString("user_record");
            p.record_date_time = rs.getString("record_date_time");
            p.user_modify = rs.getString("user_modify");
            p.modify_date_time = rs.getString("modify_date_time");
            p.active = rs.getString("active");
            list.add(p);
        }
        rs.close();
        return list;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.objdb.objdbclass;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.pcu.object.FFunctionalDependent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sompr
 */
public class FFunctionalDependentDB {

    private final ConnectionInf connectionInf;

    public FFunctionalDependentDB(ConnectionInf db) {
        connectionInf = db;
    }

    public FFunctionalDependent select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_functional_dependent where f_functional_dependent_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<FFunctionalDependent> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<FFunctionalDependent> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_functional_dependent order by f_functional_dependent_id";
            preparedStatement = connectionInf.ePQuery(sql);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<FFunctionalDependent> list(String... ids) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_functional_dependent where f_functional_dependent_id in (?) order by f_functional_dependent_id";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setArray(1, connectionInf.getConnection().createArrayOf("varchar", ids));
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<FFunctionalDependent> listByKeyword(String keyword) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_functional_dependent where description ilike ? order by description";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, "%" + (keyword == null || keyword.isEmpty() ? "" : keyword) + "%");
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<FFunctionalDependent> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<FFunctionalDependent> list = new ArrayList<FFunctionalDependent>();
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()) {
            FFunctionalDependent obj = new FFunctionalDependent();
            obj.setObjectId(rs.getString("f_functional_dependent_id"));
            obj.description = rs.getString("description");
            list.add(obj);
        }
        rs.close();
        return list;
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (FFunctionalDependent obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }

    public List<CommonInf> getComboboxDatasource(String... ids) throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (FFunctionalDependent obj : list(ids)) {
            list.add((CommonInf) obj);
        }
        return list;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.objdb.objdbclass;

import com.hospital_os.usecase.connection.ConnectionInf;
import com.pcu.object.DisabilityEquipment;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class DisabilityEquipmentDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "811";
//    private final BDisabilityEquipmentDB equipmentDB;

    public DisabilityEquipmentDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
//        equipmentDB = new BDisabilityEquipmentDB(connectionInf);
    }

    public int insert(DisabilityEquipment obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO t_health_disability_equipment(\n"
                    + "            t_health_disability_equipment_id, t_health_maim_id, b_health_disability_equipment_id, \n"
                    + "            active, user_record_id, record_date_time)\n"
                    + "    VALUES (?, ?, ?, \n"
                    + "            ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getGenID(tableId));
            preparedStatement.setString(2, obj.t_health_maim_id);
            preparedStatement.setString(3, obj.b_health_disability_equipment_id);
            preparedStatement.setString(4, obj.active);
            preparedStatement.setString(5, obj.user_record_id);
            preparedStatement.setString(6, obj.record_date_time);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(DisabilityEquipment obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_health_disability_equipment\n"
                    + "   SET t_health_maim_id=?, b_health_disability_equipment_id=?, \n"
                    + "       active=?\n");
            sql.append(" WHERE t_health_disability_equipment_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.t_health_maim_id);
            preparedStatement.setString(2, obj.b_health_disability_equipment_id);
            preparedStatement.setString(3, obj.active);
            preparedStatement.setString(4, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int inactive(DisabilityEquipment obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_health_disability_equipment\n");
            sql.append("   SET active=?, user_cancel_id=?, cancel_date_time=?\n");
            sql.append(" WHERE t_health_disability_equipment_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, "0");
            preparedStatement.setString(2, obj.user_cancel_id);
            preparedStatement.setString(3, obj.cancel_date_time);
            preparedStatement.setString(4, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public DisabilityEquipment select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from t_health_disability_equipment where t_health_disability_equipment_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<DisabilityEquipment> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<DisabilityEquipment> listByMaimId(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from t_health_disability_equipment where t_health_maim_id = ? and active = '1'";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<DisabilityEquipment> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<DisabilityEquipment> list = new ArrayList<DisabilityEquipment>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                DisabilityEquipment obj = new DisabilityEquipment();
                obj.setObjectId(rs.getString("t_health_disability_equipment_id"));
                obj.t_health_maim_id = rs.getString("t_health_maim_id");
                obj.b_health_disability_equipment_id = rs.getString("b_health_disability_equipment_id");
                obj.active = rs.getString("active");
                obj.user_record_id = rs.getString("user_record_id");
                obj.record_date_time = rs.getString("record_date_time");
                obj.user_cancel_id = rs.getString("user_cancel_id");
                obj.cancel_date_time = rs.getString("cancel_date_time");
//                try {
//                    obj.equipment = equipmentDB.select(obj.b_health_disability_equipment_id);
//                } catch (Exception ex) {
//                }
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }
}
CREATE TABLE b_drugfund_receipt_sequence (
b_drugfund_receipt_sequence_id varchar(255) NOT NULL
, drugfund_receipt_sequence_ip_address varchar(255)
, drugfund_receipt_sequence_pattern varchar(255)
, drugfund_receipt_sequence_day varchar(255)
, drugfund_receipt_sequence_month varchar(255)
, drugfund_receipt_sequence_service_point_seq varchar(255)
, drugfund_receipt_sequence_service_point varchar(255) DEFAULT '0'::character varying 
, drugfund_receipt_sequence_book_no varchar(255) DEFAULT '0'::character varying 
, drugfund_receipt_sequence_begin_no varchar(255) DEFAULT '0'::character varying 
, drugfund_receipt_sequence_end_no varchar(255) DEFAULT '0'::character varying 
, drugfund_receipt_sequence_receipt_qty varchar(255) DEFAULT '0'::character varying 
, drugfund_receipt_sequence_value varchar(255) DEFAULT '0'::character varying 
, drugfund_receipt_sequence_active varchar(255) DEFAULT '0'::character varying 
, PRIMARY KEY (b_drugfund_receipt_sequence_id)
);

ALTER TABLE t_drugfund_billing_receipt_item ADD COLUMN drugfund_billing_receipt_book_no character varying(255) DEFAULT '';

ALTER TABLE t_drugfund_billing_receipt_item ADD COLUMN drugfund_billing_receipt_begin_no character varying(255) DEFAULT '';

CREATE TABLE public.s_drugfund_version ( 
    s_drugfund_version_id              	varchar(255) NOT NULL,
    version_drugfund_number            	varchar(255) NULL,
    version_drugfund_description       	varchar(255) NULL,
    version_drugfund_application_number	varchar(255) NULL,
    version_drugfund_database_number   	varchar(255) NULL,
    version_drugfund_update_time       	varchar(255) NULL 
);

INSERT INTO s_drugfund_version VALUES ('9750000000001', '1', 'Drugfund, Community Edition', '1.00.110511', '1.00.110511', '2554-05-11 16:30:00');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.ProviderType;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class ProviderTypeDB {

    private final ConnectionInf connectionInf;

    public ProviderTypeDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public List<ProviderType> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_provider_type";
            preparedStatement = connectionInf.ePQuery(sql);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<ProviderType> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<ProviderType> list = new ArrayList<ProviderType>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                ProviderType obj = new ProviderType();
                obj.setObjectId(rs.getString("f_provider_type_id"));
                obj.code = rs.getString("code");
                obj.description = rs.getString("description");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (ProviderType obj : list()) {
            list.add((CommonInf) obj);            
        }
        return list;
    }
}

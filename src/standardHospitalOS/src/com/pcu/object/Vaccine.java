/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.object;

import com.hospital_os.usecase.connection.Persistent;
import java.util.Date;

/**
 *
 * @author tanakrit
 */
public class Vaccine extends Persistent {

    public String barcode;
    public String vaccine_name;
    public String serial_number;
    public String lot_number;
    public String f_vaccine_manufacturer_id;
    public Date receive_date;
    public Date expire_date;
    public int qty;
    public int dose;
    public int bottle_number;
    public int dose_number;
    public String b_health_epi_group_id;
    public String use_status = "1";
    public String active = "1";
    public String user_record_id;
    public Date record_date_time;
    public String user_cancel_id;
    public Date cancel_date_time;
}

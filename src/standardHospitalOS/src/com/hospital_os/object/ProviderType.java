/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class ProviderType extends Persistent implements CommonInf {

    public String code;
    public String description;

    @Override
    public String getCode() {
        return getObjectId();
    }

    @Override
    public String getName() {
        return description;
    }

    @Override
    public String toString() {
        return description;
    }

    public static final String PHYSICIAN_ID = "000001";
    public static final String DENTIST_ID = "000002";
    public static final String NURSE_ID = "000003";
    public static final String PHARMACIST_ID = "000016";
    public static final String PHYSIOTHERAPIST_ID = "000017";
}

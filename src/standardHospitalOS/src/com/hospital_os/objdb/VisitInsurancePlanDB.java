/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.VisitInsurancePlan;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class VisitInsurancePlanDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "818";

    public VisitInsurancePlanDB(ConnectionInf c) {
        this.connectionInf = c;
    }

    public int insert(VisitInsurancePlan obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO t_visit_insurance_plan(\n"
                    + "            t_visit_insurance_plan_id, t_visit_payment_id, b_finance_insurance_company_id, b_finance_insurance_plan_id, priority, active, user_record_id, record_datetime, user_update_id, update_datetime)\n"
                    + "    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getGenID(tableId));
            preparedStatement.setString(2, obj.t_visit_payment_id);
            preparedStatement.setString(3, obj.b_finance_insurance_company_id);
            preparedStatement.setString(4, obj.b_finance_insurance_plan_id);
            preparedStatement.setString(5, obj.priority);
            preparedStatement.setString(6, obj.active);
            preparedStatement.setString(7, obj.user_record_id);
            preparedStatement.setString(8, obj.record_datetime);
            preparedStatement.setString(9, obj.user_update_id);
            preparedStatement.setString(10, obj.update_datetime);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(VisitInsurancePlan obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_visit_insurance_plan\n");
            sql.append("   SET priority=?, active=?, \n");
            sql.append("       user_update_id=?, update_datetime=?\n");
            sql.append(" WHERE t_visit_insurance_plan_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.priority);
            preparedStatement.setString(2, obj.active);
            preparedStatement.setString(3, obj.user_update_id);
            preparedStatement.setString(4, obj.update_datetime);
            preparedStatement.setString(5, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

//    public int delete(VisitInsurancePlan obj) throws Exception {
//        PreparedStatement preparedStatement = null;
//        try {
//            StringBuilder sql = new StringBuilder();
//            sql.append("DELETE FROM t_visit_insurance_plan\n");
//            sql.append(" WHERE t_visit_insurance_plan_id = ?");
//            preparedStatement = connectionInf.ePQuery(sql.toString());
//            preparedStatement.setString(1, obj.getObjectId());
//            // execute update SQL stetement
//            return preparedStatement.executeUpdate();
//
//        } finally {
//            if (preparedStatement != null) {
//                preparedStatement.close();
//            }
//        }
//    }
//
    public int inactive(VisitInsurancePlan obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_visit_insurance_plan\n");
            sql.append("   SET active=?, user_update_id=?, update_datetime=?\n");
            sql.append(" WHERE t_visit_insurance_plan_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, "0");
            preparedStatement.setString(2, obj.user_update_id);
            preparedStatement.setString(3, obj.update_datetime);
            preparedStatement.setString(4, obj.getObjectId());
            // execute update SQL stetement
            preparedStatement.executeUpdate();
            // re-priority
            sql = new StringBuilder();
            sql.append("UPDATE t_visit_insurance_plan\n");
            sql.append("   SET priority= to_number(priority, '99') - 1");
            sql.append(" WHERE t_visit_payment_id = ? and t_visit_insurance_plan.active = '1' and to_number(priority, '99') > to_number(?, '99')");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.t_visit_payment_id);
            preparedStatement.setString(2, obj.priority);
            // execute update SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public VisitInsurancePlan select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select t_visit_insurance_plan.*, b_finance_insurance_company.company_name, b_finance_insurance_plan.description from t_visit_insurance_plan "
                    + "inner join b_finance_insurance_company on b_finance_insurance_company.b_finance_insurance_company_id = t_visit_insurance_plan.b_finance_insurance_company_id "
                    + "inner join b_finance_insurance_plan on b_finance_insurance_plan.b_finance_insurance_plan_id = t_visit_insurance_plan.b_finance_insurance_plan_id "
                    + "where t_visit_insurance_plan_id = ? and t_visit_insurance_plan.active = '1' order by to_number(priority, '99')";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<VisitInsurancePlan> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public VisitInsurancePlan selectByVisitPaymentIdAndInsurancePlanId(String t_visit_payment_id, String b_finance_insurance_plan_id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from t_visit_insurance_plan "
                    + "where t_visit_payment_id = ? and b_finance_insurance_plan_id = ? and t_visit_insurance_plan.active = '1'";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, t_visit_payment_id);
            preparedStatement.setString(2, b_finance_insurance_plan_id);
            List<VisitInsurancePlan> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<VisitInsurancePlan> listByVisitPaymentId(String t_visit_payment_id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select t_visit_insurance_plan.*, b_finance_insurance_company.company_name, b_finance_insurance_plan.description from t_visit_insurance_plan "
                    + "inner join b_finance_insurance_company on b_finance_insurance_company.b_finance_insurance_company_id = t_visit_insurance_plan.b_finance_insurance_company_id "
                    + "inner join b_finance_insurance_plan on b_finance_insurance_plan.b_finance_insurance_plan_id = t_visit_insurance_plan.b_finance_insurance_plan_id "
                    + "where t_visit_payment_id = ? and t_visit_insurance_plan.active = '1' order by to_number(priority, '99')";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, t_visit_payment_id);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<Object[]> listClaimInfoByBillingInvoiceId(String billingInvoiceId) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select\n"
                    + "t_visit_insurance_plan.t_visit_insurance_plan_id\n"
                    + ", t_visit_insurance_plan.priority\n"
                    + ", b_finance_insurance_company.company_name\n"
                    + ", b_finance_insurance_plan.description as plan\n"
                    + ", t_insurance_claim.claim_code\n"
                    + ", t_insurance_claim.status_insurance_approve\n"
                    + ", t_insurance_claim.t_insurance_claim_id\n"
                    + "from \n"
                    + "t_billing_invoice\n"
                    + "inner join t_visit_insurance_plan on t_visit_insurance_plan.t_visit_payment_id = t_billing_invoice.t_payment_id\n"
                    + "inner join b_finance_insurance_company on b_finance_insurance_company.b_finance_insurance_company_id = t_visit_insurance_plan.b_finance_insurance_company_id \n"
                    + "inner join b_finance_insurance_plan on b_finance_insurance_plan.b_finance_insurance_plan_id = t_visit_insurance_plan.b_finance_insurance_plan_id \n"
                    + "left join t_insurance_claim on t_insurance_claim.t_visit_insurance_plan_id = t_visit_insurance_plan.t_visit_insurance_plan_id \n"
                    + "and t_insurance_claim.t_billing_invoice_id  = t_billing_invoice.t_billing_invoice_id\n"
                    + "where \n"
                    + "t_billing_invoice.t_billing_invoice_id = ? and t_visit_insurance_plan.active = '1' order by to_number(priority, '99')";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, billingInvoiceId);
            return connectionInf.eComplexQuery(preparedStatement.toString());
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<VisitInsurancePlan> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<VisitInsurancePlan> list = new ArrayList<VisitInsurancePlan>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                VisitInsurancePlan obj = new VisitInsurancePlan();
                obj.setObjectId(rs.getString("t_visit_insurance_plan_id"));
                obj.t_visit_payment_id = rs.getString("t_visit_payment_id");
                obj.b_finance_insurance_plan_id = rs.getString("b_finance_insurance_plan_id");
                obj.b_finance_insurance_company_id = rs.getString("b_finance_insurance_company_id");
                obj.active = rs.getString("active");
                obj.priority = rs.getString("priority");
                obj.user_record_id = rs.getString("user_record_id");
                obj.record_datetime = rs.getString("record_datetime");
                obj.user_update_id = rs.getString("user_update_id");
                obj.update_datetime = rs.getString("update_datetime");
                try {
                    obj.b_finance_insurance_plan_name = rs.getString("description");
                } catch (Exception ex) {
                }
                try {
                    obj.b_finance_insurance_company_name = rs.getString("company_name");
                } catch (Exception ex) {
                }
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }
}

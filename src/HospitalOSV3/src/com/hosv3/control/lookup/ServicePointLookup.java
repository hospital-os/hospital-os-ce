/*
 * PlanLookup.java
 *
 * Created on 27 �á�Ҥ� 2548, 15:22 �.
 */
package com.hosv3.control.lookup;

import com.hospital_os.object.ServicePoint;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.LookupControlInf;
import com.hosv3.control.LookupControl;
import com.hosv3.control.SetupControl;
import com.hosv3.utility.connection.ExecuteControlInf;

/**
 *
 * @vnot deprecated because use henbe package bad read Data
 *
 * @author kingland
 */
public class ServicePointLookup implements LookupControlInf, ExecuteControlInf {

    boolean is_lookup = true;
    private LookupControl theLookup;
    private SetupControl theEC;

    /**
     * Creates a new instance of PlanLookup
     */
    public ServicePointLookup(LookupControl lookup) {
        theLookup = lookup;
    }

    public ServicePointLookup(SetupControl lookup) {
        is_lookup = false;
        theEC = lookup;
    }

    @Override
    @SuppressWarnings("UseOfObsoleteCollectionType")
    public java.util.Vector listData(String str) {
        if (str == null || str.equals("") || str.equals("%")) {
            return null;
        } else {
            return theLookup.listServicePoint(str);
        }
    }

    @Override
    public CommonInf readHosData(String str) {
        return null;
    }

    @Override
    public boolean execute(Object str) {
        theEC.addServicePoint((ServicePoint) str);
        return true;
    }
}

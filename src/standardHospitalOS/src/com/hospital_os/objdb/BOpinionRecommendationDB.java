/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.BOpinionRecommendation;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class BOpinionRecommendationDB {

    public ConnectionInf connectionInf;
    final public String tableId = "999";

    public BOpinionRecommendationDB(ConnectionInf db) {
        connectionInf = db;
    }

    public int insert(BOpinionRecommendation obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO b_opinion_recommendation(\n"
                    + "            id, code, description, active, user_record_id, user_update_id)\n"
                    + "    VALUES (?, ?, ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.getGenID(tableId));
            preparedStatement.setString(index++, obj.code);
            preparedStatement.setString(index++, obj.description);
            preparedStatement.setString(index++, obj.active);
            preparedStatement.setString(index++, obj.user_record_id);
            preparedStatement.setString(index++, obj.user_update_id);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(BOpinionRecommendation obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE b_opinion_recommendation\n"
                    + "   SET code=?, description=?,active=?, \n"
                    + "       update_datetime=current_timestamp, user_update_id=?\n"
                    + " WHERE id=?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.code);
            preparedStatement.setString(index++, obj.description);
            preparedStatement.setString(index++, obj.active);
            preparedStatement.setString(index++, obj.user_update_id);
            preparedStatement.setString(index++, obj.getObjectId());
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(BOpinionRecommendation obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM b_opinion_recommendation\n");
            sql.append(" WHERE id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public BOpinionRecommendation selectById(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_opinion_recommendation\n"
                    + "where id = ?\n";
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, id);
            List<BOpinionRecommendation> executeQuery = executeQuery(preparedStatement);
            return executeQuery.isEmpty() ? null : executeQuery.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<BOpinionRecommendation> selectAll() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_opinion_recommendation where active = '1' order by b_opinion_recommendation.code asc";
            preparedStatement = connectionInf.ePQuery(sql);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<BOpinionRecommendation> listByKeyword(String keyword) throws Exception {
        return listByKeyword(keyword, true);
    }

    public List<BOpinionRecommendation> listByKeyword(String keyword, boolean isActive) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select *  from b_opinion_recommendation where (code ilike ? or description ilike ?) and active = ? order by b_opinion_recommendation.code asc";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, "%" + keyword + "%");
            preparedStatement.setString(2, "%" + keyword + "%");
            preparedStatement.setString(3, isActive ? "1" : "0");
            List<BOpinionRecommendation> list = executeQuery(preparedStatement);
            return list;
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<BOpinionRecommendation> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<BOpinionRecommendation> list = new ArrayList<BOpinionRecommendation>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                BOpinionRecommendation obj = new BOpinionRecommendation();
                obj.setObjectId(rs.getString("id"));
                obj.code = rs.getString("code");
                obj.description = rs.getString("description");
                obj.active = rs.getString("active");
                obj.record_datetime = rs.getTimestamp("record_datetime");
                obj.user_record_id = rs.getString("user_record_id");
                obj.update_datetime = rs.getTimestamp("update_datetime");
                obj.user_update_id = rs.getString("user_update_id");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }
}

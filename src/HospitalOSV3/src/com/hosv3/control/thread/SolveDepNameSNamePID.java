/*
 * SolveDeprecatedVNThread.java
 *
 * Created on 16 �á�Ҥ� 2550, 14:22 �.
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package com.hosv3.control.thread;

import com.hospital_os.object.Patient;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hosv3.control.HosControl;
import com.hosv3.control.HosDB;
import com.hosv3.control.PatientControl;
import com.hosv3.object.HosObject;
import com.hosv3.utility.ResourceBundle;
import java.sql.SQLException;
import java.util.Vector;
import java.util.logging.Logger;

/**
 *
 * @author Aut
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class SolveDepNameSNamePID extends ControlThread {

    private PatientControl thePatientControl;

    /**
     * Creates a new instance of SolveDeprecatedVNThread
     */
    public SolveDepNameSNamePID() {
        this.setDaemon(true);
    }

    @Override
    public void setControl(ConnectionInf con, HosDB hdb, HosObject ho, UpdateStatus us, Object control) {
        theConnectionInf = con;
        theHosDB = hdb;
        theUS = us;
        theHO = ho;
        thePatientControl = (PatientControl) control;
    }

    @Override
    protected void runTask() {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            java.sql.ResultSet rs = theConnectionInf.eQuery("select count(*) from ("
                    + "select patient_pid"
                    + ",patient_firstname || '  ' || patient_lastname || ' ' || patient_pid as patient"
                    + ",count(t_patient_id) as cnt from t_patient where patient_pid <>'' and patient_active = '1'"
                    + " group by patient,patient_pid )as q1 "
                    + " where cnt > 1");
            int total = 0;
            if (rs.next()) {
                total = rs.getInt(1);
            }
            rs.close();
            rs = theConnectionInf.eQuery("select patient_pid from ("
                    + "select patient_pid"
                    + ",patient_firstname || '  ' || patient_lastname || ' ' || patient_pid as patient"
                    + ",count(t_patient_id) as cnt from t_patient where patient_pid <>'' and patient_active = '1'"
                    + " group by patient,patient_pid )as q1 "
                    + " where cnt > 1");
            int count = 0;
            while (rs.next()) {
                String pid_dep = rs.getString(1);
                Vector patientV = theHosDB.thePatientDB.selectByPID(pid_dep);
                Patient patient_main = (Patient) patientV.get(0);
                for (int i = 1; i < patientV.size(); i++) {
                    Patient patient = (Patient) patientV.get(i);
                    thePatientControl.intMovePatientHistory((UpdateStatus) theUS, patient, patient_main);
                }
                theUS.setStatus(ResourceBundle.getBundleGlobal("TEXT.MOVE") + " " + count + "/" + total, UpdateStatus.WARNING);
                count++;
            }
            theConnectionInf.getConnection().commit();
            theUS.confirmBox(ResourceBundle.getBundleText("com.hosv3.control.SystemControl.RESOLVE.DUPLICATE.PERSON") + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.UNKONW, ResourceBundle.getBundleText("com.hosv3.control.SystemControl.RESOLVE.DUPLICATE.PERSON"));
            Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
    }
}

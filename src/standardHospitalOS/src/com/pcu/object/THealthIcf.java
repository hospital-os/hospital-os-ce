/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.object;

import com.hospital_os.usecase.connection.Persistent;
import java.util.Date;

/**
 *
 * @author sompr
 */
public class THealthIcf extends Persistent {

    public String t_visit_id;
    public Date survey_date = new Date();
    public String f_icf_code_id;
    public String active = "1";
    public Date record_date_time;
    public String user_record_id = "";
    public Date update_date_time;
    public String user_update_id = "";
    public Date cancel_date_time;
    public String user_cancel_id = "";
    
    public String getGroupCode(){
        return f_icf_code_id != null && !f_icf_code_id.isEmpty() ? f_icf_code_id.substring(0, 4) : "";
    }
}

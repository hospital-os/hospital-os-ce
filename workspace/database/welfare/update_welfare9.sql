CREATE TABLE b_welfare_product_category (
    b_welfare_product_category_id character varying(255)
    ,b_welfare_product_category_description character varying(255)
);
ALTER TABLE ONLY b_welfare_product_category
    ADD CONSTRAINT b_welfare_product_category_pkey PRIMARY KEY (b_welfare_product_category_id);



INSERT INTO public.b_welfare_product_category VALUES('1', 'ยาแผนปัจจุบันที่เป็นผลิตภัณฑ์ทางการค้า');
INSERT INTO public.b_welfare_product_category VALUES('2', 'ยาแผนปัจจุบันผลิตใช้เอง');
INSERT INTO public.b_welfare_product_category VALUES('3', 'ยาแผนไทยที่เป็นผลิตภัณฑ์ทางการค้า');
INSERT INTO public.b_welfare_product_category VALUES('4', 'ยาแผนไทยผลิตใช้เอง');
INSERT INTO public.b_welfare_product_category VALUES('5', 'ยาแผนการรักษาทางเลือกอื่น');
INSERT INTO public.b_welfare_product_category VALUES('6', 'เวชภัณฑ์');
INSERT INTO public.b_welfare_product_category VALUES('7', 'อื่น ๆ');


CREATE TABLE b_map_welfare_product_category (
    b_map_welfare_product_category_id character varying(255)
    ,b_item_id                                             varchar(255) NULL
    ,b_welfare_product_category_id               varchar(255) NULL
);
ALTER TABLE ONLY b_map_welfare_product_category
    ADD CONSTRAINT b_map_welfare_product_category_pkey PRIMARY KEY (b_map_welfare_product_category_id);


INSERT INTO s_welfare_version("s_version_id", "version_number", "version_description", "version_application_number", "version_database_number", "version_update_time")
VALUES('9701000000021', '7', 'Hospital OS, Community Edition', '1.09.170912', '3.16.170912', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));



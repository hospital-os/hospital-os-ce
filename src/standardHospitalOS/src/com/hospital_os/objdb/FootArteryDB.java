/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.FootArtery;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class FootArteryDB {
    public ConnectionInf theConnectionInf;
    final public String idtable = "994";

    public FootArteryDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public int insert(FootArtery o) throws Exception {
        o.generateOID(idtable);
        String sql = "INSERT INTO t_foot_artery( "
            + "t_foot_artery_id, t_visit_id, left_dorsalis_pedis, right_dorsalis_pedis, " 
            + "left_posterior_tibial, right_posterior_tibial, left_gangrene,  "
            + "right_gangrene, left_sensory, left_sensory_count, right_sensory, " 
            + "right_sensory_count, wound, wound_position, wound_area, wound_size,  "
            + "active, record_date_time, user_record_id, update_date_time, user_update_id) "
    + "VALUES ('%s', '%s', '%s', '%s',  "
            + "'%s', '%s', '%s',  "
            + "'%s', '%s', '%s', '%s',  "
            + "'%s', '%s', '%s', '%s', '%s',  "
            + "'%s', '%s', '%s', '%s', '%s')";
        Object[] values = new Object[]{
            o.getObjectId(), o.t_visit_id, o.left_dorsalis_pedis, o.right_dorsalis_pedis,
            o.left_posterior_tibial, o.right_posterior_tibial, o.left_gangrene, o.right_gangrene,
            o.left_sensory, o.left_sensory_count, o.right_sensory,
            o.right_sensory_count, o.wound, o.wound_position,
            o.wound_area, o.wound_size, 
            o.active, o.record_date_time, o.user_record_id, o.update_date_time,
            o.user_update_id};
        return theConnectionInf.eUpdate(String.format(sql, values));
    }

    public int update(FootArtery o) throws Exception {
        String sql = "UPDATE t_foot_artery "
                + "SET left_dorsalis_pedis='%s', right_dorsalis_pedis='%s',  "
                + "left_posterior_tibial='%s', right_posterior_tibial='%s', left_gangrene='%s',  "
                + "right_gangrene='%s', left_sensory='%s', left_sensory_count='%s',  "
                + "right_sensory='%s', right_sensory_count='%s',  "
                + "wound='%s', wound_position='%s', wound_area='%s',  "
                + "wound_size='%s', "
                + "update_date_time='%s',user_update_id='%s' "
                + "WHERE t_foot_artery_id='%s'";
        Object[] values = new Object[]{
            o.left_dorsalis_pedis, o.right_dorsalis_pedis,
            o.left_posterior_tibial, o.right_posterior_tibial, o.left_gangrene, 
            o.right_gangrene, o.left_sensory, o.left_sensory_count, 
            o.right_sensory, o.right_sensory_count, 
            o.wound, o.wound_position, o.wound_area, 
            o.wound_size, 
            o.update_date_time, o.user_update_id, 
            o.getObjectId()};
        return theConnectionInf.eUpdate(String.format(sql, values));

    }

    public int delete(FootArtery o) throws Exception {
        String sql = "UPDATE t_foot_artery "
                + "SET active='0',update_date_time='%s',user_update_id='%s' "
                + "WHERE t_foot_artery_id='%s'";
        Object[] values = new Object[]{o.update_date_time,
            o.user_update_id, o.getObjectId()};
        return theConnectionInf.eUpdate(String.format(sql, values));
    }

    public FootArtery findByVisitId(String visitId) throws Exception {
        String sql = "select * from t_foot_artery where t_visit_id = '%s' and active = '1'";
        List<FootArtery> list = eQuery(String.format(sql, visitId));
        return list.isEmpty() ? null : list.get(0);
    }

    private List<FootArtery> eQuery(String sql) throws Exception {
        List<FootArtery> list = new ArrayList<FootArtery>();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        while (rs.next()) {
            FootArtery o = new FootArtery();
            o.setObjectId(rs.getString("t_foot_artery_id"));
            o.t_visit_id = rs.getString("t_visit_id");
            o.left_dorsalis_pedis = rs.getString("left_dorsalis_pedis");
            o.right_dorsalis_pedis = rs.getString("right_dorsalis_pedis");
            o.left_posterior_tibial = rs.getString("left_posterior_tibial");
            o.right_posterior_tibial = rs.getString("right_posterior_tibial");
            o.left_gangrene = rs.getString("left_gangrene");
            o.right_gangrene = rs.getString("right_gangrene");
            o.left_sensory = rs.getString("left_sensory");
            o.left_sensory_count = rs.getString("left_sensory_count");
            o.right_sensory = rs.getString("right_sensory");
            o.right_sensory_count = rs.getString("right_sensory_count");
            o.wound = rs.getString("wound");
            o.wound_position = rs.getString("wound_position");
            o.wound_area = rs.getString("wound_area");
            o.wound_size = rs.getString("wound_size");
            o.active = rs.getString("active");
            o.record_date_time = rs.getString("record_date_time");
            o.user_record_id = rs.getString("user_record_id");
            o.update_date_time = rs.getString("update_date_time");
            o.user_update_id = rs.getString("user_update_id");
            list.add(o);
        }
        rs.close();
        return list;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.MapDrugCode24;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class MapDrugCode24DB {

    public ConnectionInf connectionInf;
    final public String tableId = "861";

    public MapDrugCode24DB(ConnectionInf db) {
        connectionInf = db;
    }

    public int insert(MapDrugCode24 obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO b_nhso_map_drug(\n"
                    + "            b_nhso_map_drug_id, f_nhso_drug_id, b_item_id, b_nhso_drugcode24_id)\n"
                    + "    VALUES (?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.getGenID(tableId));
            preparedStatement.setString(index++, obj.f_nhso_drug_id);
            preparedStatement.setString(index++, obj.b_item_id);
            preparedStatement.setString(index++, obj.b_nhso_drugcode24_id);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM b_nhso_map_drug\n");
            sql.append(" WHERE b_nhso_map_drug_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, id);
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(String... ids) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "DELETE FROM b_nhso_map_drug WHERE b_nhso_map_drug_id in (%s)";
            StringBuilder sb = new StringBuilder();
            for (String id : ids) {
                sb.append("'").append(id).append("',");
            }
            preparedStatement = connectionInf.ePQuery(String.format(sql,
                    sb.toString().substring(0, sb.toString().length() - 1)));
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<MapDrugCode24> list(String keyword, String grpId, String type) throws Exception {
        String sql = "select b_item.item_common_name\n"
                + ", b_nhso_drugcode24.itemname \n"
                + "|| '^' || b_nhso_drugcode24.tradename \n"
                + "|| (case when strpos(b_nhso_drugcode24.itemname, ', ') is not null then '\n' || substr(b_nhso_drugcode24.itemname,strpos(b_nhso_drugcode24.itemname, ', ') + 2) else '' end) \n"
                + "|| '\n' || b_nhso_drugcode24.regno \n"
                + "|| '\n' || b_nhso_drugcode24.company as itemname24\n"
                + ", b_item.b_item_id\n"
                + ", b_nhso_map_drug.b_nhso_drugcode24_id\n"
                + ", b_nhso_map_drug.b_nhso_map_drug_id\n"
                + ", b_nhso_map_drug.f_nhso_drug_id\n"
                + "from\n"
                + "b_item\n"
                + "inner join b_item_subgroup on b_item.b_item_subgroup_id = b_item_subgroup.b_item_subgroup_id and b_item_subgroup.f_item_group_id in ('1','4')\n"
                + "left join b_nhso_map_drug on b_item.b_item_id = b_nhso_map_drug.b_item_id\n"
                + "left join b_nhso_drugcode24 on b_nhso_drugcode24.b_nhso_drugcode24_id = b_nhso_map_drug.b_nhso_drugcode24_id\n"
                + "\n"
                + "where b_item.item_active = '1'\n";
        if (keyword != null && !keyword.isEmpty()) {
            sql += "and (b_item.item_common_name ilike ? or b_item.item_trade_name ilike ? or b_item.item_nick_name ilike ?)\n";
        }
        if (grpId != null && !grpId.isEmpty()) {
            sql += "and b_item_subgroup.b_item_subgroup_id = ?\n";
        }
        if (type != null && !type.isEmpty()) {
            if (type.equals("1")) {
                sql += "and b_nhso_map_drug.b_nhso_map_drug_id is not null\n";
            } else if (type.equals("2")) {
                sql += "and b_nhso_map_drug.b_nhso_map_drug_id is null\n";
            }
        }
        sql += "order by b_item.item_common_name";
        PreparedStatement preparedStatement = connectionInf.ePQuery(sql.toString());
        int index = 1;
        if (keyword != null && !keyword.isEmpty()) {
            preparedStatement.setString(index++, "%" + keyword + "%");
            preparedStatement.setString(index++, "%" + keyword + "%");
            preparedStatement.setString(index++, "%" + keyword + "%");
        }
        if (grpId != null && !grpId.isEmpty()) {
            preparedStatement.setString(index++, grpId);
        }
        return executeQuery(preparedStatement);
    }

    public MapDrugCode24 selectByItemId(String itemId) throws Exception {
        String sql = "select b_nhso_map_drug.*, b_nhso_drugcode24.drugcode24 as drugcode24\n"
                + "from b_nhso_map_drug\n"
                + "inner join b_nhso_drugcode24 on b_nhso_map_drug.b_nhso_drugcode24_id = b_nhso_drugcode24.b_nhso_drugcode24_id\n"
                + "where b_nhso_map_drug.b_item_id = ?";
        PreparedStatement preparedStatement = connectionInf.ePQuery(sql.toString());
        int index = 1;
        preparedStatement.setString(index++, itemId);
        List<MapDrugCode24> executeQuery = executeQuery(preparedStatement);
        return executeQuery.isEmpty() ? null : executeQuery.get(0);
    }

    public List<MapDrugCode24> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<MapDrugCode24> list = new ArrayList<MapDrugCode24>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                MapDrugCode24 obj = new MapDrugCode24();
                obj.setObjectId(rs.getString("b_nhso_map_drug_id"));
                obj.f_nhso_drug_id = rs.getString("f_nhso_drug_id");
                obj.b_nhso_drugcode24_id = rs.getString("b_nhso_drugcode24_id");
                obj.b_item_id = rs.getString("b_item_id");
                try {
                    obj.item_name = rs.getString("item_common_name");
                } catch (Exception ex) {
                }
                try {
                    obj.item_name24 = rs.getString("itemname24");
                } catch (Exception ex) {
                }
                try {
                    obj.drugcode24 = rs.getString("drugcode24");
                } catch (Exception ex) {
                }
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }
}

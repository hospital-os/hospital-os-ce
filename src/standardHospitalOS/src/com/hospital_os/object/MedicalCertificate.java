/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;
import java.util.Date;

/**
 *
 * @author Somprasong
 */
public class MedicalCertificate extends Persistent {

    public String hn = "";
    public String vn = "";
    public String doctor_id = "";
    public int f_medical_certificate_type_id = 1;
    public String medical_certificate_no = "";
    public Date issue_date = new Date();
    public String details = "{}";
    public int print_count = 0;
    public boolean active = true;
    public Date record_datetime = new Date();
    public String user_record_id = "";
    public Date update_datetime = new Date();
    public String user_update_id = "";
    public Date cancel_datetime;
    public String user_cancel_id;

    public String typeName = "";

    public String getSeqType() {
        return f_medical_certificate_type_id == 1
                ? "mc"
                : f_medical_certificate_type_id == 2
                        ? "mc_w"
                        : "mc_a";
    }
}

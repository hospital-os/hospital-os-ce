/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.objdb.objdbclass;

import com.hospital_os.usecase.connection.ConnectionInf;
import com.pcu.object.Vaccine;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tanakrit
 */
public class VaccineDB {

    public ConnectionInf theConnectionInf;
    final private String idtable = "919";

    public VaccineDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public int insert(Vaccine p) throws Exception {
        p.generateOID(idtable);
        String sql = "INSERT INTO b_health_vaccine( \n"
                + "               b_health_vaccine_id, vaccine_name, serial_number, lot_number, \n"
                + "               f_vaccine_manufacturer_id, receive_date, expire_date, qty, dose, bottle_number, \n"
                + "               dose_number, b_health_epi_group_id, use_status, active, user_record_id) \n"
                + "       values (?, ?, ?, ? \n"
                + "             , ?, ?, ?, ?, ?, ? \n"
                + "             , ?, ?, ?, ?, ?) ";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, p.getObjectId());
            ePQuery.setString(index++, p.vaccine_name);
            ePQuery.setString(index++, p.serial_number);
            ePQuery.setString(index++, p.lot_number);
            ePQuery.setInt(index++, Integer.parseInt(p.f_vaccine_manufacturer_id));
            ePQuery.setDate(index++, p.receive_date != null ? new java.sql.Date(p.receive_date.getTime()) : null);
            ePQuery.setDate(index++, p.expire_date != null ? new java.sql.Date(p.expire_date.getTime()) : null);
            ePQuery.setInt(index++, p.qty);
            ePQuery.setInt(index++, p.dose);
            ePQuery.setInt(index++, p.bottle_number);
            ePQuery.setInt(index++, p.dose_number);
            ePQuery.setString(index++, p.b_health_epi_group_id);
            ePQuery.setString(index++, p.use_status);
            ePQuery.setString(index++, p.active);
            ePQuery.setString(index++, p.user_record_id);
            return ePQuery.executeUpdate();
        }
    }

    public int update(Vaccine p) throws Exception {
        String sql = "UPDATE b_health_vaccine \n"
                + "      SET barcode=?, vaccine_name=?, serial_number=?, "
                + "          lot_number=?, f_vaccine_manufacturer_id=?, receive_date=?, "
                + "          expire_date=?, qty=?, dose=?, bottle_number=?, dose_number=?, b_health_epi_group_id=?, "
                + "          use_status=?, active=?, user_cancel_id=?, cancel_date_time=current_timestamp\n"
                + "    WHERE b_health_vaccine_id=?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, p.barcode);
            ePQuery.setString(index++, p.vaccine_name);
            ePQuery.setString(index++, p.serial_number);
            ePQuery.setString(index++, p.lot_number);
            ePQuery.setInt(index++, Integer.parseInt(p.f_vaccine_manufacturer_id));
            ePQuery.setDate(index++, p.receive_date != null ? new java.sql.Date(p.receive_date.getTime()) : null);
            ePQuery.setDate(index++, p.expire_date != null ? new java.sql.Date(p.expire_date.getTime()) : null);
            ePQuery.setInt(index++, p.qty);
            ePQuery.setInt(index++, p.dose);
            ePQuery.setInt(index++, p.bottle_number);
            ePQuery.setInt(index++, p.dose_number);
            ePQuery.setString(index++, p.b_health_epi_group_id);
            ePQuery.setString(index++, p.use_status);
            ePQuery.setString(index++, p.active);
            ePQuery.setString(index++, p.user_record_id);
            ePQuery.setString(index++, p.getObjectId());
            return ePQuery.executeUpdate();
        }
    }

    public int updateUseStatusByPk(String status, String pk) throws Exception {
        String sql = "UPDATE b_health_vaccine SET use_status = ? WHERE b_health_vaccine_id = ? and active='1'";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, status);
            ePQuery.setString(index++, pk);
            return ePQuery.executeUpdate();
        }
    }

    public int inActiveById(Vaccine p) throws Exception {
        String sql = "UPDATE b_health_vaccine \n"
                + "      SET active='0', user_cancel_id=?, cancel_date_time=current_timestamp\n"
                + "    WHERE b_health_vaccine_id=?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, p.user_cancel_id);
            ePQuery.setString(index++, p.getObjectId());
            return ePQuery.executeUpdate();
        }
    }

    public int inActiveByIds(Object[] Ids, String userId) throws Exception {
        String sql = "UPDATE b_health_vaccine \n"
                + "      SET active='0', user_cancel_id=?, cancel_date_time=current_timestamp\n"
                + "    WHERE b_health_vaccine_id = ANY(?)";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, userId);
            ePQuery.setArray(index++, theConnectionInf.getConnection().createArrayOf("varchar", Ids));
            return ePQuery.executeUpdate();
        }
    }

    public Vaccine selectByPk(String id) throws Exception {
        String sql = "select * from b_health_vaccine where b_health_vaccine_id = ? and active ='1'";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, id);
            List<Vaccine> list = executeQuery(ePQuery);
            return list.isEmpty() ? null : list.get(0);
        }
    }

    public List<Vaccine> selectByKeywords(String keywords) throws Exception {
        String sql = "select * from b_health_vaccine where active='1' ";
        if (!keywords.isEmpty()) {
            sql += "and (vaccine_name ilike ? "
                    + "or serial_number ilike ? "
                    + "or lot_number ilike ?)";
        }
        sql += "order by barcode";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            if (!keywords.isEmpty()) {
                ePQuery.setString(index++, "%" + keywords + "%");
                ePQuery.setString(index++, "%" + keywords + "%");
                ePQuery.setString(index++, "%" + keywords + "%");
            }
            return executeQuery(ePQuery);
        }
    }

    public int selectMaxBottleBySerialAndLotNumber(String serialNumber, String lotNumber) throws Exception {
        String sql = "select max(bottle_number) as bottle_number from b_health_vaccine where serial_number = ? \n "
                + " and lot_number = ? ";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, serialNumber);
            ePQuery.setString(index++, lotNumber);
            int max = 0;
            try (ResultSet rs = ePQuery.executeQuery()) {
                while (rs.next()) {
                    max = rs.getInt("bottle_number");
                }
            }
            return max;
        }
    }

    public Vaccine selectByBarcode(String barcode) throws Exception {
        String sql = "select * from b_health_vaccine where barcode = ? ";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, barcode);
            List<Vaccine> list = executeQuery(ePQuery);
            return list.isEmpty() ? null : list.get(0);
        }
    }

    public List<String> selectAllNotUseForPrint() throws Exception {
        String sql = "select b_health_vaccine_id from b_health_vaccine "
                + " where active='1' and use_status='1' "
                + " order by barcode";
        List<String> list = new ArrayList<>();
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            ResultSet rs = ePQuery.executeQuery();
            while (rs.next()) {
                list.add(rs.getString("b_health_vaccine_id"));
            }
            return list;
        }
    }

    public List<Vaccine> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<Vaccine> list = new ArrayList<>();
        try (ResultSet rs = preparedStatement.executeQuery()) {
            while (rs.next()) {
                Vaccine p = new Vaccine();
                p.setObjectId(rs.getString("b_health_vaccine_id"));
                p.barcode = rs.getString("barcode");
                p.vaccine_name = rs.getString("vaccine_name");
                p.serial_number = rs.getString("serial_number");
                p.lot_number = rs.getString("lot_number");
                p.f_vaccine_manufacturer_id = String.valueOf(rs.getInt("f_vaccine_manufacturer_id"));
                p.receive_date = rs.getDate("receive_date");
                p.expire_date = rs.getDate("expire_date");
                p.qty = rs.getInt("qty");
                p.dose = rs.getInt("dose");
                p.bottle_number = rs.getInt("bottle_number");
                p.dose_number = rs.getInt("dose_number");
                p.b_health_epi_group_id = rs.getString("b_health_epi_group_id");
                p.use_status = rs.getString("use_status");
                p.active = rs.getString("active");
                p.user_record_id = rs.getString("user_record_id");
                p.record_date_time = rs.getTimestamp("record_date_time");
                p.user_cancel_id = rs.getString("user_cancel_id");
                p.cancel_date_time = rs.getTimestamp("cancel_date_time");
                list.add(p);
            }
            return list;
        }
    }
}

package com.hosv3.objdb;

import com.hospital_os.objdb.OccupatDB;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.Gutil;
import com.hosv3.object.Occupation2;
import java.sql.ResultSet;
import java.util.Vector;

@SuppressWarnings("UseOfObsoleteCollectionType")
public class Occupation2DB extends OccupatDB {

    public Occupation2DB(ConnectionInf db) {
        super(db);
    }

    public Occupation2 select2ByPK(String str) throws Exception {
        String sql = "select * from " + dbObj.table
                + " where " + dbObj.pk_field + " = '" + str + "'";
        Vector v = eQuery(sql);
        if (v.isEmpty()) {
            return null;
        }
        return (Occupation2) v.get(0);
    }

    @Override
    public Vector selectAll() throws Exception {
        String sql = "select * from " + dbObj.table
                + " order by " + dbObj.pk_field;
        return eQuery(sql);
    }

    public Vector selectByCN(String key) throws Exception {
         key = Gutil.CheckReservedWords(key);
        String sql = "select * from " + dbObj.table
                + " where UPPER(" + dbObj.pk_field
                + ") like UPPER('%" + key + "%') "
                + " or UPPER(" + dbObj.description
                + ") like UPPER('%" + key + "%') order by " + dbObj.description;

        return eQuery(sql);
    }

    @Override
    public Vector eQuery(String sql) throws Exception {
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            Occupation2 p = new Occupation2();
            p.setObjectId(rs.getString(dbObj.pk_field));
            p.description = rs.getString(dbObj.description);
            list.add(p);
        }
        rs.close();
        return list;
    }
}

package com.hosv3.control;

import com.hospital_os.object.CovidTmlt;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hospital_os.utility.ComplexDataSource;
import com.hospital_os.utility.DateUtil;
import com.hosv3.object.HosObject;
import com.hosv3.object.LabList;
import com.hosv3.utility.ResourceBundle;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author henbe
 * @modify Somprasong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class LabControl {

    private static final Logger LOG = Logger.getLogger(LabControl.class.getName());
    public Connection theConnection;
    public UpdateStatus theUS;
    public String str_latestTime = "";
    public String str_currentTime = "";
    public String url = "";
    public long delay = 5000;
    public int i_format = 2;
    public int sender;
    public String ln_type = "";
    private final ConnectionInf cdb;
    private final HosControl theHosControl;
    private final HosObject theHO;
    public static String ONE_LN = "1";
    public static String MANY_LN = "2";
    String current_file_name = "";

    /**
     * Creates a new instance of LabControl
     *
     * @param hc
     */
    public LabControl(HosControl hc) {
        theHosControl = hc;
        theHO = theHosControl.theHO;
        cdb = theHosControl.theConnectionInf;
        theConnection = cdb.getConnection();
    }

    public void setUpdateStatus(UpdateStatus us) {
        theUS = us;
    }

    public Vector<LabList> listLabOrderByVn(String visit_id) {
        boolean err = false;
        Vector v = new Vector();
        try {
            String sql = "select * from t_lis_ln where t_visit_id = '" + this.theHO.theVisit.getObjectId() + "'";
            ResultSet rs = executeQuery(theConnection, sql);
            if (rs.next()) {
                sql = "select t_lis_order.t_order_id as order_id "
                        + ",t_lis_order.exec_datetime as order_execute_date "
                        + ",bb.item_common_name as order_name "
                        + ",bb.item_nick_name as item_nick_name "
                        + ",aa.order_date_time as order_date "
                        + ",t_lis_order.status as status "
                        + ",t_lis_order.lab_number as lab_no "
                        + "from (select * from t_lis_ln) as cc "
                        + "inner join t_lis_order on cc.lab_number = t_lis_order.lab_number "
                        + "inner join (select t_order_id,b_item_id,order_date_time from t_order where t_order.t_visit_id = '" + visit_id + "' and t_order.f_order_status_id <> '3'  and t_order.f_order_status_id <> '0' and t_order.f_order_status_id <> '1') "
                        + "as aa on aa.t_order_id = t_lis_order.t_order_id "
                        + "inner join (select b_item_id,item_common_name,item_nick_name from b_item) as bb on bb.b_item_id = aa.b_item_id "
                        + "where cc.t_visit_id = '" + visit_id + "' "
                        + "group by t_lis_order.t_order_id"
                        + ",t_lis_order.exec_datetime"
                        + ",bb.item_common_name"
                        + ",bb.item_nick_name"
                        + ",aa.order_date_time"
                        + ",t_lis_order.status"
                        + ",t_lis_order.lab_number";
                rs = executeQuery(theConnection, sql);
                LabList listLab;
                while (rs.next()) {
                    listLab = new LabList();
                    listLab.order_id = rs.getString("order_id");
                    listLab.order_name = rs.getString("order_name");
                    listLab.item_nick_name = rs.getString("item_nick_name");
                    listLab.order_date = rs.getString("order_date");
                    listLab.order_execute_date = rs.getString("order_execute_date");
                    listLab.status = rs.getString("status");
                    listLab.ln = rs.getString("lab_no");
                    listLab.type = LabList.TYPE_NEW;
                    v.add(listLab);
                }
            } else {
                String sql_chk = "select t_order.t_order_id,order_common_name,b_item.item_nick_name from t_order "
                        + "inner join b_item on b_item.b_item_id = t_order.b_item_id "
                        + "where t_order.f_order_status_id <> '3' and t_order.f_item_group_id = '2' and t_order.t_visit_id = '" + visit_id + "' and b_item.item_nick_name = ''";
                ResultSet rs_chk = executeQuery(theConnection, sql_chk);
                while (rs_chk.next()) {
                    err = true;
                }
                if (err) {
                    theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.LabControl.NOT.SETUP.SPECIMEN"), UpdateStatus.WARNING);
                }//1007692
                sql = "select t_order.order_executed_date_time,t_order.order_date_time,t_order.t_order_id,order_common_name,b_item.item_nick_name from t_order "
                        + "inner join b_item on b_item.b_item_id = t_order.b_item_id "
                        + "where t_order.f_order_status_id <> '3' and t_order.f_item_group_id = '2' and t_order.t_visit_id = '" + visit_id + "' and b_item.item_nick_name <> ''";
                rs = executeQuery(theConnection, sql);
                LabList listLab;
                while (rs.next()) {
                    listLab = new LabList();
                    listLab.order_id = rs.getString("t_order_id");
                    listLab.order_name = rs.getString("order_common_name");
                    listLab.item_nick_name = rs.getString("item_nick_name");
                    listLab.order_date = rs.getString("order_date_time");
                    listLab.order_execute_date = rs.getString("order_executed_date_time");
                    listLab.type = LabList.TYPE_OLD;
                    v.add(listLab);
                }
                rs_chk.close();
                return null;
            }
            rs.close();
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }
        if (err && v.isEmpty()) {
            return null;
        }
        return v;
    }

    /**
     *
     * @param sql
     * @return
     */
    ResultSet executeQuery(Connection con, String sql) throws SQLException {
        theHosControl.theConnectionInf.open();
        return this.theHosControl.theConnectionInf.getConnection().createStatement().executeQuery(sql);
    }

    public Vector convertToSpecimenView(Vector<LabList> theLabListV) {
        Vector<LabList> theLabListVTmp = new Vector();
        String tmp_arr[];
        HashMap hm = new HashMap();
        String index = "";
        for (int i = 0; i < theLabListV.size(); i++) {
            LabList tmp = theLabListV.get(i);
            tmp_arr = tmp.item_nick_name.split(",");
            for (String tmp_arr1 : tmp_arr) {
                LabList tmp2 = (LabList) theLabListV.get(i).clone();
                if (hm.isEmpty()) {
                    hm.put(tmp_arr1, tmp_arr1);
                    tmp2.item_nick_name = tmp_arr1;
                    theLabListVTmp.add(tmp2);
                    if (!index.isEmpty()) {
                        index += "-" + tmp_arr1;
                    } else {
                        index = tmp_arr1;
                    }
                }
                if (hm.get(tmp_arr1) == null) {
                    hm.put(tmp_arr1, tmp_arr1);
                    tmp2.item_nick_name = tmp_arr1;
                    if (!index.isEmpty()) {
                        index += "-" + tmp_arr1;
                    } else {
                        index = tmp_arr1;
                    }
                    theLabListVTmp.add(tmp2);
                }
            }
        }
        return theLabListVTmp;
    }

    public List<ComplexDataSource> listLNByVN(String patientId, String vn, Date startDate, Date endDate) {
        List<ComplexDataSource> results = new ArrayList<ComplexDataSource>();
        theHosControl.theConnectionInf.open();
        try {
            theHosControl.theConnectionInf.getConnection().setAutoCommit(false);
            String sql = "select distinct\n"
                    + "        t_patient.patient_hn as hn\n"
                    + "        ,t_visit.visit_vn as vn\n"
                    + "        ,text_to_timestamp(t_visit.visit_begin_visit_time) as visit_datetime\n"
                    + "        ,t_lis_ln.lab_number as ln\n"
                    + "        ,t_lis_ln.t_visit_id\n"
                    + "from t_lis_ln inner join t_lis_order on t_lis_ln.lab_number =  t_lis_order.lab_number\n"
                    + "        inner join t_order on t_lis_order.t_order_id = t_order.t_order_id\n"
                    + "                                    and t_order.f_order_status_id not in ('0','3')\n"
                    + "        inner join t_visit on t_lis_ln.t_visit_id = t_visit.t_visit_id\n"
                    + "        inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id\n"
                    + "where\n"
                    + "        t_patient.t_patient_id = ?\n";
            if (vn != null && !vn.isEmpty()) {
                sql += "        and (t_visit.visit_vn ilike ? or substr(t_visit.visit_vn,4)::int::text||'/'||substr(t_visit.visit_vn,1,3)::int::text ilike ? )\n";
            }
            if (startDate != null && endDate != null) {
                sql += "      and text_to_timestamp(t_visit.visit_begin_visit_time)::date between ?::date and ?::date \n";
            }
            sql += "order by\n"
                    + "        visit_datetime desc\n"
                    + "        ,ln asc";
            PreparedStatement ps = theHosControl.theConnectionInf.ePQuery(sql);
            int index = 1;
            ps.setString(index++, patientId);
            if (vn != null && !vn.isEmpty()) {
                ps.setString(index++, "%" + vn + "%");
                ps.setString(index++, "%" + vn + "%");
            }
            if (startDate != null && endDate != null) {
                ps.setString(index++, DateUtil.convertDateToString(startDate, "yyyy-MM-dd", Locale.US));
                ps.setString(index++, DateUtil.convertDateToString(endDate, "yyyy-MM-dd", Locale.US));
            }
            List<Object[]> list = theHosControl.theConnectionInf.eComplexQuery(ps.toString());
            theHosControl.theConnectionInf.getConnection().commit();
            for (Object[] objects : list) {
                results.add(new ComplexDataSource(String.valueOf(objects[3]), new Object[]{
                    String.valueOf(objects[1]),
                    objects[2],
                    String.valueOf(objects[3])
                }));
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return results;
    }

    public List<ComplexDataSource> listOrderLabByPatientId(String patientId) {
        List<ComplexDataSource> ds = new ArrayList<ComplexDataSource>();
        PreparedStatement preparedStatement = null;
        theHosControl.theConnectionInf.open();
        try {
            theHosControl.theConnectionInf.getConnection().setAutoCommit(false);
            String sql = "select\n"
                    + "b_item.b_item_id\n"
                    + ",b_item.item_common_name\n"
                    + ",max(text_to_timestamp(visit_previous.visit_begin_visit_time)) as visit_datetime\n"
                    + "from t_patient \n"
                    + "inner join t_visit as visit_previous on t_patient.t_patient_id = visit_previous.t_patient_id\n"
                    + "and visit_previous.f_visit_status_id <> '4'\n"
                    + "inner join t_order on visit_previous.t_visit_id = t_order.t_visit_id\n"
                    + "and t_order.f_order_status_id in ('2','4','6','7')\n"
                    + "and t_order.f_item_group_id = '2'\n"
                    + "left join b_item on t_order.b_item_id = b_item.b_item_id\n"
                    + "where\n"
                    + "t_patient.t_patient_id = ?\n"
                    + "group by\n"
                    + "b_item.b_item_id\n"
                    + ",b_item.item_common_name\n"
                    + "order by\n"
                    + "item_common_name";
            preparedStatement = theHosControl.theConnectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, patientId);
            List<Object[]> eComplexQuery = theHosControl.theConnectionInf.eComplexQuery(preparedStatement.toString());
            for (Object[] objects : eComplexQuery) {
                ds.add(new ComplexDataSource(objects[0], // b_item_id
                        new Object[]{
                            String.valueOf(objects[1]), // common name
                            objects[2] // visit date time
                        }));
            }
            theHosControl.theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theHosControl.theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex);
                }
            }
            theHosControl.theConnectionInf.close();
        }
        return ds;
    }

    public List<ComplexDataSource> listOrderLabByVisitId(String visitId) {
        List<ComplexDataSource> ds = new ArrayList<ComplexDataSource>();
        PreparedStatement preparedStatement = null;
        theHosControl.theConnectionInf.open();
        try {
            theHosControl.theConnectionInf.getConnection().setAutoCommit(false);
            String sql = "select\n"
                    + "        b_item.b_item_id\n"
                    + "        ,b_item.item_common_name\n"
                    + "        ,max(text_to_timestamp(visit_previous.visit_begin_visit_time)) as visit_datetime\n"
                    + "from t_visit left join t_visit as visit_previous on t_visit.t_patient_id = visit_previous.t_patient_id\n"
                    + "             and text_to_timestamp(t_visit.visit_begin_visit_time) >= text_to_timestamp(visit_previous.visit_begin_visit_time)\n"
                    + "             and visit_previous.f_visit_status_id <> '4'\n"
                    + "        inner join t_order on visit_previous.t_visit_id = t_order.t_visit_id\n"
                    + "             and t_order.f_order_status_id in ('2','4','6','7')\n"
                    + "             and t_order.f_item_group_id = '2'\n"
                    + "        left join b_item on t_order.b_item_id = b_item.b_item_id\n"
                    + "where\n"
                    + "        t_visit.t_visit_id = ?\n"
                    + "group by\n"
                    + "        b_item.b_item_id\n"
                    + "        ,b_item.item_common_name\n"
                    + "order by\n"
                    + "        item_common_name";
            preparedStatement = theHosControl.theConnectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, visitId);
            List<Object[]> eComplexQuery = theHosControl.theConnectionInf.eComplexQuery(preparedStatement.toString());
            for (Object[] objects : eComplexQuery) {
                ds.add(new ComplexDataSource(objects[0], // b_item_id
                        new Object[]{
                            String.valueOf(objects[1]), // common name
                            objects[2] // visit date time
                        }));
            }
            theHosControl.theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theHosControl.theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex);
                }
            }
            theHosControl.theConnectionInf.close();
        }
        return ds;
    }

    public boolean checkMapEpidemLabTypeByItemId(String id) {
        boolean isMap = false;
        PreparedStatement preparedStatement = null;
        theHosControl.theConnectionInf.open();
        try {
            theHosControl.theConnectionInf.getConnection().setAutoCommit(false);
            String sql = "select * from b_map_epidem_lab_type where b_item_id = ?";
            preparedStatement = theHosControl.theConnectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, id);
            List<Object[]> eComplexQuery = theHosControl.theConnectionInf.eComplexQuery(preparedStatement.toString());
            isMap = (eComplexQuery != null && !eComplexQuery.isEmpty());
            theHosControl.theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theHosControl.theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex);
                }
            }
            theHosControl.theConnectionInf.close();
        }
        return isMap;
    }

    public CovidTmlt selectCovidTmlt(String code) {
        CovidTmlt covidTmlt = null;
        theHosControl.theConnectionInf.open();
        try {
            theHosControl.theConnectionInf.getConnection().setAutoCommit(false);
            covidTmlt = theHosControl.theHosDB.theCovidTmltDB.selectByCode(code);
            theHosControl.theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theHosControl.theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theHosControl.theConnectionInf.close();
        }
        return covidTmlt;
    }
}

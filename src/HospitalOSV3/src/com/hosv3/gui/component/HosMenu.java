/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.gui.component;

import com.hospital_os.object.OrderItem;
import com.hosv3.control.HosControl;
import com.hosv3.gui.panel.transaction.inf.PanelOrderInf;
import com.hosv3.object.HosObject;
import com.hosv3.utility.GuiLang;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Logger;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

/**
 *
 * @author henbe
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class HosMenu extends JMenu {

    public static String PRINT_PATH = System.getProperty("user.dir") + File.separator + "hprinting";
    private Vector vMenu = new Vector();
    private final JCheckBoxMenuItem jCheckBoxMenuItemPreview;
    private final JCheckBoxMenuItem jCheckBoxMenuItemChoosePrinter;
    private HosObject theHO;
    private HosControl theHC;

    public HosMenu() {
        super();
        jCheckBoxMenuItemPreview = new javax.swing.JCheckBoxMenuItem();
        jCheckBoxMenuItemChoosePrinter = new javax.swing.JCheckBoxMenuItem();
        jCheckBoxMenuItemPreview.setSelected(true);
        jCheckBoxMenuItemPreview.setText(GuiLang.setLanguage("�ʴ��Ҿ��͹�����"));
        jCheckBoxMenuItemChoosePrinter.setSelected(true);
        jCheckBoxMenuItemChoosePrinter.setText(GuiLang.setLanguage("���͡����ͧ�����"));
        this.add(jCheckBoxMenuItemPreview);
        this.add(jCheckBoxMenuItemChoosePrinter);
    }

    public void setControl(HosControl hc) {
        theHC = hc;
        theHO = hc.theHO;
    }

    public void initPrintMenu(String path) {
        PRINT_PATH = path;
        File file = new File(PRINT_PATH);
        File[] files = file.listFiles();
        if (files == null) {
            return;
        }
        for (int i = 0; i < files.length; i++) {
            if (files[i].isFile() && (files[i].getName().endsWith(".xml") || files[i].getName().endsWith(".jrxml"))) {
                JMenuItem jmi = new JMenuItem(readMenuName(files[i]));
                jmi.addActionListener(new java.awt.event.ActionListener() {

                    @Override
                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        jMenuItem1ActionPerformed(evt);
                    }
                });
                add(jmi);
            }
        }
    }

    public String readMenuName(File filename) {
        try {
            StringBuilder buffer = new StringBuilder();
            FileInputStream fis = new FileInputStream(filename.getAbsolutePath());
            InputStreamReader isr = new InputStreamReader(fis, "UTF8");
            Reader in = new BufferedReader(isr);
            int ch;
            while ((ch = in.read()) > -1) {
                buffer.append((char) ch);
            }
            in.close();
            String data = buffer.toString();
            int index1 = data.indexOf("<MENU_NAME>") + 11;
            int index2 = data.indexOf("</MENU_NAME>");
            String name = filename.getName();
            if (index1 != -1 && index2 != -1) {
                name = data.substring(index1, index2);
            }
            vMenu.add(new String[]{name, filename.getName()});
            return name;
        } catch (IOException ex) {
            return filename.getName();
        }
    }

    private String getMenuFileName(String name) {
        for (int i = 0; i < vMenu.size(); i++) {
            String[] str = (String[]) vMenu.get(i);
            if (str[0].equals(name)) {
                return str[1];
            }
        }
        return null;
    }

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {
        JMenuItem jmi = (JMenuItem) evt.getSource();
        String file_menu = getMenuFileName(jmi.getText());
        Map map = new HashMap();
        if (theHO.theVisit != null) {
            map.put("visit_id", theHO.theVisit.getObjectId());
        }
        if (theHO.thePatient != null) {
            map.put("patient_id", theHO.thePatient.getObjectId());
        }
        if (theHO.theFamily != null) {
            map.put("family_id", theHO.theFamily.getObjectId());
        }
        if (theHO.theHome != null) {
            map.put("home_id", theHO.theHome.getObjectId());
        }

        List<String> selectedOrderIds = new ArrayList<String>();
        if (theHC.theHP.aPanelOrder != null) {
            Vector<OrderItem> vOr = ((PanelOrderInf) theHC.theHP.aPanelOrder).getOrderItemV();
            for (OrderItem oi : vOr) {
                selectedOrderIds.add(oi.getObjectId());
            }
            map.put("selected_order_ids", selectedOrderIds);
        }

        theHC.theConnectionInf.open();
        try {
            theHC.theConnectionInf.getConnection().setAutoCommit(false);
            theHC.thePrintControl.printReport(jCheckBoxMenuItemPreview.isSelected()
                    ? 1
                    : 0,
                    new File(PRINT_PATH, file_menu), map, theHC.theConnectionInf.getConnection());

            theHC.theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theHC.theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theHC.theConnectionInf.close();
        }
    }
}

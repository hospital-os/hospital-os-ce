/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.SubInscl;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class SubInsclDB {

    private final ConnectionInf connectionInf;

    public SubInsclDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public SubInscl select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_subinscl where f_subinscl_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<SubInscl> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<SubInscl> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_subinscl where active = '1' order by f_subinscl_id";
            preparedStatement = connectionInf.ePQuery(sql);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<SubInscl> list(String... ids) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_subinscl where f_subinscl_id in (%s) and active = '1' order by f_subinscl_id";
            StringBuilder sb = new StringBuilder();
            for (String id : ids) {
                sb.append("'").append(id).append("',");
            }
            preparedStatement = connectionInf.ePQuery(String.format(sql,
                    sb.toString().substring(0, sb.toString().length() - 1)));
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<SubInscl> listByKeyword(String keyword) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_subinscl where upper(description) like upper(?) and active = '1' order by description";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, "%" + (keyword == null || keyword.isEmpty() ? "" : keyword) + "%");
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<SubInscl> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<SubInscl> list = new ArrayList<SubInscl>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                SubInscl obj = new SubInscl();
                obj.setObjectId(rs.getString("f_subinscl_id"));
                obj.description = rs.getString("description");
                obj.active = rs.getString("active");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (SubInscl obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }

    public List<CommonInf> getComboboxDatasource(String... ids) throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (SubInscl obj : list(ids)) {
            list.add((CommonInf) obj);
        }
        return list;
    }
}

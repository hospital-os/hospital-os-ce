/*
 * AppointmentTemplateDB.java
 *
 * Created on 10 �ԧ�Ҥ� 2549, 13:47 �.
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.*;
import com.hospital_os.usecase.connection.*;
import com.hospital_os.utility.*;
import java.sql.*;
import java.util.*;

/**
 *
 * @author amp
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class AppointmentTemplateDB {

    public ConnectionInf theConnectionInf;
    public AppointmentTemplate dbObj;
    final public String idtable = "293";

    /**
     * Creates a new instance of AppointmentTemplateDB
     */
    public AppointmentTemplateDB(ConnectionInf db) {
        theConnectionInf = db;
        dbObj = new AppointmentTemplate();
        initConfig();
    }

    private boolean initConfig() {
        dbObj.table = "b_template_appointment";
        dbObj.pk_field = "b_template_appointment_id";
        dbObj.template_name = "template_appointment_name";
        dbObj.time = "template_appointment_time";
        dbObj.time_end = "template_appointment_end_time";
        dbObj.aptype = "template_appointment_aptype";
        dbObj.service_point = "template_appointment_service_point";
        dbObj.doctor = "template_appointment_doctor";
        dbObj.clinic = "template_appointment_clinic";
        dbObj.description = "template_appointment_description";
        dbObj.queue_visit_id = "template_appointment_queue_visit_id";
        dbObj.appoint_staff_record = "template_appointment_staff_record";
        dbObj.appoint_record_date_time = "template_appointment_record_date_time";
        dbObj.appoint_staff_update = "template_appointment_staff_update";
        dbObj.appoint_update_date_time = "template_appointment_update_date_time";
        dbObj.appointment_type = "template_appointment_type";
        dbObj.auto_visit = "template_appointment_auto_visit";
        return true;
    }

    public int insert(AppointmentTemplate o) throws Exception {
        AppointmentTemplate p = o;
        p.generateOID(idtable);
        String sql = "insert into " + dbObj.table + " ("
                + dbObj.pk_field
                + " ," + dbObj.template_name
                + " ," + dbObj.time
                + " ," + dbObj.time_end
                + " ," + dbObj.aptype
                + " ," + dbObj.service_point
                + " ," + dbObj.doctor
                + " ," + dbObj.clinic
                + " ," + dbObj.description
                + " ," + dbObj.queue_visit_id
                + " ," + dbObj.appoint_staff_record
                + " ," + dbObj.appoint_record_date_time
                + " ," + dbObj.appoint_staff_update
                + " ," + dbObj.appoint_update_date_time
                + " ," + dbObj.auto_visit
                + " ," + dbObj.appointment_type
                + " , template_appointment_next_day"
                + " , template_appointment_use_set"
                + " , template_appointment_times"
                + " , template_appointment_detail"
                + " , template_appointment_set_name"
                + " ) values ('"
                + p.getObjectId()
                + "','" + p.template_name
                + "','" + p.time
                + "','" + p.time_end
                + "','" + Gutil.CheckReservedWords(p.aptype)
                + "','" + p.service_point
                + "','" + p.doctor
                + "','" + p.clinic
                + "','" + Gutil.CheckReservedWords(p.description)
                + "'," + (p.queue_visit_id == null ? "NULL" : "'" + p.queue_visit_id + "'")
                + ",'" + p.appoint_staff_record
                + "','" + p.appoint_record_date_time
                + "','" + p.appoint_staff_update
                + "','" + p.appoint_update_date_time
                + "','" + p.auto_visit
                + "','" + p.appointment_type
                + "'," + p.next_day
                + "," + p.template_appointment_use_set
                + "," + p.template_appointment_times
                + "," + (p.template_appointment_detail == null || p.template_appointment_detail.isEmpty() ? "NULL" : "'" + p.template_appointment_detail + "'::jsonb")
                + ",'" + p.template_appointment_set_name
                + "')";
        sql = Gutil.convertSQLToMySQL(sql, theConnectionInf.gettypeDatabase());
        return theConnectionInf.eUpdate(sql);
    }

    public int update(AppointmentTemplate o) throws Exception {
        String sql = "update " + dbObj.table + " set ";
        AppointmentTemplate p = o;
        String field = ""
                + "', " + dbObj.template_name + "='" + p.template_name
                + "', " + dbObj.time + "='" + p.time
                + "', " + dbObj.time_end + "='" + p.time_end
                + "', " + dbObj.aptype + "='" + Gutil.CheckReservedWords(p.aptype)
                + "', " + dbObj.service_point + "='" + p.service_point
                + "', " + dbObj.doctor + "='" + p.doctor
                + "', " + dbObj.clinic + "='" + p.clinic
                + "', " + dbObj.description + "='" + Gutil.CheckReservedWords(p.description)
                + "', " + dbObj.queue_visit_id + "=" + (p.queue_visit_id == null ? "NULL" : "'" + p.queue_visit_id + "'")
                + ", " + dbObj.appoint_staff_record + "='" + p.appoint_staff_record
                + "', " + dbObj.appoint_record_date_time + "='" + p.appoint_record_date_time
                + "', " + dbObj.appoint_staff_update + "='" + p.appoint_staff_update
                + "', " + dbObj.appoint_update_date_time + "='" + p.appoint_update_date_time
                + "', " + dbObj.auto_visit + "='" + p.auto_visit
                + "', " + dbObj.appointment_type + "='" + p.appointment_type
                + "', template_appointment_next_day= " + p.next_day
                + ", template_appointment_use_set= " + p.template_appointment_use_set
                + ", template_appointment_times= " + p.template_appointment_times
                + ", template_appointment_detail= " + (p.template_appointment_detail == null || p.template_appointment_detail.isEmpty() ? "NULL" : "'" + p.template_appointment_detail + "'::jsonb")
                + ", template_appointment_set_name= '" + p.template_appointment_set_name + "'"
                + " where " + dbObj.pk_field + "='" + p.getObjectId() + "'";
        sql = Gutil.convertSQLToMySQL(sql + field.substring(2), theConnectionInf.gettypeDatabase());
        return theConnectionInf.eUpdate(sql);
    }

    public int delete(AppointmentTemplate o) throws Exception {
        String sql = "delete from " + dbObj.table
                + " where " + dbObj.pk_field + "='" + o.getObjectId() + "'";
        return theConnectionInf.eUpdate(sql);
    }

    /**
     * @Author : amp
     * @date : 10/08/2549
     * @see : ���ҵ�Ǫ��¹Ѵ
     * @param : ����
     * @return : Vector ��Ǫ��¹Ѵ
     */
    public Vector selectAppointmentTemplate(String name) throws Exception {
        String sql = "select * from " + dbObj.table;
        if (!name.isEmpty()) {
            sql = sql + " where ( UPPER(" + dbObj.template_name + ") like UPPER('%" + Gutil.CheckReservedWords(name) + "%'))";
        }
        sql = sql + " order by " + dbObj.template_name;

        return eQuery(sql);
    }

    /**
     * @Author : amp
     * @date : 10/08/2549
     * @see : ���ҵ�Ǫ��¹Ѵ
     * @param : key_id
     * @return : Vector ��Ǫ��¹Ѵ
     */
    public AppointmentTemplate selectAppointmentTemplateByPK(String pk) throws Exception {
        String sql = "select * from " + dbObj.table
                + " where " + dbObj.pk_field + " = '" + pk + "'";

        Vector v = eQuery(sql);
        if (v.isEmpty()) {
            return null;
        } else {
            return (AppointmentTemplate) v.get(0);
        }
    }

    public Vector eQuery(String sql) throws Exception {
        AppointmentTemplate p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            p = new AppointmentTemplate();
            p.setObjectId(rs.getString(dbObj.pk_field));
            p.template_name = rs.getString(dbObj.template_name);
            p.time = rs.getString(dbObj.time);
            p.time_end = rs.getString(dbObj.time_end);
            p.aptype = rs.getString(dbObj.aptype);
            p.service_point = rs.getString(dbObj.service_point);
            p.doctor = rs.getString(dbObj.doctor);
            p.clinic = rs.getString(dbObj.clinic);
            p.description = rs.getString(dbObj.description);
            p.queue_visit_id = rs.getString(dbObj.queue_visit_id);
            p.appoint_staff_record = rs.getString(dbObj.appoint_staff_record);
            p.appoint_record_date_time = rs.getString(dbObj.appoint_record_date_time);
            p.appoint_staff_update = rs.getString(dbObj.appoint_staff_update);
            p.appoint_update_date_time = rs.getString(dbObj.appoint_update_date_time);
            p.auto_visit = rs.getString(dbObj.auto_visit);
            p.appointment_type = rs.getString(dbObj.appointment_type);
            p.next_day = rs.getInt("template_appointment_next_day");
            p.template_appointment_use_set = rs.getBoolean("template_appointment_use_set");
            p.template_appointment_times = rs.getInt("template_appointment_times");
            p.template_appointment_detail = rs.getString("template_appointment_detail");
            p.template_appointment_set_name = rs.getString("template_appointment_set_name");
            list.add(p);
        }
        rs.close();
        return list;
    }
}

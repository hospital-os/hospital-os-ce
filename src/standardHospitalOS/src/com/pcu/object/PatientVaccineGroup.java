/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.object;

import com.hospital_os.usecase.connection.Persistent;
import java.util.Date;

/**
 *
 * @author tanakrit
 */
public class PatientVaccineGroup extends Persistent {

    public String t_patient_id;
    public int f_vaccine_person_risk_type_id;
    public int need_vaccine;
    public boolean sent_data = false;
    public Date record_datetime;
    public String user_record_id;
    public Date update_datetime;
    public String user_update_id;

}

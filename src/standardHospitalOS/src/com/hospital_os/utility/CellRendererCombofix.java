/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.utility;

import java.awt.Component;
import java.util.Vector;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author Somprasong Damyos <somprasong@hospital-os.com>
 */
public class CellRendererCombofix implements TableCellRenderer {

    protected JComboBox comboBox;

    public CellRendererCombofix(Vector<ComboFix> comboFixs) {
        comboBox = new JComboBox();
        comboBox.setFont(comboBox.getFont());
        ComboboxModel.initComboBox(comboBox, comboFixs);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        if (value instanceof ComboFix) {
            comboBox.setSelectedItem((ComboFix) value);
        } else if (value instanceof Integer) {
            comboBox.setSelectedIndex((Integer) value);
        } else if (value instanceof String) {
            Gutil.setGuiData(comboBox, (String) value);
        }
        return comboBox;
    }

    @Override
    public String toString() {
        return Gutil.getGuiData(comboBox);
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.FPatientFamilyHistory;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class FPatientFamilyHistoryDB {

    private final ConnectionInf connectionInf;

    public FPatientFamilyHistoryDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public FPatientFamilyHistory select(int id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_patient_family_history where f_patient_family_history_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setInt(1, id);
            List<FPatientFamilyHistory> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<FPatientFamilyHistory> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_patient_family_history order by f_patient_family_history_id";
            preparedStatement = connectionInf.ePQuery(sql);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<FPatientFamilyHistory> listByKeyword(String keyword) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_patient_family_history where description ilike ? order by f_patient_family_history_id";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, "%" + (keyword == null || keyword.isEmpty() ? "" : keyword) + "%");
            preparedStatement.setString(2, "%" + (keyword == null || keyword.isEmpty() ? "" : keyword) + "%");
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<FPatientFamilyHistory> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<FPatientFamilyHistory> list = new ArrayList<FPatientFamilyHistory>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                FPatientFamilyHistory obj = new FPatientFamilyHistory();
                obj.f_patient_family_history_id = (rs.getInt("f_patient_family_history_id"));
                obj.description = rs.getString("description");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (FPatientFamilyHistory obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }
}

/*
 * HosModel.java
 *
 * Created on 9 ����Ҥ� 2548, 13:44 �.
 */
package com.hosv3.object;

import com.hospital_os.object.*;
import com.hospital_os.usecase.connection.Persistent;
import com.hosv3.object.printobject.PrintSelectDrugList;
import com.hosv3.utility.Constant;
import com.hosv3.utility.DateUtil;
import com.hosv3.utility.GuiLang;
import com.pcu.object.Family;
import com.pcu.object.Home;
import com.pcu.object.Village;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Logger;

/**
 *
 * @author administrator
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class HosObject {

    public static String DATE_VERIFY = GuiLang.setLanguage("�ѹ����׹�ѹ");
    public static String VERIFIER = GuiLang.setLanguage("����׹�ѹ");
    public static String EXECUTER = GuiLang.setLanguage("�����Թ���");
    public static String REPORTOR = GuiLang.setLanguage("�����§ҹ��");
    public static String DISPENSER = GuiLang.setLanguage("������");
    public static String CANCEL = GuiLang.setLanguage("���¡��ԡ");
    public static String ALLERGY = GuiLang.setLanguage("��������");
    public static String MAINAPP = "MAINAPP";
    public static String SETUPAPP = "SETUPAPP";
    public static String REPORTAPP = "REPORTAPP";
    //���Ǩ�ͺ����繤�Ǥ�ҧ���Ż�������
    public boolean flag;
    public boolean flagDoctorTreatment;
    public boolean is_admit = false;
    public boolean is_cancel_admit = false;
    public boolean is_sound_enabled = false;
    public boolean is_auto_report_bug = false;
    public boolean is_attach_file_to_mail = false;
    /**
     * ���ѧ�ѹ������˹���� MAINAPP ��� ����� Hos ��ѡ SETUPAPP ��� �����
     * Setup Hos REPORTAPP ��� ����� Report
     */
    public String running_program = "";
    /**
     * @deprecated �Դ��ѡ��èе�ͧ�� appointment ��ҹ�� parameter ��������
     *
     */
    public Hashtable theAppointment;//amp:25/02/2549 -> �纹Ѵ����ҵ���Ѵ ��� order �ͧ��ùѴ��ǧ˹�ҹ��
    /**
     * hospital_group
     */
    public Employee theEmployee;
    public GActionAuthV theGActionAuthV;
    public ListTransfer theListTransfer;
    public MapQueueVisit theMapQueueVisit;
    public LookupObject theLO;
    public String home_out_side;
    public Version theVersion;
    public String date_time;
    public DiagDoctorClinic theDiagDoctorClinic;
    public Ward theWard;
    public ServicePoint theServicePoint;
    /**
     * amp:thePatientHistory �纻Ѩ�������§,����ѵԤ�ͺ����,�ä��Шӵ�� ����ö
     * get ��һѨ�������§ ������� risk_factor ����ö get ��һ���ѵԤ�ͺ����
     * ������� family_history ����ö get ����ä��Шӵ�� �������
     * personal_disease main group
     */
    public Village theVillage;
    public Home theHome;
    public Family theFamily;
    public Patient thePatient;
    public Person thePerson;
    public Visit theVisit;
    /**
     * family group
     */
    public Death theDeath;
    public PatientFamilyHistory thePatientFamilyHistory;
    public Vector vBillingPatient;
    public Vector vDrugAllergy;
    public Vector vPatientPayment;
    public PatientXN thePatientXN;
    public Vector vRiskFactor;
    public Vector vPersonalDisease;
    public Vector vVisit;
    public Vector vNCD;
    public Vector vDrugAllergyStd;
    /**
     * visit group
     */
    public Vector vBilling;
    public Vector vBillingInvoice;
    public Vector vBillingVisit;
    public Vector vDiadIcd10Cancel;//amp:18/02/2549 -> �� keyid �ͧ diagIcd10 ���١¡��ԡ
    public Vector<X39Persistent> vDiagIcd10;
    public Vector vDiagIcd9;
    public Vector vGuide;
    public Vector vLabReferIn;
    public Vector vLabReferOut;
    public Vector vOrderCancel;//amp:18/02/2549 -> �� keyid �ͧ order ���١¡��ԡ
    public Vector vOrderItem;
    public Vector vOrderItemReceiveDrug;
    public Vector vPhysicalExam;
    public Vector vPrimarySymptom;
    public Vector vTransfer;
    public Vector vVisitPayment;
    public Vector vVitalSign;
    public VitalSign theVitalSign;
    public VitalTemplate theVitalTemplate;
    public Vector vOldVisitPayment = null;
    public Vector vMapVisitDx;
    public Vector vPastHistory;
    public Vector vHealthEducation;
    public Refer theReferIn;
    public Refer theReferOut;
    public Refer selectedReferOut; //  �ҡ������͡�ҡ���ҧ refer out
    public OrderItem theOrderItem;
    // SOmprasong add 241209
    public Surveil theSurveil;
    public Chronic theChronic;
    public VisitBed theVisitBed;
    /**
     * unknow group
     */
    public boolean theDxTemplateNew;
    public Vector vOrderDrugInteraction;//amp:27/03/2549 �纻���ѵԡ������ҷ���ջ�ԡ����ҡѹ
    public Vector vIcd9;// �� Icd9 �����Ѻ Balloon  sumo 29/03/2549
    public Vector vServicePoint;// �� ServicePoint �����Ѻ Balloon  sumo 28/04/2549
    public GuideAfterDxTransaction theGuideAfterDxTransaction;
    public PrintSelectDrugList thePrintSelectDrugList;
    public Receipt thePrintReceipt;
    public Site theSite;//��������Ǥ�Ѻ 14/01/2549 ��� sumo �ѧ��ͧ���������¨ش��੾������������
    public String labQueueRemain;
    public String orderSecret = "";//amp:07/03/2549 -> ��� order_id ���Ǩ�ͺ����鹤���Ż���Դ��������
    public String specimenCode = "";//amp:07/03/2549
//    /**
//     * @author henbe
//     * @deprecated henbe �ѹ���� vMapVisitDx
//     * ᷹���ǹ��������繵�ͧ���ѹ������ǡ���
//     */
    public Vector vDxTemplate;
    /**
     * ��㹡���к�������ա�����͡ ����ʴ� �Է�ԡ���ѡ�ҷ��١¡��ԡ�������
     * ����� false ��������͡ ����� true �����͡
     */
    public boolean showVisitPaymentCancel = false;
    /*
     * amp:04/04/2549 ����������������㹡�������ҷ����Ẻ�� + �ҡ���� 1
     * ���駢���㹡���Դ Dialog �������� ����� save �ѹ�� save
     * ���੾�е���á�������
     */
    public Vector vDrugAllergyTemp = null;
    /**
     * amp:27/03/2549:is_order:����������͡ item �������͡ order ������͡�ҡ
     * item ���� true ������͡�ҡ order ���� false
     * �������Ǩ�ͺ����ѧ��ͧ�����͹���� ���� drug interaction �ա�������
     * �� ��Ҥ�ԡ�ҡ item �е�ͧ�����͹��͹��� ���Ҥ�ԡ���͡�ҡ order
     * ������èТ����͹�ա����
     */
    public boolean is_order = false;
    /**
     * ��㹡���硢����ŷ�����Ţ xn ��ӡѹ �� String �ͧ HN
     *
     * @author padungrat(tong)
     * @date 21/04/2549,15:11
     */
    public Vector vSameXN;
    public Object theJasperViewer;
    /**
     * �� Item ������������ Map �ѺDx
     *
     * @author Pu
     * @date 09/08/2549
     */
    public Vector vItemDx;
    /**
     * �� ICD10 ������������ Map �Ѻ Chronic
     *
     * @author Pu
     * @date 09/09/2551
     */
    public Vector vICD10GCGroup;
    public Vector vICD10GCSpecifyCode;
    /**
     * �� ICD10 ������������ Map �Ѻ Surveil
     *
     * @author Pu
     * @date 10/09/2551
     */
    public Vector vICD10GSGroup;
    public Vector vICD10GSSpecifyCode;
    /**
     * �� Object �����ѧ�Ѵ�������㹢�й��
     */
    public Persistent theXObject;
    public Vector vXObject;
    public int[] vxo_index;
    public String[] target_db;
    public String[] local_db;
    public Family theFamilyFather;
    public Family theFamilyMother;
    public Family theFamilyCouple;
    public QueueVisit theQV;
    public String objectid;
    public int[] panel_order_selected_item;//������Ѻ Stock
    public int[] select_visit_dispense_stock;//������Ѻ Stock
    public Vector theQueueDispense2V;//������Ѻ Stock
    public G6PD theG6pd;
    public PatientPastVaccine thePatientPastVaccine;
    /*
     * ����Ѵ��������ش
     */
    public Appointment theFinalAppointMent;

    /**
     * �� ItemDrug ������������ Map �ѺDx
     */
    public List<DxTemplateMapItemRisk> vItemRiskDx;

    public PatientAdl thePatientAdl;
    public Patient2qPlus thePatient2qPlus;
    public boolean isQueueLab = false;

    /**
     * Creates a new instance of HosModel
     *
     * @param lo
     */
    public HosObject(LookupObject lo) {
        theLO = lo;
        theEmployee = new Employee();
        //theSite = new Site();
        theServicePoint = new ServicePoint();
        vDxTemplate = new Vector();
        theVitalSign = new VitalSign();
        theVitalTemplate = new VitalTemplate();
        flag = false;
        vGuide = new Vector();
        vOrderCancel = new Vector();
        vDiadIcd10Cancel = new Vector();
        theDiagDoctorClinic = new DiagDoctorClinic();
        theG6pd = new G6PD();
        thePatientPastVaccine = new PatientPastVaccine();
    }

    public void initSite() {
        theSite = theLO.theSite;
    }

    ///////////////////////////////////////////////////////////////////////////
    /*
     * getEmployeeUnDefine @Author : henbe pongtorn @date : 21/03/2549 @see :
     * ����� ����к� ����Ѻ���� combobox �ͧ Employee,Doctor,Nurse
     */
    public static Employee getEmployeeUD() {
        Employee undefine = new Employee();
        undefine.setObjectId("");
        undefine.person.person_firstname = Constant.getTextBundle("����к�");
        undefine.person.person_lastname = "";
        return undefine;
    }

    ///////////////////////////////////////////////////////////////////////////
    /*
     * get ServicePoint UnDefine @Author : henbe pongtorn @date : 21/03/2549
     * @see : ����� ����к� ����Ѻ���� combobox
     */
    public static ServicePoint getServicePointUD() {
        ServicePoint p = new ServicePoint();
        p.setObjectId("");
        p.name = Constant.getTextBundle("����к�");
        return p;
    }

    public void printValue() {

        LOG.log(java.util.logging.Level.INFO, "thePatient==null{0}", (thePatient == null));
        LOG.log(java.util.logging.Level.INFO, "theVisit==null{0}", (theVisit == null));
        LOG.log(java.util.logging.Level.INFO, "theListTransfer==null{0}", (theListTransfer == null));
        LOG.log(java.util.logging.Level.INFO, "thePatient==null{0}", (thePatient == null));
        LOG.log(java.util.logging.Level.INFO, "theServicePoint2==null{0}", (theServicePoint == null));
        LOG.log(java.util.logging.Level.INFO, "theVersion==null{0}", (theVersion == null));
        LOG.log(java.util.logging.Level.INFO, "theEmployee==null{0}", (theEmployee == null));
    }

    public void clearPatient() {
        thePatient = null;
        theDeath = null;
        thePatientXN = null;
        vDrugAllergy = null;
        vPastHistory = null;
        vDrugAllergy = null;
        vPatientPayment = null;
        vBillingPatient = null;
        vNCD = null;
        vPersonalDisease = null;
        vDrugAllergyStd = null;
        vRiskFactor = null;
        thePatientFamilyHistory = null;
        vPersonalDisease = null;
        vVisit = null;
        theG6pd = null;
        thePatientPastVaccine = null;
        thePatientAdl = null;
        thePatient2qPlus = null;
        clearVisit();
    }

    public void clearFamily() {
        thePerson = null;
        theFamily = null;
        theVillage = null;
        theHome = null;
        theFamilyFather = null;
        theFamilyMother = null;
        theFamilyCouple = null;
        clearPatient();
    }

    /**
     *
     * ������������ù��������ջ���ª������������
     * �ѹ��ѧ���͡������������������ henbe
     *
     * @param pt
     * @not deprecated henbe said bad pattern
     * �ѹ�繡�úѹ�֡�����¤����á�ѧ��鹢��������� ��������繤����ҧ
     * ��������� clearFamily() ���
     */
    public void setPatient(Patient pt) {
        //�ѧ��������Ҩ��պѡ���ش���� �ա�������ҵ�ͧ�礴�
        //theFamily = null;
        //theVillage = null;
        //theHome = null;
        this.clearPatient();
        thePatient = pt;
        if (theFamily == null) {
            vPatientPayment = null;
        }

        clearVisit();
    }

    public void clearVisit() {
        orderSecret = "";
        specimenCode = "";
        theVisit = null;
        theAppointment = null;
        theGuideAfterDxTransaction = null;
        theListTransfer = null;
        theReferIn = null;
        theReferOut = null;
        theVitalSign = null;
        theOrderItem = null;
        vBilling = null;
        vBillingInvoice = null;
        vDiadIcd10Cancel = null;
        vDiagIcd10 = null;
        vDiagIcd9 = null;
        vDxTemplate = null;
        vGuide = null;
        vHealthEducation = null;
        vMapVisitDx = null;
        vOrderCancel = null;
        vOrderDrugInteraction = null;
        vOrderItem = null;
        vOrderItemReceiveDrug = null;
        vPhysicalExam = null;
        vPrimarySymptom = null;
        vTransfer = null;
        vVisitPayment = null;
        vVitalSign = null;
        // SOmprasong add 241209
        theSurveil = null;
        theChronic = null;
        theVisitBed = null;
    }

    public void initVisitExt() {
        vBilling = new Vector();
        vBillingInvoice = new Vector();
        vDiadIcd10Cancel = new Vector();
        vDiagIcd10 = new Vector<X39Persistent>();
        vDiagIcd9 = new Vector();
        vDxTemplate = new Vector();
        vGuide = new Vector();
        vHealthEducation = new Vector();
        vMapVisitDx = new Vector();
        vOrderCancel = new Vector();
        vOrderDrugInteraction = new Vector();
        vOrderItem = new Vector();
        vOrderItemReceiveDrug = new Vector();
        vPhysicalExam = new Vector();
        vPrimarySymptom = new Vector();
        vTransfer = new Vector();
        vVisitPayment = new Vector();
        vVitalSign = new Vector();
    }

    public void setFamily(Family fm) {
        clearPatient();
        theFamily = fm;
    }

    public void setVisit(Visit visit) {
        clearVisit();
        theVisit = visit;
    }

    public Home initHome(String number, Village vill) {
        if (vill == null) {
            return null;
        }
        Home home = new Home();
        home.home_number = number;
        home.home_house = number;
        home.home_record_date_time = date_time;
        home.home_staff_record = theEmployee.getObjectId();
        home.village_id = vill.getObjectId();
        home.home_moo = vill.village_moo;
        home.home_amphur = vill.village_ampur;
        home.home_changwat = vill.village_changwat;
        home.home_tambol = vill.village_tambon;
        return home;
    }

    public Village initVillage(String moo) {
        Village vill = new Village();
        vill.village_number = moo;
        vill.village_name = moo;
        vill.village_moo = moo;
        vill.village_ampur = theLO.theSite.amphor;
        vill.village_tambon = theLO.theSite.tambon;
        vill.village_changwat = theLO.theSite.changwat;
        vill.village_record_date_time = date_time;
        vill.village_staff_record = theEmployee.getObjectId();
        return vill;
    }

    public DiagIcd10 initDiagIcd10() {
        DiagIcd10 theDiagIcd10 = new DiagIcd10();
        theDiagIcd10.doctor_kid = getDoctorIDInVisit();
        theDiagIcd10.clinic_kid = "";
        if (theDiagIcd10.doctor_kid.length() == 0
                && vDiagIcd10 != null && !vDiagIcd10.isEmpty()) {
            DiagIcd10 dx10 = (DiagIcd10) vDiagIcd10.get(0);
            theDiagIcd10.doctor_kid = dx10.doctor_kid;
            theDiagIcd10.clinic_kid = dx10.clinic_kid;
        }

        if (vDiagIcd10 != null && vDiagIcd10.size() > 0) {
            theDiagIcd10.type = Dxtype.getComorbidityDiagnosis();
        } else {
            theDiagIcd10.type = Dxtype.getPrimaryDiagnosis();
        }

        theDiagIcd10.diagnosis_date = date_time.substring(0, 10);
        theDiagIcd10.dischange_note = "";
        theDiagIcd10.diag_icd10_staff_record = theEmployee.getObjectId();
        theDiagIcd10.diag_icd10_record_date_time = date_time;

        return theDiagIcd10;
    }

    public DiagIcd9 initDiagIcd9(ICD9 icd9, DiagDoctorClinic ddc) {

        DiagIcd9 theDiagIcd9 = initDiagIcd9();
        theDiagIcd9.icd9_code = icd9.icd9_id;
        if (ddc != null) {
            theDiagIcd9.clinic_kid = ddc.clinic_id;
            theDiagIcd9.doctor_kid = ddc.doctor_id;
        }
        //��ѡ����
        if (theDiagIcd9.doctor_kid == null || theDiagIcd9.doctor_kid.isEmpty()) {
            theDiagIcd9.doctor_kid = getDoctorIDInVisit();
        }
        // ��Ǩ�ͺ��� DiagIcd9 ���ѹ�֡��ջ������� Principal ������� sumo 05/09/2549
        boolean have_principal = false;
        boolean have_second = false;
        for (int k = 0; k < vDiagIcd9.size(); k++) {
            DiagIcd9 diagicd9 = (DiagIcd9) vDiagIcd9.get(k);
            if (diagicd9.type.equals(Optype.PRINCIPAL)) {
                have_principal = true;
            }
            if (diagicd9.type.equals(Optype.SECONDARY)) {
                have_second = true;
            }
        }
        if (!have_principal) {
            theDiagIcd9.type = Optype.PRINCIPAL;
        } else if (!have_second) {
            theDiagIcd9.type = Optype.SECONDARY;
        } else {
            theDiagIcd9.type = Optype.OTHER;
        }
        theDiagIcd9.time_in = date_time;
        theDiagIcd9.time_out = date_time;
        return theDiagIcd9;
    }

    public DiagIcd9 initDiagIcd9() {
        DiagIcd9 theDiagIcd9 = new DiagIcd9();
        theDiagIcd9.time_in = date_time;
        theDiagIcd9.time_out = date_time;
        theDiagIcd9.diag_icd9_staff_record = theEmployee.getObjectId();
        theDiagIcd9.diag_icd9_record_date_time = date_time;
        theDiagIcd9.vn = theVisit.getObjectId();
        theDiagIcd9.diag_icd9_active = "1";
        theDiagIcd9.clinic_kid = "";
        if (vTransfer != null) {
            for (int i = vTransfer.size() - 1; i >= 0; i--) {
                Transfer theTransfer = (Transfer) vTransfer.get(i);
                if (!theTransfer.doctor_code.isEmpty()
                        && !theTransfer.doctor_code.equalsIgnoreCase("null")) {
                    theDiagIcd9.doctor_kid = theTransfer.doctor_code;
                    break;
                }
            }
        }
        return theDiagIcd9;
    }

    public VitalSign initVitalSign() {
        VitalSign vitalSign = new VitalSign();
        vitalSign.weight = "";
        vitalSign.bmi = "";
        vitalSign.height = "";
        vitalSign.pressure = "/";
        vitalSign.temp = "";
        vitalSign.puls = "";
        vitalSign.res = "";
        vitalSign.note = "";

        //amp:05/04/2549
        vitalSign.check_date = date_time.substring(0, 10);
        vitalSign.check_time = date_time.substring(11, 16);
        vitalSign.staff_modify = "";
        vitalSign.modify_date_time = "";
        vitalSign.active = "1";

        vitalSign.nutrition = NutritionType.NORMAL;
        if (theVisit != null) {
            vitalSign.visit_id = theVisit.getObjectId();
        }
        if (thePatient != null) {
            vitalSign.patient_id = thePatient.getObjectId();
        }
        return vitalSign;
    }

    public Accident initAccident(String date) {
        Accident acc = new Accident();
        acc.vn_id = "";
        acc.vn = "";
        if (theVisit != null) {
            acc.vn_id = theVisit.getObjectId();
            acc.vn = theVisit.vn;
        }
        acc.patient_id = "";
        acc.hn = "";
        if (thePatient != null) {
            acc.patient_id = thePatient.getObjectId();
            acc.hn = thePatient.hn;
        }
        acc.acctb = theLO.theSite.tambon;
        acc.accam = theLO.theSite.amphor;
        acc.acccw = theLO.theSite.changwat;
        acc.acctime = "";
        acc.date_accident = date_time.substring(0, 10);
        acc.acctime = date_time.substring(11, 16);
        acc.to_hos_date = date_time.substring(0, 10);
        acc.to_hos_time = date_time.substring(11, 16);
        acc.name_rd = "";
        acc.in_out = "";
        acc.kilo = "";
        acc.accmu = "";
        acc.ptstatus = "";
        acc.ptmobie = "0";
        acc.acc_use = "0";
        acc.acc_alc = "0";
        acc.acc_pro = "0";
        acc.reporter = "";
        acc.accident_type = "";
        acc.occur_type = "";
        acc.emergency_type = "";
        acc.cost_detail = "";
        acc.hitch_constitution = "0";
        acc.claim_code = "";
        acc.f_accident_place_id = "99";
        acc.f_accident_visit_type_id = "9";
        acc.accident_narcotic = "9";
        return acc;
    }

    public Payment initPayment(Plan p) {
        return initPayment(p, date_time.substring(0, 10), theSite.off_id, theVisit);
    }

    public Payment initPayment(Plan p, String date, String off_id, Visit visit) {
        Payment thePaymentNow = new Payment();
        thePaymentNow.plan_kid = p.getObjectId();
        if (visit != null) {
            thePaymentNow.visit_id = visit.getObjectId();
        }
        thePaymentNow.card_id = "";
        thePaymentNow.contract_kid = p.contract_id;
        thePaymentNow.b_tariff_id = p.b_tariff_id;
        thePaymentNow.money_limit = p.money_limit;
        thePaymentNow.use_money_limit = "0";
        thePaymentNow.card_ins_date = date;
        thePaymentNow.card_exp_date = "";
        thePaymentNow.hosp_main = off_id;
        try {
            if (Double.parseDouble(p.money_limit) > 0) {
                thePaymentNow.use_money_limit = "1";
            }
        } catch (NumberFormatException ex) {
        }
        return thePaymentNow;
    }

    public OrderItemDrug initOrderItemDrug(DoseDrugSet doseDrugSet, Uom2 uom, DrugFrequency2 freq) {
        OrderItemDrug oid = new OrderItemDrug();
        oid.instruction = doseDrugSet.instruction;
        oid.printing = doseDrugSet.printting;
        oid.purch_uom = doseDrugSet.purch_uom;
        oid.usage_special = doseDrugSet.usage_special;
        oid.usage_text = doseDrugSet.usage_text;
        oid.use_uom = doseDrugSet.use_uom;
        oid.frequency = doseDrugSet.frequency;
        oid.caution = doseDrugSet.caution;
        oid.day_time = doseDrugSet.day_time;
        oid.description = doseDrugSet.description;
        oid.dose = doseDrugSet.dose;
        oid.item_id = doseDrugSet.item_code;

        if (uom != null && freq != null) {
            oid.generateDoseShort(oid.dose, uom.uom_id, freq.drug_frequency_id);
        }

        return oid;
    }

    public OrderItemDrug initOrderItemDrug(Drug drug, Uom2 uom, DrugFrequency2 freq) {
        if (drug == null) {
            return null;
        }
        OrderItemDrug oid = new OrderItemDrug();
        oid.caution = drug.caution;
        oid.day_time = drug.day_time;
        oid.description = drug.description;
        oid.dose = drug.dose;
        oid.frequency = drug.frequency;
        oid.instruction = drug.instruction;
        oid.item_id = drug.item_id;
        oid.printing = drug.printting;
        oid.purch_uom = drug.purch_uom;
        oid.usage_special = "0";
        oid.usage_text = drug.usage_text;
        oid.use_uom = drug.use_uom;
        oid.b_item_manufacturer_id = drug.b_item_manufacturer_id;
        oid.b_item_distributor_id = drug.b_item_distributor_id;
        oid.pregnancy_category = drug.pregnancy_category;
        oid.print_equal_quantity = drug.print_equal_quantity;

        if (uom != null && freq != null) {
            oid.generateDoseShort(oid.dose, uom.uom_id, freq.drug_frequency_id);
        }

        return oid;
    }

    public OrderItem initOrderItem(String visitType, Item item, CategoryGroupItem cg, ItemPrice ip, String date_time) {
        return initOrderItem(visitType, item, cg, ip, date_time, null, null);
    }

    public OrderItem initOrderItem(String visitType, Item item, CategoryGroupItem cg, ItemPrice ip, String date_time, Drug drug) {
        return initOrderItem(visitType, item, cg, ip, date_time, drug, null);
    }

    public OrderItem initOrderItem(String visitType, Item item, CategoryGroupItem cg, ItemPrice ip, String date_time, ItemSupply itemSupply) {
        return initOrderItem(visitType, item, cg, ip, date_time, null, itemSupply);
    }

    public OrderItem initOrderItem(String visitType, Item item, CategoryGroupItem cgi, ItemPrice itemPrice, String dateTime, Drug drug, ItemSupply itemSupply) {
        OrderItem oi = new OrderItem();
        oi.item_code = item.getObjectId();
        oi.common_name = item.common_name;
        oi.item_group_code_category = item.item_group_code_category;
        oi.item_group_code_billing = item.item_group_code_billing;
        oi.item_16_group = item.item_16_group; // sumo 06/06/2549 �������¡���ҵðҹ
        oi.category_group = cgi.category_group_code;
        oi.order_price_type = visitType != null && !visitType.isEmpty() && visitType.equals("1") ? "1" : "0"; // opd = 0, ipd = 1
        if (itemPrice != null) {
            oi.price = Constant.doubleToDBString(
                    oi.isOPDPrice()
                    ? itemPrice.price : itemPrice.price_ipd);
            oi.order_cost = Constant.doubleToDBString(itemPrice.price_cost);
            oi.order_share_doctor = itemPrice.item_share_doctor;
            oi.order_share_hospital = itemPrice.item_share_hospital;
            oi.order_editable_price = itemPrice.item_editable_price;
            oi.order_limit_price_min = itemPrice.item_limit_price_min;
            oi.order_limit_price_max = itemPrice.item_limit_price_max;
            oi.use_price_claim = itemPrice.use_price_claim;
            oi.order_price_claim = itemPrice.item_price_claim;
        }
        oi.status = OrderStatus.NOT_VERTIFY;
        oi.qty = "1";
        oi.continue_order = "0";
        oi.charge_complete = "0";
        oi.secret = item.secret;
        oi.discontinue = "";
        oi.discontinue_time = "";
        oi.dispense = "";
        oi.dispense_time = "";
        oi.vertifier = "";
        oi.vertify_time = "";
        oi.executer = "";
        oi.executed_time = "";
        oi.order_time = dateTime;
        if (theVisit != null) {
            oi.visit_id = theVisit.getObjectId();
            oi.hn = theVisit.patient_id;
        }
        if (theEmployee != null) {
            oi.order_user = theEmployee.getObjectId();
        }
        if (theServicePoint != null) {
            oi.clinic_code = theServicePoint.getObjectId();
        }
        if (cgi.category_group_code.equals(CategoryGroup.isDrug())) {
            if (drug == null) {
                oi.qty = "1";
                oi.print_mar_type = "0";
            } else {
                oi.qty = drug.qty;
                oi.print_mar_type = drug.print_mar_type;
            }
        } else if (cgi.category_group_code.equals(CategoryGroup.isSupply())) {
            if (itemSupply == null) {
                oi.print_mar_type = "0";
            } else {
                oi.print_mar_type = itemSupply.print_mar_type;
                oi.supplement_label = itemSupply.supplement_label;
            }
        }
        oi.f_item_type_id = item.f_item_type_id;
        oi.f_lab_atk_product_id = item.f_lab_atk_product_id;
        Map<String, Object> properties = item.getProperties();
        for (String key : properties.keySet()) {
            oi.setProperty(key, properties.get(key));
        }

        return oi;
    }

    public static Visit initVisit() {
        Visit vst = new Visit();
        vst.patient_id = "";
        vst.hn = "";
        vst.visit_note = "";
        vst.is_discharge_doctor = "0";
        vst.is_discharge_ipd = "0";
        vst.is_discharge_money = "0";
        vst.visit_status = VisitStatus.isInProcess();
        vst.begin_admit_time = "";
        vst.locking = "0";
        vst.deny_allergy = "0";
        vst.service_location = "1";
        return vst;
    }

    public static boolean isVisitDeath(Visit theVisit) {
        return (theVisit.discharge_opd_status != null
                && (theVisit.discharge_opd_status.equals("52")
                || theVisit.discharge_opd_status.equals("55")))
                || (theVisit.discharge_ipd_type != null
                && (theVisit.discharge_ipd_type.equals("8")
                || theVisit.discharge_ipd_type.equals("9")));
    }

    public static boolean isVisitRefer(Visit theVisit) {
        return theVisit.discharge_opd_status.equals("54") || theVisit.discharge_ipd_type.equals("4");
    }

    public static ListTransfer initListTransfer(Patient p, Visit v, Transfer t, ServicePoint sp) {
        return initListTransfer(p, v, t, sp, true);
    }

    public static ListTransfer initListTransfer(Patient p, Visit v, Transfer t, ServicePoint sp, boolean isLocking) {
        ListTransfer lt = new ListTransfer();
        updateListTransfer(p, lt);
        lt.name = sp.name;
        lt.queue = "0";
        lt.locking = isLocking ? "1" : "0";
        lt.description = "";
        lt.color = "r=255,g=255,b=255";
        lt.vn = v.vn;
        lt.visit_type = v.visit_type;
        lt.visit_id = v.getObjectId();
        lt.doctor = t.doctor_code;
        lt.assign_time = t.assign_time;
        lt.servicepoint_id = t.service_point_id;
        lt.labstatus = com.hospital_os.object.QueueLabStatus.NOLAB;
        lt.xraystatus = com.hospital_os.object.QueueXrayStatus.NOXRAY;
        return lt;
    }

    public static boolean updateListTransfer(Patient p, ListTransfer lt) {
        if (p == null || lt == null) {
            return false;
        }
        lt.hn = p.hn;
        lt.sex = p.f_sex_id;
        lt.fname = p.patient_name;
        lt.lname = p.patient_last_name;
        lt.prefix = p.f_prefix_id;
        lt.patient_id = p.getObjectId();
        lt.patient_allergy = p.patient_drugallergy;
        return false;
    }

    public static OfficeInCup initOfficeInCup(Office of) {
        OfficeInCup oic = new OfficeInCup();
        oic.code = of.getObjectId();
        oic.name = of.name;
        return oic;
    }

    public Death initDeath() {
        return initDeath(date_time);
    }

    public Death initDeath(String datetime) {
        Death d = new Death();
        if (theFamily != null) {
            d.family_id = theFamily.getObjectId();
            d.family_name = theFamily.patient_name + "  " + theFamily.patient_last_name;
        }
        if (thePatient != null) {
            d.hn = thePatient.hn;
            d.patient_id = thePatient.getObjectId();
        }
        if (theVisit != null) {
            d.vn = theVisit.vn;
            d.vn_id = theVisit.getObjectId();
        }
        d.ddeath = datetime;
        d.cdeath_a = "";
        d.cdeath_b = "";
        d.cdeath_c = "";
        d.cdeath_d = "";
        d.odiseae = "";
        d.cdeath = "";
        d.pdeath = "1";//�����ç��Һ��
        d.nogreg = "";
        d.wpreg = "";
        d.user_record = theEmployee.getObjectId();
        d.active = "1";
        return d;
    }

    public Appointment initAppointment(String datetime) {
        Appointment appointment = new Appointment();
        if (thePatient != null) {
            appointment.patient_id = thePatient.getObjectId();
        }
        if (theVisit != null) {
            appointment.visit_id_make_appointment = theVisit.getObjectId();
        }
        appointment.date_serv = datetime;
        appointment.appoint_date = date_time;
        appointment.appoint_time = "08:00";
        appointment.appoint_end_time = "16:00";
        appointment.aptype = "��Ǩ�ѡ�ҵ�����ͧ";
        appointment.servicepoint_code = theServicePoint.getObjectId();
        appointment.description = "";
        appointment.auto_visit = "1";
        appointment.queue_visit_id = "";
        appointment.status = AppointmentStatus.WAIT;
        appointment.aptype53 = "";//"181"; default ����к�
        Vector vDoctor = getDoctorInVisit();
        if (!vDoctor.isEmpty()) {
            appointment.doctor_code = (String) vDoctor.get(0);
        }
        return appointment;
    }

    public static QueueVisit initQueueVisit() {
        QueueVisit qv = new QueueVisit();
        return qv;
    }

    public static Billing initBilling() {
        Billing billing = new Billing();
        billing.receipt_date = "";
        billing.active = "";
        billing.payback = "";
        billing.receipt_no = "";
        billing.financial_date = "";
        billing.staff_financial = "";
        billing.patient_id = "";
        billing.visit_id = "";
        billing.paid = "0";
        billing.total_discount = "0";
        billing.patient_share = "0";
        billing.payer_share = "0";
        billing.remain = "0";
        billing.total = "0";
        billing.active = Active.isEnable();
        billing.payback = "";
        billing.receipt_no = "";
        billing.financial_date = "";
        return billing;
    }

    public Payment getVPayment(String pmid) {
        for (int i = 0, size = vVisitPayment.size(); i < size; i++) {
            Payment pm = (Payment) vVisitPayment.get(i);
            if (pmid.equals(pm.getObjectId())) {
                return pm;
            }
        }
        return null;
    }

    public Payment getVPaymentByPlan(String plid) {
        for (int i = 0, size = vVisitPayment.size(); i < size; i++) {
            Payment pm = (Payment) vVisitPayment.get(i);
            if (plid.equals(pm.plan_kid)) {
                return pm;
            }
        }
        return null;
    }

    public Patient initPatient(Family family, Home home) {
        if (family == null) {
            return null;
        }
        Patient patient = new Patient();
        patient.setFamily(family);
        patient.family_id = family.getObjectId();
        patient.pid = family.pid;
        patient.f_prefix_id = family.f_prefix_id;
        patient.patient_name = family.patient_name;
        patient.patient_last_name = family.patient_last_name;
        patient.patient_birthday = family.patient_birthday;
        patient.patient_birthday_true = family.patient_birthday_true;
        patient.f_sex_id = family.f_sex_id;
        patient.marriage_status_id = family.marriage_status_id;
        patient.education_type_id = family.education_type_id;
        patient.occupation_id = family.occupation_id;
        patient.nation_id = family.nation_id;
        patient.race_id = family.race_id;
        patient.religion_id = family.religion_id;
        patient.father_firstname = family.father_firstname;
        patient.father_lastname = family.father_lastname;
        String[] mom = family.mother_firstname.split(" ");
        if (mom.length > 0) {
            patient.mother_firstname = mom[0];
        }
        if (mom.length > 1) {
            patient.mother_lastname = mom[1];
        }

        patient.couple_firstname = family.couple_firstname;
        patient.couple_lastname = family.couple_lastname;
        patient.blood_group_id = family.blood_group_id;
        patient.record_date_time = family.record_date_time;
        patient.update_date_time = family.modify_date_time;
        patient.staff_record = family.staff_record;
        patient.staff_modify = family.staff_modify;
        patient.staff_cancel = family.staff_cancel;
        patient.active = family.active;

        patient.ampur_contact = theLO.theSite.amphor;
        patient.changwat_contact = theLO.theSite.changwat;
        patient.tambon_contact = theLO.theSite.tambon;
        patient.discharge_status_id = family.discharge_status_id;
        if (home == null) {
            patient.ampur = theLO.theSite.amphor;
            patient.changwat = theLO.theSite.changwat;
            patient.tambon = theLO.theSite.tambon;
        } else {
            patient.house = home.home_house;
            patient.village = home.home_moo;
            patient.road = home.home_road;
            patient.tambon = home.home_tambol;
            patient.ampur = home.home_amphur;
            patient.changwat = home.home_changwat;
        }
        patient.updateF2P();
        return patient;
    }

    public Patient initPatient() {
        Patient patient = new Patient();
        initPatient(patient);
        return patient;
    }

    public boolean initPatient(Patient thePatient) {

        thePatient.couple_id = "";
        thePatient.father_pid = "";
        thePatient.mother_pid = "";
        thePatient.patient_birthday = "";
        thePatient.ampur = theLO.theSite.amphor;
        thePatient.changwat = theLO.theSite.changwat;
        thePatient.tambon = theLO.theSite.tambon;
        thePatient.blood_group_id = "1";
        thePatient.education_type_id = "11";
        thePatient.status_id = "2";
        thePatient.f_sex_id = "1";
        thePatient.marriage_status_id = "1";
        thePatient.nation_id = "99";
        thePatient.occupation_id = "000";
        thePatient.f_prefix_id = "000";
        thePatient.race_id = "99";
        thePatient.religion_id = "1";
        thePatient.couple_firstname = "";
        thePatient.couple_lastname = "";
        thePatient.father_firstname = "";
        thePatient.father_lastname = "";
        thePatient.patient_name = "";
        thePatient.house = "";
        thePatient.patient_last_name = "";
        thePatient.mother_firstname = "";
        thePatient.mother_lastname = "";
        thePatient.private_doc = "";
        thePatient.p_type = "";
        thePatient.road = "";
        thePatient.village = "";
        thePatient.pid = "";
        thePatient.phone = "";
        thePatient.relation = "00";
        thePatient.sex_contact = "1";
        thePatient.house_contact = "";
        thePatient.village_contact = "";
        thePatient.road_contact = "";
        thePatient.phone_contact = "";
        thePatient.ampur_contact = theLO.theSite.amphor;
        thePatient.changwat_contact = theLO.theSite.changwat;
        thePatient.tambon_contact = theLO.theSite.tambon;
        thePatient.xn = "";
        thePatient.pid = "";
        thePatient.contact_fname = "";
        thePatient.contact_lname = "";
        thePatient.patient_birthday_true = "0";
        thePatient.record_date_time = "";
        thePatient.staff_record = theEmployee.getObjectId();
        thePatient.deny_allergy = "0";
        return true;
    }

    public ListTransfer initListTransfer(Transfer t, String servicePointName, MapQueueVisit mqv, QueueVisit qv)//amp:18/02/2549
    {
        ListTransfer lt = new ListTransfer();
        lt.hn = thePatient.hn;
        lt.sex = thePatient.f_sex_id;
        lt.fname = thePatient.patient_name;
        lt.lname = thePatient.patient_last_name;
        lt.prefix = thePatient.f_prefix_id;
        lt.patient_id = thePatient.getObjectId();
        lt.patient_allergy = thePatient.patient_drugallergy;
        lt.name = servicePointName;
        lt.queue = "0";
        lt.locking = "1";
        lt.description = "";
        lt.color = "r=255,g=255,b=255";
        lt.vn = theVisit.vn;
        lt.visit_type = theVisit.visit_type;
        lt.visit_id = theVisit.getObjectId();
        lt.doctor = t.doctor_code;
        lt.assign_time = t.assign_time;
        lt.servicepoint_id = t.service_point_id;
        lt.labstatus = com.hospital_os.object.QueueLabStatus.NOLAB;
        lt.xraystatus = com.hospital_os.object.QueueXrayStatus.NOXRAY;
        if ((qv != null)) {
            lt.color = qv.color;
            lt.queue = mqv.queue;
            lt.description = qv.description;
        }
        return lt;
    }

    public static Vector initContractAdjustV(Contract c, Vector cgiv) {
        return initContractAdjustV(c.getObjectId(), cgiv);
    }

    public static Vector initContractAdjustV(String contract_id, Vector cgiv) {
        Vector vContract = new Vector();
        for (int i = 0, size = cgiv.size(); i < size; i++) {
            CategoryGroupItem cgi = (CategoryGroupItem) cgiv.get(i);
            ContractAdjust ca = new ContractAdjust();
            ca.adjustment = "-";
            ca.contract_id = contract_id;
            ca.covered_id = cgi.getObjectId();
            ca.draw = "0";
            vContract.add(ca);
        }
        return vContract;
    }

    public BorrowFilmXray initBorrowFilmXray(String datetime) {
        BorrowFilmXray bor = new BorrowFilmXray();
        if (thePatient != null) {
            bor.patient_hn = thePatient.hn;
        } else {
            bor.patient_hn = "";
        }
        bor.borrower_prefix = "0";
        bor.borrower_name = "";
        bor.borrower_lastname = "";
        bor.borrow_film_date = "";
        bor.amount_date = "";
        bor.return_film_date = "";
        bor.borrow_status = "0";
        bor.permissibly_borrow = "";
        bor.borrow_cause = "";
        bor.borrow_to = "";
        bor.date_serv = datetime;
        return bor;
    }

    public BorrowOpdCard initBorrowOpdCard(String datetime) {
        BorrowOpdCard bor = new BorrowOpdCard();
        if (thePatient != null) {
            bor.patient_hn = thePatient.hn;
        } else {
            bor.patient_hn = "";
        }
        bor.borrower_opd_prefix = "0";
        bor.borrower_opd_name = "";
        bor.borrower_opd_lastname = "";
        bor.borrow_opd_date = "";
        bor.amount_date_opd = "";
        bor.return_opd_date = "";
        bor.borrow_opd_status = "0";
        bor.permissibly_borrow_opd = "";
        bor.borrow_opd_cause = "";
        bor.borrow_opd_to = "";
        bor.date_serv_opd = datetime;
        return bor;
    }

    ///////////////////////////////////////////////////////////////////////////
    /*
     * @Author : henbe pongtorn @date : 18/03/2549 @see : �鹢����� Order
     * �ҡ�Ǥ���� �� object_id
     */
    public static OrderItem getOrderItemFromV(Vector vi, String order_id) {
        for (int i = 0, size = vi.size(); i < size; i++) {
            OrderItem it = (OrderItem) vi.get(i);
            if (it.getObjectId().equals(order_id)) {
                return it;
            }
        }
        return null;
    }

    ///////////////////////////////////////////////////////////////////////////
    /*
     * @Author : henbe pongtorn @date : 18/03/2549 @see : �Ѻ�ӹǹ Order
     * ����׹�ѹ��д��Թ��÷�����
     */
    public static int countOrderVerifyExe(Vector orderitem) {
        return countOrderStatus(orderitem, OrderStatus.EXECUTE)
                + countOrderStatus(orderitem, OrderStatus.VERTIFY);
    }

    public static int countOrderVerExeRem(Vector orderitem) {
        return countOrderStatus(orderitem, OrderStatus.EXECUTE)
                + countOrderStatus(orderitem, OrderStatus.VERTIFY)
                + countOrderStatus(orderitem, OrderStatus.REMAIN);
    }

    ///////////////////////////////////////////////////////////////////////////
    /*
     * @Author : henbe pongtorn @date : 18/03/2549 @see : �Ѻ�ӹǹ Order
     * ����ҧ�ѹ�֡������
     */
    public static int countOrderReport(Vector orderitem) {
        return countOrderStatus(orderitem, OrderStatus.REPORT);
    }

    /*
     * @Author : henbe pongtorn @date : 18/03/2549 @see : �Ѻ�ӹǹ Order
     * ����ҧ�ѹ�֡������
     */
    public static int countOrderRemain(Vector orderitem) {
        return countOrderStatus(orderitem, OrderStatus.REMAIN);
    }

    /*
     * @Author : henbe pongtorn @date : 18/03/2549 @see : �Ѻ�ӹǹ Order
     * ����ҧ�ѹ�֡������
     */
    public static int countOrderStatus(Vector orderitem, String status) {
        int count_ver_exe = 0;
        if (orderitem == null) {
            return count_ver_exe;
        }
        for (int i = 0, size = orderitem.size(); i < size; i++) {
            OrderItem oi = (OrderItem) orderitem.get(i);
            if (oi.isLab()) {
                if (oi.status.equals(status)) {
                    count_ver_exe++;
                }
            }
        }
        return count_ver_exe;
    }

    /*
     * @Author : henbe pongtorn @date : 18/03/2549 @see :
     * �ŧ�����ż������繢����������ҹ
     */
    public static Village getVillage(Patient pt) {
        Village vil = new Village();
        vil.village_number = pt.village;
        vil.village_name = pt.village;
        vil.village_moo = pt.village;
        vil.village_active = "1";
        return vil;
    }

    /*
     * @Author : henbe pongtorn @date : 18/03/2549 @see :
     * �ŧ�����ż������繢����ź�ҹ
     */
    public static Home getHome(Patient pt) {
        Home home = new Home();
        home.home_number = pt.house;
        home.home_house = pt.house;
        home.home_moo = pt.village;
        home.home_road = pt.road;
        home.home_tambol = pt.tambon;
        home.home_amphur = pt.ampur;
        home.home_changwat = pt.changwat;
        home.active = "1";
        return home;
    }

    /*
     * @Author : henbe pongtorn @date : 18/03/2549 @see :
     * �ŧ�����ż������繢����Ż�Ъҡ�
     */
    public static Family getFamily(Patient pt) {
        Family fm = new Family();
        fm.active = "1";
        getFamily(pt, fm);
        return fm;
    }

    /*
     * @Author : henbe pongtorn @date : 18/03/2549 @see :
     * �ŧ�����ż������繢����Ż�Ъҡ� @deprecated use pt.getFamily() instead
     */
    public static Family getFamily(Patient pt, Family fm) {
        return pt.getFamily();
    }

    /*
     * @author Pongtorn (henbe) @name ��㹡�õ�Ǩ�ͺ��������Ǥ����ͧ order
     * �������¡�� Order �����ʶҹдѧ����Ǻ�ҧ�������
     */
    public static boolean isOrderStatus(Vector vOrder, int[] rows, String status) {
        boolean has_status = false;
        for (int i = 0; i < rows.length; i++) {
            OrderItem theOrderItem = (OrderItem) vOrder.get(rows[i]);
            if (theOrderItem.status.equals(status)) {
                has_status = true;
            }
        }
        return has_status;
    }

    public static boolean isOrderStatus(Vector<OrderItem> vOrders, String status) {
        boolean has_status = false;
        for (OrderItem theOrderItem : vOrders) {
            if (theOrderItem.status.equals(status)) {
                has_status = true;
            }
        }
        return has_status;
    }

    /*
     * @author Pongtorn (henbe) @name ��㹡�á�˹����������鹢ͧ object result
     * xray
     */
    public ResultXRay initResultXRay(String order_id) {
        ResultXRay rx = new ResultXRay();
        rx.xn = thePatient.xn;
        rx.xray_point = "";
        rx.description = "";
        rx.hn = thePatient.getObjectId();
        rx.vn = theVisit.getObjectId();
        rx.record_time = date_time;
        rx.order_item_id = order_id;
        rx.reporter = "";
        rx.active = Active.isEnable();
        return rx;
    }

    public Transfer initTransfer(String service_point_id, String date_time) {
        return initTransfer(service_point_id, date_time, theEmployee.getObjectId(), "2");
    }

    public Transfer initUDTransfer(String service_point_id, String date_time, String doctor, String status, String ptid, String vid) {
        Transfer newTransfer = new Transfer();
        newTransfer.assign_time = date_time;
        newTransfer.doctor_code = doctor;
        newTransfer.patient_id = ptid;
        newTransfer.visit_id = vid;
        newTransfer.service_point_id = service_point_id;
        newTransfer.status = status;
        newTransfer.service_start_time = "";//date_time.substring(date_time.indexOf(',')+1);
        newTransfer.service_finish_time = "";
        newTransfer.sender_id = theEmployee.getObjectId();
        return newTransfer;

    }

    public Transfer initTransfer(String service_point_id, String date_time, String doctor, String status) {
        return initUDTransfer(service_point_id, date_time, doctor, status, thePatient.getObjectId(), theVisit.getObjectId());
    }
//
//    /*
//     * @author Pongtorn (henbe) @name ��㹡�äԴ�Թ @deprecated henbe unsued
//     */
//    public BillingOrderItem initBillingOrderItem(OrderItem orderItem, Payment payment, SpecialContrctAdjustByVGaCT special) {
//        /////////////////////////////////////////////////////////////////
//        //����ǹŴ�ͧ�Է�Թ�� ����¡�� orderitem ������������Է���������
//        String adjust = "0";
//        String draw = "0";
//        if (special != null) {
//            adjust = special.adjustment;
//            draw = special.draw;
//        }
//        //��ͧ੾����¡�÷������ǹŴ ��� ��¡�� order ��������͵�Ǩ
//        BillingOrderItem boi = new BillingOrderItem();
//        boi.theOrderItem = orderItem;
//        boi.order_item_id = orderItem.getObjectId();
//        boi.common_name = orderItem.common_name;
//        boi.plan_id = payment.plan_kid;
//        boi.payment_id = payment.getObjectId();
//        boi.request = orderItem.request;
//        boi.visit_id = orderItem.visit_id;
//        boi.patient_id = theVisit.patient_id;
//        boi.item_group_code_category = orderItem.item_group_code_category;
//        boi.charge_complete = orderItem.charge_complete;
//        boi.item_group_code_billing = orderItem.item_group_code_billing;
//        boi.item_id = orderItem.item_code;
//        boi.billing_id = "";
//        boi.draw = draw;
//        updateBillingOrderItem(boi, orderItem, adjust);
//        return boi;
//    }

    public BillingOrderItem initBillingOrderItem(OrderItem orderItem, Payment payment, PlanClaimPrice planClaimPrice, Vector contractV) {
        // ������Է�����Ҥ��ԡ���������
        boolean isPlanUsePriceClaim = planClaimPrice != null;

        //����ǹŴ�ͧ�Է�Թ�� ����¡�� orderitem ������������Է���������
        ContractAdjust ca = null;
        for (int i = 0; i < contractV.size(); i++) {
            ca = (ContractAdjust) contractV.get(i);
            if (ca.covered_id.equals(orderItem.item_group_code_category)) {
                break;
            }
        }
        String adjust = "0";
        String draw = "0";
        if (ca != null) {
            adjust = ca.adjustment;
            draw = ca.draw;
        }
        //��ͧ੾����¡�÷������ǹŴ ��� ��¡�� order ��������͵�Ǩ
        BillingOrderItem boi = new BillingOrderItem();
        boi.theOrderItem = orderItem;
        boi.order_item_id = orderItem.getObjectId();
        boi.common_name = orderItem.common_name;
        boi.plan_id = payment.plan_kid;
        boi.payment_id = payment.getObjectId();
        boi.request = orderItem.request;
        boi.visit_id = orderItem.visit_id;
        boi.patient_id = theVisit.patient_id;
        boi.item_group_code_category = orderItem.item_group_code_category;
        boi.charge_complete = orderItem.charge_complete;
        boi.item_group_code_billing = orderItem.item_group_code_billing;
        boi.item_id = orderItem.item_code;
        boi.billing_id = "";
        boi.draw = draw;
        updateBillingOrderItem(boi, orderItem, adjust, isPlanUsePriceClaim);
        return boi;
    }

    /*
     * @author Pongtorn (henbe) @name ��㹡�äԴ�Թ
     */
    public void updateBillingOrderItem(BillingOrderItem boi, OrderItem orderItem, String adjust, boolean isPlanUsePriceClaim) {
        if (orderItem.qty.isEmpty()) {
            orderItem.qty = "0";
        }
        if (orderItem.price.isEmpty()) {
            orderItem.price = "0";
        }
        boolean isCeil = theLO.theOption.use_money_ceil.equals("1");
        double price = Constant.round(isCeil
                ? Math.ceil(Double.parseDouble(orderItem.qty)
                        * Double.parseDouble(orderItem.price))
                : (Double.parseDouble(orderItem.qty)
                * Double.parseDouble(orderItem.price)));
        double payerprice = orderItem.use_price_claim && isPlanUsePriceClaim
                ? Constant.round(isCeil ? Math.ceil(orderItem.order_price_claim) : orderItem.order_price_claim)
                : Constant.round(isCeil
                        ? Math.floor(price * Double.parseDouble(
                                adjust == null || adjust.isEmpty() || adjust.equals("-") ? "0" : adjust) / 100)
                        : (price * Double.parseDouble(
                                adjust == null || adjust.isEmpty() || adjust.equals("-") ? "0" : adjust) / 100));
        double patientprice = Constant.round(price - payerprice);
        //��Ң͵�Ǩ������Ŵ��ФԴ�Թ
        if (orderItem.request.equals("1")) {
            patientprice = price;
            payerprice = 0;
        }
        boi.payershare = Constant.doubleToDBString(payerprice);
        boi.patientshare = Constant.doubleToDBString(patientprice);
    }

    /**
     * ��㹡�á�ͧ�Է�ԡ���ѡ�Ңͧ payment ����繢����Ũ�ԧ�
     * ����������Է�Է��١¡��ԡ���ʴ�
     *
     * @return Vector �ͧ Payment
     * @author padungrat(tong)
     * @date 07/04/2549,11:38
     */
    public Vector getVisitPaymentActive() {
        if (vVisitPayment != null) {
            for (int i = vVisitPayment.size() - 1; i >= 0; i--) {
                Payment payment = (Payment) vVisitPayment.get(i);
                if (!com.hospital_os.utility.Gutil.isSelected(payment.visit_payment_active)) {
                    vVisitPayment.remove(i);
                }
            }
        }
        return vVisitPayment;
    }

    /*
     * @author Pongtorn (henbe) @name ��㹡�ô֧�������Է���ҵ�Ǩ�ͺ @date
     * 15/05/2549,11:38
     */
    public Payment getPayment() {
        Payment pm = null;
        if (vVisitPayment != null && !vVisitPayment.isEmpty()) {
            pm = (Payment) vVisitPayment.get(0);
        }
        if (pm == null && vPatientPayment != null && !vPatientPayment.isEmpty()) {
            pm = (Payment) vPatientPayment.get(0);
        }
        return pm;
    }

    /*
     * @author Pongtorn (henbe) @name ��㹡�ô֧�������Է���ҵ�Ǩ�ͺ @date
     * 15/05/2549,11:38
     */
    public Refer initReferOut(Vector vrl) {
        return getUpdateRefer(initRefer(), vrl, true);
    }

    public String getFamilyHistory() {
        String hf = "";
        if (vPersonalDisease != null && !vPersonalDisease.isEmpty()) {
            if (!hf.isEmpty()) {
                hf += "\n";
            }
            hf += "�ä��Шӵ��" + ": ";
            for (int i = 0; i < vPersonalDisease.size(); i++) {
                PersonalDisease ph = (PersonalDisease) vPersonalDisease.get(i);
                hf += " " + ph.description;
            }
        }
        if (vRiskFactor != null && !vRiskFactor.isEmpty()) {
            String strRisk = getRiskFactorString();
            hf += (strRisk.isEmpty() ? "" : (hf.isEmpty() ? "" : "\n") + "�Ѩ�������§" + ": " + strRisk);
        }
        if (vPastHistory != null && !vPastHistory.isEmpty()) {
            if (!hf.isEmpty()) {
                hf += "\n";
            }
            hf += "����ѵ��ʹյ" + ": ";
            for (int i = 0; i < vPastHistory.size(); i++) {
                PastHistory ph = (PastHistory) vPastHistory.get(i);
                hf += " " + ph.topic;
                if (!ph.description.isEmpty()) {
                    hf += "-" + ph.description;
                }
            }
        }
        if (thePatientFamilyHistory != null) {
            if (!hf.isEmpty()) {
                hf += "\n";
            }
            hf += "����ѵԤ�ͺ����" + ":" + getFamilyHistoryString(false, "-");
        }
        return hf;
    }

    public Refer getUpdateRefer(Refer theRefer, Vector vrl, boolean is_refer_out) {
        ///////////////////////////////////////////////////////////////////////////
        String current_symb = "";
        String treatment = "";
        String lab = "";
        ///////////////////////////////////////////////////////////////////////////
        if (vPrimarySymptom != null && !vPrimarySymptom.isEmpty()) {
            PrimarySymptom ps = (PrimarySymptom) vPrimarySymptom.get(0);
            // Somprasong 04102012 ����ӴѺ��ͤ�������ͧ�ҡ���Ӥѭ �Ѻ����ѵԻѨ�غѹ���˹�Ҩ� Refer �������������´����͹�Ѻᶺ 3.��õ�Ǩ��ҧ���
            current_symb = ps.main_symptom + "\n" + ps.current_illness;
        }
        ///////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////
        for (int i = 0; vOrderItem != null && i < vOrderItem.size(); i++) {
            OrderItem oi = (OrderItem) vOrderItem.get(i);
            treatment = treatment + ",\n " + oi.common_name;
        }
        if (treatment.length() > 3) {
            treatment = treatment.substring(3);
        }
        ///////////////////////////////////////////////////////////////////////////////
        //tuk: 07/08/2549 ��ͧ��ô֧������ Lab ���ʴ����ǹ�ͧ refer
        if (vrl != null) {
            for (int j = 0, sizevrl = vrl.size(); j < sizevrl; j++) {
                ResultLab resultlab = (ResultLab) vrl.get(j);
                lab = lab + ",\n " + resultlab.name + " : " + resultlab.result;
            }
        }
        if (lab.length() > 3) {
            lab = lab.substring(3);
        }
        ///////////////////////////////////////////////////////////////////////////////
        if (is_refer_out) {
            theRefer.refer_date = date_time.substring(0, 10);
            theRefer.refer_time = date_time.substring(11, 16);
            theRefer.first_dx = theVisit.doctor_dx;
            theRefer.doctor_refer = getDoctorIDInVisit();
            theRefer.reporter_refer = theEmployee.getObjectId();
            theRefer.history_current = current_symb;
            theRefer.history_family = getFamilyHistory();
            theRefer.first_treatment = treatment;
            theRefer.history_lab = lab;
        } else {
//            henbe comment for Data of Refer in
            theRefer.date_result = date_time.substring(0, 10);
            theRefer.final_dx = theVisit.doctor_dx;
            theRefer.doctor_result = getDoctorIDInVisit();
            theRefer.reporter_result = theEmployee.getObjectId();
            theRefer.result_treatment = treatment;
            theRefer.result_lab = lab;
        }
        return theRefer;
    }

    private Refer initRefer() {
        if (thePatient == null || theVisit == null) {
            return null;
        } else {
            Refer theRefer = new Refer();
            theRefer.hn = thePatient.hn;
            theRefer.patient_id = thePatient.getObjectId();
            theRefer.vn = theVisit.vn;
            theRefer.vn_id = theVisit.getObjectId();
            theRefer.cause_refer = "";
            theRefer.continue_treatment = "";
            theRefer.refer_out = "1";
            theRefer.infectious = "";
            theRefer.near_localtion = "";
            theRefer.other_detail = "";
            theRefer.refer_lab = "";
            theRefer.refer_number = "";
            theRefer.refer_observe = "";
            theRefer.refer_result = "";
            theRefer.refer_treatment = "";
            theRefer.reporter_refer = theEmployee.getObjectId();
            theRefer.refer_date = date_time.substring(0, 10);
            theRefer.refer_time = date_time.substring(11, 16);
            theRefer.office_refer = theSite.off_id;
            theRefer.active = "1";
            return theRefer;
        }
    }

    public String getDoctorIDInVisit() {
        Vector v = getDoctorInVisit();
        if (!v.isEmpty()) {
            return (String) v.get(0);
        }
        return "";
    }

    public Vector getDoctorOrder() {
        Vector vdoctor = new Vector();
        Vector temps = new Vector();
        for (int i = 0; vOrderItem != null && i < vOrderItem.size(); i++) {
            OrderItem oi = (OrderItem) vOrderItem.get(i);
            Employee emp = readEmployeeById(oi.vertifier);
            if (emp != null && emp.authentication_id.equals(Authentication.DOCTOR)) {
                if (vdoctor.isEmpty()) {
                    vdoctor.add(emp.getObjectId());
                    temps.add(emp.getObjectId());
                } else {
                    Collections.sort(temps);
                    int pos = Collections.binarySearch(temps, emp.getObjectId());
                    if (pos < 0) {
                        vdoctor.add(emp.getObjectId());
                        temps.add(emp.getObjectId());
                    }
                }
            }
        }
        return vdoctor;
    }

    public Vector getDoctorDiag() {
        Vector vdoctor = new Vector();
        Vector temps = new Vector();
        for (int i = 0; vDiagIcd10 != null && i < vDiagIcd10.size(); i++) {
            DiagIcd10 oi = (DiagIcd10) vDiagIcd10.get(i);
            if (oi.type.equals(Dxtype.getPrimaryDiagnosis())) {
                Employee emp = readEmployeeById(oi.doctor_kid);
                if (emp != null) {
                    if (vdoctor.isEmpty()) {
                        vdoctor.add(emp.getObjectId());
                        temps.add(emp.getObjectId());
                    } else {
                        Collections.sort(temps);
                        int pos = Collections.binarySearch(temps, emp.getObjectId());
                        if (pos < 0) {
                            vdoctor.add(emp.getObjectId());
                            temps.add(emp.getObjectId());
                        }
                    }
                }
            }
        }
        return vdoctor;
    }

    //pu:ᾷ�����Ǩ�ҡ���ҧ MapVisitDx �ó� Dx �����Ǫ���
    public Vector getDoctorMapVisitDx() {
        Vector vdoctor = new Vector();
        Vector temps = new Vector();
        for (int i = 0; vMapVisitDx != null && i < vMapVisitDx.size(); i++) {
            MapVisitDx mvd = (MapVisitDx) vMapVisitDx.get(i);
            Employee emp = readEmployeeById(mvd.visit_diag_map_staff);
            //pu:���ͧ�ҡ MapVisitDx �纷�駾�Һ�����ᾷ�� �� visit slip ��ͧ���੾�Ъ���ᾷ��
            if (emp != null && emp.authentication_id.equals(Authentication.DOCTOR)) {
                if (vdoctor.isEmpty()) {
                    vdoctor.add(emp.getObjectId());
                    temps.add(emp.getObjectId());
                } else {
                    Collections.sort(temps);
                    int pos = Collections.binarySearch(temps, emp.getObjectId());
                    if (pos < 0) {
                        vdoctor.add(emp.getObjectId());
                        temps.add(emp.getObjectId());
                    }
                }
            }
        }
        return vdoctor;
    }

    //pu:ᾷ�����Ǩ�ҡ���ҧ Visit �ó� Dx ��������Ǫ���
    public Vector getDoctorDx() {
        Vector vdoctor = new Vector();
        if (theVisit == null) {
            return vdoctor;
        }

        if (this.theVisit.visit_patient_self_doctor != null
                && !this.theVisit.visit_patient_self_doctor.isEmpty()) {
            vdoctor.add(this.theVisit.visit_patient_self_doctor);
        }
        return vdoctor;
    }

    public Employee readEmployeeById(String id) {
        for (int i = 0; i < theLO.theEmployee.size(); i++) {
            Employee emp = (Employee) theLO.theEmployee.get(i);
            if (emp.getObjectId().equals(id)) {
                return emp;
            }
        }
        return null;
    }

    /**
     * modify by pu:����ʴ����ᾷ��� visitSlip ��� priority �ҡ ᾷ�������
     * ICD10 ����� primary ,ᾷ��������� ,ᾷ���� diag
     * ,���ᾷ�����׹�ѹ��õ�Ǩ ����ӴѺ
     *
     * @return
     */
    public Vector getDoctorInVisit() {
        //����觵�Ǽ�����
        Vector vDoctor = this.getDoctorTransfer();
        if (!vDoctor.isEmpty()) {
            return vDoctor;
        }
        //pu:ᾷ����������� ICD10 ������ primary diag
        vDoctor = this.getDoctorDiag();
        if (!vDoctor.isEmpty()) {
            return vDoctor;
        }
        //pu:ᾷ���������
        vDoctor = getDoctorOrder();
        if (!vDoctor.isEmpty()) {
            return vDoctor;
        }
        //pu:ᾷ�����Ǩ�ҡ���ҧ MapVisitDx �ó� Dx �����Ǫ���
        vDoctor = this.getDoctorMapVisitDx();
        if (!vDoctor.isEmpty()) {
            return vDoctor;
        }
        //pu:ᾷ�����Ǩ�ҡ���ҧ Visit �ó� Dx ��������Ǫ���
        vDoctor = this.getDoctorDx();
        if (!vDoctor.isEmpty()) {
            return vDoctor;
        }
        return vDoctor;
    }

    /**
     *
     * ������Ѻ�����Ũҡ��������ǡ�зӡ�á�˹���ҵ�駵����Ѻ visit
     * ��͹�ѹ�֡ŧ�ҹ������
     *
     * @param visit
     * @param patient
     */
    public void initVisit(Visit visit, Patient patient) {
        updateVisit(visit, patient);
        if (visit.begin_visit_time.isEmpty()) {
            visit.begin_visit_time = date_time;
        }
        visit.lock_time = date_time;
        visit.lock_user = theEmployee.getObjectId();
        visit.locking = "1";
        visit.visit_type = "0";
        visit.queue_lab_status = com.hospital_os.object.QueueLabStatus.NOLAB;
        visit.queue_xray_status = com.hospital_os.object.QueueXrayStatus.NOXRAY;
        visit.deny_allergy = "0";
        visit.is_hospital_service = "0";
        if (theEmployee.authentication_id.equals(Authentication.ONE)) {
            visit.is_hospital_service = "1";
        }
        visit.is_pcu_service = "0";
        if (theEmployee.authentication_id.equals(Authentication.HEALTH)) {
            visit.is_pcu_service = "1";
        }
        //��Ǩ�ͺ����繡���Ҥ����á�������
        visit.is_first = "0";
        if (patient.record_date_time != null
                && patient.record_date_time.startsWith(date_time.substring(0, 10))) {
            visit.is_first = "1";
        }
    }

    /**
     *
     * ������Ѻ�����Ũҡ��������ǡ�зӡ�á�˹���ҵ�駵����Ѻ visit
     * ��͹�ѹ�֡ŧ�ҹ������
     *
     * @param visit
     * @param patient
     */
    public void updateVisit(Visit visit, Patient patient) {
        visit.patient_id = patient.getObjectId();
        visit.hn = patient.hn;
    }

    /**
     * @param patient
     * @param date_time
     * @return
     * @deprecated henbe unused
     *
     */
    public static String calculateAge(Patient patient, String date_time) {
        String patient_age = "";
        if (!patient.patient_birthday.isEmpty()) {
            if (patient.patient_birthday_true != null && patient.patient_birthday_true.equals("1")) {
                patient_age = DateUtil.calculateAgeShort1(patient.patient_birthday, date_time);
            } else {
                patient_age = DateUtil.calculateAge(patient.patient_birthday, date_time);
            }
        }
        return patient_age;
    }

    public Refer initReferIn(Visit visit) {
        Refer theRefer = new Refer();
        theRefer.date_result = date_time;
        theRefer.refer_date = visit.begin_visit_time.substring(0, 10);
        theRefer.refer_time = visit.begin_visit_time.substring(11, 16);
        theRefer.office_refer = visit.refer_in;
        theRefer.patient_id = visit.patient_id;
        theRefer.hn = visit.hn;
        theRefer.refer_out = "0";
        theRefer.cause_refer = visit.visit_note;
        theRefer.refer_treatment = "";
        theRefer.vn = visit.vn;
        theRefer.vn_id = visit.getObjectId();
        theRefer.active = "1";
        //tuk: 02/08/2549 ������á�˹�������������� Object
        theRefer.final_dx = visit.doctor_dx;
        if (theEmployee.authentication_id.equals(Authentication.DOCTOR)) {
            theRefer.doctor_result = theEmployee.getObjectId();
        } else {
            Vector vDoctor = getDoctorInVisit();
            if (vDoctor != null && !vDoctor.isEmpty()) {
                String doctor = String.valueOf(vDoctor.get(0));
                theRefer.doctor_result = doctor;
            }
        }
        //��� vOrderItem �����ҡѺ null sumo 25/08/2549
        if (vOrderItem != null) {
            for (int i = 0, size = vOrderItem.size(); i < size; i++) {
                OrderItem oi = (OrderItem) vOrderItem.get(i);
                theRefer.result_treatment = theRefer.result_treatment + ",\n " + oi.common_name;
            }
        }
        if (theRefer.result_treatment.length() > 3) {
            theRefer.result_treatment = theRefer.result_treatment.substring(3);
        }
        theRefer.reporter_refer = theEmployee.getObjectId();
        theRefer.user_record_id = theEmployee.getObjectId();
        theRefer.user_update_id = theEmployee.getObjectId();
        return theRefer;
    }
    //��� ����繤���͡�����¤�����������

    public boolean isLockingVisit() {
        if (theVisit == null) {
            return false;
        }
        boolean is_lockbyme = theVisit.locking.equals("1")
                && theVisit.lock_user.equals(theEmployee.getObjectId());
        return is_lockbyme;
    }
    //���餹���١��͡�����¤������������

    public boolean isLockingByOther() {
        return isLockingByOther(theVisit, theEmployee.getObjectId());
    }

    //���餹���١��͡�����¤������������
    public static boolean isLockingByOther(Visit v, String eid) {
        if (v == null) {
            return false;
        }
        boolean is_lockby_other = v.locking.equals("1")
                && !v.lock_user.equals(eid);
        return is_lockby_other;
    }
    String patient_age;

    public String getPatientAge(Patient thePatient) {
        return getPatientAge(thePatient, date_time);
    }

    public String getPatientAge(Patient thePatient, String date) {
        if (thePatient.patient_birthday != null && !thePatient.patient_birthday.isEmpty()) {
            if (thePatient.patient_birthday_true.equals("0")) {
                patient_age = DateUtil.calculateAge(thePatient.patient_birthday, date);
                patient_age = patient_age + " " + Constant.getTextBundle("��");
                return patient_age;
            } else {
                patient_age = DateUtil.calculateAgeLong(thePatient.patient_birthday, date);
                return patient_age;
            }
        }
        return "";
    }

    public String getYearAge() {
        return getYearAge(this.thePatient);
    }

    public String getYearAge(Patient thePatient) {
        return getYearAge(thePatient, patient_age);
    }

    public String getYearAge(Patient thePatient, String age) {
        if (thePatient.patient_birthday_true.equals("0")) {
            return patient_age;// + " " + Constant.getTextBundle("��");
        } else {
            return Constant.getYear(age);
        }
    }

    public String getMonthAge() {
        return getMonthAge(this.thePatient);
    }

    public String getMonthAge(Patient thePatient) {
        return getMonthAge(thePatient, patient_age);
    }

    public String getMonthAge(Patient thePatient, String age) {
        if (thePatient.patient_birthday_true.equals("0")) {
            return "";
        } else {
            return Constant.getMonth(age);
        }
    }

    public String getDayAge() {
        return getDayAge(this.thePatient);
    }

    public String getDayAge(Patient thePatient) {
        return getDayAge(thePatient, patient_age);
    }

    public String getDayAge(Patient thePatient, String age) {
        if (thePatient.patient_birthday_true.equals("0")) {
            return "";
        } else {
            return Constant.getDay(age);
        }
    }

    /**
     * ��㹡�� add ������ŧ���ҧ DiagICD10 �ͧ���������
     *
     * @param mapVisitDx �� Object �ͧ MapVisitDx ������红����š�� Dx
     * @param doctor_id
     * @param clinic_id
     * @return
     * @author padungrat(tong)
     * @date 24/03/49,16:25
     */
    public DiagIcd10 initDiagICD10(MapVisitDx mapVisitDx, String doctor_id, String clinic_id) {
        return initDiagICD10(mapVisitDx.visit_id, mapVisitDx.visit_diag_map_icd, doctor_id, clinic_id);
    }

    public DiagIcd10 initDiagICD10(String visit_id, String icd_kid, String doctor_id, String clinic_id) {
        DiagIcd10 diagICD10 = initDiagIcd10();
        diagICD10.clinic_kid = clinic_id;
        diagICD10.doctor_kid = doctor_id;
        diagICD10.icd10_code = icd_kid;
        diagICD10.visit_id = visit_id;
        return diagICD10;
    }

    public MapVisitDx initMapVisitDx(DxTemplate dxt) {
        for (int i = 0; !vMapVisitDx.isEmpty() && i < vMapVisitDx.size(); i++) {
            MapVisitDx mvd = (MapVisitDx) vMapVisitDx.get(i);
            if (dxt.getObjectId() != null && dxt.getObjectId().equals(mvd.dx_template_id)) {
                return null;
            }
        }
        MapVisitDx mvd = new MapVisitDx();
        mvd.visit_id = theVisit.getObjectId();
        mvd.dx_template_id = dxt.getObjectId() == null || dxt.getObjectId().equals("-1") ? null : dxt.getObjectId();
        mvd.t_patient_id = thePatient.getObjectId();
        mvd.visit_diag_map_active = Active.isEnable();
        mvd.visit_diag_map_date_time = date_time;
        mvd.visit_diag_map_dx = dxt.description;
        mvd.visit_diag_map_icd = dxt.icd_code.isEmpty() ? null : dxt.icd_code;
        mvd.visit_diag_map_staff = theEmployee.getObjectId();
        return mvd;
    }

    // ������� genneric name �᷹���� item
    /**
     *
     * @return genneric name �������ӡѹ
     */
    public String getDrugAllergyString() {
        return getDrugAllergyStringByType("1");
    }

    public String getSurveillanceDrugAllergyString() {
        return getDrugAllergyStringByType("2");
    }

    public String getSuspectedDrugAllergyString() {
        return getDrugAllergyStringByType("3");
    }

    /**
     * @param type 1	���� 2	������ѧ������� 3	ʧ�������
     * @return
     */
    public String getDrugAllergyStringByType(String type) {
        if (vDrugAllergy == null) {
            return "";
        }
        String name = "";
        String lastName = "";
        for (int j = 0, sizej = vDrugAllergy.size(); j < sizej; j++) {
            PatientDrugAllergy drugAllergy = (PatientDrugAllergy) vDrugAllergy.get(j);
            String generic_name = drugAllergy.generic_name.trim();
            if (lastName.equalsIgnoreCase(generic_name)) {
                continue;
            }
            if (type.equals(drugAllergy.f_allergy_warning_type_id)) {
                name += name.isEmpty() ? generic_name : (", " + generic_name);
                lastName = generic_name;
            }
        }
        return name;
    }

    public String getPastHistoryString() {
        if (vPastHistory == null) {
            return "";
        }
        String past_history = "";
        for (int j = 0, sizej = vPastHistory.size(); j < sizej; j++) {
            PastHistory pastHistory = (PastHistory) vPastHistory.get(j);
            if (past_history.isEmpty()) {
                past_history = pastHistory.topic + " : " + pastHistory.description;
            } else {
                past_history = past_history + "\n" + pastHistory.topic + " : " + pastHistory.description;
            }
        }
        return past_history;
    }

    public String getFamilyHistoryString() {
        return getFamilyHistoryString(true, ": ");
    }

    public String getFamilyHistoryString(boolean isNewline, String separator) {
        if (thePatientFamilyHistory == null) {
            return "";
        }

        String text = "";
        if (thePatientFamilyHistory.family_history1_select) {
            text += theLO.patientFamilyHistoryTypes.get(0).getName()
                    + separator + thePatientFamilyHistory.family_history1_detail;
        }

        if (thePatientFamilyHistory.family_history2_select) {
            text += (isNewline ? "\n" : " ") + theLO.patientFamilyHistoryTypes.get(1).getName()
                    + separator + thePatientFamilyHistory.family_history2_detail;
        }

        if (thePatientFamilyHistory.family_history3_select) {
            text += (isNewline ? "\n" : " ") + theLO.patientFamilyHistoryTypes.get(2).getName()
                    + separator + thePatientFamilyHistory.family_history3_detail;
        }

        if (thePatientFamilyHistory.family_history4_select) {
            text += (isNewline ? "\n" : " ") + theLO.patientFamilyHistoryTypes.get(3).getName()
                    + separator + thePatientFamilyHistory.family_history4_detail;
        }

        if (thePatientFamilyHistory.family_history5_select) {
            text += (isNewline ? "\n" : " ") + theLO.patientFamilyHistoryTypes.get(4).getName()
                    + separator + thePatientFamilyHistory.family_history5_detail;
        }

        if (thePatientFamilyHistory.patient_family_history_other != null
                && !thePatientFamilyHistory.patient_family_history_other.isEmpty()) {
            text += (isNewline ? "\n" : " ") + "����" + separator + thePatientFamilyHistory.patient_family_history_other;
        }
        return text;
    }

    public String getPersonalDiseaseString() {
        if (vPersonalDisease == null) {
            return "";
        }
        String personal_disease = "";
        for (int j = 0, sizej = vPersonalDisease.size(); j < sizej; j++) {
            PersonalDisease personalDisease = (PersonalDisease) vPersonalDisease.get(j);
            if (personal_disease.isEmpty()) {
                personal_disease = personalDisease.description;
            } else {
                personal_disease = personal_disease + "\n" + personalDisease.description;
            }
        }
        return personal_disease;
    }

    public String getRiskFactorString() {
        if (vRiskFactor == null) {
            return "";
        }
        String strRisk = "";
        for (int i = 0; i < vRiskFactor.size(); i++) {
            RiskFactor ph = (RiskFactor) vRiskFactor.get(i);
            if (ph.topic.equals("������š�����")) {
                if ("1".equals(ph.risk_alcoho_result)) {
                    strRisk += ph.topic + " : �����ҹ� ����";
                } else if ("2".equals(ph.risk_alcoho_result)) {
                    strRisk += ph.topic + " : �����繤��駤���";
                } else if ("3".equals(ph.risk_alcoho_result)) {
                    strRisk += ph.topic + " : �����繻�Ш�";
                } else if ("4".equals(ph.risk_alcoho_result)) {
                    strRisk += ph.topic + " : �´�������ԡ����";
                } else {
                    strRisk += ph.topic + " : ������";
                }
            } else if (ph.topic.equals("�ٺ������")) {
                if ("1".equals(ph.risk_cigarette_result)) {
                    strRisk += (strRisk.isEmpty() ? "" : "\n") + ph.topic + " : �ٺ�ҹ� ����";
                } else if ("2".equals(ph.risk_cigarette_result)) {
                    strRisk += (strRisk.isEmpty() ? "" : "\n") + ph.topic + " : �ٺ�繤��駤���";
                } else if ("3".equals(ph.risk_cigarette_result)) {
                    strRisk += (strRisk.isEmpty() ? "" : "\n") + ph.topic + " : �ٺ�繻�Ш�";
                } else if ("4".equals(ph.risk_cigarette_result)) {
                    strRisk += (strRisk.isEmpty() ? "" : "\n") + ph.topic + " : ���ٺ����ԡ����";
                } else {
                    strRisk += (strRisk.isEmpty() ? "" : "\n") + ph.topic + " : ����ٺ";
                }
            } else if (ph.topic.equals("��ǹ")) {
                strRisk += (strRisk.isEmpty() ? "" : "\n") + ph.topic + " : " + ph.description;
            }
        }
        return strRisk;
    }

    public ReceiptItem initReceiptItem(BillingInvoiceItem bii, Receipt receipt) {
        ReceiptItem ri = initReceiptItem(bii, receipt, 0);
        ri.paid = bii.patient_share;
        return ri;
    }

    public ReceiptItem initReceiptItem(BillingInvoiceItem bii, Receipt receipt, double paid) {
        ReceiptItem bri = new ReceiptItem();
        bri.active = "1";
        bri.hn = "";
        bri.vn = "";
        bri.draw = bii.draw;
        bri.visit_id = bii.visit_id;
        bri.item_id = bii.item_id;
        bri.billing_invoice_item_id = bii.getObjectId();
        bri.patient_id = bii.patient_id;
        bri.payment_id = bii.payment_id;
        bri.receipt_id = receipt.getObjectId();
        bri.paid = Constant.doubleToDBString(paid);
        return bri;
    }

    public Receipt initReceipt(Billing billing) {
        Receipt receipt = initReceipt(billing, 0);
        receipt.paid = billing.patient_share;
        return receipt;
    }

    public Receipt initReceipt(Billing billing, double get_paid) {
        Receipt theReceipt = new Receipt();
        theReceipt.active = Active.isEnable();
        theReceipt.hn = billing.patient_id;
        theReceipt.paid = Constant.doubleToDBString(get_paid);
        theReceipt.receipt_date = date_time;
        theReceipt.vn = billing.visit_id;
        theReceipt.billing_id = billing.getObjectId();
        theReceipt.staff_receipt = theEmployee.getObjectId();
        return theReceipt;
    }

    public ReceiptSubgroup initReceiptSubgroup(BillingInvoiceSubgroup bis, Receipt theReceipt) {
        ReceiptSubgroup receipt = initReceiptSubgroup(bis, theReceipt, 0);
        receipt.paid = bis.patient_share;
        return receipt;
    }

    public ReceiptSubgroup initReceiptSubgroup(BillingInvoiceSubgroup bis, Receipt theReceipt, double paid) {
        ReceiptSubgroup rs = new ReceiptSubgroup();
        rs.billing_invoice_subgroup_id = bis.getObjectId();
        rs.receipt_id = theReceipt.getObjectId();
        rs.hn = bis.patient_id;
        rs.vn = bis.visit_id;
        rs.item_group_code_billing = bis.billing_group_item_id;
        rs.draw = bis.draw;
        rs.active = Active.isEnable();
        rs.paid = Constant.doubleToDBString(paid);
        return rs;
    }

    public String getClinicByPrimaryDx(Vector vDiagIcd10) {
        for (int i = 0; i < vDiagIcd10.size(); i++) {
            DiagIcd10 dx10 = (DiagIcd10) vDiagIcd10.get(i);
            if (dx10.type.equals(Dxtype.getPrimaryDiagnosis())) {
                return dx10.clinic_kid;
            }
        }
        return null;
    }

    public Visit getVisit() {
        return theVisit;
    }

    public boolean readSuitPatientByHN(String hn) {
        return false;
    }

    public boolean readSuitPatientByPID(String pid) {
        return false;
    }

    public boolean isVisitLocking() {
        return isLockingByOther();
    }

    /**
     * @param drugInteraction
     * @param interactionItemId
     * @param is_interaction
     * @return
     * @throws Exception
     * @Author amp
     * @date 27/03/2549
     * @see init object OrderDrugInteraction
     */
    public OrderDrugInteraction initOrderDrugInteraction(DrugInteraction drugInteraction, String interactionItemId, boolean is_interaction) throws Exception {
        OrderDrugInteraction theOrderDrugInteraction = new OrderDrugInteraction();
        theOrderDrugInteraction.interaction_item_id = interactionItemId;
        if (is_interaction)//��ǵ�駵��繵�Ƿ���ջ�ԡ����Ҵ���
        {
            theOrderDrugInteraction.order_item_drug_standard_id = drugInteraction.drug_standard_interaction_id;
            theOrderDrugInteraction.order_item_drug_standard_description = drugInteraction.drug_standard_interaction_description;
            if (!"".equals(interactionItemId)) {
                theOrderDrugInteraction.interaction_item_drug_standard_id = drugInteraction.drug_standard_original_id;
                theOrderDrugInteraction.interaction_item_drug_standard_description = drugInteraction.drug_standard_original_description;
            }
        } else {
            theOrderDrugInteraction.order_item_drug_standard_id = drugInteraction.drug_standard_original_id;
            theOrderDrugInteraction.order_item_drug_standard_description = drugInteraction.drug_standard_original_description;
            if (!"".equals(interactionItemId)) {
                theOrderDrugInteraction.interaction_item_drug_standard_id = drugInteraction.drug_standard_interaction_id;
                theOrderDrugInteraction.interaction_item_drug_standard_description = drugInteraction.drug_standard_interaction_description;
            }
        }
        theOrderDrugInteraction.interaction_blood_presure = drugInteraction.blood_presure;
        theOrderDrugInteraction.interaction_pregnant = drugInteraction.pregnant;
        theOrderDrugInteraction.interaction_type = drugInteraction.type;
        theOrderDrugInteraction.interaction_force = drugInteraction.force;
        theOrderDrugInteraction.interaction_act = drugInteraction.act;
        theOrderDrugInteraction.interaction_repair = drugInteraction.repair;
        theOrderDrugInteraction.active = Active.isEnable();
        theOrderDrugInteraction.visit_id = theVisit.getObjectId();
        return theOrderDrugInteraction;
    }

    public Surveil initSurveil() {
        Surveil surveil = new Surveil();
        surveil.vn_id = "";
        surveil.vn = "";
        if (theVisit != null) {
            surveil.vn_id = theVisit.getObjectId();
            surveil.vn = theVisit.vn;
        }
        surveil.patient_id = "";
        surveil.hn = "";
        if (thePatient != null) {
            surveil.patient_id = thePatient.getObjectId();
            surveil.hn = thePatient.hn;
        }
        return surveil;
    }

    private Vector<String> getDoctorTransfer() {
        Vector<String> vdoctor = new Vector<String>();
        Vector<String> temps = new Vector<String>();
        for (int i = 0; vTransfer != null && i < vTransfer.size(); i++) {
            Transfer transfer = (Transfer) vTransfer.get(i);
            if (!"".equals(transfer.doctor_code) && !"null".equals(transfer.doctor_code)) {   //check ���
                if (vdoctor.isEmpty()) {
                    vdoctor.add(transfer.doctor_code);
                    temps.add(transfer.doctor_code);
                } else {
                    Collections.sort(temps);
                    int pos = Collections.binarySearch(temps, transfer.doctor_code);
                    if (pos < 0) {
                        vdoctor.add(transfer.doctor_code);
                        temps.add(transfer.doctor_code);
                    }
                }
//                if (!vdoctor.isEmpty()) {
//                    for (int j = 0; j < vdoctor.size(); j++) {
//                        if (transfer.doctor_code.equals(vdoctor.get(j))) {
//                            vdoctor.remove(j);
//                        }
//                    }
//                    vdoctor.addElement(transfer.doctor_code);
//                } else {
//                    vdoctor.addElement(transfer.doctor_code);
//                }
            }
        }
        return vdoctor;
    }

    public String getDiagIcd9Code(String optype) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < vDiagIcd9.size(); i++) {
            DiagIcd9 dx10 = (DiagIcd9) vDiagIcd9.get(i);
            if (dx10.type.equals(optype)) {
                if (i > 0) {
                    sb.append(",");
                }
                sb.append(dx10.icd9_code);
            }
        }
        return sb.toString();
    }

    public String getDiagIcd10Code(String dxtype) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < vDiagIcd10.size(); i++) {
            DiagIcd10 dx10 = (DiagIcd10) vDiagIcd10.get(i);
            if (dx10.type.equals(dxtype)) {
                if (i > 0) {
                    sb.append(",");
                }
                sb.append(dx10.icd10_code);
            }
        }
        return sb.toString();
    }
    public boolean scrollPaneDrug;
    public boolean scrollPaneVitalSign;
    public boolean scrollPaneLab;
    public boolean scrollPaneXray;
    public static String opatient = null;
    private static final Logger LOG = Logger.getLogger(HosObject.class.getName());

    public static boolean isOrderChildPackage(List<OrderItem> vOrders) {
        int count = 0;
        for (OrderItem theOrderItem : vOrders) {
            if (theOrderItem.isChildPackage()) {
                count++;
            }
        }
        return vOrders.size() == count;
    }

    public BillingInvoice initBillingInvoice(BillingInvoice oldBillingInvoice) {
        BillingInvoice newBillingInvoice = new BillingInvoice();
        newBillingInvoice.patient_id = oldBillingInvoice.patient_id;
        newBillingInvoice.visit_id = oldBillingInvoice.visit_id;
        newBillingInvoice.invoice_no = oldBillingInvoice.invoice_no;
        newBillingInvoice.active = oldBillingInvoice.active;
        newBillingInvoice.billing_invoice_no = oldBillingInvoice.billing_invoice_no;
        newBillingInvoice.billing_id = oldBillingInvoice.billing_id;
        newBillingInvoice.billing_complete = oldBillingInvoice.billing_complete;
        newBillingInvoice.patient_share = oldBillingInvoice.patient_share;
        newBillingInvoice.patient_share_ceil = oldBillingInvoice.patient_share_ceil;
        newBillingInvoice.payer_share = "0";
        newBillingInvoice.payer_share_ceil = "0";
        newBillingInvoice.total = oldBillingInvoice.patient_share;
        return newBillingInvoice;
    }

    public BillingInvoiceSubgroup initBillingInvoiceSubgroup(BillingInvoiceSubgroup oldBillingInvoiceSubgroup) {
        BillingInvoiceSubgroup newBillingInvoiceSubgroup = new BillingInvoiceSubgroup();
        newBillingInvoiceSubgroup.patient_id = oldBillingInvoiceSubgroup.patient_id;
        newBillingInvoiceSubgroup.visit_id = oldBillingInvoiceSubgroup.visit_id;
        newBillingInvoiceSubgroup.billing_group_item_id = oldBillingInvoiceSubgroup.billing_group_item_id;
        newBillingInvoiceSubgroup.active = oldBillingInvoiceSubgroup.active;
        newBillingInvoiceSubgroup.draw = oldBillingInvoiceSubgroup.draw;
        newBillingInvoiceSubgroup.billing_id = oldBillingInvoiceSubgroup.billing_id;
        newBillingInvoiceSubgroup.patient_share = oldBillingInvoiceSubgroup.patient_share;
        newBillingInvoiceSubgroup.patient_share_ceil = oldBillingInvoiceSubgroup.patient_share_ceil;
        newBillingInvoiceSubgroup.payer_share = "0";
        newBillingInvoiceSubgroup.payer_share_ceil = "0";
        newBillingInvoiceSubgroup.total = oldBillingInvoiceSubgroup.patient_share;
        newBillingInvoiceSubgroup.discount_percentage = oldBillingInvoiceSubgroup.discount_percentage;
        newBillingInvoiceSubgroup.discount = oldBillingInvoiceSubgroup.discount;
        newBillingInvoiceSubgroup.final_patient_share = oldBillingInvoiceSubgroup.patient_share;
        return newBillingInvoiceSubgroup;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.BTariff;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class BTariffDB {

    public ConnectionInf connectionInf;
    final public String tableId = "862";

    public BTariffDB(ConnectionInf db) {
        connectionInf = db;
    }

    public int insert(BTariff obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO b_tariff(\n"
                    + "            b_tariff_id, code, description, active, user_record_id, user_update_id)\n"
                    + "    VALUES (?, ?, ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.getGenID(tableId));
            preparedStatement.setString(index++, obj.code);
            preparedStatement.setString(index++, obj.description);
            preparedStatement.setString(index++, obj.active);
            preparedStatement.setString(index++, obj.user_record_id);
            preparedStatement.setString(index++, obj.user_update_id);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(BTariff obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE b_tariff\n"
                    + "   SET code=?, description=?, \n"
                    + "       active=?,update_date_time=current_timestamp, user_update_id=?\n"
                    + " WHERE b_tariff_id=?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.code);
            preparedStatement.setString(index++, obj.description);
            preparedStatement.setString(index++, obj.active);
            preparedStatement.setString(index++, obj.user_update_id);
            preparedStatement.setString(index++, obj.getObjectId());
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int inactive(BTariff obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE b_tariff\n"
                    + "   SET active=false,update_date_time=current_timestamp, user_update_id=?\n"
                    + " WHERE b_tariff_id=?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.user_update_id);
            preparedStatement.setString(index++, obj.getObjectId());
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public BTariff selectById(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_tariff where b_tariff_id = ?\n";
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, id);
            List<BTariff> executeQuery = executeQuery(preparedStatement);
            return executeQuery.isEmpty() ? null : executeQuery.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public BTariff selectByCode(String code) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_tariff where code = ?\n";
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, code);
            List<BTariff> executeQuery = executeQuery(preparedStatement);
            return executeQuery.isEmpty() ? null : executeQuery.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<BTariff> listByKeyword(String keyword) throws Exception {
        return listByKeyword(keyword, "1");
    }

    public List<BTariff> listByKeyword(String keyword, String active) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select *  from b_tariff where " + (keyword == null || keyword.isEmpty() ? "" : "(code ilike ? or description ilike ?) and ") + "active = ? order by description";
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            if (keyword != null && !keyword.isEmpty()) {
                preparedStatement.setString(index++, "%" + keyword + "%");
                preparedStatement.setString(index++, "%" + keyword + "%");
            }
            preparedStatement.setString(index++, active);
            List<BTariff> list = executeQuery(preparedStatement);
            return list;
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<>();
        for (BTariff obj : listByKeyword(null)) {
            list.add((CommonInf) obj);
        }
        return list;
    }

    public List<BTariff> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<BTariff> list = new ArrayList<BTariff>();
        try ( ResultSet rs = preparedStatement.executeQuery()) {
            while (rs.next()) {
                BTariff obj = new BTariff();
                obj.setObjectId(rs.getString("b_tariff_id"));
                obj.code = rs.getString("code");
                obj.description = rs.getString("description");
                obj.active = rs.getString("active");
                obj.record_date_time = rs.getTimestamp("record_date_time");
                obj.user_record_id = rs.getString("user_record_id");
                obj.update_date_time = rs.getTimestamp("update_date_time");
                obj.user_update_id = rs.getString("user_update_id");
                list.add(obj);
            }
            return list;
        }
    }
}

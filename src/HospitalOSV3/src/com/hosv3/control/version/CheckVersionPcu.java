/*
 * CheckVersionControl.java
 *
 * Created on 2 ��Ȩԡ�¹ 2548, 15:59 �.
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
package com.hosv3.control.version;

import com.hospital_os.gui.connection.AbsVersionControl3;
import com.hospital_os.object.Version;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hosv3.control.HosDB;
import java.sql.SQLException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tong(Padungrat)
 * @Modify Ojika
 * @Modify henbe
 */
@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public class CheckVersionPcu extends AbsVersionControl3 {

    private static final Logger LOG = Logger.getLogger(CheckVersionPcu.class.getName());
    private final ConnectionInf theConnectionInf;
    ////////////////////////////////////////////////////////////////////////////////
    /**
     * ��㹡�õ�Ǩ�ͺ Version �ͧ����� PCU
     */
    private static final String VERSION0 = "";
    private static final String VERSION2 = "0.02.1048";
    private static final String VERSION3 = "0.03.1148";
    private static final String VERSION4 = "0.04.0449";
    private static final String VERSION5 = "1.0.0";
    private static final String VERSION6 = "1.1.0";
    private static final String VERSION7 = "1.2.0";
    private static final String VERSION8 = "1.3.0";
    private static final String VERSION9 = "1.4.0";
    private static final String VERSION10 = "1.5.0";
    private static final String VERSION11 = "1.6.0";
    private static final String VERSION12 = "1.7.0";
    private static final String VERSION13 = "1.8.0";
    private static final String FN_VERSION2 = "database/pcu/update_pcu_ph2.sql";
    private static final String FN_VERSION3 = "database/pcu/update_pcu_ph3.sql";
    private static final String FN_VERSION4 = "database/pcu/update_pcu_ph5.sql";
    private static final String FN_VERSION5 = "database/pcu/update_pcu_ph6.sql";
    private static final String FN_VERSION6 = "database/pcu/update_pcu_ph7.sql";
    private static final String FN_VERSION7 = "database/pcu/update_pcu_ph8.sql";
    private static final String FN_VERSION8 = "database/pcu/update_pcu_ph9.sql";
    private static final String FN_VERSION9 = "database/pcu/update_pcu_ph10.sql";
    private static final String FN_VERSION10 = "database/pcu/update_pcu_ph11.sql";
    private static final String FN_VERSION11 = "database/pcu/update_pcu_ph12.sql";
    private static final String FN_VERSION12 = "database/pcu/update_pcu_ph13.sql";
    private static final String FN_VERSION13 = "database/pcu/update_pcu_ph14.sql";
    private final HosDB theHosDB;
    private Version vs;

    public CheckVersionPcu(HosDB hosDB) {
        theHosDB = hosDB;
        theConnectionInf = hosDB.theSiteDB.theConnectionInf;
    }

    @Override
    public boolean isSchemaUpdate(String cur_version) {
        if (cur_version.startsWith(VERSION0)) {
            return true;
        } else if (cur_version.startsWith(VERSION2)) {
            return true;
        } else if (cur_version.startsWith(VERSION3)) {
            return true;
        } else if (cur_version.startsWith(VERSION4)) {
            return true;
        }
        return false;
    }

    @Override
    public String getFinalVersion() {
        return VERSION13;
    }

    @Override
    public Vector getFileUpdate(String cur_version) {
        Vector v = new Vector();
        if (cur_version.startsWith(VERSION0)) {
            v.add(FN_VERSION2);
            v.add(FN_VERSION3);
            v.add(FN_VERSION4);
            v.add(FN_VERSION5);
            v.add(FN_VERSION6);
            v.add(FN_VERSION7);
            v.add(FN_VERSION8);
            v.add(FN_VERSION9);
            v.add(FN_VERSION10);
            v.add(FN_VERSION11);
            v.add(FN_VERSION12);
            v.add(FN_VERSION13);
        } else if (cur_version.startsWith(VERSION2)) {
            v.add(FN_VERSION3);
            v.add(FN_VERSION4);
            v.add(FN_VERSION5);
            v.add(FN_VERSION6);
            v.add(FN_VERSION7);
            v.add(FN_VERSION8);
            v.add(FN_VERSION9);
            v.add(FN_VERSION10);
            v.add(FN_VERSION11);
            v.add(FN_VERSION12);
            v.add(FN_VERSION13);
        } else if (cur_version.startsWith(VERSION3)) {
            v.add(FN_VERSION4);
            v.add(FN_VERSION5);
            v.add(FN_VERSION6);
            v.add(FN_VERSION7);
            v.add(FN_VERSION8);
            v.add(FN_VERSION9);
            v.add(FN_VERSION10);
            v.add(FN_VERSION11);
            v.add(FN_VERSION12);
            v.add(FN_VERSION13);
        } else if (cur_version.startsWith(VERSION4)) {
            v.add(FN_VERSION5);
            v.add(FN_VERSION6);
            v.add(FN_VERSION7);
            v.add(FN_VERSION8);
            v.add(FN_VERSION9);
            v.add(FN_VERSION10);
            v.add(FN_VERSION11);
            v.add(FN_VERSION12);
            v.add(FN_VERSION13);
        } else if (cur_version.startsWith(VERSION5)) {
            v.add(FN_VERSION6);
            v.add(FN_VERSION7);
            v.add(FN_VERSION8);
            v.add(FN_VERSION9);
            v.add(FN_VERSION10);
            v.add(FN_VERSION11);
            v.add(FN_VERSION12);
            v.add(FN_VERSION13);
        } else if (cur_version.startsWith(VERSION6)) {
            v.add(FN_VERSION7);
            v.add(FN_VERSION8);
            v.add(FN_VERSION9);
            v.add(FN_VERSION10);
            v.add(FN_VERSION11);
            v.add(FN_VERSION12);
            v.add(FN_VERSION13);
        } else if (cur_version.startsWith(VERSION7)) {
            v.add(FN_VERSION8);
            v.add(FN_VERSION9);
            v.add(FN_VERSION10);
            v.add(FN_VERSION11);
            v.add(FN_VERSION12);
            v.add(FN_VERSION13);
        } else if (cur_version.startsWith(VERSION8)) {
            v.add(FN_VERSION9);
            v.add(FN_VERSION10);
            v.add(FN_VERSION11);
            v.add(FN_VERSION12);
            v.add(FN_VERSION13);
        } else if (cur_version.startsWith(VERSION9)) {
            v.add(FN_VERSION10);
            v.add(FN_VERSION11);
            v.add(FN_VERSION12);
            v.add(FN_VERSION13);
        } else if (cur_version.startsWith(VERSION10)) {
            v.add(FN_VERSION11);
            v.add(FN_VERSION12);
            v.add(FN_VERSION13);
        } else if (cur_version.startsWith(VERSION11)) {
            v.add(FN_VERSION12);
            v.add(FN_VERSION13);
        } else if (cur_version.startsWith(VERSION12)) {
            v.add(FN_VERSION13);
        }
        return v;
    }

    @Override
    public String getCurrentVersion() {
        if (vs == null) {
            theConnectionInf.open();
            try {
                theConnectionInf.getConnection().setAutoCommit(false);
                vs = this.theHosDB.thePcuVersionDB.selectCurrentVersion();
                theConnectionInf.getConnection().commit();
            } catch (Exception e) {
                try {
                    theConnectionInf.getConnection().rollback();
                } catch (SQLException ex) {
                    LOG.log(Level.SEVERE, null, ex);
                }
                return "";
            } finally {
                theConnectionInf.close();
            }
        }
        return vs == null ? "" : vs.db_code;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.FootExamAssessDetail;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.Gutil;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class FootExamAssessDetailDB {

    public ConnectionInf theConnectionInf;
    final public String idtable = "995";

    public FootExamAssessDetailDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public int insert(FootExamAssessDetail o) throws Exception {
        o.generateOID(idtable);
        String sql = "INSERT INTO t_foot_exam_assess_detail( "
                + "t_foot_exam_assess_detail_id, t_foot_exam_assess_id, f_foot_assess_type_id,  "
                + "other_detail, record_date_time, user_record_id, update_date_time, user_update_id) "
                + "VALUES ('%s', '%s', '%s',  "
                + "'%s', '%s', '%s', '%s', '%s')";
        Object[] values = new Object[]{
            o.getObjectId(), o.t_foot_exam_assess_id, o.f_foot_assess_type_id,
            Gutil.CheckReservedWords(o.other_detail),
            o.record_date_time, o.user_record_id, o.update_date_time, o.user_update_id};
        return theConnectionInf.eUpdate(String.format(sql, values));
    }
    
    public int update(FootExamAssessDetail o) throws Exception {
        String sql = "UPDATE t_foot_exam_assess_detail "
                + "SET other_detail='%s',"
                + "update_date_time='%s',user_update_id='%s' "
                + "WHERE t_foot_exam_assess_detail_id='%s'";
        Object[] values = new Object[]{
            Gutil.CheckReservedWords(o.other_detail),
            o.update_date_time, o.user_update_id,
            o.getObjectId()};
        return theConnectionInf.eUpdate(String.format(sql, values));

    }

    public int delete(FootExamAssessDetail o) throws Exception {
        String sql = "delete from t_foot_exam_assess_detail "
                + "WHERE t_foot_exam_assess_detail_id='%s'";
        Object[] values = new Object[]{o.getObjectId()};
        return theConnectionInf.eUpdate(String.format(sql, values));
    }

    public List<FootExamAssessDetail> listByFootExamAssessId(String id) throws Exception {
        String sql = "select * from t_foot_exam_assess_detail where t_foot_exam_assess_id = '%s'";
        return eQuery(String.format(sql, id));
    }

    private List<FootExamAssessDetail> eQuery(String sql) throws Exception {
        List<FootExamAssessDetail> list = new ArrayList<FootExamAssessDetail>();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        while (rs.next()) {
            FootExamAssessDetail o = new FootExamAssessDetail();
            o.setObjectId(rs.getString("t_foot_exam_assess_detail_id"));
            o.t_foot_exam_assess_id = rs.getString("t_foot_exam_assess_id");
            o.f_foot_assess_type_id = rs.getString("f_foot_assess_type_id");
            o.other_detail = rs.getString("other_detail");
            o.record_date_time = rs.getString("record_date_time");
            o.user_record_id = rs.getString("user_record_id");
            o.update_date_time = rs.getString("update_date_time");
            o.user_update_id = rs.getString("user_update_id");
            list.add(o);
        }
        rs.close();
        return list;
    }
}

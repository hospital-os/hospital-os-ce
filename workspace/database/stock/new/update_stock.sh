PGPATH="/usr/local/pgsql/bin"
APATH="/home/postgres/stock"
DATABASE="hospital_osv3"
HOST="localhost"
$PGPATH/pg_dump -h $HOST -i -U  postgres  -F p -a -D -v  -t b_employee  $DATABASE  >  $APATH/b_employee.sql
$PGPATH/pg_dump -h $HOST -i -U  postgres  -F p -a -D -v  -t b_item  $DATABASE  >  $APATH/b_item.sql
$PGPATH/pg_dump -h $HOST -i -U  postgres  -F p -a -D -v  -t b_item_price  $DATABASE  >  $APATH/b_item_price.sql
$PGPATH/pg_dump -h $HOST -i -U  postgres  -F p -a -D -v  -t b_service_point  $DATABASE  >  $APATH/b_service_point.sql
$PGPATH/pg_dump -h $HOST -i -U  postgres  -F p -a -D -v  -t b_visit_ward  $DATABASE  >  $APATH/b_visit_ward.sql
$PGPATH/pg_dump -h $HOST -i -U  postgres  -F p -a -D -v  -t b_item_drug_uom  $DATABASE  >  $APATH/b_item_drug_uom.sql
$PGPATH/pg_dump -h $HOST -i -U  postgres  -F p -a -D -v  -t b_site  $DATABASE  >  $APATH/b_site.sql
$PGPATH/pg_dump -h $HOST -i -U  postgres  -F p -a -D -v  -t b_site  $DATABASE  >  $APATH/f_address.sql
$PGPATH/psql -h $HOST $DATABASE -U postgres -f $APATH/deleteTable.sql
$PGPATH/psql -h $HOST $DATABASE -U postgres -f $APATH/b_item.sql
$PGPATH/psql -h $HOST $DATABASE -U postgres -f $APATH/b_item_price.sql
$PGPATH/psql -h $HOST $DATABASE -U postgres -f $APATH/b_service_point.sql
$PGPATH/psql -h $HOST $DATABASE -U postgres -f $APATH/b_visit_ward.sql
$PGPATH/psql -h $HOST $DATABASE -U postgres -f $APATH/b_item_drug_uom.sql
$PGPATH/psql -h $HOST $DATABASE -U postgres -f $APATH/b_site.sql
$PGPATH/psql -h $HOST $DATABASE -U postgres -f $APATH/f_address.sql
$PGPATH/psql -h $HOST $DATABASE -U postgres -f $APATH/b_employee.sql
rm $APATH/b_employee.sql
rm $APATH/b_item.sql
rm $APATH/b_item_price.sql
rm $APATH/b_service_point.sql
rm $APATH/b_visit_ward.sql
rm $APATH/b_item_drug_uom.sql
rm $APATH/b_site.sql
rm $APATH/f_address.sql
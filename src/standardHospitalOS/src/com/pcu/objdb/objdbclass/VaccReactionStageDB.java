/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.objdb.objdbclass;

import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.ComboFix;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tanakrit
 */
public class VaccReactionStageDB {

    public ConnectionInf theConnectionInf;

    public VaccReactionStageDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public List<ComboFix> selectAll() throws Exception {
        String sql = "select * from f_vaccine_reaction_stage order by f_vaccine_reaction_stage_id";
        return veQuery(sql);
    }

    public List<ComboFix> veQuery(String sql) throws Exception {
        ComboFix p;
        List<ComboFix> list = new ArrayList<>();
        try (ResultSet rs = theConnectionInf.eQuery(sql)) {
            while (rs.next()) {
                p = new ComboFix();
                p.code = rs.getString("f_vaccine_reaction_stage_id");
                p.name = rs.getString("vaccine_reaction_stage_name");
                list.add(p);
            }
        }
        return list;
    }
}

ALTER TABLE t_drugfund_billing_receipt ADD COLUMN cancel_reason text DEFAULT '';
ALTER TABLE t_drugfund_cash_type ADD COLUMN drugfund_cash_type_credit_cardname character varying(255) DEFAULT '';

INSERT INTO s_drugfund_version VALUES ('9750000000002', '2', 'Drugfund, Community Edition', '2.1.251011', '1.1.251011', to_char(current_timestamp, 'YYYY-MM-DD HH24:MI:SS'));

package com.hospital_os.objdb;

import com.hospital_os.object.Appointment;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.Gutil;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

@SuppressWarnings("UseOfObsoleteCollectionType")
public class AppointmentDB {

    public ConnectionInf theConnectionInf;
    public Appointment dbObj;
    final public String idtable = "110";

    /**
     * @param ConnectionInf db
     * @roseuid 3F65897F0326
     */
    public AppointmentDB(ConnectionInf db) {
        theConnectionInf = db;
        dbObj = new Appointment();
        initConfig();
    }

    private boolean initConfig() {
        dbObj.table = "t_patient_appointment";
        dbObj.pk_field = "t_patient_appointment_id";
        dbObj.patient_id = "t_patient_id";
        dbObj.date_serv = "patient_appoint_date_time";
        dbObj.appoint_date = "patient_appointment_date";
        dbObj.appoint_time = "patient_appointment_time";
        dbObj.appoint_end_time = "patient_appointment_end_time";
        dbObj.aptype = "patient_appointment";
        dbObj.doctor_code = "patient_appointment_doctor";
        dbObj.servicepoint_code = "patient_appointment_servicepoint";
        dbObj.description = "patient_appointment_notice";
        dbObj.appointmenter = "patient_appointment_staff";
        dbObj.visit_id = "t_visit_id";
        dbObj.auto_visit = "patient_appointment_auto_visit";
        dbObj.queue_visit_id = "b_visit_queue_setup_id";
        dbObj.status = "patient_appointment_status";
        dbObj.vn = "patient_appointment_vn";
        dbObj.appoint_staff_record = "patient_appointment_staff_record";
        dbObj.appoint_record_date_time = "patient_appointment_record_date_time";
        dbObj.appoint_staff_update = "patient_appointment_staff_update";
        dbObj.appoint_update_date_time = "patient_appointment_update_date_time";
        dbObj.appoint_staff_cancel = "patient_appointment_staff_cancel";
        dbObj.appoint_cancel_date_time = "patient_appointment_cancel_date_time";
        dbObj.appoint_active = "patient_appointment_active";
        dbObj.count_appointment = "count_appointment";//amp:7/8/2549
        dbObj.aptype53 = "r_rp1853_aptype_id";
        dbObj.clinic_code = "patient_appointment_clinic";
        dbObj.confirm_date = "appointment_confirm_date";
        dbObj.change_appointment_cause = "change_appointment_cause";
        dbObj.visit_id_make_appointment = "visit_id_make_appointment";
        dbObj.patient_appointment_telehealth = "patient_appointment_telehealth";
        return true;
    }

    /*
     * ////////////////////////////////////////////////////////////////////////////////////
     */
    public int updatePatientByPatient(String old_id, String new_id) throws Exception {
        String sql = "Update " + dbObj.table + " set "
                + dbObj.patient_id + " = '" + new_id + "',"
                + dbObj.description + " = " + dbObj.description + " || " + dbObj.appoint_staff_cancel + "||'-'||'" + old_id + "'"
                + " where " + dbObj.patient_id + " = '" + old_id + "'";
        return theConnectionInf.eUpdate(sql);
    }

    /**
     * @param cmd
     * @param o
     * @return int
     * @roseuid 3F6574DE0394
     */
    public int insert(Appointment p) throws Exception {
        p.generateOID(idtable);
        StringBuffer sql = new StringBuffer("insert into ").append(dbObj.table).
                append(" (").append(dbObj.pk_field).
                append(" ,").append(dbObj.patient_id).
                append(" ,").append(dbObj.visit_id).
                append(" ,").append(dbObj.date_serv).
                append(" ,").append(dbObj.appoint_date).
                append(" ,").append(dbObj.appoint_time).
                append(" ,").append(dbObj.appoint_end_time).
                append(" ,").append(dbObj.aptype).
                append(" ,").append(dbObj.doctor_code).
                append(" ,").append(dbObj.clinic_code).
                append(" ,").append(dbObj.servicepoint_code).
                append(" ,").append(dbObj.description).
                append(" ,").append(dbObj.appointmenter).
                append(" ,").append(dbObj.auto_visit).
                append(" ,").append(dbObj.queue_visit_id).
                append(" ,").append(dbObj.status).
                append(" ,").append(dbObj.confirm_date).
                append(" ,").append(dbObj.vn).
                append(" ,").append(dbObj.visit_id_make_appointment).
                append(" ,").append(dbObj.appoint_staff_record).
                append(" ,").append(dbObj.appoint_record_date_time).
                append(" ,").append(dbObj.appoint_staff_update).
                append(" ,").append(dbObj.appoint_update_date_time).
                append(" ,").append(dbObj.appoint_staff_cancel).
                append(" ,").append(dbObj.appoint_cancel_date_time).
                append(" ,").append(dbObj.appoint_active).
                append(" ,").append(dbObj.aptype53).
                append(" ,").append("patient_appointment_use_set").
                append(" ,").append("patient_appointment_name").
                append(" ,").append("b_template_appointment_id").
                append(" ,").append("patient_appointment_telehealth").
                append(" ) values ('").append(p.getObjectId()).
                append("','").append(p.patient_id).
                append("','").append(p.visit_id).
                append("','").append(p.date_serv).
                append("','").append(p.appoint_date).
                append("','").append(p.appoint_time).
                append("','").append(p.appoint_end_time).
                append("','").append(Gutil.CheckReservedWords(p.aptype)).
                append("',").append(p.doctor_code == null || p.doctor_code.isEmpty() ? "NULL" : "'" + p.doctor_code + "'").
                append(",'").append(p.clinic_code).
                append("','").append(p.servicepoint_code).
                append("','").append(Gutil.CheckReservedWords(p.description)).
                append("','").append(p.appointmenter).
                append("','").append(p.auto_visit).
                append("','").append(p.queue_visit_id).
                append("','").append(p.status).
                append("','").append(p.confirm_date).
                append("','").append(p.vn).
                append("','").append(p.visit_id_make_appointment).
                append("','").append(p.appoint_staff_record).
                append("','").append(p.appoint_record_date_time).
                append("','").append(p.appoint_staff_update).
                append("','").append(p.appoint_update_date_time).
                append("','").append(p.appoint_staff_cancel).
                append("','").append(p.appoint_cancel_date_time).
                append("','").append(p.appoint_active).
                append("','").append(p.aptype53).
                append("',").append(p.patient_appointment_use_set).
                append(",'").append(p.patient_appointment_name).
                append("','").append(p.b_template_appointment_id).
                append("','").append(p.patient_appointment_telehealth).
                append("')");
        return theConnectionInf.eUpdate(sql.toString());
    }

    public int update(Appointment p) throws Exception {
        StringBuffer sql = new StringBuffer("update ").append(dbObj.table).append(" set ").
                append(dbObj.patient_id).append("='").append(p.patient_id).
                append("', ").append(dbObj.visit_id).append("='").append(p.visit_id).
                append("', ").append(dbObj.date_serv).append("='").append(p.date_serv).
                append("', ").append(dbObj.appoint_date).append("='").append(p.appoint_date).
                append("', ").append(dbObj.appoint_time).append("='").append(p.appoint_time).
                append("', ").append(dbObj.appoint_end_time).append("='").append(p.appoint_end_time).
                append("', ").append(dbObj.aptype).append("='").append(Gutil.CheckReservedWords(p.aptype)).append("', ");
        if (p.doctor_code == null || p.doctor_code.isEmpty()) {
            sql.append(dbObj.doctor_code).append("= NULL, ");
        } else {
            sql.append(dbObj.doctor_code).append("='").append(p.doctor_code).append("', ");
        }
        sql.append(dbObj.clinic_code).append("='").append(p.clinic_code).
                append("', ").append(dbObj.servicepoint_code).append("='").append(p.servicepoint_code).
                append("', ").append(dbObj.description).append("='").append(Gutil.CheckReservedWords(p.description)).
                append("', ").append(dbObj.appointmenter).append("='").append(p.appointmenter).
                append("', ").append(dbObj.auto_visit).append("='").append(p.auto_visit).
                append("', ").append(dbObj.queue_visit_id).append("='").append(p.queue_visit_id).
                append("', ").append(dbObj.status).append("='").append(p.status).
                append("', ").append(dbObj.confirm_date).append("='").append(p.confirm_date).
                append("', ").append(dbObj.vn).append("='").append(p.vn).
                append("', ").append(dbObj.visit_id_make_appointment).append("='").append(p.visit_id_make_appointment).
                append("', ").append(dbObj.appoint_staff_record).append("='").append(p.appoint_staff_record).
                append("', ").append(dbObj.appoint_record_date_time).append("='").append(p.appoint_record_date_time).
                append("', ").append(dbObj.appoint_staff_update).append("='").append(p.appoint_staff_update).
                append("', ").append(dbObj.appoint_update_date_time).append("='").append(p.appoint_update_date_time).
                append("', ").append(dbObj.appoint_staff_cancel).append("='").append(p.appoint_staff_cancel).
                append("', ").append(dbObj.appoint_cancel_date_time).append("='").append(p.appoint_cancel_date_time).
                append("', ").append(dbObj.appoint_active).append("='").append(p.appoint_active).
                append("', ").append(dbObj.aptype53).append("='").append(p.aptype53).
                append("', ").append(dbObj.change_appointment_cause).append("='").append(Gutil.CheckReservedWords(p.change_appointment_cause)).
                append("', ").append(dbObj.patient_appointment_telehealth).append("='").append(p.patient_appointment_telehealth).
                append("' where ").append(dbObj.pk_field).append("='").append(p.getObjectId()).append("'");
        return theConnectionInf.eUpdate(sql.toString());
    }

    /*
     * ////////////////////////////////////////////////////////////////////////////
     */
    public int delete(Appointment o) throws Exception {
        StringBuffer sql = new StringBuffer("delete from ").append(dbObj.table).
                append(" where ").append(dbObj.pk_field).append("='").append(o.getObjectId()).append("'");
        return theConnectionInf.eUpdate(sql.toString());
    }

    /*
     * ////////////////////////////////////////////////////////////////////////////
     */
    public Vector selectAll() throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" order by ").append(dbObj.appoint_date).append(" , ").append(dbObj.servicepoint_code);

        Vector v = eQuery(sql.toString());
        if (v.isEmpty()) {
            return null;
        } else {
            return v;
        }
    }

    /**
     * �Ѻ�ҡ�ѹ���Ѩ�غѹ �֧�ѹ���Ѵ���������ش
     *
     * @param pat_id
     * @return
     * @throws Exception
     */
    public Vector selectFinalDate(String pat_id) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where t_patient_id = '").append(pat_id).
                append("' and (patient_appointment_status = '0' or patient_appointment_status = '6') and patient_appointment_active = '1' "
                        + "and to_date((to_number(substr(patient_appointment_date, 1, 4),'9999') - 543 || substr(patient_appointment_date, 5, 6)), 'YYYY-MM-DD') > current_date "
                        + "order by patient_appointment_date limit 1");

        Vector v = eQuery(sql.toString());
        if (v.isEmpty()) {
            return null;
        } else {
            return v;
        }
    }

    /**
     * �Ѻ�ҡ�Ѵ��ѧ�ش����ѧ�����
     *
     * @param pat_id
     * @return
     * @throws Exception
     */
    public Vector selectFinalDate2(String pat_id) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where t_patient_id = '").append(pat_id).
                append("' and patient_appointment_status = '0' and patient_appointment_active = '1' "
                        + "order by patient_appointment_date desc limit 1");

        Vector v = eQuery(sql.toString());
        if (v.isEmpty()) {
            return null;
        } else {
            return v;
        }
    }

    /*
     * ///////////////////////////////////////////////////////////////////////////////
     */
    public Vector selectByDate(String datefrom, String dateto, String patientID) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.appoint_date).append(" >= '").append(datefrom).
                append("' and ").append(dbObj.appoint_date).append(" <= '").append(dateto).
                append("' and ").append(dbObj.patient_id).append(" = '").append(patientID).append("'").
                append(" order by ").append(dbObj.appoint_date);

        Vector v = eQuery(sql.toString());
        if (v.isEmpty()) {
            return null;
        } else {
            return v;
        }
    }

    /*
     * ////////////////////////////////////////////////////////////////////////////
     */
    public Vector selectByDateSP(String datefrom, String dateto, String SP) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.appoint_date).append(" >= '").append(datefrom).
                append("' and ").append(dbObj.appoint_date).append(" <= '").append(dateto).
                append("'and ").append(dbObj.servicepoint_code).append(" = '").append(SP).append("'").
                append(" order by ").append(dbObj.appoint_date);

        Vector v = eQuery(sql.toString());
        if (v.isEmpty()) {
            return null;
        } else {
            return v;
        }
    }

    /*
     * ////////////////////////////////////////////////////////////////////////////
     */
    public Vector selectByDateNoSp(String datefrom, String dateto) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).append(" where ").append(dbObj.appoint_date).append(" >= '").append(datefrom).append("' and ").append(dbObj.appoint_date).append(" <= '").append(dateto).append("' order by ").append(dbObj.appoint_date).append(" , ").append(dbObj.servicepoint_code);

        Vector v = eQuery(sql.toString());
        if (v.isEmpty()) {
            return null;
        } else {
            return v;
        }
    }

    /*
     * ////////////////////////////////////////////////////////////////////////////
     */
    public Vector selectBySP(String SP) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.servicepoint_code).append(" = '").append(SP).append("'").
                append(" order by ").append(dbObj.appoint_date);

        Vector v = eQuery(sql.toString());
        if (v.isEmpty()) {
            return null;
        } else {
            return v;
        }
    }

    /*
     * ////////////////////////////////////////////////////////////////////////////
     */
    public Vector selectByPtId(String ptid) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).append(" where ").append(dbObj.patient_id).append(" = '").append(ptid).append("'").append(" order by ").append(dbObj.appoint_date);

        Vector v = eQuery(sql.toString());
        if (v.isEmpty()) {
            return null;
        } else {
            return v;
        }
    }

    /*
     * ////////////////////////////////////////////////////////////////////////////
     */
    public Appointment selectByPK(String pk) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.pk_field).append(" = '").append(pk).append("'");

        Vector v = eQuery(sql.toString());
        if (v.isEmpty()) {
            return null;
        } else {
            return (Appointment) v.get(0);
        }
    }

    /*
     * ////////////////////////////////////////////////////////////////////////////
     */
    public Appointment select2ByPK(String pk) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.pk_field).append(" = '").append(pk).append("'");

        Vector v = eQuery(sql.toString());
        if (v.isEmpty()) {
            return null;
        } else {
            return (Appointment) v.get(0);
        }
    }

    /*
     * ////////////////////////////////////////////////////////////////////////////
     */
    public Vector selectByDatePid(String date, String pid) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.appoint_date).append(" = '").append(date).append("'").
                append(" and ").append(dbObj.patient_id).append(" = '").append(pid).append("'");
        return eQuery(sql.toString());
    }

    /*
     * ////////////////////////////////////////////////////////////////////////////
     */
    public Vector selectByVid(String vid) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.visit_id).append(" = '").append(vid).append("'");
        return eQuery(sql.toString());
    }

    /*
     * ////////////////////////////////////////////////////////////////////////////
     */
    public Vector selectByVN(String vn) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.vn).append(" = '").append(vn).append("'");
        return eQuery(sql.toString());
    }

    /*
     * ////////////////////////////////////////////////////////////////////////////
     */
    public Vector selectWait(String patient_id) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").
                append(dbObj.table).
                append(" where ").append(dbObj.patient_id).append(" = '").append(patient_id).append("'").
                append(" and (").append(dbObj.status).append(" = '0' or ").append(dbObj.status).append(" = '6')").
                append(" and ").append(dbObj.appoint_active).append(" = '1' ").
                append("order by text_to_timestamp(").append(dbObj.appoint_date).append(")");

        Vector v = eQuery(sql.toString());
        if (v.isEmpty()) {
            return null;
        } else {
            return v;
        }
    }

    /**
     * @Author amp
     * @date 20/06/2549
     * @see �֧�Ѵ���ç�Ѻ�ѹ��� visit
     */
    public Vector selectByRecordDate(String visit, String date) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.visit_id).append(" = '").append(visit).
                append("' and substring(").append(dbObj.date_serv).append(",0,11) = '").append(date.substring(0, 10)).
                append("' order by ").append(dbObj.appoint_date);

        Vector v = eQuery(sql.toString());
        if (v.isEmpty()) {
            return null;
        } else {
            return v;
        }
    }

    /**
     * @Author: amp
     * @date: 7/8/2549
     * @see: �ӹǳ�ӹǹ�ͧ��ùѴ������ѹ
     * @param: �ѹ���Ѵ
     * @return: �ӹǹ��ùѴ�ͧ�ѹ���
     * @note: �� method �����Ѻ countAppointmentByDateAndDoctor ���ͧ�ҡ�պҧ
     * record ���ᾷ����Ѵ�繪�ͧ��ҧ ���� databse develop_hosv3
     */
    public String countAppointmentByDate(String date_appointment) throws Exception {
        StringBuffer sql = new StringBuffer("SELECT Count(").append(dbObj.pk_field).append(") AS count_appointment ").append(
                " FROM ").append(dbObj.table).append(
                " WHERE ").append(dbObj.appoint_date).append(" = '").append(date_appointment).append("'").append(
                " AND ").append(dbObj.appoint_active).append(" = '1'").append(
                " GROUP BY ").append(dbObj.appoint_date);

        Vector v = countAppointmentQuery(sql.toString());
        if (v.isEmpty()) {
            return "0";
        } else {
            return ((Appointment) v.get(0)).count_appointment;
        }
    }

    public int countByDateTime(String dateAppointment, String startDate, String endDate, Map<String, String> params) throws Exception {
        String sql = "select \n"
                + "count(*) as count_appointment\n"
                + "from public.t_patient_appointment\n"
                + "where \n"
                + "t_patient_appointment.patient_appointment_date = ?\n"
                + "and (t_patient_appointment.patient_appointment_active = '1'\n"
                + "and t_patient_appointment.patient_appointment_status <> '3')\n";

        if (startDate != null && !startDate.isEmpty() && endDate != null && !endDate.isEmpty()) {
            sql += "and (t_patient_appointment.patient_appointment_time::time, t_patient_appointment.patient_appointment_end_time::time) OVERLAPS (?::time, ?::time) \n";
        }
        Set<String> keySet = params != null ? params.keySet() : null;
        if (keySet != null) {
            for (String key : keySet) {
                String id = params.get(key);
                if (key.equals("doctorId")) {
                    if (id != null && !id.isEmpty()) {
                        sql += "and t_patient_appointment.patient_appointment_doctor = ?\n";
                    } else {
                        sql += "and t_patient_appointment.patient_appointment_doctor is null\n";
                    }
                }
                if (id == null || id.isEmpty()) {
                    continue;
                }
                switch (key) {
                    case "appId":
                        sql += "and t_patient_appointment.t_patient_appointment_id <> ?\n";
                        break;
                    case "clinicId":
                        sql += "and t_patient_appointment.patient_appointment_clinic = ?\n";
                        break;
                    case "servicepointId":
                        sql += "and t_patient_appointment.patient_appointment_servicepoint = ?\n";
                        break;
                    default:
                        break;
                }
            }
        }

        PreparedStatement ePQuery = theConnectionInf.ePQuery(sql);
        int index = 1;
        ePQuery.setString(index++, dateAppointment);
        if (startDate != null && !startDate.isEmpty() && endDate != null && !endDate.isEmpty()) {
            ePQuery.setString(index++, startDate);
            ePQuery.setString(index++, endDate);
        }
        if (keySet != null) {
            for (String key : keySet) {
                String id = params.get(key);
                if (id == null || id.isEmpty()) {
                    continue;
                }
                ePQuery.setString(index++, id);
            }
        }
        int count;
        try (ResultSet rs = theConnectionInf.eQuery(ePQuery.toString())) {
            count = 0;
            while (rs.next()) {
                count = rs.getInt("count_appointment");
            }
        }
        return count;
    }

    public int countByDateDoctor(String date_appointment, String doctor_id) throws Exception {
        StringBuffer sql = new StringBuffer("SELECT Count(").append(dbObj.pk_field).append(") AS count_appointment ").append(
                " FROM ").append(dbObj.table).append(
                " WHERE ").append(dbObj.appoint_date).append(" = '").append(date_appointment).append("'");
        if (doctor_id == null || doctor_id.isEmpty()) {
            sql.append(" AND ").append(dbObj.doctor_code).append(" is null");
        } else {
            sql.append(" AND ").append(dbObj.doctor_code).append(" = '").append(doctor_id).append("'");
        }

        sql.append(" AND ").append(dbObj.appoint_active).append(" = '1'");

        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        int count = 0;
        while (rs.next()) {
            count = rs.getInt("count_appointment");
        }
        rs.close();
        return count;
    }

    /**
     * @Author: amp
     * @date: 7/8/2549
     * @see: �ӹǳ�ӹǹ�ͧ��ùѴ������ѹ�ͧ����ᾷ��
     * @param: �ѹ���Ѵ,Key_id �ͧ employee �ͧᾷ�������͡
     * @return: �ӹǹ��ùѴ�ͧ�ѹ���ᾷ�����к�
     */
    public String countAppointmentByDateAndDoctor(String date_appointment, String doctor_id) throws Exception {
        StringBuffer sql = new StringBuffer("SELECT Count(").append(dbObj.pk_field).append(") AS count_appointment ").append(
                " FROM ").append(dbObj.table).append(
                " WHERE ").append(dbObj.appoint_date).append(" = '").append(date_appointment).append("'");

        if (doctor_id == null || doctor_id.isEmpty()) {
            sql.append(" AND ").append(dbObj.doctor_code).append(" is null");
        } else {
            sql.append(" AND ").append(dbObj.doctor_code).append(" = '").append(doctor_id).append("'");
        }

        sql.append(" AND ").append(dbObj.appoint_active).append(" = '1'");

        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        String count = "";
        while (rs.next()) {
            count = rs.getString("count_appointment");
        }
        rs.close();
        return count;
    }

    /**
     * @Author: henbe
     * @date: 7/7/2550
     * @see: �ӹǳ�ӹǹ�ͧ��ùѴ������ѹ�ͧ���Шش��ԡ��
     * @param: �ѹ���Ѵ,Key_id �ͧ employee �ͧᾷ�������͡
     * @return: �ӹǹ��ùѴ�ͧ�ѹ��Шش��ԡ�÷���к�
     */
    public String countAppointmentByDateSpid(String date_appointment, String spid) throws Exception {
        StringBuffer sql = new StringBuffer("SELECT Count(").append(dbObj.pk_field).append(") AS count_appointment ").append(
                " FROM ").append(dbObj.table).append(
                " WHERE ").append(dbObj.appoint_date).append(" = '").append(date_appointment).append("'").append(
                " AND ").append(dbObj.servicepoint_code).append(" = '").append(spid).append("'").append(
                " AND ").append(dbObj.appoint_active).append(" = '1'");

        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        String count = "";
        while (rs.next()) {
            count = rs.getString("count_appointment");
        }
        rs.close();
        return count;
    }

    /**
     * @Author: amp
     * @date: 7/8/2549
     * @see: �ӹǳ�ӹǹ�ͧ��ùѴ������ѹ
     * @deprecated henbe unused
     */
    public Vector countAppointmentQuery(String sql) throws Exception {
        Appointment p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        while (rs.next()) {
            p = new Appointment();
            p.count_appointment = rs.getString(dbObj.count_appointment);
            list.add(p);
        }
        rs.close();
        return list;
    }

    /*
     * ////////////////////////////////////////////////////////////////////////////
     */
    public Vector eQuery(String sql) throws Exception {
        Appointment p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        while (rs.next()) {
            p = new Appointment();
            p.setObjectId(rs.getString(dbObj.pk_field));
            p.patient_id = rs.getString(dbObj.patient_id);
            p.visit_id = rs.getString(dbObj.visit_id);
            p.date_serv = rs.getString(dbObj.date_serv);
            p.appoint_date = rs.getString(dbObj.appoint_date);
            p.appoint_time = rs.getString(dbObj.appoint_time);
            p.appoint_end_time = rs.getString(dbObj.appoint_end_time);
            p.aptype = rs.getString(dbObj.aptype);
            p.doctor_code = rs.getString(dbObj.doctor_code);
            p.clinic_code = rs.getString(dbObj.clinic_code);
            p.servicepoint_code = rs.getString(dbObj.servicepoint_code);
            p.description = rs.getString(dbObj.description);
            p.appointmenter = rs.getString(dbObj.appointmenter);
            p.auto_visit = rs.getString(dbObj.auto_visit);
            p.queue_visit_id = rs.getString(dbObj.queue_visit_id);
            p.status = rs.getString(dbObj.status);
            p.confirm_date = rs.getString(dbObj.confirm_date);
            p.vn = rs.getString(dbObj.vn);
            p.visit_id_make_appointment = rs.getString(dbObj.visit_id_make_appointment);
            p.appoint_staff_record = rs.getString(dbObj.appoint_staff_record);
            p.appoint_record_date_time = rs.getString(dbObj.appoint_record_date_time);
            p.appoint_staff_update = rs.getString(dbObj.appoint_staff_update);
            p.appoint_update_date_time = rs.getString(dbObj.appoint_update_date_time);
            p.appoint_staff_cancel = rs.getString(dbObj.appoint_staff_cancel);
            p.appoint_cancel_date_time = rs.getString(dbObj.appoint_cancel_date_time);
            p.appoint_active = rs.getString(dbObj.appoint_active);
            p.aptype53 = rs.getString(dbObj.aptype53);
            p.change_appointment_cause = rs.getString(dbObj.change_appointment_cause);
            p.patient_appointment_use_set = rs.getBoolean("patient_appointment_use_set");
            p.patient_appointment_name = rs.getString("patient_appointment_name");
            p.b_template_appointment_id = rs.getString("b_template_appointment_id");
            p.patient_appointment_telehealth = rs.getString("patient_appointment_telehealth");
            list.add(p);
        }
        rs.close();
        return list;
    }

    public Vector listByPIdFromCurrentDate(String pid) throws Exception {
        String sql = "select * "
                + "from t_patient_appointment where t_patient_id = '%s' "
                + "and (patient_appointment_status = '0' or patient_appointment_status = '6') and patient_appointment_active = '1'"
                + "and to_date((to_number(substring(patient_appointment_date from 1 for 4), '9999') - 543) "
                + "|| substring(patient_appointment_date from 5 for 6), 'YYYY-MM-DD') >= CURRENT_DATE";
        return eQuery(String.format(sql, pid));

    }

    public Vector listOtherAppointmentInSametimeByClinicAndDoctor(String appointmentDate, String clinicId, String doctorId, String startTime, String endTime, String appointmentId) throws Exception {
        String sql = "select *\n"
                + "from t_patient_appointment\n"
                + "inner join b_visit_clinic on b_visit_clinic.b_visit_clinic_id = t_patient_appointment.patient_appointment_clinic and b_visit_clinic.enable_sametime_appointment = '0'\n"
                + "where \n"
                + "t_patient_appointment.patient_appointment_date = ?\n"
                + "and t_patient_appointment.patient_appointment_clinic = ?\n"
                + "and (t_patient_appointment.patient_appointment_time::time, t_patient_appointment.patient_appointment_end_time::time) OVERLAPS (?::time, ?::time)\n"
                + "and (t_patient_appointment.patient_appointment_active = '1'\n"
                + "and t_patient_appointment.patient_appointment_status <> '3')\n";
        if (doctorId != null) {
            sql += "and t_patient_appointment.patient_appointment_doctor = ?\n";
        } else {
            sql += "and t_patient_appointment.patient_appointment_doctor is null\n";
        }
        if (appointmentId != null) {
            sql += "and t_patient_appointment.t_patient_appointment_id <> ?";
        }
        PreparedStatement ePQuery = theConnectionInf.ePQuery(sql);
        int index = 1;
        ePQuery.setString(index++, appointmentDate);
        ePQuery.setString(index++, clinicId);
        ePQuery.setString(index++, startTime);
        ePQuery.setString(index++, endTime);
        if (doctorId != null) {
            ePQuery.setString(index++, doctorId);
        }
        if (appointmentId != null) {
            ePQuery.setString(index++, appointmentId);
        }
        return eQuery(ePQuery.toString());
    }

    public Vector listAppointmentInSamePersonInDate(String appointmentDate, String patientId, String appointmentId) throws Exception {
        String sql = "select * from t_patient_appointment\n"
                + "where \n"
                + "t_patient_appointment.patient_appointment_date = ?\n"
                + "and t_patient_appointment.t_patient_id = ?\n"
                + "and (t_patient_appointment.patient_appointment_active = '1'\n"
                + "and t_patient_appointment.patient_appointment_status <> '3')\n";
        if (appointmentId != null) {
            sql += "and t_patient_appointment.t_patient_appointment_id <> ?";
        }
        PreparedStatement ePQuery = theConnectionInf.ePQuery(sql);
        int index = 1;
        ePQuery.setString(index++, appointmentDate);
        ePQuery.setString(index++, patientId);
        if (appointmentId != null) {
            ePQuery.setString(index++, appointmentId);
        }
        return eQuery(ePQuery.toString());
    }

    public Appointment getLastRecordAppointment(String patient_id) throws Exception {
        String sql = "select * from t_patient_appointment where t_patient_id = ? "
                + "and (patient_appointment_status = '0' or patient_appointment_status = '6') "
                + "and patient_appointment_active = '1' "
                + "order by record_date_time desc "
                + "limit 1";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            ePQuery.setString(1, patient_id);
            Vector<Appointment> list = eQuery(ePQuery.toString());
            return list.isEmpty() ? null : list.get(0);
        }
    }
}

-- isseus#667
-- t_person
ALTER TABLE t_person ALTER COLUMN t_health_home_id SET DEFAULT '7140000000000';
ALTER TABLE t_person ALTER COLUMN person_family_number SET DEFAULT '';
ALTER TABLE t_person ALTER COLUMN f_prefix_id SET DEFAULT '000';
ALTER TABLE t_person ALTER COLUMN person_firstname SET NOT NULL;
ALTER TABLE t_person ALTER COLUMN person_lastname SET NOT NULL;
ALTER TABLE t_person ALTER COLUMN person_passport SET DEFAULT '';
ALTER TABLE t_person ALTER COLUMN person_birthday SET DEFAULT (to_number(to_char(CURRENT_DATE::timestamp with time zone, 'YYYY'::text), '9999'::text) + 543::numeric) || to_char(CURRENT_DATE::timestamp with time zone, '-MM-DD'::text);
ALTER TABLE t_person ALTER COLUMN person_birthday SET NOT NULL;
ALTER TABLE t_person ALTER COLUMN f_sex_id SET DEFAULT '3';
ALTER TABLE t_person ALTER COLUMN f_patient_blood_group_id SET DEFAULT '1';
ALTER TABLE t_person ALTER COLUMN f_rh_group_id SET DEFAULT '9';
ALTER TABLE t_person ALTER COLUMN f_patient_nation_id SET DEFAULT '99';
ALTER TABLE t_person ALTER COLUMN f_patient_race_id SET DEFAULT '99';
ALTER TABLE t_person ALTER COLUMN f_patient_religion_id SET DEFAULT '1';
ALTER TABLE t_person ALTER COLUMN father_person_id SET DEFAULT NULL;
ALTER TABLE t_person ALTER COLUMN mother_person_id SET DEFAULT NULL;
ALTER TABLE t_person ALTER COLUMN couple_person_id SET DEFAULT NULL;
ALTER TABLE t_person ALTER COLUMN f_patient_marriage_status_id SET DEFAULT '1';
ALTER TABLE t_person ALTER COLUMN f_patient_education_type_id SET DEFAULT '11';
ALTER TABLE t_person ALTER COLUMN f_patient_occupation_id SET DEFAULT '000';
ALTER TABLE t_person ALTER COLUMN f_patient_family_status_id SET DEFAULT '';
ALTER TABLE t_person ALTER COLUMN f_patient_area_status_id SET DEFAULT '4';
ALTER TABLE t_person ALTER COLUMN f_person_foreigner_id SET DEFAULT '';
ALTER TABLE t_person ALTER COLUMN person_foreigner_card_no SET DEFAULT '';
ALTER TABLE t_person ALTER COLUMN person_revenu SET DEFAULT '';
ALTER TABLE t_person ALTER COLUMN person_move_in_date_time SET DEFAULT '';
ALTER TABLE t_person ALTER COLUMN record_date_time SET DEFAULT (((to_number(to_char(CURRENT_DATE::timestamp with time zone, 'YYYY'::text), '9999'::text) + 543::numeric) || to_char(CURRENT_TIMESTAMP, '-MM-DD,HH24:MI:SS'::text)));
ALTER TABLE t_person ALTER COLUMN modify_date_time SET DEFAULT (((to_number(to_char(CURRENT_DATE::timestamp with time zone, 'YYYY'::text), '9999'::text) + 543::numeric) || to_char(CURRENT_TIMESTAMP, '-MM-DD,HH24:MI:SS'::text)));
ALTER TABLE t_person ALTER COLUMN user_cancel_id SET DEFAULT NULL;
ALTER TABLE t_person ALTER COLUMN cancel_date_time SET DEFAULT NULL;

-- b_employee
UPDATE b_employee SET employee_firstname = t_person.person_firstname
	,employee_lastname = t_person.person_lastname
FROM t_person
WHERE b_employee.t_person_id = t_person.t_person_id
AND b_employee.employee_firstname IS NULL
AND b_employee.employee_lastname IS NULL;

ALTER TABLE b_employee ALTER COLUMN employee_login SET NOT NULL;
ALTER TABLE b_employee ADD CONSTRAINT b_employee_unique1 UNIQUE (employee_login);
ALTER TABLE b_employee ALTER COLUMN employee_password SET NOT NULL;
ALTER TABLE b_employee ALTER COLUMN employee_last_login SET DEFAULT '';
ALTER TABLE b_employee ALTER COLUMN employee_last_logout SET DEFAULT '';
ALTER TABLE b_employee ALTER COLUMN employee_active SET DEFAULT '1';
ALTER TABLE b_employee ALTER COLUMN employee_active SET NOT NULL;
ALTER TABLE b_employee ALTER COLUMN b_service_point_id SET NOT NULL;
ALTER TABLE b_employee ALTER COLUMN f_employee_level_id SET DEFAULT NULL;
ALTER TABLE b_employee ALTER COLUMN f_employee_rule_id SET DEFAULT NULL;
ALTER TABLE b_employee ALTER COLUMN f_employee_authentication_id SET NOT NULL;
ALTER TABLE b_employee ALTER COLUMN b_employee_default_tab SET DEFAULT '1';
ALTER TABLE b_employee ALTER COLUMN f_provider_council_code_id SET DEFAULT '0';
ALTER TABLE b_employee ALTER COLUMN f_provider_type_id SET DEFAULT '000009';
ALTER TABLE b_employee ALTER COLUMN employee_number_issue_date SET DEFAULT NULL;
ALTER TABLE b_employee ALTER COLUMN b_visit_clinic_id SET DEFAULT NULL;

-- t_health_family
ALTER TABLE t_health_family ALTER COLUMN t_health_home_id SET DEFAULT '7140000000000';
ALTER TABLE t_health_family ALTER COLUMN t_health_family_number SET DEFAULT '';
ALTER TABLE t_health_family ALTER COLUMN f_prefix_id SET DEFAULT '000';
ALTER TABLE t_health_family ALTER COLUMN patient_birthday SET DEFAULT (to_number(to_char(CURRENT_DATE::timestamp with time zone, 'YYYY'::text), '9999'::text) + 543::numeric) || to_char(CURRENT_DATE::timestamp with time zone, '-MM-DD'::text);
ALTER TABLE t_health_family ALTER COLUMN patient_work_office SET DEFAULT '';
ALTER TABLE t_health_family ALTER COLUMN modify_date_time SET DEFAULT (((to_number(to_char(CURRENT_DATE::timestamp with time zone, 'YYYY'::text), '9999'::text) + 543::numeric) || to_char(CURRENT_TIMESTAMP, '-MM-DD,HH24:MI:SS'::text)));
ALTER TABLE t_health_family ALTER COLUMN cancel_date_time SET DEFAULT '';
ALTER TABLE t_health_family ALTER COLUMN health_family_staff_modify SET DEFAULT '1570000000001';
ALTER TABLE t_health_family ALTER COLUMN health_family_staff_cancel SET DEFAULT '';
ALTER TABLE t_health_family ALTER COLUMN father_family_id SET DEFAULT '';
ALTER TABLE t_health_family ALTER COLUMN mother_family_id SET DEFAULT '';
ALTER TABLE t_health_family ALTER COLUMN couple_family_id SET DEFAULT '';
ALTER TABLE t_health_family ALTER COLUMN skin_color SET DEFAULT NULL;
ALTER TABLE t_health_family ALTER COLUMN patient_firstname_eng SET DEFAULT NULL;
ALTER TABLE t_health_family ALTER COLUMN patient_lastname_eng SET DEFAULT NULL;
ALTER TABLE t_health_family ALTER COLUMN passport_no SET DEFAULT NULL;

-- t_patient
ALTER TABLE t_patient ALTER COLUMN patient_hn SET NOT NULL;
ALTER TABLE t_patient ALTER COLUMN patient_birthday SET DEFAULT (to_number(to_char(CURRENT_DATE::timestamp with time zone, 'YYYY'::text), '9999'::text) + 543::numeric) || to_char(CURRENT_DATE::timestamp with time zone, '-MM-DD'::text);
ALTER TABLE t_patient ALTER COLUMN patient_house SET DEFAULT '';
ALTER TABLE t_patient ALTER COLUMN patient_road SET DEFAULT '';
ALTER TABLE t_patient ALTER COLUMN patient_moo SET DEFAULT '';
ALTER TABLE t_patient ALTER COLUMN patient_tambon SET DEFAULT '';
ALTER TABLE t_patient ALTER COLUMN patient_amphur SET DEFAULT '';
ALTER TABLE t_patient ALTER COLUMN patient_changwat SET DEFAULT '';
ALTER TABLE t_patient ALTER COLUMN patient_father_firstname SET DEFAULT '';
ALTER TABLE t_patient ALTER COLUMN patient_mother_firstname SET DEFAULT '';
ALTER TABLE t_patient ALTER COLUMN patient_couple_firstname SET DEFAULT '';
ALTER TABLE t_patient ALTER COLUMN patient_move_in_date_time SET DEFAULT (((to_number(to_char(CURRENT_DATE::timestamp with time zone, 'YYYY'::text), '9999'::text) + 543::numeric) || to_char(CURRENT_TIMESTAMP, '-MM-DD,HH24:MI:SS'::text)));
ALTER TABLE t_patient ALTER COLUMN f_patient_discharge_status_id SET DEFAULT '9';
ALTER TABLE t_patient ALTER COLUMN patient_discharge_date_time SET DEFAULT '';
ALTER TABLE t_patient ALTER COLUMN f_patient_blood_group_id SET DEFAULT '1';
ALTER TABLE t_patient ALTER COLUMN f_patient_foreigner_id SET DEFAULT '1';
ALTER TABLE t_patient ALTER COLUMN patient_community_status SET DEFAULT '';
ALTER TABLE t_patient ALTER COLUMN patient_private_doctor SET DEFAULT '';
ALTER TABLE t_patient ALTER COLUMN patient_mother_lastname SET DEFAULT '';
ALTER TABLE t_patient ALTER COLUMN patient_father_lastname SET DEFAULT '';
ALTER TABLE t_patient ALTER COLUMN patient_couple_lastname SET DEFAULT '';
ALTER TABLE t_patient ALTER COLUMN patient_phone_number SET DEFAULT '';
ALTER TABLE t_patient ALTER COLUMN patient_contact_phone_number SET DEFAULT '';
ALTER TABLE t_patient ALTER COLUMN patient_contact_house SET DEFAULT '';
ALTER TABLE t_patient ALTER COLUMN patient_contact_moo SET DEFAULT '';
ALTER TABLE t_patient ALTER COLUMN patient_contact_tambon SET DEFAULT '';
ALTER TABLE t_patient ALTER COLUMN patient_contact_amphur SET DEFAULT '';
ALTER TABLE t_patient ALTER COLUMN patient_contact_changwat SET DEFAULT '';
ALTER TABLE t_patient ALTER COLUMN patient_contact_road SET DEFAULT '';
ALTER TABLE t_patient ALTER COLUMN patient_contact_firstname SET DEFAULT '';
ALTER TABLE t_patient ALTER COLUMN patient_contact_lastname SET DEFAULT '';
ALTER TABLE t_patient ALTER COLUMN patient_merged SET DEFAULT NULL;
ALTER TABLE t_patient ALTER COLUMN patient_update_date_time SET DEFAULT (((to_number(to_char(CURRENT_DATE::timestamp with time zone, 'YYYY'::text), '9999'::text) + 543::numeric) || to_char(CURRENT_TIMESTAMP, '-MM-DD,HH24:MI:SS'::text)));
ALTER TABLE t_patient ALTER COLUMN patient_staff_modify SET DEFAULT '1570000000001';
ALTER TABLE t_patient ALTER COLUMN patient_staff_cancel SET DEFAULT '';
ALTER TABLE t_patient ALTER COLUMN patient_postcode SET DEFAULT NULL;
ALTER TABLE t_patient ALTER COLUMN patient_contact_postcode SET DEFAULT NULL;

-- b_item
ALTER TABLE b_item ALTER COLUMN item_number SET NOT NULL;
ALTER TABLE b_item ALTER COLUMN item_common_name SET NOT NULL;
ALTER TABLE b_item ALTER COLUMN b_item_subgroup_id SET NOT NULL;
ALTER TABLE b_item ALTER COLUMN b_item_billing_subgroup_id DROP NOT NULL;
ALTER TABLE b_item ALTER COLUMN b_item_billing_subgroup_id SET DEFAULT NULL;
ALTER TABLE b_item ALTER COLUMN b_item_16_group_id DROP NOT NULL;
ALTER TABLE b_item ALTER COLUMN b_item_16_group_id SET DEFAULT NULL;
ALTER TABLE b_item ADD CONSTRAINT b_item_unique1 UNIQUE (item_number);
ALTER TABLE b_item ALTER COLUMN item_active SET DEFAULT '1';
ALTER TABLE b_item ALTER COLUMN item_active SET NOT NULL;
ALTER TABLE b_item ALTER COLUMN item_trade_name SET DEFAULT '';
ALTER TABLE b_item ALTER COLUMN item_nick_name SET DEFAULT '';
ALTER TABLE b_item ALTER COLUMN r_rp1253_adpcode_id SET DEFAULT NULL;
ALTER TABLE b_item ALTER COLUMN r_rp1253_charitem_id SET DEFAULT NULL;
ALTER TABLE b_item ALTER COLUMN item_unit_packing_qty SET DEFAULT '';
ALTER TABLE b_item ALTER COLUMN f_item_lab_type_id SET DEFAULT NULL;
ALTER TABLE b_item ALTER COLUMN item_local_name SET DEFAULT '';
ALTER TABLE b_item ALTER COLUMN f_item_lab_location_id SET DEFAULT NULL;

-- b_item_price
ALTER TABLE b_item_price ALTER COLUMN b_item_id SET NOT NULL;
ALTER TABLE b_item_price ALTER COLUMN item_price_active_date SET DEFAULT (((to_number(to_char(CURRENT_DATE::timestamp with time zone, 'YYYY'::text), '9999'::text) + 543::numeric) || to_char(CURRENT_TIMESTAMP, '-MM-DD,HH24:MI:SS'::text)));
ALTER TABLE b_item_price ALTER COLUMN item_price_active_date SET NOT NULL;
ALTER TABLE b_item_price ALTER COLUMN item_price SET DEFAULT 0;
ALTER TABLE b_item_price ALTER COLUMN item_price_cost SET DEFAULT 0;
ALTER TABLE b_item_price ALTER COLUMN item_price SET NOT NULL;
ALTER TABLE b_item_price ALTER COLUMN item_price_cost SET NOT NULL;

-- b_item_drug
ALTER TABLE b_item_drug ALTER COLUMN item_drug_printable SET DEFAULT '1';
ALTER TABLE b_item_drug ALTER COLUMN item_drug_printable SET NOT NULL;
ALTER TABLE b_item_drug ALTER COLUMN b_item_id SET NOT NULL;
ALTER TABLE b_item_drug ALTER COLUMN item_drug_caution SET DEFAULT '';
ALTER TABLE b_item_drug ALTER COLUMN item_drug_description SET DEFAULT '';
ALTER TABLE b_item_drug ALTER COLUMN item_drug_day_time SET DEFAULT NULL;
ALTER TABLE b_item_drug ALTER COLUMN item_drug_special_prescription_text SET DEFAULT '';
ALTER TABLE b_item_drug ALTER COLUMN hight_alert SET DEFAULT NULL;
ALTER TABLE b_item_drug ALTER COLUMN b_item_manufacturer_id SET DEFAULT NULL;
ALTER TABLE b_item_drug ALTER COLUMN b_item_distributor_id SET DEFAULT NULL;

-- b_item_supply
ALTER TABLE b_item_supply ALTER COLUMN record_date_time SET DEFAULT (((to_number(to_char(CURRENT_DATE::timestamp with time zone, 'YYYY'::text), '9999'::text) + 543::numeric) || to_char(CURRENT_TIMESTAMP, '-MM-DD,HH24:MI:SS'::text)));
ALTER TABLE b_item_supply ALTER COLUMN update_date_time SET DEFAULT (((to_number(to_char(CURRENT_DATE::timestamp with time zone, 'YYYY'::text), '9999'::text) + 543::numeric) || to_char(CURRENT_TIMESTAMP, '-MM-DD,HH24:MI:SS'::text)));

-- b_item_xray
ALTER TABLE b_item_xray ALTER COLUMN b_item_xray_id TYPE VARCHAR(255);
ALTER TABLE b_item_xray ALTER COLUMN b_item_id TYPE VARCHAR(255);
ALTER TABLE b_item_xray ALTER COLUMN b_modality_id TYPE VARCHAR(255);
ALTER TABLE b_item_xray ALTER COLUMN user_record_id TYPE VARCHAR(255);
ALTER TABLE b_item_xray ALTER COLUMN user_update_id TYPE VARCHAR(255);

-- update db version
INSERT INTO s_version VALUES ('9701000000116', '116', 'Hospital OS, Community Edition', '3.12.0', '3.54.0', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_12_0.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.12.0');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
public class VisitInsurancePlan extends Persistent {

    public String t_visit_payment_id = "";
    public String b_finance_insurance_company_id = "";
    public String b_finance_insurance_plan_id = "";
    public String priority = "";
    public String active = "1";
    public String record_datetime = "";
    public String user_record_id = "";
    public String update_datetime = "";
    public String user_update_id = "";
    // in object only
    public String b_finance_insurance_company_name = "";
    public String b_finance_insurance_plan_name = "";
}

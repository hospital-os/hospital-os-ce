/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.objdb.objdbclass;

import com.hospital_os.usecase.connection.ConnectionInf;
import com.pcu.object.VaccineCovid19Reaction;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tanakrit
 */
public class VaccineCovid19ReactionDB {

    public ConnectionInf theConnectionInf;
    final private String idtable = "785";

    public VaccineCovid19ReactionDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public int insert(VaccineCovid19Reaction p) throws Exception {
        p.generateOID(idtable);
        String sql = "INSERT INTO t_health_vaccine_covid19_reaction( \n"
                + "               t_health_vaccine_covid19_reaction_id, t_health_vaccine_covid19_id, t_patient_id, t_visit_id, \n"
                + "               reaction_no, reaction_stage, f_vaccine_reaction_stage_id, reaction_date, \n"
                + "               adverse_reaction, adverse_description, f_vaccine_reaction_type_id, f_vaccine_reaction_symptom_id, \n"
                + "               symptom_description, active, \n"
                + "               user_record_id, record_date_time, user_update_id) \n"
                + "        VALUES(?, ?, ?, ?, "
                + "               ?, ?, ?, ?, "
                + "               ?, ?, ?, ?, "
                + "               ?, ?, "
                + "               ?, ?, ?)";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, p.getObjectId());
            ePQuery.setString(index++, p.t_health_vaccine_covid19_id);
            ePQuery.setString(index++, p.t_patient_id);
            ePQuery.setString(index++, p.t_visit_id);
            ePQuery.setInt(index++, p.reaction_no);
            ePQuery.setInt(index++, p.reaction_stage);
            ePQuery.setInt(index++, Integer.parseInt(p.f_vaccine_reaction_stage_id));
            ePQuery.setDate(index++, p.reaction_date != null ? new java.sql.Date(p.reaction_date.getTime()) : null);
            ePQuery.setInt(index++, p.adverse_reaction);
            ePQuery.setString(index++, p.adverse_description);
            ePQuery.setInt(index++, Integer.parseInt(p.f_vaccine_reaction_type_id));
            ePQuery.setInt(index++, Integer.parseInt(p.f_vaccine_reaction_symptom_id));
            ePQuery.setString(index++, p.symptom_description);
            ePQuery.setString(index++, p.active);
            ePQuery.setString(index++, p.user_record_id);
            ePQuery.setTimestamp(index++, p.record_date_time != null ? new java.sql.Timestamp(p.record_date_time.getTime()) : null);
            ePQuery.setString(index++, p.user_update_id);
            return ePQuery.executeUpdate();
        }
    }

    public int update(VaccineCovid19Reaction p) throws Exception {
        String sql = "UPDATE t_health_vaccine_covid19_reaction \n"
                + "      SET t_health_vaccine_covid19_id=?, t_patient_id=?, t_visit_id=?, \n"
                + "          reaction_no=?, reaction_stage=?, f_vaccine_reaction_stage_id=?, reaction_date=?, \n"
                + "          adverse_reaction=?, adverse_description=?, f_vaccine_reaction_type_id=?, f_vaccine_reaction_symptom_id=?, \n"
                + "          symptom_description=?, active=?, record_date_time=?, update_date_time=CURRENT_TIMESTAMP, user_update_id=? \n";
        if (p.active.equals("0")) {
            sql += "        ,cancel_date_time=CURRENT_TIMESTAMP, user_cancel_id=?";
        }
        sql += "       WHERE t_health_vaccine_covid19_reaction_id=?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, p.t_health_vaccine_covid19_id);
            ePQuery.setString(index++, p.t_patient_id);
            ePQuery.setString(index++, p.t_visit_id);
            ePQuery.setInt(index++, p.reaction_no);
            ePQuery.setInt(index++, p.reaction_stage);
            ePQuery.setInt(index++, Integer.parseInt(p.f_vaccine_reaction_stage_id));
            ePQuery.setDate(index++, p.reaction_date != null ? new java.sql.Date(p.reaction_date.getTime()) : null);
            ePQuery.setInt(index++, p.adverse_reaction);
            ePQuery.setString(index++, p.adverse_description);
            ePQuery.setInt(index++, Integer.parseInt(p.f_vaccine_reaction_type_id));
            ePQuery.setInt(index++, Integer.parseInt(p.f_vaccine_reaction_symptom_id));
            ePQuery.setString(index++, p.symptom_description);
            ePQuery.setString(index++, p.active);
            ePQuery.setTimestamp(index++, p.record_date_time != null ? new java.sql.Timestamp(p.record_date_time.getTime()) : null);
            ePQuery.setString(index++, p.user_update_id);
            if (p.active.equals("0")) {
                ePQuery.setString(index++, p.user_cancel_id);
            }
            ePQuery.setString(index++, p.getObjectId());
            return ePQuery.executeUpdate();
        }
    }

    public int inActiveByVaccineCovid19Id(String active, String user, String vaccineId) throws Exception {
        String sql = "UPDATE t_health_vaccine_covid19_reaction \n"
                + "      SET active=?, update_date_time=CURRENT_TIMESTAMP, user_update_id=?, \n"
                + "          cancel_date_time=CURRENT_TIMESTAMP, user_cancel_id=? \n"
                + "    WHERE t_health_vaccine_covid19_id=?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, active);
            ePQuery.setString(index++, user);
            ePQuery.setString(index++, user);
            ePQuery.setString(index++, vaccineId);
            return ePQuery.executeUpdate();
        }
    }

    public int inActiveByPk(VaccineCovid19Reaction p) throws Exception {
        String sql = "UPDATE t_health_vaccine_covid19_reaction \n"
                + "      SET active=?, update_date_time=CURRENT_TIMESTAMP, user_update_id=?, \n"
                + "          cancel_date_time=CURRENT_TIMESTAMP, user_cancel_id=? \n"
                + "    WHERE t_health_vaccine_covid19_reaction_id=?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, p.active);
            ePQuery.setString(index++, p.user_update_id);
            ePQuery.setString(index++, p.user_cancel_id);
            ePQuery.setString(index++, p.getObjectId());
            return ePQuery.executeUpdate();
        }
    }

    public List<VaccineCovid19Reaction> selectByVaccineCovid19Id(String VaccineCovid19Id) throws Exception {
        String sql = "select * from t_health_vaccine_covid19_reaction \n"
                + " where active='1' and t_health_vaccine_covid19_id = ? \n"
                + " order by reaction_no";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, VaccineCovid19Id);
            return executeQuery(ePQuery);
        }
    }

    public List<VaccineCovid19Reaction> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<VaccineCovid19Reaction> list = new ArrayList<>();
        try (ResultSet rs = preparedStatement.executeQuery()) {
            while (rs.next()) {
                VaccineCovid19Reaction p = new VaccineCovid19Reaction();
                p.setObjectId(rs.getString("t_health_vaccine_covid19_reaction_id"));
                p.t_health_vaccine_covid19_id = rs.getString("t_health_vaccine_covid19_id");
                p.t_patient_id = rs.getString("t_patient_id");
                p.t_visit_id = rs.getString("t_visit_id");
                p.reaction_no = rs.getInt("reaction_no");
                p.reaction_stage = rs.getInt("reaction_stage");
                p.f_vaccine_reaction_stage_id = String.valueOf(rs.getInt("f_vaccine_reaction_stage_id"));
                p.reaction_date = rs.getDate("reaction_date");
                p.adverse_reaction = rs.getInt("adverse_reaction");
                p.adverse_description = rs.getString("adverse_description");
                p.f_vaccine_reaction_type_id = String.valueOf(rs.getInt("f_vaccine_reaction_type_id"));
                p.f_vaccine_reaction_symptom_id = String.valueOf(rs.getInt("f_vaccine_reaction_symptom_id"));
                p.symptom_description = rs.getString("symptom_description");
                p.active = rs.getString("active");
                p.record_date_time = rs.getTimestamp("record_date_time");
                p.user_record_id = rs.getString("user_record_id");
                p.update_date_time = rs.getTimestamp("update_date_time");
                p.user_update_id = rs.getString("user_update_id");
                p.cancel_date_time = rs.getTimestamp("cancel_date_time");
                p.user_cancel_id = rs.getString("user_cancel_id");
                list.add(p);
            }
            return list;
        }
    }
}

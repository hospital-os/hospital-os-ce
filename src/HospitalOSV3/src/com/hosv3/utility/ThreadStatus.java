/*
 * TestThread.java
 *
 * Created on 17 �Զع�¹ 2548, 9:06 �.
 */
package com.hosv3.utility;

import java.awt.Color;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author henbe
 */
public class ThreadStatus extends Thread {

    private static final Logger LOG = Logger.getLogger(ThreadStatus.class.getName());
    private final JLabel jLabelStatus;
    private final JFrame jf;

    /**
     * Creates a new instance of TestThread
     */
    public ThreadStatus(JLabel us) {
        this(null, us);
    }

    public ThreadStatus(JFrame jf, JLabel us) {
        jLabelStatus = us;
        this.jf = jf;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(Constant.TIME_UPDATE_STATUS);
            jLabelStatus.setBackground(Color.GRAY);
            Thread.sleep(5000);
            jLabelStatus.setText("");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.objdb.objdbclass;

import com.hospital_os.usecase.connection.ConnectionInf;
import com.pcu.object.MapEpidemLabType;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tanakrit
 */
public class MapEpidemLabTypeDB {

    private final ConnectionInf theConnectionInf;
    private final String tableId = "462";

    public MapEpidemLabTypeDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public int insert(MapEpidemLabType obj) throws Exception {
        String sql = "INSERT INTO b_map_epidem_lab_type( "
                + "               b_map_epidem_lab_type_id, b_item_id, f_epidem_covid_lab_confirm_type_id) "
                + "       VALUES (?, ?, ?)";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, obj.getGenID(tableId));
            ePQuery.setString(index++, obj.b_item_id);
            ePQuery.setInt(index++, obj.f_epidem_covid_lab_confirm_type_id);
            return ePQuery.executeUpdate();
        }
    }

    public int update(MapEpidemLabType obj) throws Exception {
        String sql = "UPDATE b_map_epidem_lab_type\n"
                + "      SET f_epidem_covid_lab_confirm_type_id = ?\n"
                + "    WHERE b_map_epidem_lab_type_id = ?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setInt(index++, obj.f_epidem_covid_lab_confirm_type_id);
            ePQuery.setString(index++, obj.getObjectId());
            return ePQuery.executeUpdate();
        }
    }

    public int deleteByItemId(String id) throws Exception {
        String sql = "delete from b_map_epidem_lab_type where b_item_id = ?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, id);
            return ePQuery.executeUpdate();
        }
    }

    public MapEpidemLabType selectById(String id) throws Exception {
        String sql = "select * from b_map_epidem_lab_type where b_map_epidem_lab_type_id = ?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, id);
            List<MapEpidemLabType> eQuery = executeQuery(ePQuery);
            return (MapEpidemLabType) (eQuery.isEmpty() ? null : eQuery.get(0));
        }
    }

    public MapEpidemLabType selectByItemId(String id) throws Exception {
        String sql = "select * from b_map_epidem_lab_type where b_item_id = ?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, id);
            List<MapEpidemLabType> eQuery = executeQuery(ePQuery);
            return (MapEpidemLabType) (eQuery.isEmpty() ? null : eQuery.get(0));
        }
    }

    public List<Object[]> listByKeyword(String keyword, String type) throws Exception {
        String sql = "select b_item.b_item_id as item_id\n"
                + "    ,b_item.item_common_name as item_name\n"
                + "    ,f_epidem_covid_lab_confirm_type.f_epidem_covid_lab_confirm_type_id as type_id\n"
                + "    ,f_epidem_covid_lab_confirm_type.epidem_covid_lab_confirm_type_name as type_name\n"
                + "from b_item\n"
                + "    inner join b_item_subgroup on b_item.b_item_subgroup_id = b_item_subgroup.b_item_subgroup_id\n"
                + "    left join b_map_epidem_lab_type on b_item.b_item_id = b_map_epidem_lab_type.b_item_id\n"
                + "    left join f_epidem_covid_lab_confirm_type on b_map_epidem_lab_type.f_epidem_covid_lab_confirm_type_id\n"
                + "                                                = f_epidem_covid_lab_confirm_type.f_epidem_covid_lab_confirm_type_id\n"
                + "where b_item_subgroup.f_item_group_id = '2' -- ੾����¡�����\n"
                + "    and b_item.item_common_name ilike ? -- ���Ҵ��ª�����¡��\n"
                + "    and b_item.item_active = '1'\n"
                + "    and case when ? = 1 then b_map_epidem_lab_type.b_map_epidem_lab_type_id is not null  -- 1 = �Ѻ�������\n"
                + "             when ? = 2 then b_map_epidem_lab_type.b_map_epidem_lab_type_id is null -- 2 = �ѧ���Ѻ���\n"
                + "             else true end -- �ʴ�������\n"
                + "order by b_item.item_common_name";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, keyword == null || keyword.isEmpty() ? "%" : ("%" + keyword + "%"));
            ePQuery.setString(index++, type);
            ePQuery.setString(index++, type);
            return theConnectionInf.eComplexQuery(ePQuery.toString());
        }
    }

    public List<MapEpidemLabType> executeQuery(PreparedStatement ePQuery) throws Exception {
        List<MapEpidemLabType> list = new ArrayList<>();
        try (ResultSet rs = ePQuery.executeQuery()) {
            while (rs.next()) {
                MapEpidemLabType p = new MapEpidemLabType();
                p.setObjectId(rs.getString("b_map_epidem_lab_type_id"));
                p.b_item_id = rs.getString("b_item_id");
                p.f_epidem_covid_lab_confirm_type_id = rs.getInt("f_epidem_covid_lab_confirm_type_id");
                list.add(p);
            }
        }
        return list;
    }

}

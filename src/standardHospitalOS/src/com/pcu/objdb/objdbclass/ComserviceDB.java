/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.objdb.objdbclass;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.pcu.object.Comservice;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */@SuppressWarnings("ClassWithoutLogger")
public class ComserviceDB {
    private final ConnectionInf connectionInf;

    public ComserviceDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public List<Comservice> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_comservice";
            preparedStatement = connectionInf.ePQuery(sql);
            
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
    
    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (Comservice obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }

    public List<Comservice> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<Comservice> list = new ArrayList<Comservice>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Comservice obj = new Comservice();
                obj.setObjectId(rs.getString("f_comservice_id"));
                obj.code = rs.getString("code");
                obj.description = rs.getString("description");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<Comservice> select(String keyword) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_comservice where upper(code) like upper(?) or upper(description) like upper(?)";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, "%" + keyword + "%");
            preparedStatement.setString(2, "%" + keyword + "%");
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
}

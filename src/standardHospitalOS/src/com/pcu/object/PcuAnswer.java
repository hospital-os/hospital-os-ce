/*
 * PcuAnswer.java
 *
 * Created on 20 �Զع�¹ 2548, 14:50 �.
 */
package com.pcu.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author amp
 */
@SuppressWarnings("ClassWithoutLogger")
public class PcuAnswer extends Persistent {

    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of PcuAnswer
     */
    public PcuAnswer() {
    }

    /**
     * One
     *
     * @return 1
     */
    public static String One() {
        return "1";
    }

    /**
     * Zero
     *
     * @return 0
     */
    public static String Zero() {
        return "0";
    }
    
    /**
     * 
     * @return 2
     */
    public static String Two() {
        return "2";
    }
    /**
     * 
     * @return 9
     */
    public static String Nine() {
        return "9";
    }
}

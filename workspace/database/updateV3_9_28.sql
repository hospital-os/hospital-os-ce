CREATE TABLE b_item_drug_dosage_form (
    b_item_drug_dosage_form_id character varying(255) NOT NULL,
    form_name character varying(255) NOT NULL,
    active character varying(1) NOT NULL default '1',
    user_record_id character varying(255) NOT NULL,
    record_date_time character varying(19) NOT NULL,
    user_update_id character varying(255) NOT NULL,
    update_date_time character varying(19) NOT NULL,
    CONSTRAINT b_item_drug_dosage_form_pkey PRIMARY KEY (b_item_drug_dosage_form_id),
    CONSTRAINT b_item_drug_dosage_form_unique1 UNIQUE (form_name)
);

CREATE TABLE b_item_drug_strength_unit (
    b_item_drug_strength_unit_id character varying(255) NOT NULL,
    unit_name character varying(255) NOT NULL,
    active character varying(1) NOT NULL default '1',
    user_record_id character varying(255) NOT NULL,
    record_date_time character varying(19) NOT NULL,
    user_update_id character varying(255) NOT NULL,
    update_date_time character varying(19) NOT NULL,
    CONSTRAINT b_item_drug_strength_unit_pkey PRIMARY KEY (b_item_drug_strength_unit_id),
    CONSTRAINT b_item_drug_strength_unit_unique1 UNIQUE (unit_name)
);

CREATE TABLE b_item_supply (
    b_item_supply_id character varying(255) NOT NULL,
    b_item_id character varying(255) NOT NULL,
    item_supply_printable character varying(1) NOT NULL default '0',
    user_record_id character varying(255) NOT NULL,
    record_date_time character varying(19) NOT NULL,
    user_update_id character varying(255) NOT NULL,
    update_date_time character varying(19) NOT NULL,
    CONSTRAINT b_item_supply_pkey PRIMARY KEY (b_item_supply_id),
    CONSTRAINT b_item_supply_unique1 UNIQUE (b_item_id)
);
CREATE INDEX b_item_supply_b_item_id_idx
   ON b_item_supply (b_item_id ASC NULLS LAST);

ALTER TABLE b_item_drug ADD COLUMN b_item_drug_dosage_form_id character varying(255) default '';
ALTER TABLE b_item_drug ADD COLUMN item_drug_strength NUMERIC(10, 2);
ALTER TABLE b_item_drug ADD COLUMN b_item_drug_strength_unit_id character varying(255) default '';

insert into f_gui_action (f_gui_action_id, gui_action_name) VALUES ('5307', 'รูปแบบของยา (Dosage Form)');
insert into f_gui_action (f_gui_action_id, gui_action_name) VALUES ('5308', 'หน่วยขนาดความแรง');

ALTER TABLE b_map_due ADD COLUMN contract_plans character varying[];

ALTER TABLE b_map_ned ADD COLUMN contract_plans character varying[];
ALTER TABLE b_map_ned ADD COLUMN user_record_id character varying(255) NOT NULL default '';
ALTER TABLE b_map_ned ADD COLUMN record_date_time character varying(19) NOT NULL default '';
ALTER TABLE b_map_ned ADD COLUMN user_update_id character varying(255) NOT NULL default '';
ALTER TABLE b_map_ned ADD COLUMN update_date_time character varying(19) NOT NULL default '';

ALTER TABLE b_employee ADD COLUMN employee_warning_icd10 character varying(1) NOT NULL default '0';
CREATE INDEX employee_warning_icd10_idx ON b_employee (employee_warning_icd10);

ALTER TABLE b_item ADD COLUMN doctor_warning_icd10 character varying(1) NOT NULL default '0';
CREATE INDEX doctor_warning_icd10_idx ON b_item (doctor_warning_icd10);

CREATE TABLE f_transportation_type (
    f_transportation_type_id character varying(1) NOT NULL,
    description character varying(255) NOT NULL,
    CONSTRAINT f_transportation_type_pkey PRIMARY KEY (f_transportation_type_id)
);

insert into f_transportation_type values ('1', 'walk');
insert into f_transportation_type values ('2', 'wheelchair');
insert into f_transportation_type values ('3', 'on bed');
insert into f_transportation_type values ('4', 'other');

CREATE TABLE f_trama_status (
    f_trama_status_id character varying(1) NOT NULL,
    description character varying(255) NOT NULL,
    CONSTRAINT f_trama_status_pkey PRIMARY KEY (f_trama_status_id)
);

insert into f_trama_status values ('1', 'Trama');
insert into f_trama_status values ('2', 'Non Trama');

insert into f_emergency_status values ('4', 'Resuscitation');
insert into f_emergency_status values ('5', 'Semi – Urgency');

ALTER TABLE t_visit ADD COLUMN f_trama_status_id character varying(1) NOT NULL default '2';
ALTER TABLE t_visit ADD COLUMN f_transportation_type_id character varying(1) NOT NULL default '1';
ALTER TABLE t_visit ADD COLUMN other_transportation text;

alter table t_visit_primary_symptom add column illness_date_time character varying(19) NOT NULL default '';

update t_visit_primary_symptom set illness_date_time = record_date_time;

INSERT INTO s_version VALUES ('9701000000061', '61', 'Hospital OS, Community Edition', '3.9.28', '3.19.251012', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_28.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.28');
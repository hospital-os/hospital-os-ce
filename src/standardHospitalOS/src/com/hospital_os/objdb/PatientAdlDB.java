/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.PatientAdl;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sompr
 */
public class PatientAdlDB {

    protected final ConnectionInf theConnectionInf;
    private final String idtable = "6551";

    public PatientAdlDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public int insert(PatientAdl o) throws Exception {
        String sql = "INSERT INTO t_patient_adl ( \n"
                + "               t_patient_adl_id, t_patient_id, t_visit_id, screen_date, details, \n"
                + "               result_score, f_adl_result_id, active, user_record_id, user_update_id) \n"
                + "        VALUES(?, ?, ?, ?, ?::JSONB \n"
                + "              ,?, ?, ?, ?, ?)";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, o.getGenID(idtable));
            ePQuery.setString(index++, o.t_patient_id);
            ePQuery.setString(index++, o.t_visit_id);
            ePQuery.setDate(index++, o.screen_date != null ? new java.sql.Date(o.screen_date.getTime()) : null);
            ePQuery.setString(index++, o.details);
            ePQuery.setInt(index++, o.result_score);
            ePQuery.setString(index++, o.f_adl_result_id);
            ePQuery.setString(index++, o.active);
            ePQuery.setString(index++, o.user_record_id);
            ePQuery.setString(index++, o.user_update_id);
            return ePQuery.executeUpdate();
        }
    }

    public int update(PatientAdl o) throws Exception {
        String sql = "UPDATE t_patient_adl \n"
                + "      SET t_patient_id=?, t_visit_id=?, screen_date=?, details=?::JSONB, \n"
                + "          result_score=?, f_adl_result_id=?, update_date_time=CURRENT_TIMESTAMP, user_update_id=? \n"
                + "WHERE t_patient_adl_id=?";

        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, o.t_patient_id);
            ePQuery.setString(index++, o.t_visit_id);
            ePQuery.setDate(index++, o.screen_date != null ? new java.sql.Date(o.screen_date.getTime()) : null);
            ePQuery.setString(index++, o.details);
            ePQuery.setInt(index++, o.result_score);
            ePQuery.setString(index++, o.f_adl_result_id);
            ePQuery.setString(index++, o.user_update_id);
            ePQuery.setString(index++, o.getObjectId());
            return ePQuery.executeUpdate();
        }
    }

    public int delete(PatientAdl o) throws Exception {
        String sql = "UPDATE t_patient_adl "
                + "      SET user_update_id=?, update_date_time=CURRENT_TIMESTAMP, active =?, user_cancel_id=?, cancel_date_time=CURRENT_TIMESTAMP"
                + "    WHERE t_patient_adl_id =?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, o.user_update_id);
            ePQuery.setString(index++, "0");
            ePQuery.setString(index++, o.user_cancel_id);
            ePQuery.setString(index++, o.getObjectId());
            return ePQuery.executeUpdate();
        }
    }

    public PatientAdl selectByPid(String pid) throws Exception {
        String sql = "SELECT * FROM t_patient_adl \n"
                + "WHERE active = '1' \n"
                + "AND t_patient_id = ? \n"
                + "ORDER BY t_patient_adl.screen_date desc \n"
                + ",t_patient_adl.record_date_time desc";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, pid);
            List<PatientAdl> list = executeQuery(ePQuery);
            return list.isEmpty() ? null : list.get(0);
        }
    }

    public List<Object[]> listByPid(String pid) throws Exception {
        String sql = "SELECT t_patient_adl.t_patient_adl_id \n"
                + "	,t_patient_adl.screen_date \n"
                + "	,t_visit.visit_vn \n"
                + "	,t_patient_adl.result_score \n"
                + "	,f_adl_result.f_adl_result_id\n"
                + "	,f_adl_result.description \n"
                + "FROM t_patient_adl \n"
                + "LEFT JOIN t_visit ON t_patient_adl.t_visit_id = t_visit.t_visit_id \n"
                + "LEFT JOIN f_adl_result ON t_patient_adl.f_adl_result_id = f_adl_result.f_adl_result_id \n"
                + "WHERE t_patient_adl.active = '1'\n"
                + "AND t_patient_adl.t_patient_id = ? \n"
                + "ORDER BY t_patient_adl.screen_date desc\n"
                + "	,t_patient_adl.record_date_time desc";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, pid);
            return theConnectionInf.eComplexQuery(ePQuery.toString());
        }
    }

    public PatientAdl selectById(String id) throws Exception {
        String sql = "SELECT * FROM t_patient_adl \n"
                + "WHERE active = '1'\n"
                + "AND t_patient_adl_id = ? ";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, id);
            List<PatientAdl> list = executeQuery(ePQuery);
            return list.isEmpty() ? null : list.get(0);
        }
    }

    public List<PatientAdl> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<PatientAdl> list = new ArrayList<>();
        try (ResultSet rs = preparedStatement.executeQuery()) {
            while (rs.next()) {
                PatientAdl obj = new PatientAdl();
                obj.setObjectId(rs.getString("t_patient_adl_id"));
                obj.t_patient_id = rs.getString("t_patient_id");
                obj.t_visit_id = rs.getString("t_visit_id");
                obj.screen_date = rs.getDate("screen_date");
                obj.result_score = rs.getInt("result_score");
                obj.f_adl_result_id = rs.getString("f_adl_result_id");
                obj.active = rs.getString("active");
                obj.record_date_time = rs.getDate("record_date_time");
                obj.user_record_id = rs.getString("user_record_id");
                obj.update_date_time = rs.getDate("update_date_time");
                obj.user_update_id = rs.getString("user_update_id");
                obj.cancel_date_time = rs.getDate("cancel_date_time");
                obj.user_record_id = rs.getString("user_record_id");
                obj.details = rs.getString("details");
                list.add(obj);
            }
            return list;
        }
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.ServiceLimit;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class ServiceLimitDB {

    public ConnectionInf connectionInf;
    final public String tableId = "849";

    public ServiceLimitDB(ConnectionInf db) {
        connectionInf = db;
    }

    public int insert(ServiceLimit obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO b_service_limit(\n"
                    + "            b_service_limit_id, time_start, time_end, limit_appointment, limit_walkin)\n"
                    + "    VALUES (?, ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getGenID(tableId));
            preparedStatement.setString(2, obj.time_start);
            preparedStatement.setString(3, obj.time_end);
            preparedStatement.setInt(4, obj.limit_appointment);
            preparedStatement.setInt(5, obj.limit_walkin);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(ServiceLimit obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE b_service_limit\n"
                    + "   SET time_start=?, \n"
                    + "       time_end=?, limit_appointment=?, limit_walkin=?\n"
                    + " WHERE b_service_limit_id=?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.time_start);
            preparedStatement.setString(2, obj.time_end);
            preparedStatement.setInt(3, obj.limit_appointment);
            preparedStatement.setInt(4, obj.limit_walkin);
            preparedStatement.setString(5, obj.getObjectId());
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(ServiceLimit obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM b_service_limit\n");
            sql.append(" WHERE b_service_limit_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public ServiceLimit selectById(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_service_limit where b_service_limit_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<ServiceLimit> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<ServiceLimit> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_service_limit order by  time_start::time asc, time_end::time asc";
            preparedStatement = connectionInf.ePQuery(sql);
            List<ServiceLimit> list = executeQuery(preparedStatement);
            return list;
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<ServiceLimit> listByIntervalTime(String startTime, String endTime) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_service_limit where (time_start::time, time_end::time) OVERLAPS (?::time, ?::time) order by  time_start::time asc, time_end::time asc";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, startTime);
            preparedStatement.setString(2, endTime);
            List<ServiceLimit> list = executeQuery(preparedStatement);
            return list;
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public ServiceLimit getByBetweenTime(String startTime, String endTime) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "(select * from b_service_limit where time_start<=? and  time_end>=? order by  time_start::time desc, time_end::time asc limit 1)\n"
                    + "union\n"
                    + "(select * from b_service_limit where time_start>=? and  time_end<=? order by  time_start::time asc, time_end::time desc limit 1)";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, startTime);
            preparedStatement.setString(2, endTime);
            preparedStatement.setString(3, startTime);
            preparedStatement.setString(4, endTime);
            List<ServiceLimit> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<ServiceLimit> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<ServiceLimit> list = new ArrayList<ServiceLimit>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                ServiceLimit obj = new ServiceLimit();
                obj.setObjectId(rs.getString("b_service_limit_id"));
                obj.time_start = rs.getString("time_start");
                obj.time_end = rs.getString("time_end");
                obj.limit_appointment = rs.getInt("limit_appointment");
                obj.limit_walkin = rs.getInt("limit_walkin");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }
}

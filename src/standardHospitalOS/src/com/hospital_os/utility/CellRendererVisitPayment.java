/*
 * CellRendererVisitPayment.java
 *
 * Created on 4 ����¹ 2549, 10:59 �.
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
package com.hospital_os.utility;

import java.awt.Color;
import java.awt.Component;
import javax.swing.*;
import javax.swing.table.TableCellRenderer;

/**
 * ��㹡���ʴ� tooltiptext ���Ѻ cell �ͧ �Է�ԡ���ѡ��
 *
 * @author tong(Padungrat)
 */
@SuppressWarnings("ClassWithoutLogger")
public class CellRendererVisitPayment extends JLabel implements TableCellRenderer {

    private static final long serialVersionUID = 1L;
    boolean isBordered = true;
    Color color = null;
    ComboFix theComboFix = null;

    /**
     * Creates a new instance of CellRendererVisitPayment
     */
    public CellRendererVisitPayment(boolean isBordered) {
        this.isBordered = isBordered;
        this.color = new Color(255, 255, 255);
        setOpaque(true);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object combofix, boolean isSelected, boolean hasFocus, int row, int column) {
        this.setText("");
        this.setToolTipText("");
        if (combofix != null) {
            if (combofix instanceof ComboFix) {
                theComboFix = (ComboFix) combofix;
                this.setText(theComboFix.code);
                this.setToolTipText("<html>" + theComboFix.name + "</html>");
            }
        }
        if (isSelected) {
            this.setBackground(table.getSelectionBackground());
            this.setForeground(table.getSelectionForeground());
        } else {
            this.setBackground(table.getBackground());
            this.setForeground(table.getForeground());
        }
        return this;
    }
}

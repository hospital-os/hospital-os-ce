package com.hosv3.usecase.transaction;

import com.hospital_os.object.DxTemplate;

/**
 *
 * @author Somrpasong Damyos
 */
public interface ManageReDxResp {

    public void notifyAddReDx(DxTemplate dx);

    public void notifySaveReDx();
}

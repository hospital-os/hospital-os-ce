/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author LionHeart
 */
public class HWLavel extends Persistent implements CommonInf {

    public String description = "";

    public String getCode() {
        return getObjectId();
    }

    public String getName() {
        return description;
    }

    public String toString() {
        return description;
    }
}

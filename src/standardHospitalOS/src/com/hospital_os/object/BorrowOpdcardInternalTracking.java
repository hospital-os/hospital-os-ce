/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;
import java.util.Date;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class BorrowOpdcardInternalTracking extends Persistent {

    public String t_borrow_opdcard_internal_id = "";
    public String b_service_point_sender_id = "";
    public String sender_id = "";
    public Date send_datetime = null;
    public String b_service_point_receiver_id = "";
}

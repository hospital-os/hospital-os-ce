package com.hosv3.objdb;

import com.hospital_os.objdb.ReceiptSubgroupDB;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.Gutil;

public class ReceiptSubgroup2DB extends ReceiptSubgroupDB {

    public ReceiptSubgroup2DB(ConnectionInf db) {
        super(db);
    }

    public int updateAbyR(String active, String rid) throws Exception {
        String sql = "update " + dbObj.table + " set ";
        String field = dbObj.active + "='" + active
                + "' where " + dbObj.receipt_id + "='" + rid + "'";
        sql = Gutil.convertSQLToMySQL(sql + field, theConnectionInf.gettypeDatabase());
        return theConnectionInf.eUpdate(sql);
    }
}

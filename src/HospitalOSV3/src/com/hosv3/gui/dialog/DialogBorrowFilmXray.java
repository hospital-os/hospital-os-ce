/*
 * DialogBorrowFilmXray.java
 *
 * Created on 7 �ѹ�Ҥ� 2546, 14:14 �.
 * Modified on 21 ����¹ 2547, 13:00 �.
 */
package com.hosv3.gui.dialog;

import com.hospital_os.object.*;
import com.hospital_os.object.specialQuery.*;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hospital_os.utility.CelRenderer;
import com.hospital_os.utility.CellRendererHos;
import com.hospital_os.utility.Gutil;
import com.hospital_os.utility.TaBleModel;
import com.hosv3.control.*;
import com.hosv3.control.lookup.*;
import com.hosv3.object.*;
import com.hosv3.utility.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.*;
import javax.swing.*;

/**
 *
 * @author sumo
 * @modifier sumo
 */
@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public class DialogBorrowFilmXray extends JFrame implements UpdateStatus {

    private static final long serialVersionUID = 1L;
    HosControl theHC;
    HosObject theHO;
    public boolean actionCommand = false;
    JFrame aMain;
    LookupControl theLookupControl;
    PatientControl thePatientControl;
    VisitControl theVisitControl;
    SetupControl theSetupControl;
    SystemControl theSystemControl;
    Patient thePatient;
    CelRenderer cellRenderer = new CelRenderer();
    BorrowFilmXray theBorrowFilmXray = new BorrowFilmXray();
    /**
     * vector �ͧ ��¡���������� Xray
     */
    Vector vBorrow;
    private String[] collistHn = {"Hn", "����", "�ѹ���", "������", "�׹"};
    private String[] collistXn = {"Xn", "����", "�ѹ���", "������", "�׹"};
    CellRendererHos hnRender = new CellRendererHos(CellRendererHos.HN);

    /**
     * flag true ��� ��ͧ check ����ͧ���� false ��� ����ͧ check ����ͧ����
     */
//    private boolean flag;
    /**
     * Creates new form DialogAppointment
     */
    @SuppressWarnings("LeakingThisInConstructor")
    public DialogBorrowFilmXray(HosControl hc, UpdateStatus us) {
        aMain = us.getJFrame();
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/com/hosv3/gui/images/PrintXNIndex.gif")));
        theLookupControl = hc.theLookupControl;
        thePatientControl = hc.thePatientControl;
        theSetupControl = hc.theSetupControl;
        theSystemControl = hc.theSystemControl;
        hnRender = new CellRendererHos(CellRendererHos.HN, theLookupControl.getSequenceDataHN().pattern);
        theHO = hc.theHO;
        theHC = hc;
        thePatient = theHO.thePatient;
        initComponents();
        updateOGComponent();
        setDefault();
        setEnableAll(true);
        setLanguage("");
        jTextFieldCancel.setVisible(false);
        if (theHO.thePatient != null) {
            thePatient = theHC.thePatientControl.readPatientByHnToBorrowFilm(theHO.thePatient.hn, this);
            if (thePatient != null) {
                updateOGBorrowFilmXray(null, thePatient);
            }
        }
        jTextFieldBorrowToOther.setControl(new ServicePointLookup(theHC.theLookupControl), this);
//        jTextFieldBorrowToOther.setEControl(new ServicePointLookup(theHC.theSetupControl));
        theHC.theHS.theBalloonSubject.attachBalloon(jTextFieldBorrowToOther);
        jButtonPreviewListBorrowFilmXray.setVisible(false);
        jButtonPrintListBorrowFilmXray.setVisible(false);
        jTextFieldHosName.setEditable(false);
        this.dateComboBoxDateFrom.setEnabled(false);
        this.dateComboBoxDateTo.setEnabled(false);
    }

    //////////////////////////////////////////////////////////////////////////
    /**
     * dialog �����㹡���觢�ͤ������͹�����
     */
    @Override
    public void setStatus(String str, int status) {

        ThreadStatus theTT = new ThreadStatus(this, this.jLabelStatus);
        theTT.start();
        str = Constant.getTextBundle(str);
        jLabelStatus.setText(" " + str);
        if (status == UpdateStatus.WARNING) {
            jLabelStatus.setBackground(Color.YELLOW);
        }
        if (status == UpdateStatus.COMPLETE) {
            jLabelStatus.setBackground(Color.GREEN);
        }
        if (status == UpdateStatus.ERROR) {
            jLabelStatus.setBackground(Color.RED);
        }
    }

    //////////////////////////////////////////////////////////////////////////
    /**
     * dialog �����㹡���������ӡ���չ�ѹ��觵�ҧ
     */
    @Override
    public boolean confirmBox(String str, int status) {
        int i = JOptionPane.showConfirmDialog(this, str, Constant.getTextBundle("��͹"), JOptionPane.YES_NO_OPTION);
        return (i == JOptionPane.YES_OPTION);
    }

    ////////////////////////////////////////////////////////////////////////////
    /**
     * init component �ӡ��૵������Ѻ component ��ҧ� neung
     */
    private void updateOGComponent() {
        dateComboBoxDateBorrowFilm.setEditable(true);
        dateComboBoxDateFrom.setEditable(true);
        dateComboBoxDateTo.setEditable(true);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroup1 = new javax.swing.ButtonGroup();
        jLabelStatus = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        jLabelCauseBorrow = new javax.swing.JLabel();
        jLabelBorrowTo = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextAreaCauseBorrow = new javax.swing.JTextArea();
        jPanel4 = new javax.swing.JPanel();
        jButtonHos = new javax.swing.JButton();
        jTextFieldHosName = new javax.swing.JTextField();
        jTextFieldHosCode = new javax.swing.JTextField();
        jLabelPermissibly_Borrow = new javax.swing.JLabel();
        jTextFieldPermissibly = new javax.swing.JTextField();
        jPanel10 = new javax.swing.JPanel();
        jTextFieldHN = new com.hospital_os.utility.HNTextField();
        jLabelXn = new javax.swing.JLabel();
        jTextFieldXN = new javax.swing.JTextField();
        jLabelHn = new javax.swing.JLabel();
        jTextFieldCancel = new javax.swing.JTextField();
        jLabelBorrowToOther = new javax.swing.JLabel();
        jTextFieldBorrowToOther = new com.hosv3.gui.component.BalloonTextField();
        jPanel5 = new javax.swing.JPanel();
        jLabelAmountDate = new javax.swing.JLabel();
        jTextFieldAmountDate = new com.hospital_os.utility.DoubleTextField();
        dateComboBoxDateBorrowFilm = new com.hospital_os.utility.DateComboBox();
        dateComboBoxDateReturnFilm = new com.hospital_os.utility.DateComboBox();
        jCheckBoxReturnFilmDate = new javax.swing.JCheckBox();
        jLabelBorrowFilmDate = new javax.swing.JLabel();
        jLabelAmountDate1 = new javax.swing.JLabel();
        jLabelFLName = new javax.swing.JLabel();
        jTextFieldPatientName = new javax.swing.JTextField();
        jLabelBorrowName = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jTextFieldBXLName = new javax.swing.JTextField();
        jTextFieldBXFName = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        jButtonAdd = new javax.swing.JButton();
        jButtonSave = new javax.swing.JButton();
        jButtonDel = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new com.hosv3.gui.component.HJTableSort();
        jPanelSearch = new javax.swing.JPanel();
        jCheckBoxShowCancel = new javax.swing.JCheckBox();
        jCheckBoxSCurrPatient = new javax.swing.JCheckBox();
        jPanel11 = new javax.swing.JPanel();
        jLabelDateEnd = new javax.swing.JLabel();
        dateComboBoxDateFrom = new com.hospital_os.utility.DateComboBox();
        dateComboBoxDateTo = new com.hospital_os.utility.DateComboBox();
        jCheckBoxDateSearch = new javax.swing.JCheckBox();
        jButtonPreviewListBorrowFilmXray = new javax.swing.JButton();
        jButtonPrintListBorrowFilmXray = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(new java.awt.GridBagLayout());

        jLabelStatus.setFont(jLabelStatus.getFont());
        jLabelStatus.setMaximumSize(new java.awt.Dimension(4, 24));
        jLabelStatus.setMinimumSize(new java.awt.Dimension(4, 20));
        jLabelStatus.setOpaque(true);
        jLabelStatus.setPreferredSize(new java.awt.Dimension(4, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        getContentPane().add(jLabelStatus, gridBagConstraints);

        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder("Dialog_Borrow_Film_Xray_Detail"));
        jPanel7.setLayout(new java.awt.GridBagLayout());

        jLabelCauseBorrow.setFont(jLabelCauseBorrow.getFont());
        jLabelCauseBorrow.setText("CauseBorrow");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 12, 0, 0);
        jPanel7.add(jLabelCauseBorrow, gridBagConstraints);

        jLabelBorrowTo.setFont(jLabelBorrowTo.getFont());
        jLabelBorrowTo.setText("BorrowTo");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 12, 0, 0);
        jPanel7.add(jLabelBorrowTo, gridBagConstraints);

        jScrollPane2.setMaximumSize(new java.awt.Dimension(250, 150));
        jScrollPane2.setMinimumSize(new java.awt.Dimension(50, 50));
        jScrollPane2.setPreferredSize(new java.awt.Dimension(50, 50));

        jTextAreaCauseBorrow.setFont(jTextAreaCauseBorrow.getFont());
        jTextAreaCauseBorrow.setLineWrap(true);
        jTextAreaCauseBorrow.setWrapStyleWord(true);
        jScrollPane2.setViewportView(jTextAreaCauseBorrow);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 11, 0, 12);
        jPanel7.add(jScrollPane2, gridBagConstraints);

        jPanel4.setLayout(new java.awt.GridBagLayout());

        jButtonHos.setFont(jButtonHos.getFont());
        jButtonHos.setText("...");
        jButtonHos.setMaximumSize(new java.awt.Dimension(24, 24));
        jButtonHos.setMinimumSize(new java.awt.Dimension(24, 24));
        jButtonHos.setPreferredSize(new java.awt.Dimension(24, 24));
        jButtonHos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonHosActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel4.add(jButtonHos, gridBagConstraints);

        jTextFieldHosName.setFont(jTextFieldHosName.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel4.add(jTextFieldHosName, gridBagConstraints);

        jTextFieldHosCode.setFont(jTextFieldHosCode.getFont());
        jTextFieldHosCode.setMinimumSize(new java.awt.Dimension(41, 21));
        jTextFieldHosCode.setPreferredSize(new java.awt.Dimension(41, 21));
        jTextFieldHosCode.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextFieldHosCodeFocusLost(evt);
            }
        });
        jTextFieldHosCode.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldHosCodeKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel4.add(jTextFieldHosCode, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 11, 0, 12);
        jPanel7.add(jPanel4, gridBagConstraints);

        jLabelPermissibly_Borrow.setFont(jLabelPermissibly_Borrow.getFont());
        jLabelPermissibly_Borrow.setText("Permissibly_Borrow");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 12, 0, 0);
        jPanel7.add(jLabelPermissibly_Borrow, gridBagConstraints);

        jTextFieldPermissibly.setFont(jTextFieldPermissibly.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 11, 0, 12);
        jPanel7.add(jTextFieldPermissibly, gridBagConstraints);

        jPanel10.setLayout(new java.awt.GridBagLayout());

        jTextFieldHN.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldHN.setFont(jTextFieldHN.getFont());
        jTextFieldHN.setMinimumSize(new java.awt.Dimension(65, 21));
        jTextFieldHN.setPreferredSize(new java.awt.Dimension(65, 21));
        jTextFieldHN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldHNActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 0.3;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel10.add(jTextFieldHN, gridBagConstraints);

        jLabelXn.setFont(jLabelXn.getFont());
        jLabelXn.setText("XN");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel10.add(jLabelXn, gridBagConstraints);

        jTextFieldXN.setFont(jTextFieldXN.getFont());
        jTextFieldXN.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldXN.setMinimumSize(new java.awt.Dimension(65, 21));
        jTextFieldXN.setPreferredSize(new java.awt.Dimension(65, 21));
        jTextFieldXN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldXNActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 0.3;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel10.add(jTextFieldXN, gridBagConstraints);

        jLabelHn.setFont(jLabelHn.getFont());
        jLabelHn.setText("HN");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        jPanel10.add(jLabelHn, gridBagConstraints);

        jTextFieldCancel.setEditable(false);
        jTextFieldCancel.setBackground(new java.awt.Color(255, 0, 0));
        jTextFieldCancel.setFont(jTextFieldCancel.getFont());
        jTextFieldCancel.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextFieldCancel.setText("¡��ԡ");
        jTextFieldCancel.setToolTipText("");
        jTextFieldCancel.setBorder(null);
        jTextFieldCancel.setMinimumSize(new java.awt.Dimension(70, 21));
        jTextFieldCancel.setPreferredSize(new java.awt.Dimension(70, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel10.add(jTextFieldCancel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 12, 0, 12);
        jPanel7.add(jPanel10, gridBagConstraints);

        jLabelBorrowToOther.setFont(jLabelBorrowToOther.getFont());
        jLabelBorrowToOther.setText("BorrowToOther");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 12, 0, 0);
        jPanel7.add(jLabelBorrowToOther, gridBagConstraints);

        jTextFieldBorrowToOther.setFont(jTextFieldBorrowToOther.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 11, 0, 12);
        jPanel7.add(jTextFieldBorrowToOther, gridBagConstraints);

        jPanel5.setLayout(new java.awt.GridBagLayout());

        jLabelAmountDate.setFont(jLabelAmountDate.getFont());
        jLabelAmountDate.setText("�ѹ");
        jLabelAmountDate.setMaximumSize(new java.awt.Dimension(70, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jLabelAmountDate, gridBagConstraints);

        jTextFieldAmountDate.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldAmountDate.setFont(jTextFieldAmountDate.getFont());
        jTextFieldAmountDate.setMinimumSize(new java.awt.Dimension(30, 21));
        jTextFieldAmountDate.setPreferredSize(new java.awt.Dimension(30, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jTextFieldAmountDate, gridBagConstraints);

        dateComboBoxDateBorrowFilm.setFont(dateComboBoxDateBorrowFilm.getFont());
        dateComboBoxDateBorrowFilm.setMinimumSize(new java.awt.Dimension(107, 24));
        dateComboBoxDateBorrowFilm.setPreferredSize(new java.awt.Dimension(107, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(dateComboBoxDateBorrowFilm, gridBagConstraints);

        dateComboBoxDateReturnFilm.setFont(dateComboBoxDateReturnFilm.getFont());
        dateComboBoxDateReturnFilm.setMinimumSize(new java.awt.Dimension(107, 24));
        dateComboBoxDateReturnFilm.setPreferredSize(new java.awt.Dimension(107, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(dateComboBoxDateReturnFilm, gridBagConstraints);

        jCheckBoxReturnFilmDate.setFont(jCheckBoxReturnFilmDate.getFont());
        jCheckBoxReturnFilmDate.setText("�ѹ���׹");
        jCheckBoxReturnFilmDate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxReturnFilmDateActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jCheckBoxReturnFilmDate, gridBagConstraints);

        jLabelBorrowFilmDate.setFont(jLabelBorrowFilmDate.getFont());
        jLabelBorrowFilmDate.setText("�ѹ������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jLabelBorrowFilmDate, gridBagConstraints);

        jLabelAmountDate1.setFont(jLabelAmountDate1.getFont());
        jLabelAmountDate1.setText("�ӹǹ�ѹ���");
        jLabelAmountDate1.setMaximumSize(new java.awt.Dimension(70, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jLabelAmountDate1, gridBagConstraints);

        jLabelFLName.setFont(jLabelFLName.getFont());
        jLabelFLName.setText("����-ʡ�� ������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jLabelFLName, gridBagConstraints);

        jTextFieldPatientName.setEditable(false);
        jTextFieldPatientName.setFont(jTextFieldPatientName.getFont());
        jTextFieldPatientName.setBorder(null);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jTextFieldPatientName, gridBagConstraints);

        jLabelBorrowName.setFont(jLabelBorrowName.getFont());
        jLabelBorrowName.setText("����-ʡ�� ������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jLabelBorrowName, gridBagConstraints);

        jPanel8.setLayout(new java.awt.GridBagLayout());

        jTextFieldBXLName.setFont(jTextFieldBXLName.getFont());
        jTextFieldBXLName.setMaximumSize(new java.awt.Dimension(100, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel8.add(jTextFieldBXLName, gridBagConstraints);

        jTextFieldBXFName.setFont(jTextFieldBXFName.getFont());
        jTextFieldBXFName.setMaximumSize(new java.awt.Dimension(100, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        jPanel8.add(jTextFieldBXFName, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jPanel8, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 12, 0, 12);
        jPanel7.add(jPanel5, gridBagConstraints);

        jPanel1.setLayout(new java.awt.GridBagLayout());

        jButtonAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/plus16.gif"))); // NOI18N
        jButtonAdd.setMaximumSize(new java.awt.Dimension(24, 24));
        jButtonAdd.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonAdd.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAddActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel1.add(jButtonAdd, gridBagConstraints);

        jButtonSave.setFont(jButtonSave.getFont());
        jButtonSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/Save16.gif"))); // NOI18N
        jButtonSave.setText("�ѹ�֡");
        jButtonSave.setMaximumSize(new java.awt.Dimension(80, 26));
        jButtonSave.setMinimumSize(new java.awt.Dimension(80, 26));
        jButtonSave.setPreferredSize(new java.awt.Dimension(80, 26));
        jButtonSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSaveActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        jPanel1.add(jButtonSave, gridBagConstraints);

        jButtonDel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/minus16.gif"))); // NOI18N
        jButtonDel.setMaximumSize(new java.awt.Dimension(24, 24));
        jButtonDel.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonDel.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonDel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDelActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel1.add(jButtonDel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(17, 12, 11, 12);
        jPanel7.add(jPanel1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        getContentPane().add(jPanel7, gridBagConstraints);
        jPanel7.getAccessibleContext().setAccessibleParent(jPanel7);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Dialog_Borrow_Film_Xray_Search"));
        jPanel3.setRequestFocusEnabled(false);
        jPanel3.setLayout(new java.awt.GridBagLayout());

        jTable1.setFillsViewportHeight(true);
        jTable1.setFont(jTable1.getFont());
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTable1MouseReleased(evt);
            }
        });
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTable1KeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 11, 11, 11);
        jPanel3.add(jScrollPane1, gridBagConstraints);

        jPanelSearch.setLayout(new java.awt.GridBagLayout());

        jCheckBoxShowCancel.setFont(jCheckBoxShowCancel.getFont());
        jCheckBoxShowCancel.setText("��¡�÷��¡��ԡ");
        jCheckBoxShowCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxShowCancelActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 0, 0);
        jPanelSearch.add(jCheckBoxShowCancel, gridBagConstraints);

        jCheckBoxSCurrPatient.setFont(jCheckBoxSCurrPatient.getFont());
        jCheckBoxSCurrPatient.setText("CurrentPatient");
        jCheckBoxSCurrPatient.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxSCurrPatientActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 3, 0, 0);
        jPanelSearch.add(jCheckBoxSCurrPatient, gridBagConstraints);

        jPanel11.setLayout(new java.awt.GridBagLayout());

        jLabelDateEnd.setFont(jLabelDateEnd.getFont());
        jLabelDateEnd.setText("�֧");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel11.add(jLabelDateEnd, gridBagConstraints);

        dateComboBoxDateFrom.setFont(dateComboBoxDateFrom.getFont());
        dateComboBoxDateFrom.setMinimumSize(new java.awt.Dimension(107, 24));
        dateComboBoxDateFrom.setPreferredSize(new java.awt.Dimension(107, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        jPanel11.add(dateComboBoxDateFrom, gridBagConstraints);

        dateComboBoxDateTo.setFont(dateComboBoxDateTo.getFont());
        dateComboBoxDateTo.setMinimumSize(new java.awt.Dimension(107, 24));
        dateComboBoxDateTo.setPreferredSize(new java.awt.Dimension(107, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        jPanel11.add(dateComboBoxDateTo, gridBagConstraints);

        jCheckBoxDateSearch.setFont(jCheckBoxDateSearch.getFont());
        jCheckBoxDateSearch.setText("�ѹ���");
        jCheckBoxDateSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxDateSearchActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel11.add(jCheckBoxDateSearch, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 0, 3);
        jPanelSearch.add(jPanel11, gridBagConstraints);

        jButtonPreviewListBorrowFilmXray.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/preview24.gif"))); // NOI18N
        jButtonPreviewListBorrowFilmXray.setToolTipText("PreviewListBorrowFilmXray");
        jButtonPreviewListBorrowFilmXray.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonPreviewListBorrowFilmXray.setPreferredSize(new java.awt.Dimension(26, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 0);
        jPanelSearch.add(jButtonPreviewListBorrowFilmXray, gridBagConstraints);

        jButtonPrintListBorrowFilmXray.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/print24.gif"))); // NOI18N
        jButtonPrintListBorrowFilmXray.setToolTipText("PrintListBorrowFilmXray");
        jButtonPrintListBorrowFilmXray.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonPrintListBorrowFilmXray.setPreferredSize(new java.awt.Dimension(26, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 0, 0);
        jPanelSearch.add(jButtonPrintListBorrowFilmXray, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 11, 0, 11);
        jPanel3.add(jPanelSearch, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        getContentPane().add(jPanel3, gridBagConstraints);
        jPanel3.getAccessibleContext().setAccessibleParent(jPanel3);

        setSize(new java.awt.Dimension(838, 565));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jCheckBoxDateSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxDateSearchActionPerformed
        this.dateComboBoxDateFrom.setEnabled(jCheckBoxDateSearch.isSelected());
        this.dateComboBoxDateTo.setEnabled(jCheckBoxDateSearch.isSelected());
        searchBorrowFilmXray(false);
    }//GEN-LAST:event_jCheckBoxDateSearchActionPerformed

    private void jCheckBoxReturnFilmDateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxReturnFilmDateActionPerformed
        dateComboBoxDateReturnFilm.setEnabled(jCheckBoxReturnFilmDate.isSelected());
    }//GEN-LAST:event_jCheckBoxReturnFilmDateActionPerformed

    private void jCheckBoxSCurrPatientActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxSCurrPatientActionPerformed
        searchBorrowFilmXray(false);
        if (jCheckBoxSCurrPatient.isSelected() && jTextFieldXN.getText().isEmpty() && jTextFieldHN.getText().isEmpty()) {
            setStatus(Constant.getTextBundle("�������ö������") + " "
                    + Constant.getTextBundle("���ͧ�ҡ����բ����ż����»Ѩ�غѹ"), WARNING);
        }
    }//GEN-LAST:event_jCheckBoxSCurrPatientActionPerformed

    private void jTextFieldXNActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldXNActionPerformed
        thePatient = theHC.thePatientControl.readPatientByXnToBorrowFilm(jTextFieldXN.getText(), this);
        if (thePatient == null) {
            setDefault();
            return;
        }
        updateOGBorrowFilmXray(null, thePatient);
    }//GEN-LAST:event_jTextFieldXNActionPerformed

    private void jTextFieldHNActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldHNActionPerformed
        thePatient = theHC.thePatientControl.readPatientByHnToBorrowFilm(jTextFieldHN.getText(), this);
        if (thePatient == null) {
            setDefault();
            return;
        }
        updateOGBorrowFilmXray(null, thePatient);
    }//GEN-LAST:event_jTextFieldHNActionPerformed

    private void jCheckBoxShowCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxShowCancelActionPerformed
        searchBorrowFilmXray(false);
    }//GEN-LAST:event_jCheckBoxShowCancelActionPerformed

    private void jTextFieldHosCodeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldHosCodeKeyReleased
        if (jTextFieldHosCode.getText().length() != 5) {
            return;
        }
        jTextFieldHosCodeFocusLost(null);
    }//GEN-LAST:event_jTextFieldHosCodeKeyReleased

    private void jTextFieldHosCodeFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextFieldHosCodeFocusLost
        if (jTextFieldHosCode.getText().isEmpty()) {
            return;
        }

        Office office = theHC.theLookupControl.readHospitalByCode(jTextFieldHosCode.getText());
        if (office == null) {
            jTextFieldHosCode.setText("");
            jTextFieldHosName.setText("");
            setStatus(Constant.getTextBundle("��辺ʶҹ��Һ�ŷ��ç�Ѻ���ʷ���к�") + " "
                    + Constant.getTextBundle("��سҵ�Ǩ�ͺ�����ա����"), WARNING);
            if (theBorrowFilmXray != null) {
                theBorrowFilmXray.borrow_to = "";
            }
        } else {
            jTextFieldHosName.setText(office.getName());
            if (theBorrowFilmXray != null) {
                theBorrowFilmXray.borrow_to = office.getObjectId();
            }
        }
    }//GEN-LAST:event_jTextFieldHosCodeFocusLost

    private void jButtonHosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonHosActionPerformed
        Office office = new Office();
        office.setObjectId(theBorrowFilmXray.borrow_to);
        if (DialogOffice.showDialog(theHC, this, office)) {
            theBorrowFilmXray.borrow_to = office.getObjectId();
            jTextFieldHosName.setText(office.getName());
            jTextFieldHosCode.setText(office.getCode());
        }
    }//GEN-LAST:event_jButtonHosActionPerformed

    private void jTable1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_UP || evt.getKeyCode() == KeyEvent.VK_DOWN) {
            this.jTable1MouseReleased(null);
        }
    }//GEN-LAST:event_jTable1KeyReleased

    private void jTable1MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseReleased
        int row = jTable1.getSelectedRow();
        if (row == -1) {
            return;
        }
        SpecialQueryBorrowFilmXray sqbor = (SpecialQueryBorrowFilmXray) vBorrow.get(row);
        theBorrowFilmXray = thePatientControl.readBorrowFilmXrayByPK(sqbor.t_borrow_film_xray_id);
        updateOGBorrowFilmXray(theBorrowFilmXray, null);
    }//GEN-LAST:event_jTable1MouseReleased

    private void jButtonAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAddActionPerformed
        actionCommand = false;
        String date_time = theHO.date_time;
        theBorrowFilmXray = theHO.initBorrowFilmXray(date_time);
        setDefault();
        this.setStatus(Constant.getTextBundle("��سҡ�͡�����š���������� Xray"), UpdateStatus.COMPLETE);
    }//GEN-LAST:event_jButtonAddActionPerformed

    private void jButtonDelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDelActionPerformed
        int row = jTable1.getSelectedRow();
        if (row < 0) {
            setStatus(Constant.getTextBundle("��س����͡��¡������׹����� Xray ����ͧ���ź��͹"), UpdateStatus.WARNING);
            return;
        }
        boolean ret = thePatientControl.deleteBorrowFilmXray(theBorrowFilmXray, this);
        if (ret == false) {
            return;
        }
        theBorrowFilmXray = theHO.initBorrowFilmXray("");
        //updateOGBorrowFilmXray(theBorrowFilmXray, null);
        setDefault();
        searchBorrowFilmXray(false);
    }//GEN-LAST:event_jButtonDelActionPerformed

    private void jButtonSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSaveActionPerformed
        actionCommand = true;
        updateGOBorrowFilmXray(theBorrowFilmXray);
        boolean ret = thePatientControl.saveBorrowFilmXray(theBorrowFilmXray, this);
        if (ret == false) {
            return;
        }
        //this.updateOGBorrowFilmXray(theBorrowFilmXray,null);
        searchBorrowFilmXray(true);
    }//GEN-LAST:event_jButtonSaveActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        setVisible(false);
        //closeDialog = true;
        dispose();
    }//GEN-LAST:event_formWindowClosing

    /**
     *
     */
    @Override
    public JFrame getJFrame() {
        return this;
    }
    ///////////////////////////////////////////////////////////////////////////

    /**
     * �纤�Ҩҡ GUI �������� Object
     */
    public void updateGOBorrowFilmXray(BorrowFilmXray bor) {
        if (bor != null && thePatient != null) {
            bor.patient_hn = thePatient.hn;
            bor.patient_xn = thePatient.xn;
        }
        bor.date_serv = theHO.date_time;

        bor.borrow_film_date = dateComboBoxDateBorrowFilm.getText();//Gutil.getGuiBDate(dateComboBoxDateBorrowFilm.getText());
        bor.borrow_status = "0";
        bor.return_film_date = "";
        if (jCheckBoxReturnFilmDate.isSelected()) {
            bor.borrow_status = "1";
            bor.return_film_date = dateComboBoxDateReturnFilm.getText();
        }
        bor.borrow_cause = Gutil.CheckReservedWords(jTextAreaCauseBorrow.getText());
        bor.amount_date = jTextFieldAmountDate.getText();
        bor.borrower_name = jTextFieldBXFName.getText();
        bor.borrower_lastname = jTextFieldBXLName.getText();
        bor.permissibly_borrow = jTextFieldPermissibly.getText();
        bor.borrow_to = jTextFieldHosCode.getText();
        if (bor.borrow_staff_record == null || bor.borrow_staff_record.isEmpty()) {
            bor.borrow_staff_record = theHO.theEmployee.getObjectId();
            bor.borrow_record_date_time = theHO.date_time;
        }
        bor.borrow_to_other = jTextFieldBorrowToOther.getText();
    }
    ///////////////////////////////////////////////////////////////////////////

    /**
     * ૵�������Dialog
     */
//    private void setDialog()
//    {   
//        setSize(740,400);
//        setTitle("��ùѴ����");
//        Toolkit thekit = getToolkit();
//        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
//        setLocation((screenSize.width-getSize().width)/2, (screenSize.height-getSize().height)/2);
//    }
//    public void setVisit(Visit v){
//        theVisit = v;
//    }
//    public Appointment2 getAppointment(){
//        return theAppointment;
//    }
    ////////////////////////////////////////////////////////////////////////////
    /**
     * �� Static Function ���ͷӡ�����Dialog
     */
    public static boolean showDialog(HosControl hc, UpdateStatus us) {
        DialogBorrowFilmXray dlg = new DialogBorrowFilmXray(hc, us);
        dlg.setSize(640, 480);
        dlg.setTitle(Constant.getTextBundle("�������׹����� Xray"));

//        Toolkit thekit = dlg.getToolkit();
//        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
//        dlg.setLocation((screenSize.width - dlg.getSize().width) / 2, (screenSize.height - dlg.getSize().height) / 2);
        dlg.setLocationRelativeTo(null);
        dlg.setVisible(true);
        if (dlg.actionCommand) {
            return true;
        }
        System.gc();
        return false;
    }

    ///////////////////////////////////////////////////////////////////////////
    /**
     * ૵������ҧ�
     */
    private void setEnableAll(boolean var) {
        jButtonAdd.setEnabled(var);
        jButtonSave.setEnabled(var);
        jButtonDel.setEnabled(var);
    }
    ///////////////////////////////////////////////////////////////////////////

    /**
     * ������¡���������� Xray
     */
    private void searchBorrowFilmXray(boolean fromsave) {
        String datefrom = dateComboBoxDateFrom.getText();
        String dateto = dateComboBoxDateTo.getText();
        String hn = "";
        String xn = "";
        boolean show = true;
        if (fromsave == true) {
            datefrom = theBorrowFilmXray.borrow_film_date;
            dateto = theBorrowFilmXray.borrow_film_date;
        }
        boolean all_period = !this.jCheckBoxDateSearch.isSelected();
        String active = "1";
        if (jCheckBoxShowCancel.isSelected()) {
            active = "0";
        }
        if (jCheckBoxSCurrPatient.isSelected()) {
            if (jTextFieldXN.getText().isEmpty() && jTextFieldHN.getText().isEmpty()) {
//                setStatus(Constant.getTextBundle("�������ö������ ���ͧ�ҡ����բ����ż����»Ѩ�غѹ") ,WARNING);
                updateOGBorrowFilmXrayV(null, true);
                return;
            }
            if (!jTextFieldHN.getText().isEmpty()) {
                hn = jTextFieldHN.getText();
                show = true;
            }
            if (!jTextFieldXN.getText().isEmpty() && jTextFieldHN.getText().isEmpty()) {
                xn = jTextFieldXN.getText();
                show = false;
            }
        } else {
            if (jTextFieldXN.getText().isEmpty() && jTextFieldHN.getText().isEmpty()) {
                show = false;
            }
        }
        vBorrow = thePatientControl.listBorrowFilmXrayByDate(all_period, datefrom, dateto, hn, xn, active);
//        if(vBorrow == null)
//        {
//            setStatus(Constant.getTextBundle("�������ö������") ,WARNING);
//        }
//        setStatus(Constant.getTextBundle("������¡���������� Xray �������") ,COMPLETE);
        if (show == true) {
            updateOGBorrowFilmXrayV(vBorrow, true);
        } else {
            updateOGBorrowFilmXrayV(vBorrow, false);
        }
    }

    /**
     * ����ͷӡ�����͡ BorrowFilmXray
     */
    private void updateOGBorrowFilmXray(BorrowFilmXray bor, Patient patient) {
        if (bor != null) {
            theBorrowFilmXray = bor;
            dateComboBoxDateBorrowFilm.setText(DateUtil.convertFieldDate(theBorrowFilmXray.borrow_film_date));
            dateComboBoxDateReturnFilm.setText(DateUtil.convertFieldDate(theBorrowFilmXray.return_film_date));
            jTextFieldBXFName.setText(theBorrowFilmXray.borrower_name);
            jTextFieldBXLName.setText(theBorrowFilmXray.borrower_lastname);
            jTextAreaCauseBorrow.setText(theBorrowFilmXray.borrow_cause);
            jTextFieldHN.setText(bor.patient_hn);
            jTextFieldXN.setText(bor.patient_xn);
            if (theBorrowFilmXray.patient_hn != null || !theBorrowFilmXray.patient_hn.isEmpty()) {
                thePatient = thePatientControl.readPatientByHnToBorrowFilm(theBorrowFilmXray.patient_hn, this);
                if (thePatient == null) {
                    return;
                }
                jTextFieldHN.setText(thePatient.hn);
                jTextFieldXN.setText(thePatient.xn);
                Prefix prefix = theHC.theLookupControl.readPrefixById(thePatient.f_prefix_id);
                String sPrefix = "";
                if (prefix != null) {
                    sPrefix = prefix.description;
                }
                jTextFieldPatientName.setText(sPrefix + " "
                        + thePatient.patient_name + " " + thePatient.patient_last_name);
            } else {
                jTextFieldHN.setText("");
                jTextFieldXN.setText("");
                jTextFieldPatientName.setText("");
            }
            if (theBorrowFilmXray.borrow_status.equals("1")) {
                jCheckBoxReturnFilmDate.setSelected(true);
            } else {
                jCheckBoxReturnFilmDate.setSelected(false);
            }
            jCheckBoxReturnFilmDateActionPerformed(null);
            jTextFieldAmountDate.setText(theBorrowFilmXray.amount_date);
            jTextFieldPermissibly.setText(theBorrowFilmXray.permissibly_borrow);
            if (theBorrowFilmXray.borrow_to.isEmpty() || theBorrowFilmXray.borrow_to == null) {
                jTextFieldHosCode.setText("");
                jTextFieldHosName.setText("");
            } else {
                Office of = theLookupControl.readHospitalByCode(theBorrowFilmXray.borrow_to);
                jTextFieldHosName.setText(of.name);
                jTextFieldHosCode.setText(of.getCode());
            }
            jTextFieldCancel.setVisible(false);
            if (theBorrowFilmXray.borrow_active.equals("0")) {
                jTextFieldCancel.setVisible(true);
            }
            jTextFieldBorrowToOther.setText(theBorrowFilmXray.borrow_to_other);
        } else {
            jTextFieldHN.setText(patient.hn);
            jTextFieldXN.setText(patient.xn);
            Prefix prefix = theHC.theLookupControl.readPrefixById(patient.f_prefix_id);
            String sPrefix = "";
            if (prefix != null) {
                sPrefix = prefix.description;
            }
            jTextFieldPatientName.setText(sPrefix + " "
                    + patient.patient_name + " " + patient.patient_last_name);
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    /**
     * ૵���㹵��ҧ BorrowFilmXray
     */
    private void updateOGBorrowFilmXrayV(Vector vbor, boolean show) {
        TaBleModel tm;
        int[] rows = jTable1.getSelectedRows();
        if (show == true) {
            if (vbor == null || vbor.isEmpty()) {
                tm = new TaBleModel(collistHn, 0);
                jTable1.setModel(tm);
                return;
            }
            tm = new TaBleModel(collistHn, vbor.size());
            for (int i = 0; i < vbor.size(); i++) {
                SpecialQueryBorrowFilmXray sqbor = (SpecialQueryBorrowFilmXray) vbor.get(i);
                Prefix prefix = this.theLookupControl.readPrefixById(sqbor.patient_prefix);
                String prefix_str = "";
                if (prefix != null) {
                    prefix_str = prefix.description;
                }
                String pt_name = prefix_str + " "
                        + sqbor.patient_firstname + " "
                        + sqbor.patient_lastname;
                Prefix prefix1 = this.theLookupControl.readPrefixById(sqbor.borrower_prefix);
                String prefix_bor = "";
                if (prefix1 != null) {
                    prefix_bor = prefix1.description;
                }
                String bor_name = prefix_bor + " "
                        + sqbor.borrower_name + " "
                        + sqbor.borrower_lastname;
                String date = DateUtil.getDateToString(DateUtil.getDateFromText(
                        sqbor.borrow_film_date), false);
                if (date == null) {
                    date = "";
                }

                tm.setValueAt(sqbor.patient_hn, i, 0);
                tm.setValueAt(pt_name, i, 1);
                tm.setValueAt(date, i, 2);
                tm.setValueAt(bor_name, i, 3);
                tm.setValueAt(sqbor.borrow_status, i, 4);
            }
            jTable1.setModel(tm);
            setjTable1Default(rows);
        } else {
            if (vbor == null || vbor.isEmpty()) {
                tm = new TaBleModel(collistXn, 0);
                jTable1.setModel(tm);
                return;
            }
            tm = new TaBleModel(collistXn, vbor.size());
            for (int i = 0; i < vbor.size(); i++) {
                SpecialQueryBorrowFilmXray sqbor = (SpecialQueryBorrowFilmXray) vbor.get(i);
                Prefix prefix = this.theLookupControl.readPrefixById(sqbor.patient_prefix);
                String prefix_str = "";
                if (prefix != null) {
                    prefix_str = prefix.description;
                }
                String pt_name = prefix_str + " "
                        + sqbor.patient_firstname + " "
                        + sqbor.patient_lastname;
                Prefix prefix1 = this.theLookupControl.readPrefixById(sqbor.borrower_prefix);
                String prefix_bor = "";
                if (prefix1 != null) {
                    prefix_bor = prefix1.description;
                }
                String bor_name = prefix_bor + " "
                        + sqbor.borrower_name + " "
                        + sqbor.borrower_lastname;
                String date = DateUtil.getDateToString(DateUtil.getDateFromText(
                        sqbor.borrow_film_date), false);
                if (date == null) {
                    date = "";
                }

                tm.setValueAt(sqbor.patient_xn, i, 0);
                tm.setValueAt(pt_name, i, 1);
                tm.setValueAt(date, i, 2);
                tm.setValueAt(bor_name, i, 3);
                tm.setValueAt(sqbor.borrow_status, i, 4);
            }
            jTable1.setModel(tm);
            setjTable1Default(rows);
        }
    }

    private void setjTable1Default(int[] rows) {
        jTable1.getColumnModel().getColumn(0).setPreferredWidth(45);
        jTable1.getColumnModel().getColumn(0).setCellRenderer(hnRender);
        jTable1.getColumnModel().getColumn(1).setPreferredWidth(90);
        jTable1.getColumnModel().getColumn(2).setPreferredWidth(83);
        jTable1.getColumnModel().getColumn(3).setPreferredWidth(90);
        jTable1.getColumnModel().getColumn(4).setPreferredWidth(7);
        jTable1.getColumnModel().getColumn(4).setCellRenderer(cellRenderer);
    }
    ///////////////////////////////////////////////////////////////////////////

    /**
     * ��㹡��૵����
     */
    private void setLanguage(String msg) {
        GuiLang.setLanguage(jLabelAmountDate);
        GuiLang.setLanguage(jLabelAmountDate1);
        GuiLang.setLanguage(jButtonPrintListBorrowFilmXray);
        GuiLang.setLanguage(jButtonPreviewListBorrowFilmXray);
        GuiLang.setLanguage(jButtonSave);
        GuiLang.setLanguage(jLabelDateEnd);
        GuiLang.setLanguage(jLabelBorrowTo);
        GuiLang.setLanguage(jLabelBorrowFilmDate);
        GuiLang.setLanguage(jLabelCauseBorrow);
        GuiLang.setLanguage(jLabelPermissibly_Borrow);
        GuiLang.setLanguage(jCheckBoxReturnFilmDate);
        GuiLang.setLanguage(jCheckBoxShowCancel);
        GuiLang.setLanguage(jLabelBorrowName);
        GuiLang.setLanguage(jLabelFLName);
        GuiLang.setLanguage(jLabelXn);
        GuiLang.setLanguage(jLabelHn);
        GuiLang.setLanguage(jCheckBoxDateSearch);
        GuiLang.setLanguage(collistHn);
        GuiLang.setLanguage(collistXn);
        GuiLang.setLanguage(jPanel3);
        GuiLang.setLanguage(jPanel7);
        GuiLang.setLanguage(jLabelBorrowToOther);
        GuiLang.setLanguage(jCheckBoxSCurrPatient);
        jTextFieldCancel.setText(GuiLang.setLanguage(jTextFieldCancel.getText()));
    }
    ////////////////////////////////////////////////////////////////////////////

    /**
     * ��˹���� Default ����ͧ��ҧ�
     */
    private void setDefault() {
        jTextFieldHN.setText("");
        jTextFieldXN.setText("");
        jTextFieldPatientName.setText("");
        jTextFieldBXFName.setText("");
        jTextFieldBXLName.setText("");
        dateComboBoxDateBorrowFilm.setText(DateUtil.convertFieldDate(theHO.date_time));
        dateComboBoxDateReturnFilm.setText(DateUtil.convertFieldDate(dateComboBoxDateBorrowFilm.getText()));
        jCheckBoxReturnFilmDate.setSelected(false);
        dateComboBoxDateReturnFilm.setEnabled(false);
        jTextFieldAmountDate.setText("");
        jTextFieldPermissibly.setText("");
        jTextAreaCauseBorrow.setText("");
        jTextFieldHosCode.setText("");
        jTextFieldHosName.setText("");
        jTextFieldBorrowToOther.setText("");
        thePatient = new Patient();
        theBorrowFilmXray = new BorrowFilmXray();
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private com.hospital_os.utility.DateComboBox dateComboBoxDateBorrowFilm;
    private com.hospital_os.utility.DateComboBox dateComboBoxDateFrom;
    private com.hospital_os.utility.DateComboBox dateComboBoxDateReturnFilm;
    private com.hospital_os.utility.DateComboBox dateComboBoxDateTo;
    private javax.swing.JButton jButtonAdd;
    private javax.swing.JButton jButtonDel;
    private javax.swing.JButton jButtonHos;
    private javax.swing.JButton jButtonPreviewListBorrowFilmXray;
    private javax.swing.JButton jButtonPrintListBorrowFilmXray;
    private javax.swing.JButton jButtonSave;
    private javax.swing.JCheckBox jCheckBoxDateSearch;
    private javax.swing.JCheckBox jCheckBoxReturnFilmDate;
    private javax.swing.JCheckBox jCheckBoxSCurrPatient;
    private javax.swing.JCheckBox jCheckBoxShowCancel;
    private javax.swing.JLabel jLabelAmountDate;
    private javax.swing.JLabel jLabelAmountDate1;
    private javax.swing.JLabel jLabelBorrowFilmDate;
    private javax.swing.JLabel jLabelBorrowName;
    private javax.swing.JLabel jLabelBorrowTo;
    private javax.swing.JLabel jLabelBorrowToOther;
    private javax.swing.JLabel jLabelCauseBorrow;
    private javax.swing.JLabel jLabelDateEnd;
    private javax.swing.JLabel jLabelFLName;
    private javax.swing.JLabel jLabelHn;
    private javax.swing.JLabel jLabelPermissibly_Borrow;
    private javax.swing.JLabel jLabelStatus;
    private javax.swing.JLabel jLabelXn;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanelSearch;
    protected javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    protected com.hosv3.gui.component.HJTableSort jTable1;
    private javax.swing.JTextArea jTextAreaCauseBorrow;
    private com.hospital_os.utility.DoubleTextField jTextFieldAmountDate;
    private javax.swing.JTextField jTextFieldBXFName;
    private javax.swing.JTextField jTextFieldBXLName;
    private com.hosv3.gui.component.BalloonTextField jTextFieldBorrowToOther;
    private javax.swing.JTextField jTextFieldCancel;
    private com.hospital_os.utility.HNTextField jTextFieldHN;
    private javax.swing.JTextField jTextFieldHosCode;
    private javax.swing.JTextField jTextFieldHosName;
    private javax.swing.JTextField jTextFieldPatientName;
    private javax.swing.JTextField jTextFieldPermissibly;
    private javax.swing.JTextField jTextFieldXN;
    // End of variables declaration//GEN-END:variables
}

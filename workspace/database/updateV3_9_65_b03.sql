DROP TRIGGER IF exists newspews_notify_delete_trigger ON t_newspews_notify ;

CREATE OR REPLACE FUNCTION public.calculate_newspews_type(agegroup integer)
 RETURNS news_or_pews
 LANGUAGE plpgsql
AS $function$
    BEGIN
        IF agegroup = 10 then 
           return 'NEWS';
        ELSE 
           return 'PEWS';
        END IF;
    END;
$function$
;

CREATE OR REPLACE FUNCTION public.calculate_agegroup_backward(visit_age character varying)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
	BEGIN
	       if (visit_age::int >= 16) then 
	       	  return 10;
	       else 
	       	  return 1;
	       end if;
	END;
$function$
;

CREATE TYPE news_or_pews AS ENUM ('NEWS', 'PEWS');
ALTER TABLE public.t_visit ADD visit_vital_sign_newspews_type news_or_pews NULL;
ALTER TABLE public.t_newspews_notify ADD newspews_type news_or_pews NULL;
ALTER TABLE public.b_news_pews_msg_pattern ADD newspews_type news_or_pews NULL;
ALTER TABLE public.b_news_pews_score ADD newspews_type news_or_pews NULL;

update b_news_pews_msg_pattern
set newspews_type = 'NEWS';

update b_news_pews_score
set newspews_type = 'NEWS';

WITH subquery AS (
		 select calculate_newspews_type(calculate_agegroup_backward(t_visit.visit_patient_age)) as newspews_type ,t_visit.visit_patient_age ,t_newspews_notify.t_newspews_notify_id 
		 from t_newspews_notify
		 inner join t_visit_vital_sign on t_newspews_notify.t_visit_vital_sign_id = t_visit_vital_sign.t_visit_vital_sign_id 
		 inner join t_visit on t_visit_vital_sign.t_visit_id = t_visit.t_visit_id 
            )
            update t_newspews_notify set newspews_type = subquery.newspews_type
            from subquery
            where t_newspews_notify.t_newspews_notify_id = subquery.t_newspews_notify_id;

update t_visit
set visit_vital_sign_newspews_type = t_newspews_notify.newspews_type 
from t_newspews_notify
where t_visit.visit_vn = t_newspews_notify.vn and t_visit.visit_vital_sign_score  = t_newspews_notify.score and t_visit.visit_vital_sign_notify_datetime = t_newspews_notify.next_timestamp ;
 
INSERT INTO public.b_news_pews_msg_pattern
(b_news_pews_msg_pattern_id, message_pattern, user_record_id, record_date_time, user_update_id, update_date_time, is_opd, newspews_type)
VALUES('2', 'แจ้งเตือนวัด V/S ครั้งถัดไป เวลา {next_timestamp}
ผู้ป่วย {name} hn {hn}
จุดบริการ {service_name}
==================
แจ้งเตือนจากค่า Pews Score', '1578299109445', CURRENT_TIMESTAMP, '1578299109445', CURRENT_TIMESTAMP, true, 'PEWS');

INSERT INTO public.b_news_pews_msg_pattern
(b_news_pews_msg_pattern_id, message_pattern, user_record_id, record_date_time, user_update_id, update_date_time, is_opd, newspews_type)
VALUES('3', 'แจ้งเตือนวัด V/S ครั้งถัดไป เวลา {next_timestamp}
ผู้ป่วย {name} hn {hn}
วอร์ด {service_name} เตียง {bed_no}
==================
แจ้งเตือนจากค่า Pews Score', '1578299109445', CURRENT_TIMESTAMP, '1578299109445', CURRENT_TIMESTAMP, false,'PEWS');

CREATE OR REPLACE FUNCTION public.calculate_hrscore(visit_vital_sign_heart_rate text, agegroup integer)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
	declare
		 HRmaxAHRay int[];
		 HRmiduAHRay int[];
		 HRmidlAHRay int[];
		 HRmax int;
		 HRmidu int;
		 HRmid text;
		 HRmidl int;
		 HRmin text;
		 HR int;
	BEGIN
		HRmaxAHRay := '{190, 180, 150, 150, 140, 140, 125, 125, 115, 130}'::int[];
		HRmiduAHRay := '{180, 170, 140, 140, 130, 130, 115, 115, 105, 110}'::int[];
		HRmidlAHRay := '{100, 100, 80, 80, 70, 65, 60, 60, 55, 50}'::int[];
		HRmax := HRmaxAHRay[agegroup];
		HRmidu := HRmiduAHRay[agegroup];
		HRmidl := HRmidlAHRay[agegroup];
		if(agegroup = 10) then
			HRmid := '90';
			HRmin := '40';
		else
			HRmid := 'FALSE';
			HRmin := 'FALSE';
		end if;
	
		
		/*raise notice '% % % % %',HRmax,HRmidu,HRmid,HRmidl,HRmin;*/

		if(visit_vital_sign_heart_rate is null or visit_vital_sign_heart_rate = '') then 
			return 0;
		else
			HR := (visit_vital_sign_heart_rate::int);
			if(agegroup = 10) then
				if(HRmin = 'FALSE' or HR <= (HRmin::int)) then 
					return 3;
				elsif(HR <= HRmidl) then
					return 1;
				elsif(HRmid = 'FALSE' or HR <= (HRmid::int)) then
					return 0;
				elsif(HR <= HRmidu) then
					return 1;
				elsif(HR <= HRmax) then
					return 2;
				else
					return 3;
				end if;
			else
				if(HR < HRmidl) then
					return 3;
				elsif(HR < HRmidu) then
					return 0;
				elsif(HR <= HRmax) then
					return 2;
				else
					return 3;
				end if;
			end if;
		end if;
	END;
$function$
;


CREATE OR REPLACE FUNCTION public.calculate_newspews_score(vital_sign_id character varying)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
    DECLARE
        agegroup int;
        rrscore int;
        hrscore int;
        O2supScore int;
        btscore int;
        bpscore int;
        o2satscore int;
        avpuscore int;
        behaviorscore int;
        nebulizescore int;
        vomitingscore int;
        capscore int;
        cardioscore int;
        respiscore int;
    BEGIN
        select calculate_agegroup(t_visit_vital_sign.t_patient_id)
        from t_visit_vital_sign
        into agegroup
        where t_visit_vital_sign.t_visit_vital_sign_id = vital_sign_id;
        
        select calculate_rrscore(t_visit_vital_sign.visit_vital_sign_respiratory_rate,agegroup) as rrscore
            ,calculate_hrscore(t_visit_vital_sign.visit_vital_sign_heart_rate,agegroup) as hrscore
            ,calculate_O2supScore(t_visit_vital_sign.visit_vital_sign_oxygen,agegroup) as O2supScore
            ,calculate_btscore(t_visit_vital_sign.visit_vital_sign_temperature,agegroup) as btscore
            ,calculate_bpscore(t_visit_vital_sign.visit_vital_sign_blood_presure,agegroup) as bpscore
            ,calculate_o2satscore(t_visit_vital_sign.visit_vital_sign_spo2,agegroup) as o2satscore
            ,case when t_visit_vital_sign.f_avpu_type_id is not null then f_avpu_type.avpu_score else null end as avpuscore
            ,case when t_visit_vital_sign.f_behavior_type_id is not null then f_behavior_type.behavior_score else null end as behaviorscore
            ,case when t_visit_vital_sign.f_received_nebulization_id = '0' then 0 
                  when t_visit_vital_sign.f_received_nebulization_id = '1' then 1 
                  else null end as nebulizescore
            ,case when t_visit_vital_sign.f_vomitting_id = '0' then 0 
                  when t_visit_vital_sign.f_vomitting_id = '1' then 1 
                  else null end as vomitingscore
            ,case when t_visit_vital_sign.f_cardiovascular_type_id is not null then f_cardiovascular_type.cap_score else null end as capscore
        from t_visit_vital_sign
            inner join t_visit on t_visit_vital_sign.t_visit_id = t_visit.t_visit_id
            inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
            left join f_avpu_type on t_visit_vital_sign.f_avpu_type_id = f_avpu_type.f_avpu_type_id
            left join f_behavior_type on t_visit_vital_sign.f_behavior_type_id = f_behavior_type.f_behavior_type_id
            left join f_cardiovascular_type on t_visit_vital_sign.f_cardiovascular_type_id = f_cardiovascular_type.f_cardiovascular_type_id
		into rrscore,hrscore,O2supScore,btscore,bpscore,o2satscore,avpuscore,behaviorscore,nebulizescore,vomitingscore,capscore
        where t_visit_vital_sign.t_visit_vital_sign_id = vital_sign_id;

        if (capscore > hrscore) then 
            cardioscore := capscore;
        else
            cardioscore := hrscore;
        end if;

        if (rrscore > O2supScore) then 
            respiscore := rrscore;
        else
            respiscore := O2supScore;
        end if;

        if (agegroup = 10 and avpuscore is not null and rrscore is not null and hrscore is not null 
            	and O2supScore is not null and btscore is not null and bpscore is not null and o2satscore is not null) then
            return (rrscore+hrscore+O2supScore+btscore+bpscore+o2satscore+avpuscore);
        elsif (agegroup < 10 and behaviorscore is not null and nebulizescore is not null 
            	and vomitingscore is not null and cardioscore is not null and respiscore is not null) then
            return (behaviorscore+nebulizescore+vomitingscore+cardioscore+respiscore);
        else
            return null;
        end if;
          
    END;
$function$
;

CREATE OR REPLACE FUNCTION public.insert_newspews_notify()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
DECLARE
  newspews_score INTEGER;
  set_score BOOLEAN;
BEGIN
    SELECT calculate_newspews_score(NEW.t_visit_vital_sign_id) as newspews_score
          ,(select case when b_news_pews_score.b_news_pews_score_id is not null 
                        then true else null end as newspews_score 
            from b_news_pews_score 
            group by newspews_score) as set_score
    FROM b_option_detail
    INTO newspews_score,set_score
    WHERE b_option_detail_id = 'use_notify_news_pews';
    
    IF set_score IS NULL THEN
        RETURN NULL;
    ELSIF newspews_score IS NULL THEN
        RETURN NULL;
    ELSE
        IF (TG_OP = 'UPDATE') THEN 
            IF (NEW.visit_vital_sign_respiratory_rate <> OLD.visit_vital_sign_respiratory_rate
                OR NEW.visit_vital_sign_heart_rate <> OLD.visit_vital_sign_heart_rate
                OR NEW.visit_vital_sign_oxygen <> OLD.visit_vital_sign_oxygen
                OR NEW.visit_vital_sign_temperature <> OLD.visit_vital_sign_temperature
                OR NEW.visit_vital_sign_blood_presure <> OLD.visit_vital_sign_blood_presure
                OR NEW.visit_vital_sign_spo2 <> OLD.visit_vital_sign_spo2
				OR (NEW.f_avpu_type_id IS NOT NULL AND OLD.f_avpu_type_id IS NULL)
				OR (NEW.f_avpu_type_id IS NULL AND OLD.f_avpu_type_id IS NOT NULL)
				OR (NEW.f_received_nebulization_id IS NOT NULL AND OLD.f_received_nebulization_id IS NULL)
				OR (NEW.f_received_nebulization_id IS NULL AND OLD.f_received_nebulization_id IS NOT NULL)
				OR (NEW.f_vomitting_id IS NOT NULL AND OLD.f_vomitting_id IS NULL)
				OR (NEW.f_vomitting_id IS NULL AND OLD.f_vomitting_id IS NOT NULL)
				OR (NEW.f_cardiovascular_type_id IS NOT NULL AND OLD.f_cardiovascular_type_id IS NULL)
				OR (NEW.f_cardiovascular_type_id IS NULL AND OLD.f_cardiovascular_type_id IS NOT NULL)
                OR NEW.f_avpu_type_id <> OLD.f_avpu_type_id
                OR NEW.f_received_nebulization_id <> OLD.f_received_nebulization_id
                OR NEW.f_vomitting_id <> OLD.f_vomitting_id
                OR NEW.f_cardiovascular_type_id <> OLD.f_cardiovascular_type_id)
            THEN
                UPDATE t_newspews_notify SET status = 'CN'
                WHERE t_visit_vital_sign_id = NEW.t_visit_vital_sign_id
                AND t_newspews_notify.status = 'NW';

                INSERT INTO t_newspews_notify(t_newspews_notify_id, hn, vn, name, f_visit_type_id, service_id, service_name, visit_bed, score, next_timestamp, t_visit_vital_sign_id, newspews_type)
                select (select '806' || b_visit_office_id from b_site) || lpad(nextval('t_newspews_notify_id_seq')::text,17,'0') as t_newspews_notify_id
                            ,t_patient.patient_hn as hn 
                            ,t_visit.visit_vn as vn
                            ,case when f_patient_prefix.f_patient_prefix_id <> '000'
                                then f_patient_prefix.patient_prefix_description||t_patient.patient_firstname||' '||t_patient.patient_lastname
                                else t_patient.patient_firstname||' '||t_patient.patient_lastname end as name
                            ,t_visit.f_visit_type_id as f_visit_type_id
                            ,case when t_visit.f_visit_type_id = '1' then t_visit.b_visit_ward_id
                                  else t_visit_queue_transfer.b_service_point_id end as service_id
                            ,case when t_visit.f_visit_type_id = '1' then b_visit_ward.visit_ward_description
                                  else b_service_point.service_point_description end as service_name
                            ,case when t_visit.f_visit_type_id = '1' then t_visit.visit_bed
                                  else '' end as visit_bed
                            ,case when calculate_newspews_score(NEW.t_visit_vital_sign_id) is not null
                                then calculate_newspews_score(NEW.t_visit_vital_sign_id)
                                else 0 end as score
                           ,case when (calculate_newspews_score(NEW.t_visit_vital_sign_id) is not null
                                      and calculate_newspews_score(NEW.t_visit_vital_sign_id) = b_news_pews_score.score)
                                 then current_timestamp + (b_news_pews_score.hours || ' hours')::interval
                                 when (calculate_newspews_score(NEW.t_visit_vital_sign_id) is not null and b_news_pews_score.score is null)
                                 then current_timestamp + ((select hours from b_news_pews_score where b_news_pews_score.newspews_type = calculate_newspews_type(calculate_agegroup(t_patient.t_patient_id)) and calculate_newspews_score(NEW.t_visit_vital_sign_id) > score order by score desc limit 1) || ' hours')::interval
                                 else null end next_timestamp
                            ,t_visit_vital_sign.t_visit_vital_sign_id
                            ,calculate_newspews_type(calculate_agegroup(t_patient.t_patient_id)) as newspews_type
                    from t_visit_vital_sign
                        inner join t_visit on t_visit_vital_sign.t_visit_id = t_visit.t_visit_id
                        inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
                        left join f_patient_prefix on t_patient.f_patient_prefix_id = f_patient_prefix.f_patient_prefix_id
                        left join t_visit_queue_transfer on t_visit.t_visit_id = t_visit_queue_transfer.t_visit_id
                        left join b_service_point on t_visit_queue_transfer.b_service_point_id = b_service_point.b_service_point_id
                        left join b_visit_ward on t_visit.b_visit_ward_id = b_visit_ward.b_visit_ward_id
                        left join b_news_pews_score on calculate_newspews_score(NEW.t_visit_vital_sign_id) = b_news_pews_score.score
                        	  and b_news_pews_score.newspews_type = calculate_newspews_type(calculate_agegroup(t_patient.t_patient_id))
                    where t_visit_vital_sign.t_visit_vital_sign_id = NEW.t_visit_vital_sign_id
                    and t_visit_vital_sign.visit_vital_sign_active = '1';
             ELSE 
                RETURN NULL;
             END IF;
             RETURN NEW;
         ELSIF (TG_OP = 'INSERT') THEN
            INSERT INTO t_newspews_notify(t_newspews_notify_id, hn, vn, name, f_visit_type_id, service_id, service_name, visit_bed, score, next_timestamp, t_visit_vital_sign_id, newspews_type)
            select (select '806' || b_visit_office_id from b_site) || lpad(nextval('t_newspews_notify_id_seq')::text,17,'0') as t_newspews_notify_id
                        ,t_patient.patient_hn as hn 
                        ,t_visit.visit_vn as vn
                        ,case when f_patient_prefix.f_patient_prefix_id <> '000'
                            then f_patient_prefix.patient_prefix_description||t_patient.patient_firstname||' '||t_patient.patient_lastname
                            else t_patient.patient_firstname||' '||t_patient.patient_lastname end as name
                        ,t_visit.f_visit_type_id as f_visit_type_id
                        ,case when t_visit.f_visit_type_id = '1' then t_visit.b_visit_ward_id
                              else t_visit_queue_transfer.b_service_point_id end as service_id
                        ,case when t_visit.f_visit_type_id = '1' then b_visit_ward.visit_ward_description
                              else b_service_point.service_point_description end as service_name
                        ,case when t_visit.f_visit_type_id = '1' then t_visit.visit_bed
                              else '' end as visit_bed
                        ,case when calculate_newspews_score(NEW.t_visit_vital_sign_id) is not null
                            then calculate_newspews_score(NEW.t_visit_vital_sign_id)
                            else 0 end as score
                       ,case when (calculate_newspews_score(NEW.t_visit_vital_sign_id) is not null 
                                  and calculate_newspews_score(NEW.t_visit_vital_sign_id) = b_news_pews_score.score)
                             then current_timestamp + (b_news_pews_score.hours || ' hours')::interval
                             when (calculate_newspews_score(NEW.t_visit_vital_sign_id) is not null and b_news_pews_score.score is null)
                             then current_timestamp + ((select hours from b_news_pews_score where b_news_pews_score.newspews_type = calculate_newspews_type(calculate_agegroup(t_patient.t_patient_id)) and calculate_newspews_score(NEW.t_visit_vital_sign_id) > score order by score desc limit 1) || ' hours')::interval
                             else null end next_timestamp
                        ,t_visit_vital_sign.t_visit_vital_sign_id
                        ,calculate_newspews_type(calculate_agegroup(t_patient.t_patient_id)) as newspews_type
                from t_visit_vital_sign
                    inner join t_visit on t_visit_vital_sign.t_visit_id = t_visit.t_visit_id
                    inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
                    left join f_patient_prefix on t_patient.f_patient_prefix_id = f_patient_prefix.f_patient_prefix_id
                    left join t_visit_queue_transfer on t_visit.t_visit_id = t_visit_queue_transfer.t_visit_id
                    left join b_service_point on t_visit_queue_transfer.b_service_point_id = b_service_point.b_service_point_id
                    left join b_visit_ward on t_visit.b_visit_ward_id = b_visit_ward.b_visit_ward_id
                    left join b_news_pews_score on calculate_newspews_score(NEW.t_visit_vital_sign_id) = b_news_pews_score.score
                    	  and b_news_pews_score.newspews_type = calculate_newspews_type(calculate_agegroup(t_patient.t_patient_id))
                where t_visit_vital_sign.t_visit_vital_sign_id = NEW.t_visit_vital_sign_id
			    and t_visit_vital_sign.visit_vital_sign_active = '1';
            RETURN NEW;
        END IF;
    END IF;
RETURN NEW;
END;
$function$
;

CREATE OR REPLACE FUNCTION public.call_newspews_linenotify_api(newspews_notify_id text DEFAULT NULL::text)
 RETURNS character varying
 LANGUAGE sql
AS $function$
    WITH newspews_url AS (
        select json_build_object('dag_run_id',newspews_message.dag_run_id
        ,'execution_date',newspews_message.execution_date
        ,'conf',json_build_object('message',newspews_message.message
        ,'line_token',COALESCE(json_agg(newspews_message.line_token) FILTER (WHERE newspews_message.line_token IS NOT NULL), '[]')
        )) as line_notify_message
        ,replace(replace(b_restful_url.restful_url,split_part(b_restful_url.restful_url,'?',2),'/dagRuns'),'?','') as restful_url 
        ,'Basic ' || encode(split_part(b_restful_url.restful_url,'?',2)::bytea,'base64') as authen 
    from (select t_newspews_notify.t_newspews_notify_id as dag_run_id
            ,to_char((t_newspews_notify.next_timestamp at time zone 'asia/bangkok' at time zone 'utc'),'yyyy-MM-ddThh24:mi:ssZ') as execution_date
            ,replace(replace(replace(replace(replace(b_news_pews_msg_pattern.message_pattern
                ,'{next_timestamp}'
                    ,to_char((t_newspews_notify.next_timestamp at time zone 'asia/bangkok'),'DD')
                     || ' ' || case when to_char((t_newspews_notify.next_timestamp at time zone 'asia/bangkok'),'MM') = '01' then 'ม.ค.'
                                    when to_char((t_newspews_notify.next_timestamp at time zone 'asia/bangkok'),'MM') = '02' then 'ก.พ.'
                                    when to_char((t_newspews_notify.next_timestamp at time zone 'asia/bangkok'),'MM') = '03' then 'มี.ค.'
                                    when to_char((t_newspews_notify.next_timestamp at time zone 'asia/bangkok'),'MM') = '04' then 'เม.ย.'
                                    when to_char((t_newspews_notify.next_timestamp at time zone 'asia/bangkok'),'MM') = '05' then 'พ.ค.'
                                    when to_char((t_newspews_notify.next_timestamp at time zone 'asia/bangkok'),'MM') = '06' then 'มิ.ย.'
                                    when to_char((t_newspews_notify.next_timestamp at time zone 'asia/bangkok'),'MM') = '07' then 'ก.ค.'
                                    when to_char((t_newspews_notify.next_timestamp at time zone 'asia/bangkok'),'MM') = '08' then 'ส.ค.'
                                    when to_char((t_newspews_notify.next_timestamp at time zone 'asia/bangkok'),'MM') = '09' then 'ก.ย.'
                                    when to_char((t_newspews_notify.next_timestamp at time zone 'asia/bangkok'),'MM') = '10' then 'ต.ค.'
                                    when to_char((t_newspews_notify.next_timestamp at time zone 'asia/bangkok'),'MM') = '11' then 'พ.ย.'
                                    when to_char((t_newspews_notify.next_timestamp at time zone 'asia/bangkok'),'MM') = '12' then 'ธ.ค.' end
                     || ' ' || substr(((to_char((t_newspews_notify.next_timestamp at time zone 'asia/bangkok'),'YYYY')::int)+543)::text,3,2)
                     || ' ' || to_char((t_newspews_notify.next_timestamp at time zone 'asia/bangkok'),'HH24:MI น.'))
                        ,'{name}',t_newspews_notify.name)
                            ,'{service_name}',t_newspews_notify.service_name)
                                ,'{bed_no}',t_newspews_notify.visit_bed)
                                ,'{hn}',t_newspews_notify.hn) as message
            ,case when t_newspews_notify.f_visit_type_id = '1' 
                      then ipd_token.line_notify_token
                      else opd_token.line_notify_token end as line_token
        from t_newspews_notify
            left join b_news_pews_line_token as opd_token on t_newspews_notify.service_id = opd_token.b_service_point_id
            left join b_news_pews_line_token as ipd_token on t_newspews_notify.service_id = ipd_token.b_visit_ward_id
            left join b_news_pews_msg_pattern on t_newspews_notify.f_visit_type_id = (case when b_news_pews_msg_pattern.is_opd is true then '0' else '1' end) 
                  and t_newspews_notify.newspews_type = b_news_pews_msg_pattern.newspews_type
        where t_newspews_notify.t_newspews_notify_id = newspews_notify_id
        and t_newspews_notify.status = 'NW'
        ) as newspews_message
        cross join b_restful_url
    where b_restful_url.restful_code = 'news.pews.linenotify'
    group by newspews_message.dag_run_id
        ,newspews_message.execution_date
        ,newspews_message.message
        ,b_restful_url.restful_url
      )
    SELECT
       content
    FROM newspews_url
        , http_post(newspews_url.restful_url,newspews_url.line_notify_message::text ,'application/json',ARRAY[http_header('Authorization',newspews_url.authen)])
$function$
;

CREATE OR REPLACE FUNCTION public.update_visit_npscore()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
DECLARE
  newspews_score INTEGER;
  set_score BOOLEAN;
BEGIN
    SELECT calculate_newspews_score(NEW.t_visit_vital_sign_id) as newspews_score
          ,(select case when b_news_pews_score.b_news_pews_score_id is not null 
                        then true else null end as newspews_score 
            from b_news_pews_score 
            group by newspews_score) as set_score
    FROM b_option_detail
    INTO newspews_score,set_score
    WHERE b_option_detail_id = 'use_notify_news_pews';
    
    IF set_score IS NULL THEN
        RETURN NULL;
    ELSIF newspews_score IS NULL THEN
        RETURN NULL;
    ELSE
        WITH subquery AS (
            SELECT t_visit_vital_sign.t_visit_id
                ,t_newspews_notify.score
                ,t_newspews_notify.next_timestamp
                ,t_newspews_notify.newspews_type
            FROM t_newspews_notify
                inner join t_visit_vital_sign on t_newspews_notify.t_visit_vital_sign_id = t_visit_vital_sign.t_visit_vital_sign_id
            WHERE t_newspews_notify.t_visit_vital_sign_id = NEW.t_visit_vital_sign_id
            AND t_newspews_notify.status = 'NW'
            )
            update t_visit set visit_vital_sign_score = subquery.score
                ,visit_vital_sign_notify_datetime = subquery.next_timestamp
                ,visit_vital_sign_newspews_type = subquery.newspews_type
            from subquery
            where t_visit.t_visit_id = subquery.t_visit_id;
        END IF;
    RETURN NEW;
END;
$function$
;

CREATE OR REPLACE FUNCTION public.delete_visit_npscore()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
declare
begin
    WITH subquery AS (
            SELECT t_visit_vital_sign.t_visit_id
                ,t_newspews_notify.score
                ,t_newspews_notify.next_timestamp
                ,t_newspews_notify.newspews_type
            FROM t_newspews_notify
                inner join t_visit_vital_sign on t_newspews_notify.t_visit_vital_sign_id = t_visit_vital_sign.t_visit_vital_sign_id
            WHERE t_newspews_notify.t_visit_vital_sign_id = NEW.t_visit_vital_sign_id
            )
            update t_visit set visit_vital_sign_score = null
                ,visit_vital_sign_notify_datetime = null
                ,visit_vital_sign_newspews_type = null
            from subquery
            where t_visit.t_visit_id = subquery.t_visit_id
                and t_visit.visit_vital_sign_score = subquery.score
                and t_visit.visit_vital_sign_notify_datetime = subquery.next_timestamp
                and t_visit.visit_vital_sign_newspews_type = subquery.newspews_type;
               
       UPDATE t_newspews_notify SET status = 'CN'
       WHERE t_visit_vital_sign_id = NEW.t_visit_vital_sign_id
            AND t_newspews_notify.status = 'NW';
    RETURN NEW;
END;
$function$
;

CREATE OR REPLACE FUNCTION public.newspews_notify()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
declare
    status INTEGER;
begin
    SELECT CASE WHEN option_detail_name = '1' 
                THEN 1
                ELSE 0 END as status
         FROM b_option_detail
        INTO status
        WHERE b_option_detail_id = 'use_notify_news_pews';
        
        if status = 0 then
            return null;
        else
            perform call_newspews_linenotify_api(new.t_newspews_notify_id);
            UPDATE t_newspews_notify SET status_airflow = 'Success' WHERE t_newspews_notify_id = new.t_newspews_notify_id;
        RETURN NULL; -- result is ignored since this is an AFTER trigger
        end if;
        exception 
        when others then
            UPDATE t_newspews_notify SET status_airflow = 'Fail' WHERE t_newspews_notify_id = new.t_newspews_notify_id;
            RETURN NULL;
END;
$function$
;

CREATE OR REPLACE FUNCTION public.newspews_notify_delete()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
declare
    status INTEGER;
begin
    SELECT CASE WHEN option_detail_name = '1' 
                THEN 1
                ELSE 0 END as status
         FROM b_option_detail
        INTO status
        WHERE b_option_detail_id = 'use_notify_news_pews';
        
        if status = 0 then
            return null;
        else
            perform call_newspews_linenotify_delete(new.t_newspews_notify_id);
            RETURN NULL; -- result is ignored since this is an AFTER trigger
        end if;
    exception 
    when others then
         RAISE INFO 'Error Name:%',SQLERRM;
         RAISE INFO 'Error State:%', SQLSTATE;
         RETURN NULL;
END;
$function$
;

DROP function IF exists calculate_agegroup_backward;

create constraint trigger newspews_notify_delete_trigger after
update
    on
    public.t_newspews_notify deferrable initially deferred for each row
    when (((new.status)::text = 'CN'::text)) execute function newspews_notify_delete();

CREATE OR REPLACE FUNCTION public.text_to_timestampz(text)
 RETURNS timestamp with time zone
 LANGUAGE plpgsql
 IMMUTABLE
AS $function$
BEGIN
        return case when length($1) >= 10 and ((substr($1,1,4)::int-543)||substr($1,5))::timestamp is not null then timezone('Asia/Bangkok',((substr($1,1,4)::int-543)||substr($1,5))::timestamp) AT TIME ZONE 'UTC' end;
exception when others then
        return null;
END;$function$
;

-- add newspews_age_group and doctor discharge info
drop view current_visit_info;
CREATE OR REPLACE VIEW public.current_visit_info
AS select 
tv.t_visit_id as visit_id,
tv.t_patient_id as pt_id,
tv.visit_hn as hn,
tv.visit_vn as vn,
tv.f_visit_type_id as visit_type_id,
case when tv.f_visit_type_id = '0' then 'OPD' else 'IPD' end as visit_type_label,
case when fpp.f_patient_prefix_id is null or fpp.f_patient_prefix_id = '000' then '' else fpp.patient_prefix_description end
|| thf.patient_name || ' ' || thf.patient_last_name as pt_name,
case when fpp.f_patient_prefix_id is null or fpp.f_patient_prefix_id = '000' then '' else fpp.patient_prefix_description_eng end
|| thf.patient_firstname_eng || ' ' || thf.patient_lastname_eng as pt_name_eng,
case when fs2.f_sex_id is null or fs2.f_sex_id = '' then '' else fs2.f_sex_id end as pt_gender_id,
case when fs2.f_sex_id is null or fs2.f_sex_id = '' then '' else fs2.sex_description end as pt_gender_label,
case when tp.patient_birthday is null or tp.patient_birthday = '' then null else text_to_timestamp(tp.patient_birthday)::date end as pt_dob,
EXTRACT(YEAR FROM age(to_date(substr(tp.patient_birthday, 0, 5)::int - 543 || substr(tp.patient_birthday, 5), 'YYYY-MM-DD'))) as pt_age_year,
EXTRACT(YEAR FROM age(to_date(substr(tp.patient_birthday, 0, 5)::int - 543 || substr(tp.patient_birthday, 5), 'YYYY-MM-DD'))) * 12 + EXTRACT(MONTH FROM age(to_date(substr(tp.patient_birthday, 0, 5)::int - 543 || substr(tp.patient_birthday, 5), 'YYYY-MM-DD'))) AS pt_age_months,
text_to_timestampz(tv.visit_begin_visit_time) as visit_at,
text_to_timestampz(tv.visit_begin_admit_date_time) as admit_at,
tv.visit_dx as dx,
case when tv.visit_patient_self_doctor is null or tv.visit_patient_self_doctor = '' then ''
else bev.name end as doctor,
case when tv.b_visit_clinic_id is null or tv.b_visit_clinic_id = '' then ''
else tv.b_visit_clinic_id end as current_clinic_id,
case when tv.b_visit_clinic_id is null or tv.b_visit_clinic_id = '' then ''
else bvc.visit_clinic_description end as current_clinic_label,

case when tvqt.t_visit_queue_transfer_id is null then ''
else tvqt.b_service_point_id end as current_servicepoint_id,
case when tvqt.t_visit_queue_transfer_id is null then ''
else tvqt.service_point_description end as current_servicepoint_label,

case when tv.b_visit_ward_id is null or tv.b_visit_ward_id = '' then ''
else tv.b_visit_ward_id end as current_ward_id,
case when tv.b_visit_ward_id is null or tv.b_visit_ward_id = '' then ''
else bvw.visit_ward_description end as current_ward_label,
tv.visit_bed as bed_no,
calculate_agegroup(tp.t_patient_id) as newspews_age_group,

tv.visit_doctor_discharge_status = '1' as is_doctor_discharge,
case when tv.visit_staff_doctor_discharge is null or tv.visit_staff_doctor_discharge = '' then ''
else bevdd.name end as doctor_discharge,
text_to_timestampz(tv.visit_staff_doctor_discharge_date_time) as doctor_discharge_at,
case when tv.visit_doctor_discharge_status <> '1' 
	then ''
	else
		case when (tv.f_visit_type_id = '1' and (tv.f_visit_ipd_discharge_type_id is null or tv.f_visit_ipd_discharge_type_id = ''))
					or  (tv.f_visit_type_id = '0' and (tv.f_visit_opd_discharge_status_id is null or tv.f_visit_opd_discharge_status_id = ''))
		then ''
		else
			-- IPD
			case when tv.f_visit_type_id = '1' 
				then (select visit_ipd_discharge_type_description from f_visit_ipd_discharge_type where f_visit_ipd_discharge_type_id = tv.f_visit_ipd_discharge_type_id)			
				else
					-- OPD with refer
					case when tv.f_visit_opd_discharge_status_id = '54'
						then 'ส่งต่อ ' || (select visit_office_name from b_visit_office where b_visit_office_id = tv.b_visit_office_id_refer_out)
						-- OPD
						else (select visit_opd_discharge_status_description from f_visit_opd_discharge_status where f_visit_opd_discharge_status_id = tv.f_visit_opd_discharge_status_id)
						end 
				end 
		end
	end as doctor_discharge_cause,


case when tp.picture_profile is null or tp.picture_profile = '' then null else regexp_replace(encode(tp.picture_profile, 'base64'), E'[\\n\\r]+', '', 'g' ) end as profile_img
from t_visit tv 
inner join t_patient tp on tp.t_patient_id = tv.t_patient_id 
inner join t_health_family thf on thf.t_health_family_id  = tp.t_health_family_id
left join f_sex fs2 on fs2.f_sex_id = thf.f_sex_id 
left join f_patient_prefix fpp on fpp.f_patient_prefix_id = thf.f_prefix_id 
left join b_employee_view bev on bev.id = tv.visit_patient_self_doctor 
left join b_visit_clinic bvc on bvc.b_visit_clinic_id = tv.b_visit_clinic_id 
left join b_visit_ward bvw on bvw.b_visit_ward_id = tv.b_visit_ward_id 
left join t_visit_queue_transfer tvqt on tvqt.t_visit_id = tv.t_visit_id
left join b_employee_view bevdd on bevdd.id = tv.visit_staff_doctor_discharge 
where tv.f_visit_status_id = '1';

-- add newspews_type text, newspews_score integer, next_at timestamp
drop function latest_vitalsign(text) CASCADE ;

CREATE OR REPLACE FUNCTION public.latest_vitalsign(visit_id text)
 RETURNS TABLE(t_visit_id text, weight text, height text, bmi text, temp text, bp text, map text, spo2 text, pulse text, rr text,
waistline text, waistline_inch text, hips text, hips_inch text, chest text, chest_inch text,
oxygen text, nutrition text, cardiovascular text, avpu text, behavior text, received_nebulization text, 
vomitting text, note text, 
latest_vs_id text, latest_recoder_id text, latest_recoder text, latest_check_at timestamp with time zone, 
newspews_type text, newspews_score integer, next_at timestamp with time zone)
 LANGUAGE sql
AS $function$
        select
	visit_id as t_visit_id ,
	(array_remove(string_to_array(string_agg(ds.visit_vital_sign_weight, ','), ','), ''))[1] as weight ,
	(array_remove(string_to_array(string_agg(ds.visit_vital_sign_height, ','), ','), ''))[1] as height ,
	(array_remove(string_to_array(string_agg(ds.visit_vital_sign_bmi, ','), ','), ''))[1] as bmi ,
	(array_remove(string_to_array(string_agg(ds.visit_vital_sign_temperature, ','), ','), ''))[1] as temp ,
	(array_remove(string_to_array(string_agg(ds.visit_vital_sign_blood_presure, ','), ','), ''))[1] as bp ,
	(array_remove(string_to_array(string_agg(ds.visit_vital_sign_map, ','), ','), ''))[1] as map ,
	(array_remove(string_to_array(string_agg(ds.visit_vital_sign_spo2, ','), ','), ''))[1] as spo2 ,
	(array_remove(string_to_array(string_agg(ds.visit_vital_sign_heart_rate, ','), ','), ''))[1] as pulse ,
	(array_remove(string_to_array(string_agg(ds.visit_vital_sign_respiratory_rate, ','), ','), ''))[1] as rr ,
	(array_remove(string_to_array(string_agg(ds.visit_vital_sign_waistline, ','), ','), ''))[1] as waistline ,
	(array_remove(string_to_array(string_agg(ds.visit_vital_sign_waistline_inch, ','), ','), ''))[1] as waistline_inch ,
	(array_remove(string_to_array(string_agg(ds.visit_vital_sign_hips, ','), ','), ''))[1] as hips ,
	(array_remove(string_to_array(string_agg(ds.visit_vital_sign_hips_inch, ','), ','), ''))[1] as hips_inch ,
	(array_remove(string_to_array(string_agg(ds.visit_vital_sign_chest, ','), ','), ''))[1] as chest ,
	(array_remove(string_to_array(string_agg(ds.visit_vital_sign_chest_inch, ','), ','), ''))[1] as chest_inch ,
	(array_remove(string_to_array(string_agg(ds.visit_vital_sign_oxygen, ','), ','), ''))[1] as oxygen ,
	(array_remove(string_to_array(string_agg(ds.nutrition, ','), ','), ''))[1] as nutrition ,
        (array_remove(string_to_array(string_agg(ds.cardiovascular, ','), ','), ''))[1] as cardiovascular ,
	(array_remove(string_to_array(string_agg(ds.avpu, ','), ','), ''))[1] as avpu ,
	(array_remove(string_to_array(string_agg(ds.behavior, ','), ','), ''))[1] as behavior ,
	(array_remove(string_to_array(string_agg(ds.received_nebulization, ','), ','), ''))[1] as received_nebulization ,
	(array_remove(string_to_array(string_agg(ds.vomitting, ','), ','), ''))[1] as vomitting,
        (array_remove(string_to_array(string_agg(ds.visit_vital_sign_note, ','), ','), ''))[1] as note,
	(array_remove(string_to_array(string_agg(ds.t_visit_vital_sign_id, ','), ','), ''))[1] as latest_vs_id ,
	(array_remove(string_to_array(string_agg(ds.visit_vital_sign_staff_record, ','), ','), ''))[1] as latest_recoder_id ,
        (array_remove(string_to_array(string_agg(ds.recoder, ','), ','), ''))[1] as latest_recoder ,
	text_to_timestampz((array_remove(string_to_array(string_agg(ds.check_at, '|'), '|'), ''))[1]) as latest_check_at,
	(array_remove(string_to_array(string_agg(ds.newspews_type, ','), ','), ''))[1] as newspews_type,
	(array_remove(string_to_array(string_agg(ds.newspews_score, ','), ','), ''))[1]::integer as newspews_score,
	text_to_timestampz((array_remove(string_to_array(string_agg(ds.next_at, '|'), '|'), ''))[1]) as next_at
from
	(
	select
		tvvs.*,
                fvnl.visit_nutrition_level_description as nutrition,
		fct.description as cardiovascular,
		fat.description as avpu,
		fbt.description as behavior,
		fa1.answer_description as received_nebulization,
		fa2.answer_description as vomitting,
		((
		case
			when fpp.f_patient_prefix_id is null
			or fpp.f_patient_prefix_id::text = '000'::text then ''::character varying
			else fpp.patient_prefix_description
		end::text || tp.person_firstname::text) || ' '::text) || tp.person_lastname::text as recoder,
		tvvs.visit_vital_sign_check_date || ',' || tvvs.visit_vital_sign_check_time as check_at,
		tnn.newspews_type::text as newspews_type,
		tnn.score::text as newspews_score,
		timestamp_to_text(tnn.next_timestamp) as next_at
	from
		t_visit_vital_sign tvvs
	inner join b_employee be on
		be.b_employee_id = tvvs.visit_vital_sign_staff_record
	inner join t_person tp on
		tp.t_person_id::text = be.t_person_id::text
	left join f_patient_prefix fpp on
		fpp.f_patient_prefix_id::text = tp.f_prefix_id::text
        left join f_visit_nutrition_level fvnl on
                fvnl.f_visit_nutrition_level_id = tvvs.f_visit_nutrition_level_id
	left join f_cardiovascular_type fct on
		fct.f_cardiovascular_type_id = tvvs.f_cardiovascular_type_id
	left join f_avpu_type fat on
		fat.f_avpu_type_id = tvvs.f_avpu_type_id
	left join f_behavior_type fbt on
		fbt.f_behavior_type_id = tvvs.f_behavior_type_id
	left join f_answer fa1 on
		fa1.f_answer_id = tvvs.f_received_nebulization_id
	left join f_answer fa2 on
		fa2.f_answer_id = tvvs.f_vomitting_id
    left join t_newspews_notify tnn on
        tnn.t_visit_vital_sign_id = tvvs.t_visit_vital_sign_id and tnn.status != 'CN'
	where
		tvvs.t_visit_id = visit_id
		and tvvs.visit_vital_sign_active = '1'
	order by
		text_to_timestampz( visit_vital_sign_check_date || ',' || visit_vital_sign_check_time ) desc ) as ds

$function$
;

-- create new because drop function latest_vitalsign(text) CASCADE ;
CREATE OR REPLACE VIEW public.visit_medical_certificate_view
AS select 
t_visit.t_visit_id as t_visit_id
, t_patient.patient_hn as hn
, t_visit.visit_vn as vn
, case when f_patient_prefix.f_patient_prefix_id = null then '' else f_patient_prefix.patient_prefix_description end ||
t_patient.patient_firstname || ' '  ||t_patient.patient_lastname as name
, case when t_person_foreigner.t_person_foreigner_id = null 
then t_health_family.patient_firstname_eng || ' '  ||t_health_family.patient_lastname_eng 
else t_person_foreigner.passport_fname || ' '  ||t_person_foreigner.passport_lname
end as name_eng
, t_patient.patient_pid as pid
, t_person_foreigner.passport_no as passport
, text_to_timestamp(t_patient.patient_birthday)::date as dob
, (case when t_patient.patient_house is null or t_patient.patient_house = '' then '' else 'เลขที่ ' || t_patient.patient_house end) || ' ' ||
(case when t_patient.patient_moo is null or t_patient.patient_moo = '' then '' else 'หมู่ที่ ' || t_patient.patient_moo end) || ' ' ||
(case when t_patient.patient_road is null or t_patient.patient_road = '' then '' else 'ถนน' || t_patient.patient_road end) || ' ' ||
(case when tambol.f_address_id is null then '' else 'ตำบล/แขวง ' || tambol.address_description end) || ' ' ||
(case when amphur.f_address_id is null then '' else 'อำเภอ/เขต ' || amphur.address_description end) || ' ' ||
(case when changwat.f_address_id is null then '' else 'จังหวัด' || changwat.address_description end) || ' ' ||
(case when t_patient.patient_postcode is null or t_patient.patient_postcode = '' then '' else 'รหัสไปรษณีย์ ' || t_patient.patient_postcode end) as address
, t_patient.patient_phone_number as phone
, t_patient.patient_patient_mobile_phone as mobile
, f_patient_nation.patient_nation_description as nationality
, f_patient_occupation.patient_occupation_description as occupation
, t_health_family.skin_color
, vs.weight
, vs.height
, vs.bp
, vs.pulse
, array_to_string(
 array(select t_patient_personal_disease.patient_personal_disease_description from t_patient_personal_disease where t_patient_personal_disease.t_patient_id = t_patient.t_patient_id)
, ',') as disease
, t_person_foreigner.employer_name
, (case when t_person_foreigner.employer_address_house is null or t_person_foreigner.employer_address_house = '' then '' else 'เลขที่ ' || t_person_foreigner.employer_address_house end) || ' ' ||
(case when t_person_foreigner.employer_address_moo is null or t_person_foreigner.employer_address_moo = '' then '' else 'หมู่ที่ ' || t_person_foreigner.employer_address_moo end) || ' ' ||
(case when t_person_foreigner.employer_address_road is null or t_person_foreigner.employer_address_road = '' then '' else 'ถนน' || t_person_foreigner.employer_address_road end) || ' ' ||
(case when employer_tambol.f_address_id is null then '' else 'ตำบล/แขวง ' || employer_tambol.address_description end) || ' ' ||
(case when employer_amphur.f_address_id is null then '' else 'อำเภอ/เขต ' || employer_amphur.address_description end) || ' ' ||
(case when employer_changwat.f_address_id is null then '' else 'จังหวัด' || employer_changwat.address_description end) || ' ' ||
(case when t_person_foreigner.employer_address_postcode = null or t_person_foreigner.employer_address_postcode = '' then '' else 'รหัสไปรษณีย์ ' || t_person_foreigner.employer_address_postcode end) as employer_address
, t_person_foreigner.employer_contact_phone_number as employer_phone
, t_person_foreigner.employer_contact_mobile_phone_number as employer_mobile
from t_visit
inner join t_patient on t_patient.t_patient_id = t_visit.t_patient_id
inner join t_health_family on t_health_family.t_health_family_id = t_patient.t_health_family_id
left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_patient.f_patient_prefix_id
left join f_address as tambol on tambol.f_address_id = t_patient.patient_tambon
left join f_address as amphur on amphur.f_address_id = t_patient.patient_amphur
left join f_address as changwat on changwat.f_address_id = t_patient.patient_changwat
left join f_patient_nation on f_patient_nation.f_patient_nation_id = t_patient.f_patient_nation_id
left join f_patient_occupation on f_patient_occupation.f_patient_occupation_id = t_patient.f_patient_occupation_id
left join t_person_foreigner on t_person_foreigner.t_person_id = t_patient.t_person_id
left join f_address as employer_tambol on employer_tambol.f_address_id = t_person_foreigner.employer_address_tambon
left join f_address as employer_amphur on employer_amphur.f_address_id = t_person_foreigner.employer_address_amphur
left join f_address as employer_changwat on employer_changwat.f_address_id = t_person_foreigner.employer_address_changwat
left join latest_vitalsign(t_visit.t_visit_id) as vs on vs.t_visit_id = t_visit.t_visit_id;

-- update db version
INSERT INTO s_version VALUES ('9701000000102', '102', 'Hospital OS, Community Edition', '3.9.65b03', '3.45.1', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_65_b03.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.65b03');

-- require when use other postgres user
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA public to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA thai_ascvd to hospitalos;
-- GRANT ALL ON all sequences in schema thai_ascvd to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA rp50 to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA rp50nhso to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA rp50data to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA health_insurance to hospitalos;
-- GRANT ALL ON all sequences in schema public to hospitalos; 
-- GRANT ALL ON all sequences in schema rp50 to hospitalos; 
-- GRANT ALL ON all sequences in schema rp50nhso to hospitalos; 
-- GRANT ALL ON all sequences in schema rp50data to hospitalos; 
-- GRANT ALL ON all sequences in schema health_insurance to hospitalos;
-- GRANT ALL ON all sequences in schema thai_ascvd to hospitalos;
-- GRANT USAGE ON SCHEMA public TO hospitalos;
-- GRANT USAGE ON SCHEMA rp50 TO hospitalos;
-- GRANT USAGE ON SCHEMA rp50nhso TO hospitalos;
-- GRANT USAGE ON SCHEMA rp50data TO hospitalos;
-- GRANT USAGE ON SCHEMA health_insurance TO hospitalos;
-- GRANT USAGE ON SCHEMA thai_ascvd TO hospitalos;
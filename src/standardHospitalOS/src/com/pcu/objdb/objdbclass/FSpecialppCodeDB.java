/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.objdb.objdbclass;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.pcu.object.FSpecialppCode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sompr
 */
public class FSpecialppCodeDB {

    private final ConnectionInf connectionInf;

    public FSpecialppCodeDB(ConnectionInf db) {
        connectionInf = db;
    }

    public FSpecialppCode select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_specialpp_code where f_specialpp_code_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<FSpecialppCode> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<FSpecialppCode> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_specialpp_code order by f_specialpp_code_id";
            preparedStatement = connectionInf.ePQuery(sql);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<FSpecialppCode> list(String... ids) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_specialpp_code where f_specialpp_code_id in (?) order by f_specialpp_code_id";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setArray(1, connectionInf.getConnection().createArrayOf("varchar", ids));
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<FSpecialppCode> listByKeyword(String keyword) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_specialpp_code where f_specialpp_code_id ilike ? or description ilike ? order by f_specialpp_code_id";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, "%" + (keyword == null || keyword.isEmpty() ? "" : keyword) + "%");
            preparedStatement.setString(2, "%" + (keyword == null || keyword.isEmpty() ? "" : keyword) + "%");
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<FSpecialppCode> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<FSpecialppCode> list = new ArrayList<FSpecialppCode>();
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()) {
            FSpecialppCode obj = new FSpecialppCode();
            obj.setObjectId(rs.getString("f_specialpp_code_id"));
            obj.description = rs.getString("description");
            list.add(obj);
        }
        rs.close();
        return list;
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (FSpecialppCode obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }

    public List<CommonInf> getComboboxDatasource(String... ids) throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (FSpecialppCode obj : list(ids)) {
            list.add((CommonInf) obj);
        }
        return list;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.MapDrugUnit;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.Gutil;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class MapDrugUnitDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "807";

    public MapDrugUnitDB(ConnectionInf theConnectionInf) {
        this.connectionInf = theConnectionInf;
    }

    public int insert(MapDrugUnit obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO b_map_rp4356_drug_unit(\n");
            sql.append(
                    "            b_map_rp4356_drug_unit_id, b_item_drug_uom_id, r_rp4356_drug_unit_id)\n");
            sql.append(
                    "    VALUES (?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getGenID(tableId));
            preparedStatement.setString(2, obj.b_item_drug_uom_id);
            preparedStatement.setString(3, obj.r_rp4356_drug_unit_id);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(MapDrugUnit obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE b_map_rp4356_drug_unit\n");
            sql.append(
                    "   SET r_rp4356_drug_unit_id=?, \n");
            sql.append(
                    "       b_item_drug_uom_id=?\n");
            sql.append(" WHERE b_map_rp4356_drug_unit_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.r_rp4356_drug_unit_id);
            preparedStatement.setString(2, obj.b_item_drug_uom_id);
            preparedStatement.setString(3, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "delete from b_map_rp4356_drug_unit where b_map_rp4356_drug_unit_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            // execute delete SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public MapDrugUnit select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_map_rp4356_drug_unit where b_map_rp4356_drug_unit_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<MapDrugUnit> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<MapDrugUnit> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<MapDrugUnit> list = new ArrayList<MapDrugUnit>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                MapDrugUnit obj = new MapDrugUnit();
                obj.setObjectId(rs.getString("b_map_rp4356_drug_unit_id"));
                obj.b_item_drug_uom_id = rs.getString("b_item_drug_uom_id");
                obj.r_rp4356_drug_unit_id = rs.getString("r_rp4356_drug_unit_id");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<Object[]> listMapData(String keyword, boolean onlyUnmap) throws Exception {
        String sql = "select \n"
                + "b_item_drug_uom.b_item_drug_uom_id as uomid\n"
                + ",b_item_drug_uom. item_drug_uom_description || ' (' || b_item_drug_uom.item_drug_uom_number || ')'  as uomdesc\n"
                + ",r_rp4356_drug_unit.drug_unit_description as stddesc\n"
                + ",b_map_rp4356_drug_unit.b_map_rp4356_drug_unit_id as stdid\n"
                + "from b_item_drug_uom\n"
                + "left join b_map_rp4356_drug_unit on b_map_rp4356_drug_unit.b_item_drug_uom_id = b_item_drug_uom.b_item_drug_uom_id\n"
                + "left join r_rp4356_drug_unit on r_rp4356_drug_unit.r_rp4356_drug_unit_id = b_map_rp4356_drug_unit.r_rp4356_drug_unit_id\n"
                + "where b_item_drug_uom.item_drug_uom_active = '1'\n";
        if (keyword != null && !keyword.isEmpty()) {
            sql += "and (\n"
                    + "    upper(b_item_drug_uom.item_drug_uom_number) like UPPER('%s')\n"
                    + "    or upper(b_item_drug_uom.item_drug_uom_description) like UPPER('%s')\n"
                    + ")\n";
        }
        if (onlyUnmap) {
            sql += "and (\n"
                    + "    b_item_drug_uom.b_item_drug_uom_id  not in (select b_map_rp4356_drug_unit.b_item_drug_uom_id from b_map_rp4356_drug_unit)\n"
                    + ")";
        }
        sql += " order by b_item_drug_uom. item_drug_uom_description";
        List<Object[]> list = connectionInf.eComplexQuery(keyword != null && !keyword.isEmpty()
                ? String.format(sql, "%" + Gutil.CheckReservedWords(keyword) + "%", "%" + Gutil.CheckReservedWords(keyword) + "%")
                : sql);
        return list;
    }
}

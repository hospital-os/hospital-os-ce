/*
 * CheckBoxRenderer.java
 *
 * Created on 22 ����Ҥ� 2547, 20:01 �.
 */
package com.hospital_os.utility;

import java.awt.Component;
import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author tong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class CellRendererCheckBox extends JCheckBox implements TableCellRenderer {

    private static final long serialVersionUID = 1L;

    public CellRendererCheckBox() {
        setHorizontalAlignment(CellRendererCheckBox.CENTER);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value,
            boolean isSelected, boolean hasFocus, int row, int column) {
        if (isSelected) {
            this.setBackground(table.getSelectionBackground());
            this.setForeground(table.getSelectionForeground());
        } else {
            this.setBackground(table.getBackground());
            this.setForeground(table.getForeground());
        }
        try {
            if (value instanceof Boolean) {
                setSelected(((Boolean) value).booleanValue());
            }
            if (value instanceof Hashtable) {
                Hashtable ht_status = (Hashtable) value;
                setSelected(((Boolean) ht_status.get("request")).booleanValue());
                setEnabled(((Boolean) ht_status.get("status")).booleanValue());
            } else if (value instanceof String) {
                String tmp = String.valueOf(value);
                if (tmp.equals("1")) {
                    setSelected(true);
                } else {
                    setSelected(false);
                }
            } else if (value instanceof Boolean) {
                Boolean b = (Boolean) value;
                setSelected(b);
            }
            return this;
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
            return this;
        }
    }
    private static final Logger LOG = Logger.getLogger(CellRendererCheckBox.class.getName());
}

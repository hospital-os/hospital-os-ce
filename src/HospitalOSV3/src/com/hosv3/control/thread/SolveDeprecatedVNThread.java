/*
 * SolveDeprecatedVNThread.java
 *
 * Created on 16 �á�Ҥ� 2550, 14:22 �.
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package com.hosv3.control.thread;

import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hosv3.control.HosControl;
import com.hosv3.control.HosDB;
import com.hosv3.object.HosObject;
import com.hosv3.utility.ResourceBundle;
import java.sql.SQLException;
import java.util.logging.Logger;

/**
 *
 * @author Aut
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class SolveDeprecatedVNThread extends ControlThread {

    /**
     * Creates a new instance of SolveDeprecatedVNThread
     */
    public SolveDeprecatedVNThread() {
        this.setDaemon(true);
    }

    @Override
    public void setControl(ConnectionInf con, HosDB hdb, HosObject ho, UpdateStatus us, Object control) {
        theConnectionInf = con;
        theHosDB = hdb;
        theUS = us;
        theHO = ho;
    }

    @Override
    protected void runTask() {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            java.sql.ResultSet rs = theConnectionInf.eQuery(
                    "select * from "
                    + "(select visit_vn,count(visit_vn) as vn_qty "
                    + " from t_visit where f_visit_type_id = '0' group by visit_vn) as q1 "
                    + "where vn_qty > 1");
            java.util.Vector vn_vector = new java.util.Vector();
            while (rs.next()) {
                vn_vector.add(rs.getString(1));
            }
            for (int i = 0; i < vn_vector.size(); i++) {
                String vn_value_old = (String) vn_vector.get(i);
                String vn_value = theHosDB.theSequenceDataDB.updateSequence("vn", true);
                int ret = theConnectionInf.eUpdate("update t_visit set"
                        + " visit_notice = 'The original VN is " + vn_value_old + ", because this number duplicate'"
                        + ", visit_vn = '" + vn_value + "' where t_visit_id = "
                        + "(select t_visit_id from t_visit where visit_vn like '" + vn_value_old + "' limit 1)");
//                theUS.setStatus("value " + i + " : " + ret, UpdateStatus.WARNING);
            }
            theConnectionInf.getConnection().commit();
            theUS.confirmBox(ResourceBundle.getBundleText("com.hosv3.control.SystemControl.RESOLVE.DUPLICATE.VN") + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.UNKONW, ResourceBundle.getBundleText("com.hosv3.control.SystemControl.RESOLVE.DUPLICATE.VN")); 
            Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
    }
}
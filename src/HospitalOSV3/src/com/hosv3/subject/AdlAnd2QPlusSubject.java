/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.subject;

import com.hosv3.usecase.transaction.ManagerPatientHistoryResp;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sompr
 */
public class AdlAnd2QPlusSubject implements ManagerPatientHistoryResp {

    private final List<ManagerPatientHistoryResp> list = new ArrayList<>();

    public void removeAttach() {
        list.clear();
    }

    public void attach(ManagerPatientHistoryResp o) {
        list.add(o);
    }

    @Override
    public void notifySaveAdlAnd2QPlus(boolean isNew, String str) {
        for (ManagerPatientHistoryResp managerAdlAnd2QPlusResp : list) {
            managerAdlAnd2QPlusResp.notifySaveAdlAnd2QPlus(isNew, str);
        }
    }

}

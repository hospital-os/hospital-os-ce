/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
public class CigaretteFeq extends Persistent implements CommonInf {

    public String desc = "";
    public String active = "1";

    @Override
    public String getCode() {
        return getObjectId();
    }

    @Override
    public String getName() {
        return desc;
    }

    @Override
    public String toString() {
        return desc;
    }
}

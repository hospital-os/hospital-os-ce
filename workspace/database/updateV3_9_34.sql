-- บันทึกข้อความ
insert into f_gui_action (f_gui_action_id, gui_action_name) VALUES ('5806', 'สิทธิ์ลบข้อความ');

CREATE TABLE b_map_user_delete_notify (
    b_map_user_delete_notify_id    character varying(255)   NOT NULL,
    b_employee_id           character varying(255)   NOT NULL,
CONSTRAINT b_map_user_delete_notify_pkey PRIMARY KEY (b_map_user_delete_notify_id)
);
CREATE UNIQUE INDEX b_map_user_delete_notify_index1
   ON b_map_user_delete_notify (b_employee_id ASC NULLS LAST);

ALTER TABLE t_notify_note ADD COLUMN show_with_custom character varying(1) not null default '0';
ALTER TABLE t_notify_note ADD COLUMN custom_viewer text;

-- nutition Ref. http://bit.ly/16NJfrr
update f_visit_nutrition_level set visit_nutrition_level_max = '', visit_nutrition_level_min = '25' where f_visit_nutrition_level_id = '08';
update f_visit_nutrition_level set visit_nutrition_level_max = '24.99', visit_nutrition_level_min = '23' where f_visit_nutrition_level_id = '09';
update f_visit_nutrition_level set visit_nutrition_level_max = '22.99', visit_nutrition_level_min = '18.5' where f_visit_nutrition_level_id = '10';
update f_visit_nutrition_level set visit_nutrition_level_max = '', visit_nutrition_level_min = '' where f_visit_nutrition_level_id = '11';
update f_visit_nutrition_level set visit_nutrition_level_max = '18.49', visit_nutrition_level_min = '' where f_visit_nutrition_level_id = '12';

-- for e-claim
delete from r_rp1853_instype;
insert into r_rp1853_instype values ('01','สิทธิประกันสังคม','','SSS');
insert into r_rp1853_instype values ('03','สถานะคนไทยในต่างประเทศ','','FRG');
insert into r_rp1853_instype values ('04','สิทธิประกันสังคมและสิทธิข้าราชการ/สิทธิรัฐวิสาหกิจ','','SOF');
insert into r_rp1853_instype values ('05','สิทธิข้าราชการการเมือง/นักการเมือง','','BFC');
insert into r_rp1853_instype values ('06','ไม่มีสิทธิใดๆ','','');
insert into r_rp1853_instype values ('07','สถานะคนต่างด้าว','','NRD');
insert into r_rp1853_instype values ('08','สิทธิครูเอกชน/สิทธิข้าราชการการเมือง','','PBF');
insert into r_rp1853_instype values ('09','สิทธิทหารผ่านศึก','','VET');
insert into r_rp1853_instype values ('10','สิทธิประกันสังคม/สิทธิครูเอกชน/สิทธิข้าราชการ','','PSO');
insert into r_rp1853_instype values ('11','สิทธิทหารผ่านศึก/สิทธิข้าราชการ','','VOF');
insert into r_rp1853_instype values ('12','สิทธิประกันสังคม/สิทธิทหารผ่านศึก','','VSS');
insert into r_rp1853_instype values ('13','สิทธิทหารผ่านศึก/สิทธิข้าราชการการเมือง','','VBF');
insert into r_rp1853_instype values ('14','สิทธิประกันสังคม/สิทธิทหารผ่านศึก/สิทธิข้าราชการ','','VSO');
insert into r_rp1853_instype values ('15','สิทธิประกันสังคม/สิทธิทหารผ่านศึก/สิทธิข้าราชการการเมือง','','VSB');
insert into r_rp1853_instype values ('16','สิทธิครูเอกชน','','PVT');
insert into r_rp1853_instype values ('17','สิทธิประกันสังคม/สิทธิครูเอกชน','','PSS');
insert into r_rp1853_instype values ('18','สิทธิครูเอกชน/สิทธิข้าราชการ','','POF');
insert into r_rp1853_instype values ('19','สิทธิประกันสังคม/สิทธิครูเอกชน/สิทธิข้าราชการการเมือง','','PSB');
insert into r_rp1853_instype values ('20','สิทธิครูเอกชน/สิทธิทหารผ่านศึก','','VPT');
insert into r_rp1853_instype values ('21','สิทธิครูเอกชน/สิทธิประกันสังคม/สิทธิทหารผ่านศึก','','VPS');
insert into r_rp1853_instype values ('22','สิทธิครูเอกชน/สิทธิทหารผ่านศึก/สิทธิข้าราชการ','','VPO');
insert into r_rp1853_instype values ('23','สิทธิครูเอกชน/สิทธิทหารผ่านศึก/สิทธิข้าราชการการเมือง','','VPB');
insert into r_rp1853_instype values ('24','สิทธิประกันสังคมและสิทธิข้าราชการการเมือง/นักการเมือง','','SBF');
insert into r_rp1853_instype values ('25','สิทธิประกันสังคมกรณีทุพลภาพ','','SSI');
insert into r_rp1853_instype values ('26','สิทธิประกันสังคมทุพลภาพและสิทธิข้าราชการ/สิทธิรัฐวิสาหกิจ','','SIF');
insert into r_rp1853_instype values ('27','สิทธิครูเอกชน/สิทธิประกันสังคมแบบทุพพลภาพ','','PSI');
insert into r_rp1853_instype values ('28','สิทธิประกันสังคมแบบทุพพลภาพ/สิทธิทหารผ่านศึก/สิทธิข้าราชการ','','VIO');
insert into r_rp1853_instype values ('29','สิทธิประกันสังคมแบบทุพพลภาพ/สิทธิทหารผ่านศึก','','VSI');
insert into r_rp1853_instype values ('60','อาสาสมัครมาเลเรีย','ตามวันหมดอายุของบัตรประจำตัว','WEL');
insert into r_rp1853_instype values ('61','บุคคลในครอบครัวของอาสาสมัครมาเลเรีย','ตามวันหมดอายุของเจ้าของบัตรประจำตัว','WEL');
insert into r_rp1853_instype values ('62','ช่างสุขภัณฑ์หมู่บ้าน','ตามวันหมดอายุของบัตรประจำตัว','WEL');
insert into r_rp1853_instype values ('63','บุคคลในครอบครัวของช่างสุขภัณฑ์หมู่บ้าน','ตามวันหมดอายุของเจ้าของบัตรประจำตัว','WEL');
insert into r_rp1853_instype values ('64','ผู้บริหารโรงเรียนและครูของโรงเรียนเอกชนที่สอนศาสนาอิสลาม','ตามวาระที่รับมอบหมาย','WEL');
insert into r_rp1853_instype values ('65','บุคคลในครอบครัวของผู้บริหารโรงเรียนและครูของโรงเรียนเอกชนที่สอนศาสนาอิสลาม','ตามวันหมดอายุของเจ้าของบัตรประจำตัว','WEL');
insert into r_rp1853_instype values ('66','ผู้ได้รับพระราชทานเหรียญราชการชายแดน','Noexp','WEL');
insert into r_rp1853_instype values ('67','ผู้ได้รับพระราชทานเหรียญพิทักษ์เสรีชน','Noexp','WEL');
insert into r_rp1853_instype values ('68','สมาชิกผู้บริจาคโลหิตของสภากาชาดไทย ซึ่งบริจาคโลหิตตั้งแต่ 18 ครั้ง ขึ้นไป','Noexp','WEL');
insert into r_rp1853_instype values ('69','หมออาสาหมู่บ้านตามโครงการของกระทรวงกลาโหม','Noexp','WEL');
insert into r_rp1853_instype values ('70','อาสาสมัครคุมประพฤ กระทรวงยุติธรรม','ตามวาระที่ได้รับมอบหมาย','WEL');
insert into r_rp1853_instype values ('71','เด็กอายุไม่เกิน 12 ปีบริบูรณ์','ณ วันที่ ครบ 12 ปีบริบูรณ์','WEL');
insert into r_rp1853_instype values ('72','ผู้มีรายได้น้อย','3 ปี ','WEL');
insert into r_rp1853_instype values ('73','นักเรียนมัธยมศึกษาตอนต้น','3 ปี ','WEL');
insert into r_rp1853_instype values ('74','บุคคลผู้พิการ','Noexp','WEL');
insert into r_rp1853_instype values ('75','ทหารผ่านศึกชั้น 1-3 ที่มีบัตรทหารผ่านศึก รวมถึงผู้ได้รับพระราชทาน','Noexp','WEL');
insert into r_rp1853_instype values ('76','พระภิกษุ สามเณร และแม่ชีในพระพุทธศาสนาซึ่งหนังสือสุทธิรับรอง','ตามสถานะภาพที่ปรากฎ','WEL');
insert into r_rp1853_instype values ('77','ผู้มีอายุเกิน 60 ปีบริบูรณ์','Noexp','WEL');
insert into r_rp1853_instype values ('78','อื่น ๆ','1 ปี','WEL');
insert into r_rp1853_instype values ('79','ว่างงาน','1 ปี','WEL');
insert into r_rp1853_instype values ('80','บุคคลในครอบครัวทหารผ่านศึกชั้น 1-3 รวมถึงผู้ได้รับพระราชทานเหรียญสมรภูมิ','Noexp','WEL');
insert into r_rp1853_instype values ('81','ผู้นำชุมชน (กำนัน สารวัตรกำนัน ผู้ใหญ่บ้าน ผู้ช่วยผู้ใหญ่บ้านและแพทย์ประจำตำบล)','ตามวาระที่ได้รับมอบหมาย','WEL');
insert into r_rp1853_instype values ('82','อาสาสมัครสาธารณสุขประจำหมู่บ้าน (อสม.) อาสาสมัครสาธารณสุข','ตามวันหมดอายุของบัตรประจำตัว','WEL');
insert into r_rp1853_instype values ('83','ผู้นำศาสนาอิสลาม ( อิหม่าม คอเต็บ บิหลั่น)','ตามวันหมดอายุของบัตรประจำตัว','WEL');
insert into r_rp1853_instype values ('84','บุคคลในครอบครัวของผู้นำศาสนาอิสลามของผู้นำศาสนาอิสลาม ( อิหม่าม คอเต็บ บิหลั่น)','ตามวันหมดอายุของเจ้าของบัตรประจำตัว','WEL');
insert into r_rp1853_instype values ('85','ผู้ได้รับพระราชทานเหรียญงานสงครามในทวีปยุโรป','ตามวันหมดอายุของบัตรประจำตัว','WEL');
insert into r_rp1853_instype values ('86','บุคคลในครอบครัวของผู้ได้รับพระราชทานเหรียญงานสงครามในทวีปยุโรป','ตามวันหมดอายุของเจ้าของบัตรประจำตัว','WEL');
insert into r_rp1853_instype values ('87','บุคคลในครอบครัวของผู้นำชุมชน (กำนัน สารวัตรกำนัน ผู้ใหญ่บ้าน ผู้ช่วยผู้ใหญ่บ้านและแพทย์ประจำตำบล)','ตามวันหมดอายุของเจ้าของบัตรประจำตัว','WEL');
insert into r_rp1853_instype values ('88','บุคคลในครอบครัวของอาสาสมัครสาธารณสุขประจำหมู่บ้าน (อสม.) อาสาสมัครสาธารณสุข','ตามวันหมดอายุของเจ้าของบัตรประจำตัว','WEL');
insert into r_rp1853_instype values ('89','ช่วงอายุ 12-59 ปี','ระหว่างช่วงอายุ 12 - 59 ปี','UCS');
insert into r_rp1853_instype values ('90','ทหารเกณฑ์','ตามวันที่ปลดประจำการ','WEL');
insert into r_rp1853_instype values ('91','ผู้ที่พำนักในสถานที่ภายใต้การดูแลของส่วนราชการ(ราชทัณฑ์)','ตามวันที่พ้นโทษ','WEL');
insert into r_rp1853_instype values ('92','ผู้ที่พำนักในสถานที่ภายใต้การดูแลของส่วนราชการ    (สถานพินิจและสถานสงเคราะห์)','ตามช่วงเวลาที่อยู่ในความดูแล','WEL');
insert into r_rp1853_instype values ('93','นักเรียนทหาร','ตามวันที่จบการศึกษา','WEL');
insert into r_rp1853_instype values ('94','ทหารผ่นศึกชั้น 4 ที่มีบัตรทหารผ่านศึก รวมถึงผู้ได้รับพระราชทาน','Noexp','WEL');
insert into r_rp1853_instype values ('95','บุคคลในครอบครัวทหารผ่านศึกชั้น 4 รวมถึงผู้ได้รับพระราชทานเหรียญสมรภูมิ','Noexp','WEL');
insert into r_rp1853_instype values ('96','ทหารพราน','ตามวันหมดอายุของบัตรประจำตัว','WEL');
insert into r_rp1853_instype values ('97','บุคคลในครอบครัวทหารของกรมสวัสดิการ 3 เหล่าทัพ','Noexp','WEL');
insert into r_rp1853_instype values ('98','บุคคลในครอบครัวทหารผ่านศึกนอกประจำการบัตรชั้นที่ 1','Noexp','WEL');
insert into r_rp1853_instype values ('B1','สิทธิเบิกกรุงเทพมหานคร(ข้าราชการ)','','OFC');
insert into r_rp1853_instype values ('B2','สิทธิเบิกกรุงเทพมหานคร(ลูกจ้างประจำ)','','OFC');
insert into r_rp1853_instype values ('B3','สิทธิเบิกกรุงเทพมหานคร(ผู้รับเบี้ยหวัดบำนาญ)','','OFC');
insert into r_rp1853_instype values ('B4','สิทธิเบิกกรุงเทพมหานคร(บุคคลในครอบครัว)','','OFC');
insert into r_rp1853_instype values ('B5','สิทธิเบิกกรุงเทพมหานคร(บุคคลในครอบครัวผู้รับเบี้ยหวัดบำนาญ)','','OFC');
insert into r_rp1853_instype values ('C1','สิทธิเบิกหน่วยงานตนเองหรือรัฐวิสาหกิจ(เจ้าหน้าที่)','','OFC');
insert into r_rp1853_instype values ('C2','สิทธิเบิกหน่วยงานตนเองหรือรัฐวิสาหกิจ(พนักงาน)','','OFC');
insert into r_rp1853_instype values ('C3','สิทธิเบิกหน่วยงานตนเองหรือรัฐวิสาหกิจ(ผู้รับเบี้ยหวัดบำนาญ)','','OFC');
insert into r_rp1853_instype values ('C4','สิทธิเบิกจากหน่วยงานตนเองหรือรัฐวิสาหกิจ(บุคคลในครอบครัว)','','OFC');
insert into r_rp1853_instype values ('C5','สิทธิเบิกจากหน่วยงานตนเองหรือรัฐวิสาหกิจ (บุคคลในครอบครัวผู้รับเบี้ยหวัดบำนาญ)','','OFC');
insert into r_rp1853_instype values ('C6','สิทธิเบิกจากหน่วยงานตนเองหรือรัฐวิสาหกิจ (กรณีได้รับสิทธิเฉพาะหน่วยงาน)','','OFC');
insert into r_rp1853_instype values ('L1','สิทธิสวัสดิการพนักงานส่วนท้องถิ่น(ข้าราชการ/พนักงาน/ลูกจ้างประจำ/ครูผู้ดูแลเด็ก/ครูผู้ช่วย)','','LGO');
insert into r_rp1853_instype values ('L2','สิทธิสวัสดิการพนักงานส่วนท้องถิ่น(บุคคลในครอบครัว)','','LGO');
insert into r_rp1853_instype values ('L3','สิทธิสวัสดิการพนักงานส่วนท้องถิ่น(ผู้รับเบี้ยหวัดบำนาญ)','','LGO');
insert into r_rp1853_instype values ('L4','สิทธิสวัสดิการพนักงานส่วนท้องถิ่น(บุคคลในครอบครัวผู้รับเบี้ยหวัดบำนาญ)','','LGO');
insert into r_rp1853_instype values ('L5','สิทธิสวัสดิการพนักงานส่วนท้องถิ่น(ข้าราชการการเมือง)','','LGO');
insert into r_rp1853_instype values ('L6','สิทธิสวัสดิการพนักงานส่วนท้องถิ่น(บุคคลในครอบครัวข้าราชการการเมือง)','','LGO');
insert into r_rp1853_instype values ('O1','สิทธิเบิกกรมบัญชีกลาง(ข้าราชการ)','','OFC');
insert into r_rp1853_instype values ('O2','สิทธิเบิกกรมบัญชีกลาง(ลูกจ้างประจำ)','','OFC');
insert into r_rp1853_instype values ('O3','สิทธิเบิกกรมบัญชีกลาง(ผู้รับเบี้ยหวัดบำนาญ)','','OFC');
insert into r_rp1853_instype values ('O4','สิทธิเบิกกรมบัญชีกลาง(บุคคลในครอบครัว)','','OFC');
insert into r_rp1853_instype values ('O5','สิทธิเบิกกรมบัญชีกลาง(บุคคลในครอบครัวผู้รับเบี้ยหวัดบำนาญ)','','OFC');
insert into r_rp1853_instype values ('S1','สิทธิเบิกกองทุนประกันสังคม(ผู้ประกันตน)','','SSS');
insert into r_rp1853_instype values ('S2','สิทธิเบิกกองทุนประกันสังคม(คู่สมรสผู้ประกันตน)','','SSS');
insert into r_rp1853_instype values ('S3','สิทธิเบิกกองทุนประกันสังคม(บุตรผู้ประกันตน)','','SSS');
insert into r_rp1853_instype values ('ST','สิทธิเบิกงานประกันสุขภาพกระทรวงสาธารณสุข','','STP');

-- update db version
INSERT INTO s_version VALUES ('9701000000067', '67', 'Hospital OS, Community Edition', '3.9.34', '3.21.140813', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_34.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.34');
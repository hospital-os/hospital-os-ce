/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.AccidentSysptomMovement;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class AccidentSysptomMovementDB {

    private final ConnectionInf connectionInf;

    public AccidentSysptomMovementDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public List<AccidentSysptomMovement> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_accident_symptom_movement";
            preparedStatement = connectionInf.ePQuery(sql);
            
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<AccidentSysptomMovement> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<AccidentSysptomMovement> list = new ArrayList<AccidentSysptomMovement>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                AccidentSysptomMovement obj = new AccidentSysptomMovement();
                obj.setObjectId(rs.getString("f_accident_symptom_movement_id"));
                obj.description = rs.getString("accident_symptom_movement_description");
                obj.accident_symptom_movement_score = rs.getString("accident_symptom_movement_score");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (AccidentSysptomMovement obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }

    public AccidentSysptomMovement select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_accident_symptom_movement where f_accident_symptom_movement_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<AccidentSysptomMovement> executeQuery = executeQuery(preparedStatement);
            return executeQuery.isEmpty() ? null : executeQuery.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
}

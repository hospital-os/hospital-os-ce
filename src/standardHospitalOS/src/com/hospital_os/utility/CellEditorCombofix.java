/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.utility;

import java.awt.Component;
import java.util.Vector;
import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

/**
 *
 * @author Somprasong Damyos <somprasong@hospital-os.com>
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class CellEditorCombofix extends AbstractCellEditor implements TableCellEditor {

    private static final long serialVersionUID = 1L;
    protected JComboBox comboBox;
    private final Vector<ComboFix> comboFixs;

    public CellEditorCombofix(Vector<ComboFix> comboFixs) {
        comboBox = new JComboBox();
        comboBox.setFont(comboBox.getFont());
        this.comboFixs = comboFixs;
        comboBox.addItemListener(new java.awt.event.ItemListener() {
            @Override
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                System.err.println("itemStateChanged");
                fireEditingStopped();
            }
        });

//        comboBox.addActionListener(new java.awt.event.ActionListener() {
//            @Override
//            public void actionPerformed(java.awt.event.ActionEvent evt) {
//                 System.err.println("actionPerformed");
//                fireEditingStopped();
//            }
//        });
        ComboboxModel.initComboBox(comboBox, comboFixs);
    }

    @Override
    public Object getCellEditorValue() {
        return comboFixs.get(comboBox.getSelectedIndex()).getCode();//ComboboxModel.getCodeComboBox(comboBox);//comboBox.getSelectedItem();
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        if (value instanceof ComboFix) {
            comboBox.setSelectedItem((ComboFix) value);
        } else if (value instanceof Integer) {
            comboBox.setSelectedIndex((Integer) value);
        } else if (value instanceof String) {
            Gutil.setGuiData(comboBox, (String) value);
        }

        return comboBox;
    }
}

/*
 * BalloonTextArea.java
 *
 * Created on 22 �չҤ� 2549, 16:43 �.
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
package com.hosv3.gui.component;

import com.hospital_os.usecase.connection.LookupControlInf;
import com.hosv3.usecase.transaction.ManageBalloon;
import com.hosv3.utility.connection.ExecuteControlInf;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Vector;
import javax.swing.JFrame;
import javax.swing.JTextArea;

/**
 *
 * @author henbe
 */
@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public class BalloonTextArea1 extends JTextArea implements KeyListener, ManageBalloon {

    boolean mode_active = true;
    static final long serialVersionUID = 0;
    Balloon balloon;
    BalloonPanel balloonpanel;
    JFrame jframe;
    LookupControlInf controle;
    com.hospital_os.usecase.connection.LookupControlInf hcontrole;
    ExecuteControlInf theEC;
    Vector vnew_search = new Vector();
    String after_select = "\n";
    String before_select = "";
    int old_length = 0;
    boolean checkrepeate = false;
    Vector vList = new Vector();
    Vector vNew = new Vector();

    /**
     * Creates a new instance of BalloonTextArea
     */
    @SuppressWarnings("LeakingThisInConstructor")
    public BalloonTextArea1() {
        addKeyListener(this);
        setLineWrap(true);
        vnew_search.add(", ");
        vnew_search.add("\n");
        vnew_search.add(" ");
    }

    @SuppressWarnings("LeakingThisInConstructor")
    public BalloonTextArea1(JFrame jf) {
        addKeyListener(this);
        setLineWrap(true);
        setJFrame(jf);
    }

    public void setActive(boolean b) {
        mode_active = b;
    }

    /**
     * ����ͤ����������Դ�������� ��������������鹤���������������������
     */
    public void setSelectAround(String before, String after) {
        after_select = after;
        before_select = before;
    }

    public void setNewSearch(String str) {
        vnew_search.add(str);
    }

    @Override
    public void keyPressed(java.awt.event.KeyEvent e) {
    }
    boolean found_lastsearch = true;
    int index = 0;

    @Override
    public void keyReleased(java.awt.event.KeyEvent e) {
        if (!mode_active) {
            return;
        }
        String str = this.getText();
        if (str.equals("") && balloon != null) {
            balloon.dispose();
        }

        if (e.getKeyCode() == KeyEvent.VK_DOWN || e.getKeyCode() == KeyEvent.VK_UP) {
            if (balloon != null) {
                index = 0;
                balloon.getTable().requestFocus();
                balloon.getTable().setRowSelectionInterval(0, 0);
            }
            return;
        } else if (e.isActionKey()
                || e.getKeyCode() == KeyEvent.VK_SPACE) {
            found_lastsearch = true;
            if (balloon != null) {
                balloon.dispose();
                balloon = null;
            }
            return;
        } else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
            if (balloon != null) {
                balloon.dispose();
                balloon = null;
            }
            return;
        } else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            found_lastsearch = true;
            setText(str);
            return;
        } else if (e.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
            found_lastsearch = true;
//            return;
        }
        //��Ǩ�ͺ����繡�á�͡�ҡ��ͧ��͡�������� �
        //ҡ���Ըա�� cut paste �����������͹�
        //�ҡ��͡Ẻ�������������ѡ�á���������
        if (old_length == str.length() || old_length + 5 < str.length()) {
            old_length = str.length();
            return;
        }
        old_length = str.length();
//        String[] astr = str.split(after_select);
        for (int i = 0; i < vnew_search.size(); i++) {
            String new_search = String.valueOf(vnew_search.get(i));
            if (index < str.lastIndexOf(new_search)) {
                found_lastsearch = true;
                index = str.lastIndexOf(new_search) + new_search.length();
            }
        }
        if (index > str.length()) {
            index = 0;
        }
        String search = str.substring(index);
        int length = search.length();
        if (length >= 2 && (controle != null || hcontrole != null)
                && found_lastsearch) {
            Vector itemSearch;
            if (controle != null) {
                itemSearch = controle.listData(search.trim());
            } else {
                itemSearch = hcontrole.listData(search.trim());
            }

            if (itemSearch == null || itemSearch.isEmpty()) {
                if (balloon != null) {
                    balloon.dispose();
                    balloon = null;
                }
                found_lastsearch = false;
                return;
            }
            if (balloon != null) {
                balloon.dispose();
                balloon = null;
            }
            balloon = new Balloon(jframe);
            balloon.setSize(300, 100);
            balloonpanel = new BalloonPanel();
            balloonpanel.setEControl(theEC);
            balloonpanel.setTable(itemSearch);
            balloonpanel.setSelectAround(before_select, after_select);
            balloonpanel.setBalloon(balloon);
            balloonpanel.setComponent(this, index);
            balloonpanel.setCheckRepeat(checkrepeate);
            balloon.setComponent(balloonpanel);
            balloon.setVisible(true);
            balloon.requestFocus();
        }
        this.requestFocus();
    }

    @Override
    public void keyTyped(java.awt.event.KeyEvent e) {
    }

    public void setJFrame(JFrame jf) {
        jframe = jf;
    }

    public void setControl(LookupControlInf c) {
        controle = c;
    }

    @Override
    public void notifyCloseBalloon(String str, int status) {
        if (balloon != null) {
            balloon.dispose();
            balloon = null;
        }
    }

    public void setEControl(ExecuteControlInf c) {
        theEC = c;
    }

    public static void showPopup() {
        JFrame jp = new JFrame();
        com.hosv3.control.HosControl hc = new com.hosv3.control.HosControl();
        com.hosv3.control.lookup.DxTemplateLookup vtl = new com.hosv3.control.lookup.DxTemplateLookup(hc.theLookupControl);
        BalloonTextArea pnl = new BalloonTextArea();
        pnl.setSelectAround("", "\n");
        pnl.setControl(vtl);
        pnl.setJFrame(jp);
        jp.getContentPane().add(pnl);
        jp.pack();
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension frameSize = jp.getSize();

        if (frameSize.height > screenSize.height) {
            frameSize.height = screenSize.height;
        }
        if (frameSize.width > screenSize.width) {
            frameSize.width = screenSize.width;
        }
        jp.setLocation((screenSize.width - frameSize.width) / 2, (screenSize.height - frameSize.height) / 2);
        jp.setVisible(true);
    }

    public void setCheckRepeate(boolean repeate) {
        checkrepeate = repeate;
    }
}

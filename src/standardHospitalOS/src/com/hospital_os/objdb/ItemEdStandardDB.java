/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.ItemEdStandard;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class ItemEdStandardDB {

    private final ConnectionInf theConnectionInf;

    public ItemEdStandardDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public ItemEdStandard selectById(String id) throws Exception {
        String sql = "select * from b_item_ed_standard where b_item_ed_standard_id = '%s'";
        List eQuery = eQuery(String.format(sql, id));
        return (ItemEdStandard) (eQuery.isEmpty() ? null : eQuery.get(0));
    }

    public List<ItemEdStandard> listAll() throws Exception {
        String sql = "select * from b_item_ed_standard order by generic_name";
        List eQuery = eQuery(sql);
        return eQuery;
    }

    public List eQuery(String sql) throws Exception {
        List list = new ArrayList();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        while (rs.next()) {
            ItemEdStandard p = new ItemEdStandard();
            p.setObjectId(rs.getString("b_item_ed_standard_id"));
            p.grcode1 = rs.getString("grcode1");
            p.name1 = rs.getString("name1");
            p.grcode2 = rs.getString("grcode2");
            p.name2 = rs.getString("name2");
            p.grcode3 = rs.getString("grcode3");
            p.name3 = rs.getString("name3");
            p.grcode4 = rs.getString("grcode4");
            p.name4 = rs.getString("name4");
            p.generic_name = rs.getString("generic_name");
            p.syn_name = rs.getString("syn_name");
            p.type = rs.getString("type");
            p.dosage = rs.getString("dosage");
            p.ed = rs.getString("ed");
            list.add(p);
        }
        rs.close();
        return list;
    }

    public List<ItemEdStandard> listByPId(String pid) throws Exception {
        String sql = "select b_item_ed_standard.* from b_item_ed_standard "
                + "where b_item_ed_standard_id not in "
                + "(select b_item_ed_standard_id from t_patient_drug_allergy where t_patient_drug_allergy.t_patient_id = '" + pid + "')"
                + "order by generic_name";
        List eQuery = eQuery(sql);
        return eQuery;
    }
}

-- เพิ่มเหตุผลการสั่ง lab
CREATE TABLE b_lab_order_cause (
    b_lab_order_cause_id  character varying(30)   NOT NULL,
    code                  character varying(2)   NOT NULL,
    description           character varying(255)   NOT NULL,
    active         boolean NOT NULL DEFAULT true,
    user_record_id varchar(255) NULL,
    record_date_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    user_update_id varchar(255) NULL,
    update_date_time timestamp NULL DEFAULT CURRENT_TIMESTAMP,
CONSTRAINT b_lab_order_cause_pkey PRIMARY KEY (b_lab_order_cause_id)
);
CREATE UNIQUE INDEX b_lab_order_cause_index1
   ON b_lab_order_cause (code ASC NULLS LAST);

INSERT INTO b_lab_order_cause VALUES ('1','1','คัดกรองด่านระหว่างประเทศ');
INSERT INTO b_lab_order_cause VALUES ('2','2','คัดกรอง PUI');
INSERT INTO b_lab_order_cause VALUES ('3','3','คัดกรองผู้สัมผัส (contact)');
INSERT INTO b_lab_order_cause VALUES ('4','4','คัดกรองผู้ถูกกักกัน');
INSERT INTO b_lab_order_cause VALUES ('5','5','ค้นหาผู้ป่วยเชิงรุก (Active case finding)');
INSERT INTO b_lab_order_cause VALUES ('6','6','คัดกรองกลุ่มเสี่ยง (sentinel surveillance)');
INSERT INTO b_lab_order_cause VALUES ('7','7','คัดกรองก่อนผ่าตัด (Pre Operation)');
INSERT INTO b_lab_order_cause VALUES ('8','8','ไม่เข้าเกณฑ์ (non PUI)');
INSERT INTO b_lab_order_cause VALUES ('9','9','Fit to Fly');
INSERT INTO b_lab_order_cause VALUES ('10','10','ตรวจยืนยัน (confirm)');
INSERT INTO b_lab_order_cause VALUES ('11','11','อื่น ๆ');
INSERT INTO b_lab_order_cause VALUES ('12','12','การตรวจหาสายพันธุ์ของเชื้อไวรัสโคโรนา 2019 (SARS-CoV-2)');
INSERT INTO b_lab_order_cause VALUES ('13','13','ตรวจยืนยัน ATK');


ALTER TABLE t_order ADD COLUMN b_lab_order_cause_id character varying(30) NULL default null; 
CREATE INDEX t_order_lab_order_cause_id
   ON t_order (b_lab_order_cause_id ASC NULLS LAST);

-- query report
ALTER TABLE public.r_sql_template ADD sql_template_is_query_by_plan varchar(1) NOT NULL DEFAULT '0';
CREATE TABLE r_sql_template_auth_groups (
r_sql_template_id                             character varying(50),
f_employee_authentication_id	character varying(50) NOT NULL,
record_datetime       		timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP ,
user_record_id        		character varying(50) NOT NULL,
CONSTRAINT r_sql_template_auth_groups_pk PRIMARY KEY (r_sql_template_id, f_employee_authentication_id),
CONSTRAINT r_sql_template_auth_groups_fk1 FOREIGN KEY (r_sql_template_id)
REFERENCES r_sql_template (r_sql_template_id) MATCH SIMPLE
ON UPDATE NO ACTION ON DELETE NO ACTION,
CONSTRAINT r_sql_template_auth_groups_fk2 FOREIGN KEY (f_employee_authentication_id)
REFERENCES f_employee_authentication (f_employee_authentication_id) MATCH SIMPLE
ON UPDATE NO ACTION ON DELETE NO ACTION
);
CREATE TABLE r_sql_template_auth_users (
r_sql_template_id                             character varying(50),
b_employee_id	      		character varying(50) NOT NULL,
record_datetime       		timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP ,
user_record_id        		character varying(255) NOT NULL,
CONSTRAINT r_sql_template_auth_users_pk PRIMARY KEY (r_sql_template_id, b_employee_id),
CONSTRAINT r_sql_template_auth_users_fk1 FOREIGN KEY (r_sql_template_id)
REFERENCES r_sql_template (r_sql_template_id) MATCH SIMPLE
ON UPDATE NO ACTION ON DELETE NO ACTION,
CONSTRAINT r_sql_template_auth_users_fk2 FOREIGN KEY (b_employee_id)
REFERENCES b_employee (b_employee_id) MATCH SIMPLE
ON UPDATE NO ACTION ON DELETE NO ACTION
);

-- update db version
INSERT INTO s_version VALUES ('9701000000104', '104', 'Hospital OS, Community Edition', '3.9.66', '3.46.0', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_66.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.66b01');

-- require when use other postgres user
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA public to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA thai_ascvd to hospitalos;
-- GRANT ALL ON all sequences in schema thai_ascvd to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA rp50 to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA rp50nhso to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA rp50data to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA health_insurance to hospitalos;
-- GRANT ALL ON all sequences in schema public to hospitalos; 
-- GRANT ALL ON all sequences in schema rp50 to hospitalos; 
-- GRANT ALL ON all sequences in schema rp50nhso to hospitalos; 
-- GRANT ALL ON all sequences in schema rp50data to hospitalos; 
-- GRANT ALL ON all sequences in schema health_insurance to hospitalos;
-- GRANT ALL ON all sequences in schema thai_ascvd to hospitalos;
-- GRANT USAGE ON SCHEMA public TO hospitalos;
-- GRANT USAGE ON SCHEMA rp50 TO hospitalos;
-- GRANT USAGE ON SCHEMA rp50nhso TO hospitalos;
-- GRANT USAGE ON SCHEMA rp50data TO hospitalos;
-- GRANT USAGE ON SCHEMA health_insurance TO hospitalos;
-- GRANT USAGE ON SCHEMA thai_ascvd TO hospitalos;
-- #527
-- เพิ่มตารางเก็บข้อมูลรายการในชุด กรณีใช้ระบบคลัง  
CREATE TABLE IF NOT EXISTS b_hstock_item_package (   
    b_hstock_item_package_id    CHARACTER VARYING(50) NOT NULL,
    b_item_id                   CHARACTER VARYING(50) NOT NULL, --ชุดรายการ
    b_item_sub_id               CHARACTER VARYING(50) NOT NULL, --รายการในชุด
    b_hstock_item_id            CHARACTER VARYING(50) DEFAULT NULL, --รายการในคลัง
    item_seq                    INTEGER NOT NULL DEFAULT 0, --ลำดับของข้อมูล
    item_qty_purch              FLOAT NOT NULL DEFAULT 1, --จำนวนที่จ่าย
    CONSTRAINT b_hstock_item_package_pk PRIMARY KEY (b_hstock_item_package_id),
    CONSTRAINT b_item_id_fk FOREIGN KEY (b_item_id) 
        REFERENCES b_item (b_item_id) ON UPDATE NO ACTION ON DELETE NO ACTION,  
    CONSTRAINT b_item_sub_id_fk FOREIGN KEY (b_item_sub_id) 
        REFERENCES b_item (b_item_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT b_hstock_item_id_fk FOREIGN KEY (b_hstock_item_id) 
        REFERENCES b_hstock_item (b_hstock_item_id) ON UPDATE NO ACTION ON DELETE NO ACTION
);

INSERT INTO s_stock_version (version_app, version_db, description)VALUES ('2.0.3', '2.0.4', 'Add Req Stock Module for Hos v4.0.2');
INSERT INTO s_script_update_log values ('Stock_Module','update_stock_005.sql',(select current_date) || ','|| (select current_time),'Add Req Stock Module for Hos v4.0.2');

/*
 * Nutrition.java
 *
 * Created on 23 �Զع�¹ 2548, 9:48 �.
 */
package com.pcu.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Administrator
 */
@SuppressWarnings("ClassWithoutLogger")
public class Nutrition extends Persistent {

    private static final long serialVersionUID = 1L;
    private static String init = "";
    public String patient_id = init;
    public String nutrition_vn = init;
    public String office_id = init;
    public String visit_id = init;
    public String nutrition_hn = init;
    public String nutrition_age = init;
    public String nutrition_rim = init;
    public String nutrition_weight = init;
    public String nutrition_high = init;
    public String nutrition_newtooth = init;
    public String nutrition_badtooth = init;
    public String answer_id = init;
    public String nutrition_level_id = init;
    public String nutrition_bmi = init;
    public String nutrition_result = init;
    public String nutrition_notice = init;
    public String nutrition_staff_record = init;
    public String nutrition_staff_modify = init;
    public String nutrition_staff_cancel = init;
    public String record_date_time = init;
    public String modify_date_time = init;
    public String cancel_date_time = init;
    public String active = init;
    public String family_id = init;
    public String survey_date = init;
    public String f_ha_level_id = init;
    public String f_hw_level_id = init;
    public String nutri_first_check = init;
    public String age_weight = init;
    public String food = init;
    public String bottle = init;
}

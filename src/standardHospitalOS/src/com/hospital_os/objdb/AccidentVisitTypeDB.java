/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.AccidentVisitType;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class AccidentVisitTypeDB {

    private final ConnectionInf connectionInf;

    public AccidentVisitTypeDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public List<AccidentVisitType> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_accident_visit_type";
            preparedStatement = connectionInf.ePQuery(sql);
            
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<AccidentVisitType> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<AccidentVisitType> list = new ArrayList<AccidentVisitType>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                AccidentVisitType obj = new AccidentVisitType();
                obj.setObjectId(rs.getString("f_accident_visit_type_id"));
                obj.description = rs.getString("accident_visit_type_description");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (AccidentVisitType obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }
}

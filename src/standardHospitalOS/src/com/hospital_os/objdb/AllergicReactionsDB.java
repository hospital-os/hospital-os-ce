/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.AllergicReactions;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.Gutil;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.Vector;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class AllergicReactionsDB {

    private final ConnectionInf theConnectionInf;
    private final String tableId = "993";

    public AllergicReactionsDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public int insert(AllergicReactions obj) throws Exception {
        String sql = "INSERT INTO t_allergic_reactions( "
                + "t_allergic_reactions_id, t_patient_id, f_allergic_reactions_type_id, allergic_reactions_list,  "
                + "allergic_reactions_check, allergic_reactions_check_detail, user_record,  "
                + "record_date_time, user_modify, modify_date_time, active) "
                + "VALUES ('%s', '%s', '%s', '%s',  "
                + "'%s', '%s', '%s',  "
                + "'%s', '%s', '%s', '%s')";
        sql = String.format(sql, obj.getGenID(tableId), obj.t_patient_id, obj.f_allergic_reactions_type_id,
                Gutil.CheckReservedWords(obj.allergic_reactions_list),
                obj.allergic_reactions_check,
                Gutil.CheckReservedWords(obj.allergic_reactions_check_detail),
                obj.user_record, obj.record_date_time,
                obj.user_modify, obj.modify_date_time, "1");
        return theConnectionInf.eUpdate(sql);
    }

    public int update(AllergicReactions obj) throws Exception {
        String sql = "UPDATE t_allergic_reactions "
                + "SET f_allergic_reactions_type_id='%s', allergic_reactions_list='%s',  "
                + "allergic_reactions_check='%s', allergic_reactions_checker='%s', allergic_reactions_check_detail='%s',  "
                + "user_modify='%s', modify_date_time='%s', active ='%s' "
                + "WHERE t_allergic_reactions_id='%s' ";
        sql = String.format(sql, obj.f_allergic_reactions_type_id,
                Gutil.CheckReservedWords(obj.allergic_reactions_list),
                obj.allergic_reactions_check, obj.allergic_reactions_checker,
                Gutil.CheckReservedWords(obj.allergic_reactions_check_detail),
                obj.user_modify, obj.modify_date_time, obj.active, obj.getObjectId());
        return theConnectionInf.eUpdate(sql);
    }

    public AllergicReactions selectById(String id) throws Exception {
        String sql = "select * from t_allergic_reactions where t_allergic_reactions_id = '%s'";
        Vector eQuery = eQuery(String.format(sql, id));
        return (AllergicReactions) (eQuery.isEmpty() ? null : eQuery.get(0));
    }

    public Vector<AllergicReactions> selectByPId(String pid) throws Exception {
        String sql = "select * from t_allergic_reactions where t_patient_id = '%s' and active = '1' order by record_date_time desc";
        Vector eQuery = eQuery(String.format(sql, pid));
        return eQuery;
    }

    public Vector<AllergicReactions> selectUnCheckedByPId(String pid) throws Exception {
        String sql = "select * from t_allergic_reactions where t_patient_id = '%s' and (allergic_reactions_check = '0' or allergic_reactions_check = '2') and active = '1' order by record_date_time desc";
        Vector eQuery = eQuery(String.format(sql, pid));
        return eQuery;
    }

    /**
     *
     * @param id
     * @param typeId
     * @param status 1 = checked, 0 = unchecked and null is all status
     * @return
     * @throws Exception
     */
    public List<Object[]> listByPId(String id, String typeId, String... status) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select "
                    + "t_allergic_reactions.t_allergic_reactions_id "
                    + ",f_allergic_reactions_type.allergic_reactions_type_description "
                    + ",t_allergic_reactions.allergic_reactions_list "
                    + ",recorder.b_employee_id || ':' || prefix1.patient_prefix_description || person1.person_firstname || ' ' || person1.person_lastname as recoder "
                    + ",t_allergic_reactions.record_date_time "
                    + ",t_allergic_reactions.allergic_reactions_check "
                    + ",t_allergic_reactions.allergic_reactions_check_detail "
                    + ",prefix2.patient_prefix_description || person2.person_firstname || ' ' || person2.person_lastname as checker "
                    + "from t_allergic_reactions  "
                    + "inner join f_allergic_reactions_type on f_allergic_reactions_type.f_allergic_reactions_type_id = t_allergic_reactions.f_allergic_reactions_type_id "
                    + "inner join b_employee as recorder on recorder.b_employee_id = t_allergic_reactions.user_record \n"
                    + "inner join t_person as person1 on person1.t_person_id = recorder.t_person_id\n"
                    + "left join f_patient_prefix as prefix1 on prefix1.f_patient_prefix_id = person1.f_prefix_id\n"
                    + "left join b_employee as chker on chker.b_employee_id = t_allergic_reactions.allergic_reactions_checker \n"
                    + "left join t_person as person2 on person2.t_person_id = chker.t_person_id\n"
                    + "left join f_patient_prefix as prefix2 on prefix2.f_patient_prefix_id = person2.f_prefix_id\n"
                    + "where t_allergic_reactions.t_patient_id = ?  "
                    + "and f_allergic_reactions_type.f_allergic_reactions_type_id like ? "
                    + "and t_allergic_reactions.active = '1' ";
            if (status != null) {
                sql += "and allergic_reactions_check = any (?) \n";
            }
            sql += "order by record_date_time desc";

            preparedStatement = theConnectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, id);
            preparedStatement.setString(index++, typeId);
            if (status != null) {
                preparedStatement.setArray(index++, theConnectionInf.getConnection().createArrayOf("varchar", status));
            }
            return theConnectionInf.eComplexQuery(preparedStatement.toString());
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public Vector eQuery(String sql) throws Exception {
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        while (rs.next()) {
            AllergicReactions p = new AllergicReactions();
            p.setObjectId(rs.getString("t_allergic_reactions_id"));
            p.t_patient_id = rs.getString("t_patient_id");
            p.f_allergic_reactions_type_id = rs.getString("f_allergic_reactions_type_id");
            p.allergic_reactions_list = rs.getString("allergic_reactions_list");
            p.allergic_reactions_check = rs.getString("allergic_reactions_check");
            p.allergic_reactions_checker = rs.getString("allergic_reactions_checker");
            p.allergic_reactions_check_detail = rs.getString("allergic_reactions_check_detail");
            p.user_record = rs.getString("user_record");
            p.record_date_time = rs.getString("record_date_time");
            p.user_modify = rs.getString("user_modify");
            p.modify_date_time = rs.getString("modify_date_time");
            p.active = rs.getString("active");
            list.add(p);
        }
        rs.close();
        return list;
    }

    public int delete(AllergicReactions obj) throws Exception {
        String sql = "UPDATE t_allergic_reactions "
                + "SET user_modify='%s', modify_date_time='%s', active ='%s' "
                + "WHERE t_allergic_reactions_id='%s' ";
        sql = String.format(sql, obj.user_modify, obj.modify_date_time, "0", obj.getObjectId());
        return theConnectionInf.eUpdate(sql);
    }

    public int updatePatientByPatient(String old_id, String new_id) throws Exception {
        String sql = "Update t_allergic_reactions set t_patient_id = '" + new_id + "'"
                + ",allergic_reactions_list = allergic_reactions_list ||'-'||'" + old_id + "'"
                + " where t_patient_id = '" + old_id + "'";
        return theConnectionInf.eUpdate(sql);
    }
}

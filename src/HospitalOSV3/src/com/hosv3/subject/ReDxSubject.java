package com.hosv3.subject;

import com.hospital_os.object.DxTemplate;
import com.hosv3.usecase.transaction.ManageReDxResp;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somrpasong Damyos
 */
@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public class ReDxSubject implements ManageReDxResp {

    private final List<ManageReDxResp> theVitalObsV = new ArrayList<ManageReDxResp>();

    public ReDxSubject() {
    }

    public void removeAttach() {
        theVitalObsV.clear();

    }

    public void attachManage(ManageReDxResp o) {
        theVitalObsV.add(o);
    }

    @Override
    public void notifyAddReDx(DxTemplate dx) {
        for (int i = 0, size = theVitalObsV.size(); i < size; i++) {
            ((ManageReDxResp) theVitalObsV.get(i)).notifyAddReDx(dx);
        }
    }

    @Override
    public void notifySaveReDx() {
        for (int i = 0, size = theVitalObsV.size(); i < size; i++) {
            ((ManageReDxResp) theVitalObsV.get(i)).notifySaveReDx();
        }
    }
}

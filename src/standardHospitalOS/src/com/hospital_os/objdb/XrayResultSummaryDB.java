/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.XrayResultSummary;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sompr
 */
public class XrayResultSummaryDB {

    private final ConnectionInf connectionInf;

    public XrayResultSummaryDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public XrayResultSummary select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_xray_result_summary where f_xray_result_summary_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<XrayResultSummary> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<XrayResultSummary> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_xray_result_summary order by description";
            preparedStatement = connectionInf.ePQuery(sql);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<XrayResultSummary> listByKeyword(String keyword) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_xray_result_summary where upper(description) like upper(?) order by description";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, "%" + (keyword == null || keyword.isEmpty() ? "" : keyword) + "%");
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<XrayResultSummary> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<XrayResultSummary> list = new ArrayList<XrayResultSummary>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                XrayResultSummary obj = new XrayResultSummary();
                obj.setObjectId(rs.getString("f_xray_result_summary_id"));
                obj.description = rs.getString("description");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (XrayResultSummary obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }
}
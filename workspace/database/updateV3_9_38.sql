-- appointment
ALTER TABLE b_template_appointment ADD COLUMN template_appointment_clinic character varying(255) NOT NULL default '';

update b_template_appointment set template_appointment_clinic = b_visit_clinic_id
from b_employee where b_employee.b_visit_clinic_id is not null and b_employee.b_employee_id = template_appointment_doctor;

-- drug allergy
ALTER TABLE t_patient
   ALTER COLUMN patient_drugallergy TYPE character varying(1);
ALTER TABLE t_patient
   ALTER COLUMN patient_drugallergy SET DEFAULT '0';
ALTER TABLE t_patient
   ALTER COLUMN patient_drugallergy SET NOT NULL;
ALTER TABLE t_visit
   ALTER COLUMN visit_deny_allergy TYPE character varying;
ALTER TABLE t_visit
   ALTER COLUMN visit_deny_allergy SET DEFAULT '0';
ALTER TABLE t_visit
   ALTER COLUMN visit_deny_allergy SET NOT NULL;

-- service point & ward
ALTER TABLE b_service_point
  ADD COLUMN is_ipd character varying(1) NOT NULL DEFAULT '0';

CREATE INDEX b_service_point_index_1
   ON b_service_point (alert_send_opdcard ASC NULLS LAST);

CREATE INDEX b_service_point_index_2
   ON b_service_point (is_ipd ASC NULLS LAST);

Update b_service_point set is_ipd = '1' where b_service_point_id = '2409840463402';

ALTER TABLE b_visit_ward
  ADD COLUMN b_service_point_id character varying(255) NOT NULL DEFAULT '2409840463402';

CREATE INDEX b_visit_ward_index_1
   ON b_visit_ward (b_service_point_id ASC NULLS LAST);


-- b_visit_bed
CREATE TABLE b_visit_bed
(
    b_visit_bed_id 					CHARACTER VARYING(255) NOT NULL,
    b_visit_ward_id 					CHARACTER VARYING(255) NOT NULL,
    sequences						INTEGER NOT NULL,
    bed_number             				CHARACTER VARYING(255) NOT NULL,
    continue_b_item_ids					CHARACTER VARYING[] DEFAULT '{}',
    active 						CHARACTER VARYING(1) NOT NULL DEFAULT '1', -- 0 = inactive , 1 = active
    record_date_time           				TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    user_record             				CHARACTER VARYING(255) NOT NULL,
    modify_date_time        				TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    user_modify             				CHARACTER VARYING(255) NOT NULL,

    CONSTRAINT b_visit_bed_pkey PRIMARY KEY (b_visit_bed_id),
    CONSTRAINT b_visit_bed_unique1 UNIQUE (b_visit_ward_id,bed_number),
    CONSTRAINT b_visit_bed_unique2 UNIQUE (b_visit_ward_id,sequences,active)
);


-- t_visit_bed
CREATE TABLE t_visit_bed
(
    t_visit_bed_id 					CHARACTER VARYING(255) NOT NULL,
    t_visit_id 						CHARACTER VARYING(255) NOT NULL,	
    b_visit_bed_id 					CHARACTER VARYING(255) NOT NULL,
    bed_number						CHARACTER VARYING(255) NOT NULL,
    continue_b_item_ids					CHARACTER VARYING[] DEFAULT '{}',
    current_bed						CHARACTER VARYING(1) NOT NULL DEFAULT '1', -- 0 = Non-current bed , 1 = Current bed
    move_date_time      				TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    move_out_date_time    				TIMESTAMP WITHOUT TIME ZONE,
    reason						TEXT DEFAULT '',
    active 						CHARACTER VARYING(1) NOT NULL DEFAULT '1', -- 0 = inactive , 1 = active
    record_date_time             			TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    user_record             				CHARACTER VARYING(255) NOT NULL,
    modify_date_time             			TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    user_modify             				CHARACTER VARYING(255) NOT NULL,
    CONSTRAINT t_visit_bed_pkey PRIMARY KEY (t_visit_bed_id)
);

-- ย้อนกลับการจำหน่ายผู้ป่วยใน
insert into f_gui_action (f_gui_action_id, gui_action_name) VALUES ('0903', 'ย้อนกลับการจำหน่ายผู้ป่วยใน');

-- t_visit_bed_schedule_item
CREATE TABLE t_visit_bed_schedule_item 
(	
	t_visit_bed_schedule_item_id 	CHARACTER VARYING(255) NOT NULL,
	t_visit_id 						CHARACTER VARYING(255) NOT NULL,
        schedule_item_date_time        	TIMESTAMP WITHOUT TIME ZONE NOT NULL ,
        record_date_time        	TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP ,
        last_order_date_time        	TIMESTAMP WITHOUT TIME ZONE NULL ,
    CONSTRAINT t_visit_bed_schedule_item_pkey PRIMARY KEY (t_visit_bed_schedule_item_id),
	CONSTRAINT t_visit_bed_schedule_item_unique UNIQUE (t_visit_id)
);


ALTER TABLE t_visit ADD COLUMN visit_ipd_staff_discharge CHARACTER VARYING (255)  DEFAULT '';
ALTER TABLE t_visit ADD COLUMN visit_ipd_discharge_date_time CHARACTER VARYING (19)  DEFAULT '';
ALTER TABLE t_visit ADD COLUMN visit_ipd_staff_reverse CHARACTER VARYING (255)  DEFAULT '';
ALTER TABLE t_visit ADD COLUMN visit_ipd_reverse_date_time CHARACTER VARYING (19)  DEFAULT '';

-- ใบ Drug profile > โดยมีเมนูย่อย พิมพ์ , ภาพก่อนพิมพ์ พิมพ์ได้เฉพาะผู้ป่วยใน
insert into f_gui_action (f_gui_action_id, gui_action_name) VALUES ('0814', 'ใบ Drug profile');

-- ใบ MAR ของยา และเวชภัณฑ์
ALTER TABLE b_item_drug ADD COLUMN print_mar_type CHARACTER VARYING(1) NOT NULL DEFAULT '0';
ALTER TABLE b_item_supply ADD COLUMN print_mar_type CHARACTER VARYING(1) NOT NULL DEFAULT '0';
ALTER TABLE t_order ADD COLUMN print_mar_type CHARACTER VARYING(1) NOT NULL DEFAULT '0';

update f_address set address_description = 'ภูซาง' where f_address_id = '560800';

-- เพิ่มบันทึกข้อมูลผู้ส่ง ที่ส่งต่อผู้ป่วยไปยังจุดบริการอื่นๆ ในตาราง t_visit_service
ALTER TABLE t_visit_service ADD COLUMN sender_id CHARACTER VARYING(255);

-- update db version
INSERT INTO s_version VALUES ('9701000000071', '71', 'Hospital OS, Community Edition', '3.9.38', '3.24.190914', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_38.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.38');
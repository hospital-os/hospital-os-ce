/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

/**
 *
 * @author tanakrit
 */
public class QueueXrayStatus {

    public static String NOXRAY = "0";
    public static String WAIT = "1";
    public static String REPORT = "2";

    public static String NOLAB_STR = "������͡�����";
    public static String WAIT_STR = "�ͼ�";
    public static String REPORT_STR = "��§ҹ��";

    public static String NOXRAY_FN = "/com/hosv3/gui/images/xray_none.png";
    public static String WAIT_FN = "/com/hosv3/gui/images/xray_order.png";
    public static String REPORT_FN = "/com/hosv3/gui/images/xray_report.png";

    public static String getString(String code) {
        if (code.equals(NOXRAY)) {
            return (NOLAB_STR);
        } else if (code.equals(WAIT)) {
            return (WAIT_STR);
        } else if (code.equals(REPORT)) {
            return (REPORT_STR);
        } else {
            return "";
        }
    }

    public static String getQStatus(int total, int report) {
        String status = "";
        if (total == 0) {
            status = QueueXrayStatus.NOXRAY;
        } else if (report == 0) {
            status = QueueXrayStatus.WAIT;
        } else if (report == total) {
            status = QueueXrayStatus.REPORT;
        }
        return status;
    }
}

package com.hosv3.objdb;

import com.hospital_os.objdb.DrugInstructionDB;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.Gutil;
import com.hosv3.object.DrugInstruction2;
import java.sql.ResultSet;
import java.util.Vector;

@SuppressWarnings("UseOfObsoleteCollectionType")
public class DrugInstruction2DB extends DrugInstructionDB {

    public DrugInstruction2DB(ConnectionInf db) {
        super(db);
    }

    public Vector selectActive() throws Exception {
        String sql = "select * from " + dbObj.table
                + " where " + dbObj.active + " = '1' order by " + dbObj.description;
        return eQuery(sql);
    }

    @Override
    public Vector selectAll() throws Exception {
        String sql = "select * from " + dbObj.table
                + " order by " + dbObj.description;
        return eQuery(sql);
    }

    public Vector selectByCN(String key) throws Exception {
        return selectByCNA(key, "1");
    }

    public Vector selectByCNA(String key, String active) throws Exception {
        String sql = "select * from " + dbObj.table
                + " where (" + dbObj.drug_instruction_id
                + " ilike '%" + Gutil.CheckReservedWords(key) + "%' "
                + " or " + dbObj.description
                + " ilike '%" + Gutil.CheckReservedWords(key) + "%') and "
                + dbObj.active + " = '" + active + "' order by " + dbObj.description;
        return eQuery(sql);
    }

    @Override
    public Vector eQuery(String sql) throws Exception {
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            DrugInstruction2 p = new DrugInstruction2();
            p.setObjectId(rs.getString(dbObj.pk_field));
            p.drug_instruction_id = rs.getString(dbObj.drug_instruction_id);
            p.description = rs.getString(dbObj.description);
            if (p.description != null) {
                p.description = p.description.replace('\n', ' ');
            }
            p.edqm_route_code = rs.getString(dbObj.edqm_route_code);
            p.active = rs.getString(dbObj.active);
            list.add(p);
        }
        rs.close();
        return list;
    }
}

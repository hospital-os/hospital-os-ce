/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.usecase.transaction;

import com.hospital_os.usecase.connection.UpdateStatus;

/**
 *
 * @author Khanate
 */
public interface ManageRefer {

    public void notifyInactiveRefer(String referId, UpdateStatus us) throws Exception;
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
public class DueTypeDetail extends Persistent {

    public String b_due_type_id;
    public String description;
    public String active;
    public String user_record_id;
    public String record_date_time;
    public String user_update_id;
    public String update_date_time;
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.object;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author sompr
 */
public class FRehabilitationCode extends Persistent implements CommonInf {

    public String description_en = "";
    public String description_th = "";

    @Override
    public String getCode() {
        return getObjectId();
    }

    @Override
    public String getName() {
        return getObjectId() + " : "+(description_th == null || description_th.isEmpty() ? description_en : description_th);
    }

    @Override
    public String toString() {
        return getObjectId() + " : "+(description_th == null || description_th.isEmpty() ? description_en : description_th);
    }
}

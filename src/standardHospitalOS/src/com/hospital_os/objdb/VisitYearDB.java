/*
 * VisitYear.java
 *
 * Created on 10 ����Ҥ� 2548, 14:23 �.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.*;
import com.hospital_os.usecase.connection.*;
import java.sql.*;
import java.util.*;

/**
 * �繵��ҧ����������������㹡���� �վ�. ������㹡�õ�Ǩ�ͺ �Ţ VN
 *
 * @author tong
 * @ since V2.0 b8
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class VisitYearDB {

    private ConnectionInf theConnectionInf;
    private final String idtable = "273";

    public VisitYearDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public int insert(VisitYear visityear) throws Exception {
        visityear.generateOID(idtable);
        if (visityear.visit_year == null || visityear.visit_year.isEmpty()) {
            ResultSet rs = theConnectionInf.eQuery("select to_char(current_date,'yyyy')");
            rs.next();
            int year_now = rs.getInt(1);
            year_now = year_now + 543 - 2500;
            visityear.visit_year = String.valueOf(year_now);
        }
        String SQL = "INSERT INTO t_visit_year (t_visit_year_id, visit_year) "
                + " VALUES("
                + "'" + visityear.getObjectId()
                + "','" + visityear.visit_year
                + "')";

        return this.theConnectionInf.eUpdate(SQL);

    }

    public int updateCurrenctYear(String year) throws Exception {
        String SQL = "UPDATE t_visit_year SET visit_year = '" + year + "', auto_reset_date = current_date";
        return this.theConnectionInf.eUpdate(SQL);
    }

    /**
     *
     * @param day
     * @param month
     * @param resetDate
     * @return
     * @throws Exception
     */
    public int updateAutoResetYear(int day, int month, java.util.Date resetDate) throws Exception {
        int ret = 0;
        String SQL = "UPDATE t_visit_year SET auto_reset_on_day = ?, auto_reset_on_month = ?, auto_reset_date = (case when auto_reset_date is null then ? else auto_reset_date end)";
        PreparedStatement ePQuery = theConnectionInf.ePQuery(SQL);
        ePQuery.setInt(1, day);
        ePQuery.setInt(2, month);
        ePQuery.setDate(3, new java.sql.Date(resetDate.getTime()));
        ret = ePQuery.executeUpdate();
        ePQuery.close();
        return ret;
    }

    public VisitYear getVisitYear() throws Exception {
        String SQL = "SELECT * FROM t_visit_year ORDER BY visit_year DESC";
        Vector vVisitYear = vQuery(SQL);
        VisitYear visitYear = (VisitYear) (vVisitYear.isEmpty() ? null : vVisitYear.get(0));
        if (visitYear == null) {
            insert(new VisitYear());
        }
        return visitYear;
    }
    private java.util.Date lastDateCheckVisitYear = null;
    private String currentVisitYear = null;

    public String selectCurrentYear() throws Exception {
        java.util.Date currentDate = null;
        ResultSet resultSetDate = theConnectionInf.eQuery("select current_date");
        if (resultSetDate.next()) {
            currentDate = resultSetDate.getDate(1);
        }
        if (currentVisitYear == null
                || currentVisitYear.isEmpty()
                || lastDateCheckVisitYear == null
                || lastDateCheckVisitYear.before(currentDate)) {
            lastDateCheckVisitYear = currentDate;
            VisitYear visitYear = getVisitYear();
            ResultSet resultSetAutoResetYear = theConnectionInf.eQuery("select option_detail_name from b_option_detail where b_option_detail_id = 'auto_reset_year'");
            String isAutoResetYear = "0";
            if (resultSetAutoResetYear.next()) {
                isAutoResetYear = resultSetAutoResetYear.getString(1);
            }
            if ("0".equals(isAutoResetYear)) {
                currentVisitYear = visitYear.visit_year;
            } else {
                Calendar tmpCalendar = Calendar.getInstance();
                tmpCalendar.setTime(currentDate);
                tmpCalendar.set(Calendar.MONTH, visitYear.auto_reset_on_month);
                tmpCalendar.set(Calendar.DAY_OF_MONTH, visitYear.auto_reset_on_day < 1 ? 1 : visitYear.auto_reset_on_day);
                tmpCalendar.set(Calendar.HOUR_OF_DAY, 0);
                tmpCalendar.set(Calendar.MINUTE, 0);
                tmpCalendar.set(Calendar.SECOND, 0);
                tmpCalendar.set(Calendar.MILLISECOND, 0);

                if (!tmpCalendar.getTime().after(currentDate)
                        && (visitYear.auto_reset_date == null
                        || (visitYear.auto_reset_date.before(tmpCalendar.getTime())))) {
                    int nextYear = Integer.parseInt(visitYear.visit_year) + 1;
                    currentVisitYear = nextYear > 99 ? "00" : String.valueOf(nextYear);
                    updateCurrenctYear(currentVisitYear);
                    // update seq too.
                    String sql = "update b_sequence_data set sequence_data_value = '1' where b_sequence_data_id in ('vn', 'an', 'xn' ,'dfn') or (position('yy' in sequence_data_pattern) > 0 and sequence_data_active = '1')";
                    theConnectionInf.eUpdate(sql);
                    // �Ͷ����͹
//                sql = "update b_receipt_sequence set receipt_sequence_value = '1'";
//                theConnectionInf.eUpdate(sql);
                } else {
                    currentVisitYear = visitYear.visit_year;
                }
            }
        }
        return currentVisitYear;
    }

    /**
     * ��㹡�� �ŧ������ŧ Vector
     *
     * @author tong
     * @since V2.0 b8
     * @return Vector �ͧ Object VisitYear
     */
    private Vector vQuery(String sql) throws Exception {
        Vector vVisitYear = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            VisitYear visitYear = new VisitYear();
            visitYear.setObjectId(rs.getString("t_visit_year_id"));
            visitYear.visit_year = rs.getString("visit_year");
            visitYear.auto_reset_on_day = rs.getInt("auto_reset_on_day");
            visitYear.auto_reset_on_month = rs.getInt("auto_reset_on_month");
            visitYear.auto_reset_date = rs.getDate("auto_reset_date");
            vVisitYear.add(visitYear);
        }
        return vVisitYear;
    }
}

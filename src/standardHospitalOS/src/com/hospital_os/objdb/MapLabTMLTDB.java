package com.hospital_os.objdb;

import com.hospital_os.object.MapLabTMLT;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author tanakrit
 */
public class MapLabTMLTDB {

    public ConnectionInf connectionInf;
    final public String tableId = "998";

    public MapLabTMLTDB(ConnectionInf db) {
        connectionInf = db;
    }

    public int insert(MapLabTMLT obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "INSERT INTO b_map_lab_tmlt( \n"
                    + "               b_map_lab_tmlt_id, b_item_id, b_lab_tmlt_tmltcode) \n"
                    + "       VALUES (?, ?, ?)";
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, obj.getGenID(tableId));
            preparedStatement.setString(index++, obj.b_item_id);
            preparedStatement.setString(index++, obj.b_lab_tmlt_tmltcode);
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "DELETE FROM b_map_lab_tmlt \n"
                    + " WHERE b_map_lab_tmlt_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(String... ids) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "DELETE FROM b_map_lab_tmlt WHERE b_lab_tmlt_tmltcode in (%s)";
            StringBuilder sb = new StringBuilder();
            for (String id : ids) {
                sb.append("'").append(id).append("',");
            }
            preparedStatement = connectionInf.ePQuery(String.format(sql,
                    sb.toString().substring(0, sb.toString().length() - 1)));
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<MapLabTMLT> list(String keyword, String grpId, String type) throws Exception {
        String sql = "select b_item.b_item_id \n"
                + "    ,b_item.item_number \n"
                + "    ,b_item.item_common_name \n"
                + "    ,case when b_lab_tmlt.tmlt_name is not null \n"
                + "          then b_lab_tmlt.tmlt_name \n"
                + "          else '' end as tmlt_name \n"
                + "    ,b_item_subgroup.b_item_subgroup_id \n"
                + "    ,b_map_lab_tmlt.b_map_lab_tmlt_id \n"
                + "from b_item \n"
                + "    inner join b_item_subgroup on b_item.b_item_subgroup_id = b_item_subgroup.b_item_subgroup_id \n"
                + "    left join b_map_lab_tmlt on b_item.b_item_id = b_map_lab_tmlt.b_item_id \n"
                + "    left join b_lab_tmlt on b_map_lab_tmlt.b_lab_tmlt_tmltcode = b_lab_tmlt.tmltcode \n"
                + "where b_item_subgroup.f_item_group_id = '2'";
        if (grpId != null && !grpId.isEmpty()) {
            sql += "and b_item_subgroup.b_item_subgroup_id = ?\n";
        }
        if (keyword != null && !keyword.isEmpty()) {
            sql += "and ((b_item.item_number ilike ?) \n"
                    + "	or (b_item.item_common_name ilike ?))";
        }
        if (type != null && !type.isEmpty()) {
            if (type.equals("1")) {
                sql += "and b_map_lab_tmlt.b_map_lab_tmlt_id is not null \n";
            } else if (type.equals("2")) {
                sql += "and b_map_lab_tmlt.b_map_lab_tmlt_id is null \n";
            }
        }
        sql += "and b_item.item_active = '1' \n"
                + "order by b_item.item_common_name";
        PreparedStatement preparedStatement = connectionInf.ePQuery(sql);
        int index = 1;
        if (grpId != null && !grpId.isEmpty()) {
            preparedStatement.setString(index++, grpId);
        }
        if (keyword != null && !keyword.isEmpty()) {
            preparedStatement.setString(index++, "%" + keyword + "%");
            preparedStatement.setString(index++, "%" + keyword + "%");
        }
        return executeQuery(preparedStatement);
    }

    public MapLabTMLT selectByItemId(String itemId) throws Exception {
        String sql = "select * \n"
                + "from b_map_lab_tmlt \n"
                + "where b_map_lab_tmlt.b_item_id = ?";
        PreparedStatement preparedStatement = connectionInf.ePQuery(sql);
        int index = 1;
        preparedStatement.setString(index++, itemId);
        List<MapLabTMLT> executeQuery = executeQuery(preparedStatement);
        return executeQuery.isEmpty() ? null : executeQuery.get(0);
    }

    public List<MapLabTMLT> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<MapLabTMLT> list = new ArrayList<>();
        try (ResultSet rs = preparedStatement.executeQuery()) {
            while (rs.next()) {
                MapLabTMLT obj = new MapLabTMLT();
                obj.setObjectId(rs.getString("b_map_lab_tmlt_id"));
                obj.b_lab_tmlt_tmltcode = rs.getString("tmlt_name");
                obj.b_item_id = rs.getString("b_item_id");
                try {
                    obj.item_name = rs.getString("item_common_name");
                } catch (SQLException ex) {
                }
                try {
                    obj.fsn = rs.getString("tmlt_name");
                } catch (SQLException ex) {
                }
                list.add(obj);
            }
            return list;
        }
    }
}

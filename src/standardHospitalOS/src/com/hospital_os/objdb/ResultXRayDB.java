/*
 * ResultXRayDB.java
 *
 * Created on 18 ���Ҥ� 2546, 18:45 �.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.*;
import com.hospital_os.usecase.connection.*;
import java.sql.*;
import java.util.*;

/**
 *
 * @author tong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class ResultXRayDB {

    public ConnectionInf theConnectionInf;
    final public String idtable = "232";

    public ResultXRayDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public int insert(ResultXRay o) throws Exception {
        ResultXRay p = o;
        p.generateOID(idtable);
        String sql = "INSERT INTO t_result_xray(\n"
                + "            t_result_xray_id, result_xray_xn, t_patient_id, t_visit_id, result_xray, \n"
                + "            result_xray_film_size, result_xray_staff_execute, record_date_time, \n"
                + "            f_xray_result_summary_id, result_xray_description, t_order_id, result_xray_notice, result_xray_staff_record, \n"
                + "            result_xray_active, result_xray_complete, \n";
        if (p.result_complete.equals("1")) {
            sql += "result_xray_complete_datetime, \n";
        }
        sql += "            t_patient_xn_id, result_xray_time, \n"
                + "            pacs_status, pacs_datetime)\n"
                + "    VALUES (?, ?, ?, ?, ?, \n"
                + "            ?, ?, ?, \n"
                + "            ?, ?, ?, ?, ?, \n"
                + "            ?, ?, \n";
        if (p.result_complete.equals("1")) {
            sql += "current_timestamp, \n";
        }
        sql += "?, ?, \n"
                + "            ?, ?)";
        PreparedStatement ePQuery = null;
        try {
            ePQuery = theConnectionInf.ePQuery(sql);
            int index = 1;
            ePQuery.setString(index++, p.getObjectId());
            ePQuery.setString(index++, p.xn);
            ePQuery.setString(index++, p.hn);
            ePQuery.setString(index++, p.vn);
            ePQuery.setString(index++, p.xray_id);
            ePQuery.setString(index++, p.size_film);
            ePQuery.setString(index++, p.excetue_film);
            ePQuery.setString(index++, p.record_time);
            ePQuery.setString(index++, p.f_xray_result_summary_id);
            ePQuery.setString(index++, p.description);
            ePQuery.setString(index++, p.order_item_id);
            ePQuery.setString(index++, p.xray_point);
            ePQuery.setString(index++, p.reporter);
            ePQuery.setString(index++, p.active);
            ePQuery.setString(index++, p.result_complete);
            ePQuery.setString(index++, p.t_patient_xn_id);
            ePQuery.setString(index++, p.xray_time);
            ePQuery.setString(index++, p.pacs_status);
            ePQuery.setTimestamp(index++, p.pacs_datetime == null ? null : new java.sql.Timestamp(p.pacs_datetime.getTime()));
            return ePQuery.executeUpdate();
        } finally {
            if (ePQuery != null) {
                ePQuery.close();
            }
        }
    }

    public int update(ResultXRay o) throws Exception {
        ResultXRay p = o;
        String sql = "UPDATE t_result_xray\n"
                + "   SET result_xray_xn=?, t_patient_id=?, t_visit_id=?, \n"
                + "       result_xray=?, result_xray_film_size=?, result_xray_staff_execute=?, \n"
                + "       record_date_time=?, f_xray_result_summary_id=?,result_xray_description=?, t_order_id=?, \n"
                + "       result_xray_notice=?, result_xray_staff_record=?, result_xray_active=?, \n"
                + "       result_xray_complete=?,\n";
        if (p.result_complete.equals("1")) {
            sql += "result_xray_complete_datetime = current_timestamp, \n";
        }
        sql += " t_patient_xn_id=?, result_xray_time=?, \n"
                + "       pacs_status=?, pacs_datetime=?\n"
                + " WHERE t_result_xray_id=?";
        PreparedStatement ePQuery = null;
        try {
            ePQuery = theConnectionInf.ePQuery(sql);
            int index = 1;
            ePQuery.setString(index++, p.xn);
            ePQuery.setString(index++, p.hn);
            ePQuery.setString(index++, p.vn);
            ePQuery.setString(index++, p.xray_id);
            ePQuery.setString(index++, p.size_film);
            ePQuery.setString(index++, p.excetue_film);
            ePQuery.setString(index++, p.record_time);
            ePQuery.setString(index++, p.f_xray_result_summary_id);
            ePQuery.setString(index++, p.description);
            ePQuery.setString(index++, p.order_item_id);
            ePQuery.setString(index++, p.xray_point);
            ePQuery.setString(index++, p.reporter);
            ePQuery.setString(index++, p.active);
            ePQuery.setString(index++, p.result_complete);
            ePQuery.setString(index++, p.t_patient_xn_id);
            ePQuery.setString(index++, p.xray_time);
            ePQuery.setString(index++, p.pacs_status);
            ePQuery.setTimestamp(index++, p.pacs_datetime == null ? null : new java.sql.Timestamp(p.pacs_datetime.getTime()));
            ePQuery.setString(index++, p.getObjectId());
            return ePQuery.executeUpdate();
        } finally {
            if (ePQuery != null) {
                ePQuery.close();
            }
        }
    }

    public ResultXRay selectByPK(String pk) throws Exception {
        String sql = "select * from t_result_xray where t_result_xray_id = ? and result_xray_active = '1'";
        PreparedStatement ePQuery = null;
        try {
            ePQuery = theConnectionInf.ePQuery(sql);
            int index = 1;
            ePQuery.setString(index++, pk);
            Vector v = eQuery(ePQuery.toString());
            return (ResultXRay) (v.isEmpty() ? null : v.get(0));
        } finally {
            if (ePQuery != null) {
                ePQuery.close();
            }
        }
    }

    public Vector selectAll() throws Exception {
        Vector v = eQuery("select * from t_result_xray");
        return v.isEmpty() ? null : v;
    }

    public Vector selectAll(String pk) throws Exception {
        String sql = "select * from t_result_xray where result_xray_active = '1'";
        if (pk != null && !pk.isEmpty()) {
            sql += "and (t_patient_id = ? or t_visit_id = ?)";
        }
        PreparedStatement ePQuery = null;
        try {
            ePQuery = theConnectionInf.ePQuery(sql);
            int index = 1;
            if (pk != null && !pk.isEmpty()) {
                ePQuery.setString(index++, pk);
                ePQuery.setString(index++, pk);
            }
            Vector v = eQuery(ePQuery.toString());
            return v.isEmpty() ? null : v;
        } finally {
            if (ePQuery != null) {
                ePQuery.close();
            }
        }
    }

    public Vector eQuery(String sql) throws Exception {
        ResultXRay p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        while (rs.next()) {
            p = new ResultXRay();
            p.setObjectId(rs.getString("t_result_xray_id"));
            p.xn = rs.getString("result_xray_xn");
            p.hn = rs.getString("t_patient_id");
            p.vn = rs.getString("t_visit_id");
            p.xray_id = rs.getString("result_xray");
            p.size_film = rs.getString("result_xray_film_size");
            p.excetue_film = rs.getString("result_xray_staff_execute");
            p.record_time = rs.getString("record_date_time");
            p.f_xray_result_summary_id = rs.getString("f_xray_result_summary_id");
            p.description = rs.getString("result_xray_description");
            p.order_item_id = rs.getString("t_order_id");
            p.xray_point = rs.getString("result_xray_notice");
            p.reporter = rs.getString("result_xray_staff_record");
            p.result_complete = rs.getString("result_xray_complete");
            p.result_xray_complete_datetime = rs.getTimestamp("result_xray_complete_datetime");
            p.active = rs.getString("result_xray_active");
            p.t_patient_xn_id = rs.getString("t_patient_xn_id");
            p.xray_time = rs.getString("result_xray_time");
            p.pacs_status = rs.getString("pacs_status");
            p.pacs_datetime = rs.getTimestamp("pacs_datetime");
            list.add(p);

        }
        rs.close();
        return list;
    }

    public ResultXRay selectOrderItemByVNItemId(String orderId, String visitUd) throws Exception {
        String sql = "select * from t_result_xray where t_order_id = ? and t_visit_id = ? and result_xray_active = '1'";
        PreparedStatement ePQuery = null;
        try {
            ePQuery = theConnectionInf.ePQuery(sql);
            int index = 1;
            ePQuery.setString(index++, orderId);
            ePQuery.setString(index++, visitUd);
            Vector v = eQuery(ePQuery.toString());
            return (ResultXRay) (v.isEmpty() ? null : v.get(0));
        } finally {
            if (ePQuery != null) {
                ePQuery.close();
            }
        }
    }

    public Vector selectByPtid(String patient_id) throws Exception {
        String sql = "select * from t_result_xray where t_patient_id = ? and result_xray_active = '1'";
        PreparedStatement ePQuery = null;
        try {
            ePQuery = theConnectionInf.ePQuery(sql);
            int index = 1;
            ePQuery.setString(index++, patient_id);
            return eQuery(ePQuery.toString());
        } finally {
            if (ePQuery != null) {
                ePQuery.close();
            }
        }
    }
}

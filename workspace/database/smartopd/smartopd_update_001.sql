CREATE TABLE t_smartopd_url (
    t_smartopd_url_id                    varchar(255) NOT NULL,
    url            	varchar(255) NOT NULL,
    update_user       	varchar(255) NOT NULL,
    update_time       	timestamp NOT NULL default now()
    );

ALTER TABLE t_smartopd_url
	ADD CONSTRAINT t_smartopd_url_pkey
	PRIMARY KEY (t_smartopd_url_id);

INSERT INTO t_smartopd_url VALUES ('WS', 'http://223.27.208.89/SmartOPD_WS_HOSPITAL-OS/Service1.asmx', 'System');

INSERT INTO t_smartopd_url VALUES ('XRAY', 'http://223.27.208.89/hiteon/hososlink.aspx', 'System');

INSERT INTO s_smartopd_version VALUES ('9780000000002', '2', 'SmartOPD Module', '1.1.0', '1.1.0', (select current_date) || ','|| (select current_time));

INSERT INTO s_script_update_log values ('SmartOPD_Module','smartopd_update_001.sql',(select current_date) || ','|| (select current_time),'Update SmartOPD Module');
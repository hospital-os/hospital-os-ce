/*
 * ColorRenderer.java (compiles with releases 1.2, 1.3, and 1.4) is used by
 * TableDialogEditDemo.java.
 */
package com.hosv3.gui.component;

import com.hospital_os.utility.Gutil;
import java.awt.Color;
import java.awt.Component;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.table.TableCellRenderer;

public class CellRendererColor extends JLabel implements TableCellRenderer {

    Border unselectedBorder = null;
    Border selectedBorder = null;
    boolean isBordered = true;

    public CellRendererColor(boolean isBordered) {
        this.isBordered = isBordered;
        this.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        setOpaque(true); //MUST do this for background to show up.
    }

    @Override
    public Component getTableCellRendererComponent(
            JTable table, Object value,
            boolean isSelected, boolean hasFocus,
            int row, int column) {
        Color newColor;
        try {
            String color = (String) value;
            if (color == null || color.isEmpty() || color.trim().equals("|")) {
                throw new Exception();
            }
            String[] temps = color.split("[|]");
            String col = temps.length >= 1 ? temps[0] : "";
            String queueName = temps.length >= 2 ? temps[1] : "";
            String queueNo = temps.length >= 3 ? temps[2] : "";
            newColor = Gutil.reconvertColor(col);
            setText(queueName);
            setToolTipText(queueName + (queueNo.isEmpty() ? "" : (": " + queueNo)));
        } catch (Exception e) {
            newColor = Color.GRAY;
            setText("");
            setToolTipText("��辺������");
        }
        setBackground(newColor);
        if (getBackground().equals(Color.BLACK)
                || getBackground().equals(Color.black)
                || getBackground().equals(new Color(0, 0, 0))) {
            setForeground(Color.WHITE);
        } else {
            setForeground(Color.BLACK);
        }
        if (isBordered) {
            if (isSelected) {
                if (selectedBorder == null) {
                    selectedBorder = BorderFactory.createMatteBorder(2, 5, 2, 5,
                            table.getSelectionBackground());
                }
                setBorder(selectedBorder);
            } else {
                if (unselectedBorder == null) {
                    unselectedBorder = BorderFactory.createMatteBorder(2, 5, 2, 5,
                            table.getBackground());
                }
                setBorder(unselectedBorder);
            }
        }

        return this;
    }
}

package com.hosv3.subject;

import com.hosv3.usecase.transaction.ManageQueueResp;
import java.util.Vector;

/**
 *
 * @author somprasong@hospital-os.com
 */
@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public class QueueSubject implements ManageQueueResp {

    Vector theObsV;

    /**
     * Creates a new instance of visitSubject
     */
    public QueueSubject() {
        theObsV = new Vector();
    }

    public void removeAttach() {
        theObsV.removeAllElements();

    }

    public void attachManageQueue(ManageQueueResp o) {
        theObsV.add(o);
    }

    public void detach(ManageQueueResp o) {
        theObsV.remove(o);
    }

    @Override
    public void notifyUpdateQueue(String msg, int state) {
        for (int i = 0, size = theObsV.size(); i < size; i++) {
            ((ManageQueueResp) theObsV.get(i)).notifyUpdateQueue(msg, state);
        }
    }

}

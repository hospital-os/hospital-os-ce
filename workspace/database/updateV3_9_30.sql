--HOME
ALTER TABLE t_health_home ADD COLUMN health_home_volunteer_family_id character varying(255) default '';

ALTER TABLE t_health_family DROP COLUMN patient_hn;

--DISABILITY
ALTER TABLE t_health_maim DROP COLUMN t_patient_id ;
ALTER TABLE t_health_maim DROP COLUMN t_visit_id ;

ALTER TABLE t_health_maim ADD COLUMN health_maim_date character varying(10) default '';
ALTER TABLE t_health_maim ADD COLUMN disability_cause character varying(1) default '0';
ALTER TABLE t_health_maim ADD COLUMN b_icd10_id character varying(10) default '';


-- Table t_health_disability

CREATE TABLE t_health_disability(
 t_health_disability_id Character varying(255) NOT NULL,
 t_person_id Character varying(255) NOT NULL,
 health_disability_number Character varying(20),
 health_disability_register_date Character varying(10),
 health_disability_note Text,
 user_record_id Character varying(255) NOT NULL,
 record_date_time Character varying(19) NOT NULL,
 user_modify_id Character varying(255) NOT NULL,
 modify_date_time Character varying(19) NOT NULL,
 CONSTRAINT t_health_disability_pkey PRIMARY KEY (t_health_disability_id)
);

-- Table t_health_disability_equipment

CREATE TABLE t_health_disability_equipment(
 t_health_disability_equipment_id Character varying(255) NOT NULL,
 t_health_maim_id Character varying(255) NOT NULL,
 b_health_disability_equipment_id Character varying(255) NOT NULL,
 active Character varying(1) DEFAULT 1 NOT NULL,
 user_record_id Character varying(255) NOT NULL,
 record_date_time Character varying(19) NOT NULL,
 user_cancel_id Character varying(255),
 cancel_date_time Character varying(19),
 CONSTRAINT t_health_disability_equipment_pkey PRIMARY KEY (t_health_disability_equipment_id)
);


-- Table b_health_disability_equipment

CREATE TABLE b_health_disability_equipment(
 b_health_disability_equipment_id Character varying(4) NOT NULL,
 disability_equipment_description Character varying(255) NOT NULL,
 CONSTRAINT b_health_disability_equipment_pkey PRIMARY KEY (b_health_disability_equipment_id)
);


insert into b_health_disability_equipment values ('8201','ขาเทียมระดับข้อเท้า (Symes)');
insert into b_health_disability_equipment values ('8202','ขาเทียมระดับใต้เข่า แกนนอก');
insert into b_health_disability_equipment values ('8203','ขาเทียมระดับใต้เข่าแกนใน');
insert into b_health_disability_equipment values ('8204','ขาเทียมระดับข้อเข่า');
insert into b_health_disability_equipment values ('8205','ขาเทียมระดับเหนือเข่าแกนนอก');
insert into b_health_disability_equipment values ('8206','ขาเทียมระดับเหนือเข่าแกนใน');
insert into b_health_disability_equipment values ('8207','ขาเทียมระดับตะโพกแกนนอก');
insert into b_health_disability_equipment values ('8208','ขาเทียมระดับสะโพกแกนใน');
insert into b_health_disability_equipment values ('8209','เท้าเทียมที่ต้องใส่ร่วมกับขาเทียมแบบต่าง ๆ');
insert into b_health_disability_equipment values ('8210','ขาเทียมชั่วคราวใต้เข่าเบ้าทำด้วยเฝือก');
insert into b_health_disability_equipment values ('8211','ขาเทียมชั่วคราวใต้เข่าเบ้าทำด้วยพลาสติก');
insert into b_health_disability_equipment values ('8212','ขาเทียมชั่วคราวระดับเข่าเบ้าทำด้วยเฝือก');
insert into b_health_disability_equipment values ('8213','ขาเทียมชั่วคราวระดับเข่าเบ้าทำด้วยพลาสติก');
insert into b_health_disability_equipment values ('8214','ขาเทียมชั่วคราวเหนือเข่าเบ้าทำด้วยเฝือก');
insert into b_health_disability_equipment values ('8215','ขาเทียมชั่วคราวเหนือเข่าเบ้าทำด้วยพลาสติก');
insert into b_health_disability_equipment values ('8216','ขาเทียมชั่วคราวสะโพกเบ้าทำด้วยเฝือก');
insert into b_health_disability_equipment values ('8217','ขาเทียมชั่วคราวสะโพกเบ้าทำด้วยพลาสติก');
insert into b_health_disability_equipment values ('8218','เบ้าขาเทียมใต้เข่า');
insert into b_health_disability_equipment values ('8219','เบ้าขาเทียมระดับเข่า');
insert into b_health_disability_equipment values ('8220','เบ้าขาเทียมเหนือเข่า');
insert into b_health_disability_equipment values ('8221','เบ้าขาเทียมสะโพก');
insert into b_health_disability_equipment values ('8521','โลหะดามขาภายนอก -เด็กเล็กดามข้อเท้า (Knee-orthosis)');
insert into b_health_disability_equipment values ('8522','โลหะดามขาภายนอก -ขาเด็กขนาดกลางดามข้อเท้า  (Knee-orthosis)');
insert into b_health_disability_equipment values ('8523','โลหะดามขาภายนอก -ขาขนาดใหญ่ดามข้อเท้า  (Knee-orthosis)');
insert into b_health_disability_equipment values ('8101','แขนเทียมต่ำกว่าระดับศอกส่วนปลายชนิดห้านิ้วมีระบบการใช้งาน');
insert into b_health_disability_equipment values ('8102','แขนเทียมต่ำกว่าระดับศอกส่วนปลายชนิดห้านิ้วไม่มีระบบการใช้งาน');
insert into b_health_disability_equipment values ('8103','แขนเทียมต่ำกว่าระดับศอกส่วนปลายชนิดตะขอโลหะ');
insert into b_health_disability_equipment values ('8104','แขนเทียมเหนือศอกส่วนปลายชนิดห้านิ้วข้อศอกล็อกได้ด้วยมือ');
insert into b_health_disability_equipment values ('8105','แขนเทียมเหนือศอกส่วนปลายชนิดตะขอโลหะข้อศอกล็อกได้ด้วยมือ');
insert into b_health_disability_equipment values ('8106','แขนเทียมชิดไหล่หรือแนบไหล่ ส่วนปลายชนิดห้านิ้วข้อศอกล็อกได้ด้วยมือ');
insert into b_health_disability_equipment values ('8107','แขนเทียมระดับเหนือศอกแบบ 5 นิ้ว เหมือนของจริง ระบบใช้งานได้');
insert into b_health_disability_equipment values ('8108','เบ้าแขนเทียมใต้ศอก (สำหรับการเปลี่ยนเฉพาะเบ้า)');
insert into b_health_disability_equipment values ('8109','เบ้าแขนเทียมระดับศอก (สำหรับการเปลี่ยนเฉพาะเบ้า)');
insert into b_health_disability_equipment values ('8110','เบ้าแขนเทียมระดับเหนือศอก (สำหรับการเปลี่ยนเฉพาะเบ้า)');
insert into b_health_disability_equipment values ('8111','เบ้าแขนเทียมระดับไหล่ (สำหรับการเปลี่ยนเฉพาะเบ้า)');
insert into b_health_disability_equipment values ('8112','สายบังคับแขนเทียม (สำหรับการเปลี่ยนเฉพาะสาย)');
insert into b_health_disability_equipment values ('8801','รองเท้าคนพิการขนาดเล็ก');
insert into b_health_disability_equipment values ('8802','รองเท้าคนพิการขนาดกลาง');
insert into b_health_disability_equipment values ('8803','รองเท้าคนพิการขนาดใหญ่');
insert into b_health_disability_equipment values ('8804','รองเท้าคนพิการขนาดใหญ่พิเศษ');
insert into b_health_disability_equipment values ('8805','ค่าดัดแปลงรองเท้าคนพิการ');
insert into b_health_disability_equipment values ('8807','แผ่นเสริมภายในขนาดใหญ่');
insert into b_health_disability_equipment values ('8808','แผ่นเสริมภายในขนาดเล็ก');
insert into b_health_disability_equipment values ('8809','เสริมฝ่าเท้าส่วนหน้า');
insert into b_health_disability_equipment values ('8810','T-strap');
insert into b_health_disability_equipment values ('2301','เครื่องช่วยการมองเห็นสำหรับคนสายตาพิการ  (Visual aids)');
insert into b_health_disability_equipment values ('2302','กล้องส่องดูไกลขนาดเล็กน้ำหนักเบาพับเก็บได้ สำหรับผู้มีสายตาเลือนรางใช้พกพา กำลังขยาย 8 เท่า');
insert into b_health_disability_equipment values ('2303','กล้องส่องดูไกลชนิดตาเดียว กำลังขยาย 6 เท่า monocular 6X');
insert into b_health_disability_equipment values ('2304','กล้องส่องดูไกลชนิดตาเดียว กำลังขยาย 8 เท่า');
insert into b_health_disability_equipment values ('2305','กล้องส่องดูไกลชนิดตาเดียว กำลังขยาย 8 เท่า monocular 6X');
insert into b_health_disability_equipment values ('2306','กล้องส่องดูไกลหน้าเลนส์สี่เหลี่ยมสำหรับมองสิ่งรอบตัวได้ในระยะใกล้ 1 เมตร กำลังขยาย 6 เท่า');
insert into b_health_disability_equipment values ('2307','กล้องส่องตาเดียว แวริโอ พลัส ระบบปริซึม');
insert into b_health_disability_equipment values ('2308','กล้องส่องทางไกลสองตา กำลังขยาย 10 เท่า');
insert into b_health_disability_equipment values ('2309','แท่นรองอ่านหนังสือสำหรับผู้ที่มีความบกพร่องทางการมองเห็น');
insert into b_health_disability_equipment values ('2310','เลนส์แก้วขยายภาพชนิดรวมแสง ใช้วางทาบบนวัตถุ ขนาด 2X/50 mm, 2X/65 mm');
insert into b_health_disability_equipment values ('2311','เลนส์ขยายภาพชนิดรวมแสงใช้วางทาบบนวัตถุ กำลังขยาย 6 เท่า');
insert into b_health_disability_equipment values ('2312','แว่นกรองแสงสำหรับผู้มีสายตาเลือนราง (กรองแสงที่ 380 nm.) Special Glasser');
insert into b_health_disability_equipment values ('2313','แว่นกรองแสงสำหรับผู้มีสายตาเลือนราง (กรองแสงที่ 450 หรือ 511 หรือ 527 nm.) Special Glasser');
insert into b_health_disability_equipment values ('2314','แว่นขยายชนิดพกพาขนาดเล็ก กำลังขยาย 10 เท่า');
insert into b_health_disability_equipment values ('2315','แว่นขยายชนิดพกพาขนาดเล็ก กำลังขยาย 4 เท่า');
insert into b_health_disability_equipment values ('2316','แว่นขยายชนิดพกพาขนาดเล็ก กำลังขยาย 7 เท่า');
insert into b_health_disability_equipment values ('2317','แว่นขยายแบบมือถือชนิดด้ามถือมีเลนส์ขยาย 5 เท่า');
insert into b_health_disability_equipment values ('2318','แว่นขยายมือถือแบบพกพาที่มีแสงไฟในตัว ใช้มองพร้อมมีกล้องส่องทางไกลใช้มองไกลในตัวเดียวกันกำลังขยาย มองใกล้ 3.2 เท่า มองไกล 2.5 เท่า');
insert into b_health_disability_equipment values ('2319','แว่นขยายมือถือแบบพกพาที่มีแสงไฟในตัว กำลังขยาย 10 เท่า');
insert into b_health_disability_equipment values ('2320','แว่นขยายมือถือแบบพกพาที่มีแสงไฟในตัว กำลังขยาย 5 เท่า' );
insert into b_health_disability_equipment values ('2321','แว่นขยายมือถือแบบพกพาที่มีแสงไฟในตัวหน้าเลนส์กว้าง กำลังขยาย 3.5 เท่า');
insert into b_health_disability_equipment values ('2322','แว่นขยายระบบ Video เพื่อขยายภาพออกจอโทรทัศน์ กำลังขยาย 17.5 เท่า');
insert into b_health_disability_equipment values ('2323','แว่นตาขยายภาพชนิดเลนส์ นูนที่มีกำลังขยายสูง 4 - 12 X (เลนส์มีสองข้าง)');
insert into b_health_disability_equipment values ('2324','แว่นตาขยายภาพชนิดเลนส์ นูนที่มีกำลังขยายสูง 4-12 X (เลนส์มีข้างเดียว)');
insert into b_health_disability_equipment values ('2325','แว่นตาขยายภาพแบบสองตา (Binocalar) ซึ่งมี Optics ชนิด Diffactive กำลัง 2.5 X (+10)');
insert into b_health_disability_equipment values ('2326','แว่นตาติดกล้องส่องทางไกล (สองตา) สำหรับมองไกล กำลังขยาย 2.8 เท่า');
insert into b_health_disability_equipment values ('2327','แว่นตาติดกล้องส่องทางไกล (สองตา) สำหรับมองไกล กำลังขยาย 3.5 เท่า');
insert into b_health_disability_equipment values ('2328','แว่นตาระบบกล้องส่องดูไกลชนิดสองตา ระบบ Galileun');
insert into b_health_disability_equipment values ('2329','อุปกรณ์แว่นขยายแบบโคมไฟมีแสงไฟในตัว กำลังขยาย 1.75 เท่า');
insert into b_health_disability_equipment values ('2330','อุปกรณ์แว่นขยายแบบวางตั้ง กำลังขยาย 10 เท่า ชนิดมีที่สอดปากกาที่ฐาน');
insert into b_health_disability_equipment values ('2331','อุปกรณ์แว่นขยายแบบวางตั้ง กำลังขยาย 12.5 เท่า ชนิดมีที่สอดปากกาที่ฐาน');
insert into b_health_disability_equipment values ('2332','อุปกรณ์แว่นขยายแบบวางตั้ง ชนิดมีที่สอดปากกาที่ฐาน กำลังขยาย 6 เท่า');
insert into b_health_disability_equipment values ('2501','เครื่องช่วยฟังสำหรับคนหูพิการ สำหรับเด็กอายุต่ำกว่า 10 ปี');
insert into b_health_disability_equipment values ('2502','เครื่องช่วยฟังสำหรับคนหูพิการ สำหรับผู้ใหญ่');
insert into b_health_disability_equipment values ('6006','สายสวนปัสสาวะแบบสวนด้วยตนเอง');
insert into b_health_disability_equipment values ('8222','สายเข็มขัดเทียม');
insert into b_health_disability_equipment values ('8223','แป้นสายเข็มขัด');
insert into b_health_disability_equipment values ('8524','PTB brace');
insert into b_health_disability_equipment values ('8525','เบรสขาสั้น');
insert into b_health_disability_equipment values ('8526','ที่คลุมเข่า Knee pad ');
insert into b_health_disability_equipment values ('8706','ไม้ค้ำยันรักแร้แบบอลูมิเนียม');
insert into b_health_disability_equipment values ('8707','ไม้เท้าอลูมิเนียมแบบสามขา');
insert into b_health_disability_equipment values ('8708','ไม้เท้าสำหรับคนตาบอดพับได้ด้วยสายยืดหยุ่นชนิดมีด้าม');
insert into b_health_disability_equipment values ('8709','ที่ช่วยฝึกเดินแบบมีล้อขนาดกลาง (Anterior Wheel Walker)');
insert into b_health_disability_equipment values ('8710','ที่ช่วยฝึกเดินแบบมีล้อขนาดเล็ก (Anterior Wheel Walker)');
insert into b_health_disability_equipment values ('8901','รถนั่งคนพิการชนิดพับได้ทำด้วยโลหะ แบบปรับให้เหมาะสมกับความพิการได้');
insert into b_health_disability_equipment values ('8902','รถนั่งคนพิการชนิดพับได้ทำด้วยโลหะ แบบปรับไม่ได้');
insert into b_health_disability_equipment values ('8903','เบาะรองนั่งสำหรับผู้พิการ');
insert into b_health_disability_equipment values ('9001','ฟองน้ำรองตัวสำหรับผู้ป่วยหนัก หรือผู้ป่วยอัมพาต');


--COMMUNITY_ACTIVITY

-- Table t_health_community
CREATE TABLE t_health_community(
 t_health_community_id Character varying(255) NOT NULL,
 health_community_id Character varying(10) NOT NULL,
 health_community_name Character varying(255) NOT NULL,
 health_community_register Character varying(1) DEFAULT 0 NOT NULL,
 health_community_soi Character varying(255),
 health_community_road Character varying(255),
 t_health_village_id Character varying(255) NOT NULL,
 health_community_person_id Character varying(255),
 health_volunteer_person_id Character varying(255),
 health_volunteer Character varying(20),
 health_community_detail Text,
 active Character varying(1) DEFAULT 1 NOT NULL,
 user_record_id Character varying(255) NOT NULL,
 record_date_time Character varying(19) NOT NULL,
 user_modify_id Character varying(255) NOT NULL,
 modify_date_time Character varying(19) NOT NULL,
 user_cancel_id Character varying(255),
 cancel_date_time Character varying(19),
 CONSTRAINT t_health_communit_pkey PRIMARY KEY (t_health_community_id),
 CONSTRAINT t_health_community_unique1 UNIQUE (health_community_id)
);

-- Table t_health_community_activity

DROP TABLE t_health_community_activity;

CREATE TABLE t_health_community_activity(
 t_health_community_activity_id Character varying(255) NOT NULL,
 t_health_community_id Character varying(255) NOT NULL,
 f_comactivity_id Character varying(255) NOT NULL,
 start_date Character varying(10) NOT NULL,
 finish_date Character varying(10) NOT NULL,
 active Character varying(1) NOT NULL,
 user_record_id Character varying(255) NOT NULL,
 record_date_time Character varying(19) NOT NULL,
 user_modify_id Character varying(255) NOT NULL,
 modify_date_time Character varying(19) NOT NULL,
 user_cancel_id Character varying(255),
 cancel_date_time Character varying(19),
 CONSTRAINT t_health_community_activity_pkey PRIMARY KEY (t_health_community_activity_id)
);

-- Table t_health_cancer

CREATE TABLE t_health_cancer(
 t_health_cancer_id Character varying(255) NOT NULL,
 t_person_id Character varying(255) NOT NULL,
 t_visit_id Character varying(255) NOT NULL,
 health_cancer_survey_date Character varying(10) NOT NULL,
 health_cancer_breast_exam Character varying(1) DEFAULT 0 NOT NULL,
 health_cancer_cervix_exam Character varying(1) DEFAULT 0 NOT NULL,
 health_cancer_pregnant_exam Character varying(1),
 health_cancer_breast_exam_note Character varying(255),
 health_cancer_cervix_exam_note Character varying(255),
 health_cancer_note Character varying(255),
 health_cancer_first_check Character varying(1) DEFAULT 0 NOT NULL,
 active Character varying(1) DEFAULT 1 NOT NULL,
 user_record_id Character varying(255) NOT NULL,
 record_date_time Character varying(19) NOT NULL,
 user_modify_id Character varying(255) NOT NULL,
 modify_date_time Character varying(19) NOT NULL,
 user_cancel_id Character varying(255),
 cancel_date_time Character varying(19),
 CONSTRAINT t_health_cancer_pkey PRIMARY KEY (t_health_cancer_id)
);

ALTER TABLE t_health_pp_care DROP COLUMN pp_care_deliver_place ;

--newborn
ALTER TABLE t_health_pp ADD COLUMN pp_gestational_age_week Integer;
ALTER TABLE t_health_pp ADD COLUMN pp_gestational_age_day Integer;
ALTER TABLE t_health_pp ADD COLUMN pp_delivery_type character varying(1) default '1';  --1 = คลอดเดี่ยว  ,2 = คลอดเด็กแฝด 


--dental
ALTER TABLE t_health_dental RENAME f_gum_id  TO gum_id;
ALTER TABLE t_health_dental ALTER gum_id TYPE character varying(6);
ALTER TABLE t_health_dental ALTER COLUMN gum_id SET DEFAULT '999999'::character varying;

--person
ALTER TABLE t_health_family ADD COLUMN f_person_village_status_id character varying(2) default '5';

--provider
update b_employee set council = case when council ='01' then '1'
				    when council ='02' then '2'
                                    when council ='03' then '3'
                                    when council ='04' then '4'
                                    when council ='05' then '5'
                                    when council ='06' then '6'
                                    when council ='07' then '7'
                                   else '0' end ;

ALTER TABLE b_employee RENAME council TO f_provider_council_code_id;
ALTER TABLE b_employee ALTER f_provider_council_code_id TYPE character varying(2);
ALTER TABLE b_employee ALTER COLUMN f_provider_council_code_id SET DEFAULT '0'::character varying;

--create f_provider_council_code
CREATE TABLE f_provider_council_code (
        f_provider_council_code_id character varying(2) NOT NULL,
        provider_council_code_description character varying(255) NOT NULL,
        CONSTRAINT f_provider_council_code_pkey PRIMARY KEY (f_provider_council_code_id)
);

INSERT INTO f_provider_council_code VALUES ('0','อื่นๆ');
INSERT INTO f_provider_council_code VALUES ('1','แพทยสภา');
INSERT INTO f_provider_council_code VALUES ('2','สภาการพยาบาล');
INSERT INTO f_provider_council_code VALUES ('3','สภาเภสัชกรรม');
INSERT INTO f_provider_council_code VALUES ('4','ทันตแพทยสภา');
INSERT INTO f_provider_council_code VALUES ('5','สภากายภาพบำบัด');
INSERT INTO f_provider_council_code VALUES ('6','สภาเทคนิคการแพทย์');
INSERT INTO f_provider_council_code VALUES ('7','สัตวแพทยสภา');


--update f_provider_type
update f_provider_type set description = 'เจ้าพนักงานสาธารณสุขชุมชน' where f_provider_type_id = '000004';
update f_provider_type set description = 'นักวิชาการสาธารณสุข' where f_provider_type_id = '000005';

--map_rp4356_drug_unit

-- Table r_rp4356_drug_unit

CREATE TABLE r_rp4356_drug_unit(
 r_rp4356_drug_unit_id Character varying(3) NOT NULL,
 drug_unit_description Character varying(255) NOT NULL,
 CONSTRAINT r_rp4356_drug_unit_pkey PRIMARY KEY (r_rp4356_drug_unit_id)
 
);
--insert r_rp4356_drug_unit
INSERT INTO r_rp4356_drug_unit VALUES ('001','กระป๋อง');
INSERT INTO r_rp4356_drug_unit VALUES ('002','กระปุก');
INSERT INTO r_rp4356_drug_unit VALUES ('003','กล่อง');
INSERT INTO r_rp4356_drug_unit VALUES ('004','ก้อน');
INSERT INTO r_rp4356_drug_unit VALUES ('005','แกลลอน');
INSERT INTO r_rp4356_drug_unit VALUES ('006','ขวด');
INSERT INTO r_rp4356_drug_unit VALUES ('007','คาทริดจ์');
INSERT INTO r_rp4356_drug_unit VALUES ('008','คู่');
INSERT INTO r_rp4356_drug_unit VALUES ('009','แคปซูล');
INSERT INTO r_rp4356_drug_unit VALUES ('010','ชิ้น');
INSERT INTO r_rp4356_drug_unit VALUES ('011','ชุด');
INSERT INTO r_rp4356_drug_unit VALUES ('012','ชุดทดสอบ');
INSERT INTO r_rp4356_drug_unit VALUES ('013','ซอง');
INSERT INTO r_rp4356_drug_unit VALUES ('014','ด้าม');
INSERT INTO r_rp4356_drug_unit VALUES ('015','ตลับ');
INSERT INTO r_rp4356_drug_unit VALUES ('016','ถัง');
INSERT INTO r_rp4356_drug_unit VALUES ('017','ถุง');
INSERT INTO r_rp4356_drug_unit VALUES ('018','แถบ');
INSERT INTO r_rp4356_drug_unit VALUES ('019','ท่อ');
INSERT INTO r_rp4356_drug_unit VALUES ('020','แท่ง');
INSERT INTO r_rp4356_drug_unit VALUES ('021','แทงค์');
INSERT INTO r_rp4356_drug_unit VALUES ('022','ใบ');
INSERT INTO r_rp4356_drug_unit VALUES ('023','ปี๊บ');
INSERT INTO r_rp4356_drug_unit VALUES ('024','แผง');
INSERT INTO r_rp4356_drug_unit VALUES ('025','แผ่น');
INSERT INTO r_rp4356_drug_unit VALUES ('026','ม้วน');
INSERT INTO r_rp4356_drug_unit VALUES ('027','เม็ด');
INSERT INTO r_rp4356_drug_unit VALUES ('028','ลิตร');
INSERT INTO r_rp4356_drug_unit VALUES ('029','ไวอัล');
INSERT INTO r_rp4356_drug_unit VALUES ('030','เส้น');
INSERT INTO r_rp4356_drug_unit VALUES ('031','หลอด');
INSERT INTO r_rp4356_drug_unit VALUES ('032','หัว');
INSERT INTO r_rp4356_drug_unit VALUES ('033','โหล');
INSERT INTO r_rp4356_drug_unit VALUES ('034','อัน');
INSERT INTO r_rp4356_drug_unit VALUES ('035','แอมพูล');

-- Table b_map_rp4356_drug_unit

CREATE TABLE b_map_rp4356_drug_unit(
 b_map_rp4356_drug_unit_id Character varying(255) NOT NULL,
 b_item_drug_uom_id Character varying(255) NOT NULL,
 r_rp4356_drug_unit_id Character varying(3) NOT NULL,
 CONSTRAINT b_map_rp4356_drug_unit_pkey PRIMARY KEY (b_map_rp4356_drug_unit_id)
);

INSERT INTO s_version VALUES ('9701000000063', '63', 'Hospital OS, Community Edition', '3.9.30', '3.21.050313', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_30.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.30');
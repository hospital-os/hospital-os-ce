/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class Person extends Persistent {

    public String person_hcis = "";
    public String t_health_home_id;
    public String person_family_number = "";
    public String f_prefix_id = "000";
    public String person_firstname = "";
    public String person_lastname = "";
    public String person_prefix_eng = "";
    public String person_firstname_eng = "";
    public String person_lastname_eng = "";
    public String person_pid = "";
    public String person_passport = "";
    public String person_birthday = "";
    public String person_birthday_true = "0";
    public String f_sex_id = "3";
    public String f_patient_blood_group_id = "1";
    public String f_rh_group_id;
    public String f_patient_nation_id = "99";
    public String f_patient_race_id = "99";
    public String f_patient_religion_id = "1";
    public String father_person_id;
    public String mother_person_id;
    public String couple_person_id;
    public String f_patient_marriage_status_id = "1";
    public String f_patient_education_type_id = "1";
    public String f_patient_occupation_id = "000";
    public String f_patient_family_status_id = "";
    public String f_person_village_status_id = "5";
    public String f_patient_area_status_id = "";
    public String f_person_foreigner_id = "";
    public String person_foreigner_card_no = "";
    public String person_revenu = "";
    public String person_move_in_date_time = "";
    public String active = "1";
    public String user_record_id = "";
    public String record_date_time = "";
    public String user_modify_id = "";
    public String modify_date_time = "";
    public String user_cancel_id;
    public String cancel_date_time;
    public String f_person_affiliated_id = "0";
    public String f_person_rank_id = "0";
    public String f_person_jobtype_id = "0";
    // join table 1-1
    public Prefix prefix = new Prefix();

    public String getPersonName() {
        return (prefix != null ? (prefix.getObjectId() == null
                || prefix.getObjectId().equals("000") ? "" : prefix.description) : "")
                + person_firstname + " " + person_lastname;
    }

    public String getPersonNameEn() {
        return person_prefix_eng + person_firstname_eng + " " + person_lastname_eng;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.gui.dialog;

import com.hospital_os.object.DrugFavorite;
import com.hospital_os.object.Employee;
import com.hospital_os.object.Visit;
import com.hospital_os.utility.ComboboxModel;
import com.hospital_os.utility.DataSource;
import com.hospital_os.utility.Gutil;
import com.hospital_os.utility.TableModelDataSource;
import com.hosv3.control.HosControl;
import java.util.List;
import java.util.Vector;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class DialogDrugFavorite extends javax.swing.JDialog {

    private HosControl theHC;
    private TableModelDataSource tmcdsItem = new TableModelDataSource("��¡����");

    /**
     * Creates new form DialogDrugFavorite
     */
    public DialogDrugFavorite(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setIconImage(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/favorite_24.png")).getImage());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jComboBoxDoctor = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        jComboBoxClinic = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();
        btnAdd = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("��¡���ҷ�������");
        getContentPane().setLayout(new java.awt.GridBagLayout());

        jPanel1.setLayout(new java.awt.GridBagLayout());

        jLabel4.setFont(jLabel4.getFont());
        jLabel4.setText("ᾷ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(jLabel4, gridBagConstraints);

        jComboBoxDoctor.setEditable(true);
        jComboBoxDoctor.setFont(jComboBoxDoctor.getFont());
        jComboBoxDoctor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxDoctorActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(jComboBoxDoctor, gridBagConstraints);

        jLabel2.setFont(jLabel2.getFont());
        jLabel2.setText("��չԡ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(jLabel2, gridBagConstraints);

        jComboBoxClinic.setEditable(true);
        jComboBoxClinic.setFont(jComboBoxClinic.getFont());
        jComboBoxClinic.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxClinicActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(jComboBoxClinic, gridBagConstraints);

        table.setFont(table.getFont());
        table.setModel(tmcdsItem);
        table.setFillsViewportHeight(true);
        table.setRowHeight(20);
        table.setSelectionMode(javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        table.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tableMouseReleased(evt);
            }
        });
        table.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tableKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(table);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(jScrollPane1, gridBagConstraints);

        btnAdd.setFont(btnAdd.getFont().deriveFont(btnAdd.getFont().getStyle() | java.awt.Font.BOLD));
        btnAdd.setText("+");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(btnAdd, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.5;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        getContentPane().add(jPanel1, gridBagConstraints);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jComboBoxDoctorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxDoctorActionPerformed
        if (evt.getActionCommand().equals("comboBoxEdited")) {
            String keyword = String.valueOf(jComboBoxDoctor.getSelectedItem());
            if (jComboBoxDoctor.getSelectedIndex() >= 0) {
                Employee employee = (Employee) jComboBoxDoctor.getSelectedItem();
                if (employee.getName().trim().equals(keyword.trim())) {
                    return;
                }
            }
            Vector v = theHC.theLookupControl.listDoctorDiag(keyword.trim());
            if (v == null) {
                v = new Vector();
            }
            if (!v.isEmpty()) {
                ComboboxModel.initComboBox(jComboBoxDoctor, v);
            }
        }
        updateTable();
    }//GEN-LAST:event_jComboBoxDoctorActionPerformed

    private void jComboBoxClinicActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxClinicActionPerformed
        if (evt.getActionCommand().equals("comboBoxEdited")) {
            String keyword = String.valueOf(jComboBoxClinic.getSelectedItem());
            Vector v = theHC.theLookupControl.listClinic(keyword);
            if (v == null) {
                v = new Vector();
            }
            if (!v.isEmpty()) {
                ComboboxModel.initComboBox(jComboBoxClinic, v);
            }
        }
        updateTable();
    }//GEN-LAST:event_jComboBoxClinicActionPerformed

    private void tableMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableMouseReleased
        btnAdd.setEnabled(table.getSelectedRowCount() > 0);
    }//GEN-LAST:event_tableMouseReleased

    private void tableKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tableKeyReleased
        btnAdd.setEnabled(table.getSelectedRowCount() > 0);
    }//GEN-LAST:event_tableKeyReleased

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        doAdd();
    }//GEN-LAST:event_btnAddActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JComboBox jComboBoxClinic;
    private javax.swing.JComboBox jComboBoxDoctor;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable table;
    // End of variables declaration//GEN-END:variables

    public void setControl(HosControl theHC) {
        this.theHC = theHC;
        // init combobox
        ComboboxModel.initComboBox(jComboBoxClinic, theHC.theLookupControl.listClinic());
        ComboboxModel.initComboBox(jComboBoxDoctor, theHC.theLookupControl.listDoctorDiag());
        if ("3".equals(theHC.theHO.theEmployee.authentication_id)) {
            Gutil.setGuiData(jComboBoxDoctor, theHC.theHO.theEmployee.getObjectId());
            jComboBoxDoctor.setEnabled(false);
        } else {
            jComboBoxDoctor.setEnabled(true);
        }
    }

    public void updateTable() {
        tmcdsItem.clearTable();
        List<DataSource> list = theHC.theSetupControl.listDrugFavorite(Gutil.getGuiData(jComboBoxDoctor), Gutil.getGuiData(jComboBoxClinic));
        tmcdsItem.setDataSources(list);
        tmcdsItem.fireTableDataChanged();
        if (table.getRowCount() > 0) {
            table.setRowSelectionInterval(0, 0);
        }
        btnAdd.setEnabled(table.getSelectedRowCount() > 0);
    }

    public void showDialog() {
        updateTable();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void doAdd() {
        List<Object> selectedIds = tmcdsItem.getSelectedIds(table.getSelectedRows());
        DrugFavorite[] favorites = new DrugFavorite[selectedIds.size()];
        for (int i = 0; i < favorites.length; i++) {
            favorites[i] = (DrugFavorite) selectedIds.get(i);
        }
        if (theHC.theOrderControl.saveOrderItemFromDrugFavorite(favorites)) {
            this.dispose();
        }
    }
}

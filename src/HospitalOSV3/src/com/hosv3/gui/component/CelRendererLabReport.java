/*
 * BooleanImageTableCellRenderer.java
 *
 * Created on 16 ����Ҥ� 2547, 9:12 �.
 */
package com.hosv3.gui.component;

import java.awt.Component;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author amp
 */
public class CelRendererLabReport implements TableCellRenderer {

    @Override
    public Component getTableCellRendererComponent(JTable table,
            Object value, boolean isSelected, boolean hasFocus,
            int row, int column) {
        try {
            if (((String) value).equals("4")) {
                return lblReported;
            }
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
        }
        return lblReport;
    }
    private JLabel lblReported = new JLabel(new ImageIcon(getClass().getResource("/com/hospital_os/images/refer_out.gif")));
    private JLabel lblReport = new JLabel(new ImageIcon(getClass().getResource("/com/hospital_os/images/no_refer_out.gif")));
    private static final Logger LOG = Logger.getLogger(CelRendererLabReport.class.getName());
}

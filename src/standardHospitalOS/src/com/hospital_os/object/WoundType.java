package com.hospital_os.object;

/**
 *
 * @author Administrator
 */
public class WoundType {

    public static final int CONTUSSION = 1;
    public static final int SWELLING = 2;
    public static final int ABRASION = 3;
    public static final int CUTWOUND = 4;
    public static final int LACERATION = 5;
    public static final int FRACTURE = 6;
    public static final int DISLOCATION = 7;
    public static final int OTHER = 0;

    /** Creates a new instance of WoundType */
    public WoundType() {
    }
}

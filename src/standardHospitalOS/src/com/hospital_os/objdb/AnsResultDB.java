/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.AnsResult;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.ResultSet;
import java.util.Vector;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class AnsResultDB {

    public ConnectionInf theConnectionInf;
    public AnsResult dbObj;
    private final String idtable = "302";

    public AnsResultDB(ConnectionInf db) {
        theConnectionInf = db;
        dbObj = new AnsResult();
        this.initConfig();
    }

    private boolean initConfig() {
        dbObj.table = "f_cigarette_feq";
        dbObj.pk_field = "f_cigarette_feq_id";
        dbObj.desc = "ans_result_description";
        dbObj.active = "active";
        return true;
    }

    @SuppressWarnings("UseOfObsoleteCollectionType")
    public Vector selectAll() throws Exception {
        String sql = "select * from " + dbObj.table;

        Vector v = eQuery(sql);
        if (v.isEmpty()) {
            return null;
        } else {
            return v;
        }
    }

    @SuppressWarnings("UseOfObsoleteCollectionType")
    public Vector eQuery(String sql) throws Exception {
        AnsResult p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            p = new AnsResult();
            p.setObjectId(rs.getString(dbObj.pk_field));
            p.desc = rs.getString(dbObj.desc);
            p.active = rs.getString(dbObj.active);
            list.add(p);
        }
        rs.close();
        return list;
    }
}

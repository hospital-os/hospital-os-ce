/*
 * AppointmentOrderDB.java
 *
 * Created on 23 ����Ҿѹ�� 2549, 14:51 �.
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.*;
import com.hospital_os.usecase.connection.*;
import com.hospital_os.utility.*;
import java.sql.*;
import java.util.*;

/**
 *
 * @author amp
 */
@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public class AppointmentOrderDB {

    public ConnectionInf theConnectionInf;
    public AppointmentOrder dbObj;
    final public String idtable = "280";

    /**
     * Creates a new instance of AppointmentOrderDB
     *
     * @param db
     */
    public AppointmentOrderDB(ConnectionInf db) {
        theConnectionInf = db;
        dbObj = new AppointmentOrder();
        initConfig();
    }

    private boolean initConfig() {
        dbObj.table = "t_patient_appointment_order";
        dbObj.pk_field = "t_patient_appointment_order_id";
        dbObj.patient_id = "t_patient_id";
        dbObj.appointment_id = "t_patient_appointment_id";
        dbObj.item_id = "b_item_id";
        dbObj.item_common_name = "patient_appointment_order_common_name";
        dbObj.b_item_set_id = "b_item_set_id";
        return true;
    }

    public int insert(AppointmentOrder o) throws Exception {
        AppointmentOrder p = o;
        p.generateOID(idtable);
        String sql = "insert into " + dbObj.table + " ("
                + dbObj.pk_field
                + " ," + dbObj.patient_id
                + " ," + dbObj.appointment_id
                + " ," + dbObj.item_id
                + " ," + dbObj.item_common_name
                + " ," + dbObj.b_item_set_id;
        if (p.getProperty("b_hstock_item_id") != null) {
            sql += " ,b_hstock_item_id";
        }
        if (p.getProperty("b_hstock_item_set_id") != null) {
            sql += " ,b_hstock_item_set_id";
        }
        sql += " ) values ('"
                + p.getObjectId()
                + "','" + p.patient_id
                + "','" + p.appointment_id
                + "','" + p.item_id
                + "','" + p.item_common_name
                + "','" + p.b_item_set_id;
        if (p.getProperty("b_hstock_item_id") != null) {
            sql += "','" + p.getProperty("b_hstock_item_id");
        }
        if (p.getProperty("b_hstock_item_set_id") != null) {
            sql += "','" + p.getProperty("b_hstock_item_set_id");
        }
        sql += "')";
        sql = Gutil.convertSQLToMySQL(sql, theConnectionInf.gettypeDatabase());
        return theConnectionInf.eUpdate(sql);
    }

    public int update(AppointmentOrder o) throws Exception {
        String sql = "update " + dbObj.table + " set ";
        AppointmentOrder p = o;
        String field = ""
                + "', " + dbObj.patient_id + "='" + p.patient_id
                + "', " + dbObj.appointment_id + "='" + p.appointment_id
                + "', " + dbObj.item_id + "='" + p.item_id
                + "', " + dbObj.item_common_name + "='" + p.item_common_name
                + "', " + dbObj.b_item_set_id + "='" + p.b_item_set_id
                + "' where " + dbObj.pk_field + "='" + p.getObjectId() + "'";
        sql = Gutil.convertSQLToMySQL(sql + field.substring(2), theConnectionInf.gettypeDatabase());
        return theConnectionInf.eUpdate(sql);
    }

    public int delete(AppointmentOrder o) throws Exception {
        String sql = "delete from " + dbObj.table
                + " where " + dbObj.pk_field + "='" + o.getObjectId() + "'";
        return theConnectionInf.eUpdate(sql);
    }

    public int deleteByAppid(String aid) throws Exception {
        String sql = "delete from " + dbObj.table
                + " where " + dbObj.appointment_id + "='" + aid + "'";
        return theConnectionInf.eUpdate(sql);
    }

    public Vector selectByPatientAndAppointment(String patient_id, String appointment_id) throws Exception {
        String sql = "select * from " + dbObj.table
                + " where " + dbObj.patient_id
                + " = '" + patient_id + "' and " + dbObj.appointment_id
                + " = '" + appointment_id + "'";

        return eQuery(sql);
    }

    public Vector selectByAppointment(String appointment_id) throws Exception {
        String sql = "select * from " + dbObj.table
                + " where " + dbObj.appointment_id + " = '" + appointment_id + "'";
        return eQuery(sql);
    }

    public Vector eQuery(String sql) throws Exception {
        AppointmentOrder p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            p = new AppointmentOrder();
            p.setObjectId(rs.getString(dbObj.pk_field));
            p.patient_id = rs.getString(dbObj.patient_id);
            p.appointment_id = rs.getString(dbObj.appointment_id);
            p.item_id = rs.getString(dbObj.item_id);
            p.item_common_name = rs.getString(dbObj.item_common_name);
            p.b_item_set_id = rs.getString(dbObj.b_item_set_id);// for hstock module
            try {
                p.setProperty("b_hstock_item_id", rs.getString("b_hstock_item_id"));
            } catch (SQLException ex) {

            }
            try {
                p.setProperty("b_hstock_item_set_id", rs.getString("b_hstock_item_set_id"));
            } catch (SQLException ex) {

            }
            list.add(p);
        }
        rs.close();
        return list;
    }
}

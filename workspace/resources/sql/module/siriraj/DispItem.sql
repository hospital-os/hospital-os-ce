select
     lpad(t_patient.patient_hn,10,'0')||lpad(t_visit.visit_vn,10,'0') as dispense_id
     ,b_map_welfare_product_category.b_welfare_product_category_id as product_category
     ,t_order.b_item_id as hospital_drug_id
     ,b_welfare_drug_tmt.tpucode as drug_id
     ,'' as dfs_code
     ,t_order.order_common_name as dfs_text
     ,drug_purch.item_drug_uom_description as pack_size
     ,b_item_drug_instruction.item_drug_instruction_number||b_item_drug_frequency.item_drug_frequency_number as sig_code
     ,b_item_drug_instruction.item_drug_instruction_description as ins_desc
     ,t_order_drug.order_drug_dose as drug_dose
     ,b_item_drug_uom.item_drug_uom_description ||' '||b_item_drug_frequency.item_drug_frequency_description as drug_end_text 
     ,t_order.order_qty as quantity
     ,t_order.order_price as unit_price
     ,(t_billing_invoice_item.billing_invoice_item_total::decimal(10,2))::text  as charge_amount
     ,t_order.order_price  as reimb_price
     ,(t_billing_invoice_item.billing_invoice_item_payer_share::decimal(10,2))::text   as reimb_amount 
     ,'' as product_selection_code
     ,'' as refill
	, case  when t_order_ned.f_ned_reason_id = '0' then 'E-'
			when t_order_ned.f_ned_reason_id is null then '' 
				else f_ned_reason.ned_code end as claim_control
     ,'' as claim_category
     , t_order.order_date_time as order_date_time
 
from
        t_billing_invoice inner join t_visit on t_billing_invoice.t_visit_id = t_visit.t_visit_id   
                                    and t_billing_invoice.billing_invoice_active = '1'
                                    and t_visit.f_visit_type_id = '0'
                                    and t_visit.f_visit_status_id in ('2','3')
                                    and cast(t_billing_invoice.billing_invoice_payer_share as float) > 0.0
     inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id 
     inner join t_billing_invoice_item on t_billing_invoice.t_billing_invoice_id = t_billing_invoice_item.t_billing_invoice_id 
     and t_billing_invoice_item.billing_invoice_item_active = '1' 
     inner join t_order on t_order.t_order_id = t_billing_invoice_item.t_order_item_id 
     and t_order.f_order_status_id not in ('0','3')
     and t_order.f_item_group_id = '1'
     left join t_order_drug on t_order.t_order_id = t_order_drug.t_order_id and t_order_drug.order_drug_active ='1'
     left join b_item_drug_instruction on t_order_drug.b_item_drug_instruction_id = b_item_drug_instruction.b_item_drug_instruction_id        
     left join b_item_drug_uom on t_order_drug.b_item_drug_uom_id_use =  b_item_drug_uom.b_item_drug_uom_id
     left join b_item_drug_uom as drug_purch on t_order_drug.b_item_drug_uom_id_purch = drug_purch.b_item_drug_uom_id
     left join b_item_drug_frequency on t_order_drug.b_item_drug_frequency_id = b_item_drug_frequency.b_item_drug_frequency_id
     left join b_map_welfare_product_category on t_order.b_item_id = b_map_welfare_product_category.b_item_id
     inner join t_visit_payment on  t_billing_invoice.t_payment_id = t_visit_payment.t_visit_payment_id
     and t_visit_payment.visit_payment_active = '1'
     and t_visit_payment.b_contract_plans_id in (select  b_welfare_direct_draw_map_plan.b_contract_plans_id   from    b_welfare_direct_draw_map_plan ) 

     left join t_order_ned  on t_order.t_order_id = t_order_ned.t_order_id
     left join f_ned_reason on t_order_ned.f_ned_reason_id = f_ned_reason.f_ned_reason_id
     left join b_map_welfare_drug_tmt on t_order.b_item_id = b_map_welfare_drug_tmt.b_item_id
     left join b_welfare_drug_tmt on b_map_welfare_drug_tmt.b_welfare_drug_tmt_id = b_welfare_drug_tmt.b_welfare_drug_tmt_id
     cross join b_site

 where
        lpad(t_visit.visit_vn,9,'0') ||0|| replace(substr(t_billing_invoice.t_billing_invoice_date_time,12,19),':','')  = ?


group by
     dispense_id
     ,product_category
     ,hospital_drug_id
     ,drug_id
     ,dfs_code
     ,dfs_text
     ,pack_size
     ,sig_code
     ,ins_desc
     ,drug_dose
     ,drug_end_text
     ,quantity
     , unit_price
     ,charge_amount
     ,reimb_price
     ,reimb_amount 
     ,product_selection_code
     ,refill
     ,claim_control
     ,claim_category
     ,order_date_time
     order by
     dispense_id  asc;
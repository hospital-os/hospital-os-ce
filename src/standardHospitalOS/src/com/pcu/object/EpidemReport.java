/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.object;

import com.hospital_os.usecase.connection.Persistent;
import java.util.Date;

/**
 *
 * @author tanakrit
 */
public class EpidemReport extends Persistent {

    public String t_visit_id;
    public Date report_datetime;
    public String user_report_id;
    public Date onset_date;
    public Date treated_date;
    public String treated_hospital_code;
    public int f_epidem_person_status_id;
    public int f_epidem_covid_symptom_type_id;
    public boolean pregnant_status = false;
    public boolean respirator_status = false;
    public boolean vaccinated_status = false;
    public boolean exposure_epidemic_area_status = false;
    public boolean exposure_healthcare_worker_status = false;
    public boolean exposure_closed_contact_status = false;
    public boolean exposure_occupation_status = false;
    public boolean exposure_travel_status = false;
    public int f_epidem_risk_history_type_id;
    public String t_address_id;
    public String isolate_province_id;
    public int f_epidem_covid_isolate_place_id;
    public Date specimen_date;
    public int f_epidem_covid_spcm_place_id;
    public int f_epidem_covid_reason_type_id;
    public int f_epidem_covid_lab_confirm_type_id;
    public String b_item_id;
    public Date lab_report_date;
    public String lab_name;
    public String lab_value;
    public String active = "1";
    public Date record_datetime;
    public String user_record_id;
    public Date update_datetime;
    public String user_update_id;
    public Date cancel_datetime;
    public String user_cancel_id;
    public int sent_complete = 0;
    public boolean sent_epidem = true;
    public boolean sent_moph = true;
    public int sent_moph_complete = 0;
    //obj
    public String hcode_name;
    public String epidem_report_address;
    public String vn;
}

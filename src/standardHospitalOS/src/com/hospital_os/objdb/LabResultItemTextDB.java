/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.LabResultItemText;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class LabResultItemTextDB {
    private final ConnectionInf connectionInf;
    private final String tableId = "811";

    public LabResultItemTextDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }
    
    public int insert(LabResultItemText obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO b_item_lab_result_text(\n");
            sql.append("            b_item_lab_result_text_id, b_item_lab_result_id, result_text)\n");
            sql.append("    VALUES (?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getGenID(tableId));
            preparedStatement.setString(2, obj.b_item_lab_result_id);
            preparedStatement.setString(3, obj.result_text);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
    
    public int delete(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "delete from b_item_lab_result_text where b_item_lab_result_text_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            // execute delete SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
    
    public int deleteByBItemLabResultId(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "delete from b_item_lab_result_text where b_item_lab_result_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            // execute delete SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public LabResultItemText select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_item_lab_result_text where b_item_lab_result_text_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<LabResultItemText> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
    
    public List<LabResultItemText> selectByBItemLabResultId(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_item_lab_result_text where b_item_lab_result_id = ? order by result_text";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
    
    public List<LabResultItemText> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<LabResultItemText> list = new ArrayList<LabResultItemText>();
        ResultSet rs = null;
        try {
            // execute insert SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                LabResultItemText obj = new LabResultItemText();
                obj.setObjectId(rs.getString("b_item_lab_result_text_id"));
                obj.b_item_lab_result_id = rs.getString("b_item_lab_result_id");
                obj.result_text = rs.getString("result_text");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }
}

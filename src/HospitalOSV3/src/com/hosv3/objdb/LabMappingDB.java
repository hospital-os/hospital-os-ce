/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.objdb;

import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.Gutil;
import com.hosv3.object.LabMapping;
import java.sql.ResultSet;
import java.util.Vector;

@SuppressWarnings("UseOfObsoleteCollectionType")
public class LabMappingDB {

    public ConnectionInf theConnectionInf;
    public LabMapping dbObj;
    final public String idtable = "";

    /**
     * @param ConnectionInf db @roseuid 3F65897F0326
     */
    public LabMappingDB(ConnectionInf db) {
        theConnectionInf = db;
        dbObj = new LabMapping();
        initConfig();
    }

    public boolean initConfig() {
        dbObj.table = "b_lab_mapping";
        dbObj.pk_field = "lab_mapping_id";
        dbObj.b_item_id = "b_item_id";
        dbObj.f_lab_name = "f_lab_name";
        dbObj.f_lab_std_id = "f_lab_std_id";
        dbObj.labgrp = "labgrp";
        dbObj.labgrp_name = "labgrp_name";
        dbObj.b_item_lab_group_id = "b_item_lab_group_id";
        return true;
    }

    public int insert(LabMapping p) throws Exception {
        p.generateOID(this.idtable);
        String sql = "insert into " + dbObj.table + " ("
                + dbObj.pk_field
                + " ," + dbObj.b_item_id
                + " ," + dbObj.f_lab_name
                + " ," + dbObj.f_lab_std_id
                + " ," + dbObj.labgrp
                + " ," + dbObj.labgrp_name
                + " ," + dbObj.b_item_lab_group_id
                + " ) values ('"
                + p.getObjectId()
                + "','" + p.b_item_id
                + "','" + p.f_lab_name
                + "','" + p.f_lab_std_id
                + "','" + p.labgrp
                + "','" + p.labgrp_name
                + "','" + p.b_item_lab_group_id
                + "')";
        sql = Gutil.convertSQLToMySQL(sql, theConnectionInf.gettypeDatabase());
        return theConnectionInf.eUpdate(sql);
    }

    public int update(LabMapping o) throws Exception {
        String sql = "update " + dbObj.table + " set ";
        String field = ""
                + "', " + dbObj.b_item_id + "='" + o.b_item_id
                + "', " + dbObj.f_lab_name + "='" + o.f_lab_name
                + "', " + dbObj.f_lab_std_id + "='" + o.f_lab_std_id
                + "', " + dbObj.labgrp + "='" + o.labgrp
                + "', " + dbObj.labgrp_name + "='" + o.labgrp_name
                + "', " + dbObj.b_item_lab_group_id + "='" + o.b_item_lab_group_id
                + "' where " + dbObj.pk_field + "='" + o.getObjectId() + "'";
        sql = Gutil.convertSQLToMySQL(sql + field.substring(2), theConnectionInf.gettypeDatabase());
        return theConnectionInf.eUpdate(sql);
    }

    public int delete(LabMapping of) throws Exception {
        String sql = "delete from " + dbObj.table
                + " where " + dbObj.pk_field
                + " = '" + of.getObjectId() + "'";
        return theConnectionInf.eUpdate(sql);
    }

    public int deleteByItem(String b_item_id) throws Exception {
        String sql = "delete from " + dbObj.table
                + " where " + dbObj.b_item_id
                + " = '" + b_item_id + "'";
        return theConnectionInf.eUpdate(sql);
    }

    public Vector selectAll() throws Exception {
        String sql = "select * from " + dbObj.table;
        return eQuery(sql);
    }

    public LabMapping selectByID(String id) throws Exception {
        String sql = "select * from " + dbObj.table
                + " where " + dbObj.pk_field + " = '" + id + "'";
        Vector v = eQuery(sql);
        if (v.isEmpty()) {
            return null;
        } else {
            return (LabMapping) v.get(0);
        }
    }

    public LabMapping getByItem(String b_item_id) throws Exception {
        String sql = "select * from " + dbObj.table
                + " where " + dbObj.b_item_id + " = '" + b_item_id + "'";
        Vector v = eQuery(sql);
        if (v.isEmpty()) {
            return null;
        } else {
            return (LabMapping) v.get(0);
        }
    }

    public Vector selectByItem(String b_item_id) throws Exception {
        String sql = "select * from " + dbObj.table + " where " + dbObj.b_item_id + " ='" + b_item_id + "'";
        return eQuery(sql);
    }

    public Vector eQuery(String sql) throws Exception {
        LabMapping p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            p = new LabMapping();
            p.setObjectId(rs.getString(dbObj.pk_field));
            p.b_item_id = rs.getString(dbObj.b_item_id);
            p.f_lab_name = rs.getString(dbObj.f_lab_name);
            p.f_lab_std_id = rs.getString(dbObj.f_lab_std_id);
            p.labgrp = rs.getString(dbObj.labgrp);
            p.labgrp_name = rs.getString(dbObj.labgrp_name);
            p.b_item_lab_group_id = rs.getString(dbObj.b_item_lab_group_id);
            list.add(p);
        }
        rs.close();
        return list;
    }
}
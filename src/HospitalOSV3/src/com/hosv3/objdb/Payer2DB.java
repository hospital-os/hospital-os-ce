/*
 * Payer2DB.java
 *
 * Created on 20 �ѹ�Ҥ� 2548, 10:57 �.
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
package com.hosv3.objdb;

import com.hospital_os.objdb.PayerDB;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.Gutil;
import com.hosv3.object.Payer2;
import java.sql.ResultSet;
import java.util.Vector;

/**
 *
 * @author sumo1
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class Payer2DB extends PayerDB {

    /**
     * Creates a new instance of Payer2DB
     */
    public Payer2DB(ConnectionInf db) {
        super(db);
    }

    @Override
    public Vector selectAllByName(String pk, String active) throws Exception {
        String sql = "select * from " + dbObj.table
                + " where ";
        if (!pk.isEmpty()) {
            pk = Gutil.CheckReservedWords(pk);
            sql = sql + "(" + dbObj.payer_id
                    + " like '%" + pk + "%'" + " or UPPER("
                    + dbObj.description + ") like UPPER('%" + pk + "%')" + ") and ";

        }

        sql = sql + dbObj.active + " = '" + active + "'" + " order by "
                + dbObj.payer_id;
        Vector v = eQuery(sql);
        if (v.isEmpty()) {
            return null;
        } else {
            return v;
        }
    }

    @Override
    public Vector eQuery(String sql) throws Exception {
        Payer2 p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            p = new Payer2();
            p.setObjectId(rs.getString(dbObj.pk_field));
            p.payer_id = rs.getString(dbObj.payer_id);
            p.description = rs.getString(dbObj.description);
            p.active = rs.getString(dbObj.active);
            list.add(p);
        }
        rs.close();
        return list;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.objdb.squery;

import com.hospital_os.objdb.PatientDrugAllergyDB;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hosv3.object.squery.SpecialQueryPatientDrugAllergy;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class SpecialQueryPatientDrugAllergyDB extends PatientDrugAllergyDB {

    public SpecialQueryPatientDrugAllergyDB(ConnectionInf db) {
        super(db);
    }

    public SpecialQueryPatientDrugAllergy selectByPatientIdItemId(String patientId, String itemId) throws Exception {
        String sql = "select t_patient_drug_allergy.* "
                + ", b_item_drug_standard.item_drug_standard_description "
                + ", f_allergy_type.allergy_type_description as allergy_type "
                + ", f_naranjo_interpretation.naranjo_interpretation_detail as naranjo_interpretation "
                + ", f_allergy_warning_type.warning_type_description as warning_type "
                + ", prefix1.patient_prefix_description || person1.person_firstname || ' ' || person1.person_lastname as pharma_assess "
                + ", prefix2.patient_prefix_description || person2.person_firstname || ' ' || person2.person_lastname as doctor_diag "
                + ", t_naranjo.naranjo_total as naranjo_point "
                + "from t_patient_drug_allergy "
                + "inner join b_item_drug_standard_map_item on b_item_drug_standard_map_item.b_item_drug_standard_id = t_patient_drug_allergy.b_item_drug_standard_id "
                + "inner join b_item_drug_standard on b_item_drug_standard.b_item_drug_standard_id = t_patient_drug_allergy.b_item_drug_standard_id "
                + "inner join f_allergy_type on f_allergy_type.f_allergy_type_id = t_patient_drug_allergy.f_allergy_type_id "
                + "inner join f_naranjo_interpretation on f_naranjo_interpretation.f_naranjo_interpretation_id = t_patient_drug_allergy.f_naranjo_interpretation_id "
                + "inner join f_allergy_warning_type on f_allergy_warning_type.f_allergy_warning_type_id = t_patient_drug_allergy.f_allergy_warning_type_id "
                + "inner join b_employee as pharma on pharma.b_employee_id = t_patient_drug_allergy.pharma_assess_id \n"
                + "inner join t_person as person1 on person1.t_person_id = pharma.t_person_id\n"
                + "left join f_patient_prefix as prefix1 on prefix1.f_patient_prefix_id = person1.f_prefix_id\n"
                + "left join b_employee as doctor on doctor.b_employee_id = t_patient_drug_allergy.doctor_diag_id \n"
                + "left join t_person as person2 on person2.t_person_id = doctor.t_person_id\n"
                + "left join f_patient_prefix as prefix2 on prefix2.f_patient_prefix_id = person2.f_prefix_id\n"
                + "left join t_naranjo on (t_naranjo.t_patient_drug_allergy_id = t_patient_drug_allergy.t_patient_drug_allergy_id and t_naranjo.active = '1') "
                + "where t_patient_drug_allergy.t_patient_id = '" + patientId + "' "
                + "and b_item_drug_standard_map_item.b_item_id = '" + itemId + "' and t_patient_drug_allergy.active = '1' "
                + "order by b_item_drug_standard.item_drug_standard_description ";
        List<SpecialQueryPatientDrugAllergy> list = eSPQuery(sql);
        return list.isEmpty() ? null : list.get(0);
    }

    private List<SpecialQueryPatientDrugAllergy> eSPQuery(String sql) throws Exception {
        List<SpecialQueryPatientDrugAllergy> list = new ArrayList<SpecialQueryPatientDrugAllergy>();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            SpecialQueryPatientDrugAllergy p = new SpecialQueryPatientDrugAllergy();
            p.patient_drug_allergy_id = rs.getString("t_patient_drug_allergy_id");
            p.patient_id = rs.getString("t_patient_id");
            p.item_drug_standard_id = rs.getString("b_item_drug_standard_id");
            p.generic_name = rs.getString("item_drug_standard_description");
            p.allergy_type_id = rs.getString("f_allergy_type_id");
            p.allergy_type = rs.getString("allergy_type");
            p.symtom_date = rs.getString("drug_allergy_symtom_date");
            p.symtom = rs.getString("drug_allergy_symtom");
            p.naranjo_interpretation_id = rs.getString("f_naranjo_interpretation_id");
            p.naranjo_interpretation = rs.getString("naranjo_interpretation");
            p.drug_allergy_note = rs.getString("drug_allergy_note");
            p.allergy_warning_type_id = rs.getString("f_allergy_warning_type_id");
            p.warning_type = rs.getString("warning_type");
            p.report_date = rs.getString("drug_allergy_report_date");
            p.pharma_assess_id = rs.getString("pharma_assess_id");
            p.pharma_assess = rs.getString("pharma_assess");
            p.doctor_diag_id = rs.getString("doctor_diag_id");
            p.doctor_diag = rs.getString("doctor_diag");
            p.naranjo_point = rs.getString("naranjo_point");
            list.add(p);
        }
        rs.close();
        return list;
    }

    public List<SpecialQueryPatientDrugAllergy> listDrugAllergyByPId(String patientId) throws Exception {
        String sql = "select t_patient_drug_allergy.* "
                + ", b_item_drug_standard.item_drug_standard_description "
                + ", f_allergy_type.allergy_type_description as allergy_type "
                + ", f_naranjo_interpretation.naranjo_interpretation_detail as naranjo_interpretation "
                + ", f_allergy_warning_type.warning_type_description as warning_type "
                + ", prefix1.patient_prefix_description || person1.person_firstname || ' ' || person1.person_lastname as pharma_assess "
                + ", prefix2.patient_prefix_description || person2.person_firstname || ' ' || person2.person_lastname as doctor_diag "
                + ", t_naranjo.naranjo_total as naranjo_point "
                + "from t_patient_drug_allergy "
                + "inner join b_item_drug_standard on b_item_drug_standard.b_item_drug_standard_id = t_patient_drug_allergy.b_item_drug_standard_id "
                + "inner join f_allergy_type on f_allergy_type.f_allergy_type_id = t_patient_drug_allergy.f_allergy_type_id "
                + "inner join f_naranjo_interpretation on f_naranjo_interpretation.f_naranjo_interpretation_id = t_patient_drug_allergy.f_naranjo_interpretation_id "
                + "inner join f_allergy_warning_type on f_allergy_warning_type.f_allergy_warning_type_id = t_patient_drug_allergy.f_allergy_warning_type_id "
                + "inner join b_employee as pharma on pharma.b_employee_id = t_patient_drug_allergy.pharma_assess_id \n"
                + "inner join t_person as person1 on person1.t_person_id = pharma.t_person_id\n"
                + "left join f_patient_prefix as prefix1 on prefix1.f_patient_prefix_id = person1.f_prefix_id\n"
                + "left join b_employee as doctor on doctor.b_employee_id = t_patient_drug_allergy.doctor_diag_id \n"
                + "left join t_person as person2 on person2.t_person_id = doctor.t_person_id\n"
                + "left join f_patient_prefix as prefix2 on prefix2.f_patient_prefix_id = person2.f_prefix_id\n"
                + "left join t_naranjo on (t_naranjo.t_patient_drug_allergy_id = t_patient_drug_allergy.t_patient_drug_allergy_id and t_naranjo.active = '1') "
                + "where t_patient_drug_allergy.t_patient_id = '" + patientId + "' "
                + "and t_patient_drug_allergy.active = '1' "
                + "order by b_item_drug_standard.item_drug_standard_description ";
        List<SpecialQueryPatientDrugAllergy> list = eSPQuery(sql);
        return list;
    }
}

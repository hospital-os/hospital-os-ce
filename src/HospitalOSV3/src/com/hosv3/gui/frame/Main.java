package com.hosv3.gui.frame;

/*
 * Main.java
 *
 * Created on 29 �ѹ��¹ 2546, 9:31 �.
 */
import com.hospital_os.object.Authentication;
import com.hospital_os.object.InsuranceClaim;
import com.hospital_os.object.ServicePoint;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hosv3.HosUpdate;
import com.hosv3.control.HosControl;
import com.hosv3.gui.dialog.DialogInsuraceRepayWarnning;
import com.hosv3.gui.dialog.HosDialog;
import com.hosv3.gui.panel.transaction.HosPanel;
import com.hosv3.object.HosObject;
import com.hosv3.usecase.transaction.ManageSystemResp;
import com.hosv3.utility.Config;
import com.hosv3.utility.Constant;
import com.hosv3.utility.ReadModule;
import com.hosv3.utility.Splash;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Surachai Thowong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class Main implements ManageSystemResp, Runnable {

    /**
     * ���к����� GUI ��й�� ��ʶҹ������Դ���
     */
    private Splash theSplash = new Splash();
    Vector vModule;
    FrameMain aFrameMain;
    FrameSetup aFrameSetup;
    HosPanel theHP;
    HosDialog theHD;
    HosControl theHC;
    String[] args;
    String user;
    char[] password;
    private final boolean setup_mode;

    @SuppressWarnings({"LeakingThisInConstructor", "CallToThreadStartDuringObjectConstruction"})
    public Main(String[] args, boolean setup_mode) {
        Logger.getGlobal().setLevel(Level.INFO);
        this.args = args;
        this.setup_mode = setup_mode;
        theSplash.setVisible(true);
        Thread thread = new Thread(this);
        thread.start();
    }

    /**
     * �������к�
     *
     * @param str
     * @param status
     */
    @Override
    public void notifyLogin(String str, int status) {
        theHD = new HosDialog(theHC, aFrameMain);
        theHP = new HosPanel(theHD, theHC, aFrameMain);
        aFrameMain.setExtendedState(JFrame.MAXIMIZED_BOTH);
        aFrameMain.setSplash(theSplash);
        aFrameMain.setLocationRelativeTo(null);
        aFrameMain.setControl(theHC, theHP, theHD);
        theSplash.setVisible(false);
        try {
            aFrameMain.showModule(ReadModule.loadModule(args, Config.MODULE_PATH, null));
        } catch (Exception e) {
            aFrameMain.confirmBox(Constant.getTextBundle("����ʴ��Ţͧ�����������Դ��Ҵ") + " "
                    + Constant.getTextBundle("��سҵԴ��ͼ������к�"), UpdateStatus.WARNING);
            LOG.log(Level.SEVERE, e.getMessage(), e);
        }
        aFrameMain.setVisible(true);
        // thread to show repay warnning dialog (੾�С���Թ��ҹ��)
        if (Authentication.CASH.equals(theHC.theHO.theEmployee.authentication_id)) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    List<InsuranceClaim> insuranceClaims = theHC.theSystemControl.listInsuranceClaimAlertRepay();
                    if (!insuranceClaims.isEmpty()) {
                        DialogInsuraceRepayWarnning dirw = new DialogInsuraceRepayWarnning(aFrameMain, true);
                        dirw.openDialog(theHC, insuranceClaims);
                    }
                }
            });
            thread.start();
        }
    }

    @Override
    public void notifyLoginSetup(String str, int status) {
        aFrameSetup = new FrameSetup();
        aFrameSetup.setStatus("ʶҹ�������������ҹ", UpdateStatus.COMPLETE);
        theHC.setUpdateStatus(aFrameSetup);
        theHD = new HosDialog(theHC, aFrameSetup);
        aFrameSetup.setControl(theHD, theHC);
        theSplash.setVisible(false);
        boolean ret = aFrameSetup.setModule(ReadModule.loadModule(args, Config.MODULE_PATH, Config.MODULE_PATH_RP));
        if (!ret) {
            aFrameSetup.confirmBox(Constant.getTextBundle("����ʴ��Ţͧ�����������Դ��Ҵ") + " "
                    + Constant.getTextBundle("��سҵԴ��ͼ������к�"), UpdateStatus.WARNING);
        }
//        aFrameSetup.setSize(800, 640);
        aFrameSetup.setLocationRelativeTo(null);
        aFrameSetup.setVisible(true);
    }

    /**
     * ��Ǩ�ͺ��� parameter �����������ͧ������������������ѹ
     * ����������͹���ѧ�����ҹ �������
     *
     * @ 26jul04 henbe add for check external module
     */
    private boolean isCheckVersion(String[] args) {
        for (String arg : args) {
            if (arg.startsWith("-version=false")) {
                return false;
            }
        }
        return true;
    }

    /**
     * �͡�ҡ�����
     *
     * @param str
     * @param status
     */
    @Override
    public void notifyExit(String str, int status) {

    }

    @Override
    public void notifyLogout(String str, int status) {
        if (JOptionPane.showConfirmDialog(aFrameMain,
                ResourceBundle.getBundle(Constant.LANGUAGE).getString("DIALOG_CONFIRM_LOGOUT_PROGRAM"),
                ResourceBundle.getBundle(Constant.LANGUAGE).getString("DIALOG_CONFIRM_TITLE"),
                JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
        }
    }
    private static final Logger LOG = Logger.getLogger(Main.class.getName());

    @Override
    public void run() {
        Config config = new Config();
        aFrameMain = new FrameMain();
        // check new version from server
        HosUpdate.checkUpdate(theSplash);
        // create database connection from file
        ConnectionInf con_inf = config.getConnectionInfFromFile(args, theSplash, aFrameMain);
        if (con_inf == null) {
            System.exit(0);
        }
        // init HosControl
        theHC = new HosControl(con_inf);
        // register from receive notification
        theHC.theHS.theSystemSubject.attachManageSystem(this);
        // check mode to load ui
        if (setup_mode) {
            theHC.theHO.running_program = HosObject.SETUPAPP;
        } else {
            theHC.theHO.running_program = HosObject.MAINAPP;
        }
        theSplash.setVisible(false);
        // check database version is match program version?
        theHC.theSystemControl.checkVersion(aFrameMain, false, setup_mode);
        if (setup_mode) {
            // check database structure is correct?
            theHC.theSystemControl.checkAlert(aFrameMain);
        }
        // show login ui
        ServicePoint sp = null;
        // show news
        theSplash.showNews(con_inf);
        if (user == null || password == null) {
            if (setup_mode) {
                if (!FrameLogin.showDialog(aFrameMain, theHC, user, password, sp, 2)) {
                    System.exit(0);
                }
            } else {
                if (!FrameLogin.showDialog(aFrameMain, theHC, user, password, sp, 1)) {
                    System.exit(0);
                }
            }
        }

        if (isCheckVersion(args)) {
            if (theHC.theSystemControl.setVersion(aFrameMain, false, theSplash)) {
                if (!theHC.theSystemControl.checkVersion(aFrameMain, false, setup_mode)) {
                    theSplash.setVisible(false);
                    System.exit(0);
                }
            } else {
                System.exit(0);
            }
        }
        // check have hospital profile?
        if (theHC.theHO.theSite == null) {
            theSplash.setVisible(false);
            aFrameMain.confirmBox(Constant.getTextBundle("��辺������ʶҹ��Һ��") + " "
                    + Constant.getTextBundle("��سҺѹ�֡������ʶҹ��Һ�š�͹��ҹ�����"), UpdateStatus.WARNING);
            System.exit(0);
        }
        // check have hospital code?
        try {
            Integer.parseInt(theHC.theHO.theSite.off_id);
        } catch (Exception e) {
            theSplash.setVisible(false);
            aFrameMain.confirmBox(Constant.getTextBundle("����ʶҹ��Һ�żԴ�ٻẺ") + " "
                    + Constant.getTextBundle("��سҺѹ�֡������ʶҹ��Һ�š�͹��ҹ�����"), UpdateStatus.WARNING);
            System.exit(0);
        }
        // check length of hospital code = 5?
        if (theHC.theHO.theSite.off_id.length() != 5) {
            theSplash.setVisible(false);
            if (!aFrameMain.confirmBox(Constant.getTextBundle("����ʶҹ��Һ�����١��ͧ") + " "
                    + Constant.getTextBundle("��سҺѹ�֡������ʶҹ��Һ�š�͹��ҹ�����"), UpdateStatus.WARNING)) {
                System.exit(0);
            }
        }
        // warinning if hospital code is 00000 (default by program)
        if (theHC.theHO.theSite.off_id.equals("00000")) {
            theSplash.setVisible(false);
            boolean has_hid = aFrameMain.confirmBox(Constant.getTextBundle("����ʶҹ��Һ�����١��ͧ") + " "
                    + Constant.getTextBundle("��سҺѹ�֡������ʶҹ��Һ�š�͹��ҹ�����"), UpdateStatus.WARNING);
            if (!has_hid) {
                System.exit(0);
            }
        }
        theSplash.setVisible(true);
        // do login process by ui mode
        if (setup_mode) {
            theHC.theSystemControl.loginSetup(user, password, sp);
        } else {
            theHC.setUpdateStatus(aFrameMain);
            theHC.theSystemControl.login(user, password, sp);
        }
        theSplash = null;
    }
}

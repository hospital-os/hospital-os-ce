/*
 * PanelPerson.java
 *
 * Created on 8 �ѹ��¹ 2548, 12:11 �.
 */
package com.hosv3.gui.panel.transaction;

import com.hospital_os.object.*;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hospital_os.utility.*;
import com.hosv3.control.*;
import com.hosv3.control.lookup.*;
import com.hosv3.gui.dialog.HosDialog;
import com.hosv3.gui.panel.detail.PanelLookup;
import com.hosv3.gui.panel.transaction.inf.PanelButtonPluginManager;
import com.hosv3.gui.panel.transaction.inf.PanelButtonPluginProvider;
import com.hosv3.object.HosObject;
import com.hosv3.object.Occupation2;
import com.hosv3.usecase.transaction.ManageLabXrayResp;
import com.hosv3.usecase.transaction.ManagePatientResp;
import com.hosv3.usecase.transaction.ManageServiceLoader;
import com.hosv3.usecase.transaction.ManageVisitResp;
import com.hosv3.utility.GuiLang;
import com.pcu.object.Family;
import com.pcu.object.Home;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import sd.util.LengthDocument;

/**
 *
 * @author jao
 */
@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public class PanelPersonData extends javax.swing.JPanel
        implements ManagePatientResp, ManageVisitResp, ManageLabXrayResp,
        ManageServiceLoader {

    private static final Logger LOG = Logger.getLogger(PanelPersonData.class.getName());
    private static final long serialVersionUID = 1L;
    protected Patient thePatient;
    protected Family theFamily;
    protected Home theHome;
    protected PatientControl thePatientControl;
    protected LookupControl theLookupControl;
    protected HosDialog theHosDialog;
    protected UpdateStatus theUS;
    protected MapCon theMC;
    protected ResultControl theResultControl;
    protected VisitControl theVisitControl;
    protected HosObject theHO;
    protected HosControl theHC;
    protected Vector vNCD;
    protected PrintControl thePrintControl;
    private ComboBoxBlueRenderer blueRenderer = new ComboBoxBlueRenderer();

    private final DocumentListener dlFnameEN = new DocumentListener() {
        @Override
        public void insertUpdate(DocumentEvent e) {
            updateField(e, "insert");
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            updateField(e, "remove");
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            updateField(e, "update");
        }

        public void updateField(DocumentEvent e, String action) {
            Document doc = (Document) e.getDocument();
            String propName = (String) doc.getProperty("name");
            if (propName.equals("firstNameEn")) {
                txtPassportFname.setText(txtFirstNameEng.getText());
            } else if (propName.equals("lastNameEn")) {
                txtPassportLName.setText(txtLastNameEng.getText());
            } else if (propName.equals("passportNo")) {
                txtPassportNo.setText(txtPassport.getText());
            }
        }
    };

    public PanelPersonData() {
        this.initComponents();
        // set blue filed
        Color blue = new Color(204, 255, 255);
        jComboBoxOccup.getEditor().getEditorComponent().setBackground(blue);
        ((JTextField) jComboBoxOccup.getEditor().getEditorComponent()).setOpaque(true);
        ((JTextField) jComboBoxOccup.getEditor().getEditorComponent()).setBackground(blue);
        jComboBoxNation.getEditor().getEditorComponent().setBackground(blue);
        ((JTextField) jComboBoxNation.getEditor().getEditorComponent()).setOpaque(true);
        ((JTextField) jComboBoxNation.getEditor().getEditorComponent()).setBackground(blue);
        jComboBoxRace.getEditor().getEditorComponent().setBackground(blue);
        ((JTextField) jComboBoxRace.getEditor().getEditorComponent()).setOpaque(true);
        ((JTextField) jComboBoxRace.getEditor().getEditorComponent()).setBackground(blue);

        panelAddressPatient.setEnableBlueField(true);

        try {
            Class.forName("com.hosos.module.checkup.CheckupModule");
            chkbSendEmailInvite.setVisible(true);
        } catch (ClassNotFoundException ex) {
            // No checkup module
            chkbSendEmailInvite.setVisible(false);
        }

        // register document listeners
        txtFirstNameEng.getDocument().addDocumentListener(dlFnameEN);
        txtFirstNameEng.getDocument().putProperty("name", "firstNameEn");
        txtLastNameEng.getDocument().addDocumentListener(dlFnameEN);
        txtLastNameEng.getDocument().putProperty("name", "lastNameEn");
        txtPassport.getDocument().addDocumentListener(dlFnameEN);
        txtPassport.getDocument().putProperty("name", "passportNo");
    }

    public void setControl(HosControl hc, HosDialog hd, UpdateStatus us) {
        theHC = hc;
        theHosDialog = hd;
        theMC = new MapCon(MapCon.LOOK_PERSON, theUS, hc.theConnectionInf);
        theResultControl = theHC.theResultControl;
        theVisitControl = theHC.theVisitControl;
        thePrintControl = theHC.thePrintControl;
        theHO = theHC.theHO;
        thePatientControl = theHC.thePatientControl;
        theLookupControl = theHC.theLookupControl;
        theUS = us;
        pidPanel.setComponentColor(204, 255, 255);
        theHC.theHS.thePatientSubject.attachManagePatient(this);
        theHC.theHS.theVisitSubject.attachManageVisit(this);
        theHC.theHS.theResultSubject.attachManageLab(this);
        theHC.theHS.theResultSubject.attachManageXray(this);
        theHC.theHS.theServiceLoaderSubject.attachManage(this);
        initDatas();
        setDefaultComboBox();
        setLanguage();
        setDefaultLocation();
        setFamily(null, null, null, null, null);
        setPatient(null);
    }

    public void initDatas() {
        jComboBoxPrename.setControl(new PrefixLookup(theLookupControl), true);
        jComboBoxOccup.setControl(null, new OccupationLookup(theHC.theLookupControl), new Occupation2());
        jComboBoxRace.setControl(null, new NationLookup(theHC.theLookupControl), new Nation());
        jComboBoxNation.setControl(null, new NationLookup(theHC.theLookupControl), new Nation());
        jComboBoxRelation.setControl(new RelationContractLookup(theHC.theLookupControl), true);
        ComboboxModel.initComboBox(jComboBoxBlood, theHC.theLookupControl.listBlood());
        ComboboxModel.initComboBox(jComboBoxRH, theHC.theLookupControl.listRHGroup());
        ComboboxModel.initComboBox(jComboBoxGender, theHC.theLookupControl.listGender());
        ComboboxModel.initComboBox(jComboBoxMarriage, theHC.theLookupControl.listMarriage());
        ComboboxModel.initComboBox(jComboBoxReligion, theHC.theLookupControl.listReligion());
        ComboboxModel.initComboBox(jComboBoxEducate, theHC.theLookupControl.listEducate());
        ComboboxModel.initComboBox(jComboBoxTypeArea, theHC.theLookupControl.listTypeArea());
        ComboboxModel.initComboBox(this.jcbForeignerType, theHC.theLookupControl.listPersonForeignerType());
        ComboboxModel.initComboBox(this.jcbForeignerNoType, theHC.theLookupControl.listForeignerNoType());
        ComboboxModel.initComboBox(jComboBoxContactSex, theHC.theLookupControl.listGender());
        panelAddressPatient.setControl(theHC, theUS);
        panelAddressContact.setControl(theHC, theUS);
        panelAddressEmployer.setControl(theHC, theUS);
        panelExtraHosWingInfo.setVisible(theHC.theHO.theSite.is_hospital_wing);
        if (theHC.theHO.theSite.is_hospital_wing) {
            ComboboxModel.initComboBox(jComboBoxAffiliated, theHC.theLookupControl.listPersonAffiliateds());
            ComboboxModel.initComboBox(jComboBoxRank, theHC.theLookupControl.listPersonRanks());
            ComboboxModel.initComboBox(jComboBoxJobType, theHC.theLookupControl.listPersonJobTypes());
        }
    }

    public void updateData() {
        setFamily(theHO == null ? null : theHO.theFamily,
                theHO == null ? null : theHO.theHome,
                theHO == null ? null : theHO.theFamilyFather,
                theHO == null ? null : theHO.theFamilyMother,
                theHO == null ? null : theHO.theFamilyCouple);
        setPatient(theHO == null ? null : theHO.thePatient);
        // check  button plugin isvisble when select patient or visit
        for (PanelButtonPluginProvider buttonPluginProvider : buttonPluginProviders) {
            buttonPluginProvider.getButton(PanelPersonData.class).setVisible(buttonPluginProvider.isVisible());
        }
    }

    protected void setPatient(Patient pat) {
        thePatient = pat;
        if (thePatient == null) {
            if (theHO.theFamily != null) {
                thePatient = theHO.initPatient(theHO.theFamily, theHO.theHome);
            } else {
                thePatient = theHO.initPatient();
            }
            jLabelPID.setText("PID");
        }
        if (thePatient.sex_contact != null && !thePatient.sex_contact.isEmpty()) {
            Gutil.setGuiData(jComboBoxContactSex, thePatient.sex_contact);
        }
        Gutil.setGuiData(jTextFieldContactFname, thePatient.contact_fname);
        Gutil.setGuiData(jTextFieldContactLname, thePatient.contact_lname);
        if (thePatient.relation != null && !thePatient.relation.isEmpty()) {
            jComboBoxRelation.setText(thePatient.relation);
        }
        if (thePatient.deny_allergy.equals("1")) {
            rbtnDrugAllergy1.setSelected(true);
        } else if (thePatient.deny_allergy.equals("2")) {
            rbtnDrugAllergy2.setSelected(true);
        } else if (thePatient.deny_allergy.equals("3")) {
            rbtnDrugAllergy3.setSelected(true);
        } else {
            buttonGroupDrugAllergy.clearSelection();
        }
        panelAddressPatient.setAddress(thePatient.house, thePatient.village, thePatient.road, thePatient.phone,
                thePatient.mobile_phone, thePatient.changwat, thePatient.ampur, thePatient.tambon,
                thePatient.patient_postcode, thePatient.patient_address_eng,
                thePatient.is_other_country, thePatient.other_address,
                String.valueOf(thePatient.latitude),
                String.valueOf(thePatient.longitude), thePatient.patient_email
        );
        panelAddressContact.setAddress(thePatient.house_contact, thePatient.village_contact,
                thePatient.road_contact, thePatient.phone_contact, thePatient.contact_mobile_phone,
                thePatient.changwat_contact, thePatient.ampur_contact, thePatient.tambon_contact, thePatient.patient_contact_postcode, "", thePatient.contact_email);
        if (theHO.theFamily != null) {
            jLabelPID.setText("PID:" + theHO.theFamily.hn_hcis);
        }
        jTextFieldPrivateDoctor.setText(thePatient.private_doc);
        if (chkbSendEmailInvite.isVisible()) {
            chkbSendEmailInvite.setSelected(thePatient.sent_email_invitation.equals("1"));
        }
    }

    public void setDefaultLocation() {
        panelAddressPatient.setAddress("", "", "", "", "", this.theHO.theSite.changwat, this.theHO.theSite.amphor, this.theHO.theSite.tambon, "", "");
        panelAddressContact.setAddress("", "", "", "", "", this.theHO.theSite.changwat, this.theHO.theSite.amphor, this.theHO.theSite.tambon, "", "");
        panelAddressEmployer.setAddress("", "", "", "", "", this.theHO.theSite.changwat, this.theHO.theSite.amphor, this.theHO.theSite.tambon, "", "");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroupFm = new javax.swing.ButtonGroup();
        buttonGroupPt = new javax.swing.ButtonGroup();
        buttonGroupMed = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        buttonGroup3 = new javax.swing.ButtonGroup();
        buttonGroupDrugAllergy = new javax.swing.ButtonGroup();
        jPanel10 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jPanel4 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jComboBoxPrename = new com.hosv3.gui.component.HosComboBox();
        jTextFieldFname = new javax.swing.JTextField();
        jTextFieldLname = new javax.swing.JTextField();
        jPanel15 = new javax.swing.JPanel();
        lblInterName = new javax.swing.JLabel();
        txtFirstNameEng = new javax.swing.JTextField();
        txtLastNameEng = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jLabelCID = new javax.swing.JLabel();
        pidPanel = new com.hosv3.gui.component.PIDPanel();
        jLabelPID = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        txtPassport = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabelGender = new javax.swing.JLabel();
        jComboBoxGender = new javax.swing.JComboBox();
        jCheckBoxTrueBirthday = new javax.swing.JCheckBox();
        jLabelAge = new javax.swing.JLabel();
        jTextFieldAge = new com.hospital_os.utility.IntegerTextField();
        jLabel3 = new javax.swing.JLabel();
        jdcDOB = new sd.comp.jcalendar.JDateChooser();
        jPanel6 = new javax.swing.JPanel();
        jComboBoxReligion = new javax.swing.JComboBox();
        jLabelEducate = new javax.swing.JLabel();
        jComboBoxEducate = new javax.swing.JComboBox();
        jLabelReligion = new javax.swing.JLabel();
        jLabelNation = new javax.swing.JLabel();
        jLabelRace = new javax.swing.JLabel();
        jComboBoxRace = new com.hosv3.gui.component.HosComboBox();
        jComboBoxNation = new com.hosv3.gui.component.HosComboBox();
        jLabelOccup = new javax.swing.JLabel();
        jComboBoxOccup = new com.hosv3.gui.component.HosComboBox();
        jLabelBlood = new javax.swing.JLabel();
        jComboBoxBlood = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        jComboBoxRH = new javax.swing.JComboBox();
        jComboBoxMarriage = new javax.swing.JComboBox();
        jLabelMarriage = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jTextFieldRevenue = new com.hospital_os.utility.IntegerTextField();
        jLabel5 = new javax.swing.JLabel();
        panelExtraHosWingInfo = new javax.swing.JPanel();
        jComboBoxJobType = new javax.swing.JComboBox();
        jLabelMarriage3 = new javax.swing.JLabel();
        jComboBoxRank = new javax.swing.JComboBox();
        jLabelMarriage1 = new javax.swing.JLabel();
        jLabelMarriage2 = new javax.swing.JLabel();
        jComboBoxAffiliated = new javax.swing.JComboBox();
        chkbSendEmailInvite = new javax.swing.JCheckBox();
        lblInterName1 = new javax.swing.JLabel();
        txtSkinColor = new javax.swing.JTextField();
        jPanel7 = new javax.swing.JPanel();
        jTextFieldFatherFname = new javax.swing.JTextField();
        jTextFieldFatherLname = new javax.swing.JTextField();
        jTextFieldMotherFname = new javax.swing.JTextField();
        jTextFieldMotherLname = new javax.swing.JTextField();
        jTextFieldCoupleFname = new javax.swing.JTextField();
        jTextFieldCoupleLname = new javax.swing.JTextField();
        jButtonFather = new javax.swing.JButton();
        jButtonMother = new javax.swing.JButton();
        jButtonCouple = new javax.swing.JButton();
        jButtonCancelCouple = new javax.swing.JButton();
        jButtonCancelMother = new javax.swing.JButton();
        jButtonCancelFather = new javax.swing.JButton();
        jLabelFatherName = new javax.swing.JLabel();
        jLabelMotherName = new javax.swing.JLabel();
        jLabelCoupleName = new javax.swing.JLabel();
        jLabelPrivateDoctor = new javax.swing.JLabel();
        jTextFieldPrivateDoctor = new javax.swing.JTextField();
        jPanel13 = new javax.swing.JPanel();
        rbtnDrugAllergy1 = new javax.swing.JRadioButton();
        rbtnDrugAllergy2 = new javax.swing.JRadioButton();
        rbtnDrugAllergy3 = new javax.swing.JRadioButton();
        jPanel16 = new javax.swing.JPanel();
        jComboBoxTypeArea = new javax.swing.JComboBox();
        jLabelFstatus = new javax.swing.JLabel();
        panelPersonForeignerInfo = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        txtPassportNo = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jdcPassportExp = new sd.comp.jcalendar.JDateChooser();
        jPanel12 = new javax.swing.JPanel();
        rbtnForeignerGroup1 = new javax.swing.JRadioButton();
        rbtnForeignerGroup2 = new javax.swing.JRadioButton();
        jPanel8 = new javax.swing.JPanel();
        txtPassportFname = new javax.swing.JTextField();
        txtPassportLName = new javax.swing.JTextField();
        panelExtraPersonForienerInfo = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        txtForeignerNo = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jdcForeignerNoExp = new sd.comp.jcalendar.JDateChooser();
        jLabel13 = new javax.swing.JLabel();
        jcbForeignerNoType = new javax.swing.JComboBox();
        jLabelLabor = new javax.swing.JLabel();
        jcbForeignerType = new javax.swing.JComboBox();
        jLabel15 = new javax.swing.JLabel();
        txtEmployerName = new javax.swing.JTextField();
        jPanel9 = new javax.swing.JPanel();
        rbtnEmployerType1 = new javax.swing.JRadioButton();
        rbtnEmployerType2 = new javax.swing.JRadioButton();
        jLabel14 = new javax.swing.JLabel();
        txtEmployerCID = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        txtEmployerReg = new javax.swing.JTextField();
        panelAddressEmployer = new com.hosv3.gui.component.PanelAddress();
        jScrollPane1 = new javax.swing.JScrollPane();
        jPanelDetail = new javax.swing.JPanel();
        panelAddressPatient = new com.hosv3.gui.component.PanelAddress();
        jPanelContact = new javax.swing.JPanel();
        jPanel25 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jComboBoxContactSex = new javax.swing.JComboBox();
        jComboBoxRelation = new com.hosv3.gui.component.HosComboBox();
        btnCopyAddress = new javax.swing.JButton();
        panelAddressContact = new com.hosv3.gui.component.PanelAddress();
        jLabelContact = new javax.swing.JLabel();
        jTextFieldContactFname = new javax.swing.JTextField();
        jTextFieldContactLname = new javax.swing.JTextField();
        jPanelAction = new javax.swing.JPanel();
        jButtonActive = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jButtonReset2 = new javax.swing.JButton();
        jButtonSearchPatient1 = new javax.swing.JButton();
        jButtonDelete1 = new javax.swing.JButton();
        jButtonPrintOPDCard = new javax.swing.JButton();
        jButtonSave1 = new javax.swing.JButton();
        jCheckBoxCheck = new javax.swing.JCheckBox();
        panelButtonPlugin = new javax.swing.JPanel();
        jButtonIndex = new javax.swing.JButton();

        setLayout(new java.awt.GridBagLayout());

        jPanel10.setMaximumSize(new java.awt.Dimension(725, 2147483647));
        jPanel10.setMinimumSize(new java.awt.Dimension(760, 600));
        jPanel10.setPreferredSize(new java.awt.Dimension(760, 600));
        jPanel10.setLayout(new java.awt.GridBagLayout());

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("�����Ż�Ъҡ�"));
        jPanel4.setLayout(new java.awt.GridBagLayout());

        jPanel1.setLayout(new java.awt.GridBagLayout());

        jComboBoxPrename.setFont(jComboBoxPrename.getFont());
        jComboBoxPrename.setMinimumSize(new java.awt.Dimension(80, 21));
        jComboBoxPrename.setPreferredSize(new java.awt.Dimension(80, 21));
        jComboBoxPrename.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxPrenameActionPerformed(evt);
            }
        });
        jComboBoxPrename.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jComboBoxPrenameFocusLost(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(jComboBoxPrename, gridBagConstraints);

        jTextFieldFname.setBackground(new java.awt.Color(204, 255, 255));
        jTextFieldFname.setFont(jTextFieldFname.getFont());
        jTextFieldFname.setPreferredSize(new java.awt.Dimension(6, 22));
        jTextFieldFname.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldFnameActionPerformed(evt);
            }
        });
        jTextFieldFname.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextFieldFnameFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(jTextFieldFname, gridBagConstraints);

        jTextFieldLname.setBackground(new java.awt.Color(204, 255, 255));
        jTextFieldLname.setFont(jTextFieldLname.getFont());
        jTextFieldLname.setMinimumSize(new java.awt.Dimension(6, 22));
        jTextFieldLname.setPreferredSize(new java.awt.Dimension(6, 22));
        jTextFieldLname.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldLnameActionPerformed(evt);
            }
        });
        jTextFieldLname.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextFieldLnameFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(jTextFieldLname, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel4.add(jPanel1, gridBagConstraints);

        jPanel15.setLayout(new java.awt.GridBagLayout());

        lblInterName.setFont(lblInterName.getFont());
        lblInterName.setText("����-ʡ��� Passport");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel15.add(lblInterName, gridBagConstraints);

        txtFirstNameEng.setFont(txtFirstNameEng.getFont());
        txtFirstNameEng.setName("firstNameEn"); // NOI18N
        txtFirstNameEng.setPreferredSize(new java.awt.Dimension(6, 22));
        txtFirstNameEng.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtFirstNameEngFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel15.add(txtFirstNameEng, gridBagConstraints);

        txtLastNameEng.setFont(txtLastNameEng.getFont());
        txtLastNameEng.setMinimumSize(new java.awt.Dimension(6, 22));
        txtLastNameEng.setName("lastNameEn"); // NOI18N
        txtLastNameEng.setPreferredSize(new java.awt.Dimension(6, 22));
        txtLastNameEng.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtLastNameEngFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel15.add(txtLastNameEng, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel4.add(jPanel15, gridBagConstraints);

        jPanel2.setLayout(new java.awt.GridBagLayout());

        jLabelCID.setFont(jLabelCID.getFont());
        jLabelCID.setText("CID");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(jLabelCID, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(pidPanel, gridBagConstraints);

        jLabelPID.setFont(jLabelPID.getFont().deriveFont(jLabelPID.getFont().getStyle() | java.awt.Font.BOLD));
        jLabelPID.setForeground(new java.awt.Color(0, 0, 204));
        jLabelPID.setText("PID");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 10, 2, 2);
        jPanel2.add(jLabelPID, gridBagConstraints);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/file-copy-icon.png"))); // NOI18N
        jButton1.setToolTipText("�Ѵ�͡�����Ţ�ѵû�ЪҪ�");
        jButton1.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButton1.setMaximumSize(new java.awt.Dimension(28, 28));
        jButton1.setMinimumSize(new java.awt.Dimension(28, 28));
        jButton1.setPreferredSize(new java.awt.Dimension(28, 28));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(jButton1, gridBagConstraints);

        txtPassport.setColumns(15);
        txtPassport.setDocument(new LengthDocument(10));
        txtPassport.setMinimumSize(new java.awt.Dimension(120, 20));
        txtPassport.setName("passport"); // NOI18N
        txtPassport.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPassportKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(txtPassport, gridBagConstraints);

        jLabel17.setFont(jLabel17.getFont());
        jLabel17.setText("Passport No.");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 30, 2, 2);
        jPanel2.add(jLabel17, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel4.add(jPanel2, gridBagConstraints);

        jPanel3.setLayout(new java.awt.GridBagLayout());

        jLabelGender.setFont(jLabelGender.getFont());
        jLabelGender.setText("Sex");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jLabelGender, gridBagConstraints);

        jComboBoxGender.setBackground(new java.awt.Color(204, 255, 255));
        jComboBoxGender.setFont(jComboBoxGender.getFont());
        jComboBoxGender.setRenderer(blueRenderer);
        jComboBoxGender.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jComboBoxGenderFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jComboBoxGender, gridBagConstraints);

        jCheckBoxTrueBirthday.setFont(jCheckBoxTrueBirthday.getFont());
        jCheckBoxTrueBirthday.setText("�ѹ�Դ");
        jCheckBoxTrueBirthday.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxTrueBirthdayActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jCheckBoxTrueBirthday, gridBagConstraints);

        jLabelAge.setFont(jLabelAge.getFont());
        jLabelAge.setText("Age");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jLabelAge, gridBagConstraints);

        jTextFieldAge.setColumns(3);
        jTextFieldAge.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldAge.setFont(jTextFieldAge.getFont());
        jTextFieldAge.setMinimumSize(new java.awt.Dimension(39, 22));
        jTextFieldAge.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldAgeKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jTextFieldAge, gridBagConstraints);

        jLabel3.setFont(jLabel3.getFont());
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Year");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jLabel3, gridBagConstraints);

        jdcDOB.setFont(jdcDOB.getFont());
        jdcDOB.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jdcDOBPropertyChange(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jdcDOB, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel4.add(jPanel3, gridBagConstraints);

        jPanel6.setLayout(new java.awt.GridBagLayout());

        jComboBoxReligion.setBackground(new java.awt.Color(204, 255, 255));
        jComboBoxReligion.setFont(jComboBoxReligion.getFont());
        jComboBoxReligion.setRenderer(blueRenderer);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jComboBoxReligion, gridBagConstraints);

        jLabelEducate.setFont(jLabelEducate.getFont());
        jLabelEducate.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabelEducate.setText("����֡��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jLabelEducate, gridBagConstraints);

        jComboBoxEducate.setBackground(new java.awt.Color(204, 255, 255));
        jComboBoxEducate.setFont(jComboBoxEducate.getFont());
        jComboBoxEducate.setRenderer(blueRenderer);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jComboBoxEducate, gridBagConstraints);

        jLabelReligion.setFont(jLabelReligion.getFont());
        jLabelReligion.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabelReligion.setText("��ʹ�");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jLabelReligion, gridBagConstraints);

        jLabelNation.setFont(jLabelNation.getFont());
        jLabelNation.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabelNation.setText("���ͪҵ�");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jLabelNation, gridBagConstraints);

        jLabelRace.setFont(jLabelRace.getFont());
        jLabelRace.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabelRace.setText("�ѭ�ҵ�");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jLabelRace, gridBagConstraints);

        jComboBoxRace.setDoubleBuffered(true);
        jComboBoxRace.setFont(jComboBoxRace.getFont());
        jComboBoxRace.setRenderer(blueRenderer);
        jComboBoxRace.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jComboBoxRaceKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jComboBoxRace, gridBagConstraints);

        jComboBoxNation.setDoubleBuffered(true);
        jComboBoxNation.setFont(jComboBoxNation.getFont());
        jComboBoxNation.setRenderer(blueRenderer);
        jComboBoxNation.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxNationActionPerformed(evt);
            }
        });
        jComboBoxNation.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jComboBoxNationKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jComboBoxNation, gridBagConstraints);

        jLabelOccup.setFont(jLabelOccup.getFont());
        jLabelOccup.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabelOccup.setText("�Ҫվ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jLabelOccup, gridBagConstraints);

        jComboBoxOccup.setFont(jComboBoxOccup.getFont());
        jComboBoxOccup.setRenderer(blueRenderer);
        jComboBoxOccup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxOccupActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jComboBoxOccup, gridBagConstraints);

        jLabelBlood.setFont(jLabelBlood.getFont());
        jLabelBlood.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabelBlood.setText("BloodGroup");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jLabelBlood, gridBagConstraints);

        jComboBoxBlood.setFont(jComboBoxBlood.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 0.5;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jComboBoxBlood, gridBagConstraints);

        jLabel1.setFont(jLabel1.getFont());
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("RH");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jLabel1, gridBagConstraints);

        jComboBoxRH.setFont(jComboBoxRH.getFont());
        jComboBoxRH.setMinimumSize(new java.awt.Dimension(48, 18));
        jComboBoxRH.setPreferredSize(new java.awt.Dimension(48, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 0.5;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jComboBoxRH, gridBagConstraints);

        jComboBoxMarriage.setBackground(new java.awt.Color(204, 255, 255));
        jComboBoxMarriage.setFont(jComboBoxMarriage.getFont());
        jComboBoxMarriage.setRenderer(blueRenderer);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jComboBoxMarriage, gridBagConstraints);

        jLabelMarriage.setFont(jLabelMarriage.getFont());
        jLabelMarriage.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabelMarriage.setText("ʶҹ�Ҿ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jLabelMarriage, gridBagConstraints);

        jPanel11.setLayout(new java.awt.GridBagLayout());

        jLabel2.setFont(jLabel2.getFont());
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("���������͹");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel11.add(jLabel2, gridBagConstraints);

        jTextFieldRevenue.setColumns(10);
        jTextFieldRevenue.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldRevenue.setFont(jTextFieldRevenue.getFont());
        jTextFieldRevenue.setMinimumSize(new java.awt.Dimension(200, 22));
        jTextFieldRevenue.setPreferredSize(new java.awt.Dimension(200, 22));
        jTextFieldRevenue.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextFieldRevenueFocusLost(evt);
            }
        });
        jTextFieldRevenue.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldRevenueKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel11.add(jTextFieldRevenue, gridBagConstraints);

        jLabel5.setFont(jLabel5.getFont());
        jLabel5.setText("�ҷ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel11.add(jLabel5, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        jPanel6.add(jPanel11, gridBagConstraints);

        panelExtraHosWingInfo.setLayout(new java.awt.GridBagLayout());

        jComboBoxJobType.setFont(jComboBoxJobType.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 0.5;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraHosWingInfo.add(jComboBoxJobType, gridBagConstraints);

        jLabelMarriage3.setFont(jLabelMarriage3.getFont());
        jLabelMarriage3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabelMarriage3.setText("�������ҹ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraHosWingInfo.add(jLabelMarriage3, gridBagConstraints);

        jComboBoxRank.setFont(jComboBoxRank.getFont());
        jComboBoxRank.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxRankActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 0.5;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraHosWingInfo.add(jComboBoxRank, gridBagConstraints);

        jLabelMarriage1.setFont(jLabelMarriage1.getFont());
        jLabelMarriage1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabelMarriage1.setText("�����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraHosWingInfo.add(jLabelMarriage1, gridBagConstraints);

        jLabelMarriage2.setFont(jLabelMarriage2.getFont());
        jLabelMarriage2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabelMarriage2.setText("�ѧ�Ѵ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraHosWingInfo.add(jLabelMarriage2, gridBagConstraints);

        jComboBoxAffiliated.setFont(jComboBoxAffiliated.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 0.5;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraHosWingInfo.add(jComboBoxAffiliated, gridBagConstraints);

        chkbSendEmailInvite.setText("��������ԭ�ǹ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraHosWingInfo.add(chkbSendEmailInvite, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel6.add(panelExtraHosWingInfo, gridBagConstraints);

        lblInterName1.setFont(lblInterName1.getFont());
        lblInterName1.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        lblInterName1.setText("�ռ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(lblInterName1, gridBagConstraints);

        txtSkinColor.setFont(txtSkinColor.getFont());
        txtSkinColor.setPreferredSize(new java.awt.Dimension(6, 22));
        txtSkinColor.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtSkinColorFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(txtSkinColor, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel4.add(jPanel6, gridBagConstraints);

        jPanel7.setLayout(new java.awt.GridBagLayout());

        jTextFieldFatherFname.setFont(jTextFieldFatherFname.getFont());
        jTextFieldFatherFname.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextFieldFatherFnameCaretUpdate(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jTextFieldFatherFname, gridBagConstraints);

        jTextFieldFatherLname.setFont(jTextFieldFatherLname.getFont());
        jTextFieldFatherLname.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextFieldFatherLnameCaretUpdate(evt);
            }
        });
        jTextFieldFatherLname.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextFieldFatherLnameFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jTextFieldFatherLname, gridBagConstraints);

        jTextFieldMotherFname.setFont(jTextFieldMotherFname.getFont());
        jTextFieldMotherFname.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextFieldMotherFnameCaretUpdate(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jTextFieldMotherFname, gridBagConstraints);

        jTextFieldMotherLname.setFont(jTextFieldMotherLname.getFont());
        jTextFieldMotherLname.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextFieldMotherLnameCaretUpdate(evt);
            }
        });
        jTextFieldMotherLname.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextFieldMotherLnameFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jTextFieldMotherLname, gridBagConstraints);

        jTextFieldCoupleFname.setFont(jTextFieldCoupleFname.getFont());
        jTextFieldCoupleFname.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextFieldCoupleFnameCaretUpdate(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jTextFieldCoupleFname, gridBagConstraints);

        jTextFieldCoupleLname.setFont(jTextFieldCoupleLname.getFont());
        jTextFieldCoupleLname.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextFieldCoupleLnameCaretUpdate(evt);
            }
        });
        jTextFieldCoupleLname.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextFieldCoupleLnameFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jTextFieldCoupleLname, gridBagConstraints);

        jButtonFather.setFont(jButtonFather.getFont());
        jButtonFather.setText("...");
        jButtonFather.setMaximumSize(new java.awt.Dimension(22, 22));
        jButtonFather.setMinimumSize(new java.awt.Dimension(22, 22));
        jButtonFather.setPreferredSize(new java.awt.Dimension(22, 22));
        jButtonFather.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonFatherActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jButtonFather, gridBagConstraints);

        jButtonMother.setFont(jButtonMother.getFont());
        jButtonMother.setText("...");
        jButtonMother.setMaximumSize(new java.awt.Dimension(22, 22));
        jButtonMother.setMinimumSize(new java.awt.Dimension(22, 22));
        jButtonMother.setPreferredSize(new java.awt.Dimension(22, 22));
        jButtonMother.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonMotherActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jButtonMother, gridBagConstraints);

        jButtonCouple.setFont(jButtonCouple.getFont());
        jButtonCouple.setText("...");
        jButtonCouple.setMaximumSize(new java.awt.Dimension(22, 22));
        jButtonCouple.setMinimumSize(new java.awt.Dimension(22, 22));
        jButtonCouple.setPreferredSize(new java.awt.Dimension(22, 22));
        jButtonCouple.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCoupleActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jButtonCouple, gridBagConstraints);

        jButtonCancelCouple.setFont(jButtonCancelCouple.getFont());
        jButtonCancelCouple.setText("x");
        jButtonCancelCouple.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonCancelCouple.setMaximumSize(new java.awt.Dimension(22, 22));
        jButtonCancelCouple.setMinimumSize(new java.awt.Dimension(22, 22));
        jButtonCancelCouple.setPreferredSize(new java.awt.Dimension(22, 22));
        jButtonCancelCouple.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelCoupleActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jButtonCancelCouple, gridBagConstraints);

        jButtonCancelMother.setFont(jButtonCancelMother.getFont());
        jButtonCancelMother.setText("x");
        jButtonCancelMother.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonCancelMother.setMaximumSize(new java.awt.Dimension(22, 22));
        jButtonCancelMother.setMinimumSize(new java.awt.Dimension(22, 22));
        jButtonCancelMother.setPreferredSize(new java.awt.Dimension(22, 22));
        jButtonCancelMother.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelMotherActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jButtonCancelMother, gridBagConstraints);

        jButtonCancelFather.setFont(jButtonCancelFather.getFont());
        jButtonCancelFather.setText("x");
        jButtonCancelFather.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonCancelFather.setMaximumSize(new java.awt.Dimension(22, 22));
        jButtonCancelFather.setMinimumSize(new java.awt.Dimension(22, 22));
        jButtonCancelFather.setPreferredSize(new java.awt.Dimension(22, 22));
        jButtonCancelFather.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelFatherActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jButtonCancelFather, gridBagConstraints);

        jLabelFatherName.setFont(jLabelFatherName.getFont());
        jLabelFatherName.setText("FatherName");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jLabelFatherName, gridBagConstraints);

        jLabelMotherName.setFont(jLabelMotherName.getFont());
        jLabelMotherName.setText("MotherName");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jLabelMotherName, gridBagConstraints);

        jLabelCoupleName.setFont(jLabelCoupleName.getFont());
        jLabelCoupleName.setText("���ͤ������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jLabelCoupleName, gridBagConstraints);

        jLabelPrivateDoctor.setFont(jLabelPrivateDoctor.getFont());
        jLabelPrivateDoctor.setText("ᾷ���Шӵ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jLabelPrivateDoctor, gridBagConstraints);

        jTextFieldPrivateDoctor.setFont(jTextFieldPrivateDoctor.getFont());
        jTextFieldPrivateDoctor.setMaximumSize(new java.awt.Dimension(100, 21));
        jTextFieldPrivateDoctor.setMinimumSize(new java.awt.Dimension(100, 21));
        jTextFieldPrivateDoctor.setPreferredSize(new java.awt.Dimension(100, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jTextFieldPrivateDoctor, gridBagConstraints);

        jPanel13.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel13.setLayout(new java.awt.GridBagLayout());

        buttonGroupDrugAllergy.add(rbtnDrugAllergy1);
        rbtnDrugAllergy1.setFont(rbtnDrugAllergy1.getFont());
        rbtnDrugAllergy1.setText("����ջ���ѵԡ������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel13.add(rbtnDrugAllergy1, gridBagConstraints);

        buttonGroupDrugAllergy.add(rbtnDrugAllergy2);
        rbtnDrugAllergy2.setFont(rbtnDrugAllergy2.getFont());
        rbtnDrugAllergy2.setText("����Һ����ѵԡ������ ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(1, 6, 1, 1);
        jPanel13.add(rbtnDrugAllergy2, gridBagConstraints);

        buttonGroupDrugAllergy.add(rbtnDrugAllergy3);
        rbtnDrugAllergy3.setFont(rbtnDrugAllergy3.getFont());
        rbtnDrugAllergy3.setText("�ջ���ѵԡ������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 7, 1, 1);
        jPanel13.add(rbtnDrugAllergy3, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        jPanel7.add(jPanel13, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel4.add(jPanel7, gridBagConstraints);

        jPanel16.setLayout(new java.awt.GridBagLayout());

        jComboBoxTypeArea.setBackground(new java.awt.Color(204, 255, 255));
        jComboBoxTypeArea.setFont(jComboBoxTypeArea.getFont());
        jComboBoxTypeArea.setRenderer(blueRenderer);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel16.add(jComboBoxTypeArea, gridBagConstraints);

        jLabelFstatus.setFont(jLabelFstatus.getFont());
        jLabelFstatus.setText("�/�͡ࢵ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel16.add(jLabelFstatus, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel4.add(jPanel16, gridBagConstraints);

        panelPersonForeignerInfo.setBorder(javax.swing.BorderFactory.createTitledBorder("�����źؤ��������ѭ�ҵ���"));
        panelPersonForeignerInfo.setLayout(new java.awt.GridBagLayout());

        jLabel4.setFont(jLabel4.getFont());
        jLabel4.setText("Passport No.");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelPersonForeignerInfo.add(jLabel4, gridBagConstraints);

        txtPassportNo.setEditable(false);
        txtPassportNo.setColumns(15);
        txtPassportNo.setDocument(new LengthDocument(10));
        txtPassportNo.setMinimumSize(new java.awt.Dimension(100, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelPersonForeignerInfo.add(txtPassportNo, gridBagConstraints);

        jLabel6.setFont(jLabel6.getFont());
        jLabel6.setText("�������ؤ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelPersonForeignerInfo.add(jLabel6, gridBagConstraints);

        jLabel9.setFont(jLabel9.getFont());
        jLabel9.setText("����-ʡ��� Passport");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelPersonForeignerInfo.add(jLabel9, gridBagConstraints);

        jLabel10.setFont(jLabel10.getFont());
        jLabel10.setText("�ѹ�������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_END;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelPersonForeignerInfo.add(jLabel10, gridBagConstraints);

        jdcPassportExp.setEnableFuture(true);
        jdcPassportExp.setFont(jdcPassportExp.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelPersonForeignerInfo.add(jdcPassportExp, gridBagConstraints);

        jPanel12.setLayout(new java.awt.GridBagLayout());

        buttonGroup3.add(rbtnForeignerGroup1);
        rbtnForeignerGroup1.setFont(rbtnForeignerGroup1.getFont());
        rbtnForeignerGroup1.setText("�ѡ��ͧ�����/�ѡ�֡��");
        rbtnForeignerGroup1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnForeignerGroup1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel12.add(rbtnForeignerGroup1, gridBagConstraints);

        buttonGroup3.add(rbtnForeignerGroup2);
        rbtnForeignerGroup2.setFont(rbtnForeignerGroup2.getFont());
        rbtnForeignerGroup2.setSelected(true);
        rbtnForeignerGroup2.setText("�ç�ҹ��ҧ����");
        rbtnForeignerGroup2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnForeignerGroup2ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel12.add(rbtnForeignerGroup2, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        panelPersonForeignerInfo.add(jPanel12, gridBagConstraints);

        jPanel8.setLayout(new java.awt.GridBagLayout());

        txtPassportFname.setEditable(false);
        txtPassportFname.setFont(txtPassportFname.getFont());
        txtPassportFname.setPreferredSize(new java.awt.Dimension(6, 22));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel8.add(txtPassportFname, gridBagConstraints);

        txtPassportLName.setEditable(false);
        txtPassportLName.setFont(txtPassportLName.getFont());
        txtPassportLName.setPreferredSize(new java.awt.Dimension(6, 22));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel8.add(txtPassportLName, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        panelPersonForeignerInfo.add(jPanel8, gridBagConstraints);

        panelExtraPersonForienerInfo.setBorder(javax.swing.BorderFactory.createTitledBorder("�������ç�ҹ��ҧ����"));
        panelExtraPersonForienerInfo.setLayout(new java.awt.GridBagLayout());

        jLabel11.setFont(jLabel11.getFont());
        jLabel11.setText("�Ţ����ҧ����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraPersonForienerInfo.add(jLabel11, gridBagConstraints);

        txtForeignerNo.setColumns(9);
        txtForeignerNo.setFont(txtForeignerNo.getFont());
        txtForeignerNo.setMinimumSize(new java.awt.Dimension(105, 22));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraPersonForienerInfo.add(txtForeignerNo, gridBagConstraints);

        jLabel12.setFont(jLabel12.getFont());
        jLabel12.setText("�ѹ�������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_END;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraPersonForienerInfo.add(jLabel12, gridBagConstraints);

        jdcForeignerNoExp.setEnableFuture(true);
        jdcForeignerNoExp.setFont(jdcForeignerNoExp.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraPersonForienerInfo.add(jdcForeignerNoExp, gridBagConstraints);

        jLabel13.setFont(jLabel13.getFont());
        jLabel13.setText("������˹ѧ���");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraPersonForienerInfo.add(jLabel13, gridBagConstraints);

        jcbForeignerNoType.setFont(jcbForeignerNoType.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraPersonForienerInfo.add(jcbForeignerNoType, gridBagConstraints);

        jLabelLabor.setFont(jLabelLabor.getFont());
        jLabelLabor.setText("��������ҧ����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraPersonForienerInfo.add(jLabelLabor, gridBagConstraints);

        jcbForeignerType.setFont(jcbForeignerType.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraPersonForienerInfo.add(jcbForeignerType, gridBagConstraints);

        jLabel15.setFont(jLabel15.getFont());
        jLabel15.setText("���͹�¨�ҧ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraPersonForienerInfo.add(jLabel15, gridBagConstraints);

        txtEmployerName.setFont(txtEmployerName.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraPersonForienerInfo.add(txtEmployerName, gridBagConstraints);

        jPanel9.setLayout(new java.awt.GridBagLayout());

        buttonGroup2.add(rbtnEmployerType1);
        rbtnEmployerType1.setFont(rbtnEmployerType1.getFont());
        rbtnEmployerType1.setSelected(true);
        rbtnEmployerType1.setText("�ؤ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel9.add(rbtnEmployerType1, gridBagConstraints);

        buttonGroup2.add(rbtnEmployerType2);
        rbtnEmployerType2.setFont(rbtnEmployerType2.getFont());
        rbtnEmployerType2.setText("����ѷ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel9.add(rbtnEmployerType2, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        panelExtraPersonForienerInfo.add(jPanel9, gridBagConstraints);

        jLabel14.setFont(jLabel14.getFont());
        jLabel14.setText("CID ��¨�ҧ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraPersonForienerInfo.add(jLabel14, gridBagConstraints);

        txtEmployerCID.setDocument(new sd.comp.textfield.NumberDocument(13));
        txtEmployerCID.setFont(txtEmployerCID.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 0.5;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraPersonForienerInfo.add(txtEmployerCID, gridBagConstraints);

        jLabel16.setFont(jLabel16.getFont());
        jLabel16.setText("˹��¢�鹷���¹");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_END;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraPersonForienerInfo.add(jLabel16, gridBagConstraints);

        txtEmployerReg.setFont(txtEmployerReg.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 0.5;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraPersonForienerInfo.add(txtEmployerReg, gridBagConstraints);

        panelAddressEmployer.setBorder(javax.swing.BorderFactory.createTitledBorder("�����ŷ�������¨�ҧ"));
        panelAddressEmployer.setOtherAddressVisible(false);
        panelAddressEmployer.setPreferredSize(new java.awt.Dimension(400, 220));
        panelAddressEmployer.setShowEmail(false);
        panelAddressEmployer.setShowLatLong(true);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraPersonForienerInfo.add(panelAddressEmployer, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        panelPersonForeignerInfo.add(panelExtraPersonForienerInfo, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel4.add(panelPersonForeignerInfo, gridBagConstraints);

        jScrollPane2.setViewportView(jPanel4);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel10.add(jScrollPane2, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weighty = 1.0;
        add(jPanel10, gridBagConstraints);

        jPanelDetail.setLayout(new java.awt.GridBagLayout());

        panelAddressPatient.setBorder(javax.swing.BorderFactory.createTitledBorder("�����ŷ������"));
        panelAddressPatient.setMaximumSize(new java.awt.Dimension(550, 2147483647));
        panelAddressPatient.setOtherAddressVisible(true);
        panelAddressPatient.setPreferredSize(new java.awt.Dimension(400, 300));
        panelAddressPatient.setShowLatLong(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelDetail.add(panelAddressPatient, gridBagConstraints);

        jPanelContact.setBorder(javax.swing.BorderFactory.createTitledBorder("�����ż��Դ���"));
        jPanelContact.setMaximumSize(new java.awt.Dimension(550, 2147483647));
        jPanelContact.setPreferredSize(new java.awt.Dimension(400, 310));
        jPanelContact.setLayout(new java.awt.GridBagLayout());

        jPanel25.setLayout(new java.awt.GridBagLayout());

        jLabel7.setFont(jLabel7.getFont());
        jLabel7.setText("����Ǣ�ͧ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel25.add(jLabel7, gridBagConstraints);

        jLabel8.setFont(jLabel8.getFont());
        jLabel8.setText("��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel25.add(jLabel8, gridBagConstraints);

        jComboBoxContactSex.setFont(jComboBoxContactSex.getFont());
        jComboBoxContactSex.setMinimumSize(new java.awt.Dimension(70, 21));
        jComboBoxContactSex.setPreferredSize(new java.awt.Dimension(70, 21));
        jComboBoxContactSex.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jComboBoxContactSexKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel25.add(jComboBoxContactSex, gridBagConstraints);

        jComboBoxRelation.setFont(jComboBoxRelation.getFont());
        jComboBoxRelation.setMinimumSize(new java.awt.Dimension(100, 21));
        jComboBoxRelation.setPreferredSize(new java.awt.Dimension(100, 21));
        jComboBoxRelation.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxRelationActionPerformed(evt);
            }
        });
        jComboBoxRelation.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jComboBoxRelationKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel25.add(jComboBoxRelation, gridBagConstraints);

        btnCopyAddress.setFont(btnCopyAddress.getFont());
        btnCopyAddress.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/down.gif"))); // NOI18N
        btnCopyAddress.setToolTipText("����͹������������");
        btnCopyAddress.setMinimumSize(new java.awt.Dimension(25, 25));
        btnCopyAddress.setPreferredSize(new java.awt.Dimension(25, 25));
        btnCopyAddress.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCopyAddressActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        jPanel25.add(btnCopyAddress, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelContact.add(jPanel25, gridBagConstraints);

        panelAddressContact.setBorder(javax.swing.BorderFactory.createTitledBorder("�����ŷ��������Դ���"));
        panelAddressContact.setEnAddressVisible(false);
        panelAddressContact.setOtherAddressVisible(false);
        panelAddressContact.setPreferredSize(new java.awt.Dimension(400, 250));
        panelAddressContact.setShowLatLong(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelContact.add(panelAddressContact, gridBagConstraints);

        jLabelContact.setFont(jLabelContact.getFont());
        jLabelContact.setText("���ͼ��Դ���");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelContact.add(jLabelContact, gridBagConstraints);

        jTextFieldContactFname.setFont(jTextFieldContactFname.getFont());
        jTextFieldContactFname.setMaximumSize(new java.awt.Dimension(88, 21));
        jTextFieldContactFname.setMinimumSize(new java.awt.Dimension(88, 21));
        jTextFieldContactFname.setPreferredSize(new java.awt.Dimension(88, 21));
        jTextFieldContactFname.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextFieldContactFnameFocusLost(evt);
            }
        });
        jTextFieldContactFname.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldContactFnameKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelContact.add(jTextFieldContactFname, gridBagConstraints);

        jTextFieldContactLname.setFont(jTextFieldContactLname.getFont());
        jTextFieldContactLname.setMaximumSize(new java.awt.Dimension(88, 21));
        jTextFieldContactLname.setMinimumSize(new java.awt.Dimension(88, 21));
        jTextFieldContactLname.setPreferredSize(new java.awt.Dimension(88, 21));
        jTextFieldContactLname.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextFieldContactLnameFocusGained(evt);
            }
        });
        jTextFieldContactLname.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldContactLnameKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelContact.add(jTextFieldContactLname, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelDetail.add(jPanelContact, gridBagConstraints);

        jScrollPane1.setViewportView(jPanelDetail);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weighty = 1.0;
        add(jScrollPane1, gridBagConstraints);

        jPanelAction.setLayout(new java.awt.GridBagLayout());

        jButtonActive.setFont(jButtonActive.getFont());
        jButtonActive.setText("Active");
        jButtonActive.setToolTipText("�ӡ�Ѻ��������");
        jButtonActive.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonActive.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonActiveActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelAction.add(jButtonActive, gridBagConstraints);

        jPanel5.setLayout(new java.awt.GridBagLayout());

        jButtonReset2.setFont(jButtonReset2.getFont());
        jButtonReset2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/plus16.gif"))); // NOI18N
        jButtonReset2.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonReset2.setMaximumSize(new java.awt.Dimension(32, 32));
        jButtonReset2.setMinimumSize(new java.awt.Dimension(32, 32));
        jButtonReset2.setPreferredSize(new java.awt.Dimension(32, 32));
        jButtonReset2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonReset2ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel5.add(jButtonReset2, gridBagConstraints);

        jButtonSearchPatient1.setFont(jButtonSearchPatient1.getFont());
        jButtonSearchPatient1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/find.gif"))); // NOI18N
        jButtonSearchPatient1.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonSearchPatient1.setMaximumSize(new java.awt.Dimension(32, 32));
        jButtonSearchPatient1.setMinimumSize(new java.awt.Dimension(32, 32));
        jButtonSearchPatient1.setPreferredSize(new java.awt.Dimension(32, 32));
        jButtonSearchPatient1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSearchPatient1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel5.add(jButtonSearchPatient1, gridBagConstraints);

        jButtonDelete1.setFont(jButtonDelete1.getFont());
        jButtonDelete1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/minus16.gif"))); // NOI18N
        jButtonDelete1.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonDelete1.setMaximumSize(new java.awt.Dimension(32, 32));
        jButtonDelete1.setMinimumSize(new java.awt.Dimension(32, 32));
        jButtonDelete1.setPreferredSize(new java.awt.Dimension(32, 32));
        jButtonDelete1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDelete1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel5.add(jButtonDelete1, gridBagConstraints);

        jButtonPrintOPDCard.setFont(jButtonPrintOPDCard.getFont());
        jButtonPrintOPDCard.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/expenses.gif"))); // NOI18N
        jButtonPrintOPDCard.setToolTipText("OPD Card");
        jButtonPrintOPDCard.setMaximumSize(new java.awt.Dimension(32, 32));
        jButtonPrintOPDCard.setMinimumSize(new java.awt.Dimension(32, 32));
        jButtonPrintOPDCard.setPreferredSize(new java.awt.Dimension(32, 32));
        jButtonPrintOPDCard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPrintOPDCardActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel5.add(jButtonPrintOPDCard, gridBagConstraints);

        jButtonSave1.setFont(jButtonSave1.getFont());
        jButtonSave1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/Save16.gif"))); // NOI18N
        jButtonSave1.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonSave1.setMaximumSize(new java.awt.Dimension(32, 32));
        jButtonSave1.setMinimumSize(new java.awt.Dimension(32, 32));
        jButtonSave1.setPreferredSize(new java.awt.Dimension(32, 32));
        jButtonSave1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSave1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel5.add(jButtonSave1, gridBagConstraints);

        jCheckBoxCheck.setFont(jCheckBoxCheck.getFont());
        jCheckBoxCheck.setText("Check");
        jCheckBoxCheck.setToolTipText("��Ǩ�ͺ������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel5.add(jCheckBoxCheck, gridBagConstraints);

        panelButtonPlugin.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        jPanel5.add(panelButtonPlugin, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelAction.add(jPanel5, gridBagConstraints);

        jButtonIndex.setFont(jButtonIndex.getFont());
        jButtonIndex.setText("� index");
        jButtonIndex.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonIndex.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonIndexActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelAction.add(jButtonIndex, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 0.3;
        add(jPanelAction, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jTextFieldAgeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldAgeKeyReleased
        if (jTextFieldAge.isEnabled() && !jTextFieldAge.getText().isEmpty()) {
            int age = Integer.parseInt(jTextFieldAge.getText());
            int yearCurr = Integer.parseInt(theHO.date_time.substring(0, 4));
            Calendar c = Calendar.getInstance();
            c.set(Calendar.DATE, 1);
            c.set(Calendar.MONTH, 6);// Jan is 0
            c.set(Calendar.YEAR, yearCurr - age);
            jdcDOB.setDate(c.getTime());
        }
    }//GEN-LAST:event_jTextFieldAgeKeyReleased

    private void jComboBoxPrenameFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jComboBoxPrenameFocusLost
        jComboBoxPrenameActionPerformed(null);
    }//GEN-LAST:event_jComboBoxPrenameFocusLost

    private void jComboBoxGenderFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jComboBoxGenderFocusGained
        boolean result = thePatientControl.checkPatientPid(pidPanel.getText());
        if (theFamily != null && theFamily.getObjectId() == null && result) {
            theUS.setStatus("�����Ţ�ѵû�ЪҪ���ӡѺ�����¤���� ��سҡ�͡�����Ţ�ѵû�ЪҪ������ա����", UpdateStatus.WARNING);
            pidPanel.requestFocus();
        }
    }//GEN-LAST:event_jComboBoxGenderFocusGained

    private void jTextFieldLnameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldLnameActionPerformed
        this.jButtonSearchPatient1.doClick();
    }//GEN-LAST:event_jTextFieldLnameActionPerformed

    private void jTextFieldFnameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldFnameActionPerformed
        this.jButtonSearchPatient1.doClick();
    }//GEN-LAST:event_jTextFieldFnameActionPerformed

    private void jTextFieldCoupleLnameCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextFieldCoupleLnameCaretUpdate
        //jTextFieldCoupleName.setText(jTextFieldCoupleFname.getText()+" "+jTextFieldCoupleLname.getText());
    }//GEN-LAST:event_jTextFieldCoupleLnameCaretUpdate

    private void jTextFieldCoupleFnameCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextFieldCoupleFnameCaretUpdate
        // jTextFieldCoupleName.setText(jTextFieldCoupleFname.getText()+" "+jTextFieldCoupleLname.getText());
    }//GEN-LAST:event_jTextFieldCoupleFnameCaretUpdate

    private void jTextFieldMotherLnameCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextFieldMotherLnameCaretUpdate
        // jTextFieldMotherName.setText(jTextFieldMotherFname.getText()+" "+jTextFieldMotherLname.getText());
    }//GEN-LAST:event_jTextFieldMotherLnameCaretUpdate

    private void jTextFieldFatherLnameCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextFieldFatherLnameCaretUpdate
        //jTextFieldFatherName.setText(jTextFieldFatherFname.getText()+" "+jTextFieldFatherLname.getText());
    }//GEN-LAST:event_jTextFieldFatherLnameCaretUpdate

    private void jTextFieldMotherFnameCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextFieldMotherFnameCaretUpdate
        //jTextFieldMotherName.setText(jTextFieldMotherFname.getText()+" "+jTextFieldMotherLname.getText());
    }//GEN-LAST:event_jTextFieldMotherFnameCaretUpdate

    private void jTextFieldCoupleLnameFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextFieldCoupleLnameFocusGained

        if (!jTextFieldCoupleFname.getText().trim().isEmpty()) {
            if (jTextFieldCoupleLname.getText().trim().isEmpty()) {
                Gutil.setGuiData(jTextFieldCoupleLname, Gutil.getGuiData(jTextFieldLname));
            }
        }
    }//GEN-LAST:event_jTextFieldCoupleLnameFocusGained

    private void jTextFieldMotherLnameFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextFieldMotherLnameFocusGained

        if (!jTextFieldMotherFname.getText().trim().isEmpty()) {
            if (jTextFieldMotherLname.getText().trim().isEmpty()) {
                Gutil.setGuiData(jTextFieldMotherLname, Gutil.getGuiData(jTextFieldLname));
            }
        }
    }//GEN-LAST:event_jTextFieldMotherLnameFocusGained

    private void jTextFieldFatherLnameFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextFieldFatherLnameFocusGained

        if (!jTextFieldFatherFname.getText().trim().isEmpty()) {
            if (jTextFieldFatherLname.getText().trim().isEmpty()) {
                Gutil.setGuiData(jTextFieldFatherLname, Gutil.getGuiData(jTextFieldLname));
            }
        }
    }//GEN-LAST:event_jTextFieldFatherLnameFocusGained

    private void jTextFieldFatherFnameCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextFieldFatherFnameCaretUpdate
        //jTextFieldFatherName.setText(jTextFieldFatherFname.getText()+" "+jTextFieldFatherLname.getText());
    }//GEN-LAST:event_jTextFieldFatherFnameCaretUpdate

    private void jComboBoxOccupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxOccupActionPerformed
        //jTextFieldOcc.setText(jComboBoxOccup.getText());
    }//GEN-LAST:event_jComboBoxOccupActionPerformed

    private void jCheckBoxTrueBirthdayActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxTrueBirthdayActionPerformed
        if (jCheckBoxTrueBirthday.isSelected()) {
            jdcDOB.setEnabled(true);
            jTextFieldAge.setEnabled(false);
            jdcDOB.requestFocus();
        } else {
            jdcDOB.setEnabled(false);
            jTextFieldAge.setEnabled(true);
            jTextFieldAge.requestFocus();
        }
    }//GEN-LAST:event_jCheckBoxTrueBirthdayActionPerformed

    private void jComboBoxPrenameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxPrenameActionPerformed
        if (jComboBoxPrename.getSelectedItem() instanceof Prefix) {
            Prefix thePrefix = (Prefix) jComboBoxPrename.getSelectedItem();
            if (thePrefix != null) {
                Gutil.setGuiData(jComboBoxGender, thePrefix.sex);
            }
        } else if (jComboBoxPrename.getSelectedItem() instanceof String) {
            if (jComboBoxPrename.getSelectedItem().toString().isEmpty()) {
                jComboBoxPrename.setText("000");
            }
        }
    }//GEN-LAST:event_jComboBoxPrenameActionPerformed

    private void jButtonFatherActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonFatherActionPerformed
        if (theFamily == null) {
            this.theUS.setStatus("��سҺѹ�֡�����Ż�Ъҡá�͹", UpdateStatus.WARNING);
            return;
        }
        String family_id = PanelLookup.showDialog(theUS.getJFrame(), theMC, this.jTextFieldFatherFname.getText() + " " + this.jTextFieldFatherLname.getText());
        if (family_id == null) {
            return;
        }

        theFamily.father_fid = family_id;
        Family fam = this.thePatientControl.readFamilyByFamilyIdRet(family_id);
        setTextFieldDE(jTextFieldFatherFname, fam.patient_name);
        setTextFieldDE(jTextFieldFatherLname, fam.patient_last_name);
        theFamily.father_firstname = fam.patient_name;
        theFamily.father_lastname = fam.patient_last_name;
        theFamily.father_pid = fam.pid;
    }//GEN-LAST:event_jButtonFatherActionPerformed

    private void jButtonMotherActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonMotherActionPerformed
        if (theFamily == null) {
            this.theUS.setStatus("��سҺѹ�֡�����Ż�Ъҡá�͹", UpdateStatus.WARNING);
            return;
        }
        String family_id = PanelLookup.showDialog(theUS.getJFrame(), theMC, this.jTextFieldMotherFname.getText() + " " + this.jTextFieldMotherLname.getText());
        if (family_id == null) {
            return;
        }

        theFamily.mother_fid = family_id;
        Family fam = this.thePatientControl.readFamilyByFamilyIdRet(family_id);
        setTextFieldDE(jTextFieldMotherFname, fam.patient_name);
        setTextFieldDE(jTextFieldMotherLname, fam.patient_last_name);
        theFamily.mother_firstname = fam.patient_name;
        theFamily.mother_lastname = fam.patient_last_name;
        theFamily.mother_pid = fam.pid;
    }//GEN-LAST:event_jButtonMotherActionPerformed

    private void jButtonCoupleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCoupleActionPerformed
        if (theFamily == null) {
            this.theUS.setStatus("��سҺѹ�֡�����Ż�Ъҡá�͹", UpdateStatus.WARNING);
            return;
        }
        String family_id = PanelLookup.showDialog(theUS.getJFrame(), theMC, this.jTextFieldCoupleFname.getText() + " " + this.jTextFieldCoupleLname.getText());
        if (family_id == null) {
            return;
        }

        theFamily.couple_fid = family_id;
        Family fam = this.thePatientControl.readFamilyByFamilyIdRet(family_id);
        setTextFieldDE(jTextFieldCoupleFname, fam.patient_name);
        setTextFieldDE(jTextFieldCoupleLname, fam.patient_last_name);
        theFamily.couple_firstname = fam.patient_name;
        theFamily.couple_lastname = fam.patient_last_name;
        theFamily.couple_id = fam.pid;
    }//GEN-LAST:event_jButtonCoupleActionPerformed

    private void jButtonCancelCoupleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelCoupleActionPerformed
        theFamily.couple_fid = "";
        theFamily.couple_firstname = "";
        theFamily.couple_lastname = "";
        theFamily.couple_id = "";
        setTextFieldDE(jTextFieldCoupleFname, "");
        setTextFieldDE(jTextFieldCoupleLname, "");
}//GEN-LAST:event_jButtonCancelCoupleActionPerformed

    private void jButtonCancelMotherActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelMotherActionPerformed
        theFamily.mother_fid = "";
        theFamily.mother_firstname = "";
        theFamily.mother_lastname = "";
        theFamily.mother_pid = "";
        setTextFieldDE(jTextFieldMotherFname, "");
        setTextFieldDE(jTextFieldMotherLname, "");
}//GEN-LAST:event_jButtonCancelMotherActionPerformed

    private void jButtonCancelFatherActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelFatherActionPerformed
        theFamily.father_fid = "";
        theFamily.father_firstname = "";
        theFamily.father_lastname = "";
        theFamily.father_pid = "";
        setTextFieldDE(jTextFieldFatherFname, "");
        setTextFieldDE(jTextFieldFatherLname, "");
}//GEN-LAST:event_jButtonCancelFatherActionPerformed

    private void jComboBoxContactSexKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBoxContactSexKeyReleased
        Gutil.setTransferCursor(evt, jComboBoxRelation, panelAddressPatient, jTextFieldLname, panelAddressContact, jComboBoxContactSex);
    }//GEN-LAST:event_jComboBoxContactSexKeyReleased

    private void jComboBoxRelationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxRelationActionPerformed
    }//GEN-LAST:event_jComboBoxRelationActionPerformed

    private void jComboBoxRelationKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBoxRelationKeyReleased
        Gutil.setTransferCursor(evt, jComboBoxRelation, panelAddressPatient, jTextFieldLname, panelAddressContact, jComboBoxContactSex);
    }//GEN-LAST:event_jComboBoxRelationKeyReleased

    private void btnCopyAddressActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCopyAddressActionPerformed
        String h = panelAddressPatient.getHouse();
        String v = panelAddressPatient.getVillage();
        String r = panelAddressPatient.getRoad();
        String p = panelAddressPatient.getPhone();
        String e = panelAddressPatient.getEmail();
        String m = panelAddressPatient.getMobile();
        String c = panelAddressPatient.getChangwat();
        String a = panelAddressPatient.getAmpur();
        String t = panelAddressPatient.getTambon();
        String pc = panelAddressPatient.getPostcode();
        panelAddressContact.setAddress(h, v, r, p, m, c, a, t, pc, "", e);
    }//GEN-LAST:event_btnCopyAddressActionPerformed

    private void jTextFieldContactFnameFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextFieldContactFnameFocusLost
        jTextFieldContactLnameFocusGained(null);
    }//GEN-LAST:event_jTextFieldContactFnameFocusLost

    private void jTextFieldContactFnameKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldContactFnameKeyReleased
        Gutil.setTransferCursor(evt, jTextFieldContactFname, jTextFieldCoupleFname, null, jButtonSave1, jTextFieldContactLname);
    }//GEN-LAST:event_jTextFieldContactFnameKeyReleased

    private void jTextFieldContactLnameFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextFieldContactLnameFocusGained
        if (!jTextFieldContactFname.getText().trim().isEmpty()) {
            if (jTextFieldContactLname.getText().trim().isEmpty()) {
                Gutil.setGuiData(jTextFieldContactLname, Gutil.getGuiData(jTextFieldLname));
            }
        }
    }//GEN-LAST:event_jTextFieldContactLnameFocusGained

    private void jTextFieldContactLnameKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldContactLnameKeyReleased
        Gutil.setTransferCursor(evt, jTextFieldContactLname, jTextFieldCoupleLname, jTextFieldContactFname, jButtonSave1, null);
    }//GEN-LAST:event_jTextFieldContactLnameKeyReleased

    private void jButtonSave1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSave1ActionPerformed
        this.doSave();
    }//GEN-LAST:event_jButtonSave1ActionPerformed

    private void jButtonPrintOPDCardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPrintOPDCardActionPerformed
        if (thePatient == null) {
            theUS.setStatus("��س����͡������", UpdateStatus.WARNING);
            return;
        }
        thePrintControl.printOPDCard(PrintControl.MODE_PREVIEW, theHO.vVisitPayment);
    }//GEN-LAST:event_jButtonPrintOPDCardActionPerformed

    private void jButtonActiveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonActiveActionPerformed
        thePatientControl.activePatient();
    }//GEN-LAST:event_jButtonActiveActionPerformed

    private void jComboBoxRaceKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBoxRaceKeyReleased
        Gutil.setTransferCursor(evt, jComboBoxRace, jComboBoxOccup, null, jComboBoxReligion, jComboBoxNation);
}//GEN-LAST:event_jComboBoxRaceKeyReleased

    private void jComboBoxNationKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBoxNationKeyReleased
        Gutil.setTransferCursor(evt, jComboBoxNation, jComboBoxOccup, jComboBoxRace, jComboBoxEducate, null);
}//GEN-LAST:event_jComboBoxNationKeyReleased

    private void jButtonReset2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonReset2ActionPerformed
        theHC.thePatientControl.resetPatient();
    }//GEN-LAST:event_jButtonReset2ActionPerformed

    private void jButtonSearchPatient1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSearchPatient1ActionPerformed
        String dob = jCheckBoxTrueBirthday.isSelected() ? jdcDOB.getStringDate("yyyy/MM/dd", new Locale("th", "TH")) : "";
        if (!dob.isEmpty()) {
            String[] temp = dob.split("-");
            dob = "";
            for (int i = temp.length - 1; i >= 0; i--) {
                dob += dob.isEmpty() ? temp[i] : "/" + temp[i];
            }
        }
        theHosDialog.showDialogSearchPatient(jTextFieldFname.getText(), jTextFieldLname.getText(), "", "", dob);
    }//GEN-LAST:event_jButtonSearchPatient1ActionPerformed

    private void jButtonDelete1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDelete1ActionPerformed
        if (theFamily != null) {
            theHC.thePatientControl.deletePerson(theFamily, thePatient);
        }
    }//GEN-LAST:event_jButtonDelete1ActionPerformed

    private void jButtonIndexActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonIndexActionPerformed
        if (theHO.thePatient == null) {
            theHC.theUS.setStatus("�ѧ��������͡������", UpdateStatus.WARNING);
            return;
        }
        theHC.thePrintControl.printIndex();
}//GEN-LAST:event_jButtonIndexActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        StringSelection stringSelection = new StringSelection(this.pidPanel.getText());
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(stringSelection, null);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jTextFieldRevenueFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextFieldRevenueFocusLost

    }//GEN-LAST:event_jTextFieldRevenueFocusLost

    private void jTextFieldRevenueKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldRevenueKeyReleased

    }//GEN-LAST:event_jTextFieldRevenueKeyReleased

    private void jComboBoxNationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxNationActionPerformed
        this.doSelectedNation();
    }//GEN-LAST:event_jComboBoxNationActionPerformed

    private void jdcDOBPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jdcDOBPropertyChange
        String bdate = jdcDOB.getStringDate("yyyy/MM/dd", new Locale("th", "TH"));
        if (bdate != null && bdate.length() > 0 && bdate.length() != 10) {
            theUS.setStatus("��سҡ�͡�ѹ�Դ���١�ٻẺ ��/��/����", UpdateStatus.WARNING);
            return;
        }
        String date_time = theHO.date_time;
        if (!bdate.isEmpty() && com.hosv3.utility.DateUtil.countDateDiff(bdate, date_time) > 0) {
            theUS.setStatus("��س��к��ѹ�Դ���ѹ�ʹյ", UpdateStatus.WARNING);
            return;
        }
        if (!bdate.isEmpty()) {
            String age = DateUtil.calculateAge(bdate, date_time);
            jTextFieldAge.setText(age);
        }
    }//GEN-LAST:event_jdcDOBPropertyChange

    private void jTextFieldFnameFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextFieldFnameFocusGained
        jTextFieldFname.selectAll();
    }//GEN-LAST:event_jTextFieldFnameFocusGained

    private void jTextFieldLnameFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextFieldLnameFocusGained
        jTextFieldLname.selectAll();
    }//GEN-LAST:event_jTextFieldLnameFocusGained

    private void rbtnForeignerGroup1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnForeignerGroup1ActionPerformed
        panelExtraPersonForienerInfo.setVisible(rbtnForeignerGroup2.isSelected());
    }//GEN-LAST:event_rbtnForeignerGroup1ActionPerformed

    private void rbtnForeignerGroup2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnForeignerGroup2ActionPerformed
        panelExtraPersonForienerInfo.setVisible(rbtnForeignerGroup2.isSelected());
    }//GEN-LAST:event_rbtnForeignerGroup2ActionPerformed
    private final Set<String> AUTO_DEFAULT_CODE = new HashSet<String>(Arrays.asList(new String[]{"1", "2", "5"}));
    private void jComboBoxRankActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxRankActionPerformed
        if (chkbSendEmailInvite.isVisible() && thePatient != null && thePatient.getObjectId() == null) {
            String code = ComboboxModel.getCodeComboBox(jComboBoxRank);
            chkbSendEmailInvite.setSelected(AUTO_DEFAULT_CODE.contains(code));
        }
    }//GEN-LAST:event_jComboBoxRankActionPerformed

    private void txtFirstNameEngFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtFirstNameEngFocusGained
        txtFirstNameEng.selectAll();
    }//GEN-LAST:event_txtFirstNameEngFocusGained

    private void txtLastNameEngFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtLastNameEngFocusGained
        txtLastNameEng.selectAll();
    }//GEN-LAST:event_txtLastNameEngFocusGained

    private void txtSkinColorFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtSkinColorFocusGained
        txtSkinColor.selectAll();
    }//GEN-LAST:event_txtSkinColorFocusGained

    private void txtPassportKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPassportKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            theHosDialog.showDialogSearchPatientByPassportNo(txtPassportNo.getText().trim());
        }
    }//GEN-LAST:event_txtPassportKeyReleased

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCopyAddress;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.ButtonGroup buttonGroup3;
    private javax.swing.ButtonGroup buttonGroupDrugAllergy;
    private javax.swing.ButtonGroup buttonGroupFm;
    private javax.swing.ButtonGroup buttonGroupMed;
    private javax.swing.ButtonGroup buttonGroupPt;
    private javax.swing.JCheckBox chkbSendEmailInvite;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButtonActive;
    private javax.swing.JButton jButtonCancelCouple;
    private javax.swing.JButton jButtonCancelFather;
    private javax.swing.JButton jButtonCancelMother;
    private javax.swing.JButton jButtonCouple;
    private javax.swing.JButton jButtonDelete1;
    private javax.swing.JButton jButtonFather;
    private javax.swing.JButton jButtonIndex;
    private javax.swing.JButton jButtonMother;
    private javax.swing.JButton jButtonPrintOPDCard;
    private javax.swing.JButton jButtonReset2;
    private javax.swing.JButton jButtonSave1;
    private javax.swing.JButton jButtonSearchPatient1;
    private javax.swing.JCheckBox jCheckBoxCheck;
    private javax.swing.JCheckBox jCheckBoxTrueBirthday;
    private javax.swing.JComboBox jComboBoxAffiliated;
    private javax.swing.JComboBox jComboBoxBlood;
    private javax.swing.JComboBox jComboBoxContactSex;
    private javax.swing.JComboBox jComboBoxEducate;
    private javax.swing.JComboBox jComboBoxGender;
    private javax.swing.JComboBox jComboBoxJobType;
    private javax.swing.JComboBox jComboBoxMarriage;
    private com.hosv3.gui.component.HosComboBox jComboBoxNation;
    private com.hosv3.gui.component.HosComboBox jComboBoxOccup;
    private com.hosv3.gui.component.HosComboBox jComboBoxPrename;
    private javax.swing.JComboBox jComboBoxRH;
    private com.hosv3.gui.component.HosComboBox jComboBoxRace;
    private javax.swing.JComboBox jComboBoxRank;
    private com.hosv3.gui.component.HosComboBox jComboBoxRelation;
    private javax.swing.JComboBox jComboBoxReligion;
    private javax.swing.JComboBox jComboBoxTypeArea;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelAge;
    private javax.swing.JLabel jLabelBlood;
    private javax.swing.JLabel jLabelCID;
    private javax.swing.JLabel jLabelContact;
    private javax.swing.JLabel jLabelCoupleName;
    private javax.swing.JLabel jLabelEducate;
    private javax.swing.JLabel jLabelFatherName;
    private javax.swing.JLabel jLabelFstatus;
    private javax.swing.JLabel jLabelGender;
    private javax.swing.JLabel jLabelLabor;
    private javax.swing.JLabel jLabelMarriage;
    private javax.swing.JLabel jLabelMarriage1;
    private javax.swing.JLabel jLabelMarriage2;
    private javax.swing.JLabel jLabelMarriage3;
    private javax.swing.JLabel jLabelMotherName;
    private javax.swing.JLabel jLabelNation;
    private javax.swing.JLabel jLabelOccup;
    private javax.swing.JLabel jLabelPID;
    private javax.swing.JLabel jLabelPrivateDoctor;
    private javax.swing.JLabel jLabelRace;
    private javax.swing.JLabel jLabelReligion;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel25;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JPanel jPanelAction;
    private javax.swing.JPanel jPanelContact;
    private javax.swing.JPanel jPanelDetail;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private com.hospital_os.utility.IntegerTextField jTextFieldAge;
    private javax.swing.JTextField jTextFieldContactFname;
    private javax.swing.JTextField jTextFieldContactLname;
    private javax.swing.JTextField jTextFieldCoupleFname;
    private javax.swing.JTextField jTextFieldCoupleLname;
    private javax.swing.JTextField jTextFieldFatherFname;
    private javax.swing.JTextField jTextFieldFatherLname;
    private javax.swing.JTextField jTextFieldFname;
    private javax.swing.JTextField jTextFieldLname;
    private javax.swing.JTextField jTextFieldMotherFname;
    private javax.swing.JTextField jTextFieldMotherLname;
    private javax.swing.JTextField jTextFieldPrivateDoctor;
    private com.hospital_os.utility.IntegerTextField jTextFieldRevenue;
    private javax.swing.JComboBox jcbForeignerNoType;
    private javax.swing.JComboBox jcbForeignerType;
    private sd.comp.jcalendar.JDateChooser jdcDOB;
    private sd.comp.jcalendar.JDateChooser jdcForeignerNoExp;
    private sd.comp.jcalendar.JDateChooser jdcPassportExp;
    private javax.swing.JLabel lblInterName;
    private javax.swing.JLabel lblInterName1;
    private com.hosv3.gui.component.PanelAddress panelAddressContact;
    private com.hosv3.gui.component.PanelAddress panelAddressEmployer;
    private com.hosv3.gui.component.PanelAddress panelAddressPatient;
    private javax.swing.JPanel panelButtonPlugin;
    private javax.swing.JPanel panelExtraHosWingInfo;
    private javax.swing.JPanel panelExtraPersonForienerInfo;
    private javax.swing.JPanel panelPersonForeignerInfo;
    private com.hosv3.gui.component.PIDPanel pidPanel;
    private javax.swing.JRadioButton rbtnDrugAllergy1;
    private javax.swing.JRadioButton rbtnDrugAllergy2;
    private javax.swing.JRadioButton rbtnDrugAllergy3;
    private javax.swing.JRadioButton rbtnEmployerType1;
    private javax.swing.JRadioButton rbtnEmployerType2;
    private javax.swing.JRadioButton rbtnForeignerGroup1;
    private javax.swing.JRadioButton rbtnForeignerGroup2;
    private javax.swing.JTextField txtEmployerCID;
    private javax.swing.JTextField txtEmployerName;
    private javax.swing.JTextField txtEmployerReg;
    private javax.swing.JTextField txtFirstNameEng;
    private javax.swing.JTextField txtForeignerNo;
    private javax.swing.JTextField txtLastNameEng;
    private javax.swing.JTextField txtPassport;
    private javax.swing.JTextField txtPassportFname;
    private javax.swing.JTextField txtPassportLName;
    private javax.swing.JTextField txtPassportNo;
    private javax.swing.JTextField txtSkinColor;
    // End of variables declaration//GEN-END:variables

    private void setTextFieldDE(JTextField jTextField, String string) {
        jTextField.setText(string);
        jTextField.setEditable(string.isEmpty());
    }

    public Patient getPatient() {
        if (thePatient == null) {
            thePatient = theHO.initPatient();
        }
        thePatient.setFamily(getFamily());
        thePatient.relation = jComboBoxRelation.getText();
        thePatient.sex_contact = Gutil.getGuiData(jComboBoxContactSex);
        thePatient.contact_fname = Gutil.getGuiData(jTextFieldContactFname);
        thePatient.contact_lname = Gutil.getGuiData(jTextFieldContactLname);
        if (!panelAddressPatient.isOtherCountry().equals("1")) {
            thePatient.changwat = panelAddressPatient.getChangwat();
            thePatient.tambon = panelAddressPatient.getTambon();
            thePatient.ampur = panelAddressPatient.getAmpur();
            thePatient.patient_postcode = panelAddressPatient.getPostcode();
            thePatient.patient_address_eng = panelAddressPatient.getAddressEn();
        } else {
            thePatient.changwat = "";
            thePatient.tambon = "";
            thePatient.ampur = "";
            thePatient.patient_postcode = "";
            thePatient.patient_address_eng = "";
        }
        thePatient.changwat_contact = panelAddressContact.getChangwat();
        thePatient.ampur_contact = panelAddressContact.getAmpur();
        thePatient.tambon_contact = panelAddressContact.getTambon();
        thePatient.patient_contact_postcode = panelAddressContact.getPostcode();
        thePatient.house = panelAddressPatient.getHouse();
        thePatient.house_contact = panelAddressContact.getHouse();
        thePatient.phone = panelAddressPatient.getPhone();
        thePatient.phone_contact = panelAddressContact.getPhone();
        thePatient.mobile_phone = panelAddressPatient.getMobile();
        thePatient.contact_mobile_phone = panelAddressContact.getMobile();
        thePatient.patient_email = panelAddressPatient.getEmail();
        thePatient.contact_email = panelAddressContact.getEmail();
        thePatient.road = panelAddressPatient.getRoad();
        thePatient.road_contact = panelAddressContact.getRoad();
        thePatient.village = panelAddressPatient.getVillage();
        thePatient.village_contact = panelAddressContact.getVillage();
        thePatient.is_other_country = panelAddressPatient.isOtherCountry();
        thePatient.other_address = panelAddressPatient.getOtherCountry();
        thePatient.latitude = panelAddressPatient.getLatitude();
        thePatient.longitude = panelAddressPatient.getLongitude();
        thePatient.deny_allergy = rbtnDrugAllergy1.isSelected() ? "1"
                : rbtnDrugAllergy2.isSelected() ? "2"
                : rbtnDrugAllergy3.isSelected() ? "3"
                : "";
        thePatient.private_doc = jTextFieldPrivateDoctor.getText().trim();
        // for checkup module
        if (chkbSendEmailInvite.isVisible()) {
            thePatient.sent_email_invitation = chkbSendEmailInvite.isSelected() ? "1" : "0";
        }
        thePatient.xn = thePatient.xn == null ? "" : thePatient.xn;
        return thePatient;
    }

    protected Family getFamily() {
        if (theFamily == null) {
            theFamily = new Family();
        }
        if (jCheckBoxTrueBirthday.isSelected() || !jTextFieldAge.getText().isEmpty()) {
            theFamily.patient_birthday = jdcDOB.getStringDate("yyyy-MM-dd", new Locale("th", "TH"));
        }
        theFamily.blood_group_id = Gutil.getGuiData(jComboBoxBlood);
        theFamily.rh = Gutil.getGuiData(jComboBoxRH);
        theFamily.revenue = jTextFieldRevenue.getText();
        theFamily.education_type_id = Gutil.getGuiData(jComboBoxEducate);
        theFamily.f_sex_id = Gutil.getGuiData(jComboBoxGender);
        theFamily.marriage_status_id = Gutil.getGuiData(jComboBoxMarriage);
        theFamily.skin_color = txtSkinColor.getText().trim();
        theFamily.nation_id = Gutil.getGuiData(jComboBoxNation);
        theFamily.occupation_id = jComboBoxOccup.getText();
        theFamily.f_prefix_id = jComboBoxPrename.getText();
        if (theFamily.f_prefix_id.isEmpty()) {
            theFamily.f_prefix_id = "add:" + jComboBoxPrename.getDetail();
        }
        theFamily.race_id = Gutil.getGuiData(jComboBoxRace);
        theFamily.religion_id = Gutil.getGuiData(jComboBoxReligion);
        theFamily.area_status_id = Gutil.getGuiData(jComboBoxTypeArea);
        theFamily.couple_firstname = (Gutil.getGuiData(jTextFieldCoupleFname));
        theFamily.couple_lastname = (Gutil.getGuiData(jTextFieldCoupleLname));
        theFamily.father_firstname = (Gutil.getGuiData(jTextFieldFatherFname));
        theFamily.father_lastname = (Gutil.getGuiData(jTextFieldFatherLname));
        theFamily.patient_name = (Gutil.getGuiData(jTextFieldFname));
        theFamily.patient_last_name = (Gutil.getGuiData(jTextFieldLname));
        theFamily.pid = pidPanel.getText();
        theFamily.passport_no = txtPassport.getText().trim();
        theFamily.patient_name = (Gutil.getGuiData(jTextFieldFname));
        theFamily.patient_firstname_eng = txtFirstNameEng.getText().trim();
        theFamily.patient_lastname_eng = txtLastNameEng.getText().trim();
        theFamily.patient_last_name = (Gutil.getGuiData(jTextFieldLname));
        theFamily.mother_firstname = (Gutil.getGuiData(jTextFieldMotherFname));
        theFamily.mother_lastname = (Gutil.getGuiData(jTextFieldMotherLname));

        if (theHome != null && theHome.getObjectId() != null) {
            theFamily.home_id = theHome.getObjectId();
        }

        if (jCheckBoxTrueBirthday.isSelected()) {
            theFamily.patient_birthday_true = Gutil.getGuiData("1");
        } else {
            theFamily.patient_birthday_true = Gutil.getGuiData("0");
        }

        if (theFamily.getObjectId() == null) {
            theFamily.record_date_time = theHO.date_time;
            theFamily.staff_record = theHC.theHO.theEmployee.getObjectId();
        } else {
            theFamily.modify_date_time = theHO.date_time;
            theFamily.staff_modify = theHC.theHO.theEmployee.getObjectId();
        }
        if (theHC.theHO.theSite.is_hospital_wing) {
            theFamily.f_person_affiliated_id = Gutil.getGuiData(jComboBoxAffiliated);
            theFamily.f_person_rank_id = Gutil.getGuiData(jComboBoxRank);
            theFamily.f_person_jobtype_id = Gutil.getGuiData(jComboBoxJobType);
        }
        getPersonForeigner();
        return theFamily;
    }

    public void setDefaultComboBox() {
        jComboBoxNation.refresh();
        jComboBoxNation.setText("99");
        jComboBoxRace.refresh();
        jComboBoxRace.setText("99");
        Gutil.setGuiData(jComboBoxBlood, "1");
        Gutil.setGuiData(jComboBoxMarriage, "1");
        txtSkinColor.setText("");
        jComboBoxRelation.refresh();
        jComboBoxRelation.setText("00");
        jComboBoxOccup.refresh();
        jComboBoxOccup.setText("000");
        jComboBoxPrename.refresh();
        jComboBoxPrename.setText("000");
        Gutil.setGuiData(jComboBoxReligion, "1");
        Gutil.setGuiData(jComboBoxEducate, "11");
        setTextFieldDE(jTextFieldCoupleFname, "");
        setTextFieldDE(jTextFieldCoupleLname, "");
        setTextFieldDE(jTextFieldFatherFname, "");
        setTextFieldDE(jTextFieldFatherLname, "");
        Gutil.setGuiData(jTextFieldFname, "");
        Gutil.setGuiData(jTextFieldLname, "");
        txtFirstNameEng.setText("");
        txtLastNameEng.setText("");
        setTextFieldDE(jTextFieldMotherFname, "");
        setTextFieldDE(jTextFieldMotherLname, "");
        pidPanel.setText("");
        txtPassport.setText("");
        Gutil.setGuiData(jTextFieldAge, "");
        Gutil.setGuiData(jComboBoxTypeArea, "4");
        setBirthDateTrue("0");
        jdcDOB.setDate(new Date());
        jTextFieldRevenue.setText("");
        Gutil.setGuiData(jComboBoxRH, "9");
        Gutil.setGuiData(jComboBoxGender, "3");
        Gutil.setGuiData(jComboBoxContactSex, "3");
        if (theHC.theHO.theSite.is_hospital_wing) {
            Gutil.setGuiData(jComboBoxAffiliated, "0");
            Gutil.setGuiData(jComboBoxRank, "0");
            Gutil.setGuiData(jComboBoxJobType, "0");
        }
        // Foreigner Panel
        rbtnForeignerGroup2.setSelected(true);
        txtPassportFname.setText("");
        txtPassportLName.setText("");
        txtPassportNo.setText("");
        jdcPassportExp.setDate(null);
        txtForeignerNo.setText("");
        jdcForeignerNoExp.setDate(null);
        Gutil.setGuiData(jcbForeignerType, "0");
        Gutil.setGuiData(jcbForeignerNoType, "0");
        txtEmployerName.setText("");
        buttonGroup2.clearSelection();
        txtEmployerCID.setText("");
        txtEmployerReg.setText("");
        buttonGroupDrugAllergy.clearSelection();

        setDefaultLocation();
    }

    private void setBirthDateTrue(String str) {
        boolean bdate_true = Gutil.isSelected(str);
        jCheckBoxTrueBirthday.setSelected(bdate_true);
        jdcDOB.setEnabled(bdate_true);
        jTextFieldAge.setEnabled(!bdate_true);
    }

    protected void setFamily(Family family, Home home, Family father, Family mother, Family couple) {
        // SOmprasong add 281209 clear all before set object
        theFamily = family;
        thePatient = null;
        jLabelPID.setText("PID:");
        // Somprasong clear ��ҡ�͹ ���Ǥ��� set ��� object ������Ѻ��
        setDefaultComboBox();
        if (theFamily == null) {
            return;
        }
        jComboBoxPrename.setText(theFamily.f_prefix_id.isEmpty() ? "000" : theFamily.f_prefix_id);
        Gutil.setGuiData(jTextFieldFname, theFamily.patient_name);
        Gutil.setGuiData(jTextFieldLname, theFamily.patient_last_name);
        txtFirstNameEng.setText(theFamily.patient_firstname_eng);
        txtLastNameEng.setText(theFamily.patient_lastname_eng);
        if (theFamily.f_sex_id != null && !theFamily.f_sex_id.isEmpty()) {
            Gutil.setGuiData(jComboBoxGender, theFamily.f_sex_id);
        }
        String age = DateUtil.calculateAge(theFamily.patient_birthday, theHO.date_time);
        if (age.equals("0")) {
            if (thePatient != null) {
                age = DateUtil.calculateAge(thePatient.patient_birthday, theHO.date_time);
            }
        }
        Gutil.setGuiData(jTextFieldAge, age);
        if (theFamily.marriage_status_id != null && !theFamily.marriage_status_id.isEmpty()) {
            Gutil.setGuiData(jComboBoxMarriage, theFamily.marriage_status_id);
        }
        txtPassport.setText(theFamily.passport_no);
        txtSkinColor.setText(theFamily.skin_color);
        if (theFamily.blood_group_id != null && !theFamily.blood_group_id.isEmpty()) {
            Gutil.setGuiData(jComboBoxBlood, theFamily.blood_group_id);
        }
        if (theFamily.rh != null && !theFamily.rh.isEmpty()) {
            Gutil.setGuiData(jComboBoxRH, theFamily.rh);
        }
        jTextFieldRevenue.setText(theFamily.revenue);
        if (theFamily.occupation_id != null && !theFamily.occupation_id.isEmpty()) {
            jComboBoxOccup.setText(theFamily.occupation_id);
        }
        if (theFamily.race_id != null && !theFamily.race_id.isEmpty()) {
            Gutil.setGuiData(jComboBoxRace, theFamily.race_id);
        }
        if (theFamily.nation_id != null && !theFamily.nation_id.isEmpty()) {
            Gutil.setGuiData(jComboBoxNation, theFamily.nation_id);
        }
        if (theFamily.religion_id != null && !theFamily.religion_id.isEmpty()) {
            Gutil.setGuiData(jComboBoxReligion, theFamily.religion_id);
        }
        if (theFamily.education_type_id != null && !theFamily.education_type_id.isEmpty()) {
            Gutil.setGuiData(jComboBoxEducate, theFamily.education_type_id);
        }
        if (father != null) {
            setTextFieldDE(jTextFieldFatherFname, father.patient_name);
            setTextFieldDE(jTextFieldFatherLname, father.patient_last_name);
        } else {
            jTextFieldFatherFname.setEditable(true);
            jTextFieldFatherLname.setEditable(true);
            Gutil.setGuiData(jTextFieldFatherFname, theFamily.father_firstname);
            Gutil.setGuiData(jTextFieldFatherLname, theFamily.father_lastname);
        }
        if (mother != null) {
            setTextFieldDE(jTextFieldMotherFname, mother.patient_name);
            setTextFieldDE(jTextFieldMotherLname, mother.patient_last_name);
        } else {
            jTextFieldMotherFname.setEditable(true);
            jTextFieldMotherLname.setEditable(true);
            Gutil.setGuiData(jTextFieldMotherFname, theFamily.mother_firstname);
            Gutil.setGuiData(jTextFieldMotherLname, theFamily.mother_lastname);
        }
        if (couple != null) {
            setTextFieldDE(jTextFieldCoupleFname, couple.patient_name);
            setTextFieldDE(jTextFieldCoupleLname, couple.patient_last_name);
        } else {
            jTextFieldCoupleFname.setEditable(true);
            jTextFieldCoupleLname.setEditable(true);
            Gutil.setGuiData(jTextFieldCoupleFname, theFamily.couple_firstname);
            Gutil.setGuiData(jTextFieldCoupleLname, theFamily.couple_lastname);
        }
        if (theFamily.area_status_id != null && !theFamily.area_status_id.isEmpty()) {
            Gutil.setGuiData(jComboBoxTypeArea, theFamily.area_status_id);
        } else {
            Gutil.setGuiData(jComboBoxTypeArea, "4");
        }
        pidPanel.setText(theFamily.pid);
        setBirthDateTrue(theFamily.patient_birthday_true);
        jdcDOB.setDate(DateUtil.convertStringToDate(theFamily.patient_birthday, "yyyy-MM-dd", new Locale("th", "TH")));
        jLabelPID.setText("PID:" + theFamily.hn_hcis);

        if (theHC.theHO.theSite.is_hospital_wing) {
            if (theFamily.f_person_affiliated_id != null && !theFamily.f_person_affiliated_id.isEmpty()) {
                Gutil.setGuiData(jComboBoxAffiliated, theFamily.f_person_affiliated_id);
            }

            if (theFamily.f_person_rank_id != null && !theFamily.f_person_rank_id.isEmpty()) {
                Gutil.setGuiData(jComboBoxRank, theFamily.f_person_rank_id);
            }

            if (theFamily.f_person_jobtype_id != null && !theFamily.f_person_jobtype_id.isEmpty()) {
                Gutil.setGuiData(jComboBoxJobType, theFamily.f_person_jobtype_id);
            }
        }
        this.setPersonForeigner(theFamily.personForeigner);
    }

    public void setLanguage() {
        GuiLang.setLanguage(jLabelPID);
        GuiLang.setLanguage(jButtonPrintOPDCard);
        GuiLang.setLanguage(btnCopyAddress);
        GuiLang.setLanguage(jPanel10);
        GuiLang.setLanguage(jLabelCID);
        GuiLang.setLanguage(jLabelGender);
        GuiLang.setLanguage(jCheckBoxTrueBirthday);
        GuiLang.setLanguage(jLabelAge);
        GuiLang.setLanguage(jLabel3);
        GuiLang.setLanguage(jLabelMarriage);
        GuiLang.setLanguage(jLabelBlood);
        GuiLang.setLanguage(jLabelOccup);
        GuiLang.setLanguage(jLabelRace);
        GuiLang.setLanguage(jLabelNation);
        GuiLang.setLanguage(jLabelReligion);
        GuiLang.setLanguage(jLabelEducate);
        GuiLang.setLanguage(jLabelFatherName);
        GuiLang.setLanguage(jLabelMotherName);
        GuiLang.setLanguage(jLabelCoupleName);
        GuiLang.setLanguage(jLabelFstatus);
        GuiLang.setLanguage(jLabelLabor);
        GuiLang.setLanguage(jLabelContact);
        GuiLang.setLanguage(jLabel7);
        GuiLang.setLanguage(jLabel8);
        GuiLang.setLanguage(btnCopyAddress);
        GuiLang.setLanguage(jButtonActive);
        GuiLang.setLanguage(jPanelContact);
        panelAddressEmployer.setLanguage("");
        panelAddressContact.setLanguage("");
        panelAddressPatient.setLanguage("");
    }

    public void setUpdateStatus(UpdateStatus us) {
        theUS = us;
    }

    @Override
    public void notifySavePatient(String str, int status) {
        this.updateData();
    }

    @Override
    public void notifyResetPatient(String str, int status) {
        this.updateData();
    }

    @Override
    public void notifyDeletePatient(String str, int status) {
        this.updateData();
    }

    @Override
    public void notifyReadPatient(String str, int status) {
        this.updateData();
    }

    @Override
    public void notifySaveAppointment(String str, int status) {
    }

    @Override
    public void notifyManageDrugAllergy(String str, int status) {
    }

    @Override
    public void notifySavePatientPayment(String str, int status) {
    }

    @Override
    public void notifyDeletePatientPayment(String str, int status) {
    }

    @Override
    public void notifyReadFamily(String str, int status) {
        this.updateData();
    }

    @Override
    public void notifySaveInformIntolerance(Object... object) {
    }

    @Override
    public void notifyReadVisit(String str, int status) {
        this.updateData();
    }

    @Override
    public void notifyReverseFinancial(String str, int status) {
        this.updateData();
    }

    @Override
    public void notifyReverseDoctor(String str, int status) {
        this.updateData();
    }

    @Override
    public void notifyVisitPatient(String str, int status) {
        this.updateData();
    }

    @Override
    public void notifySendVisit(String str, int status) {
        this.updateData();
    }

    @Override
    public void notifySendVisitBackWard(String str, int status) {
        this.updateData();
    }

    @Override
    public void notifyObservVisit(String str, int status) {
    }

    @Override
    public void notifyUnlockVisit(String str, int status) {
        this.updateData();
    }

    @Override
    public void notifyDropVisit(String str, int status) {
        this.updateData();
    }

    @Override
    public void notifyAdmitVisit(String str, int status) {
        this.updateData();
    }

    @Override
    public void notifyCheckDoctorTreament(String msg, int state) {
    }

    @Override
    public void notifyDischargeDoctor(String str, int status) {
        this.updateData();
    }

    @Override
    public void notifyRemainDoctorDischarge(String str, int status) {
        this.updateData();
    }

    @Override
    public void notifyDischargeFinancial(String str, int status) {
        this.updateData();
    }

    @Override
    public void notifyDeleteVisitPayment(String str, int status) {
    }

    @Override
    public void notifyReverseAdmit(String str, int status) {
    }

    @Override
    public void notifyAdmitLeaveHome(String str, int status) {
    }

    @Override
    public void notifyManagePatientLabReferIn(String str, int status) {
    }

    @Override
    public void notifySaveLabResult(String str, int status) {
    }

    @Override
    public void notifyDeleteLabResult(String str, int status) {
    }

    @Override
    public void notifyDeleteLabOrder(String str, int status) {
    }

    @Override
    public void notifySaveRemainLabResult(String str, int status) {
    }

    @Override
    public void notifySaveFilmXray(String str, int status) {
    }

    @Override
    public void notifyDeleteFilmXray(String str, int status) {
    }

    @Override
    public void notifyDeleteResultXray(String str, int status) {
    }

    @Override
    public void notifySaveXrayPosition(String str, int status) {
    }

    @Override
    public void notifyDeleteXrayPosition(String str, int status) {
    }

    @Override
    public void notifySaveResultXray(String str, int status) {
    }

    @Override
    public void notifyXrayReportComplete(String str, int status) {
        this.updateData();
    }

    @Override
    public void notifyReportResultLab(String str, int status) {
    }

    @Override
    public void notifyAddLabReferOut(String str, int status) {
    }

    @Override
    public void notifyAddLabReferIn(String str, int status) {
    }

    @Override
    public void notifySendResultLab(String str, int status) {
    }

    @Override
    public void notifyDeleteQueueLab(String str, int status) {
        this.updateData();
    }

    @Override
    public void notifyLoaderService() {
        this.serviceLoader();
    }
    private final List<PanelButtonPluginProvider> buttonPluginProviders = new ArrayList<>();

    private void serviceLoader() {
        panelButtonPlugin.removeAll();
        Iterator<PanelButtonPluginProvider> iterator = PanelButtonPluginManager.getInstance().getServices();
        String className = this.getClass().getName();
        // get buttons to list
        List<JButton> buttons = new ArrayList<>();
        while (iterator.hasNext()) {
            PanelButtonPluginProvider provider = iterator.next();
            if (provider.isOwnerPanel(PanelPersonData.class)) {
                JButton button = provider.getButton(PanelPersonData.class);
                boolean isVisible = provider.isVisible();
                LOG.log(java.util.logging.Level.INFO, "{0} get button from {1}, visible: {2}", new Object[]{className, provider.getClass().getName(), isVisible});
                button.setVisible(isVisible);
                buttonPluginProviders.add(provider);
                int index = provider.getIndex();
                if (index > buttons.size()) {
                    buttons.add(button);
                } else {
                    buttons.add(index, button);
                }
            }
        }
        if (buttons.isEmpty()) {
            LOG.log(java.util.logging.Level.INFO, "{0}  no button plugin!", className);
        } else {
            LOG.log(java.util.logging.Level.INFO, "{0}  found {1} buttons plugin!", new Object[]{className, buttons.size()});
        }
        // add all plugin buttons to panel
        buttons.forEach((button) -> {
            GridBagConstraints gridBagConstraints = new java.awt.GridBagConstraints();
            gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
            gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
            gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 3);
            panelButtonPlugin.add(button, gridBagConstraints);
        });
        panelButtonPlugin.revalidate();
        panelButtonPlugin.repaint();
    }

    private void doSave() {
        if (jCheckBoxCheck.isSelected()
                && (Gutil.getGuiData(jComboBoxNation).isEmpty()
                || Gutil.getGuiData(jComboBoxNation).equals("99"))) {
            if (jTextFieldFname.getText().isEmpty()) {
                jTextFieldFname.requestFocus();
                this.theUS.setStatus("��س��кت��ͼ�����", UpdateStatus.WARNING);
                return;
            } else if (jTextFieldLname.getText().isEmpty()) {
                jTextFieldLname.requestFocus();
                this.theUS.setStatus("��س��кع��ʡ��", UpdateStatus.WARNING);
                return;
            } else if (pidPanel.getText().isEmpty()) {
                pidPanel.requestFocusFirstDigit();
                this.theUS.setStatus("��س��к��Ţ�ѵû�ЪҪ�", UpdateStatus.WARNING);
                return;
            }
        }
        if (!rbtnDrugAllergy3.isSelected() && (theHO.vDrugAllergy != null && !theHO.vDrugAllergy.isEmpty())) {
            boolean ret = theUS.confirmBox(Constant.getTextBundle("����������¡���ҷ��������") + " "
                    + Constant.getTextBundle("�׹�ѹ��û���ʸ����"), UpdateStatus.WARNING);
            if (!ret) {
                return;
            }
        }
        String oldXN = thePatient != null && thePatient.xn != null ? thePatient.xn : "";
        if (thePatient != null) {
            oldXN = thePatient.xn;
        }
        Patient pt = getPatient();
        if (!oldXN.equals(pt.xn)) {
            theResultControl.savePatientXn(pt.xn, false);
        }
        int ret = thePatientControl.savePatient(pt, jTextFieldAge.getText(), vNCD);
        switch (ret) {
            case 3:
                pidPanel.requestFocus();
                break;
            case 6:
                jTextFieldContactFname.requestFocus();
                break;
            case 9:
                jTextFieldAge.requestFocus();
                break;
            case 10:
                jComboBoxOccup.requestFocus();
                break;
            case 5:
                jTextFieldAge.requestFocus();
                break;
            default:
                break;
        }
    }

    private void doSelectedNation() {
        // not thai must have foreigner infomation
        if (!(Gutil.getGuiData(jComboBoxNation).isEmpty() || Gutil.getGuiData(jComboBoxNation).equals("99"))) {
            if (theFamily == null) {
                this.setPersonForeigner(new PersonForeigner());
            } else if (theFamily.personForeigner == null) {
                theFamily.personForeigner = theFamily.getObjectId() != null
                        ? thePatientControl.getPersonForeignerByPersonId(theFamily.getObjectId())
                        : new PersonForeigner();
                if (theFamily.personForeigner == null) {
                    theFamily.personForeigner = new PersonForeigner();
                }
                this.setPersonForeigner(theFamily.personForeigner);
            }
            // show
            panelPersonForeignerInfo.setVisible(true);
        } else {
            // hide
            panelPersonForeignerInfo.setVisible(false);
        }
    }

    ;

    private void setPersonForeigner(PersonForeigner personForeigner) {
        if (personForeigner != null) {
            if (personForeigner.foreigner_group == 1) {
                rbtnForeignerGroup1.setSelected(true);
                panelExtraPersonForienerInfo.setVisible(false);
            } else {
                rbtnForeignerGroup2.setSelected(true);
                panelExtraPersonForienerInfo.setVisible(true);
            }
            txtPassportFname.setText(personForeigner.getObjectId() == null
                    ? txtFirstNameEng.getText() : personForeigner.passport_fname);
            txtPassportLName.setText(personForeigner.getObjectId() == null
                    ? txtLastNameEng.getText() : personForeigner.passport_lname);
            txtPassportNo.setText(personForeigner.getObjectId() == null
                    ? txtPassport.getText() : personForeigner.passport_no);
            jdcPassportExp.setDate(personForeigner.passport_no_exp);
            txtForeignerNo.setText(personForeigner.foreigner_no);
            jdcForeignerNoExp.setDate(personForeigner.foreigner_no_exp);
            Gutil.setGuiData(jcbForeignerType, personForeigner.f_person_foreigner_id);
            Gutil.setGuiData(jcbForeignerNoType, personForeigner.foreigner_no_type);
            txtEmployerName.setText(personForeigner.employer_name);
            switch (personForeigner.employer_type) {
                case 1:
                    rbtnEmployerType1.setSelected(true);
                    break;
                case 2:
                    rbtnEmployerType2.setSelected(true);
                    break;
                default:
                    buttonGroup2.clearSelection();
                    break;
            }
            txtEmployerCID.setText(personForeigner.employer_id);
            txtEmployerReg.setText(personForeigner.employer_registration);

            panelAddressEmployer.setAddress(personForeigner.employer_address_house, personForeigner.employer_address_moo,
                    personForeigner.employer_address_road, personForeigner.employer_contact_phone_number, personForeigner.employer_contact_mobile_phone_number,
                    personForeigner.employer_address_changwat, personForeigner.employer_address_amphur, personForeigner.employer_address_tambon, personForeigner.employer_address_postcode);
        }
    }

    private void getPersonForeigner() {
        if (theFamily != null) {
            if (!(Gutil.getGuiData(jComboBoxNation).isEmpty() || Gutil.getGuiData(jComboBoxNation).equals("99"))) {
                if (theFamily.personForeigner == null) {
                    theFamily.personForeigner = new PersonForeigner();
                }
                theFamily.personForeigner.active = "1";
                theFamily.personForeigner.foreigner_group = rbtnForeignerGroup1.isSelected() ? 1 : 2;
                theFamily.personForeigner.passport_fname = txtPassportFname.getText();
                theFamily.personForeigner.passport_lname = txtPassportLName.getText();
                theFamily.personForeigner.passport_no = txtPassportNo.getText();
                theFamily.personForeigner.passport_no_exp = jdcPassportExp.getDate();
                theFamily.personForeigner.foreigner_no = rbtnForeignerGroup2.isSelected()
                        ? txtForeignerNo.getText()
                        : "";
                theFamily.personForeigner.foreigner_no_exp = rbtnForeignerGroup2.isSelected()
                        ? jdcForeignerNoExp.getDate()
                        : null;
                theFamily.personForeigner.f_person_foreigner_id = rbtnForeignerGroup2.isSelected()
                        ? Gutil.getGuiData(jcbForeignerType)
                        : "0";
                theFamily.personForeigner.foreigner_no_type = rbtnForeignerGroup2.isSelected()
                        ? Gutil.getGuiData(jcbForeignerNoType)
                        : "0";
                theFamily.personForeigner.employer_name = rbtnForeignerGroup2.isSelected()
                        ? txtEmployerName.getText()
                        : "";
                theFamily.personForeigner.employer_type = rbtnForeignerGroup2.isSelected()
                        ? (rbtnEmployerType1.isSelected() ? 1 : (rbtnEmployerType2.isSelected() ? 2 : 0))
                        : 0;
                theFamily.personForeigner.employer_id = rbtnForeignerGroup2.isSelected()
                        ? txtEmployerCID.getText()
                        : "";
                theFamily.personForeigner.employer_registration = rbtnForeignerGroup2.isSelected()
                        ? txtEmployerReg.getText()
                        : "";
                theFamily.personForeigner.employer_address_house = panelAddressEmployer.getHouse();
                theFamily.personForeigner.employer_address_moo = panelAddressEmployer.getVillage();
                theFamily.personForeigner.employer_address_road = panelAddressEmployer.getRoad();
                theFamily.personForeigner.employer_address_tambon = panelAddressEmployer.getTambon();
                theFamily.personForeigner.employer_address_amphur = panelAddressEmployer.getAmpur();
                theFamily.personForeigner.employer_address_changwat = panelAddressEmployer.getChangwat();
                theFamily.personForeigner.employer_address_postcode = panelAddressEmployer.getPostcode();
                theFamily.personForeigner.employer_contact_phone_number = panelAddressEmployer.getPhone();
                theFamily.personForeigner.employer_contact_mobile_phone_number = panelAddressEmployer.getMobile();
            } else {
                if (theFamily.personForeigner != null && theFamily.personForeigner.getObjectId() != null) {
                    theFamily.personForeigner.active = "0";
                }
            }
        }
    }

    @Override
    public void notifyUpdatePatientPictureProfile(Object obj) {

    }
}

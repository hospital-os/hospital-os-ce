/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.objdb.objdbclass;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.pcu.object.FFunctionalTest;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sompr
 */
public class FFunctionalTestDB {

    private final ConnectionInf connectionInf;

    public FFunctionalTestDB(ConnectionInf db) {
        connectionInf = db;
    }

    public FFunctionalTest select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_functional_test where f_functional_test_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<FFunctionalTest> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<FFunctionalTest> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_functional_test order by f_functional_test_id";
            preparedStatement = connectionInf.ePQuery(sql);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<FFunctionalTest> list(String... ids) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_functional_test where f_functional_test_id in (?) order by f_functional_test_id";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setArray(1, connectionInf.getConnection().createArrayOf("varchar", ids));
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<FFunctionalTest> listByKeyword(String keyword) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_functional_test where description ilike ? order by description";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, "%" + (keyword == null || keyword.isEmpty() ? "" : keyword) + "%");
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<FFunctionalTest> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<FFunctionalTest> list = new ArrayList<FFunctionalTest>();
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()) {
            FFunctionalTest obj = new FFunctionalTest();
            obj.setObjectId(rs.getString("f_functional_test_id"));
            obj.description = rs.getString("description");
            list.add(obj);
        }
        rs.close();
        return list;
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (FFunctionalTest obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }

    public List<CommonInf> getComboboxDatasource(String... ids) throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (FFunctionalTest obj : list(ids)) {
            list.add((CommonInf) obj);
        }
        return list;
    }
}

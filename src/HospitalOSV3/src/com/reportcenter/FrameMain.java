/*
 * FrameMain.java
 *
 * Created on 28 �á�Ҥ� 2548, 14:41 �.
 */
package com.reportcenter;

import com.hospital_os.gui.connection.*;
import com.hospital_os.object.MenuItemPlugIn;
import com.hospital_os.object.Version;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.usecase.connection.ModuleInfTool;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hospital_os.utility.Audio;
import com.hosv3.control.HosControl;
import com.hosv3.control.version.CheckVersionHos;
import com.hosv3.control.version.CheckVersionReport;
import com.hosv3.gui.dialog.DialogTheme2;
import com.hosv3.utility.ConnectionDBMgr;
import com.hosv3.utility.DialogConfig;
import com.hosv3.utility.ThreadStatus;
import com.reportcenter.control.ManageControl;
import com.reportcenter.control.ManageGUI;
import com.reportcenter.gui.dialog.*;
import com.reportcenter.help.ShowHelpReportCenter;
import com.reportcenter.utility.Language;
import java.awt.Color;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileInputStream;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author tong(Padungrat)
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class FrameMain extends javax.swing.JFrame
        implements ManageGUI, UpdateStatus {

    public static final String filename = "./.reportcenter.cfg";
    private static final long serialVersionUID = 1L;
    private final String DONT_REMIND = "DONT_REMIND";
    private final String SERVER = "SERVER";
    private final String DATABASE = "DATABASE";
    private final String PORT = "PORT";
    private final String USERNAME = "USERNAME";
    private final String PASSWORD = "PASSWORD";
    private final String URL = "URL";
    private final String DRIVER = "DRIVER";
    private final String TYPE = "TYPE";
    private final String DATABASETYPE = "DATABASETYPE";
    private ConnectionInf theConnectionInf = null;
    public Properties settings = new Properties();
    /**
     * �� Object �ͧ ModuleInfTool
     */
    private ModuleInfTool mi;
    private int language = 1;
    private boolean checkConnection = true;
    private DialogAllModule theDialogAllModule;
    private DialogHelp theDialogHelp;
    private DialogShowMessage theDialogShowMessage;
    /**
     * ��㹡�� �ʴ� �����ͧ͢ Center Report
     */
    private ShowHelpReportCenter theShowHelpReportCenter;
    private boolean selectChoose;
    ManageControl theManageControl;
    private HosControl theHC;
    private DialogTheme2 theDialogTheme;
    private UpdateStatus theUS;

    /**
     * Creates new form FrameMain
     */
    public FrameMain() {
        initComponents();
    }

    public FrameMain(ConnectionInf conf) {
        initComponents();
        setControl(conf, null);
    }

    public void setControl(ConnectionInf conf, HosControl hc) {
        //1. ����� args �Ѻ����������Ѻ Module
        selectChoose = true;
        theHC = hc;
        theUS = this;
        this.theConnectionInf = conf;
        setGUI();
        initComponentModule();
        //2. ��˹������˹�� GUI
        setLanguage();
        //3. ��˹� Property ��� 㹡�õԴ��Ͱҹ������
        initProperties();
        //4. ��㹡����ҹ�����š�õԴ��Ͱҹ�������������á�ͧ��� load �����
        theManageControl = new ManageControl(theConnectionInf);
        setVersion(this, true);
        //6. ��˹���Ҵ�ͧ GUI
        setShowGUI();
        //������١�õ�駤�Ңͧ�к��͡仡�͹ (���Ǥ���)
        jLabelPicWorking.setVisible(false);
        //���Ǩ�ͺ ����ա���� Connection �Ҩҡ ������������� ������ ��������������ö�Դ��
        if (selectChoose) {
            setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        }
    }

    public boolean setVersion(javax.swing.JFrame frm, boolean show) {
        try {
            theConnectionInf.open();
            AbsVersionControl cvreport = new CheckVersionReport(theConnectionInf);
            AbsVersionControl3 cvhos = new CheckVersionHos(this.theHC.theHosDB);
            if (!cvhos.isVersionCorrect() || !cvreport.isVersionCorrect()) {
                String warning = "<html><body><h3>"
                        + com.hosv3.utility.Constant.getTextBundle("�����������ѹ�ͧ�ҹ������ ����������ѹ�Ѩ�غѹ")
                        + "<br> ";
                if (!cvhos.isVersionCorrect()) {
                    warning += "<br> HospitalOS ";
                    warning += com.hosv3.utility.Constant.getTextBundle("�ҡ");
                    warning += " " + cvhos.getWarningMessage();
                }
                if (!cvreport.isVersionCorrect()) {
                    warning += "<br> Report ";
                    warning += com.hosv3.utility.Constant.getTextBundle("�ҡ");
                    warning += " " + cvreport.getWarningMessage();
                }
                warning += "<br></h3></body></html>";
                if (!theUS.confirmBox(warning, UpdateStatus.WARNING)) {
                    System.exit(0);
                }
            }
            return true;
        } catch (Exception e) {
            LOG.log(Level.SEVERE, e.getMessage(), e);
            return false;
        }
    }

    private void initDialog(String msg) {
        theDialogShowMessage = new DialogShowMessage(this, true);
        theDialogShowMessage.showDialogMessage(msg, true);
    }

    /**
     * ��㹡�á�˹���Ҵ����ʴ��ź� GUI ��С���ʴ��ٻ�Ҿ
     */
    private void setShowGUI() {
        this.setSize(800, 600);
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/com/reportcenter/images/icon.jpg")));
        jMenuItemModules.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/reportcenter/images/module.gif")));
        theShowHelpReportCenter = new ShowHelpReportCenter();
        theShowHelpReportCenter.addID(jMenuItemReportCenter);

    }

    public void initComponentModule() {
        theDialogAllModule = new DialogAllModule(this, true, null);
    }

    /**
     * ��㹡�á�˹� Connection ���������Ѻ �ء� Panel
     */
    public boolean initConnection() {
        if (this.theConnectionInf == null) {
            LOG.info("---Create Connection----");
            theConnectionInf = new ConnectionDBMgr(
                    settings.getProperty(DRIVER), settings.getProperty(URL), settings.getProperty(USERNAME), settings.getProperty(PASSWORD), settings.getProperty(TYPE));
        }
        return (theConnectionInf.open());
    }

    /**
     * ��㹡�á�˹� Property File �������
     */
    public void initProperties() {
        settings.put(DONT_REMIND, "0");
        settings.put(SERVER, "");
        settings.put(DATABASE, "");
        settings.put(PORT, "5432");
        settings.put(USERNAME, "");
        settings.put(PASSWORD, "");
        settings.put(URL, "");
        settings.put(DRIVER, "org.postgresql.Driver");
        settings.put(TYPE, "0");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelStatus = new javax.swing.JPanel();
        jLabelShowStatus = new javax.swing.JLabel();
        jLabelPicWorking = new javax.swing.JLabel();
        jPanelMainModule = new javax.swing.JPanel();
        jTabbedPaneReport = new javax.swing.JTabbedPane();
        jMenuBar = new javax.swing.JMenuBar();
        jMenuMain = new javax.swing.JMenu();
        jMenuItemConnect = new javax.swing.JMenuItem();
        jMenuItemConnect1 = new javax.swing.JMenuItem();
        jMenuItemTheme = new javax.swing.JMenuItem();
        jSeparator = new javax.swing.JSeparator();
        jMenuItemExit = new javax.swing.JMenuItem();
        jMenuTool = new javax.swing.JMenu();
        jMenuHelp = new javax.swing.JMenu();
        jSeparator1 = new javax.swing.JSeparator();
        jMenuItemAbout = new javax.swing.JMenuItem();
        jMenuItemModules = new javax.swing.JMenuItem();
        jMenuItemReportCenter = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });
        getContentPane().setLayout(new java.awt.GridBagLayout());

        jPanelStatus.setBackground(new java.awt.Color(204, 204, 204));
        jPanelStatus.setMinimumSize(new java.awt.Dimension(71, 21));
        jPanelStatus.setPreferredSize(new java.awt.Dimension(71, 21));
        jPanelStatus.setLayout(new java.awt.GridBagLayout());

        jLabelShowStatus.setOpaque(true);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanelStatus.add(jLabelShowStatus, gridBagConstraints);

        jLabelPicWorking.setPreferredSize(new java.awt.Dimension(22, 18));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        jPanelStatus.add(jLabelPicWorking, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        getContentPane().add(jPanelStatus, gridBagConstraints);

        jPanelMainModule.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanelMainModule.add(jTabbedPaneReport, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        getContentPane().add(jPanelMainModule, gridBagConstraints);

        jMenuMain.setText("����");

        jMenuItemConnect.setText("�Դ�������ͧ������");
        jMenuItemConnect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemConnectActionPerformed(evt);
            }
        });
        jMenuMain.add(jMenuItemConnect);

        jMenuItemConnect1.setText("�Դ�������ͧ���������ͧ");
        jMenuItemConnect1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemConnect1ActionPerformed(evt);
            }
        });
        jMenuMain.add(jMenuItemConnect1);

        jMenuItemTheme.setText("Look and Feel");
        jMenuItemTheme.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemThemeActionPerformed(evt);
            }
        });
        jMenuMain.add(jMenuItemTheme);
        jMenuMain.add(jSeparator);

        jMenuItemExit.setText("�͡�ҡ�к�");
        jMenuItemExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemExitActionPerformed(evt);
            }
        });
        jMenuMain.add(jMenuItemExit);

        jMenuBar.add(jMenuMain);

        jMenuTool.setText("����ͧ���");
        jMenuBar.add(jMenuTool);

        jMenuHelp.setText("��Ǫ���");
        jMenuHelp.add(jSeparator1);

        jMenuItemAbout.setText("����ǡѺ�����");
        jMenuItemAbout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemAboutActionPerformed(evt);
            }
        });
        jMenuHelp.add(jMenuItemAbout);

        jMenuItemModules.setText("ShowDetailModule");
        jMenuItemModules.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemModulesActionPerformed(evt);
            }
        });
        jMenuHelp.add(jMenuItemModules);

        jMenuItemReportCenter.setText("Report Center Help");
        jMenuHelp.add(jMenuItemReportCenter);

        jMenuBar.add(jMenuHelp);

        setJMenuBar(jMenuBar);

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width-800)/2, (screenSize.height-600)/2, 800, 600);
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItemThemeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemThemeActionPerformed
        String theme = "";
        File f = new File("config/gui/LookAndFeel.xml");
        try {
            FileInputStream fstream = new FileInputStream(f);
            byte b;
            char d[] = new char[100];
            int i = 0;
            do {
                b = (byte) fstream.read();
                d[i] = (char) b;
                if (b != -1) {
                    theme = theme + d[i];
                }
                i++;
            } while (b != -1);
            LOG.info(theme);
            fstream.close();
        } catch (Exception e) {
            LOG.log(Level.SEVERE, e.getMessage(), e);
        }
        if (theDialogTheme == null) {
            theDialogTheme = new DialogTheme2(theme, this, this.theHC);
        }
        theDialogTheme.setVisible(true);
    }//GEN-LAST:event_jMenuItemThemeActionPerformed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        this.doCloseApplication();
    }//GEN-LAST:event_formWindowClosed

    private void jMenuItemModulesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemModulesActionPerformed
        showDetailModules();
    }//GEN-LAST:event_jMenuItemModulesActionPerformed

    private void jMenuItemAboutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemAboutActionPerformed
        showDialogHelp();
    }//GEN-LAST:event_jMenuItemAboutActionPerformed

    private void jMenuItemExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemExitActionPerformed
        if (selectChoose) {
            this.doCloseApplication();
        } else {
            this.dispose();
        }
    }//GEN-LAST:event_jMenuItemExitActionPerformed

    private void jMenuItemConnectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemConnectActionPerformed
        boolean b = DialogConfig.showDialog(this, "�кذҹ������", true, false);
        if (b) {
            if (confirmBox("��سһԴ����Դ������ա����˹�� �׹�ѹ", UpdateStatus.WARNING)) {
                this.doCloseApplication();
            }
        }
    }//GEN-LAST:event_jMenuItemConnectActionPerformed

    private void jMenuItemConnect1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemConnect1ActionPerformed
        boolean b = DialogConfig.showDialog(this, "�кذҹ���������ͧ", true, true);
    }//GEN-LAST:event_jMenuItemConnect1ActionPerformed

    /**
     * ��㹡���ʴ���������ǹ�ͧ Setup�
     */
    public void showSetupReport() {
    }

    public void showDialogHelp() {
        if (theDialogHelp == null) {
            theDialogHelp = new DialogHelp(this, true);
        }
        theDialogHelp.showDialog();
    }

    public void showDetailModules() {
        if (theDialogAllModule == null) {
            theDialogAllModule = new DialogAllModule(this, true, null);
        }
        theDialogAllModule.setVisible(true);
    }

    /**
     * ��㹡���ʴ� Module ����Ѻ�Ҩҡ vector
     *
     * @param theModuleV �� Vector �ͧ Object ModuleInfTool
     */
    public void showModule(Vector theModuleV) {
        int size = theModuleV.size();
        LOG.log(Level.INFO, "Have {0} modules to load.", size);
        Vector vVersion = new Vector();
        for (int i = 0; i < size; i++) {
            LOG.log(Level.INFO, "Load module No.{0}", (i + 1));
            mi = (ModuleInfTool) theModuleV.get(i);
            //Set Connection for module
//            mi.setHosControl(this.theConnectionInf);
//            mi.setHosControl(this);
            mi.setHosControl(theHC);
            if (mi.getJPanel() != null) {
                LOG.log(Level.INFO, "Add Module in tab : {0}", mi.getNamePanel());
                jTabbedPaneReport.addTab(mi.getNamePanel(), mi.getJPanel());
                vVersion.add((Version) mi.getObjectVersion());
            }
            /**
             * ����������ŧ JmenuItem
             */
            Vector vMenuItem = mi.getVectorJMenuItem("");
            if (vMenuItem != null && vMenuItem.size() > 0) {
                LOG.log(Level.INFO, "This module have {0} menus.", vMenuItem.size());
                jMenuTool.add(new javax.swing.JSeparator());
                for (int j = 0, sizeMenuItem = vMenuItem.size(); j < sizeMenuItem; j++) {
                    MenuItemPlugIn theMenuItemPlugIn = (MenuItemPlugIn) vMenuItem.get(j);
                    if (theMenuItemPlugIn != null && theMenuItemPlugIn.authen) {
                        jMenuTool.add(theMenuItemPlugIn.theJMenuItem);
                        LOG.log(Level.INFO, "Add menu No.{0} \"{1}\" complete!",
                                new Object[]{j + 1, theMenuItemPlugIn.theJMenuItem.getText()});
                    }
                }
            }
        }
        theDialogAllModule.setAllModule(vVersion);
    }

    /**
     * ��㹡�� setConnection �ͧ module �ա�ͺ������ա�� set connection ����
     *
     * @param vmodule �� vector �ͧ module interface
     */
    public void setConnectionInModule(Vector vmodule) {
        if (vmodule != null) {
            JOptionPane.showMessageDialog(this, "SetConnection To Module");
            for (int i = 0, size = vmodule.size(); i < size; i++) {
                mi = (ModuleInfTool) vmodule.get(i);

                mi.setHosControl(theConnectionInf);
            }
        }
    }

    public void setLanguage() {
        this.setTitle("ReportCenter" + " - " + theHC.theSystemControl.getAppVersion().app_code);
        jMenuMain.setText(Language.getTextBundle(jMenuMain.getText(), language));
        jMenuHelp.setText(Language.getTextBundle(jMenuHelp.getText(), language));
        jMenuItemAbout.setText(Language.getTextBundle(jMenuItemAbout.getText(), language));
        jMenuItemConnect.setText(Language.getTextBundle(jMenuItemConnect.getText(), language));
        jMenuItemExit.setText(Language.getTextBundle(jMenuItemExit.getText(), language));
        jMenuItemModules.setText(Language.getTextBundle(jMenuItemModules.getText(), language));
        jMenuTool.setText(Language.getTextBundle(jMenuTool.getText(), language));
        jMenuItemReportCenter.setText(Language.getTextBundle(jMenuItemReportCenter.getText(), language));
        jMenuItemTheme.setText(Language.getTextBundle(jMenuItemTheme.getText(), language));
    }

    public void setGUI() {
        jMenuItemConnect.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/reportcenter/images/connect.gif")));
        jMenuItemExit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/reportcenter/images/delete.gif")));
        jMenuItemAbout.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/reportcenter/images/about_program.gif")));
    }

    @Override
    public void showDialogMessage(String msg, boolean shows) {
        initDialog(msg);
    }

    @Override
    public void setStatus(String str, int status) {
        ThreadStatus theTT = new ThreadStatus(this, this.jLabelShowStatus);
        jLabelShowStatus.setText(" " + str);
        if (status == UpdateStatus.WARNING) {
            jLabelShowStatus.setBackground(Color.YELLOW);
            if (theHC.theSystemControl.isSoundEnabled()) {
                Audio.getAudio("config/sound/warning.wav").play();
            }
        }
        if (status == UpdateStatus.COMPLETE) {
            jLabelShowStatus.setBackground(Color.GREEN);
            if (theHC.theSystemControl.isSoundEnabled()) {
                Audio.getAudio("config/sound/complete.wav").play();
            }
        }
        if (status == UpdateStatus.ERROR) {
            jLabelShowStatus.setBackground(Color.RED);
            if (theHC.theSystemControl.isSoundEnabled()) {
                Audio.getAudio("config/sound/error.wav").play();
            }
        }
        if (status == UpdateStatus.NORMAL)//amp:20/03/2549
        {
            jLabelShowStatus.setBackground(Color.GRAY);
        }
        theTT.start();
    }

    @Override
    public boolean confirmBox(String str, int status) {
        int i = JOptionPane.showConfirmDialog(this, str, "��͹", JOptionPane.YES_NO_OPTION);
        return (i == JOptionPane.YES_OPTION);
    }

    @Override
    public JFrame getJFrame() {
        return this;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabelPicWorking;
    private javax.swing.JLabel jLabelShowStatus;
    private javax.swing.JMenuBar jMenuBar;
    private javax.swing.JMenu jMenuHelp;
    private javax.swing.JMenuItem jMenuItemAbout;
    private javax.swing.JMenuItem jMenuItemConnect;
    private javax.swing.JMenuItem jMenuItemConnect1;
    private javax.swing.JMenuItem jMenuItemExit;
    private javax.swing.JMenuItem jMenuItemModules;
    private javax.swing.JMenuItem jMenuItemReportCenter;
    private javax.swing.JMenuItem jMenuItemTheme;
    private javax.swing.JMenu jMenuMain;
    private javax.swing.JMenu jMenuTool;
    private javax.swing.JPanel jPanelMainModule;
    private javax.swing.JPanel jPanelStatus;
    private javax.swing.JSeparator jSeparator;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTabbedPane jTabbedPaneReport;
    // End of variables declaration//GEN-END:variables

    private void doCloseApplication() {
        if (theConnectionInf != null) {
            try {
                if (!theConnectionInf.getConnection().getAutoCommit()) {
                    theConnectionInf.getConnection().commit();
                }
                if (!theConnectionInf.getConnection().isClosed()) {
                    theConnectionInf.getConnection().close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(FrameMain.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.exit(0);
    }
    private static final Logger LOG = Logger.getLogger(FrameMain.class.getName());
}

/*
 * SpecialQueryResultLabDB.java
 *
 * Created on 25 ���Ҥ� 2547, 20:58 �.
 */
package com.hospital_os.objdb.specialQuery;

import com.hospital_os.object.specialQuery.SpecialQueryResultLab;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.ResultSet;
import java.util.Vector;

/**
 *
 * @author Amp
 */
@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public class SpecialQueryResultLabDB {

    public ConnectionInf theConnectionInf;
    private SpecialQueryResultLab dbObj;

    public SpecialQueryResultLabDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    //count only sputum afb by patient
    public Vector queryLabAfbTbByPatientID(String pid, String startDate, String endDate) throws Exception {
        String sql = "SELECT t_result_lab.b_item_id as b_item_id, t_order.order_verify_date_time as order_verify_date_time, "
                + "t_result_lab.t_result_lab_id as t_result_lab_id, t_result_lab.t_visit_id as t_visit_id "
                + "from t_result_lab, t_order "
                + "where t_order.t_patient_id = '" + pid + "' "
                + "and (t_order.f_order_status_id <> '3' and t_order.f_order_status_id <> '0') "
                + "and (t_result_lab.b_item_id = '174TB00001' or t_result_lab.b_item_id = '174TB00002') "
                + "and t_result_lab.result_lab_active = '1' "
                + "and substr(t_order.order_verify_date_time,1,10) between '" + startDate + "' and '" + endDate + "' "
                + "and t_result_lab.t_order_id = t_order.t_order_id "
                + "GROUP BY t_result_lab.b_item_id, t_order.order_verify_date_time, t_result_lab_id , t_result_lab.t_visit_id "
                + "ORDER BY t_result_lab.b_item_id, t_order.order_verify_date_time";

        Vector vc = eQuery(sql);
        if (vc.isEmpty()) {
            return null;
        } else {
            return vc;
        }
    }

    private Vector eQuery(String sql) throws Exception {
        SpecialQueryResultLab p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        int i = 0;
        while (rs.next()) {
            p = new SpecialQueryResultLab();
            p.item_id = rs.getString("b_item_id");
            p.verify_date_time = rs.getString("order_verify_date_time");
            p.result_lab_id = rs.getString("t_result_lab_id");
            p.time = "";
            p.visit_id = rs.getString("t_visit_id");
            list.add(p);
        }
        rs.close();
        return list;
    }
}

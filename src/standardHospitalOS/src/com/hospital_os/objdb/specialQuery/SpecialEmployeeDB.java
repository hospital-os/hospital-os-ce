/*
 * SpecialPatientDB.java
 *
 * Created on 25 ���Ҥ� 2548, 09:58 �.
 */
package com.hospital_os.objdb.specialQuery;

import com.hospital_os.object.Authentication;
import com.hospital_os.object.OrderStatus;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.ComboFix;
import java.sql.ResultSet;
import java.util.Vector;

/**
 *
 * @author ojika
 */
@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public class SpecialEmployeeDB {

    public ConnectionInf theConnectionInf;

    public SpecialEmployeeDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public String queryByEmployeeName(String visit_id) throws Exception {
        String sql = "SELECT \n"
                + " case when f_patient_prefix.f_patient_prefix_id is not null then \n"
                + "case when f_patient_prefix.f_patient_prefix_id = '000' then '' \n"
                + "else f_patient_prefix.patient_prefix_description end else '' end\n"
                + "|| t_person.person_firstname || ' ' || t_person.person_lastname AS EMPLOYEE_NAME \n"
                + " FROM t_visit_service, b_employee \n"
                + "inner join t_person on t_person.t_person_id = b_employee.t_person_id\n"
                + "left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_person.f_prefix_id\n"
                + " WHERE t_visit_service.t_visit_id = '" + visit_id + "' \n"
                + " AND b_employee.b_employee_id = t_visit_service.visit_service_staff_doctor \n"
                + " ORDER BY t_visit_service.visit_service_finish_date_time DESC LIMIT 1 ";


        return eQuery(sql);
    }

    public Vector queryStaffDoctorVerifyOrder(String visit_id) throws Exception {
        String sql = "SELECT b_employee.b_employee_id, t_person.person_firstname, t_person.person_lastname \n"
                + " FROM b_employee INNER JOIN t_order ON b_employee.b_employee_id = t_order.order_staff_verify\n"
                + "inner join t_person on t_person.t_person_id = b_employee.t_person_id\n"
                //+ "left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_person.f_prefix_id\n"
                + " WHERE ("
                + "     ((t_order.f_order_status_id)<>'" + OrderStatus.DIS_CONTINUE + "'"
                + "   And (t_order.f_order_status_id)<>'" + OrderStatus.NOT_VERTIFY + "')"
                + " AND ((b_employee.f_employee_authentication_id)='" + Authentication.DOCTOR + "') "
                + " AND ((t_order.t_visit_id)='" + visit_id + "')"
                + " )\n"
                + " GROUP BY b_employee.b_employee_id, t_person.person_firstname, t_person.person_lastname";

        return eQueryStaffDoctor(sql);
    }

    private Vector eQueryStaffDoctor(String sql) throws Exception {
        Vector vc = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            ComboFix cf = new ComboFix();
            cf.name = rs.getString(2) + " " + rs.getString(3);
            cf.code = rs.getString(1);
            vc.add(cf);
        }
        rs.close();
        return vc;
    }

    /*////////////////////////////////////////////////////////////////////////////
     */
    private String eQuery(String sql) throws Exception {
        String employee_name = "";
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            employee_name = rs.getString("EMPLOYEE_NAME");
        }
        rs.close();
        return employee_name;
    }

    public Vector getDoctorInServiceDoctor(String staff_doctor_id) throws Exception {
        String sql = "SELECT b_employee.b_employee_id, t_person.person_firstname, t_person.person_lastname \n"
                + " FROM b_service_point_doctor ,b_employee\n"
                + "inner join t_person on t_person.t_person_id = b_employee.t_person_id\n"
                //+ "left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_person.f_prefix_id\n"
                + "where \n"
                + " b_service_point_doctor.service_point_doctor_staff = b_employee.b_employee_id \n"
                + "and b_service_point_doctor.b_service_point_id = '" + staff_doctor_id + "'";
        Vector vc = veQuery(sql);
        if (vc.isEmpty()) {
            return null;
        } else {
            return vc;
        }
    }

    private Vector veQuery(String sql) throws Exception {
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            ComboFix p = new ComboFix();
            p.code = rs.getString(1);
            p.name = rs.getString(2) + " " + rs.getString(3);
            list.add(p);
        }
        rs.close();
        return list;
    }
}

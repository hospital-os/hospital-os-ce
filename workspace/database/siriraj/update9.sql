DELETE FROM b_map_hos_siriraj_plan;
ALTER TABLE b_map_hos_siriraj_plan
	ADD CONSTRAINT b_map_hos_siriraj_plan_unique
	UNIQUE (b_contract_plans_id, b_srj_contract_plans_id);

INSERT INTO s_siriraj_version VALUES ('9750000000009', '9', 'Siriraj, Community Edition', '2.9.0', '1.4.0', to_char(current_timestamp, 'YYYY-MM-DD HH24:MI:SS'));

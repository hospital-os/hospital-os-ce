/*
 * SolveHNPatternThread.java
 *
 * Created on 16 �á�Ҥ� 2550, 13:55 �.
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package com.hosv3.control.thread;

import com.hospital_os.object.SequenceData;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hosv3.control.HosControl;
import com.hosv3.control.HosDB;
import com.hosv3.control.LookupControl;
import com.hosv3.object.HosObject;
import com.hosv3.utility.ResourceBundle;
import java.sql.SQLException;
import java.util.logging.Logger;

/**
 *
 * @author Aut
 */
public class SolveHNPatternThread extends ControlThread {

    LookupControl theLookupControl;

    /**
     * Creates a new instance of SolveHNPatternThread
     */
    public SolveHNPatternThread() {
        this.setDaemon(true);
    }

    @Override
    public void setControl(ConnectionInf con, HosDB hdb, HosObject ho, UpdateStatus us, Object control) {
        theConnectionInf = con;
        theHosDB = hdb;
        theHO = ho;
        theUS = us;
        theLookupControl = (LookupControl) control;
    }

    @Override
    protected void runTask() {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            SequenceData hn_seq = theLookupControl.getSequenceDataHN();
            int length = hn_seq.pattern.length();
            java.sql.ResultSet rs = theConnectionInf.eQuery(
                    "select t_patient_id,patient_hn from t_patient "
                    + " where length(patient_hn) <> " + length);
            int i = 0;
            while (rs.next()) {
                String patient_id = rs.getString(1);
                String hn = rs.getString(2);
                if (hn.length() != length) {
                    while (hn.length() < length) {
                        hn = "0" + hn;
                    }
                    while (hn.length() > length) {
                        hn = hn.substring(1);
                    }
                }
                int ret = theConnectionInf.eUpdate("update t_patient set"
                        + " patient_hn = '" + hn + "' where t_patient_id = "
                        + "'" + patient_id + "'");
                if (i % 300 == 0) {
                    theUS.setStatus("value " + i++ + " : " + ret, UpdateStatus.WARNING);
                }
            }
            theConnectionInf.getConnection().commit();
            theUS.confirmBox(ResourceBundle.getBundleText("control.SystemControl.RESOLVE.HN.PATTERN") + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.UNKONW, ResourceBundle.getBundleText("control.SystemControl.RESOLVE.HN.PATTERN")); 
            Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
    }
}

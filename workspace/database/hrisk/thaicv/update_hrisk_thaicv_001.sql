INSERT INTO f_heart_risk_type VALUES (2,'Thai CV Risk');


CREATE SCHEMA thai_ascvd;

CREATE OR REPLACE FUNCTION thai_ascvd.tascvd_formular(age numeric, smoke numeric, dm numeric, sbp numeric, sex numeric, tc numeric, ldl numeric, hdl numeric, whr numeric, wc numeric)
RETURNS text[] AS $$
DECLARE
        full_score numeric := 0;
        compare_score numeric := 0;
        predicted_risk numeric := 0;
        compare_risk numeric := 0;
        compare_whr numeric := 0.52667;
        compare_wc numeric := 79;
        compare_sbp numeric := 120;
        compare_hdl numeric := 44;
        sur_root numeric := 0.978296;
BEGIN   
        IF (sex = 0) THEN 
            compare_hdl = 49 ;
        END IF;
        IF (sex = 1 AND age > 60) THEN
            compare_sbp = 132 ;
        END IF;
        IF (sex = 0 AND age <= 60) THEN 
            compare_sbp = 115 ;
        END IF;
        IF (sex = 0 AND age > 60) THEN 
            compare_sbp = 130 ;
        END IF;
        IF (sex = 1) THEN
            compare_whr = 0.58125;
            compare_wc = 93;
        END IF;
        IF (age > 1 AND sbp > 50) THEN
            IF (ldl > 0) THEN            
                IF (hdl > 0) THEN
                    full_score = (0.08305 * age) + (0.24893 * sex) + (0.02164 * sbp) + (0.65224 * dm) + (0.00243 * ldl) + ((-0.01965) * hdl) + (0.43868 * smoke);
                    predicted_risk = 1 - (power(sur_root, exp(full_score - 5.9826)));
                    compare_score = (0.08305 * age) + (0.24893 * sex) + (0.02164 * compare_sbp) + (0.00243 * 130) + ((-0.01965) * compare_hdl);
                    compare_risk = 1 - (power(sur_root, exp(compare_score - 5.9826)));
                ELSE
                    full_score = (0.08169 * age) + (0.35156 * sex) + (0.02084 * sbp) + (0.65052 * dm) + (0.02094 * ldl) + (0.45639 * smoke);
                    predicted_risk = 1 - (power(sur_root, exp(full_score - 6.99911)));
                    compare_score = (0.08169 * age) + (0.35156 * sex) + (0.02084 * compare_sbp) + (0.02094 * 130);
                    compare_risk = 1 - (power(sur_root, exp(compare_score - 6.99911)));
                END IF;
            ELSEIF (tc > 0) THEN
                IF (hdl > 0) THEN
                    full_score = (0.083 * age) + (0.28094 * sex) + (0.02111 * sbp) + (0.69005 * dm) + (0.00214 * tc) + ((-0.02148) * hdl) + (0.40068 * smoke);
                    predicted_risk = 1 - (power(sur_root, exp(full_score - 6.00168)));
                    compare_score = (0.083 * age) + (0.28094 * sex) + (0.02111 * compare_sbp) + (0.00214 * 200) + ((-0.02148) * compare_hdl);
                    compare_risk = 1 - (power(sur_root, exp(compare_score - 6.00168)));
                ELSE
                    full_score = (0.08183 * age) + (0.39499 * sex) + (0.02084 * sbp) + (0.69974 * dm) + (0.00212 * tc) + (0.41916 * smoke);
                    predicted_risk = 1 - (power(sur_root, exp(full_score - 7.04423)));
                    compare_score = (0.08183 * age) + (0.39499 * sex) + (0.02084 * compare_sbp) + (0.00212 * 200);
                    compare_risk = 1 - (power(sur_root, exp(compare_score - 7.04423)));
                END IF;
            ELSEIF (whr > 0) THEN   --ของ อ.ปริญ มีสูตรนี้สูตรเดียว
                full_score = (0.079 * age) + (0.128 * sex) + (0.019350987 * sbp) + (0.58454 * dm) + (3.512566 * whr) + (0.459 * smoke);
                predicted_risk = 1 - (power(sur_root, exp(full_score - 7.720484)));
                compare_score = (0.079 * age) + (0.128 * sex) + (0.019350987 * compare_sbp) + (3.512566 * compare_whr);
                compare_risk = 1 - (power(sur_root, exp(compare_score - 7.720484)));
            ELSEIF (wc > 0) THEN
                full_score = (0.08372 * age) + (0.05988 * sex) + (0.02034 * sbp) + (0.59953 * dm) + (0.01283 * wc) + (0.459 * smoke);
                predicted_risk = 1 - (power(sur_root, exp(full_score - 7.31047)));
                compare_score = (0.08372 * age) + (0.05988 * sex) + (0.02034 * compare_sbp) + (0.01283 * compare_wc);
                compare_risk = 1 - (power(sur_root, exp(compare_score - 7.31047)));
            ELSE

            END IF;
        END IF;
        IF (predicted_risk > 0.3) THEN
            predicted_risk = 0.3 ;
        END IF;

        RETURN array[full_score::text , predicted_risk::text ,  compare_score::text, compare_risk::text ];

END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION thai_ascvd.tascvd_result(age numeric, smoke numeric, dm numeric, sbp numeric, sex_his numeric, tc numeric, ldl numeric, hdl numeric, whr numeric, wc_inch numeric, height numeric ,lang text)
RETURNS JSON AS $$
DECLARE
        sum_risk text[] ;
        tt_risk numeric;
        sc1 text;
        sc2 text;
        sc4 text;
        sc5 text;
        sug text;
        wc numeric;
        sex numeric;
BEGIN   
        --age in year, smoke 0-1, dm 0-1, sbp in mmHg, sex 0-1, lipid mg/dL, wc in cm, weight in kg, height in cm
        --HospitalOS sex 1=ชาย , 2 = หญิง
        IF (sex_his = 1) THEN 
            sex = 1;
        ELSEIF (sex_his = 2) THEN
            sex = 0;
        END IF;
        wc = trunc(wc_inch * 2.5);

        IF (wc > 0 AND height > 0) THEN
             whr = wc / height ;
        END IF;
        
        --FORMULAR
        sum_risk = thai_ascvd.tascvd_formular(age, smoke, dm, sbp, sex, tc, ldl, hdl, whr, wc);          
        --Display

       	IF (sum_risk[1+1]::numeric > 0) THEN
            IF (lang = 'th') THEN
                tt_risk = round((sum_risk[1+1]::numeric / sum_risk[3+1]::numeric),1);
                sc2 = round((sum_risk[1+1]::numeric * 100),2);
                IF (tt_risk > 1.1) THEN
                    sc1 = ('ซึ่งระดับความเสี่ยงของท่านสูงเป็น ' || tt_risk ||' เท่า');
                ELSEIF (tt_risk < 0.9) THEN
                    sc1 = ('ซึ่งระดับความเสี่ยงของท่านต่ำเป็น ' || tt_risk ||' เท่า');
                ELSE
                    sc1 = ('ซึ่งใกล้เคียงกับระดับความเสี่ยง');
                END IF;
                --Group of risk and suggestion
                sug := '';
                IF (smoke = 1) THEN 
                    sug = (sug || ' เลิกสูบบุหรี่');
                END IF;
                IF (dm = 1) THEN
                    sug = (sug || ' รักษาระดับน้ำตาลในเลือดให้อยู่ในเกณฑ์ปกติ'); 
                END IF;
                IF (sbp >= 140) THEN 
                    sug = (sug || ' ควบคุมระดับความดันโลหิตให้ดี'); 
                END IF;
                IF (tc >= 220 OR ldl >= 190) THEN 
                    sug = (sug || ' เข้ารับการรักษาเพื่อลดโคเรสเตอรอลในเลือด'); 
                END IF;
                IF ((wc >= 38 AND sex = 1) OR (wc > 32 AND sex = 0)) THEN 
                    sug = (sug || ' ลดน้ำหนักให้อยู่ในเกณฑ์ปกติ');
                END IF;
                IF (sum_risk[1+1]::numeric < 0.1) THEN
                    sc5 = ('จัดอยู่ในกลุ่มเสี่ยงน้อย');
                    sc4 = ('เพื่อป้องกันการเกิดโรคหลอดเลือดในอนาคต ควรออกกำลังกายอย่างสม่ำเสมอ รับประทานผักผลไม้เป็นประจำ' || sug || ' และตรวจสุขภาพประจำปี');
                ELSEIF (sum_risk[1+1]::numeric >= 0.1 AND sum_risk[1+1]::numeric < 0.2) THEN
                    sc5 = ('จัดอยู่ในกลุ่มเสี่ยงปานกลาง');
                    sc4 = ('ควรออกกำลังกายอย่างสม่ำเสมอ รับประทานผักผลไม้เป็นประจำ' || sug || ' และควรได้รับการตรวจร่างกายประจำปีอย่างสม่ำเสมอ');
                ELSEIF (sum_risk[1+1]::numeric >= 0.2) THEN
                    sc5 = ('จัดอยู่ในกลุ่มเสี่ยงสูง');
                    sc4 = ('ควรเข้ารับคำปรึกษาจากแพทย์ ในเบื้องต้นควรออกกำลังกายอย่างสม่ำเสมอ รับประทานผักผลไม้เป็นประจำ' || sug || ' และเข้ารับการตรวจสุขภาพประจำปีอย่างสม่ำเสมอ');
                ELSE
                    sc5 = ('ไม่พบความเสี่ยง');
                    sc4 = ('สามารถป้องกันการเกิดโรคหลอดเลือดหัวใจในอนาคตได้ด้วยการออกกำลังกายอย่างสม่ำเสมอ');
                END IF;

            ELSEIF (lang = 'en') THEN
                tt_risk = round((sum_risk[1+1]::numeric / sum_risk[3+1]::numeric),1);
                sc2 = round((sum_risk[1+1]::numeric * 100),2);

                IF (tt_risk > 1.1) THEN
                    sc1 = ('that is higher about ' || tt_risk ||' times ');
                ELSEIF (tt_risk < 0.9) THEN
                    sc1 = ('that is lower about ' || tt_risk ||' times ');
                ELSE
                    sc1 = ('that has no dIFferent ');
                END IF;
                --Group of risk and suggestion
                sug := '';
                IF (smoke = 1) THEN 
                    sug = sug || ', quit smoking'; 
                END IF;
                IF (dm = 1) THEN 
                    sug = sug + ', keep blood sugar level within normal range';
                END IF;
                IF (sbp >= 140) THEN 
                    sug = sug || ', achieve goal of blood pressure controlling'; 
                END IF;
                IF (tc >= 220 OR ldl >= 190) THEN 
                    sug = sug || ', intensIFy cholesterol-lowering therapy'; 
                END IF;
                IF ((wc >= 38 AND sex = 1) OR (wc > 32 AND sex = 0)) THEN 
                    sug = sug || ', body weight lowering'; 
                END IF;
                IF (sum_risk[1+1]::numeric < 0.1) THEN
                    sc5 = ('classIFied as low risk');
                    sc4 = ('It is reasonable to prevent atherosclerotic cardiovascular disease in the future by regular execise, high fiber dietary' || sug || ' and annual health checkup.');
                ELSEIF (sum_risk[1+1]::numeric >= 0.1 AND sum_risk[1+1]::numeric < 0.2) THEN
                    sc5 = ('classIFied as medium risk');
                    sc4 = ('You should have regular execise, high fiber dietary' || sug || ' and annual health checkup.');
                ELSEIF(sum_risk[1+1]::numeric >= 0.2) THEN
                    sc5 = ('classIFied as high risk');
                    sc4 = ('You must visit the physician for health checkup and recieve properly therapy. You must have regular execise, high fiber dietary' || sug || ' now.');
                ELSE
                    sc5 = ('no risk');
                    sc4 = ('Regular exercise could help you prevent atherosclerotic cardiovascular disease in the future.');
                END IF; 
            END IF; 
        END IF;

        RETURN json_build_object('parameter' ,json_build_object('age' ,age , 'smoke' ,smoke , 'dm' ,dm , 'sbp' ,sbp , 'sex' ,sex_his , 'tc' ,tc , 'ldl' ,ldl , 'hdl' ,hdl , 'whr' ,whr ,'wc' , wc_inch , 'height' , height ,'lang' ,lang) ,'result',json_build_object('sc1' ,sc1 ,'sc2',sc2 ,'sc4',sc4 ,'sc5',sc5)); 
        --RETURN json_build_object('sum_risk',sum_risk,'sum_risk_0' ,sum_risk[0+1],'sum_risk_1' ,sum_risk[1] ,'sum_risk_2',sum_risk[2] ,'sum_risk_3',sum_risk[3] ,'sum_risk_4',sum_risk[4]);
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION heart_risk_visit (visit_id text) 
RETURNS TABLE( t_visit_id text , t_heart_risk_id int ,  f_heart_risk_type_id int , heart_risk_type text ,  assessment_datetime timestamp ,heart_risk text,  heart_risk_result text , heart_risk_advice text ) 
AS $$
DECLARE
        patient_age int :=  (select case when  text_to_timestamp(t_patient.patient_birthday) is not null 
                                                        then EXTRACT(YEAR FROM age(text_to_timestamp(t_visit.visit_begin_visit_time),text_to_timestamp(t_patient.patient_birthday))) end::int AS patient_age 
                                    from t_visit inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
                                    where
                                            t_visit.t_visit_id = visit_id ) ; 
BEGIN
    RETURN QUERY
         select
                t_heart_risk.t_visit_id::text as t_visit_id
                ,t_heart_risk.t_heart_risk_id as t_heart_risk_id
                ,t_heart_risk.f_heart_risk_type_id as f_heart_risk_type_id
                ,f_heart_risk_type.description as heart_risk_type
                ,t_heart_risk.assessment_datetime as assessment_datetime
                ,case when patient_age not between 35 and 70 
                        then (t_heart_risk.result->>'retetype_label')::text
                        else (t_heart_risk.result->>'sc5')::text 
                        end as heart_risk

                ,case when patient_age not between 35 and 70 
                        then 
                'ค่าความเสี่ยงที่ได้คือ : '||(t_heart_risk.result->>'rate')::text        
                        else  
                'ความเสี่ยงต่อการเกิดโรคเส้นเลือดหัวใจและหลอดเลือดในระยะเวลา 10 ปีของท่านเท่ากับ '||(t_heart_risk.result->>'sc2')::text||'% '||(t_heart_risk.result->>'sc5')::text
                ||chr(10)||(t_heart_risk.result->>'sc1')::text||' ของคนไทยเพศเดียวกัน อายุเท่ากัน และปราศจากปัจจัยเสี่ยง'
                        end as heart_risk_result

                ,case when patient_age not between 35 and 70 
                        then 
                 (t_heart_risk.result->>'retetype_label')||chr(10)||(t_heart_risk.result->>'demeanour')
                        else 
                (t_heart_risk.result->>'sc4') 
                        end as  heart_risk_advice
              --  ,t_heart_risk.result
        from t_heart_risk 
                inner join (select
                                    t_heart_risk.t_visit_id
                                    ,max(t_heart_risk.assessment_datetime) as assessment_datetime
                                 from t_heart_risk
                                 where
                                    t_heart_risk.f_heart_risk_type_id = (case when patient_age not between 35 and 70 then 1 else 2 end)
                                    and t_heart_risk.active = '1'
                                    and t_heart_risk.t_visit_id = visit_id
                                group by
                                    t_heart_risk.t_visit_id ) as last_heart_risk
                on t_heart_risk.t_visit_id = last_heart_risk.t_visit_id
                        and t_heart_risk.assessment_datetime = last_heart_risk.assessment_datetime
                left join f_heart_risk_type on t_heart_risk.f_heart_risk_type_id = f_heart_risk_type.f_heart_risk_type_id
        where
                t_heart_risk.f_heart_risk_type_id = (case when patient_age not between 35 and 70 then 1 else 2 end)
                and t_heart_risk.active = '1'
                and t_heart_risk.t_visit_id = visit_id ;
END;
$$ LANGUAGE plpgsql;


-- create s_hrisk_thaicv_version
CREATE TABLE s_hrisk_thaicv_version (
    version_id            varchar(255) NOT NULL,
    version_number            	varchar(255) NULL,
    version_description       	varchar(255) NULL,
    version_application_number	varchar(255) NULL,
    version_database_number   	varchar(255) NULL,
    version_update_time       	varchar(255) NULL 
);

ALTER TABLE s_hrisk_thaicv_version
	ADD CONSTRAINT s_hrisk_thaicv_version_pkey
	PRIMARY KEY (version_id);

INSERT INTO s_hrisk_thaicv_version VALUES ('1', '1', 'Thai CV Risk Module', '1.0.0', '1.0.0', (select current_date) || ','|| (select current_time));

INSERT INTO s_script_update_log values ('Thai_CV_Risk_Module','update_hrisk_thaicv_001.sql',(select current_date) || ','|| (select current_time),'Initialize Thai CV Risk Module');

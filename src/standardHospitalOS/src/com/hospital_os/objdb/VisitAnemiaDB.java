/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.VisitAnemia;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.Gutil;
import java.sql.ResultSet;
import java.util.Vector;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class VisitAnemiaDB {

    private final ConnectionInf theConnectionInf;
    private final String tableId = "989";

    public VisitAnemiaDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public int insert(VisitAnemia obj) throws Exception {
        String sql = "INSERT INTO t_visit_anemia( "
                + "t_visit_anemia_id, t_visit_id, anemia_status, user_record, record_date_time, "
                + "user_modify, modify_date_time, active) "
                + "VALUES ('%s', '%s', '%s', '%s', '%s', "
                + "'%s', '%s', '%s')";
        sql = String.format(sql, obj.getGenID(tableId), obj.t_visit_id, obj.anemia_status,
                obj.user_record, obj.record_date_time,
                obj.user_modify, obj.modify_date_time, "1");
        return theConnectionInf.eUpdate(sql);
    }

    public int update(VisitAnemia obj) throws Exception {
        String sql = "UPDATE t_visit_anemia "
                + "SET anemia_status='%s',  "
                + "user_modify='%s', modify_date_time='%s' "
                + "WHERE t_visit_anemia_id='%s' ";
        sql = String.format(sql, obj.anemia_status,
                obj.user_modify, obj.modify_date_time, obj.getObjectId());
        return theConnectionInf.eUpdate(sql);
    }

    public VisitAnemia selectById(String id) throws Exception {
        String sql = "select * from t_visit_anemia where t_visit_anemia_id = '%s'";
        Vector eQuery = eQuery(String.format(sql, id));
        return (VisitAnemia) (eQuery.isEmpty() ? null : eQuery.get(0));
    }

    public VisitAnemia selectByVisitId(String patient_id) throws Exception {
        String sql = "select * from t_visit_anemia where t_visit_id = '%s'";
        Vector eQuery = eQuery(String.format(sql, patient_id));
        return (VisitAnemia) (eQuery.isEmpty() ? null : eQuery.get(0));
    }

    public Vector<VisitAnemia> eQuery(String sql) throws Exception {
        Vector<VisitAnemia> list = new Vector<VisitAnemia>();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        while (rs.next()) {
            VisitAnemia p = new VisitAnemia();
            p.setObjectId(rs.getString("t_visit_anemia_id"));
            p.t_visit_id = rs.getString("t_visit_id");
            p.anemia_status = rs.getString("anemia_status");
            p.user_record = rs.getString("user_record");
            p.record_date_time = rs.getString("record_date_time");
            p.user_modify = rs.getString("user_modify");
            p.modify_date_time = rs.getString("modify_date_time");
            p.active = rs.getString("active");
            list.add(p);
        }
        rs.close();
        return list;
    }
}

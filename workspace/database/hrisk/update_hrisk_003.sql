DROP FUNCTION public.heart_risk_visit(visit_id text);

CREATE OR REPLACE FUNCTION public.heart_risk_visit(visit_id text)
 RETURNS TABLE(t_visit_id text, t_heart_risk_id integer, f_heart_risk_type_id integer, heart_risk_type text, assessment_datetime timestamp, heart_risk text, heart_risk_result text, heart_risk_advice text)
 LANGUAGE plpgsql
AS $function$
DECLARE
        patient_age int := 40; 
BEGIN
    RETURN QUERY
         select
                t_heart_risk.t_visit_id::text as t_visit_id
                ,t_heart_risk.t_heart_risk_id as t_heart_risk_id
                ,t_heart_risk.f_heart_risk_type_id as f_heart_risk_type_id
                ,f_heart_risk_type.description as heart_risk_type
                ,t_heart_risk.assessment_datetime as assessment_datetime
                ,case when patient_age not between 35 and 70 
                        then (t_heart_risk.result->>'retetype_label')::text
                        else (t_heart_risk.result->>'sc5')::text 
                        end as heart_risk

                ,case when patient_age not between 35 and 70 
                        then 
                'ค่าความเสี่ยงที่ได้คือ : '||(t_heart_risk.result->>'rate')::text        
                        else  
                'ความเสี่ยงต่อการเกิดโรคเส้นเลือดหัวใจและหลอดเลือดในระยะเวลา 10 ปีของท่านเท่ากับ '||(t_heart_risk.result->>'sc2')::text||'% '||(t_heart_risk.result->>'sc5')::text
                ||chr(10)||(t_heart_risk.result->>'sc1')::text||' ของคนไทยเพศเดียวกัน อายุเท่ากัน และปราศจากปัจจัยเสี่ยง'
                        end as heart_risk_result

                ,case when patient_age not between 35 and 70 
                        then 
                 (t_heart_risk.result->>'retetype_label')||chr(10)||(t_heart_risk.result->>'demeanour')
                        else 
                (t_heart_risk.result->>'sc4') 
                        end as  heart_risk_advice
              --  ,t_heart_risk.result
        from t_heart_risk 
                inner join (select
                                    t_heart_risk.t_visit_id
                                    ,max(t_heart_risk.assessment_datetime) as assessment_datetime
                                 from t_heart_risk
                                 where
                                    t_heart_risk.f_heart_risk_type_id = (case when patient_age not between 35 and 70 then 1 else 2 end)
                                    and t_heart_risk.active = '1'
                                    and t_heart_risk.t_visit_id = visit_id
                                group by
                                    t_heart_risk.t_visit_id ) as last_heart_risk
                on t_heart_risk.t_visit_id = last_heart_risk.t_visit_id
                        and t_heart_risk.assessment_datetime = last_heart_risk.assessment_datetime
                left join f_heart_risk_type on t_heart_risk.f_heart_risk_type_id = f_heart_risk_type.f_heart_risk_type_id
        where
                t_heart_risk.f_heart_risk_type_id = (case when patient_age not between 35 and 70 then 1 else 2 end)
                and t_heart_risk.active = '1'
                and t_heart_risk.t_visit_id = visit_id ;
END;
$function$
;

INSERT INTO s_hrisk_version VALUES ('3', '3', 'Heart Risk Module', '1.1.0', '1.1.1', (select current_date) || ','|| (select current_time));

INSERT INTO s_script_update_log values ('Heart_Risk_Module','update_hrisk_003.sql',(select current_date) || ','|| (select current_time),'Update Heart Risk Module');

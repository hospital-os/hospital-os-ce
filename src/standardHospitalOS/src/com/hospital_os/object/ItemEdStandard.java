package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

@SuppressWarnings("ClassWithoutLogger")
public class ItemEdStandard extends Persistent {
    private static final long serialVersionUID = 1L;

    public String grcode1 = "";
    public String name1 = "";
    public String grcode2 = "";
    public String name2 = "";
    public String grcode3 = "";
    public String name3 = "";
    public String grcode4 = "";
    public String name4 = "";
    public String generic_name = "";
    public String syn_name = "";
    public String type = "";
    public String dosage = "";
    public String ed = "";

    @Override
    public String toString() {
        return generic_name;
    }
    
    
}

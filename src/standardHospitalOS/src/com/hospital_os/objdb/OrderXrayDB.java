/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.OrderXray;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author sompr
 */
public class OrderXrayDB {

    public ConnectionInf theConnectionInf;
    final public String idtable = "864";

    public OrderXrayDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public int insert(OrderXray o) throws Exception {
        OrderXray p = o;
        p.generateOID(idtable);
        String sql = "INSERT INTO t_order_xray(\n"
                + "            t_order_xray_id, t_visit_id, t_order_id, b_modality_id, accession_number, \n"
                + "            priority, order_status, pacs_status, doctor_id, order_datetime, \n"
                + "            executor_id, execute_datetime, cancel_id, cancel_datetime)\n"
                + "    VALUES (?, ?, ?, ?, ?, \n"
                + "            ?, ?, ?, ?, " + (p.order_datetime != null ? "?" : "CURRENT_TIMESTAMP") + ", \n"
                + "            ?, " + (p.execute_datetime != null ? "?" : "CURRENT_TIMESTAMP") + ", ?, ?)";
        PreparedStatement ePQuery = null;
        try {
            ePQuery = theConnectionInf.ePQuery(sql);
            int index = 1;
            ePQuery.setString(index++, p.getObjectId());
            ePQuery.setString(index++, p.t_visit_id);
            ePQuery.setString(index++, p.t_order_id);
            ePQuery.setString(index++, p.b_modality_id);
            ePQuery.setString(index++, p.accession_number);
            ePQuery.setString(index++, p.priority);
            ePQuery.setString(index++, p.order_status);
            ePQuery.setString(index++, p.pacs_status);
            ePQuery.setString(index++, p.doctor_id);
            if (p.order_datetime != null) {
                ePQuery.setTimestamp(index++, new java.sql.Timestamp(p.order_datetime.getTime()));
            }
            ePQuery.setString(index++, p.executor_id);
            if (p.execute_datetime != null) {
                ePQuery.setTimestamp(index++, new java.sql.Timestamp(p.execute_datetime.getTime()));
            }
            ePQuery.setString(index++, p.cancel_id);
            ePQuery.setTimestamp(index++, p.cancel_datetime != null ? new java.sql.Timestamp(p.cancel_datetime.getTime()) : null);
            return ePQuery.executeUpdate();
        } finally {
            if (ePQuery != null) {
                ePQuery.close();
            }
        }
    }

    public int cancel(String orderId, String cancelId, Date cancelDate) throws Exception {
        String sql = "UPDATE t_order_xray SET order_status = '0', pacs_status = '0',cancel_id = ?, cancel_datetime = " + (cancelDate != null ? "?" : "CURRENT_TIMESTAMP") + " where t_order_id = ?";
        PreparedStatement ePQuery = null;
        try {
            ePQuery = theConnectionInf.ePQuery(sql);
            int index = 1;
            ePQuery.setString(index++, cancelId);
            if (cancelDate != null) {
                ePQuery.setTimestamp(index++, new java.sql.Timestamp(cancelDate.getTime()));
            }
            ePQuery.setString(index++, orderId);
            return ePQuery.executeUpdate();
        } finally {
            if (ePQuery != null) {
                ePQuery.close();
            }
        }
    }

    public OrderXray select(String orderXrayId) throws Exception {
        String sql = "select * from t_order_xray where t_order_xray_id = ?";
        PreparedStatement ePQuery = null;
        try {
            ePQuery = theConnectionInf.ePQuery(sql);
            int index = 1;
            ePQuery.setString(index++, orderXrayId);
            List<OrderXray> v = eQuery(ePQuery.toString());
            return v.isEmpty() ? null : v.get(0);
        } finally {
            if (ePQuery != null) {
                ePQuery.close();
            }
        }
    }

    public List<OrderXray> selectByPtid(String visitId) throws Exception {
        String sql = "select * from t_order_xray where t_visit_id = ?";
        PreparedStatement ePQuery = null;
        try {
            ePQuery = theConnectionInf.ePQuery(sql);
            int index = 1;
            ePQuery.setString(index++, visitId);
            return eQuery(ePQuery.toString());
        } finally {
            if (ePQuery != null) {
                ePQuery.close();
            }
        }
    }

    public List<OrderXray> eQuery(String sql) throws Exception {
        OrderXray p;
        List<OrderXray> list = new ArrayList<OrderXray>();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        while (rs.next()) {
            p = new OrderXray();
            p.setObjectId(rs.getString("t_order_xray_id"));
            p.t_visit_id = rs.getString("result_xray_t_visit_id");
            p.t_order_id = rs.getString("t_patient_id");
            p.b_modality_id = rs.getString("t_visit_id");
            p.accession_number = rs.getString("result_xray");
            p.priority = rs.getString("result_xray_film_size");
            p.order_status = rs.getString("result_xray_staff_execute");
            p.pacs_status = rs.getString("record_date_time");
            p.doctor_id = rs.getString("result_xray_doctor_id");
            p.order_datetime = rs.getTimestamp("t_order_id");
            p.executor_id = rs.getString("result_xray_notice");
            p.execute_datetime = rs.getTimestamp("result_xray_staff_record");
            p.cancel_datetime = rs.getTimestamp("result_xray_complete");
            p.cancel_id = rs.getString("result_xray_cancel_id");
            p.record_datetime = rs.getTimestamp("record_datetime");
            p.dicom_status = rs.getString("dicom_status");
            p.dicom_text = rs.getString("dicom_text");
            list.add(p);

        }
        rs.close();
        return list;
    }

    public String getAccessionNumber(String visitId, String vn) throws Exception {
        String sql = "select max(t_order_xray.accession_number) as accessionNo from t_order_xray where t_visit_id = ?";
        PreparedStatement ePQuery = null;
        ResultSet rs = null;
        String accessionNo = "";
        try {
            ePQuery = theConnectionInf.ePQuery(sql);
            ePQuery.setString(1, visitId);
            rs = ePQuery.executeQuery();
            if (rs.next()) {
                accessionNo = rs.getString("accessionNo");
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ePQuery != null) {
                ePQuery.close();
            }
        }
        if (accessionNo == null || accessionNo.isEmpty()) {
            return vn + "-0001";
        } else {
            String vnOld = accessionNo.substring(0, accessionNo.indexOf("-"));
            String nextNumber = String.valueOf(Integer.parseInt(accessionNo.substring(accessionNo.indexOf("-") + 1)) + 1);
            switch (nextNumber.length()) {
                case 1:
                    accessionNo = vnOld + "-000" + nextNumber;
                    break;
                case 2:
                    accessionNo = vnOld + "-00" + nextNumber;
                    break;
                case 3:
                    accessionNo = vnOld + "-0" + nextNumber;
                    break;
                default:
                    accessionNo = vnOld + "-" + nextNumber;
                    break;
            }
            return accessionNo;
        }
    }
}

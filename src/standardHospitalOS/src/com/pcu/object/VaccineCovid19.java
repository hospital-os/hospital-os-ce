/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.object;

import com.hospital_os.usecase.connection.Persistent;
import java.util.Date;

/**
 *
 * @author tanakrit
 */
public class VaccineCovid19 extends Persistent {

    public String t_patient_id;
    public String t_visit_id;
    public String b_health_epi_group_id;
    public int vaccine_no;
    public Date receive_date;
    public Date receive_time;
    public String b_health_vaccine_id;
    public String barcode;
    public String description;
    public boolean sign_consent = false;
    public boolean follow_up = false;
    public boolean line_hmo_promp = false;
    public String user_procedure_id;
    public int sent_complete = 0;
    public String active = "1";
    public Date record_date_time = new Date();
    public String user_record_id;
    public Date update_date_time = new Date();
    public String user_update_id;
    public Date cancel_date_time;
    public String user_cancel_id;
    public int ref_code;
    public String act_site_code;
    public String immunization_route_code;
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class AccidentSysptomSpeak extends Persistent implements CommonInf {

    public String description;
    public String accident_symptom_speak_score;

    @Override
    public String getCode() {
        return getObjectId();
    }

    @Override
    public String getName() {
        return accident_symptom_speak_score + " : " + description;
    }

    @Override
    public String toString() {
        return description;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class AllergyIcd10 extends Persistent implements CommonInf {
    
    public String allergy_icd10_number = "";
    public String description = "";

    @Override
    public String getCode() {
        return getObjectId();
    }

    @Override
    public String getName() {
        return description;
    }

    @Override
    public String toString() {
        return (allergy_icd10_number == null || allergy_icd10_number.isEmpty() ? "" : allergy_icd10_number + ": ") + description;
    }
}

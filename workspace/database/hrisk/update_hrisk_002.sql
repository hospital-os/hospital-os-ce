INSERT INTO b_map_heart_risk_item_lab (b_map_heart_risk_item_lab_id,lab_code,lab_description,user_update_id) 
VALUES (4,'32502','ไขมันไทรกลีเซอรายด์ (Triglyceride)','')
,(5,'32203','น้ำตาล (FBS)','');


CREATE OR REPLACE FUNCTION patient_last_triglyceride (patient_id text ,assessment_datetime timestamp )
RETURNS JSON AS $$
        select
               json_build_object('value' ,t_result_lab.result_lab_value::numeric
                        , 'record_datetime' ,text_to_timestamp(t_order.order_report_date_time) 
						) as triglyceride
        from t_patient inner join t_visit on t_patient.t_patient_id =t_visit.t_patient_id
                inner join t_order on t_visit.t_visit_id = t_order.t_visit_id
                inner join t_result_lab on t_order.t_order_id = t_result_lab.t_order_id
                inner join (
                select
                        t_patient.t_patient_id
                        ,max(text_to_timestamp(t_order.order_report_date_time)) as record_datetime
                from t_patient inner join t_visit on t_patient.t_patient_id =t_visit.t_patient_id
                        inner join t_order on t_visit.t_visit_id = t_order.t_visit_id
                        inner join t_result_lab on t_order.t_order_id = t_result_lab.t_order_id
                where
                        t_patient.patient_active = '1'
                        and t_visit.f_visit_status_id <> '4'
                        and t_order.f_order_status_id = '4'
                        and t_result_lab.b_item_id in (select b_item_id from b_map_heart_risk_item_lab where b_map_heart_risk_item_lab_id = 4)
                        and t_result_lab.result_lab_value ~ '^([0-9]+\.?[0-9]*|\.[0-9]+)$' 
                        and t_patient.t_patient_id = patient_id
                        and text_to_timestamp(t_order.order_report_date_time) between (assessment_datetime - interval '1 year') and assessment_datetime 
                group by
                        t_patient.t_patient_id) as last_triglyceride
                on  t_patient.t_patient_id = last_triglyceride.t_patient_id
                        and text_to_timestamp(t_order.order_report_date_time) = last_triglyceride.record_datetime
        where
                t_patient.patient_active = '1'
                and t_visit.f_visit_status_id <> '4'
                and t_order.f_order_status_id = '4'
                and t_result_lab.b_item_id in (select b_item_id from b_map_heart_risk_item_lab where b_map_heart_risk_item_lab_id = 4)
                and t_result_lab.result_lab_value ~ '^([0-9]+\.?[0-9]*|\.[0-9]+)$' 
                and t_patient.t_patient_id = patient_id
                and text_to_timestamp(t_order.order_report_date_time) between (assessment_datetime - interval '1 year') and assessment_datetime  ;
$$ LANGUAGE SQL;


CREATE OR REPLACE FUNCTION patient_last_fbs (patient_id text ,assessment_datetime timestamp )
RETURNS JSON AS $$
        select
               json_build_object('value' ,t_result_lab.result_lab_value::numeric
                        , 'record_datetime' ,text_to_timestamp(t_order.order_report_date_time) 
						) as fbs
        from t_patient inner join t_visit on t_patient.t_patient_id =t_visit.t_patient_id
                inner join t_order on t_visit.t_visit_id = t_order.t_visit_id
                inner join t_result_lab on t_order.t_order_id = t_result_lab.t_order_id
                inner join (
                select
                        t_patient.t_patient_id
                        ,max(text_to_timestamp(t_order.order_report_date_time)) as record_datetime
                from t_patient inner join t_visit on t_patient.t_patient_id =t_visit.t_patient_id
                        inner join t_order on t_visit.t_visit_id = t_order.t_visit_id
                        inner join t_result_lab on t_order.t_order_id = t_result_lab.t_order_id
                where
                        t_patient.patient_active = '1'
                        and t_visit.f_visit_status_id <> '4'
                        and t_order.f_order_status_id = '4'
                        and t_result_lab.b_item_id in (select b_item_id from b_map_heart_risk_item_lab where b_map_heart_risk_item_lab_id = 5)
                        and t_result_lab.result_lab_value ~ '^([0-9]+\.?[0-9]*|\.[0-9]+)$' 
                        and t_patient.t_patient_id = patient_id
                        and text_to_timestamp(t_order.order_report_date_time) between (assessment_datetime - interval '1 year') and assessment_datetime 
                group by
                        t_patient.t_patient_id) as last_fbs
                on  t_patient.t_patient_id = last_fbs.t_patient_id
                        and text_to_timestamp(t_order.order_report_date_time) = last_fbs.record_datetime
        where
                t_patient.patient_active = '1'
                and t_visit.f_visit_status_id <> '4'
                and t_order.f_order_status_id = '4'
                and t_result_lab.b_item_id in (select b_item_id from b_map_heart_risk_item_lab where b_map_heart_risk_item_lab_id = 5)
                and t_result_lab.result_lab_value ~ '^([0-9]+\.?[0-9]*|\.[0-9]+)$' 
                and t_patient.t_patient_id = patient_id
                and text_to_timestamp(t_order.order_report_date_time) between (assessment_datetime - interval '1 year') and assessment_datetime  ;
$$ LANGUAGE SQL;

INSERT INTO s_hrisk_version VALUES ('2', '2', 'Heart Risk Module', '1.1.0', '1.1.0', (select current_date) || ','|| (select current_time));

INSERT INTO s_script_update_log values ('Heart_Risk_Module','update_hrisk_002.sql',(select current_date) || ','|| (select current_time),'Initialize Heart Risk Module');

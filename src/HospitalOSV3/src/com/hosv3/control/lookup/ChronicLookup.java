/*
 * ChronicLookup.java
 *
 * Created on 17 �Զع�¹ 2549, 10:02 �.
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
package com.hosv3.control.lookup;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.LookupControlInf;
import com.hosv3.control.LookupControl;
//import com.hosv3.utility.*;

/**
 *
 * @author amp
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class ChronicLookup implements LookupControlInf {

    private LookupControl thePC;

    /**
     * Creates a new instance of ChronicLookup
     */
    public ChronicLookup(LookupControl pc) {
        thePC = pc;
    }

    @Override
    public java.util.Vector listData(String str) {
        return thePC.listGroupChronicEng(str);
    }

    public CommonInf readData(String pk) {
        return thePC.readGroupChronicByCode(pk);
    }

    @Override
    public CommonInf readHosData(String pk) {
        return thePC.readGroupChronicByCode(pk);
    }
}

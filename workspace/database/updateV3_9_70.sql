CREATE TABLE public.t_module_licenses (
    id uuid NOT NULL,
    hcode CHARACTER VARYING(10) NOT NULL,
    module_id CHARACTER VARYING(50) NOT NULL,
    license text NOT NULL,
    expires_at timestamptz NOT NULL,
    record_date_time timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
    user_record_id CHARACTER VARYING(30) NOT NULL,
    CONSTRAINT t_module_licenses_pk PRIMARY KEY (id),
    CONSTRAINT t_module_licenses_un UNIQUE (module_id)
);

-- update db version
INSERT INTO s_version VALUES ('9701000000108', '108', 'Hospital OS, Community Edition', '3.9.70', '3.49.0', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_70.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.70b01');

-- require when use other postgres user
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA public to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA thai_ascvd to hospitalos;
-- GRANT ALL ON all sequences in schema thai_ascvd to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA rp50 to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA rp50nhso to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA rp50data to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA health_insurance to hospitalos;
-- GRANT ALL ON all sequences in schema public to hospitalos; 
-- GRANT ALL ON all sequences in schema rp50 to hospitalos; 
-- GRANT ALL ON all sequences in schema rp50nhso to hospitalos; 
-- GRANT ALL ON all sequences in schema rp50data to hospitalos; 
-- GRANT ALL ON all sequences in schema health_insurance to hospitalos;
-- GRANT ALL ON all sequences in schema thai_ascvd to hospitalos;
-- GRANT USAGE ON SCHEMA public TO hospitalos;
-- GRANT USAGE ON SCHEMA rp50 TO hospitalos;
-- GRANT USAGE ON SCHEMA rp50nhso TO hospitalos;
-- GRANT USAGE ON SCHEMA rp50data TO hospitalos;
-- GRANT USAGE ON SCHEMA health_insurance TO hospitalos;
-- GRANT USAGE ON SCHEMA thai_ascvd TO hospitalos;
/*
 * CheckBoxEdit.java
 *
 * Created on 24 ����Ҥ� 2547, 21:31 �.
 */
package com.hospital_os.utility;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author tong
 */
@SuppressWarnings("ClassWithoutLogger")
public class CheckBoxEditor extends DefaultCellEditor implements ItemListener {
    private static final long serialVersionUID = 1L;

    private JCheckBox button;
    private boolean is_edit = true;

    public CheckBoxEditor(JCheckBox checkBox) {
        super(checkBox);
        button = checkBox;
    }

    public void setCheckBoxEnabled(boolean b) {
        is_edit = b;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value,
            boolean isSelected, int row, int column) {
        if (value == null) {
            return null;
        }
        try {
            button = (JCheckBox) value;
            button.setEnabled(is_edit);//henbe_test
            button.addItemListener(this);
        } catch (Exception e) {
            Boolean val = (Boolean) value;
            button.setSelected(val.booleanValue());
        }
        return button;
    }
    //���繵�ͧ�տѧ�ѹ�����������к������騴����Ҥ�������������͡������������ 
    //�ҡ����տѧ�ѹ��� �ѹ��� reset ����������ʴ���ҷ������� set ������

    @Override
    public Object getCellEditorValue() {
        return Boolean.valueOf(button.isSelected());
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        super.fireEditingStopped();
    }
}

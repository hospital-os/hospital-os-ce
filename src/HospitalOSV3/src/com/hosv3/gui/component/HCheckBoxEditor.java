/*
 * CheckBoxEdit.java
 *
 * Created on 24 ����Ҥ� 2547, 21:31 �.
 */
package com.hosv3.gui.component;

import com.hosv3.utility.connection.*;
import java.awt.Component;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Hashtable;
import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JTable;

/**
 *
 * @author tong
 */
@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public class HCheckBoxEditor extends DefaultCellEditor implements ItemListener {

    private static final long serialVersionUID = 1L;
    private JCheckBox button = new JCheckBox();
    private ExecuteControlInf theEC;
    private String type;

    ///////////////////////////////////////////////////////////////////////////
    public HCheckBoxEditor(JCheckBox checkBox) {
        super(checkBox);
        button.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxContinueActionPerformed(evt);
            }
        });
    }
    ///////////////////////////////////////////////////////////////////////////

    public void setEControl(ExecuteControlInf ec) {
        theEC = ec;
    }
    ///////////////////////////////////////////////////////////////////////////

    public void setType(String str) {
        type = str;
    }
    ///////////////////////////////////////////////////////////////////////////

    public void jCheckBoxContinueActionPerformed(java.awt.event.ActionEvent evt) {
        if (theEC == null) {
            return;
        }

        if (button.isSelected()) {
            boolean b = theEC.execute(type + "1");
            if (!b) {
                button.setSelected(false);
            }
        } else {
            boolean b = theEC.execute(type + "0");
            if (!b) {
                button.setSelected(true);
            }
        }
    }
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value,
            boolean isSelected, int row, int column) {
        if (value instanceof Boolean) {
            button.setSelected(((Boolean) value).booleanValue());
        }
        if (value instanceof Hashtable) {
            Hashtable ht_status = (Hashtable) value;
            button.setSelected(((Boolean) ht_status.get("request")).booleanValue());
            button.setEnabled(((Boolean) ht_status.get("status")).booleanValue());
        }
        return button;
    }
    ///////////////////////////////////////////////////////////////////////////
    //���繵�ͧ�տѧ�ѹ�����������к������騴����Ҥ�������������͡������������ 
    //�ҡ����տѧ�ѹ��� �ѹ��� reset ����������ʴ���ҷ������� set ������

    @Override
    public Object getCellEditorValue() {
        return Boolean.valueOf(button.isSelected());
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        super.fireEditingStopped();
    }
}

-- map 12/43 Files
CREATE TABLE b_map_rp1855_education
(
  id character varying(255) NOT NULL,
  r_rp1855_education_id character varying(255) NOT NULL,
  f_patient_education_id character varying(255) NOT NULL,
  CONSTRAINT b_map_rp1855_education_pkey PRIMARY KEY (id)
);

CREATE TABLE b_map_rp1855_instype
(
  id character varying(255) NOT NULL,
  r_rp1855_instype_id character varying(255) NOT NULL,
  b_contract_plans_id character varying(255) NOT NULL,
  CONSTRAINT b_map_rp1855_instype_pkey PRIMARY KEY (id)
);

CREATE TABLE b_map_rp1855_nation
(
  id character varying(255) NOT NULL,
  r_rp1855_nation_id character varying(255) NOT NULL,
  f_patient_nation_id character varying(255) NOT NULL,
  CONSTRAINT b_map_rp1855_nation_pkey PRIMARY KEY (id)
);

CREATE TABLE b_map_rp1855_occupation
(
  id character varying(255) NOT NULL,
  r_rp1855_occupation_id character varying(255) NOT NULL,
  f_patient_occupation_id character varying(255) NOT NULL,
  CONSTRAINT b_map_rp1855_occupation_pkey PRIMARY KEY (id)
);

CREATE TABLE b_map_rp1855_religion
(
  id character varying(255) NOT NULL,
  r_rp1855_religion_id character varying(255) NOT NULL,
  f_patient_religion_id character varying(255) NOT NULL,
  CONSTRAINT b_map_rp1855_religion_pkey PRIMARY KEY (id)
);

CREATE TABLE r_rp1855_education
(
  id character varying(255) NOT NULL,
  name character varying(255) NOT NULL,
  CONSTRAINT education_pkey PRIMARY KEY (id)
);

INSERT INTO r_rp1855_education (id, name) VALUES ('0','ไม่ได้ศึกษา/ไม่มีวุฒิการศึกษา');
INSERT INTO r_rp1855_education (id, name) VALUES ('1','ก่อนประถมศึกษา   หมายถึง   ระดับการศึกษา   เตรียมอนุบาล,อนุบาล,เด็กเล็ก,ต่ำกว่าประถมศึกษา');
INSERT INTO r_rp1855_education (id, name) VALUES ('2','ประถมศึกษา  หมายถึง ระดับการศึกษาประถมศึกษาปีที่ 1-7');
INSERT INTO r_rp1855_education (id, name) VALUES ('3','มัธยมศึกษา  หมายถึง  ระดับการศึกษามัธยมศึกษาตอนต้น (ม.1-3) / มัธยมศึกษา ตอนปลาย  (ม.4- 6)   มัธยมศึกษาตอนต้นสายสามัญ (ม.ศ.1-3) / มัธยมศึกษาตอนปลาย สายสามัญ (ม.ศ.4-5) / มัธยมศึกษา(ม.1-8) / ประกาศนียบัตรวิชาชีพ (ปวช.) ');
INSERT INTO r_rp1855_education (id, name) VALUES ('4','อนุปริญญา   หมายถึง   ระดับการศึกษาประกาศนียบัตรวิชาชีพ เทคนิค (ปวท.)   ประกาศนียบัตรวิชาชีพชั้นสูง(ปวส.), อนุปริญญา , นาฎศิลป์ชั้นสูง');
INSERT INTO r_rp1855_education (id, name) VALUES ('5','ปริญญาตรี   หมายถึง   ระดับการศึกษา   ปริญญาตรี,ประกาศนียบัตรวิชาชีพเทคนิคครูชั้นสูง   ประกาศนียบัตรบัณฑิต');
INSERT INTO r_rp1855_education (id, name) VALUES ('6','สูงกว่าปริญญาตรี    หมายถึง   ระดับการศึกษา  ปริญญาโท,ประกาศนียบัตรบัณฑิตชั้นสูง  ปริญญาเอก/เทียบเท่า');
INSERT INTO r_rp1855_education (id, name) VALUES ('9','ไม่ระบุ/ไม่ทราบ หมายถึง ไม่ได้ระบุไว้ตามที่กำหนด');

CREATE TABLE r_rp1855_instype
(
  id character varying(255) NOT NULL,
  name character varying(255) NOT NULL,
  CONSTRAINT instype_pkey PRIMARY KEY (id)
);

INSERT INTO r_rp1855_instype (id, name) VALUES ('0100','หลักประกันสุขภาพ');
INSERT INTO r_rp1855_instype (id, name) VALUES ('1100','ข้าราชการประจำ');
INSERT INTO r_rp1855_instype (id, name) VALUES ('1200','ข้าราชการบำนาญ');
INSERT INTO r_rp1855_instype (id, name) VALUES ('1300','ลูกจ้างประจำ');
INSERT INTO r_rp1855_instype (id, name) VALUES ('1400','ลูกจ้างประจำบำนาญ');
INSERT INTO r_rp1855_instype (id, name) VALUES ('1900','ข้าราชการการเมือง');
INSERT INTO r_rp1855_instype (id, name) VALUES ('2101','ข้าราชการประจำกรุงเทพมหานคร');
INSERT INTO r_rp1855_instype (id, name) VALUES ('2102','ข้าราชการบำนาญกรุงเทพมหานคร');
INSERT INTO r_rp1855_instype (id, name) VALUES ('2103','ลูกจ้างประจำกรุงเทพมหานคร');
INSERT INTO r_rp1855_instype (id, name) VALUES ('2104','ลูกจ้างประจำบำนาญกรุงเทพมหานคร');
INSERT INTO r_rp1855_instype (id, name) VALUES ('2105','ข้าราชการประจำเมืองพัทยา');
INSERT INTO r_rp1855_instype (id, name) VALUES ('2106','ข้าราชการบำนาญเมืองพัทยา');
INSERT INTO r_rp1855_instype (id, name) VALUES ('2107','ลูกจ้างประจำเมืองพัทยา');
INSERT INTO r_rp1855_instype (id, name) VALUES ('2108','ลูกจ้างประจำบำนาญเมืองพัทยา');
INSERT INTO r_rp1855_instype (id, name) VALUES ('2201','ข้าราชการประจำเทศบาล');
INSERT INTO r_rp1855_instype (id, name) VALUES ('2202','ข้าราชการบำนาญเทศบาล');
INSERT INTO r_rp1855_instype (id, name) VALUES ('2203','ลูกจ้างประจำเทศบาล');
INSERT INTO r_rp1855_instype (id, name) VALUES ('2204','ลูกจ้างประจำบำนาญเทศบาล');
INSERT INTO r_rp1855_instype (id, name) VALUES ('2301','ข้าราชการประจำองค์การบริหารส่วนจังหวัด');
INSERT INTO r_rp1855_instype (id, name) VALUES ('2302','ข้าราชการบำนาญองค์การบริหารส่วนจังหวัด');
INSERT INTO r_rp1855_instype (id, name) VALUES ('2303','ลูกจ้างประจำองค์การบริหารส่วนจังหวัด');
INSERT INTO r_rp1855_instype (id, name) VALUES ('2304','ลูกจ้างประจำบำนาญองค์การบริหารส่วนจังหวัด             ');
INSERT INTO r_rp1855_instype (id, name) VALUES ('2401','ข้าราชการประจำองค์การบริหารส่วนตำบล');
INSERT INTO r_rp1855_instype (id, name) VALUES ('2402','ข้าราชการบำนาญองค์การบริหารส่วนตำบล');
INSERT INTO r_rp1855_instype (id, name) VALUES ('2403','ลูกจ้างประจำองค์การบริหารส่วนตำบล');
INSERT INTO r_rp1855_instype (id, name) VALUES ('2404','ลูกจ้างประจำบำนาญองค์การบริหารส่วนตำบล');
INSERT INTO r_rp1855_instype (id, name) VALUES ('2501','ข้าราชการการเมืองกรุงเทพมหานคร');
INSERT INTO r_rp1855_instype (id, name) VALUES ('2502','ข้าราชการการเมืองเมืองพัทยา');
INSERT INTO r_rp1855_instype (id, name) VALUES ('2601','ข้าราชการการเมืองเทศบาล');
INSERT INTO r_rp1855_instype (id, name) VALUES ('2701','ข้าราชการการเมืององค์การบริหารส่วนจังหวัด');
INSERT INTO r_rp1855_instype (id, name) VALUES ('2801','ข้าราชการการเมืององค์การบริหารส่วนตำบล');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3101','การไฟฟ้านครหลวง');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3102','การไฟฟ้าฝ่ายผลิตแห่งประเทศไทย');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3103','การไฟฟ้าส่วนภูมิภาค');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3104','บริษัท ปตท. จำกัด (มหาชน)');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3201','การทางพิเศษแห่งประเทศไทย');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3202','การท่าเรือแห่งประเทศไทย');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3203','การรถไฟฟ้ามวลชนแห่งประเทศไทย');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3204','การรถไฟแห่งประเทศไทย');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3205','บริษัท การบินไทย จำกัด (มหาชน)');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3206','บริษัทขนส่ง จำกัด');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3207','บริษัท ท่าอากาศยายไทย จำกัด (มหาชน)');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3208','ไทยเดินเรือทะเล จำกัด');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3209','บริษัท วิทยุการบินแห่งประเทศไทย จำกัด');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3210','สถาบันการบินพลเรือน');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3211','องค์การขนส่งมวลชนกรุงเทพ');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3301','บริษัท กสท. โทรคมนาคม จำกัด (มหาชน)');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3302','บริษัท ทีโอที จำกัด (มหาชน)');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3303','บริษัทไปรษณีย์ไทย จำกัด');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3304','บริษัท อสมท. จำกัด');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3401','การเคหะแห่งชาติ');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3402','การประปานครหลวง');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3403','การประปาส่วนภูมิภาค');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3404','องค์การจัดการน้ำเสีย');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3501','การนิคมอุตสาหกรรมแห่งประเทศไทย');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3502','บริษัท อู่กรุงเทพ จำกัด');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3503','โรงงานไพ่ กรมสรรพสามิต');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3504','โรงงานยาสูบ กระทรวงการคลัง');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3505','โรงพิมพ์ตำรวจ สำนักงานตำรวจแห่งชาติ');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3506','องค์การสุรา กรมสรรพสามิต');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3601','บริษัท ส่งเสริมธุรกิจเกษตรกรไทย จำกัด');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3602','สำนักงานกองทุนสงเคราะห์การทำสวนยาง');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3603','องค์การสุรา กรมสรรพสามิต');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3604','องค์การส่งเสริมกิจการโคนมแห่งประเทศไทย');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3605','องค์การสวนพฤกษศาสตร์');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3606','องค์การสวนยาง');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3607','องค์การสะพานปลา');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3608','องค์การอุตสาหกรรมป่าไม้');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3701','บริษัท ธนารักษ์พัฒนาสินทรัพย์ จำกัด');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3702','สหโรงแรมไทยและการท่องเที่ยว จำกัด');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3703','สำนักงานสลากกินแบ่งรัฐบาล');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3704','องค์การคลังสินค้า');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3705','องค์การตลาด');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3801','การกีฬาแห่งประเทศไทย');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3802','การท่องเที่ยวแห่งประเทศไทย');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3803','สถาบันวิจัยวิทยาศาสตร์และเทคโนโลยีแห่งประเทศไทย');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3804','สำนักงานธนานุเคราะห์ กรมพัฒนาสังคมและสวัสดิการ');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3805','องค์การพิพิธภัณฑ์วิทยาศาสตร์แห่งชาติ');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3806','องค์การเภสัชกรรม');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3807','องค์การสวนสัตว์');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3901','ธนาคารกรุงไทย จำกัด (มหาชน)');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3902','ธนาคารพัฒนาวิสาหกิจขนาดกลางและขนาดย่อมแห่งประเทศไทย  ');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3903','ธนาคารเพื่อการเกษตรและสหกรณ์การเกษตร');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3904','ธนาคารเพื่อการส่งออกและนำเข้าแห่งประเทศไทย');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3905','ธนาคารออมสิน');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3906','ธนาคารอาคารสงเคราะห์');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3907','ธนาคารอิสลามแห่งประเทศไทย');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3908','บรรษัทตลาดรองสินเชื่อที่อยู่อาศัย');
INSERT INTO r_rp1855_instype (id, name) VALUES ('3909','บรรษัทประกันสินเชื่ออุตสาหกรรมขนาดย่อม');
INSERT INTO r_rp1855_instype (id, name) VALUES ('4100','กองทุนเงินทดแทน');
INSERT INTO r_rp1855_instype (id, name) VALUES ('4200','กองทุนประกันสังคม');
INSERT INTO r_rp1855_instype (id, name) VALUES ('5100','ประกันสุขภาพภาคเอกชน');
INSERT INTO r_rp1855_instype (id, name) VALUES ('6100','กองทุนผู้ประสบภัยจากรถ');
INSERT INTO r_rp1855_instype (id, name) VALUES ('7100','พนักงาน/เจ้าหน้าที่หรือผู้ปฏิบัติงานสังกัดองค์กรอิสระตามรัฐธรรมนูญ');
INSERT INTO r_rp1855_instype (id, name) VALUES ('8100','สิทธิพิเศษเฉพาะกลุ่มอนุเคราะห์');
INSERT INTO r_rp1855_instype (id, name) VALUES ('8200','สิทธิที่โรงพยาบาลให้การอนุเคราะห์');
INSERT INTO r_rp1855_instype (id, name) VALUES ('8300','บุคคลที่มีปัญหาสถานะ');
INSERT INTO r_rp1855_instype (id, name) VALUES ('8400','บุคคลที่มีสิทธิต่างด้าว');
INSERT INTO r_rp1855_instype (id, name) VALUES ('9100','ชำระเงินเอง');

CREATE TABLE r_rp1855_nation
(
  id character varying(255) NOT NULL,
  name character varying(255) NOT NULL,
  CONSTRAINT nation_pkey PRIMARY KEY (id)
);

INSERT INTO r_rp1855_nation (id, name) VALUES ('002','โปรตุเกส');
INSERT INTO r_rp1855_nation (id, name) VALUES ('003','ดัตช์');
INSERT INTO r_rp1855_nation (id, name) VALUES ('004','เยอรมัน');
INSERT INTO r_rp1855_nation (id, name) VALUES ('005','ฝรั่งเศส');
INSERT INTO r_rp1855_nation (id, name) VALUES ('006','เดนมาร์ก');
INSERT INTO r_rp1855_nation (id, name) VALUES ('007','สวีเดน');
INSERT INTO r_rp1855_nation (id, name) VALUES ('008','สวิส');
INSERT INTO r_rp1855_nation (id, name) VALUES ('009','อิตาลี');
INSERT INTO r_rp1855_nation (id, name) VALUES ('010','นอร์เวย์');
INSERT INTO r_rp1855_nation (id, name) VALUES ('011','ออสเตรีย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('012','ไอริช');
INSERT INTO r_rp1855_nation (id, name) VALUES ('013','ฟินแลนด์');
INSERT INTO r_rp1855_nation (id, name) VALUES ('014','เบลเยียม');
INSERT INTO r_rp1855_nation (id, name) VALUES ('015','สเปน');
INSERT INTO r_rp1855_nation (id, name) VALUES ('016','รัสเซีย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('017','โปแลนด์');
INSERT INTO r_rp1855_nation (id, name) VALUES ('018','เช็ก');
INSERT INTO r_rp1855_nation (id, name) VALUES ('019','ฮังการี');
INSERT INTO r_rp1855_nation (id, name) VALUES ('020','กรีก');
INSERT INTO r_rp1855_nation (id, name) VALUES ('021','ยูโกสลาฟ');
INSERT INTO r_rp1855_nation (id, name) VALUES ('022','ลักเซมเบิร์ก');
INSERT INTO r_rp1855_nation (id, name) VALUES ('023','วาติกัน');
INSERT INTO r_rp1855_nation (id, name) VALUES ('024','มอลตา');
INSERT INTO r_rp1855_nation (id, name) VALUES ('025','ลีซู');
INSERT INTO r_rp1855_nation (id, name) VALUES ('026','บัลแกเรีย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('027','โรมาเนีย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('028','ไซปรัส');
INSERT INTO r_rp1855_nation (id, name) VALUES ('029','อเมริกัน');
INSERT INTO r_rp1855_nation (id, name) VALUES ('030','แคนาดา');
INSERT INTO r_rp1855_nation (id, name) VALUES ('031','เม็กซิโก');
INSERT INTO r_rp1855_nation (id, name) VALUES ('032','คิวบา');
INSERT INTO r_rp1855_nation (id, name) VALUES ('033','อาร์เจนตินา');
INSERT INTO r_rp1855_nation (id, name) VALUES ('034','บราซิล');
INSERT INTO r_rp1855_nation (id, name) VALUES ('035','ชิลี');
INSERT INTO r_rp1855_nation (id, name) VALUES ('036','อาข่า');
INSERT INTO r_rp1855_nation (id, name) VALUES ('037','โคลัมเบีย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('038','ลั๊ว');
INSERT INTO r_rp1855_nation (id, name) VALUES ('039','เปรู');
INSERT INTO r_rp1855_nation (id, name) VALUES ('040','ปานามา');
INSERT INTO r_rp1855_nation (id, name) VALUES ('041','อุรุกวัย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('042','เวเนซุเอลา');
INSERT INTO r_rp1855_nation (id, name) VALUES ('043','เปอร์โตริโก้');
INSERT INTO r_rp1855_nation (id, name) VALUES ('044','จีน');
INSERT INTO r_rp1855_nation (id, name) VALUES ('045','อินเดีย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('046','เวียดนาม');
INSERT INTO r_rp1855_nation (id, name) VALUES ('047','ญี่ปุ่น');
INSERT INTO r_rp1855_nation (id, name) VALUES ('048','พม่า');
INSERT INTO r_rp1855_nation (id, name) VALUES ('049','ฟิลิปปิน');
INSERT INTO r_rp1855_nation (id, name) VALUES ('050','มาเลเซีย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('051','อินโดนีเซีย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('052','ปากีสถาน');
INSERT INTO r_rp1855_nation (id, name) VALUES ('053','เกาหลีใต้');
INSERT INTO r_rp1855_nation (id, name) VALUES ('054','สิงคโปร์');
INSERT INTO r_rp1855_nation (id, name) VALUES ('055','เนปาล');
INSERT INTO r_rp1855_nation (id, name) VALUES ('056','ลาว');
INSERT INTO r_rp1855_nation (id, name) VALUES ('057','กัมพูชา');
INSERT INTO r_rp1855_nation (id, name) VALUES ('058','ศรีลังกา');
INSERT INTO r_rp1855_nation (id, name) VALUES ('059','ซาอุดีอาระเบีย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('060','อิสราเอล');
INSERT INTO r_rp1855_nation (id, name) VALUES ('061','เลบานอน');
INSERT INTO r_rp1855_nation (id, name) VALUES ('062','อิหร่าน');
INSERT INTO r_rp1855_nation (id, name) VALUES ('063','ตุรกี');
INSERT INTO r_rp1855_nation (id, name) VALUES ('064','บังกลาเทศ');
INSERT INTO r_rp1855_nation (id, name) VALUES ('065','ถูกถอนสัญชาติ');
INSERT INTO r_rp1855_nation (id, name) VALUES ('066','ซีเรีย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('067','อิรัก');
INSERT INTO r_rp1855_nation (id, name) VALUES ('068','คูเวต');
INSERT INTO r_rp1855_nation (id, name) VALUES ('069','บรูไน');
INSERT INTO r_rp1855_nation (id, name) VALUES ('070','แอฟริกาใต้');
INSERT INTO r_rp1855_nation (id, name) VALUES ('071','กะเหรี่ยง');
INSERT INTO r_rp1855_nation (id, name) VALUES ('072','ลาหู่');
INSERT INTO r_rp1855_nation (id, name) VALUES ('073','เคนยา');
INSERT INTO r_rp1855_nation (id, name) VALUES ('074','อียิปต์');
INSERT INTO r_rp1855_nation (id, name) VALUES ('075','เอธิโอเปีย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('076','ไนจีเรีย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('077','สหรัฐอาหรับเอมิเรตส์');
INSERT INTO r_rp1855_nation (id, name) VALUES ('078','กินี');
INSERT INTO r_rp1855_nation (id, name) VALUES ('079','ออสเตรเลีย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('080','นิวซีแลนด์');
INSERT INTO r_rp1855_nation (id, name) VALUES ('081','ปาปัวนิวกินี');
INSERT INTO r_rp1855_nation (id, name) VALUES ('082','ม้ง');
INSERT INTO r_rp1855_nation (id, name) VALUES ('083','เมี่ยน');
INSERT INTO r_rp1855_nation (id, name) VALUES ('084','ชาวเขาที่ไม่ได้รับสัญขาติไทย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('085','-');
INSERT INTO r_rp1855_nation (id, name) VALUES ('086','จีนฮ่อ');
INSERT INTO r_rp1855_nation (id, name) VALUES ('087','จีน (อดีตทหารจีนคณะชาติ ,อดีตทหารจีนชาติ)');
INSERT INTO r_rp1855_nation (id, name) VALUES ('088','ผู้พลัดถิ่นสัญชาติพม่า');
INSERT INTO r_rp1855_nation (id, name) VALUES ('089','ผู้อพยพเชื้อสายจากกัมพูชา');
INSERT INTO r_rp1855_nation (id, name) VALUES ('090','ลาว (ลาวอพยพ)');
INSERT INTO r_rp1855_nation (id, name) VALUES ('091','เขมรอพยพ');
INSERT INTO r_rp1855_nation (id, name) VALUES ('092','ผู้อพยพอินโดจีนสัญชาติเวียดนาม');
INSERT INTO r_rp1855_nation (id, name) VALUES ('093','รอให้สัญชาติไทย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('094','ไทย-อิสลาม,อิสลาม-ไทย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('095','ไทย-จีน,จีน-ไทย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('096','ไร้สัญชาติ');
INSERT INTO r_rp1855_nation (id, name) VALUES ('097','อื่นๆ');
INSERT INTO r_rp1855_nation (id, name) VALUES ('098','ไม่ได้สัญชาติไทย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('099','ไทย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('100','อัฟกัน');
INSERT INTO r_rp1855_nation (id, name) VALUES ('101','บาห์เรน');
INSERT INTO r_rp1855_nation (id, name) VALUES ('102','ภูฏาน');
INSERT INTO r_rp1855_nation (id, name) VALUES ('103','จอร์แดน');
INSERT INTO r_rp1855_nation (id, name) VALUES ('104','เกาหลีเหนือ');
INSERT INTO r_rp1855_nation (id, name) VALUES ('105','มัลดีฟ');
INSERT INTO r_rp1855_nation (id, name) VALUES ('106','มองโกเลีย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('107','โอมาน');
INSERT INTO r_rp1855_nation (id, name) VALUES ('108','กาตาร์');
INSERT INTO r_rp1855_nation (id, name) VALUES ('109','เยเมน');
INSERT INTO r_rp1855_nation (id, name) VALUES ('110','เยเมน(ใต้)**');
INSERT INTO r_rp1855_nation (id, name) VALUES ('111','หมู่เกาะฟิจิ');
INSERT INTO r_rp1855_nation (id, name) VALUES ('112','คิริบาส');
INSERT INTO r_rp1855_nation (id, name) VALUES ('113','นาอูรู');
INSERT INTO r_rp1855_nation (id, name) VALUES ('114','หมู่เกาะโซโลมอน');
INSERT INTO r_rp1855_nation (id, name) VALUES ('115','ตองก้า');
INSERT INTO r_rp1855_nation (id, name) VALUES ('116','ตูวาลู');
INSERT INTO r_rp1855_nation (id, name) VALUES ('117','วานูอาตู');
INSERT INTO r_rp1855_nation (id, name) VALUES ('118','ซามัว');
INSERT INTO r_rp1855_nation (id, name) VALUES ('119','แอลเบเนีย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('120','อันดอร์รา');
INSERT INTO r_rp1855_nation (id, name) VALUES ('121','เยอรมนีตะวันออก**');
INSERT INTO r_rp1855_nation (id, name) VALUES ('122','ไอซ์แลนด์');
INSERT INTO r_rp1855_nation (id, name) VALUES ('123','ลิกเตนสไตน์');
INSERT INTO r_rp1855_nation (id, name) VALUES ('124','โมนาโก');
INSERT INTO r_rp1855_nation (id, name) VALUES ('125','ซานมารีโน');
INSERT INTO r_rp1855_nation (id, name) VALUES ('126','บริติช  (อังกฤษ, สก็อตแลนด์)');
INSERT INTO r_rp1855_nation (id, name) VALUES ('127','แอลจีเรีย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('128','แองโกลา');
INSERT INTO r_rp1855_nation (id, name) VALUES ('129','เบนิน');
INSERT INTO r_rp1855_nation (id, name) VALUES ('130','บอตสวานา');
INSERT INTO r_rp1855_nation (id, name) VALUES ('131','บูร์กินาฟาโซ');
INSERT INTO r_rp1855_nation (id, name) VALUES ('132','บุรุนดี');
INSERT INTO r_rp1855_nation (id, name) VALUES ('133','แคเมอรูน');
INSERT INTO r_rp1855_nation (id, name) VALUES ('134','เคปเวิร์ด');
INSERT INTO r_rp1855_nation (id, name) VALUES ('135','แอฟริกากลาง');
INSERT INTO r_rp1855_nation (id, name) VALUES ('136','ชาด');
INSERT INTO r_rp1855_nation (id, name) VALUES ('137','คอสตาริกา');
INSERT INTO r_rp1855_nation (id, name) VALUES ('138','คองโก');
INSERT INTO r_rp1855_nation (id, name) VALUES ('139','ไอโวเรี่ยน');
INSERT INTO r_rp1855_nation (id, name) VALUES ('140','จิบูตี');
INSERT INTO r_rp1855_nation (id, name) VALUES ('141','อิเควทอเรียลกินี');
INSERT INTO r_rp1855_nation (id, name) VALUES ('142','กาบอง');
INSERT INTO r_rp1855_nation (id, name) VALUES ('143','แกมเบีย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('144','กานา');
INSERT INTO r_rp1855_nation (id, name) VALUES ('145','กินีบีสเซา');
INSERT INTO r_rp1855_nation (id, name) VALUES ('146','เลโซโท');
INSERT INTO r_rp1855_nation (id, name) VALUES ('147','ไลบีเรีย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('148','ลิเบีย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('149','มาลากาซี');
INSERT INTO r_rp1855_nation (id, name) VALUES ('150','มาลาวี');
INSERT INTO r_rp1855_nation (id, name) VALUES ('151','มาลี');
INSERT INTO r_rp1855_nation (id, name) VALUES ('152','มอริเตเนีย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('153','มอริเชียส');
INSERT INTO r_rp1855_nation (id, name) VALUES ('154','โมร็อกโก');
INSERT INTO r_rp1855_nation (id, name) VALUES ('155','โมซัมบิก');
INSERT INTO r_rp1855_nation (id, name) VALUES ('156','ไนเจอร์');
INSERT INTO r_rp1855_nation (id, name) VALUES ('157','รวันดา');
INSERT INTO r_rp1855_nation (id, name) VALUES ('158','เซาโตเมและปรินซิเป');
INSERT INTO r_rp1855_nation (id, name) VALUES ('159','เซเนกัล');
INSERT INTO r_rp1855_nation (id, name) VALUES ('160','เซเชลส์');
INSERT INTO r_rp1855_nation (id, name) VALUES ('161','เซียร์ราลีโอน');
INSERT INTO r_rp1855_nation (id, name) VALUES ('162','โซมาลี');
INSERT INTO r_rp1855_nation (id, name) VALUES ('163','ซูดาน');
INSERT INTO r_rp1855_nation (id, name) VALUES ('164','สวาซี');
INSERT INTO r_rp1855_nation (id, name) VALUES ('165','แทนซาเนีย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('166','โตโก');
INSERT INTO r_rp1855_nation (id, name) VALUES ('167','ตูนิเซีย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('168','ยูกันดา');
INSERT INTO r_rp1855_nation (id, name) VALUES ('169','ซาอีร์');
INSERT INTO r_rp1855_nation (id, name) VALUES ('170','แซมเบีย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('171','ซิมบับเว');
INSERT INTO r_rp1855_nation (id, name) VALUES ('172','แอนติกาและบาร์บูดา');
INSERT INTO r_rp1855_nation (id, name) VALUES ('173','บาฮามาส');
INSERT INTO r_rp1855_nation (id, name) VALUES ('174','บาร์เบโดส');
INSERT INTO r_rp1855_nation (id, name) VALUES ('175','เบลิซ');
INSERT INTO r_rp1855_nation (id, name) VALUES ('176','คอสตาริกา');
INSERT INTO r_rp1855_nation (id, name) VALUES ('177','โดมินิกา');
INSERT INTO r_rp1855_nation (id, name) VALUES ('178','โดมินิกัน');
INSERT INTO r_rp1855_nation (id, name) VALUES ('179','เอลซัลวาดอร์');
INSERT INTO r_rp1855_nation (id, name) VALUES ('180','เกรเนดา');
INSERT INTO r_rp1855_nation (id, name) VALUES ('181','กัวเตมาลา');
INSERT INTO r_rp1855_nation (id, name) VALUES ('182','เฮติ');
INSERT INTO r_rp1855_nation (id, name) VALUES ('183','ฮอนดูรัส');
INSERT INTO r_rp1855_nation (id, name) VALUES ('184','จาเมกา');
INSERT INTO r_rp1855_nation (id, name) VALUES ('185','นิการากัว');
INSERT INTO r_rp1855_nation (id, name) VALUES ('186','เซนต์คิตส์และเนวิส');
INSERT INTO r_rp1855_nation (id, name) VALUES ('187','เซนต์ลูเซีย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('188','เซนต์วินเซนต์และเกรนาดีนส์');
INSERT INTO r_rp1855_nation (id, name) VALUES ('189','ตรินิแดดและโตเบโก');
INSERT INTO r_rp1855_nation (id, name) VALUES ('190','โบลีเวีย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('191','เอกวาดอร์');
INSERT INTO r_rp1855_nation (id, name) VALUES ('192','กายอานา');
INSERT INTO r_rp1855_nation (id, name) VALUES ('193','ปารากวัย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('194','ซูรินาเม');
INSERT INTO r_rp1855_nation (id, name) VALUES ('195','อาหรับ');
INSERT INTO r_rp1855_nation (id, name) VALUES ('196','คะฉิ่น');
INSERT INTO r_rp1855_nation (id, name) VALUES ('197','ว้า');
INSERT INTO r_rp1855_nation (id, name) VALUES ('198','ไทยใหญ่');
INSERT INTO r_rp1855_nation (id, name) VALUES ('199','ไทยลื้อ');
INSERT INTO r_rp1855_nation (id, name) VALUES ('200','ขมุ');
INSERT INTO r_rp1855_nation (id, name) VALUES ('201','ตองสู');
INSERT INTO r_rp1855_nation (id, name) VALUES ('202','เงี้ยว**');
INSERT INTO r_rp1855_nation (id, name) VALUES ('203','ละว้า');
INSERT INTO r_rp1855_nation (id, name) VALUES ('204','แม้ว');
INSERT INTO r_rp1855_nation (id, name) VALUES ('205','ปะหร่อง');
INSERT INTO r_rp1855_nation (id, name) VALUES ('206','ถิ่น');
INSERT INTO r_rp1855_nation (id, name) VALUES ('207','ปะโอ');
INSERT INTO r_rp1855_nation (id, name) VALUES ('208','มอญ');
INSERT INTO r_rp1855_nation (id, name) VALUES ('209','มลาบรี');
INSERT INTO r_rp1855_nation (id, name) VALUES ('210','เฮาะ**');
INSERT INTO r_rp1855_nation (id, name) VALUES ('211','สก็อตแลน์**');
INSERT INTO r_rp1855_nation (id, name) VALUES ('212','จีน (จีนฮ่ออิสระ)');
INSERT INTO r_rp1855_nation (id, name) VALUES ('213','จีนอพยพ**');
INSERT INTO r_rp1855_nation (id, name) VALUES ('214','จีน (จีนฮ่ออพยพ)');
INSERT INTO r_rp1855_nation (id, name) VALUES ('215','ไต้หวัน**');
INSERT INTO r_rp1855_nation (id, name) VALUES ('216','ยูเครน');
INSERT INTO r_rp1855_nation (id, name) VALUES ('217','อาณานิคมอังกฤษ**');
INSERT INTO r_rp1855_nation (id, name) VALUES ('218','ดูไบ**');
INSERT INTO r_rp1855_nation (id, name) VALUES ('219','จีน(ฮ่องกง)');
INSERT INTO r_rp1855_nation (id, name) VALUES ('220','จีน(ไต้หวัน)');
INSERT INTO r_rp1855_nation (id, name) VALUES ('221','โครเอเชีย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('223','คาซัค');
INSERT INTO r_rp1855_nation (id, name) VALUES ('222','บริทิธ**');
INSERT INTO r_rp1855_nation (id, name) VALUES ('224','อาร์เมเนีย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('225','อาเซอร์ไบจาน');
INSERT INTO r_rp1855_nation (id, name) VALUES ('226','จอร์เจีย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('227','คีร์กีซ');
INSERT INTO r_rp1855_nation (id, name) VALUES ('228','ทาจิก');
INSERT INTO r_rp1855_nation (id, name) VALUES ('229','อุซเบก');
INSERT INTO r_rp1855_nation (id, name) VALUES ('230','หมู่เกาะมาร์แชลล์');
INSERT INTO r_rp1855_nation (id, name) VALUES ('231','ไมโครนีเซีย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('232','ปาเลา');
INSERT INTO r_rp1855_nation (id, name) VALUES ('233','เบลารุส');
INSERT INTO r_rp1855_nation (id, name) VALUES ('234','บอสเนียและเฮอร์เซโกวีนา');
INSERT INTO r_rp1855_nation (id, name) VALUES ('235','เติร์กเมน');
INSERT INTO r_rp1855_nation (id, name) VALUES ('236','เอสโตเนีย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('237','ลัตเวีย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('238','ลิทัวเนีย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('239','มาซิโดเนีย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('240','มอลโดวา');
INSERT INTO r_rp1855_nation (id, name) VALUES ('241','สโลวัก');
INSERT INTO r_rp1855_nation (id, name) VALUES ('242','สโลวีน');
INSERT INTO r_rp1855_nation (id, name) VALUES ('243','เอริเทรีย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('244','นามิเบีย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('245','โบลิเวีย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('246','หมู่เกาะคุก');
INSERT INTO r_rp1855_nation (id, name) VALUES ('247','เนปาล (เนปาลอพยพ)');
INSERT INTO r_rp1855_nation (id, name) VALUES ('248','มอญ  (ผู้พลัดถิ่นสัญชาติพม่า)');
INSERT INTO r_rp1855_nation (id, name) VALUES ('249','ไทยใหญ่  (ผู้พลัดถิ่นสัญชาติพม่า)');
INSERT INTO r_rp1855_nation (id, name) VALUES ('250','เวียดนาม  (ญวนอพยพ)');
INSERT INTO r_rp1855_nation (id, name) VALUES ('251','มาเลเชีย  (อดีต จีนคอมมิวนิสต์)');
INSERT INTO r_rp1855_nation (id, name) VALUES ('252','จีน  (อดีต จีนคอมมิวนิสต์)');
INSERT INTO r_rp1855_nation (id, name) VALUES ('253','สิงคโปร์  (อดีต จีนคอมมิวนิสต์)');
INSERT INTO r_rp1855_nation (id, name) VALUES ('254','กะเหรี่ยง  (ผู้หลบหนีเข้าเมือง)');
INSERT INTO r_rp1855_nation (id, name) VALUES ('255','มอญ  (ผู้หลบหนีเข้าเมือง)');
INSERT INTO r_rp1855_nation (id, name) VALUES ('256','ไทยใหญ่  (ผู้หลบหนีเข้าเมือง)');
INSERT INTO r_rp1855_nation (id, name) VALUES ('257','กัมพูชา  (ผู้หลบหนีเข้าเมือง)');
INSERT INTO r_rp1855_nation (id, name) VALUES ('258','มอญ  (ชุมชนบนพื้นที่สูง)');
INSERT INTO r_rp1855_nation (id, name) VALUES ('259','กะเหรี่ยง  (ชุมชนบนพื้นที่สูง)');
INSERT INTO r_rp1855_nation (id, name) VALUES ('260','ปาเลสไตน์');
INSERT INTO r_rp1855_nation (id, name) VALUES ('261','ติมอร์ตะวันออก');
INSERT INTO r_rp1855_nation (id, name) VALUES ('262','สละสัญชาติไทย');
INSERT INTO r_rp1855_nation (id, name) VALUES ('263','เซอร์เบีย แอนด์ มอนเตเนโกร');
INSERT INTO r_rp1855_nation (id, name) VALUES ('264','กัมพูชา(แรงงาน)');
INSERT INTO r_rp1855_nation (id, name) VALUES ('265','พม่า(แรงงาน)');
INSERT INTO r_rp1855_nation (id, name) VALUES ('266','ลาว(แรงงาน)');
INSERT INTO r_rp1855_nation (id, name) VALUES ('267','เซอร์เบียน');
INSERT INTO r_rp1855_nation (id, name) VALUES ('268','มอนเตเนกริน');
INSERT INTO r_rp1855_nation (id, name) VALUES ('989','บุคคลที่ไม่มีสถานะทางทะเบียน');
INSERT INTO r_rp1855_nation (id, name) VALUES ('999','ไม่ระบุ');

INSERT INTO b_map_rp1855_nation VALUES ('99999000000000001','02','002');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000002','04','004');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000003','05','005');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000004','06','006');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000005','07','007');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000006','09','009');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000007','10','010');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000008','11','011');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000009','13','013');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000010','15','015');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000011','16','016');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000012','17','017');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000013','19','019');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000014','20','020');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000015','23','023');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000016','28','028');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000017','31','031');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000018','32','032');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000019','33','033');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000020','34','034');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000021','35','035');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000022','37','037');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000023','39','039');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000024','40','040');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000025','41','041');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000026','43','043');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000027','44','044');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000028','45','045');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000029','46','046');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000030','47','047');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000031','48','048');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000032','50','050');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000033','51','051');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000034','52','052');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000035','54','054');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000036','55','055');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000037','56','056');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000038','57','057');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000039','58','058');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000040','60','060');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000041','61','061');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000042','63','063');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000043','66','066');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000044','68','068');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000045','69','069');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000046','73','073');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000047','74','074');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000048','75','075');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000049','076','076');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000050','077','077');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000051','078','078');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000052','79','079');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000053','80','080');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000054','081','081');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000055','082','082');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000056','083','083');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000057','86','086');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000058','92','092');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000059','94','094');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000060','95','095');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000061','96','096');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000062','99','099');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000063','100','100');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000064','101','101');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000065','102','102');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000066','103','103');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000067','104','104');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000068','105','105');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000069','106','106');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000070','107','107');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000071','108','108');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000072','109','109');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000073','110','110');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000074','111','111');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000075','112','112');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000076','113','113');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000077','114','114');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000078','115','115');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000079','116','116');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000080','117','117');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000081','118','118');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000082','119','119');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000083','120','120');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000084','121','121');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000085','122','122');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000086','123','123');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000087','124','124');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000088','125','125');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000089','127','127');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000090','128','128');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000091','129','129');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000092','130','130');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000093','131','131');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000094','132','132');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000095','133','133');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000096','134','134');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000097','135','135');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000098','136','136');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000099','176','137');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000100','138','138');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000101','140','140');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000102','141','141');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000103','142','142');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000104','143','143');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000105','144','144');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000106','145','145');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000107','146','146');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000108','147','147');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000109','148','148');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000110','149','149');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000111','150','150');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000112','151','151');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000113','152','152');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000114','153','153');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000115','154','154');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000116','155','155');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000117','156','156');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000118','157','157');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000119','158','158');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000120','159','159');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000121','160','160');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000122','161','161');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000123','162','162');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000124','163','163');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000125','164','164');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000126','165','165');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000127','166','166');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000128','167','167');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000129','168','168');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000130','169','169');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000131','170','170');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000132','171','171');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000133','172','172');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000134','173','173');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000135','174','174');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000136','175','175');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000137','176','176');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000138','177','177');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000139','178','178');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000140','179','179');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000141','180','180');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000142','181','181');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000143','182','182');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000144','183','183');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000145','184','184');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000146','185','185');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000147','186','186');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000148','187','187');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000149','188','188');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000150','189','189');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000151','190','190');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000152','191','191');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000153','192','192');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000154','193','193');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000155','194','194');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000156','195','195');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000157','65','195');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000158','196','196');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000159','197','197');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000160','198','198');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000161','199','199');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000162','200','200');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000163','201','201');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000164','202','202');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000165','203','203');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000166','205','205');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000167','206','206');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000168','207','207');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000169','208','208');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000170','209','209');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000171','210','210');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000172','212','212');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000173','213','213');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000174','214','214');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000175','215','215');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000176','216','216');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000177','217','217');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000178','218','218');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000179','219','219');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000180','220','220');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000181','221','221');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000182','222','222');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000183','223','223');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000184','224','224');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000185','225','225');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000186','226','226');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000187','227','227');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000188','228','228');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000189','229','229');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000190','230','230');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000191','231','231');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000192','232','232');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000193','233','233');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000194','234','234');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000195','235','235');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000196','236','236');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000197','237','237');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000198','238','238');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000199','239','239');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000200','240','240');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000201','241','241');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000202','242','242');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000203','243','243');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000204','244','244');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000205','245','245');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000206','246','246');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000207','247','247');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000208','260','260');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000209','261','261');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000210','262','262');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000211','263','263');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000212','989','989');
INSERT INTO b_map_rp1855_nation VALUES ('99999000000000213','0','999');

CREATE TABLE r_rp1855_occupation
(
  id character varying(255) NOT NULL,
  code character varying(255) NOT NULL,
  name character varying(255) NOT NULL,
  CONSTRAINT occupation_pkey PRIMARY KEY (id)
);

INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('1','1111','ข้าราชการการเมืองและนักการเมือง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('2','1112','ข้าราชการระดับอาวุโส : ผู้ปฏิบัติงานเกี่ยวกับการให้คำปรึกษาด้านนโยบาย งบประมาณ และระเบียบข้อบังคับต่างๆ แก่รัฐบาล');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('3','1112','ผู้บัญชาการตำรวจแห่งชาติ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('4','1112','จเรตำรวจ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('5','1113','กำนัน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('6','1113','ผู้ใหญ่บ้าน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('7','1113','หัวหน้าหมู่บ้าน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('8','1120','กรรมการผู้จัดการและผู้บริหารระดับสูงภาคเอกชน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('9','1210','ผู้จัดการด้านบริการธุรกิจและบริหารจัดการ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('10','1211','ผู้จัดการด้านการเงิน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('11','1212','ผู้จัดการด้านทรัพยากรบุคคล');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('12','1213','ผู้จัดการด้านนโยบายและแผน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('13','1219','ผู้จัดการด้านบริการและบริหารจัดการ ซึ่งมิได้จัดประเภทไว้ที่อื่น');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('14','1220','ผู้จัดการด้านการขาย การตลาด และการพัฒนาธุรกิจ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('15','1221','ผู้จัดการด้านการขาย ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('16','1221','ผู้จัดการด้านการตลาด ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('17','1222','ผู้จัดการด้านโฆษณาและประชาสัมพันธ์ ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('18','1223','ผู้จัดการด้านวิจัยและพัฒนา ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('19','1311','ผู้จัดการด้านการผลิตในภาคการเกษตรและการป่าไม้');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('20','1311','ผู้จัดการผลิตด้านป่าไม้');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('21','1311','ผู้จัดการผลิตด้านเพาะปลูก');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('22','1311','ผู้จัดการผลิตด้านการเลี้ยงปศุสัตว์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('23','1312','ผู้จัดการฝ่ายผลิตด้านการเพาะเลี้ยงสัตว์น้ำ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('24','1312','ผู้จัดการฝ่ายผลิตด้านการประมง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('25','1312','ผู้จัดการเรือลากอวน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('26','1321','ผู้จัดการด้านการผลิต');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('27','1322','ผู้จัดการด้านการทำ/ผลิตเหมืองแร่');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('28','1322','ผู้จัดการด้านการทำ/ผลิตเหมืองหิน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('29','1322','ผู้จัดการฝ่ายผลิตในการขุดเจาะน้ำมันและก๊าซ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('30','1323','ผู้จัดการโครงการก่อสร้าง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('31','1323','ผู้รับเหมาก่อสร้าง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('32','1324','ผู้จัดการด้านการจัดหาและจัดส่งสินค้า');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('33','1324','ผู้จัดการสถานีรถประจำทาง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('34','1324','ผู้จัดการสถานีรถไฟ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('35','1324','ผู้จัดการบริษัทขนส่ง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('36','1330','ผู้จัดการด้านการบริการเทคโนโลยีสารสนเทศและการสื่อสาร');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('37','1341','ผู้จัดการด้านการบริการดูแลเด็ก');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('38','1342','ผู้จัดการด้านการบริการสุขภาพ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('39','1342','ผู้อำนวยการคลินิก');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('40','1342','ผู้อำนวยการสถานพยาบาล');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('41','1342','หัวหน้าสำนักงานสาธารณสุข');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('42','1342','หัวหน้างานพยาบาล');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('43','1343','ผู้จัดการด้านการบริการดูแลผู้สูงอายุ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('44','1343','ผู้จัดการบ้านพักคนชรา');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('45','1344','ผู้จัดการด้านงานสังคมสงเคราะห์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('46','1345','ผู้จัดการด้านการศึกษา');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('47','1345','ผู้อำนวยการวิทยาลัย');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('48','1345','ครูใหญ่');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('49','1345','คณบดีมหาวิทยาลัย');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('50','1345','ผู้อำนวยการโรงเรียน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('51','1346','ผู้จัดการธนาคาร');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('52','1346','ผู้จัดการสาขาสถานบันการเงิน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('53','1346','ผู้จัดการสหกรณ์เคหะ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('54','1346','ผู้จัดการด้านการประกันภัย');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('55','1349','ผู้กำกับการตำรวจ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('56','1349','ผู้บัญชาการเรือนจำ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('57','1411','ผู้จัดการโรงแรม');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('58','1412','ผู้จัดการภัตตาคาร/ร้านอาหาร');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('59','1420','ผู้จัดการด้านการค้าส่ง และค้าปลีก');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('60','1431','ผู้จัดการด้านการกีฬา นันทนาการ และศูนย์วัฒนธรรม');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('61','1431','ผู้จัดการโรงภาพยนต์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('62','1431','ผู้จัดการโรงเรียนฝึกสอนขี่ม้า');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('63','1431','ผู้จัดการโรงละคร');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('64','1439','ผู้จัดการด้านการบริการ ซึ่งมิได้จัดประเภทไว้ในที่อื่น');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('65','2111','นักฟิสิกส์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('66','2111','นักฟิสิกส์การแพทย์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('67','2111','นักนิวเคลียร์ฟิสิกส์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('68','2111','นักดาราศาสตร์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('69','2112','นักอุตุนิยมวิทยา');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('70','2112','นักอุตุ-อุทกวิทยา');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('71','2112','นักพยากรณ์อากาศ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('72','2113','นักเคมี');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('73','2114','นักธรณีวิทยาและนักธรณีฟิสิกส์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('74','2120','นักคณิตศาสตร์ และนักสถิติ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('75','2120','นักคณิตศาสตร์ประกันภัย');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('76','2120','นักสถิติ / เวชสถิติ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('77','2120','นักประชากรศาสตร์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('78','2131','นักชีววิทยา นักพฤกษศาสตร์ นักสัตวศาสตร์ และผู้ประกอบวิชาชีพที่เกี่ยวข้อง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('79','2131','นักเภสัชศาสตร์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('80','2132','ผู้ให้คำปรึกษาด้านการเกษตร การป่าไม้ และการประมง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('81','2132','นักวิชาการพืชสวน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('82','2132','นักปฐพีวิทยา');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('83','2132','นักวิทยาศาสตร์ทางดิน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('84','2132','นักวนวัฒนาวิทยา');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('85','2133','นักอนุรักษ์ทรัพยากรธรรมชาติ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('86','2133','นักนิเวศวิทยา');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('87','2133','นักวิเคราะห์คุณภาพน้ำ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('88','2133','เจ้าหน้าที่ดูแลป่าและอุทยาน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('89','2133','นักวิทยาศาสตร์สิ่งแวดล้อม');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('90','2133','ผู้ประกอบวิชีพด้านการรักษาสิ่งแวดล้อม');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('91','2141','วิศวกรอุตสาหการและการผลิต');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('92','2142','วิศวกรโยธา');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('93','2143','วิศวกรสิ่งแวดล้อม');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('94','2144','วิศวกรเครื่องกล');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('95','2144','วิศวกรอากาศยาน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('96','2144','วิศวกรต่อเรือ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('97','2145','วิศวกรเคมี');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('98','2145','วิศวกรกระบวนการกลั่น');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('99','2146','วิศวกรเหมืองแร่ นักโลหะกรรม และผู้ประกอบวิชาชีพที่เกี่ยวข้อง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('100','2149','วิศวกร (ยกเว้นวิศวกรเทคโนโลยีไฟฟ้า)ซึ่งมิได้จัดประเภทไว้ที่อื่น');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('101','2151','วิศวกรไฟฟ้า');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('102','2152','วิศวกรอิเล็กทรอนิกส์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('103','2152','วิศวกรคอมพิวเตอร์ฮาร์ดแวร์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('104','2152','วิศวกรวัดคุม');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('105','2153','วิศวกรโทรคมนาคม');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('106','2161','สถาปนิกก่อสร้าง และออกแบบภายใน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('107','2162','นักภูมิสถาปัตย์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('108','2163','นักออกแบบผลิตภัณฑ์และเครื่องแต่งกาย');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('109','2164','นักวางผังเมือง และระบบการจราจร');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('110','2165','นักทำแผนที่และนักสำรวจ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('111','2165','นักรังวัดที่ดิน /นักสำรวจที่ดิน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('112','2165','นักสำรวจทางอากาศ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('113','2166','นักออกแบบภาพกราฟฟิกและสื่อผสม');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('114','2166','นักออกแบบงานพิมพ์โฆษณา');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('115','2166','นักออกแบบเว็บไซต์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('116','2166','นักออกแบบกราฟฟิก/กราฟฟิกดีไซเนอร์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('117','2166','นักออกแบบเกมส์คอมพิวเตอร์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('118','2211','แพทย์ทั่วไป');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('119','2212','แพทย์เฉพาะทาง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('120','2221','พยาบาล');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('121','2222','ผดุงครรภ์วิชาชีพ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('122','2230','แพทย์แผนโบราณและแพทย์ทางเลือก');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('123','2240','ผู้ช่วยแพทย์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('124','2250','สัตวแพทย์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('125','2261','ทันตแพทย์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('126','2262','เภสัชกร');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('127','2263','ผู้ประกอบวิชาชีพด้านสุขภาพและสุขอนามัยที่เกี่ยวข้องกับสิ่งแวดล้อมและการประกอบวิชาชีพ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('128','2263','ผู้เชี่ยวชาญด้านการป้องกันรังสี');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('129','2264','นักกายภาพบำบัด');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('130','2265','นักโภชนาการ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('131','2266','นักแก้ไขการได้ยินและการพูด');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('132','2269','นักศิลปะบำบัด');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('133','2269','นักเคลื่อนไหวบำบัด');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('134','2269','นักกิจกรรมบำบัด/อาชีวบำบัด');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('135','2269','นักนันทนาการบำบัด');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('136','2310','ครูสอนระดับมหาวิทยาลัยและระดับอุดมศึกษา');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('137','2320','ครูสอนระดับอาชีวศึกษา');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('138','2330','ครูสอนระดับมัธยมศึกษา');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('139','2341','ครูสอนระดับประถมศึกษา');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('140','2342','ครูสอนระดับปฐมวัย');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('141','2351','ผู้เชี่ยวชาญด้านการศึกษา');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('142','2352','ครูสอนพิเศษ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('143','2353','ครูสอนภาษา');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('144','2354','ครูสอนดนตรี');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('145','2355',' ครูสอนศิลปะ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('146','2356','ผู้ฝึกอบรมด้านเทคโนโลยีสารสนเทศ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('147','2411','นักบัญชี');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('148','2412','ที่ปรึกษาด้านการเงิน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('149','2413','นักวิเคราะห์การเงิน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('150','2421','นักวิเคราะห์ด้านการบริหารและองค์การ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('151','2422','นักวิเคราะห์นโยบาย');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('152','2422','เจ้าหน้าที่ข่าวกรอง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('153','2422','ที่ปรึกษาด้านการเมือง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('154','2423','ผู้ประกอบวิชาชีพด้านงานบุคคลและอาชีพ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('155','2424','ผู้ประกอบวิชาชีพด้านการฝึกอบรมและการพัฒนาพนักงาน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('156','2431','ผู้ประกอบวิชาชีพด้านโฆษณาและการตลาด');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('157','2432','ผู้ประกอบวิชาชีพด้านการประชาสัมพันธ์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('158','2433','ผู้ประกอบวิชาชีพการขายด้านเทคนิคและด้านการแพทย์ (ยกเว้นด้านเทคโนโลยีสารสนเทศและการสื่อสาร');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('159','2434','ผู้ประกอบวิชาชีพการขายด้านเทคโนโลยีสารสนเทศและการสื่อสาร');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('160','2511','นักวิเคราะห์ระบบคอมพิวเตอร์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('161','2511','นักวิทยาศาสตร์คอมพิวเตอร์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('162','2511','นักวิเคราะห์ธุรกิจด้านเทคโนโลยีสานสนเทศและการสื่อสาร');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('163','2512','นักพัฒนาซอฟต์แวร์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('164','2513','นักพัฒนาเว็บไซต์และสื่อผสม');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('165','2514','โปรแกรมเมอร์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('166','2519','นักวิเคราะห์และพัฒนาซอฟแวร์และโปรแกรมประยุกต์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('167','2521','นักออกแบบและผู้บริหารฐานข้อมูล');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('168','2522','ผู้บริหารระบบงานคอมพิวเตอร์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('169','2523','ผู้ประกอบวิชาชีพด้านเครือข่ายคอมพิวเตอร์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('170','2523','นักวิเคราะห์ระบบสื่อสาร');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('171','2523','นักวิเคราะห์ระบบเครือข่าย');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('172','2529','ผู้เชี่ยวชาญด้านความปลอดภัยในระบบเทคโนโลยีสารสนเทศและการสื่อสาร');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('173','2611','อัยการ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('174','2611','นิติกร');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('175','2611','นักกฎหมาย/เนติบัณทิต');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('176','2611','ทนายความ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('177','2612','ผู้พิพากษา');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('178','2619','ผู้ประกอบวิชาชีพ ด้านกฎหมายซึ่งมิได้จัดประเภทๆไว้ที่อื่น');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('179','2619','จ่าศาล');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('180','2619','เจ้าหน้าที่ชันสูตรศพ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('181','2621','ผู้เก็บหรือดูแลเอกสารหรือบันทึกสำคัญและภัณฑารักษ์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('182','2622','บรรณารักษ์และผู้ประกอบวิชาชีพด้านงานสารสนเทศที่เกี่ยวข้อง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('183','2631','นักเศรษฐศาสตร์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('184','2632','นักสังคมวิทยา นักมานุษยวิทยา   และผู้ประกอบวิชาชีพที่เกี่ยวข้อง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('185','2633','นักปรัชญา นักประวัติศาสตร์ และนักรัฐศาสตร์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('186','2634','นักจิตวิทยา');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('187','2635','นักสังคมสงเคราะห์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('188','2636','ผู้ประกอบวิชาชีพด้านศาสนา');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('189','2641','นักประพันธ์และนักเขียน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('190','2642','นักหนังสือพิมพ์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('191','2642','ผู้สื่อข่าว/โทรทัศน์/วิทยุ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('192','2642','ผู้สื่อข่าวหนังสือพิมพ์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('193','2643','นักแปล ล่ามและนักภาษาศาสตร์อื่นๆ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('194','2651','ประติมากร');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('195','2651','นักวาดภาพ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('196','2651','ช่างศิลป์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('197','2651','นักทัศนศิลป์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('198','2651','นักปั้นเซรามิก');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('199','2652','นักร้อง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('200','2652','นักดนตรี');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('201','2652','นักประพันธ์เพลง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('202','2652','วาทยากร');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('203','2653','นักเต้นรำและนักออกแบบท่าเต้น');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('204','2654','นักบัลเลย์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('205','2654','ผู้กำกับและผู้ผลิตภาพยนตร์ ละครและงานสาขาอื่นๆที่เกี่ยวข้อง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('206','2654','เจ้าหน้าที่ตัดต่อภาพยนตร์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('207','2655','นักแสดง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('208','2656','ผู้ประกาศทางวิทยุ โทรทัศน์  และผู้ประกาศด้านอื่นๆ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('209','2656','นักจัดรายการทอล์คโชว์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('210','2659','นักกายกรรม');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('211','2659','นักแสดงตลก');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('212','2659','นักมายากล');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('213','2659','นักพากย์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('214','2659','นักเชิดหุ่นกระบอก');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('215','3111','ช่างเทคนิคด้านเคมีและวิทยาศาสตร์กายภาพ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('216','3111','ช่างเทคนิคด้านธรณีวิทยา');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('217','3111','ช่างเทคนิคด้านอุตุนิยมวิทยา');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('218','3112','ช่างเทคนิคด้านวิศวกรรมโยธา');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('219','3112','เจ้าหน้าที่ตรวจสอบอาคาร');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('220','3112','ผู้ควบคุมดูแลวัสดุก่อสร้างและคนงาน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('221','3112','ช่างเทคนิคด้านการสำรวจ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('222','3112','เจ้าหน้าที่ตรวจสอบสถานที่เกิดเพลิงไหม้');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('223','3112','เจ้าหน้าที่ตรวจสอบสถานที่เกิดเพลิงไหม้');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('224','3112','ผู้เชี่ยวชาญด้านการป้องกันไฟไหม้');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('225','3113','ช่างเทคนิคด้านวิศวกรรมไฟฟ้า');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('226','3114','ช่างเทคนิคด้านวิศวกรรมอิเล็กทรอนิกส์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('227','3115','ช่างเทคนิคด้านวิศวกรรมเครื่องกล');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('228','3115','เจ้าหน้าที่สำรวจทางทะเล');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('229','3116','ช่างเทคนิคด้านวิศวกรรมเคมี');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('230','3117','ช่างเทคนิคด้านเหมืองแร่และโลหะวิทยา');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('231','3117','ช่างสำรวจเหมืองแร่');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('232','3118','ช่างเขียนแบบ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('233','3119','เจ้าหน้าที่เทคนิคด้านนิติวิทยาศาสตร์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('234','3119','เจ้าหน้าที่ตรวจสอบสาเหตุการเกิดเพลิงไหม้');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('235','3119','ช่างเทคนิคด้านวิศวกรรมการผลิต');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('236','3119','ช่างเทคนิคด้านหุ้นยนต์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('237','3123','หัวหน้าคุมงานก่อสร้าง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('238','3121','หัวหน้าคุมงานด้านเหมืองแร่');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('239','3122','หัวหน้าคุมงานด้านการผลิต');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('240','3131','ช่างเทคนิคควบคุมเครื่องจักรโรงงานผลิตพลังงานไฟฟ้า');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('241','3132','ช่างเทคนิคควบคุมเครื่องจักรไอน้ำและหม้อน้ำ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('242','3132','ช่างเทคนิคควบคุมสถานีสูบน้ำ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('243','3132','ช่างเทคนิคควบคุมเครื่องจักรโรงงานกำจัดขยะ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('244','3132','ช่างเทคนิคควบคุมเครื่องจักรบำบัดน้ำเสีย');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('245','3133','ช่างเทคนิคควบคุมเครื่องจักรโรงงานแปรรูปทางเคมี');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('246','3134','ช่างเทคนิคควบคุมเครื่องจักรโรงงานกลั่นปิโตรเลียมและก๊าซธรรมชาติ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('247','3135','ช่างเทคนิคควบคุมกระบวนการผลิตโลหะ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('248','3139','ช่างเทคนิคควบคุมกระบวนการอื่นๆ ซึ่งมิได้จัดประเภทไว้ในที่อื่น');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('249','3139','ช่างเทคนิคควบคุมแผงควบคุมการผลิตเยื่อกระดาษและกระดาษ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('250','3141','เจ้าหน้าที่เทคนิคด้านวิทยาศาสตร์สิ่งมีชีวิต(ยกเว้นแพทย์)');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('251','3142','เจ้าหน้าที่เทคนิคด้านการเกตษรและการประมง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('252','3143','เจ้าหน้าที่เทคนิคด้านการป่าไม้');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('253','3151','ช่างเทคนิคหรือช่างเครื่องประจำเรือ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('254','3151','วิศวกรประจำเรือ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('255','3152','กัปตันเรือ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('256','3152','เจ้าหน้าที่ประจำห้องบังคับการเรือและผู้นำร่อง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('257','3152','ไต๋กงเรือ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('258','3153','นักบินและผู้ประกอบวิชาชีพอื่นๆที่เกี่ยวข้อง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('259','3154','ผู้ควบคุมการจราจรทางอากาศ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('260','3155','วิศวกรความปลอดภัยด้านการจราจรทางอากาศ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('261','3155','ช่างเทคนิคความปลอดภัยด้านการจราจรทางอากาศ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('262','3211','เจ้าหน้าที่เทคนิคด้านการสร้างภาพทางการแพทย์และอุปกรณ์การบำบักรักษาโรค');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('263','3211','เจ้าหน้าที่ถ่ายภาพรังสีเพื่อการวินิจฉัยทางการแพทย์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('264','3211','เจ้าหน้าที่รังสีเทคนิคเพื่อการตรวจเต้านมด้วยเครื่องแมมโมแกรม');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('265','3211','เจ้าหน้าที่บำบักด้วยรังสี');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('266','3211','เจ้าหน้าที่เอกเรย์ด้วยคลื่นแม่เหล็กไฟฟ้า (MRI)');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('267','3211','นักเทคโนโลยีการแพทย์นิวเคลียร์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('268','3211','เจ้าหน้าที่อัลตราซาวนด์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('269','3212','เจ้าหน้าที่ เทคนิคในห้องปฏิบัติการทางการแพทย์และพยาธิวิทยา');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('270','3212','เจ้าหน้าที่เทคนิคในธนาคารเลือด');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('271','3212','เจ้าหน้าที่เทคนิคด้านเชลล์วิทยา');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('272','3213','เจ้าหน้าที่เทคนิคด้านเภสัชกรรมและผู้ช่วย');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('273','3214','เจ้าหน้าที่เทคนิคด้านอุปกรณ์การแพทย์เทียมและฟันเทียม');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('274','3221','ผู้ประกอบวิชาชีพที่เกี่ยวข้องกับการพยาบาล');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('275','3221','ผู้ช่วยพยาบาล');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('276','3222','ผู้ประกอบวิชาชีพเกี่ยวข้องกับการผดุงครรภ์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('277','3222','หมอตำแย');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('278','3230','ผู้ประกอบวิชาชีพที่เกี่ยวข้องกับแพทย์แผนโบราณและแพทย์ทางเลือก');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('279','3230','นักสมุนไพร');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('280','3230','หมอผี/หมอไสยศาสตร์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('281','3240','ผู้ช่วยและผู้ประกอบวิชาชีพที่เกี่ยวข้องกับสัตวแพทย์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('282','3251','ผู้ช่วยทันตแพทย์และนักทันตกรรมบำบัด');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('283','3252','เจ้าหน้าที่เวชระเบียนและเจ้าหน้าที่เทคนิคด้านข้อมูลสุขภาพ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('284','3253','เจ้าหน้าที่ด้านสุขภาพของชุมชน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('285','3254','ผู้ตรวจวัดสายตาและช่างประกอบแว่นตา');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('286','3255','เจ้าหน้าที่เทคนิคและผู้ช่วยด้านกายภาพบำบัด');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('287','3255','นักบำบัดโรคด้วยนวดแบบกดจุด');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('288','3256','ผู้ช่วยด้านการแพทย์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('289','3257','ผู้ตรวจสอบด้านสิ่งแวดล้อม การทำงาน สุขภาพ และงานที่เกี่ยวข้อง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('290','3257','เจ้าหน้าที่อนามัย');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('291','3258','เจ้าหน้าที่เทคนิคทางการแพทย์ฉุกเฉิน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('292','3258','เจ้าหน้าที่ประจำรถพยาบาล');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('293','3258','เจ้าหน้าที่ผู้ช่วยแพทย์ฉุกเฉิน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('294','3259','เจ้าหน้าที่เทคนิคด้านการวางยาชาและยาสลบ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('295','3259','นักบำบัดโรคด้วยการจัดกระดูกสันหลัง (ไคโรแพรคติค)');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('296','3259','ที่ปรึกษาด้านการวางแผนครอบครัว');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('297','3259','นักบำบัดโรคด้วยการจัดกระดูกสันหลัง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('298','3259','นักบำบัดโรคด้วยการจัดกระดูก');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('299','3311','นายหน้าที่หลักทรัพย์และการเงิน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('300','3311','ตัวแทนจำหน่ายเงินตราต่างประเทศ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('301','3311','นายหน้าซื้อขายหุ้น');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('302','3312','เจ้าหน้าที่สินเชื่อและให้กู้ยืม');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('303','3313','ผู้ประกอบวิชาชีพที่เกี่ยวข้องทางด้านบัญชี');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('304','3314','ผู้ประกอบวิชาชีพที่เกี่ยวข้องกับด้านสถิติ คณิตศาสตร์ และคณิตศาสตร์ประกันภัย');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('305','3315','ผู้ประเมินราคาและมูลค่าความเสียหาย');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('306','3315','ผู้ประเมินราคาอสังหาริมทรัพย์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('307','3315','ผู้ประเมินราคาค่าสินไหมทดแทน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('308','3315','ผู้ประเมินราคาด้านการประกันภัย');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('309','3321','ตัวแทนขายประกัน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('310','3321','เจ้าหน้าที่รับประกันภัย');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('311','3322','ตัวแทนขายด้านการค้า');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('312','3323','ผู้จัดซื้อ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('313','3324','นายหน้าทางการค้า');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('314','3331','ตัวแทนพิธีการศุลกากรและตัวแทนผู้รับจัดการขนส่งสินค้า');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('315','3332','นักวางแผนการจัดงานและการประชุม');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('316','3332','เจ้าหน้าที่จัดการประชุมและการแสดง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('317','3333','ตัวแทนจัดหางานและผู้ทำสัญญาด้านแรงงานทั้งภาครัฐและเอกชน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('318','3334','ตัวแทนอสังหาริมทรัพย์และผู้จัดการทรัพย์สิน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('319','3334','นายหน้าซื้อขายสังหาริมทรัพย์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('320','3334','พนังงานขายอสังหาริมทรัพย์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('321','3339','ตัวแทนการให้บริการทางธุรกิจ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('322','3339','พนักงานขายงานโฆษณา');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('323','3339','ตัวแทนธุรกิจด้านงานแสดงดนตรี/งานละคร');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('324','3339','ตัวแทนธุรกิจด้านงานประพันธ์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('325','3341','หัวหน้าคุมงานในสำนักงาน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('326','3342','เลขานุการด้านกฎหมาย');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('327','3343','เลขานุการด้านการบริหารและเลขานุการผู้บริหาร');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('328','3343','เจ้าหน้าที่บันทึกคำให้การในศาล');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('329','3344','เลขานุการด้านการแพทย์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('330','3351','เจ้าหน้าที่ประจำด่านศุลกากรและบริเวณพรมแดน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('331','3351','เจ้าหน้าที่ตรวจหนังสือเดินทาง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('332','3351','เจ้าหน้าที่ตรวจคนเข้าเมือง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('333','3351','เจ้าหน้าที่ศุลกากร');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('334','3352','เจ้าหน้าที่สรรพากรและสรรพสามิตของรัฐ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('335','3352','เจ้าหน้าที่ตรวจสอบภาษี');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('336','3353','เจ้าหน้าที่ดูแลสิทธิประโยชน์ทางสังคมของรัฐ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('337','3354','เจ้าหน้าที่ด้านการออกใบอนุญาตของรัฐ ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('338','3355','ตำรวจสืบสวนและนักสืบ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('339','3355','สารวัตร');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('340','3355','พนักงานสืบสวนสอบสวน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('341','3359','ผู้ประกอบวิชาชีพที่เกี่ยวกับด้านกฎระเบียนของรัฐ ซึ่งมิได้จัดประเภทไว้ในที่อื่นๆ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('342','3411','นักสืบเอกชน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('343','3411','ผู้ประกอบวิชาชีพที่เกี่ยวข้องกับกฎหมายและงานที่เกี่ยวข้อง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('344','3411','เจ้าหน้าที่ตรวจสอบหลักฐานทะเบียนที่ดิน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('345','3412','ผู้ประกอบวิชาชีพที่เกี่ยวกับงานสังคมสังเคราะห์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('346','3413','ผู้ประกอบวิชาชีพที่เกี่ยวข้องกับศาสนา');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('347','3421','นักกรีฑาและนักกีฬา');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('348','3422','เจ้าหน้าที่และผู้ฝึกสอนกีฬา');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('349','3423','ผู้ฝึกสอนและผู้ดูแลโปรแกรมการสร้างสมรรถภาพของร่างการและการนันทนาการ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('350','3431','ช่างถ่ายภาพ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('351','3432','นักออกแบบตกแต่งภายในและมัณฑนากร');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('352','3433','ข่างเทคนิคดูแลห้องแสดงภาพ ห้องสมุดและพิพิธภัณฑ์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('353','3434','หัวหน้าพ่อครัว/แม่ครัว');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('354','3435','ผู้ประกอบวิชาชีพที่เกี่ยวข้องด้านศิลปะ วัฒนธรรม ซึ่งมิได้จัดประเภทไว้ในที่อื่น');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('355','3435','นักแสดงแทน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('356','3435','ช่างแต่งตัวนักแสดง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('357','3435','ช่างสัก');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('358','3511','ช่างเทคนิคปฏิบัติการด้านเทคโนโลยีสารสนเทศและการสื่อสาร');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('359','3512','ช่างเทคนิคให้ความช่วยเหลือและแก้ปัญหาด้านเทคโนโลยีสารสนเทศและการสื่อสารกับผู้ใช้งาน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('360','3513','ช่างเทคนิคด้านเครือข่ายและระบบคอมพิวเตอร์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('361','3514','ช่างเทคนิคด้านเว็บไซต์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('362','3514','ผู้บริหารเว็บไซต์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('363','3514','ผู้ดูแลเว็บไซต์/เว็บมาสเตอร์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('364','3514','ผู้เชี่ยวชาญด้านเว็บไซต์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('365','3521','ช่างเทคนิคด้านการแพร่ภาพกระจายเสียงและโสตทัศนูปกรณ์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('366','3521','ช่างกล้องถ่ายภาพยนตร์/ถ่ายวิดีโอ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('367','3522','ช่างเทคนิควิศวกรโทรคมนาคม');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('368','4110','เสมียน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('369','4120','เลขานุการทั่วไป');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('370','4131','พนักงานพิมพ์ดีด');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('371','4131','เจ้าหน้าที่จดชวเลข');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('372','4132','เจ้าหน้าที่บันทึกข้อมูล');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('373','4214','เจ้าพนักงานเร่งรัดหนี้สิน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('374','4221','ผู้จัดนำเที่ยว');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('375','4222','พนักงานศูนย์บริการข้อมูลข่าวสาร');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('376','4223','พนักงานรับโทรศัพท์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('377','4223','พนักงานควบคุมแผงเครื่องติดต่อทางโทรศัพท์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('378','4224','พนักงานตอนรับของโรงแรม');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('379','4225','พนักงานเคาน์เตอร์ติดต่อสอบถาม');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('380','4225','พนักงานประชาสัมพันธ์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('381','4226','พนักงานต้อนรับทั่วไป');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('382','4227','พนักงานสัมภาษณ์ในโครงการสำรวจและวิจัยตลาด');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('383','4229','เจ้าหน้าที่คัดกรองผู้มีสิทธิ์เข้ารับบริการ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('384','4412','บุรุษไปรษณีย์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('385','5111','พนักงานต้อนรับและบริการในการเดินทาง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('386','5111','แอร์โฮสเตส');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('387','5112','พนักงานดูแลและเก็บค่าโดยสารยานพาหนะ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('388','5113','มัคคุเทศก์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('389','5120','พ่อครัว/แม่ครัว');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('390','5131','พนักงานเสิร์ฟอาหารและเครื่องดื่ม');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('391','5132','พนักงานผสมเครื่องดื่ม(บาร์เทนเดอร์)');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('392','5141','ช่างแต่งผม');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('393','5141','ช่างตัดผม');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('394','5141','ผู้เชี่ยวชาญด้านการดูแลเส้นผม');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('395','5142','ช่างเสริมสวยและผู้ปฏิบัติงานที่เกี่ยวข้อง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('396','5142','ที่ปรึกษาด้านการลดน้ำหนักและกระชับสัดส่วน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('397','5151','หัวหน้าคุมงานด้านการทำความสะอาดและงานแม่บ้านในสำนักงาน โรงแรมและสถานประกอบการอื่นๆ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('398','5152','พนักงานดูแลงานบ้าน/แม่บ้าน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('399','5153','พนักงานดูแลความเรียบร้อยของอาคารสถานที่');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('400','5153','นักการ/ภารโรง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('401','5161','โหร  ผู้ทำนายโชคชะตา  และผู้ประกอบอาชีพที่เกี่ยวข้อง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('402','5161','หมอดู');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('403','5161','นักโหราเลขศาสตร์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('404','5162','คนรับใช้');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('405','5163','สัปเหร่อ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('406','5163','เจ้าหน้าที่ฉีดยาศพ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('407','5164','ช่างตัดแต่งขนสัตว์เลี้ยงและพนักงานดูแลสัตว์เลี้ยง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('408','5164','ผู้ดูแลสวนสัตว์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('409','5165','ครูสอนขับรถ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('410','5169','พนักงานบริการส่วนบุคคล');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('411','5169','พนักงานต้อนรับชาย/หญิงในคลับ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('412','5211','ผู้จำหน่ายสินค้าตามแผงลอยและตลาด');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('413','5212','ผู้จำหน่วยอาหารตามถนน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('414','5212','พ่อค้า/แม่ค้าเร่ขายอาหาร');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('415','5221','เจ้าของร้านค้า');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('416','5221','ตัวแทนจำหน่ายหนังสือพิมพ์หรือนิตยสารต่างๆ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('417','5241','นายแบบและนางแบบแฟชั่น  และผู้แสดงแบบอื่นๆ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('418','5222','หัวหน้าคุมงานในร้านค้า');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('419','5222','หัวหน้าคุมงานในซุปเปอร์มาร์เก็ต');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('420','5223','พนังงานช่วยขายในร้านค้า');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('421','5223','พนังงานในร้านค้าปลีก/ค้าส่ง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('422','5230','พนักงานแคชเชียร์และเสมียนขายตัว');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('423','5230','พนักงานแคชเชียร์ประจำสถานีบริการน้ำมันเชื้อเพลิง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('424','5241','นายแบบและนางแบบแฟชั่นและผู้แสดงแบบอื่นๆ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('425','5241','นายแบบและนางแบบงานโฆษณา');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('426','5241','นายแบบและนางแบบงานศิลปะ ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('427','5242','พนักงานสาธิตและขายสินค้า');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('428','5243','ผู้จำหน่วยสินค้าตามบ้าน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('429','5244','พนักงานขายในศูนย์บริการลูกค้า');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('430','5244','ผู้จำหน่ายสินค้าในศูนย์บริการทางโทรศัพท์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('431','5244','ผู้จำหน่ายสินค้าทางอินเตอร์เน็ต');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('432','5245','พนักงานบริการในสถานีบริการน้ำมันเชื้อเพลิง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('433','5245','พนักงานประจำท่าจอดเรือ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('434','5246','พนักงานเคาน์เตอร์บริการอาหาร');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('435','5249','ผู้จำหน่ายสินค้าอื่นๆซึ่งมิได้จัดประเภทไว้ในที่อื่นๆ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('436','5311','ผู้ปฏิบัติงานดูแลเด็ก');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('437','5311','พี่เลี้ยงเด็ก');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('438','5312','ครูผู้ช่วยเด็กปฐมวัย');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('439','5321','ผู้ช่วยงานดูแลสุขภาพ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('440','5321','เจ้าหน้าที่ช่วยงานพยาบาลตามคลินิกหรือโรงพยาบาล');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('441','5321','เจ้าหน้าที่ช่วยงานผดุงครรภ์ตามคลินิกหรือโรงพยาบาล');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('442','5321','เจ้าหน้าที่ช่วยดูแลผู้ป่วย');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('443','5322','ผู้ดูแลส่วนบุคคลตามบ้าน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('444','5322','ผู้ดูแลส่วนบุคคลด้านสุขภาพซึ่งมิได้จัดประเภทไว้ในที่อื่น');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('445','5329','เจ้าหน้าที่เจาะเลือด');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('446','5411','พนักงานดับเพลิง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('447','5411','พนังงานดับไฟป่า');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('448','5412','เจ้าหน้าที่ตำรวจ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('449','5413','ผู้คุมนักโทษ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('450','5413','พัศดี');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('451','5414','พนักงานรักษาความปลอดภัย');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('452','5414','ยามรักษาการณ์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('453','5419','ผู้ให้บริการด้านการป้องกันภัย ซึ่งมิได้จัดประเภทไว้ในที่อื่นๆ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('454','5419','เจ้าหน้าที่ดูแลสัตว์ป่า');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('455','5419','ผู้ดูแลสวนสัตว์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('456','6111','ผู้ปลูกพืชไร่และพืชผัก');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('457','6111','ทำไร่');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('458','6111','ชาวนาปลูกข้าว');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('459','6112','ผู้ปลูกไม้ยืนต้นและไม้ผล');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('460','6112','คนงานกรีดยาง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('461','6112','ผู้ปลูกยางพารา');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('462','6113','ผู้ปลูกพืชสวน ไม้ดอกไม้ประดับ และพืชในเรือนเพาะชำ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('463','6113','ผู้ปลูกไม้ดอกไม้ประดับ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('464','6113','นักออกแบบตกแต่งสวน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('465','6113','เพาะต้นไม้ขาย');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('466','6113','ผู้เพาะเห็ด');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('467','9211','คนงานเก็บผลไม้');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('468','6114','ผู้ปลูกพืชแบบผสมผสาน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('469','6121','ผู้เลี้ยงปศุสัตว์และผลิตภัณฑ์นม');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('470','6121','ทำฟาร์มนม');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('471','6122','ผู้เลี้ยงสัตว์ปีก');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('472','6122','ผู้ผสมพันธุ์สัตว์ปีก');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('473','6122','ผู้ทำฟาร์มสัตว์ปีก');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('474','6123','ผู้เลี้ยงผึ้งและไหม');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('475','6129','ผู้เลี้ยงสัตว์เพื่อการค้าขาย ซึ่งมิได้จัดประเภทไว้ในที่อื่น');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('476','6129','ผู้เลี้ยงนกกระจอกเทศ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('477','6130','ผู้ปลูกพืชร่วมกับการเลี้ยงสัตว์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('478','6210','ผู้ปฏิบัติงานด้านการป่าไม้และงานอื่นที่เกี่ยวข้อง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('479','6210','คนเผาถ่าน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('480','6210','ผู้สำรวจทรัพยากรป่าไม้');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('481','6210','คนงานผีมือด้านการป่าไม้');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('482','6221','ผู้ปฏิบัติงานด้านการเพาะเลี้ยงสัตว์น้ำ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('483','6221','ผู้ทำฟาร์มสัตว์น้ำ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('484','6221','ผู้เลี้ยงหอยนางรม');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('485','6222','ผู้ปฏิบัติงานด้านการประมงน้ำจืดและประมงชายฝั่งทะเล');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('486','6222','กัปตันเรือประมงชายฝั่ง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('487','6222','ชาวประมงพื้นบ้าน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('488','6223','ชาวประมงทะเลน้ำลึก');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('489','6223','กัปตันเรือประมงทะเลน้ำลึก');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('490','6223','กัปตันเรือลากอวน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('491','6224','ผู้ปฏิบัติงานด้านล่าสัตว์และวางกับดักสัตว์ต่างๆ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('492','6310','ผู้ปฏิบัติงานด้านการปลูกพืชเพื่อการดำรงชีพ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('493','6320','ผู้ปฏิบัติงานด้านการเลี้ยงสัตว์เพื่อการดำรงชีพ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('494','6330','ผู้ปฏิบัติงานด้านการปลูกพืชร่วมกับการเลี้ยงสัตว์เพื่อการดำรงชีพ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('495','6340','ผู้ปฏิบัติงานด้านการประมง ล่าสัตว์และเก็บพืชผลเพื่อการดำรงชีพ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('496','6340','คนหาของป่า');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('497','6340','คนงมหาของใต้น้ำเพื่อการดำรงชีพ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('498','7111','ช่างก่อสร้างที่อยู่อาศัย');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('499','7112','ช่างก่ออิฐและผู้ปฏิบัติงานที่เกี่ยวข้อง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('500','7113','ช่างหิน ตัดหิน เจาะสกัดหิน และแกะหิน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('501','7113','ช่างขัดเงาหิน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('502','7122','ช่างปูกระเบื้องและผนัง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('503','7114','ช่างหล่อเทคอนกรีต ตกแต่งผิวซีเมนต์ และผู้ปฏิบัติงานที่เกี่ยวข้อง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('504','7114','ช่างทำพื้นหินขัด');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('505','7115','ช่างไม้');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('506','7115','ช่างต่อเรือไม้');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('507','7119','ช่างก่อสร้างโครงสร้างอาคาร สิ่งปลูกสร้าง และผู้ปฏิบัติงานที่เกี่ยวข้อง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('508','7119','ช่างรื้อถอนอาคาร');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('509','7121','ช่างมุ้งหลังคา');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('510','7121','ช่างซ่อมหลังคา');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('511','7121','ช่างติดตั้งหลังคาโลหะ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('512','7122','ช่างปูพื้นและกระเบื้อง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('513','7122','ช่างปูพรม');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('514','7123','ช่างฉาบปูนและตกแต่งงานปูน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('515','7124','ช่างติดตั้งฉนวน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('516','7125','ช่างติดกระจก');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('517','7126','ช่างประปาและวางท่อ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('518','7126','ช่างติดตั้งท่อระบายน้ำ รางน้ำ และท่อโลหะ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('519','7126','ช่างซ่อมท่อ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('520','7127','ช่างเครื่องระบบปรับอากาศและระบบความเย็น');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('521','7131','ช่างทาสีและปฏิบัติงานที่เกี่ยวข้อง ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('522','7131','ช่างปิดวอลเปเปอร์หรือผ้าลงบนพนัก/เพดาน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('523','7132','ช่างพ่นสีขัดเงา');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('524','7133','ช่างทำความสะอาดโครงสร้างอาคาร สิ่งปลูกสร้าง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('525','7211','ช่างทำแบบหล่อโลหะและแกนแบบหล่อโลหะ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('526','7212','ช่างเชื่อมและช่างตัดโลหะด้วยเปลวไฟ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('527','7212','ช่างทำเครื่องทองเหลือง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('528','7213','ช่างโลหะแผ่น');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('529','7213','ช่างทำผลิตภัณฑ์โลหะแผ่นสำหรับให้ช่างมุงหลังคาใช้งานและติดตั้ง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('530','7214','ช่างเตรียมและติดตั้งโครงสร้างโลหะ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('531','7215','ช่างติดตั้งเครื่องยกหรือสายเคเบิล');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('532','7221','ช่างเหล็ก ช่างตีเหล็ก และช่างคุมเครื่องอัดขึ้นรูปโลหะ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('533','7222','ช่างทำกุญแจ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('534','7222','ช่างปืน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('535','7223','ช่างปรับตั้งและใช้เครื่องกลงานโลหะ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('536','7223','ช่างกลึงโลหะ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('537','7223','ช่างคุมเครื่องหล่อโลหะ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('538','7223','ช่างคุมเครื่องกลึงโหละ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('539','7223','ช่างคุมเครื่องคว้านโลหะ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('540','7224','ช่างขัดเงาโลหะ ช่างเจียระไนโลหะ และช่างลับเครื่องมือ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('541','7224','ช่างลับมีด');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('542','7231','ช่างเครื่องและช่างซ่อมเครื่องยนต์ยานยนต์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('543','7232','ช่างเครื่องและช่างปรับเครื่องยนต์อากาศยาน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('544','7232','วิศวกรบำรุงรักษาโครงสร้างอากาศยาน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('545','7232','วิศวกรบำรุงรักษาเครื่องยนต์อากาศยาน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('546','7232','ช่างซ่อมเฮลิคอปเตอร์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('547','7233','ช่างเครื่องและช่างปรับเครื่องจักรกลทางการเกษตรหรืออุตสาหกรรม');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('548','7234','ช่างซ่อมรถจักรยานและผู้ปฏิบัติงานที่เกี่ยวข้อง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('549','7234','ช่างซ่อมรถเข็นเด็กทารก');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('550','7234','ช่างซ่อมรถเข็นสำหรับคนป่วยหรือคนพิการ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('551','7311','ช่างทำและซ่อมเครื่องมือที่มีความเที่ยงตรงแม่นยำ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('552','7311','ช่างซ่อมและทำนาฬิกา');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('553','7311','ช่างทำและซ่อมเครื่องมือที่ใช้ในการพยากรณ์อากาศ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('554','7311','ช่างซ่อมอุปกรณ์ถ่ายภาพ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('555','7312','ช่างทำเครื่องดนตรีและปรับเสียงดนตรี');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('556','7312','ช่างปรับเสียงเปียโน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('557','7312','ช่างทำเสียงดนตรีประเภทสาย');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('558','7313','ช่างทำเครื่องเพชรพลอยและรูปพรรณ และโลหะมีค่า');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('559','7313','ช่างทอง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('560','7313','ช่างฝังอัญมณี');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('561','7313','ช่างเงิน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('562','7313','ช่างลงยาเครื่องเพชรพลอยและรูปพรรณ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('563','7314','ช่างทำเครื่องปั้นดินเผาและผู้ปฏิบัติงานที่เกี่ยวข้อง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('564','7315','ช่างทำเครื่องแก้ว ช่างตัด ช่างเจียระไน และช่างตกแต่งเครื่องแก้ว');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('565','7316','ช่างเขียนเครื่องหมาย ช่างลงสี ช่างแกะสลัก และช่างกับลายแก้ว');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('566','7316','ช่างเขียนป้ายหรือเครื่องหมาย');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('567','7316','ช่างเคลือบแก้ว');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('568','7317','ช่างงานหัตกรรมไม้ เครื่องจักรสาน และวัสดุที่เกี่ยวข้อง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('569','7317','ช่างทำเฟอร์นิเจอร์สาน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('570','7317','ช่างทำตะกร้า');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('571','7318','ช่างงานหัตกรรมสิ่งทอ เครื่องหนัง และวัสดุที่เกี่ยวข้อง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('572','7318','ช่างทอเสื้อผ้า');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('573','7319','ช่างงานหัตกรรม  ซึ่งมิได้จัดประเภทไว้ในที่อื่น');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('574','7319','ช่างแกะสลักหิน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('575','7321','ช่างเทคนิคก่อนงานพิมพ์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('576','7321','ช่างประกอบฟิล์ม');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('577','7321','ช่างจัดพิมพ์ด้วยคอมพิวเตอร์(Desktop publishing)');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('578','7321','ช่างเทคนิคงานก่อนงานพิมพ์อิเล็กทรอนิกส์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('579','7321','ช่างทำเพลตงานพิมพ์สกรีน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('580','7321','ช่างทำแม่พิมพ์งานพิมพ์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('581','7321','ช่างเรียงพิมพ์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('582','7321','ช่างสร้างลายด้วยแสง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('583','7322','ช่างพิมพ์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('584','7323','ช่างตกแต่งงานพิมพ์และเข้าเล่ม');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('585','7323','พนักงานเครื่องตัดกระดาษ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('586','7411','ช่างเดินสายไฟภายในอาคารและอุปกรณ์ไฟฟ้าที่เกี่ยวข้อง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('587','7411','ช่างไฟฟ้า');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('588','7411','ช่างซ่อมไฟฟ้าประจำอาคาร');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('589','7412','ช่างเครื่องและช่างปรับอุปกรณ์ไฟฟ้า');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('590','7412','ช่างไฟฟ้ายานยนต์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('591','74712','ช่างพันไดนาโม');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('592','7412','ช่างซ่อมลิฟต์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('593','7413','ช่างติดตั้งและซ่อมสายส่งกระแสไฟฟ้า');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('594','7421','ช่างเครื่องและผู้ให้บริการด้านอุปกรณ์อิเล็กทรอนิกส์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('595','7421','วิศวกรบำรุงรักษาอุปกรณ์ไฟฟ้าและอิเล็กทรอนิกส์อากาศยาน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('596','7421','ช่างเทคนิคเครื่องถ่ายเอกสาร');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('597','7422','ช่างติดตั้งและผู้ให้บริการด้านอุปกรณ์เทคโนโลยีสารสนเทศและการสื่อสาร');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('598','7422','ช่างติดตั้งโทรศัพท์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('599','7511','ผู้ฆ่าชำแหละเนื้อสัตว์และสัตว์น้ำ และผู้จัดเตรียมอาหารที่เกี่ยวข้อง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('600','7511','คนขายปลา');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('601','7512','ผู้ทำขนมปัง ขนมเค้ก ขนมหวาน และลูกกวาด');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('602','7513','ผู้ผลิตผลิตภัณฑ์นม');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('603','7514','ผู้ถนอมผลไม้ พืชผัก และผู้ถนอมอาหารที่เกี่ยวข้อง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('604','7515','ผู้ชิมรสและจัดระดับชั้นคุณภาพของอาหารและเครื่องดื่ม');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('605','7516','ผู้เตรียมใบยาสูบและผลิตผลิตภัณฑ์ยาสูบ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('606','7521','ช่างอัดน้ำยาและอบไม้');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('607','7521','ช่างควบคุมเตาอบไม้');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('608','7522','ช่างทำเฟอร์นิเจอร์ไม้และผู้ปฏิบัติงานที่เกี่ยวข้อง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('609','7522','ช่างทำตู้');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('610','7522','ช่างทำเกวียน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('611','7523','ช่างปรับตั้งและใช้เครื่องจักรงานไม้');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('612','7523','ช่างเครื่องแกะสลักไม้');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('613','7523','ช่างเครื่องผลิตเฟอร์นิเจอร์ไม้');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('614','7531','ช่างตัดเย็บเสื้อผ้า ช่างทำเสื้อผ้าขนสัตว์ และช่างทำหมวก');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('615','7531','ผู้คัดคุณภาพหนังเฟอร์นิเจอร์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('616','7532','ช่างสร้างแบบและตัดวัสดุเพื่อผลิตเครื่องแต่งกายและสิ่งของที่เกี่ยวข้อง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('617','7532','ช่างสร้างแบบเครื่องแต่งกาย  ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('618','7532','ช่างตัดเครื่องแต่งกาย');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('619','7533','ช่างเย็บ ช่างปัก และผู้ปฏิบัติงานที่เกี่ยวข้อง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('620','7533','ช่างทำร่ม');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('621','7534','ช่างหุ้มเบาะและผู้ปฏิบัติงานที่เกี่ยวข้อง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('622','7534','ช่างหุ้มอุปกรณ์ทางศัลยศาสตร์กระดูก');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('623','7534','ช่างทำฟูกที่นอน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('624','7535','ช่างตกแต่งหนังสัตว์ ช่างฟอกหนัง และช่างถากหนังขูดขนสัตว์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('625','7535','ช่างย้อมหนังสัตว์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('626','7536','ช่างทำรองเท้าและผู้ปฏิบัติงานที่เกี่ยวข้อง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('627','7536','ช่างซ่อมรองเท้า');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('628','7536','ช่างทำอานม้า');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('629','7541','ผู้ปฏิบัติงานใต้น้ำ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('630','7541','นักประดาน้ำกู้ภัย');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('631','7541','คนเก็บหอยนางรม');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('632','7542','ผู้ปฏิบัติด้านวัตถุระเบิดและผู้ปฏิบัติงานจุดระเบิด');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('633','7542','เจ้าหน้าที่จุดระเบิดในเหมืองแร่และเหมืองหิน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('634','7543','ผู้คัดคุณภาพและทดสอบผลิตภัณฑ์(ยกเว้นอาหารและเครื่องดื่ม)');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('635','7544','ผู้รมยาและกำจัดแมลงและสัตว์ศัตรูพืช');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('636','7544','ผู้รมยากำจัดปลวก/แมลงสาป');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('637','7549','คนจัดดอกไม้');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('638','7549','ช่างตัดแต่งเลนส์ชนิดใช้ในทางทัศนศาสตร์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('639','8111','ผู้ปฏิบัติงานในเหมืองแร่และเหมืองหิน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('640','8111','ผู้ควบคุมเครื่องจักรแบบต่อเนื่องในเหมืองแร่');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('641','8111','ผู้ควบคุมเครื่องจักรขุดเจาะในการทำเหมืองแร่');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('642','8111','ผู้ควบคุมเครื่องจักรขนาดใหญ่ในการทำเหมืองแร่');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('643','8111','ผู้ควบคุมเครื่องยกขนาดใหญ่ในเหมืองแร่');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('644','8111','ผู้ควบคุมเครื่องจักรโรงงานในเหมืองแร่');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('645','8111','พนักงานขุดเจาะด้วยเครื่องจักรในเหมืองแร่');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('646','8112','ผู้ควบคุมเครื่องจักรโรงงานแปรรูปแร่และหิน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('647','8112','คนร่อนทอง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('648','8113','ผู้ควบคุมเครื่องจักรขุดเจาะบ่อและปฏิบัติงานที่เกี่ยวข้อง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('649','8113','ผู้ควบคุมเครื่องจักรเจาะคว้าน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('650','8113','ผู้ควบคุมเครื่องเจาะบ่อน้ำมันหรือก๊าซ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('651','8113','ผู้ควบคุมเครื่องจักรขุดเจาะขนาดใหญ่');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('652','8114','ผู้ควบคุมเครื่องจักรผลิตผลิตภัณฑ์ซีเมนต์ หิน และแร่อื่นๆ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('653','8114','ผู้ควบคุมเครื่องจักรผลิตคอนกรีตหล่อสำเร็จรูป');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('654','8114','ผู้ควบคุมเครื่องจักรผลิตผลิตภัณฑ์คอนกรีต');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('655','8121','ผู้ควบคุมเครื่องจักรโรงงานแปรรูปโลหะ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('656','8122','ผู้ควบคุมเครื่องจักรตกแต่ง ชุบ  และเคลือบผิวโลหะ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('657','8131','ผู้ควบคุมเครื่องจักรโรงงานและเครื่องจักรผลิตผลิตภัณฑ์เคมี');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('658','8131','ผู้ควบคุมเครื่องจักรผลิตผลิตภัณฑ์ทางเภสัชกรรมและเครื่องประเทืองโฉม');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('659','8132','ผู้ควบคุมเครื่องจักรผลิตผลิตภัณฑ์ด้านการถ่ายภาพ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('660','8132','ช่างเทคนิคในห้องมือสำหรับล้างรูป');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('661','8132','ผู้ควบคุมเครื่องล้างฟิล์ม');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('662','8132','เจ้าหน้าที่ล้างอัดรูป');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('663','8132','เจ้าหน้าที่อัดขยายภาพ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('664','8141','ผู้ควบคุมเครื่องจักรผลิตผลิตภัณฑ์ยาง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('665','8142','ผู้ควบคุมเครื่องจักรผลิตผลิตภัณฑ์พลาสติก');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('666','8142','พนักงานดึงเส้นใยแก้ว');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('667','8143','ผู้ควบคุมเครื่องจักรผลิตผลิตภัณฑ์กระดาษ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('668','8151','ผู้ควบคุมเครื่องจักรจัดเตรียมเส้นใย ปั่น และกรอเส้นใย');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('669','8152','ผู้ควบคุมเครื่องจักรทอผ้าและเครื่องจักรถักนิต');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('670','8152','ผู้ควบคุมเครื่องจักรผลิตตาข่าย');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('671','8153','ผู้ควบคุมเครื่องจักรเย็บผ้า');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('672','8153','ผู้ควบคุมเครื่องจักร ปักลวดลาย');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('673','8154','ผู้ควบคุมเครื่องจักรฟอก ย้อม  และทำความสะอาดเส้นใย');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('674','8155','ผู้ควบคุมเครื่องจักรจัดเตรียมขนสัตว์และหนังฟอก');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('675','8156','ผู้ควบคุมเครื่องจักรผลิตร้องเท้า');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('676','8157','ผู้ควบคุมเครื่องจักรด้านการซักรีด');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('677','8159','ผู้ควบคุมเครื่องจักรผลิตผลิตภัณฑ์สิ่งทอ ขนสัตว์ และหนังฟอก ซึ่งมิได้จัดประเภทไว้ในที่อื่น');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('678','8159','ผู้ควบคุมเครื่องจักรสร้างแบบ(pattern)');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('679','8159','ผู้ควบคุมเครื่องจักรผลิตหมวก');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('680','8159','ผู้ควบคุมเครื่องจักรผลิตเปีย');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('681','8159','ผู้ควบคุมเครื่องจักรผลิตเต็นท์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('682','8160','ผู้ควบคุมเครื่องจักรผลิตผลิตภัณฑ์อาหารและผลิตภัณฑ์ที่เกี่ยวข้อง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('683','8160','ผู้ควบคุมเครื่องจักรผลิตบุหรี่');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('684','8171','ผู้ควบคุมเครื่องจักรโรงงานผลิตเยื่อกระดาษและกระดาษ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('685','8172','ผู้ควบคุมเครื่องจักรแปรรูปไม้');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('686','8181','ผู้ปฏิบัติงานเครื่องจักรโรงงานผลิตแก้วและเซรามิก');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('687','8181','ผู้ควบคุมเตาเผาอิฐและกระเบื้อง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('688','8181','ผู้ควบคุมเครื่องจักรผสมดินเหนียว');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('689','8181','ผู้ควบคุมเครื่องปั้นดินเผาและเครื่องพอร์ชเลน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('690','8182','ผู้ปฏิบัติงานเครื่องจักรไอน้ำและหม้อน้ำ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('691','8183','ผู้ปฏิบัติงานเครื่องจักรที่ใช้ในการห่อและบรรจุผลิตภัณฑ์ การบรรจุขวด และการติดฉลาก');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('692','8189','ผู้ควบคุมเครื่องจักรฟั่นต่อสายเคเบิลและเชือก');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('693','8211','ผู้ประกอบเครื่องจักรกล');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('694','8211','พนักงานประกอบกระปุกเกียร์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('695','8211','พนักงานประกอบเครื่องยนต์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('696','8211','พนักงานประกอบยานยนต์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('697','8211','พนักงานติดตั้งเครื่องยนต์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('698','8211','พนักงานประกอบอากาศยาน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('699','8211','พนักงานประกอบเครื่องกังหัน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('700','8212','ผู้ประกอบอุปกรณ์ไฟฟ้าและอิเล็กทรอนิกส์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('701','8212','พนักงานประกอบโทรศัพท์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('702','8212','พนักงานประกอบโทรทัศน์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('703','8212','พนักงานประกอบนาฬิกา');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('704','8219','ผู้ปฏิบัติงานด้านการประกอบอื่นๆ  นอกเหนือจากชิ้นส่วนอิเล็กทรอนิกส์ ไฟฟ้าและเครื่องจักรกล');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('705','8311','ผู้ขับหัวรถจักร');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('706','8311','พนักงานขับรถไฟฟ้า');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('707','8311','พนักงานขับรถไฟ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('708','8312','พนักงานห้ามล้อรถไฟ พนักงานส่งสัญญาณรถไฟ และพนักงานสับเปลี่ยนรถไฟ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('709','8321','ผู้ขับขี่รถจักรยานยนต์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('710','8321','คนขับขี่รถจักรยานยนต์ส่งเอกสาร/สินค้า');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('711','8321','คนขับขี่รถสามล้อเครื่อง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('712','8322','ผู้ขับขี่รถยนต์ รถแท็กซี่ และรถตู้');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('713','8331','ผู้ขับรถโดยสารและรถราง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('714','8331','ผู้ขับรถโดยสารประจำทาง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('715','8332','ผู้ขับรถบรรทุกขนาดใหญ่');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('716','8332','คนขับรถพ่วง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('717','8332','คนขับรถขยะ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('718','8341','ผู้ควบคุมเครื่องจักรโรงงานชนิดเคลื่อนที่ได้ที่ใช้ในด้านการเกษตรและป่าไม้');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('719','8341','คนขับรถแทรกเตอร์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('720','8342','ผู้ควบคุมเครื่องจักรขนย้ายดินและหิน และเครื่องจักรโรงงานที่เกี่ยวข้อง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('721','8342','พนักงานขับรถบดถนน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('722','8342','พนักงานขับรถแทรกเตอร์เกลี่ยดิน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('723','8342','พนักงานขับรถปรับดิน/บลูโดเซอร์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('724','8342','พนักงานรถขุดดิน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('725','8343','ผู้ควบคุมปั้นจั่น รอกยก และเครื่องจักรโรงงานที่เกี่ยวข้อง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('726','8344','ผู้ควบคุมรถยกสินค้า');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('727','8344','คนขับรถยก');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('728','8350','ลูกเรือบนเรือและผู้ปฏิบัติงานอื่นๆที่เกี่ยวข้อง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('729','8350','กลาสีเรือ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('730','9111','คนงานและผู้ช่วยทำความสะอาดที่พักอาศัย');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('731','9111','คนทำงานบ้านทั่วไป');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('732','9112','คนงานและผู้ช่วยทำความสะอาดสำนักงาน โรงแรม และสถานประกอบการอื่นๆ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('733','9121','คนงานซักรีดเสื้อผ้าด้วยมือ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('734','9122','คนงานทำความสะอาดยานพาหนะ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('735','9123','คนงานทำความสะอาดหน้าต่าง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('736','9129','คนงานทำความสะอาดอื่นๆ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('737','9129','คนงาน ทำความสะอาดสระว่ายน้ำ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('738','9211','คนงานปลูกพืชไร่/พืชผัก');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('739','9212','คนงานเลี้ยงสัตว์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('740','9212','คนต้อนฝูงสัตว์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('741','9212','คนเก็บฟืน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('742','9212','คนตักน้ำ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('743','9213','คนงานปลูกพืชร่วมกับเลี้ยงสัตว์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('744','9214','คนงานปลูกพืชสวนและไม้ดอกไม้ประดับ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('745','9214','คนงานตัดหญ้าสนาม');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('746','9214','คนงานเพาะชำพันธุ์ไม้');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('747','9215','คนงานป่าไม้');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('748','9216','คนงานประมงและเพาะเลี้ยงสัตว์น้ำ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('749','9311','คนงานเหมืองแร่และเหมืองหิน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('750','9311','คนทดสอบตัวอย่างหิน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('751','9312','คนงานก่อสร้างและบำรุงรักษาถนน เขื่อน และงานวิศวกรรมโยธาอื่นๆที่คล้ายกัน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('752','9312','คนงานขนดิน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('753','9313','คนงานก่อสร้างอาคาร สิ่งปลูกสร้าง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('754','9313','คนงานรื้อถอนอาคาร/สิ่งปลูกสร้าง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('755','9313','คนงานแบกของในสถานที่ก่อสร้าง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('756','9321','คนงานบรรจุผลิตภัณฑ์ด้วยมือ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('757','9321','คนงานติดฉลากผลิตภัณฑ์ด้วยมือ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('758','9321','คนงานห่อผลิตภัณฑ์ด้วยมือ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('759','9329','คนงานด้านการผลิต ซึ่งมิได้จัดประเภทไว้ในที่อื่น');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('760','9331','คนงานขับเคลื่อนยานพาหนะโดยใช้มือหรือเท้า');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('761','9331','คนถีบสามล้อ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('762','9331','คนเข็นของ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('763','9332','คนงานขับเคลื่อนยานพาหนะและเครื่องจักรต่างๆ ที่ลากจูงโดยสัตว์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('764','9332','ควาญช้าง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('765','9333','คนงานขนถ่ายสินค้า');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('766','9334','คนงานเติมสินค้าบนชั้นวาง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('767','9411','ผู้ประกอบอาหารจานด่วน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('768','9412','ผู้ช่วยงานครัว');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('769','9412','เจ้าหน้าที่โรงครัว');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('770','9510','ผู้ให้บริการตามถนนและสถานที่ที่คล้ายกัน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('771','9510','คนเฝ้ารถยนต์/รับฝากรถยนต์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('772','9520','ผู้จำหน่ายสินค้าตามถนน(ยกเว้นอาหารพร้อมบริโภค)');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('773','9520','คนหาบของเร่ขาย');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('774','9520','คนเร่ขายของ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('775','9520','คนขายหนังสือพิมพ์ตามถนน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('776','9611','คนเก็บขยะและวัสดุที่สามารถนำกลับมาใช้ใหม่');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('777','9611','คนงานเก็บขยะ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('778','9612','คนงานคัดแยกขยะ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('779','9612','คนเก็บขยะหรือของเก่าขาย');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('780','9612','คนขายขยะรีไซเคิล');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('781','9613','คนกวาดถนนและผู้ใช้แรงงานที่เกี่ยวข้อง');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('782','9613','คนกวาดสวนสาธารณะ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('783','9613','คนกวาดขยะ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('784','9621','พนักงานรับส่งข่าวสาร หีบห่อสิ่วของและสัมภาระ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('785','9621','คนส่งหนังสือพิมพ์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('786','9621','พนักงานยกกระเป๋าโรงแรม');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('787','9621','คนส่งหนังสือพิมพ์');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('788','9621','คนเดินเอกสาร');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('789','9622','คนงานรับจ้างทั่วไป');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('790','9623','คนจดมาตรวัดและเก็บเงินจากเครื่องจำหน่ายสินค้าอัตโนมัติ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('791','9624','คนตักน้ำและเก็บฟืน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('792','9629','ผู้ประกอบวิชาชีพงานพื้นฐานอื่นๆซึ่งมิได้จัดประเภทไว้ที่อื่น');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('793','9629','พนักงานประจำลานจอดรถ');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('794','0110','ทหารชั้นสัญญาบัตร');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('795','0210','ทหารชั้นประทวน');
INSERT INTO r_rp1855_occupation (id, code, name) VALUES ('796','0310','ทหารยศอื่นๆ');

CREATE TABLE r_rp1855_religion
(
  id character varying(255) NOT NULL,
  name character varying(255) NOT NULL,
  CONSTRAINT religion_pkey PRIMARY KEY (id)
);

INSERT INTO r_rp1855_religion (id, name) VALUES ('01','ศาสนาพุทธ');
INSERT INTO r_rp1855_religion (id, name) VALUES ('02','ศาสนาอิสลาม');
INSERT INTO r_rp1855_religion (id, name) VALUES ('03','ศาสนาคริสต์');
INSERT INTO r_rp1855_religion (id, name) VALUES ('04','ศาสนาพราหมณ์-ฮินดู');
INSERT INTO r_rp1855_religion (id, name) VALUES ('05','ศาสนาซิกข์');
INSERT INTO r_rp1855_religion (id, name) VALUES ('06','ศาสนายิว');
INSERT INTO r_rp1855_religion (id, name) VALUES ('07','ศาสนาเชน');
INSERT INTO r_rp1855_religion (id, name) VALUES ('08','ศาสนาโซโรอัสเตอร์');
INSERT INTO r_rp1855_religion (id, name) VALUES ('09','ศาสนาบาไฮ');
INSERT INTO r_rp1855_religion (id, name) VALUES ('00','อศาสนา/ไม่นับถือศาสนา');
INSERT INTO r_rp1855_religion (id, name) VALUES ('99',' ไม่ระบุ');

-- map 12 file
CREATE TABLE b_map_rp1253_adpcode
(
  id character varying(255) NOT NULL,
  r_rp1253_adpcode_id character varying(255) NOT NULL,
  b_item_id character varying(255) NOT NULL,
  CONSTRAINT b_map_rp1253_adpcode_pkey PRIMARY KEY (id)
);

INSERT INTO b_map_rp1253_adpcode select b_item_id as id, r_rp1253_adpcode_id, b_item_id from b_item where r_rp1253_adpcode_id is not null and item_active = '1';

-- maybe exception when export data
--ALTER TABLE b_item DROP COLUMN r_rp1253_adpcode_id;

CREATE TABLE b_map_rp1253_charitem
(
  id character varying(255) NOT NULL,
  r_rp1253_charitem_id character varying(255) NOT NULL,
  b_item_id character varying(255) NOT NULL,
  CONSTRAINT b_map_rp1253_charitem_pkey PRIMARY KEY (id)
);

INSERT INTO b_map_rp1253_charitem select b_item_id as id, r_rp1253_charitem_id, b_item_id from b_item where r_rp1253_charitem_id is not null and item_active = '1';

-- maybe exception when export data
--ALTER TABLE b_item DROP COLUMN r_rp1253_charitem_id;

-- map 18 file
CREATE TABLE b_map_rp1853_instype
(
  id character varying(255) NOT NULL,
  r_rp1853_instype_id character varying(255) NOT NULL,
  b_contract_plans_id character varying(255) NOT NULL,
  CONSTRAINT b_map_rp1853_instype_pkey PRIMARY KEY (id)
);

INSERT INTO b_map_rp1853_instype select b_contract_plans_id as id, r_rp1853_instype_id, b_contract_plans_id from b_contract_plans where r_rp1853_instype_id is not null and r_rp1853_instype_id <> '' and contract_plans_active = '1';

-- maybe exception when export data
--ALTER TABLE b_contract_plans DROP COLUMN r_rp1853_instype_id;

CREATE TABLE b_map_rp1853_prefix
(
  id character varying(255) NOT NULL,
  r_rp1853_prefix_id character varying(255) NOT NULL,
  f_patient_prefix_id character varying(255) NOT NULL,
  CONSTRAINT b_map_rp1853_prefix_pkey PRIMARY KEY (id)
);

INSERT INTO b_map_rp1853_prefix select f_patient_prefix_id as id, r_rp1853_prefix_id, f_patient_prefix_id from f_patient_prefix where r_rp1853_prefix_id is not null and r_rp1853_prefix_id <> '' and active = '1';

-- maybe exception when export data
--ALTER TABLE f_patient_prefix DROP COLUMN r_rp1853_prefix_id;

CREATE TABLE b_map_rp1853_occupation
(
  id character varying(255) NOT NULL,
  r_rp1853_occupation_id character varying(255) NOT NULL,
  f_patient_occupation_id character varying(255) NOT NULL,
  CONSTRAINT b_map_rp1853_occupation_pkey PRIMARY KEY (id)
);

INSERT INTO b_map_rp1853_occupation
select f_patient_occupation_id as id, r_rp1853_occupation_id, f_patient_occupation_id from f_patient_occupation where r_rp1853_occupation_id is not null and r_rp1853_occupation_id <> '';

-- maybe exception when export data
--ALTER TABLE f_patient_occupation DROP COLUMN r_rp1853_occupation_id;

CREATE TABLE b_map_rp1853_nation
(
  id character varying(255) NOT NULL,
  r_rp1853_nation_id character varying(255) NOT NULL,
  f_patient_nation_id character varying(255) NOT NULL,
  CONSTRAINT b_map_rp1853_nation_pkey PRIMARY KEY (id)
);

INSERT INTO b_map_rp1853_nation
select f_patient_nation_id as id, r_rp1853_nation_id, f_patient_nation_id from f_patient_nation where r_rp1853_nation_id is not null and r_rp1853_nation_id <> '';

-- maybe exception when export data
--ALTER TABLE f_patient_nation DROP COLUMN r_rp1853_nation_id;

CREATE TABLE b_map_rp1853_education
(
  id character varying(255) NOT NULL,
  r_rp1853_education_id character varying(255) NOT NULL,
  f_patient_education_id character varying(255) NOT NULL,
  CONSTRAINT b_map_rp1853_education_pkey PRIMARY KEY (id)
);

INSERT INTO b_map_rp1853_instype
select f_patient_education_type_id as id, r_rp1853_education_id, f_patient_education_type_id from f_patient_education_type where r_rp1853_education_id is not null and r_rp1853_education_id <> '';

-- maybe exception when export data
--ALTER TABLE f_patient_education_type DROP COLUMN r_rp1853_education_id;

-- create s_mapping_std
CREATE TABLE s_mapping_std (
    version_id            varchar(255) NOT NULL,
    version_number            	varchar(255) NULL,
    version_description       	varchar(255) NULL,
    version_application_number	varchar(255) NULL,
    version_database_number   	varchar(255) NULL,
    version_update_time       	varchar(255) NULL 
);

ALTER TABLE s_mapping_std
	ADD CONSTRAINT s_mapping_std_pkey
	PRIMARY KEY (version_id);

INSERT INTO s_mapping_std VALUES ('1', '1', 'ReportMapStd Module', '1.0.0', '1.0.0', (select current_date) || ','|| (select current_time));

INSERT INTO s_script_update_log values ('ReportMapStd_Module','update_mapping_std_001.sql',(select current_date) || ','|| (select current_time),'Initialize ReportMapStd Module');

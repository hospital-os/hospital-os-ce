-- fix bug convert to timestamp with zone
CREATE OR REPLACE FUNCTION public.text_to_timestampz(text)
 RETURNS timestamp with time zone
 LANGUAGE plpgsql
 IMMUTABLE
AS $function$
BEGIN
        return case when length($1) >= 10 and ((substr($1,1,4)::int-543)||substr($1,5))::timestamp is not null then timezone('Asia/Bangkok',((substr($1,1,4)::int-543)||substr($1,5))::timestamp)  end;
exception when others then
        return null;
END;$function$
;

-- fix bug cal newspews score
CREATE OR REPLACE FUNCTION public.calculate_rrscore(visit_vital_sign_respiratory_rate text, agegroup integer)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
    declare
         RRmaxArray int[];
         RRmiduArray int[];
         RRmidlArray int[];
         RRmax int;
         RRmidu int;
         RRmidl int;
         RRmin text;
         RR int;
    BEGIN
        RRmaxArray := '{80, 80, 75, 65, 65, 48, 45, 45, 30, 24}'::int[];
        RRmiduArray := '{70, 70, 65, 55, 55, 38, 35, 35, 20, 20}'::int[];
        RRmidlArray := '{34, 34, 29, 24, 24, 14, 12, 12, 10, 11}'::int[];
        RRmax := RRmaxArray[agegroup];
        RRmidu := RRmiduArray[agegroup];
        RRmidl := RRmidlArray[agegroup];
    
        if(agegroup = 10) then
            RRmin := '8';
        else
            RRmin := 'FALSE';
        end if;
        
        /*raise notice '% % % %',RRmax,RRmidu,RRmidl,RRmin;*/

        if(visit_vital_sign_respiratory_rate is null or visit_vital_sign_respiratory_rate ='') then 
            return null;
        else
            RR := (visit_vital_sign_respiratory_rate::int);
            if(agegroup = 10) then
                if(RRmin = 'FALSE' or RR <= (RRmin::int)) then 
                    return 3;
                elsif(RR <= RRmidl) then
                    return 1;
                elsif(RR <= RRmidu) then
                    return 0;
                elsif(RR <= RRmax) then
                    return 2;
                else
                    return 3;
                end if;
            else
                if(RR < RRmidl) then
                    return 3;
                elsif(RR < RRmidu) then
                    return 0;
                elsif(RR <= RRmax) then
                    return 2;
                else
                    return 3;
                end if;
            end if;
        end if;
    END;
$function$
;

CREATE OR REPLACE FUNCTION public.calculate_hrscore(visit_vital_sign_heart_rate text, agegroup integer)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
	declare
		 HRmaxAHRay int[];
		 HRmiduAHRay int[];
		 HRmidlAHRay int[];
		 HRmax int;
		 HRmidu int;
		 HRmid text;
		 HRmidl int;
		 HRmin text;
		 HR int;
	BEGIN
		HRmaxAHRay := '{190, 180, 150, 150, 140, 140, 125, 125, 115, 130}'::int[];
		HRmiduAHRay := '{180, 170, 140, 140, 130, 130, 115, 115, 105, 110}'::int[];
		HRmidlAHRay := '{100, 100, 80, 80, 70, 65, 60, 60, 55, 50}'::int[];
		HRmax := HRmaxAHRay[agegroup];
		HRmidu := HRmiduAHRay[agegroup];
		HRmidl := HRmidlAHRay[agegroup];
		if(agegroup = 10) then
			HRmid := '90';
			HRmin := '40';
		else
			HRmid := 'FALSE';
			HRmin := 'FALSE';
		end if;
	
		
		/*raise notice '% % % % %',HRmax,HRmidu,HRmid,HRmidl,HRmin;*/

		if(visit_vital_sign_heart_rate is null or visit_vital_sign_heart_rate = '') then 
			return null;
		else
			HR := (visit_vital_sign_heart_rate::int);
			if(agegroup = 10) then
				if(HRmin = 'FALSE' or HR <= (HRmin::int)) then 
					return 3;
				elsif(HR <= HRmidl) then
					return 1;
				elsif(HRmid = 'FALSE' or HR <= (HRmid::int)) then
					return 0;
				elsif(HR <= HRmidu) then
					return 1;
				elsif(HR <= HRmax) then
					return 2;
				else
					return 3;
				end if;
			else
				if(HR < HRmidl) then
					return 3;
				elsif(HR < HRmidu) then
					return 0;
				elsif(HR <= HRmax) then
					return 2;
				else
					return 3;
				end if;
			end if;
		end if;
	END;
$function$
;

CREATE OR REPLACE FUNCTION public.calculate_o2supscore(visit_vital_sign_oxygen text, agegroup integer)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
    declare
        oxygen int;
    begin
        if(visit_vital_sign_oxygen = '') then
            return null;
        end if;
    
        oxygen := (visit_vital_sign_oxygen::int);
        
        if(ageGroup = 10) then
            if(oxygen = 0) then
                return 0;
            else 
                return 2;
            end if;
        else
            if(oxygen <= 2) then
                return 0;
            elsif(oxygen <= 5) then 
                return 1;
            elsif(oxygen <= 7) then 
                return 2;
            else
                return 3;
            end if;
        end if;
    END;
$function$
;

CREATE OR REPLACE FUNCTION public.calculate_btscore(visit_vital_sign_temperature text, agegroup integer)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
    declare
        BT float;
    begin
        if(visit_vital_sign_temperature is null or visit_vital_sign_temperature = '') then
            return null;
        else
            BT := (visit_vital_sign_temperature::float);
            if(ageGroup = 10) then
                if(BT <= 35.0) then
                    return 3;
                elsif(BT <= 36.0) then 
                    return 1;
                elsif(BT <= 38.0) then 
                    return 0;
                elsif(BT <= 39.0) then 
                    return 1;
                else
                    return 2;
                end if;
            else
                return 0;
            end if;     
        end if;
    END;
$function$
;

CREATE OR REPLACE FUNCTION public.calculate_bpscore(visit_vital_sign_blood_presure text, agegroup integer)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
    declare
        BPminArray int[];
        BPmin int;
        BP int;
    begin
        BPminArray := '{60, 70, 70, 70, 72, 76, 82, 86, 90, 90}'::int[];
        BPmin := BPminArray[agegroup];
    
        if(visit_vital_sign_blood_presure is null or visit_vital_sign_blood_presure = '') then
            return null;
        else
            BP := cast(substring(visit_vital_sign_blood_presure,1,(position('/' in visit_vital_sign_blood_presure)-1)) as numeric);
            if(ageGroup = 10) then
                if(BP < 90) then
                    return '3';
                elsif(BP <= 100) then 
                    return '2';
                elsif(BP <= 110) then 
                    return '1';
                elsif(BP <= 219) then 
                    return '0';
                else
                    return '3';
                end if;
            else
                return 0;
            end if;     
        end if;
    END;
$function$
;

CREATE OR REPLACE FUNCTION public.calculate_o2satscore(visit_vital_sign_spo2 text, agegroup integer)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
    declare
        o2sat int;
    begin
        if(visit_vital_sign_spo2 is null or visit_vital_sign_spo2 = '') then
            return null;
        else
            
            o2sat := visit_vital_sign_spo2::int;
            if(ageGroup = 10) then
                if(o2sat <= 91) then
                    return 3;
                elsif(o2sat <= 93) then 
                    return 2;
                elsif(o2sat <= 95) then 
                    return 1;
                else
                    return 0;
                end if;
            else
                return 0;
            end if;     
        end if;
    END;
$function$
;


ALTER TABLE public.b_news_pews_score ADD mins int4 NULL DEFAULT 0;

CREATE OR REPLACE FUNCTION public.insert_newspews_notify()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
DECLARE
  newspews_score INTEGER;
  set_score BOOLEAN;
BEGIN
    SELECT calculate_newspews_score(NEW.t_visit_vital_sign_id) as newspews_score
          ,(select case when b_news_pews_score.b_news_pews_score_id is not null 
                        then true else null end as newspews_score 
            from b_news_pews_score 
            group by newspews_score) as set_score
    FROM b_option_detail
    INTO newspews_score,set_score
    WHERE b_option_detail_id = 'use_notify_news_pews';
    
    IF set_score IS NULL THEN
        RETURN NULL;
    ELSIF newspews_score IS NULL THEN
        update t_visit set visit_vital_sign_score = null
                ,visit_vital_sign_notify_datetime = null
                ,visit_vital_sign_newspews_type = null
        where t_visit.t_visit_id = new.t_visit_id;
        RETURN NULL;
    ELSE
        IF (TG_OP = 'UPDATE') THEN 
            IF (NEW.visit_vital_sign_respiratory_rate <> OLD.visit_vital_sign_respiratory_rate
                OR NEW.visit_vital_sign_heart_rate <> OLD.visit_vital_sign_heart_rate
                OR NEW.visit_vital_sign_oxygen <> OLD.visit_vital_sign_oxygen
                OR NEW.visit_vital_sign_temperature <> OLD.visit_vital_sign_temperature
                OR NEW.visit_vital_sign_blood_presure <> OLD.visit_vital_sign_blood_presure
                OR NEW.visit_vital_sign_spo2 <> OLD.visit_vital_sign_spo2
				OR (NEW.f_avpu_type_id IS NOT NULL AND OLD.f_avpu_type_id IS NULL)
				OR (NEW.f_avpu_type_id IS NULL AND OLD.f_avpu_type_id IS NOT NULL)
				OR (NEW.f_received_nebulization_id IS NOT NULL AND OLD.f_received_nebulization_id IS NULL)
				OR (NEW.f_received_nebulization_id IS NULL AND OLD.f_received_nebulization_id IS NOT NULL)
				OR (NEW.f_vomitting_id IS NOT NULL AND OLD.f_vomitting_id IS NULL)
				OR (NEW.f_vomitting_id IS NULL AND OLD.f_vomitting_id IS NOT NULL)
				OR (NEW.f_cardiovascular_type_id IS NOT NULL AND OLD.f_cardiovascular_type_id IS NULL)
				OR (NEW.f_cardiovascular_type_id IS NULL AND OLD.f_cardiovascular_type_id IS NOT NULL)
                OR NEW.f_avpu_type_id <> OLD.f_avpu_type_id
                OR NEW.f_received_nebulization_id <> OLD.f_received_nebulization_id
                OR NEW.f_vomitting_id <> OLD.f_vomitting_id
                OR NEW.f_cardiovascular_type_id <> OLD.f_cardiovascular_type_id)
            THEN
                UPDATE t_newspews_notify SET status = 'CN'
                WHERE t_visit_vital_sign_id = NEW.t_visit_vital_sign_id
                AND t_newspews_notify.status = 'NW';

                INSERT INTO t_newspews_notify(t_newspews_notify_id, hn, vn, name, f_visit_type_id, service_id, service_name, visit_bed, score, next_timestamp, t_visit_vital_sign_id, newspews_type)
                select (select '806' || b_visit_office_id from b_site) || lpad(nextval('t_newspews_notify_id_seq')::text,17,'0') as t_newspews_notify_id
                            ,t_patient.patient_hn as hn 
                            ,t_visit.visit_vn as vn
                            ,case when f_patient_prefix.f_patient_prefix_id <> '000'
                                then f_patient_prefix.patient_prefix_description||t_patient.patient_firstname||' '||t_patient.patient_lastname
                                else t_patient.patient_firstname||' '||t_patient.patient_lastname end as name
                            ,t_visit.f_visit_type_id as f_visit_type_id
                            ,case when t_visit.f_visit_type_id = '1' then t_visit.b_visit_ward_id
                                  else t_visit_queue_transfer.b_service_point_id end as service_id
                            ,case when t_visit.f_visit_type_id = '1' then b_visit_ward.visit_ward_description
                                  else b_service_point.service_point_description end as service_name
                            ,case when t_visit.f_visit_type_id = '1' then t_visit.visit_bed
                                  else '' end as visit_bed
                            ,case when newspews_score is not null
                                then newspews_score
                                else 0 end as score
                             ,case when (newspews_score is not null
                                      and newspews_score = b_news_pews_score.score)
                                 then current_timestamp + (b_news_pews_score.hours || ' hours ' || b_news_pews_score.mins || ' minutes')::interval
                                 when (newspews_score is not null and b_news_pews_score.score is null)
                                 then current_timestamp + (select hours || ' hours ' || mins || ' minutes' from b_news_pews_score 
                                     where b_news_pews_score.newspews_type = calculate_newspews_type(calculate_agegroup(t_patient.t_patient_id)) 
                                     and newspews_score > score order by score desc limit 1)::interval
                                 else null end next_timestamp
                            ,t_visit_vital_sign.t_visit_vital_sign_id
                            ,calculate_newspews_type(calculate_agegroup(t_patient.t_patient_id)) as newspews_type
                    from t_visit_vital_sign
                        inner join t_visit on t_visit_vital_sign.t_visit_id = t_visit.t_visit_id
                        inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
                        left join f_patient_prefix on t_patient.f_patient_prefix_id = f_patient_prefix.f_patient_prefix_id
                        left join t_visit_queue_transfer on t_visit.t_visit_id = t_visit_queue_transfer.t_visit_id
                        left join b_service_point on t_visit_queue_transfer.b_service_point_id = b_service_point.b_service_point_id
                        left join b_visit_ward on t_visit.b_visit_ward_id = b_visit_ward.b_visit_ward_id
                        left join b_news_pews_score on newspews_score = b_news_pews_score.score
                        	  and b_news_pews_score.newspews_type = calculate_newspews_type(calculate_agegroup(t_patient.t_patient_id))
                    where t_visit_vital_sign.t_visit_vital_sign_id = NEW.t_visit_vital_sign_id
                    and t_visit_vital_sign.visit_vital_sign_active = '1';
             ELSE 
                RETURN NULL;
             END IF;
             RETURN NEW;
         ELSIF (TG_OP = 'INSERT') THEN
            INSERT INTO t_newspews_notify(t_newspews_notify_id, hn, vn, name, f_visit_type_id, service_id, service_name, visit_bed, score, next_timestamp, t_visit_vital_sign_id, newspews_type)
            select (select '806' || b_visit_office_id from b_site) || lpad(nextval('t_newspews_notify_id_seq')::text,17,'0') as t_newspews_notify_id
                        ,t_patient.patient_hn as hn 
                        ,t_visit.visit_vn as vn
                        ,case when f_patient_prefix.f_patient_prefix_id <> '000'
                            then f_patient_prefix.patient_prefix_description||t_patient.patient_firstname||' '||t_patient.patient_lastname
                            else t_patient.patient_firstname||' '||t_patient.patient_lastname end as name
                        ,t_visit.f_visit_type_id as f_visit_type_id
                        ,case when t_visit.f_visit_type_id = '1' then t_visit.b_visit_ward_id
                              else t_visit_queue_transfer.b_service_point_id end as service_id
                        ,case when t_visit.f_visit_type_id = '1' then b_visit_ward.visit_ward_description
                              else b_service_point.service_point_description end as service_name
                        ,case when t_visit.f_visit_type_id = '1' then t_visit.visit_bed
                              else '' end as visit_bed
                        ,case when newspews_score is not null
                            then newspews_score
                            else 0 end as score
                        ,case when (newspews_score is not null
                                      and newspews_score = b_news_pews_score.score)
                                 then current_timestamp + (b_news_pews_score.hours || ' hours ' || b_news_pews_score.mins || ' minutes')::interval
                                 when (newspews_score is not null and b_news_pews_score.score is null)
                                 then current_timestamp + (select hours || ' hours ' || mins || ' minutes' from b_news_pews_score 
                                     where b_news_pews_score.newspews_type = calculate_newspews_type(calculate_agegroup(t_patient.t_patient_id)) 
                                     and newspews_score > score order by score desc limit 1)::interval
                                 else null end next_timestamp
                        ,t_visit_vital_sign.t_visit_vital_sign_id
                        ,calculate_newspews_type(calculate_agegroup(t_patient.t_patient_id)) as newspews_type
                from t_visit_vital_sign
                    inner join t_visit on t_visit_vital_sign.t_visit_id = t_visit.t_visit_id
                    inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
                    left join f_patient_prefix on t_patient.f_patient_prefix_id = f_patient_prefix.f_patient_prefix_id
                    left join t_visit_queue_transfer on t_visit.t_visit_id = t_visit_queue_transfer.t_visit_id
                    left join b_service_point on t_visit_queue_transfer.b_service_point_id = b_service_point.b_service_point_id
                    left join b_visit_ward on t_visit.b_visit_ward_id = b_visit_ward.b_visit_ward_id
                    left join b_news_pews_score on newspews_score = b_news_pews_score.score
                    	  and b_news_pews_score.newspews_type = calculate_newspews_type(calculate_agegroup(t_patient.t_patient_id))
                where t_visit_vital_sign.t_visit_vital_sign_id = NEW.t_visit_vital_sign_id
			    and t_visit_vital_sign.visit_vital_sign_active = '1';
            RETURN NEW;
        END IF;
    END IF;
RETURN NEW;
END;
$function$
;

-- add newspews_type text, newspews_score integer, next_at timestamp
drop function latest_vitalsign(text) CASCADE ;

CREATE OR REPLACE FUNCTION public.latest_vitalsign(visit_id text)
 RETURNS TABLE(t_visit_id text, weight text, height text, bmi text, temp text, bp text, map text, spo2 text, pulse text, rr text,
waistline text, waistline_inch text, hips text, hips_inch text, chest text, chest_inch text,
oxygen text, nutrition text, cardiovascular text, avpu text, behavior text, received_nebulization text, 
vomitting text, note text, 
latest_vs_id text, latest_recoder_id text, latest_recoder text, latest_check_at timestamp with time zone, 
newspews_type text, newspews_score integer, next_at timestamp with time zone)
 LANGUAGE sql
AS $function$
        select
	visit_id as t_visit_id ,
	(array_remove(string_to_array(string_agg(ds.visit_vital_sign_weight, ','), ','), ''))[1] as weight ,
	(array_remove(string_to_array(string_agg(ds.visit_vital_sign_height, ','), ','), ''))[1] as height ,
	(array_remove(string_to_array(string_agg(ds.visit_vital_sign_bmi, ','), ','), ''))[1] as bmi ,
	(array_remove(string_to_array(string_agg(ds.visit_vital_sign_temperature, ','), ','), ''))[1] as temp ,
	(array_remove(string_to_array(string_agg(ds.visit_vital_sign_blood_presure, ','), ','), ''))[1] as bp ,
	(array_remove(string_to_array(string_agg(ds.visit_vital_sign_map, ','), ','), ''))[1] as map ,
	(array_remove(string_to_array(string_agg(ds.visit_vital_sign_spo2, ','), ','), ''))[1] as spo2 ,
	(array_remove(string_to_array(string_agg(ds.visit_vital_sign_heart_rate, ','), ','), ''))[1] as pulse ,
	(array_remove(string_to_array(string_agg(ds.visit_vital_sign_respiratory_rate, ','), ','), ''))[1] as rr ,
	(array_remove(string_to_array(string_agg(ds.visit_vital_sign_waistline, ','), ','), ''))[1] as waistline ,
	(array_remove(string_to_array(string_agg(ds.visit_vital_sign_waistline_inch, ','), ','), ''))[1] as waistline_inch ,
	(array_remove(string_to_array(string_agg(ds.visit_vital_sign_hips, ','), ','), ''))[1] as hips ,
	(array_remove(string_to_array(string_agg(ds.visit_vital_sign_hips_inch, ','), ','), ''))[1] as hips_inch ,
	(array_remove(string_to_array(string_agg(ds.visit_vital_sign_chest, ','), ','), ''))[1] as chest ,
	(array_remove(string_to_array(string_agg(ds.visit_vital_sign_chest_inch, ','), ','), ''))[1] as chest_inch ,
	(array_remove(string_to_array(string_agg(ds.visit_vital_sign_oxygen, ','), ','), ''))[1] as oxygen ,
	(array_remove(string_to_array(string_agg(ds.nutrition, ','), ','), ''))[1] as nutrition ,
        (array_remove(string_to_array(string_agg(ds.cardiovascular, ','), ','), ''))[1] as cardiovascular ,
	(array_remove(string_to_array(string_agg(ds.avpu, ','), ','), ''))[1] as avpu ,
	(array_remove(string_to_array(string_agg(ds.behavior, ','), ','), ''))[1] as behavior ,
	(array_remove(string_to_array(string_agg(ds.received_nebulization, ','), ','), ''))[1] as received_nebulization ,
	(array_remove(string_to_array(string_agg(ds.vomitting, ','), ','), ''))[1] as vomitting,
        (array_remove(string_to_array(string_agg(ds.visit_vital_sign_note, ','), ','), ''))[1] as note,
	(array_remove(string_to_array(string_agg(ds.t_visit_vital_sign_id, ','), ','), ''))[1] as latest_vs_id ,
	(array_remove(string_to_array(string_agg(ds.visit_vital_sign_staff_record, ','), ','), ''))[1] as latest_recoder_id ,
        (array_remove(string_to_array(string_agg(ds.recoder, ','), ','), ''))[1] as latest_recoder ,
	text_to_timestampz((array_remove(string_to_array(string_agg(ds.check_at, '|'), '|'), ''))[1]) as latest_check_at,
	(string_to_array(string_agg(case when ds.newspews_type is null then '' else ds.newspews_type end, ','), ','))[1] as newspews_type,
	(string_to_array(string_agg(case when ds.newspews_score is null then '0' else ds.newspews_score end, ','), ','))[1]::integer as newspews_score,
	text_to_timestampz((string_to_array(string_agg(case when ds.next_at is null then '' else ds.next_at end, '|'), '|'))[1]) as next_at
from
	(
	select
		tvvs.*,
                fvnl.visit_nutrition_level_description as nutrition,
		fct.description as cardiovascular,
		fat.description as avpu,
		fbt.description as behavior,
		fa1.answer_description as received_nebulization,
		fa2.answer_description as vomitting,
		((
		case
			when fpp.f_patient_prefix_id is null
			or fpp.f_patient_prefix_id::text = '000'::text then ''::character varying
			else fpp.patient_prefix_description
		end::text || tp.person_firstname::text) || ' '::text) || tp.person_lastname::text as recoder,
		tvvs.visit_vital_sign_check_date || ',' || tvvs.visit_vital_sign_check_time as check_at,
		tnn.newspews_type::text as newspews_type,
		tnn.score::text as newspews_score,
		timestamp_to_text(tnn.next_timestamp) as next_at
	from
		t_visit_vital_sign tvvs
	inner join b_employee be on
		be.b_employee_id = tvvs.visit_vital_sign_staff_record
	inner join t_person tp on
		tp.t_person_id::text = be.t_person_id::text
	left join f_patient_prefix fpp on
		fpp.f_patient_prefix_id::text = tp.f_prefix_id::text
        left join f_visit_nutrition_level fvnl on
                fvnl.f_visit_nutrition_level_id = tvvs.f_visit_nutrition_level_id
	left join f_cardiovascular_type fct on
		fct.f_cardiovascular_type_id = tvvs.f_cardiovascular_type_id
	left join f_avpu_type fat on
		fat.f_avpu_type_id = tvvs.f_avpu_type_id
	left join f_behavior_type fbt on
		fbt.f_behavior_type_id = tvvs.f_behavior_type_id
	left join f_answer fa1 on
		fa1.f_answer_id = tvvs.f_received_nebulization_id
	left join f_answer fa2 on
		fa2.f_answer_id = tvvs.f_vomitting_id
    left join t_newspews_notify tnn on
        tnn.t_visit_vital_sign_id = tvvs.t_visit_vital_sign_id and tnn.status != 'CN'
	where
		tvvs.t_visit_id = visit_id
		and tvvs.visit_vital_sign_active = '1'
	order by
		text_to_timestampz( visit_vital_sign_check_date || ',' || visit_vital_sign_check_time ) desc ) as ds

$function$
;

-- create new because drop function latest_vitalsign(text) CASCADE ;
CREATE OR REPLACE VIEW public.visit_medical_certificate_view
AS select 
t_visit.t_visit_id as t_visit_id
, t_patient.patient_hn as hn
, t_visit.visit_vn as vn
, case when f_patient_prefix.f_patient_prefix_id = null then '' else f_patient_prefix.patient_prefix_description end ||
t_patient.patient_firstname || ' '  ||t_patient.patient_lastname as name
, case when t_person_foreigner.t_person_foreigner_id = null 
then t_health_family.patient_firstname_eng || ' '  ||t_health_family.patient_lastname_eng 
else t_person_foreigner.passport_fname || ' '  ||t_person_foreigner.passport_lname
end as name_eng
, t_patient.patient_pid as pid
, t_person_foreigner.passport_no as passport
, text_to_timestamp(t_patient.patient_birthday)::date as dob
, (case when t_patient.patient_house is null or t_patient.patient_house = '' then '' else 'เลขที่ ' || t_patient.patient_house end) || ' ' ||
(case when t_patient.patient_moo is null or t_patient.patient_moo = '' then '' else 'หมู่ที่ ' || t_patient.patient_moo end) || ' ' ||
(case when t_patient.patient_road is null or t_patient.patient_road = '' then '' else 'ถนน' || t_patient.patient_road end) || ' ' ||
(case when tambol.f_address_id is null then '' else 'ตำบล/แขวง ' || tambol.address_description end) || ' ' ||
(case when amphur.f_address_id is null then '' else 'อำเภอ/เขต ' || amphur.address_description end) || ' ' ||
(case when changwat.f_address_id is null then '' else 'จังหวัด' || changwat.address_description end) || ' ' ||
(case when t_patient.patient_postcode is null or t_patient.patient_postcode = '' then '' else 'รหัสไปรษณีย์ ' || t_patient.patient_postcode end) as address
, t_patient.patient_phone_number as phone
, t_patient.patient_patient_mobile_phone as mobile
, f_patient_nation.patient_nation_description as nationality
, f_patient_occupation.patient_occupation_description as occupation
, t_health_family.skin_color
, vs.weight
, vs.height
, vs.bp
, vs.pulse
, array_to_string(
 array(select t_patient_personal_disease.patient_personal_disease_description from t_patient_personal_disease where t_patient_personal_disease.t_patient_id = t_patient.t_patient_id)
, ',') as disease
, t_person_foreigner.employer_name
, (case when t_person_foreigner.employer_address_house is null or t_person_foreigner.employer_address_house = '' then '' else 'เลขที่ ' || t_person_foreigner.employer_address_house end) || ' ' ||
(case when t_person_foreigner.employer_address_moo is null or t_person_foreigner.employer_address_moo = '' then '' else 'หมู่ที่ ' || t_person_foreigner.employer_address_moo end) || ' ' ||
(case when t_person_foreigner.employer_address_road is null or t_person_foreigner.employer_address_road = '' then '' else 'ถนน' || t_person_foreigner.employer_address_road end) || ' ' ||
(case when employer_tambol.f_address_id is null then '' else 'ตำบล/แขวง ' || employer_tambol.address_description end) || ' ' ||
(case when employer_amphur.f_address_id is null then '' else 'อำเภอ/เขต ' || employer_amphur.address_description end) || ' ' ||
(case when employer_changwat.f_address_id is null then '' else 'จังหวัด' || employer_changwat.address_description end) || ' ' ||
(case when t_person_foreigner.employer_address_postcode = null or t_person_foreigner.employer_address_postcode = '' then '' else 'รหัสไปรษณีย์ ' || t_person_foreigner.employer_address_postcode end) as employer_address
, t_person_foreigner.employer_contact_phone_number as employer_phone
, t_person_foreigner.employer_contact_mobile_phone_number as employer_mobile
from t_visit
inner join t_patient on t_patient.t_patient_id = t_visit.t_patient_id
inner join t_health_family on t_health_family.t_health_family_id = t_patient.t_health_family_id
left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_patient.f_patient_prefix_id
left join f_address as tambol on tambol.f_address_id = t_patient.patient_tambon
left join f_address as amphur on amphur.f_address_id = t_patient.patient_amphur
left join f_address as changwat on changwat.f_address_id = t_patient.patient_changwat
left join f_patient_nation on f_patient_nation.f_patient_nation_id = t_patient.f_patient_nation_id
left join f_patient_occupation on f_patient_occupation.f_patient_occupation_id = t_patient.f_patient_occupation_id
left join t_person_foreigner on t_person_foreigner.t_person_id = t_patient.t_person_id
left join f_address as employer_tambol on employer_tambol.f_address_id = t_person_foreigner.employer_address_tambon
left join f_address as employer_amphur on employer_amphur.f_address_id = t_person_foreigner.employer_address_amphur
left join f_address as employer_changwat on employer_changwat.f_address_id = t_person_foreigner.employer_address_changwat
left join latest_vitalsign(t_visit.t_visit_id) as vs on vs.t_visit_id = t_visit.t_visit_id;

-- update db version
INSERT INTO s_version VALUES ('9701000000103', '103', 'Hospital OS, Community Edition', '3.9.65b05', '3.45.2', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_65_b05.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.65b03');

-- require when use other postgres user
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA public to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA thai_ascvd to hospitalos;
-- GRANT ALL ON all sequences in schema thai_ascvd to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA rp50 to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA rp50nhso to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA rp50data to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA health_insurance to hospitalos;
-- GRANT ALL ON all sequences in schema public to hospitalos; 
-- GRANT ALL ON all sequences in schema rp50 to hospitalos; 
-- GRANT ALL ON all sequences in schema rp50nhso to hospitalos; 
-- GRANT ALL ON all sequences in schema rp50data to hospitalos; 
-- GRANT ALL ON all sequences in schema health_insurance to hospitalos;
-- GRANT ALL ON all sequences in schema thai_ascvd to hospitalos;
-- GRANT USAGE ON SCHEMA public TO hospitalos;
-- GRANT USAGE ON SCHEMA rp50 TO hospitalos;
-- GRANT USAGE ON SCHEMA rp50nhso TO hospitalos;
-- GRANT USAGE ON SCHEMA rp50data TO hospitalos;
-- GRANT USAGE ON SCHEMA health_insurance TO hospitalos;
-- GRANT USAGE ON SCHEMA thai_ascvd TO hospitalos;
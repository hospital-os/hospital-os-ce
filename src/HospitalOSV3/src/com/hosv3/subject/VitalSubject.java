package com.hosv3.subject;

import com.hosv3.usecase.transaction.ManageVitalResp;
import java.util.Vector;

/**
 *
 * @author tong
 */
@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public class VitalSubject implements ManageVitalResp {

    Vector theVitalObsV;
    Vector thesee;

    /**
     * Creates a new instance of visitSubject
     */
    public VitalSubject() {
        theVitalObsV = new Vector();
        thesee = new Vector();
    }

    public void removeAttach() {
        theVitalObsV.removeAllElements();
        thesee.removeAllElements();

    }

    public void attachManageVital(ManageVitalResp o) {
        theVitalObsV.add(o);
    }

    public void detach(ManageVitalResp o) {
        theVitalObsV.remove(o);
    }

    /**
     * @roseuid 3F8400140271
     */
    @Override
    public void notifyManageVitalSign(String msg, int state) {
        for (int i = 0, size = theVitalObsV.size(); i < size; i++) {
            ((ManageVitalResp) theVitalObsV.get(i)).notifyManageVitalSign(msg, state);
        }
    }

    @Override
    public void notifySaveDiagDoctor(String msg, int state) {
        for (int i = 0, size = theVitalObsV.size(); i < size; i++) {
            ((ManageVitalResp) theVitalObsV.get(i)).notifySaveDiagDoctor(msg, state);
        }
    }

    @Override
    public void notifySaveDxNote(String msg, int state) {
        for (int i = 0, size = theVitalObsV.size(); i < size; i++) {
            ((ManageVitalResp) theVitalObsV.get(i)).notifySaveDxNote(msg, state);
        }
    }

    @Override
    public void notifyAddDxTemplate(String msg, int state) {
        for (int i = 0, size = theVitalObsV.size(); i < size; i++) {
            ((ManageVitalResp) theVitalObsV.get(i)).notifyAddDxTemplate(msg, state);
        }
    }

    @Override
    public void notifyManagePrimarySymptom(String msg, int state) {
        for (int i = 0, size = theVitalObsV.size(); i < size; i++) {
            ((ManageVitalResp) theVitalObsV.get(i)).notifyManagePrimarySymptom(msg, state);
        }
    }

    @Override
    public void notifyDeleteMapVisitDxByVisitId(String msg, int state) {
        for (int i = 0, size = theVitalObsV.size(); i < size; i++) {
            ((ManageVitalResp) theVitalObsV.get(i)).notifyDeleteMapVisitDxByVisitId(msg, state);
        }
    }

    @Override
    public void notifyManagePhysicalExam(String str, int status) {
        for (int i = 0, size = theVitalObsV.size(); i < size; i++) {
            ((ManageVitalResp) theVitalObsV.get(i)).notifyManagePhysicalExam(str, status);
        }
    }

    /*
     * ������ա�������ҡ�ûѨ�غѹ�ҡDialog searchsub
     */
    @Override
    public void notifyAddPrimarySymptom(String str, int status) {
        for (int i = 0, size = theVitalObsV.size(); i < size; i++) {
            ((ManageVitalResp) theVitalObsV.get(i)).notifyAddPrimarySymptom(str, status);
        }
    }

}

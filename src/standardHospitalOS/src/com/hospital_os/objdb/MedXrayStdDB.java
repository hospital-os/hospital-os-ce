/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.MedXrayStd;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.ResultSet;
import java.util.Vector;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class MedXrayStdDB {

    private final ConnectionInf theConnectionInf;

    public MedXrayStdDB(ConnectionInf db) {
        theConnectionInf = db;
    }
    public MedXrayStd selectById(String id) throws Exception {
        String sql = "select * from b_med_xray_std where b_med_xray_std_id = '%s'";
        Vector eQuery = eQuery(String.format(sql, id));
        return (MedXrayStd) (eQuery.isEmpty() ? null : eQuery.get(0));
    }
    
    public Vector<MedXrayStd> listAll() throws Exception {
        String sql = "select * from b_med_xray_std";
        Vector eQuery = eQuery(sql);
        return eQuery;
    }

    public Vector eQuery(String sql) throws Exception {
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        while (rs.next()) {
            MedXrayStd p = new MedXrayStd();
            p.setObjectId(rs.getString("b_med_xray_std_id"));
            p.code = rs.getString("code");
            p.engname = rs.getString("engname");
            p.active = rs.getString("active");
            p.code_std = rs.getString("code_std");
            list.add(p);
        }
        rs.close();
        return list;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.objdb.objdbclass;

import com.hospital_os.usecase.connection.ConnectionInf;
import com.pcu.object.EpidemReport;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author tanakrit
 */
public class EpidemReportDB {

    private final ConnectionInf theConnectionInf;
    private final String tableId = "888";

    public EpidemReportDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public int insert(EpidemReport obj) throws Exception {
        String sql = "INSERT INTO t_health_epidem_report( \n"
                + "               t_health_epidem_report_id, t_visit_id, report_datetime, user_report_id, onset_date, \n"
                + "               treated_date, treated_hospital_code, f_epidem_person_status_id, f_epidem_covid_symptom_type_id, pregnant_status, \n"
                + "               respirator_status, vaccinated_status, exposure_epidemic_area_status, exposure_healthcare_worker_status, exposure_closed_contact_status, \n"
                + "               exposure_occupation_status, exposure_travel_status, f_epidem_risk_history_type_id, t_address_id, isolate_province_id, \n"
                + "               f_epidem_covid_isolate_place_id, specimen_date, f_epidem_covid_spcm_place_id, f_epidem_covid_reason_type_id, f_epidem_covid_lab_confirm_type_id, \n"
                + "               b_item_id, lab_report_date, lab_name, lab_value, active, \n"
                + "               record_datetime, user_record_id, user_update_id, \n"
                + "               sent_epidem, sent_moph, sent_moph_complete) \n"
                + "        VALUES( ?, ?, ?, ?, ?, \n"
                + "                ?, ?, ?, ?, ?, \n"
                + "                ?, ?, ?, ?, ?, \n"
                + "                ?, ?, ?, ?, ?, \n"
                + "                ?, ?, ?, ?, ?, \n"
                + "                ?, ?, ?, ?, ?, \n"
                + "                ?, ?, ?, \n"
                + "                ?, ?, ?)";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, obj.getGenID(tableId));
            ePQuery.setString(index++, obj.t_visit_id);
            ePQuery.setTimestamp(index++, new java.sql.Timestamp(obj.report_datetime != null ? obj.record_datetime.getTime() : new Date().getTime()));
            ePQuery.setString(index++, obj.user_report_id);
            ePQuery.setDate(index++, new java.sql.Date(obj.onset_date != null ? obj.onset_date.getTime() : new Date().getTime()));
            ePQuery.setDate(index++, new java.sql.Date(obj.treated_date != null ? obj.treated_date.getTime() : new Date().getTime()));
            ePQuery.setString(index++, obj.treated_hospital_code);
            ePQuery.setInt(index++, obj.f_epidem_person_status_id);
            ePQuery.setInt(index++, obj.f_epidem_covid_symptom_type_id);
            ePQuery.setBoolean(index++, obj.pregnant_status);
            ePQuery.setBoolean(index++, obj.respirator_status);
            ePQuery.setBoolean(index++, obj.vaccinated_status);
            ePQuery.setBoolean(index++, obj.exposure_epidemic_area_status);
            ePQuery.setBoolean(index++, obj.exposure_healthcare_worker_status);
            ePQuery.setBoolean(index++, obj.exposure_closed_contact_status);
            ePQuery.setBoolean(index++, obj.exposure_occupation_status);
            ePQuery.setBoolean(index++, obj.exposure_travel_status);
            ePQuery.setInt(index++, obj.f_epidem_risk_history_type_id);
            ePQuery.setString(index++, obj.t_address_id);
            ePQuery.setString(index++, obj.isolate_province_id);
            ePQuery.setInt(index++, obj.f_epidem_covid_isolate_place_id);
            ePQuery.setDate(index++, new java.sql.Date(obj.specimen_date != null ? obj.specimen_date.getTime() : new Date().getTime()));
            if (obj.f_epidem_covid_spcm_place_id == 0) {
                ePQuery.setNull(index++, java.sql.Types.INTEGER);
            } else {
                ePQuery.setInt(index++, obj.f_epidem_covid_spcm_place_id);
            }
            if (obj.f_epidem_covid_reason_type_id == 0) {
                ePQuery.setNull(index++, java.sql.Types.INTEGER);
            } else {
                ePQuery.setInt(index++, obj.f_epidem_covid_reason_type_id);
            }
            if (obj.f_epidem_covid_lab_confirm_type_id == 0) {
                ePQuery.setNull(index++, java.sql.Types.INTEGER);
            } else {
                ePQuery.setInt(index++, obj.f_epidem_covid_lab_confirm_type_id);
            }
            ePQuery.setString(index++, obj.b_item_id);
            ePQuery.setDate(index++, new java.sql.Date(obj.lab_report_date != null ? obj.lab_report_date.getTime() : new Date().getTime()));
            ePQuery.setString(index++, obj.lab_name);
            ePQuery.setString(index++, obj.lab_value);
            ePQuery.setString(index++, obj.active);
            ePQuery.setTimestamp(index++, new java.sql.Timestamp(obj.record_datetime != null ? obj.record_datetime.getTime() : new Date().getTime()));
            ePQuery.setString(index++, obj.user_record_id);
            ePQuery.setString(index++, obj.user_update_id);
            ePQuery.setBoolean(index++, obj.sent_epidem);
            ePQuery.setBoolean(index++, obj.sent_moph);
            ePQuery.setInt(index++, obj.sent_moph_complete);
            return ePQuery.executeUpdate();
        }
    }

    public int update(EpidemReport obj) throws Exception {
        String sql = "UPDATE t_health_epidem_report \n"
                + "      SET t_visit_id=?, report_datetime=?, user_report_id=?, onset_date=?, \n"
                + "          treated_date=?, treated_hospital_code=?, f_epidem_person_status_id=?, f_epidem_covid_symptom_type_id=?, pregnant_status=?, \n"
                + "          respirator_status=?, vaccinated_status=?, exposure_epidemic_area_status=?, exposure_healthcare_worker_status=?, exposure_closed_contact_status=?, \n"
                + "          exposure_occupation_status=?, exposure_travel_status=?, f_epidem_risk_history_type_id=?, t_address_id=?, isolate_province_id=?, \n"
                + "          f_epidem_covid_isolate_place_id=?, specimen_date=?, f_epidem_covid_spcm_place_id=?, f_epidem_covid_reason_type_id=?, f_epidem_covid_lab_confirm_type_id=?, \n"
                + "          b_item_id=?, lab_report_date=?, lab_name=?, lab_value=?, active=?, record_datetime=?,\n"
                + "          user_update_id=?, update_datetime=current_timestamp, sent_epidem=?, sent_moph=?, sent_moph_complete=? \n"
                + "    WHERE t_health_epidem_report_id=?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, obj.t_visit_id);
            ePQuery.setTimestamp(index++, new java.sql.Timestamp(obj.report_datetime != null ? obj.report_datetime.getTime() : new Date().getTime()));
            ePQuery.setString(index++, obj.user_report_id);
            ePQuery.setDate(index++, new java.sql.Date(obj.onset_date != null ? obj.onset_date.getTime() : new Date().getTime()));
            ePQuery.setDate(index++, new java.sql.Date(obj.treated_date != null ? obj.treated_date.getTime() : new Date().getTime()));
            ePQuery.setString(index++, obj.treated_hospital_code);
            ePQuery.setInt(index++, obj.f_epidem_person_status_id);
            ePQuery.setInt(index++, obj.f_epidem_covid_symptom_type_id);
            ePQuery.setBoolean(index++, obj.pregnant_status);
            ePQuery.setBoolean(index++, obj.respirator_status);
            ePQuery.setBoolean(index++, obj.vaccinated_status);
            ePQuery.setBoolean(index++, obj.exposure_epidemic_area_status);
            ePQuery.setBoolean(index++, obj.exposure_healthcare_worker_status);
            ePQuery.setBoolean(index++, obj.exposure_closed_contact_status);
            ePQuery.setBoolean(index++, obj.exposure_occupation_status);
            ePQuery.setBoolean(index++, obj.exposure_travel_status);
            ePQuery.setInt(index++, obj.f_epidem_risk_history_type_id);
            ePQuery.setString(index++, obj.t_address_id);
            ePQuery.setString(index++, obj.isolate_province_id);
            ePQuery.setInt(index++, obj.f_epidem_covid_isolate_place_id);
            ePQuery.setDate(index++, new java.sql.Date(obj.specimen_date != null ? obj.specimen_date.getTime() : new Date().getTime()));
            if (obj.f_epidem_covid_spcm_place_id == 0) {
                ePQuery.setNull(index++, java.sql.Types.INTEGER);
            } else {
                ePQuery.setInt(index++, obj.f_epidem_covid_spcm_place_id);
            }
            if (obj.f_epidem_covid_reason_type_id == 0) {
                ePQuery.setNull(index++, java.sql.Types.INTEGER);
            } else {
                ePQuery.setInt(index++, obj.f_epidem_covid_reason_type_id);
            }
            if (obj.f_epidem_covid_lab_confirm_type_id == 0) {
                ePQuery.setNull(index++, java.sql.Types.INTEGER);
            } else {
                ePQuery.setInt(index++, obj.f_epidem_covid_lab_confirm_type_id);
            }
            ePQuery.setString(index++, obj.b_item_id);
            ePQuery.setDate(index++, new java.sql.Date(obj.lab_report_date != null ? obj.lab_report_date.getTime() : new Date().getTime()));
            ePQuery.setString(index++, obj.lab_name);
            ePQuery.setString(index++, obj.lab_value);
            ePQuery.setString(index++, obj.active);
            ePQuery.setTimestamp(index++, new java.sql.Timestamp(obj.record_datetime != null ? obj.record_datetime.getTime() : new Date().getTime()));
            ePQuery.setString(index++, obj.user_update_id);
            ePQuery.setBoolean(index++, obj.sent_epidem);
            ePQuery.setBoolean(index++, obj.sent_moph);
            ePQuery.setInt(index++, obj.sent_moph_complete);
            ePQuery.setString(index++, obj.getObjectId());
            return ePQuery.executeUpdate();
        }
    }

    public int delete(EpidemReport obj) throws Exception {
        String sql = "UPDATE t_health_epidem_report \n"
                + "      SET active=?, user_update_id=?, update_datetime=current_timestamp ,user_cancel_id=?, cancel_datetime=current_timestamp \n"
                + "    WHERE t_health_epidem_report_id=?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, obj.active);
            ePQuery.setString(index++, obj.user_update_id);
            ePQuery.setString(index++, obj.user_cancel_id);
            ePQuery.setString(index++, obj.getObjectId());
            return ePQuery.executeUpdate();
        }
    }

    public int updateStatusSendOnline(EpidemReport obj) throws Exception {
        String sql = "UPDATE t_health_epidem_report \n"
                + "      SET sent_complete=?, sent_moph_complete=?, user_update_id=?, update_datetime=current_timestamp \n"
                + "    WHERE t_health_epidem_report_id=?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setInt(index++, obj.sent_complete);
            ePQuery.setInt(index++, obj.sent_moph_complete);
            ePQuery.setString(index++, obj.user_update_id);
            ePQuery.setString(index++, obj.getObjectId());
            return ePQuery.executeUpdate();
        }
    }

    public EpidemReport selectByItemId(String id) throws Exception {
        String sql = "select * from b_map_epidem_lab_type where b_item_id = ?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, id);
            List<EpidemReport> eQuery = executeQuery(ePQuery);
            return (EpidemReport) (eQuery.isEmpty() ? null : eQuery.get(0));
        }
    }

    public String queryEpidemReportByPk(String pk) throws Exception {
        String sql = "select * from doe_epidem_report_v39(?)";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, pk);
            String jsonData = "";
            try (ResultSet rs = ePQuery.executeQuery()) {
                while (rs.next()) {
                    jsonData = rs.getString(1);
                }
            }
            return jsonData;
        }
    }

    public String queryMOPHReportByPk(String pk) throws Exception {
        String sql = "select * from moph_epidem_report_V39(?)";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, pk);
            String jsonData = "";
            try (ResultSet rs = ePQuery.executeQuery()) {
                while (rs.next()) {
                    jsonData = rs.getString(1);
                }
            }
            return jsonData;
        }
    }

    public List<EpidemReport> executeQuery(PreparedStatement ePQuery) throws Exception {
        List<EpidemReport> list = new ArrayList<>();
        try (ResultSet rs = ePQuery.executeQuery()) {
            while (rs.next()) {
                EpidemReport p = new EpidemReport();
                p.setObjectId(rs.getString("t_health_epidem_report_id"));
                p.t_visit_id = rs.getString("t_visit_id");
                p.report_datetime = rs.getTimestamp("report_datetime");
                p.user_report_id = rs.getString("user_report_id");
                p.onset_date = rs.getDate("onset_date");
                p.treated_date = rs.getDate("treated_date");
                p.treated_hospital_code = rs.getString("treated_hospital_code");
                p.f_epidem_person_status_id = rs.getInt("f_epidem_person_status_id");
                p.f_epidem_covid_symptom_type_id = rs.getInt("f_epidem_covid_symptom_type_id");
                p.pregnant_status = rs.getBoolean("pregnant_status");
                p.respirator_status = rs.getBoolean("respirator_status");
                p.vaccinated_status = rs.getBoolean("vaccinated_status");
                p.exposure_epidemic_area_status = rs.getBoolean("exposure_epidemic_area_status");
                p.exposure_healthcare_worker_status = rs.getBoolean("exposure_healthcare_worker_status");
                p.exposure_closed_contact_status = rs.getBoolean("exposure_closed_contact_status");
                p.exposure_occupation_status = rs.getBoolean("exposure_occupation_status");
                p.exposure_travel_status = rs.getBoolean("exposure_travel_status");
                p.f_epidem_risk_history_type_id = rs.getInt("f_epidem_risk_history_type_id");
                p.t_address_id = rs.getString("t_address_id");
                p.isolate_province_id = rs.getString("isolate_province_id");
                p.f_epidem_covid_isolate_place_id = rs.getInt("f_epidem_covid_isolate_place_id");
                p.specimen_date = rs.getDate("specimen_date");
                p.f_epidem_covid_spcm_place_id = rs.getInt("f_epidem_covid_spcm_place_id");
                p.f_epidem_covid_reason_type_id = rs.getInt("f_epidem_covid_reason_type_id");
                p.f_epidem_covid_lab_confirm_type_id = rs.getInt("f_epidem_covid_lab_confirm_type_id");
                p.b_item_id = rs.getString("b_item_id");
                p.lab_report_date = rs.getDate("lab_report_date");
                p.lab_name = rs.getString("lab_name");
                p.lab_value = rs.getString("lab_value");
                p.active = rs.getString("active");
                p.record_datetime = rs.getTimestamp("record_datetime");
                p.user_record_id = rs.getString("user_record_id");
                p.user_cancel_id = rs.getString("user_cancel_id");
                p.sent_epidem = rs.getBoolean("sent_epidem");
                p.sent_moph = rs.getBoolean("sent_moph");
                p.sent_moph_complete = rs.getInt("sent_moph_complete");
                list.add(p);
            }
        }
        return list;
    }

}

-- isseus#718
-- เพิ่มเก็บข้อมูล b_hstock_item_id ในรายการรายการรักษาอัตโนมัติ กรณีที่เปิดใช้งานระบบคลัง
ALTER TABLE b_item_auto ADD IF NOT EXISTS b_hstock_item_id VARCHAR(255) DEFAULT NULL; 
-- เพิ่มเก็บข้อมูล b_hstock_item_id ในรายการรายการยาที่ใช้บ่อย กรณีที่เปิดใช้งานระบบคลัง
ALTER TABLE b_item_drug_favorite ADD IF NOT EXISTS b_hstock_item_id VARCHAR(255) DEFAULT NULL;  

--issues#719
-- เพิ่มเก็บข้อมูล b_hstock_item_id ในรายการยาและเวชภัณฑ์คุมกำเนิด กรณีที่เปิดใช้งานระบบคลัง
ALTER TABLE b_health_family_planing_item ADD IF NOT EXISTS b_hstock_item_id VARCHAR(255) DEFAULT NULL; 

-- issues#711
INSERT INTO b_option_detail(b_option_detail_id, option_detail_name, option_detail_note)
SELECT 'increase_newborn_age', '0', ''
ON CONFLICT (b_option_detail_id)
DO NOTHING;

-- issues#709
ALTER TABLE b_item ADD IF NOT EXISTS item_not_use_to_order VARCHAR(1) NOT NULL DEFAULT '0';

-- update db version
INSERT INTO s_version VALUES ('9701000000115', '115', 'Hospital OS, Community Edition', '3.11.0', '3.53.1', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_11_0.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.11.0');

-- require when use other postgres user
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA public to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA thai_ascvd to hospitalos;
-- GRANT ALL ON all sequences in schema thai_ascvd to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA rp50 to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA rp50nhso to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA rp50data to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA health_insurance to hospitalos;
-- GRANT ALL ON all sequences in schema public to hospitalos; 
-- GRANT ALL ON all sequences in schema rp50 to hospitalos; 
-- GRANT ALL ON all sequences in schema rp50nhso to hospitalos; 
-- GRANT ALL ON all sequences in schema rp50data to hospitalos; 
-- GRANT ALL ON all sequences in schema health_insurance to hospitalos;
-- GRANT ALL ON all sequences in schema thai_ascvd to hospitalos;
-- GRANT USAGE ON SCHEMA public TO hospitalos;
-- GRANT USAGE ON SCHEMA rp50 TO hospitalos;
-- GRANT USAGE ON SCHEMA rp50nhso TO hospitalos;
-- GRANT USAGE ON SCHEMA rp50data TO hospitalos;
-- GRANT USAGE ON SCHEMA health_insurance TO hospitalos;
-- GRANT USAGE ON SCHEMA thai_ascvd TO hospitalos;
CREATE TABLE public.t_audiometry (
	t_audiometry_id varchar(30) NOT NULL,
	t_visit_id varchar(30) NOT NULL,
	left_result varchar(255) NULL,
	right_result varchar(255) NULL,
	user_test_id varchar(30) NOT NULL,
	active varchar(1) NOT NULL DEFAULT '1'::character varying,
	record_date_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	user_record_id varchar(30) NOT NULL,
	update_date_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	user_update_id varchar(30) NOT NULL,
	CONSTRAINT t_audiometry_pkey PRIMARY KEY (t_audiometry_id)
);
CREATE INDEX t_visit_id_audiometry ON public.t_audiometry USING btree (t_visit_id);

ALTER TABLE public.t_eyes_exam ADD color_vision_result varchar(1) NOT NULL DEFAULT '9'; -- ไม่ได้ตรวจ
ALTER TABLE public.t_eyes_exam ADD color_vision_test_1_result text NULL;
ALTER TABLE public.t_eyes_exam ADD color_vision_test_2_result text NULL;
ALTER TABLE public.t_eyes_exam ADD color_vision_test_3_result text NULL;
ALTER TABLE public.t_eyes_exam ADD color_vision_test_4_result text NULL;
ALTER TABLE public.t_eyes_exam ADD color_vision_test_5_result text NULL;
ALTER TABLE public.t_eyes_exam ADD color_vision_test_6_result text NULL;
ALTER TABLE public.t_eyes_exam ADD color_vision_test_7_result text NULL;
ALTER TABLE public.t_eyes_exam ADD color_vision_test_8_result text NULL;
ALTER TABLE public.t_eyes_exam ADD color_vision_test_9_result text NULL;
ALTER TABLE public.t_eyes_exam ADD color_vision_test_10_result text NULL;
ALTER TABLE public.t_eyes_exam ADD color_vision_test_11_result text NULL;
ALTER TABLE public.t_eyes_exam ADD color_vision_test_12_result text NULL;
ALTER TABLE public.t_eyes_exam ADD color_vision_test_13_result text NULL;
ALTER TABLE public.t_eyes_exam ADD color_vision_test_14_result text NULL;
ALTER TABLE public.t_eyes_exam ADD color_vision_test_15_result text NULL;
ALTER TABLE public.t_eyes_exam ADD color_vision_test_16_result text NULL;
ALTER TABLE public.t_eyes_exam ADD color_vision_test_17_result text NULL;
ALTER TABLE public.t_eyes_exam ADD color_vision_test_18_result text NULL;
ALTER TABLE public.t_eyes_exam ADD color_vision_test_19_result text NULL;
ALTER TABLE public.t_eyes_exam ADD color_vision_test_20_result text NULL;
ALTER TABLE public.t_eyes_exam ADD color_vision_test_21_result text NULL;
ALTER TABLE public.t_eyes_exam ADD color_vision_test_22_result text NULL;
ALTER TABLE public.t_eyes_exam ADD color_vision_test_23_result text NULL;
ALTER TABLE public.t_eyes_exam ADD color_vision_test_24_result text NULL;
ALTER TABLE public.t_eyes_exam ADD color_vision_test_user_id varchar(30) NULL;

ALTER TABLE public.t_visit ALTER COLUMN visit_pregnant TYPE varchar(1) USING visit_pregnant::varchar;
ALTER TABLE public.t_visit ADD visit_lmp date NULL DEFAULT null;


ALTER TABLE public.f_patient_prefix ADD patient_prefix_description_eng varchar(255) NULL;
ALTER TABLE public.f_patient_prefix ALTER COLUMN patient_prefix_description SET NOT NULL;
ALTER TABLE public.f_patient_prefix ALTER COLUMN active SET NOT NULL;
ALTER TABLE public.f_patient_prefix ALTER COLUMN f_sex_id TYPE varchar(1) USING f_sex_id::varchar;
ALTER TABLE public.f_patient_prefix ALTER COLUMN f_sex_id SET NOT NULL;

update f_patient_prefix set patient_prefix_description_eng = '' where f_sex_id = '';
update f_patient_prefix set patient_prefix_description_eng = 'Mr.' where f_sex_id = '1';
update f_patient_prefix set patient_prefix_description_eng = 'Miss' where f_sex_id = '2';
update f_patient_prefix set patient_prefix_description_eng = 'Mrs.' where patient_prefix_description = 'นาง';


ALTER TABLE t_patient ADD patient_addrees_eng text DEFAULT '';

ALTER TABLE t_health_family ADD patient_firstname_eng character varying(255) NULL;
ALTER TABLE t_health_family ADD patient_lastname_eng character varying(255) NULL;
ALTER TABLE t_health_family ADD passport_no varchar(10) NULL;

update t_health_family set passport_no = tpf.passport_no 
, patient_firstname_eng = tpf.passport_fname 
, patient_lastname_eng = tpf.passport_lname 
FROM t_person_foreigner tpf
where t_health_family.t_health_family_id = tpf.t_person_id;

update t_health_family set patient_firstname_eng = tp.patient_firstname_eng 
, patient_lastname_eng = tp.patient_lastname_eng 
FROM t_patient tp
where t_health_family.t_health_family_id = tp.t_health_family_id;

-- change name_eng filed
DROP VIEW public.visit_medical_certificate_view;
CREATE OR REPLACE VIEW public.visit_medical_certificate_view
AS select 
t_visit.t_visit_id as t_visit_id
, t_patient.patient_hn as hn
, t_visit.visit_vn as vn
, case when f_patient_prefix.f_patient_prefix_id = null then '' else f_patient_prefix.patient_prefix_description end ||
t_patient.patient_firstname || ' '  ||t_patient.patient_lastname as name
, case when t_person_foreigner.t_person_foreigner_id = null 
then t_health_family.patient_firstname_eng || ' '  ||t_health_family.patient_lastname_eng 
else t_person_foreigner.passport_fname || ' '  ||t_person_foreigner.passport_lname
end as name_eng
, t_patient.patient_pid as pid
, t_person_foreigner.passport_no as passport
, text_to_timestamp(t_patient.patient_birthday)::date as dob
, (case when t_patient.patient_house is null or t_patient.patient_house = '' then '' else 'เลขที่ ' || t_patient.patient_house end) || ' ' ||
(case when t_patient.patient_moo is null or t_patient.patient_moo = '' then '' else 'หมู่ที่ ' || t_patient.patient_moo end) || ' ' ||
(case when t_patient.patient_road is null or t_patient.patient_road = '' then '' else 'ถนน' || t_patient.patient_road end) || ' ' ||
(case when tambol.f_address_id is null then '' else 'ตำบล/แขวง ' || tambol.address_description end) || ' ' ||
(case when amphur.f_address_id is null then '' else 'อำเภอ/เขต ' || amphur.address_description end) || ' ' ||
(case when changwat.f_address_id is null then '' else 'จังหวัด' || changwat.address_description end) || ' ' ||
(case when t_patient.patient_postcode is null or t_patient.patient_postcode = '' then '' else 'รหัสไปรษณีย์ ' || t_patient.patient_postcode end) as address
, t_patient.patient_phone_number as phone
, t_patient.patient_patient_mobile_phone as mobile
, f_patient_nation.patient_nation_description as nationality
, f_patient_occupation.patient_occupation_description as occupation
, t_health_family.skin_color
, vs.weight
, vs.height
, vs.bp
, vs.pulse
, array_to_string(
 array(select t_patient_personal_disease.patient_personal_disease_description from t_patient_personal_disease where t_patient_personal_disease.t_patient_id = t_patient.t_patient_id)
, ',') as disease
, t_person_foreigner.employer_name
, (case when t_person_foreigner.employer_address_house is null or t_person_foreigner.employer_address_house = '' then '' else 'เลขที่ ' || t_person_foreigner.employer_address_house end) || ' ' ||
(case when t_person_foreigner.employer_address_moo is null or t_person_foreigner.employer_address_moo = '' then '' else 'หมู่ที่ ' || t_person_foreigner.employer_address_moo end) || ' ' ||
(case when t_person_foreigner.employer_address_road is null or t_person_foreigner.employer_address_road = '' then '' else 'ถนน' || t_person_foreigner.employer_address_road end) || ' ' ||
(case when employer_tambol.f_address_id is null then '' else 'ตำบล/แขวง ' || employer_tambol.address_description end) || ' ' ||
(case when employer_amphur.f_address_id is null then '' else 'อำเภอ/เขต ' || employer_amphur.address_description end) || ' ' ||
(case when employer_changwat.f_address_id is null then '' else 'จังหวัด' || employer_changwat.address_description end) || ' ' ||
(case when t_person_foreigner.employer_address_postcode = null or t_person_foreigner.employer_address_postcode = '' then '' else 'รหัสไปรษณีย์ ' || t_person_foreigner.employer_address_postcode end) as employer_address
, t_person_foreigner.employer_contact_phone_number as employer_phone
, t_person_foreigner.employer_contact_mobile_phone_number as employer_mobile
from t_visit
inner join t_patient on t_patient.t_patient_id = t_visit.t_patient_id
inner join t_health_family on t_health_family.t_health_family_id = t_patient.t_health_family_id
left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_patient.f_patient_prefix_id
left join f_address as tambol on tambol.f_address_id = t_patient.patient_tambon
left join f_address as amphur on amphur.f_address_id = t_patient.patient_amphur
left join f_address as changwat on changwat.f_address_id = t_patient.patient_changwat
left join f_patient_nation on f_patient_nation.f_patient_nation_id = t_patient.f_patient_nation_id
left join f_patient_occupation on f_patient_occupation.f_patient_occupation_id = t_patient.f_patient_occupation_id
left join t_person_foreigner on t_person_foreigner.t_person_id = t_patient.t_person_id
left join f_address as employer_tambol on employer_tambol.f_address_id = t_person_foreigner.employer_address_tambon
left join f_address as employer_amphur on employer_amphur.f_address_id = t_person_foreigner.employer_address_amphur
left join f_address as employer_changwat on employer_changwat.f_address_id = t_person_foreigner.employer_address_changwat
left join latest_vitalsign(t_visit.t_visit_id) as vs on vs.t_visit_id = t_visit.t_visit_id;

ALTER TABLE public.t_patient DROP COLUMN patient_firstname_eng;
ALTER TABLE public.t_patient DROP COLUMN patient_lastname_eng;

insert into f_gui_action (f_gui_action_id, gui_action_name) VALUES ('5520', 'สิทธิแสดงการแจ้งข้อมูลแพ้ยา/สารเคมีและอื่นๆ');
insert into f_gui_action (f_gui_action_id, gui_action_name) VALUES ('5521', 'สิทธิแสดงการแจ้งเตือนแพ้ยา');

CREATE TABLE IF NOT EXISTS b_map_warning_inform_intolerance (
    b_map_warning_inform_intolerance_id  CHARACTER VARYING(30) NOT NULL,
    f_employee_authentication_id   CHARACTER VARYING(255) NOT NULL,
    user_record_id character varying(255) NOT NULL,
    record_date_time timestamp without time zone NOT NULL default current_timestamp,
CONSTRAINT b_map_warning_inform_intolerance_pkey PRIMARY KEY (b_map_warning_inform_intolerance_id),
CONSTRAINT b_map_warning_inform_intolerance_unique UNIQUE (b_map_warning_inform_intolerance_id));

INSERT INTO b_map_warning_inform_intolerance(b_map_warning_inform_intolerance_id,f_employee_authentication_id, user_record_id)
SELECT '863' || to_char(current_timestamp, 'YYMMDD')||f_employee_authentication.f_employee_authentication_id
||rpad(row_number() OVER (partition by f_employee_authentication.f_employee_authentication_id)::text,5,trunc(random()*10^10)::text) as b_map_warning_inform_intolerance_id
    ,f_employee_authentication.f_employee_authentication_id
, ''
FROM f_employee_authentication
WHERE f_employee_authentication.f_employee_authentication_id in ('2','3','6','13');

CREATE TABLE IF NOT EXISTS b_map_warning_drug_allergy (
    b_map_warning_drug_allergy_id  CHARACTER VARYING(30) NOT NULL,
    f_employee_authentication_id   CHARACTER VARYING(255) NOT NULL,
    user_record_id character varying(255) NOT NULL,
    record_date_time timestamp without time zone NOT NULL default current_timestamp,
CONSTRAINT b_map_warning_drug_allergy_pkey PRIMARY KEY (b_map_warning_drug_allergy_id),
CONSTRAINT b_map_warning_drug_allergy_unique UNIQUE (b_map_warning_drug_allergy_id));

INSERT INTO b_map_warning_drug_allergy(b_map_warning_drug_allergy_id,f_employee_authentication_id, user_record_id)
SELECT '863' || to_char(current_timestamp, 'YYMMDD')||f_employee_authentication.f_employee_authentication_id
||rpad(row_number() OVER (partition by f_employee_authentication.f_employee_authentication_id)::text,5,trunc(random()*10^10)::text) as b_map_warning_drug_allergy_id
    ,f_employee_authentication.f_employee_authentication_id
, ''
FROM f_employee_authentication
WHERE f_employee_authentication.f_employee_authentication_id in ('3','6');


-- เพิ่ม min-max ค่าวิกฤต (Critical Values) ตาราง t_result_lab
ALTER TABLE t_result_lab ADD COLUMN IF NOT EXISTS result_lab_critical_min VARCHAR(255) DEFAULT '';
ALTER TABLE t_result_lab ADD COLUMN IF NOT EXISTS result_lab_critical_max VARCHAR(255) DEFAULT '';

-- เพิ่ม min-max ค่าวิกฤต (Critical Values) แยกตามเพศ ตาราง b_item_lab_result_gender
ALTER TABLE b_item_lab_result_gender ADD COLUMN IF NOT EXISTS male_result_critical_min VARCHAR(255) DEFAULT '';
ALTER TABLE b_item_lab_result_gender ADD COLUMN IF NOT EXISTS male_result_critical_max VARCHAR(255) DEFAULT '';
ALTER TABLE b_item_lab_result_gender ADD COLUMN IF NOT EXISTS female_result_critical_min VARCHAR(255) DEFAULT '';
ALTER TABLE b_item_lab_result_gender ADD COLUMN IF NOT EXISTS female_result_critical_max VARCHAR(255) DEFAULT '';

-- เพิ่ม min-max ค่าวิกฤต (Critical Values) แยกตามอายุและเพศ ตาราง b_item_lab_result_age
ALTER TABLE b_item_lab_result_age ADD COLUMN IF NOT EXISTS male_critical_min VARCHAR(255) DEFAULT '';
ALTER TABLE b_item_lab_result_age ADD COLUMN IF NOT EXISTS male_critical_max VARCHAR(255) DEFAULT '';
ALTER TABLE b_item_lab_result_age ADD COLUMN IF NOT EXISTS female_critical_min VARCHAR(255) DEFAULT '';
ALTER TABLE b_item_lab_result_age ADD COLUMN IF NOT EXISTS female_critical_max VARCHAR(255) DEFAULT '';

-- query boost
CREATE INDEX ON public.t_notify_note USING btree(t_patient_hn,f_notify_type_id,active,t_notify_note_id,show_with_custom);
CREATE INDEX ON public.t_visit USING btree(f_visit_type_id,f_visit_status_id,b_visit_ward_id);
CREATE INDEX ON public.t_order USING btree(t_visit_id,f_order_status_id,f_item_group_id);
CREATE INDEX ON public.t_visit_bed USING btree(t_visit_id,current_bed,active);
CREATE INDEX ON public.t_visit_refer_in_out USING btree(f_visit_refer_type_id,t_visit_id,visit_refer_in_out_active);
CREATE INDEX ON public.t_order_drug_interaction USING btree(order_item_id,order_drug_interaction_active);
CREATE INDEX ON public.t_visit USING btree(t_visit_id,f_visit_type_id);
CREATE INDEX ON public.t_patient_appointment USING btree(t_patient_id,patient_appointment_active);
CREATE INDEX ON public.t_result_xray_size USING btree(t_result_xray_id,result_xray_size_active);
CREATE INDEX ON public.t_order USING btree(order_verify_date_time,f_item_group_id);
CREATE INDEX ON public.t_death USING btree(t_patient_id,death_active);
CREATE INDEX ON public.t_visit_queue_lab USING btree(t_visit_id,visit_queue_order);
CREATE INDEX ON public.t_patient_personal_disease USING btree(t_patient_id);
CREATE INDEX ON public.t_visit_queue_lab USING btree(visit_queue_lab_remain);
CREATE INDEX ON public.t_oppp56_notpass USING btree(t_visit_id);
CREATE INDEX ON public.t_visit_queue_coding USING btree(assign_date_time);
CREATE INDEX ON public.b_item_drug USING btree(b_item_id);
CREATE INDEX ON public.b_item_price USING btree(b_item_id);
CREATE INDEX ON public.b_item_lab_result USING btree(b_item_id);
CREATE INDEX ON public.t_order_drug_return USING btree(t_visit_id);
CREATE INDEX ON public.t_visit_physical_exam USING btree(t_visit_id);
CREATE INDEX ON public.t_visit_discharge_advice USING btree(t_visit_id);
CREATE INDEX ON public.t_patient_past_history USING btree(t_patient_id);
CREATE INDEX ON public.b_icd10 USING btree(active);
CREATE INDEX ON public.t_notify_note USING btree(t_visit_id_last_view);
CREATE INDEX ON public.b_item USING btree(item_active);
CREATE INDEX ON public.t_patient_appointment USING btree(patient_appointment_status);
CREATE INDEX ON public.t_patient_risk_factor USING btree(t_patient_id);

-- update db version
INSERT INTO s_version VALUES ('9701000000092', '92', 'Hospital OS, Community Edition', '3.9.59', '3.39.0', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_59.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.59');
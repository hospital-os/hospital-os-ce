/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.BVisitRangeAge;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tanakrit
 */
public class BVisitRangeAgeDB {

    private final ConnectionInf theConnectionInf;
    final public String tableId = "974";

    public BVisitRangeAgeDB(ConnectionInf connectionInf) {
        this.theConnectionInf = connectionInf;
    }

    public int insert(BVisitRangeAge p) throws Exception {
        String sql = "INSERT INTO b_visit_range_age( \n"
                + "               b_visit_range_age_id, code, description, \n"
                + "               begin_age, end_age, active, user_record_id, user_update_id) \n"
                + "        VALUES(?, ?, ?, ?, ?, ? , ?, ?)";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, p.getGenID(tableId));
            ePQuery.setString(index++, p.code);
            ePQuery.setString(index++, p.description);
            ePQuery.setInt(index++, Integer.parseInt(p.begin_age));
            ePQuery.setInt(index++, Integer.parseInt(p.end_age));
            ePQuery.setString(index++, p.active);
            ePQuery.setString(index++, p.user_record_id);
            ePQuery.setString(index++, p.user_update_id);
            return ePQuery.executeUpdate();
        }
    }

    public int update(BVisitRangeAge p) throws Exception {
        String sql = "UPDATE b_visit_range_age \n"
                + "      SET code=?, description=?, begin_age=?, end_age=?, active=?, update_date_time=CURRENT_TIMESTAMP, user_update_id=? \n"
                + "    WHERE b_visit_range_age_id=?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, p.code);
            ePQuery.setString(index++, p.description);
            ePQuery.setInt(index++, Integer.parseInt(p.begin_age));
            ePQuery.setInt(index++, Integer.parseInt(p.end_age));
            ePQuery.setString(index++, p.active);
            ePQuery.setString(index++, p.user_update_id);
            ePQuery.setString(index++, p.getObjectId());
            return ePQuery.executeUpdate();
        }
    }

    public int inactive(BVisitRangeAge p) throws Exception {
        String sql = "UPDATE b_visit_range_age\n"
                + "      SET active='0' ,update_date_time=current_timestamp, user_update_id=?\n"
                + "    WHERE b_visit_range_age_id=?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, p.user_update_id);
            ePQuery.setString(index++, p.getObjectId());
            return ePQuery.executeUpdate();
        }
    }

    public BVisitRangeAge selectBetweenAge(String age) throws Exception {
        if (age == null || age.isEmpty()) {
            return null;
        }
        String sql = "select * from b_visit_range_age where ? between begin_age and end_age and active='1' \n";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setInt(index++, Integer.parseInt(age));
            List<BVisitRangeAge> executeQuery = executeQuery(ePQuery);
            return executeQuery.isEmpty() ? null : executeQuery.get(0);
        }
    }

    public List<BVisitRangeAge> listByKeyword(String keyword) throws Exception {
        return listByKeyword(keyword, "1");
    }

    public List<BVisitRangeAge> listByKeyword(String keyword, String active) throws Exception {
        String sql = "select *  from b_visit_range_age where " + (keyword == null || keyword.isEmpty() ? "" : "(code ilike ? or description ilike ?) and ") + "active = ? order by description";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            if (keyword != null && !keyword.isEmpty()) {
                ePQuery.setString(index++, "%" + keyword + "%");
                ePQuery.setString(index++, "%" + keyword + "%");
            }
            ePQuery.setString(index++, active);
            List<BVisitRangeAge> list = executeQuery(ePQuery);
            return list;
        }
    }

    public List<BVisitRangeAge> listAll() throws Exception {
        String sql = "select * from b_visit_range_age \n";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            List<BVisitRangeAge> list = executeQuery(ePQuery);
            return list;
        }
    }

    public List<BVisitRangeAge> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<BVisitRangeAge> list = new ArrayList<>();
        try (ResultSet rs = preparedStatement.executeQuery()) {
            while (rs.next()) {
                BVisitRangeAge obj = new BVisitRangeAge();
                obj.setObjectId(rs.getString("b_visit_range_age_id"));
                obj.code = rs.getString("code");
                obj.description = rs.getString("description");
                obj.active = rs.getString("active");
                obj.begin_age = String.valueOf(rs.getInt("begin_age"));
                obj.end_age = String.valueOf(rs.getInt("end_age"));
                obj.record_date_time = rs.getTimestamp("record_date_time");
                obj.user_record_id = rs.getString("user_record_id");
                obj.update_date_time = rs.getTimestamp("update_date_time");
                obj.user_update_id = rs.getString("user_update_id");
                list.add(obj);
            }
            return list;
        }
    }
}

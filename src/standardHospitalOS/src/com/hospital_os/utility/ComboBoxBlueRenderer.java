/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.utility;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JList;
import javax.swing.UIManager;

/**
 *
 * @author Somprasong
 */
public class ComboBoxBlueRenderer extends javax.swing.plaf.basic.BasicComboBoxRenderer {

    private final Color oldColor;

    public ComboBoxBlueRenderer() {
        super();
        oldColor = UIManager.getColor("ComboBox.selectionBackground");
        setOpaque(true);
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        setText(value == null ? "" : value.toString());
        setToolTipText(getText());
        if (isSelected) {
            setBackground(oldColor);
        } else {
            setBackground(new Color(204, 255, 255));
        }
        return this;
    }
}
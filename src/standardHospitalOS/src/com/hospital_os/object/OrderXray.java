/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;
import java.util.Date;

/**
 *
 * @author sompr
 */
public class OrderXray extends Persistent {

    public String t_visit_id = "";
    public String t_order_id = "";
    public String b_modality_id = "";
    public String accession_number = "";
    public String priority = "O";
    public String order_status = "1";
    public String pacs_status = "0";
    public String doctor_id;
    public Date order_datetime;
    public String executor_id = "";
    public Date execute_datetime;
    public String cancel_id;
    public Date cancel_datetime;
    public Date record_datetime;
    public String dicom_status = "0";
    public String dicom_text;
}

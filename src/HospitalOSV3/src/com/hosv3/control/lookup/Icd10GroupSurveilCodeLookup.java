/*
 * Icd9Lookup.java
 *
 * Created on 20 �ѹ��¹ 2548, 9:48 �.
 */
package com.hosv3.control.lookup;

import com.hospital_os.object.ICD10;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.LookupControlInf;
import com.hosv3.control.LookupControl;
import com.hosv3.control.SetupControl;
import com.hosv3.utility.connection.ExecuteControlInf;

/**
 *
 * @not deprecated because use henbe package and bad read data
 *
 * @author sumo
 */
public class Icd10GroupSurveilCodeLookup implements LookupControlInf, ExecuteControlInf {

    boolean is_lookup = true;
    private LookupControl theLookup;
    private SetupControl theEC;

    /**
     * Creates a new instance of VitalTemplateLookup
     */
    public Icd10GroupSurveilCodeLookup(LookupControl lookup) {
        theLookup = lookup;
    }

    public Icd10GroupSurveilCodeLookup(SetupControl lookup) {
        is_lookup = false;
        theEC = lookup;
    }

    @Override
    @SuppressWarnings("UseOfObsoleteCollectionType")
    public java.util.Vector listData(String str) {
        if (str == null || str.equals("") || str.equals("%")) {
            return null;
        } else {
            return theLookup.listIcd10ByGroup(str, "", false, true);
        }
    }

    @Override
    public CommonInf readHosData(String str) {
        return theLookup.readICD10ById(str);
    }

    @Override
    public boolean execute(Object str) {
        theEC.addICD10GSSpecifyCode((ICD10) str);
        return true;
    }
}

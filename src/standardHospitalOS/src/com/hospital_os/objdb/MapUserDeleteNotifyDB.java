/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.Employee;
import com.hospital_os.object.MapUserDeleteNotify;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.ResultSet;
import java.util.List;
import java.util.Vector;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class MapUserDeleteNotifyDB {

    private final ConnectionInf theConnectionInf;
    private final String tableId = "825";
    private final EmployeeDB employeeDB;

    public MapUserDeleteNotifyDB(ConnectionInf db) {
        theConnectionInf = db;
        employeeDB = new EmployeeDB(db);
    }

    public int insert(MapUserDeleteNotify obj) throws Exception {
        String sql = "INSERT INTO b_map_user_delete_notify( "
                + "b_map_user_delete_notify_id, b_employee_id) "
                + "VALUES ('%s', '%s')";
        sql = String.format(sql, obj.getGenID(tableId), obj.b_employee_id);
        return theConnectionInf.eUpdate(sql);
    }

    public int delete(MapUserDeleteNotify itemDrugMapG6PD) throws Exception {
        String sql = "delete from b_map_user_delete_notify where b_map_user_delete_notify_id = '%s'";
        return theConnectionInf.eUpdate(String.format(sql, itemDrugMapG6PD.getObjectId()));
    }

    public MapUserDeleteNotify selectById(String id) throws Exception {
        String sql = "select * from b_map_user_delete_notify where b_map_user_delete_notify_id = '%s'";
        Vector eQuery = eQuery(String.format(sql, id));
        return (MapUserDeleteNotify) (eQuery.isEmpty() ? null : eQuery.get(0));
    }

    public MapUserDeleteNotify selectByItemId(String id) throws Exception {
        String sql = "select * from b_map_user_delete_notify where b_employee_id = '%s'";
        Vector eQuery = eQuery(String.format(sql, id));
        return (MapUserDeleteNotify) (eQuery.isEmpty() ? null : eQuery.get(0));
    }

    public Vector<MapUserDeleteNotify> listAll() throws Exception {
        String sql = "select * from b_map_user_delete_notify";
        Vector eQuery = eQuery(sql);
        return eQuery;
    }

    public Vector eQuery(String sql) throws Exception {
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        while (rs.next()) {
            MapUserDeleteNotify p = new MapUserDeleteNotify();
            p.setObjectId(rs.getString("b_map_user_delete_notify_id"));
            p.b_employee_id = rs.getString("b_employee_id");
            list.add(p);
        }
        rs.close();
        return list;
    }

    public List<Object[]> listByKeyword(String keyword) throws Exception {
        String sql = "select b_map_user_delete_notify.b_map_user_delete_notify_id"
                + ", case when f_patient_prefix.f_patient_prefix_id is not null then \n"
                + "case when f_patient_prefix.f_patient_prefix_id = '000' then '' \n"
                + "else f_patient_prefix.patient_prefix_description end else '' end\n"
                + "|| t_person.person_firstname || ' ' || t_person.person_lastname as personname "
                + "from b_map_user_delete_notify "
                + "inner join b_employee on b_map_user_delete_notify.b_employee_id = b_employee.b_employee_id and employee_active = '1' "
                + "inner join t_person on t_person.t_person_id = b_employee.t_person_id "
                + "left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_person.f_prefix_id "
                + "where UPPER(t_person.person_firstname) like UPPER('%s') "
                + "or UPPER(t_person.person_lastname) like UPPER('%s') "
                + "order by t_person.person_firstname, t_person.person_lastname";
        return theConnectionInf.eComplexQuery(String.format(sql,
                keyword == null || keyword.isEmpty() ? "%" : ("%" + keyword + "%"),
                keyword == null || keyword.isEmpty() ? "%" : ("%" + keyword + "%")));
    }

    public List<Object[]> listNotMapByKeyword(String keyword) throws Exception {
        String sql = "select b_employee.b_employee_id"
                + ", case when f_patient_prefix.f_patient_prefix_id is not null then \n"
                + "case when f_patient_prefix.f_patient_prefix_id = '000' then '' \n"
                + "else f_patient_prefix.patient_prefix_description end else '' end\n"
                + "|| t_person.person_firstname || ' ' || t_person.person_lastname as personname "
                + "from b_employee "
                + "inner join t_person on t_person.t_person_id = b_employee.t_person_id "
                + "left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_person.f_prefix_id "
                + "where b_employee.employee_active = '1' "
                + "and b_employee.b_employee_id not in (select b_employee_id from b_map_user_delete_notify) "
                + "and (UPPER(t_person.person_firstname) like UPPER('%s') "
                + "or UPPER(t_person.person_lastname) like UPPER('%s'))  "
                + "order by t_person.person_firstname, t_person.person_lastname";
        return theConnectionInf.eComplexQuery(String.format(sql,
                keyword == null || keyword.isEmpty() ? "%" : ("%" + keyword + "%"),
                keyword == null || keyword.isEmpty() ? "%" : ("%" + keyword + "%")));
    }

    public Vector<Employee> listEmployee() throws Exception {
        String sql = "select b_employee.* \n"
                + ", t_person.person_firstname\n"
                + ", t_person.person_lastname\n"
                + ", case when f_patient_prefix.f_patient_prefix_id is not null then \n"
                + "case when f_patient_prefix.f_patient_prefix_id = '000' then '' \n"
                + "else f_patient_prefix.patient_prefix_description end else '' end as prefix\n"
                + "from b_map_user_delete_notify \n"
                + "inner join b_employee on b_map_user_delete_notify.b_employee_id = b_employee.b_employee_id and employee_active = '1' \n"
                + "inner join t_person on t_person.t_person_id = b_employee.t_person_id\n"
                + "left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_person.f_prefix_id\n"
                + "order by t_person.person_firstname, t_person.person_lastname";
        return employeeDB.eQuery(sql);
    }
}
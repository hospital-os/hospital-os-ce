/*
 * TextFieldRultLab.java
 *
 * Created on 28 �ѹ��¹ 2548, 16:46 �.
 */
package com.hosv3.gui.component;

import com.hospital_os.usecase.connection.UpdateStatus;
import com.hospital_os.utility.Constant;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.logging.Logger;
import javax.swing.JTextField;

/**
 *
 * @author kingland
 */
public class TextFieldResultLab extends JTextField implements FocusListener, KeyListener {

    private static final long serialVersionUID = 1L;
    /**
     * Creates a new instance of TextFieldRultLab
     */
    private Float max = null;
    private Float min = null;
    private Float maxCV = null;
    private Float minCV = null;
    private int mode = 0;
    private UpdateStatus theUS;
    // Somprasong 20101012
    private String flag = "";
    private String[] defaultValues = null;

    @SuppressWarnings("LeakingThisInConstructor")
    public TextFieldResultLab(UpdateStatus us) {
        this.theUS = us;
        this.setRequestFocusEnabled(true);
        this.addKeyListener(this);
    }

    @SuppressWarnings("LeakingThisInConstructor")
    public TextFieldResultLab(int mode, UpdateStatus us) {
        this.theUS = us;
        this.mode = mode;
        this.setRequestFocusEnabled(true);
        this.addKeyListener(this);
    }

    /**
     * ����觤�� Flag 㹡óշ�� ����¡�õ�Ǩ��Ф�һ����繵���Ţ ���ա���觤��
     * Flag �繴ѧ��� "||" ����դ�����͡�ҡ���� Flag
     * �����ҧ㹡óշ��š�õ�Ǩ�������º�Ѻ ��һ��Ԣͧ LIS �繤�һ��� ��
     * ��һ��� "10 - 20" �š�õ�Ǩ "15" "N" ����� Flag
     * 㹡óշ��š�õ�Ǩ�������º�Ѻ ��һ��Ԣͧ LIS �繤�һ��� �� ��һ���
     * "Negative" �š�õ�Ǩ "Negative" ���� "< 20" �š�õ�Ǩ "15" "L" �����
     * Flag 㹡óշ��š�õ�Ǩ�������º�Ѻ ��һ��Ԣͧ LIS �繤�һ��� ��
     * ��һ��� "10 - 20" �š�õ�Ǩ "5" ���� ��һ��� "> 20" �š�õ�Ǩ "5" "H"
     * ����� Flag 㹡óշ��š�õ�Ǩ�������º�Ѻ ��һ��Ԣͧ LIS �繤�һ���
     * �� ��һ��� "10 - 20" �š�õ�Ǩ "25" ���� ��һ��� "< 20" �š�õ�Ǩ "25"
     * "AA" ����� Flag 㹡óշ��š�õ�Ǩ�������º�Ѻ ��һ��Ԣͧ LIS
     * �繤�һ��� �� ��һ��� "Negative" �š�õ�Ǩ "Trace" ���� ��һ���
     * "Negative" �š�õ�Ǩ "Positive" ��觤�� Flag "AA" @return
     */
    public boolean isAbnormalValue() {
        try {
            boolean normal = (flag == null || flag.equals("null") || flag.isEmpty()) ? true : (flag.equalsIgnoreCase("n") ? true : false);
            if (normal && (flag == null || flag.equals("null") || flag.isEmpty())) {
                // �� default values Ẻ texts
                if (defaultValues != null) {
                    for (String string : defaultValues) {
                        if (string.equalsIgnoreCase(getText().trim())) {
                            return false;
                        }
                    }
                    return true;
                } else { // �դ�� normal range
                    float value = Float.parseFloat(getText().trim());
                    if (max == null || min == null) {
                        return false;
                    }
                    if (value > max || value < min) {
                        return true;
                    }
                    return false;
                }
            } else if (normal) {
                return false;
            } else {
                return true;
            }
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean isCriticalValue() {
        try {
            boolean normal = (flag == null || flag.equals("null") || flag.isEmpty()) ? true : (flag.equalsIgnoreCase("n") ? true : false);
            if (normal && (flag == null || flag.equals("null") || flag.isEmpty())) {
                // �� default values Ẻ texts
                if (defaultValues != null) {
                    for (String string : defaultValues) {
                        if (string.equalsIgnoreCase(getText().trim())) {
                            return false;
                        }
                    }
                    return true;
                } else { // �դ�� normal range
                    float value = Float.parseFloat(getText().trim());
                    if (maxCV == null && minCV == null) {
                        return false;
                    }
                    if ((maxCV != null && value >= maxCV) || (minCV != null && value <= minCV)) {
                        return true;
                    }
                    return false;
                }
            } else if (normal) {
                return false;
            } else {
                return true;
            }
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean isNormalValue() {
        try {
            boolean normal = (flag.equalsIgnoreCase("n") ? true : false);
            if (normal) {
                return true;
            } else {
                float value = Float.parseFloat(getText());
                if (value <= max && value >= min) {
                    return true;
                }
                return false;
            }
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            return false;
        }
    }
    ///////////////////////////////////////////////////////////////////////////

    public void setMax(String max) {
        if (max != null && !max.isEmpty()) {
            try {
                this.max = Float.parseFloat(max);
            } catch (Exception e) {
            }
        }
    }

    public void setMin(String min) {
        if (min != null && !min.isEmpty()) {
            try {
                this.min = Float.parseFloat(min);
            } catch (Exception ex) {
            }
        }
    }

    public void setMaxCV(String maxCV) {
        if (maxCV != null && !maxCV.isEmpty()) {
            try {
                this.maxCV = Float.parseFloat(maxCV);
            } catch (Exception e) {
            }
        }
    }

    public void setMinCV(String minCV) {
        if (minCV != null && !minCV.isEmpty()) {
            try {
                this.minCV = Float.parseFloat(minCV);
            } catch (Exception ex) {
            }
        }
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    @Override
    public void focusGained(FocusEvent e) {
        String str = this.getText();
        try {
            if (!str.isEmpty()) {
                float value = Float.parseFloat(str);
                if (value > max || value < min) {
                    this.theUS.setStatus(Constant.getTextBundle("��һ��Ԣͧ���Ż�����������ҧ")
                            + " " + min + " - " + max, UpdateStatus.WARNING);
                    this.requestFocus();
                }
            }
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            this.theUS.setStatus("��һ��Ե�ͧ�繵���Ţ���� ���� ��ҷȹ���", UpdateStatus.WARNING);
            this.requestFocus();
        }
    }

    @Override
    public void focusLost(FocusEvent e) {
        String str = this.getText();
        try {
            if (!str.isEmpty()) {
                float value = Float.parseFloat(str);
                if (value > max || value < min) {
                    this.theUS.setStatus(Constant.getTextBundle("��һ��Ԣͧ���Ż�����������ҧ")
                            + " " + min + " - " + max, UpdateStatus.WARNING);
                    this.requestFocus();
                }
            }
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            this.theUS.setStatus("��һ��Ե�ͧ�繵���Ţ���� ���� ��ҷȹ���", UpdateStatus.WARNING);
            //this.setText("");
            this.requestFocus();
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
        this.requestFocus();
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    public void check() {
        String str = this.getText();
        char[] c = str.toCharArray();
        if (mode == 1) {
        } else {
            for (int i = 0; i < c.length; i++) {
                if (c[i] == '.') {
                    this.theUS.setStatus("��سҡ�͡����Ţ�ӹǹ���", UpdateStatus.WARNING);
                    return;
                }
            }
        }

        try {
            if (!str.isEmpty()) {
                float value = Float.parseFloat(str);
                if (value > max || value < min) {
                    this.theUS.setStatus(Constant.getTextBundle("��һ��Ԣͧ���Ż�����������ҧ")
                            + min
                            + " - "
                            + max, UpdateStatus.WARNING);
                    this.requestFocus();
                }
            }
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            this.theUS.setStatus("��һ��Ե�ͧ�繵���Ţ���� ���� ��ҷȹ���", UpdateStatus.WARNING);
            this.requestFocus();
        }
    }

    public void setDefaultValues(String[] defaultValues) {
        this.defaultValues = defaultValues;
    }
    private static final Logger LOG = Logger.getLogger(TextFieldResultLab.class.getName());
}

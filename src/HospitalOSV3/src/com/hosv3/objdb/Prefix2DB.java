package com.hosv3.objdb;

import com.hospital_os.objdb.PrefixDB;
import com.hospital_os.object.Prefix;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.Gutil;
import com.hosv3.object.Prefix2;
import java.sql.ResultSet;
import java.util.Vector;

@SuppressWarnings("UseOfObsoleteCollectionType")
public class Prefix2DB extends PrefixDB {

    public Prefix2DB(ConnectionInf db) {
        super(db);
    }

    //////////////////////////////////////////////////////////////////////////////
    /**
     * @deprecated henbe unused
     *
     */
    @Override
    public Vector selectTlcok2() throws Exception {
        String sql = "select * from " + dbObj.table
                + " where " + dbObj.tlock
                + " = 2 order by " + dbObj.description;
        Vector vc = veQuery(sql);
        if (vc.isEmpty()) {
            return null;
        } else {
            return vc;
        }
    }

    /**
     * ��㹡���Ҥӹ�˹�ҵ��ҧ active ����к��� 1 ���੾�з�� active ����� 0
     * ���੾�з����� active ��Ҩ�������� ������͡�� 2
     *
     * @param active �� String
     * @return �� Vector
     * @author padungrat(tong) @date 13/03/49,15:50
     */
    //////////////////////////////////////////////////////////////////////////////
    @Override
    public Vector selectAll(String active) throws Exception {
        String sql = "select * from " + dbObj.table;
        if (active != null && (active.equals("1") || active.equals("0"))) {
            sql = sql + " where " + dbObj.active + "='" + active.trim() + "'";
        }
        sql = sql + " order by " + dbObj.pk_field;
        Vector vc = eQuery(sql);
        if (vc.isEmpty()) {
            return null;
        } else {
            return vc;
        }
    }
    //////////////////////////////////////////////////////////////////////////////

    @Override
    public Vector eQuery(String sql) throws Exception {
        Prefix2 p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);

        while (rs.next()) {
            p = new Prefix2();
            p.setObjectId(rs.getString(dbObj.pk_field));
            p.description = rs.getString(dbObj.description);
            p.sex = rs.getString(dbObj.sex);
            p.tlock = rs.getString(dbObj.tlock);
            p.active = rs.getString(dbObj.active);
            p.prefix_id53 = rs.getString(dbObj.prefix_id53);
            list.add(p);
        }
        rs.close();
        return list;
    }
    ////////////////////////////////////////////////////////////////////////////

    public Vector selectByCN(String key) throws Exception {
        key = Gutil.CheckReservedWords(key);
        String sql = "select * from " + dbObj.table
                + " where (UPPER(" + dbObj.pk_field + ") like UPPER('%" + key + "%') "
                + " or UPPER(" + dbObj.description + ") like UPPER('%" + key + "%'))"
                + " and " + dbObj.active + " like '1' order by " + dbObj.pk_field;
        return eQuery(sql);
    }

    public String selectCount() throws Exception {
        String sql = "select count(" + dbObj.pk_field + ") as max_prefix_id"
                + " from " + dbObj.table;
        Vector vc = prefixQuery(sql);
        if (vc.isEmpty()) {
            return "0";
        } else {
            return ((Prefix) vc.get(0)).getObjectId();
        }
    }
}

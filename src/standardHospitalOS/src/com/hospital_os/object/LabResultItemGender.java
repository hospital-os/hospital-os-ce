/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class LabResultItemGender extends Persistent {

    public String b_item_lab_result_id = "";
    public String male_result_min = "";
    public String male_result_max = "";
    public String female_result_min = "";
    public String female_result_max = "";
    public String male_result_critical_min = "";
    public String male_result_critical_max = "";
    public String female_result_critical_min = "";
    public String female_result_critical_max = "";
}

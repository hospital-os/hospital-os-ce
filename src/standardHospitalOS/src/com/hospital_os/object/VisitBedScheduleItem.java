/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;
import java.util.Date;

/**
 *
 * @author Somprasong
 */
public class VisitBedScheduleItem extends Persistent {

    public String t_visit_id = "";
    public Date schedule_item_date_time;
    public Date record_date_time = new Date();
    public Date last_order_date_time;
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.control.lookup;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.LookupControlInf;
import com.hosv3.control.LookupControl;
import java.util.Vector;

/**
 *
 * @author tanakrit
 */
public class Hl7ImmunizationRouteLookup implements LookupControlInf {

    private final LookupControl theLookupControl;

    public Hl7ImmunizationRouteLookup(LookupControl theLookupControl) {
        this.theLookupControl = theLookupControl;
    }

    @Override
    public CommonInf readHosData(String pk) {
        return null;
    }

    @Override
    public Vector listData(String str) {
        return new Vector(theLookupControl.listHl7ImmunizationRoute(str));
    }

}

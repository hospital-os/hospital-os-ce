/*
 * PercentTextField.java
 *
 * Created on 14 �ԧ�Ҥ� 2549, 14:54 �.
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
package com.hospital_os.utility;

import java.awt.event.*;
import java.text.*;
import javax.swing.*;
import javax.swing.text.*;

/**
 *
 * @author Administrator
 */
@SuppressWarnings("ClassWithoutLogger")
public class PercentTextField extends JTextField implements KeyListener, FocusListener {

    private static final long serialVersionUID = 1L;
    public NumberFormat numberFormat = NumberFormat.getInstance();
    boolean noDot = true;

    /**
     * Creates a new instance of PercentTextField
     */
    @SuppressWarnings("LeakingThisInConstructor")
    public PercentTextField() {
        setDocument(new PercentDocument());
        addKeyListener(this);
        addFocusListener(this);
        numberFormat.setMaximumFractionDigits(3);
        numberFormat.setGroupingUsed(false);
    }

    protected class PercentDocument extends PlainDocument {

        private static final long serialVersionUID = 1L;
        String text = "";

        @Override
        public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
            noDot = true;
            text = super.getText(0, super.getLength());
            for (int i = 0; i < (text.length()); i++) {
                if (text.charAt(i) == '.') {
                    noDot = false;
                }
            }
            if (str != null) {
                char[] source = str.toCharArray();
                char[] result = new char[source.length];
                int j = 0;
                for (int i = 0; i < result.length; i++) {
                    if (Character.isDigit(source[i]) || (source[i] == '.')) {
                        if (source[i] != '.') {
                            result[j++] = source[i];
                        } else if (source[i] == '.' && noDot) {
                            result[j++] = source[i];
                            noDot = false;
                        }

                    } else {
                    }
                }
                if (offs >= 2 && Float.parseFloat(text + str) <= 100) {
                    super.insertString(offs, new String(result, 0, j), a);
                } else if (offs < 2) {
                    super.insertString(offs, new String(result, 0, j), a);
                }
            }
        }

        @Override
        public void remove(int offs, int len) throws BadLocationException {
            super.remove(offs, len);
        }
    }

    @Override
    public void focusGained(FocusEvent focusEvent) {
        setSelectionStart(0);
    }

    @Override
    public void focusLost(FocusEvent focusEvent) {
        this.getText();
    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
        try {
            if (keyEvent.getKeyCode() != KeyEvent.VK_UP && keyEvent.getKeyCode() != KeyEvent.VK_DOWN
                    && keyEvent.getKeyCode() != KeyEvent.VK_LEFT && keyEvent.getKeyCode() != KeyEvent.VK_RIGHT
                    && keyEvent.getKeyCode() != KeyEvent.VK_TAB && keyEvent.getKeyCode() != KeyEvent.VK_SHIFT) {
                numberFormat.parse(getText());
            }
        } catch (Exception ex) {
        }
    }

    @Override
    public void keyTyped(KeyEvent keyEvent) {
    }

    @Override
    public String getText() {
        String retValue;
        retValue = super.getText();
        for (int i = 0; retValue != null && retValue.length() > 0 && i < 1; i++) {
            if (retValue.charAt(i) == '.') {
                super.setText("0" + retValue);
            }
        }
        return super.getText();
    }
}

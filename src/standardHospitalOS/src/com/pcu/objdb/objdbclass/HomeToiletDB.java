/*
 * HomeToiletDB.java
 *
 * Created on 13 �Զع�¹ 2548, 15:17 �.
 */
package com.pcu.objdb.objdbclass;

import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.ComboFix;
import com.pcu.object.HomeToilet;
import java.sql.ResultSet;
import java.util.Vector;

/**
 *
 * @author Administrator
 */
@SuppressWarnings({"ClassWithoutLogger", "UseOfObsoleteCollectionType"})
public class HomeToiletDB {

    /**
     * Creates a new instance of HomeToiletDB
     */
    public HomeToiletDB() {
    }
    public ConnectionInf theConnectionInf;
    public HomeToilet dbObj;

    public HomeToiletDB(ConnectionInf db) {
        theConnectionInf = db;
        dbObj = new HomeToilet();
        initConfig();
    }

    public boolean initConfig() {
        dbObj.table = "f_health_home_toilet";
        dbObj.pk_field = "f_health_home_toilet_id";
        dbObj.description = "health_home_toilet_description";

        return true;
    }

    public Vector selectHomeToilet() throws Exception {
        Vector vHomeToilet = new Vector();

        String sql = "select * from "
                + dbObj.table;

        vHomeToilet = veQuery(sql);

        if (vHomeToilet.isEmpty()) {
            return null;
        } else {
            return vHomeToilet;
        }
    }

    public Vector eQuery(String sql) throws Exception {
        HomeToilet p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            p = new HomeToilet();
            p.setObjectId(rs.getString(dbObj.pk_field));
            p.description = rs.getString(dbObj.description);
            list.add(p);
        }
        rs.close();
        return list;
    }

    public Vector veQuery(String sql) throws Exception {
        ComboFix p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            p = new ComboFix();
            p.code = rs.getString(dbObj.pk_field);
            p.name = rs.getString(dbObj.description);
            list.add(p);
        }
        rs.close();
        return list;
    }
}

ALTER TABLE t_receipt_detail ADD COLUMN user_record character varying(255) NOT NULL  default '';
ALTER TABLE t_receipt_detail ADD COLUMN  record_date_time character varying(19) NOT NULL  default '';
ALTER TABLE t_receipt_detail ADD COLUMN  user_modify character varying(255) NOT NULL  default '';
ALTER TABLE t_receipt_detail ADD COLUMN  modify_date_time character varying(19) NOT NULL  default '';
ALTER TABLE t_receipt_detail ADD COLUMN  backup_first_book_number character varying(255) NOT NULL  default '';
ALTER TABLE t_receipt_detail ADD COLUMN  backup_first_print_receipt_number character varying(255) NOT NULL  default '';
ALTER TABLE t_receipt_detail ADD COLUMN  modify_counter integer NOT NULL  default 0;

INSERT INTO s_sph_version VALUES ('shp101000000004', '04', 'Support HOS, Community Edition', '1.2.260712', '1.3.260712', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('Support HOS for Niranam Clinic','update2.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'),'เพิ่ม column database ของ Module Suport Hos');



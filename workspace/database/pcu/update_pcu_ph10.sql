-- #462
-- ประเภทผลตรวจที่ใช้ยืนยัน
CREATE TABLE IF NOT EXISTS f_epidem_covid_lab_confirm_type (
    f_epidem_covid_lab_confirm_type_id   INTEGER NOT NULL,
    epidem_covid_lab_confirm_type_name   CHARACTER VARYING(255) NOT NULL,
CONSTRAINT f_epidem_covid_lab_confirm_type_pk PRIMARY KEY (f_epidem_covid_lab_confirm_type_id));

BEGIN;
INSERT INTO f_epidem_covid_lab_confirm_type SELECT 1,'RT-PCR'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_covid_lab_confirm_type WHERE f_epidem_covid_lab_confirm_type_id = 1); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_covid_lab_confirm_type SELECT 2,'Antigen'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_covid_lab_confirm_type WHERE f_epidem_covid_lab_confirm_type_id = 2); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_covid_lab_confirm_type SELECT 3,'Antibody'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_covid_lab_confirm_type WHERE f_epidem_covid_lab_confirm_type_id = 3); 
COMMIT;

-- จับคู่การตรวจที่ใช้ยืนยัน
CREATE TABLE IF NOT EXISTS b_map_epidem_lab_type (
    b_map_epidem_lab_type_id            CHARACTER VARYING(50) NOT NULL,
    b_item_id                           CHARACTER VARYING(50) NOT NULL,
    f_epidem_covid_lab_confirm_type_id  INTEGER NOT NULL,
CONSTRAINT b_map_epidem_lab_type_pkey PRIMARY KEY (b_map_epidem_lab_type_id),
CONSTRAINT b_map_epidem_lab_type_unique UNIQUE (b_map_epidem_lab_type_id),
CONSTRAINT b_item_id_fk FOREIGN KEY (b_item_id) REFERENCES b_item (b_item_id) ON DELETE SET NULL,
CONSTRAINT f_epidem_covid_lab_confirm_type_id_fk FOREIGN KEY (f_epidem_covid_lab_confirm_type_id) 
    REFERENCES f_epidem_covid_lab_confirm_type (f_epidem_covid_lab_confirm_type_id) ON DELETE SET NULL);

-- เพิ่มกลุ่มรหัสโรคขึ้นต้นด้วย U
BEGIN;
INSERT INTO b_icd10_group_type SELECT '2250000000022','รหัสเพื่อวัตถุประสงค์พิเศษ','U00 - U99'
WHERE NOT EXISTS (SELECT 1 FROM b_icd10_group_type WHERE b_icd10_group_type_id = '2250000000022'); 
COMMIT;

-- ปรับแก้และเพิ่มกลุ่มโรค 506
update f_group_rp506 set group_rp506_description_th = 'บิด ไม่ระะบุ [04]' where f_group_rp506_id = '3080000000004';
update f_group_rp506 set group_rp506_description_th = 'ไข้เอนเทอริค (ไข้รากสาดน้อยและไข้รากสาดเทียม) [07]' where f_group_rp506_id = '3080000000007';
update f_group_rp506 set group_rp506_description_th = 'ไข้รากสาดน้อย, ไข้ไทฟอยด์ [08]' where f_group_rp506_id = '3080000000008';
update f_group_rp506 set group_rp506_description_th = 'ไข้รากสาดเทียม, ไข้พาราไทฟอยด์ [09]' where f_group_rp506_id = '3080000000009';
update f_group_rp506 set group_rp506_description_th = 'ตับอักเสบ ไม่ระบุชนิด [10]' where f_group_rp506_id = '3080000000010';
update f_group_rp506 set group_rp506_description_th = 'โปลิโอมัยเอไลติส,ไข้ไขสันหลังอักเสบ [20]' where f_group_rp506_id = '3080000000020';
update f_group_rp506 set group_rp506_description_th = 'ไข้เลือดออกช็อค (DSS) [27]' where f_group_rp506_id = '3080000000029';
update f_group_rp506 set group_rp506_description_th = 'ไข้สมองอักเสบ จากเชื้อวรัา เจแปนนีส บี [29]' where f_group_rp506_id = '3080000000031';
update f_group_rp506 set group_rp506_description_th = 'มาลาเรีย - พบเชื้อมากกว่า 1 ชนิด,ไม่ทราบ หรือไม่ระบุ [30]' where f_group_rp506_id = '3080000000035';
update f_group_rp506 set group_rp506_description_th = 'พิษจากสารปิโตเลียม [50]' where f_group_rp506_id = '3080000000056';
update f_group_rp506 set group_rp506_description_th = 'พิษจากแก๊ส สารไอระเหย [51]' where f_group_rp506_id = '3080000000057';
update f_group_rp506 set group_rp506_description_th = 'ฆ่าตัวตาย [60]' where f_group_rp506_id = '3080000000061';
update f_group_rp506 set group_rp506_description_th = 'ฝีในตับ [61]' where f_group_rp506_id = '3080000000062';
update f_group_rp506 set group_rp506_description_th = 'มือ เท้า ปาก เปื่อย [71]' where f_group_rp506_id = '3080000000068';
update f_group_rp506 set group_rp506_description_th = 'ไข้ดําแดง [74]' where f_group_rp506_id = '3080000000071';
update f_group_rp506 set group_rp506_description_th = 'พยาธิใบไม้ในตับ [75]' where f_group_rp506_id = '3080000000072';
update f_group_rp506 set group_rp506_description_th = 'โรคเท้าช้าง [76]' where f_group_rp506_id = '3080000000073';
update f_group_rp506 set group_rp506_description_th = 'อาการไม่พึงประสงค์ภายหลังได้รับวัคซีน [78]' where f_group_rp506_id = '3080000000075';
update f_group_rp506 set group_rp506_description_th = 'เยื่อหุ้มสมองอักเสบ [54]' where f_group_rp506_id = '3080000000077';
update f_group_rp506 set group_rp506_description_th = 'เยื่อหุ้มสมองอักเสบ จากเชื้อพยาธิ [55]' where f_group_rp506_id = '3080000000078';
update f_group_rp506 set group_rp506_description_th = 'เรย์ซินโดรม [62]' where f_group_rp506_id = '3080000000081';
update f_group_rp506 set group_rp506_description_th = 'แคปิลลาเรีย [63]' where f_group_rp506_id = '3080000000082';
update f_group_rp506 set group_rp506_description_th = 'กล้ามเนื้ออัมพาตอ่อนปวกเปียกแบบเฉียบพลัน [65]' where f_group_rp506_id = '3080000000084';
update f_group_rp506 set group_rp506_description_th = 'โรคจากปัจจัยทางกายภาพ [67]' where f_group_rp506_id = '3080000000085';
update f_group_rp506 set group_rp506_description_th = 'พยาธิช่องคลอด [81.1]' where f_group_rp506_id = '3080000000088';
update f_group_rp506 set group_rp506_description_th = 'หูดข้าวสุกที่อวัยวะเพศ [81.3]' where f_group_rp506_id = '3080000000090';

BEGIN;
INSERT INTO f_group_rp506 SELECT '3080000000092','82','โรคติดเชื้อสเตร็ปโตคอคคัสซูอิส [82]','Streptococcus suis infection'
WHERE NOT EXISTS (SELECT 1 FROM f_group_rp506 WHERE f_group_rp506_id = '3080000000092'); 
COMMIT;
BEGIN;
INSERT INTO f_group_rp506 SELECT '3080000000093','83','โรคบรูเซลโลสิส [83]','Brucellosis'
WHERE NOT EXISTS (SELECT 1 FROM f_group_rp506 WHERE f_group_rp506_id = '3080000000093'); 
COMMIT;
BEGIN;
INSERT INTO f_group_rp506 SELECT '3080000000094','84','ไข้ชิคุนกุนยา [84]','Chikungunya fever'
WHERE NOT EXISTS (SELECT 1 FROM f_group_rp506 WHERE f_group_rp506_id = '3080000000094'); 
COMMIT;
BEGIN;
INSERT INTO f_group_rp506 SELECT '3080000000095','85','อาหารเป็นพิษจากโบทูลิซึม [85]','Botulism'
WHERE NOT EXISTS (SELECT 1 FROM f_group_rp506 WHERE f_group_rp506_id = '3080000000095'); 
COMMIT;
BEGIN;
INSERT INTO f_group_rp506 SELECT '3080000000096','86','พยาธิทริโคโมแนสของระบบสืบพันธุ์ และทางเดินปัสสาวะ [86]','Vaginal trichomoniasis'
WHERE NOT EXISTS (SELECT 1 FROM f_group_rp506 WHERE f_group_rp506_id = '3080000000096'); 
COMMIT;
BEGIN;
INSERT INTO f_group_rp506 SELECT '3080000000097','87','โรคติดเชื้อไวรัสซิกา [87]','Zika virus disease'
WHERE NOT EXISTS (SELECT 1 FROM f_group_rp506 WHERE f_group_rp506_id = '3080000000097'); 
COMMIT;
BEGIN;
INSERT INTO f_group_rp506 SELECT '3080000000098','88','โลนที่อวัยวะเพศ [88]','Pediculosis Pubis'
WHERE NOT EXISTS (SELECT 1 FROM f_group_rp506 WHERE f_group_rp506_id = '3080000000098'); 
COMMIT;
BEGIN;
INSERT INTO f_group_rp506 SELECT '3080000000099','89','หูดข้าวสุก [89]','Genital Molluscum Contagiosum'
WHERE NOT EXISTS (SELECT 1 FROM f_group_rp506 WHERE f_group_rp506_id = '3080000000099'); 
COMMIT;
BEGIN;
INSERT INTO f_group_rp506 SELECT '3080000000100','90','ไข้เอนเทอโรไวรัส [90]','Enterovirus Fever'
WHERE NOT EXISTS (SELECT 1 FROM f_group_rp506 WHERE f_group_rp506_id = '3080000000100'); 
COMMIT;
BEGIN;
INSERT INTO f_group_rp506 SELECT '3080000000101','91','ไข้หวัดนก [91]','Avian Influenza'
WHERE NOT EXISTS (SELECT 1 FROM f_group_rp506 WHERE f_group_rp506_id = '3080000000101'); 
COMMIT;
BEGIN;
INSERT INTO f_group_rp506 SELECT '3080000000102','92','โรคติดเชื้อไวรัสโคโรนา 2019 [92]','Coronavirus Disease 2019'
WHERE NOT EXISTS (SELECT 1 FROM f_group_rp506 WHERE f_group_rp506_id = '3080000000102'); 
COMMIT;

-- เพิ่มรายการกลุ่ม icd10 โรคติดเชื้อไวรัสโคโรนา 2019
BEGIN;
INSERT INTO b_group_icd10 
SELECT '310' || b_site.b_visit_office_id || substr(replace(((random())*((row_number() over())))::text,'.',''),1,10) as b_group_icd10_id
    ,'U07.1' as group_icd10_number
    ,'' as group_icd10_group_rp504
    ,'' as group_icd10_group_rp505
    ,'92' as group_icd10_group_rp506
    ,'' as group_icd10_b_group_chronic_id
    ,'' as group_icd10_other
    ,'' as f_group_rp504_id
    ,'' as f_group_rp505_id
    ,'3080000000102' as f_group_rp506_id
    ,'' as b_group_chronic_id
    ,'7830000000000' as b_health_disease_id
from b_site
WHERE NOT EXISTS (SELECT 1 FROM b_group_icd10 WHERE group_icd10_number = 'U07.1'); 
COMMIT;
BEGIN;
INSERT INTO b_group_icd10 
SELECT '310' || b_site.b_visit_office_id || substr(replace(((random())*((row_number() over())))::text,'.',''),1,10) as b_group_icd10_id
    ,'U07.2' as group_icd10_number
    ,'' as group_icd10_group_rp504
    ,'' as group_icd10_group_rp505
    ,'92' as group_icd10_group_rp506
    ,'' as group_icd10_b_group_chronic_id
    ,'' as group_icd10_other
    ,'' as f_group_rp504_id
    ,'' as f_group_rp505_id
    ,'3080000000102' as f_group_rp506_id
    ,'' as b_group_chronic_id
    ,'7830000000000' as b_health_disease_id
from b_site
WHERE NOT EXISTS (SELECT 1 FROM b_group_icd10 WHERE group_icd10_number = 'U07.2'); 
COMMIT;

-- ประเภทลักษณะที่พักอาศัย
CREATE TABLE IF NOT EXISTS f_epidem_covid_accm_type (
    f_epidem_covid_accm_type_id   INTEGER NOT NULL,
    epidem_covid_accm_type_name   CHARACTER VARYING(255) NOT NULL,
CONSTRAINT f_epidem_covid_accm_type_pk PRIMARY KEY (f_epidem_covid_accm_type_id));

BEGIN;
INSERT INTO f_epidem_covid_accm_type SELECT 1,'บ้านเดี่ยว'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_covid_accm_type WHERE f_epidem_covid_accm_type_id = 1); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_covid_accm_type SELECT 2,'ตึกแถว/ทาวน์เฮ้าส์'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_covid_accm_type WHERE f_epidem_covid_accm_type_id = 2); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_covid_accm_type SELECT 3,'หอพัก/คอนโด/ห้องเช่า'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_covid_accm_type WHERE f_epidem_covid_accm_type_id = 3); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_covid_accm_type SELECT 4,'พักห้องรวมกันคนจำนวนมาก'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_covid_accm_type WHERE f_epidem_covid_accm_type_id = 4); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_covid_accm_type SELECT 5,'อื่นๆ'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_covid_accm_type WHERE f_epidem_covid_accm_type_id = 5); 
COMMIT;

-- ประเภท cluster
CREATE TABLE IF NOT EXISTS f_epidem_covid_cluster_type (
    f_epidem_covid_cluster_type_id   INTEGER NOT NULL,
    epidem_covid_cluster_type_name   CHARACTER VARYING(255) NOT NULL,
CONSTRAINT f_epidem_covid_cluster_type_pk PRIMARY KEY (f_epidem_covid_cluster_type_id));

BEGIN;
INSERT INTO f_epidem_covid_cluster_type SELECT 1,'งานแต่ง'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_covid_cluster_type WHERE f_epidem_covid_cluster_type_id = 1); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_covid_cluster_type SELECT 2,'สวนสาธารณะ'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_covid_cluster_type WHERE f_epidem_covid_cluster_type_id = 2); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_covid_cluster_type SELECT 3,'ห้างสรรพสินค้า'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_covid_cluster_type WHERE f_epidem_covid_cluster_type_id = 3); 
COMMIT;

-- ประเภทสถานที่ Isolate
CREATE TABLE IF NOT EXISTS f_epidem_covid_isolate_place (
    f_epidem_covid_isolate_place_id   INTEGER NOT NULL,
    epidem_covid_isolate_place_name   CHARACTER VARYING(255) NOT NULL,
CONSTRAINT f_epidem_covid_isolate_place_pk PRIMARY KEY (f_epidem_covid_isolate_place_id));

BEGIN;
INSERT INTO f_epidem_covid_isolate_place SELECT 1,'โรงพยาบาล'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_covid_isolate_place WHERE f_epidem_covid_isolate_place_id = 1); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_covid_isolate_place SELECT 2,'โรงพยาบาลสนาม'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_covid_isolate_place WHERE f_epidem_covid_isolate_place_id = 2); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_covid_isolate_place SELECT 3,'Hospitel'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_covid_isolate_place WHERE f_epidem_covid_isolate_place_id = 3); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_covid_isolate_place SELECT 4,'แยกกักในชุมชน (CI)'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_covid_isolate_place WHERE f_epidem_covid_isolate_place_id = 4); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_covid_isolate_place SELECT 5,'ที่พักอาศัย (HI)'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_covid_isolate_place WHERE f_epidem_covid_isolate_place_id = 5); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_covid_isolate_place SELECT 6,'อื่นๆ'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_covid_isolate_place WHERE f_epidem_covid_isolate_place_id = 6); 
COMMIT;

-- ประเภทเหตุผลของการตรวจ
CREATE TABLE IF NOT EXISTS f_epidem_covid_reason_type (
    f_epidem_covid_reason_type_id   INTEGER NOT NULL,
    epidem_covid_reason_type_name   CHARACTER VARYING(255) NOT NULL,
CONSTRAINT f_epidem_covid_reason_type_pk PRIMARY KEY (f_epidem_covid_reason_type_id));

BEGIN;
INSERT INTO f_epidem_covid_reason_type SELECT 1,'เข้าเกณฑ์ PUI (ยกเว้นกรณีสัมผัสผู้ติดเชื้อยืนยัน)'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_covid_reason_type WHERE f_epidem_covid_reason_type_id = 1); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_covid_reason_type SELECT 2,'มาตรวจด้วยประวัติสัมผัสผู้ติดเชื้อในครอบครัว/ร่วมบ้าน'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_covid_reason_type WHERE f_epidem_covid_reason_type_id = 2); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_covid_reason_type SELECT 3,'มาตรวจด้วยประวัติสัมผัสผู้ติดเชื้อนอกครอบครัว/ที่ทำงาน/ชุมชน'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_covid_reason_type WHERE f_epidem_covid_reason_type_id = 3); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_covid_reason_type SELECT 4,'ขอตรวจเอง (เข้าทำงาน/ผลตรวจ ATK / เดินทางไปต่างประเทศ / กังวัล)'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_covid_reason_type WHERE f_epidem_covid_reason_type_id = 4); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_covid_reason_type SELECT 5,'Active case finding /Contact tracing (ค้นหาผู้สัมผัสในครอบครัว/ที่ทำงาน/ชุมชน)'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_covid_reason_type WHERE f_epidem_covid_reason_type_id = 5); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_covid_reason_type SELECT 6,'สุ่มตรวจเชิงรุก/คัดกรอง ในพื้นที่ที่ไม่เคยมีรายงานการติดเชื้อ/สถานที่เสี่ยง นอกสถานพยาบาล'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_covid_reason_type WHERE f_epidem_covid_reason_type_id = 6); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_covid_reason_type SELECT 7,'Sentinel surveillance'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_covid_reason_type WHERE f_epidem_covid_reason_type_id = 7); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_covid_reason_type SELECT 8,'ตรวจก่อนทำหัตการ / Admit'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_covid_reason_type WHERE f_epidem_covid_reason_type_id = 8); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_covid_reason_type SELECT 9,'ให้ตรวจเพิ่มเติมโดยสถานพยาบาล (เจ้าหน้าที่/พนักงาน/ญาติผู้ป่วย)'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_covid_reason_type WHERE f_epidem_covid_reason_type_id = 9); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_covid_reason_type SELECT 10,'อื่นๆ'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_covid_reason_type WHERE f_epidem_covid_reason_type_id = 10); 
COMMIT;

-- ประเภทสถานที่เก็บตัวอย่างตรวจ
CREATE TABLE IF NOT EXISTS f_epidem_covid_spcm_place (
    f_epidem_covid_spcm_place_id   INTEGER NOT NULL,
    epidem_covid_spcm_place_name   CHARACTER VARYING(255) NOT NULL,
CONSTRAINT f_epidem_covid_spcm_place_pk PRIMARY KEY (f_epidem_covid_spcm_place_id));

BEGIN;
INSERT INTO f_epidem_covid_spcm_place SELECT 1,'สถานพยาบาล (โรงพยาบาล/คลินิก)'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_covid_spcm_place WHERE f_epidem_covid_spcm_place_id = 1); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_covid_spcm_place SELECT 2,'ศูนย์ตรวจทางห้องปฏิบัติการเอกชน'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_covid_spcm_place WHERE f_epidem_covid_spcm_place_id = 2); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_covid_spcm_place SELECT 3,'ชุมชน'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_covid_spcm_place WHERE f_epidem_covid_spcm_place_id = 3); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_covid_spcm_place SELECT 4,'ด่านควบคุมโรคระหว่างประเทศ'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_covid_spcm_place WHERE f_epidem_covid_spcm_place_id = 4); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_covid_spcm_place SELECT 5,'สถานกักกันที่รัฐกำหนด'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_covid_spcm_place WHERE f_epidem_covid_spcm_place_id = 5); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_covid_spcm_place SELECT 6,'ประชาชนตรวจด้วยตัวเอง'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_covid_spcm_place WHERE f_epidem_covid_spcm_place_id = 6); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_covid_spcm_place SELECT 7,'อื่นๆ'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_covid_spcm_place WHERE f_epidem_covid_spcm_place_id = 7); 
COMMIT;

-- ประเภทอาการที่แสดง
CREATE TABLE IF NOT EXISTS f_epidem_covid_symptom_type (
    f_epidem_covid_symptom_type_id   INTEGER NOT NULL,
    epidem_covid_symptom_type_name   CHARACTER VARYING(255) NOT NULL,
CONSTRAINT f_epidem_covid_symptom_type_pk PRIMARY KEY (f_epidem_covid_symptom_type_id));

BEGIN;
INSERT INTO f_epidem_covid_symptom_type SELECT 1,'ไม่มีอาการ'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_covid_symptom_type WHERE f_epidem_covid_symptom_type_id = 1); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_covid_symptom_type SELECT 2,'มีอาการแต่ไม่รุนแรง และไม่เกี่ยวกับระบบทางเดินหายใจ'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_covid_symptom_type WHERE f_epidem_covid_symptom_type_id = 2); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_covid_symptom_type SELECT 3,'มีอาการที่เกี่ยวกับระบบทางเดินหายใจ (ปอดบวม)'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_covid_symptom_type WHERE f_epidem_covid_symptom_type_id = 3); 
COMMIT;

-- ประเภทสถานะตอนรายงานโรค
CREATE TABLE IF NOT EXISTS f_epidem_person_status (
    f_epidem_person_status_id   INTEGER NOT NULL,
    epidem_person_status_name   CHARACTER VARYING(255) NOT NULL,
CONSTRAINT f_epidem_person_status_pk PRIMARY KEY (f_epidem_person_status_id));

BEGIN;
INSERT INTO f_epidem_person_status SELECT 1,'หาย'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_person_status WHERE f_epidem_person_status_id = 1); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_person_status SELECT 2,'ตาย'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_person_status WHERE f_epidem_person_status_id = 2); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_person_status SELECT 3,'ยังรักษาตัวอยู่'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_person_status WHERE f_epidem_person_status_id = 3); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_person_status SELECT 4,'ไม่ทราบ'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_person_status WHERE f_epidem_person_status_id = 4); 
COMMIT;

-- ประเภทประวัติเสี่ยงใน 14 วัน
CREATE TABLE IF NOT EXISTS f_epidem_risk_history_type (
    f_epidem_risk_history_type_id   INTEGER NOT NULL,
    epidem_risk_history_type_name   CHARACTER VARYING(255) NOT NULL,
CONSTRAINT f_epidem_risk_history_type_pk PRIMARY KEY (f_epidem_risk_history_type_id));

BEGIN;
INSERT INTO f_epidem_risk_history_type SELECT 1,'ทำงานในโรงพยาบาล/คลินิก'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_risk_history_type WHERE f_epidem_risk_history_type_id = 1); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_risk_history_type SELECT 2,'ทำงานในโรงงาน/สถานประกอบการ/ประมง/การเกษตร ที่มีพนักงานตั้งแต่ 50 คนขึ้นไป'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_risk_history_type WHERE f_epidem_risk_history_type_id = 2); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_risk_history_type SELECT 3,'ทำงานในตลาด/ไปตลาดเป็นประจำ'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_risk_history_type WHERE f_epidem_risk_history_type_id = 3); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_risk_history_type SELECT 4,'ทำงานในแคมป์ก่อสร้าง'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_risk_history_type WHERE f_epidem_risk_history_type_id = 4); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_risk_history_type SELECT 5,'ผู้ต้องขังในเรือนจำ/ทัณฑสถาน'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_risk_history_type WHERE f_epidem_risk_history_type_id = 5); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_risk_history_type SELECT 6,'อยู่ในศูนย์พักพิงชั่วคราว/ศูนย์กักกัน'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_risk_history_type WHERE f_epidem_risk_history_type_id = 6); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_risk_history_type SELECT 7,'มาจากต่างประเทศ (เข้าประเทศอย่างถูกกฎหมาย)'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_risk_history_type WHERE f_epidem_risk_history_type_id = 7); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_risk_history_type SELECT 8,'มาจากต่างประเทศ (ลักลอบเข้าประเทศ)'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_risk_history_type WHERE f_epidem_risk_history_type_id = 8); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_risk_history_type SELECT 9,'อยู่ในชุมชนแออัด'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_risk_history_type WHERE f_epidem_risk_history_type_id = 9); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_risk_history_type SELECT 10,'ทำงานในหน่วยงานราชการ/ สำนักงาน/ บริษัท ที่มีพนักงานตั้งแต่ 50 คนขึ้นไป'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_risk_history_type WHERE f_epidem_risk_history_type_id = 10); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_risk_history_type SELECT 11,'อยู่อาศัย หรือทำงานในศูนย์ดูแลผู้สูงอายุ/ สถานสงเคราะห์คนชราหรือดูแลผู้ป่วยโรคเรื้อรัง/ long-tern ca'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_risk_history_type WHERE f_epidem_risk_history_type_id = 11); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_risk_history_type SELECT 12,'ไปหรือทำงานที่โรงเรียน/ สถาบันศึกษา/ ศูนย์เด็กเล็ก'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_risk_history_type WHERE f_epidem_risk_history_type_id = 12); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_risk_history_type SELECT 13,'ประกอบอาชีพเสี่ยง (กลุ่มอาชีพที่ไม่สามารถทำงานจากที่บ้านได้ เช่น ค้าขาย รับจ้าง ขับรถสาธารณะ เป็นต้น'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_risk_history_type WHERE f_epidem_risk_history_type_id = 13); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_risk_history_type SELECT 14,'ไม่มีประวัติตามข้อ 1-13 ข้างต้น'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_risk_history_type WHERE f_epidem_risk_history_type_id = 14); 
COMMIT;

-- ประเภทสถานที่เสี่ยง
CREATE TABLE IF NOT EXISTS f_epidem_risk_type (
    f_epidem_risk_type_id   INTEGER NOT NULL,
    epidem_risk_type_name   CHARACTER VARYING(255) NOT NULL,
CONSTRAINT f_epidem_risk_type_pk PRIMARY KEY (f_epidem_risk_type_id));

BEGIN;
INSERT INTO f_epidem_risk_type SELECT 1,'สถานบันเทิง,ผับ'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_risk_type WHERE f_epidem_risk_type_id = 1); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_risk_type SELECT 2,'สนามมวย'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_risk_type WHERE f_epidem_risk_type_id = 2); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_risk_type SELECT 3,'โรงพยาบาล'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_risk_type WHERE f_epidem_risk_type_id = 3); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_risk_type SELECT 4,'ตลาดนัด'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_risk_type WHERE f_epidem_risk_type_id = 4); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_risk_type SELECT 5,'ห้างสรรพสินค้า'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_risk_type WHERE f_epidem_risk_type_id = 5); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_risk_type SELECT 6,'สถานีขนส่งผู้โดยสาร'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_risk_type WHERE f_epidem_risk_type_id = 6); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_risk_type SELECT 7,'สนามบิน'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_risk_type WHERE f_epidem_risk_type_id = 7); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_risk_type SELECT 8,'ชุมชนแออัด'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_risk_type WHERE f_epidem_risk_type_id = 8); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_risk_type SELECT 9,'โรงงาน'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_risk_type WHERE f_epidem_risk_type_id = 9); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_risk_type SELECT 10,'ศาสนสถาน'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_risk_type WHERE f_epidem_risk_type_id = 10); 
COMMIT;
BEGIN;
INSERT INTO f_epidem_risk_type SELECT 11,'อื่นๆ'
WHERE NOT EXISTS (SELECT 1 FROM f_epidem_risk_type WHERE f_epidem_risk_type_id = 11); 
COMMIT;

CREATE OR REPLACE FUNCTION cid_pattern(person_pid text)
RETURNS TEXT
 AS $$
    DECLARE
    BEGIN
        return case when length($1) = 13 and $1 is not null 
                    then substr($1,1,1) || '-' || substr($1,2,4) || '-' || substr($1,6,5) || '-' || substr($1,11,2) || '-' || substr($1,13,1) end;
        exception when others then
        return null;
END;
$$
LANGUAGE 'plpgsql';

-- ตารางข้อมูลรายงานผลระบาดวิทยา
CREATE TABLE IF NOT EXISTS t_health_epidem_report (
	t_health_epidem_report_id           CHARACTER VARYING(50) NOT NULL,
	t_visit_id                          CHARACTER VARYING(50) NOT NULL,
    report_datetime                     TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP, -- วันที่/เวลา ที่รายงานโรค
    user_report_id                      CHARACTER VARYING(255) NOT NULL, -- ผู้รายงาน
    onset_date                          DATE NOT NULL DEFAULT CURRENT_DATE, -- วันที่เริ่มมีอาการ
    treated_date                        DATE NOT NULL DEFAULT CURRENT_DATE, -- วันที่เริ่มรักษา
    treated_hospital_code               CHARACTER VARYING(5) NOT NULL, -- รหัสโรงพยาบาลที่กำลังรักษาตัว
    f_epidem_person_status_id           INTEGER, -- สถานะ ณ ตอนรายงาน
    f_epidem_covid_symptom_type_id      INTEGER, -- อาการที่แสดง
    pregnant_status                     BOOLEAN NOT NULL DEFAULT FALSE, -- สถานะการตั้งครรภ์
    respirator_status                   BOOLEAN NOT NULL DEFAULT FALSE, -- ใส่เครื่องช่วยหายใจ
    vaccinated_status                   BOOLEAN NOT NULL DEFAULT FALSE, -- มีประวัติการได้รับวัคซีน
    exposure_epidemic_area_status       BOOLEAN NOT NULL DEFAULT FALSE, -- เคยอาศัยอยู่หรือเดินทางมาจากพื้นที่มีการระบาด
    exposure_healthcare_worker_status   BOOLEAN NOT NULL DEFAULT FALSE, -- เป็นบุคลากรทางการแพทย์ ที่เกี่ยวข้องกับการรักษาผู้ป่วย
    exposure_closed_contact_status      BOOLEAN NOT NULL DEFAULT FALSE, -- ได้อยู่ใกล้ชิดกับผู้ที่ยืนยันว่าเป็นโรค Covid-19
    exposure_occupation_status          BOOLEAN NOT NULL DEFAULT FALSE, -- ทำงานในอาชีพที่สัมผัสใกล้ชิดกับนักท่องเที่ยวต่างชาติ หรือกลุ่มเสี่ยงสูง
    exposure_travel_status              BOOLEAN NOT NULL DEFAULT FALSE, -- ได้เดินทางหรือทำกิจกรรม ในสถานที่ๆ มีคนอยู่หนาแน่น
    f_epidem_risk_history_type_id       INTEGER, -- ประวัติเสี่ยง 14 วัน
    t_address_id                        CHARACTER VARYING(50) DEFAULT NULL, -- ที่อยู่ขณะสำรวจว่าเป็นโรค
    isolate_province_id                 CHARACTER VARYING(6) DEFAULT NULL, -- รหัสจังหวัดที่ isolate
    f_epidem_covid_isolate_place_id     INTEGER, -- สถานที่ isolate
    specimen_date                       DATE DEFAULT CURRENT_DATE, -- วันที่เก็บตัวอย่าง specimen
    f_epidem_covid_spcm_place_id        INTEGER, -- รหัสสถานที่เก็บตัวอย่าง
    f_epidem_covid_reason_type_id       INTEGER, -- รหัสเหตุผลการตรวจ
    f_epidem_covid_lab_confirm_type_id  INTEGER, -- รหัสผลการตรวจที่ยืนยัน
    b_item_id                           CHARACTER VARYING(50) DEFAULT NULL, -- รหัสรายการแล็บ
    lab_report_date                     DATE DEFAULT NULL, -- วันที่รายงานผลแล็บ
    lab_name                            CHARACTER VARYING(255) DEFAULT NULL, -- ชื่อแล็บ
    lab_value                           CHARACTER VARYING(255) DEFAULT NULL, -- ผลแล็บ
    active                              CHARACTER VARYING(1) NOT NULL DEFAULT '1', -- 1 = active, 0 = inactive
    record_datetime                     TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    user_record_id                      CHARACTER VARYING(255) NOT NULL,
    update_datetime                     TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    user_update_id                      CHARACTER VARYING(255) NOT NULL,
	cancel_datetime                     TIMESTAMP WITH TIME ZONE,
    user_cancel_id                      CHARACTER VARYING(255),
	epidem_report_guid                  UUID NOT NULL DEFAULT uuid_generate_v4(),
	CONSTRAINT t_health_epidem_report_pk PRIMARY KEY (t_health_epidem_report_id),
    CONSTRAINT f_epidem_person_status_id_fk FOREIGN KEY (f_epidem_person_status_id) 
        REFERENCES f_epidem_person_status (f_epidem_person_status_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT f_epidem_covid_symptom_type_id_fk FOREIGN KEY (f_epidem_covid_symptom_type_id) 
        REFERENCES f_epidem_covid_symptom_type (f_epidem_covid_symptom_type_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT f_epidem_risk_history_type_id_fk FOREIGN KEY (f_epidem_risk_history_type_id) 
        REFERENCES f_epidem_risk_history_type (f_epidem_risk_history_type_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT t_address_id_fk FOREIGN KEY (t_address_id) 
        REFERENCES t_address (t_address_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT isolate_province_id_fk FOREIGN KEY (isolate_province_id) 
        REFERENCES f_address (f_address_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT f_epidem_covid_isolate_place_id_fk FOREIGN KEY (f_epidem_covid_isolate_place_id) 
        REFERENCES f_epidem_covid_isolate_place (f_epidem_covid_isolate_place_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT f_epidem_covid_spcm_place_id_fk FOREIGN KEY (f_epidem_covid_spcm_place_id) 
        REFERENCES f_epidem_covid_spcm_place (f_epidem_covid_spcm_place_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT f_epidem_covid_reason_type_id_fk FOREIGN KEY (f_epidem_covid_reason_type_id) 
        REFERENCES f_epidem_covid_reason_type (f_epidem_covid_reason_type_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT f_epidem_covid_lab_confirm_type_id_fk FOREIGN KEY (f_epidem_covid_lab_confirm_type_id) 
        REFERENCES f_epidem_covid_lab_confirm_type (f_epidem_covid_lab_confirm_type_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT b_item_id_fk FOREIGN KEY (b_item_id) 
        REFERENCES b_item (b_item_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT user_report_id_fk FOREIGN KEY (user_report_id) 
        REFERENCES b_employee (b_employee_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT user_record_id_fk FOREIGN KEY (user_record_id) 
        REFERENCES b_employee (b_employee_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT user_update_id_fk FOREIGN KEY (user_update_id) 
        REFERENCES b_employee (b_employee_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT user_cancel_id_fk FOREIGN KEY (user_cancel_id) 
        REFERENCES b_employee (b_employee_id) ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE OR REPLACE FUNCTION moph_epidem_report_V4(epidem_report_id text)
RETURNS TABLE (epidem_query json) AS $$
    DECLARE
    BEGIN
return query 
select json_build_object(
        'hospital',jsonb_build_object(
            'hospital_code',epidem_report.hospital_code
            ,'hospital_name',epidem_report.hospital_name
            ,'his_identifier',epidem_report.his_identifier
            ),
        'person',jsonb_build_object(
            'cid',epidem_report.cid
            ,'passport_no',epidem_report.passport_no
            ,'prefix',epidem_report.prefix
            ,'first_name',epidem_report.first_name
            ,'last_name',epidem_report.last_name
            ,'nationality',epidem_report.nationality
            ,'gender',epidem_report.gender
            ,'birth_date',epidem_report.birth_date
            ,'age_y',epidem_report.age_y
            ,'age_m',epidem_report.age_m
            ,'age_d',epidem_report.age_d
            ,'marital_status_id',epidem_report.marital_status_id
            ,'address',epidem_report.address
            ,'moo',epidem_report.moo
            ,'road',epidem_report.road
            ,'chw_code',epidem_report.chw_code
            ,'amp_code',epidem_report.amp_code
            ,'tmb_code',epidem_report.tmb_code
            ,'mobile_phone',epidem_report.mobile_phone
            ,'occupation',epidem_report.occupation
            ),
        'epidem_report',jsonb_build_object(
            'epidem_report_guid',epidem_report.epidem_report_guid
            ,'epidem_report_group_id',epidem_report.epidem_report_group_id
            ,'treated_hospital_code',epidem_report.treated_hospital_code
            ,'report_datetime',epidem_report.report_datetime
            ,'onset_date',epidem_report.onset_date
            ,'treated_date',epidem_report.treated_date
            ,'diagnosis_date',epidem_report.diagnosis_date
            ,'informer_name',epidem_report.informer_name
            ,'principal_diagnosis_icd10',epidem_report.principal_diagnosis_icd10
            ,'diagnosis_icd10_list',epidem_report.diagnosis_icd10_list
            ,'epidem_person_status_id',epidem_report.epidem_person_status_id
            ,'epidem_symptom_type_id',epidem_report.epidem_symptom_type_id
            ,'pregnant_status',epidem_report.pregnant_status
            ,'respirator_status',epidem_report.respirator_status
            ,'epidem_accommodation_type_id',epidem_report.epidem_accommodation_type_id
            ,'vaccinated_status',epidem_report.vaccinated_status
            ,'exposure_epidemic_area_status',epidem_report.exposure_epidemic_area_status
            ,'exposure_healthcare_worker_status',epidem_report.exposure_healthcare_worker_status
            ,'exposure_closed_contact_status',epidem_report.exposure_closed_contact_status
            ,'exposure_occupation_status',epidem_report.exposure_occupation_status
            ,'exposure_travel_status',epidem_report.exposure_travel_status
            ,'risk_history_type_id',epidem_report.risk_history_type_id
            ,'epidem_address',epidem_report.epidem_address
            ,'epidem_moo',epidem_report.epidem_moo
            ,'epidem_road',epidem_report.epidem_road
            ,'epidem_chw_code',epidem_report.epidem_chw_code
            ,'epidem_amp_code',epidem_report.epidem_amp_code
            ,'epidem_tmb_code',epidem_report.epidem_tmb_code
            ,'location_gis_latitude',epidem_report.location_gis_latitude
            ,'location_gis_longitude',epidem_report.location_gis_longitude
            ,'isolate_chw_code',epidem_report.isolate_chw_code
            ,'isolate_place_id',epidem_report.isolate_place_id
            ,'patient_type',epidem_report.patient_type
            ),
        'lab_report',case when epidem_report.b_item_id IS NULL then '{}' 
                else jsonb_build_object(
            'epidem_lab_confirm_type_id',epidem_report.epidem_lab_confirm_type_id
            ,'lab_report_date',epidem_report.lab_report_date
            ,'lab_report_result',epidem_report.lab_report_result
            ,'specimen_date',epidem_report.specimen_date
            ,'specimen_place_id',epidem_report.specimen_place_id
            ,'tests_reason_type_id',epidem_report.tests_reason_type_id
            ,'lab_his_ref_code',epidem_report.lab_his_ref_code
            ,'lab_his_ref_name',epidem_report.lab_his_ref_name
            ,'tmlt_code',epidem_report.tmlt_code
            ) end) as epidem_report
from (select b_site.b_visit_office_id as hospital_code
        ,b_site.site_full_name as hospital_name
        ,'HospitalOS ' || (select version_app from s_version order by s_version.update_datetime desc limit 1) as his_identifier
        ,t_person.person_pid as cid
        ,t_person.person_passport as passport_no
        ,case when f_patient_prefix.patient_prefix_description is null then ''
              else f_patient_prefix.patient_prefix_description end as prefix
        ,t_person.person_firstname as first_name
        ,t_person.person_lastname as last_name
        ,f_patient_nation.r_rp1853_nation_id as nationality
        ,case when t_person.f_sex_id in ('1','2') then t_person.f_sex_id
              else '0' end::int as gender
        ,to_char(t_person.person_dob,'YYYY-MM-DD') as birth_date
        ,case when EXTRACT(YEAR FROM age(t_person.person_dob))::int IS NOT NULL and EXTRACT(YEAR FROM age(t_person.person_dob))::int > 0
              then EXTRACT(YEAR FROM age(t_person.person_dob))::int else 0 end as age_y
        ,case when EXTRACT(MONTH FROM age(t_person.person_dob))::int IS NOT NULL and EXTRACT(MONTH FROM age(t_person.person_dob))::int > 0
              then EXTRACT(MONTH FROM age(t_person.person_dob))::int else 0 end as age_m
        ,case when EXTRACT(DAY FROM age(t_person.person_dob))::int IS NOT NULL and EXTRACT(DAY FROM age(t_person.person_dob))::int > 0
              then EXTRACT(DAY FROM age(t_person.person_dob))::int else 0 end as age_d
        ,case when t_person.f_patient_marriage_status_id in ('1','2') then t_person.f_patient_marriage_status_id::int
              when t_person.f_patient_marriage_status_id in ('3','4') then '3'::int
              when t_person.f_patient_marriage_status_id = '5' then '4'::int
              else null end as marital_status_id
        ,case when person_address.house_no is not null then person_address.house_no
              else '' end as address
        ,case when person_address.village_no is not null then person_address.village_no
              else '' end as moo
        ,case when person_address.road is not null then person_address.road
              else '' end as road
        ,case when person_address.sub_district_id is not null then substr(person_address.sub_district_id,1,2)
              else '' end as chw_code
        ,case when person_address.sub_district_id is not null then substr(person_address.sub_district_id,3,2)
              else '' end as amp_code
        ,case when person_address.sub_district_id is not null then substr(person_address.sub_district_id,5,2)
              else '' end as tmb_code
        ,case when t_person_contact.f_contact_type_id = '2' then t_person_contact.contact_info
              else '' end as mobile_phone
        ,case when r_rp1855_occupation.code is not null then r_rp1855_occupation.code
              else '' end as occupation
        ,case when t_health_epidem_report.epidem_report_guid is not null then '{' || upper(t_health_epidem_report.epidem_report_guid::text) || '}'
                  else '' end epidem_report_guid
        ,92 as epidem_report_group_id
        ,t_health_epidem_report.treated_hospital_code
        ,to_char(t_health_epidem_report.report_datetime,'YYYY-MM-DDThh24:mi:ss.ms') as report_datetime
        ,to_char(t_health_epidem_report.onset_date,'YYYY-MM-DD') as onset_date
        ,to_char(t_health_epidem_report.treated_date,'YYYY-MM-DD') as treated_date
        ,case when icd10_primary.diagnosis_date is not null 
              then to_char(icd10_primary.diagnosis_date,'YYYY-MM-DD')
              else '' end as diagnosis_date
        ,case when employee_prefix.f_patient_prefix_id is not null and employee_prefix.f_patient_prefix_id <> '000'
              then employee_prefix.patient_prefix_description else '' end
         || employee_person.person_firstname || '  ' || employee_person.person_lastname as informer_name
        ,case when icd10_primary.icd10_code is not null then icd10_primary.icd10_code
              else '' end as principal_diagnosis_icd10
        ,case when icd10_list.icd10_code is not null then icd10_list.icd10_code
              else '' end as diagnosis_icd10_list
        ,t_health_epidem_report.f_epidem_person_status_id as epidem_person_status_id
        ,t_health_epidem_report.f_epidem_covid_symptom_type_id as epidem_symptom_type_id
        ,case when t_health_epidem_report.pregnant_status is true then 'Y'
              else 'N' end as pregnant_status
        ,case when t_health_epidem_report.respirator_status is true then 'Y'
              else 'N' end as respirator_status
        ,case when epidem_address.f_address_housetype_id in ('1','2') then epidem_address.f_address_housetype_id::int
              when epidem_address.f_address_housetype_id in ('3','4') then 3
              when epidem_address.f_address_housetype_id = '5' then 4
              when epidem_address.f_address_housetype_id = '8' then 5
              else null end as epidem_accommodation_type_id
        ,case when t_health_epidem_report.vaccinated_status is true then 'Y'
              else 'N' end as vaccinated_status
        ,case when t_health_epidem_report.exposure_epidemic_area_status is true then 'Y'
              else 'N' end as exposure_epidemic_area_status
        ,case when t_health_epidem_report.exposure_healthcare_worker_status is true then 'Y'
              else 'N' end as exposure_healthcare_worker_status
        ,case when t_health_epidem_report.exposure_closed_contact_status is true then 'Y'
              else 'N' end as exposure_closed_contact_status
        ,case when t_health_epidem_report.exposure_occupation_status is true then 'Y'
              else 'N' end as exposure_occupation_status
        ,case when t_health_epidem_report.exposure_travel_status is true then 'Y'
              else 'N' end as exposure_travel_status
        ,t_health_epidem_report.f_epidem_risk_history_type_id as risk_history_type_id
        ,case when epidem_address.house_no is not null then epidem_address.house_no
              else '' end as epidem_address
        ,case when epidem_address.village_no is not null then epidem_address.village_no
              else '' end as epidem_moo
        ,case when epidem_address.road is not null then epidem_address.road
              else '' end as epidem_road
        ,case when epidem_address.sub_district_id is not null then substr(epidem_address.sub_district_id,1,2)
              else '' end as epidem_chw_code
        ,case when epidem_address.sub_district_id is not null then substr(epidem_address.sub_district_id,3,2)
              else '' end as epidem_amp_code
        ,case when epidem_address.sub_district_id is not null then substr(epidem_address.sub_district_id,5,2)
              else '' end as epidem_tmb_code
        ,case when epidem_address.latitude is not null then epidem_address.latitude::float
              else 0 end as location_gis_latitude
        ,case when epidem_address.longitude is not null then epidem_address.longitude::float
              else 0 end as location_gis_longitude
        ,case when t_health_epidem_report.isolate_province_id is not null then substr(t_health_epidem_report.isolate_province_id,1,2)
              else '' end as isolate_chw_code
        ,t_health_epidem_report.f_epidem_covid_isolate_place_id as isolate_place_id
        ,case when t_visit.f_visit_type_id = '1' then 'IPD' else 'OPD' end as patient_type
        ,case when t_health_epidem_report.b_item_id is not null
              then t_health_epidem_report.f_epidem_covid_lab_confirm_type_id
              else null end as epidem_lab_confirm_type_id
        ,case when t_health_epidem_report.b_item_id is not null and t_health_epidem_report.lab_report_date is not null
              then to_char(t_health_epidem_report.lab_report_date,'YYYY-MM-DD')
              else null end as lab_report_date
        ,case when t_health_epidem_report.b_item_id is not null and t_health_epidem_report.lab_value is not null
              then t_health_epidem_report.lab_value
              else null end as lab_report_result
        ,case when t_health_epidem_report.b_item_id is not null and t_health_epidem_report.specimen_date is not null
              then to_char(t_health_epidem_report.specimen_date,'YYYY-MM-DD')
              else null end as specimen_date
        ,case when t_health_epidem_report.b_item_id is not null
              then t_health_epidem_report.f_epidem_covid_spcm_place_id
              else null end as specimen_place_id
        ,case when t_health_epidem_report.b_item_id is not null
              then t_health_epidem_report.f_epidem_covid_reason_type_id
              else null end as tests_reason_type_id
        ,case when t_health_epidem_report.b_item_id is not null and b_item.item_number is not null
              then b_item.item_number
              else '' end as lab_his_ref_code
        ,case when t_health_epidem_report.b_item_id is not null and t_health_epidem_report.lab_name is not null
              then t_health_epidem_report.lab_name
              else '' end as lab_his_ref_name
        ,case when t_health_epidem_report.b_item_id is not null and b_map_lab_tmlt.b_lab_tmlt_tmltcode is not null
              then b_map_lab_tmlt.b_lab_tmlt_tmltcode
              else '' end as tmlt_code
        ,b_item.b_item_id
    from t_health_epidem_report
        inner join t_visit on t_health_epidem_report.t_visit_id = t_visit.t_visit_id
        inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
        inner join t_person on t_patient.t_person_id = t_person.t_person_id
        left join f_patient_prefix on t_person.f_prefix_id = f_patient_prefix.f_patient_prefix_id
        left join f_patient_nation on t_person.f_patient_nation_id = f_patient_nation.f_patient_nation_id
        left join f_patient_occupation on t_person.f_patient_occupation_id = f_patient_occupation.f_patient_occupation_id
        left join b_map_rp1855_occupation on f_patient_occupation.f_patient_occupation_id = b_map_rp1855_occupation.f_patient_occupation_id
        left join r_rp1855_occupation on b_map_rp1855_occupation.r_rp1855_occupation_id = r_rp1855_occupation.id
        left join t_person_address on t_person.t_person_id = t_person_address.t_person_id
                                  and t_person_address.f_address_type_id = '1'
                                  and t_person_address.priority = 1
                                  and t_person_address.active = '1'
        left join t_address as person_address on t_person_address.t_address_id = person_address.t_address_id
        left join t_address as epidem_address on t_health_epidem_report.t_address_id = epidem_address.t_address_id
        left join (select t_person_contact.t_person_id
                        ,t_person_contact.f_contact_type_id
                        ,t_person_contact.contact_info
                        ,ROW_NUMBER () OVER (PARTITION BY t_person_id,f_contact_type_id ORDER BY priority) as seq
                    from t_person_contact 
                    where t_person_contact.active = '1'
                        and t_person_contact.f_contact_type_id = '2'
                ) as t_person_contact on t_person.t_person_id = t_person_contact.t_person_id
                                     and t_person_contact.seq = 1
        left join (select t_diag_icd10.diag_icd10_vn as t_visit_id
                        ,t_diag_icd10.diag_icd10_diagnosis_date as diagnosis_date
                        ,replace(t_diag_icd10.diag_icd10_number,'.','') as icd10_code
                    from t_diag_icd10
                        inner join t_visit on t_diag_icd10.diag_icd10_vn = t_visit.t_visit_id
                        inner join t_health_epidem_report on t_health_epidem_report.t_visit_id = t_visit.t_visit_id
                    where t_diag_icd10.diag_icd10_active = '1'
                        and t_diag_icd10.f_diag_icd10_type_id = '1'
                        and t_health_epidem_report.t_health_epidem_report_id = $1
                )as icd10_primary on icd10_primary.t_visit_id = t_visit.t_visit_id
        left join (select t_diag_icd10.diag_icd10_vn as t_visit_id
                        ,array_to_string(array_agg(replace(t_diag_icd10.diag_icd10_number,'.','')),',') as icd10_code
                    from t_diag_icd10
                        inner join t_visit on t_diag_icd10.diag_icd10_vn = t_visit.t_visit_id
                        inner join t_health_epidem_report on t_health_epidem_report.t_visit_id = t_visit.t_visit_id
                    where t_diag_icd10.diag_icd10_active = '1'
                        and t_diag_icd10.f_diag_icd10_type_id <> '1'
                        and t_health_epidem_report.t_health_epidem_report_id = $1
                    group by t_diag_icd10.diag_icd10_vn
                )as icd10_list on icd10_list.t_visit_id = t_visit.t_visit_id
        left join b_item on t_health_epidem_report.b_item_id = b_item.b_item_id
        left join b_map_lab_tmlt on b_item.b_item_id = b_map_lab_tmlt.b_item_id
        left join b_employee on t_health_epidem_report.user_report_id = b_employee.b_employee_id
        left join t_person as employee_person on b_employee.t_person_id = employee_person.t_person_id
        left join f_patient_prefix as employee_prefix on employee_person.f_prefix_id = employee_prefix.f_patient_prefix_id
        cross join b_site
    where t_health_epidem_report.t_health_epidem_report_id = $1
        and t_health_epidem_report.active = '1'
    ) as epidem_report;
END;
$$
LANGUAGE 'plpgsql';

ALTER TABLE t_health_epidem_report ADD IF NOT EXISTS sent_complete INTEGER NOT NULL DEFAULT 0; --0 = not sent ,1 = sent completed, 2 = sent failed

INSERT INTO b_restful_url
(b_restful_url_id, restful_code, restful_url, user_update_id, update_datetime, is_system)
VALUES('99912137epidem.moph', 'epidem.moph', 'http://1:90/api/SendEPIDEM?x-api-key=[YOUR_API_KEY]&endpoint=epidem', '157121370000000000', current_timestamp, true);

--# 469
CREATE OR REPLACE FUNCTION moph_epidem_report_V39(epidem_report_id text)
RETURNS TABLE (epidem_query json) AS $$
    DECLARE
    BEGIN
return query 
select json_build_object(
        'hospital',jsonb_build_object(
            'hospital_code',epidem_report.hospital_code
            ,'hospital_name',epidem_report.hospital_name
            ,'his_identifier',epidem_report.his_identifier
            ),
        'person',jsonb_build_object(
            'cid',epidem_report.cid
            ,'passport_no',epidem_report.passport_no
            ,'prefix',epidem_report.prefix
            ,'first_name',epidem_report.first_name
            ,'last_name',epidem_report.last_name
            ,'nationality',epidem_report.nationality
            ,'gender',epidem_report.gender
            ,'birth_date',epidem_report.birth_date
            ,'age_y',epidem_report.age_y
            ,'age_m',epidem_report.age_m
            ,'age_d',epidem_report.age_d
            ,'marital_status_id',epidem_report.marital_status_id
            ,'address',epidem_report.address
            ,'moo',epidem_report.moo
            ,'road',epidem_report.road
            ,'chw_code',epidem_report.chw_code
            ,'amp_code',epidem_report.amp_code
            ,'tmb_code',epidem_report.tmb_code
            ,'mobile_phone',epidem_report.mobile_phone
            ,'occupation',epidem_report.occupation
            ),
        'epidem_report',jsonb_build_object(
            'epidem_report_guid',epidem_report.epidem_report_guid
            ,'epidem_report_group_id',epidem_report.epidem_report_group_id
            ,'treated_hospital_code',epidem_report.treated_hospital_code
            ,'report_datetime',epidem_report.report_datetime
            ,'onset_date',epidem_report.onset_date
            ,'treated_date',epidem_report.treated_date
            ,'diagnosis_date',epidem_report.diagnosis_date
            ,'informer_name',epidem_report.informer_name
            ,'principal_diagnosis_icd10',epidem_report.principal_diagnosis_icd10
            ,'diagnosis_icd10_list',epidem_report.diagnosis_icd10_list
            ,'epidem_person_status_id',epidem_report.epidem_person_status_id
            ,'epidem_symptom_type_id',epidem_report.epidem_symptom_type_id
            ,'pregnant_status',epidem_report.pregnant_status
            ,'respirator_status',epidem_report.respirator_status
            ,'epidem_accommodation_type_id',epidem_report.epidem_accommodation_type_id
            ,'vaccinated_status',epidem_report.vaccinated_status
            ,'exposure_epidemic_area_status',epidem_report.exposure_epidemic_area_status
            ,'exposure_healthcare_worker_status',epidem_report.exposure_healthcare_worker_status
            ,'exposure_closed_contact_status',epidem_report.exposure_closed_contact_status
            ,'exposure_occupation_status',epidem_report.exposure_occupation_status
            ,'exposure_travel_status',epidem_report.exposure_travel_status
            ,'risk_history_type_id',epidem_report.risk_history_type_id
            ,'epidem_address',epidem_report.epidem_address
            ,'epidem_moo',epidem_report.epidem_moo
            ,'epidem_road',epidem_report.epidem_road
            ,'epidem_chw_code',epidem_report.epidem_chw_code
            ,'epidem_amp_code',epidem_report.epidem_amp_code
            ,'epidem_tmb_code',epidem_report.epidem_tmb_code
            ,'location_gis_latitude',epidem_report.location_gis_latitude
            ,'location_gis_longitude',epidem_report.location_gis_longitude
            ,'isolate_chw_code',epidem_report.isolate_chw_code
            ,'isolate_place_id',epidem_report.isolate_place_id
            ,'patient_type',epidem_report.patient_type
            ),
        'lab_report',case when epidem_report.b_item_id IS NULL then '{}' 
                else jsonb_build_object(
            'epidem_lab_confirm_type_id',epidem_report.epidem_lab_confirm_type_id
            ,'lab_report_date',epidem_report.lab_report_date
            ,'lab_report_result',epidem_report.lab_report_result
            ,'specimen_date',epidem_report.specimen_date
            ,'specimen_place_id',epidem_report.specimen_place_id
            ,'tests_reason_type_id',epidem_report.tests_reason_type_id
            ,'lab_his_ref_code',epidem_report.lab_his_ref_code
            ,'lab_his_ref_name',epidem_report.lab_his_ref_name
            ,'tmlt_code',epidem_report.tmlt_code
            ) end) as epidem_report
from (select b_site.b_visit_office_id as hospital_code
        ,b_site.site_full_name as hospital_name
        ,'HospitalOS ' || (select version_application_number from s_version order by s_version.version_update_time desc limit 1) as his_identifier
        ,t_patient.patient_pid as cid
        ,t_health_family.passport_no as passport_no
        ,case when f_patient_prefix.patient_prefix_description is null then ''
              else f_patient_prefix.patient_prefix_description end as prefix
        ,t_patient.patient_firstname as first_name
        ,t_patient.patient_lastname as last_name
        ,f_patient_nation.r_rp1853_nation_id as nationality
        ,case when t_patient.f_sex_id in ('1','2') then t_patient.f_sex_id
              else '0' end::int as gender
        ,to_char(text_to_timestamp(t_patient.patient_birthday),'YYYY-MM-DD') as birth_date
        ,case when EXTRACT(YEAR FROM age(text_to_timestamp(t_patient.patient_birthday)))::int IS NOT NULL and EXTRACT(YEAR FROM age(text_to_timestamp(t_patient.patient_birthday)))::int > 0
              then EXTRACT(YEAR FROM age(text_to_timestamp(t_patient.patient_birthday)))::int else 0 end as age_y
        ,case when EXTRACT(MONTH FROM age(text_to_timestamp(t_patient.patient_birthday)))::int IS NOT NULL and EXTRACT(MONTH FROM age(text_to_timestamp(t_patient.patient_birthday)))::int > 0
              then EXTRACT(MONTH FROM age(text_to_timestamp(t_patient.patient_birthday)))::int else 0 end as age_m
        ,case when EXTRACT(DAY FROM age(text_to_timestamp(t_patient.patient_birthday)))::int IS NOT NULL and EXTRACT(DAY FROM age(text_to_timestamp(t_patient.patient_birthday)))::int > 0
              then EXTRACT(DAY FROM age(text_to_timestamp(t_patient.patient_birthday)))::int else 0 end as age_d
        ,case when t_patient.f_patient_marriage_status_id in ('1','2') then t_patient.f_patient_marriage_status_id::int
              when t_patient.f_patient_marriage_status_id in ('3','4') then '3'::int
              when t_patient.f_patient_marriage_status_id = '5' then '4'::int
              else null end as marital_status_id
        ,case when t_patient.patient_house <> '' then t_patient.patient_house
              else '' end as address
        ,case when t_patient.patient_moo <> '' then t_patient.patient_moo
              else '' end as moo
        ,case when t_patient.patient_road <> '' then t_patient.patient_road
              else '' end as road
        ,case when t_patient.patient_tambon <> '' then substr(t_patient.patient_tambon,1,2)
              else '' end as chw_code
        ,case when t_patient.patient_tambon <> '' then substr(t_patient.patient_tambon,3,2)
              else '' end as amp_code
        ,case when t_patient.patient_tambon <> '' then substr(t_patient.patient_tambon,5,2)
              else '' end as tmb_code
        ,case when t_patient.patient_patient_mobile_phone <> '' then t_patient.patient_patient_mobile_phone
              else '' end as mobile_phone
        ,case when r_rp1855_occupation.code is not null then r_rp1855_occupation.code
              else '' end as occupation
        ,case when t_health_epidem_report.epidem_report_guid is not null then '{' || upper(t_health_epidem_report.epidem_report_guid::text) || '}'
              else '' end epidem_report_guid
        ,92 as epidem_report_group_id
        ,t_health_epidem_report.treated_hospital_code
        ,to_char(t_health_epidem_report.report_datetime,'YYYY-MM-DDThh24:mi:ss.ms') as report_datetime
        ,to_char(t_health_epidem_report.onset_date,'YYYY-MM-DD') as onset_date
        ,to_char(t_health_epidem_report.treated_date,'YYYY-MM-DD') as treated_date
        ,case when icd10_primary.diagnosis_date is not null 
              then to_char(icd10_primary.diagnosis_date,'YYYY-MM-DD')
              else '' end as diagnosis_date
        ,case when employee_prefix.f_patient_prefix_id is not null and employee_prefix.f_patient_prefix_id <> '000'
              then employee_prefix.patient_prefix_description else '' end
         || employee_person.person_firstname || '  ' || employee_person.person_lastname as informer_name
        ,case when icd10_primary.icd10_code is not null then icd10_primary.icd10_code
              else '' end as principal_diagnosis_icd10
        ,case when icd10_list.icd10_code is not null then icd10_list.icd10_code
              else '' end as diagnosis_icd10_list
        ,t_health_epidem_report.f_epidem_person_status_id as epidem_person_status_id
        ,t_health_epidem_report.f_epidem_covid_symptom_type_id as epidem_symptom_type_id
        ,case when t_health_epidem_report.pregnant_status is true then 'Y'
              else 'N' end as pregnant_status
        ,case when t_health_epidem_report.respirator_status is true then 'Y'
              else 'N' end as respirator_status
        ,case when epidem_address.f_address_housetype_id in ('1','2') then epidem_address.f_address_housetype_id::int
              when epidem_address.f_address_housetype_id in ('3','4') then 3
              when epidem_address.f_address_housetype_id = '5' then 4
              when epidem_address.f_address_housetype_id = '8' then 5
              else null end as epidem_accommodation_type_id
        ,case when t_health_epidem_report.vaccinated_status is true then 'Y'
              else 'N' end as vaccinated_status
        ,case when t_health_epidem_report.exposure_epidemic_area_status is true then 'Y'
              else 'N' end as exposure_epidemic_area_status
        ,case when t_health_epidem_report.exposure_healthcare_worker_status is true then 'Y'
              else 'N' end as exposure_healthcare_worker_status
        ,case when t_health_epidem_report.exposure_closed_contact_status is true then 'Y'
              else 'N' end as exposure_closed_contact_status
        ,case when t_health_epidem_report.exposure_occupation_status is true then 'Y'
              else 'N' end as exposure_occupation_status
        ,case when t_health_epidem_report.exposure_travel_status is true then 'Y'
              else 'N' end as exposure_travel_status
        ,t_health_epidem_report.f_epidem_risk_history_type_id as risk_history_type_id
        ,case when epidem_address.house_no is not null then epidem_address.house_no
              else '' end as epidem_address
        ,case when epidem_address.village_no is not null then epidem_address.village_no
              else '' end as epidem_moo
        ,case when epidem_address.road is not null then epidem_address.road
              else '' end as epidem_road
        ,case when epidem_address.sub_district_id is not null then substr(epidem_address.sub_district_id,1,2)
              else '' end as epidem_chw_code
        ,case when epidem_address.sub_district_id is not null then substr(epidem_address.sub_district_id,3,2)
              else '' end as epidem_amp_code
        ,case when epidem_address.sub_district_id is not null then substr(epidem_address.sub_district_id,5,2)
              else '' end as epidem_tmb_code
        ,case when epidem_address.latitude is not null then epidem_address.latitude::float
              else 0 end as location_gis_latitude
        ,case when epidem_address.longitude is not null then epidem_address.longitude::float
              else 0 end as location_gis_longitude
        ,case when t_health_epidem_report.isolate_province_id is not null then substr(t_health_epidem_report.isolate_province_id,1,2)
              else '' end as isolate_chw_code
        ,t_health_epidem_report.f_epidem_covid_isolate_place_id as isolate_place_id
        ,case when t_visit.f_visit_type_id = '1' then 'IPD' else 'OPD' end as patient_type
        ,case when t_health_epidem_report.b_item_id is not null
              then t_health_epidem_report.f_epidem_covid_lab_confirm_type_id
              else null end as epidem_lab_confirm_type_id
        ,case when t_health_epidem_report.b_item_id is not null and t_health_epidem_report.lab_report_date is not null
              then to_char(t_health_epidem_report.lab_report_date,'YYYY-MM-DD')
              else null end as lab_report_date
        ,case when t_health_epidem_report.b_item_id is not null and t_health_epidem_report.lab_value is not null
              then t_health_epidem_report.lab_value
              else null end as lab_report_result
        ,case when t_health_epidem_report.b_item_id is not null and t_health_epidem_report.specimen_date is not null
              then to_char(t_health_epidem_report.specimen_date,'YYYY-MM-DD')
              else null end as specimen_date
        ,case when t_health_epidem_report.b_item_id is not null
              then t_health_epidem_report.f_epidem_covid_spcm_place_id
              else null end as specimen_place_id
        ,case when t_health_epidem_report.b_item_id is not null
              then t_health_epidem_report.f_epidem_covid_reason_type_id
              else null end as tests_reason_type_id
        ,case when t_health_epidem_report.b_item_id is not null and b_item.item_number is not null
              then b_item.item_number
              else '' end as lab_his_ref_code
        ,case when t_health_epidem_report.b_item_id is not null and t_health_epidem_report.lab_name is not null
              then t_health_epidem_report.lab_name
              else '' end as lab_his_ref_name
        ,case when t_health_epidem_report.b_item_id is not null and b_map_lab_tmlt.b_lab_tmlt_tmltcode is not null
              then b_map_lab_tmlt.b_lab_tmlt_tmltcode
              else '' end as tmlt_code
        ,b_item.b_item_id
    from t_health_epidem_report
        inner join t_visit on t_health_epidem_report.t_visit_id = t_visit.t_visit_id
        inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
        inner join t_health_family on t_patient.t_health_family_id = t_health_family.t_health_family_id
        left join f_patient_prefix on t_patient.f_patient_prefix_id = f_patient_prefix.f_patient_prefix_id
        left join f_patient_nation on t_patient.f_patient_nation_id = f_patient_nation.f_patient_nation_id
        left join f_patient_occupation on t_patient.f_patient_occupation_id = f_patient_occupation.f_patient_occupation_id
        left join b_map_rp1855_occupation on f_patient_occupation.f_patient_occupation_id = b_map_rp1855_occupation.f_patient_occupation_id
        left join r_rp1855_occupation on b_map_rp1855_occupation.r_rp1855_occupation_id = r_rp1855_occupation.id
        left join t_address as epidem_address on t_health_epidem_report.t_address_id = epidem_address.t_address_id
        left join (select t_diag_icd10.diag_icd10_vn as t_visit_id
                        ,text_to_timestamp(t_diag_icd10.diag_icd10_diagnosis_date) as diagnosis_date
                        ,replace(t_diag_icd10.diag_icd10_number,'.','') as icd10_code
                    from t_diag_icd10
                        inner join t_visit on t_diag_icd10.diag_icd10_vn = t_visit.t_visit_id
                        inner join t_health_epidem_report on t_health_epidem_report.t_visit_id = t_visit.t_visit_id
                    where t_diag_icd10.diag_icd10_active = '1'
                        and t_diag_icd10.f_diag_icd10_type_id = '1'
                        and t_health_epidem_report.t_health_epidem_report_id = $1
                )as icd10_primary on icd10_primary.t_visit_id = t_visit.t_visit_id
        left join (select t_diag_icd10.diag_icd10_vn as t_visit_id
                        ,array_to_string(array_agg(replace(t_diag_icd10.diag_icd10_number,'.','')),',') as icd10_code
                    from t_diag_icd10
                        inner join t_visit on t_diag_icd10.diag_icd10_vn = t_visit.t_visit_id
                        inner join t_health_epidem_report on t_health_epidem_report.t_visit_id = t_visit.t_visit_id
                    where t_diag_icd10.diag_icd10_active = '1'
                        and t_diag_icd10.f_diag_icd10_type_id <> '1'
                        and t_health_epidem_report.t_health_epidem_report_id = $1
                    group by t_diag_icd10.diag_icd10_vn
                )as icd10_list on icd10_list.t_visit_id = t_visit.t_visit_id
        left join b_item on t_health_epidem_report.b_item_id = b_item.b_item_id
        left join b_map_lab_tmlt on b_item.b_item_id = b_map_lab_tmlt.b_item_id
        left join b_employee on t_health_epidem_report.user_report_id = b_employee.b_employee_id
        left join t_person as employee_person on b_employee.t_person_id = employee_person.t_person_id
        left join f_patient_prefix as employee_prefix on employee_person.f_prefix_id = employee_prefix.f_patient_prefix_id
        cross join b_site
    where t_health_epidem_report.t_health_epidem_report_id = $1
        and t_health_epidem_report.active = '1'
    ) as epidem_report;
END;
$$
LANGUAGE 'plpgsql';

-- #470
CREATE OR REPLACE FUNCTION covid_lab_V39(t_visit_id text)
RETURNS TABLE (covid_lab_query json) AS $$
    DECLARE
    BEGIN
return query
select json_build_object(
        'hospital',jsonb_build_object(
            'hospital_code',covid_lab.hospital_code
            ,'hospital_name',covid_lab.hospital_name
            ,'his_identifier',covid_lab.his_identifier
            )
       ,'patient',jsonb_build_object(
            'cid',covid_lab.cid
            ,'passport_no',covid_lab.passport_no
            ,'hn',covid_lab.hn
            ,'patient_guid',covid_lab.patient_guid
            ,'prefix',covid_lab.prefix
            ,'first_name',covid_lab.first_name
            ,'last_name',covid_lab.last_name
            ,'prefix_eng',covid_lab.prefix_eng
            ,'first_name_eng',covid_lab.first_name_eng
            ,'middle_name_eng',''
            ,'last_name_eng',covid_lab.last_name_eng
            ,'gender',covid_lab.gender
            ,'birth_date',covid_lab.birth_date
            ,'home_phone',covid_lab.home_phone
            ,'mobile_phone',covid_lab.mobile_phone
            ,'address',covid_lab.address
            ,'moo',covid_lab.moo
            ,'road',covid_lab.road
            ,'chw_code',covid_lab.chw_code
            ,'amp_code',covid_lab.amp_code
            ,'tmb_code',covid_lab.tmb_code
            ,'address_full_thai',covid_lab.address_full_thai
            ,'address_full_english',covid_lab.address_full_english
            ,'nationality',covid_lab.nationality
            )
       ,'lab',covid_lab.result_lab_detail
       ) as covid_lab
from (select b_site.b_visit_office_id as hospital_code
        ,b_site.site_full_name as hospital_name
        ,'HospitalOS ' || (select version_application_number from s_version order by s_version.version_update_time desc limit 1) as his_identifier
        ,t_patient.patient_pid as cid
        ,t_health_family.passport_no as passport_no
        ,t_patient.patient_hn as hn
        ,case when t_patient.patient_guid is not null then '{' || upper(t_patient.patient_guid::text) || '}'
              else '' end as patient_guid
        ,case when f_patient_prefix.patient_prefix_description is null then ''
              else f_patient_prefix.patient_prefix_description end as prefix
        ,t_patient.patient_firstname as first_name
        ,t_patient.patient_lastname as last_name
        ,case when f_patient_prefix.patient_prefix_description_eng is null then ''
              else f_patient_prefix.patient_prefix_description_eng end as prefix_eng
        ,case when t_health_family.patient_firstname_eng is null then ''
              else t_health_family.patient_firstname_eng end as first_name_eng
        ,case when t_health_family.patient_lastname_eng is null then ''
              else t_health_family.patient_lastname_eng end as last_name_eng
        ,case when t_patient.f_sex_id in ('1','2') then t_patient.f_sex_id
              else '0' end::int as gender
        ,to_char(text_to_timestamp(t_patient.patient_birthday),'YYYY-MM-DD') as birth_date
        ,max(case when t_patient.patient_phone_number <> '' then t_patient.patient_phone_number
                  else '' end) as home_phone
        ,max(case when t_patient.patient_patient_mobile_phone <> '' then t_patient.patient_patient_mobile_phone
                  else '' end) as mobile_phone
        ,case when t_patient.patient_house <> '' then t_patient.patient_house
              else '' end as address
        ,case when t_patient.patient_moo <> '' then t_patient.patient_moo
              else '' end as moo
        ,case when t_patient.patient_road <> '' then t_patient.patient_road
              else '' end as road
        ,case when t_patient.patient_tambon <> '' then substr(t_patient.patient_tambon,1,2)
              else '' end as chw_code
        ,case when t_patient.patient_tambon <> '' then substr(t_patient.patient_tambon,3,2)
              else '' end as amp_code
        ,case when t_patient.patient_tambon <> '' then substr(t_patient.patient_tambon,5,2)
              else '' end as tmb_code
        ,case when t_patient.patient_house <> '' then t_patient.patient_house else '' end 
         || case when t_patient.patient_moo <> '' then ' หมู่ ' || t_patient.patient_moo else '' end 
         || case when t_patient.patient_road <> '' then ' ถนน' || t_patient.patient_road else '' end
         || case when t_patient.patient_tambon <> '' then ' ตำบล' || tambon.address_description else '' end
         || case when t_patient.patient_amphur <> '' then ' อำเภอ' || amphur.address_description else '' end
         || case when t_patient.patient_changwat <> '' then ' จังหวัด' || changwat.address_description else '' end
         || case when t_patient.patient_postcode <> '' then ' ' || t_patient.patient_postcode
                 when t_patient.patient_tambon <> '' then ' ' || tambon.postcode else '' end
         as address_full_thai
        ,case when t_patient.patient_house <> '' then t_patient.patient_house else '' end 
         || case when t_patient.patient_moo <> '' then ' Moo ' || t_patient.patient_moo else '' end 
         || case when t_patient.patient_road <> '' then t_patient.patient_road || ' Road' else '' end
         || case when t_patient.patient_tambon <> '' then ' ' || tambon.address_description_eng else '' end
         || case when t_patient.patient_amphur <> '' then ' ,' || amphur.address_description_eng else '' end
         || case when t_patient.patient_changwat <> '' then ' ,' || changwat.address_description_eng else '' end
         || case when t_patient.patient_postcode <> '' then ' ' || t_patient.patient_postcode
                 when t_patient.patient_tambon <> '' then ' ' || tambon.postcode else '' end
         as address_full_english
        ,f_patient_nation.r_rp1853_nation_id as nationality
        ,COALESCE(json_agg(result_lab.result_lab_detail) FILTER (WHERE result_lab.result_lab_detail IS NOT NULL), '[]') as result_lab_detail
    from t_visit 
        inner join (select t_visit.t_visit_id
                        ,json_build_object('report_datetime',to_char(text_to_timestamp(t_order.order_report_date_time),'YYYY-MM-DDThh24:mi:ss.ms')
                        ,'patient_lab_ref_code',case when b_item.item_number is not null then b_item.item_number else '' end
                        ,'patient_lab_name_ref',b_item.item_common_name
                        ,'patient_lab_normal_value_ref'
                        ,case when t_result_lab.default_values is not null then array_to_string(t_result_lab.default_values, ',', '*')
                              when t_result_lab.result_lab_min <> '' and t_result_lab.result_lab_min is not null then t_result_lab.result_lab_min
                              when t_result_lab.result_lab_max <> '' and t_result_lab.result_lab_max is not null then t_result_lab.result_lab_max
                              else '' end
                        ,'tmlt_code',case when b_map_lab_tmlt.b_lab_tmlt_tmltcode is not null then b_map_lab_tmlt.b_lab_tmlt_tmltcode else '' end
                        ,'patient_lab_result_text',case when t_result_lab.result_lab_value <> '' and t_result_lab.result_lab_value is not null then t_result_lab.result_lab_value else '' end
                        ,'authorized_officer_name'
                        ,case when report_prefix.f_patient_prefix_id is not null and report_prefix.f_patient_prefix_id <> '000'
                              then report_prefix.patient_prefix_description else '' end
                         || report_person.person_firstname || '  ' || report_person.person_lastname
                        ,'lab_atk_fda_reg_no',case when f_lab_atk_product.lab_atk_fda_reg_no is not null then f_lab_atk_product.lab_atk_fda_reg_no else null end
                        ) as result_lab_detail
                    from t_order
                        inner join t_result_lab on t_order.t_order_id = t_result_lab.t_order_id
                                               and t_result_lab.result_lab_active = '1'
                        inner join t_visit on t_order.t_visit_id = t_visit.t_visit_id
                                          and t_visit.f_visit_status_id <> '4'
                        inner join b_item on t_order.b_item_id = b_item.b_item_id
						inner join b_map_epidem_lab_type on t_order.b_item_id = b_map_epidem_lab_type.b_item_id
                        left join b_map_lab_tmlt on b_item.b_item_id = b_map_lab_tmlt.b_item_id
                        left join b_employee on t_order.order_staff_report = b_employee.b_employee_id
                        left join t_person as report_person on b_employee.t_person_id = report_person.t_person_id
                        left join f_patient_prefix as report_prefix on report_person.f_prefix_id = report_prefix.f_patient_prefix_id
                        left join f_lab_atk_product on t_order.f_lab_atk_product_id = f_lab_atk_product.f_lab_atk_product_id
                    where t_visit.t_visit_id = $1
                        and t_order.f_order_status_id = '4'
                    order by t_order.order_report_date_time) as result_lab on result_lab.t_visit_id = t_visit.t_visit_id
        inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
        inner join t_health_family on t_patient.t_health_family_id = t_health_family.t_health_family_id
        left join f_patient_prefix on t_patient.f_patient_prefix_id = f_patient_prefix.f_patient_prefix_id
        left join f_patient_nation on t_patient.f_patient_nation_id = f_patient_nation.f_patient_nation_id
        left join f_address as tambon on t_patient.patient_tambon = tambon.f_address_id
        left join f_address as amphur on t_patient.patient_amphur = amphur.f_address_id
        left join f_address as changwat on t_patient.patient_changwat = changwat.f_address_id
        cross join b_site
    where t_visit.t_visit_id = $1
        and t_visit.f_visit_status_id <> '4'
    group by b_site.b_visit_office_id
        ,b_site.site_full_name
        ,t_patient.patient_pid
        ,t_health_family.passport_no
        ,t_patient.patient_hn
        ,t_patient.patient_guid
        ,f_patient_prefix.patient_prefix_description
        ,t_patient.patient_firstname
        ,t_patient.patient_lastname
        ,f_patient_prefix.patient_prefix_description_eng
        ,t_health_family.patient_firstname_eng
        ,t_health_family.patient_lastname_eng
        ,t_patient.f_sex_id
        ,t_patient.patient_birthday
        ,t_patient.f_patient_marriage_status_id
        ,t_patient.patient_house
        ,t_patient.patient_moo
        ,t_patient.patient_road
        ,t_patient.patient_tambon
        ,tambon.address_description
        ,t_patient.patient_amphur
        ,amphur.address_description
        ,t_patient.patient_changwat
        ,changwat.address_description
        ,t_patient.patient_postcode
        ,tambon.postcode
        ,tambon.address_description_eng
        ,amphur.address_description_eng
        ,changwat.address_description_eng
        ,f_patient_nation.r_rp1853_nation_id
     ) as covid_lab;
END;
$$
LANGUAGE 'plpgsql';

-- update db version
INSERT INTO s_health_version VALUES ('9710000000010','10','PCU, Community Edition','1.33.0','1.4.0', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

insert into s_script_update_log values ('pcu','update_pcu_ph10.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'update PCU ph9 -> ph10');
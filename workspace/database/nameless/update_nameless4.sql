ALTER TABLE t_visit_the_withdraw ADD COLUMN b_item_billing_subgroup_id character varying(255) DEFAULT '';

ALTER TABLE t_visit_the_withdraw ADD COLUMN item_billing_subgroup_description character varying(255) DEFAULT '';

ALTER TABLE t_visit_the_withdraw ADD COLUMN order_price character varying(255) DEFAULT '';

ALTER TABLE t_visit_the_withdraw ADD COLUMN order_qty character varying(255) DEFAULT '';

INSERT INTO s_nameless_version VALUES ('9750000000004', '4', 'NAMELESS, Community Edition', '1.04.201010', '1.04.201010', '2553-10-20 14:00:00');
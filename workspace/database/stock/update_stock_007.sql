--issues#539
CREATE TABLE IF NOT EXISTS f_hstock_management (   
    id          CHARACTER VARYING(1) NOT NULL,
    description CHARACTER VARYING(255) NOT NULL,  
    CONSTRAINT f_hstock_management_pk PRIMARY KEY (id)
);

BEGIN;
INSERT INTO f_hstock_management SELECT '1','FIFO (First In First Out)'
WHERE NOT EXISTS (SELECT 1 FROM f_hstock_management WHERE id = '1'); 
COMMIT;
BEGIN;
INSERT INTO f_hstock_management SELECT '2','FEFO (First Expire First Out)'
WHERE NOT EXISTS (SELECT 1 FROM f_hstock_management WHERE id = '2'); 
COMMIT;

CREATE TABLE IF NOT EXISTS b_hstock_setup (   
    b_hstock_setup_id       CHARACTER VARYING(1) NOT NULL,
    f_hstock_management_id  CHARACTER VARYING(1) NOT NULL,  
    CONSTRAINT b_hstock_setup_pk PRIMARY KEY (b_hstock_setup_id)
);

BEGIN;
INSERT INTO b_hstock_setup SELECT '1','2'
WHERE NOT EXISTS (SELECT 1 FROM b_hstock_setup WHERE b_hstock_setup_id = '1'); 
COMMIT;

INSERT INTO s_stock_version (version_app, version_db, description)VALUES ('2.2.0', '2.1.0', 'Add Req Stock Module');
INSERT INTO s_script_update_log values ('Stock_Module','update_stock_007.sql',(select current_date) || ','|| (select current_time),'Add Req Stock Module');

INSERT INTO f_employee_authentication VALUES ('98','TB');

CREATE TABLE f_tb_type (
    tb_type_id character varying(255) NOT NULL,
    tb_type_description character varying(255),
    CONSTRAINT f_tb_type_pkey PRIMARY KEY (tb_type_id)
);

INSERT INTO f_tb_type VALUES ('0','ไม่ทราบ');
INSERT INTO f_tb_type VALUES ('1','ในปอด');
INSERT INTO f_tb_type VALUES ('2','นอกปอด');
INSERT INTO f_tb_type VALUES ('3','ในและนอกปอด');

CREATE TABLE f_tb_place (
    tb_place_id character varying(255) NOT NULL,
    tb_place_description character varying(255),
    CONSTRAINT f_tb_place_pkey PRIMARY KEY (tb_place_id)
);

INSERT INTO f_tb_place VALUES ('0','ไม่ทราบ');
INSERT INTO f_tb_place VALUES ('1','ต่อมน้ำเหลือง');
INSERT INTO f_tb_place VALUES ('2','เยื่อหุ้มปอด');
INSERT INTO f_tb_place VALUES ('3','เยื่อหุ้มสมอง');
INSERT INTO f_tb_place VALUES ('4','ลำไส้');
INSERT INTO f_tb_place VALUES ('5','เยื่อบุช่องท้อง');
INSERT INTO f_tb_place VALUES ('6','กระดูก/ข้อ');
INSERT INTO f_tb_place VALUES ('7','ผิวหนัง');
INSERT INTO f_tb_place VALUES ('8','อื่นๆ');
INSERT INTO f_tb_place VALUES ('9','กระจายทั่วร่าง');
INSERT INTO f_tb_place VALUES ('10','ท่อปัสสาวะ');

CREATE TABLE f_tb_patient_type (
    tb_patient_type_id character varying(255) NOT NULL,
    tb_patient_type_description character varying(255),
    CONSTRAINT f_tb_patient_type_pkey PRIMARY KEY (tb_patient_type_id)
);

INSERT INTO f_tb_patient_type VALUES ('1','New');
INSERT INTO f_tb_patient_type VALUES ('2','Relapse');
INSERT INTO f_tb_patient_type VALUES ('3','TAF');
INSERT INTO f_tb_patient_type VALUES ('4','TAD');
INSERT INTO f_tb_patient_type VALUES ('5','TI');
INSERT INTO f_tb_patient_type VALUES ('6','Other');

CREATE TABLE f_tb_phelgmconversion (
    tb_phelgmconversion_id character varying(255) NOT NULL,
    tb_phelgmconversion_description character varying(255),
    CONSTRAINT f_tb_phelgmconversion_pkey PRIMARY KEY (tb_phelgmconversion_id)
);

INSERT INTO f_tb_phelgmconversion VALUES ('1','เสมหะลบ');
INSERT INTO f_tb_phelgmconversion VALUES ('2','เสมหะบวก');
INSERT INTO f_tb_phelgmconversion VALUES ('3','ไม่ได้ตรวจ');
INSERT INTO f_tb_phelgmconversion VALUES ('4','ตาย');
INSERT INTO f_tb_phelgmconversion VALUES ('5','ขาดยาเกิน 2 เดือน');
INSERT INTO f_tb_phelgmconversion VALUES ('6','โอนออก');
INSERT INTO f_tb_phelgmconversion VALUES ('7','ไม่นำมาประเมิน');

CREATE TABLE t_tb_chronic (
    t_tb_chronic_id character varying(255) NOT NULL,
    tb_chronic_hn character varying(255),
    tb_chronic_vn character varying(255),
    tb_chronic_diagnosis_date character varying(255),
    tb_chronic_discharge_date character varying(255),
    tb_type_id character varying(255),
    tb_place_id character varying(255),
    tb_chronic_specify character varying(255),
    tb_patient_type_id character varying(255),
    tb_chronic_type_diagnosis_date character varying(255),
    tb_chronic_phelgmconversion_diagnosis_date character varying(255),
    tb_phelgmconversion_id character varying(255),
    f_chronic_discharge_status_id character varying(255),
    tb_chronic_notice character varying(255),
    tb_chronic_icd10 character varying(255),
    t_visit_id character varying(255),
    t_patient_id character varying(255),
    record_date_time character varying(255),
    tb_chronic_site_treat character varying(255) DEFAULT ''::character varying,
    t_health_family_id character varying(255) DEFAULT ''::character varying,
    modify_date_time character varying(255) DEFAULT ''::character varying,
    cancel_date_time character varying(255) DEFAULT ''::character varying,
    staff_record character varying(255) DEFAULT ''::character varying,
    staff_modify character varying(255) DEFAULT ''::character varying,
    staff_cancel character varying(255) DEFAULT ''::character varying,
    tb_chronic_active character varying(255) DEFAULT ''::character varying,
    tb_chronic_survey_date character varying(255) DEFAULT ''::character varying,
    CONSTRAINT t_tb_chronic_pkey PRIMARY KEY (t_tb_chronic_id)
);

CREATE TABLE b_tb_number_run (
    tb_number_run_year character varying(255) NOT NULL,
    tb_number_run_last character varying(255),
    CONSTRAINT b_tb_number_run_pkey PRIMARY KEY (tb_number_run_year)
);

CREATE TABLE t_tb_patient (
    tb_patient_id character varying(255) NOT NULL,
    tb_tbn character varying(255),
    t_patient_id character varying(255),
    prison_status character varying(255),
    mentor_type character varying(255),
    mentor_name character varying(255),
    tb_patient_register_date character varying(255),
    tb_patient_record_date character varying(255),
    tb_patient_staff_record character varying(255),
    tb_patient_modify_date character varying(255),
    tb_patient_staff_modify character varying(255),
    CONSTRAINT t_tb_patient_pkey PRIMARY KEY (tb_patient_id)
);

CREATE TABLE t_prison (
    prison_id character varying(255) NOT NULL,
    t_visit_id character varying(255),
    tb_patient_id character varying(255),
    prison_record_date character varying(255),
    prison_staff_record character varying(255),
    CONSTRAINT t_prison_pkey PRIMARY KEY (prison_id)
);

CREATE TABLE f_patient_address_type (
    person_address_type_id character varying(255) NOT NULL,
    person_address_type_description character varying(255),
    CONSTRAINT f_person_address_type_pkey PRIMARY KEY (person_address_type_id)
);

INSERT INTO f_patient_address_type VALUES ('1','ที่อยู่ผู้ป่วย');
INSERT INTO f_patient_address_type VALUES ('2','ที่อยู่ต่างประเทศ');
INSERT INTO f_patient_address_type VALUES ('3','ที่อยู่ผู้ติดต่อ');

CREATE TABLE t_experience_house (
    experience_house_id character varying(255) NOT NULL,
    experience_house_screen_date character varying(255),
    experience_house_live character varying(255),
    experience_house_symptoms_compatible_tb character varying(255),
    experience_house_tb character varying(255),
    experience_house_screen_tb character varying(255),
    experience_house_child character varying(255),
    experience_house_lung_xray character varying(255),
    experience_house_treatment character varying(255),
    experience_house_sputum character varying(255),
    t_patient_id character varying(255),
    patient_address_type_id character varying(255),
    experience_house_address character varying(255),
    staff_record character varying(255),
    staff_modify character varying(255),
    date_record character varying(255),
    date_modify character varying(255),
    CONSTRAINT t_experience_house_pkey PRIMARY KEY (experience_house_id)
);

-- 21.09.11
CREATE TABLE f_tb_cat_standard (
    cat_standard_id character varying(255) NOT NULL,
    cat_standard_description character varying(255),
    CONSTRAINT f_tb_cat_standard_pkey PRIMARY KEY (cat_standard_id)
);

INSERT INTO f_tb_cat_standard VALUES ('0','ไม่ระบุ');
INSERT INTO f_tb_cat_standard VALUES ('1','CAT-1(1)<2RHZE/4HR>(สำหรับผู้ใหญ่)');
INSERT INTO f_tb_cat_standard VALUES ('2','CAT-1(2)<2RHZS/4HR>(สำหรับเด็ก)');
INSERT INTO f_tb_cat_standard VALUES ('3','CAT-2<2HRZES/1HRZE/5HRE>');
INSERT INTO f_tb_cat_standard VALUES ('4','CAT-3<2HRZ/4HR>');
INSERT INTO f_tb_cat_standard VALUES ('5','CAT-4(1.1)<3K5 OPEZ/15OPEZ>');
INSERT INTO f_tb_cat_standard VALUES ('6','CAT-4(1.2)<3S5 OPEZ/15OPEZ>');
INSERT INTO f_tb_cat_standard VALUES ('7','CAT-4(2.1)<3K5 OPEtZ/15OPEtZ>');
INSERT INTO f_tb_cat_standard VALUES ('8','CAT-4(2.2)<3K5 OPEZ/3K3 OPEZ/12OPEZ>');
INSERT INTO f_tb_cat_standard VALUES ('9','CAT-4(2.3)<3K5 OPEtZ/3K3 OPEtZ/12OPEtZ>');
INSERT INTO f_tb_cat_standard VALUES ('10','CAT modify');

CREATE TABLE t_tb_cat (
    tb_cat_id character varying(255) NOT NULL,
    t_patient_id character varying(255),
    t_visit_id character varying(255),
    cat_standard_id character varying(255),
    date_record character varying(255),
    staff_record character varying(255),
    CONSTRAINT t_tb_cat_pkey PRIMARY KEY (tb_cat_id)
);

CREATE TABLE f_health_mentor_type (
    mentor_type_id character varying(255) NOT NULL,
    mentor_type_description character varying(255),
    CONSTRAINT f_health_mentor_type_pkey PRIMARY KEY (mentor_type_id)
);

INSERT INTO f_health_mentor_type VALUES ('1','เจ้าหน้าที่สาธารณะสุข');
INSERT INTO f_health_mentor_type VALUES ('2','อสม');
INSERT INTO f_health_mentor_type VALUES ('3','ผู้นำชุมชน');
INSERT INTO f_health_mentor_type VALUES ('4','ญาติ');
INSERT INTO f_health_mentor_type VALUES ('5','ไม่มีผู้กำกับการกินยา');
INSERT INTO f_health_mentor_type VALUES ('6','อื่นๆ');


CREATE TABLE f_health_symptoms_adjacent (
    symptoms_adjacent_id character varying(255) NOT NULL,
    symptoms_adjacent_description character varying(255),
    CONSTRAINT f_health_symptoms_adjacent_pkey PRIMARY KEY (symptoms_adjacent_id)
);

INSERT INTO f_health_symptoms_adjacent VALUES ('1','ไม่มี');
INSERT INTO f_health_symptoms_adjacent VALUES ('2','ผื่นคัน');
INSERT INTO f_health_symptoms_adjacent VALUES ('3','ปวดข้อ');
INSERT INTO f_health_symptoms_adjacent VALUES ('4','ตามัว');
INSERT INTO f_health_symptoms_adjacent VALUES ('5','ตาเหลือง');
INSERT INTO f_health_symptoms_adjacent VALUES ('6','ตัวเหลือง');
INSERT INTO f_health_symptoms_adjacent VALUES ('7','หูอื้อ');
INSERT INTO f_health_symptoms_adjacent VALUES ('8','อื่นๆ');

CREATE TABLE t_health_visit_home_tb (
    t_health_visit_home_tb_id character varying(255) NOT NULL,
    visit_home_tb_symptoms_adjacent character varying(255),
    visit_home_tb_mentor_type character varying(255),
    visit_home_tb_mentor_name character varying(255),
    visit_home_tb_problem character varying(255),
    visit_home_tb_object character varying(255),
    visit_home_tb_maintain character varying(255),
    visit_home_tb_assess character varying(255),
    visit_home_tb_plane character varying(255),
    visit_home_tb_date character varying(255),
    visit_home_tb_nextdate character varying(255),
    visit_home_tb_remark character varying(255),
    visit_home_tb_record_time character varying(255),
    visit_home_tb_modify_time character varying(255),
    visit_home_tb_cancle_time character varying(255),
    visit_home_tb_staff_record character varying(255),
    visit_home_tb_staff_modify character varying(255),
    visit_home_tb_staff_cancle character varying(255),
    visit_home_tb_active character varying(255),
    t_patient_id character varying(255),
    t_health_family_id character varying(255) DEFAULT ''::character varying,
    CONSTRAINT t_health_visit_home_tb_pkey PRIMARY KEY (t_health_visit_home_tb_id)
);


CREATE TABLE t_health_visit_home_tb_vitalsign (
    t_health_visit_home_tb_vitalsign_id character varying(255) NOT NULL,
    health_vitalsign_tb_height character varying(255),
    health_vitalsign_tb_weight character varying(255),
    health_vitalsign_tb_blood_presure character varying(255),
    health_vitalsign_tb_temperature character varying(255),
    health_vitalsign_tb_heart_rate character varying(255),
    health_vitalsign_tb_respiratory_rate character varying(255),
    f_visit_nutrition_level_id character varying(255),
    health_vitalsign_tb_bmi character varying(255),
    health_vitalsign_tb_check_date character varying(255),
    health_vitalsign_tb_check_time character varying(255),
    health_vitalsign_tb_record_time character varying(255),
    health_vitalsign_tb_record_date character varying(255),
    health_vitalsign_tb_staff_record character varying(255),
    health_vitalsign_tb_staff_modify character varying(255),
    health_vitalsign_tb_modify_date_time character varying(255),
    health_vitalsign_tb_cancle_date_time character varying(255),
    health_vitalsign_tb_staff_cancle character varying(255),
    health_vitalsign_tb_active character varying(255),
    t_health_visit_home_tb_id character varying(255),
    CONSTRAINT t_health_visit_home_tb_vitalsign_pkey PRIMARY KEY (t_health_visit_home_tb_vitalsign_id)
);

CREATE TABLE s_tb_version (
    s_tb_version_id                    integer NOT NULL,
    version_tb_description       	varchar(255) NULL,
    version_tb_application_number	varchar(255) NULL,
    version_tb_database_number   	varchar(255) NULL,
    version_tb_update_time       	varchar(255) NULL
    );

ALTER TABLE s_tb_version
	ADD CONSTRAINT s_tb_version_pkey
	PRIMARY KEY (s_tb_version_id);


INSERT INTO s_tb_version VALUES (1, 'TB Module', '1.0.111220', '1.0.111220', (select current_date) || ','|| (select current_time));

INSERT INTO s_script_update_log values ('TB_Module','update_tb_001.sql',(select current_date) || ','|| (select current_time),'Initialize TB Module');
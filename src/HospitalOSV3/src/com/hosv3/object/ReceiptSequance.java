/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.object;

import com.hospital_os.usecase.connection.Persistent;
import java.text.DecimalFormat;
import java.util.Calendar;

/**
 *
 * @author LionHeart
 */
@SuppressWarnings("ClassWithoutLogger")
public class ReceiptSequance extends Persistent {

    String init = "";
    public String receipt_sequence_pattern = init;
    public String receipt_sequence_value = init;
    public String receipt_sequence_ip = init;
    public String receipt_sequence_active = init;
    public String book_no = init;
    public String begin_no = init;
    public String end_no = init;
    public String receipt_qty = init;
    public String timeStamp_column = init;

    public static String getDBText(String pattern, int value, String year2d) {
        String patt = pattern;
        String start = "";
        String year = "";
        if (pattern.contains("yy")) {
            year = year2d;
            patt = pattern.substring(pattern.lastIndexOf("yy") + 2);
            start = pattern.substring(0, pattern.indexOf("yy"));
            Calendar c = Calendar.getInstance();
            c.setTimeInMillis(System.currentTimeMillis());
        }
        if (pattern.indexOf('.') != -1) {
            patt = pattern.substring(pattern.lastIndexOf('.') + 1);
            start = pattern.substring(0, pattern.indexOf('.'));
        }
        DecimalFormat d = new DecimalFormat();
        d.applyPattern(patt);// Bug#2950 ���õ�駤��Ẻ���᷹ "6'0'1100000"
        String show = start + year + d.format(value);
        return show;
    }
}

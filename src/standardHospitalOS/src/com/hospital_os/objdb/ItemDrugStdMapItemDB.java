/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.ItemDrugStdMapItem;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.ResultSet;
import java.util.List;
import java.util.Vector;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class ItemDrugStdMapItemDB {

    private final ConnectionInf theConnectionInf;
    private final String tableId = "988";

    public ItemDrugStdMapItemDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public int insert(ItemDrugStdMapItem obj) throws Exception {
        String sql = "INSERT INTO b_item_drug_std_map_item( "
                + "b_item_drug_std_map_item_id, b_item_id, b_item_ed_standard_id) "
                + "VALUES ('%s', '%s', '%s')";
        sql = String.format(sql, obj.getGenID(tableId), obj.b_item_id, obj.b_item_ed_standard_id);
        return theConnectionInf.eUpdate(sql);
    }

    public int delete(ItemDrugStdMapItem itemDrugStdMapItem) throws Exception {
        String sql = "delete from b_item_drug_std_map_item where b_item_drug_std_map_item_id = '%s'";
        return theConnectionInf.eUpdate(String.format(sql, itemDrugStdMapItem.getObjectId()));
    }

    public ItemDrugStdMapItem selectById(String id) throws Exception {
        String sql = "select * from b_item_drug_std_map_item where b_item_drug_std_map_item_id = '%s'";
        Vector eQuery = eQuery(String.format(sql, id));
        return (ItemDrugStdMapItem) (eQuery.isEmpty() ? null : eQuery.get(0));
    }

    public Vector<ItemDrugStdMapItem> listAll() throws Exception {
        String sql = "select * from b_item_drug_std_map_item";
        Vector eQuery = eQuery(sql);
        return eQuery;
    }

    public Vector eQuery(String sql) throws Exception {
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        while (rs.next()) {
            ItemDrugStdMapItem p = new ItemDrugStdMapItem();
            p.setObjectId(rs.getString("b_item_drug_std_map_item_id"));
            p.b_item_id = rs.getString("b_item_id");
            p.b_item_ed_standard_id = rs.getString("b_item_ed_standard_id");
            list.add(p);
        }
        rs.close();
        return list;
    }

    public List<Object[]> listMaped(String keyword) throws Exception {
        String sql = "select b_item_drug_std_map_item.b_item_drug_std_map_item_id"
                + ", b_item.item_common_name "
                + ", b_item_ed_standard.generic_name "
                + ", b_item_ed_standard.name1 "
                + ", b_item_ed_standard.name2 "
                + ", b_item_ed_standard.name3 "
                + ", b_item_ed_standard.name4 "
                + "from b_item_drug_std_map_item "
                + "inner join b_item on b_item_drug_std_map_item.b_item_id = b_item.b_item_id "
                + "inner join b_item_ed_standard on b_item_drug_std_map_item.b_item_ed_standard_id = b_item_ed_standard.b_item_ed_standard_id "
                + "where UPPER(b_item.item_common_name) like UPPER('%s') order by b_item.item_common_name";
        return theConnectionInf.eComplexQuery(String.format(sql, keyword == null || keyword.isEmpty() ? "%" : keyword));
    }

    public List<Object[]> listNotMap() throws Exception {
        String sql = "select b_item.b_item_id, b_item.item_common_name "
                + "from b_item "
                + "inner join b_item_subgroup on b_item_subgroup.b_item_subgroup_id = b_item.b_item_subgroup_id and b_item_subgroup.f_item_group_id = '1' "
                + "where b_item.item_active = '1' "
                + "and b_item.b_item_id not in (select b_item_id from b_item_drug_std_map_item) order by b_item.item_common_name";
        List<Object[]> eComplexQuery = theConnectionInf.eComplexQuery(sql);
        return eComplexQuery;
    }
}

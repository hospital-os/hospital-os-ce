/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class Cancer extends Persistent {

    public String t_visit_id = "";
    public String health_cancer_survey_date = "";
    public String health_cancer_breast_exam = "0";
    public String health_cancer_cervix_exam = "0";
    public String health_cancer_cervix_method = "";
    public String health_cancer_breast_exam_note = "";
    public String health_cancer_cervix_exam_note = "";
    public String health_cancer_note = "";
    public String health_cancer_first_check = "0";
    public String active = "1";
    public String user_record_id = "";
    public String record_date_time = "";
    public String user_modify_id = "";
    public String modify_date_time = "";
    public String user_cancel_id = "";
    public String cancel_date_time = "";
    public String t_person_id = "";
}

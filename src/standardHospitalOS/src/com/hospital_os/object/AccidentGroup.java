/*
 * AccidentGroup.java
 *
 * Created on 25 ����Ҥ� 2549, 18:45 �.
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Padungrat(tong)
 */
@SuppressWarnings("ClassWithoutLogger")
public class AccidentGroup extends Persistent {
    // public String t_accident_group_id;

    public String acctime = "";
    public String date_accident = "";
    public String name_rd = "";
    public String in_out = "";
    public String kilo = "";
    public String accmu = "";
    public String acctb = "";
    public String accam = "";
    public String acccw = "";
    public String ptmobie = "";
    public String reporter = "";
    public String record_date_time = "";
    public String staff_update = "";
    public String update_date_time = "";
    public String staff_cancel = "";
    public String cancel_date_time = "";
    public String active = "1";
}

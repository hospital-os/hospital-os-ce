/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.VisitIllnessAddress;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class VisitIllnessAddressDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "803";
    private final AddressDB addressDB;

    public VisitIllnessAddressDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
        this.addressDB = new AddressDB(connectionInf);
    }

    public int insert(VisitIllnessAddress obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "INSERT INTO t_visit_illness_address(\n"
                    + "               t_visit_illness_address_id, t_visit_id, t_address_id, active, user_record_id, user_update_id)\n"
                    + "       VALUES (?, ?, ?, \n"
                    + "               ?, ?, ?)";
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, obj.getGenID(tableId));
            preparedStatement.setString(index++, obj.t_visit_id);
            preparedStatement.setString(index++, obj.t_address_id);
            preparedStatement.setString(index++, obj.active);
            preparedStatement.setString(index++, obj.user_record_id);
            preparedStatement.setString(index++, obj.user_update_id);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(VisitIllnessAddress obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "UPDATE t_visit_illness_address\n"
                    + "      SET t_visit_id = ?, t_address_id = ?, active=?, \n"
                    + "          user_update_id=?, update_date_time=current_timestamp\n"
                    + "    WHERE t_visit_illness_address_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, obj.t_visit_id);
            preparedStatement.setString(index++, obj.t_address_id);
            preparedStatement.setString(index++, obj.active);
            preparedStatement.setString(index++, obj.user_update_id);
            preparedStatement.setString(index++, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int inactive(VisitIllnessAddress obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "UPDATE t_visit_illness_address\n"
                    + "      SET active=?, user_update_id=?, update_date_time=current_timestamp, user_cancel_id=?, cancel_date_time=current_timestamp\n"
                    + "    WHERE t_visit_illness_address_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, "0");
            preparedStatement.setString(index++, obj.user_update_id);
            preparedStatement.setString(index++, obj.user_cancel_id);
            preparedStatement.setString(index++, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "delete from t_visit_illness_address where t_visit_illness_address_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            // execute delete SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public VisitIllnessAddress select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from t_visit_illness_address where t_visit_illness_address_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<VisitIllnessAddress> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<VisitIllnessAddress> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<VisitIllnessAddress> list = new ArrayList<>();
        try (ResultSet rs = preparedStatement.executeQuery()) {
            while (rs.next()) {
                VisitIllnessAddress obj = new VisitIllnessAddress();
                obj.setObjectId(rs.getString("t_visit_illness_address_id"));
                obj.t_visit_id = rs.getString("t_visit_id");
                obj.t_address_id = rs.getString("t_address_id");
                obj.active = rs.getString("active");
                obj.user_record_id = rs.getString("user_record_id");
                obj.record_date_time = rs.getTimestamp("record_date_time");
                obj.user_update_id = rs.getString("user_update_id");
                obj.update_date_time = rs.getTimestamp("update_date_time");
                obj.user_cancel_id = rs.getString("user_cancel_id");
                obj.cancel_date_time = rs.getTimestamp("cancel_date_time");
                obj.setAddress(addressDB.select(obj.t_address_id));
                list.add(obj);
            }
            return list;
        }
    }

    public VisitIllnessAddress selectByVisitId(String visitId) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from t_visit_illness_address where t_visit_id = ? and active ='1'";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, visitId);
            List<VisitIllnessAddress> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
}

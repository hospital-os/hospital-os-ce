/*
 * Test.java
 *
 * Created on 28 �á�Ҥ� 2548, 14:59 �.
 */
package com.reportcenter;

import com.hospital_os.object.ServicePoint;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hosv3.HosUpdate;
import com.hosv3.control.HosControl;
import com.hosv3.gui.frame.FrameLogin;
import com.hosv3.object.HosObject;
import com.hosv3.utility.Config;
import com.hosv3.utility.ReadModule;
import com.hosv3.utility.Splash;

/**
 *
 * @author tong(Padungrat)
 */
@SuppressWarnings("ClassWithoutLogger")
public class MainReport implements Runnable {

    private String[] args;
    private Splash theSplash = new Splash();

    @Override
    public void run() {
        Config config = new Config();       
        // start auto update
        HosUpdate.checkUpdate(theSplash);
        // create ReportCenter Frame
        FrameMain fMain = new FrameMain();
        // create jdbc connection        
        ConnectionInf con_inf = config.getConnectionInfFromFile(args, theSplash, fMain);
        // if create fail program will close now.
        if (con_inf == null) {
            System.exit(0);
        }
        // show news
        theSplash.showNews(con_inf);

        String user = new String();
        char[] pass = new char[100];
        ServicePoint sp = new ServicePoint();
        theSplash.setVisible(false);
        theSplash = null;
        // create new instance of HosControl
        HosControl theHC = new HosControl(con_inf);
        theHC.setUpdateStatus(fMain);
        // Show login dialog, if cancel program will close now.
        if (!FrameLogin.showDialog(fMain, theHC, user, pass, sp, 2)) {
            System.exit(0);
        }
        // login process.
        theHC.theSystemControl.login(user, pass, sp);
        theHC.theHO.running_program = HosObject.REPORTAPP;
        fMain.setControl(con_inf, theHC);
        if (args.length == 0) {
            args = new String[]{"-module_xml"};
        }
        // load report modules and add them to ReportCenter Frame.
        fMain.showModule(ReadModule.loadModule(args, Config.MODULE_PATH_RP, null));
        fMain.setVisible(true);
        
    }

    public static void main(String args[]) {
        MainReport mainReport = new MainReport();
        mainReport.load(args);
    }

    private void load(String[] args) {
        this.args = args;
        Thread thread = new Thread(this);
        thread.start();
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * DialogLeaveTheHouseInfo.java
 *
 * Created on 17 �.�. 2554, 17:21:31
 */
package com.hosv3.gui.dialog;

import com.hospital_os.object.AdmitLeaveDay;
import com.hospital_os.object.Visit;
import com.hospital_os.utility.ComboboxModel;
import com.hospital_os.utility.Gutil;
import com.hosv3.control.HosControl;
import java.awt.Component;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Vector;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import sd.util.datetime.DateTimeUtil;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class DialogAdmitLeaveHomeInfo extends javax.swing.JDialog {

    private static final long serialVersionUID = 1L;
    private HosControl hosControl;
    private Visit visit;
    private final TableModel tableModel = new TableModel();
    private AdmitLeaveDay admitLeaveDay;

    /**
     * Creates new form DialogLeaveTheHouseInfo
     */
    public DialogAdmitLeaveHomeInfo(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        initTable();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jScrollPane1 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();
        toobarButton = new javax.swing.JToolBar();
        btnAdd = new javax.swing.JButton();
        btnDel = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        panelDetail = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        dateOut = new sd.comp.jcalendar.JDateChooser();
        timeOut = new sd.comp.jcalendar.JTimeChooser();
        jLabel2 = new javax.swing.JLabel();
        dateIn = new sd.comp.jcalendar.JDateChooser();
        timeIn = new sd.comp.jcalendar.JTimeChooser();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        leaveCause = new javax.swing.JTextArea();
        doctorApprove = new javax.swing.JComboBox();
        cbComeBackHos = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("�������ҡ�Ѻ��ҹ");
        getContentPane().setLayout(new java.awt.GridBagLayout());

        table.setFont(table.getFont());
        table.setFillsViewportHeight(true);
        table.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        table.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tableMouseReleased(evt);
            }
        });
        table.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tableKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(table);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        getContentPane().add(jScrollPane1, gridBagConstraints);

        toobarButton.setFloatable(false);
        toobarButton.setRollover(true);

        btnAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/btn_add.png"))); // NOI18N
        btnAdd.setFocusable(false);
        btnAdd.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnAdd.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });
        toobarButton.add(btnAdd);

        btnDel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/btn_delete.png"))); // NOI18N
        btnDel.setFocusable(false);
        btnDel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnDel.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnDel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDelActionPerformed(evt);
            }
        });
        toobarButton.add(btnDel);

        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/btn_save.png"))); // NOI18N
        btnSave.setFocusable(false);
        btnSave.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnSave.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        toobarButton.add(btnSave);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 0);
        getContentPane().add(toobarButton, gridBagConstraints);

        panelDetail.setLayout(new java.awt.GridBagLayout());

        jLabel1.setFont(jLabel1.getFont());
        jLabel1.setText("�ѹ����ҡ�Ѻ��ҹ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 2, 2, 2);
        panelDetail.add(jLabel1, gridBagConstraints);

        dateOut.setFont(dateOut.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelDetail.add(dateOut, gridBagConstraints);

        timeOut.setFont(timeOut.getFont());
        timeOut.setTimeFormat("HH:mm");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelDetail.add(timeOut, gridBagConstraints);

        jLabel2.setFont(jLabel2.getFont());
        jLabel2.setText("�˵ؼš����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelDetail.add(jLabel2, gridBagConstraints);

        dateIn.setEnabled(false);
        dateIn.setFont(dateIn.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 2, 2, 2);
        panelDetail.add(dateIn, gridBagConstraints);

        timeIn.setEnabled(false);
        timeIn.setFont(timeIn.getFont());
        timeIn.setTimeFormat("HH:mm");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 2, 2, 2);
        panelDetail.add(timeIn, gridBagConstraints);

        jLabel3.setFont(jLabel3.getFont());
        jLabel3.setText("ᾷ����͹حҵ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 2, 2, 2);
        panelDetail.add(jLabel3, gridBagConstraints);

        leaveCause.setColumns(20);
        leaveCause.setFont(leaveCause.getFont());
        leaveCause.setLineWrap(true);
        leaveCause.setRows(3);
        jScrollPane2.setViewportView(leaveCause);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelDetail.add(jScrollPane2, gridBagConstraints);

        doctorApprove.setEditable(true);
        doctorApprove.setFont(doctorApprove.getFont());
        doctorApprove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                doctorApproveActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.ipadx = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelDetail.add(doctorApprove, gridBagConstraints);

        cbComeBackHos.setFont(cbComeBackHos.getFont());
        cbComeBackHos.setText("�ѹ����Ѻ�� þ.");
        cbComeBackHos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbComeBackHosActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelDetail.add(cbComeBackHos, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        getContentPane().add(panelDetail, gridBagConstraints);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void doctorApproveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_doctorApproveActionPerformed
        if (evt.getActionCommand().equals("comboBoxEdited")) {
            doFindDoctor();
        }
    }//GEN-LAST:event_doctorApproveActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        doAddNew();
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnDelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDelActionPerformed
        doDelete();
    }//GEN-LAST:event_btnDelActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        doSaveOrUpdate();
    }//GEN-LAST:event_btnSaveActionPerformed

    private void tableMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableMouseReleased
        doSelectedItem();
    }//GEN-LAST:event_tableMouseReleased

    private void tableKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tableKeyReleased
        doSelectedItem();
    }//GEN-LAST:event_tableKeyReleased

    private void cbComeBackHosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbComeBackHosActionPerformed
        doSelectedBackHome();
    }//GEN-LAST:event_cbComeBackHosActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnDel;
    private javax.swing.JButton btnSave;
    private javax.swing.JCheckBox cbComeBackHos;
    private sd.comp.jcalendar.JDateChooser dateIn;
    private sd.comp.jcalendar.JDateChooser dateOut;
    private javax.swing.JComboBox doctorApprove;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea leaveCause;
    private javax.swing.JPanel panelDetail;
    private javax.swing.JTable table;
    private sd.comp.jcalendar.JTimeChooser timeIn;
    private sd.comp.jcalendar.JTimeChooser timeOut;
    private javax.swing.JToolBar toobarButton;
    // End of variables declaration//GEN-END:variables

    private void initTable() {
        table.setModel(tableModel);
        TableColumnModel cm = table.getColumnModel();
        DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();
        dtcr.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);
        cm.getColumn(0).setCellRenderer(dtcr);
        cm.getColumn(0).setPreferredWidth(50);
        cm.getColumn(1).setCellRenderer(new DateCellRenderer(
                "yyyy-MM-dd,HH:mm",
                new Locale("th", "TH"),
                "dd-MMM-yyyy, HH:mm",
                new Locale("th", "TH")));

        cm.getColumn(1).setMinWidth(150);
        cm.getColumn(1).setMaxWidth(150);
        cm.getColumn(1).setPreferredWidth(150);
        cm.getColumn(2).setCellRenderer(new DateCellRenderer(
                "yyyy-MM-dd,HH:mm",
                new Locale("th", "TH"),
                "dd-MMM-yyyy, HH:mm",
                new Locale("th", "TH")));
        cm.getColumn(2).setMinWidth(150);
        cm.getColumn(2).setMaxWidth(150);
        cm.getColumn(2).setPreferredWidth(150);
        cm.getColumn(3).setCellRenderer(dtcr);
        cm.getColumn(3).setPreferredWidth(250);

        jScrollPane1.setViewportView(table);
        jScrollPane1.revalidate();
        jScrollPane1.repaint();
    }

    public void setControl(HosControl theHC) {
        this.hosControl = theHC;
        ComboboxModel.initComboBox(doctorApprove, hosControl.theLookupControl.listDoctor());
    }

    public void openDialog(Visit visit) {
        if (visit == null) {
            return;
        }
        this.visit = visit;
        this.listTableData();
        toobarButton.setVisible(!visit.isDischargeMoney() && !visit.isDischargeIPD());
        this.setSize(600, 480);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    private void clearUI() {
        tableModel.getData().clear();
        tableModel.fireTableDataChanged();
        btnAdd.setEnabled(true);
        btnDel.setEnabled(false);
        btnSave.setEnabled(false);
        panelDetail.setVisible(false);
        admitLeaveDay = null;
    }

    private void listTableData() {
        clearUI();
        List<Object[]> list = hosControl.theVisitControl.listAdmitLeaveDayByVisitId(visit.getObjectId());
        tableModel.getData().addAll(list);
        tableModel.fireTableDataChanged();
        validateUI();
    }

    private void validateUI() {
        btnAdd.setEnabled(table.getRowCount() == 0 ? true : !tableModel.getValueAt(0, 2).toString().isEmpty());
        btnDel.setEnabled(table.getRowCount() > 0 && table.getSelectedRowCount() > 0);
        panelDetail.setVisible(table.getRowCount() > 0 && table.getSelectedRowCount() > 0);
        btnSave.setEnabled(panelDetail.isVisible());
    }

    @SuppressWarnings("UseOfObsoleteCollectionType")
    private void doFindDoctor() {

        String keyword = String.valueOf(doctorApprove.getSelectedItem());
        Vector v = hosControl.theLookupControl.listDoctor(keyword.trim());
        if (v == null) {
            v = new Vector();
        }
        if (v.isEmpty()) {
//                jComboBoxDoctor.setSelectedIndex(index);
        } else {
            ComboboxModel.initComboBox(doctorApprove, v);
        }
    }

    private void doSelectedItem() {
        String id = (String) table.getModel().getValueAt(table.getSelectedRow(), 4);
        setDetailPanel(hosControl.theVisitControl.findAdmitLeaveDayById(id));
    }

    private void setDetailPanel(AdmitLeaveDay admitLeaveDay) {
        this.admitLeaveDay = admitLeaveDay;
        if (this.admitLeaveDay == null) {
            Date now = new Date();
            dateOut.setDate(now);
            timeOut.setDate(now);
            cbComeBackHos.setSelected(false);
            cbComeBackHos.setEnabled(false);
            dateIn.setDate(now);
            timeIn.setDate(now);
            dateIn.setEnabled(cbComeBackHos.isSelected());
            timeIn.setEnabled(cbComeBackHos.isSelected());
            leaveCause.setText("");
            if (visit.visit_patient_self_doctor != null && !visit.visit_patient_self_doctor.isEmpty()) {
                Gutil.setGuiData(doctorApprove, visit.visit_patient_self_doctor);
            } else if ("3".equals(hosControl.theHO.theEmployee.authentication_id)) {
                Gutil.setGuiData(doctorApprove, hosControl.theHO.theEmployee.getObjectId());
            } else {
                doctorApprove.setSelectedIndex(0);
            }
        } else {
            dateOut.setDate(DateTimeUtil.stringToDate(this.admitLeaveDay.date_out, "yyyy-MM-dd", DateTimeUtil.LOCALE_TH));
            timeOut.setDate(DateTimeUtil.stringToDate(this.admitLeaveDay.time_out, "HH:mm", DateTimeUtil.LOCALE_TH));
            cbComeBackHos.setSelected(this.admitLeaveDay.comeback.equals("1"));
            cbComeBackHos.setEnabled(true);
            dateIn.setDate(cbComeBackHos.isSelected() ? DateTimeUtil.stringToDate(this.admitLeaveDay.date_in, "yyyy-MM-dd", DateTimeUtil.LOCALE_TH) : new Date());
            timeIn.setDate(cbComeBackHos.isSelected() ? DateTimeUtil.stringToDate(this.admitLeaveDay.time_in, "HH:mm", DateTimeUtil.LOCALE_TH) : new Date());
            dateIn.setEnabled(cbComeBackHos.isSelected());
            timeIn.setEnabled(cbComeBackHos.isSelected());
            leaveCause.setText(this.admitLeaveDay.leave_cause);
            Gutil.setGuiData(doctorApprove, this.admitLeaveDay.doctor_approve);
        }
        panelDetail.setVisible(true);
        btnSave.setEnabled(true);
        btnDel.setEnabled(this.admitLeaveDay != null);
    }

    private void doAddNew() {
        setDetailPanel(null);
    }

    private void doDelete() {
        String id = (String) table.getModel().getValueAt(table.getSelectedRow(), 4);
        hosControl.theVisitControl.deleteAdmitLeaveDayById(id);
        this.listTableData();
    }

    private void doSaveOrUpdate() {
        if (this.admitLeaveDay == null) {
            this.admitLeaveDay = new AdmitLeaveDay();
        }
        this.admitLeaveDay.date_out = dateOut.getStringDate("yyyy-MM-dd", DateTimeUtil.LOCALE_TH);
        this.admitLeaveDay.time_out = timeOut.getStringDate("HH:mm");
        this.admitLeaveDay.comeback = cbComeBackHos.isSelected() ? "1" : "0";
        this.admitLeaveDay.date_in = cbComeBackHos.isSelected() ? dateIn.getStringDate("yyyy-MM-dd", DateTimeUtil.LOCALE_TH) : "";
        this.admitLeaveDay.time_in = cbComeBackHos.isSelected() ? timeIn.getStringDate("HH:mm") : "";
        this.admitLeaveDay.leave_cause = leaveCause.getText().trim();
        this.admitLeaveDay.doctor_approve = Gutil.getGuiData(doctorApprove);
        this.admitLeaveDay.t_visit_id = this.visit.getObjectId();
        int ret = hosControl.theVisitControl.saveOrUpdateAdmitLeaveDay(this.admitLeaveDay);
        if (ret == 1) {
            this.listTableData();
        }
    }

    private void doSelectedBackHome() {
        dateIn.setEnabled(cbComeBackHos.isSelected());
        timeIn.setEnabled(cbComeBackHos.isSelected());
    }

    private class TableModel extends AbstractTableModel {

        private static final long serialVersionUID = 1L;
        private String[] columns = {
            "�ӴѺ",
            "�ѹ����ҡ�Ѻ��ҹ",
            "�ѹ����Ѻ��þ.",
            "ᾷ����͹حҵ"};
        private List<Object[]> data = new ArrayList<Object[]>();

        public Object[] getRow(int row) {
            return data.get(row);
        }

        @Override
        public int getRowCount() {
            return data.size();
        }

        @Override
        public int getColumnCount() {
            return columns.length;
        }

        @Override
        public String getColumnName(int col) {
            return columns[col];
        }

        @Override
        public Object getValueAt(int row, int col) {
            Object[] object = data.get(row);

            switch (col) {
                case 0:
                    return row + 1;
                case 1:
                    return object[1] == null ? "" : object[1].toString();
                case 2:
                    return object[2] == null ? "" : object[2].toString();
                case 3:
                    return object[3] == null ? "" : object[3].toString();
                default:
                    return object[0] == null ? "" : object[0].toString();
            }
        }

        public List<Object[]> getData() {
            return data;
        }
    }

    private class DateCellRenderer extends JLabel implements TableCellRenderer {

        private static final long serialVersionUID = 1L;
        private String orgFormat = "yyyy-MM-dd,HH:mm";
        private Locale orgLocale = new Locale("th", "TH");
        private String toFormat = "dd MMM yyyy, HH:mm";
        private Locale toLocale = new Locale("th", "TH");

        /**
         * Creates new StringCellRenderer
         */
        DateCellRenderer(String orgFormat, Locale orgLocale, String toFormat, Locale toLocale) {
            this.orgFormat = orgFormat;
            this.orgLocale = orgLocale;
            this.toFormat = toFormat;
            this.toLocale = toLocale;
            setOpaque(true);
            setHorizontalAlignment(SwingConstants.CENTER);
        }

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            setFont(table.getFont());
            if (value instanceof String) {
                try {
                    SimpleDateFormat sdf = new SimpleDateFormat(orgFormat, orgLocale);
                    Date date = sdf.parse(String.valueOf(value));
                    sdf = new SimpleDateFormat(toFormat, toLocale);
                    String strDate = sdf.format(date);
                    setText(strDate);
                    setToolTipText(strDate);
                } catch (ParseException ex) {
                    setText(String.valueOf(value));
                    setToolTipText(String.valueOf(value));
                }
            }


            if (isSelected) {
                this.setBackground(table.getSelectionBackground());
                this.setForeground(table.getSelectionForeground());
            } else {
                this.setBackground(table.getBackground());
                this.setForeground(table.getForeground());
            }
            return this;
        }
    }
}

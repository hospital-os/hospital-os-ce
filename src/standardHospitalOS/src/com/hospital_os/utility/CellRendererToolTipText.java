/*
 * CellRendererToolTipText.java
 *
 * Created on 24 �չҤ� 2549, 11:45 �.
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
package com.hospital_os.utility;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author tong(Padungrat)
 */
@SuppressWarnings("ClassWithoutLogger")
public class CellRendererToolTipText extends JLabel implements TableCellRenderer {

    private static final long serialVersionUID = 1L;
    boolean isBordered = true;
    Color color = null;
    Color fontcolor = null;

    public CellRendererToolTipText(boolean isBordered) {
        this.isBordered = isBordered;
        this.color = new Color(255, 255, 255);
        this.fontcolor = new Color(0, 0, 0);
        setOpaque(true);
    }

    @Override
    public Component getTableCellRendererComponent(
            JTable table, Object namecell,
            boolean isSelected, boolean hasFocus,
            int row, int column) {
        if (namecell != null) {
            String name = (String) namecell;
            if (isBordered) {
                if (isSelected) {
                    this.setBackground(table.getSelectionBackground());
                    this.setForeground(table.getSelectionForeground());
                } else {
                    this.setBackground(table.getBackground());
                    this.setForeground(table.getForeground());
                }
            }
            setToolTipText("<html><BODY BGCOLOR = #E7FAAF>" + name + "</BODY></html>");
            setText(name);
        }
        return this;
    }
}

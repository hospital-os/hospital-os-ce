/*
 * PlanLookup.java
 *
 * Created on 27 �á�Ҥ� 2548, 15:22 �.
 */
package com.hosv3.control.lookup;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.LookupControlInf;
import com.hosv3.control.LookupControl;

/**
 *
 * @not deprecated because use henbe package
 *
 * @author kingland
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class PlanLookup implements LookupControlInf {

    private LookupControl theLookup;

    /**
     * Creates a new instance of PlanLookup
     */
    public PlanLookup(LookupControl lookup) {
        theLookup = lookup;
    }

    @Override
    public java.util.Vector listData(String str) {
        return theLookup.listPlan(str);
    }

    @Override
    public CommonInf readHosData(String str) {
        return theLookup.readPlanById(str);
    }
}

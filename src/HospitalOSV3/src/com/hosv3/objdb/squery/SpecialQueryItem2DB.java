package com.hosv3.objdb.squery;

import com.hospital_os.objdb.ItemDB;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.util.Vector;

@SuppressWarnings("UseOfObsoleteCollectionType")
public class SpecialQueryItem2DB extends ItemDB {

    public SpecialQueryItem2DB(ConnectionInf db) {
        super(db);
    }
    //////////////////////////////////////////////////////////////////////////////

    @Override
    public Vector selectItemByGroup(String groupid) throws Exception {
        String sql = "select * from b_item where item_active = '1' "
                + "and b_item_id in (select b_item_id  from b_item_set "
                + "where b_item_group_id = '" + groupid + "') order by item_common_name";
        return eQuery(sql);
    }

    public Vector listItemByStdId(String item_drug_standard_id) throws Exception {
        String sql = "select  "
                + "b_item.* "
                + "from b_item "
                + "inner join b_item_drug_standard_map_item on b_item_drug_standard_map_item.b_item_id = b_item.b_item_id "
                + "where  "
                + "b_item_drug_standard_map_item.b_item_drug_standard_id = '" + item_drug_standard_id + "' "
                + "and b_item.item_active = '1' "
                + "order by b_item.item_common_name";
        return eQuery(sql);
    }
}

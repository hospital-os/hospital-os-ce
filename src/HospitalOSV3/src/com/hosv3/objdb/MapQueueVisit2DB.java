package com.hosv3.objdb;

import com.hospital_os.objdb.MapQueueVisitDB;
import com.hospital_os.object.MapQueueVisit;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hosv3.utility.Constant;

@SuppressWarnings("UseOfObsoleteCollectionType")
public class MapQueueVisit2DB extends MapQueueVisitDB {

    public MapQueueVisit2DB(ConnectionInf db) {
        super(db);
    }

    public int save(MapQueueVisit mqv) throws Exception {
        if (mqv.getObjectId() == null) {
            mqv.setObjectId(Constant.generateOid(idtable, MapQueueVisit.hospital_site));
            return insert(mqv);
        } else {
            return update(mqv);
        }
    }

    @Override
    public int insert(MapQueueVisit o) throws Exception {
        MapQueueVisit p = o;
        String sql = "insert into " + dbObj.table + " ("
                + dbObj.pk_field
                + " ," + dbObj.visit_id
                + " ," + dbObj.queue_visit
                + " ," + dbObj.queue
                + " ," + dbObj.active
                + " ) values ('"
                + p.getObjectId()
                + "','" + p.visit_id
                + "','" + p.queue_visit
                + "','" + p.queue
                + "','" + p.active
                + "')";
        return theConnectionInf.eUpdate(sql);
    }
}

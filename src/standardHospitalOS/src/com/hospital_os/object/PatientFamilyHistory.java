/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;
import java.util.Date;

/**
 *
 * @author sompr
 */
public class PatientFamilyHistory extends Persistent {

    private String f_patient_family_historys = "";
    public String patient_family_history_other = "";
    public String user_update_id = "";
    public Date update_datetime;
    public Date record_datetime;
    public String user_record_id = "";

    public String getFPatientFamilyHistorys() {
        f_patient_family_historys = "";
        if (family_history1_select) {
            f_patient_family_historys += ("\"1\": \"" + family_history1_detail + "\"");
        }
        if (family_history2_select) {
            f_patient_family_historys += ((f_patient_family_historys.isEmpty() ? "" : ", ")
                    + ("\"2\": \"" + family_history2_detail + "\""));
        }
        if (family_history3_select) {
            f_patient_family_historys += ((f_patient_family_historys.isEmpty() ? "" : ", ")
                    + ("\"3\": \"" + family_history3_detail + "\""));
        }
        if (family_history4_select) {
            f_patient_family_historys += ((f_patient_family_historys.isEmpty() ? "" : ", ")
                    + ("\"4\": \"" + family_history4_detail + "\""));
        }
        if (family_history5_select) {
            f_patient_family_historys += ((f_patient_family_historys.isEmpty() ? "" : ", ")
                    + ("\"5\": \"" + family_history5_detail + "\""));
        }

        return "{" + f_patient_family_historys + "}";
    }

    public void setFPatientFamilyHistorys(String jsonString) {
        this.f_patient_family_historys = jsonString;
    }

    // object only
    public boolean family_history1_select;
    public String family_history1_detail = "";
    public boolean family_history2_select;
    public String family_history2_detail = "";
    public boolean family_history3_select;
    public String family_history3_detail = "";
    public boolean family_history4_select;
    public String family_history4_detail = "";
    public boolean family_history5_select;
    public String family_history5_detail = "";
}

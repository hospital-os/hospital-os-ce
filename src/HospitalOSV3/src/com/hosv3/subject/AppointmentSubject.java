/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.subject;

import com.hosv3.usecase.transaction.ManageAppointment;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class AppointmentSubject implements ManageAppointment {

    private List<ManageAppointment> list = new ArrayList<ManageAppointment>();

    public void removeAttach() {
        list.clear();
    }

    public void attach(ManageAppointment o) {
        list.add(o);
    }

    @Override
    public void notifySaveAppointment(String str, int status) {
        for (ManageAppointment managePhysicalTherapy : list) {
            managePhysicalTherapy.notifySaveAppointment(str, status);
        }
    }

    @Override
    public void notifyDeleteAppointment(String str, int status) {
        for (ManageAppointment managePhysicalTherapy : list) {
            managePhysicalTherapy.notifyDeleteAppointment(str, status);
        }
    }
}

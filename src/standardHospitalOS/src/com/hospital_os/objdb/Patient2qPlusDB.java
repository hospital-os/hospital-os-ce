/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.Patient2qPlus;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sompr
 */
public class Patient2qPlusDB {

    protected final ConnectionInf theConnectionInf;
    private final String idtable = "6552";

    public Patient2qPlusDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public int insert(Patient2qPlus o) throws Exception {
        String sql = "INSERT INTO t_patient_2qplus ("
                + "               t_patient_2qplus_id, t_patient_id, t_visit_id, screen_date, "
                + "               screen_2qplus_q1, screen_2qplus_q2, screen_2qplus_q3, active, user_record_id, user_update_id)\n"
                + "        VALUES(?, ?, ?, ?, "
                + "               ?, ?, ?, ?, ?, ?)";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, o.getGenID(idtable));
            ePQuery.setString(index++, o.t_patient_id);
            ePQuery.setString(index++, o.t_visit_id);
            ePQuery.setDate(index++, o.screen_date != null ? new java.sql.Date(o.screen_date.getTime()) : null);
            ePQuery.setString(index++, o.screen_2qplus_q1);
            ePQuery.setString(index++, o.screen_2qplus_q2);
            ePQuery.setString(index++, o.screen_2qplus_q3);
            ePQuery.setString(index++, o.active);
            ePQuery.setString(index++, o.user_record_id);
            ePQuery.setString(index++, o.user_update_id);
            return ePQuery.executeUpdate();
        }
    }

    public int update(Patient2qPlus o) throws Exception {
        String sql = "UPDATE t_patient_2qplus\n"
                + "      SET t_patient_id=?, t_visit_id=?, screen_date=?, screen_2qplus_q1=?, screen_2qplus_q2=?, screen_2qplus_q3=?, "
                + "          update_date_time=CURRENT_TIMESTAMP, user_update_id=?"
                + "    WHERE t_patient_2qplus_id=?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, o.t_patient_id);
            ePQuery.setString(index++, o.t_visit_id);
            ePQuery.setDate(index++, o.screen_date != null ? new java.sql.Date(o.screen_date.getTime()) : null);
            ePQuery.setString(index++, o.screen_2qplus_q1);
            ePQuery.setString(index++, o.screen_2qplus_q2);
            ePQuery.setString(index++, o.screen_2qplus_q3);
            ePQuery.setString(index++, o.user_update_id);
            ePQuery.setString(index++, o.getObjectId());
            return ePQuery.executeUpdate();
        }
    }

    public int delete(Patient2qPlus o) throws Exception {
        String sql = "UPDATE t_patient_2qplus "
                + "      SET user_update_id=?, update_date_time=CURRENT_TIMESTAMP, active =?, user_cancel_id=?, cancel_date_time=CURRENT_TIMESTAMP"
                + "    WHERE t_patient_2qplus_id =?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, o.user_update_id);
            ePQuery.setString(index++, "0");
            ePQuery.setString(index++, o.user_cancel_id);
            ePQuery.setString(index++, o.getObjectId());
            return ePQuery.executeUpdate();
        }
    }

    public Patient2qPlus selectByPid(String pid) throws Exception {
        String sql = "SELECT * FROM t_patient_2qplus \n"
                + "WHERE active = '1' \n"
                + "AND t_patient_id = ? \n"
                + "ORDER BY screen_date desc \n"
                + "	,record_date_time desc";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, pid);
            List<Patient2qPlus> list = executeQuery(ePQuery);
            return list.isEmpty() ? null : list.get(0);
        }
    }

    public List<Patient2qPlus> listByPid(String pid) throws Exception {
        String sql = "SELECT t_patient_2qplus.* \n"
                + "         , t_visit.visit_vn as visit_vn \n"
                + "FROM t_patient_2qplus \n"
                + "LEFT JOIN t_visit ON t_patient_2qplus.t_visit_id = t_visit.t_visit_id \n"
                + "WHERE t_patient_2qplus.active = '1'\n"
                + "AND t_patient_2qplus.t_patient_id = ? \n"
                + "ORDER BY t_patient_2qplus.screen_date desc\n"
                + "	,t_patient_2qplus.record_date_time desc";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, pid);
            return executeQuery(ePQuery);
        }
    }

    public Patient2qPlus selectById(String id) throws Exception {
        String sql = "SELECT * FROM t_patient_2qplus \n"
                + "WHERE active = '1'\n"
                + "AND t_patient_2qplus_id = ? ";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, id);
            List<Patient2qPlus> list = executeQuery(ePQuery);
            return list.isEmpty() ? null : list.get(0);
        }
    }

    public List<Patient2qPlus> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<Patient2qPlus> list = new ArrayList<>();
        try (ResultSet rs = preparedStatement.executeQuery()) {
            while (rs.next()) {
                Patient2qPlus obj = new Patient2qPlus();
                obj.setObjectId(rs.getString("t_patient_2qplus_id"));
                obj.t_patient_id = rs.getString("t_patient_id");
                obj.t_visit_id = rs.getString("t_visit_id");
                obj.screen_date = rs.getDate("screen_date");
                obj.screen_2qplus_q1 = rs.getString("screen_2qplus_q1");
                obj.screen_2qplus_q2 = rs.getString("screen_2qplus_q2");
                obj.screen_2qplus_q3 = rs.getString("screen_2qplus_q3");
                obj.active = rs.getString("active");
                obj.record_date_time = rs.getDate("record_date_time");
                obj.user_record_id = rs.getString("user_record_id");
                obj.update_date_time = rs.getDate("update_date_time");
                obj.user_update_id = rs.getString("user_update_id");
                obj.cancel_date_time = rs.getDate("cancel_date_time");
                obj.user_record_id = rs.getString("user_record_id");

                try {
                    obj.vn = rs.getString("visit_vn");
                } catch (SQLException ex) {

                }
                list.add(obj);
            }
            return list;
        }
    }
}

ALTER TABLE public.t_chronic ADD hcode_dx varchar(5) NULL;
COMMENT ON COLUMN public.t_chronic.hcode_dx IS 'รหัสสถานพยาบาลที่วินิจฉัยครั้งแรก';
ALTER TABLE public.t_chronic ADD hcode_rx varchar(5) NULL;
COMMENT ON COLUMN public.t_chronic.hcode_rx IS 'รหัสสถานพยาบาลที่ไปรับบริการประจำ';

-- update db version
INSERT INTO s_version VALUES ('9701000000089', '89', 'Hospital OS, Community Edition', '3.9.56', '3.36.0', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_56.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.56');
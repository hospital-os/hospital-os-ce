package com.hospital_os.objdb;

import com.hospital_os.object.Item;
import com.hospital_os.object.ItemType;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.ComboFix;
import com.hospital_os.utility.Gutil;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public class ItemDB extends X39DB {

    public Item dbObj;
    final public String idtable = "174";

    /**
     * @param db
     * @roseuid 3F65897F0326
     */
    public ItemDB(ConnectionInf db) {
        theConnectionInf = db;
        dbObj = Item.initConfig();
        dbObjX = dbObj;
    }

    public int insert(Item obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO b_item(\n"
                    + "            b_item_id, item_number, item_common_name, item_trade_name, item_nick_name, item_local_name, \n"
                    + "            item_active, b_item_subgroup_id, b_item_billing_subgroup_id, \n"
                    + "            item_secret, b_item_16_group_id, \n"
                    + "            item_unit_packing_qty, f_item_lab_type_id, \n"
                    + "            item_general_number, doctor_warning_icd10, b_specimen_id, \n"
                    + "            user_record, user_modify, f_item_lab_location_id, \n"
                    + "            item_lab_duration, item_lab_duration_day, f_lab_atk_product_id,\n"
                    + "            f_item_type_id, item_package_use_hstock, item_not_use_to_order)\n"
                    + "    VALUES (?, ?, ?, ?, ?, ?, \n"
                    + "            ?, ?, ?, \n"
                    + "            ?, ?, \n"
                    + "            ?, ?, \n"
                    + "            ?, ?, ?, \n"
                    + "            ?, ?, ?, \n"
                    + "            ?, ?, ?, \n"
                    + "            ?, ?, ?)");
            preparedStatement = theConnectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.getGenID(idtable));
            preparedStatement.setString(index++, obj.item_id);
            preparedStatement.setString(index++, obj.common_name);
            preparedStatement.setString(index++, obj.trade_name);
            preparedStatement.setString(index++, obj.nick_name);
            preparedStatement.setString(index++, obj.local_name);
            preparedStatement.setString(index++, obj.active);
            preparedStatement.setString(index++, obj.item_group_code_category);
            preparedStatement.setString(index++, obj.item_group_code_billing);
            preparedStatement.setString(index++, obj.secret);
            preparedStatement.setString(index++, obj.item_16_group);
            preparedStatement.setString(index++, obj.unit_pack53);
            preparedStatement.setString(index++, obj.rp_lab_type);
            preparedStatement.setString(index++, obj.item_general_number);
            preparedStatement.setString(index++, obj.warning_icd10);
            preparedStatement.setString(index++, obj.b_specimen_id);
            preparedStatement.setString(index++, obj.user_record);
            preparedStatement.setString(index++, obj.user_modify);
            preparedStatement.setString(index++, obj.f_item_lab_location_id);
            preparedStatement.setString(index++, obj.item_lab_duration);
            preparedStatement.setInt(index++, Integer.valueOf(obj.item_lab_duration_day));
            if (obj.f_lab_atk_product_id == null || obj.f_lab_atk_product_id.isEmpty()) {
                preparedStatement.setNull(index++, java.sql.Types.INTEGER);
            } else {
                preparedStatement.setInt(index++, Integer.valueOf(obj.f_lab_atk_product_id));
            }

            preparedStatement.setString(index++, obj.f_item_type_id);
            preparedStatement.setString(index++, obj.item_package_use_hstock);
            preparedStatement.setString(index++, obj.item_not_use_to_order);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(Item obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE b_item\n"
                    + "   SET item_number=?, item_common_name=?, item_trade_name=?, item_nick_name=?, \n"
                    + "       item_local_name=?, item_active=?, b_item_subgroup_id=?, b_item_billing_subgroup_id=?, \n"
                    + "       item_secret=?, b_item_16_group_id=?, \n"
                    + "       item_unit_packing_qty=?, f_item_lab_type_id=?, \n"
                    + "       item_general_number=?, doctor_warning_icd10=?, \n"
                    + "       b_specimen_id=?, user_modify=?, \n"
                    + "       modify_datetime = current_timestamp, \n"
                    + "       f_item_lab_location_id=?, \n"
                    + "       item_lab_duration=?, item_lab_duration_day=?, f_lab_atk_product_id=?, f_item_type_id=?, item_package_use_hstock=?, item_not_use_to_order=? \n");
            sql.append(" WHERE b_item_id=?");
            preparedStatement = theConnectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.item_id);
            preparedStatement.setString(index++, obj.common_name);
            preparedStatement.setString(index++, obj.trade_name);
            preparedStatement.setString(index++, obj.nick_name);
            preparedStatement.setString(index++, obj.local_name);
            preparedStatement.setString(index++, obj.active);
            preparedStatement.setString(index++, obj.item_group_code_category);
            preparedStatement.setString(index++, obj.item_group_code_billing);
            preparedStatement.setString(index++, obj.secret);
            preparedStatement.setString(index++, obj.item_16_group);
            preparedStatement.setString(index++, obj.unit_pack53);
            preparedStatement.setString(index++, obj.rp_lab_type);
            preparedStatement.setString(index++, obj.item_general_number);
            preparedStatement.setString(index++, obj.warning_icd10);
            preparedStatement.setString(index++, obj.b_specimen_id);
            preparedStatement.setString(index++, obj.user_modify);
            preparedStatement.setString(index++, obj.f_item_lab_location_id);
            preparedStatement.setString(index++, obj.item_lab_duration);
            preparedStatement.setInt(index++, Integer.valueOf(obj.item_lab_duration_day));
            if (obj.f_lab_atk_product_id == null || obj.f_lab_atk_product_id.isEmpty()) {
                preparedStatement.setNull(index++, java.sql.Types.INTEGER);
            } else {
                preparedStatement.setInt(index++, Integer.valueOf(obj.f_lab_atk_product_id));
            }
            preparedStatement.setString(index++, obj.f_item_type_id);
            preparedStatement.setString(index++, obj.item_package_use_hstock);
            preparedStatement.setString(index++, obj.item_not_use_to_order);
            preparedStatement.setString(index++, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(Item o) throws Exception {
        StringBuffer sql = new StringBuffer("delete from ").append(dbObj.table).append(" where ").append(dbObj.pk_field).append("='").append(o.getObjectId()).append("'");
        return theConnectionInf.eUpdate(sql.toString());
    }

    public Vector selectItemByGroup(String groupid) throws Exception {
        StringBuffer sql = new StringBuffer("select * from b_item where item_active = '1' ").append(
                "and b_item_id in (select b_item_id  from b_item_set ").append(
                        "where b_item_group_id = '").append(groupid).append("')\n");
        sql.append("order by item_common_name, item_trade_name, item_nick_name");
        return this.eQueryX(sql.toString(), new String[]{});
    }

    public Vector selectAllLab() throws Exception {
        String sql = "select * from " + dbObj.table + " inner join b_item_subgroup on b_item_subgroup.b_item_subgroup_id = "
                + "b_item.b_item_subgroup_id and b_item_subgroup.f_item_group_id = '2' order by item_common_name";
        return this.eQueryX(sql, new String[]{});
    }

    public int delete(String pk) throws Exception {
        StringBuffer sql = new StringBuffer("delete from ").append(dbObj.table).append(" where ").append(dbObj.pk_field).append("='").append(pk).append("'");
        return theConnectionInf.eUpdate(sql.toString());
    }

    /**
     * ���� ITEM �������� ��Ф�� key word ������
     *
     * @param keygroup
     * @param keyword
     * @param active
     * @return
     * @throws Exception
     */
    public Vector selectByItemGroup(String keygroup, String keyword, String active) throws Exception {
        return selectByItemGroup(keygroup, keyword, active, true, ItemType.isSingle(), false);
    }

    public Vector selectByItemGroup(String keygroup, String keyword, String active, boolean begin_with, String itemType, boolean isCanOrder) throws Exception {
        StringBuilder sql = new StringBuilder("select * from ").append(dbObj.table).append(" where ");
        if (!keygroup.isEmpty()) {
            sql.append(dbObj.item_group_code_category).append(" = ?\n");
            if (!keyword.isEmpty()) {
                sql.append("and ");
            }
        }
        if (!keyword.isEmpty()) {
            sql.append(" (").append(dbObj.common_name).append(" ilike ?  ").append(
                    " or ").append(dbObj.nick_name).append(" ilike ? ").append(
                    " or ").append(dbObj.trade_name).append(" ilike ? ").append(
                    " or ").append(dbObj.item_id).append(" ilike ?)\n");
        }
        if (!keygroup.isEmpty() || !keyword.isEmpty()) {
            sql.append(" and ");
        }
        sql.append(dbObj.active).append(" = ?\n");
        // �ʴ���¡�÷������������ʶҹ� active ����
        if (isCanOrder && active.equals("1")) {
            sql.append(" or item_active = '2' \n");
        }
        sql.append(" and f_item_type_id = ? \n");
        if (itemType.equals("1")) {
            sql.append(" and item_package_use_hstock is null \n");
        } else {
            sql.append(" and item_package_use_hstock = '0' \n");
        }
        sql.append(" order by ").append(dbObj.common_name);
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql.toString())) {
            int index = 1;
            if (!keygroup.isEmpty()) {
                ePQuery.setString(index++, keygroup);
            }
            if (!keyword.isEmpty()) {
                keyword = (!begin_with ? "%" : "") + keyword + "%";
                ePQuery.setString(index++, keyword);
                ePQuery.setString(index++, keyword);
                ePQuery.setString(index++, keyword);
                ePQuery.setString(index++, keyword);
            }
            ePQuery.setString(index++, active);
            ePQuery.setString(index++, itemType);
            return this.eQuery(ePQuery.toString());
        }
    }

    public Vector selectByItemGroupId(String itemGroupid, String keyword, String active, boolean begin_with) throws Exception {
        StringBuilder sql = new StringBuilder("select b_item.* from b_item\n");
        sql.append("inner join b_item_subgroup on b_item_subgroup.b_item_subgroup_id = b_item.b_item_subgroup_id\n");
        sql.append("where b_item_subgroup.f_item_group_id = '").append(itemGroupid).append("'");

        if (!keyword.isEmpty()) {
            keyword = (!begin_with ? "%" : "") + Gutil.CheckReservedWords(keyword);
            sql.append(" and ( ").append(dbObj.common_name).append(" ilike '").append(keyword).append("%'  ").append(
                    " or ").append(dbObj.nick_name).append(" ilike '").append(keyword).append("%' ").append(
                    " or ").append(dbObj.trade_name).append(" ilike '").append(keyword).append("%' ").append(
                    " or ").append(dbObj.item_id).append(" ilike '").append(keyword).append("%') ");
        }
        sql.append(" and ").append(dbObj.active).append(" = '").append(active).append("' ");
        sql.append(" order by ").append(dbObj.common_name);
        return this.eQueryX(sql.toString(), new String[]{});
    }

    /**
     * @Author : amp
     * @date : 27/02/2549
     * @see : ������¡���Ż�������� �Ż���Դ
     * @param keygroup ��ФӤ�
     * @param pk
     * @return : Vector ��¡�� Item �Ż�����軡�Դ
     * @throws Exception
     */
    public Vector selectItemLabNotSecret(String keygroup, String pk) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).append(" where ");
        if (!keygroup.isEmpty()) {
            sql.append(dbObj.item_group_code_category).append(" = '").append(keygroup).append("' ");
            if (!pk.isEmpty()) {
                sql.append("and ");
            }
        }
        if (!pk.isEmpty()) {
            pk = Gutil.CheckReservedWords(pk);
            sql.append(" ( ").append(dbObj.common_name).append(" ilike '%").append(pk).append("%' ");
            sql.append(" or ").append(dbObj.nick_name).append(" ilike '%").append(pk).append("%' ");
            sql.append(" or ").append(dbObj.trade_name).append(" ilike '%").append(pk).append("%' ");
            sql.append(" or ").append(dbObj.item_id).append(" ilike '%").append(pk).append("%')");
        }
        if (!keygroup.isEmpty() || !pk.isEmpty()) {
            sql.append(" and ");
        }
        sql.append(dbObj.active).append(" = '1' ");
        sql.append(" and ").append(dbObj.secret).append(" <> '1'");
        sql.append(" order by ").append(dbObj.common_name);

        return this.eQueryX(sql.toString(), new String[]{});
    }

    /**
     * @param pk
     * @param active
     * @return
     * @throws Exception
     * @deplicated ����� ���� ITEM ���੾�С����������Ǫ�ѳ�� ��Ф�� key word
     * ������
     *
     */
    public Vector selectItemDrugAndSupply(String pk, String active) throws Exception {
        pk = Gutil.CheckReservedWords(pk);
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ( ").append(dbObj.item_group_code_category).append(" = '1' or ").append(dbObj.item_group_code_category).append(" = '4' ) and ");
        sql.append(" ( ").append(dbObj.common_name).append(" ilike '%").append(pk).append("%' ");
        sql.append(" or ").append(dbObj.nick_name).append(" ilike '%").append(pk).append("%' ");
        sql.append(" or ").append(dbObj.trade_name).append(" ilike '%").append(pk).append("%' )");
        sql.append(" and ( ").append(dbObj.active).append(" = '").append(active).append("' ) ");
        sql.append(" order by ").append(dbObj.common_name);

        return this.eQueryX(sql.toString(), new String[]{});
    }

    public Item selectByPK(String pk) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.pk_field).append(" = '").append(pk).append("'");
        return (Item) eQueryX1(sql.toString(), new String[]{});
    }

    public Vector selectByName(String name) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.item_id).append(" = '").append(Gutil.CheckReservedWords(name)).append("'\n");
        sql.append("order by item_common_name, item_trade_name, item_nick_name");

        return this.eQueryX(sql.toString(), new String[]{});
    }

    public Vector selectAll() throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).append("\n");
        sql.append("order by item_common_name, item_trade_name, item_nick_name");
        return this.eQueryX(sql.toString(), new String[]{});
    }

    public Item selectById(String id) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.item_id).append(" = '").append(Gutil.CheckReservedWords(id)).append("'");

        Item it = (Item) eQueryX1(sql.toString(), new String[]{});
        return it;
    }

    public Vector selectByIds(String... id) throws Exception {
        String pkey = "";
        for (String string : id) {
            pkey += ",'" + string + "'";
        }
        pkey = pkey.substring(1);
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.pk_field).append(" in (").append(pkey).append(") and doctor_warning_icd10 = '1' ");

        Vector items = eQueryX(sql.toString(), new String[]{});
        return items;
    }

    public Vector<Item> selectByItemIds(String[] ids) throws Exception {
        String pkey = "";
        for (String string : ids) {
            pkey += ",'" + string + "'";
        }
        pkey = pkey.substring(1);
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.pk_field).append(" in (").append(pkey).append(") and item_active = '1' order by item_common_name");

        Vector items = eQueryX(sql.toString(), new String[]{});
        return items;
    }

    public Vector selectByCgi(String keygroup) throws Exception {
        return selectByCgi(keygroup, null);
    }

    public Vector selectByCgi(String keygroup, String active) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.item_group_code_category).append(" = '").append(keygroup).append("'\n");
        if (active != null) {
            sql.append(" and ").append(dbObj.active).append(" = '").append(active).append("'\n");
        }
        sql.append("order by item_common_name, item_trade_name, item_nick_name");
        return this.eQueryX(sql.toString(), new String[]{});
    }

    public Vector selectBy16gi(String keygroup) throws Exception {
        return selectBy16gi(keygroup, null);
    }

    public Vector selectBy16gi(String keygroup, String active) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.item_16_group).append(" = '").append(keygroup).append("'\n");
        if (active != null) {
            sql.append(" and ").append(dbObj.active).append(" = '").append(active).append("'\n");
        }
        sql.append("order by item_common_name, item_trade_name, item_nick_name");
        return this.eQueryX(sql.toString(), new String[]{});
    }

    public Item selectMaxByCgi(String keygroup) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.item_group_code_category).append(" = '").append(keygroup).append("' order by ").append(dbObj.item_id).append(" desc limit 1 ");
        Vector v = this.eQueryX(sql.toString(), new String[]{});
        if (v.isEmpty()) {
            return null;
        } else {
            return (Item) v.get(0);
        }
    }

    public Vector selectByBgi(String keygroup) throws Exception {
        return selectByBgi(keygroup, null);
    }

    public Vector selectByBgi(String keygroup, String active) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.item_group_code_billing).append(" = '").append(keygroup).append("' ");
        if (active != null) {
            sql.append(" and ").append(dbObj.active).append(" = '").append(active).append("'\n");
        }
        sql.append("order by item_common_name, item_trade_name, item_nick_name");
        return this.eQueryX(sql.toString(), new String[]{});
    }

    /**
     * For Setup
     *
     * @param pk
     * @param active
     * @return
     * @throws Exception
     */
    public Vector selectAllByPK(String pk, String active) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).append(" where ");
        if (pk.trim().length() != 0) {
            pk = Gutil.CheckReservedWords(pk);
            sql.append("(").append(
                    dbObj.common_name).append(" ilike '%").append(pk).append("%'").append(" or ").append(
                    dbObj.nick_name).append(" ilike '%").append(pk).append("%'").append(" or ").append(
                    dbObj.trade_name).append(" ilike '%").append(pk).append("%'").append(") and ");
        }
        sql.append(dbObj.secret).append(" <> '1' and ").append(dbObj.active).append(" = '").append(active).append("'").append(" order by ").append(dbObj.common_name);

        return this.eQueryX(sql.toString(), new String[]{});
    }

    /**
     * @param sql
     * @return
     * @throws Exception
     * @deprecated henbe unused
     *
     */
    public Vector veQuery(String sql) throws Exception {
        ComboFix p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        int i = 0;
        while (rs.next()) {
            p = new ComboFix();
            p.code = rs.getString(dbObj.pk_field);
            p.name = rs.getString(dbObj.common_name);
            list.add(p);
            i++;
            //if(i>100) break;
        }
        rs.close();
        return list;
    }

    public Vector selectBySpecimenId(String id) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.b_specimen_id).append(" = '").append(id).append("' ");
        return this.eQueryX(sql.toString(), new String[]{});
    }

    public Vector selectByItemGroupId(String itemGroupid, String keyword, boolean begin_with) throws Exception {
        StringBuilder sql = new StringBuilder("select b_item.* from b_item\n");
        sql.append("inner join b_item_subgroup on b_item_subgroup.b_item_subgroup_id = b_item.b_item_subgroup_id\n");
        sql.append("where b_item_subgroup.f_item_group_id = ?");

        if (!keyword.isEmpty()) {
            sql.append(" and ( ").append(dbObj.common_name).append(" ilike ?  ").append(
                    " or ").append(dbObj.nick_name).append(" ilike ? ").append(
                    " or ").append(dbObj.trade_name).append(" ilike ? ").append(
                    " or ").append(dbObj.item_id).append(" ilike ?) ");
        }
        sql.append(" order by ").append(dbObj.common_name);
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql.toString())) {
            int index = 1;
            ePQuery.setString(index++, itemGroupid);
            if (!keyword.isEmpty()) {
                keyword = (!begin_with ? "%" : "") + keyword + "%";
                ePQuery.setString(index++, keyword);
                ePQuery.setString(index++, keyword);
                ePQuery.setString(index++, keyword);
                ePQuery.setString(index++, keyword);
            }
            return this.eQuery(ePQuery.toString());
        }
    }
}

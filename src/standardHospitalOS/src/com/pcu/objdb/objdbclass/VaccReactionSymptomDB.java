/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.objdb.objdbclass;

import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.ComboFix;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tanakrit
 */
public class VaccReactionSymptomDB {

    public ConnectionInf theConnectionInf;

    public VaccReactionSymptomDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public List<ComboFix> selectAll() throws Exception {
        String sql = "select * from f_vaccine_reaction_symptom order by fda_id asc";
        return veQuery(sql);
    }

    public List<ComboFix> selectByName(String name) throws Exception {
        String sql = "select * from f_vaccine_reaction_symptom \n";
        if (name != null && !name.isEmpty()) {
            sql += " where vaccine_reaction_symptom_name ilike ? \n";
        }
        sql += "order by f_vaccine_reaction_symptom_id";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            if (name != null && !name.isEmpty()) {
                ePQuery.setString(index++, "%" + name + "%");
            }
            return veQuery(ePQuery.toString());
        }
    }

    public List<ComboFix> veQuery(String sql) throws Exception {
        ComboFix p;
        List<ComboFix> list = new ArrayList<>();
        try (ResultSet rs = theConnectionInf.eQuery(sql)) {
            while (rs.next()) {
                p = new ComboFix();
                p.code = rs.getString("f_vaccine_reaction_symptom_id");
                p.name = rs.getString("vaccine_reaction_symptom_name");
                list.add(p);
            }
        }
        return list;
    }
}

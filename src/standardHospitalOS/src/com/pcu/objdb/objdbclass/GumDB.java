/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.objdb.objdbclass;

import com.pcu.object.Gum;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class GumDB {

    private final ConnectionInf connectionInf;

    public GumDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public List<Gum> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_gum";
            preparedStatement = connectionInf.ePQuery(sql);
            
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<Gum> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<Gum> list = new ArrayList<Gum>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Gum obj = new Gum();
                obj.setObjectId(rs.getString("f_gum_id"));
                obj.description = rs.getString("description");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (Gum obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }
}

package com.hospital_os.object;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.Persistent;

public class ICD9 extends Persistent implements CommonInf {

    public String icd9_id = "";
    public String description = "";
    public String other_description = "";
    public String cost53 = "";
    public String icd_10_tm = "";
    public String active = "1";

    /**
     * @roseuid 3F658BBB036E
     */
    public ICD9() {
    }

    @Override
    public String getCode() {
        return this.getObjectId();
    }

    @Override
    public String getName() {
        return icd9_id + " : " + description;
    }

    @Override
    public String toString() {
        return getName();
    }
}

insert into f_gui_action (f_gui_action_id, gui_action_name) VALUES ('0813', 'พิมพ์ Sticker ติด Tube');

INSERT INTO s_version VALUES ('9701000000057', '57', 'Hospital OS, Community Edition', '3.9.24', '3.18.170712', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_24.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.24');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.objdb.objdbclass;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.pcu.object.Comactivity;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class ComactivityDB {

    private final ConnectionInf connectionInf;

    public ComactivityDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public List<Comactivity> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_comactivity";
            preparedStatement = connectionInf.ePQuery(sql);
            
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
    
    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (Comactivity obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }

    public List<Comactivity> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<Comactivity> list = new ArrayList<Comactivity>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Comactivity obj = new Comactivity();
                obj.setObjectId(rs.getString("f_comactivity_id"));
                obj.code = rs.getString("code");
                obj.description = rs.getString("description");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<Comactivity> select(String keyword) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_comactivity where upper(code) like upper(?) or upper(description) like upper(?)";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, "%" + keyword + "%");
            preparedStatement.setString(2, "%" + keyword + "%");
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
}

--แฟ้ม ADMISSION เพิ่ม GROUPER_VERSION เลข Version ของโปรแกรม Grouper ใช้ในการคํานวณข้อมูลผู้ป่วยใน
alter table s_tdrg_version add column tdrg_grouper_number character varying(255)  NOT NULL default '0';
update s_tdrg_version set tdrg_grouper_number = regexp_replace(version_tdrg_description, '[^0-9.]+', '');

INSERT INTO s_script_update_log values ('DRG_Module','update_tdrg_002.sql',(select current_date) || ','|| (select current_time),'Alter column');
CREATE OR REPLACE FUNCTION text_to_timestamp(text) RETURNS timestamp language plpgsql immutable as $$
BEGIN
        return case when length($1) >= 10 and ((substr($1,1,4)::int-543)||substr($1,5))::timestamp is not null then ((substr($1,1,4)::int-543)||substr($1,5))::timestamp end;
exception when others then
        return null;
END;$$;

CREATE TABLE IF NOT EXISTS f_heart_risk_type
(
	f_heart_risk_type_id		integer ,
	description   				text ,
	CONSTRAINT f_heart_risk_type_pk PRIMARY KEY (f_heart_risk_type_id)
);

CREATE TABLE IF NOT EXISTS t_heart_risk 
(
	t_heart_risk_id		serial,
	f_heart_risk_type_id	integer NOT NULL,	
	t_visit_id 		character varying(50) NOT NULL,
	assessment_datetime 	timestamp without time zone NOT NULL,
	parameter		json,
	result			json,
	active 			character varying(1) NOT NULL default '1',  
	record_datetime 		timestamp without time zone NOT NULL default current_timestamp,
	user_record_id 			character varying(50) NOT NULL,
	update_datetime 		timestamp without time zone NOT NULL default current_timestamp,
	user_update_id 			character varying(50) NOT NULL,
	
	CONSTRAINT t_heart_risk_pk PRIMARY KEY (t_heart_risk_id),  
	CONSTRAINT t_heart_risk_visit_fk FOREIGN KEY (t_visit_id) 
		REFERENCES t_visit (t_visit_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT t_heart_risk_type_fk FOREIGN KEY (f_heart_risk_type_id) 
		REFERENCES f_heart_risk_type (f_heart_risk_type_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT t_heart_risk_assessment_datetime_uk UNIQUE(t_visit_id,assessment_datetime)
);

CREATE TABLE IF NOT EXISTS b_map_heart_risk_user
(
	b_map_heart_risk_user_id 	serial,
	b_employee_id 				character varying(50) NOT NULL,
	record_datetime 			timestamp without time zone NOT NULL default current_timestamp,
	user_record_id 				character varying(50) NOT NULL,	
	CONSTRAINT b_map_heart_risk_user_pk PRIMARY KEY (b_map_heart_risk_user_id),  
	CONSTRAINT b_map_heart_risk_user_employee_fk FOREIGN KEY (b_employee_id) 
		REFERENCES b_employee (b_employee_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT b_map_heart_risk_user_employee_uk UNIQUE(b_employee_id)
);

CREATE TABLE IF NOT EXISTS b_map_heart_risk_item_lab
(
	b_map_heart_risk_item_lab_id 	int,
	lab_code						character varying(5) NOT NULL,
	lab_description					text NOT NULL,
	b_item_id 						character varying(50),
	update_datetime 				timestamp without time zone NOT NULL default current_timestamp,
	user_update_id 					character varying(50) NOT NULL,
	
	CONSTRAINT b_map_heart_risk_item_lab_pk PRIMARY KEY (b_map_heart_risk_item_lab_id),  
	CONSTRAINT b_map_heart_risk_item_lab_item_fk FOREIGN KEY (b_item_id) 
		REFERENCES b_item (b_item_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT b_map_heart_risk_item_lab_code_uk UNIQUE(lab_code),
	CONSTRAINT b_map_heart_risk_item_lab_item_uk UNIQUE(b_item_id)
);

INSERT INTO b_map_heart_risk_item_lab (b_map_heart_risk_item_lab_id,lab_code,lab_description,user_update_id) 
VALUES (1,'32501','โคเรสเตอรอลรวม (Cholesterol)','')
,(2,'32503','โคเรสเตอรอลดี (HDL)','')
,(3,'32504','โคเรสเตอรอลเลว (LDL)','');

CREATE OR REPLACE FUNCTION patient_last_height (patient_id text ,assessment_datetime timestamp)
RETURNS JSON AS $$
 select
   json_build_object('value',t_visit_vital_sign.visit_vital_sign_height::numeric
     , 'record_datetime',text_to_timestamp(t_visit_vital_sign.record_date||','||t_visit_vital_sign.record_time)  
     ) as height
 from t_patient inner join t_visit on t_patient.t_patient_id =t_visit.t_patient_id
   inner join t_visit_vital_sign on t_visit.t_visit_id = t_visit_vital_sign.t_visit_id
   inner join (
   select 
     t_patient.t_patient_id
     ,max(text_to_timestamp(t_visit_vital_sign.record_date||','||t_visit_vital_sign.record_time)) as record_datetime
   from t_patient inner join t_visit on t_patient.t_patient_id =t_visit.t_patient_id
     inner join t_visit_vital_sign on t_visit.t_visit_id = t_visit_vital_sign.t_visit_id
   where
     t_patient.patient_active = '1'
     and t_visit.f_visit_status_id <> '4'
     and t_visit_vital_sign.visit_vital_sign_active = '1'
     and t_visit_vital_sign.visit_vital_sign_height ~ '^([0-9]+\.?[0-9]*|\.[0-9]+)$' 
     and t_patient.t_patient_id = patient_id
                    and text_to_timestamp(t_visit_vital_sign.record_date||','||t_visit_vital_sign.record_time) between (assessment_datetime - interval '1 year') and assessment_datetime 
   group by
     t_patient.t_patient_id ) as last_vital_sign_height
   on t_patient.t_patient_id = last_vital_sign_height.t_patient_id
     and text_to_timestamp(t_visit_vital_sign.record_date||','||t_visit_vital_sign.record_time) = last_vital_sign_height.record_datetime
 where
   t_patient.patient_active = '1'
   and t_visit.f_visit_status_id <> '4'
   and t_patient.t_patient_id = patient_id
            and text_to_timestamp(t_visit_vital_sign.record_date||','||t_visit_vital_sign.record_time) between (assessment_datetime - interval '1 year') and assessment_datetime ;
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION patient_last_sbp (patient_id text,assessment_datetime timestamp)
RETURNS JSON AS $$
        select
               json_build_object('value' ,case when position( '/' in t_visit_vital_sign.visit_vital_sign_blood_presure) > 0
											then case when substr(t_visit_vital_sign.visit_vital_sign_blood_presure,1,position( '/' in t_visit_vital_sign.visit_vital_sign_blood_presure)-1)~'^([0-9]+\.?[0-9]*|\.[0-9]+)$' 
															then substr(t_visit_vital_sign.visit_vital_sign_blood_presure,1,position( '/' in t_visit_vital_sign.visit_vital_sign_blood_presure)-1)::numeric::int end
											end 
                        , 'record_datetime' ,text_to_timestamp(t_visit_vital_sign.record_date||','||t_visit_vital_sign.record_time)  
						) as sbp
        from t_patient inner join t_visit on t_patient.t_patient_id =t_visit.t_patient_id
                inner join t_visit_vital_sign on t_visit.t_visit_id = t_visit_vital_sign.t_visit_id
                inner join (
                select 
                        t_patient.t_patient_id
                        ,max(text_to_timestamp(t_visit_vital_sign.record_date||','||t_visit_vital_sign.record_time)) as record_datetime
                from t_patient inner join t_visit on t_patient.t_patient_id =t_visit.t_patient_id
                        inner join t_visit_vital_sign on t_visit.t_visit_id = t_visit_vital_sign.t_visit_id
                where
                        t_patient.patient_active = '1'
                        and t_visit.f_visit_status_id <> '4'
                        and t_visit_vital_sign.visit_vital_sign_active = '1'
                        and t_visit_vital_sign.visit_vital_sign_blood_presure <> ''
                        and t_patient.t_patient_id = patient_id
                        and text_to_timestamp(t_visit_vital_sign.record_date||','||t_visit_vital_sign.record_time) between (assessment_datetime - interval '1 year') and assessment_datetime 
                group by
                        t_patient.t_patient_id ) as last_vital_sign_blood_presure
                on t_patient.t_patient_id = last_vital_sign_blood_presure.t_patient_id
                        and text_to_timestamp(t_visit_vital_sign.record_date||','||t_visit_vital_sign.record_time) = last_vital_sign_blood_presure.record_datetime
        where
                t_patient.patient_active = '1'
                and t_visit.f_visit_status_id <> '4'
                and t_patient.t_patient_id = patient_id
                and text_to_timestamp(t_visit_vital_sign.record_date||','||t_visit_vital_sign.record_time) between (assessment_datetime - interval '1 year') and assessment_datetime ;
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION patient_last_waistline (patient_id text,assessment_datetime timestamp)
RETURNS JSON AS $$
        select
               json_build_object('value' ,t_visit_vital_sign.visit_vital_sign_waistline_inch::numeric
								, 'record_datetime' ,text_to_timestamp(t_visit_vital_sign.record_date||','||t_visit_vital_sign.record_time)  
							) as waistline
        from t_patient inner join t_visit on t_patient.t_patient_id =t_visit.t_patient_id
                inner join t_visit_vital_sign on t_visit.t_visit_id = t_visit_vital_sign.t_visit_id
                inner join (
                select 
                        t_patient.t_patient_id
                        ,max(text_to_timestamp(t_visit_vital_sign.record_date||','||t_visit_vital_sign.record_time)) as record_datetime
                from t_patient inner join t_visit on t_patient.t_patient_id =t_visit.t_patient_id
                        inner join t_visit_vital_sign on t_visit.t_visit_id = t_visit_vital_sign.t_visit_id
                where
                        t_patient.patient_active = '1'
                        and t_visit.f_visit_status_id <> '4'
                        and t_visit_vital_sign.visit_vital_sign_active = '1'
                        and t_visit_vital_sign.visit_vital_sign_waistline_inch ~ '^([0-9]+\.?[0-9]*|\.[0-9]+)$' 
                        and t_patient.t_patient_id = patient_id
                        and text_to_timestamp(t_visit_vital_sign.record_date||','||t_visit_vital_sign.record_time) between (assessment_datetime - interval '1 year') and assessment_datetime 
                group by
                        t_patient.t_patient_id ) as last_vital_sign_blood_presure
                on t_patient.t_patient_id = last_vital_sign_blood_presure.t_patient_id
                        and text_to_timestamp(t_visit_vital_sign.record_date||','||t_visit_vital_sign.record_time) = last_vital_sign_blood_presure.record_datetime
        where
                t_patient.patient_active = '1'
                and t_visit.f_visit_status_id <> '4'
                and t_patient.t_patient_id = patient_id
                and text_to_timestamp(t_visit_vital_sign.record_date||','||t_visit_vital_sign.record_time) between (assessment_datetime - interval '1 year') and assessment_datetime ;
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION patient_last_cholesterol (patient_id text ,assessment_datetime timestamp)
RETURNS JSON AS $$
        select
               json_build_object('value' ,t_result_lab.result_lab_value::numeric
                        , 'record_datetime' ,text_to_timestamp(t_order.order_report_date_time) 
						) as cholesterol
        from t_patient inner join t_visit on t_patient.t_patient_id =t_visit.t_patient_id
                inner join t_order on t_visit.t_visit_id = t_order.t_visit_id
                inner join t_result_lab on t_order.t_order_id = t_result_lab.t_order_id
                inner join (
                select
                        t_patient.t_patient_id
                        ,max(text_to_timestamp(t_order.order_report_date_time)) as record_datetime
                from t_patient inner join t_visit on t_patient.t_patient_id =t_visit.t_patient_id
                        inner join t_order on t_visit.t_visit_id = t_order.t_visit_id
                        inner join t_result_lab on t_order.t_order_id = t_result_lab.t_order_id
                where
                        t_patient.patient_active = '1'
                        and t_visit.f_visit_status_id <> '4'
                        and t_order.f_order_status_id = '4'
                        and t_result_lab.b_item_id in (select b_item_id from b_map_heart_risk_item_lab where b_map_heart_risk_item_lab_id = 1)
                        and t_result_lab.result_lab_value ~ '^([0-9]+\.?[0-9]*|\.[0-9]+)$' 
                        and t_patient.t_patient_id = patient_id
                        and text_to_timestamp(t_order.order_report_date_time) between (assessment_datetime - interval '1 year') and assessment_datetime 
                group by
                        t_patient.t_patient_id) as last_cholesterol 
                on  t_patient.t_patient_id = last_cholesterol.t_patient_id
                        and text_to_timestamp(t_order.order_report_date_time) = last_cholesterol.record_datetime
        where
                t_patient.patient_active = '1'
                and t_visit.f_visit_status_id <> '4'
                and t_order.f_order_status_id = '4'
                and t_result_lab.b_item_id in (select b_item_id from b_map_heart_risk_item_lab where b_map_heart_risk_item_lab_id = 1)
                and t_result_lab.result_lab_value ~ '^([0-9]+\.?[0-9]*|\.[0-9]+)$' 
                and t_patient.t_patient_id = patient_id
                and text_to_timestamp(t_order.order_report_date_time) between (assessment_datetime - interval '1 year') and assessment_datetime  ;
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION patient_last_ldl (patient_id text ,assessment_datetime timestamp)
RETURNS JSON AS $$
        select
               json_build_object('value' ,t_result_lab.result_lab_value::numeric
                        , 'record_datetime' ,text_to_timestamp(t_order.order_report_date_time) 
						) as ldl
        from t_patient inner join t_visit on t_patient.t_patient_id =t_visit.t_patient_id
                inner join t_order on t_visit.t_visit_id = t_order.t_visit_id
                inner join t_result_lab on t_order.t_order_id = t_result_lab.t_order_id
                inner join (
                select
                        t_patient.t_patient_id
                        ,max(text_to_timestamp(t_order.order_report_date_time)) as record_datetime
                from t_patient inner join t_visit on t_patient.t_patient_id =t_visit.t_patient_id
                        inner join t_order on t_visit.t_visit_id = t_order.t_visit_id
                        inner join t_result_lab on t_order.t_order_id = t_result_lab.t_order_id
                where
                        t_patient.patient_active = '1'
                        and t_visit.f_visit_status_id <> '4'
                        and t_order.f_order_status_id = '4'
                        and t_result_lab.b_item_id in (select b_item_id from b_map_heart_risk_item_lab where b_map_heart_risk_item_lab_id = 3)
                        and t_result_lab.result_lab_value ~ '^([0-9]+\.?[0-9]*|\.[0-9]+)$' 
                        and t_patient.t_patient_id = patient_id
                        and text_to_timestamp(t_order.order_report_date_time) between (assessment_datetime - interval '1 year') and assessment_datetime 
                group by
                        t_patient.t_patient_id) as last_ldl
                on  t_patient.t_patient_id = last_ldl.t_patient_id
                        and text_to_timestamp(t_order.order_report_date_time) = last_ldl.record_datetime
        where
                t_patient.patient_active = '1'
                and t_visit.f_visit_status_id <> '4'
                and t_order.f_order_status_id = '4'
                and t_result_lab.b_item_id in (select b_item_id from b_map_heart_risk_item_lab where b_map_heart_risk_item_lab_id = 3)
                and t_result_lab.result_lab_value ~ '^([0-9]+\.?[0-9]*|\.[0-9]+)$' 
                and t_patient.t_patient_id = patient_id
                and text_to_timestamp(t_order.order_report_date_time) between (assessment_datetime - interval '1 year') and assessment_datetime  ;
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION patient_last_hdl (patient_id text ,assessment_datetime timestamp )
RETURNS JSON AS $$
        select
               json_build_object('value' ,t_result_lab.result_lab_value::numeric
                        , 'record_datetime' ,text_to_timestamp(t_order.order_report_date_time) 
						) as hdl
        from t_patient inner join t_visit on t_patient.t_patient_id =t_visit.t_patient_id
                inner join t_order on t_visit.t_visit_id = t_order.t_visit_id
                inner join t_result_lab on t_order.t_order_id = t_result_lab.t_order_id
                inner join (
                select
                        t_patient.t_patient_id
                        ,max(text_to_timestamp(t_order.order_report_date_time)) as record_datetime
                from t_patient inner join t_visit on t_patient.t_patient_id =t_visit.t_patient_id
                        inner join t_order on t_visit.t_visit_id = t_order.t_visit_id
                        inner join t_result_lab on t_order.t_order_id = t_result_lab.t_order_id
                where
                        t_patient.patient_active = '1'
                        and t_visit.f_visit_status_id <> '4'
                        and t_order.f_order_status_id = '4'
                        and t_result_lab.b_item_id in (select b_item_id from b_map_heart_risk_item_lab where b_map_heart_risk_item_lab_id = 2)
                        and t_result_lab.result_lab_value ~ '^([0-9]+\.?[0-9]*|\.[0-9]+)$' 
                        and t_patient.t_patient_id = patient_id
                        and text_to_timestamp(t_order.order_report_date_time) between (assessment_datetime - interval '1 year') and assessment_datetime 
                group by
                        t_patient.t_patient_id) as last_hdl
                on  t_patient.t_patient_id = last_hdl.t_patient_id
                        and text_to_timestamp(t_order.order_report_date_time) = last_hdl.record_datetime
        where
                t_patient.patient_active = '1'
                and t_visit.f_visit_status_id <> '4'
                and t_order.f_order_status_id = '4'
                and t_result_lab.b_item_id in (select b_item_id from b_map_heart_risk_item_lab where b_map_heart_risk_item_lab_id = 2)
                and t_result_lab.result_lab_value ~ '^([0-9]+\.?[0-9]*|\.[0-9]+)$' 
                and t_patient.t_patient_id = patient_id
                and text_to_timestamp(t_order.order_report_date_time) between (assessment_datetime - interval '1 year') and assessment_datetime  ;
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION heart_risk_visit (visit_id text) 
RETURNS TABLE( t_visit_id text , t_heart_risk_id int ,  f_heart_risk_type_id int , heart_risk_type text ,  assessment_datetime timestamp ,heart_risk text,  heart_risk_result text , heart_risk_advice text ) 
AS $$
DECLARE
        patient_age int :=  (select case when  text_to_timestamp(t_patient.patient_birthday) is not null 
                                                        then EXTRACT(YEAR FROM age(text_to_timestamp(t_visit.visit_begin_visit_time),text_to_timestamp(t_patient.patient_birthday))) end::int AS patient_age 
                                    from t_visit inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
                                    where
                                            t_visit.t_visit_id = visit_id ) ; 
BEGIN
    RETURN QUERY
         select
                t_heart_risk.t_visit_id::text as t_visit_id
                ,t_heart_risk.t_heart_risk_id as t_heart_risk_id
                ,t_heart_risk.f_heart_risk_type_id as f_heart_risk_type_id
                ,f_heart_risk_type.description as heart_risk_type
                ,t_heart_risk.assessment_datetime as assessment_datetime
                ,case when patient_age not between 35 and 70 
                        then (t_heart_risk.result->>'retetype_label')::text
                        else (t_heart_risk.result->>'sc5')::text 
                        end as heart_risk

                ,case when patient_age not between 35 and 70 
                        then 
                'ค่าความเสี่ยงที่ได้คือ : '||(t_heart_risk.result->>'rate')::text        
                        else  
                'ความเสี่ยงต่อการเกิดโรคเส้นเลือดหัวใจและหลอดเลือดในระยะเวลา 10 ปีของท่านเท่ากับ '||(t_heart_risk.result->>'sc2')::text||'% '||(t_heart_risk.result->>'sc5')::text
                ||chr(10)||(t_heart_risk.result->>'sc1')::text||' ของคนไทยเพศเดียวกัน อายุเท่ากัน และปราศจากปัจจัยเสี่ยง'
                        end as heart_risk_result

                ,case when patient_age not between 35 and 70 
                        then 
                 (t_heart_risk.result->>'retetype_label')||chr(10)||(t_heart_risk.result->>'demeanour')
                        else 
                (t_heart_risk.result->>'sc4') 
                        end as  heart_risk_advice
              --  ,t_heart_risk.result
        from t_heart_risk 
                inner join (select
                                    t_heart_risk.t_visit_id
                                    ,max(t_heart_risk.assessment_datetime) as assessment_datetime
                                 from t_heart_risk
                                 where
                                    t_heart_risk.f_heart_risk_type_id = (case when patient_age not between 35 and 70 then 1 else 2 end)
                                    and t_heart_risk.active = '1'
                                    and t_heart_risk.t_visit_id = visit_id
                                group by
                                    t_heart_risk.t_visit_id ) as last_heart_risk
                on t_heart_risk.t_visit_id = last_heart_risk.t_visit_id
                        and t_heart_risk.assessment_datetime = last_heart_risk.assessment_datetime
                left join f_heart_risk_type on t_heart_risk.f_heart_risk_type_id = f_heart_risk_type.f_heart_risk_type_id
        where
                t_heart_risk.f_heart_risk_type_id = (case when patient_age not between 35 and 70 then 1 else 2 end)
                and t_heart_risk.active = '1'
                and t_heart_risk.t_visit_id = visit_id ;
END;
$$ LANGUAGE plpgsql;

-- create s_hrisk_version
CREATE TABLE s_hrisk_version (
    version_id            varchar(255) NOT NULL,
    version_number            	varchar(255) NULL,
    version_description       	varchar(255) NULL,
    version_application_number	varchar(255) NULL,
    version_database_number   	varchar(255) NULL,
    version_update_time       	varchar(255) NULL 
);

ALTER TABLE s_hrisk_version
	ADD CONSTRAINT s_hrisk_version_pkey
	PRIMARY KEY (version_id);

INSERT INTO s_hrisk_version VALUES ('1', '1', 'Heart Risk Module', '1.0.0', '1.0.0', (select current_date) || ','|| (select current_time));

INSERT INTO s_script_update_log values ('Heart_Risk_Module','update_hrisk_001.sql',(select current_date) || ','|| (select current_time),'Initialize Heart Risk Module');

/*
 * PanelVitalSign.java
 *
 * Created on 18 ��jTextAreaPhysicalExam�Ҥ� 2546, 9:45 �.
 */
package com.hosv3.gui.panel.transaction;

import com.hospital_os.object.Appointment;
import com.hospital_os.object.Authentication;
import com.hospital_os.object.CategoryGroup;
import com.hospital_os.object.DxTemplate;
import com.hospital_os.object.Employee;
import com.hospital_os.object.GuideAfterDxTransaction;
import com.hospital_os.object.MapVisitDx;
import com.hospital_os.object.Patient;
import com.hospital_os.object.PhysicalExam;
import com.hospital_os.object.PhysicalExamNan;
import com.hospital_os.object.PrimarySymptom;
import com.hospital_os.object.Sex;
import com.hospital_os.object.Visit;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hospital_os.utility.CellRendererHos;
import com.hospital_os.utility.ComboboxModel;
import com.hospital_os.utility.Gutil;
import com.hospital_os.utility.TaBleModel;
import com.hosv3.control.GHospitalSuit;
import com.hosv3.control.GPatientSuit;
import com.hosv3.control.HosControl;
import com.hosv3.control.lookup.BodyOrganLookup;
import com.hosv3.control.lookup.DxTemplateLookup;
import com.hosv3.control.lookup.GuideLookup;
import com.hosv3.control.lookup.VitalTemplateLookup;
import com.hosv3.gui.component.PanelSeverity;
import com.hosv3.gui.component.PanelTrama;
import com.hosv3.gui.component.PanelTransportation;
import com.hosv3.gui.dialog.DialogChooseICD10FromTemplate;
import com.hosv3.gui.dialog.DialogReDiag;
import com.hosv3.gui.dialog.HosDialog;
import com.hosv3.gui.dialog.PanelHealthEducation;
import com.hosv3.gui.dialog.PanelNCD;
import com.hosv3.gui.dialog.PanelPatientHistory;
import com.hosv3.gui.dialog.PanelPhysicalExam;
import com.hosv3.gui.dialog.PanelRegisterNCD;
import com.hosv3.gui.dialog.PanelWound;
import com.hosv3.gui.panel.transaction.inf.PanelButtonPluginManager;
import com.hosv3.gui.panel.transaction.inf.PanelButtonPluginProvider;
import com.hosv3.object.HosObject;
import com.hosv3.subject.HosSubject;
import com.hosv3.usecase.transaction.ManageDiagnosisResp;
import com.hosv3.usecase.transaction.ManageLabXrayResp;
import com.hosv3.usecase.transaction.ManagePatientResp;
import com.hosv3.usecase.transaction.ManageReDxResp;
import com.hosv3.usecase.transaction.ManageServiceLoader;
import com.hosv3.usecase.transaction.ManageVisitResp;
import com.hosv3.usecase.transaction.ManageVitalResp;
import com.hosv3.utility.Constant;
import com.hosv3.utility.DateUtil;
import com.hosv3.utility.GuiLang;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JPopupMenu;
import javax.swing.ListSelectionModel;
import javax.swing.text.JTextComponent;

/**
 *
 *
 * @author henbe
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class PanelVitalSign extends javax.swing.JPanel implements
        ManageVisitResp,
        ManageVitalResp,
        ManagePatientResp,
        ManageLabXrayResp,
        ManageReDxResp,
        ManageDiagnosisResp,
        ManageServiceLoader {

    private static final long serialVersionUID = 0;
    private HosObject theHO;
    private HosControl theHC;
    private HosSubject theHS;
    private UpdateStatus theUS;
    private HosDialog theHD;
    protected DialogChooseICD10FromTemplate theDCFT;
    protected PanelPhysicalExam thePPE;
    protected PanelWound thePW;
    protected PanelPatientHistory thePPH;
    protected PanelHealthEducation thePHE;
    protected PanelNCD thePNCD;
    protected PanelRegisterNCD thePRNCD;
    protected String[] column_jTableListPhyEx = {"��ҧ���", "�š�õ�Ǩ"};
    protected String[] col_jTableVitalSign = {"�ѹ-���ҵ�Ǩ"};
    protected String[] col_jTablePrimarySymptom = {"�ѹ-���Һѹ�֡"};
    protected String[] col_jTableAllergy = {"������", "�ҡ�÷����"};
    protected String[] col_jTableDx = {"��¡�� Dx"};
    private final CellRendererHos dateRender = new CellRendererHos(CellRendererHos.DATE_TIME);
    private final CellRendererHos theCellRendererTooltip = new CellRendererHos(CellRendererHos.TOOL_TIP_TEXT);
    private Vector vPrimarySymptom;
    private Vector vPhysicalExam;
    private Vector vHealthEducation;
    private Vector vPhysicalExamNan;
    private Visit theVisit;
    private Patient thePatient;
    private PrimarySymptom thePrimarySymptom;
    private Vector vGuide;
    private GuideAfterDxTransaction theGuideAfterDxTransaction;
    /*
     * ��ùѴ sumo 07/08/2549
     */
    private Appointment theAppointment;
    /**
     * �繤�Ǣͧ Visit
     */
    private Vector vCalDateAppointment;
    private GHospitalSuit theGHS;
    private GPatientSuit theGPS;
    protected DialogReDiag dialogReDiag;
    private List<MapVisitDx> vMapVisitDx;

    /**
     * Creates new form PanelVitalSign
     */
    public PanelVitalSign() {
        initComponents();
        jTablePrimarySymptom.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        //����ͧ�������Һѹ�ѡ��ҹ lost focus ��ҹ���������͹�����ѹ��͹˹��
        jButtonSaveDx.setVisible(false);
        this.jPanelHide.setVisible(false);
        // set popup
        this.setPopup();
    }

    /*
     * neung �ӡ�õ�Ǩ�ͺ��Ҽ�������� ����������ö���������ҧ
     * ��зӡ��૵visitble��������
     */
    private void setAuthentication(Employee theEmployee) {
        this.setEnabled(theHO.theGActionAuthV.isWriteTabVitalSign());
        jCheckBoxPEText.setEnabled(theHO.theGActionAuthV.isWritePVitalPhysicalExam());
        this.jTextAreaPhysicalExam.setEnabled(theHO.theGActionAuthV.isWritePVitalPhysicalExam());
        this.jButtonPhysicalExam.setVisible(theHO.theGActionAuthV.isWritePVitalPhysicalExam());
        this.jButtonSaveDx.setVisible(theHO.theGActionAuthV.isWritePVitalDiagnosis());
        this.jTableDx.setEnabled(theHO.theGActionAuthV.isWritePVitalDiagnosis());
        this.jComboBoxDxTemplate.setEnabled(theHO.theGActionAuthV.isWritePVitalDiagnosis());
        this.jButtonDeleteDx.setEnabled(theHO.theGActionAuthV.isWritePVitalDiagnosis());
        jButtonReDx.setVisible(theHO.theGActionAuthV.isWritePVitalDiagnosis());
        // set visible button
        btnViewLISResult.setVisible(theHC.theLookupControl.readOption().enable_lis_result_viewer.equals("1")
                && theHC.theHO.theGActionAuthV.isReadPVitalViewLISResultFile());
        btnViewEKGResult.setVisible(theHC.theLookupControl.readOption().enable_ekg_result_viewer.equals("1")
                && theHC.theHO.theGActionAuthV.isReadPVitalViewEKGResultFile());
        btnMedCer.setVisible(
                theHO.theGActionAuthV.isReadPVitalCreateMedicalCertificate()
                && theHO.theGActionAuthV.isWritePVitalCreateMedicalCertificate());
        btnProgressNote.setVisible(theHC.theHO.theGActionAuthV.isReadPVitalProgressNote());
    }

    public void setControl(HosControl hc, GPatientSuit gps, GHospitalSuit ghs, UpdateStatus us) {
        theHC = hc;
        theHO = hc.theHO;
        theHS = hc.theHS;
        theUS = us;
        theGPS = gps;
        theGHS = ghs;
        this.pDVitalSign1.setControl(hc, us);
        theHS.theVitalSubject.attachManageVital(this);
        theHS.thePatientSubject.attachManagePatient(this);
        theHS.theVisitSubject.attachManageVisit(this);
        theHS.theResultSubject.attachManageXray(this);
        theHS.theResultSubject.attachManageLab(this);
        theHS.theReDxSubject.attachManage(this);
        theHS.theDiagnosisSubject.attachManageDiagnosis(this);
        theHS.theServiceLoaderSubject.attachManage(this);

        jTextAreaMainSymptom.setControl(new VitalTemplateLookup(theHC.theLookupControl));
        jTextAreaMainSymptom.setJFrame(theUS.getJFrame());
        theHS.theBalloonSubject.attachBalloon(jTextAreaMainSymptom);

        jTextAreaCurrent.setControl(new VitalTemplateLookup(theHC.theLookupControl));
        jTextAreaCurrent.setJFrame(theUS.getJFrame());
        theHS.theBalloonSubject.attachBalloon(jTextAreaCurrent);

        jTextAreaGuide.setSelectAround("", "\n");
        jTextAreaGuide.setControl(new GuideLookup(theHC.theLookupControl));
        jTextAreaGuide.setJFrame(theUS.getJFrame());
        theHS.theBalloonSubject.attachBalloon(jTextAreaGuide);

        jTextAreaPhysicalExam.setSelectAround("", ": ");
        jTextAreaPhysicalExam.setControl(new BodyOrganLookup(theHC.theLookupControl));
        jTextAreaPhysicalExam.setJFrame(theUS.getJFrame());
        theHS.theBalloonSubject.attachBalloon(jTextAreaPhysicalExam);

        setAuthentication(theHO.theEmployee);
        initComboBox();
        setLanguage(null);
        setHosObject(theHO);
    }

    public void initComboBox() {
        vCalDateAppointment = theHC.theLookupControl.listCalDateAppointment();
        ComboboxModel.initComboBox(jComboBoxAppointment, vCalDateAppointment);
        this.jComboBoxDxTemplate.setControl(new DxTemplateLookup(theHC.theLookupControl), true);
        List<DxTemplate> vDxTemplate = theHC.theLookupControl.listDxTemplateByName("");
        ComboboxModel.initComboBox(jComboBoxDxTemplate, vDxTemplate);
        jComboBoxDxTemplate.setSelectedIndex(-1);
        String command = theHC.theLookupControl.readOption().b2_command;
        String ttt = theHC.theLookupControl.readOption().b2_description;
        String icon = theHC.theLookupControl.readOption().b2_icon;
        this.jButtonB2.setVisible(!command.isEmpty());
        this.jButtonB2.setToolTipText(ttt);
        if (!icon.isEmpty()) {
            this.jButtonB2.setIcon(new javax.swing.ImageIcon("resources/image/" + icon));
            this.jButtonB2.setText("");
        }
        ////////////////////////////////////////////////////////////////
        dateComboBoxIllness.setEditable(true);
    }

    public void setDialog(HosDialog hd) {
        theHD = hd;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        buttonGroupDrugAllergy = new javax.swing.ButtonGroup();
        jSplitPane1 = new javax.swing.JSplitPane();
        jPanel3 = new javax.swing.JPanel();
        jPanelDescription = new javax.swing.JPanel();
        jPanelHide = new javax.swing.JPanel();
        jButtonViewPTCard = new javax.swing.JButton();
        jButtonViewSpecialClinic = new javax.swing.JButton();
        jButtonViewDentistry = new javax.swing.JButton();
        jButtonViewOBGYNCard = new javax.swing.JButton();
        jScrollPanePriSym = new javax.swing.JScrollPane();
        jTablePrimarySymptom = new com.hosv3.gui.component.HJTableSort();
        jPanel9 = new javax.swing.JPanel();
        jButtonAddPrimarySymptom = new javax.swing.JButton();
        jButtonDelPrimarySymptom = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        jCheckBoxBalloon = new javax.swing.JCheckBox();
        panelMainSymptom = new javax.swing.JPanel();
        jScrollPaneMainSymptom = new javax.swing.JScrollPane();
        jTextAreaMainSymptom = new com.hosv3.gui.component.BalloonTextArea();
        currentIllnessPanel = new javax.swing.JPanel();
        jScrollPaneCurrent = new javax.swing.JScrollPane();
        jTextAreaCurrent = new com.hosv3.gui.component.BalloonTextArea();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        lblDiffDatetime = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        dateComboBoxIllness = new com.hospital_os.utility.DateComboBox();
        jLabel9 = new javax.swing.JLabel();
        timeTextFieldIllness = new com.hospital_os.utility.TimeTextField();
        btnIllnessAddr = new javax.swing.JButton();
        panelDatatime = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        dateComboBoxCheck = new com.hospital_os.utility.DateComboBox();
        jLabel6 = new javax.swing.JLabel();
        timeTextFieldCheck = new com.hospital_os.utility.TimeTextField();
        jLabel10 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jButtonRegiterNCD = new javax.swing.JButton();
        jButtonViewNCD = new javax.swing.JButton();
        jButtonAccident = new javax.swing.JButton();
        jButtonLabResult = new javax.swing.JButton();
        jButtonViewHistory = new javax.swing.JButton();
        jPanel14 = new javax.swing.JPanel();
        rbtnDrugAllergy1 = new javax.swing.JRadioButton();
        rbtnDrugAllergy2 = new javax.swing.JRadioButton();
        rbtnDrugAllergy3 = new javax.swing.JRadioButton();
        jCheckBoxPregnant = new javax.swing.JCheckBox();
        panelLMP = new javax.swing.JPanel();
        chkbLMP = new javax.swing.JCheckBox();
        dateComboBoxLMP = new com.hospital_os.utility.DateComboBox();
        btnViewLISResult = new javax.swing.JButton();
        btnViewEKGResult = new javax.swing.JButton();
        jPanel12 = new javax.swing.JPanel();
        jButtonSaveVs = new javax.swing.JButton();
        jButtonChronic = new javax.swing.JButton();
        jButtonSurveil = new javax.swing.JButton();
        jButtonDeath = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        jPanel15 = new javax.swing.JPanel();
        lblTxtTransportation = new javax.swing.JLabel();
        lblShowTransportation = new javax.swing.JLabel();
        jPanel16 = new javax.swing.JPanel();
        lblTxtTrama = new javax.swing.JLabel();
        lblShowTrama = new javax.swing.JLabel();
        jPanel17 = new javax.swing.JPanel();
        lblTxtSeverity = new javax.swing.JLabel();
        lblShowSeverity = new javax.swing.JLabel();
        jPanel21 = new javax.swing.JPanel();
        jButtonSavePrimarySymptom = new javax.swing.JButton();
        pDVitalSign1 = new com.hosv3.gui.panel.detail.PDVitalSign();
        jSplitPane2 = new javax.swing.JSplitPane();
        jPanel20 = new javax.swing.JPanel();
        jScrollPanePhyEx1 = new javax.swing.JScrollPane();
        jTextAreaPhysicalExam = new com.hosv3.gui.component.BalloonTextArea();
        jPanel10 = new javax.swing.JPanel();
        jButtonPhysicalExam = new javax.swing.JButton();
        jButtonB2 = new javax.swing.JButton();
        jCheckBoxPEText = new javax.swing.JCheckBox();
        btnFootExam = new javax.swing.JButton();
        btnEyesExam = new javax.swing.JButton();
        btnEyesExam1 = new javax.swing.JButton();
        btnAudiometry = new javax.swing.JButton();
        panelButtonPlugin = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jSplitPane3 = new javax.swing.JSplitPane();
        jPanel18 = new javax.swing.JPanel();
        jPanel22 = new javax.swing.JPanel();
        jButtonReDx = new javax.swing.JButton();
        jComboBoxDxTemplate = new com.hosv3.gui.component.HosComboBox();
        btnAddDx = new javax.swing.JButton();
        jScrollPaneMainSymptom1 = new javax.swing.JScrollPane();
        jTableDx = new com.hosv3.gui.component.HJTableSort();
        jButtonDeleteDx = new javax.swing.JButton();
        jPanel23 = new javax.swing.JPanel();
        jScrollPaneNote = new javax.swing.JScrollPane();
        jTextAreaNote = new javax.swing.JTextArea();
        jButtonSaveDx = new javax.swing.JButton();
        jPanel13 = new javax.swing.JPanel();
        jButtonSaveGuide = new javax.swing.JButton();
        jButtonPrintGuide = new javax.swing.JButton();
        jButtonHealthEducation = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTextAreaGuide = new com.hosv3.gui.component.BalloonTextArea();
        jPanel19 = new javax.swing.JPanel();
        jComboBoxAppointment = new javax.swing.JComboBox();
        jTextFieldAppointment = new javax.swing.JTextField();
        jButtonAppointment = new javax.swing.JButton();
        jCheckBoxAppointment = new javax.swing.JCheckBox();
        jPanel4 = new javax.swing.JPanel();
        jCheckBoxAdmit = new javax.swing.JCheckBox();
        jButtonAdmit = new javax.swing.JButton();
        jCheckBoxRefer = new javax.swing.JCheckBox();
        jButtonRefer = new javax.swing.JButton();
        btnMedCer = new javax.swing.JButton();
        btnProgressNote = new javax.swing.JButton();

        setLayout(new java.awt.GridBagLayout());

        jSplitPane1.setBorder(null);
        jSplitPane1.setDividerLocation(620);
        jSplitPane1.setOneTouchExpandable(true);

        jPanel3.setMaximumSize(new java.awt.Dimension(580, 2147483647));
        jPanel3.setPreferredSize(new java.awt.Dimension(580, 500));
        jPanel3.setLayout(new java.awt.GridBagLayout());

        jPanelDescription.setBorder(javax.swing.BorderFactory.createTitledBorder("�ҡ���Ӥѭ"));
        jPanelDescription.setMinimumSize(new java.awt.Dimension(140, 371));
        jPanelDescription.setPreferredSize(new java.awt.Dimension(240, 371));
        jPanelDescription.setLayout(new java.awt.GridBagLayout());

        jPanelHide.setLayout(new java.awt.GridBagLayout());

        jButtonViewPTCard.setFont(jButtonViewPTCard.getFont());
        jButtonViewPTCard.setText("PTcard");
        jButtonViewPTCard.setEnabled(false);
        jButtonViewPTCard.setMargin(new java.awt.Insets(2, 2, 2, 2));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanelHide.add(jButtonViewPTCard, gridBagConstraints);

        jButtonViewSpecialClinic.setFont(jButtonViewSpecialClinic.getFont());
        jButtonViewSpecialClinic.setText("��ԹԤ�����");
        jButtonViewSpecialClinic.setEnabled(false);
        jButtonViewSpecialClinic.setMargin(new java.awt.Insets(2, 2, 2, 2));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanelHide.add(jButtonViewSpecialClinic, gridBagConstraints);

        jButtonViewDentistry.setFont(jButtonViewDentistry.getFont());
        jButtonViewDentistry.setText("Dentistry");
        jButtonViewDentistry.setEnabled(false);
        jButtonViewDentistry.setMargin(new java.awt.Insets(2, 2, 2, 2));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanelHide.add(jButtonViewDentistry, gridBagConstraints);

        jButtonViewOBGYNCard.setFont(jButtonViewOBGYNCard.getFont());
        jButtonViewOBGYNCard.setText("OB-GYN");
        jButtonViewOBGYNCard.setEnabled(false);
        jButtonViewOBGYNCard.setMargin(new java.awt.Insets(2, 2, 2, 2));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanelHide.add(jButtonViewOBGYNCard, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 0, 0, 0);
        jPanelDescription.add(jPanelHide, gridBagConstraints);

        jScrollPanePriSym.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPanePriSym.setMinimumSize(new java.awt.Dimension(250, 40));
        jScrollPanePriSym.setPreferredSize(new java.awt.Dimension(250, 40));

        jTablePrimarySymptom.setFillsViewportHeight(true);
        jTablePrimarySymptom.setFont(jTablePrimarySymptom.getFont());
        jTablePrimarySymptom.setRowHeight(30);
        jTablePrimarySymptom.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTablePrimarySymptomMouseReleased(evt);
            }
        });
        jTablePrimarySymptom.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTablePrimarySymptomKeyReleased(evt);
            }
        });
        jScrollPanePriSym.setViewportView(jTablePrimarySymptom);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        jPanelDescription.add(jScrollPanePriSym, gridBagConstraints);

        jPanel9.setLayout(new java.awt.GridBagLayout());

        jButtonAddPrimarySymptom.setFont(jButtonAddPrimarySymptom.getFont());
        jButtonAddPrimarySymptom.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/plus16.gif"))); // NOI18N
        jButtonAddPrimarySymptom.setMaximumSize(new java.awt.Dimension(24, 24));
        jButtonAddPrimarySymptom.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonAddPrimarySymptom.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonAddPrimarySymptom.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAddPrimarySymptomActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        jPanel9.add(jButtonAddPrimarySymptom, gridBagConstraints);

        jButtonDelPrimarySymptom.setFont(jButtonDelPrimarySymptom.getFont());
        jButtonDelPrimarySymptom.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/minus16.gif"))); // NOI18N
        jButtonDelPrimarySymptom.setMaximumSize(new java.awt.Dimension(24, 24));
        jButtonDelPrimarySymptom.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonDelPrimarySymptom.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonDelPrimarySymptom.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDelPrimarySymptomActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 0, 0, 0);
        jPanel9.add(jButtonDelPrimarySymptom, gridBagConstraints);

        jButton1.setFont(jButton1.getFont());
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/down-icon.png"))); // NOI18N
        jButton1.setMaximumSize(new java.awt.Dimension(26, 26));
        jButton1.setMinimumSize(new java.awt.Dimension(26, 26));
        jButton1.setPreferredSize(new java.awt.Dimension(26, 26));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(3, 0, 0, 0);
        jPanel9.add(jButton1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        jPanelDescription.add(jPanel9, gridBagConstraints);

        jPanel6.setLayout(new java.awt.GridBagLayout());

        jCheckBoxBalloon.setFont(jCheckBoxBalloon.getFont());
        jCheckBoxBalloon.setSelected(true);
        jCheckBoxBalloon.setText("���Ǫ���");
        jCheckBoxBalloon.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxBalloonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        jPanel6.add(jCheckBoxBalloon, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        jPanelDescription.add(jPanel6, gridBagConstraints);

        panelMainSymptom.setBorder(javax.swing.BorderFactory.createTitledBorder("�ҡ���Ӥѭ"));
        panelMainSymptom.setMaximumSize(new java.awt.Dimension(2147483647, 100));
        panelMainSymptom.setMinimumSize(new java.awt.Dimension(100, 100));
        panelMainSymptom.setPreferredSize(new java.awt.Dimension(100, 100));
        panelMainSymptom.setLayout(new java.awt.GridBagLayout());

        jScrollPaneMainSymptom.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPaneMainSymptom.setAutoscrolls(true);

        jTextAreaMainSymptom.setBackground(new java.awt.Color(204, 255, 255));
        jTextAreaMainSymptom.setWrapStyleWord(true);
        jTextAreaMainSymptom.setFont(jTextAreaMainSymptom.getFont());
        jTextAreaMainSymptom.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextAreaMainSymptomFocusLost(evt);
            }
        });
        jTextAreaMainSymptom.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextAreaMainSymptomKeyReleased(evt);
            }
        });
        jScrollPaneMainSymptom.setViewportView(jTextAreaMainSymptom);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        panelMainSymptom.add(jScrollPaneMainSymptom, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        jPanelDescription.add(panelMainSymptom, gridBagConstraints);

        currentIllnessPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("����ѵԻѨ�غѹ"));
        currentIllnessPanel.setLayout(new java.awt.GridBagLayout());

        jScrollPaneCurrent.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPaneCurrent.setAutoscrolls(true);

        jTextAreaCurrent.setWrapStyleWord(true);
        jTextAreaCurrent.setFont(jTextAreaCurrent.getFont());
        jTextAreaCurrent.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextAreaCurrentKeyReleased(evt);
            }
        });
        jScrollPaneCurrent.setViewportView(jTextAreaCurrent);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        currentIllnessPanel.add(jScrollPaneCurrent, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 0.7;
        jPanelDescription.add(currentIllnessPanel, gridBagConstraints);

        jPanel2.setLayout(new java.awt.GridBagLayout());

        jLabel1.setFont(jLabel1.getFont().deriveFont(jLabel1.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel1.setText("�ѹ������������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel2.add(jLabel1, gridBagConstraints);

        lblDiffDatetime.setForeground(new java.awt.Color(0, 0, 255));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel2.add(lblDiffDatetime, gridBagConstraints);

        jPanel11.setLayout(new java.awt.GridBagLayout());

        dateComboBoxIllness.setFont(dateComboBoxIllness.getFont());
        dateComboBoxIllness.setMinimumSize(new java.awt.Dimension(100, 23));
        dateComboBoxIllness.setPreferredSize(new java.awt.Dimension(100, 23));
        dateComboBoxIllness.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                dateComboBoxIllnessFocusLost(evt);
            }
        });
        dateComboBoxIllness.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dateComboBoxIllnessActionPerformed(evt);
            }
        });
        dateComboBoxIllness.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                dateComboBoxIllnessKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel11.add(dateComboBoxIllness, gridBagConstraints);

        jLabel9.setFont(jLabel9.getFont());
        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/clock.gif"))); // NOI18N
        jLabel9.setToolTipText("���ҷ���Ǩ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel11.add(jLabel9, gridBagConstraints);

        timeTextFieldIllness.setFont(timeTextFieldIllness.getFont());
        timeTextFieldIllness.setMinimumSize(new java.awt.Dimension(45, 23));
        timeTextFieldIllness.setPreferredSize(new java.awt.Dimension(45, 23));
        timeTextFieldIllness.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                timeTextFieldIllnessMouseClicked(evt);
            }
        });
        timeTextFieldIllness.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                timeTextFieldIllnessFocusLost(evt);
            }
        });
        timeTextFieldIllness.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                timeTextFieldIllnessKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel11.add(timeTextFieldIllness, gridBagConstraints);

        btnIllnessAddr.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/home.png"))); // NOI18N
        btnIllnessAddr.setMaximumSize(new java.awt.Dimension(28, 28));
        btnIllnessAddr.setMinimumSize(new java.awt.Dimension(28, 28));
        btnIllnessAddr.setPreferredSize(new java.awt.Dimension(28, 28));
        btnIllnessAddr.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIllnessAddrActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel11.add(btnIllnessAddr, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel2.add(jPanel11, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        jPanelDescription.add(jPanel2, gridBagConstraints);

        panelDatatime.setLayout(new java.awt.GridBagLayout());

        jLabel5.setFont(jLabel5.getFont());
        jLabel5.setText("�ѹ���");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        panelDatatime.add(jLabel5, gridBagConstraints);

        dateComboBoxCheck.setFont(dateComboBoxCheck.getFont());
        dateComboBoxCheck.setMinimumSize(new java.awt.Dimension(100, 23));
        dateComboBoxCheck.setPreferredSize(new java.awt.Dimension(100, 23));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        panelDatatime.add(dateComboBoxCheck, gridBagConstraints);

        jLabel6.setFont(jLabel6.getFont());
        jLabel6.setText("�.");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        panelDatatime.add(jLabel6, gridBagConstraints);

        timeTextFieldCheck.setFont(timeTextFieldCheck.getFont());
        timeTextFieldCheck.setMinimumSize(new java.awt.Dimension(45, 23));
        timeTextFieldCheck.setPreferredSize(new java.awt.Dimension(45, 23));
        timeTextFieldCheck.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                timeTextFieldCheckFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        panelDatatime.add(timeTextFieldCheck, gridBagConstraints);

        jLabel10.setFont(jLabel10.getFont());
        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/clock.gif"))); // NOI18N
        jLabel10.setToolTipText("���ҷ���Ǩ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        panelDatatime.add(jLabel10, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 0, 0, 0);
        jPanelDescription.add(panelDatatime, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        jPanel3.add(jPanelDescription, gridBagConstraints);

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel5.setLayout(new java.awt.GridBagLayout());

        jButtonRegiterNCD.setFont(jButtonRegiterNCD.getFont());
        jButtonRegiterNCD.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/IconTitle.gif"))); // NOI18N
        jButtonRegiterNCD.setToolTipText("ŧ����¹ NCD ");
        jButtonRegiterNCD.setMaximumSize(new java.awt.Dimension(32, 32));
        jButtonRegiterNCD.setMinimumSize(new java.awt.Dimension(32, 32));
        jButtonRegiterNCD.setPreferredSize(new java.awt.Dimension(32, 32));
        jButtonRegiterNCD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonRegiterNCDActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jButtonRegiterNCD, gridBagConstraints);

        jButtonViewNCD.setFont(jButtonViewNCD.getFont());
        jButtonViewNCD.setText("NCD");
        jButtonViewNCD.setToolTipText("�ٻ���ѵ� NCD");
        jButtonViewNCD.setMaximumSize(new java.awt.Dimension(60, 32));
        jButtonViewNCD.setMinimumSize(new java.awt.Dimension(60, 32));
        jButtonViewNCD.setPreferredSize(new java.awt.Dimension(60, 32));
        jButtonViewNCD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonViewNCDActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jButtonViewNCD, gridBagConstraints);

        jButtonAccident.setFont(jButtonAccident.getFont());
        jButtonAccident.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/accident.gif"))); // NOI18N
        jButtonAccident.setToolTipText("�������غѵ��˵�");
        jButtonAccident.setMaximumSize(new java.awt.Dimension(32, 32));
        jButtonAccident.setMinimumSize(new java.awt.Dimension(32, 32));
        jButtonAccident.setPreferredSize(new java.awt.Dimension(32, 32));
        jButtonAccident.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAccidentActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jButtonAccident, gridBagConstraints);

        jButtonLabResult.setFont(jButtonLabResult.getFont());
        jButtonLabResult.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/order_history.gif"))); // NOI18N
        jButtonLabResult.setToolTipText("����ѵ�");
        jButtonLabResult.setMaximumSize(new java.awt.Dimension(32, 32));
        jButtonLabResult.setMinimumSize(new java.awt.Dimension(32, 32));
        jButtonLabResult.setPreferredSize(new java.awt.Dimension(32, 32));
        jButtonLabResult.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonLabResultActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jButtonLabResult, gridBagConstraints);

        jButtonViewHistory.setFont(jButtonViewHistory.getFont());
        jButtonViewHistory.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/patient_history.gif"))); // NOI18N
        jButtonViewHistory.setToolTipText("����ѵ�ʹյ ����ѵԤ�ͺ���� ��������§ ����������");
        jButtonViewHistory.setMargin(new java.awt.Insets(2, 4, 2, 4));
        jButtonViewHistory.setMaximumSize(new java.awt.Dimension(32, 32));
        jButtonViewHistory.setMinimumSize(new java.awt.Dimension(32, 32));
        jButtonViewHistory.setPreferredSize(new java.awt.Dimension(32, 32));
        jButtonViewHistory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonViewHistoryActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jButtonViewHistory, gridBagConstraints);

        jPanel14.setLayout(new java.awt.GridBagLayout());

        buttonGroupDrugAllergy.add(rbtnDrugAllergy1);
        rbtnDrugAllergy1.setFont(rbtnDrugAllergy1.getFont());
        rbtnDrugAllergy1.setText("����ջ���ѵԡ������");
        rbtnDrugAllergy1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnDrugAllergy1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel14.add(rbtnDrugAllergy1, gridBagConstraints);

        buttonGroupDrugAllergy.add(rbtnDrugAllergy2);
        rbtnDrugAllergy2.setFont(rbtnDrugAllergy2.getFont());
        rbtnDrugAllergy2.setText("����Һ����ѵԡ������ ");
        rbtnDrugAllergy2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnDrugAllergy2ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel14.add(rbtnDrugAllergy2, gridBagConstraints);

        buttonGroupDrugAllergy.add(rbtnDrugAllergy3);
        rbtnDrugAllergy3.setFont(rbtnDrugAllergy3.getFont());
        rbtnDrugAllergy3.setText("�ջ���ѵԡ������");
        rbtnDrugAllergy3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnDrugAllergy3ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel14.add(rbtnDrugAllergy3, gridBagConstraints);

        jCheckBoxPregnant.setFont(jCheckBoxPregnant.getFont());
        jCheckBoxPregnant.setText("��駤����");
        jCheckBoxPregnant.setToolTipText("�ѹ�֡��õ�駤�ö�");
        jCheckBoxPregnant.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxPregnantActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel14.add(jCheckBoxPregnant, gridBagConstraints);

        panelLMP.setLayout(new java.awt.GridBagLayout());

        chkbLMP.setFont(chkbLMP.getFont());
        chkbLMP.setText("LMP");
        chkbLMP.setToolTipText("�ѹ�֡��õ�駤�ö�");
        chkbLMP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkbLMPActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelLMP.add(chkbLMP, gridBagConstraints);

        dateComboBoxLMP.setEnabled(false);
        dateComboBoxLMP.setFont(dateComboBoxLMP.getFont());
        dateComboBoxLMP.setMinimumSize(new java.awt.Dimension(100, 23));
        dateComboBoxLMP.setPreferredSize(new java.awt.Dimension(100, 23));
        dateComboBoxLMP.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                dateComboBoxLMPFocusLost(evt);
            }
        });
        dateComboBoxLMP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dateComboBoxLMPActionPerformed(evt);
            }
        });
        dateComboBoxLMP.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                dateComboBoxLMPKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelLMP.add(dateComboBoxLMP, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel14.add(panelLMP, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel5.add(jPanel14, gridBagConstraints);

        btnViewLISResult.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/folderlab@24.png"))); // NOI18N
        btnViewLISResult.setMaximumSize(new java.awt.Dimension(32, 32));
        btnViewLISResult.setMinimumSize(new java.awt.Dimension(32, 32));
        btnViewLISResult.setPreferredSize(new java.awt.Dimension(32, 32));
        btnViewLISResult.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewLISResultActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(btnViewLISResult, gridBagConstraints);

        btnViewEKGResult.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/folder-ekg@24.png"))); // NOI18N
        btnViewEKGResult.setMaximumSize(new java.awt.Dimension(32, 32));
        btnViewEKGResult.setMinimumSize(new java.awt.Dimension(32, 32));
        btnViewEKGResult.setPreferredSize(new java.awt.Dimension(32, 32));
        btnViewEKGResult.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewEKGResultActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(btnViewEKGResult, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 2, 0, 5);
        jPanel3.add(jPanel5, gridBagConstraints);

        jPanel12.setLayout(new java.awt.GridBagLayout());

        jButtonSaveVs.setFont(jButtonSaveVs.getFont());
        jButtonSaveVs.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/btn_save24.png"))); // NOI18N
        jButtonSaveVs.setMaximumSize(new java.awt.Dimension(32, 32));
        jButtonSaveVs.setMinimumSize(new java.awt.Dimension(32, 32));
        jButtonSaveVs.setPreferredSize(new java.awt.Dimension(32, 32));
        jButtonSaveVs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSaveVsActionPerformed(evt);
            }
        });
        jButtonSaveVs.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jButtonSaveVsKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 10);
        jPanel12.add(jButtonSaveVs, gridBagConstraints);

        jButtonChronic.setFont(jButtonChronic.getFont());
        jButtonChronic.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/chronic.gif"))); // NOI18N
        jButtonChronic.setToolTipText("�ä������ѧ");
        jButtonChronic.setMaximumSize(new java.awt.Dimension(32, 32));
        jButtonChronic.setMinimumSize(new java.awt.Dimension(32, 32));
        jButtonChronic.setPreferredSize(new java.awt.Dimension(32, 32));
        jButtonChronic.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonChronicActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        jPanel12.add(jButtonChronic, gridBagConstraints);

        jButtonSurveil.setFont(jButtonSurveil.getFont());
        jButtonSurveil.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/surveil.gif"))); // NOI18N
        jButtonSurveil.setToolTipText("�ä������ѧ");
        jButtonSurveil.setMaximumSize(new java.awt.Dimension(32, 32));
        jButtonSurveil.setMinimumSize(new java.awt.Dimension(32, 32));
        jButtonSurveil.setPreferredSize(new java.awt.Dimension(32, 32));
        jButtonSurveil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSurveilActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        jPanel12.add(jButtonSurveil, gridBagConstraints);

        jButtonDeath.setFont(jButtonDeath.getFont());
        jButtonDeath.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/death_icon.gif"))); // NOI18N
        jButtonDeath.setToolTipText("�����š�õ��");
        jButtonDeath.setMaximumSize(new java.awt.Dimension(32, 32));
        jButtonDeath.setMinimumSize(new java.awt.Dimension(32, 32));
        jButtonDeath.setPreferredSize(new java.awt.Dimension(32, 32));
        jButtonDeath.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDeathActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        jPanel12.add(jButtonDeath, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 5, 0);
        jPanel3.add(jPanel12, gridBagConstraints);

        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel7.setLayout(new java.awt.GridBagLayout());

        jPanel15.setLayout(new java.awt.GridBagLayout());

        lblTxtTransportation.setFont(lblTxtTransportation.getFont().deriveFont(lblTxtTransportation.getFont().getStyle() | java.awt.Font.BOLD));
        lblTxtTransportation.setText("�Ըա���� þ.");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel15.add(lblTxtTransportation, gridBagConstraints);

        lblShowTransportation.setFont(lblShowTransportation.getFont());
        lblShowTransportation.setForeground(new java.awt.Color(0, 0, 255));
        lblShowTransportation.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/walk.png"))); // NOI18N
        lblShowTransportation.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblShowTransportation.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblShowTransportationMouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel15.add(lblShowTransportation, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        jPanel7.add(jPanel15, gridBagConstraints);

        jPanel16.setLayout(new java.awt.GridBagLayout());

        lblTxtTrama.setFont(lblTxtTrama.getFont().deriveFont(lblTxtTrama.getFont().getStyle() | java.awt.Font.BOLD));
        lblTxtTrama.setText("����������� þ.");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel16.add(lblTxtTrama, gridBagConstraints);

        lblShowTrama.setFont(lblShowTrama.getFont());
        lblShowTrama.setForeground(new java.awt.Color(0, 0, 255));
        lblShowTrama.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblShowTrama.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblShowTramaMouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel16.add(lblShowTrama, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        jPanel7.add(jPanel16, gridBagConstraints);

        jPanel17.setLayout(new java.awt.GridBagLayout());

        lblTxtSeverity.setFont(lblTxtSeverity.getFont().deriveFont(lblTxtSeverity.getFont().getStyle() | java.awt.Font.BOLD));
        lblTxtSeverity.setText("�дѺ�����ع�ç");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel17.add(lblTxtSeverity, gridBagConstraints);

        lblShowSeverity.setFont(lblShowSeverity.getFont());
        lblShowSeverity.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblShowSeverity.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/na@24.png"))); // NOI18N
        lblShowSeverity.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblShowSeverity.setOpaque(true);
        lblShowSeverity.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblShowSeverityMouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.ipadx = 10;
        gridBagConstraints.ipady = 10;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel17.add(lblShowSeverity, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        jPanel7.add(jPanel17, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 0, 0);
        jPanel3.add(jPanel7, gridBagConstraints);

        jPanel21.setLayout(new java.awt.GridBagLayout());

        jButtonSavePrimarySymptom.setFont(jButtonSavePrimarySymptom.getFont());
        jButtonSavePrimarySymptom.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/btn_save24.png"))); // NOI18N
        jButtonSavePrimarySymptom.setMaximumSize(new java.awt.Dimension(32, 32));
        jButtonSavePrimarySymptom.setMinimumSize(new java.awt.Dimension(32, 32));
        jButtonSavePrimarySymptom.setPreferredSize(new java.awt.Dimension(32, 32));
        jButtonSavePrimarySymptom.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSavePrimarySymptomActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 5);
        jPanel21.add(jButtonSavePrimarySymptom, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 5, 5);
        jPanel3.add(jPanel21, gridBagConstraints);

        pDVitalSign1.setBorder(javax.swing.BorderFactory.createTitledBorder("VitalSign"));
        pDVitalSign1.setMinimumSize(new java.awt.Dimension(300, 305));
        pDVitalSign1.setPreferredSize(new java.awt.Dimension(300, 305));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel3.add(pDVitalSign1, gridBagConstraints);

        jSplitPane1.setLeftComponent(jPanel3);

        jSplitPane2.setBorder(null);
        jSplitPane2.setDividerLocation(150);
        jSplitPane2.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        jSplitPane2.setOneTouchExpandable(true);

        jPanel20.setPreferredSize(new java.awt.Dimension(104, 100));
        jPanel20.setLayout(new java.awt.GridBagLayout());

        jScrollPanePhyEx1.setMinimumSize(new java.awt.Dimension(104, 1));
        jScrollPanePhyEx1.setPreferredSize(new java.awt.Dimension(104, 1));

        jTextAreaPhysicalExam.setWrapStyleWord(true);
        jTextAreaPhysicalExam.setEnabled(false);
        jTextAreaPhysicalExam.setFont(jTextAreaPhysicalExam.getFont());
        jTextAreaPhysicalExam.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextAreaPhysicalExamFocusLost(evt);
            }
        });
        jTextAreaPhysicalExam.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextAreaPhysicalExamKeyReleased(evt);
            }
        });
        jScrollPanePhyEx1.setViewportView(jTextAreaPhysicalExam);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel20.add(jScrollPanePhyEx1, gridBagConstraints);

        jPanel10.setLayout(new java.awt.GridBagLayout());

        jButtonPhysicalExam.setFont(jButtonPhysicalExam.getFont());
        jButtonPhysicalExam.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/btn_chk_body.png"))); // NOI18N
        jButtonPhysicalExam.setToolTipText("��Ǩ��ҧ���");
        jButtonPhysicalExam.setMargin(new java.awt.Insets(2, 4, 2, 4));
        jButtonPhysicalExam.setMaximumSize(new java.awt.Dimension(32, 32));
        jButtonPhysicalExam.setMinimumSize(new java.awt.Dimension(32, 32));
        jButtonPhysicalExam.setPreferredSize(new java.awt.Dimension(32, 32));
        jButtonPhysicalExam.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPhysicalExamActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        jPanel10.add(jButtonPhysicalExam, gridBagConstraints);

        jButtonB2.setFont(jButtonB2.getFont());
        jButtonB2.setText("B2");
        jButtonB2.setMargin(new java.awt.Insets(2, 2, 2, 2));
        jButtonB2.setMaximumSize(new java.awt.Dimension(28, 28));
        jButtonB2.setMinimumSize(new java.awt.Dimension(28, 28));
        jButtonB2.setPreferredSize(new java.awt.Dimension(28, 28));
        jButtonB2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonB2ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        jPanel10.add(jButtonB2, gridBagConstraints);

        jCheckBoxPEText.setFont(jCheckBoxPEText.getFont());
        jCheckBoxPEText.setText("������ :�ŵ�Ǩ");
        jCheckBoxPEText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxPETextActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel10.add(jCheckBoxPEText, gridBagConstraints);

        btnFootExam.setFont(btnFootExam.getFont());
        btnFootExam.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/btn_chk_foot.png"))); // NOI18N
        btnFootExam.setToolTipText("��Ǩ���");
        btnFootExam.setMaximumSize(new java.awt.Dimension(32, 32));
        btnFootExam.setMinimumSize(new java.awt.Dimension(32, 32));
        btnFootExam.setPreferredSize(new java.awt.Dimension(32, 32));
        btnFootExam.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFootExamActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 3);
        jPanel10.add(btnFootExam, gridBagConstraints);

        btnEyesExam.setFont(btnEyesExam.getFont());
        btnEyesExam.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/btn_chk_eyes.png"))); // NOI18N
        btnEyesExam.setToolTipText("��Ǩ��");
        btnEyesExam.setMaximumSize(new java.awt.Dimension(32, 32));
        btnEyesExam.setMinimumSize(new java.awt.Dimension(32, 32));
        btnEyesExam.setPreferredSize(new java.awt.Dimension(32, 32));
        btnEyesExam.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEyesExamActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 3);
        jPanel10.add(btnEyesExam, gridBagConstraints);

        btnEyesExam1.setFont(btnEyesExam1.getFont());
        btnEyesExam1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/btn_chk_anemia.png"))); // NOI18N
        btnEyesExam1.setToolTipText("���Ыմ");
        btnEyesExam1.setMaximumSize(new java.awt.Dimension(32, 32));
        btnEyesExam1.setMinimumSize(new java.awt.Dimension(32, 32));
        btnEyesExam1.setPreferredSize(new java.awt.Dimension(32, 32));
        btnEyesExam1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEyesExam1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 3);
        jPanel10.add(btnEyesExam1, gridBagConstraints);

        btnAudiometry.setFont(btnAudiometry.getFont());
        btnAudiometry.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/btn_chk_ear.png"))); // NOI18N
        btnAudiometry.setToolTipText("��Ǩ������Թ");
        btnAudiometry.setMaximumSize(new java.awt.Dimension(32, 32));
        btnAudiometry.setMinimumSize(new java.awt.Dimension(32, 32));
        btnAudiometry.setPreferredSize(new java.awt.Dimension(32, 32));
        btnAudiometry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAudiometryActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 3);
        jPanel10.add(btnAudiometry, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 0, 5);
        jPanel20.add(jPanel10, gridBagConstraints);

        panelButtonPlugin.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 5);
        jPanel20.add(panelButtonPlugin, gridBagConstraints);

        jSplitPane2.setLeftComponent(jPanel20);

        jPanel1.setLayout(new java.awt.GridBagLayout());

        jSplitPane3.setDividerLocation(250);
        jSplitPane3.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        jSplitPane3.setOneTouchExpandable(true);

        jPanel18.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createTitledBorder(""), "Dx"));
        jPanel18.setFont(jPanel18.getFont());
        jPanel18.setLayout(new java.awt.GridBagLayout());

        jPanel22.setLayout(new java.awt.GridBagLayout());

        jButtonReDx.setFont(jButtonReDx.getFont());
        jButtonReDx.setText("ReDx");
        jButtonReDx.setMaximumSize(new java.awt.Dimension(70, 28));
        jButtonReDx.setMinimumSize(new java.awt.Dimension(70, 28));
        jButtonReDx.setPreferredSize(new java.awt.Dimension(70, 28));
        jButtonReDx.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonReDxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel22.add(jButtonReDx, gridBagConstraints);

        jComboBoxDxTemplate.setBackground(new java.awt.Color(204, 255, 255));
        jComboBoxDxTemplate.setFont(jComboBoxDxTemplate.getFont());
        jComboBoxDxTemplate.setShowSearchText(true);
        jComboBoxDxTemplate.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jComboBoxDxTemplateFocusGained(evt);
            }
        });
        jComboBoxDxTemplate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxDxTemplateActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel22.add(jComboBoxDxTemplate, gridBagConstraints);

        btnAddDx.setFont(btnAddDx.getFont());
        btnAddDx.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/add.png"))); // NOI18N
        btnAddDx.setMaximumSize(new java.awt.Dimension(26, 26));
        btnAddDx.setMinimumSize(new java.awt.Dimension(26, 26));
        btnAddDx.setPreferredSize(new java.awt.Dimension(26, 26));
        btnAddDx.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddDxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel22.add(btnAddDx, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel18.add(jPanel22, gridBagConstraints);

        jScrollPaneMainSymptom1.setMinimumSize(new java.awt.Dimension(140, 100));
        jScrollPaneMainSymptom1.setPreferredSize(new java.awt.Dimension(140, 100));

        jTableDx.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTableDx.setFillsViewportHeight(true);
        jTableDx.setFont(jTableDx.getFont());
        jTableDx.setRowHeight(30);
        jScrollPaneMainSymptom1.setViewportView(jTableDx);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel18.add(jScrollPaneMainSymptom1, gridBagConstraints);

        jButtonDeleteDx.setFont(jButtonDeleteDx.getFont());
        jButtonDeleteDx.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/delete.png"))); // NOI18N
        jButtonDeleteDx.setMaximumSize(new java.awt.Dimension(26, 26));
        jButtonDeleteDx.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonDeleteDx.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonDeleteDx.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDeleteDxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        jPanel18.add(jButtonDeleteDx, gridBagConstraints);

        jSplitPane3.setLeftComponent(jPanel18);

        jPanel23.setBorder(javax.swing.BorderFactory.createTitledBorder("�����˵�"));
        jPanel23.setFont(jPanel23.getFont());
        jPanel23.setLayout(new java.awt.GridBagLayout());

        jScrollPaneNote.setMinimumSize(new java.awt.Dimension(103, 56));
        jScrollPaneNote.setPreferredSize(new java.awt.Dimension(103, 56));

        jTextAreaNote.setFont(jTextAreaNote.getFont());
        jTextAreaNote.setLineWrap(true);
        jTextAreaNote.setRows(2);
        jTextAreaNote.setWrapStyleWord(true);
        jTextAreaNote.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextAreaNoteFocusLost(evt);
            }
        });
        jTextAreaNote.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextAreaNoteKeyReleased(evt);
            }
        });
        jScrollPaneNote.setViewportView(jTextAreaNote);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel23.add(jScrollPaneNote, gridBagConstraints);

        jButtonSaveDx.setFont(jButtonSaveDx.getFont());
        jButtonSaveDx.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/btn_save24.png"))); // NOI18N
        jButtonSaveDx.setMaximumSize(new java.awt.Dimension(26, 26));
        jButtonSaveDx.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonSaveDx.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonSaveDx.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSaveDxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTHWEST;
        jPanel23.add(jButtonSaveDx, gridBagConstraints);

        jSplitPane3.setRightComponent(jPanel23);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel1.add(jSplitPane3, gridBagConstraints);

        jPanel13.setLayout(new java.awt.GridBagLayout());

        jButtonSaveGuide.setFont(jButtonSaveGuide.getFont());
        jButtonSaveGuide.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/btn_save24.png"))); // NOI18N
        jButtonSaveGuide.setMaximumSize(new java.awt.Dimension(26, 26));
        jButtonSaveGuide.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonSaveGuide.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonSaveGuide.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSaveGuideActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        jPanel13.add(jButtonSaveGuide, gridBagConstraints);

        jButtonPrintGuide.setFont(jButtonPrintGuide.getFont());
        jButtonPrintGuide.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/print.gif"))); // NOI18N
        jButtonPrintGuide.setMaximumSize(new java.awt.Dimension(26, 26));
        jButtonPrintGuide.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonPrintGuide.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonPrintGuide.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPrintGuideActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        jPanel13.add(jButtonPrintGuide, gridBagConstraints);

        jButtonHealthEducation.setFont(jButtonHealthEducation.getFont());
        jButtonHealthEducation.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/home.gif"))); // NOI18N
        jButtonHealthEducation.setToolTipText("�������آ�֡��");
        jButtonHealthEducation.setMaximumSize(new java.awt.Dimension(26, 26));
        jButtonHealthEducation.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonHealthEducation.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonHealthEducation.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonHealthEducationActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTHEAST;
        gridBagConstraints.weighty = 1.0;
        jPanel13.add(jButtonHealthEducation, gridBagConstraints);

        jLabel2.setFont(jLabel2.getFont());
        jLabel2.setText("���йӼ�����/�������آ�֡��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 0, 2);
        jPanel13.add(jLabel2, gridBagConstraints);

        jScrollPane5.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jScrollPane5.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        jTextAreaGuide.setWrapStyleWord(true);
        jTextAreaGuide.setFont(jTextAreaGuide.getFont());
        jTextAreaGuide.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextAreaGuideKeyReleased(evt);
            }
        });
        jScrollPane5.setViewportView(jTextAreaGuide);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridheight = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 0, 0, 0);
        jPanel13.add(jScrollPane5, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weighty = 0.4;
        gridBagConstraints.insets = new java.awt.Insets(3, 5, 5, 5);
        jPanel1.add(jPanel13, gridBagConstraints);

        jPanel19.setLayout(new java.awt.GridBagLayout());

        jComboBoxAppointment.setFont(jComboBoxAppointment.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 0);
        jPanel19.add(jComboBoxAppointment, gridBagConstraints);

        jTextFieldAppointment.setFont(jTextFieldAppointment.getFont());
        jTextFieldAppointment.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldAppointmentActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 3, 0, 0);
        jPanel19.add(jTextFieldAppointment, gridBagConstraints);

        jButtonAppointment.setFont(jButtonAppointment.getFont());
        jButtonAppointment.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/appoint.gif"))); // NOI18N
        jButtonAppointment.setToolTipText("��ùѴ������");
        jButtonAppointment.setMaximumSize(new java.awt.Dimension(26, 26));
        jButtonAppointment.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonAppointment.setPreferredSize(new java.awt.Dimension(28, 28));
        jButtonAppointment.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAppointmentActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        jPanel19.add(jButtonAppointment, gridBagConstraints);

        jCheckBoxAppointment.setFont(jCheckBoxAppointment.getFont());
        jCheckBoxAppointment.setText("�Ѵ");
        jCheckBoxAppointment.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxAppointmentActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel19.add(jCheckBoxAppointment, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(3, 5, 0, 5);
        jPanel1.add(jPanel19, gridBagConstraints);

        jPanel4.setLayout(new java.awt.GridBagLayout());

        jCheckBoxAdmit.setFont(jCheckBoxAdmit.getFont());
        jCheckBoxAdmit.setText("Admit");
        jCheckBoxAdmit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxAdmitActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel4.add(jCheckBoxAdmit, gridBagConstraints);

        jButtonAdmit.setFont(jButtonAdmit.getFont());
        jButtonAdmit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/admit.png"))); // NOI18N
        jButtonAdmit.setToolTipText("��� Admit ������");
        jButtonAdmit.setMaximumSize(new java.awt.Dimension(30, 30));
        jButtonAdmit.setMinimumSize(new java.awt.Dimension(30, 30));
        jButtonAdmit.setPreferredSize(new java.awt.Dimension(30, 30));
        jButtonAdmit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAdmitActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel4.add(jButtonAdmit, gridBagConstraints);

        jCheckBoxRefer.setFont(jCheckBoxRefer.getFont());
        jCheckBoxRefer.setText("Refer");
        jCheckBoxRefer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxReferActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel4.add(jCheckBoxRefer, gridBagConstraints);

        jButtonRefer.setFont(jButtonRefer.getFont());
        jButtonRefer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/refer.gif"))); // NOI18N
        jButtonRefer.setToolTipText("�觼�������ѡ�ҵ�� (refer)");
        jButtonRefer.setMaximumSize(new java.awt.Dimension(30, 30));
        jButtonRefer.setMinimumSize(new java.awt.Dimension(30, 30));
        jButtonRefer.setPreferredSize(new java.awt.Dimension(30, 30));
        jButtonRefer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonReferActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel4.add(jButtonRefer, gridBagConstraints);

        btnMedCer.setText("�͡��Ѻ�ͧᾷ��");
        btnMedCer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMedCerActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel4.add(btnMedCer, gridBagConstraints);

        btnProgressNote.setText("ProgressNote");
        btnProgressNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProgressNoteActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel4.add(btnProgressNote, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 5, 0, 5);
        jPanel1.add(jPanel4, gridBagConstraints);

        jSplitPane2.setRightComponent(jPanel1);

        jSplitPane1.setRightComponent(jSplitPane2);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        add(jSplitPane1, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jTextFieldAppointmentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldAppointmentActionPerformed
        saveAppointment();
    }//GEN-LAST:event_jTextFieldAppointmentActionPerformed

    private void jButtonB2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonB2ActionPerformed
        String command = theHC.theLookupControl.readOption().b2_command;
        try {
            Process proc = Runtime.getRuntime().exec(command);
        } catch (IOException ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
        }
    }//GEN-LAST:event_jButtonB2ActionPerformed

    private void jButtonDeathActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDeathActionPerformed
        theHD.showDialogDeath();
    }//GEN-LAST:event_jButtonDeathActionPerformed

    private void jButtonAccidentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAccidentActionPerformed
        theHD.showDialogAccident();
    }//GEN-LAST:event_jButtonAccidentActionPerformed

    private void jButtonSurveilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSurveilActionPerformed
        theHD.showDialogSurveil(null);
    }//GEN-LAST:event_jButtonSurveilActionPerformed

    private void jButtonChronicActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonChronicActionPerformed
        theHD.showDialogChronic(theHO.theVisit, null);
    }//GEN-LAST:event_jButtonChronicActionPerformed

    private void jTextAreaPhysicalExamFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextAreaPhysicalExamFocusLost
    }//GEN-LAST:event_jTextAreaPhysicalExamFocusLost

    private void jCheckBoxPETextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxPETextActionPerformed
        this.jTextAreaPhysicalExam.setEnabled(jCheckBoxPEText.isSelected());
        if (jCheckBoxPEText.isSelected()) {
            this.vPhysicalExam = new Vector();
        }
    }//GEN-LAST:event_jCheckBoxPETextActionPerformed

    private void jButtonAppointmentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAppointmentActionPerformed
        theHD.showDialogAppointment(theHO.thePatient, theHO.theVisit);
    }//GEN-LAST:event_jButtonAppointmentActionPerformed

    private void jTextAreaGuideKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextAreaGuideKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            jButtonSaveGuideActionPerformed(null);
        }
    }//GEN-LAST:event_jTextAreaGuideKeyReleased

    private void jTextAreaPhysicalExamKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextAreaPhysicalExamKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            vPhysicalExam = new Vector();
            boolean ret = getPhysicalExam(vPhysicalExam);
            if (ret == false) {
                setPhysicalExamNanV(vPhysicalExam);
                theUS.setStatus("��سҡ�͡ ��ǹ�ͧ��ҧ���: �š�õ�Ǩ", UpdateStatus.WARNING);
                return;
            }
            theHC.theVitalControl.savePhysicalExam(vPhysicalExam);
        }
    }//GEN-LAST:event_jTextAreaPhysicalExamKeyReleased

    private void jCheckBoxReferActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxReferActionPerformed
        boolean ret = theHC.theVisitControl.updateVisitRefer(jCheckBoxRefer.isSelected());
    }//GEN-LAST:event_jCheckBoxReferActionPerformed

    private void jButtonAdmitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdmitActionPerformed
        theHD.showDialogAdmit(theHO.theVisit);
    }//GEN-LAST:event_jButtonAdmitActionPerformed

    private void jCheckBoxAppointmentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxAppointmentActionPerformed
        boolean ret = theHC.theVisitControl.updateVisitAppointment(jCheckBoxAppointment.isSelected());
        if (jCheckBoxAppointment.isSelected()) {
            jTextFieldAppointment.setEnabled(true);
            jComboBoxAppointment.setEnabled(true);
        } else {
            jTextFieldAppointment.setEnabled(false);
            jTextFieldAppointment.setText("");
            jComboBoxAppointment.setEnabled(false);
            jComboBoxAppointment.setSelectedIndex(0);
        }
    }//GEN-LAST:event_jCheckBoxAppointmentActionPerformed

    private void jCheckBoxAdmitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxAdmitActionPerformed
        boolean ret = theHC.theVisitControl.updateVisitAdmit(jCheckBoxAdmit.isSelected());
    }//GEN-LAST:event_jCheckBoxAdmitActionPerformed

    private void jButtonRegiterNCDActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonRegiterNCDActionPerformed
    {//GEN-HEADEREND:event_jButtonRegiterNCDActionPerformed
        //amp:17/06/254:ŧ����¹ NCD / ���ѡ�Ҵ�¡�����ä NCD
        if (theHO.thePatient == null) {
            theUS.setStatus("��س����͡������", UpdateStatus.WARNING);
            return;
        }
//        thePRNCD = new PanelRegisterNCD(theHC, theUS);
//        thePRNCD.showDialog(false, true);
        // ����˹�Ҩ� copy �ҡ Med �˹�һ�Ъҡâͧ PCU
        theHD.showDialogPatientNCD(theHO.thePatient);
    }//GEN-LAST:event_jButtonRegiterNCDActionPerformed

    private void jButtonViewNCDActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonViewNCDActionPerformed
    {//GEN-HEADEREND:event_jButtonViewNCDActionPerformed
        //amp:15/06/254:�ʴ�����ѵ� NCD
        if (theHO.thePatient == null) {
            theUS.setStatus("��س����͡������", UpdateStatus.WARNING);
            return;
        }
        if (thePNCD == null) {
            thePNCD = new PanelNCD(theHC, theUS);
        }
        thePNCD.showDialog();
    }//GEN-LAST:event_jButtonViewNCDActionPerformed

    private void jButtonHealthEducationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonHealthEducationActionPerformed
        showHealthEducation();
    }//GEN-LAST:event_jButtonHealthEducationActionPerformed

    private void jButtonPhysicalExamActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPhysicalExamActionPerformed
        showPhysicalExam();
        this.jCheckBoxPEText.setSelected(false);
        this.jTextAreaPhysicalExam.setEnabled(false);
    }//GEN-LAST:event_jButtonPhysicalExamActionPerformed

    private void jButtonViewHistoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonViewHistoryActionPerformed
        showPatientHistory();
    }//GEN-LAST:event_jButtonViewHistoryActionPerformed

    private void jButtonReferActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonReferActionPerformed
        theHD.showDialogReferIn(theHO.theVisit);
    }//GEN-LAST:event_jButtonReferActionPerformed

    private void jTextAreaCurrentFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextAreaCurrentFocusLost
    }//GEN-LAST:event_jTextAreaCurrentFocusLost

    private void jTextAreaMainSymptomFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextAreaMainSymptomFocusLost
    }//GEN-LAST:event_jTextAreaMainSymptomFocusLost

    private void jTextAreaCurrentKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextAreaCurrentKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_F1) {
            theHD.showDialogPrimarySymptomTemplate(jTextAreaCurrent);
        }
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            jTextAreaCurrent.transferFocus();
        }
    }//GEN-LAST:event_jTextAreaCurrentKeyReleased

    private void jTextAreaMainSymptomKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextAreaMainSymptomKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_F1) {
            theHD.showDialogPrimarySymptomTemplate(jTextAreaMainSymptom);
        }
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            jTextAreaMainSymptom.transferFocus();
        }
    }//GEN-LAST:event_jTextAreaMainSymptomKeyReleased

    private void jButtonPrintGuideActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPrintGuideActionPerformed
        theGuideAfterDxTransaction = theHC.theVisitControl.listGuideByVisitId(theVisit.getObjectId());
        theHC.thePrintControl.printGuide(theGuideAfterDxTransaction);
    }//GEN-LAST:event_jButtonPrintGuideActionPerformed

    public void saveAppointment() {
        String date_time = theHC.theLookupControl.getTextCurrentDateTime();
        if (theVisit.appointment_id == null || theVisit.appointment_id.isEmpty()) {
            theAppointment = theHO.initAppointment(date_time);
        } else {
            theAppointment = theHC.thePatientControl.readAppointmentByPK(theVisit.appointment_id);
            if (theAppointment == null || theAppointment.appoint_active.equals("0")) {
                theAppointment = theHO.initAppointment(date_time);
            }
        }
        theAppointment.aptype = jTextFieldAppointment.getText();
        theVisit.cal_date_appointment = Gutil.getGuiData(jComboBoxAppointment);
        theHC.theVisitControl.saveAppointment(theVisit, theAppointment, date_time);
    }
    private void jButtonSaveGuideActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonSaveGuideActionPerformed
    {//GEN-HEADEREND:event_jButtonSaveGuideActionPerformed
        ///////////////////////////////////////////////////////////////////////
        //��õ�Ǩ�ͺ��õ�Ǩ��ҧ���������Ǫ������������ҡ�������ͧ�ѹ�֡�ա
        if (this.jCheckBoxPEText.isSelected()) {
            vPhysicalExam = new Vector();
            boolean ret = getPhysicalExam(vPhysicalExam);
            if (ret == false) {
                theUS.setStatus("�š�õ�Ǩ��ҧ��¼Դ��Ҵ��سҡ�͡ ��ǹ�ͧ��ҧ���: �š�õ�Ǩ", UpdateStatus.WARNING);
                return;
            }
            theHC.theVitalControl.savePhysicalExam(vPhysicalExam);
        }
        saveAppointment();
        getGuide();
        theHC.theVisitControl.saveGuideHealthEducation(vGuide);
        setHealthEducationV(theHO.vHealthEducation);
    }//GEN-LAST:event_jButtonSaveGuideActionPerformed

    private void jButtonLabResultActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonLabResultActionPerformed
        String currentdate = theHO.date_time;
        String reversedate = DateUtil.getReverseDay(currentdate, 1);
        theHD.showDialogOrderHistory(theHO.thePatient, reversedate, null, CategoryGroup.isLab());
    }//GEN-LAST:event_jButtonLabResultActionPerformed

    private void jButtonSaveVsKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButtonSaveVsKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            jButtonSaveVsActionPerformed(null);
        }
    }//GEN-LAST:event_jButtonSaveVsKeyReleased

    private void jCheckBoxPregnantActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxPregnantActionPerformed
        boolean ret = theHC.theVisitControl.updateVisitPregnant(jCheckBoxPregnant.isSelected());
        if (!ret) {
            jCheckBoxPregnant.setSelected(!jCheckBoxPregnant.isSelected());
        }
    }//GEN-LAST:event_jCheckBoxPregnantActionPerformed

    private void jTablePrimarySymptomKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTablePrimarySymptomKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_UP
                || evt.getKeyCode() == KeyEvent.VK_DOWN) {
            int row = jTablePrimarySymptom.getSelectedRow();
            if (row == -1) {
                return;
            }
            thePrimarySymptom = (PrimarySymptom) vPrimarySymptom.get(row);
            setPrimarySymptom(thePrimarySymptom);
        }
    }//GEN-LAST:event_jTablePrimarySymptomKeyReleased

    private void jTablePrimarySymptomMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTablePrimarySymptomMouseReleased
        int row = jTablePrimarySymptom.getSelectedRow();
        if (row == -1) {
            return;
        }
        thePrimarySymptom = (PrimarySymptom) vPrimarySymptom.get(row);
        setPrimarySymptom(thePrimarySymptom);
    }//GEN-LAST:event_jTablePrimarySymptomMouseReleased

    private void jButtonSavePrimarySymptomActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSavePrimarySymptomActionPerformed
        PrimarySymptom ps = getPrimarySymptom();
        theHC.theVitalControl.savePrimarySymptom(ps);
        //henbe comment  ������ҵ͹�ѹ�֡ vital sign �����������ѹ�����͹�����͡����º͡����Һѹ�֡�������
//        jButtonSaveVsActionPerformed(null);
    }//GEN-LAST:event_jButtonSavePrimarySymptomActionPerformed

    private void jButtonDelPrimarySymptomActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDelPrimarySymptomActionPerformed
        int[] row = jTablePrimarySymptom.getSelectedRows();
        theHC.theVitalControl.deletePrimarySymptom(vPrimarySymptom, row);
    }//GEN-LAST:event_jButtonDelPrimarySymptomActionPerformed

    private void jButtonAddPrimarySymptomActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAddPrimarySymptomActionPerformed
        theUS.setStatus("��سҡ�͡��¡���ҡ���Ӥѭ", UpdateStatus.WARNING);
        jTablePrimarySymptom.clearSelection();
        setPrimarySymptom(null);
        jTextAreaMainSymptom.requestFocus();
    }//GEN-LAST:event_jButtonAddPrimarySymptomActionPerformed

   private void jButtonSaveVsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSaveVsActionPerformed
        this.pDVitalSign1.saveVitalSign();
    }//GEN-LAST:event_jButtonSaveVsActionPerformed

   private void jCheckBoxBalloonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxBalloonActionPerformed
        this.jTextAreaCurrent.setActive(this.jCheckBoxBalloon.isSelected());
        this.jTextAreaMainSymptom.setActive(this.jCheckBoxBalloon.isSelected());
   }//GEN-LAST:event_jCheckBoxBalloonActionPerformed

   private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        this.addFromSelected();
   }//GEN-LAST:event_jButton1ActionPerformed

    private void btnFootExamActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFootExamActionPerformed
        this.showFootExamDialog();
    }//GEN-LAST:event_btnFootExamActionPerformed

    private void btnEyesExamActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEyesExamActionPerformed
        this.showEyesExamDialog();
    }//GEN-LAST:event_btnEyesExamActionPerformed

    private void btnEyesExam1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEyesExam1ActionPerformed
        this.showInformAnemiaDialog();
    }//GEN-LAST:event_btnEyesExam1ActionPerformed

    private void dateComboBoxIllnessKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_dateComboBoxIllnessKeyReleased
        //amp:05/04/2549
        if (evt.getKeyCode() == KeyEvent.VK_DOWN || evt.getKeyCode() == KeyEvent.VK_ENTER) {
            timeTextFieldIllness.requestFocus();
        }
    }//GEN-LAST:event_dateComboBoxIllnessKeyReleased

    private void timeTextFieldIllnessMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_timeTextFieldIllnessMouseClicked
        timeTextFieldIllness.selectAll();
    }//GEN-LAST:event_timeTextFieldIllnessMouseClicked

    private void timeTextFieldIllnessKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_timeTextFieldIllnessKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_UP) {
            dateComboBoxIllness.requestFocus();
        }
    }//GEN-LAST:event_timeTextFieldIllnessKeyReleased

    private void dateComboBoxIllnessActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dateComboBoxIllnessActionPerformed
        this.calIllnessDateDiff();
    }//GEN-LAST:event_dateComboBoxIllnessActionPerformed

    private void dateComboBoxIllnessFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_dateComboBoxIllnessFocusLost
        this.calIllnessDateDiff();
    }//GEN-LAST:event_dateComboBoxIllnessFocusLost

    private void timeTextFieldIllnessFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_timeTextFieldIllnessFocusLost
        this.calIllnessDateDiff();
    }//GEN-LAST:event_timeTextFieldIllnessFocusLost

    private void lblShowTransportationMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblShowTransportationMouseClicked
        popupTransportationType.show(evt.getComponent(), evt.getX(), evt.getY());
    }//GEN-LAST:event_lblShowTransportationMouseClicked

    private void lblShowTramaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblShowTramaMouseClicked
        popupTrama.show(evt.getComponent(), evt.getX(), evt.getY());
    }//GEN-LAST:event_lblShowTramaMouseClicked

    private void lblShowSeverityMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblShowSeverityMouseClicked
        popupSeverity.show(evt.getComponent(), evt.getX(), evt.getY());
    }//GEN-LAST:event_lblShowSeverityMouseClicked

    private void btnIllnessAddrActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIllnessAddrActionPerformed
        theHD.showDialogIllnessAddress();
    }//GEN-LAST:event_btnIllnessAddrActionPerformed

    private void rbtnDrugAllergy1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnDrugAllergy1ActionPerformed
        this.doUpdateDrugAllergyType();
    }//GEN-LAST:event_rbtnDrugAllergy1ActionPerformed

    private void rbtnDrugAllergy3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnDrugAllergy3ActionPerformed
        this.doUpdateDrugAllergyType();
    }//GEN-LAST:event_rbtnDrugAllergy3ActionPerformed

    private void rbtnDrugAllergy2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnDrugAllergy2ActionPerformed
        this.doUpdateDrugAllergyType();
    }//GEN-LAST:event_rbtnDrugAllergy2ActionPerformed

    private void timeTextFieldCheckFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_timeTextFieldCheckFocusGained
        timeTextFieldCheck.selectAll();
    }//GEN-LAST:event_timeTextFieldCheckFocusGained

    private void btnViewLISResultActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewLISResultActionPerformed
        theHD.showDialogLISResult();
    }//GEN-LAST:event_btnViewLISResultActionPerformed

    private void btnViewEKGResultActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewEKGResultActionPerformed
        theHD.showDialogEKGResult();
    }//GEN-LAST:event_btnViewEKGResultActionPerformed

    private void btnMedCerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMedCerActionPerformed
        theHD.showDialogCreateMedicalCertoficate(theVisit);
    }//GEN-LAST:event_btnMedCerActionPerformed

    private void chkbLMPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkbLMPActionPerformed
        boolean ret = theHC.theVisitControl.updateVisitLMP(chkbLMP.isSelected()
                ? dateComboBoxLMP.getDate()
                : null);
        if (!ret) {
            chkbLMP.setSelected(!chkbLMP.isSelected());
        }
        dateComboBoxLMP.setEnabled(chkbLMP.isSelected());
    }//GEN-LAST:event_chkbLMPActionPerformed

    private void dateComboBoxLMPFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_dateComboBoxLMPFocusLost
        this.updateVisitLMP();
    }//GEN-LAST:event_dateComboBoxLMPFocusLost

    private void dateComboBoxLMPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dateComboBoxLMPActionPerformed
        this.updateVisitLMP();
    }//GEN-LAST:event_dateComboBoxLMPActionPerformed

    private void dateComboBoxLMPKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_dateComboBoxLMPKeyReleased
        this.updateVisitLMP();
    }//GEN-LAST:event_dateComboBoxLMPKeyReleased

    private void btnAudiometryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAudiometryActionPerformed
        this.showAudiometryDialog();
    }//GEN-LAST:event_btnAudiometryActionPerformed

    private void btnProgressNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProgressNoteActionPerformed
        theHD.showDialogProgressNote(theVisit);
    }//GEN-LAST:event_btnProgressNoteActionPerformed

    private void jButtonReDxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonReDxActionPerformed
        if (dialogReDiag == null) {
            dialogReDiag = new DialogReDiag(null, true);
            dialogReDiag.setControl(theHC);
        }
        List<DxTemplate> openDialog = dialogReDiag.openDialog();
        for (DxTemplate dx : openDialog) {
            saveDxForEmployee(dx, true);
        }
    }//GEN-LAST:event_jButtonReDxActionPerformed

    private void jComboBoxDxTemplateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxDxTemplateActionPerformed
        if (evt.getActionCommand().equals("comboBoxEdited")) {
            saveDxTemplate();
        }
    }//GEN-LAST:event_jComboBoxDxTemplateActionPerformed

    private void jButtonDeleteDxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDeleteDxActionPerformed
        int[] rows = jTableDx.getSelectedRows();
        if (rows.length <= 0) {
            return;
        }
        this.deleteDxs(rows);
    }//GEN-LAST:event_jButtonDeleteDxActionPerformed

    private void jTextAreaNoteFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextAreaNoteFocusLost

    }//GEN-LAST:event_jTextAreaNoteFocusLost

    private void jTextAreaNoteKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextAreaNoteKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            jButtonHealthEducation.requestFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            theHC.theVitalControl.saveDxNote(jTextAreaNote.getText());
        }
    }//GEN-LAST:event_jTextAreaNoteKeyReleased

    private void jButtonSaveDxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSaveDxActionPerformed
        getDxTemplate();
    }//GEN-LAST:event_jButtonSaveDxActionPerformed

    private void btnAddDxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddDxActionPerformed
        saveDxTemplate();
    }//GEN-LAST:event_btnAddDxActionPerformed

    private void jComboBoxDxTemplateFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jComboBoxDxTemplateFocusGained
        JTextComponent tc = (JTextComponent) jComboBoxDxTemplate.getEditor().getEditorComponent();
        tc.selectAll();
    }//GEN-LAST:event_jComboBoxDxTemplateFocusGained

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddDx;
    private javax.swing.JButton btnAudiometry;
    private javax.swing.JButton btnEyesExam;
    private javax.swing.JButton btnEyesExam1;
    private javax.swing.JButton btnFootExam;
    private javax.swing.JButton btnIllnessAddr;
    private javax.swing.JButton btnMedCer;
    private javax.swing.JButton btnProgressNote;
    private javax.swing.JButton btnViewEKGResult;
    private javax.swing.JButton btnViewLISResult;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.ButtonGroup buttonGroupDrugAllergy;
    private javax.swing.JCheckBox chkbLMP;
    private javax.swing.JPanel currentIllnessPanel;
    private com.hospital_os.utility.DateComboBox dateComboBoxCheck;
    private com.hospital_os.utility.DateComboBox dateComboBoxIllness;
    private com.hospital_os.utility.DateComboBox dateComboBoxLMP;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButtonAccident;
    private javax.swing.JButton jButtonAddPrimarySymptom;
    private javax.swing.JButton jButtonAdmit;
    private javax.swing.JButton jButtonAppointment;
    private javax.swing.JButton jButtonB2;
    private javax.swing.JButton jButtonChronic;
    private javax.swing.JButton jButtonDeath;
    private javax.swing.JButton jButtonDelPrimarySymptom;
    private javax.swing.JButton jButtonDeleteDx;
    private javax.swing.JButton jButtonHealthEducation;
    private javax.swing.JButton jButtonLabResult;
    private javax.swing.JButton jButtonPhysicalExam;
    private javax.swing.JButton jButtonPrintGuide;
    private javax.swing.JButton jButtonReDx;
    private javax.swing.JButton jButtonRefer;
    private javax.swing.JButton jButtonRegiterNCD;
    private javax.swing.JButton jButtonSaveDx;
    private javax.swing.JButton jButtonSaveGuide;
    private javax.swing.JButton jButtonSavePrimarySymptom;
    private javax.swing.JButton jButtonSaveVs;
    private javax.swing.JButton jButtonSurveil;
    private javax.swing.JButton jButtonViewDentistry;
    private javax.swing.JButton jButtonViewHistory;
    private javax.swing.JButton jButtonViewNCD;
    private javax.swing.JButton jButtonViewOBGYNCard;
    private javax.swing.JButton jButtonViewPTCard;
    private javax.swing.JButton jButtonViewSpecialClinic;
    private javax.swing.JCheckBox jCheckBoxAdmit;
    private javax.swing.JCheckBox jCheckBoxAppointment;
    private javax.swing.JCheckBox jCheckBoxBalloon;
    private javax.swing.JCheckBox jCheckBoxPEText;
    private javax.swing.JCheckBox jCheckBoxPregnant;
    private javax.swing.JCheckBox jCheckBoxRefer;
    private javax.swing.JComboBox jComboBoxAppointment;
    private com.hosv3.gui.component.HosComboBox jComboBoxDxTemplate;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel23;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JPanel jPanelDescription;
    private javax.swing.JPanel jPanelHide;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPaneCurrent;
    private javax.swing.JScrollPane jScrollPaneMainSymptom;
    private javax.swing.JScrollPane jScrollPaneMainSymptom1;
    private javax.swing.JScrollPane jScrollPaneNote;
    private javax.swing.JScrollPane jScrollPanePhyEx1;
    private javax.swing.JScrollPane jScrollPanePriSym;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JSplitPane jSplitPane2;
    private javax.swing.JSplitPane jSplitPane3;
    private com.hosv3.gui.component.HJTableSort jTableDx;
    private com.hosv3.gui.component.HJTableSort jTablePrimarySymptom;
    private com.hosv3.gui.component.BalloonTextArea jTextAreaCurrent;
    private com.hosv3.gui.component.BalloonTextArea jTextAreaGuide;
    private com.hosv3.gui.component.BalloonTextArea jTextAreaMainSymptom;
    private javax.swing.JTextArea jTextAreaNote;
    private com.hosv3.gui.component.BalloonTextArea jTextAreaPhysicalExam;
    private javax.swing.JTextField jTextFieldAppointment;
    private javax.swing.JLabel lblDiffDatetime;
    private javax.swing.JLabel lblShowSeverity;
    private javax.swing.JLabel lblShowTrama;
    private javax.swing.JLabel lblShowTransportation;
    private javax.swing.JLabel lblTxtSeverity;
    private javax.swing.JLabel lblTxtTrama;
    private javax.swing.JLabel lblTxtTransportation;
    private com.hosv3.gui.panel.detail.PDVitalSign pDVitalSign1;
    private javax.swing.JPanel panelButtonPlugin;
    private javax.swing.JPanel panelDatatime;
    private javax.swing.JPanel panelLMP;
    private javax.swing.JPanel panelMainSymptom;
    private javax.swing.JRadioButton rbtnDrugAllergy1;
    private javax.swing.JRadioButton rbtnDrugAllergy2;
    private javax.swing.JRadioButton rbtnDrugAllergy3;
    private com.hospital_os.utility.TimeTextField timeTextFieldCheck;
    private com.hospital_os.utility.TimeTextField timeTextFieldIllness;
    // End of variables declaration//GEN-END:variables
    public void getDxTemplate() {
        DxTemplate dt = new DxTemplate();
        if (jComboBoxDxTemplate.getSelectedItem() instanceof DxTemplate) {
            dt = (DxTemplate) jComboBoxDxTemplate.getSelectedItem();
        } else if (!jComboBoxDxTemplate.isNewText()) {
            dt.description = (String) jComboBoxDxTemplate.getSelectedItem();
            dt.setObjectId(String.valueOf(jComboBoxDxTemplate.getSelectedIndex()));
        }
        saveDxForEmployee(dt, false);
    }

    public void saveDxTemplate() {
        DxTemplate dt = null;
        if (jComboBoxDxTemplate.getSelectedItem() instanceof DxTemplate) {
            dt = new DxTemplate();
            dt = (DxTemplate) jComboBoxDxTemplate.getSelectedItem();
        } else if (!jComboBoxDxTemplate.isNewText()) {
            dt = new DxTemplate();
            dt.description = (String) jComboBoxDxTemplate.getSelectedItem();
            dt.setObjectId(String.valueOf(jComboBoxDxTemplate.getSelectedIndex()));
        }
        saveDxForEmployee(dt, false);
    }

    public void saveDxForEmployee(DxTemplate dxt, boolean isReDx) {
        boolean isSuccessDx = false;
        if (dxt != null && !dxt.description.isEmpty()) {
            // �����ᾷ������ͧ�ʴ� dialog ������������ʴ��� default clinic �繢ͧ����� login ����
            if (theHC.theLookupControl.readOption().isUseAutoDxICD10()) {
                if (Authentication.isDoctorDiagGroup(theHO.theEmployee.authentication_id)
                        && theHC.theLookupControl.readOption().isUseAutoDxICD10UseDoctorLogin()) {
                    theHO.theDiagDoctorClinic.doctor_id = theHO.theEmployee.getObjectId();
                    theHO.theDiagDoctorClinic.clinic_id = theHO.theEmployee.b_visit_clinic_id;
                } else {
                    theHO.theDiagDoctorClinic.doctor_id = theHO.getDoctorIDInVisit();
                    theHO.theDiagDoctorClinic.clinic_id = theHO.theEmployee.b_visit_clinic_id;
                    boolean isOK = theHD.showDialogDiag(theHO.theDiagDoctorClinic);
                    if (!isOK) {
                        return;
                    }
                }

            }

            boolean isComplete = theHC.theVitalControl.addDxTemplate(dxt);
            if (!isComplete) {
                return;
            }
            if (theHC.theLookupControl.readOption().isUseAutoDxICD10()) {
                //�����Ũж١�ѹ�֡ŧ㹵���âͧ HO ����������Ǵѧ�����Ҩе�ͧ�礵�ǹ�����ҧ���ǡ��
                if (dxt.icd_code.indexOf('.') == -1) {
                    MapVisitDx mvd = null;
                    for (int j = 0; j < theHO.vMapVisitDx.size(); j++) {
                        mvd = (MapVisitDx) theHO.vMapVisitDx.get(j);
                        if (mvd.dx_template_id == null ? dxt.getObjectId() == null : mvd.dx_template_id.equals(dxt.getObjectId())) {
                            break;
                        }
                    }
                    if (theDCFT == null) {
                        theDCFT = new DialogChooseICD10FromTemplate(theHC, theGPS, theGHS, theUS);
                    }
                    if (dxt.icd_type.equals("2")) {
                        theDCFT.showDialog(mvd);
                    }
                }
            }

            theVisit.diagnosis_note = jTextAreaNote.getText();
            isSuccessDx = theHC.theVitalControl.saveDiagDoctor(theVisit, theHO.theDiagDoctorClinic, dxt);
        }
        // save only remark
        if (!isSuccessDx) {
            theHC.theVitalControl.saveDxNote(jTextAreaNote.getText());
        }
    }

    private void deleteDxs(int[] rows) {
        theHC.theVitalControl.deleteDiagMaps(theVisit, vMapVisitDx, rows);
    }

    private void setEnabledPrimarySymptom(boolean b) {
        jButtonAddPrimarySymptom.setEnabled(b);
        jButtonDelPrimarySymptom.setEnabled(b);
        jButtonSavePrimarySymptom.setEnabled(b);
        jTextAreaMainSymptom.setEditable(b);
        jTextAreaCurrent.setEditable(b);
        dateComboBoxIllness.setEnabled(b);
        timeTextFieldIllness.setEnabled(b);
        btnIllnessAddr.setEnabled(b);
    }

    private void setPrimarySymptomV(Vector vPrimarySymptom1) {
        vPrimarySymptom = vPrimarySymptom1;
        TaBleModel tm;
        if (vPrimarySymptom == null || vPrimarySymptom.isEmpty()) {
            tm = new TaBleModel(col_jTablePrimarySymptom, 0);
            jTablePrimarySymptom.setModel(tm);
            PrimarySymptom vss = null;
            setPrimarySymptom(vss);
            return;
        }
        tm = new TaBleModel(col_jTablePrimarySymptom, vPrimarySymptom.size());
        for (int i = 0; i < vPrimarySymptom.size(); i++) {
            PrimarySymptom ps = (PrimarySymptom) vPrimarySymptom.get(i);
//            tm.setValueAt(DateUtil.getDateFromText(ps.record_date_time), i, 0);
            tm.setValueAt(DateUtil.getDateFromText(ps.check_date + "," + ps.check_time), i, 0);
        }
        int row = jTablePrimarySymptom.getSelectedRow();
        if (row == -1 || row >= vPrimarySymptom.size()) {
            row = 0;
        }
        jTablePrimarySymptom.setModel(tm);
        jTablePrimarySymptom.getColumnModel().getColumn(0).setCellRenderer(dateRender);
        jTablePrimarySymptom.setRowSelectionInterval(row, row);
        PrimarySymptom vss = (PrimarySymptom) vPrimarySymptom.get(row);
        setPrimarySymptom(vss);
    }

    private PrimarySymptom getPrimarySymptom() {
        if (thePrimarySymptom == null) {
            thePrimarySymptom = new PrimarySymptom();
        }
        // Somprasong 04102012 move Gutil.CheckReservedWords to DB object
        thePrimarySymptom.main_symptom = jTextAreaMainSymptom.getText();//Gutil.CheckReservedWords(jTextAreaMainSymptom.getText());
        thePrimarySymptom.current_illness = jTextAreaCurrent.getText();//Gutil.CheckReservedWords(jTextAreaCurrent.getText());
        thePrimarySymptom.illness_date_time = (dateComboBoxIllness.getText().isEmpty()
                ? theHC.theLookupControl.getTextCurrentDate() : dateComboBoxIllness.getText()) + "," + (timeTextFieldIllness.getTextTime().length() != 5 ? "00:00" : timeTextFieldIllness.getTextTime());
        thePrimarySymptom.check_date = dateComboBoxCheck.getText();
        thePrimarySymptom.check_time = timeTextFieldCheck.getText();
        return thePrimarySymptom;
    }

    private void setPrimarySymptom(PrimarySymptom ps) {
        thePrimarySymptom = ps;
        dateComboBoxIllness.setText(null);
        timeTextFieldIllness.resetTime();
        dateComboBoxCheck.reset();
        timeTextFieldCheck.resetTime();
        if (thePrimarySymptom == null) {
            thePrimarySymptom = new PrimarySymptom();
            jTextAreaMainSymptom.setText("");
            jTextAreaCurrent.setText("");
            dateComboBoxIllness.setText(null);
            timeTextFieldIllness.resetTime();
            dateComboBoxCheck.reset();
            timeTextFieldCheck.resetTime();
            return;
        }
        jTextAreaMainSymptom.setText(thePrimarySymptom.main_symptom);
        jTextAreaCurrent.setText(thePrimarySymptom.current_illness);
        if (thePrimarySymptom.illness_date_time != null
                && !thePrimarySymptom.illness_date_time.isEmpty()) {
            dateComboBoxIllness.setText(DateUtil.convertFieldDate(thePrimarySymptom.illness_date_time));
            if (thePrimarySymptom.illness_date_time.length() > 11) {
                timeTextFieldIllness.setText(thePrimarySymptom.illness_date_time.substring(11));
            }
        }
        if (thePrimarySymptom.check_date != null && !thePrimarySymptom.check_date.isEmpty()) {
            this.dateComboBoxCheck.setText(DateUtil.convertFieldDate(thePrimarySymptom.check_date));
        }
        if (thePrimarySymptom.check_time != null && !thePrimarySymptom.check_time.isEmpty()) {
            this.timeTextFieldCheck.setText(thePrimarySymptom.check_time);
        }
        this.calIllnessDateDiff();
    }

    private void calIllnessDateDiff() {
        String illDate = sd.util.datetime.DateTimeUtil.convertDatePattern(
                dateComboBoxIllness.getText()
                + "," + (timeTextFieldIllness.getTextTime().length() != 5 ? "00:00" : timeTextFieldIllness.getTextTime()),
                "yyyy-MM-dd,HH:mm", sd.util.datetime.DateTimeUtil.LOCALE_TH,
                "yyyy-MM-dd,HH:mm:ss", sd.util.datetime.DateTimeUtil.LOCALE_EN);
        String currentDate = sd.util.datetime.DateTimeUtil.convertDatePattern(
                theHC.theLookupControl.getTextCurrentDateTime(),
                "yyyy-MM-dd,HH:mm:ss", sd.util.datetime.DateTimeUtil.LOCALE_TH,
                "yyyy-MM-dd,HH:mm:ss", sd.util.datetime.DateTimeUtil.LOCALE_EN);
        String calDateTimeDiff = DateUtil.calIllnessDateTimeDiff(illDate, currentDate);
        lblDiffDatetime.setText("(" + calDateTimeDiff + ")");
    }

    /**
     * ૵�����������ͧ˹�� VitalSign check_old
     * ��Ǩ�ͺʶҹ��ʹյ���������������� ��������������
     * ����������������
     *
     * @param b
     */
    public void setPatientEnabled(boolean b) {
        jButtonLabResult.setEnabled(b);
        jButtonViewHistory.setEnabled(b);
        jButtonRegiterNCD.setEnabled(b);// �Դ���ŧ������������ͧ visit
        //jButtonViewNCD.setEnabled(b);//amp:15/06/2549
        jButtonAccident.setEnabled(b);
        jButtonDeath.setEnabled(b);
        jButtonSurveil.setEnabled(b);
        jButtonChronic.setEnabled(b);
    }

    @Override
    public void setEnabled(boolean b) {
        setPatientEnabled(b);
        setVisitEnabled(b);
    }
    boolean visit_enabled = false;

    public void setVisitEnabled(boolean b) {
        visit_enabled = b;
        rbtnDrugAllergy1.setEnabled(b);
        rbtnDrugAllergy2.setEnabled(b);
        rbtnDrugAllergy3.setEnabled(b);
        jTextAreaNote.setEnabled(b);
        jButtonSaveDx.setEnabled(b);
        jCheckBoxPregnant.setEnabled(b && is_woman);
        chkbLMP.setEnabled(b && is_woman);
        dateComboBoxLMP.setEnabled(b && is_woman && chkbLMP.isSelected());
        lblShowTransportation.setEnabled(b);
        lblShowTrama.setEnabled(b);
        lblShowSeverity.setEnabled(b);
        jButtonSaveGuide.setEnabled(b);
        jCheckBoxAppointment.setEnabled(b);
        jCheckBoxAdmit.setEnabled(b);
        jCheckBoxRefer.setEnabled(b);
        jButtonAppointment.setEnabled(b);
        jButtonAdmit.setEnabled(b);
        jTextAreaGuide.setEnabled(b);
        jCheckBoxPEText.setEnabled(b); // sumo 25/08/2549
        jTableDx.setEnabled(b);
        jTextAreaNote.setEnabled(b);
        setEnabledPrimarySymptom(b);
        pDVitalSign1.setEnabled(b);
        jButtonHealthEducation.setEnabled(b);
        jButtonSaveVs.setEnabled(b);
        jButtonHealthEducation.setEnabled(b);
        jButtonPrintGuide.setEnabled(b);
        jTextAreaPhysicalExam.setEnabled(b && jCheckBoxPEText.isSelected());
        jButtonDeleteDx.setEnabled(b);
        jButtonReDx.setEnabled(b);
    }

    /**
     * �ѹ��������ҡ�Դ�֧ ��������ѹ��ͧ����� ������ ���
     * ��������������� �ҡ Visit
     * ����դ�ҡ����¤�������������շҧ���仺ѹ�֡�������� �ҡ����� Visit
     * �ӡѺ
     */
    private void setVisit(Visit v) {
        theVisit = v;
        if (theVisit == null) {
            jTextAreaNote.setText("");
            jCheckBoxAdmit.setSelected(false);
            jCheckBoxRefer.setSelected(false);
            jTextFieldAppointment.setText("");
            jComboBoxAppointment.setSelectedIndex(0);
            jComboBoxDxTemplate.setSelectedIndex(-1);
            setVisitEnabled(false);
            jTextAreaGuide.setText("");//amp : 16/02/2549
            updateTransportationType("1", false);
            updateTrama("2", false);
            updateSeverity("2", false);
            return;
        }
        //setData//////////////////////////////////////////////
        jCheckBoxPregnant.setSelected(Gutil.isSelected(theVisit.pregnant));

        chkbLMP.setSelected(theVisit.visit_lmp != null);
        dateComboBoxLMP.setEnabled(chkbLMP.isSelected());
        dateComboBoxLMP.setDate(theVisit.visit_lmp);

        jTextAreaNote.setText(v.diagnosis_note);
        if (v.deny_allergy.equals("1")) {
            rbtnDrugAllergy1.setSelected(true);
        } else if (v.deny_allergy.equals("2")) {
            rbtnDrugAllergy2.setSelected(true);
        } else if (v.deny_allergy.equals("3")) {
            rbtnDrugAllergy3.setSelected(true);
        } else {
            buttonGroupDrugAllergy.clearSelection();
        }

        /////////////////////////////////////////////////////////
        //sumo: 28/07/2549 �������͹䢡������Ҽ������ա�ùѴ
        jComboBoxAppointment.setSelectedIndex(0);
        jTextFieldAppointment.setText("");
        boolean is_appoint = v.have_appointment.equals("1");
        jCheckBoxAppointment.setSelected(is_appoint);
        jTextFieldAppointment.setEnabled(is_appoint);
        jComboBoxAppointment.setEnabled(is_appoint);
        if (is_appoint) {
            jTextFieldAppointment.setText(theVisit.cause_appointment);
            Gutil.setGuiData(jComboBoxAppointment, theVisit.cal_date_appointment);
        }
        jCheckBoxAdmit.setSelected(v.have_admit.equals("1"));
        jCheckBoxRefer.setSelected(v.have_refer.equals("1"));
        //�������͡�ҡ��кǹ��������
        //�����¨�˹��·ҧ���ᾷ������
        //�����������١��͡�¼���餹���

        if (theVisit.isLockingByOther(theHO.theEmployee.getObjectId())
                || theVisit.isDischargeDoctor()
                || theVisit.isDropVisit()) {
            setVisitEnabled(false);
            return;
        }
        updateTransportationType("4".equals(theVisit.f_transportation_type_id)
                ? theVisit.other_transportation
                : theVisit.f_transportation_type_id, false);
        updateTrama(theVisit.f_trama_status_id, false);
        updateSeverity(theVisit.emergency, false);
        setVisitEnabled(true);
        if (theHO.theEmployee.authentication_id.equals(Authentication.DOCTOR)) {
            jTableDx.requestFocus();
        }
        btnProgressNote.setVisible(
                theHC.theHO.theGActionAuthV.isReadPVitalProgressNote()
                && !theVisit.isVisitTypeOPD());
    }

    public void setLanguage(String msg) {
        GuiLang.setLanguage(col_jTableAllergy);
        GuiLang.setLanguage(col_jTablePrimarySymptom);
        GuiLang.setLanguage(col_jTableVitalSign);
        GuiLang.setLanguage(column_jTableListPhyEx);
        GuiLang.setLanguage(jButtonAccident);
        GuiLang.setLanguage(jButtonAdmit);
        GuiLang.setLanguage(jButtonAppointment);
        GuiLang.setLanguage(jButtonChronic);
        GuiLang.setLanguage(jButtonDeath);
        GuiLang.setLanguage(jButtonHealthEducation);
        GuiLang.setLanguage(jButtonLabResult);
        GuiLang.setLanguage(jButtonPhysicalExam);
        GuiLang.setLanguage(jButtonRefer);
        GuiLang.setLanguage(jButtonRegiterNCD);
        GuiLang.setLanguage(jButtonSavePrimarySymptom);
        GuiLang.setLanguage(jButtonSaveVs);
        GuiLang.setLanguage(jButtonSurveil);
        GuiLang.setLanguage(jButtonViewHistory);
        GuiLang.setLanguage(jButtonViewNCD);
        GuiLang.setLanguage(jButtonReDx);
        GuiLang.setLanguage(jCheckBoxAdmit);
        GuiLang.setLanguage(jCheckBoxAppointment);
        GuiLang.setLanguage(jCheckBoxPEText);
        GuiLang.setLanguage(jCheckBoxPregnant);
        GuiLang.setLanguage(jCheckBoxRefer);
        GuiLang.setLanguage(jLabel2);
        GuiLang.setTextBundle(jPanelDescription);
        GuiLang.setLanguage(pDVitalSign1);
        GuiLang.setLanguage(jCheckBoxBalloon);
        GuiLang.setLanguage(jLabel2);
    }

    /*
     * set Physical Exam � vector ���Ѻ GUI pu
     */
    private void setPhysicalExamNanV(Vector vPENan) {
        vPhysicalExamNan = vPENan;
        if (vPhysicalExamNan == null || vPhysicalExamNan.isEmpty()) {
            setPhysicalExamV(null);
            return;
        }
        PhysicalExamNan phn = new PhysicalExamNan();
        Vector vPhIn = phn.getForTextArea(vPENan);
        setPhysicalExamV(vPhIn);
    }
    //����������ѹ������ǹ�

    private void setPhysicalExamV(Vector vP) {
        vPhysicalExam = vP;
        String textArea = PhysicalExam.getForTextArea(vPhysicalExam);
        if (!textArea.isEmpty()
                && !(textArea.trim().endsWith(":") || textArea.trim().endsWith(","))) {
            textArea += "\n";
        }
        jTextAreaPhysicalExam.setText(textArea);
    }

    private void setHosObject(HosObject ho) {
        this.setEnabled(true);
        setAuthentication(ho.theEmployee);
        setPatient(ho.thePatient, ho.vDrugAllergy);
        setVisit(ho.theVisit);
        this.pDVitalSign1.setObjectV(ho.vVitalSign);
        setPrimarySymptomV(ho.vPrimarySymptom);
        setPhysicalExamNanV(ho.vPhysicalExam);
        setHealthEducationV(ho.vHealthEducation);
        setDxTemplateV(ho.vMapVisitDx);
        // check  button plugin isvisble when select patient or visit
        for (PanelButtonPluginProvider buttonPluginProvider : buttonPluginProviders) {
            buttonPluginProvider.getButton(PanelVitalSign.class).setVisible(buttonPluginProvider.isVisible());
        }
    }
    /**
     * ���ͤӹǳ�����������㹪�ǧ 0-6 �� �������� ���͹� check ��� �е�ͧ set
     * ����дѺ����ҡ���������� ���������: ����� �������͡: �����
     */
    boolean is_woman = false;

    private void setPatient(Patient p, Vector allergy) {
        thePatient = p;
        boolean patient_not_null = (thePatient != null);
        if (thePatient != null) {
            // ��Ҽ����·�����͡���Ȫ������͹ CheckBox ��駤���� �������˭ԧ����ʴ�  sumo 1/8/2549
            is_woman = theHO.thePatient.f_sex_id.equals(Sex.isWOMAN());
        }
        boolean patient_has_ncd = (theHO.vNCD != null && !theHO.vNCD.isEmpty());
        jButtonViewNCD.setEnabled(patient_not_null && patient_has_ncd);
    }

    private void setDxTemplateV(List<MapVisitDx> vMapDx) {
        vMapVisitDx = vMapDx;
        TaBleModel tm;
        if (vMapVisitDx == null || vMapVisitDx.isEmpty()) {
            tm = new TaBleModel(col_jTableDx, 0);
            jTableDx.setModel(tm);
            return;
        }
        tm = new TaBleModel(col_jTableDx, vMapVisitDx.size());
        for (int i = 0; i < vMapVisitDx.size(); i++) {
            MapVisitDx theMapVisit = (MapVisitDx) vMapVisitDx.get(i);
            String dxName = theMapVisit.visit_diag_map_dx + (theMapVisit.thaidescription == null ? "" : ":" + theMapVisit.thaidescription);
            tm.setValueAt(dxName, i, 0);
        }
        jTableDx.setModel(tm);
        jTableDx.getColumnModel().getColumn(0).setCellRenderer(theCellRendererTooltip);
        jTableDx.setRowSelectionInterval(0, 0);
    }

    public void showPatientHistory() {
        if (theHO.thePatient == null) {
            theUS.setStatus("��س����͡������", UpdateStatus.WARNING);
            return;
        }
        if (thePPH == null) {
            thePPH = new PanelPatientHistory(theHC, theUS);
        }
        thePPH.showDialog(0);
    }

    public void showPhysicalExam() {
        if (theHO.theVisit == null) {
            theUS.setStatus("��س����͡�����·������㹡�кǹ���", UpdateStatus.WARNING);
            return;
        }
        if (thePPE == null) {
            thePPE = new PanelPhysicalExam(theHC, theUS);
        }

        boolean is_enabled;
        if (theHO.theVisit == null) {
            is_enabled = true;
        } else {
            is_enabled = !theHO.theVisit.is_discharge_doctor.equals("1")
                    && theHO.isLockingVisit();
        }
        thePPE.showDialog(vPhysicalExamNan, is_enabled);
    }

    public void showHealthEducation() {
        if (theHO.theVisit == null) {
            theUS.setStatus("��س����͡�����·������㹡�кǹ���", UpdateStatus.WARNING);
            return;
        }
        if (thePHE == null) {
            thePHE = new PanelHealthEducation(theHC, theUS);
        }
        if (vHealthEducation == null) {
            vHealthEducation = new Vector();
        }

        Vector temp = (Vector) vHealthEducation.clone();
        if (thePHE.showDialog(this.vHealthEducation)) {
            if (!jTextAreaGuide.getText().trim().isEmpty()) {
                if (!theUS.confirmBox(Constant.getTextBundle("�������Ť��й�����Ѻ������") + " "
                        + Constant.getTextBundle("��س��׹�ѹ��úѹ�֡�Ѻ�����Ť��й�����Ѻ������"), UpdateStatus.WARNING)) {
                    vHealthEducation = temp;
                    return;
                }
            }
            theHC.theVisitControl.saveGuideHealthEducation(vHealthEducation);

            setHealthEducationV(vHealthEducation);
        }
    }

    /**
     * @Author : sumo
     * @date : 20/06/2549
     * @see : �ʴ��������آ�֡��
     */
    private void setHealthEducationV(Vector vHealthEdu) {
        vHealthEducation = vHealthEdu;
        String textArea = GuideAfterDxTransaction.toString(vHealthEdu);
        if (textArea != null && !textArea.isEmpty()
                && !textArea.trim().endsWith(":")) {
            textArea += "\n";
        }
        jTextAreaGuide.setText(textArea);
    }

    private boolean getPhysicalExam(Vector vP) {
        if (vP == null) {
            return false;
        }
        vP.removeAllElements();
        if (jTextAreaPhysicalExam.getText().trim().isEmpty() || jTextAreaPhysicalExam.getText().trim().equals("\n")) {
            return true;
        }
        int line_count = this.jTextAreaPhysicalExam.getLineCount();
        int count = 0;
        for (int i = 0; i < line_count; i++) {
            try {
                int start = jTextAreaPhysicalExam.getLineStartOffset(i);
                int end = jTextAreaPhysicalExam.getLineEndOffset(i);
                int char_length = end - start;
                String line = jTextAreaPhysicalExam.getText(start, char_length);
                if (line.length() == 0) {
                    continue;
                }
                int colon_index = line.indexOf(':');
                if (colon_index == -1) {
                    return false;
                } else {
                    count++;
                }
                // ���:ᵡ,�����ʹ�͡,��ͧ���
                int start_index = colon_index + 1;
                int end_index = line.indexOf(',');
                String body = line.substring(0, colon_index).trim();
                if (body.isEmpty()) {
                    return false;
                }
                while (end_index != -1) {
                    PhysicalExam pe = new PhysicalExam();
                    pe.physical_body = body;
                    pe.detail = line.substring(start_index, end_index).trim();
                    if (pe.detail.endsWith("\n")) {
                        pe.detail = pe.detail.substring(0, pe.detail.length() - 1);
                    }
                    vP.add(pe);
                    start_index = end_index + 1;
                    end_index = line.indexOf(',', start_index);
                }
                end_index = line.length();
                PhysicalExam pe = new PhysicalExam();
                pe.physical_body = body;
                pe.detail = line.substring(start_index, end_index).trim();
                if (pe.detail.endsWith("\n")) {
                    pe.detail = pe.detail.substring(0, pe.detail.length() - 1);
                }
                vP.add(pe);
            } catch (Exception ex) {
                LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
                return false;
            }
        }
        return count > 0;
    }

    private void getGuide() {
        int line_count = this.jTextAreaGuide.getLineCount();
        vGuide = new Vector();
        if (jTextAreaGuide.getText().trim().isEmpty()) {
            return;
        }
        for (int i = 0; i < line_count; i++) {
            try {
                int start = jTextAreaGuide.getLineStartOffset(i);
                int end = jTextAreaGuide.getLineEndOffset(i);
                String line = jTextAreaGuide.getText(start, end - start);
                if (line.endsWith("\n")) {
                    line = line.substring(0, line.length() - 1);
                }
                if (!line.trim().isEmpty()) {
                    GuideAfterDxTransaction gu = new GuideAfterDxTransaction();
                    if (line.indexOf(':') > -1) {
                        gu.health_head = line.substring(0, line.indexOf(':'));
                        gu.guide = line.substring(line.indexOf(':') + 1, line.length());
                    } else {
                        gu.health_head = "";
                        gu.guide = line;
                    }
                    vGuide.add(gu);
                }
            } catch (Exception ex) {
                LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
                //Constant.println("PanelVitalSign:" + e.printStackTrace(Constant.getPrintStream()));
            }
        }
    }

    @Override
    public void notifyAdmitVisit(String str, int status) {
    }

    @Override
    public void notifyReadVisit(String str, int status) {
        setHosObject(theHO);
    }

    @Override
    public void notifyObservVisit(String str, int status) {
    }

    @Override
    public void notifyUnlockVisit(String str, int status) {
        setHosObject(theHO);
    }

    @Override
    public void notifyVisitPatient(String str, int status) {
        setHosObject(theHO);
    }

    @Override
    public void notifyManageDrugAllergy(String str, int status) {
        setVisit(theHO.theVisit);
    }

    @Override
    public void notifyManageVitalSign(String str, int status) {
        pDVitalSign1.setObjectV(theHO.vVitalSign);
    }

    @Override
    public void notifySavePatientPayment(String str, int status) {
    }

    @Override
    public void notifyCheckDoctorTreament(String str, int status) {
    }

    @Override
    public void notifyDischargeDoctor(String str, int status) {
        setVisit(theHO.theVisit);
    }

    @Override
    public void notifyManagePrimarySymptom(String str, int status) {
        setPrimarySymptomV(theHO.vPrimarySymptom);
    }

    @Override
    public void notifyDeleteMapVisitDxByVisitId(String str, int status) {
    }

    @Override
    public void notifySaveDiagDoctor(String str, int status) {
        setHealthEducationV(theHO.vHealthEducation);
        setDxTemplateV(theHO.vMapVisitDx);
        jComboBoxDxTemplate.requestFocus();
    }

    @Override
    public void notifyDropVisit(String str, int status) {
        setHosObject(theHO);
    }

    @Override
    public void notifySendVisit(String str, int status) {
        setHosObject(theHO);
    }

    @Override
    public void notifyDischargeFinancial(String str, int status) {
        setHosObject(theHO);
    }

    @Override
    public void notifyReverseFinancial(String str, int status) {
        this.setEnabled(true);
        setVisit(theHO.theVisit);
    }

    @Override
    public void notifyManagePhysicalExam(String str, int status) {
        setPhysicalExamNanV(theHO.vPhysicalExam);
    }

    @Override
    public void notifyReverseDoctor(String str, int status) {
        this.setEnabled(true);
        setVisit(theHO.theVisit);
    }

    @Override
    public void notifyAddDxTemplate(String str, int status) {
        //setGuide();
    }

    @Override
    public void notifyDeleteVisitPayment(String str, int status) {
    }

    @Override
    public void notifyDeletePatientPayment(String str, int status) {
    }

    @Override
    public void notifyAddPrimarySymptom(String str, int status) {
        String text = jTextAreaMainSymptom.getText();
        if (!text.isEmpty()) {
            jTextAreaMainSymptom.setText(text + "," + theHO.theVitalTemplate.description);
        } else {
            jTextAreaMainSymptom.setText(theHO.theVitalTemplate.description);
        }
    }

    @Override
    public void notifyReadPatient(String str, int status) {
        setHosObject(theHO);
    }

    @Override
    public void notifyReadFamily(String str, int status) {
        setHosObject(theHO);
    }

    @Override
    public void notifyDeletePatient(String str, int status) {
    }

    @Override
    public void notifySavePatient(String str, int status) {
        setPatient(thePatient, null);
    }

    @Override
    public void notifyRemainDoctorDischarge(String str, int status) {
        setVisit(theHO.theVisit);
    }

    @Override
    public void notifySendVisitBackWard(String str, int status) {
        setHosObject(theHO);
    }

    @Override
    public void notifySaveAppointment(String str, int status) {
    }

    @Override
    public void notifyReverseAdmit(String str, int status) {
    }

    @Override
    public void notifyResetPatient(String str, int status) {
        setHosObject(theHO);
    }

    @Override
    public void notifyManagePatientLabReferIn(String str, int status) {
    }

    @Override
    public void notifySaveLabResult(String str, int status) {
    }

    @Override
    public void notifyDeleteLabResult(String str, int status) {
    }

    @Override
    public void notifyDeleteLabOrder(String str, int status) {
    }

    @Override
    public void notifySaveRemainLabResult(String str, int status) {
    }

    @Override
    public void notifySaveFilmXray(String str, int status) {
    }

    @Override
    public void notifyDeleteFilmXray(String str, int status) {
    }

    @Override
    public void notifyDeleteResultXray(String str, int status) {
    }

    @Override
    public void notifySaveXrayPosition(String str, int status) {
    }

    @Override
    public void notifyDeleteXrayPosition(String str, int status) {
    }

    @Override
    public void notifySaveResultXray(String str, int status) {
    }

    @Override
    public void notifyXrayReportComplete(String str, int status) {
        setHosObject(theHO);
    }

    @Override
    public void notifyReportResultLab(String str, int status) {
    }

    @Override
    public void notifyAddLabReferOut(String str, int status) {
    }

    @Override
    public void notifyAddLabReferIn(String str, int status) {
    }

    @Override
    public void notifySendResultLab(String str, int status) {
        setHosObject(theHO);
    }

    @Override
    public void notifyDeleteQueueLab(String str, int status) {
        setHosObject(theHO);
    }

    @Override
    public void notifyAdmitLeaveHome(String str, int status) {
    }

    private void addFromSelected() {
        int row = jTablePrimarySymptom.getSelectedRow();
        if (row == -1) {
            return;
        }
        jTablePrimarySymptom.clearSelection();
        PrimarySymptom primarySymptom = (PrimarySymptom) vPrimarySymptom.get(row);
        thePrimarySymptom = new PrimarySymptom();
        thePrimarySymptom.main_symptom = primarySymptom.main_symptom;
        thePrimarySymptom.current_illness = primarySymptom.current_illness;
        thePrimarySymptom.illness_date_time = null;
        thePrimarySymptom.check_date = null;
        thePrimarySymptom.check_time = null;
        setPrimarySymptom(thePrimarySymptom);
        jTextAreaMainSymptom.requestFocus();
    }

    private void showFootExamDialog() {
        if (theHO.theVisit == null) {
            theUS.setStatus("��س����͡�����·������㹡�кǹ���", UpdateStatus.WARNING);
            return;
        }
        theHD.showFootExamDialog(theHO.theVisit);
    }

    private void showEyesExamDialog() {
        if (theHO.theVisit == null) {
            theUS.setStatus("��س����͡�����·������㹡�кǹ���", UpdateStatus.WARNING);
            return;
        }
        theHD.showEyeExamDialog(theHO.theVisit);
    }

    private void showAudiometryDialog() {
        if (theHO.theVisit == null) {
            theUS.setStatus("��س����͡�����·������㹡�кǹ���", UpdateStatus.WARNING);
            return;
        }
        theHD.showAudiometryDialog(theHO.theVisit);
    }

    @Override
    public void notifySaveInformIntolerance(Object... object) {
        //
    }

    private void showInformAnemiaDialog() {
        if (theHO.theVisit == null) {
            theUS.setStatus("��س����͡�����·������㹡�кǹ���", UpdateStatus.WARNING);
            return;
        }
        if (!(theHO.theEmployee.authentication_id.equals(Authentication.DOCTOR)
                || theHO.theEmployee.authentication_id.equals(Authentication.NURSE)
                || theHO.theEmployee.authentication_id.equals(Authentication.ONE))) {
            theUS.setStatus("੾�м����ҹ������Է����� ᾷ�� ���;�Һ����ҹ��", UpdateStatus.WARNING);
            return;
        }
        theHD.showInformAnemiaDialog(theHO.theVisit);
    }

    @Override
    public void notifyAddReDx(DxTemplate dx) {

    }

    @Override
    public void notifySaveReDx() {
    }
    private final List<PanelButtonPluginProvider> buttonPluginProviders = new ArrayList<>();

    private void serviceLoader() {
        panelButtonPlugin.removeAll();
        Iterator<PanelButtonPluginProvider> iterator = PanelButtonPluginManager.getInstance().getServices();
        String className = this.getClass().getName();
        // get buttons to list
        List<JButton> buttons = new ArrayList<>();
        while (iterator.hasNext()) {
            PanelButtonPluginProvider provider = iterator.next();
            if (provider.isOwnerPanel(PanelVitalSign.class)) {
                JButton button = provider.getButton(PanelVitalSign.class);
                boolean isVisible = provider.isVisible();
                LOG.log(Level.INFO, "{0} get button from {1}, visible: {2}", new Object[]{className, provider.getClass().getName(), isVisible});
                button.setVisible(isVisible);

                button.setMaximumSize(new Dimension(provider.getButtonWidth(), provider.getButtonHeight()));
                button.setMinimumSize(new Dimension(provider.getButtonWidth(), provider.getButtonHeight()));
                button.setPreferredSize(new Dimension(provider.getButtonWidth(), provider.getButtonHeight()));

                buttonPluginProviders.add(provider);
                int index = provider.getIndex();
                if (index > buttons.size()) {
                    buttons.add(button);
                } else {
                    buttons.add(index, button);
                }
            }
        }
        if (buttons.isEmpty()) {
            LOG.log(Level.INFO, "{0}  no button plugin!", className);
        } else {
            LOG.log(Level.INFO, "{0}  found {1} buttons plugin!", new Object[]{className, buttons.size()});
        }
        // add all plugin buttons to panel
        int iCount = 0;
        for (JButton button : buttons) {
            GridBagConstraints gridBagConstraints = new java.awt.GridBagConstraints();
            gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
            gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
            if (iCount == buttons.size() - 1) {
                gridBagConstraints.weightx = 1.0;
            }
            gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 3);
            panelButtonPlugin.add(button, gridBagConstraints);
            iCount++;
        }
        panelButtonPlugin.revalidate();
        panelButtonPlugin.repaint();
    }
    private static final Logger LOG = Logger.getLogger(PanelVitalSign.class.getName());
    private JPopupMenu popupTransportationType;
    private JPopupMenu popupTrama;
    private JPopupMenu popupSeverity;

    private void setPopup() {
        // PanelTransportation
        final PanelTransportation pt = new PanelTransportation();
        popupTransportationType = new JPopupMenu() {
            private static final long serialVersionUID = 1L;

            @Override
            public void setVisible(boolean b) {
                Boolean isCanceled = (Boolean) getClientProperty(
                        "JPopupMenu.firePopupMenuCanceled");
                if (b || !b
                        || ((isCanceled != null) && !b && isCanceled.booleanValue())) {
                    if (b) {
                        if (theVisit != null) {
                            pt.setVisit(theVisit);
                        } else {
                            b = false;
                        }
                    }
                    super.setVisible(b);
                }
            }
        };
        lblShowTransportation.setComponentPopupMenu(popupTransportationType);

        popupTransportationType.add(pt);
        pt.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (evt.getPropertyName().equals(PanelTransportation.PROP_NAME)) {
                    popupTransportationType.setVisible(false);
                    updateTransportationType((String) evt.getNewValue(), true);
                }
            }
        });
        // PanelTrama
        final PanelTrama trama = new PanelTrama();
        popupTrama = new JPopupMenu() {
            private static final long serialVersionUID = 1L;

            @Override
            public void setVisible(boolean b) {
                Boolean isCanceled = (Boolean) getClientProperty(
                        "JPopupMenu.firePopupMenuCanceled");
                if (b || !b
                        || ((isCanceled != null) && !b && isCanceled.booleanValue())) {
                    if (b) {
                        if (theVisit != null) {
                            trama.setVisit(theVisit);
                        } else {
                            b = false;
                        }
                    }
                    super.setVisible(b);
                }
            }
        };
        lblShowTrama.setComponentPopupMenu(popupTrama);

        popupTrama.add(trama);
        trama.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (evt.getPropertyName().equals(PanelTrama.PROP_NAME)) {
                    popupTrama.setVisible(false);
                    updateTrama((String) evt.getNewValue(), true);
                }
            }
        });
        // PanelSeverity
        final PanelSeverity severity = new PanelSeverity();
        popupSeverity = new JPopupMenu() {
            private static final long serialVersionUID = 1L;

            @Override
            public void setVisible(boolean b) {
                Boolean isCanceled = (Boolean) getClientProperty(
                        "JPopupMenu.firePopupMenuCanceled");
                if (b || !b
                        || ((isCanceled != null) && !b && isCanceled.booleanValue())) {
                    if (b) {
                        if (theVisit != null) {
                            severity.setVisit(theVisit);
                        } else {
                            b = false;
                        }
                    }
                    super.setVisible(b);
                }
            }
        };
        lblShowSeverity.setComponentPopupMenu(popupSeverity);

        popupSeverity.add(severity);
        severity.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (evt.getPropertyName().equals(PanelSeverity.PROP_NAME)) {
                    popupSeverity.setVisible(false);
                    updateSeverity((String) evt.getNewValue(), true);
                }
            }
        });
    }

    private void updateTransportationType(String type, boolean updateAble) {
        if ("1".equals(type)) {
            lblShowTransportation.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/walk.png"))); // NOI18N
            lblShowTransportation.setText("");
            lblShowTransportation.setToolTipText("�Թ��");
        } else if ("2".equals(type)) {
            lblShowTransportation.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/wheelchair.png"))); // NOI18N
            lblShowTransportation.setText("");
            lblShowTransportation.setToolTipText("�����");
        } else if ("3".equals(type)) {
            lblShowTransportation.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/on_bed.png"))); // NOI18N
            lblShowTransportation.setText("");
            lblShowTransportation.setToolTipText("�͹��");
        } else {
            lblShowTransportation.setIcon(null);
            lblShowTransportation.setText(type);
            lblShowTransportation.setToolTipText("����");
        }
        if (updateAble) {
            theHC.theVisitControl.updateVisitTransporttationType(type);
        }
    }

    private void updateTrama(String type, boolean updateAble) {
        if ("1".equals(type)) {
            lblShowTrama.setText("Trauma");
            lblShowTrama.setToolTipText("Trauma");
        } else {
            lblShowTrama.setText("Non Trauma");
            lblShowTrama.setToolTipText("Non Trauma");
        }
        if (updateAble) {
            theHC.theVisitControl.updateVisitTrama(type);
        }
    }

    private void updateSeverity(String type, boolean updateAble) {
        if ("1".equals(type)) {
            lblShowSeverity.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/nu@24.png"))); // NOI18N
            lblShowSeverity.setToolTipText("Non - Urgency");
            lblShowSeverity.setBackground(Color.white);
        } else if ("2".equals(type)) {
            lblShowSeverity.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/su@24.png"))); // NOI18N
            lblShowSeverity.setToolTipText("Semi - Urgency");
            lblShowSeverity.setBackground(Color.green);
        } else if ("3".equals(type)) {
            lblShowSeverity.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/ur@24.png"))); // NOI18N
            lblShowSeverity.setToolTipText("Urgency");
            lblShowSeverity.setBackground(Color.yellow);
        } else if ("4".equals(type)) {
            lblShowSeverity.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/em@24.png"))); // NOI18N
            lblShowSeverity.setToolTipText("Emergency");
            lblShowSeverity.setBackground(Color.pink);
        } else if ("5".equals(type)) {
            lblShowSeverity.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/rs@24.png"))); // NOI18N
            lblShowSeverity.setToolTipText("Resuscitation");
            lblShowSeverity.setBackground(Color.red);
        } else {
            lblShowSeverity.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/na@24.png"))); // NOI18N
            lblShowSeverity.setToolTipText("Undefine");
            lblShowSeverity.setBackground(Color.lightGray);
        }
        if (updateAble) {
            theHC.theVisitControl.updateVisitSeverity(type);
        }
    }

    @Override
    public void notifyManageDiagIcd9(String str, int status) {
    }

    @Override
    public void notifyManageDiagIcd10(String str, int status) {
        setHealthEducationV(theHO.vHealthEducation);
    }

    @Override
    public void notifyDeleteParticipateOr(String str, int status) {
    }

    @Override
    public void notifyLoaderService() {
        this.serviceLoader();
    }

    private void doUpdateDrugAllergyType() {
        int ret = theHC.thePatientControl.savePatientAllergy(
                rbtnDrugAllergy1.isSelected() ? "1"
                : rbtnDrugAllergy2.isSelected() ? "2"
                : rbtnDrugAllergy3.isSelected() ? "3"
                : "", theHO.vDrugAllergy);
        //�������¡�÷������������׹�ѹ���ź��¡���ҷ������� CheckBox ������������� sumo 04/09/2549
        if (ret != 0) {
            buttonGroupDrugAllergy.clearSelection();
        }
    }

    @Override
    public void notifyUpdatePatientPictureProfile(Object obj) {

    }

    private void updateVisitLMP() {
        if (chkbLMP.isEnabled() && chkbLMP.isSelected()) {
            theHC.theVisitControl.updateVisitLMP(dateComboBoxLMP.getDate());
        }
    }

    @Override
    public void notifySaveDxNote(String str, int status) {

    }
}

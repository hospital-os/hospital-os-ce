/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
public class ServiceLimitClinic extends Persistent {

    public String b_visit_clinic_id;
    public String time_start = "";
    public String time_end = "";
    public int limit_appointment = 0;
    public int limit_walkin = 0;
    // in object only
    public String clinicName = "";
}

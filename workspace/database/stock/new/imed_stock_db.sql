CREATE SCHEMA imed;

------------------------------------------------- Create All Table In iMed -------------------------------------------------

--
-- Name: abc_ven; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.abc_ven (
    abc_ven_id character varying(255) NOT NULL,
    fix_abc_id character varying(255),
    fix_ven_id character varying(255),
    range_of_day character varying(255),
    range_re_order character varying(255),
    range_max character varying(255),
    range_min character varying(255)
);


ALTER TABLE imed.abc_ven OWNER TO postgres;

--
-- Name: accident_emergency_report; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.accident_emergency_report (
    accident_emergency_report_id character varying(255) NOT NULL,
    visit_id character varying(255),
    patient_id character varying(255),
    claim_code character varying(255),
    outside_changwat character varying(255),
    accident_date character varying(255),
    accident_time character varying(255),
    accident_payment character varying(255),
    ucae character varying(255),
    emergency_type character varying(255),
    dz8 character varying(255),
    hc9 character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255)
);


ALTER TABLE imed.accident_emergency_report OWNER TO postgres;

--
-- Name: admission_note; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.admission_note (
    admission_note_id character varying(254) NOT NULL,
    admit_id character varying(254),
    main_symptom text,
    current_illness text,
    patient_history text,
    family_history text,
    drug_allergy text,
    patient_examine text,
    temperature character varying(254),
    pressure_max character varying(254),
    pressure_min character varying(254),
    pulse character varying(254),
    respiration character varying(254),
    diagnosis text,
    plan text,
    doctor_eid character varying(254),
    modify_eid character varying(254),
    modify_date character varying(254),
    modify_time character varying(254)
);


ALTER TABLE imed.admission_note OWNER TO postgres;

--
-- Name: admit; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.admit (
    admit_id character varying(255) NOT NULL,
    visit_id character varying(255),
    patient_id character varying(255),
    hn character varying(255),
    an character varying(255),
    admit_doctor_eid character varying(255),
    admit_eid character varying(255),
    admit_spid character varying(255),
    admit_date character varying(255),
    admit_time character varying(255),
    patient_relate character varying(255),
    patient_relate_note character varying(255),
    base_department_id character varying(255),
    length_of_stay character varying(255),
    ipd_discharge character varying(255),
    ipd_discharge_eid character varying(255),
    ipd_discharge_date character varying(255),
    ipd_discharge_time character varying(255),
    doctor_allow character varying(255),
    doctor_allow_eid character varying(255),
    doctor_allow_date character varying(255),
    doctor_allow_time character varying(255),
    times_admit character varying(255),
    assess_stay character varying(255),
    assess_price character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    active character varying(255),
    fix_ipd_file_status_id character varying(255),
    fix_patient_class_id character varying(255),
    base_med_department_id character varying(255),
    is_observe character varying(255)
);


ALTER TABLE imed.admit OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: anes_assessment_data; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.anes_assessment_data (
    anes_assessment_data_id character varying(255) NOT NULL,
    anes_data_id character varying(255) NOT NULL,
    fix_anes_asa_level character varying(255),
    is_asa_e character varying(255),
    is_assessment_problem character varying(255),
    assessment_problem_note text,
    anesthesia_plan character varying(255),
    suggested_preparations character varying(255),
    is_blood_prepared_prc character varying(255),
    blood_prepared_prc_quantity character varying(255),
    is_blood_prepared_ffp character varying(255),
    blood_prepared_ffp_quantity character varying(255),
    is_blood_prepared_platelet character varying(255),
    blood_prepared_platelet_quantity character varying(255),
    is_blood_prepared_cryoprecipitate character varying(255),
    blood_prepared_cryoprecipitate_quantity character varying(255),
    assessment_preparation_other text
);


ALTER TABLE imed.anes_assessment_data OWNER TO postgres;

--
-- Name: anes_complication; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.anes_complication (
    anes_complication_id character varying(255) NOT NULL,
    anes_data_id character varying(255) NOT NULL,
    base_anes_complication_id character varying(255) NOT NULL,
    fix_anes_complication_level character varying(255)
);


ALTER TABLE imed.anes_complication OWNER TO postgres;

--
-- Name: anes_cvs; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.anes_cvs (
    anes_cvs_id character varying(255) NOT NULL,
    anes_data_id character varying(255) NOT NULL,
    base_anes_cvs_id character varying(255) NOT NULL
);


ALTER TABLE imed.anes_cvs OWNER TO postgres;

--
-- Name: anes_data; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.anes_data (
    anes_data_id character varying(255) NOT NULL,
    visit_id character varying(255),
    patient_id character varying(255),
    op_set_id character varying(255),
    op_registered_id character varying(255),
    start_anes_date character varying(255),
    start_anes_time character varying(255),
    finish_anes_date character varying(255),
    finish_anes_time character varying(255),
    base_anes_outcome_id character varying(255),
    outcome_reason character varying(255),
    fix_anes_direct_transferred_to character varying(255),
    fix_anes_patient_satisfaction character varying(255),
    patient_satisfaction_reason character varying(255),
    recorder_eid character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    is_discharge character varying(255),
    anes_number character varying(255)
);


ALTER TABLE imed.anes_data OWNER TO postgres;

--
-- Name: anes_general_data; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.anes_general_data (
    anes_general_data_id character varying(255) NOT NULL,
    anes_data_id character varying(255) NOT NULL,
    is_inform character varying(255),
    is_consent character varying(255),
    fix_anes_functional_class character varying(255),
    fix_anes_evaluation character varying(255),
    is_prev_anesthesia character varying(255),
    prev_anesthesia_note text,
    current_medication character varying(255),
    is_perform_emergency character varying(255),
    is_perform_outtime character varying(255),
    is_complication character varying(255),
    complication_note text,
    allergy character varying(255)
);


ALTER TABLE imed.anes_general_data OWNER TO postgres;

--
-- Name: anes_inhalation; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.anes_inhalation (
    anes_inhalation_id character varying(255) NOT NULL,
    anes_data_id character varying(255) NOT NULL,
    base_anes_inhalation_id character varying(255) NOT NULL
);


ALTER TABLE imed.anes_inhalation OWNER TO postgres;

--
-- Name: anes_lab_investigation; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.anes_lab_investigation (
    anes_lab_investigation_id character varying(255) NOT NULL,
    anes_data_id character varying(255) NOT NULL,
    is_lab_cxr_abnormal character varying(255),
    lab_cxr_note text,
    is_lab_ecg_abnormal character varying(255),
    lab_ecg_note text,
    is_lab_echo_abnormal character varying(255),
    lab_echo_note text,
    lab_investigate_other text
);


ALTER TABLE imed.anes_lab_investigation OWNER TO postgres;

--
-- Name: anes_local_agent; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.anes_local_agent (
    anes_local_agent_id character varying(255) NOT NULL,
    anes_data_id character varying(255) NOT NULL,
    base_anes_local_agent_id character varying(255) NOT NULL,
    local_agent_percent character varying(255),
    local_agent_quantity character varying(255)
);


ALTER TABLE imed.anes_local_agent OWNER TO postgres;

--
-- Name: anes_monitor_technique; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.anes_monitor_technique (
    anes_monitor_technique_id character varying(255) NOT NULL,
    anes_data_id character varying(255) NOT NULL,
    monitor_note text,
    technique_note text,
    special_tech_note text,
    airway_note text,
    local_agent_note text,
    anesthetic_inhalation_note text,
    fix_anes_laryngoscopic_view character varying(255)
);


ALTER TABLE imed.anes_monitor_technique OWNER TO postgres;

--
-- Name: anes_monitors; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.anes_monitors (
    anes_monitors_id character varying(255) NOT NULL,
    anes_data_id character varying(255) NOT NULL,
    base_anes_monitor_id character varying(255) NOT NULL
);


ALTER TABLE imed.anes_monitors OWNER TO postgres;

--
-- Name: anes_pacu_data; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.anes_pacu_data (
    anes_pacu_data_id character varying(255) NOT NULL,
    anes_data_id character varying(255) NOT NULL,
    start_date character varying(255),
    start_time character varying(255),
    discharge_date character varying(255),
    discharge_time character varying(255),
    ps_min character varying(255),
    ps_max character varying(255),
    pacu_reason character varying(255)
);


ALTER TABLE imed.anes_pacu_data OWNER TO postgres;

--
-- Name: anes_physical_exam_data; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.anes_physical_exam_data (
    anes_physical_exam_data_id character varying(255) NOT NULL,
    anes_data_id character varying(255) NOT NULL,
    weight character varying(255),
    height character varying(255),
    bmi character varying(255),
    pressure_max character varying(255),
    pressure_min character varying(255),
    heart_rate character varying(255),
    respiration_rate character varying(255),
    temperature character varying(255),
    fix_anes_concious_type character varying(255),
    fix_anes_airway_class character varying(255),
    fix_anes_thyromental_distance character varying(255),
    fix_anes_mouth_opening character varying(255),
    is_patency_of_nares character varying(255),
    is_limitation_motility character varying(255),
    limitation_motility_note text,
    is_difficult_intubation_expected character varying(255),
    difficult_intubation_expected_note text,
    fix_anes_dental character varying(255),
    dental_note text,
    is_rs_abnormal character varying(255),
    rs_note text,
    is_ventilatory_pattern character varying(255),
    ventilatory_pattern_note text,
    oxygen_theraphy_note text,
    cvs_note text,
    is_abdomen_abnormal character varying(255),
    abdomen_note text,
    is_extremities_abnormal character varying(255),
    extremities_note text,
    is_neurologic_abnormal character varying(255),
    neurologic_note text,
    gcs_e character varying(255),
    gcs_v character varying(255),
    gcs_m character varying(255),
    is_landmark_for_ra_abnormal character varying(255),
    landmark_for_ra_note text,
    fix_anes_oxygen_theraphy character varying(255),
    is_cvs_abnormal character varying(255)
);


ALTER TABLE imed.anes_physical_exam_data OWNER TO postgres;

--
-- Name: anes_po_pain_management; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.anes_po_pain_management (
    anes_po_pain_management_id character varying(255) NOT NULL,
    anes_data_id character varying(255) NOT NULL,
    ps_at_rest character varying(255),
    ps_on_activity character varying(255),
    is_iv_im character varying(255),
    iv_im_note text,
    is_pca character varying(255),
    pca_note text,
    is_lumbar_epidural character varying(255),
    is_lumbar_epidural_remain character varying(255),
    lumbar_epidural_off_date character varying(255),
    is_thoracic_epidural character varying(255),
    is_thoracic_epidural_remain character varying(255),
    thoracic_epidural_off_date character varying(255),
    is_nerve_block character varying(255),
    is_nerve_block_remain character varying(255),
    nerve_block_off_date character varying(255),
    is_caudal character varying(255),
    is_spinal character varying(255),
    is_oral character varying(255),
    oral_note text,
    is_other character varying(255),
    other_note text,
    start_use_painkiller_date character varying(255),
    start_use_painkiller_time character varying(255)
);


ALTER TABLE imed.anes_po_pain_management OWNER TO postgres;

--
-- Name: anes_pre_op_condition; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.anes_pre_op_condition (
    anes_pre_op_condition_id character varying(255) NOT NULL,
    anes_data_id character varying(255) NOT NULL,
    base_anes_pre_op_condition_id character varying(255) NOT NULL
);


ALTER TABLE imed.anes_pre_op_condition OWNER TO postgres;

--
-- Name: anes_premedication; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.anes_premedication (
    anes_premedication_id character varying(255) NOT NULL,
    anes_data_id character varying(255) NOT NULL,
    base_anes_premedication_id character varying(255) NOT NULL,
    fix_anes_premedication_result character varying(255)
);


ALTER TABLE imed.anes_premedication OWNER TO postgres;

--
-- Name: anes_service; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.anes_service (
    anes_service_id character varying(255) NOT NULL,
    anes_data_id character varying(255) NOT NULL,
    base_anes_service_id character varying(255) NOT NULL
);


ALTER TABLE imed.anes_service OWNER TO postgres;

--
-- Name: anes_special_tech; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.anes_special_tech (
    anes_special_tech_id character varying(255) NOT NULL,
    anes_data_id character varying(255) NOT NULL,
    base_anes_special_tech_id character varying(255) NOT NULL
);


ALTER TABLE imed.anes_special_tech OWNER TO postgres;

--
-- Name: anes_team; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.anes_team (
    anes_team_id character varying(255) NOT NULL,
    anes_data_id character varying(255) NOT NULL,
    employee_id character varying(255) NOT NULL,
    base_op_role_id character varying(255) NOT NULL
);


ALTER TABLE imed.anes_team OWNER TO postgres;

--
-- Name: anes_technique; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.anes_technique (
    anes_technique_id character varying(255) NOT NULL,
    anes_data_id character varying(255) NOT NULL,
    base_anes_technique_id character varying(255) NOT NULL
);


ALTER TABLE imed.anes_technique OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: appointment; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.appointment (
    appointment_id character varying(255) NOT NULL,
    patient_id character varying(255),
    base_spid character varying(255),
    doctor_eid character varying(255),
    doctor_assigner_eid character varying(255),
    base_department_id character varying(255),
    basic_advice text,
    note character varying(255),
    appoint_date character varying(255),
    appoint_time character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    fix_appointment_status_id character varying(255),
    appointment_schedule_id character varying(255),
    fix_appointment_type_id character varying(255),
    continue_ref_no character varying(255),
    continue_note character varying(255),
    visit_id character varying(255),
    close_status_date character varying(255),
    close_status_time character varying(255),
    make_appointment_visit_id character varying(255),
    base_cancel_appointment_reason_id character varying(255),
    estimate_hour character varying(255),
    estimate_minute character varying(255),
    fix_appointment_method_id character varying(255)
);


ALTER TABLE imed.appointment OWNER TO postgres;

--
-- Name: appointment_doctor; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.appointment_doctor (
    appointment_doctor_id character varying(255) NOT NULL,
    appointment_id character varying(255),
    doctor_eid character varying(255),
    base_clinic_id character varying(255),
    basic_advice character varying(255),
    note character varying(255),
    appoint_date character varying(255),
    appoint_time character varying(255)
);


ALTER TABLE imed.appointment_doctor OWNER TO postgres;

--
-- Name: appointment_order_item; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.appointment_order_item (
    appointment_order_item_id character varying(255) NOT NULL,
    appointment_id character varying(255) DEFAULT ''::character varying,
    item_id character varying(255) DEFAULT ''::character varying,
    fix_item_type_id character varying(255) DEFAULT ''::character varying,
    base_drug_usage_code character varying(255) DEFAULT ''::character varying,
    instruction_format character varying(255) DEFAULT ''::character varying,
    base_drug_instruction_id character varying(255) DEFAULT ''::character varying,
    dose_quantity character varying(255) DEFAULT ''::character varying,
    base_dose_unit_id character varying(255) DEFAULT ''::character varying,
    base_drug_frequency_id character varying(255) DEFAULT ''::character varying,
    instruction_text_line1 character varying(255) DEFAULT ''::character varying,
    instruction_text_line2 character varying(255) DEFAULT ''::character varying,
    instruction_text_line3 character varying(255) DEFAULT ''::character varying,
    quantity character varying(255) DEFAULT ''::character varying,
    base_unit_id character varying(255) DEFAULT ''::character varying,
    description character varying(255) DEFAULT ''::character varying,
    caution character varying(255) DEFAULT ''::character varying,
    modify_eid character varying(255) DEFAULT ''::character varying,
    modify_date character varying(255) DEFAULT ''::character varying,
    modify_time character varying(255) DEFAULT ''::character varying
);


ALTER TABLE imed.appointment_order_item OWNER TO postgres;

--
-- Name: assign_lab; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.assign_lab (
    assign_lab_id character varying(255) NOT NULL,
    visit_id character varying(255),
    ln character varying(255),
    assign_doctor_eid character varying(255),
    assign_eid character varying(255),
    assign_spid character varying(255),
    assign_date character varying(255),
    assign_time character varying(255),
    receive_spid character varying(255),
    assign_order_status character varying(255),
    keep_specimen_eid character varying(255),
    receive_specimen_eid character varying(255),
    receive_specimen_date character varying(255),
    receive_specimen_time character varying(255),
    complete_date character varying(255),
    complete_time character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    max_print_times character varying(255),
    receive_all_specimen character varying(255),
    assign_lab_note text,
    keep_specimen_date character varying(255),
    keep_specimen_time character varying(255),
    clinical_history text,
    clinical_diagnosis character varying(255),
    fix_lab_result_type_id character varying(255),
    out_doctor character varying(255),
    out_doctor_hospital_id character varying(255),
    out_doctor_note character varying(255),
    is_frozen_section character varying(255),
    is_assign_over_time character varying(255)
);


ALTER TABLE imed.assign_lab OWNER TO postgres;

--
-- Name: assign_times; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.assign_times (
    assign_times_id character varying(254) NOT NULL,
    patient_id character varying(254),
    xray_times character varying(254),
    ultrasound_times character varying(254),
    ekg_times character varying(254)
);


ALTER TABLE imed.assign_times OWNER TO postgres;

--
-- Name: assign_xray; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.assign_xray (
    assign_xray_id character varying(254) NOT NULL,
    visit_id character varying(254),
    assign_order_number character varying(254),
    assign_doctor_eid character varying(254),
    assign_eid character varying(254),
    assign_spid character varying(254),
    assign_date character varying(254),
    assign_time character varying(254),
    receive_spid character varying(254),
    xray_times character varying(254),
    ultrasound_times character varying(254),
    ekg_times character varying(254),
    assign_order_status character varying(254),
    complete_execute_date character varying(254),
    complete_execute_time character varying(254),
    complete_date character varying(254),
    complete_time character varying(254),
    is_wheel_chair_type character varying(254),
    is_stretcher_type character varying(254),
    is_portable_type character varying(254),
    is_o2_type character varying(254),
    is_slide_type character varying(254),
    is_wheel_bed_type character varying(254),
    modify_eid character varying(254),
    modify_date character varying(254),
    modify_time character varying(254),
    max_print_times character varying(255)
);


ALTER TABLE imed.assign_xray OWNER TO postgres;

--
-- Name: attending_physician; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.attending_physician (
    attending_physician_id character varying(255) NOT NULL,
    employee_id character varying(255),
    visit_id character varying(255),
    priority character varying(255),
    base_department_id character varying(255),
    fix_doctor_visit_status character varying(255),
    begin_date character varying(255),
    begin_time character varying(255),
    finish_date character varying(255),
    finish_time character varying(255),
    supervisor_eid character varying(255),
    assign_date character varying(255),
    assign_time character varying(255)
);


ALTER TABLE imed.attending_physician OWNER TO postgres;

--
-- Name: base_accident_zone; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_accident_zone (
    base_accident_zone_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_accident_zone OWNER TO postgres;

--
-- Name: base_address; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_address (
    fullcode character varying(255) NOT NULL,
    description character varying(255),
    type character varying(255),
    ampcode character varying(255),
    cgwcode character varying(255),
    region character varying(255),
    post_code character varying(255)
);


ALTER TABLE imed.base_address OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: base_adp_code; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_adp_code (
    base_adp_code_id character varying(255) NOT NULL,
    base_adp_type_id character varying(255) DEFAULT ''::character varying,
    description character varying(255) DEFAULT ''::character varying
);


ALTER TABLE imed.base_adp_code OWNER TO postgres;

--
-- Name: base_adp_type; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_adp_type (
    base_adp_type_id character varying(255) NOT NULL,
    description character varying(255) DEFAULT ''::character varying
);


ALTER TABLE imed.base_adp_type OWNER TO postgres;

--
-- Name: base_anes_airway; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_anes_airway (
    base_anes_airway_id character varying(255) NOT NULL,
    item_id character varying(255)
);


ALTER TABLE imed.base_anes_airway OWNER TO postgres;

--
-- Name: base_anes_anesthetic_agent; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_anes_anesthetic_agent (
    base_anes_anesthetic_agent_id character varying(255) NOT NULL,
    item_id character varying(255)
);


ALTER TABLE imed.base_anes_anesthetic_agent OWNER TO postgres;

--
-- Name: base_anes_complication; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_anes_complication (
    base_anes_complication_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_anes_complication OWNER TO postgres;

--
-- Name: base_anes_cvs; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_anes_cvs (
    base_anes_cvs_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_anes_cvs OWNER TO postgres;

--
-- Name: base_anes_inhalation; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_anes_inhalation (
    base_anes_inhalation_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_anes_inhalation OWNER TO postgres;

--
-- Name: base_anes_local_agent; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_anes_local_agent (
    base_anes_local_agent_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_anes_local_agent OWNER TO postgres;

--
-- Name: base_anes_monitor; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_anes_monitor (
    base_anes_monitor_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_anes_monitor OWNER TO postgres;

--
-- Name: base_anes_outcome; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_anes_outcome (
    base_anes_outcome_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_anes_outcome OWNER TO postgres;

--
-- Name: base_anes_pre_op_condition; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_anes_pre_op_condition (
    base_anes_pre_op_condition_id character varying(255) NOT NULL,
    base_anes_pre_op_condition_group_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_anes_pre_op_condition OWNER TO postgres;

--
-- Name: base_anes_pre_op_condition_group; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_anes_pre_op_condition_group (
    base_anes_pre_op_condition_group_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_anes_pre_op_condition_group OWNER TO postgres;

--
-- Name: base_anes_premedication; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_anes_premedication (
    base_anes_premedication_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_anes_premedication OWNER TO postgres;

--
-- Name: base_anes_service; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_anes_service (
    base_anes_service_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_anes_service OWNER TO postgres;

--
-- Name: base_anes_special_tech; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_anes_special_tech (
    base_anes_special_tech_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_anes_special_tech OWNER TO postgres;

--
-- Name: base_anes_technique; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_anes_technique (
    base_anes_technique_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_anes_technique OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: base_antibiotic; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_antibiotic (
    base_antibiotic_id character varying(254) NOT NULL,
    description character varying(254),
    show_default character varying(254),
    resistant_less_than character varying(255),
    susceptible_more_than character varying(255)
);


ALTER TABLE imed.base_antibiotic OWNER TO postgres;

--
-- Name: base_any; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_any (
    base_any_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_any OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: base_aw_result; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_aw_result (
    base_aw_result_id character(2) NOT NULL,
    description character(10)
);


ALTER TABLE imed.base_aw_result OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: base_bad_film_reason; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_bad_film_reason (
    base_bad_film_reason_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_bad_film_reason OWNER TO postgres;

--
-- Name: base_bank; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_bank (
    base_bank_id character varying(255) NOT NULL,
    bank_name_th character varying(255),
    bank_name_en character varying(255)
);


ALTER TABLE imed.base_bank OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: base_bb_blood_transfusion_reason; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_bb_blood_transfusion_reason (
    base_bb_blood_transfusion_reason_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_bb_blood_transfusion_reason OWNER TO postgres;

--
-- Name: base_bb_issue_status; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_bb_issue_status (
    base_bb_issue_status_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_bb_issue_status OWNER TO postgres;

--
-- Name: base_bb_packcell_wastage_reason; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_bb_packcell_wastage_reason (
    base_bb_packcell_wastage_reason_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_bb_packcell_wastage_reason OWNER TO postgres;

--
-- Name: base_bb_xm_result; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_bb_xm_result (
    base_bb_xm_result_id character varying(255) NOT NULL,
    description character varying(255),
    is_default character varying(255),
    is_abnormal character varying(255)
);


ALTER TABLE imed.base_bb_xm_result OWNER TO postgres;

--
-- Name: base_bb_xm_result_detail; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_bb_xm_result_detail (
    base_bb_xm_result_detail_id character varying(255) NOT NULL,
    base_bb_xm_result_id character varying(255),
    result_rt character varying(255),
    result_37 character varying(255),
    result_ahg character varying(255)
);


ALTER TABLE imed.base_bb_xm_result_detail OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: base_billing_group; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_billing_group (
    base_billing_group_id character varying(255) NOT NULL,
    fix_item_type_id character varying(255),
    code character varying(255),
    law_code character varying(255),
    description_th character varying(255),
    description_en character varying(255),
    law_sum character varying(255),
    assu_code character varying(255),
    description_assu_th character varying(255),
    description_assu_en character varying(255),
    assu_sum character varying(255),
    fax_claim_code character varying(255),
    description_fc_th character varying(255),
    description_fc_en character varying(255),
    fax_claim_sum character varying(255),
    map2ss character varying(255),
    acc_group character varying(255),
    is_print_ipd_acc character varying(255)
);


ALTER TABLE imed.base_billing_group OWNER TO postgres;

--
-- Name: base_blood_group; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_blood_group (
    base_blood_group_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_blood_group OWNER TO postgres;

--
-- Name: base_branch_bank; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_branch_bank (
    base_branch_bank_id character varying(255) NOT NULL,
    base_bank_id character varying(255),
    branch_bank_code character varying(255),
    branch_bank_name character varying(255),
    branch_address text,
    fix_tambol_id character varying(255),
    fix_amphur_id character varying(255),
    fix_changwat_id character varying(255),
    comments text
);


ALTER TABLE imed.base_branch_bank OWNER TO postgres;

--
-- Name: base_cancel_appointment_reason; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_cancel_appointment_reason (
    base_cancel_appointment_reason_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_cancel_appointment_reason OWNER TO postgres;

--
-- Name: base_cancel_receipt_reason; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_cancel_receipt_reason (
    base_cancel_receipt_reason_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_cancel_receipt_reason OWNER TO postgres;

--
-- Name: base_category_group; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_category_group (
    base_category_group_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_category_group OWNER TO postgres;

--
-- Name: base_clinic; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_clinic (
    base_clinic_id character varying(255) NOT NULL,
    description character varying(255),
    fix_clinic_id character varying(255),
    active character varying(255)
);


ALTER TABLE imed.base_clinic OWNER TO postgres;

--
-- Name: base_consult_case; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_consult_case (
    base_consult_case_id character varying(255) NOT NULL,
    description character varying(255),
    base_department_id character varying(255)
);


ALTER TABLE imed.base_consult_case OWNER TO postgres;

--
-- Name: base_consult_result; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_consult_result (
    base_consult_result_id character varying(255) NOT NULL,
    description character varying(255),
    base_consult_case_id character varying(255)
);


ALTER TABLE imed.base_consult_result OWNER TO postgres;

--
-- Name: base_consult_treatment; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_consult_treatment (
    base_consult_treatment_id character varying(255) NOT NULL,
    description character varying(255),
    base_consult_case_id character varying(255)
);


ALTER TABLE imed.base_consult_treatment OWNER TO postgres;

--
-- Name: base_country; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_country (
    description character varying(255),
    base_country_id character varying(255) NOT NULL
);


ALTER TABLE imed.base_country OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: base_credit_card; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_credit_card (
    base_credit_card_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_credit_card OWNER TO postgres;

--
-- Name: base_custom_print; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_custom_print (
    base_custom_print_id character varying(255) NOT NULL,
    custom_print_name character varying(255),
    custom_print_file character varying(255),
    custom_print_priority character varying(255)
);


ALTER TABLE imed.base_custom_print OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: base_custom_tab; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_custom_tab (
    base_custom_tab_id character varying(255) NOT NULL,
    custom_tab_name character varying(255),
    custom_tab_url character varying(255),
    custom_tab_priority character varying(255)
);


ALTER TABLE imed.base_custom_tab OWNER TO postgres;

--
-- Name: base_death_status; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_death_status (
    base_death_status_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_death_status OWNER TO postgres;

--
-- Name: base_deliver_document; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_deliver_document (
    base_deliver_document_id character varying(255) NOT NULL,
    description_th character varying(255),
    description_en character varying(255)
);


ALTER TABLE imed.base_deliver_document OWNER TO postgres;

--
-- Name: base_dent_diag_detail; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_dent_diag_detail (
    base_dent_diag_detail_id character varying(255) NOT NULL,
    description text
);


ALTER TABLE imed.base_dent_diag_detail OWNER TO postgres;

--
-- Name: base_dent_diagnosis; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_dent_diagnosis (
    base_dent_diagnosis_id character varying(255) NOT NULL,
    description character varying(255) DEFAULT ''::character varying
);


ALTER TABLE imed.base_dent_diagnosis OWNER TO postgres;

--
-- Name: base_dent_operation; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_dent_operation (
    base_dent_operation_id character varying(255) NOT NULL,
    description character varying(255) DEFAULT ''::character varying
);


ALTER TABLE imed.base_dent_operation OWNER TO postgres;

--
-- Name: base_dent_organic; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_dent_organic (
    base_dent_organic_id character varying(255) NOT NULL,
    description character varying(255) DEFAULT ''::character varying
);


ALTER TABLE imed.base_dent_organic OWNER TO postgres;

--
-- Name: base_department; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_department (
    base_department_id character varying(255) NOT NULL,
    description character varying(255),
    is_treat_patient character varying(255),
    base_clinic_id character varying(255),
    active character varying(255)
);


ALTER TABLE imed.base_department OWNER TO postgres;

--
-- Name: base_deposit_bank; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_deposit_bank (
    base_deposit_bank_id character varying(255) NOT NULL,
    bank_code character varying(255),
    bank_branch_code character varying(255),
    account_number character varying(255),
    description character varying(255)
);


ALTER TABLE imed.base_deposit_bank OWNER TO postgres;

--
-- Name: base_df_mode; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_df_mode (
    base_df_mode_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_df_mode OWNER TO postgres;

--
-- Name: base_diagnosis505; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_diagnosis505 (
    base_diagnosis505_id character varying(255) NOT NULL,
    icd10_range character varying(255),
    diagnosis505_description character varying(255)
);


ALTER TABLE imed.base_diagnosis505 OWNER TO postgres;

--
-- Name: base_diagnosis506; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_diagnosis506 (
    base_diagnosis506_id character varying(255) NOT NULL,
    diagnosis506_description_en text,
    diagnosis506_description_th text,
    alias_name character varying(255),
    icd10_range character varying(255)
);


ALTER TABLE imed.base_diagnosis506 OWNER TO postgres;

--
-- Name: base_diagnosis_employee; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_diagnosis_employee (
    base_diagnosis_employee_id character varying(255) NOT NULL,
    base_diagnosis_set_id character varying(255),
    base_clinic_id character varying(255),
    employee_id character varying(255)
);


ALTER TABLE imed.base_diagnosis_employee OWNER TO postgres;

--
-- Name: base_diagnosis_set; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_diagnosis_set (
    base_diagnosis_set_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_diagnosis_set OWNER TO postgres;

--
-- Name: base_disc_command; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_disc_command (
    base_disc_command_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_disc_command OWNER TO postgres;

--
-- Name: base_disc_special_card; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_disc_special_card (
    base_disc_special_card_id character varying(255) NOT NULL,
    description character varying(255),
    template_discount_id character varying(255),
    inspire_date character varying(255),
    expire_date character varying(255),
    active character varying(255),
    condition_to_use text
);


ALTER TABLE imed.base_disc_special_card OWNER TO postgres;

--
-- Name: base_discount_reason; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_discount_reason (
    base_discount_reason_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_discount_reason OWNER TO postgres;

--
-- Name: base_doctor_discharge_type; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_doctor_discharge_type (
    base_doctor_discharge_type_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_doctor_discharge_type OWNER TO postgres;

--
-- Name: base_dose_unit; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_dose_unit (
    base_dose_unit_id character varying(255) NOT NULL,
    description_th character varying(255),
    description_en character varying(255)
);


ALTER TABLE imed.base_dose_unit OWNER TO postgres;

--
-- Name: base_drug_caution; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_drug_caution (
    base_drug_caution_id character varying(255) NOT NULL,
    description_th character varying(255),
    description_en character varying(255)
);


ALTER TABLE imed.base_drug_caution OWNER TO postgres;

--
-- Name: base_drug_dose; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_drug_dose (
    base_drug_dose_id character varying(255) NOT NULL,
    description_th character varying(255),
    description_en character varying(255),
    is_dose_unit_include character varying(255)
);


ALTER TABLE imed.base_drug_dose OWNER TO postgres;

--
-- Name: base_drug_format; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_drug_format (
    base_drug_format_id character varying(255) NOT NULL,
    description character varying(255) DEFAULT ''::character varying
);


ALTER TABLE imed.base_drug_format OWNER TO postgres;

--
-- Name: base_drug_frequency; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_drug_frequency (
    base_drug_frequency_id character varying(255) NOT NULL,
    description_th character varying(255),
    description_en character varying(255),
    times_per_day character varying(255)
);


ALTER TABLE imed.base_drug_frequency OWNER TO postgres;

--
-- Name: base_drug_generic_group_name; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_drug_generic_group_name (
    base_drug_name_id character varying(255) NOT NULL,
    base_drug_generic_name character varying(255),
    base_drug_group_name character varying(255)
);


ALTER TABLE imed.base_drug_generic_group_name OWNER TO postgres;

--
-- Name: base_drug_generic_name; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_drug_generic_name (
    base_drug_generic_name character varying(255) NOT NULL
);


ALTER TABLE imed.base_drug_generic_name OWNER TO postgres;

--
-- Name: base_drug_generic_trade_name; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_drug_generic_trade_name (
    base_drug_name_id character varying(255) NOT NULL,
    base_drug_generic_name character varying(255),
    base_drug_trade_name character varying(255)
);


ALTER TABLE imed.base_drug_generic_trade_name OWNER TO postgres;

--
-- Name: base_drug_group; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_drug_group (
    base_drug_group_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_drug_group OWNER TO postgres;

--
-- Name: base_drug_group_name; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_drug_group_name (
    base_drug_group_name character varying(255) NOT NULL
);


ALTER TABLE imed.base_drug_group_name OWNER TO postgres;

--
-- Name: base_drug_instruction; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_drug_instruction (
    base_drug_instruction_id character varying(255) NOT NULL,
    description_th character varying(255),
    description_en character varying(255),
    base_dose_unit_id character varying(255)
);


ALTER TABLE imed.base_drug_instruction OWNER TO postgres;

--
-- Name: base_drug_interaction; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_drug_interaction (
    base_drug_interaction_id character varying(255) NOT NULL,
    drug_generic_name_1 character varying(255),
    drug_generic_name_2 character varying(255),
    degree character varying(255),
    effect text,
    mechanism text,
    recommend text
);


ALTER TABLE imed.base_drug_interaction OWNER TO postgres;

--
-- Name: base_drug_major_class; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_drug_major_class (
    base_drug_major_class_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_drug_major_class OWNER TO postgres;

--
-- Name: base_drug_pain; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_drug_pain (
    base_drug_pain_id character varying(255) NOT NULL,
    description_th character varying(255),
    description_en character varying(255)
);


ALTER TABLE imed.base_drug_pain OWNER TO postgres;

--
-- Name: base_drug_pregnancy_risk; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_drug_pregnancy_risk (
    base_drug_pregnancy_risk_id character varying(255) NOT NULL,
    drug_generic_name character varying(255),
    detail text
);


ALTER TABLE imed.base_drug_pregnancy_risk OWNER TO postgres;

--
-- Name: base_drug_special; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_drug_special (
    base_drug_special_id character varying(255) NOT NULL,
    description_th character varying(255),
    description_th_line2 character varying(255),
    description_en character varying(255),
    description_en_line2 character varying(255)
);


ALTER TABLE imed.base_drug_special OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: base_drug_std; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_drug_std (
    base_drug_std_id integer NOT NULL,
    std_code character varying(255) DEFAULT ''::character varying,
    regno character varying(255) DEFAULT ''::character varying,
    t_code character varying(255) DEFAULT ''::character varying,
    gpocode character varying(255) DEFAULT ''::character varying,
    gpocheck character varying(255) DEFAULT ''::character varying,
    comp character varying(255) DEFAULT ''::character varying,
    tradename character varying(255) DEFAULT ''::character varying,
    dosage_form character varying(255) DEFAULT ''::character varying,
    dgdsfnm character varying(255) DEFAULT ''::character varying,
    unit character varying(255) DEFAULT ''::character varying,
    drugname character varying(255) DEFAULT ''::character varying,
    strength character varying(255) DEFAULT ''::character varying,
    manufacturer character varying(255) DEFAULT ''::character varying,
    country character varying(255) DEFAULT ''::character varying,
    un_spsc character varying(255) DEFAULT ''::character varying,
    ok_record character varying(255) DEFAULT ''::character varying,
    select_true_false character varying(255) DEFAULT ''::character varying,
    version character varying(255) DEFAULT ''::character varying,
    findcomp character varying(255) DEFAULT ''::character varying
);


ALTER TABLE imed.base_drug_std OWNER TO postgres;

--
-- Name: base_drug_std_base_drug_std_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE imed.base_drug_std_base_drug_std_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE imed.base_drug_std_base_drug_std_id_seq OWNER TO postgres;

--
-- Name: base_drug_std_base_drug_std_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE base_drug_std_base_drug_std_id_seq OWNED BY base_drug_std.base_drug_std_id;


SET default_with_oids = true;

--
-- Name: base_drug_sub_class; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_drug_sub_class (
    base_drug_sub_class_id character varying(255) NOT NULL,
    base_drug_major_class_id character varying(255),
    description character varying(255)
);


ALTER TABLE imed.base_drug_sub_class OWNER TO postgres;

--
-- Name: base_drug_time; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_drug_time (
    base_drug_time_id character varying(255) NOT NULL,
    description_th character varying(255),
    description_en character varying(255)
);


ALTER TABLE imed.base_drug_time OWNER TO postgres;

--
-- Name: base_drug_trade_name; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_drug_trade_name (
    base_drug_trade_name character varying(255) NOT NULL
);


ALTER TABLE imed.base_drug_trade_name OWNER TO postgres;

--
-- Name: base_drug_type; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_drug_type (
    base_drug_type_id character varying(255) NOT NULL,
    description character varying(255),
    base_dose_unit_id character varying(255),
    base_drug_instruction_id character varying(255),
    icon_name character varying(255)
);


ALTER TABLE imed.base_drug_type OWNER TO postgres;

--
-- Name: base_drug_usage; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_drug_usage (
    base_drug_usage_id character varying(255) NOT NULL,
    base_drug_usage_code character varying(255),
    instruction_format character varying(255),
    base_drug_instruction_id character varying(255),
    dose_quantity character varying(255),
    base_dose_unit_id character varying(255),
    base_drug_frequency_id character varying(255),
    instruction_text_line1 character varying(255),
    instruction_text_line2 character varying(255),
    instruction_text_line3 character varying(255),
    instruction_text_line1_en character varying(255),
    instruction_text_line2_en character varying(255),
    instruction_text_line3_en character varying(255)
);


ALTER TABLE imed.base_drug_usage OWNER TO postgres;

--
-- Name: base_dt_diagnosis; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_dt_diagnosis (
    base_dt_diagnosis_id character varying(255) NOT NULL,
    description character varying(255),
    base_dt_symbol_id character varying(255),
    fix_dt_action_type_id character varying(255)
);


ALTER TABLE imed.base_dt_diagnosis OWNER TO postgres;

--
-- Name: base_dt_diagnosis_default; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_dt_diagnosis_default (
    base_dt_diagnosis_default_id character varying(255) NOT NULL,
    base_dt_diagnosis_id character varying(255),
    employee_id character varying(255)
);


ALTER TABLE imed.base_dt_diagnosis_default OWNER TO postgres;

--
-- Name: base_dt_item; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_dt_item (
    base_dt_item_id character varying(255) NOT NULL,
    item_id character varying(255),
    base_dt_symbol_id character varying(255),
    fix_dt_action_type_id character varying(255),
    is_treatment character varying(255)
);


ALTER TABLE imed.base_dt_item OWNER TO postgres;

--
-- Name: base_dt_symbol; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_dt_symbol (
    base_dt_symbol_id character varying(255) NOT NULL,
    fix_dt_symbol_type_id character varying(255)
);


ALTER TABLE imed.base_dt_symbol OWNER TO postgres;

--
-- Name: base_dt_tx_type; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_dt_tx_type (
    base_dt_tx_type_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_dt_tx_type OWNER TO postgres;

--
-- Name: base_employee_role; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_employee_role (
    base_employee_role_id character varying(255) NOT NULL,
    description character varying(255),
    tabmenu_auth character varying(255),
    mainmenu_auth character varying(255),
    patient_data_submenu_auth character varying(255),
    service_submenu_auth character varying(255),
    order_submenu_auth character varying(255),
    cash_submenu_auth character varying(255),
    appointment_submenu_auth character varying(255),
    opd_submenu_auth character varying(255),
    ipd_submenu_auth character varying(255),
    medical_submenu_auth character varying(255),
    xray_submenu_auth character varying(255),
    schedule_submenu_auth character varying(255),
    print_submenu_auth character varying(255),
    system_submenu_auth character varying(255),
    patient_social_button_auth character varying(255),
    operation_button_auth character varying(255),
    labor_button_auth character varying(255),
    emergency_button_auth character varying(255),
    patient_medical_button_auth character varying(255),
    lab_button_auth character varying(255),
    xray_button_auth character varying(255),
    vital_sign_button_auth character varying(255),
    order_button_auth character varying(255),
    icd10_button_auth character varying(255),
    icd9_button_auth character varying(255),
    billing_opd_button_auth character varying(255),
    billing_ipd_button_auth character varying(255),
    admit_button_auth character varying(255),
    emr_auth character varying(255),
    order_auth character varying(255),
    extra_auth character varying(255),
    page_default character varying(255),
    admin_module_auth character varying(255),
    admin_system_auth character varying(255),
    admin_user_and_sp_auth character varying(255),
    admin_item_auth character varying(255),
    admin_drug_detail_auth character varying(255),
    admin_stock_auth character varying(255),
    admin_stock_manage_auth character varying(255),
    admin_stock_report_auth character varying(255),
    admin_medical_auth character varying(255),
    admin_lab_auth character varying(255),
    admin_xray_auth character varying(255),
    admin_operation_auth character varying(255),
    admin_register_auth character varying(255),
    admin_dental_auth character varying(255),
    admin_billing_auth character varying(255),
    admin_other_auth character varying(255),
    admin_report_auth character varying(255),
    admin_manage_item_auth character varying(255),
    admin_manage_base_auth character varying(255),
    admin_manage_help_auth character varying(255),
    report_tab_module_auth character varying(255),
    custom_tab_auth character varying(255),
    nutrition_button_auth character varying(255),
    doc_scan_auth character varying(255) DEFAULT '000'::character varying,
    admin_manage_med_device_auth character varying(255),
    admin_nutrition_auth character varying(255),
    admin_blood_bank_auth character varying(255),
    anesthesia_button_auth character varying(255),
    admin_forensic_auth character varying(255),
    out_hospital_access_auth character varying(255),
    df_submenu_auth character varying(255)
);


ALTER TABLE imed.base_employee_role OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: base_er_victim_type; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_er_victim_type (
    base_er_victim_type_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_er_victim_type OWNER TO postgres;

--
-- Name: base_forensic_death_behaviour; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_forensic_death_behaviour (
    base_forensic_death_behaviour_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_forensic_death_behaviour OWNER TO postgres;

--
-- Name: base_forensic_in_exam_detail; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_forensic_in_exam_detail (
    base_forensic_in_exam_detail_id character varying(255) NOT NULL,
    description character varying(255),
    base_forensic_in_exam_group_id character varying(255),
    fix_forensic_in_exam_type character varying(255)
);


ALTER TABLE imed.base_forensic_in_exam_detail OWNER TO postgres;

--
-- Name: base_forensic_in_exam_group; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_forensic_in_exam_group (
    base_forensic_in_exam_group_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_forensic_in_exam_group OWNER TO postgres;

--
-- Name: base_forensic_out_exam; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_forensic_out_exam (
    base_forensic_out_exam_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_forensic_out_exam OWNER TO postgres;

--
-- Name: base_generation; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_generation (
    base_generation_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_generation OWNER TO postgres;

--
-- Name: base_group_practice; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_group_practice (
    base_group_practice_id character varying(255) NOT NULL,
    base_group_practice_name character varying(255),
    tax_id character varying(255),
    bank_account_name character varying(255),
    bank_account_number character varying(255),
    bank_name character varying(255)
);


ALTER TABLE imed.base_group_practice OWNER TO postgres;

--
-- Name: base_group_practice_doctor; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_group_practice_doctor (
    base_group_practice_doctor_id character varying(255) NOT NULL,
    base_group_practice_id character varying(255),
    employee_id character varying(255)
);


ALTER TABLE imed.base_group_practice_doctor OWNER TO postgres;

--
-- Name: base_hp_checkup_sugges; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_hp_checkup_sugges (
    base_hp_checkup_sugges_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_hp_checkup_sugges OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: base_intake; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_intake (
    base_intake_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_intake OWNER TO postgres;

--
-- Name: base_invoice_group; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_invoice_group (
    base_invoice_group_id character varying(255) NOT NULL,
    description_th character varying(255),
    description_en character varying(255)
);


ALTER TABLE imed.base_invoice_group OWNER TO postgres;

--
-- Name: base_item_set_sub_group; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_item_set_sub_group (
    base_item_set_sub_group_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_item_set_sub_group OWNER TO postgres;

--
-- Name: base_item_xray_film; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_item_xray_film (
    base_item_xray_film_id character varying(254) NOT NULL,
    item_id character varying(254),
    base_xray_film_id character varying(254),
    film_quantity character varying(254)
);


ALTER TABLE imed.base_item_xray_film OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: base_lab_ignore_report_reason; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_lab_ignore_report_reason (
    base_lab_ignore_report_reason_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_lab_ignore_report_reason OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: base_lab_micro_agent_set; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_lab_micro_agent_set (
    base_lab_micro_agent_set_id character varying(255) NOT NULL,
    base_lab_micro_agent_set_name character varying(255),
    base_antibiotic_description character varying(255)
);


ALTER TABLE imed.base_lab_micro_agent_set OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: base_lab_seq_ln_type; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_lab_seq_ln_type (
    base_lab_seq_ln_type_id character varying(255) NOT NULL,
    base_lab_type_id character varying(255),
    seq_ln_id character varying(255)
);


ALTER TABLE imed.base_lab_seq_ln_type OWNER TO postgres;

--
-- Name: base_lab_service_point; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_lab_service_point (
    base_lab_service_point_id character varying(255) NOT NULL,
    base_lab_type_id character varying(255),
    base_service_point_id character varying(255)
);


ALTER TABLE imed.base_lab_service_point OWNER TO postgres;

--
-- Name: base_lab_thalas_item; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_lab_thalas_item (
    base_lab_thalas_item_id character varying(255) NOT NULL,
    item_id character varying(255)
);


ALTER TABLE imed.base_lab_thalas_item OWNER TO postgres;

--
-- Name: base_lab_thalas_result; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_lab_thalas_result (
    base_lab_thalas_result_id character varying(255) NOT NULL,
    description character varying(255),
    fix_lab_thalas_type_id character varying(255),
    is_risk character varying(255) DEFAULT '0'::character varying
);


ALTER TABLE imed.base_lab_thalas_result OWNER TO postgres;

--
-- Name: base_lab_tube; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_lab_tube (
    base_lab_tube_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_lab_tube OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: base_lab_type; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_lab_type (
    base_lab_type_id character varying(255) NOT NULL,
    description character varying(255),
    print_order character varying(255)
);


ALTER TABLE imed.base_lab_type OWNER TO postgres;

--
-- Name: base_ldap; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_ldap (
    base_ldap_id character varying(255) NOT NULL,
    dn character varying(255) DEFAULT ''::character varying,
    ldap_server character varying(255) DEFAULT ''::character varying,
    ldap_port character varying(255) DEFAULT ''::character varying,
    active character varying(255) DEFAULT ''::character varying
);


ALTER TABLE imed.base_ldap OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: base_lis_automate; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_lis_automate (
    base_lis_automate_id character varying(255) NOT NULL,
    automate_name character varying(255),
    automate_alias character varying(255)
);


ALTER TABLE imed.base_lis_automate OWNER TO postgres;

--
-- Name: base_lis_test_sort; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_lis_test_sort (
    base_lis_test_sort_id character varying(255) NOT NULL,
    template_lab_test_id character varying(255),
    base_automate_id character varying(255),
    "position" character varying(255)
);


ALTER TABLE imed.base_lis_test_sort OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: base_long_waiting_reason; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_long_waiting_reason (
    base_long_waiting_reason_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_long_waiting_reason OWNER TO postgres;

--
-- Name: base_lr_child_health; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_lr_child_health (
    base_lr_child_health_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_lr_child_health OWNER TO postgres;

--
-- Name: base_lr_delivery_abnormal; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_lr_delivery_abnormal (
    base_lr_delivery_abnormal_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_lr_delivery_abnormal OWNER TO postgres;

--
-- Name: base_lr_delivery_medication; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_lr_delivery_medication (
    base_lr_delivery_medication_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_lr_delivery_medication OWNER TO postgres;

--
-- Name: base_lr_delivery_method; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_lr_delivery_method (
    base_lr_delivery_method_id character varying(255) NOT NULL,
    description character varying(255),
    nhso_btype_id character varying(255)
);


ALTER TABLE imed.base_lr_delivery_method OWNER TO postgres;

--
-- Name: base_lr_episiotomy_degree_of_tear; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_lr_episiotomy_degree_of_tear (
    base_lr_episiotomy_degree_of_tear_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_lr_episiotomy_degree_of_tear OWNER TO postgres;

--
-- Name: base_lr_episiotomy_type; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_lr_episiotomy_type (
    base_lr_episiotomy_type_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_lr_episiotomy_type OWNER TO postgres;

--
-- Name: base_lr_exam_organ; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_lr_exam_organ (
    base_lr_exam_organ_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_lr_exam_organ OWNER TO postgres;

--
-- Name: base_lr_fetal_present; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_lr_fetal_present (
    base_lr_fetal_present_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_lr_fetal_present OWNER TO postgres;

--
-- Name: base_lr_fluid_appearance; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_lr_fluid_appearance (
    base_lr_fluid_appearance_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_lr_fluid_appearance OWNER TO postgres;

--
-- Name: base_lr_fundal_ht; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_lr_fundal_ht (
    base_lr_fundal_ht_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_lr_fundal_ht OWNER TO postgres;

--
-- Name: base_lr_ga_deliver; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_lr_ga_deliver (
    base_lr_ga_deliver_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_lr_ga_deliver OWNER TO postgres;

--
-- Name: base_lr_laceration_specify; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_lr_laceration_specify (
    base_lr_laceration_specify_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_lr_laceration_specify OWNER TO postgres;

--
-- Name: base_lr_newborn_medication; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_lr_newborn_medication (
    base_lr_newborn_medication_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_lr_newborn_medication OWNER TO postgres;

--
-- Name: base_lr_operation_method; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_lr_operation_method (
    base_lr_operation_method_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_lr_operation_method OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: base_lr_placenta_delivery_method; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_lr_placenta_delivery_method (
    base_lr_placenta_delivery_method_id character varying(255) NOT NULL,
    description character varying(255) NOT NULL
);


ALTER TABLE imed.base_lr_placenta_delivery_method OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: base_lr_postpartum_abnormal; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_lr_postpartum_abnormal (
    base_lr_postpartum_abnormal_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_lr_postpartum_abnormal OWNER TO postgres;

--
-- Name: base_lr_pregnancy_abnormal; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_lr_pregnancy_abnormal (
    base_lr_pregnancy_abnormal_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_lr_pregnancy_abnormal OWNER TO postgres;

--
-- Name: base_lr_pregnancy_risk; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_lr_pregnancy_risk (
    base_lr_pregnancy_risk_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_lr_pregnancy_risk OWNER TO postgres;

--
-- Name: base_lr_resuscitation; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_lr_resuscitation (
    base_lr_resuscitation_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_lr_resuscitation OWNER TO postgres;

--
-- Name: base_lr_risk; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_lr_risk (
    base_lr_risk_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_lr_risk OWNER TO postgres;

--
-- Name: base_lr_spont_position; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_lr_spont_position (
    base_lr_spont_position_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_lr_spont_position OWNER TO postgres;

--
-- Name: base_lr_umbilical_cord_insertion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_lr_umbilical_cord_insertion (
    base_lr_umbilical_cord_insertion_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_lr_umbilical_cord_insertion OWNER TO postgres;

--
-- Name: base_lr_us_gynae_exam; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_lr_us_gynae_exam (
    base_lr_us_gynae_exam_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_lr_us_gynae_exam OWNER TO postgres;

--
-- Name: base_lr_us_obs_exam; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_lr_us_obs_exam (
    base_lr_us_obs_exam_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_lr_us_obs_exam OWNER TO postgres;

--
-- Name: base_med_department; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_med_department (
    base_med_department_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_med_department OWNER TO postgres;

--
-- Name: base_med_device; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_med_device (
    base_med_device_id character varying(255) NOT NULL,
    description character varying(255),
    base_med_device_sub_type_id character varying(255),
    fix_med_device_status character varying(255)
);


ALTER TABLE imed.base_med_device OWNER TO postgres;

--
-- Name: base_med_device_sub_type; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_med_device_sub_type (
    base_med_device_sub_type_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_med_device_sub_type OWNER TO postgres;

--
-- Name: base_micro_result; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_micro_result (
    base_micro_result_id character varying(254) NOT NULL,
    description character varying(254)
);


ALTER TABLE imed.base_micro_result OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: base_narcotic; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_narcotic (
    base_narcotic_id character varying(255) NOT NULL,
    description character varying(255) NOT NULL
);


ALTER TABLE imed.base_narcotic OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: base_nt_nutrition_facts; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_nt_nutrition_facts (
    base_nt_nutrition_facts_id character varying(255) NOT NULL,
    description character varying(255),
    unit character varying(255)
);


ALTER TABLE imed.base_nt_nutrition_facts OWNER TO postgres;

--
-- Name: base_nt_special_group; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_nt_special_group (
    base_nt_special_group_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_nt_special_group OWNER TO postgres;

--
-- Name: base_nt_supplement_type; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_nt_supplement_type (
    base_nt_supplement_type_id character varying(255) NOT NULL,
    description character varying(255),
    supplement_unit character varying(255),
    item_id character varying(255)
);


ALTER TABLE imed.base_nt_supplement_type OWNER TO postgres;

--
-- Name: base_nt_therapeutic_type; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_nt_therapeutic_type (
    base_nt_therapeutic_type_id character varying(255) NOT NULL,
    description character varying(255),
    priority character varying(255)
);


ALTER TABLE imed.base_nt_therapeutic_type OWNER TO postgres;

--
-- Name: base_nt_type; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_nt_type (
    base_nt_type_id character varying(255) NOT NULL,
    description character varying(255),
    fix_nt_diet_type_id character varying(255)
);


ALTER TABLE imed.base_nt_type OWNER TO postgres;

--
-- Name: base_office; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_office (
    base_office_id character varying(255) NOT NULL,
    off_name character varying(255) NOT NULL,
    full_name character varying(255) NOT NULL,
    changwat character varying(255) NOT NULL,
    amphur character varying(255) NOT NULL
);


ALTER TABLE imed.base_office OWNER TO postgres;

--
-- Name: base_official_holiday; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_official_holiday (
    base_official_holiday_id character varying(255) NOT NULL,
    holiday_day character varying(255),
    holiday_month character varying(255),
    description character varying(255)
);


ALTER TABLE imed.base_official_holiday OWNER TO postgres;

--
-- Name: base_op_anes_type; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_op_anes_type (
    base_op_anes_type_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_op_anes_type OWNER TO postgres;

--
-- Name: base_op_clinic; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_op_clinic (
    base_op_clinic_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_op_clinic OWNER TO postgres;

--
-- Name: base_op_delay_reason; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_op_delay_reason (
    base_op_delay_reason_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_op_delay_reason OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: base_op_operation_posture; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_op_operation_posture (
    base_op_operation_posture_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_op_operation_posture OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: base_op_role; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_op_role (
    base_op_role_id character varying(255) NOT NULL,
    description character varying(255) DEFAULT ''::character varying
);


ALTER TABLE imed.base_op_role OWNER TO postgres;

--
-- Name: base_op_room; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_op_room (
    base_op_room_id character varying(255) NOT NULL,
    description character varying(255) DEFAULT ''::character varying
);


ALTER TABLE imed.base_op_room OWNER TO postgres;

--
-- Name: base_op_set_extra; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_op_set_extra (
    base_op_set_extra_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_op_set_extra OWNER TO postgres;

--
-- Name: base_op_type; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_op_type (
    base_op_type_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_op_type OWNER TO postgres;

--
-- Name: base_op_wound; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_op_wound (
    base_op_wound_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_op_wound OWNER TO postgres;

--
-- Name: base_operation_employee; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_operation_employee (
    base_operation_employee_id character varying(255) NOT NULL,
    base_operation_set_id character varying(255),
    base_clinic_id character varying(255)
);


ALTER TABLE imed.base_operation_employee OWNER TO postgres;

--
-- Name: base_operation_set; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_operation_set (
    base_operation_set_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_operation_set OWNER TO postgres;

--
-- Name: base_order_drug_reason; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_order_drug_reason (
    base_order_drug_reason_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_order_drug_reason OWNER TO postgres;

--
-- Name: base_organic; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_organic (
    base_organic_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_organic OWNER TO postgres;

--
-- Name: base_organisms; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_organisms (
    base_organisms character varying(255) NOT NULL
);


ALTER TABLE imed.base_organisms OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: base_other_allergy; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_other_allergy (
    base_other_allergy_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_other_allergy OWNER TO postgres;

--
-- Name: base_other_allergy_level; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_other_allergy_level (
    base_other_allergy_level_id character varying(255) NOT NULL,
    base_other_allergy_id character varying(255),
    description character varying(255)
);


ALTER TABLE imed.base_other_allergy_level OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: base_out_doctor; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_out_doctor (
    base_out_doctor_id character varying(255) NOT NULL,
    prename character varying(255),
    firstname character varying(255),
    lastname character varying(255),
    note text
);


ALTER TABLE imed.base_out_doctor OWNER TO postgres;

--
-- Name: base_output; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_output (
    base_output_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_output OWNER TO postgres;

--
-- Name: base_paid_method; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_paid_method (
    base_paid_method_id character varying(255) NOT NULL,
    description_th character varying(255),
    description_en character varying(255),
    fix_paid_method_id character varying(255),
    active character varying(255)
);


ALTER TABLE imed.base_paid_method OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: base_patho_contraceptive; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_patho_contraceptive (
    base_patho_contraceptive_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_patho_contraceptive OWNER TO postgres;

--
-- Name: base_patho_hormonal_status; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_patho_hormonal_status (
    base_patho_hormonal_status_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_patho_hormonal_status OWNER TO postgres;

--
-- Name: base_patho_snomed_morphology; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_patho_snomed_morphology (
    base_patho_snomed_morphology_id character varying(255) NOT NULL,
    code character varying(255),
    description character varying(255)
);


ALTER TABLE imed.base_patho_snomed_morphology OWNER TO postgres;

--
-- Name: base_patho_snomed_topography; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_patho_snomed_topography (
    base_patho_snomed_topography_id character varying(255) NOT NULL,
    code character varying(255),
    description character varying(255)
);


ALTER TABLE imed.base_patho_snomed_topography OWNER TO postgres;

--
-- Name: base_patho_stain_routine; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_patho_stain_routine (
    base_patho_stain_routine_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_patho_stain_routine OWNER TO postgres;

--
-- Name: base_patient_classification; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_patient_classification (
    base_patient_classification_id character varying(255) NOT NULL,
    description character varying(255),
    adult_min_score character varying(255),
    adult_max_score character varying(255),
    child_min_score character varying(255),
    child_max_score character varying(255)
);


ALTER TABLE imed.base_patient_classification OWNER TO postgres;

--
-- Name: base_patient_classify_determiners; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_patient_classify_determiners (
    base_patient_classify_determiners_id character varying(255) NOT NULL,
    base_patient_classify_standard_id character varying(255),
    description text,
    score character varying(255),
    fix_patient_age character varying(255)
);


ALTER TABLE imed.base_patient_classify_determiners OWNER TO postgres;

--
-- Name: base_patient_classify_standard; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_patient_classify_standard (
    base_patient_classify_standard_id character varying(255) NOT NULL,
    description text,
    fix_patient_age character varying(255)
);


ALTER TABLE imed.base_patient_classify_standard OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: base_patient_file_location; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_patient_file_location (
    base_patient_file_location_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_patient_file_location OWNER TO postgres;

--
-- Name: base_patient_group; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_patient_group (
    base_patient_group_id character varying(255) NOT NULL,
    description character varying(255),
    active character varying(255)
);


ALTER TABLE imed.base_patient_group OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: base_patient_image_folder; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_patient_image_folder (
    base_patient_image_folder_id character varying(255) NOT NULL,
    folder_name character varying(255),
    file_name_prefix character varying(255)
);


ALTER TABLE imed.base_patient_image_folder OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: base_patient_media_perspective; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_patient_media_perspective (
    base_patient_media_perspective_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_patient_media_perspective OWNER TO postgres;

--
-- Name: base_patient_type; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_patient_type (
    base_patient_type_id character varying(255) NOT NULL,
    description character varying(255),
    active character varying(255)
);


ALTER TABLE imed.base_patient_type OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: base_patient_type_special; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_patient_type_special (
    base_patient_type_special_id character varying(255) NOT NULL,
    base_patient_type_id character varying(255),
    fix_patient_type_special_id character varying(255)
);


ALTER TABLE imed.base_patient_type_special OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: base_patient_unit; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_patient_unit (
    base_patient_unit_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_patient_unit OWNER TO postgres;

--
-- Name: base_plan_group; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_plan_group (
    base_plan_group_id character varying(255) NOT NULL,
    base_plan_group_code character varying(255),
    description character varying(255),
    print_form_cr character varying(255),
    bg_color character varying(255)
);


ALTER TABLE imed.base_plan_group OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: base_police_station; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_police_station (
    base_police_station_id character varying(255) NOT NULL,
    description character varying(255),
    fix_amphur_id character varying(255),
    fix_changwat_id character varying(255),
    fix_amphur_name character varying(255),
    fix_changwat_name character varying(255)
);


ALTER TABLE imed.base_police_station OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: base_prename; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_prename (
    base_prename_id character varying(255) NOT NULL,
    description character varying(255),
    fix_gender_id character varying(255)
);


ALTER TABLE imed.base_prename OWNER TO postgres;

--
-- Name: base_receipt_category; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_receipt_category (
    base_receipt_category_id character varying(255) NOT NULL,
    description character varying(255),
    receipt_title text,
    fix_receipt_type_id character varying(255)
);


ALTER TABLE imed.base_receipt_category OWNER TO postgres;

--
-- Name: base_refer_out_reason; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_refer_out_reason (
    base_refer_out_reason_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_refer_out_reason OWNER TO postgres;

--
-- Name: base_religion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_religion (
    base_religion_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_religion OWNER TO postgres;

--
-- Name: base_room; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_room (
    base_room_id character varying(255) NOT NULL,
    base_service_point_id character varying(255),
    room_number character varying(255),
    bed_number character varying(255),
    fix_bed_status_id character varying(255),
    fix_service_status_id character varying(255),
    base_room_type_id character varying(255)
);


ALTER TABLE imed.base_room OWNER TO postgres;

--
-- Name: base_room_item; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_room_item (
    base_room_item_id character varying(254) NOT NULL,
    base_room_id character varying(254),
    item_id character varying(254),
    is_room_order character varying(255),
    is_room_price character varying(255),
    is_food_price character varying(255)
);


ALTER TABLE imed.base_room_item OWNER TO postgres;

--
-- Name: base_room_med_department; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_room_med_department (
    base_room_med_department_id character varying(255) NOT NULL,
    base_med_department_id character varying(255),
    base_room_id character varying(255)
);


ALTER TABLE imed.base_room_med_department OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: base_room_observe_item; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_room_observe_item (
    base_room_observe_item_id character varying(255) NOT NULL,
    base_room_id character varying(255),
    item_id character varying(255)
);


ALTER TABLE imed.base_room_observe_item OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: base_room_type; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_room_type (
    base_room_type_id character varying(254) NOT NULL,
    description character varying(254)
);


ALTER TABLE imed.base_room_type OWNER TO postgres;

--
-- Name: base_service_point; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_service_point (
    base_service_point_id character varying(255) NOT NULL,
    description character varying(255),
    fix_service_point_type_id character varying(255),
    fix_service_point_subtype_id character varying(255),
    fix_service_point_group_id character varying(255),
    ward_code character varying(255),
    base_department_id character varying(255),
    stock_id character varying(255),
    max_waiting_time character varying(255),
    setup_queue_column character varying(255),
    sort_queue_type character varying(255),
    active character varying(255),
    default_search_item_in_stock character varying(255),
    telephone character varying(255),
    ward_floor character varying(255),
    is_allow_file_stamp character varying(255),
    base_site_branch_id character varying(255),
    stock_consignment_id character varying(255)
);


ALTER TABLE imed.base_service_point OWNER TO postgres;

--
-- Name: base_service_point_ip_address; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_service_point_ip_address (
    base_service_point_ip_address_id character varying(255) NOT NULL,
    base_service_point_id character varying(255),
    ip_address character varying(255)
);


ALTER TABLE imed.base_service_point_ip_address OWNER TO postgres;

--
-- Name: base_service_point_patient_no; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_service_point_patient_no (
    base_service_point_patient_no_id character varying(255) NOT NULL,
    base_service_point_id character varying(255),
    base_patient_number_type character varying(255)
);


ALTER TABLE imed.base_service_point_patient_no OWNER TO postgres;

--
-- Name: base_service_point_role; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_service_point_role (
    base_service_point_role_id character varying(255) NOT NULL,
    base_service_point_id character varying(255),
    tabmenu_auth character varying(255),
    mainmenu_auth character varying(255)
);


ALTER TABLE imed.base_service_point_role OWNER TO postgres;

--
-- Name: base_site; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_site (
    base_site_id character varying(255) NOT NULL,
    site_name character varying(255),
    address character varying(255),
    village character varying(255),
    tambol character varying(255),
    amphur character varying(255),
    changwat character varying(255),
    postcode character varying(255),
    tel character varying(255),
    protect_table character varying(255),
    login_message_active character varying(255),
    login_message text,
    identified_picture_default_path character varying(255),
    identified_picture_default_url character varying(255) DEFAULT ''::character varying,
    fix_hospital_type character varying(255),
    doc_scan_default_path character varying(255),
    doc_scan_store_url character varying(255) DEFAULT 'http://127.0.0.1:8080/imed-image-folder/'::character varying,
    doc_scan_service_url character varying(255) DEFAULT 'http://127.0.0.1:8080/imed-image/'::character varying,
    ejb_service_server character varying(255) DEFAULT '127.0.0.1'::character varying,
    jnp_port character varying(255) DEFAULT '1099'::character varying,
    doc_scan_store_path character varying(255) DEFAULT ''::character varying,
    fix_branch_type character varying(255),
    site_latlng character varying(255)
);


ALTER TABLE imed.base_site OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: base_site_app_server; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_site_app_server (
    base_site_app_server_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_site_app_server OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: base_site_branch; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_site_branch (
    base_site_branch_id character varying(255) NOT NULL,
    description character varying(255),
    base_tariff_id character varying(255)
);


ALTER TABLE imed.base_site_branch OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: base_skin_color; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_skin_color (
    base_skin_color_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_skin_color OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: base_special_nurse_care; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_special_nurse_care (
    base_special_nurse_care_id character varying(255) NOT NULL,
    description character varying(255),
    fix_special_nurse_care_type character varying(255)
);


ALTER TABLE imed.base_special_nurse_care OWNER TO postgres;

--
-- Name: base_specimen; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_specimen (
    base_specimen_id character varying(255) NOT NULL,
    description character varying(255) DEFAULT ''::character varying,
    base_lab_type_id character varying(255)
);


ALTER TABLE imed.base_specimen OWNER TO postgres;

--
-- Name: base_sql_group; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_sql_group (
    base_sql_group_id character varying(255) NOT NULL,
    base_sql_group_code character varying(255),
    base_sql_group_name character varying(255)
);


ALTER TABLE imed.base_sql_group OWNER TO postgres;

--
-- Name: base_state; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_state (
    base_state_id character varying(255) NOT NULL,
    description character varying(255),
    base_country_id character varying(255)
);


ALTER TABLE imed.base_state OWNER TO postgres;

--
-- Name: base_stock_exchange_note; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_stock_exchange_note (
    base_stock_exchange_note_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_stock_exchange_note OWNER TO postgres;

--
-- Name: base_tariff; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_tariff (
    base_tariff_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_tariff OWNER TO postgres;

--
-- Name: base_template_advice; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_template_advice (
    base_template_advice_id character varying(255) NOT NULL,
    description text
);


ALTER TABLE imed.base_template_advice OWNER TO postgres;

--
-- Name: base_template_app_detail; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_template_app_detail (
    base_template_app_detail_id character varying(255) NOT NULL,
    description text
);


ALTER TABLE imed.base_template_app_detail OWNER TO postgres;

--
-- Name: base_template_diagnosis; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_template_diagnosis (
    base_template_diagnosis_id character varying(255) NOT NULL,
    base_diagnosis_set_id character varying(255),
    description character varying(255),
    description_th character varying(255),
    icd10_code character varying(255),
    imed_stat_use character varying(255)
);


ALTER TABLE imed.base_template_diagnosis OWNER TO postgres;

--
-- Name: base_template_doctor_note; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_template_doctor_note (
    base_template_doctor_note_id character varying(255) NOT NULL,
    description text,
    employee_id character varying(255),
    base_clinic_id character varying(255)
);


ALTER TABLE imed.base_template_doctor_note OWNER TO postgres;

--
-- Name: base_template_illness; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_template_illness (
    base_template_illness_id character varying(255) NOT NULL,
    description text,
    base_clinic_id character varying(255)
);


ALTER TABLE imed.base_template_illness OWNER TO postgres;

--
-- Name: base_template_item_set_multi_visit; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_template_item_set_multi_visit (
    base_template_item_set_multi_visit_id character varying(255) NOT NULL,
    item_set_id character varying(255),
    times character varying(255),
    price character varying(255)
);


ALTER TABLE imed.base_template_item_set_multi_visit OWNER TO postgres;

--
-- Name: base_template_lab_result; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_template_lab_result (
    base_template_lab_result_id character varying(255) NOT NULL,
    description text
);


ALTER TABLE imed.base_template_lab_result OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: base_template_mc_approve; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_template_mc_approve (
    base_template_mc_approve_id character varying(255) NOT NULL,
    description text
);


ALTER TABLE imed.base_template_mc_approve OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: base_template_operation; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_template_operation (
    base_template_operation_id character varying(255) NOT NULL,
    base_operation_set_id character varying(255),
    description character varying(255),
    description_th character varying(255),
    icd9_code text,
    item_id character varying(255)
);


ALTER TABLE imed.base_template_operation OWNER TO postgres;

--
-- Name: base_template_phyex; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_template_phyex (
    base_template_phyex_id character varying(255) NOT NULL,
    description text,
    base_clinic_id character varying(255),
    base_organic_id character varying(255),
    employee_id character varying(255)
);


ALTER TABLE imed.base_template_phyex OWNER TO postgres;

--
-- Name: base_template_symptom; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_template_symptom (
    base_template_symptom_id character varying(255) NOT NULL,
    description text,
    base_clinic_id character varying(255)
);


ALTER TABLE imed.base_template_symptom OWNER TO postgres;

--
-- Name: base_template_xray_result; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_template_xray_result (
    base_template_xray_result_id character varying(255) NOT NULL,
    description text
);


ALTER TABLE imed.base_template_xray_result OWNER TO postgres;

--
-- Name: base_time_stamp; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_time_stamp (
    base_time_stamp_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_time_stamp OWNER TO postgres;

--
-- Name: base_unit; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_unit (
    base_unit_id character varying(255) NOT NULL,
    description_th character varying(255),
    description_en character varying(255)
);


ALTER TABLE imed.base_unit OWNER TO postgres;

--
-- Name: base_vs_coma_scale; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_vs_coma_scale (
    base_vs_coma_scale_id character varying(255) NOT NULL,
    description character varying(255),
    min_score character varying(255),
    color character varying(255)
);


ALTER TABLE imed.base_vs_coma_scale OWNER TO postgres;

--
-- Name: base_vs_coma_scale_eye; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_vs_coma_scale_eye (
    base_vs_coma_scale_eye_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_vs_coma_scale_eye OWNER TO postgres;

--
-- Name: base_vs_coma_scale_motion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_vs_coma_scale_motion (
    base_vs_coma_scale_motion_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_vs_coma_scale_motion OWNER TO postgres;

--
-- Name: base_vs_coma_scale_verbal; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_vs_coma_scale_verbal (
    base_vs_coma_scale_verbal_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_vs_coma_scale_verbal OWNER TO postgres;

--
-- Name: base_vs_ext_advice; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_vs_ext_advice (
    base_vs_ext_advice_id character varying(255) NOT NULL,
    fix_vs_ext_advice_type character varying(255),
    base_vs_ext_description character varying(255),
    fix_clinic_id character varying(255)
);


ALTER TABLE imed.base_vs_ext_advice OWNER TO postgres;

--
-- Name: base_vs_motor_power; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_vs_motor_power (
    base_vs_motor_power_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_vs_motor_power OWNER TO postgres;

--
-- Name: base_vs_pupil_score; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_vs_pupil_score (
    base_vs_pupil_score_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.base_vs_pupil_score OWNER TO postgres;

--
-- Name: base_xray_film; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_xray_film (
    base_xray_film_id character varying(254) NOT NULL,
    description character varying(254),
    unit_price_cost character varying(254),
    unit_price_sale character varying(254)
);


ALTER TABLE imed.base_xray_film OWNER TO postgres;

--
-- Name: base_xray_type; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.base_xray_type (
    base_xray_type_id character varying(255) NOT NULL,
    description character varying(255),
    fix_xray_type_id character varying(255),
    fix_pacs_modality_code character varying(255)
);


ALTER TABLE imed.base_xray_type OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: bb_base_product_pack_cell; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.bb_base_product_pack_cell (
    bb_base_product_pack_cell_id character varying(255) NOT NULL,
    description character varying(255),
    volume character varying(255),
    product_age_day character varying(255),
    item_id character varying(255)
);


ALTER TABLE imed.bb_base_product_pack_cell OWNER TO postgres;

--
-- Name: bb_cross_matching; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.bb_cross_matching (
    bb_cross_matching_id character varying(255) NOT NULL,
    lab_result_id character varying(255),
    order_item_id character varying(255),
    bb_pack_cell_id character varying(255),
    unit_id character varying(255),
    result_rt character varying(255),
    result_37 character varying(255),
    result_ahg character varying(255),
    xm_result character varying(255),
    is_xm_result_abnormal character varying(255),
    base_bb_blood_transfusion_reason_id character varying(255),
    report_eid character varying(255),
    report_date character varying(255),
    report_time character varying(255),
    result_issue_status character varying(255)
);


ALTER TABLE imed.bb_cross_matching OWNER TO postgres;

--
-- Name: bb_donate_data; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.bb_donate_data (
    bb_donate_data_id character varying(255) NOT NULL,
    donate_number character varying(255),
    unit_id character varying(255),
    collect_date character varying(255),
    collect_time character varying(255),
    collect_eid character varying(255),
    bb_donor_social_data_id character varying(255),
    is_send_to_lab character varying(255),
    visit_id character varying(255)
);


ALTER TABLE imed.bb_donate_data OWNER TO postgres;

--
-- Name: bb_donor_social_data; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.bb_donor_social_data (
    bb_donor_social_data_id character varying(255) NOT NULL,
    patient_id character varying(255),
    donor_id character varying(255),
    is_infection character varying(255)
);


ALTER TABLE imed.bb_donor_social_data OWNER TO postgres;

--
-- Name: bb_pack_cell; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.bb_pack_cell (
    bb_pack_cell_id character varying(255) NOT NULL,
    unit_id character varying(255),
    expire_date character varying(255),
    bb_base_product_pack_cell_id character varying(255),
    blood_group character varying(255),
    collect_eid character varying(255),
    collect_date character varying(255),
    collect_time character varying(255),
    volume character varying(255),
    note text,
    fix_pack_cell_status_id character varying(255),
    bb_donor_social_data_id character varying(255),
    use_patient_id character varying(255),
    use_spid character varying(255),
    holding_eid character varying(255),
    holding_date character varying(255),
    holding_time character varying(255),
    dispense_eid character varying(255),
    dispense_date character varying(255),
    dispense_time character varying(255),
    base_bb_packcell_wastage_reason_id character varying(255)
);


ALTER TABLE imed.bb_pack_cell OWNER TO postgres;

--
-- Name: bb_stock_card; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.bb_stock_card (
    bb_stock_card_id character varying(255) NOT NULL,
    bb_stock_id character varying(255),
    bb_pack_cell_id character varying(255),
    card_date character varying(255),
    card_time character varying(255),
    card_eid character varying(255),
    fix_bb_stock_card_type_id character varying(255)
);


ALTER TABLE imed.bb_stock_card OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: bed_booking; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.bed_booking (
    bed_booking_id character varying(254) NOT NULL,
    booking_name character varying(254),
    patient_id character varying(254),
    note character varying(50),
    move_date character varying(254),
    move_time character varying(254),
    base_room_id character varying(254),
    base_service_point_id character varying(254),
    room_number character varying(254),
    bed_number character varying(254),
    base_room_type_id character varying(254),
    booking_date character varying(254),
    booking_time character varying(254),
    modify_eid character varying(254),
    modify_date character varying(254),
    modify_time character varying(254),
    doctor_eid character varying(255),
    base_med_department_id character varying(255)
);


ALTER TABLE imed.bed_booking OWNER TO postgres;

--
-- Name: bed_management; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.bed_management (
    bed_management_id character varying(254) NOT NULL,
    admit_id character varying(254),
    base_room_id character varying(254),
    base_service_point_id character varying(254),
    room_number character varying(254),
    bed_number character varying(254),
    base_room_type_id character varying(254),
    move_date character varying(254),
    move_time character varying(254),
    move_out_date character varying(254),
    move_out_time character varying(254),
    current_bed character varying(254),
    ward_code character varying(254),
    base_department_id character varying(254),
    note text,
    modify_eid character varying(254),
    modify_date character varying(254),
    modify_time character varying(254),
    order_continue_id character varying(50)
);


ALTER TABLE imed.bed_management OWNER TO postgres;

--
-- Name: bed_mgnt_order_continue; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.bed_mgnt_order_continue (
    bed_mgnt_order_continue_id character varying(255) NOT NULL,
    bed_management_id character varying(255),
    order_continue_id character varying(255)
);


ALTER TABLE imed.bed_mgnt_order_continue OWNER TO postgres;

--
-- Name: connection_profile; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.connection_profile (
    connection_profile_id character varying(255) NOT NULL,
    connection_profile_name character varying(255),
    driver_jdbc character varying(255),
    server_ip character varying(255),
    port character varying(255),
    database_name character varying(255),
    username character varying(255),
    password character varying(255)
);


ALTER TABLE imed.connection_profile OWNER TO postgres;

--
-- Name: consult; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.consult (
    consult_id character varying(255) NOT NULL,
    visit_id character varying(255),
    consult_to_eid character varying(255),
    consult_to_spid character varying(255),
    fix_urgent_status_id character varying(255),
    diagnosis text,
    consult_problem text,
    request_eid character varying(255),
    request_spid character varying(255),
    request_date character varying(255),
    request_time character varying(255),
    consult_result text,
    receive_eid character varying(255),
    receive_date character varying(255),
    receive_time character varying(255),
    fix_consult_status_id character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    is_receive_over_time character varying(255)
);


ALTER TABLE imed.consult OWNER TO postgres;

--
-- Name: consult_case; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.consult_case (
    consult_case_id character varying(255) NOT NULL,
    consult_id character varying(255),
    base_consult_case_id character varying(255)
);


ALTER TABLE imed.consult_case OWNER TO postgres;

--
-- Name: consult_case_result; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.consult_case_result (
    consult_case_result_id character varying(255) NOT NULL,
    consult_case_id character varying(255),
    start_date character varying(255),
    start_time character varying(255),
    finish_date character varying(255),
    finish_time character varying(255),
    base_consult_result_id character varying(255),
    note text,
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255)
);


ALTER TABLE imed.consult_case_result OWNER TO postgres;

--
-- Name: consult_treatment; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.consult_treatment (
    consult_treatment_id character varying(255) NOT NULL,
    consult_case_result_id character varying(255),
    base_consult_treatment_id character varying(255)
);


ALTER TABLE imed.consult_treatment OWNER TO postgres;

--
-- Name: current_drug_use; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.current_drug_use (
    current_drug_use_id character varying(255) NOT NULL,
    patient_id character varying(255),
    drug_name character varying(255),
    note character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255)
);


ALTER TABLE imed.current_drug_use OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: custom_report; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.custom_report (
    id integer NOT NULL,
    xml text
);


ALTER TABLE imed.custom_report OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: daily_record_ipd; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.daily_record_ipd (
    daily_record_ipd_id character varying(255) NOT NULL,
    admit_id character varying(255),
    weight character varying(255),
    height character varying(255),
    diet character varying(255),
    intake_oral_fluide character varying(255),
    intake_parenteral character varying(255),
    intake_other character varying(255),
    output_urine character varying(255),
    output_emesis character varying(255),
    output_drainage character varying(255),
    output_aspiration character varying(255),
    output_other character varying(255),
    stools_times character varying(255),
    urine_times character varying(255),
    medications character varying(255),
    note character varying(255),
    bmi character varying(255),
    measure_eid character varying(255),
    measure_date character varying(255),
    measure_time character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255)
);


ALTER TABLE imed.daily_record_ipd OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: death; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.death (
    death_id character varying(255) NOT NULL,
    patient_id character varying(255),
    visit_id character varying(255),
    base_death_status_id character varying(255),
    death_cause character varying(255),
    death_date character varying(255),
    death_time character varying(255),
    comment character varying(255)
);


ALTER TABLE imed.death OWNER TO postgres;

--
-- Name: dental; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.dental (
    dental_id character varying(255) NOT NULL,
    patient_id character varying(255) DEFAULT ''::character varying,
    visit_id character varying(255) DEFAULT ''::character varying,
    base_dent_organic_id character varying(255) DEFAULT ''::character varying,
    base_dent_diagnosis_id character varying(255) DEFAULT ''::character varying,
    diagnosis_date character varying(255) DEFAULT ''::character varying,
    diagnosis_time character varying(255) DEFAULT ''::character varying,
    note text DEFAULT ''::text,
    doctor_eid character varying(255) DEFAULT ''::character varying,
    dental_date character varying(255) DEFAULT ''::character varying,
    dental_time character varying(255) DEFAULT ''::character varying,
    plan_status character varying(255) DEFAULT ''::character varying,
    plan_priority character varying(255) DEFAULT ''::character varying,
    make_plan_eid character varying(255) DEFAULT ''::character varying,
    continue_type character varying(255) DEFAULT ''::character varying,
    continue_ref_no character varying(255) DEFAULT ''::character varying,
    modify_eid character varying(255) DEFAULT ''::character varying,
    modify_date character varying(255) DEFAULT ''::character varying,
    modify_time character varying(255) DEFAULT ''::character varying
);


ALTER TABLE imed.dental OWNER TO postgres;

--
-- Name: dental_order; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.dental_order (
    dental_order_id character varying(255) NOT NULL,
    dental_id character varying(255) DEFAULT ''::character varying,
    order_item_id character varying(255) DEFAULT ''::character varying,
    item_id character varying(255) DEFAULT ''::character varying,
    base_dent_operation_id character varying(255) DEFAULT ''::character varying,
    description character varying(255) DEFAULT ''::character varying,
    doctor_eid character varying(255) DEFAULT ''::character varying,
    operation_date character varying(255) DEFAULT ''::character varying,
    operation_time character varying(255) DEFAULT ''::character varying,
    continue_ref_no character varying(255) DEFAULT ''::character varying,
    beginning_price character varying(255) DEFAULT ''::character varying
);


ALTER TABLE imed.dental_order OWNER TO postgres;

--
-- Name: dental_plan_order; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.dental_plan_order (
    dental_plan_order_id character varying(255) NOT NULL,
    dental_id character varying(255) DEFAULT ''::character varying,
    item_id character varying(255) DEFAULT ''::character varying,
    fix_item_type_id character varying(255) DEFAULT ''::character varying,
    base_dent_operation_id character varying(255) DEFAULT ''::character varying,
    base_drug_usage_code character varying(255) DEFAULT ''::character varying,
    instruction_format character varying(255) DEFAULT ''::character varying,
    base_drug_instruction_id character varying(255) DEFAULT ''::character varying,
    dose_quantity character varying(255) DEFAULT ''::character varying,
    base_dose_unit_id character varying(255) DEFAULT ''::character varying,
    base_drug_frequency_id character varying(255) DEFAULT ''::character varying,
    instruction_text_line1 character varying(255) DEFAULT ''::character varying,
    instruction_text_line2 character varying(255) DEFAULT ''::character varying,
    instruction_text_line3 character varying(255) DEFAULT ''::character varying,
    quantity character varying(255) DEFAULT ''::character varying,
    base_unit_id character varying(255) DEFAULT ''::character varying,
    description text DEFAULT ''::text,
    caution text DEFAULT ''::text,
    beginning_price character varying(255) DEFAULT ''::character varying,
    doctor_eid character varying(255) DEFAULT ''::character varying,
    modify_eid character varying(255) DEFAULT ''::character varying,
    modify_date character varying(255) DEFAULT ''::character varying,
    modify_time character varying(255) DEFAULT ''::character varying
);


ALTER TABLE imed.dental_plan_order OWNER TO postgres;

--
-- Name: dental_teeth; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.dental_teeth (
    dental_teeth_id character varying(255) NOT NULL,
    dental_id character varying(255) DEFAULT ''::character varying,
    tooth_number character varying(255) DEFAULT ''::character varying,
    tooth_part character varying(255) DEFAULT ''::character varying
);


ALTER TABLE imed.dental_teeth OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: df_approve; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.df_approve (
    df_approve_id character varying(255) NOT NULL,
    order_item_id character varying(255),
    employee_id character varying(255),
    fix_employee_type_id character varying(255),
    patient_id character varying(255),
    visit_id character varying(255),
    item_id character varying(255),
    fix_item_type_id character varying(255),
    base_df_mode_id character varying(255),
    df_date character varying(255),
    df_time character varying(255),
    approve_amount character varying(255),
    approve_eid character varying(255),
    approve_date character varying(255),
    approve_time character varying(255)
);


ALTER TABLE imed.df_approve OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: df_order_item; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.df_order_item (
    df_order_item_id character varying(255) NOT NULL,
    item_id character varying(255),
    employee_id character varying(255),
    quantity character varying(255),
    base_drug_usage_code character varying(255),
    instruction_text_line1 character varying(255),
    instruction_text_line2 character varying(255),
    instruction_text_line3 character varying(255),
    is_imed_learning character varying(255)
);


ALTER TABLE imed.df_order_item OWNER TO postgres;

--
-- Name: df_order_item_set; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.df_order_item_set (
    df_order_item_set_id character varying(255) NOT NULL,
    item_set_id character varying(255),
    item_id character varying(255),
    quantity character varying(255),
    order_position character varying(255),
    base_drug_usage_code character varying(255),
    instruction_text_line1 character varying(255),
    instruction_text_line2 character varying(255),
    instruction_text_line3 character varying(255),
    set_price character varying(255),
    doctor_share character varying(255),
    same_type_id character varying(255),
    base_item_set_sub_group_id character varying(255)
);


ALTER TABLE imed.df_order_item_set OWNER TO postgres;

--
-- Name: diagnosis_icd10; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.diagnosis_icd10 (
    diagnosis_icd10_id character varying(255) NOT NULL,
    visit_id character varying(255),
    icd10_code character varying(255),
    icd10_description character varying(255),
    fix_diagnosis_type_id character varying(255),
    doctor_eid character varying(255),
    base_clinic_id character varying(255),
    diagnosis_date character varying(255),
    diagnosis_time character varying(255),
    comments character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    beginning_diagnosis character varying(255),
    beginning_diagnosis_th character varying(255),
    is_secret character varying(255),
    self_dx_doctor_eid character varying(255)
);


ALTER TABLE imed.diagnosis_icd10 OWNER TO postgres;

--
-- Name: diagnosis_icd9; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.diagnosis_icd9 (
    diagnosis_icd9_id character varying(255) NOT NULL,
    visit_id character varying(255),
    icd9_code character varying(255),
    icd9_description character varying(255),
    fix_operation_type_id character varying(255),
    doctor_eid character varying(255),
    base_clinic_id character varying(255),
    date_in character varying(255),
    time_in character varying(255),
    date_out character varying(255),
    time_out character varying(255),
    comments character varying(255),
    beginning_operation character varying(255),
    beginning_operation_th character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    self_dx_doctor_eid character varying(255)
);


ALTER TABLE imed.diagnosis_icd9 OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: dialysis_operator; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.dialysis_operator (
    dialysis_operator_id character varying(255) NOT NULL,
    dialysis_registered_id character varying(255),
    operator_eid character varying(255)
);


ALTER TABLE imed.dialysis_operator OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: dialysis_registered; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.dialysis_registered (
    dialysis_registered_id character varying(255) NOT NULL,
    visit_id character varying(255),
    fix_dialysis_case_id character varying(255),
    medical_device_used_id character varying(255),
    note text,
    doctor_eid character varying(255)
);


ALTER TABLE imed.dialysis_registered OWNER TO postgres;

--
-- Name: distributor; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.distributor (
    distributor_id character varying(255) NOT NULL,
    distributor_name character varying(255) DEFAULT ''::character varying,
    tax_payer_number character varying(255) DEFAULT ''::character varying,
    fix_stock_pay_method_id character varying(255) DEFAULT ''::character varying,
    credit_on_pay character varying(255) DEFAULT ''::character varying,
    grade character varying(255) DEFAULT ''::character varying,
    address text DEFAULT ''::text,
    office_tel character varying(255) DEFAULT ''::character varying,
    office_fax character varying(255) DEFAULT ''::character varying,
    office_mobile character varying(255) DEFAULT ''::character varying,
    mail character varying(255) DEFAULT ''::character varying,
    contact_person character varying(255) DEFAULT ''::character varying,
    deliver_day character varying(255) DEFAULT ''::character varying,
    member_number character varying(255),
    active character varying(255),
    discount_percent character varying(255),
    comment text,
    is_local character varying(255),
    is_green_book character varying(255)
);


ALTER TABLE imed.distributor OWNER TO postgres;

--
-- Name: doc_scan_template_message; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.doc_scan_template_message (
    doc_scan_template_message_id character varying(255) NOT NULL,
    comments_message character varying(255)
);


ALTER TABLE imed.doc_scan_template_message OWNER TO postgres;

--
-- Name: doctor_absent; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.doctor_absent (
    doctor_absent_id character varying(255) NOT NULL,
    doctor_schedule_id character varying(255),
    employee_id character varying(255),
    spid character varying(255),
    absent_date character varying(255),
    fix_day_of_week character varying(255),
    start_time character varying(255),
    end_time character varying(255)
);


ALTER TABLE imed.doctor_absent OWNER TO postgres;

--
-- Name: doctor_diagnosis; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.doctor_diagnosis (
    doctor_diagnosis_id character varying(255) NOT NULL,
    visit_id character varying(255),
    beginning_diagnosis character varying(255),
    beginning_diagnosis_th character varying(255),
    note text,
    secret_status character varying(255),
    doctor_eid character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255)
);


ALTER TABLE imed.doctor_diagnosis OWNER TO postgres;

--
-- Name: doctor_discharge_ipd; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.doctor_discharge_ipd (
    doctor_discharge_ipd_id character varying(255) NOT NULL,
    visit_id character varying(255),
    discharge_doctor_eid character varying(255),
    discharge_eid character varying(255),
    discharge_date character varying(255),
    discharge_time character varying(255),
    fix_ipd_discharge_status_id character varying(255),
    fix_ipd_discharge_type_id character varying(255),
    note character varying(255)
);


ALTER TABLE imed.doctor_discharge_ipd OWNER TO postgres;

--
-- Name: doctor_discharge_opd; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.doctor_discharge_opd (
    doctor_discharge_opd_id character varying(255) NOT NULL,
    visit_id character varying(255),
    discharge_eid character varying(255),
    discharge_date character varying(255),
    discharge_time character varying(255),
    fix_opd_discharge_status_id character varying(255),
    note character varying(255),
    base_doctor_discharge_type_id character varying(255)
);


ALTER TABLE imed.doctor_discharge_opd OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: doctor_parttime_time_stamp; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.doctor_parttime_time_stamp (
    doctor_parttime_time_stamp_id character varying(255) NOT NULL,
    employee_id character varying(255),
    stamp_date character varying(255),
    time_in character varying(255),
    time_out character varying(255),
    work_hours character varying(255),
    is_approve character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    modify_eid character varying(255),
    approve_date character varying(255),
    approve_time character varying(255),
    approve_eid character varying(255),
    cancel_approve_date character varying(255),
    cancel_approve_time character varying(255),
    cancel_approve_eid character varying(255)
);


ALTER TABLE imed.doctor_parttime_time_stamp OWNER TO postgres;

--
-- Name: doctor_profile; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.doctor_profile (
    doctor_profile_id character varying(255) NOT NULL,
    employee_id character varying(255),
    doctor_fee_percent character varying(255),
    min_guarantee_per_hour character varying(255),
    min_guarantee_per_month character varying(255),
    salary character varying(255),
    position_allowance character varying(255),
    other character varying(255)
);


ALTER TABLE imed.doctor_profile OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: doctor_schedule; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.doctor_schedule (
    doctor_schedule_id character varying(255) NOT NULL,
    employee_id character varying(255),
    spid character varying(255),
    fix_day_of_week character varying(255),
    start_time character varying(255),
    end_time character varying(255),
    limit_num_appoint character varying(255)
);


ALTER TABLE imed.doctor_schedule OWNER TO postgres;

--
-- Name: document_scan; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.document_scan (
    document_scan_id character varying(255) NOT NULL,
    patient_id character varying(255),
    visit_id character varying(255),
    prescription_id character varying(255),
    image_file_name character varying(255),
    scan_eid character varying(255),
    scan_spid character varying(255),
    scan_date character varying(255),
    scan_time character varying(255),
    update_eid character varying(255),
    update_spid character varying(255),
    update_date character varying(255),
    update_time character varying(255),
    fix_document_scan_status_id character varying(255) DEFAULT '0'::character varying,
    folder_name character varying(255) DEFAULT ''::character varying,
    reference_image_id character varying(255)
);


ALTER TABLE imed.document_scan OWNER TO postgres;

--
-- Name: document_scan_comments; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.document_scan_comments (
    document_scan_comments_id character varying(255) NOT NULL,
    document_scan_id character varying(255),
    visit_id character varying(255),
    comments_message character varying(255),
    comments_eid character varying(255),
    comments_date character varying(255),
    comments_time character varying(255)
);


ALTER TABLE imed.document_scan_comments OWNER TO postgres;

--
-- Name: document_scan_drawing; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.document_scan_drawing (
    document_scan_drawing_id character varying(255) NOT NULL,
    document_scan_id character varying(255),
    x character varying(255),
    y character varying(255),
    color character varying(255),
    tool character varying(255),
    draw_eid character varying(255),
    draw_date character varying(255),
    draw_time character varying(255)
);


ALTER TABLE imed.document_scan_drawing OWNER TO postgres;

--
-- Name: document_scan_dx_date; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.document_scan_dx_date (
    document_scan_dx_date_id character varying(255) NOT NULL,
    document_scan_id character varying(255),
    dx_date character varying(255),
    dx_message text,
    update_eid character varying(255),
    update_spid character varying(255),
    update_date character varying(255),
    update_time character varying(255)
);


ALTER TABLE imed.document_scan_dx_date OWNER TO postgres;

--
-- Name: drawing_details; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.drawing_details (
    drawing_details_id character varying(255) NOT NULL,
    drawing_physical_exam_id character varying(255),
    mode character varying(255),
    sub_mode character varying(255),
    x_axis character varying(255),
    y_axis character varying(255),
    width character varying(255),
    height character varying(255),
    rotation character varying(255),
    color character varying(255),
    text_desc character varying(255),
    line_width character varying(255),
    x_txt character varying(255),
    y_txt character varying(255),
    width_txt character varying(255),
    height_txt character varying(255)
);


ALTER TABLE imed.drawing_details OWNER TO postgres;

--
-- Name: drawing_physical_exam; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.drawing_physical_exam (
    drawing_physical_exam_id character varying(255) NOT NULL,
    visit_id character varying(255),
    attending_physician_id character varying(255),
    template_drawing_id character varying(255),
    doctor_eid character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255)
);


ALTER TABLE imed.drawing_physical_exam OWNER TO postgres;

--
-- Name: drug_allergy; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.drug_allergy (
    drug_allergy_id character varying(255) NOT NULL,
    patient_id character varying(255),
    generic_name character varying(255),
    trade_name character varying(255),
    adr_group_name character varying(255),
    assess_result character varying(255),
    approve character varying(255),
    symptom text,
    approve_eid character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    fix_drug_allergy_type character varying(255),
    naranjo_score character varying(255)
);


ALTER TABLE imed.drug_allergy OWNER TO postgres;

--
-- Name: drug_allergy_naranjo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.drug_allergy_naranjo (
    drug_allergy_naranjo_id character varying(255) NOT NULL,
    drug_allergy_id character varying(255),
    fix_naranjo_question_id character varying(255),
    naranjo_score character varying(255),
    fix_naranjo_answer_id character varying(255)
);


ALTER TABLE imed.drug_allergy_naranjo OWNER TO postgres;

--
-- Name: drug_allergy_temp; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.drug_allergy_temp (
    drug_allergy_temp_id character varying(255) NOT NULL,
    patient_id character varying(255),
    note text,
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255)
);


ALTER TABLE imed.drug_allergy_temp OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: drug_specialist_only; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.drug_specialist_only (
    drug_specialist_only_id character varying(255) NOT NULL,
    item_id character varying(255),
    employee_role_group_id character varying(255)
);


ALTER TABLE imed.drug_specialist_only OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: dt_diagnosis; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.dt_diagnosis (
    dt_diagnosis_id character varying(255) NOT NULL,
    visit_id character varying(255),
    patient_id character varying(255),
    vital_sign_extend_id character varying(255),
    diagnosis character varying(255),
    tooth_number character varying(255),
    tooth_surface character varying(255),
    dx_note character varying(255),
    doctor_eid character varying(255),
    diagnosis_date character varying(255),
    diagnosis_time character varying(255)
);


ALTER TABLE imed.dt_diagnosis OWNER TO postgres;

--
-- Name: dt_teeth_chart; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.dt_teeth_chart (
    dt_teeth_chart_id character varying(255) NOT NULL,
    patient_id character varying(255),
    tooth_number character varying(255),
    fix_dt_tooth_type_id character varying(255),
    is_missing_tooth character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    modify_eid character varying(255)
);


ALTER TABLE imed.dt_teeth_chart OWNER TO postgres;

--
-- Name: dt_treatment; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.dt_treatment (
    dt_treatment_id character varying(255) NOT NULL,
    item_id character varying(255),
    order_item_id character varying(255),
    patient_id character varying(255),
    visit_id character varying(255),
    fix_item_type_id character varying(255),
    tooth_number character varying(255),
    tooth_surface character varying(255),
    tx_note text,
    course_treatment_id character varying(255),
    course_price_estimate character varying(255),
    course_next_tx_note text,
    is_course_completed character varying(255),
    doctor_eid character varying(255),
    treatment_date character varying(255),
    treatment_time character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255)
);


ALTER TABLE imed.dt_treatment OWNER TO postgres;

--
-- Name: dt_treatment_external; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.dt_treatment_external (
    dt_treatment_external_id character varying(255) NOT NULL,
    item_id character varying(255),
    patient_id character varying(255),
    visit_id character varying(255),
    tooth_number character varying(255),
    tooth_surface character varying(255),
    tx_note text,
    doctor_eid character varying(255),
    treatment_date character varying(255),
    treatment_time character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255)
);


ALTER TABLE imed.dt_treatment_external OWNER TO postgres;

--
-- Name: dt_treatment_plan; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.dt_treatment_plan (
    dt_treatment_plan_id character varying(255) NOT NULL,
    item_id character varying(255),
    patient_id character varying(255),
    visit_id character varying(255),
    plan_no character varying(255),
    tooth_number character varying(255),
    tooth_surface character varying(255),
    tx_note text,
    doctor_eid character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255)
);


ALTER TABLE imed.dt_treatment_plan OWNER TO postgres;

--
-- Name: emergency_accident; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.emergency_accident (
    emergency_accident_id character varying(255) NOT NULL,
    emergency_data_id character varying(255),
    visit_id character varying(255),
    fix_accident_type_id character varying(255),
    appearance_place character varying(255),
    appearance_date character varying(255),
    appearance_time character varying(255),
    sender character varying(255),
    risk_alcohol character varying(255),
    risk_safty_helmet character varying(255),
    risk_safty_belt character varying(255),
    risk_other character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    is_case character varying(255),
    case_note text,
    base_accident_zone_id character varying(255),
    base_er_victim_type_id character varying(255)
);


ALTER TABLE imed.emergency_accident OWNER TO postgres;

--
-- Name: emergency_data; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.emergency_data (
    emergency_data_id character varying(255) NOT NULL,
    visit_id character varying(255),
    height character varying(255),
    weight character varying(255),
    bmi character varying(255),
    blood_sugar character varying(255),
    hct character varying(255),
    main_symptom character varying(255),
    current_illness character varying(255),
    patient_examine character varying(255),
    fix_emergency_type_id character varying(255),
    fix_coming_type_id character varying(255),
    record_eid character varying(255),
    record_date character varying(255),
    record_time character varying(255),
    doctor_eid character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255)
);


ALTER TABLE imed.emergency_data OWNER TO postgres;

--
-- Name: emergency_vital_sign; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.emergency_vital_sign (
    emergency_vital_sign_id character varying(255) NOT NULL,
    emergency_data_id character varying(255),
    pressure_max character varying(255),
    pressure_min character varying(255),
    pulse character varying(255),
    respiration character varying(255),
    temperature character varying(255),
    sat_o2 character varying(255),
    abnormal_pressure character varying(255),
    abnormal_pulse character varying(255),
    measure_eid character varying(255),
    measure_date character varying(255),
    measure_time character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255)
);


ALTER TABLE imed.emergency_vital_sign OWNER TO postgres;

--
-- Name: employee; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.employee (
    employee_id character varying(255) NOT NULL,
    employee_code character varying(255),
    prename character varying(255),
    firstname character varying(255),
    lastname character varying(255),
    password character varying(255),
    profession_code character varying(255),
    alias_name character varying(255),
    base_clinic_id character varying(255),
    base_service_point_id character varying(255),
    fix_employee_type_id character varying(255),
    employee_position character varying(255),
    connection_profile_id character varying(255),
    on_work character varying(255),
    print_doctor_schedule text,
    queue_xray_type character varying(255),
    doctor_type character varying(255),
    fix_clinic_id character varying(255),
    active character varying(255),
    fix_doctor_category_id character varying(255),
    pid character varying(255),
    base_med_department_id character varying(255),
    intername character varying(255),
    member_id character varying(255)
);


ALTER TABLE imed.employee OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: employee_department; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.employee_department (
    employee_department_id character varying(255) NOT NULL,
    employee_id character varying(255),
    base_department_id character varying(255)
);


ALTER TABLE imed.employee_department OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: employee_report_role; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.employee_report_role (
    employee_report_role_id character varying(255) NOT NULL,
    employee_id character varying(255),
    report_sql_query_auth character varying(255),
    report_module_auth character varying(255),
    report_opd_billing_auth character varying(255),
    report_ipd_billing_auth character varying(255),
    report_drug_and_supply_auth character varying(255),
    report_xray_auth character varying(255),
    report_ipd_auth character varying(255),
    report_account_auth character varying(255),
    report_other_auth character varying(255),
    report_setup_auth character varying(255)
);


ALTER TABLE imed.employee_report_role OWNER TO postgres;

--
-- Name: employee_role; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.employee_role (
    employee_role_id character varying(255) NOT NULL,
    employee_id character varying(255),
    tabmenu_auth character varying(255),
    mainmenu_auth character varying(255),
    patient_data_submenu_auth character varying(255),
    service_submenu_auth character varying(255),
    order_submenu_auth character varying(255),
    cash_submenu_auth character varying(255),
    appointment_submenu_auth character varying(255),
    opd_submenu_auth character varying(255),
    ipd_submenu_auth character varying(255),
    medical_submenu_auth character varying(255),
    xray_submenu_auth character varying(255),
    schedule_submenu_auth character varying(255),
    print_submenu_auth character varying(255),
    system_submenu_auth character varying(255),
    patient_social_button_auth character varying(255),
    operation_button_auth character varying(255),
    labor_button_auth character varying(255),
    emergency_button_auth character varying(255),
    patient_medical_button_auth character varying(255),
    lab_button_auth character varying(255),
    xray_button_auth character varying(255),
    vital_sign_button_auth character varying(255),
    order_button_auth character varying(255),
    icd10_button_auth character varying(255),
    icd9_button_auth character varying(255),
    billing_opd_button_auth character varying(255),
    billing_ipd_button_auth character varying(255),
    admit_button_auth character varying(255),
    emr_auth character varying(255),
    order_auth character varying(255),
    extra_auth character varying(255),
    page_default character varying(255),
    admin_module_auth character varying(255),
    admin_system_auth character varying(255),
    admin_user_and_sp_auth character varying(255),
    admin_item_auth character varying(255),
    admin_drug_detail_auth character varying(255),
    admin_stock_auth character varying(255),
    admin_stock_manage_auth character varying(255),
    admin_stock_report_auth character varying(255),
    admin_medical_auth character varying(255),
    admin_lab_auth character varying(255),
    admin_xray_auth character varying(255),
    admin_operation_auth character varying(255),
    admin_register_auth character varying(255),
    admin_dental_auth character varying(255),
    admin_billing_auth character varying(255),
    admin_other_auth character varying(255),
    admin_report_auth character varying(255),
    admin_manage_item_auth character varying(255),
    admin_manage_base_auth character varying(255),
    admin_manage_help_auth character varying(255),
    report_tab_module_auth character varying(255),
    custom_tab_auth character varying(255),
    base_employee_role_id character varying(255),
    nutrition_button_auth character varying(255),
    doc_scan_auth character varying(255) DEFAULT '000'::character varying,
    admin_manage_med_device_auth character varying(255),
    admin_nutrition_auth character varying(255),
    admin_blood_bank_auth character varying(255),
    anesthesia_button_auth character varying(255),
    admin_forensic_auth character varying(255),
    custom_menu_auth character varying(255),
    out_hospital_access_auth character varying(255),
    df_submenu_auth character varying(255),
    patho_button_auth character varying(255),
    custom_print_auth character varying(255),
    df_parttime_submenu_auth character varying(255)
);


ALTER TABLE imed.employee_role OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: employee_role_group; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.employee_role_group (
    employee_role_group_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.employee_role_group OWNER TO postgres;

--
-- Name: employee_role_group_detail; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.employee_role_group_detail (
    employee_role_group_detail_id character varying(255) NOT NULL,
    employee_role_group_id character varying(255),
    employee_id character varying(255)
);


ALTER TABLE imed.employee_role_group_detail OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: employee_type_default; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.employee_type_default (
    employee_type_default_id character varying(255) NOT NULL,
    fix_employee_type_id character varying(255),
    list_order_condition character varying(255),
    show_visit_cost character varying(255),
    search_item_option character varying(255),
    order_drug_cursor character varying(255)
);


ALTER TABLE imed.employee_type_default OWNER TO postgres;

--
-- Name: family_history; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.family_history (
    family_history_id character varying(255) NOT NULL,
    patient_id character varying(255),
    name character varying(255),
    note character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255)
);


ALTER TABLE imed.family_history OWNER TO postgres;

--
-- Name: feature_cash_doc; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.feature_cash_doc (
    feature_cash_doc_id character varying(255) NOT NULL,
    fix_receipt_type_id character varying(255),
    fix_visit_type_id character varying(255),
    active character varying(255) DEFAULT '1'::character varying,
    print_num character varying(255) DEFAULT '1'::character varying
);


ALTER TABLE imed.feature_cash_doc OWNER TO postgres;

--
-- Name: feature_doctor_assign; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.feature_doctor_assign (
    feature_doctor_assign_id character varying(255) NOT NULL,
    base_service_point_id character varying(255),
    finish_assign_type character varying(255),
    lab_xray_assign_type character varying(255),
    after_seen_doctor_spid character varying(255),
    default_pharm_spid character varying(255),
    default_cash_spid character varying(255)
);


ALTER TABLE imed.feature_doctor_assign OWNER TO postgres;

--
-- Name: feature_exception; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.feature_exception (
    feature_exception_id character varying(255) NOT NULL,
    persistence_id character varying(255),
    fix_exception_type_id character varying(255)
);


ALTER TABLE imed.feature_exception OWNER TO postgres;

--
-- Name: feature_option; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.feature_option (
    feature_option_id character varying(255) NOT NULL,
    description character varying(255),
    enable_feature character varying(255),
    addition_feature character varying(255)
);


ALTER TABLE imed.feature_option OWNER TO postgres;

--
-- Name: feature_order_short_key; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.feature_order_short_key (
    feature_order_short_key_id character varying(255) NOT NULL,
    item_id character varying(255)
);


ALTER TABLE imed.feature_order_short_key OWNER TO postgres;

--
-- Name: file4_chronic; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.file4_chronic (
    file4_chronic_id character varying(255) NOT NULL,
    visit_id character varying(255),
    datedx character varying(255),
    chronic character varying(255),
    detail character varying(255),
    typedx character varying(255),
    daterx character varying(255),
    typerx character varying(255),
    current character varying(255),
    datedis character varying(255),
    typedis character varying(255),
    resdis character varying(255),
    update character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255)
);


ALTER TABLE imed.file4_chronic OWNER TO postgres;

--
-- Name: file4_death; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.file4_death (
    file4_death_id character varying(255) NOT NULL,
    visit_id character varying(255),
    death_date character varying(255),
    death_time character varying(255),
    cdeath_a character varying(255),
    tdeath_a_date character varying(255),
    tdeath_a_time character varying(255),
    cdeath_b character varying(255),
    tdeath_b_date character varying(255),
    tdeath_b_time character varying(255),
    cdeath_c character varying(255),
    tdeath_c_date character varying(255),
    tdeath_c_time character varying(255),
    cdeath_d character varying(255),
    tdeath_d_date character varying(255),
    tdeath_d_time character varying(255),
    odisease character varying(255),
    cdeath character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255)
);


ALTER TABLE imed.file4_death OWNER TO postgres;

--
-- Name: file4_promote; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.file4_promote (
    file4_promote_id character varying(255) NOT NULL,
    visit_id character varying(255),
    hptype character varying(255),
    hpcode character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255)
);


ALTER TABLE imed.file4_promote OWNER TO postgres;

--
-- Name: file4_surveil; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.file4_surveil (
    file4_surveil_id character varying(255) NOT NULL,
    visit_id character varying(255),
    diagcode character varying(255),
    code506 character varying(255),
    illdate character varying(255),
    illhouse character varying(255),
    illvill character varying(255),
    illtamb character varying(255),
    illampu character varying(255),
    illchan character varying(255),
    ptstat character varying(255),
    ddeath character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255)
);


ALTER TABLE imed.file4_surveil OWNER TO postgres;

--
-- Name: finger_print; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.finger_print (
    finger_print_id character varying(10) NOT NULL,
    hn character varying(255),
    template text
);


ALTER TABLE imed.finger_print OWNER TO postgres;

--
-- Name: fix_bed_status; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.fix_bed_status (
    fix_bed_status_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.fix_bed_status OWNER TO postgres;

--
-- Name: fix_dt_surface; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.fix_dt_surface (
    fix_dt_surface_id character varying(255) NOT NULL,
    surface_description character varying(255)
);


ALTER TABLE imed.fix_dt_surface OWNER TO postgres;

--
-- Name: fix_dt_tooth; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.fix_dt_tooth (
    fix_dt_tooth_id character varying(255) NOT NULL,
    description character varying(255),
    permanent_number character varying(255),
    milk_number character varying(255)
);


ALTER TABLE imed.fix_dt_tooth OWNER TO postgres;

--
-- Name: fix_dt_tooth_type; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.fix_dt_tooth_type (
    fix_dt_tooth_type_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.fix_dt_tooth_type OWNER TO postgres;

--
-- Name: fix_emergency_type; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.fix_emergency_type (
    fix_emergency_type_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.fix_emergency_type OWNER TO postgres;

--
-- Name: fix_film_status; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.fix_film_status (
    fix_film_status_id character varying(254) NOT NULL,
    description character varying(254)
);


ALTER TABLE imed.fix_film_status OWNER TO postgres;

--
-- Name: fix_gender; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.fix_gender (
    fix_gender_id character varying(255) NOT NULL,
    gender_name character varying(255)
);


ALTER TABLE imed.fix_gender OWNER TO postgres;

--
-- Name: fix_icd10; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.fix_icd10 (
    fix_icd10_id character varying(255) NOT NULL,
    code character varying(255),
    description character varying(255),
    detail text,
    nick_name text,
    is_secret character varying(255),
    is_not_dx character varying(255) DEFAULT '0'::character varying
);


ALTER TABLE imed.fix_icd10 OWNER TO postgres;

--
-- Name: fix_icd10_4; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.fix_icd10_4 (
    fix_icd10_4_id character varying(255) NOT NULL,
    code_begin character varying(255),
    code_end character varying(255),
    description text
);


ALTER TABLE imed.fix_icd10_4 OWNER TO postgres;

--
-- Name: fix_icd10_5; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.fix_icd10_5 (
    fix_icd10_5_id character varying(255) NOT NULL,
    code_begin character varying(255),
    code_end character varying(255),
    description text
);


ALTER TABLE imed.fix_icd10_5 OWNER TO postgres;

--
-- Name: fix_icd9; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.fix_icd9 (
    fix_icd9_id character varying(255) NOT NULL,
    code character varying(255),
    description character varying(255),
    detail text,
    nick_name text,
    is_not_dx character varying(255) DEFAULT '0'::character varying
);


ALTER TABLE imed.fix_icd9 OWNER TO postgres;

--
-- Name: fix_idx10v3; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.fix_idx10v3 (
    fix_idx10v3_id character varying(255) NOT NULL,
    code1 character varying(255),
    code2 character varying(255),
    description character varying(255),
    see character varying(255),
    char4 character varying(255),
    char5 character varying(255)
);


ALTER TABLE imed.fix_idx10v3 OWNER TO postgres;

--
-- Name: fix_ipd_discharge_type; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.fix_ipd_discharge_type (
    fix_ipd_discharge_type_id character varying(255) NOT NULL,
    fix_ipd_discharge_type_name character varying(255)
);


ALTER TABLE imed.fix_ipd_discharge_type OWNER TO postgres;

--
-- Name: fix_item_type; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.fix_item_type (
    fix_item_type_id character varying(255) NOT NULL,
    item_type_name character varying(255)
);


ALTER TABLE imed.fix_item_type OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: fix_lab_normal_value_type; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.fix_lab_normal_value_type (
    fix_lab_normal_value_type_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.fix_lab_normal_value_type OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: fix_lab_test_type; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.fix_lab_test_type (
    fix_lab_test_type_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.fix_lab_test_type OWNER TO postgres;

--
-- Name: fix_marriage; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.fix_marriage (
    fix_marriage_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.fix_marriage OWNER TO postgres;

--
-- Name: fix_nationality; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.fix_nationality (
    fix_nationality_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.fix_nationality OWNER TO postgres;

--
-- Name: fix_occupation; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.fix_occupation (
    fix_occupation_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.fix_occupation OWNER TO postgres;

--
-- Name: fix_pindex99; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.fix_pindex99 (
    fix_pindex99_id character varying(255) NOT NULL,
    code character varying(255),
    description text
);


ALTER TABLE imed.fix_pindex99 OWNER TO postgres;

--
-- Name: fix_race; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.fix_race (
    fix_race_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.fix_race OWNER TO postgres;

--
-- Name: fix_visit_type; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.fix_visit_type (
    fix_visit_type_id character varying(255) NOT NULL,
    visit_type_name_th character varying(255),
    visit_type_name_en character varying(255)
);


ALTER TABLE imed.fix_visit_type OWNER TO postgres;

--
-- Name: fix_xray_type; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.fix_xray_type (
    fix_xray_type_id character varying(254) NOT NULL,
    description character varying(254)
);


ALTER TABLE imed.fix_xray_type OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: forensic_data; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.forensic_data (
    forensic_data_id character varying(255) NOT NULL,
    patient_id character varying(255),
    forensic_number character varying(255),
    base_police_station_id character varying(255),
    police_name character varying(255),
    deliver_history text,
    death_date character varying(255),
    death_time character varying(255),
    found_date character varying(255),
    found_time character varying(255),
    examine_date character varying(255),
    examine_time character varying(255),
    is_check_place character varying(255),
    check_place_doctor character varying(255),
    check_place_staff character varying(255),
    base_generation_id character varying(255),
    base_skin_color_id character varying(255),
    height character varying(255),
    weight character varying(255),
    base_forensic_death_behaviour_id character varying(255),
    forensic_out_wound_note text,
    is_forensic_in_exam character varying(255),
    death_reason character varying(255),
    note text,
    doctor_eid character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    reported character varying(255)
);


ALTER TABLE imed.forensic_data OWNER TO postgres;

--
-- Name: forensic_in_exam; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.forensic_in_exam (
    forensic_in_exam_id character varying(255) NOT NULL,
    forensic_data_id character varying(255),
    is_found character varying(255),
    amount character varying(255),
    note character varying(255),
    base_forensic_in_exam_detail_id character varying(255),
    fix_forensic_in_exam_type character varying(255)
);


ALTER TABLE imed.forensic_in_exam OWNER TO postgres;

--
-- Name: forensic_out_exam; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.forensic_out_exam (
    forensic_out_exam_id character varying(255) NOT NULL,
    forensic_data_id character varying(255),
    base_forensic_out_exam_id character varying(255),
    exam_result text
);


ALTER TABLE imed.forensic_out_exam OWNER TO postgres;

--
-- Name: forensic_team; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.forensic_team (
    forensic_team_id character varying(255) NOT NULL,
    forensic_data_id character varying(255),
    employee_id character varying(255)
);


ALTER TABLE imed.forensic_team OWNER TO postgres;

--
-- Name: freport_advice; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.freport_advice (
    freport_advice_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.freport_advice OWNER TO postgres;

--
-- Name: freport_appointstatus; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.freport_appointstatus (
    freport_appointstatus_id character(1) NOT NULL,
    description character varying
);


ALTER TABLE imed.freport_appointstatus OWNER TO postgres;

--
-- Name: freport_child_age; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.freport_child_age (
    freport_child_age_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.freport_child_age OWNER TO postgres;

--
-- Name: freport_doctor_type; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.freport_doctor_type (
    freport_doctor_type_id character varying NOT NULL,
    description character varying
);


ALTER TABLE imed.freport_doctor_type OWNER TO postgres;

--
-- Name: freport_patient_age; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.freport_patient_age (
    freport_patient_age_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.freport_patient_age OWNER TO postgres;

--
-- Name: health_promotion_visit; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.health_promotion_visit (
    health_promotion_visit_id character varying(255) NOT NULL,
    patient_id character varying(255),
    visit_id character varying(255),
    pre_visit_id character varying(255),
    examine_result text,
    doctor_eid character varying(255),
    examine_date character varying(255),
    examine_time character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255)
);


ALTER TABLE imed.health_promotion_visit OWNER TO postgres;

--
-- Name: health_promotion_xray_result; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.health_promotion_xray_result (
    health_promotion_xray_result_id character varying(255) NOT NULL,
    health_promotion_visit_id character varying(255),
    order_item_id character varying(255),
    item_id character varying(255),
    hp_xray_result text,
    is_print character varying(255)
);


ALTER TABLE imed.health_promotion_xray_result OWNER TO postgres;

--
-- Name: help_admit_physician_data; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.help_admit_physician_data (
    help_admit_physician_data_id character varying(255) NOT NULL,
    visit_id character varying(255),
    admit_doctor_eid character varying(255),
    attending_doctor_eid character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255)
);


ALTER TABLE imed.help_admit_physician_data OWNER TO postgres;

--
-- Name: hilosequences; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.hilosequences (
    sequencename character varying(50) NOT NULL,
    highvalues integer NOT NULL
);


ALTER TABLE imed.hilosequences OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: hist; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.hist (
    hist_id character(27) NOT NULL,
    hn character(12),
    doc_no character(4),
    store_no character(3),
    date_in character(10),
    time_in character(8),
    room_in character(6),
    dr_code character(6),
    dr_fee character(8),
    dr_diag character(50),
    staff_no character(6),
    ya_code character(10),
    ya_name character(80),
    ya_um character(8),
    ya_amount character(9),
    ya_pack character(10),
    ya_uprice character(12),
    ya_ucost character(12),
    item_code character(24),
    usage character(15),
    is_updated character(1),
    is_takehome character(1)
);


ALTER TABLE imed.hist OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: hp_checkup; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.hp_checkup (
    hp_checkup_id character varying(255) NOT NULL,
    visit_id character varying(255),
    lab_recommen text,
    other_test text,
    conclusion text,
    doctor_eid character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    is_close_hp_checkup character varying(255),
    xray_recommen text,
    pre_visit_id character varying(255)
);


ALTER TABLE imed.hp_checkup OWNER TO postgres;

--
-- Name: hp_checkup_sugges; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.hp_checkup_sugges (
    hp_checkup_sugges_id character varying(255) NOT NULL,
    base_hp_checkup_sugges_id character varying(255),
    hp_checkup_id character varying(255),
    hp_checkup_sugges_value character varying(255)
);


ALTER TABLE imed.hp_checkup_sugges OWNER TO postgres;

--
-- Name: hp_lab_commit; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.hp_lab_commit (
    hp_lab_commit_id character varying(255) NOT NULL,
    hp_checkup_id character varying(255) NOT NULL,
    order_item_id character varying(255) NOT NULL,
    commit_position character varying(255) NOT NULL
);


ALTER TABLE imed.hp_lab_commit OWNER TO postgres;

--
-- Name: hp_medical_history; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.hp_medical_history (
    hp_medical_history_id character varying(255) NOT NULL,
    hp_checkup_id character varying(255),
    fix_medical_group_id character varying(255),
    fix_medical_name_id character varying(255),
    fix_medical_result_id character varying(255),
    note text,
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255)
);


ALTER TABLE imed.hp_medical_history OWNER TO postgres;

--
-- Name: imed_config; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.imed_config (
    imed_config_id character varying(255) NOT NULL,
    config_detail bytea,
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    extra_module_code character varying(255),
    extra_module_description character varying(255)
);


ALTER TABLE imed.imed_config OWNER TO postgres;

--
-- Name: imed_language; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.imed_language (
    imed_language_id character varying(255) NOT NULL,
    config_detail bytea,
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255)
);


ALTER TABLE imed.imed_language OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: imed_version; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.imed_version (
    imed_version_id character varying(255) NOT NULL
);


ALTER TABLE imed.imed_version OWNER TO postgres;

--
-- Name: ipd_attending_physician; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.ipd_attending_physician (
    ipd_attending_physician_id character varying(254) NOT NULL,
    admit_id character varying(254),
    employee_id character varying(254),
    priority character varying(254),
    base_med_department_id character varying(255),
    begin_date character varying(255),
    begin_time character varying(255),
    end_date character varying(255),
    end_time character varying(255),
    is_current character varying(255)
);


ALTER TABLE imed.ipd_attending_physician OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: ipensook_billing_group_category_mapping; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.ipensook_billing_group_category_mapping (
    base_category_group_id character varying(255) NOT NULL,
    base_billing_group_id character varying(255)
);


ALTER TABLE imed.ipensook_billing_group_category_mapping OWNER TO postgres;

--
-- Name: ipensook_interface_item_update; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.ipensook_interface_item_update (
    id character varying(255) NOT NULL,
    last_update_date character varying(255)
);


ALTER TABLE imed.ipensook_interface_item_update OWNER TO postgres;

--
-- Name: ipensook_interface_log; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.ipensook_interface_log (
    ipensook_interface_log_id character varying(255) NOT NULL,
    op_name character varying(255) NOT NULL,
    op_start_date character varying(255) NOT NULL,
    op_start_time character varying(255) NOT NULL,
    op_status character varying(255) NOT NULL,
    op_xml_data text,
    op_finish_date character varying(255),
    op_finish_time character varying(255)
);


ALTER TABLE imed.ipensook_interface_log OWNER TO postgres;

--
-- Name: ipensook_interface_order; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.ipensook_interface_order (
    imed_id character varying(255) NOT NULL,
    interface_order_id character varying(255) NOT NULL
);


ALTER TABLE imed.ipensook_interface_order OWNER TO postgres;

--
-- Name: ipensook_item_category_mapping; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.ipensook_item_category_mapping (
    base_category_group_id character varying(255) NOT NULL,
    fix_item_type_id character varying(255)
);


ALTER TABLE imed.ipensook_item_category_mapping OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: item; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.item (
    item_id character varying(255) NOT NULL,
    item_code character varying(255),
    common_name character varying(255),
    drug_generic_name character varying(255),
    drug_group_name character varying(255),
    drug_trade_name character varying(255),
    nick_name character varying(255),
    note character varying(255),
    fix_item_type_id character varying(255),
    fix_chrgitem_id character varying(255),
    base_category_group_id character varying(255),
    base_billing_group_opd_id character varying(255),
    base_billing_group_ipd_id character varying(255),
    base_billing_group_home_id character varying(255),
    base_invoice_group_id character varying(255),
    base_unit_id character varying(255),
    pack_size character varying(255) DEFAULT '1'::character varying,
    pack_unit_id character varying(255),
    base_lab_type_id character varying(255),
    fix_lab_result_type_id character varying(255),
    base_specimen_id character varying(255),
    fix_report_type_id character varying(255),
    description text,
    description_en text,
    base_drug_instruction_id character varying(255),
    base_dose_unit_id character varying(255),
    caution text,
    caution_en text,
    base_drug_type_id character varying(255),
    base_drug_group_id character varying(255),
    base_drug_format_id character varying(255),
    film_charge character varying(255),
    base_xray_type_id character varying(255),
    hospital_item character varying(255) DEFAULT '1'::character varying,
    stock_critical character varying(255) DEFAULT '0'::character varying,
    mid_price1 character varying(255),
    mid_price1_note character varying(255),
    mid_price2 character varying(255),
    mid_price2_note character varying(255),
    mid_price3 character varying(255),
    mid_price3_note character varying(255),
    unit_price_cost character varying(255) DEFAULT '0'::character varying,
    ned_group character varying(255),
    nled_group_id character varying(255),
    reg_no character varying(255),
    gpo_price1 character varying(255),
    gpo_price2 character varying(255),
    gpo_price3 character varying(255),
    base_dent_operation_id character varying(255),
    dental_color_code character varying(255),
    fix_set_type_id character varying(255) DEFAULT '0'::character varying,
    fix_sticker_type_id character varying(255) DEFAULT '0'::character varying,
    print_report character varying(255) DEFAULT '1'::character varying,
    report_price character varying(255),
    edit_price character varying(255) DEFAULT '0'::character varying,
    use_stock character varying(255) DEFAULT '1'::character varying,
    doctor_share character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    active character varying(255) DEFAULT '1'::character varying,
    secret_status character varying(255),
    max_price character varying(255),
    is_bidding character varying(255),
    bidder_id character varying(255),
    quota_qty character varying(255),
    quota_unit_id character varying(255),
    base_drug_sub_class_id character varying(255),
    print_name character varying(255),
    base_dt_tx_type_id character varying(255),
    estimate_unit_price_cost character varying(255) DEFAULT '0'::character varying,
    investigate_time character varying(255),
    is_produce character varying(255),
    produce_process text,
    is_high_alert_drug character varying(255),
    is_narcotic_drug character varying(255),
    is_psychotropic_drug character varying(255),
    drug_use_detail text,
    is_must_set_expire_date character varying(255),
    high_alert_drug_note text,
    narcotic_drug_note text,
    is_drug_supply_consignment character varying(255),
    is_drug_due character varying(255),
    is_lab_have_summary character varying(255),
    fix_lab_result_special_id character varying(255),
    base_chi_group_id character varying(255) DEFAULT 'G'::character varying,
    out_of_order_alert character varying(255),
    nhso_vaccine_id character varying(255) DEFAULT ''::character varying,
    nhso_drug_code_id character varying(255) DEFAULT ''::character varying,
    nhso_fptype_id character varying(255) DEFAULT ''::character varying,
    fix_stock_item_type character varying(255),
    base_adp_code_id character varying(255) DEFAULT ''::character varying,
    base_drug_std_code character varying(255) DEFAULT ''::character varying,
    fix_stock_request_pack_size character varying(255),
    barcode character varying(255)
);


ALTER TABLE imed.item OWNER TO postgres;

--
-- Name: item_discount; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.item_discount (
    item_discount_id character varying(255) NOT NULL,
    item_id character varying(255),
    plan_id character varying(255),
    share_limit character varying(255)
);


ALTER TABLE imed.item_discount OWNER TO postgres;

--
-- Name: item_dispensed; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.item_dispensed (
    item_dispense_id character varying(255) NOT NULL,
    item_id character varying(255),
    stock_id character varying(255),
    sp_id character varying(255),
    dispensed_quantity character varying(255),
    dispensed_date character varying(255),
    update_date character varying(255),
    update_time character varying(255),
    update_eid character varying(255),
    active character varying(255)
);


ALTER TABLE imed.item_dispensed OWNER TO postgres;

--
-- Name: item_image; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.item_image (
    item_image_id character varying(255) NOT NULL,
    item_id character varying(255),
    image_name character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    modify_eid character varying(255),
    image_note text,
    is_default character varying(255)
);


ALTER TABLE imed.item_image OWNER TO postgres;

--
-- Name: item_plan_group; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.item_plan_group (
    item_plan_group_id character varying(255) NOT NULL,
    item_id character varying(255),
    base_plan_group_id character varying(255)
);


ALTER TABLE imed.item_plan_group OWNER TO postgres;

--
-- Name: item_price; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.item_price (
    item_price_id character varying(255) NOT NULL,
    item_id character varying(255),
    base_tariff_id character varying(255),
    active_date character varying(255),
    unit_price character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    modify_eid character varying(255)
);


ALTER TABLE imed.item_price OWNER TO postgres;

--
-- Name: item_repack; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.item_repack (
    item_repack_id character varying(255) NOT NULL,
    item_src_id character varying(255),
    item_dest_id character varying(255),
    rate character varying(255)
);


ALTER TABLE imed.item_repack OWNER TO postgres;

--
-- Name: item_set; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.item_set (
    item_set_id character varying(255) NOT NULL,
    description character varying(255),
    fix_order_set_type_id character varying(255),
    item_id character varying(255),
    order_price character varying(255),
    item_set_note text,
    is_multi_visit character varying(255)
);


ALTER TABLE imed.item_set OWNER TO postgres;

--
-- Name: item_set_print_out; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.item_set_print_out (
    item_set_print_out_id character varying(255) NOT NULL,
    item_set_id character varying(255),
    detail character varying(255),
    detail_price character varying(255)
);


ALTER TABLE imed.item_set_print_out OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: itemwetcha; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.itemwetcha (
    item_id character varying(255) NOT NULL,
    item_code character varying(255),
    common_name character varying(255),
    drug_generic_name character varying(255),
    drug_group_name character varying(255),
    drug_trade_name character varying(255),
    nick_name character varying(255),
    note character varying(255),
    fix_item_type_id character varying(255),
    fix_chrgitem_id character varying(255),
    base_category_group_id character varying(255),
    base_billing_group_opd_id character varying(255),
    base_billing_group_ipd_id character varying(255),
    base_billing_group_home_id character varying(255),
    base_invoice_group_id character varying(255),
    base_unit_id character varying(255),
    pack_size character varying(255),
    pack_unit_id character varying(255),
    base_lab_type_id character varying(255),
    fix_lab_result_type_id character varying(255),
    base_specimen_id character varying(255),
    fix_report_type_id character varying(255),
    description text,
    description_en text,
    base_drug_instruction_id character varying(255),
    base_dose_unit_id character varying(255),
    caution text,
    caution_en text,
    base_drug_type_id character varying(255),
    base_drug_group_id character varying(255),
    base_drug_format_id character varying(255),
    film_charge character varying(255),
    base_xray_type_id character varying(255),
    hospital_item character varying(255),
    stock_critical character varying(255),
    mid_price1 character varying(255),
    mid_price1_note character varying(255),
    mid_price2 character varying(255),
    mid_price2_note character varying(255),
    mid_price3 character varying(255),
    mid_price3_note character varying(255),
    unit_price_cost character varying(255),
    ned_group character varying(255),
    nled_group_id character varying(255),
    reg_no character varying(255),
    gpo_price1 character varying(255),
    gpo_price2 character varying(255),
    gpo_price3 character varying(255),
    base_dent_operation_id character varying(255),
    dental_color_code character varying(255),
    fix_set_type_id character varying(255),
    fix_sticker_type_id character varying(255),
    print_report character varying(255),
    report_price character varying(255),
    edit_price character varying(255),
    use_stock character varying(255),
    doctor_share character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    active character varying(255),
    secret_status character varying(255),
    max_price character varying(255),
    is_bidding character varying(255),
    bidder_id character varying(255),
    quota_qty character varying(255),
    quota_unit_id character varying(255),
    base_drug_sub_class_id character varying(255),
    print_name character varying(255),
    base_dt_tx_type_id character varying(255),
    estimate_unit_price_cost character varying(255),
    investigate_time character varying(255),
    is_produce character varying(255),
    produce_process text,
    is_high_alert_drug character varying(255),
    is_narcotic_drug character varying(255),
    is_psychotropic_drug character varying(255),
    drug_use_detail text,
    is_must_set_expire_date character varying(255),
    high_alert_drug_note text,
    narcotic_drug_note text,
    is_drug_supply_consignment character varying(255),
    is_drug_due character varying(255),
    is_lab_have_summary character varying(255),
    fix_lab_result_special_id character varying(255),
    base_chi_group_id character varying(255),
    out_of_order_alert character varying(255),
    nhso_vaccine_id character varying(255),
    nhso_drug_code_id character varying(255),
    nhso_fptype_id character varying(255),
    fix_stock_item_type character varying(255),
    base_adp_code_id character varying(255),
    base_drug_std_code character varying(255),
    fix_stock_request_pack_size character varying(255)
);


ALTER TABLE imed.itemwetcha OWNER TO postgres;

--
-- Name: jbm_counter; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.jbm_counter (
    name character varying(255) NOT NULL,
    next_id bigint
);


ALTER TABLE imed.jbm_counter OWNER TO postgres;

--
-- Name: jbm_dual; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.jbm_dual (
    dummy integer NOT NULL
);


ALTER TABLE imed.jbm_dual OWNER TO postgres;

--
-- Name: jbm_id_cache; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.jbm_id_cache (
    node_id integer NOT NULL,
    cntr integer NOT NULL,
    jbm_id character varying(255)
);


ALTER TABLE imed.jbm_id_cache OWNER TO postgres;

--
-- Name: jbm_msg; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.jbm_msg (
    message_id bigint NOT NULL,
    reliable character(1),
    expiration bigint,
    "timestamp" bigint,
    priority smallint,
    type smallint,
    headers bytea,
    payload bytea
);


ALTER TABLE imed.jbm_msg OWNER TO postgres;

--
-- Name: jbm_msg_ref; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.jbm_msg_ref (
    message_id bigint NOT NULL,
    channel_id bigint NOT NULL,
    transaction_id bigint,
    state character(1),
    ord bigint,
    page_ord bigint,
    delivery_count integer,
    sched_delivery bigint
);


ALTER TABLE imed.jbm_msg_ref OWNER TO postgres;

--
-- Name: jbm_postoffice; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.jbm_postoffice (
    postoffice_name character varying(255) NOT NULL,
    node_id integer NOT NULL,
    queue_name character varying(255) NOT NULL,
    cond character varying(1023),
    selector character varying(1023),
    channel_id bigint,
    clustered character(1),
    all_nodes character(1)
);


ALTER TABLE imed.jbm_postoffice OWNER TO postgres;

--
-- Name: jbm_role; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.jbm_role (
    role_id character varying(32) NOT NULL,
    user_id character varying(32) NOT NULL
);


ALTER TABLE imed.jbm_role OWNER TO postgres;

--
-- Name: jbm_tx; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.jbm_tx (
    node_id integer,
    transaction_id bigint NOT NULL,
    branch_qual bytea,
    format_id integer,
    global_txid bytea
);


ALTER TABLE imed.jbm_tx OWNER TO postgres;

--
-- Name: jbm_user; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.jbm_user (
    user_id character varying(32) NOT NULL,
    passwd character varying(32) NOT NULL,
    clientid character varying(128)
);


ALTER TABLE imed.jbm_user OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: lab_microbiology; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.lab_microbiology (
    lab_microbiology_id character varying(254) NOT NULL,
    lab_result_id character varying(254),
    organisms_position character varying(254),
    base_organisms character varying(254),
    base_antibiotic character varying(254),
    value character varying(254),
    organisms_qty character varying(255),
    zone_diameter character varying(255)
);


ALTER TABLE imed.lab_microbiology OWNER TO postgres;

--
-- Name: lab_result; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.lab_result (
    lab_result_id character varying(254) NOT NULL,
    assign_lab_id character varying(254),
    visit_id character varying(254),
    order_item_id character varying(254),
    item_id character varying(254),
    base_lab_type_id character varying(254),
    patient_id character varying(254),
    ln character varying(254),
    start_date character varying(254),
    start_time character varying(254),
    finish_date character varying(254),
    finish_time character varying(254),
    note text,
    times_reported character varying(254),
    fix_lab_result_type_id character varying(254),
    reported character varying(254),
    report_eid character varying(254),
    secret_status character varying(254),
    secret_eid character varying(254),
    modify_eid character varying(254),
    modify_spid character varying(254),
    modify_date character varying(254),
    modify_time character varying(254),
    lab_micro_note text,
    fix_lab_result_special_id character varying(255)
);


ALTER TABLE imed.lab_result OWNER TO postgres;

--
-- Name: lab_test; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.lab_test (
    lab_test_id character varying(254) NOT NULL,
    lab_result_id character varying(254),
    order_item_id character varying(254),
    name character varying(254),
    value text,
    value_addition character varying(254),
    normal_value_max character varying(254),
    normal_value_min character varying(254),
    abnormal character varying(254),
    unit_text character varying(254),
    result_position character varying(254),
    fix_lab_test_type_id character varying(254),
    template_lab_test_id character varying(254),
    reported character varying(254),
    is_repeat character varying(254),
    modify_eid character varying(254),
    modify_date character varying(254),
    modify_time character varying(254),
    active character varying(254),
    critical_value_max character varying(255),
    critical_value_min character varying(255),
    fix_lab_normal_value_type_id character varying(255),
    sub_test_name character varying(255),
    is_have_addition_value character varying(255),
    comment text
);


ALTER TABLE imed.lab_test OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: lab_thalas_result; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.lab_thalas_result (
    lab_thalas_result_id character varying(255) NOT NULL,
    female_patient_id character varying(255),
    male_patient_id character varying(255),
    fix_lab_thalas_type_id character varying(255),
    base_lab_thalas_result_id character varying(255),
    thalas_result_name character varying(255),
    doctor_eid character varying(255),
    note text,
    assign_spid character varying(255),
    is_assign_out_hospital character varying(255),
    assign_hospital_id character varying(255),
    assign_date character varying(255),
    assign_time character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    female_visit_id character varying(255),
    male_visit_id character varying(255)
);


ALTER TABLE imed.lab_thalas_result OWNER TO postgres;

--
-- Name: lis_order; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.lis_order (
    lis_order_id character varying(255) NOT NULL,
    lis_req_res_id character varying(255),
    test_no character varying(255),
    normal character varying(255),
    ln character varying(255),
    result character varying(255),
    send_date character varying(255),
    send_time character varying(255),
    text_date character varying(255),
    text_time character varying(255),
    approve_date character varying(255),
    approve_time character varying(255),
    approve_by character varying(255),
    item_id character varying(255),
    automate_id character varying(255),
    is_express character varying(255),
    is_confrec character varying(255),
    is_confres character varying(255),
    base_lab_type_id character varying(255),
    order_item_id character varying(255),
    active character varying(255) DEFAULT '0'::character varying,
    repeat_result character varying(255),
    base_specimen_id character varying(255),
    result_graph text,
    flag character varying(255)
);


ALTER TABLE imed.lis_order OWNER TO postgres;

--
-- Name: lis_req_res; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.lis_req_res (
    lis_req_res_id character varying(255) NOT NULL,
    assign_lab_id character varying(255),
    ln character varying(255),
    doctor_eid character varying(255),
    patient_id character varying(255),
    send_spid character varying(255),
    is_send_to_his character varying(255),
    hn character varying(255),
    vn character varying(255),
    an character varying(255),
    fix_visit_type_id character varying(255),
    status character varying(255),
    is_delete character varying(255),
    request_date character varying(255),
    visit_id character varying(255),
    patient_birthdate character varying(255)
);


ALTER TABLE imed.lis_req_res OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: location_record; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.location_record (
    location_record_id character varying(255) NOT NULL,
    visit_id character varying(255),
    location_spid character varying(255),
    operate_eid character varying(255),
    incomming_date character varying(255),
    incomming_time character varying(255),
    outgoing_date character varying(255),
    outgoing_time character varying(255)
);


ALTER TABLE imed.location_record OWNER TO postgres;

--
-- Name: lr_anc; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.lr_anc (
    lr_anc_id character varying(255) NOT NULL,
    lr_pregnancy_id character varying(255),
    visit_id character varying(255),
    ga_week character varying(255),
    ga_day character varying(255),
    weight character varying(255),
    pressure_max character varying(255),
    pressure_min character varying(255),
    is_swell character varying(255),
    abnormal_swell character varying(255),
    abnormal_symptom character varying(255),
    base_lr_fundal_ht_id character varying(255),
    fundal_ht_cm character varying(255),
    base_lr_fetal_present_id character varying(255),
    fetal_present_position character varying(255),
    is_fhs character varying(255),
    fhs character varying(255),
    is_fetal_movement character varying(255),
    fetal_movement character varying(255),
    treatment character varying(255),
    examine_eid character varying(255),
    examine_date character varying(255),
    examine_time character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255)
);


ALTER TABLE imed.lr_anc OWNER TO postgres;

--
-- Name: lr_anc_exam_organ; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.lr_anc_exam_organ (
    lr_anc_exam_organ_id character varying(255) NOT NULL,
    lr_first_anc_id character varying(255),
    base_lr_exam_organ_id character varying(255),
    is_abnormal character varying(255),
    note text
);


ALTER TABLE imed.lr_anc_exam_organ OWNER TO postgres;

--
-- Name: lr_apgar_score; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.lr_apgar_score (
    lr_apgar_score_id character varying(255) NOT NULL,
    lr_newborn_id character varying(255),
    fix_apgar_minute character varying(255),
    heart_rate character varying(255),
    heart_rate_score character varying(255),
    respiration_effort character varying(255),
    muscle_tone character varying(255),
    reflex_irritability character varying(255),
    skin_color character varying(255),
    total_score character varying(255)
);


ALTER TABLE imed.lr_apgar_score OWNER TO postgres;

--
-- Name: lr_delivery; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.lr_delivery (
    lr_delivery_id character varying(255) NOT NULL,
    lr_pregnancy_id character varying(255),
    visit_id character varying(255),
    is_induction character varying(255),
    membrane_ruptured_date character varying(255),
    membrane_ruptured_time character varying(255),
    fix_membrane_ruptured_type character varying(255),
    base_lr_fluid_appearance_id character varying(255),
    labour_state_1_hr character varying(255),
    labour_state_1_min character varying(255),
    labour_state_2_hr character varying(255),
    labour_state_2_min character varying(255),
    labour_state_3_hr character varying(255),
    labour_state_3_min character varying(255),
    labour_total_hr character varying(255),
    labour_total_min character varying(255),
    fix_delivery_method character varying(255),
    base_lr_spont_position_id character varying(255),
    base_lr_delivery_method_id character varying(255),
    delivery_indication character varying(255),
    delivery_operator character varying(255),
    is_episiotomy character varying(255),
    base_lr_episiotomy_type_id character varying(255),
    is_laceration character varying(255),
    base_lr_laceration_specify_id character varying(255),
    blood_loss character varying(255),
    is_post_partum_record character varying(255),
    blood_pressure_max character varying(255),
    blood_pressure_min character varying(255),
    pulse_rate character varying(255),
    respiration_rate character varying(255),
    fundus character varying(255),
    pp_med_treatment character varying(255),
    placenta_delivered_date character varying(255),
    placenta_delivered_time character varying(255),
    is_abnormal_placenta character varying(255),
    abnormal_placenta_specify character varying(255),
    placenta_weight character varying(255),
    is_incomplete_membranes character varying(255),
    physician_eid character varying(255),
    nurse_eid character varying(255),
    anesthesist_eid character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    is_augmentation character varying(255),
    infant_quantity character varying(255),
    base_lr_episiotomy_degree_of_tear_id character varying(255),
    episiotomy_operator character varying(255),
    episiotomy_operator_supervisor character varying(255),
    intra_partum_complications character varying(255),
    complication character varying(255)
);


ALTER TABLE imed.lr_delivery OWNER TO postgres;

--
-- Name: lr_delivery_abnormal; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.lr_delivery_abnormal (
    lr_delivery_abnormal_id character varying(255) NOT NULL,
    lr_delivery_id character varying(255),
    base_lr_delivery_abnormal_id character varying(255)
);


ALTER TABLE imed.lr_delivery_abnormal OWNER TO postgres;

--
-- Name: lr_delivery_infant; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.lr_delivery_infant (
    lr_delivery_infant_id character varying(255) NOT NULL,
    lr_delivery_id character varying(255),
    no_of_infant character varying(255),
    labor_pain_date character varying(255),
    labor_pain_time character varying(255),
    membrane_ruptured_date character varying(255),
    membrane_ruptured_time character varying(255),
    fix_membrane_ruptured_type character varying(255),
    base_lr_fluid_appearance_id character varying(255),
    bearing_down_date character varying(255),
    bearing_down_time character varying(255),
    fetal_delivery_date character varying(255),
    fetal_delivery_time character varying(255),
    base_lr_fetal_delivery_method_id character varying(255),
    base_lr_fetal_delivery_position_id character varying(255),
    fetal_delivery_indication character varying(255),
    fetal_delivery_operator character varying(255),
    fetal_delivery_operator_name character varying(255),
    fetal_delivery_operator_supervisor character varying(255),
    fetal_delivery_operator_supervisor_name character varying(255),
    placenta_delivery_date character varying(255),
    placenta_delivery_time character varying(255),
    base_lr_placenta_delivery_method_id character varying(255),
    placenta_weight character varying(255),
    is_abnormal_placenta character varying(255),
    abnormal_placenta_specify character varying(255),
    is_incomplete_membranes character varying(255),
    umbilical_cord_length character varying(255),
    base_lr_umbilical_cord_insertion_id character varying(255),
    round_cord_around_neck character varying(255),
    labour_1_state_hr character varying(255),
    labour_1_state_min character varying(255),
    labour_2_state_hr character varying(255),
    labour_2_state_min character varying(255),
    labour_3_state_hr character varying(255),
    labour_3_state_min character varying(255),
    labour_total_hr character varying(255),
    labour_total_min character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255)
);


ALTER TABLE imed.lr_delivery_infant OWNER TO postgres;

--
-- Name: lr_delivery_medication; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.lr_delivery_medication (
    lr_delivery_medication_id character varying(255) NOT NULL,
    lr_delivery_id character varying(255),
    base_lr_delivery_medication_id character varying(255)
);


ALTER TABLE imed.lr_delivery_medication OWNER TO postgres;

--
-- Name: lr_first_anc; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.lr_first_anc (
    lr_first_anc_id character varying(255) NOT NULL,
    lr_pregnancy_id character varying(255),
    symptom character varying(255),
    symptom_other character varying(255),
    height character varying(255),
    weight character varying(255),
    pressure_max character varying(255),
    pressure_min character varying(255),
    treatment character varying(255),
    examine_eid character varying(255),
    examine_date character varying(255),
    examine_time character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255)
);


ALTER TABLE imed.lr_first_anc OWNER TO postgres;

--
-- Name: lr_newborn; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.lr_newborn (
    lr_newborn_id character varying(255) NOT NULL,
    lr_pregnancy_id character varying(255),
    visit_id character varying(255),
    infant_firstname character varying(255),
    infant_lastname character varying(255),
    infant_date character varying(255),
    infant_time character varying(255),
    infant_sex character varying(255),
    weight character varying(255),
    body_length character varying(255),
    head_length character varying(255),
    chest_length character varying(255),
    fix_infant_status character varying(255),
    is_anomalies character varying(255),
    anomalies_specify character varying(255),
    base_lr_resuscitation_id character varying(255),
    note character varying(255),
    physician_eid character varying(255),
    nurse_eid character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    lr_delivery_infant_id character varying(255),
    temperature character varying(255)
);


ALTER TABLE imed.lr_newborn OWNER TO postgres;

--
-- Name: lr_newborn_medication; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.lr_newborn_medication (
    lr_newborn_medication_id character varying(255) NOT NULL,
    lr_newborn_id character varying(255),
    base_lr_newborn_medication_id character varying(255)
);


ALTER TABLE imed.lr_newborn_medication OWNER TO postgres;

--
-- Name: lr_other_labor; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.lr_other_labor (
    lr_other_labor_id character varying(255) NOT NULL,
    patient_id character varying(255),
    gravida character varying(255),
    year_delivery character varying(255),
    base_lr_pregnancy_abnormal_id character varying(255),
    base_lr_ga_deliver_id character varying(255),
    base_lr_delivery_method_id character varying(255),
    base_lr_postpartum_abnormal_id character varying(255),
    is_normal_infant character varying(255),
    sex character varying(255),
    infant_weight character varying(255),
    base_lr_child_health_id character varying(255),
    note character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255)
);


ALTER TABLE imed.lr_other_labor OWNER TO postgres;

--
-- Name: lr_postpartum_abnormal; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.lr_postpartum_abnormal (
    lr_postpartum_abnormal_id character varying(255) NOT NULL,
    lr_delivery_id character varying(255),
    base_lr_postpartum_abnormal_id character varying(255)
);


ALTER TABLE imed.lr_postpartum_abnormal OWNER TO postgres;

--
-- Name: lr_pregnancy; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.lr_pregnancy (
    lr_pregnancy_id character varying(255) NOT NULL,
    patient_id character varying(255),
    visit_id character varying(255),
    gravida character varying(255),
    parity character varying(255),
    birth_on_edc character varying(255),
    birth_on_edc_last_times character varying(255),
    preterm_birth character varying(255),
    preterm_birth_last_times character varying(255),
    abortion character varying(255),
    abortion_last_times character varying(255),
    lmp_date character varying(255),
    edc_date character varying(255),
    first_anc_date character varying(255),
    first_fetal_movement_date character varying(255),
    base_lr_pregnancy_risk_id character varying(255),
    other_pregnancy_risk text,
    anc_eid character varying(255),
    lr_eid character varying(255),
    cancel_reason character varying(255),
    discharge character varying(255),
    discharge_eid character varying(255),
    discharge_date character varying(255),
    discharge_time character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    active character varying(255),
    edc_by_us_date character varying(255),
    base_lr_ga_deliver_id character varying(255),
    note text
);


ALTER TABLE imed.lr_pregnancy OWNER TO postgres;

--
-- Name: lr_pregnancy_abnormal; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.lr_pregnancy_abnormal (
    lr_pregnancy_abnormal_id character varying(255) NOT NULL,
    lr_pregnancy_id character varying(255),
    base_lr_pregnancy_abnormal_id character varying(255),
    note character varying(255)
);


ALTER TABLE imed.lr_pregnancy_abnormal OWNER TO postgres;

--
-- Name: lr_tetanus_vaccine; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.lr_tetanus_vaccine (
    lr_tetanus_vaccine_id character varying(255) NOT NULL,
    lr_pregnancy_id character varying(255),
    first_times character varying(255),
    second_times character varying(255),
    third_times character varying(255),
    fix_third_times_delivery character varying(255),
    is_not_get_vaccine character varying(255)
);


ALTER TABLE imed.lr_tetanus_vaccine OWNER TO postgres;

--
-- Name: lr_us_gynae_exam_result; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.lr_us_gynae_exam_result (
    lr_us_gynae_exam_result_id character varying(255) NOT NULL,
    lr_us_gynae_result_id character varying(255),
    base_lr_us_gynae_exam_id character varying(255),
    is_abnormal character varying(255),
    note character varying(255)
);


ALTER TABLE imed.lr_us_gynae_exam_result OWNER TO postgres;

--
-- Name: lr_us_gynae_result; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.lr_us_gynae_result (
    lr_us_gynae_result_id character varying(255) NOT NULL,
    lr_pregnancy_id character varying(255),
    fix_lr_gynae_method_id character varying(255),
    uterus_width character varying(255),
    uterus_length character varying(255),
    uterus_depth character varying(255),
    endometrial_thickness character varying(255),
    fix_lr_gynae_uterine_cavity_id character varying(255),
    ovary_rt_width character varying(255),
    ovary_rt_length character varying(255),
    ovary_rt_depth character varying(255),
    ovary_lt_width character varying(255),
    ovary_lt_length character varying(255),
    ovary_lt_depth character varying(255),
    note character varying(255),
    exam_eid character varying(255),
    exam_date character varying(255),
    exam_time character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255)
);


ALTER TABLE imed.lr_us_gynae_result OWNER TO postgres;

--
-- Name: lr_us_obs_exam_result; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.lr_us_obs_exam_result (
    lr_us_obs_exam_result_id character varying(255) NOT NULL,
    lr_us_obs_fetus_result_id character varying(255),
    base_lr_us_obs_exam_id character varying(255),
    is_abnormal character varying(255),
    note character varying(255)
);


ALTER TABLE imed.lr_us_obs_exam_result OWNER TO postgres;

--
-- Name: lr_us_obs_fetus_result; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.lr_us_obs_fetus_result (
    lr_us_obs_fetus_result_id character varying(255) NOT NULL,
    lr_us_obs_result_id character varying(255),
    fhs character varying(255),
    crown_rump_length character varying(255),
    biparietal_diameter character varying(255),
    head_circumfereance character varying(255),
    abdominal_circumfereance character varying(255),
    femur_length character varying(255),
    fix_lr_placenta_location_id character varying(255),
    placenta_location character varying(255),
    fix_lr_grade_id character varying(255),
    amniotic_volumn character varying(255),
    dvp character varying(255),
    fix_lr_fetus_presentation_id character varying(255),
    est_fetal_weight character varying(255),
    sex character varying(255),
    note character varying(255),
    no_of_fetus character varying(255)
);


ALTER TABLE imed.lr_us_obs_fetus_result OWNER TO postgres;

--
-- Name: lr_us_obs_result; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.lr_us_obs_result (
    lr_us_obs_result_id character varying(255) NOT NULL,
    lr_pregnancy_id character varying(255),
    ga_week character varying(255),
    ga_day character varying(255),
    exam_eid character varying(255),
    exam_date character varying(255),
    exam_time character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255)
);


ALTER TABLE imed.lr_us_obs_result OWNER TO postgres;

--
-- Name: lr_us_result; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.lr_us_result (
    lr_us_result_id character varying(255) NOT NULL,
    lr_pregnancy_id character varying(255),
    ga_week character varying(255),
    ga_day character varying(255),
    biparietal_diameter character varying(255),
    femur_length character varying(255),
    placenta character varying(255),
    fhs character varying(255),
    amniotic_volume character varying(255),
    note character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    examine_eid character varying(255),
    examine_date character varying(255),
    examine_time character varying(255)
);


ALTER TABLE imed.lr_us_result OWNER TO postgres;

--
-- Name: manufacturer; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.manufacturer (
    manufacturer_id character varying(255) NOT NULL,
    manufacturer_name character varying(255) DEFAULT ''::character varying,
    office_tel character varying(255) DEFAULT ''::character varying,
    office_fax character varying(255) DEFAULT ''::character varying,
    office_mobile character varying(255) DEFAULT ''::character varying,
    contact_person character varying(255) DEFAULT ''::character varying,
    active character varying(255)
);


ALTER TABLE imed.manufacturer OWNER TO postgres;

--
-- Name: medical_certificate; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.medical_certificate (
    medical_certificate_id character varying(255) NOT NULL,
    visit_id character varying(255),
    doctor_eid character varying(255),
    diagnosis_date character varying(255),
    patient_disease character varying(255),
    doctor_note text,
    stay_length character varying(255),
    stay_start character varying(255),
    stay_finish character varying(255),
    make_date character varying(255),
    print_eid character varying(255),
    print_date character varying(255),
    print_time character varying(255),
    fix_medcert_type_id character varying(255),
    patient_rank character varying(255),
    patient_amphur character varying(255),
    patient_changwat character varying(255),
    patient_department character varying(255),
    patient_ministry character varying(255),
    patient_symptom text,
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    patient_current_illness text,
    mc_number character varying(255)
);


ALTER TABLE imed.medical_certificate OWNER TO postgres;

--
-- Name: medical_device_used; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.medical_device_used (
    medical_device_used_id character varying(255) NOT NULL,
    visit_id character varying(255),
    base_med_device_id character varying(255),
    start_date character varying(255),
    start_time character varying(255),
    finish_date character varying(255),
    finish_time character varying(255),
    operator_eid character varying(255),
    current_used character varying(255)
);


ALTER TABLE imed.medical_device_used OWNER TO postgres;

--
-- Name: medical_history; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.medical_history (
    medical_history_id character varying(255) NOT NULL,
    patient_id character varying(255),
    name character varying(255),
    note character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255)
);


ALTER TABLE imed.medical_history OWNER TO postgres;

--
-- Name: medication_error; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.medication_error (
    medication_error_id character varying(255) NOT NULL,
    visit_id character varying(255),
    record_date character varying(255),
    record_time character varying(255),
    quantity_drug character varying(255),
    quantity_error character varying(255),
    rec_type character varying(255),
    rec_format character varying(255),
    rec_strength character varying(255),
    rec_quantity character varying(255),
    rec_instruction character varying(255),
    rec_frequency character varying(255),
    rec_dose character varying(255),
    rec_over character varying(255),
    rec_other character varying(255),
    chk_type character varying(255),
    chk_format character varying(255),
    chk_strength character varying(255),
    chk_quantity character varying(255),
    chk_instruction character varying(255),
    chk_frequency character varying(255),
    chk_dose character varying(255),
    chk_over character varying(255),
    chk_other character varying(255),
    pack_type character varying(255),
    pack_format character varying(255),
    pack_strength character varying(255),
    pack_quantity character varying(255),
    pack_over character varying(255),
    pack_mixed character varying(255),
    pack_other character varying(255),
    pack_rec_eid character varying(255),
    disp_miss_pat character varying(255),
    disp_miss_pat_pat_complain character varying(255),
    disp_miss_pat_self_check character varying(255),
    disp_not_complete character varying(255),
    disp_rec_eid character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255)
);


ALTER TABLE imed.medication_error OWNER TO postgres;

--
-- Name: pacs_mwlwl_mwl_key_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE imed.pacs_mwlwl_mwl_key_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE imed.pacs_mwlwl_mwl_key_seq OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: mwlwl; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.mwlwl (
    mwl_key integer DEFAULT nextval('pacs_mwlwl_mwl_key_seq'::regclass) NOT NULL,
    trigger_dttm character varying(14) NOT NULL,
    replica_dttm character varying(14) DEFAULT 'A'::character varying NOT NULL,
    envent_type character varying(32),
    character_set character varying(32) DEFAULT 'ISO_IR_100'::character varying NOT NULL,
    scheduled_aetitle character varying(16) DEFAULT 'ANY'::character varying NOT NULL,
    scheduled_dttm character varying(14) NOT NULL,
    scheduled_modality character varying(8) NOT NULL,
    scheduled_station character varying(32),
    scheduled_location character varying(32),
    scheduled_proc_id character varying(32) NOT NULL,
    scheduled_proc_desc character varying(64),
    scheduled_action_codes character varying(255),
    scheduled_proc_status character varying(32) NOT NULL,
    premedication character varying(32),
    contrast_agent character varying(32),
    requested_proc_id character varying(32) NOT NULL,
    requested_proc_desc character varying(64),
    requested_proc_codes character varying(255),
    requested_proc_priority character varying(32),
    requested_proc_reason character varying(64),
    requested_proc_comment character varying(255),
    study_instance_uid character varying(64) NOT NULL,
    proc_placer_order_no character varying(32),
    proc_filler_order_no character varying(32),
    accession_no character varying(32) NOT NULL,
    attend_doctor character varying(32),
    perform_doctor character varying(32),
    consult_doctor character varying(32),
    request_doctor character varying(32) NOT NULL,
    refer_doctor character varying(32),
    request_department character varying(32) NOT NULL,
    imaging_request_reason character varying(64),
    imaging_request_comments character varying(255),
    imaging_request_dttm character varying(14) NOT NULL,
    isr_placer_order_no character varying(32),
    isr_filler_order_no character varying(32),
    admission_id character varying(32),
    patient_transport character varying(32),
    patient_location character varying(32),
    patient_residency character varying(32),
    patient_name character varying(32) NOT NULL,
    patient_id character varying(32) NOT NULL,
    other_patient_name character varying(64),
    other_patient_id character varying(32),
    patient_birth_dttm character varying(8) NOT NULL,
    patient_sex character(1) NOT NULL,
    patient_weight character varying(16),
    patient_size character varying(16),
    patient_state character varying(32),
    confidentiality character varying(32),
    pregnancy_status character varying(32),
    medical_alerts character varying(32),
    contrast_allergies character varying(32),
    special_needs character varying(32),
    specialty character varying(32),
    diagnosis text,
    admit_dttm character varying(14),
    register_dttm character varying(14)
);


ALTER TABLE imed.mwlwl OWNER TO postgres;

--
-- Name: nhso_506; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.nhso_506 (
    icd10_code character varying(255) NOT NULL,
    group_506 character varying(255),
    description_en character varying(255),
    description_th character varying(255),
    alias_name character varying(255)
);


ALTER TABLE imed.nhso_506 OWNER TO postgres;

--
-- Name: nhso_anc; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.nhso_anc (
    nhso_anc_id character varying(255) NOT NULL,
    pcucode character varying(255),
    pid character varying(255),
    seq character varying(255),
    date_serv character varying(255),
    aplace character varying(255),
    gravida character varying(255),
    ancno character varying(255),
    ga character varying(255),
    ancres character varying(255),
    d_update character varying(255)
);


ALTER TABLE imed.nhso_anc OWNER TO postgres;

--
-- Name: nhso_appoint; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.nhso_appoint (
    nhso_appoint_id character varying(255) NOT NULL,
    pcucode character varying(255),
    pid character varying(255),
    seq character varying(255),
    date_serv character varying(255),
    apdate character varying(255),
    aptype character varying(255),
    apdiag character varying(255)
);


ALTER TABLE imed.nhso_appoint OWNER TO postgres;

--
-- Name: nhso_btype; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.nhso_btype (
    nhso_btype_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.nhso_btype OWNER TO postgres;

--
-- Name: nhso_card; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.nhso_card (
    nhso_card_id character varying(255) NOT NULL,
    pcucode character varying(255),
    cid character varying(255),
    pid character varying(255),
    instype character varying(255),
    insid character varying(255),
    start character varying(255),
    expir character varying(255),
    main character varying(255),
    sub character varying(255),
    d_update character varying(255)
);


ALTER TABLE imed.nhso_card OWNER TO postgres;

--
-- Name: nhso_chronic; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.nhso_chronic (
    nhso_chronic_id character varying(255) NOT NULL,
    pcucode character varying(255),
    cid character varying(255),
    pid character varying(255),
    datedx character varying(255),
    chronic character varying(255),
    datedis character varying(255),
    typedis character varying(255),
    d_update character varying(255)
);


ALTER TABLE imed.nhso_chronic OWNER TO postgres;

--
-- Name: nhso_death; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.nhso_death (
    nhso_death_id character varying(255) NOT NULL,
    pcucode character varying(255),
    cid character varying(255),
    pid character varying(255),
    ddeath character varying(255),
    cdeath_a character varying(255),
    cdeath_b character varying(255),
    cdeath_c character varying(255),
    cdeath_d character varying(255),
    odisease character varying(255),
    cdeath character varying(255),
    pdeath character varying(255),
    source character varying(255)
);


ALTER TABLE imed.nhso_death OWNER TO postgres;

--
-- Name: nhso_diag; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.nhso_diag (
    nhso_diag_id character varying(255) NOT NULL,
    pcucode character varying(255),
    pid character varying(255),
    seq character varying(255),
    date_serv character varying(255),
    diagtype character varying(255),
    diagcode character varying(255)
);


ALTER TABLE imed.nhso_diag OWNER TO postgres;

--
-- Name: nhso_drug; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.nhso_drug (
    nhso_drug_id character varying(255) NOT NULL,
    pcucode character varying(255),
    pid character varying(255),
    seq character varying(255),
    date_serv character varying(255),
    did character varying(255),
    amount character varying(255),
    drugpric character varying(255),
    drugcost character varying(255)
);


ALTER TABLE imed.nhso_drug OWNER TO postgres;

--
-- Name: nhso_drug_code; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.nhso_drug_code (
    nhso_drug_code_id character varying(255) NOT NULL,
    drug_name character varying(255),
    drug_pack character varying(255),
    drug_unit character varying(255),
    drug_usage character varying(255)
);


ALTER TABLE imed.nhso_drug_code OWNER TO postgres;

--
-- Name: nhso_epi; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.nhso_epi (
    nhso_epi_id character varying(255) NOT NULL,
    pcucode character varying(255),
    pid character varying(255),
    seq character varying(255),
    date_serv character varying(255),
    vcctype character varying(255),
    vccplace character varying(255),
    d_update character varying(255)
);


ALTER TABLE imed.nhso_epi OWNER TO postgres;

--
-- Name: nhso_fp; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.nhso_fp (
    nhso_fp_id character varying(255) NOT NULL,
    pcucode character varying(255),
    pid character varying(255),
    seq character varying(255),
    date_serv character varying(255),
    fptype character varying(255),
    did character varying(255),
    amount character varying(255),
    fpplace character varying(255)
);


ALTER TABLE imed.nhso_fp OWNER TO postgres;

--
-- Name: nhso_fptype; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.nhso_fptype (
    nhso_fptype_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.nhso_fptype OWNER TO postgres;

--
-- Name: nhso_home; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.nhso_home (
    nhso_home_id character varying(255) NOT NULL,
    pcucode character varying(255),
    hid character varying(255),
    house_id character varying(255),
    house character varying(255),
    village character varying(255),
    road character varying(255),
    tambon character varying(255),
    ampur character varying(255),
    changwat character varying(255),
    nfamily character varying(255),
    locatype character varying(255),
    vhvid character varying(255),
    headid character varying(255),
    toilet character varying(255),
    water character varying(255),
    wattype character varying(255),
    garbage character varying(255),
    hcare character varying(255),
    durable character varying(255),
    clean character varying(255),
    ventila character varying(255),
    light character varying(255),
    watertm character varying(255),
    mfood character varying(255),
    bctrl character varying(255),
    actrl character varying(255),
    d_update character varying(255),
    addrcode character varying(255)
);


ALTER TABLE imed.nhso_home OWNER TO postgres;

--
-- Name: nhso_mch; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.nhso_mch (
    nhso_mch_id character varying(255) NOT NULL,
    pcucode character varying(255),
    pid character varying(255),
    gravida character varying(255),
    lmp character varying(255),
    edc character varying(255),
    vdrl_rs character varying(255),
    hb_rs character varying(255),
    hiv_rs character varying(255),
    datehct character varying(255),
    hct_rs character varying(255),
    thalass character varying(255),
    dental character varying(255),
    tcaries character varying(255),
    tartar character varying(255),
    guminf character varying(255),
    bdate character varying(255),
    bresult character varying(255),
    bplace character varying(255),
    bhosp character varying(255),
    btype character varying(255),
    bdoctor character varying(255),
    lborn character varying(255),
    sborn character varying(255),
    ppcare1 character varying(255),
    ppcare2 character varying(255),
    ppcare3 character varying(255),
    ppres character varying(255),
    d_update character varying(255)
);


ALTER TABLE imed.nhso_mch OWNER TO postgres;

--
-- Name: nhso_nutri; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.nhso_nutri (
    nhso_nutri_id character varying(255) NOT NULL,
    pcucode character varying(255),
    pid character varying(255),
    seq character varying(255),
    date_serv character varying(255),
    agemonth character varying(255),
    weight character varying(255),
    height character varying(255),
    nlevel character varying(255),
    d_update character varying(255)
);


ALTER TABLE imed.nhso_nutri OWNER TO postgres;

--
-- Name: nhso_person; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.nhso_person (
    nhso_person_id character varying(255) NOT NULL,
    pcucode character varying(255),
    cid character varying(255),
    pid character varying(255),
    hid character varying(255),
    prename character varying(255),
    name character varying(255),
    lname character varying(255),
    hn character varying(255),
    sex character varying(255),
    birth character varying(255),
    house character varying(255),
    village character varying(255),
    road character varying(255),
    tambon character varying(255),
    ampur character varying(255),
    changwat character varying(255),
    mstatus character varying(255),
    occupa character varying(255),
    race character varying(255),
    nation character varying(255),
    religion character varying(255),
    educate character varying(255),
    fstatus character varying(255),
    father character varying(255),
    mother character varying(255),
    couple character varying(255),
    movein character varying(255),
    dischar character varying(255),
    ddisch character varying(255),
    bgroup character varying(255),
    labor character varying(255),
    d_update character varying(255),
    typelive character varying(255)
);


ALTER TABLE imed.nhso_person OWNER TO postgres;

--
-- Name: nhso_pp; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.nhso_pp (
    nhso_pp_id character varying(255) NOT NULL,
    pcucode character varying(255),
    pid character varying(255),
    mpid character varying(255),
    gravida character varying(255),
    bdate character varying(255),
    bplace character varying(255),
    bhosp character varying(255),
    btype character varying(255),
    bdoctor character varying(255),
    bweigth character varying(255),
    asphyxia character varying(255),
    vitk character varying(255),
    bcare1 character varying(255),
    bcare2 character varying(255),
    bcare3 character varying(255),
    bcres character varying(255),
    d_update character varying(255)
);


ALTER TABLE imed.nhso_pp OWNER TO postgres;

--
-- Name: nhso_proced; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.nhso_proced (
    nhso_proced_id character varying(255) NOT NULL,
    pcucode character varying(255),
    pid character varying(255),
    seq character varying(255),
    date_serv character varying(255),
    proced character varying(255),
    servpric character varying(255)
);


ALTER TABLE imed.nhso_proced OWNER TO postgres;

--
-- Name: nhso_service; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.nhso_service (
    nhso_service_id character varying(255) NOT NULL,
    pcucode character varying(255),
    pid character varying(255),
    seq character varying(255),
    date_serv character varying(255),
    locate character varying(255),
    pttype character varying(255),
    intime character varying(255),
    price character varying(255),
    instype character varying(255),
    insid character varying(255),
    main character varying(255),
    pay character varying(255),
    referin character varying(255),
    refinhos character varying(255),
    referout character varying(255),
    refouhos character varying(255),
    doctor character varying(255)
);


ALTER TABLE imed.nhso_service OWNER TO postgres;

--
-- Name: nhso_surveil; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.nhso_surveil (
    nhso_surveil_id character varying(255) NOT NULL,
    pcucode character varying(255),
    cid character varying(255),
    pid character varying(255),
    seq character varying(255),
    date_serv character varying(255),
    diagcode character varying(255),
    code506 character varying(255),
    illdate character varying(255),
    illhouse character varying(255),
    illvill character varying(255),
    illtamb character varying(255),
    illampu character varying(255),
    illchan character varying(255),
    ptstat character varying(255),
    date_death character varying(255),
    complica character varying(255),
    organism character varying(255)
);


ALTER TABLE imed.nhso_surveil OWNER TO postgres;

--
-- Name: nhso_vaccine; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.nhso_vaccine (
    nhso_vaccine_id character varying(255) NOT NULL,
    description_en character varying(255),
    description_th character varying(255)
);


ALTER TABLE imed.nhso_vaccine OWNER TO postgres;

--
-- Name: nhso_women; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.nhso_women (
    nhso_women_id character varying(255) NOT NULL,
    pcucode character varying(255),
    pid character varying(255),
    fptype character varying(255),
    nofp character varying(255),
    numson character varying(255),
    d_update character varying(255)
);


ALTER TABLE imed.nhso_women OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: nled_group; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.nled_group (
    nled_group_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.nled_group OWNER TO postgres;

--
-- Name: nt_change_order_sheet; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.nt_change_order_sheet (
    nt_change_order_sheet_id character varying(255) NOT NULL,
    nt_food_order_sheet_id character varying(255),
    change_status character varying(255),
    change_note character varying(255),
    change_eid character varying(255),
    change_date character varying(255),
    change_time character varying(255),
    accept_change_eid character varying(255),
    accept_change_date character varying(255),
    accept_change_time character varying(255)
);


ALTER TABLE imed.nt_change_order_sheet OWNER TO postgres;

--
-- Name: nt_food_order_sheet; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.nt_food_order_sheet (
    nt_food_order_sheet_id character varying(255) NOT NULL,
    visit_id character varying(255),
    delivery_date character varying(255),
    fix_nt_order_sheet_type_id character varying(255),
    is_same_all_meal character varying(255),
    assign_order_status character varying(255),
    assign_eid character varying(255),
    assign_date character varying(255),
    assign_time character varying(255),
    accept_eid character varying(255),
    accept_date character varying(255),
    accept_time character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    assign_spid character varying(255)
);


ALTER TABLE imed.nt_food_order_sheet OWNER TO postgres;

--
-- Name: nt_nutrition_facts; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.nt_nutrition_facts (
    nt_nutrition_facts_id character varying(255) NOT NULL,
    nt_order_id character varying(255),
    base_nt_nutrition_facts_id character varying(255),
    value character varying(255)
);


ALTER TABLE imed.nt_nutrition_facts OWNER TO postgres;

--
-- Name: nt_order; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.nt_order (
    nt_order_id character varying(255) NOT NULL,
    nt_food_order_sheet_id character varying(255),
    visit_id character varying(255),
    item_id character varying(255),
    order_item_id character varying(255),
    fix_order_status_id character varying(255),
    fix_nt_meal_type_id character varying(255),
    base_nt_type_id character varying(255),
    feeding_quantity character varying(255),
    feeding_frequency character varying(255),
    feeding_ratio character varying(255),
    total_energy character varying(255),
    order_quantity character varying(255),
    order_unit_price_sale character varying(255),
    order_plan_id character varying(255),
    note text,
    dispense_eid character varying(255),
    dispense_date character varying(255),
    dispense_time character varying(255),
    carbohydrate_ratio character varying(255),
    protein_ratio character varying(255),
    fats_ratio character varying(255)
);


ALTER TABLE imed.nt_order OWNER TO postgres;

--
-- Name: nt_patient_nutrition; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.nt_patient_nutrition (
    nt_patient_nutrition_id character varying(255) NOT NULL,
    patient_id character varying(255),
    food_allergy text,
    is_food_forbid character varying(255),
    is_infected character varying(255)
);


ALTER TABLE imed.nt_patient_nutrition OWNER TO postgres;

--
-- Name: nt_special_group; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.nt_special_group (
    nt_special_group_id character varying(255) NOT NULL,
    nt_order_id character varying(255),
    base_nt_special_group_id character varying(255)
);


ALTER TABLE imed.nt_special_group OWNER TO postgres;

--
-- Name: nt_supplement_diet; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.nt_supplement_diet (
    nt_supplement_diet_id character varying(255) NOT NULL,
    nt_order_id character varying(255),
    base_nt_supplement_type_id character varying(255),
    quantity character varying(255),
    order_item_id character varying(255)
);


ALTER TABLE imed.nt_supplement_diet OWNER TO postgres;

--
-- Name: nt_therapeutic_diet; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.nt_therapeutic_diet (
    nt_therapeutic_diet_id character varying(255) NOT NULL,
    nt_order_id character varying(255),
    base_nt_therapeutic_type_id character varying(255)
);


ALTER TABLE imed.nt_therapeutic_diet OWNER TO postgres;

--
-- Name: nursing_care_plan; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.nursing_care_plan (
    nursing_care_plan_id character varying(255) NOT NULL,
    admit_id character varying(255),
    problem_details text,
    plan_details text,
    result_details text,
    start_date character varying(255),
    start_time character varying(255),
    finish_date character varying(255),
    finish_time character varying(255),
    approve_eid character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255)
);


ALTER TABLE imed.nursing_care_plan OWNER TO postgres;

--
-- Name: old_hn; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.old_hn (
    old_hn_id character varying(255) NOT NULL,
    patient_id character varying(255),
    old_hn character varying(255),
    new_hn character varying(255),
    changed_date character varying(255),
    changed_time character varying(255),
    changed_eid character varying(255),
    changed_spid character varying(255)
);


ALTER TABLE imed.old_hn OWNER TO postgres;

--
-- Name: omega_general_data; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.omega_general_data (
    omega_general_data_id character varying(255) NOT NULL,
    sequence_number character varying(255),
    hn character varying(255),
    ln character varying(255),
    patient_firstname character varying(255),
    patient_lastname character varying(255),
    patient_birthday character varying(255),
    patient_gender character varying(255),
    date_time_register character varying(255),
    priority character varying(255),
    is_file_created character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    send_eid character varying(255),
    send_spid character varying(255),
    patient_prename character varying(255),
    visit_id character varying(255),
    visit_type character varying(255)
);


ALTER TABLE imed.omega_general_data OWNER TO postgres;

--
-- Name: omega_order_and_result_data; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.omega_order_and_result_data (
    omega_order_and_result_data_id character varying(255) NOT NULL,
    omega_general_data_id character varying(255),
    sequence_number character varying(255),
    item_name character varying(255),
    test_code character varying(255),
    action_code character varying(255),
    specimen_code character varying(255),
    priority character varying(255),
    section_code character varying(255),
    report_type character varying(255),
    result_value character varying(255),
    result_unit character varying(255),
    result_min_normal character varying(255),
    result_max_normal character varying(255),
    result_final character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    item_code character varying(255)
);


ALTER TABLE imed.omega_order_and_result_data OWNER TO postgres;

--
-- Name: op_doctor_schedule; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.op_doctor_schedule (
    op_doctor_schedule_id character varying(255) NOT NULL,
    employee_id character varying(255),
    fix_day_of_week character varying(255),
    start_time character varying(255),
    end_time character varying(255)
);


ALTER TABLE imed.op_doctor_schedule OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: op_operation_posture; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.op_operation_posture (
    op_operation_posture_id character varying(255) NOT NULL,
    op_registered_operation_id character varying(255),
    base_op_operation_posture_id character varying(255)
);


ALTER TABLE imed.op_operation_posture OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: op_registered; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.op_registered (
    op_registered_id character varying(255) NOT NULL,
    visit_id character varying(255),
    patient_id character varying(255),
    op_set_id character varying(255),
    base_op_room_id character varying(255),
    base_op_type_id character varying(255),
    base_op_clinic_id character varying(255),
    start_date character varying(255),
    start_time character varying(255),
    finish_date character varying(255),
    finish_time character varying(255),
    start_anes_date character varying(255),
    start_anes_time character varying(255),
    finish_anes_date character varying(255),
    finish_anes_time character varying(255),
    registered_eid character varying(255),
    registered_date character varying(255),
    registered_time character varying(255),
    result text,
    report_result_eid character varying(255),
    discharge character varying(255),
    discharge_eid character varying(255),
    discharge_date character varying(255),
    discharge_time character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    active character varying(255),
    base_op_wound_id character varying(255),
    blood_loss character varying(255)
);


ALTER TABLE imed.op_registered OWNER TO postgres;

--
-- Name: op_registered_anes_type; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.op_registered_anes_type (
    op_registered_anes_type_id character varying(255) NOT NULL,
    op_registered_id character varying(255),
    base_op_anes_type_id character varying(255),
    other text
);


ALTER TABLE imed.op_registered_anes_type OWNER TO postgres;

--
-- Name: op_registered_diagnosis; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.op_registered_diagnosis (
    op_registered_diagnosis_id character varying(255) NOT NULL,
    op_registered_id character varying(255),
    diagnosis_name character varying(255),
    icd10_code character varying(255),
    description character varying(255)
);


ALTER TABLE imed.op_registered_diagnosis OWNER TO postgres;

--
-- Name: op_registered_operation; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.op_registered_operation (
    op_registered_operation_id character varying(255) NOT NULL,
    op_registered_id character varying(255),
    operation_name character varying(255),
    operation_position character varying(255),
    icd9_code character varying(255),
    description character varying(255),
    re_operation_registered_id character varying(255),
    re_operation_times character varying(255),
    re_operation_hours character varying(255),
    re_operation_minutes character varying(255)
);


ALTER TABLE imed.op_registered_operation OWNER TO postgres;

--
-- Name: op_registered_physician; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.op_registered_physician (
    op_registered_physician_id character varying(255) NOT NULL,
    op_registered_id character varying(255),
    employee_id character varying(255),
    base_op_role_id character varying(255)
);


ALTER TABLE imed.op_registered_physician OWNER TO postgres;

--
-- Name: op_room_schedule; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.op_room_schedule (
    op_room_schedule_id character varying(255) NOT NULL,
    base_op_room_id character varying(255),
    fix_day_of_week character varying(255),
    start_time character varying(255),
    end_time character varying(255)
);


ALTER TABLE imed.op_room_schedule OWNER TO postgres;

--
-- Name: op_set; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.op_set (
    op_set_id character varying(255) NOT NULL,
    patient_id character varying(255),
    op_registered_id character varying(255),
    fix_op_set_status_id character varying(255),
    fix_op_status_id character varying(255),
    base_op_room_id character varying(255),
    op_appoint_date character varying(255),
    op_appoint_time character varying(255),
    estimate_hour character varying(255),
    estimate_minute character varying(255),
    set_spid character varying(255),
    set_doctor_eid character varying(255),
    set_eid character varying(255),
    set_date character varying(255),
    set_time character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    note text,
    accept_eid character varying(255),
    accept_date character varying(255),
    accept_time character varying(255),
    is_use_anes_service character varying(255),
    refrain_food_date character varying(255),
    refrain_food_time character varying(255)
);


ALTER TABLE imed.op_set OWNER TO postgres;

--
-- Name: op_set_anes_type; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.op_set_anes_type (
    op_set_anes_type_id character varying(255) NOT NULL,
    op_set_id character varying(255),
    base_op_anes_type_id character varying(255),
    other text
);


ALTER TABLE imed.op_set_anes_type OWNER TO postgres;

--
-- Name: op_set_delay; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.op_set_delay (
    op_set_delay_id character varying(255) NOT NULL,
    op_set_id character varying(255),
    fix_op_set_status_id character varying(255),
    base_op_delay_reason_id character varying(255),
    change_eid character varying(255),
    change_date character varying(255),
    change_time character varying(255)
);


ALTER TABLE imed.op_set_delay OWNER TO postgres;

--
-- Name: op_set_diagnosis; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.op_set_diagnosis (
    op_set_diagnosis_id character varying(255) NOT NULL,
    op_set_id character varying(255),
    diagnosis_name character varying(255)
);


ALTER TABLE imed.op_set_diagnosis OWNER TO postgres;

--
-- Name: op_set_extra; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.op_set_extra (
    op_set_extra_id character varying(255) NOT NULL,
    op_set_id character varying(255),
    base_op_set_extra_id character varying(255),
    note text
);


ALTER TABLE imed.op_set_extra OWNER TO postgres;

--
-- Name: op_set_operation; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.op_set_operation (
    op_set_operation_id character varying(255) NOT NULL,
    op_set_id character varying(255),
    operation_name character varying(255),
    operation_position character varying(255),
    re_operation_registered_id character varying(255)
);


ALTER TABLE imed.op_set_operation OWNER TO postgres;

--
-- Name: op_set_physician; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.op_set_physician (
    op_set_physician_id character varying(255) NOT NULL,
    op_set_id character varying(255),
    employee_id character varying(255),
    base_op_role_id character varying(255)
);


ALTER TABLE imed.op_set_physician OWNER TO postgres;

--
-- Name: opd_stat; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.opd_stat (
    id character varying(255) NOT NULL,
    hn_search character varying(255),
    on_date character varying(255),
    on_time character varying(255),
    on_case character varying(255),
    new_fox character varying(255),
    sex character varying(255),
    staff character varying(255),
    sf character varying(255),
    memo character varying(255),
    ret_date character varying(255),
    emergency character varying(255),
    vn character varying(255),
    check_fox character varying(255),
    icd character varying(255),
    usr_no character varying(255),
    refer character varying(255),
    pressure_h character varying(255),
    pressure_l character varying(255),
    tall character varying(255),
    weight character varying(255),
    temper character varying(255),
    ss_id character varying(255),
    uc_id character varying(255),
    pulse character varying(255),
    respirat character varying(255),
    opd_place character varying(255),
    check_out character varying(255),
    on_side character varying(255),
    visittype character varying(255),
    is_updated character varying(255)
);


ALTER TABLE imed.opd_stat OWNER TO postgres;

--
-- Name: order_continue; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.order_continue (
    order_continue_id character varying(255) NOT NULL,
    visit_id character varying(255),
    item_id character varying(255),
    fix_item_type_id character varying(255),
    base_drug_usage_code character varying(255),
    instruction_format character varying(255),
    base_drug_instruction_id character varying(255),
    dose_quantity character varying(255),
    base_dose_unit_id character varying(255),
    base_drug_frequency_id character varying(255),
    instruction_text_line1 character varying(255),
    instruction_text_line2 character varying(255),
    instruction_text_line3 character varying(255),
    quantity character varying(255),
    base_unit_id character varying(255),
    description character varying(255),
    caution character varying(255),
    unit_price_sale character varying(255),
    order_eid character varying(255),
    order_spid character varying(255),
    start_date character varying(255),
    start_time character varying(255),
    finish_date character varying(255),
    finish_time character varying(255),
    "interval" character varying(255),
    next_start_prepared_date character varying(255),
    next_start_prepared_time character varying(255),
    fix_order_continue_status_id character varying(255),
    discontinue_eid character varying(255),
    discontinue_date character varying(255),
    discontinue_time character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    next_order_date character varying(255),
    next_order_time character varying(255),
    is_room_price character varying(255),
    is_food_price character varying(255),
    drug_times_per_day character varying(255)
);


ALTER TABLE imed.order_continue OWNER TO postgres;

--
-- Name: order_continue_prepared; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.order_continue_prepared (
    order_continue_prepared_id character varying(255) NOT NULL,
    order_continue_id character varying(255),
    visit_id character varying(255),
    item_id character varying(255),
    fix_item_type_id character varying(255),
    base_drug_usage_code character varying(255),
    instruction_format character varying(255),
    base_drug_instruction_id character varying(255),
    dose_quantity character varying(255),
    base_dose_unit_id character varying(255),
    base_drug_frequency_id character varying(255),
    instruction_text_line1 character varying(255),
    instruction_text_line2 character varying(255),
    instruction_text_line3 character varying(255),
    quantity character varying(255),
    base_unit_id character varying(255),
    description character varying(255),
    caution character varying(255),
    take_home character varying(255),
    unit_price_sale character varying(255),
    order_eid character varying(255),
    order_spid character varying(255),
    order_date character varying(255),
    order_time character varying(255),
    active character varying(255)
);


ALTER TABLE imed.order_continue_prepared OWNER TO postgres;

--
-- Name: order_drug_interaction; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.order_drug_interaction (
    order_drug_interaction_id character varying(255) NOT NULL,
    order_date character varying(255),
    order_time character varying(255),
    order_item_id character varying(255),
    item_id character varying(255),
    order_item_interaction_id character varying(255),
    item_interaction_id character varying(255),
    order_interaction_reason_id character varying(255),
    order_generic_name character varying(255),
    order_interaction_generic_name character varying(255)
);


ALTER TABLE imed.order_drug_interaction OWNER TO postgres;

--
-- Name: order_item; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.order_item (
    order_item_id character varying(255) NOT NULL,
    visit_id character varying(255) DEFAULT ''::character varying,
    item_id character varying(255) DEFAULT ''::character varying,
    patient_id character varying(255) DEFAULT ''::character varying,
    fix_item_type_id character varying(255) DEFAULT ''::character varying,
    base_category_group_id character varying(255) DEFAULT ''::character varying,
    base_billing_group_id character varying(255) DEFAULT ''::character varying,
    base_lab_type_id character varying(255) DEFAULT ''::character varying,
    base_drug_usage_code character varying(255) DEFAULT ''::character varying,
    instruction_format character varying(255) DEFAULT ''::character varying,
    base_drug_instruction_id character varying(255) DEFAULT ''::character varying,
    dose_quantity character varying(255) DEFAULT ''::character varying,
    base_dose_unit_id character varying(255) DEFAULT ''::character varying,
    base_drug_frequency_id character varying(255) DEFAULT ''::character varying,
    instruction_text_line1 character varying(255) DEFAULT ''::character varying,
    instruction_text_line2 character varying(255) DEFAULT ''::character varying,
    instruction_text_line3 character varying(255) DEFAULT ''::character varying,
    quantity character varying(255) DEFAULT ''::character varying,
    base_unit_id character varying(255) DEFAULT ''::character varying,
    description text DEFAULT ''::text,
    caution text DEFAULT ''::text,
    take_home character varying(255) DEFAULT '0'::character varying,
    doctor_fee_eid character varying(255) DEFAULT ''::character varying,
    verify_eid character varying(255) DEFAULT ''::character varying,
    verify_date character varying(255) DEFAULT ''::character varying,
    verify_time character varying(255) DEFAULT ''::character varying,
    verify_spid character varying(255) DEFAULT ''::character varying,
    execute_eid character varying(255) DEFAULT ''::character varying,
    execute_date character varying(255) DEFAULT ''::character varying,
    execute_time character varying(255) DEFAULT ''::character varying,
    execute_spid character varying(255) DEFAULT ''::character varying,
    dispense_eid character varying(255) DEFAULT ''::character varying,
    dispense_date character varying(255) DEFAULT ''::character varying,
    dispense_time character varying(255) DEFAULT ''::character varying,
    dispense_spid character varying(255) DEFAULT ''::character varying,
    dispense_pin_code character varying(255) DEFAULT ''::character varying,
    remain_report_eid character varying(255) DEFAULT ''::character varying,
    remain_report_date character varying(255) DEFAULT ''::character varying,
    remain_report_note text DEFAULT ''::text,
    ignore_report_eid character varying(255) DEFAULT ''::character varying,
    ignore_report_date character varying(255) DEFAULT ''::character varying,
    ignore_report_time character varying(255) DEFAULT ''::character varying,
    ignore_report_note text DEFAULT ''::text,
    submit_ignore_eid character varying(255) DEFAULT ''::character varying,
    unit_price_cost character varying(255) DEFAULT '0'::character varying,
    unit_price_sale character varying(255) DEFAULT '0'::character varying,
    charge_complete character varying(255) DEFAULT ''::character varying,
    fix_order_status_id character varying(255) DEFAULT ''::character varying,
    di_status character varying(255) DEFAULT ''::character varying,
    fix_dispense_type_id character varying(255) DEFAULT ''::character varying,
    ned_group character varying(255) DEFAULT ''::character varying,
    fix_lab_result_type_id character varying(255) DEFAULT ''::character varying,
    base_specimen_id character varying(255) DEFAULT ''::character varying,
    keep_specimen_eid character varying(255) DEFAULT ''::character varying,
    dental_id character varying(255) DEFAULT ''::character varying,
    fix_assign_status_id character varying(255) DEFAULT ''::character varying,
    is_deposit character varying(255) DEFAULT ''::character varying,
    deposit_number character varying(255) DEFAULT ''::character varying,
    com_number character varying(255) DEFAULT ''::character varying,
    print_deposit_eid character varying(255) DEFAULT ''::character varying,
    print_deposit_date character varying(255) DEFAULT ''::character varying,
    print_deposit_time character varying(255) DEFAULT ''::character varying,
    fix_set_type_id character varying(255) DEFAULT ''::character varying,
    set_order_id character varying(255) DEFAULT ''::character varying,
    set_unit_price character varying(255) DEFAULT ''::character varying,
    fix_order_type_id character varying(255) DEFAULT ''::character varying,
    order_doctor_eid character varying(255) DEFAULT ''::character varying,
    order_spid character varying(255) DEFAULT ''::character varying,
    assigned_user character varying(255) DEFAULT ''::character varying,
    assigned_sp character varying(255) DEFAULT ''::character varying,
    assigned_date character varying(255) DEFAULT ''::character varying,
    assigned_time character varying(255) DEFAULT ''::character varying,
    assigned_ref_no character varying(255) DEFAULT ''::character varying,
    print_caution character varying(255) DEFAULT ''::character varying,
    fix_mix_drug_type_id character varying(255) DEFAULT ''::character varying,
    mix_drug_no character varying(255) DEFAULT ''::character varying,
    visit_payment_id character varying(255) DEFAULT ''::character varying,
    plan_id character varying(255) DEFAULT ''::character varying,
    fix_except_plan_id character varying(255) DEFAULT ''::character varying,
    modify_eid character varying(255) DEFAULT ''::character varying,
    modify_date character varying(255) DEFAULT ''::character varying,
    modify_time character varying(255) DEFAULT ''::character varying,
    original_unit_price character varying(255),
    base_df_mode_id character varying(255),
    base_xray_type_id character varying(255),
    diagnosis_icd9_id character varying(255),
    fix_language_type_id character varying(255),
    out_doctor character varying(255),
    out_doctor_hospital_id character varying(255),
    out_doctor_note text,
    sticker_quantity character varying(255),
    print_description character varying(255),
    dose_note character varying(255),
    op_registered_id character varying(255),
    base_specimen_description character varying(255),
    order_continue_id character varying(255),
    is_urgent character varying(255),
    estimate_unit_price_cost character varying(255),
    fix_drug_use character varying(255),
    order_drug_allergy_reason_id character varying(255),
    payer_share_unit character varying(255),
    unit_price_remain character varying(255),
    order_drug_pregnancy_reason_id character varying(255),
    is_drug_sticker_reprint character varying(255),
    order_drug_specialist_reason_id character varying(255),
    is_send_to_lis character varying(255) DEFAULT '0'::character varying,
    base_lab_ignore_report_reason_id character varying(255),
    fix_lab_result_special_id character varying(255),
    drug_times_per_day character varying(255),
    remain_report_out_hospital_id character varying(255),
    remain_report_out_date character varying(255),
    is_execute_over_time character varying(255),
    is_send_to_pacs character varying(255),
    is_extra_price character varying(255),
    is_update_stock character varying(255),
    dose_per_use character varying(255),
    accession_number character varying(255)
);


ALTER TABLE imed.order_item OWNER TO postgres;

--
-- Name: order_set_multi_visit; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.order_set_multi_visit (
    order_set_multi_visit_id character varying(255) NOT NULL,
    patient_id character varying(255),
    item_id character varying(255),
    courses_price character varying(255),
    is_complete character varying(255),
    times character varying(255) DEFAULT '0'::character varying,
    owner_set_multi_visit_id character varying(255),
    is_transfer_other character varying(255)
);


ALTER TABLE imed.order_set_multi_visit OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: order_set_multi_visit_child; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.order_set_multi_visit_child (
    order_set_multi_visit_child_id character varying(255) NOT NULL,
    order_set_multi_visit_id character varying(255),
    item_id character varying(255),
    quantity character varying(255)
);


ALTER TABLE imed.order_set_multi_visit_child OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: order_set_multi_visit_detail; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.order_set_multi_visit_detail (
    order_set_multi_visit_detail_id character varying(255) NOT NULL,
    order_set_multi_visit_id character varying(255),
    order_item_id character varying(255),
    visit_id character varying(255),
    order_no character varying(255),
    order_price character varying(255)
);


ALTER TABLE imed.order_set_multi_visit_detail OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: patho_assign_lab; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.patho_assign_lab (
    patho_assign_lab_id character varying(255) NOT NULL,
    assign_lab_id character varying(255),
    fix_lab_result_special_id character varying(255),
    is_contraceptive character varying(255),
    base_patho_hormonal_status_id character varying(255),
    lmp_date character varying(255),
    previous_pap_result character varying(255),
    fix_patho_autopsy_allow character varying(255),
    allow_almost_except_note text,
    allow_part_note text,
    is_consult character varying(255)
);


ALTER TABLE imed.patho_assign_lab OWNER TO postgres;

--
-- Name: patho_assign_pap_contraceptive; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.patho_assign_pap_contraceptive (
    patho_assign_pap_contraceptive_id character varying(255) NOT NULL,
    patho_assign_lab_id character varying(255),
    base_patho_contraceptive_id character varying(255)
);


ALTER TABLE imed.patho_assign_pap_contraceptive OWNER TO postgres;

--
-- Name: patho_autopsy_result; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.patho_autopsy_result (
    patho_autopsy_result_id character varying(255) NOT NULL,
    patho_result_id character varying(255),
    death_date character varying(255),
    death_time character varying(255),
    receive_date character varying(255),
    receive_time character varying(255),
    examine_date character varying(255),
    examine_time character varying(255)
);


ALTER TABLE imed.patho_autopsy_result OWNER TO postgres;

--
-- Name: patho_pap_result_abnormal; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.patho_pap_result_abnormal (
    patho_pap_result_abnormal_id character varying(255) NOT NULL,
    patho_pap_smear_result_id character varying(255),
    fix_patho_pap_squamous_cell character varying(255),
    is_squamous_hsil_with_feature character varying(255),
    is_glandular_atypical character varying(255),
    glandular_atypical_specify text,
    is_glandular_neoplastic character varying(255),
    glandular_neoplastic_specify text,
    is_glandular_ais character varying(255),
    is_glandular_adenocarcinoma character varying(255),
    glandular_adeno_specify text,
    is_other_malignant character varying(255),
    other_malignant_specify text
);


ALTER TABLE imed.patho_pap_result_abnormal OWNER TO postgres;

--
-- Name: patho_pap_result_negative; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.patho_pap_result_negative (
    patho_pap_result_negative_id character varying(255) NOT NULL,
    patho_pap_smear_result_id character varying(255),
    is_organism_trichomonas character varying(255),
    is_organism_fungus character varying(255),
    is_organism_suggestive character varying(255),
    is_organism_bacteria character varying(255),
    is_organism_cellular character varying(255),
    is_organism_other character varying(255),
    organism_other_specify text,
    is_other_reactive_change character varying(255),
    is_other_rc_inflammation character varying(255),
    is_other_rc_rediation character varying(255),
    is_other_rc_iud character varying(255),
    is_other_glandular character varying(255),
    is_other_atrophy character varying(255)
);


ALTER TABLE imed.patho_pap_result_negative OWNER TO postgres;

--
-- Name: patho_pap_result_others; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.patho_pap_result_others (
    patho_pap_result_others_id character varying(255) NOT NULL,
    patho_pap_smear_result_id character varying(255),
    is_endometrial_cells character varying(255),
    others_specify text
);


ALTER TABLE imed.patho_pap_result_others OWNER TO postgres;

--
-- Name: patho_pap_smear_result; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.patho_pap_smear_result (
    patho_pap_smear_result_id character varying(255) NOT NULL,
    patho_result_id character varying(255),
    fix_patho_pap_specimen_satis character varying(255),
    fix_patho_pap_specimen_processed character varying(255),
    is_general_malignancy character varying(255),
    is_general_epithelial_cell character varying(255),
    is_general_others character varying(255),
    note text
);


ALTER TABLE imed.patho_pap_smear_result OWNER TO postgres;

--
-- Name: patho_result; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.patho_result (
    patho_result_id character varying(255) NOT NULL,
    visit_id character varying(255),
    ln character varying(255),
    fix_lab_result_special_id character varying(255),
    base_lab_type_id character varying(255),
    clinical_information text,
    gross_microscopic_description text,
    diagnosis text,
    addendum text,
    note text,
    pathologist_eid character varying(255),
    second_pathologist_eid character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    reported character varying(255),
    report_date character varying(255),
    report_time character varying(255)
);


ALTER TABLE imed.patho_result OWNER TO postgres;

--
-- Name: patho_snomed; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.patho_snomed (
    patho_snomed_id character varying(255) NOT NULL,
    patho_result_id character varying(255),
    snomed_t_code character varying(255),
    snomed_m_code character varying(255),
    snomed_t_name character varying(255),
    snomed_m_name character varying(255)
);


ALTER TABLE imed.patho_snomed OWNER TO postgres;

--
-- Name: patho_specimen; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.patho_specimen (
    patho_specimen_id character varying(255) NOT NULL,
    order_item_id character varying(255),
    specimen_description character varying(255),
    patho_operation character varying(255),
    note text
);


ALTER TABLE imed.patho_specimen OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: patient; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.patient (
    patient_id character varying(255) NOT NULL,
    hn character varying(255),
    prename character varying(255),
    firstname character varying(255),
    lastname character varying(255),
    nsd character varying(255),
    lsd character varying(255),
    nickname character varying(255),
    pid character varying(255),
    any_id character varying(255),
    fix_gender_id character varying(255),
    birthdate character varying(255),
    birthdate_true character varying(255),
    blood_group character varying(255),
    email character varying(255),
    mobile character varying(255),
    fix_marriage_id character varying(255),
    fix_nationality_id character varying(255),
    fix_race_id character varying(255),
    fix_occupation_id character varying(255) DEFAULT '000'::character varying,
    religion character varying(255),
    couple_firstname character varying(255),
    couple_lastname character varying(255),
    father_firstname character varying(255),
    father_lastname character varying(255),
    mother_firstname character varying(255),
    mother_lastname character varying(255),
    patient_noname character varying(255),
    fix_new_in_year_id character varying(255),
    base_tariff_id character varying(255),
    fix_opd_file_status_id character varying(255),
    fix_ipd_file_status_id character varying(255),
    xn character varying(255),
    un character varying(255),
    en character varying(255),
    total_admit_times character varying(255),
    base_patient_type_id character varying(255),
    place_name character varying(255),
    place_nearly character varying(255),
    home_id character varying(255),
    road character varying(255),
    village character varying(255),
    fix_tambol_id character varying(255),
    fix_amphur_id character varying(255),
    fix_changwat_id character varying(255),
    postcode character varying(255),
    telephone character varying(255),
    foreign_address text,
    base_country_id character varying(255),
    generate_id character varying(255),
    proxy_status character varying(255),
    proxy_user character varying(255),
    proxy_date character varying(255),
    proxy_time character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    active character varying(255),
    is_death character varying(255),
    rtfn character varying(255),
    rtln character varying(255),
    crm text,
    manage_file_eid character varying(255),
    manage_file_date character varying(255),
    manage_file_time character varying(255),
    base_state_id character varying(255),
    base_any_id character varying(255),
    lane character varying(255),
    last_modify_blood_group_date character varying(255),
    last_modify_blood_group_time character varying(255),
    last_modify_blood_group_eid character varying(255),
    base_patient_group_id character varying(255) DEFAULT ''::character varying,
    base_patient_unit_id character varying(255) DEFAULT '00'::character varying,
    base_patient_file_location_id character varying(255) DEFAULT ''::character varying,
    patient_image_path character varying(255),
    patient_image_seq character varying(255) DEFAULT '0'::character varying,
    image_opd_card_seq character varying(255) DEFAULT '0'::character varying,
    image_chart_seq character varying(255) DEFAULT '0'::character varying,
    image_ekg_seq character varying(255) DEFAULT '0'::character varying,
    image_picture_seq character varying(255) DEFAULT '0'::character varying,
    image_xray_seq character varying(255) DEFAULT '0'::character varying,
    death_age character varying(255),
    old_hn character varying(255),
    crm_eid character varying(255),
    crm_date character varying(255),
    crm_time character varying(255),
    member_id character varying(255),
    home_latlng character varying(255)
);


ALTER TABLE imed.patient OWNER TO postgres;

--
-- Name: patient_address; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.patient_address (
    patient_address_id character varying(254) NOT NULL,
    patient_id character varying(254),
    place_name character varying(254),
    place_nearly character varying(254),
    home_id character varying(254),
    road character varying(254),
    village character varying(254),
    fix_tambol_id character varying(254),
    fix_amphur_id character varying(254),
    fix_changwat_id character varying(254),
    postcode character varying(254),
    telephone character varying(254),
    fix_address_type_id character varying(254),
    lane character varying(255)
);


ALTER TABLE imed.patient_address OWNER TO postgres;

--
-- Name: patient_contact; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.patient_contact (
    patient_contact_id character varying(255) NOT NULL,
    patient_id character varying(255),
    firstname character varying(255),
    lastname character varying(255),
    fix_gender_id character varying(255),
    relate_text text,
    email character varying(255),
    mobile character varying(255),
    place_name character varying(255),
    place_nearly character varying(255),
    home_id character varying(255),
    road character varying(255),
    village character varying(255),
    fix_tambol_id character varying(255),
    fix_amphur_id character varying(255),
    fix_changwat_id character varying(255),
    postcode character varying(255),
    telephone character varying(255),
    lane character varying(255)
);


ALTER TABLE imed.patient_contact OWNER TO postgres;

--
-- Name: patient_file_borrow; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.patient_file_borrow (
    patient_file_borrow_id character varying(255) NOT NULL,
    patient_id character varying(255),
    hn character varying(255),
    file_type character varying(255),
    fix_person_type_id character varying(255),
    borrow_name character varying(255),
    borrow_pid character varying(255),
    borrow_eid character varying(255),
    borrow_spid character varying(255),
    borrow_patient_id character varying(255),
    borrow_date character varying(255),
    borrow_time character varying(255),
    borrow_address text,
    telephone character varying(255),
    fix_borrow_file_type_id character varying(255),
    borrow_detail text,
    approve_eid character varying(255),
    return_person_type character varying(255),
    return_eid character varying(255),
    return_name character varying(255),
    return_date character varying(255),
    return_time character varying(255),
    receive_eid character varying(255),
    fix_file_status_id character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    an character varying(255),
    admit_id character varying(255),
    base_med_department_id character varying(255),
    is_tmp_file character varying(255) DEFAULT '0'::character varying
);


ALTER TABLE imed.patient_file_borrow OWNER TO postgres;

--
-- Name: patient_film; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.patient_film (
    patient_film_id character varying(255) NOT NULL,
    patient_id character varying(255),
    hn character varying(255),
    film_number character varying(255),
    fix_film_type_id character varying(255),
    fix_film_status_id character varying(255)
);


ALTER TABLE imed.patient_film OWNER TO postgres;

--
-- Name: patient_film_borrow; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.patient_film_borrow (
    patient_film_borrow_id character varying(255) NOT NULL,
    patient_film_id character varying(255),
    fix_person_type_id character varying(255),
    borrow_name character varying(255),
    borrow_pid character varying(255),
    borrow_eid character varying(255),
    borrow_spid character varying(255),
    borrow_patient_id character varying(255),
    borrow_doctor_eid character varying(255),
    borrow_date character varying(255),
    borrow_time character varying(255),
    borrow_address text,
    telephone character varying(255),
    borrow_detail text,
    approve_eid character varying(255),
    return_person_type character varying(255),
    return_eid character varying(255),
    return_name character varying(255),
    return_date character varying(255),
    return_time character varying(255),
    receive_eid character varying(255),
    fix_film_status_id character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    film_date character varying(255)
);


ALTER TABLE imed.patient_film_borrow OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: patient_image_seq; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.patient_image_seq (
    patient_image_seq_id character varying(255) NOT NULL,
    patient_id character varying(255),
    folder_name character varying(255),
    seq_value character varying(255)
);


ALTER TABLE imed.patient_image_seq OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: patient_media_perspective; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.patient_media_perspective (
    patient_media_perspective_id character varying(255) NOT NULL,
    patient_id character varying(255),
    base_patient_media_perspective_id character varying(255)
);


ALTER TABLE imed.patient_media_perspective OWNER TO postgres;

--
-- Name: patient_name_history; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.patient_name_history (
    patient_name_history_id character varying(255) NOT NULL,
    patient_id character varying(255),
    firstname character varying(255),
    lastname character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255)
);


ALTER TABLE imed.patient_name_history OWNER TO postgres;

--
-- Name: patient_number; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.patient_number (
    patient_number_id character varying(255) NOT NULL,
    patient_id character varying(255),
    base_patient_number_type character varying(255),
    identity_number character varying(255),
    out_hospital_id character varying(255),
    out_hospital_hn character varying(255)
);


ALTER TABLE imed.patient_number OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: patient_other_allergy; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.patient_other_allergy (
    patient_other_allergy_id character varying(255) NOT NULL,
    patient_id character varying(255),
    base_other_allergy_id character varying(255),
    base_other_allergy_level_id character varying(255),
    volume character varying(255),
    description character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255)
);


ALTER TABLE imed.patient_other_allergy OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: patient_payment; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.patient_payment (
    patient_payment_id character varying(255) NOT NULL,
    patient_id character varying(255),
    plan_id character varying(255),
    card_id character varying(255),
    inspire_date character varying(255),
    expire_date character varying(255),
    main_hospital_code character varying(255),
    sub_hospital_code character varying(255),
    subtype character varying(255)
);


ALTER TABLE imed.patient_payment OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: patient_relation_number; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.patient_relation_number (
    patient_relation_number_id character varying(255) NOT NULL,
    patient_id character varying(255),
    couple_patient_id character varying(255),
    mother_patient_id character varying(255),
    lr_newborn_id character varying(255)
);


ALTER TABLE imed.patient_relation_number OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: payer; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.payer (
    payer_id character varying(255) NOT NULL,
    prename character varying(255) DEFAULT ''::character varying,
    description character varying(255) DEFAULT ''::character varying,
    more_description character varying(255) DEFAULT ''::character varying,
    contract_sub character varying(255) DEFAULT ''::character varying,
    contract_sub_code character varying(255) DEFAULT ''::character varying,
    contact_name character varying(255) DEFAULT ''::character varying,
    telephone character varying(255) DEFAULT ''::character varying,
    home_id character varying(255) DEFAULT ''::character varying,
    road character varying(255) DEFAULT ''::character varying,
    village character varying(255) DEFAULT ''::character varying,
    fix_tambol_id character varying(255) DEFAULT ''::character varying,
    fix_amphur_id character varying(255) DEFAULT ''::character varying,
    fix_changwat_id character varying(255) DEFAULT ''::character varying,
    postcode character varying(255) DEFAULT ''::character varying,
    last_update character varying(255) DEFAULT ''::character varying,
    fix_receipt_name_format_id character varying(255),
    active character varying(255)
);


ALTER TABLE imed.payer OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: payment_voucher; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.payment_voucher (
    payment_voucher_id character varying(255) NOT NULL,
    patient_id character varying(255),
    visit_id character varying(255),
    pv_number character varying(255),
    com_number character varying(255),
    fix_visit_type_id character varying(255),
    fix_payment_voucher_type_id character varying(255),
    pay_to character varying(255),
    amount character varying(255),
    pay_eid character varying(255),
    pay_date character varying(255),
    pay_time character varying(255),
    last_print_eid character varying(255),
    last_print_date character varying(255),
    last_print_time character varying(255),
    ref_doc_id character varying(255),
    ref_doc_number character varying(255),
    max_print_time character varying(255)
);


ALTER TABLE imed.payment_voucher OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: personal_illness; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.personal_illness (
    personal_illness_id character varying(255) NOT NULL,
    patient_id character varying(255),
    illness_name character varying(255),
    note character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    icd10_code character varying(255),
    icd10_des character varying(255)
);


ALTER TABLE imed.personal_illness OWNER TO postgres;

--
-- Name: physical_examination; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.physical_examination (
    physical_examination_id character varying(255) NOT NULL,
    vital_sign_extend_id character varying(255),
    organ character varying(255),
    detail text,
    result_position character varying(255),
    is_normal character varying(255)
);


ALTER TABLE imed.physical_examination OWNER TO postgres;

--
-- Name: plan; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.plan (
    plan_id character varying(255) NOT NULL,
    plan_code character varying(255),
    prename character varying(255),
    description character varying(255),
    nsd character varying(255),
    alias_name character varying(255),
    payer_id character varying(255),
    contract_office_id character varying(255),
    base_plan_group_id character varying(255),
    fix_plan_type_id character varying(255) DEFAULT '0'::character varying,
    fix_contract_type_id character varying(255),
    inscl character varying(255),
    subtype character varying(255),
    pttype character varying(255),
    fix_0110_5_id character varying(255),
    auto_order character varying(255) DEFAULT '0'::character varying,
    inspire_date character varying(255),
    expire_date character varying(255),
    last_update character varying(255),
    plan_set_billing_group_id character varying(255),
    template_discount_id character varying(255),
    active character varying(255) DEFAULT '1'::character varying,
    check_expire character varying(255),
    is_have_other_payer character varying(255),
    fix_employee_welfare_type_id character varying(255),
    acc_code character varying(255)
);


ALTER TABLE imed.plan OWNER TO postgres;

--
-- Name: plan_auto_order; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.plan_auto_order (
    plan_auto_order_id character varying(255) NOT NULL,
    plan_id character varying(255) DEFAULT ''::character varying,
    item_id character varying(255) DEFAULT ''::character varying,
    order_day character varying(255) DEFAULT ''::character varying,
    begin_order_time character varying(255) DEFAULT ''::character varying,
    finish_order_time character varying(255) DEFAULT ''::character varying
);


ALTER TABLE imed.plan_auto_order OWNER TO postgres;

--
-- Name: plan_billing_group; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.plan_billing_group (
    plan_billing_group_id character varying(255) NOT NULL,
    plan_set_billing_group_id character varying(255),
    description character varying(255),
    base_billing_group_id character varying(255),
    priority character varying(255)
);


ALTER TABLE imed.plan_billing_group OWNER TO postgres;

--
-- Name: plan_billing_share_except; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.plan_billing_share_except (
    plan_billing_share_except_id character varying(255) NOT NULL,
    plan_id character varying(255),
    base_billing_group_id character varying(255)
);


ALTER TABLE imed.plan_billing_share_except OWNER TO postgres;

--
-- Name: plan_document; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.plan_document (
    plan_document_id character varying(255) NOT NULL,
    plan_id character varying(255) DEFAULT ''::character varying,
    description text,
    discount character varying(255) DEFAULT ''::character varying,
    fix_visit_type_id character varying(255) DEFAULT ''::character varying
);


ALTER TABLE imed.plan_document OWNER TO postgres;

--
-- Name: plan_dx_except; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.plan_dx_except (
    plan_dx_except_id character varying(255) NOT NULL,
    plan_id character varying(255) DEFAULT ''::character varying,
    dx_code character varying(255) DEFAULT ''::character varying,
    dx_description text,
    fix_visit_type_id character varying(255) DEFAULT ''::character varying
);


ALTER TABLE imed.plan_dx_except OWNER TO postgres;

--
-- Name: plan_external; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.plan_external (
    plan_external_id character varying(255) NOT NULL,
    plan_code character varying(255),
    hn character varying(255),
    pid character varying(255),
    prename character varying(255),
    firstname character varying(255),
    lastname character varying(255),
    nsd character varying(255),
    lsd character varying(255),
    card_id character varying(255),
    inspire_date character varying(255),
    expire_date character varying(255),
    main_hospital_code character varying(255),
    sub_hospital_code character varying(255),
    last_update character varying(255),
    remark character varying(255),
    employee_id character varying(255),
    credit_limit character varying(255),
    current_credit_limit character varying(255),
    active character varying(255) DEFAULT '1'::character varying,
    credit_family_limit character varying(255),
    current_family_credit character varying(255)
);


ALTER TABLE imed.plan_external OWNER TO postgres;

--
-- Name: plan_external_visit; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.plan_external_visit (
    plan_external_visit_id character varying(255) NOT NULL,
    plan_external_id character varying(255),
    visit_id character varying(255)
);


ALTER TABLE imed.plan_external_visit OWNER TO postgres;

--
-- Name: plan_item_share_limit; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.plan_item_share_limit (
    plan_item_share_limit_id character varying(255) NOT NULL,
    plan_id character varying(255),
    item_id character varying(255),
    share_limit character varying(255)
);


ALTER TABLE imed.plan_item_share_limit OWNER TO postgres;

--
-- Name: plan_payer_other; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.plan_payer_other (
    plan_payer_other_id character varying(255) NOT NULL,
    plan_id character varying(255),
    payer_id character varying(255)
);


ALTER TABLE imed.plan_payer_other OWNER TO postgres;

--
-- Name: plan_set_billing_group; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.plan_set_billing_group (
    plan_set_billing_group_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.plan_set_billing_group OWNER TO postgres;

--
-- Name: prepack_item; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.prepack_item (
    prepack_item_id character varying(255) NOT NULL,
    item_id character varying(255),
    quantity character varying(255),
    base_unit_id character varying(255),
    produce_date character varying(255),
    expire_date character varying(255),
    lot_number character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255)
);


ALTER TABLE imed.prepack_item OWNER TO postgres;

--
-- Name: prescription; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.prescription (
    prescription_id character varying(255) NOT NULL,
    visit_id character varying(255),
    patient_id character varying(255),
    pn character varying(255),
    assign_doctor_eid character varying(255),
    assign_eid character varying(255),
    assign_spid character varying(255),
    assign_date character varying(255),
    assign_time character varying(255),
    receive_spid character varying(255),
    assign_order_status character varying(255),
    execute_eid character varying(255),
    dispense_eid character varying(255),
    complete_date character varying(255),
    complete_time character varying(255),
    take_home character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    proxy_status character varying(255),
    proxy_user character varying(255),
    assigned_nurse_eid character varying(255),
    is_stat_drug_use character varying(255)
);


ALTER TABLE imed.prescription OWNER TO postgres;

--
-- Name: previous_drug_used; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.previous_drug_used (
    previous_drug_used_id character varying(255) NOT NULL,
    patient_id character varying(255),
    drug_name character varying(255),
    note character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255)
);


ALTER TABLE imed.previous_drug_used OWNER TO postgres;

--
-- Name: previous_test; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.previous_test (
    previous_test_id character varying(255) NOT NULL,
    patient_id character varying(255),
    name character varying(255),
    note character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255)
);


ALTER TABLE imed.previous_test OWNER TO postgres;

--
-- Name: progress_note; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.progress_note (
    progress_note_id character varying(255) NOT NULL,
    admit_id character varying(255),
    note text,
    doctor_eid character varying(255),
    record_date character varying(255),
    record_time character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255)
);


ALTER TABLE imed.progress_note OWNER TO postgres;

--
-- Name: queue_mgnt; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.queue_mgnt (
    queue_mgnt_id character varying(255) NOT NULL,
    base_service_point_id character varying(255),
    employee_id character varying(255),
    max_queue_number_opd character varying(255),
    max_queue_number_ipd character varying(255)
);


ALTER TABLE imed.queue_mgnt OWNER TO postgres;

--
-- Name: receipt; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.receipt (
    receipt_id character varying(255) NOT NULL,
    patient_id character varying(255),
    visit_id character varying(255),
    receipt_number character varying(255),
    com_number character varying(255),
    fax_claim_number character varying(255),
    office_number character varying(255),
    head_office_number character varying(255),
    document_number character varying(255),
    debt_number character varying(255),
    document_date character varying(255),
    diagnosis character varying(255),
    fix_visit_type_id character varying(255),
    fix_receipt_type_id character varying(255),
    fix_receipt_status_id character varying(255),
    receive_eid character varying(255),
    receive_date character varying(255),
    receive_time character varying(255),
    last_print_eid character varying(255),
    last_print_date character varying(255),
    last_print_time character varying(255),
    prepare_cancel_eid character varying(255),
    prepare_cancel_date character varying(255),
    prepare_cancel_time character varying(255),
    cancel_eid character varying(255),
    cancel_date character varying(255),
    cancel_time character varying(255),
    return_eid character varying(255),
    return_date character varying(255),
    return_time character varying(255),
    max_print_times character varying(255),
    discharge character varying(255),
    fix_payment_type_id character varying(255),
    sum_paid_receipt_bill_grp character varying(255),
    discount character varying(255),
    special_discount character varying(255),
    decimal_discount character varying(255),
    expense character varying(255),
    cost character varying(255),
    paid character varying(255),
    pay character varying(255),
    change character varying(255),
    deposit character varying(255),
    cut_from_receipt_id character varying(255),
    template_discount_id character varying(255),
    template_discount_name character varying(255),
    base_deliver_document_id character varying(255),
    base_discount_reason_id character varying(255),
    discount_sub_reason_id character varying(255),
    special_discount_reason_id character varying(255),
    special_disc_sub_reason_id character varying(255),
    fix_billing_group_type_id character varying(255),
    begin_date character varying(255),
    begin_time character varying(255),
    end_date character varying(255),
    end_time character varying(255),
    num_date character varying(255),
    plan_id character varying(255),
    calculate_times_receipt character varying(255),
    sp_id character varying(255),
    fix_language_type_id character varying(255),
    note character varying(255),
    is_print_over_cost character varying(255),
    debt_change character varying(255),
    base_receipt_category_id character varying(255),
    invoice_status character varying(255),
    invoice_remain character varying(255),
    receipt_name character varying(255),
    base_cancel_receipt_reason_id character varying(255),
    tax_amount character varying(255),
    bank_fee_amount character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    pay_invoice_discount character varying(255),
    payer_id character varying(255),
    is_receipt_voucher character varying(255),
    acc_code character varying(255)
);


ALTER TABLE imed.receipt OWNER TO postgres;

--
-- Name: receipt_attach; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.receipt_attach (
    receipt_attach_id character varying(255) NOT NULL,
    receipt_id character varying(255),
    fix_billing_group_type_id character varying(255),
    cost character varying(255)
);


ALTER TABLE imed.receipt_attach OWNER TO postgres;

--
-- Name: receipt_attach_bill_grp; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.receipt_attach_bill_grp (
    receipt_attach_bill_grp_id character varying(255) NOT NULL,
    receipt_attach_id character varying(255),
    base_billing_group_id character varying(255),
    cost character varying(255)
);


ALTER TABLE imed.receipt_attach_bill_grp OWNER TO postgres;

--
-- Name: receipt_billing_group; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.receipt_billing_group (
    receipt_billing_group_id character varying(255) NOT NULL,
    receipt_id character varying(255),
    base_billing_group_id character varying(255),
    patient_id character varying(255),
    visit_id character varying(255),
    expense character varying(255),
    cost character varying(255),
    discount_percent character varying(255),
    discount character varying(255),
    paid character varying(255),
    debt_change character varying(255),
    ipd_accommodation character varying(255),
    payer_share character varying(255),
    patient_share character varying(255),
    payer_share_discount character varying(255),
    patient_share_discount character varying(255),
    payer_share_paid character varying(255),
    patient_share_paid character varying(255)
);


ALTER TABLE imed.receipt_billing_group OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: receipt_order; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.receipt_order (
    receipt_order_id character varying(255) NOT NULL,
    item_id character varying(255),
    receipt_id character varying(255),
    receipt_billing_group_id character varying(255),
    unit_order_cost character varying(255),
    quantity character varying(255),
    cost character varying(255),
    payer_share character varying(255),
    patient_share character varying(255),
    payer_share_unit character varying(255),
    patient_share_unit character varying(255)
);


ALTER TABLE imed.receipt_order OWNER TO postgres;

--
-- Name: receipt_order_detail; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.receipt_order_detail (
    receipt_order_detail_id character varying(255) NOT NULL,
    receipt_order_id character varying(255),
    order_item_id character varying(255)
);


ALTER TABLE imed.receipt_order_detail OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: receipt_paid_method; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.receipt_paid_method (
    receipt_paid_method_id character varying(255) NOT NULL,
    receipt_id character varying(255),
    base_paid_method_id character varying(255),
    paid character varying(255) DEFAULT '0'::character varying,
    reference_number character varying(255),
    transfer_date character varying(255),
    approve_number character varying(255),
    base_credit_card_id character varying(255),
    charge_percent character varying(255),
    charge character varying(255),
    total_paid character varying(255),
    bank_code character varying(255),
    bank_branch_code character varying(255),
    is_multi_receipt character varying(255),
    value character varying(255),
    due_date character varying(255),
    comments character varying(255)
);


ALTER TABLE imed.receipt_paid_method OWNER TO postgres;

--
-- Name: receipt_pay_invoice; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.receipt_pay_invoice (
    receipt_pay_invoice_id character varying(255) NOT NULL,
    pay_receipt_id character varying(255),
    invoice_receipt_id character varying(255)
);


ALTER TABLE imed.receipt_pay_invoice OWNER TO postgres;

--
-- Name: receipt_print_group; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.receipt_print_group (
    receipt_print_group_id character varying(255) NOT NULL,
    receipt_id character varying(255),
    print_code character varying(255),
    print_description character varying(255),
    expense character varying(255),
    cost character varying(255),
    discount_percent character varying(255),
    discount character varying(255),
    paid character varying(255),
    print_position character varying(255),
    ipd_accommodation character varying(255),
    payer_share character varying(255),
    patient_share character varying(255),
    payer_share_discount character varying(255),
    patient_share_discount character varying(255),
    payer_share_paid character varying(255),
    patient_share_paid character varying(255)
);


ALTER TABLE imed.receipt_print_group OWNER TO postgres;

--
-- Name: receipt_print_history; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.receipt_print_history (
    receipt_print_history_id character varying(255) NOT NULL,
    receipt_id character varying(255),
    receipt_print_eid character varying(255),
    receipt_print_date character varying(255),
    receipt_print_time character varying(255),
    print_times character varying(255),
    note text
);


ALTER TABLE imed.receipt_print_history OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: receipt_revenue_order; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.receipt_revenue_order (
    receipt_revenue_order_id character varying(255) NOT NULL,
    receipt_id character varying(255),
    order_item_id character varying(255),
    item_id character varying(255),
    quantity character varying(255),
    unit_price_sale character varying(255),
    cost character varying(255),
    discount character varying(255)
);


ALTER TABLE imed.receipt_revenue_order OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: refer_in; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.refer_in (
    refer_in_id character varying(255) NOT NULL,
    base_office_id character varying(255),
    visit_id character varying(255),
    refer_eid character varying(255),
    refer_date character varying(255),
    refer_time character varying(255),
    refer_type character varying(255),
    note text,
    doctor_eid character varying(255),
    refer_number character varying(255),
    illness_history text,
    current_illness text,
    lab_result text,
    treatment text,
    diagnosis character varying(255),
    reply_eid character varying(255),
    reply_date character varying(255),
    reply_time character varying(255),
    reply_diagnosis character varying(255),
    reply_treatment text,
    reply_lab_result text,
    reply_request_treatment text,
    reply_note text,
    is_finish character varying(255),
    doctor_eid_name character varying(255),
    patient_id character varying(255)
);


ALTER TABLE imed.refer_in OWNER TO postgres;

--
-- Name: refer_out; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.refer_out (
    refer_out_id character varying(255) NOT NULL,
    base_office_id character varying(255),
    visit_id character varying(255),
    refer_eid character varying(255),
    refer_date character varying(255),
    refer_time character varying(255),
    refer_type character varying(255),
    note text,
    refer_number character varying(255),
    doctor_eid character varying(255),
    illness_history text,
    current_illness text,
    lab_result text,
    treatment text,
    diagnosis character varying(255),
    reply_eid character varying(255),
    reply_date character varying(255),
    reply_time character varying(255),
    reply_diagnosis character varying(255),
    reply_treatment text,
    reply_lab_result text,
    reply_request_treatment text,
    reply_note text,
    is_finish character varying(255),
    reply_name character varying(255),
    patient_id character varying(255)
);


ALTER TABLE imed.refer_out OWNER TO postgres;

--
-- Name: refer_out_reason; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.refer_out_reason (
    refer_out_reason_id character varying(255) NOT NULL,
    refer_out_id character varying(255),
    base_refer_out_reason_id character varying(255)
);


ALTER TABLE imed.refer_out_reason OWNER TO postgres;

--
-- Name: report_drug_interaction; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.report_drug_interaction (
    report_drug_interaction_id character varying(255) NOT NULL,
    visit_id character varying(255),
    item_id1 character varying(255),
    item_id2 character varying(255),
    drug1 character varying(255),
    drug2 character varying(255),
    significance character varying(255),
    onset character varying(255),
    severity character varying(255),
    document character varying(255),
    effects character varying(255),
    mechanism character varying(255),
    management character varying(255),
    ref character varying(255),
    page character varying(255)
);


ALTER TABLE imed.report_drug_interaction OWNER TO postgres;

--
-- Name: report_drug_pregnancy; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.report_drug_pregnancy (
    report_drug_pregnancy_id character varying(255) NOT NULL,
    visit_id character varying(255),
    order_item_id character varying(255),
    effect_level character varying(255),
    comments character varying(255)
);


ALTER TABLE imed.report_drug_pregnancy OWNER TO postgres;

--
-- Name: report_group; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.report_group (
    report_group_id character varying(255) NOT NULL,
    description character varying(255),
    fix_report_id character varying(255)
);


ALTER TABLE imed.report_group OWNER TO postgres;

--
-- Name: report_group_billing; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.report_group_billing (
    report_group_billing_id character varying(255) NOT NULL,
    report_group_id character varying(255),
    base_billing_group_id character varying(255)
);


ALTER TABLE imed.report_group_billing OWNER TO postgres;

--
-- Name: report_group_item; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.report_group_item (
    report_group_item_id character varying(255) NOT NULL,
    report_group_id character varying(255),
    item_id character varying(255)
);


ALTER TABLE imed.report_group_item OWNER TO postgres;

--
-- Name: report_group_plan; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.report_group_plan (
    report_group_plan_id character varying(255) NOT NULL,
    report_gruop_id character varying(255),
    plan_id character varying(255)
);


ALTER TABLE imed.report_group_plan OWNER TO postgres;

--
-- Name: report_icd10_group_accident; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.report_icd10_group_accident (
    report_icd10_group_accident_id integer NOT NULL,
    group_type character varying(255),
    group_name character varying(255),
    icd10_code3 character varying(255)
);


ALTER TABLE imed.report_icd10_group_accident OWNER TO postgres;

--
-- Name: report_icd10_group_accident_report_icd10_group_accident_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE imed.report_icd10_group_accident_report_icd10_group_accident_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE imed.report_icd10_group_accident_report_icd10_group_accident_id_seq OWNER TO postgres;

--
-- Name: report_range_time; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.report_range_time (
    report_range_time character varying(255) NOT NULL,
    description character varying(255),
    time_from character varying(255),
    time_to character varying(255)
);


ALTER TABLE imed.report_range_time OWNER TO postgres;

--
-- Name: reportwl_reportwl_key_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE imed.reportwl_reportwl_key_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE imed.reportwl_reportwl_key_seq OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: reportwl; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.reportwl (
    reportwl_key integer DEFAULT nextval('reportwl_reportwl_key_seq'::regclass) NOT NULL,
    trigger_dttm character varying(14),
    replica_dttm character varying(14),
    exam_id character varying(32) NOT NULL,
    patient_id character varying(32) NOT NULL,
    report_stat character varying(32) NOT NULL,
    creator_id character varying(255),
    creator_name character varying(255),
    create_dttm character varying(255),
    dictator_id character varying(255),
    dictator_name character varying(255),
    dictate_dttm character varying(255),
    approver_id character varying(32) NOT NULL,
    approver_name character varying(32) NOT NULL,
    approve_dttm character varying(14) NOT NULL,
    reviser_id character varying(255),
    reviser_name character varying(255),
    revise_dttm character varying(255),
    report_type character varying(255),
    report_text text,
    conclusion text
);


ALTER TABLE imed.reportwl OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: return_drug; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.return_drug (
    return_drug_id character varying(255) NOT NULL,
    visit_id character varying(255),
    patient_id character varying(255),
    item_id character varying(255),
    return_quantity character varying(255),
    return_note character varying(255),
    return_drug_allergy character varying(255),
    return_eid character varying(255),
    return_spid character varying(255),
    return_date character varying(255),
    return_time character varying(255),
    receive_quantity character varying(255),
    approve_quantity character varying(255),
    lot_no character varying(255),
    receive_note character varying(255),
    receive_eid character varying(255),
    receive_spid character varying(255),
    receive_date character varying(255),
    receive_time character varying(255),
    order_item_id character varying(255),
    dispense_unit_price character varying(255),
    dispense_visit_id character varying(255),
    lot_number_id character varying(255),
    return_drug_no character varying(255)
);


ALTER TABLE imed.return_drug OWNER TO postgres;

--
-- Name: risk_factor; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.risk_factor (
    risk_factor_id character varying(255) NOT NULL,
    patient_id character varying(255),
    name character varying(255),
    note character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255)
);


ALTER TABLE imed.risk_factor OWNER TO postgres;

--
-- Name: seq_an; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.seq_an (
    seq_an_id character varying(254) NOT NULL,
    value character varying(254)
);


ALTER TABLE imed.seq_an OWNER TO postgres;

--
-- Name: seq_axn; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.seq_axn (
    seq_axn_id character varying(254) NOT NULL,
    value character varying(254)
);


ALTER TABLE imed.seq_axn OWNER TO postgres;

--
-- Name: seq_en; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.seq_en (
    seq_en_id character varying(255) DEFAULT 1 NOT NULL,
    value character varying(255) DEFAULT 0 NOT NULL
);


ALTER TABLE imed.seq_en OWNER TO postgres;

--
-- Name: seq_finger_print; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.seq_finger_print (
    seq_finger_print_id character varying(255) NOT NULL,
    value character varying(10)
);


ALTER TABLE imed.seq_finger_print OWNER TO postgres;

--
-- Name: seq_hn; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.seq_hn (
    seq_hn_id character varying(255) DEFAULT 1 NOT NULL,
    value character varying(255) DEFAULT 0 NOT NULL
);


ALTER TABLE imed.seq_hn OWNER TO postgres;

--
-- Name: seq_ln; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.seq_ln (
    seq_ln_id character varying(254) NOT NULL,
    value character varying(254)
);


ALTER TABLE imed.seq_ln OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: seq_medical_certificate; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.seq_medical_certificate (
    seq_medical_certificate_id character varying(255) NOT NULL,
    value character varying(255)
);


ALTER TABLE imed.seq_medical_certificate OWNER TO postgres;

--
-- Name: seq_pacs; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.seq_pacs (
    seq_pacs_id character varying(255) NOT NULL,
    value character varying(255)
);


ALTER TABLE imed.seq_pacs OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: seq_patient_number; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.seq_patient_number (
    seq_patient_number_id character varying(255) NOT NULL,
    value character varying(255)
);


ALTER TABLE imed.seq_patient_number OWNER TO postgres;

--
-- Name: seq_pn; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.seq_pn (
    seq_pn_id character varying(255) DEFAULT 1 NOT NULL,
    value character varying(255) DEFAULT 0 NOT NULL
);


ALTER TABLE imed.seq_pn OWNER TO postgres;

--
-- Name: seq_receipt_number; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.seq_receipt_number (
    seq_receipt_number_id character varying(255) NOT NULL,
    com_number character varying(255) DEFAULT ''::character varying NOT NULL,
    ip_address character varying(255) DEFAULT ''::character varying NOT NULL,
    value character varying(255) DEFAULT 0 NOT NULL,
    last_print_maximum character varying(255) DEFAULT 0,
    last_print_minimum character varying(255) DEFAULT 0,
    fix_visit_type_id character varying(255) DEFAULT ''::character varying,
    fix_receipt_type_id character varying(255) DEFAULT ''::character varying,
    base_receipt_category_id character varying(255)
);


ALTER TABLE imed.seq_receipt_number OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: seq_refer_out_no; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.seq_refer_out_no (
    seq_refer_out_no_id character varying(255) NOT NULL,
    value character varying(255)
);


ALTER TABLE imed.seq_refer_out_no OWNER TO postgres;

--
-- Name: seq_return_drug_no; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.seq_return_drug_no (
    seq_return_drug_no_id character varying(255) NOT NULL,
    value character varying(255)
);


ALTER TABLE imed.seq_return_drug_no OWNER TO postgres;

--
-- Name: seq_un; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.seq_un (
    seq_un_id character varying(255) DEFAULT 1 NOT NULL,
    value character varying(255) DEFAULT 0 NOT NULL
);


ALTER TABLE imed.seq_un OWNER TO postgres;

--
-- Name: seq_vn; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.seq_vn (
    seq_vn_id character varying(255) DEFAULT 1 NOT NULL,
    value character varying(255) DEFAULT 0 NOT NULL
);


ALTER TABLE imed.seq_vn OWNER TO postgres;

--
-- Name: seq_xn; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.seq_xn (
    seq_xn_id character varying(255) DEFAULT 1 NOT NULL,
    value character varying(255) DEFAULT 0 NOT NULL
);


ALTER TABLE imed.seq_xn OWNER TO postgres;

--
-- Name: service_point_schedule; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.service_point_schedule (
    service_point_schedule_id character varying(255) NOT NULL,
    base_service_point_id character varying(255),
    fix_day_of_week character varying(255),
    fix_time_of_day character varying(255),
    open_service character varying(255)
);


ALTER TABLE imed.service_point_schedule OWNER TO postgres;

--
-- Name: service_time_stamp; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.service_time_stamp (
    service_time_stamp_id character varying(255) NOT NULL,
    visit_id character varying(255),
    spid character varying(255),
    doctor_eid character varying(255),
    assign_date character varying(255),
    assign_time character varying(255),
    begin_date character varying(255),
    begin_time character varying(255),
    finish_date character varying(255),
    finish_time character varying(255)
);


ALTER TABLE imed.service_time_stamp OWNER TO postgres;

--
-- Name: special_nurse_care; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.special_nurse_care (
    special_nurse_care_id character varying(255) NOT NULL,
    visit_id character varying(255),
    special_nurse_care_date character varying(255),
    special_nurse_care_time character varying(255),
    nurse_id character varying(255),
    base_service_point_id character varying(255)
);


ALTER TABLE imed.special_nurse_care OWNER TO postgres;

--
-- Name: special_nurse_care_detail; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.special_nurse_care_detail (
    nurse_care_detail_id character varying(255) NOT NULL,
    special_nurse_care_id character varying(255),
    base_special_nurse_care_id character varying(255),
    fix_special_nurse_care_detail character varying(255)
);


ALTER TABLE imed.special_nurse_care_detail OWNER TO postgres;

--
-- Name: sql_except; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.sql_except (
    sql_except_id character varying(255) NOT NULL,
    sql_statement_id character varying(255),
    sql_group_role_id character varying(255)
);


ALTER TABLE imed.sql_except OWNER TO postgres;

--
-- Name: sql_group_role; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.sql_group_role (
    sql_group_role_id character varying(255) NOT NULL,
    base_sql_group_id character varying(255),
    employee_id character varying(255)
);


ALTER TABLE imed.sql_group_role OWNER TO postgres;

--
-- Name: sql_statement; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.sql_statement (
    sql_statement_id character varying(255) NOT NULL,
    sql_statement_name character varying(255),
    sql_statement_detail character varying(255),
    base_sql_group_id character varying(255)
);


ALTER TABLE imed.sql_statement OWNER TO postgres;

--
-- Name: visit; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.visit (
    visit_id character varying(255) NOT NULL,
    patient_id character varying(255),
    old_patient_id character varying(255),
    hn character varying(255),
    vn character varying(255),
    an character varying(255),
    visit_eid character varying(255),
    visit_spid character varying(255),
    visit_date character varying(255),
    visit_time character varying(255),
    base_tariff_id character varying(255),
    special_care_note character varying(255),
    fix_visit_type_id character varying(255),
    pregnancy character varying(255),
    pregnancy_interval character varying(255),
    fix_pregnancy_risk_category_id character varying(255),
    pregnancy_risk_comment character varying(255),
    drug_in_lactation character varying(255),
    financial_discharge character varying(255),
    financial_discharge_eid character varying(255),
    financial_discharge_date character varying(255),
    financial_discharge_time character varying(255),
    reverse_discharge_visit character varying(255),
    doctor_discharge character varying(255),
    doctor_discharge_date character varying(255),
    doctor_discharge_time character varying(255),
    opd_card_no character varying(255),
    fix_new_in_year_id character varying(255),
    new_patient character varying(255),
    patient_age character varying(255),
    ignore_drug_allergy character varying(255),
    from_appointment character varying(255),
    admit_times character varying(255),
    proxy_status character varying(255),
    proxy_date character varying(255),
    proxy_time character varying(255),
    last_location_spid character varying(255),
    location_spid character varying(255),
    last_operate_eid character varying(255),
    operate_eid character varying(255),
    assign_lab_status character varying(255),
    assign_xray_status character varying(255),
    assign_pharmacy_status character varying(255),
    base_patient_type_id character varying(255),
    max_calculate_times character varying(255),
    last_calculate_date character varying(255),
    last_calculate_time character varying(255),
    move_visit_eid character varying(255),
    move_visit_date character varying(255),
    move_visit_time character varying(255),
    is_no_pay character varying(255),
    patient_file_borrow_id character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    active character varying(255),
    fix_coming_type character varying(255),
    fix_emergency_type_id character varying(255),
    is_lock_order character varying(255),
    lock_order_spid character varying(255),
    is_abnormal_physical_exam character varying(255),
    base_patient_group_id character varying(255) DEFAULT ''::character varying,
    base_patient_unit_id character varying(255) DEFAULT '00'::character varying,
    is_external_register character varying(255),
    is_observe character varying(255),
    doc_scan_path character varying(255),
    doc_scan_seq character varying(255) DEFAULT '0'::character varying,
    patient_number character varying(255),
    base_patient_number_type character varying(255),
    observe_date character varying(255),
    observe_time character varying(255),
    doctor_admit character varying(255),
    doctor_admit_eid character varying(255),
    doctor_admit_note text,
    fix_visit_status character varying(255),
    queue_status_date character varying(255),
    queue_status_time character varying(255),
    base_site_branch_id character varying(255)
);


ALTER TABLE imed.visit OWNER TO postgres;

--
-- Name: visit_payment; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.visit_payment (
    visit_payment_id character varying(255) NOT NULL,
    visit_id character varying(255),
    plan_id character varying(255),
    plan_code character varying(255),
    fix_contract_type_id character varying(255),
    base_plan_group_id character varying(255),
    card_id character varying(255),
    inspire_date character varying(255),
    expire_date character varying(255),
    main_hospital_code character varying(255),
    sub_hospital_code character varying(255),
    subtype character varying(255),
    priority character varying(255),
    credit_limit character varying(255),
    fix_credit_type_id character varying(255),
    note character varying(255),
    visit_credit_limit character varying(255),
    current_credit_limit character varying(255),
    start_payment_id character varying(255),
    cont_credit_active character varying(255),
    plan_external_id character varying(255),
    employee_welfare_eid character varying(255),
    agent_name character varying(255),
    agent_lastname character varying(255),
    payer_id character varying(255),
    reference_document character varying(255),
    inactive character varying(255),
    inactive_eid character varying(255),
    inactive_reason text,
    inactive_date character varying(255),
    inactive_time character varying(255)
);


ALTER TABLE imed.visit_payment OWNER TO postgres;

--
-- Name: stock; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock (
    stock_id character varying(255) NOT NULL,
    stock_name character varying(255),
    fix_stock_type_id character varying(255),
    fix_stock_update_type_id character varying(255),
    limit_per_year character varying(255),
    limit_summary character varying(255),
    limit_order_value character varying(255),
    seq_request_number character varying(255),
    seq_transfer_number character varying(255),
    seq_return_number character varying(255),
    seq_po_number character varying(255),
    is_to_period character varying(255),
    seq_exchange_number character varying(255),
    seq_print_receive_order_number character varying(255),
    seq_pr_number character varying(255),
    seq_dispense_number character varying(255),
    seq_disp_to_produce_number character varying(255),
    seq_produce_number character varying(255),
    seq_use_in_stock_number character varying(255),
    default_stock_request_drug character varying(255),
    default_stock_request_supply character varying(255),
    seq_receive_other_number character varying(255),
    seq_adjust_number character varying(255),
    seq_prepack_item_number character varying(255),
    seq_cssd_machine character varying(255),
    seq_merge_po_number character varying(255),
    seq_borrow_number character varying(255),
    seq_cssd_produce character varying(255),
    is_to_cal_avg character varying(255) DEFAULT '1'::character varying,
    default_in_use_department_id character varying(255),
    base_site_branch_id character varying(255)
);


ALTER TABLE imed.stock OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: stock_adjust; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock_adjust (
    stock_adjust_id character varying(255) NOT NULL,
    stock_adjust_number character varying(255),
    stock_id character varying(255),
    item_id character varying(255),
    lot_number character varying(255),
    fix_stock_reason_adjust character varying(255),
    fix_stock_method_id character varying(255),
    qty character varying(255),
    small_unit_id character varying(255),
    distributor_id character varying(255),
    invoice_number character varying(255),
    expire_date character varying(255),
    note text,
    req_adjust_date_time character varying(255),
    req_adjust_eid character varying(255),
    fix_stock_adjust_status character varying(255),
    stock_mgnt_id character varying(255)
);


ALTER TABLE imed.stock_adjust OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: stock_auth; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock_auth (
    stock_auth_id character varying(255) NOT NULL,
    stock_id character varying(255),
    employee_id character varying(255),
    template_name character varying(255),
    stock_module character varying(255),
    manage_item character varying(255),
    dispense_patient character varying(255),
    receive_item character varying(255),
    receive_by_request character varying(255),
    receive_by_order character varying(255),
    receive_by_manufact character varying(255),
    dispense_stock character varying(255),
    make_request character varying(255),
    make_order character varying(255),
    make_exchange character varying(255),
    make_request_order character varying(255),
    make_request_produce character varying(255),
    adjust_qty character varying(255),
    prepack_item character varying(255),
    merge_po character varying(255),
    send_cssd character varying(255),
    dispense_cssd character varying(255),
    receive_cssd character varying(255)
);


ALTER TABLE imed.stock_auth OWNER TO postgres;

--
-- Name: stock_auto_receive; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock_auto_receive (
    stock_auto_receive_id character varying(255) NOT NULL,
    disp_stock_id character varying(255),
    recv_stock_id character varying(255),
    auto_receive character varying(255)
);


ALTER TABLE imed.stock_auto_receive OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: stock_base_exchange_note; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock_base_exchange_note (
    stock_base_exchange_note_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.stock_base_exchange_note OWNER TO postgres;

--
-- Name: stock_base_reason_of_request_order; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock_base_reason_of_request_order (
    stock_base_reason_of_request_order_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.stock_base_reason_of_request_order OWNER TO postgres;

--
-- Name: stock_budget; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock_budget (
    stock_budget_id character varying(255) NOT NULL,
    description character varying(255),
    fix_budget_type_id character varying(255)
);


ALTER TABLE imed.stock_budget OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: stock_card; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock_card (
    stock_card_id character varying(255) NOT NULL,
    stock_mgnt_id character varying(255),
    fix_stock_method_id character varying(255),
    request_or_order_id character varying(255),
    invoice_number character varying(255),
    in_id character varying(255),
    out_id character varying(255),
    manufacturer_id character varying(255),
    stock_id character varying(255),
    item_id character varying(255),
    lot_number character varying(255),
    update_date character varying(255),
    update_time character varying(255),
    update_eid character varying(255),
    previous_qty character varying(255),
    previous_qty_lot character varying(255),
    max_qty character varying(255),
    min_qty character varying(255),
    qty character varying(255),
    free_qty character varying(255),
    update_qty character varying(255),
    update_qty_lot character varying(255),
    is_arrear character varying(255) DEFAULT '0'::character varying,
    arrear_qty character varying(255) DEFAULT '0'::character varying,
    small_unit_price_cost character varying(255),
    small_unit_price_sale character varying(255),
    small_unit_id character varying(255),
    big_unit_price_cost character varying(255),
    cost_purchase_no_discount character varying(255),
    cost_purchase character varying(255),
    discount character varying(255),
    discount_percent character varying(255),
    big_unit_id character varying(255),
    unit_rate character varying(255),
    mid_unit_rate character varying(255),
    fiscal_year character varying(255),
    comments character varying(255),
    is_transfer character varying(255),
    transfer_date_from character varying(255),
    transfer_time_from character varying(255),
    transfer_date_to character varying(255),
    transfer_time_to character varying(255),
    have_lot_analyse character varying(255),
    have_fda character varying(255),
    fda_number character varying(255),
    have_gmp character varying(255),
    gmp_number character varying(255),
    return_befor_expire_day character varying(255),
    expire_date character varying(255),
    receive_date character varying(255),
    receive_time character varying(255),
    receive_order_number character varying(255),
    update_date_time character varying(255),
    order_item_id character varying(255),
    bill_number character varying(255),
    book_number character varying(255),
    stock_dispense_number character varying(255),
    avg_unit_price_cost character varying(255),
    fix_stock_reason_adjust character varying(255),
    produce_date character varying(255),
    range_to_expire character varying(255),
    stock_exchange_id character varying(255),
    stock_request_produce_id character varying(255),
    loss_qty character varying(255),
    produce_eid character varying(255),
    stock_cssd_machine_id character varying(255),
    is_merge_po character varying(255),
    seq_machine character varying(255),
    pr_number character varying(255),
    blue_slip_number character varying(255),
    smu character varying(255),
    financial_category character varying(255),
    fund_name character varying(255),
    financial_number character varying(255),
    lot_number_id character varying(255),
    payment_date character varying(255),
    payment_time character varying(255),
    item_trade_name character varying(255),
    total_discount_per_item character varying(255),
    total_discount character varying(255)
);


ALTER TABLE imed.stock_card OWNER TO postgres;

--
-- Name: stock_chk_on_hand; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock_chk_on_hand (
    stock_chk_on_hand_id character varying(255) NOT NULL,
    stock_id character varying(255),
    chk_stock_id character varying(255)
);


ALTER TABLE imed.stock_chk_on_hand OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: stock_cssd; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock_cssd (
    stock_cssd_id character varying(255) NOT NULL,
    disp_stock_id character varying(255),
    recv_stock_id character varying(255),
    document_number character varying(255),
    item_id character varying(255),
    fix_stock_method_id character varying(255),
    stock_sterile_method_id character varying(255),
    lot_number character varying(255),
    qty character varying(255),
    request_eid character varying(255),
    request_date character varying(255),
    request_time character varying(255),
    dispense_eid character varying(255),
    dispense_date character varying(255),
    dispense_time character varying(255),
    receive_eid character varying(255),
    receive_date character varying(255),
    receive_time character varying(255),
    update_eid character varying(255),
    update_date character varying(255),
    update_time character varying(255),
    fix_stock_cssd_status_id character varying(255),
    tmp_produce_date character varying(255),
    tmp_range_to_expire character varying(255),
    tmp_expire_date character varying(255),
    tmp_lot_number character varying(255),
    tmp_qty character varying(255),
    tmp_arrear_qty character varying(255),
    tmp_loss_qty character varying(255),
    tmp_note character varying(255),
    tmp_produce_eid character varying(255),
    tmp_stock_cssd_machine_id character varying(255),
    tmp_seq_machine character varying(255),
    tmp_sticker_qty character varying(255),
    is_produce character varying(255),
    note character varying(255),
    tmp_stock_mgnt_id character varying(255)
);


ALTER TABLE imed.stock_cssd OWNER TO postgres;

--
-- Name: stock_cssd_machine; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock_cssd_machine (
    stock_cssd_machine_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.stock_cssd_machine OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: stock_cssd_produce; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock_cssd_produce (
    stock_cssd_produce_id character varying(255) NOT NULL,
    stock_id character varying(255),
    stock_cssd_produce_number character varying(255),
    item_id character varying(255),
    produce_date character varying(255),
    produce_time character varying(255),
    expire_date character varying(255),
    range_to_expire character varying(255),
    lot_number character varying(255),
    note character varying(255),
    produce_eid character varying(255),
    stock_cssd_machine_id character varying(255),
    seq_machine character varying(255),
    qty character varying(255),
    sticker_qty character varying(255),
    stock_sterile_method_id character varying(255),
    fix_stock_method_id character varying(255),
    stock_cssd_produce_status character varying(255),
    document_date character varying(255),
    document_time character varying(255),
    document_eid character varying(255)
);


ALTER TABLE imed.stock_cssd_produce OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: stock_cssd_sub_item; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock_cssd_sub_item (
    stock_cssd_sub_item_id character varying(255) NOT NULL,
    stock_cssd_id character varying(255),
    item_id character varying(255),
    qty character varying(255)
);


ALTER TABLE imed.stock_cssd_sub_item OWNER TO postgres;

--
-- Name: stock_custom_po; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock_custom_po (
    stock_custom_po_id character varying(255) NOT NULL,
    stock_id character varying(255),
    custom_po_number character varying(255),
    custom_po_eid character varying(255),
    custom_po_date character varying(255),
    custom_po_time character varying(255),
    stock_budget_id character varying(255)
);


ALTER TABLE imed.stock_custom_po OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: stock_department; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock_department (
    stock_department_id character varying(255) NOT NULL,
    department_name character varying(255),
    stock_id character varying(255)
);


ALTER TABLE imed.stock_department OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: stock_dispense; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock_dispense (
    stock_dispense_id character varying(255) NOT NULL,
    stock_request_id character varying(255),
    stock_id character varying(255),
    request_number character varying(255),
    dispense_number character varying(255),
    item_id character varying(255),
    lot_number character varying(255),
    expire_date character varying(255),
    qty character varying(255),
    stock_mgnt_id character varying(255)
);


ALTER TABLE imed.stock_dispense OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: stock_dispense_other_item; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock_dispense_other_item (
    stock_dispense_other_item_id character varying(255) NOT NULL,
    item_id character varying(255),
    dispense_item_id character varying(255),
    stock_id character varying(255),
    unit_price_cost character varying(255)
);


ALTER TABLE imed.stock_dispense_other_item OWNER TO postgres;

--
-- Name: stock_exchange; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock_exchange (
    stock_exchange_id character varying(255) NOT NULL,
    exchange_from_stock_id character varying(255),
    exchange_to_distributor_id character varying(255),
    exchange_number character varying(255),
    exchange_eid character varying(255),
    exchange_date character varying(255),
    exchange_time character varying(255),
    item_id character varying(255),
    lot_number character varying(255),
    exchange_qty character varying(255),
    exchange_note character varying(255),
    fix_stock_exchange_status_id character varying(255),
    gen_exchange_number_date character varying(255),
    gen_exchange_number_time character varying(255),
    invoice_number character varying(255),
    stock_mgnt_id character varying(255)
);


ALTER TABLE imed.stock_exchange OWNER TO postgres;

--
-- Name: stock_exchange_detail; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock_exchange_detail (
    stock_exchange_detail_id character varying(255) NOT NULL,
    stock_exchange_id character varying(255),
    item_id character varying(255),
    lot_number character varying(255),
    expire_date character varying(255),
    big_unit_id character varying(255),
    small_unit_id character varying(255),
    mid_unit_rate character varying(255),
    unit_rate character varying(255),
    qty character varying(255),
    comment character varying(255),
    old_unit_price_cost character varying(255),
    new_unit_price_cost character varying(255)
);


ALTER TABLE imed.stock_exchange_detail OWNER TO postgres;

--
-- Name: stock_free_item; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock_free_item (
    stock_free_item_id character varying(255) NOT NULL,
    stock_order_id character varying(255),
    item_id character varying(255),
    small_unit_id character varying(255),
    unit_rate character varying(255),
    mid_unit_rate character varying(255),
    big_unit_id character varying(255),
    free_qty character varying(255),
    fix_stock_free_item_method_id character varying(255),
    fix_stock_free_item_status_id character varying(255),
    comments character varying(255),
    stock_card_id character varying(255)
);


ALTER TABLE imed.stock_free_item OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: stock_item_set; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock_item_set (
    stock_item_set_id character varying(255) NOT NULL,
    root_item_id character varying(255),
    item_id character varying(255),
    qty character varying(255)
);


ALTER TABLE imed.stock_item_set OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: stock_item_trade_name; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock_item_trade_name (
    stock_item_trade_name_id character varying(255) NOT NULL,
    trade_name character varying(255),
    item_id character varying(255),
    distributor_id character varying(255)
);


ALTER TABLE imed.stock_item_trade_name OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: stock_merge_po; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock_merge_po (
    stock_merge_po_id character varying(255) NOT NULL,
    stock_id character varying(255),
    merge_po_number character varying(255),
    merge_po_eid character varying(255),
    merge_po_date character varying(255),
    merge_po_time character varying(255),
    stock_budget_id character varying(255),
    stock_card_id character varying(255),
    fix_stock_merge_po_status_id character varying(255),
    merge_po_edit_eid character varying(255),
    ic_offer_unit_code character varying(255),
    ic_offer_unit_desc character varying(255),
    ic_offer_sub_unit_code character varying(255),
    ic_offer_sub_unit_desc character varying(255),
    ic_offer_plan_code character varying(255),
    ic_offer_plan_desc character varying(255),
    ic_offer_sub_plan_code character varying(255),
    ic_offer_sub_plan_desc character varying(255),
    ic_offer_sub_plan_detail_code character varying(255),
    ic_offer_sub_plan_detail_desc character varying(255),
    ic_offer_date character varying(255),
    ic_offer_subject character varying(255),
    ic_offer_to character varying(255),
    ic_offer_create_person character varying(255),
    ic_offer_create_date character varying(255),
    ic_offer_under_unit_desc character varying(255),
    ic_offer_cause character varying(255),
    ic_offer_budget_desc text,
    ic_offer_limit_cash character varying(255),
    ic_offer_limit_cash_text character varying(255),
    ic_offer_autothorize_name1 character varying(255),
    ic_offer_autothorize_position1 character varying(255),
    ic_offer_autothorize_date1 character varying(255),
    ic_offer_autothorize_name2 character varying(255),
    ic_offer_autothorize_position2 character varying(255),
    ic_offer_autothorize_date2 character varying(255),
    ic_offer_autothorize_name3 character varying(255),
    ic_offer_autothorize_position3 character varying(255),
    ic_offer_autothorize_date3 character varying(255),
    ic_request_date character varying(255),
    ic_request_order_method character varying(255),
    ic_request_chairman_name character varying(255),
    ic_request_chairman_positioin character varying(255),
    ic_request_commitee_name1 character varying(255),
    ic_request_commitee_positioin1 character varying(255),
    ic_request_commitee_name2 character varying(255),
    ic_request_commitee_positioin2 character varying(255),
    ic_request_create_person character varying(255),
    ic_request_create_position character varying(255),
    ic_request_audit_person character varying(255),
    ic_request_audit_position character varying(255),
    ic_request_audit_date character varying(255),
    ic_request_autothorize_name character varying(255),
    ic_request_autothorize_position character varying(255),
    ic_request_autothorize_date character varying(255),
    ic_order_distributor_id character varying(255),
    ic_order_paid_method_desc character varying(255),
    ic_order_po_number character varying(255),
    ic_order_limit_day character varying(255),
    ic_order_date character varying(255),
    ic_order_send_place character varying(255),
    ic_order_quotation_number character varying(255),
    ic_order_quotation_date character varying(255),
    ic_order_autothorize_name character varying(255),
    ic_order_autothorize_position character varying(255),
    ic_order_seller_name character varying(255),
    ic_receive_invoice_date character varying(255),
    ic_receive_chairman_name character varying(255),
    ic_receive_commitee_name1 character varying(255),
    ic_receive_commitee_name2 character varying(255),
    ic_receive_inspect_account_name character varying(255),
    ic_receive_inspect_account_date character varying(255),
    ic_receive_inspect_autothorize_name1 character varying(255),
    ic_receive_inspect_autothorize_position1 character varying(255),
    ic_receive_inspect_autothorize_name2 character varying(255),
    ic_receive_inspect_autothorize_position2 character varying(255),
    ic_requisition_date character varying(255),
    ic_requisition_requestor_name1 character varying(255),
    ic_requisition_requestor_position1 character varying(255),
    ic_requisition_request_date1 character varying(255),
    ic_requisition_requestor_name2 character varying(255),
    ic_requisition_requestor_position2 character varying(255),
    ic_requisition_request_date2 character varying(255),
    ic_requisition_requestor_name3 character varying(255),
    ic_requisition_requestor_position3 character varying(255),
    ic_requisition_request_date3 character varying(255),
    ic_requisition_dispense_name1 character varying(255),
    ic_requisition_dispense_date1 character varying(255),
    ic_requisition_dispense_name2 character varying(255),
    ic_requisition_dispense_date2 character varying(255),
    ic_requisition_dispense_name3 character varying(255),
    ic_requisition_dispense_date3 character varying(255),
    ic_requisition_inspect_account_name character varying(255),
    ic_requisition_inspect_account_position character varying(255),
    bg_offer_date character varying(255),
    bg_offer_desc character varying(255),
    bg_offer_method character varying(255),
    bg_offer_limit_cash character varying(255),
    bg_offer_limit_cash_text character varying(255),
    bg_offer_stock_id character varying(255),
    bg_offer_cause character varying(255),
    bg_offer_chairman_name character varying(255),
    bg_offer_commitee_name1 character varying(255),
    bg_offer_commitee_name2 character varying(255),
    bg_offer_commitee_name3 character varying(255),
    bg_offer_autothorize_name1 character varying(255),
    bg_offer_autothorize_position1 character varying(255),
    bg_offer_autothorize_budget_desc text,
    bg_offer_autothorize_name2 character varying(255),
    bg_offer_autothorize_position2 character varying(255),
    bg_offer_autothorize_name3 character varying(255),
    bg_offer_autothorize_position3 character varying(255),
    bg_offer_autothorize_name4 character varying(255),
    bg_offer_autothorize_position4 character varying(255),
    bg_significance_unit_desc character varying(255),
    bg_significance_date character varying(255),
    bg_significance_subject character varying(255),
    bg_significance_to character varying(255),
    bg_significance_person character varying(255),
    bg_significance_under_unit_desc character varying(255),
    bg_significance_order_desc character varying(255),
    bg_significance_order_method character varying(255),
    bg_significance_cause character varying(255),
    bg_significance_budget_desc text,
    bg_significance_autothorize_name1 character varying(255),
    bg_significance_autothorize_position1 character varying(255),
    bg_significance_autothorize_date1 character varying(255),
    bg_significance_autothorize_name2 character varying(255),
    bg_significance_autothorize_position2 character varying(255),
    bg_significance_autothorize_date2 character varying(255),
    bg_significance_autothorize_name3 character varying(255),
    bg_significance_autothorize_position3 character varying(255),
    bg_significance_autothorize_date3 character varying(255),
    bg_significance_autothorize_name4 character varying(255),
    bg_significance_autothorize_position4 character varying(255),
    bg_significance_autothorize_date4 character varying(255),
    bg_approve_number character varying(255),
    bg_approve_date character varying(255),
    bg_approve_subject character varying(255),
    bg_approve_to character varying(255),
    bg_approve_ref_quotation_number character varying(255),
    bg_approve_ref_quotation_date character varying(255),
    bg_approve_limit_receive_date character varying(255),
    bg_approve_order_name character varying(255),
    bg_approve_order_position character varying(255),
    bg_receive_create_place character varying(255),
    bg_receive_create_date character varying(255),
    bg_receive_plan_code1 character varying(255),
    bg_receive_work_code1 character varying(255),
    bg_receive_unit_code1 character varying(255),
    bg_receive_category character varying(255),
    bg_receive_plan_code2 character varying(255),
    bg_receive_work_code2 character varying(255),
    bg_receive_unit_code2 character varying(255),
    bg_receive_plan_code3 character varying(255),
    bg_receive_work_code3 character varying(255),
    bg_receive_unit_code3 character varying(255),
    bg_receive_distributor_id character varying(255),
    bg_receive_po_number character varying(255),
    bg_receive_po_date character varying(255),
    bg_receive_invoice_number character varying(255),
    bg_receive_invoice_date character varying(255),
    bg_receive_to character varying(255),
    bg_receive_chairman_name character varying(255),
    bg_receive_commitee_name1 character varying(255),
    bg_receive_commitee_name2 character varying(255),
    bg_receive_autothorize_name1 character varying(255),
    bg_receive_autothorize_position1 character varying(255),
    bg_receive_autothorize_name2 character varying(255),
    bg_receive_autothorize_position2 character varying(255),
    bg_requisition_date character varying(255),
    bg_requisition_create_place character varying(255),
    bg_requisition_subject character varying(255),
    bg_requisition_person character varying(255),
    bg_requisition_receiver_name character varying(255),
    bg_requisition_requestor_name character varying(255),
    bg_requisition_requestor_position character varying(255),
    organize_date character varying(255),
    book_number character varying(255),
    begin_page_number character varying(255),
    last_page_number character varying(255)
);


ALTER TABLE imed.stock_merge_po OWNER TO postgres;

--
-- Name: stock_merge_po_detail; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock_merge_po_detail (
    stock_merge_po_detail_id character varying(255) NOT NULL,
    stock_merge_po_id character varying(255),
    stock_card_id character varying(255)
);


ALTER TABLE imed.stock_merge_po_detail OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: stock_mgnt; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock_mgnt (
    stock_mgnt_id character varying(255) NOT NULL,
    stock_id character varying(255),
    item_id character varying(255),
    item_code character varying(255),
    lot_number character varying(255),
    manufacturer_id character varying(255),
    distributor_id character varying(255),
    base_drug_format_id character varying(255),
    produce_date character varying(255),
    expire_date character varying(255),
    range_to_expire character varying(255),
    place character varying(255),
    fix_abc_id character varying(255),
    fix_ven_id character varying(255),
    safety_stock character varying(255),
    stock_day character varying(255),
    fix_stock_method_id character varying(255),
    receive_date character varying(255),
    receive_time character varying(255),
    last_update_date character varying(255),
    last_update_time character varying(255),
    last_request_stock character varying(255),
    unit_price character varying(255),
    unit_price_no_vat character varying(255),
    mid_unit_price character varying(255),
    mid_unit_price_no_vat character varying(255),
    big_unit_price character varying(255),
    big_unit_price_no_vat character varying(255),
    cost_purchase_no_discount character varying(255),
    cost_purchase character varying(255),
    discount character varying(255),
    discount_percent character varying(255),
    vat character varying(255),
    fix_receive_method_id character varying(255),
    cur_quantity character varying(255),
    max_quantity character varying(255),
    min_quantity character varying(255),
    receive_quantity character varying(255),
    free_quantity character varying(255),
    update_qty character varying(255),
    big_unit_id character varying(255),
    small_unit_id character varying(255),
    unit_rate character varying(255),
    mid_unit_rate character varying(255),
    have_lot_analyse character varying(255),
    have_fda character varying(255),
    fda_number character varying(255),
    have_gmp character varying(255),
    gmp_number character varying(255),
    return_befor_expire_day character varying(255),
    active character varying(255),
    is_editable_lot character varying(255),
    fix_tax_type_id character varying(255),
    cost_purchase_no_discount_no_vat character varying(255),
    critical_quantity character varying(255),
    is_not_update_stock_mgnt character varying(255),
    note text,
    produce_eid character varying(255),
    stock_cssd_machine_id character varying(255),
    seq_machine character varying(255),
    lot_number_id character varying(255),
    item_trade_name character varying(255),
    base_drug_std_code character varying(255) DEFAULT ''::character varying
);


ALTER TABLE imed.stock_mgnt OWNER TO postgres;

--
-- Name: stock_mgnt_period; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock_mgnt_period (
    stock_mgnt_period_id character varying(255) NOT NULL,
    stock_id character varying(255),
    item_id character varying(255),
    item_code character varying(255),
    lot_number character varying(255),
    manufacturer_id character varying(255),
    distributor_id character varying(255),
    base_drug_format_id character varying(255),
    produce_date character varying(255),
    expire_date character varying(255),
    range_to_expire character varying(255),
    place character varying(255),
    fix_abc_id character varying(255),
    fix_ven_id character varying(255),
    safety_stock character varying(255),
    stock_day character varying(255),
    fix_stock_method_id character varying(255),
    receive_date character varying(255),
    receive_time character varying(255),
    last_update_date character varying(255),
    last_update_time character varying(255),
    last_request_stock character varying(255),
    unit_price character varying(255),
    unit_price_no_vat character varying(255),
    mid_unit_price character varying(255),
    mid_unit_price_no_vat character varying(255),
    big_unit_price character varying(255),
    big_unit_price_no_vat character varying(255),
    cost_purchase character varying(255),
    vat character varying(255),
    fix_receive_method_id character varying(255),
    cur_quantity character varying(255),
    max_quantity character varying(255),
    min_quantity character varying(255),
    receive_quantity character varying(255),
    free_quantity character varying(255),
    update_qty character varying(255),
    big_unit_id character varying(255),
    small_unit_id character varying(255),
    unit_rate character varying(255),
    mid_unit_rate character varying(255),
    active character varying(255),
    is_editable_lot character varying(255),
    period character varying(225)
);


ALTER TABLE imed.stock_mgnt_period OWNER TO postgres;

--
-- Name: stock_order; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock_order (
    stock_order_id character varying(255) NOT NULL,
    order_from_stock_id character varying(255),
    distributor_id character varying(255),
    manufacturer_id character varying(255),
    plan_code character varying(255),
    unit_code character varying(255),
    fund_code character varying(255),
    fund_name character varying(255),
    item_id character varying(255),
    small_unit_id character varying(255),
    unit_rate character varying(255) DEFAULT '1'::character varying,
    mid_unit_rate character varying(255) DEFAULT '1'::character varying,
    big_unit_id character varying(255),
    order_unit_id character varying(255),
    payer character varying(255),
    payer_unit character varying(255),
    payer_subunit character varying(255),
    cash_limit character varying(255),
    chairman_eid character varying(255),
    chairman_position character varying(255),
    commitee1_eid character varying(255),
    commitee1_position character varying(255),
    commitee2_eid character varying(255),
    commitee2_position character varying(255),
    commitee3_eid character varying(255),
    commitee3_position character varying(255),
    purchasing_offer_code character varying(255),
    purchasing_offer_number character varying(255),
    purchasing_offer_title character varying(255),
    purchasing_offer_to_officer character varying(255),
    purchasing_offer_unit character varying(255),
    purchasing_offer_team character varying(255),
    purchasing_offer_person character varying(255),
    purchasing_offer_cause character varying(255),
    purchasing_offer_quantity character varying(255),
    purchasing_offer_price_cost character varying(255),
    purchasing_offer_price character varying(255),
    purchasing_offer_price_no_vat character varying(255),
    purchasing_offer_discount character varying(255),
    purchasing_offer_discount_percent character varying(255),
    purchasing_offer_vat character varying(255),
    purchasing_offer_eid character varying(255),
    purchasing_offer_date character varying(255),
    purchasing_offer_time character varying(255),
    needs_order_date character varying(255),
    fix_stock_order_status_id character varying(255),
    free_quantity character varying(255) DEFAULT '0'::character varying,
    fiscal_year character varying(255),
    fix_stock_pay_method_id character varying(255),
    credit_on_pay character varying(255),
    fix_tax_type_id character varying(255),
    purchasing_offer_price_cost_no_vat character varying(255),
    purchasing_offer_note character varying(255),
    purchasing_offer_date_time character varying(255),
    purchasing_request_number character varying(255),
    order_comments character varying(255),
    order_not_received_comments character varying(255),
    is_insert character varying(255),
    last_big_unit_price_cost character varying(255),
    last_small_unit_price_cost character varying(255),
    last_receive_date character varying(255),
    last_receive_time character varying(255),
    last_receive_eid character varying(255),
    item_trade_name character varying(255),
    assume_receive_date character varying(255),
    po_expire_date character varying(255),
    total_discount character varying(255)
);


ALTER TABLE imed.stock_order OWNER TO postgres;

--
-- Name: stock_order_tmp; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock_order_tmp (
    stock_order_tmp_id character varying(255) NOT NULL,
    stock_order_id character varying(255),
    po_number character varying(255),
    item_id character varying(255),
    fix_stock_method_id character varying(255),
    distributor_id character varying(255),
    manufacturer_id character varying(255),
    invoice_number character varying(255),
    bill_number character varying(255),
    book_number character varying(255),
    stock_id character varying(255),
    have_lot_analyse character varying(255),
    lot_number character varying(255),
    expire_date character varying(255),
    produce_date character varying(255),
    range_to_expire character varying(255),
    have_fda character varying(255),
    fda_number character varying(255),
    have_gmp character varying(255),
    gmp_number character varying(255),
    return_befor_expire_day character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    modify_eid character varying(255),
    order_qty character varying(255),
    save_qty character varying(255),
    small_unit_id character varying(255),
    big_unit_id character varying(255),
    unit_rate character varying(255),
    mid_unit_rate character varying(255),
    fix_tax_type_id character varying(255),
    vat character varying(255),
    small_unit_price_cost character varying(255),
    big_unit_price_cost character varying(255),
    cost_purchase character varying(255),
    cost_purchase_no_discount character varying(255),
    discount character varying(255),
    discount_percent character varying(255),
    comments character varying(255),
    receive_date character varying(255),
    receive_time character varying(255),
    is_received character varying(255),
    sticker_qty character varying(255),
    stock_card_id character varying(255),
    payment_date character varying(255),
    payment_time character varying(255),
    pr_number character varying(255),
    total_discount character varying(255)
);


ALTER TABLE imed.stock_order_tmp OWNER TO postgres;

--
-- Name: stock_place; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock_place (
    stock_place_id character varying(255) NOT NULL,
    place character varying(255)
);


ALTER TABLE imed.stock_place OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: stock_prepack_item; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock_prepack_item (
    stock_prepack_item_id character varying(255) NOT NULL,
    stock_id character varying(255),
    stock_prepack_numer character varying(255),
    item_id character varying(255),
    distributor_id character varying(255),
    manufacturer_id character varying(255),
    lot_number character varying(255),
    big_unit_id character varying(255),
    unit_rate character varying(255),
    mid_unit_rate character varying(255),
    small_unit_id character varying(255),
    produce_date character varying(255),
    expire_date character varying(255),
    physical_appearance text,
    qty_per_pack character varying(255),
    prepack_lot_number character varying(255),
    prepack_produce_date character varying(255),
    prepack_expire_date character varying(255),
    container character varying(255),
    container_unit_id character varying(255),
    prepack_qty character varying(255),
    theory_pack_qty character varying(255),
    real_pack_qty character varying(255),
    prepack_eid character varying(255),
    audit_eid character varying(255),
    prepack_date character varying(255),
    prepack_time character varying(255),
    note text,
    sticker_prepack_qty character varying(255),
    fix_prepack_item_status_id character varying(255),
    stock_mgnt_id character varying(255)
);


ALTER TABLE imed.stock_prepack_item OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: stock_request; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock_request (
    stock_request_id character varying(255) NOT NULL,
    disp_stock_id character varying(255) DEFAULT ''::character varying,
    recv_stock_id character varying(255) DEFAULT ''::character varying,
    req_number character varying(255) DEFAULT ''::character varying,
    req_spid character varying(255) DEFAULT ''::character varying,
    req_eid character varying(255) DEFAULT ''::character varying,
    cancel_eid character varying(255) DEFAULT ''::character varying,
    req_date character varying(255) DEFAULT ''::character varying,
    req_time character varying(255) DEFAULT ''::character varying,
    cancel_date character varying(255) DEFAULT ''::character varying,
    cancel_time character varying(255) DEFAULT ''::character varying,
    item_id character varying(255) DEFAULT ''::character varying,
    req_quantity character varying(255) DEFAULT ''::character varying,
    fix_stock_request_status_id character varying(255) DEFAULT ''::character varying,
    fix_stock_request_method_id character varying(255) DEFAULT ''::character varying,
    is_request_from_dispense character varying(255) DEFAULT ''::character varying,
    request_date_from character varying(255) DEFAULT ''::character varying,
    request_time_from character varying(255) DEFAULT ''::character varying,
    request_date_to character varying(255) DEFAULT ''::character varying,
    request_time_to character varying(255) DEFAULT ''::character varying,
    req_unit_id character varying(255),
    req_unit_price character varying(255),
    req_date_time character varying(255),
    tmp_dispense_qty character varying(255),
    tmp_comment character varying(255),
    tmp_arrear_qty character varying(255),
    tmp_is_arrear character varying(255),
    is_borrow character varying(255),
    is_pay_back character varying(255),
    order_item_id character varying(255)
);


ALTER TABLE imed.stock_request OWNER TO postgres;

--
-- Name: stock_request_order; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock_request_order (
    stock_request_order_id character varying(255) NOT NULL,
    stock_request_order_number character varying(255),
    stock_id character varying(255),
    distributor_id character varying(255),
    manufacturer_id character varying(255),
    item_id character varying(255),
    big_unit_id character varying(255),
    small_unit_id character varying(255),
    mid_unit_rate character varying(255),
    unit_rate character varying(255),
    suggest_order_qty character varying(255),
    fix_order_qty character varying(255),
    fix_free_order_qty character varying(255),
    req_order_qty character varying(255),
    free_req_order_qty character varying(255),
    req_order_date_time character varying(255),
    req_order_eid character varying(255),
    fix_req_order_status character varying(255),
    comment character varying(255),
    is_insert character varying(255),
    stock_base_reason_of_request_order_id character varying(255),
    cost_purchase character varying(255),
    vat character varying(255),
    cost_purchase_no_vat character varying(255),
    fix_tax_type_id character varying(255),
    item_trade_name character varying(255),
    purchase_order_qty character varying(255),
    minimum_qty character varying(255)
);


ALTER TABLE imed.stock_request_order OWNER TO postgres;

--
-- Name: stock_request_produce; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock_request_produce (
    stock_request_produce_id character varying(255) NOT NULL,
    stock_request_produce_number character varying(255),
    stock_id character varying(255),
    item_id character varying(255),
    big_unit_id character varying(255),
    small_unit_id character varying(255),
    mid_unit_rate character varying(255),
    unit_rate character varying(255),
    reqed_produce_qty character varying(255),
    suggest_produce_qty character varying(255),
    req_produce_qty character varying(255),
    req_produce_date_time character varying(255),
    req_produce_eid character varying(255),
    comment character varying(255),
    fix_req_produce_status character varying(255),
    is_insert character varying(255),
    produce_process text,
    lot_number character varying(255),
    produce_date character varying(255),
    expire_date character varying(255),
    other_cost character varying(255),
    total_cost character varying(255),
    is_cut_off_material character varying(255),
    sticker_qty character varying(255)
);


ALTER TABLE imed.stock_request_produce OWNER TO postgres;

--
-- Name: stock_request_produce_material; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock_request_produce_material (
    stock_request_produce_material_id character varying(255) NOT NULL,
    stock_request_produce_id character varying(255),
    material_id character varying(255),
    material_qty character varying(255),
    product_qty character varying(255)
);


ALTER TABLE imed.stock_request_produce_material OWNER TO postgres;

--
-- Name: stock_return; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock_return (
    stock_return_id character varying(255) NOT NULL,
    disp_stock_id character varying(255),
    recv_stock_id character varying(255),
    return_number character varying(255),
    return_eid character varying(255),
    return_date character varying(255),
    return_time character varying(255),
    item_id character varying(255),
    lot_number character varying(255),
    return_quantity character varying(255),
    fix_stock_return_status_id character varying(255),
    note text,
    reason_of_return character varying(255),
    expire_date character varying(255),
    req_number character varying(255),
    borrow_date character varying(255),
    borrow_eid character varying(255),
    stock_mgnt_id character varying(255)
);


ALTER TABLE imed.stock_return OWNER TO postgres;

--
-- Name: stock_setup_order; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock_setup_order (
    stock_setup_order_id character varying(255) NOT NULL,
    item_id character varying(255),
    distributor_id character varying(255),
    manufacturer_id character varying(255),
    active character varying(255),
    item_trade_name character varying(255)
);


ALTER TABLE imed.stock_setup_order OWNER TO postgres;

--
-- Name: stock_setup_order_detail; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock_setup_order_detail (
    stock_setup_order_detail_id character varying(255) NOT NULL,
    stock_setup_order_id character varying(255),
    big_unit_id character varying(255),
    small_unit_id character varying(255),
    mid_unit_rate character varying(255),
    unit_rate character varying(255),
    vat character varying(255),
    fix_tax_type_id character varying(255),
    order_unit_price character varying(255),
    discount character varying(255),
    discount_percent character varying(255),
    order_qty character varying(255),
    free_qty character varying(255),
    active_date character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    modify_eid character varying(255),
    minimum_qty character varying(255)
);


ALTER TABLE imed.stock_setup_order_detail OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: stock_sterile_method; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock_sterile_method (
    stock_sterile_method_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.stock_sterile_method OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: stock_team_work; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock_team_work (
    stock_team_work_id character varying(255) NOT NULL,
    team_work_name character varying(255)
);


ALTER TABLE imed.stock_team_work OWNER TO postgres;

--
-- Name: stock_template_produce; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock_template_produce (
    stock_template_produce_id character varying(255) NOT NULL,
    item_id character varying(255),
    material_id character varying(255),
    material_qty character varying(255),
    product_qty character varying(255)
);


ALTER TABLE imed.stock_template_produce OWNER TO postgres;

--
-- Name: stock_used_in_stock_or_produce; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.stock_used_in_stock_or_produce (
    stock_used_in_stock_or_produce_id character varying(255) NOT NULL,
    item_id character varying(255),
    used_in_stock_or_produce_number character varying(255),
    fix_stock_method_id character varying(255),
    fix_stock_used_in_stock_or_produce_status character varying(255),
    qty character varying(255),
    stock_id character varying(255),
    department_id character varying(255),
    dispense_eid character varying(255),
    comment character varying(255)
);


ALTER TABLE imed.stock_used_in_stock_or_produce OWNER TO postgres;

--
-- Name: sub_drawing_details; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.sub_drawing_details (
    sub_drawing_details_id character varying(255) NOT NULL,
    drawing_details_id character varying(255),
    sub_x_axis character varying(255),
    sub_y_axis character varying(255)
);


ALTER TABLE imed.sub_drawing_details OWNER TO postgres;

--
-- Name: sum_cost; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.sum_cost (
    sum_cost_id character varying(255) NOT NULL,
    patient_id character varying(255),
    visit_id character varying(255),
    sum_cost_number character varying(255),
    created_eid character varying(255),
    created_date character varying(255),
    created_time character varying(255),
    last_print_eid character varying(255),
    last_print_date character varying(255),
    last_print_time character varying(255),
    max_print_times character varying(255),
    begin_date character varying(255),
    begin_time character varying(255),
    end_date character varying(255),
    end_time character varying(255),
    num_date character varying(255),
    cost character varying(255),
    discount character varying(255),
    paid character varying(255),
    deposit character varying(255),
    invoice_paid character varying(255),
    general_paid character varying(255),
    remain_cost character varying(255)
);


ALTER TABLE imed.sum_cost OWNER TO postgres;

--
-- Name: sum_cost_billing_group; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.sum_cost_billing_group (
    sum_cost_billing_group_id character varying(255) NOT NULL,
    sum_cost_id character varying(255),
    base_billing_group_id character varying(255),
    patient_id character varying(255),
    visit_id character varying(255),
    cost character varying(255),
    discount character varying(255),
    paid character varying(255),
    ipd_accommodation character varying(255)
);


ALTER TABLE imed.sum_cost_billing_group OWNER TO postgres;

--
-- Name: template_appointment; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.template_appointment (
    template_appointment_id character varying(255) NOT NULL,
    description character varying(255),
    basic_advice character varying(255),
    note character varying(255),
    active character varying(255)
);


ALTER TABLE imed.template_appointment OWNER TO postgres;

--
-- Name: template_appointment_detail; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.template_appointment_detail (
    template_appointment_detail_id character varying(255) NOT NULL,
    template_appointment_id character varying(255),
    continue_note character varying(255),
    length_of_day character varying(255),
    priority character varying(255)
);


ALTER TABLE imed.template_appointment_detail OWNER TO postgres;

--
-- Name: template_appointment_order; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.template_appointment_order (
    template_appointment_order_id character varying(255) NOT NULL,
    template_appointment_detail_id character varying(255),
    item_id character varying(255),
    fix_item_type_id character varying(255),
    base_drug_usage_code character varying(255),
    instruction_text_line1 character varying(255),
    instruction_text_line2 character varying(255),
    instruction_text_line3 character varying(255),
    quantity character varying(255),
    base_unit_id character varying(255),
    description character varying(255),
    caution character varying(255),
    priority character varying(255)
);


ALTER TABLE imed.template_appointment_order OWNER TO postgres;

--
-- Name: template_dent_operation; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.template_dent_operation (
    template_dent_operation_id character varying(255) NOT NULL,
    template_code character varying(255) DEFAULT ''::character varying,
    description text DEFAULT ''::text,
    base_dent_operation_id character varying(255) DEFAULT ''::character varying,
    employee_id character varying(255) DEFAULT ''::character varying
);


ALTER TABLE imed.template_dent_operation OWNER TO postgres;

--
-- Name: template_discount; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.template_discount (
    template_discount_id character varying(255) NOT NULL,
    description character varying(255),
    base_billing_group_id text,
    discount_percent text
);


ALTER TABLE imed.template_discount OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: template_drug_allergy_note; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.template_drug_allergy_note (
    template_drug_allergy_note_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.template_drug_allergy_note OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: template_drug_usage; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.template_drug_usage (
    template_drug_usage_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.template_drug_usage OWNER TO postgres;

--
-- Name: template_dt_tx_note; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.template_dt_tx_note (
    template_dt_tx_note_id character varying(255) NOT NULL,
    code character varying(255),
    description text,
    employee_id character varying(255),
    base_dt_tx_type_id character varying(255)
);


ALTER TABLE imed.template_dt_tx_note OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: template_forensic_out_exam; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.template_forensic_out_exam (
    template_forensic_out_exam_id character varying(255) NOT NULL,
    description character varying(255),
    base_forensic_out_exam_id character varying(255)
);


ALTER TABLE imed.template_forensic_out_exam OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: template_item_set; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.template_item_set (
    template_item_set_id character varying(255) NOT NULL,
    item_set_id character varying(255),
    employee_id character varying(255),
    base_service_point_id character varying(255)
);


ALTER TABLE imed.template_item_set OWNER TO postgres;

--
-- Name: template_lab_choice; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.template_lab_choice (
    template_lab_choice_id character varying(254) NOT NULL,
    template_lab_test_id character varying(254),
    description character varying(254),
    abnormal character varying(254),
    abnormal_text text
);


ALTER TABLE imed.template_lab_choice OWNER TO postgres;

--
-- Name: template_lab_item; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.template_lab_item (
    template_lab_item_id character varying(255) NOT NULL,
    template_lab_test_id character varying(255),
    item_id character varying(255),
    result_position character varying(255)
);


ALTER TABLE imed.template_lab_item OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: template_lab_item_tube; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.template_lab_item_tube (
    template_lab_item_tube_id character varying(255) NOT NULL,
    base_lab_tube_id character varying(255),
    item_id character varying(255)
);


ALTER TABLE imed.template_lab_item_tube OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: template_lab_normal_value; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.template_lab_normal_value (
    template_lab_normal_value_id character varying(255) NOT NULL,
    template_lab_test_id character varying(255),
    fix_lab_normal_value_type_id character varying(255),
    normal_value_max character varying(255),
    normal_value_min character varying(255),
    critical_value_max character varying(255),
    critical_value_min character varying(255)
);


ALTER TABLE imed.template_lab_normal_value OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: template_lab_special_request; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.template_lab_special_request (
    template_lab_special_request_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.template_lab_special_request OWNER TO postgres;

--
-- Name: template_lab_sub_test; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.template_lab_sub_test (
    template_lab_sub_test_id character varying(255) NOT NULL,
    template_lab_test_id character varying(255),
    sub_test_name character varying(255),
    result_position character varying(255)
);


ALTER TABLE imed.template_lab_sub_test OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: template_lab_test; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.template_lab_test (
    template_lab_test_id character varying(254) NOT NULL,
    name character varying(254),
    unit_text character varying(254),
    fix_lab_test_type_id character varying(254),
    report_alert text,
    print_name character varying(255),
    normal_value_text text,
    less_min_text text,
    less_cc_min_text text,
    more_max_text text,
    more_cc_max_text text,
    formula character varying(255),
    is_have_sub_test character varying(255),
    is_have_addition_value character varying(255),
    nick_name character varying(255)
);


ALTER TABLE imed.template_lab_test OWNER TO postgres;

--
-- Name: template_lr_indication; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.template_lr_indication (
    template_lr_indication_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.template_lr_indication OWNER TO postgres;

--
-- Name: template_lr_pp_med_treatment; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.template_lr_pp_med_treatment (
    template_lr_pp_med_treatment_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.template_lr_pp_med_treatment OWNER TO postgres;

--
-- Name: template_ncp_group; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.template_ncp_group (
    template_ncp_group_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.template_ncp_group OWNER TO postgres;

--
-- Name: template_ncp_plan_details; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.template_ncp_plan_details (
    template_ncp_plan_details_id character varying(255) NOT NULL,
    description character varying(255),
    template_ncp_problem_id character varying(255)
);


ALTER TABLE imed.template_ncp_plan_details OWNER TO postgres;

--
-- Name: template_ncp_problem; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.template_ncp_problem (
    template_ncp_problem_id character varying(255) NOT NULL,
    description character varying(255),
    template_ncp_group_id character varying(255)
);


ALTER TABLE imed.template_ncp_problem OWNER TO postgres;

--
-- Name: template_ncp_problem_details; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.template_ncp_problem_details (
    template_ncp_problem_details_id character varying(255) NOT NULL,
    description character varying(255),
    template_ncp_problem_id character varying(255)
);


ALTER TABLE imed.template_ncp_problem_details OWNER TO postgres;

--
-- Name: template_ncp_result_details; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.template_ncp_result_details (
    template_ncp_result_details_id character varying(255) NOT NULL,
    description character varying(255),
    template_ncp_problem_id character varying(255)
);


ALTER TABLE imed.template_ncp_result_details OWNER TO postgres;

--
-- Name: template_nt_allergy; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.template_nt_allergy (
    template_nt_allergy_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.template_nt_allergy OWNER TO postgres;

--
-- Name: template_nt_change_order; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.template_nt_change_order (
    template_nt_change_order_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.template_nt_change_order OWNER TO postgres;

--
-- Name: template_nt_default_order; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.template_nt_default_order (
    template_nt_default_order_id character varying(255) NOT NULL,
    fix_nt_meal_type_id character varying(255),
    fix_nt_diet_type_id character varying(255),
    base_room_type_id character varying(255),
    item_id character varying(255),
    base_nt_therapeutic_type_id character varying(255),
    base_nt_type_id character varying(255)
);


ALTER TABLE imed.template_nt_default_order OWNER TO postgres;

--
-- Name: template_op_position; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.template_op_position (
    template_op_position_id character varying(255) NOT NULL,
    description character varying(255),
    base_template_operation_id character varying(255)
);


ALTER TABLE imed.template_op_position OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: template_patho_operation; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.template_patho_operation (
    template_patho_operation_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.template_patho_operation OWNER TO postgres;

--
-- Name: template_patho_result; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.template_patho_result (
    template_patho_result_id character varying(255) NOT NULL,
    template_name character varying(255),
    description text,
    diagnosis text,
    base_lab_type_id character varying(255),
    employee_id character varying(255)
);


ALTER TABLE imed.template_patho_result OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: template_personal_illness; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.template_personal_illness (
    template_personal_illness_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.template_personal_illness OWNER TO postgres;

--
-- Name: template_phyex; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.template_phyex (
    template_phyex_id character varying(255) NOT NULL,
    description character varying(255),
    show_default character varying(255)
);


ALTER TABLE imed.template_phyex OWNER TO postgres;

--
-- Name: template_phyex_default; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.template_phyex_default (
    template_phyex_default_id character varying(255) NOT NULL,
    template_phyex_id character varying(255),
    employee_id character varying(255),
    base_clinic_id character varying(255)
);


ALTER TABLE imed.template_phyex_default OWNER TO postgres;

--
-- Name: template_phyex_detail; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.template_phyex_detail (
    template_phyex_detail_id character varying(255) NOT NULL,
    template_phyex_id character varying(255),
    base_organic_id character varying(255),
    organ_position character varying(255),
    phyex_result character varying(255)
);


ALTER TABLE imed.template_phyex_detail OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: template_return_drug_reason; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.template_return_drug_reason (
    template_return_drug_reason_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.template_return_drug_reason OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: template_risk_factor; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.template_risk_factor (
    template_risk_factor_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.template_risk_factor OWNER TO postgres;

--
-- Name: template_symptom; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.template_symptom (
    template_symptom_id character varying(255) NOT NULL,
    template_symptom_group_id character varying(255),
    description character varying(255)
);


ALTER TABLE imed.template_symptom OWNER TO postgres;

--
-- Name: template_symptom_default; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.template_symptom_default (
    template_symptom_default_id character varying(255) NOT NULL,
    template_symptom_group_id character varying(255),
    base_clinic_id character varying(255),
    is_shortcut character varying(255)
);


ALTER TABLE imed.template_symptom_default OWNER TO postgres;

--
-- Name: template_symptom_group; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.template_symptom_group (
    template_symptom_group_id character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE imed.template_symptom_group OWNER TO postgres;

--
-- Name: template_xray_result; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.template_xray_result (
    template_xray_result_id character varying(254) NOT NULL,
    code character varying(254),
    employee_id character varying(254),
    result text,
    impression text
);


ALTER TABLE imed.template_xray_result OWNER TO postgres;

--
-- Name: time_stamp_details; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.time_stamp_details (
    time_stamp_details_id character varying(255) NOT NULL,
    base_time_stamp_id character varying(255),
    fix_service_point_group_id character varying(255),
    begin_stamp_time character varying(255),
    finish_stamp_time character varying(255)
);


ALTER TABLE imed.time_stamp_details OWNER TO postgres;

--
-- Name: time_stamp_receive_file; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.time_stamp_receive_file (
    time_stamp_receive_file_id character varying(255) NOT NULL,
    visit_id character varying(255),
    receive_eid character varying(255),
    receive_spid character varying(255),
    receive_date character varying(255),
    receive_time character varying(255)
);


ALTER TABLE imed.time_stamp_receive_file OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: timers; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.timers (
    timerid character varying(80) NOT NULL,
    targetid character varying(250) NOT NULL,
    initialdate timestamp without time zone NOT NULL,
    timerinterval bigint,
    instancepk bytea,
    info bytea
);


ALTER TABLE imed.timers OWNER TO postgres;

--
-- Name: user_audit; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.user_audit (
    user_audit_id character varying(255) NOT NULL,
    fix_audit_type_id character varying(255),
    referent_id character varying(255),
    employee_id character varying(255),
    audit_date character varying(255),
    audit_time character varying(255)
);


ALTER TABLE imed.user_audit OWNER TO postgres;

--
-- Name: visit_clinic; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.visit_clinic (
    visit_clinic_id character varying(255) NOT NULL,
    visit_id character varying(255),
    base_clinic_id character varying(255)
);


ALTER TABLE imed.visit_clinic OWNER TO postgres;

--
-- Name: visit_deliver; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.visit_deliver (
    visit_deliver_id character varying(255) NOT NULL,
    visit_id character varying(255) DEFAULT ''::character varying,
    deliver_name character varying(255) DEFAULT ''::character varying,
    fix_gender_id character varying(255) DEFAULT ''::character varying,
    home_id character varying(255) DEFAULT ''::character varying,
    road character varying(255) DEFAULT ''::character varying,
    village character varying(255) DEFAULT ''::character varying,
    fix_tambol_id character varying(255) DEFAULT ''::character varying,
    fix_amphur_id character varying(255) DEFAULT ''::character varying,
    fix_changwat_id character varying(255) DEFAULT ''::character varying,
    postcode character varying(255) DEFAULT ''::character varying,
    telephone character varying(255) DEFAULT ''::character varying,
    note text
);


ALTER TABLE imed.visit_deliver OWNER TO postgres;

--
-- Name: visit_queue; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.visit_queue (
    visit_queue_id character varying(255) NOT NULL,
    patient_id character varying(255),
    visit_id character varying(255),
    fix_visit_type_id character varying(255),
    next_location_spid character varying(255),
    next_operate_eid character varying(255),
    next_location_date character varying(255),
    next_location_time character varying(255),
    assign_location_spid character varying(255),
    assign_operate_eid character varying(255),
    sort_queue_number character varying(255),
    queue_number character varying(255),
    queue_status character varying(255),
    next_department_id character varying(255)
);


ALTER TABLE imed.visit_queue OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: visit_sound_file; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.visit_sound_file (
    visit_sound_file_id character varying(255) NOT NULL,
    visit_id character varying(255),
    sound_tag character varying(255),
    sound_note text,
    sound_name character varying(255),
    added_eid character varying(255),
    added_date character varying(255),
    added_time character varying(255)
);


ALTER TABLE imed.visit_sound_file OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: visit_special_card; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.visit_special_card (
    visit_special_card_id character varying(255) NOT NULL,
    visit_id character varying(255),
    base_disc_special_card_id character varying(255),
    card_id character varying(255),
    inspire_date character varying(255),
    expire_date character varying(255),
    note text
);


ALTER TABLE imed.visit_special_card OWNER TO postgres;

--
-- Name: visit_waiting_reason; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.visit_waiting_reason (
    visit_waiting_reason_id character varying(255) NOT NULL,
    visit_id character varying(255),
    employee_id character varying(255),
    base_long_waiting_reason_id character varying(255)
);


ALTER TABLE imed.visit_waiting_reason OWNER TO postgres;

--
-- Name: vital_sign_extend; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.vital_sign_extend (
    vital_sign_extend_id character varying(255) NOT NULL,
    visit_id character varying(255),
    main_symptom text,
    current_illness text,
    doctor_eid character varying(255),
    examine_date character varying(255),
    examine_time character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    attending_physician_id character varying(255)
);


ALTER TABLE imed.vital_sign_extend OWNER TO postgres;

--
-- Name: vital_sign_ipd; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.vital_sign_ipd (
    vital_sign_ipd_id character varying(254) NOT NULL,
    admit_id character varying(254),
    pressure_max character varying(254),
    pressure_min character varying(254),
    temperature character varying(254),
    pulse character varying(254),
    respiration character varying(254),
    note text,
    abnormal_pressure character varying(254),
    abnormal_pulse character varying(254),
    measure_eid character varying(254),
    measure_date character varying(254),
    measure_time character varying(254),
    modify_eid character varying(254),
    modify_date character varying(254),
    modify_time character varying(254),
    fix_patient_class_id character varying(255),
    sat_o2 character varying(255),
    urine character varying(255),
    stools character varying(255)
);


ALTER TABLE imed.vital_sign_ipd OWNER TO postgres;

--
-- Name: vital_sign_opd; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.vital_sign_opd (
    vital_sign_opd_id character varying(255) NOT NULL,
    visit_id character varying(255),
    weight character varying(255),
    height character varying(255),
    pressure_max character varying(255),
    pressure_min character varying(255),
    temperature character varying(255),
    pulse character varying(255),
    respiration character varying(255),
    bmi character varying(255),
    abnormal_pressure character varying(255),
    abnormal_pulse character varying(255),
    measure_eid character varying(255),
    measure_date character varying(255),
    measure_time character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255),
    sat_o2 character varying(255),
    measure_spid character varying(255),
    waist_width character varying(255),
    nurse_note text,
    dtx character varying(255),
    hct character varying(255),
    head_length character varying(255)
);


ALTER TABLE imed.vital_sign_opd OWNER TO postgres;

--
-- Name: vs_coma_scale; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.vs_coma_scale (
    vs_coma_scale_id character varying(255) NOT NULL,
    visit_id character varying(255),
    base_vs_coma_scale_eye_id character varying(255),
    base_vs_coma_scale_verbal_id character varying(255),
    base_vs_coma_scale_motion_id character varying(255),
    base_vs_coma_scale_id character varying(255),
    total_coma_scale character varying(255),
    pupil_score_right_size character varying(255),
    pupil_score_right_reaction character varying(255),
    pupil_score_left_size character varying(255),
    pupil_score_left_reaction character varying(255),
    motor_power_right_arm character varying(255),
    motor_power_right_leg character varying(255),
    motor_power_left_arm character varying(255),
    motor_power_left_leg character varying(255),
    exam_date character varying(255),
    exam_time character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255)
);


ALTER TABLE imed.vs_coma_scale OWNER TO postgres;

--
-- Name: vs_ext_adv_check; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.vs_ext_adv_check (
    vs_ext_adv_check_id character varying(255) NOT NULL,
    vs_ext_advice_id character varying(255),
    base_vs_ext_advice_id character varying(255)
);


ALTER TABLE imed.vs_ext_adv_check OWNER TO postgres;

--
-- Name: vs_ext_advice; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.vs_ext_advice (
    vs_ext_advice_id character varying(255) NOT NULL,
    vital_sign_extend_id character varying(255) NOT NULL,
    doctor_other_advice text,
    nurse_other_advice character varying(255),
    nurse_id character varying(255)
);


ALTER TABLE imed.vs_ext_advice OWNER TO postgres;

--
-- Name: vs_ipd_intake; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.vs_ipd_intake (
    vs_ipd_intake_id character varying(255) NOT NULL,
    vital_sign_ipd_id character varying(255),
    base_intake_id character varying(255),
    quantity character varying(255)
);


ALTER TABLE imed.vs_ipd_intake OWNER TO postgres;

--
-- Name: vs_ipd_output; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.vs_ipd_output (
    vs_ipd_output_id character varying(255) NOT NULL,
    vital_sign_ipd_id character varying(255),
    base_output_id character varying(255),
    quantity character varying(255)
);


ALTER TABLE imed.vs_ipd_output OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: vs_patient_classification; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.vs_patient_classification (
    vs_patient_classification_id character varying(255) NOT NULL,
    patient_id character varying(255),
    admit_id character varying(255),
    classify_date character varying(255),
    fix_time_of_day character varying(255),
    total_score character varying(255),
    base_patient_classification_id character varying(255),
    fix_patient_age character varying(255),
    modify_eid character varying(255),
    modify_date character varying(255),
    modify_time character varying(255)
);


ALTER TABLE imed.vs_patient_classification OWNER TO postgres;

--
-- Name: vs_patient_classification_detail; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.vs_patient_classification_detail (
    vs_patient_classification_detail_id character varying(255) NOT NULL,
    vs_patient_classification_id character varying(255),
    base_patient_classify_standard_id character varying(255),
    base_patient_classify_determiners_id character varying(255),
    score character varying(255)
);


ALTER TABLE imed.vs_patient_classification_detail OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: xray_execute_detail; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.xray_execute_detail (
    xray_execute_detail_id character varying(254) NOT NULL,
    assign_xray_id character varying(254),
    order_item_id character varying(254),
    execute_eid character varying(254),
    execute_date character varying(254),
    execute_time character varying(254),
    xray_point character varying(254),
    note text,
    film_charge character varying(254),
    modify_eid character varying(254),
    modify_date character varying(254),
    modify_time character varying(254)
);


ALTER TABLE imed.xray_execute_detail OWNER TO postgres;

--
-- Name: xray_film; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.xray_film (
    xray_film_id character varying(254) NOT NULL,
    xray_execute_detail_id character varying(254),
    patient_id character varying(254),
    base_xray_film_id character varying(254),
    fix_film_status_id character varying(254),
    unit_price_sale character varying(254),
    base_bad_film_reason_id character varying(255)
);


ALTER TABLE imed.xray_film OWNER TO postgres;

--
-- Name: xray_film_depository; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.xray_film_depository (
    xray_film_depository_id character varying(255) NOT NULL,
    ref_no character varying(255),
    patient_id character varying(255),
    fix_xray_type character varying(255),
    quantity character varying(255),
    hospital_name character varying(255),
    receive_eid character varying(255),
    return_eid character varying(255),
    receive_date character varying(255),
    return_date character varying(255),
    fix_film_status character varying(255),
    note text
);


ALTER TABLE imed.xray_film_depository OWNER TO postgres;

--
-- Name: xray_result; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE imed.xray_result (
    xray_result_id character varying(254) NOT NULL,
    assign_xray_id character varying(254),
    visit_id character varying(254),
    patient_id character varying(254),
    order_item_id character varying(254),
    item_id character varying(254),
    fix_xray_type_id character varying(254),
    times_reported character varying(254),
    reported character varying(254),
    report_eid character varying(254),
    modify_eid character varying(254),
    modify_spid character varying(254),
    modify_date character varying(254),
    modify_time character varying(254),
    title text,
    result text
);


ALTER TABLE imed.xray_result OWNER TO postgres;

--
-- Name: base_drug_std_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE imed.base_drug_std ALTER COLUMN base_drug_std_id SET DEFAULT nextval('base_drug_std_base_drug_std_id_seq'::regclass);


--
-- Name: abc_ven_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.abc_ven
    ADD CONSTRAINT abc_ven_pkey PRIMARY KEY (abc_ven_id);


--
-- Name: accident_emergency_report_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.accident_emergency_report
    ADD CONSTRAINT accident_emergency_report_pkey PRIMARY KEY (accident_emergency_report_id);


--
-- Name: admission_note_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.admission_note
    ADD CONSTRAINT admission_note_pkey PRIMARY KEY (admission_note_id);


--
-- Name: admit_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.admit
    ADD CONSTRAINT admit_pkey PRIMARY KEY (admit_id);


--
-- Name: anes_assessment_data_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.anes_assessment_data
    ADD CONSTRAINT anes_assessment_data_pkey PRIMARY KEY (anes_assessment_data_id);


--
-- Name: anes_complication_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.anes_complication
    ADD CONSTRAINT anes_complication_pkey PRIMARY KEY (anes_complication_id);


--
-- Name: anes_cvs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.anes_cvs
    ADD CONSTRAINT anes_cvs_pkey PRIMARY KEY (anes_cvs_id);


--
-- Name: anes_data_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.anes_data
    ADD CONSTRAINT anes_data_pkey PRIMARY KEY (anes_data_id);


--
-- Name: anes_general_data_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.anes_general_data
    ADD CONSTRAINT anes_general_data_pkey PRIMARY KEY (anes_general_data_id);


--
-- Name: anes_inhalation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.anes_inhalation
    ADD CONSTRAINT anes_inhalation_pkey PRIMARY KEY (anes_inhalation_id);


--
-- Name: anes_lab_investigation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.anes_lab_investigation
    ADD CONSTRAINT anes_lab_investigation_pkey PRIMARY KEY (anes_lab_investigation_id);


--
-- Name: anes_local_agent_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.anes_local_agent
    ADD CONSTRAINT anes_local_agent_pkey PRIMARY KEY (anes_local_agent_id);


--
-- Name: anes_monitor_technique_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.anes_monitor_technique
    ADD CONSTRAINT anes_monitor_technique_pkey PRIMARY KEY (anes_monitor_technique_id);


--
-- Name: anes_monitors_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.anes_monitors
    ADD CONSTRAINT anes_monitors_pkey PRIMARY KEY (anes_monitors_id);


--
-- Name: anes_pacu_data_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.anes_pacu_data
    ADD CONSTRAINT anes_pacu_data_pkey PRIMARY KEY (anes_pacu_data_id);


--
-- Name: anes_physical_exam_data_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.anes_physical_exam_data
    ADD CONSTRAINT anes_physical_exam_data_pkey PRIMARY KEY (anes_physical_exam_data_id);


--
-- Name: anes_po_pain_management_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.anes_po_pain_management
    ADD CONSTRAINT anes_po_pain_management_pkey PRIMARY KEY (anes_po_pain_management_id);


--
-- Name: anes_pre_op_condition_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.anes_pre_op_condition
    ADD CONSTRAINT anes_pre_op_condition_pkey PRIMARY KEY (anes_pre_op_condition_id);


--
-- Name: anes_premedication_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.anes_premedication
    ADD CONSTRAINT anes_premedication_pkey PRIMARY KEY (anes_premedication_id);


--
-- Name: anes_service_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.anes_service
    ADD CONSTRAINT anes_service_pkey PRIMARY KEY (anes_service_id);


--
-- Name: anes_special_tech_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.anes_special_tech
    ADD CONSTRAINT anes_special_tech_pkey PRIMARY KEY (anes_special_tech_id);


--
-- Name: anes_team_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.anes_team
    ADD CONSTRAINT anes_team_pkey PRIMARY KEY (anes_team_id);


--
-- Name: anes_technique_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.anes_technique
    ADD CONSTRAINT anes_technique_pkey PRIMARY KEY (anes_technique_id);


--
-- Name: appointment_doctor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.appointment_doctor
    ADD CONSTRAINT appointment_doctor_pkey PRIMARY KEY (appointment_doctor_id);


--
-- Name: appointment_order_item_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.appointment_order_item
    ADD CONSTRAINT appointment_order_item_pkey PRIMARY KEY (appointment_order_item_id);


--
-- Name: appointment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.appointment
    ADD CONSTRAINT appointment_pkey PRIMARY KEY (appointment_id);


--
-- Name: assign_lab_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.assign_lab
    ADD CONSTRAINT assign_lab_pkey PRIMARY KEY (assign_lab_id);


--
-- Name: assign_times_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.assign_times
    ADD CONSTRAINT assign_times_pkey PRIMARY KEY (assign_times_id);


--
-- Name: assign_xray_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.assign_xray
    ADD CONSTRAINT assign_xray_pkey PRIMARY KEY (assign_xray_id);


--
-- Name: attending_physician_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.attending_physician
    ADD CONSTRAINT attending_physician_pkey PRIMARY KEY (attending_physician_id);


--
-- Name: base_accident_zone_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_accident_zone
    ADD CONSTRAINT base_accident_zone_pkey PRIMARY KEY (base_accident_zone_id);


--
-- Name: base_address_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_address
    ADD CONSTRAINT base_address_pkey PRIMARY KEY (fullcode);


--
-- Name: base_adp_code_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_adp_code
    ADD CONSTRAINT base_adp_code_pkey PRIMARY KEY (base_adp_code_id);


--
-- Name: base_adp_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_adp_type
    ADD CONSTRAINT base_adp_type_pkey PRIMARY KEY (base_adp_type_id);


--
-- Name: base_anes_airway_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_anes_airway
    ADD CONSTRAINT base_anes_airway_pkey PRIMARY KEY (base_anes_airway_id);


--
-- Name: base_anes_anesthetic_agent_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_anes_anesthetic_agent
    ADD CONSTRAINT base_anes_anesthetic_agent_pkey PRIMARY KEY (base_anes_anesthetic_agent_id);


--
-- Name: base_anes_complication_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_anes_complication
    ADD CONSTRAINT base_anes_complication_pkey PRIMARY KEY (base_anes_complication_id);


--
-- Name: base_anes_cvs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_anes_cvs
    ADD CONSTRAINT base_anes_cvs_pkey PRIMARY KEY (base_anes_cvs_id);


--
-- Name: base_anes_inhalation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_anes_inhalation
    ADD CONSTRAINT base_anes_inhalation_pkey PRIMARY KEY (base_anes_inhalation_id);


--
-- Name: base_anes_local_agent_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_anes_local_agent
    ADD CONSTRAINT base_anes_local_agent_pkey PRIMARY KEY (base_anes_local_agent_id);


--
-- Name: base_anes_monitor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_anes_monitor
    ADD CONSTRAINT base_anes_monitor_pkey PRIMARY KEY (base_anes_monitor_id);


--
-- Name: base_anes_outcome_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_anes_outcome
    ADD CONSTRAINT base_anes_outcome_pkey PRIMARY KEY (base_anes_outcome_id);


--
-- Name: base_anes_pre_op_condition_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_anes_pre_op_condition_group
    ADD CONSTRAINT base_anes_pre_op_condition_group_pkey PRIMARY KEY (base_anes_pre_op_condition_group_id);


--
-- Name: base_anes_pre_op_condition_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_anes_pre_op_condition
    ADD CONSTRAINT base_anes_pre_op_condition_pkey PRIMARY KEY (base_anes_pre_op_condition_id);


--
-- Name: base_anes_premedication_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_anes_premedication
    ADD CONSTRAINT base_anes_premedication_pkey PRIMARY KEY (base_anes_premedication_id);


--
-- Name: base_anes_service_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_anes_service
    ADD CONSTRAINT base_anes_service_pkey PRIMARY KEY (base_anes_service_id);


--
-- Name: base_anes_special_tech_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_anes_special_tech
    ADD CONSTRAINT base_anes_special_tech_pkey PRIMARY KEY (base_anes_special_tech_id);


--
-- Name: base_anes_technique_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_anes_technique
    ADD CONSTRAINT base_anes_technique_pkey PRIMARY KEY (base_anes_technique_id);


--
-- Name: base_antibiotic_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_antibiotic
    ADD CONSTRAINT base_antibiotic_pkey PRIMARY KEY (base_antibiotic_id);


--
-- Name: base_any_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_any
    ADD CONSTRAINT base_any_pkey PRIMARY KEY (base_any_id);


--
-- Name: base_aw_result_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_aw_result
    ADD CONSTRAINT base_aw_result_pkey PRIMARY KEY (base_aw_result_id);


--
-- Name: base_bad_film_reason_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_bad_film_reason
    ADD CONSTRAINT base_bad_film_reason_pkey PRIMARY KEY (base_bad_film_reason_id);


--
-- Name: base_bank_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_bank
    ADD CONSTRAINT base_bank_pkey PRIMARY KEY (base_bank_id);


--
-- Name: base_bb_blood_transfusion_reason_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_bb_blood_transfusion_reason
    ADD CONSTRAINT base_bb_blood_transfusion_reason_pkey PRIMARY KEY (base_bb_blood_transfusion_reason_id);


--
-- Name: base_bb_issue_status_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_bb_issue_status
    ADD CONSTRAINT base_bb_issue_status_pkey PRIMARY KEY (base_bb_issue_status_id);


--
-- Name: base_bb_packcell_wastage_reason_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_bb_packcell_wastage_reason
    ADD CONSTRAINT base_bb_packcell_wastage_reason_pkey PRIMARY KEY (base_bb_packcell_wastage_reason_id);


--
-- Name: base_bb_xm_result_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_bb_xm_result_detail
    ADD CONSTRAINT base_bb_xm_result_detail_pkey PRIMARY KEY (base_bb_xm_result_detail_id);


--
-- Name: base_bb_xm_result_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_bb_xm_result
    ADD CONSTRAINT base_bb_xm_result_pkey PRIMARY KEY (base_bb_xm_result_id);


--
-- Name: base_billing_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_billing_group
    ADD CONSTRAINT base_billing_group_pkey PRIMARY KEY (base_billing_group_id);


--
-- Name: base_blood_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_blood_group
    ADD CONSTRAINT base_blood_group_pkey PRIMARY KEY (base_blood_group_id);


--
-- Name: base_branch_bank_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_branch_bank
    ADD CONSTRAINT base_branch_bank_pkey PRIMARY KEY (base_branch_bank_id);


--
-- Name: base_cancel_appointment_reason_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_cancel_appointment_reason
    ADD CONSTRAINT base_cancel_appointment_reason_pkey PRIMARY KEY (base_cancel_appointment_reason_id);


--
-- Name: base_cancel_receipt_reason_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_cancel_receipt_reason
    ADD CONSTRAINT base_cancel_receipt_reason_pkey PRIMARY KEY (base_cancel_receipt_reason_id);


--
-- Name: base_category_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_category_group
    ADD CONSTRAINT base_category_group_pkey PRIMARY KEY (base_category_group_id);


--
-- Name: base_clinic_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_clinic
    ADD CONSTRAINT base_clinic_pkey PRIMARY KEY (base_clinic_id);


--
-- Name: base_consult_case_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_consult_case
    ADD CONSTRAINT base_consult_case_pkey PRIMARY KEY (base_consult_case_id);


--
-- Name: base_consult_result_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_consult_result
    ADD CONSTRAINT base_consult_result_pkey PRIMARY KEY (base_consult_result_id);


--
-- Name: base_consult_treatment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_consult_treatment
    ADD CONSTRAINT base_consult_treatment_pkey PRIMARY KEY (base_consult_treatment_id);


--
-- Name: base_country_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_country
    ADD CONSTRAINT base_country_pkey PRIMARY KEY (base_country_id);


--
-- Name: base_credit_card_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_credit_card
    ADD CONSTRAINT base_credit_card_pkey PRIMARY KEY (base_credit_card_id);


--
-- Name: base_custom_print_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_custom_print
    ADD CONSTRAINT base_custom_print_pkey PRIMARY KEY (base_custom_print_id);


--
-- Name: base_custom_tab_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_custom_tab
    ADD CONSTRAINT base_custom_tab_pkey PRIMARY KEY (base_custom_tab_id);


--
-- Name: base_death_status_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_death_status
    ADD CONSTRAINT base_death_status_pkey PRIMARY KEY (base_death_status_id);


--
-- Name: base_deliver_document_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_deliver_document
    ADD CONSTRAINT base_deliver_document_pkey PRIMARY KEY (base_deliver_document_id);


--
-- Name: base_dent_diag_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_dent_diag_detail
    ADD CONSTRAINT base_dent_diag_detail_pkey PRIMARY KEY (base_dent_diag_detail_id);


--
-- Name: base_dent_diagnosis_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_dent_diagnosis
    ADD CONSTRAINT base_dent_diagnosis_pkey PRIMARY KEY (base_dent_diagnosis_id);


--
-- Name: base_dent_operation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_dent_operation
    ADD CONSTRAINT base_dent_operation_pkey PRIMARY KEY (base_dent_operation_id);


--
-- Name: base_dent_organic_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_dent_organic
    ADD CONSTRAINT base_dent_organic_pkey PRIMARY KEY (base_dent_organic_id);


--
-- Name: base_department_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_department
    ADD CONSTRAINT base_department_pkey PRIMARY KEY (base_department_id);


--
-- Name: base_deposit_bank_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_deposit_bank
    ADD CONSTRAINT base_deposit_bank_pkey PRIMARY KEY (base_deposit_bank_id);


--
-- Name: base_df_mode_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_df_mode
    ADD CONSTRAINT base_df_mode_pkey PRIMARY KEY (base_df_mode_id);


--
-- Name: base_diagnosis505_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_diagnosis505
    ADD CONSTRAINT base_diagnosis505_pkey PRIMARY KEY (base_diagnosis505_id);


--
-- Name: base_diagnosis506_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_diagnosis506
    ADD CONSTRAINT base_diagnosis506_pkey PRIMARY KEY (base_diagnosis506_id);


--
-- Name: base_diagnosis_employee_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_diagnosis_employee
    ADD CONSTRAINT base_diagnosis_employee_pkey PRIMARY KEY (base_diagnosis_employee_id);


--
-- Name: base_diagnosis_set_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_diagnosis_set
    ADD CONSTRAINT base_diagnosis_set_pkey PRIMARY KEY (base_diagnosis_set_id);


--
-- Name: base_disc_command_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_disc_command
    ADD CONSTRAINT base_disc_command_pkey PRIMARY KEY (base_disc_command_id);


--
-- Name: base_disc_special_card_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_disc_special_card
    ADD CONSTRAINT base_disc_special_card_pkey PRIMARY KEY (base_disc_special_card_id);


--
-- Name: base_discount_reason_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_discount_reason
    ADD CONSTRAINT base_discount_reason_pkey PRIMARY KEY (base_discount_reason_id);


--
-- Name: base_doctor_discharge_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_doctor_discharge_type
    ADD CONSTRAINT base_doctor_discharge_type_pkey PRIMARY KEY (base_doctor_discharge_type_id);


--
-- Name: base_dose_unit_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_dose_unit
    ADD CONSTRAINT base_dose_unit_pkey PRIMARY KEY (base_dose_unit_id);


--
-- Name: base_drug_caution_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_drug_caution
    ADD CONSTRAINT base_drug_caution_pkey PRIMARY KEY (base_drug_caution_id);


--
-- Name: base_drug_dose_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_drug_dose
    ADD CONSTRAINT base_drug_dose_pkey PRIMARY KEY (base_drug_dose_id);


--
-- Name: base_drug_format_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_drug_format
    ADD CONSTRAINT base_drug_format_pkey PRIMARY KEY (base_drug_format_id);


--
-- Name: base_drug_frequency_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_drug_frequency
    ADD CONSTRAINT base_drug_frequency_pkey PRIMARY KEY (base_drug_frequency_id);


--
-- Name: base_drug_generic_group_name_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_drug_generic_group_name
    ADD CONSTRAINT base_drug_generic_group_name_pkey PRIMARY KEY (base_drug_name_id);


--
-- Name: base_drug_generic_name_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_drug_generic_name
    ADD CONSTRAINT base_drug_generic_name_pkey PRIMARY KEY (base_drug_generic_name);


--
-- Name: base_drug_generic_trade_name_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_drug_generic_trade_name
    ADD CONSTRAINT base_drug_generic_trade_name_pkey PRIMARY KEY (base_drug_name_id);


--
-- Name: base_drug_group_name_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_drug_group_name
    ADD CONSTRAINT base_drug_group_name_pkey PRIMARY KEY (base_drug_group_name);


--
-- Name: base_drug_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_drug_group
    ADD CONSTRAINT base_drug_group_pkey PRIMARY KEY (base_drug_group_id);


--
-- Name: base_drug_instruction_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_drug_instruction
    ADD CONSTRAINT base_drug_instruction_pkey PRIMARY KEY (base_drug_instruction_id);


--
-- Name: base_drug_interaction_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_drug_interaction
    ADD CONSTRAINT base_drug_interaction_pkey PRIMARY KEY (base_drug_interaction_id);


--
-- Name: base_drug_major_class_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_drug_major_class
    ADD CONSTRAINT base_drug_major_class_pkey PRIMARY KEY (base_drug_major_class_id);


--
-- Name: base_drug_pain_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_drug_pain
    ADD CONSTRAINT base_drug_pain_pkey PRIMARY KEY (base_drug_pain_id);


--
-- Name: base_drug_pregnancy_risk_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_drug_pregnancy_risk
    ADD CONSTRAINT base_drug_pregnancy_risk_pkey PRIMARY KEY (base_drug_pregnancy_risk_id);


--
-- Name: base_drug_special_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_drug_special
    ADD CONSTRAINT base_drug_special_pkey PRIMARY KEY (base_drug_special_id);


--
-- Name: base_drug_std_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_drug_std
    ADD CONSTRAINT base_drug_std_pkey PRIMARY KEY (base_drug_std_id);


--
-- Name: base_drug_sub_class_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_drug_sub_class
    ADD CONSTRAINT base_drug_sub_class_pkey PRIMARY KEY (base_drug_sub_class_id);


--
-- Name: base_drug_time_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_drug_time
    ADD CONSTRAINT base_drug_time_pkey PRIMARY KEY (base_drug_time_id);


--
-- Name: base_drug_trade_name_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_drug_trade_name
    ADD CONSTRAINT base_drug_trade_name_pkey PRIMARY KEY (base_drug_trade_name);


--
-- Name: base_drug_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_drug_type
    ADD CONSTRAINT base_drug_type_pkey PRIMARY KEY (base_drug_type_id);


--
-- Name: base_drug_usage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_drug_usage
    ADD CONSTRAINT base_drug_usage_pkey PRIMARY KEY (base_drug_usage_id);


--
-- Name: base_dt_diagnosis_default_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_dt_diagnosis_default
    ADD CONSTRAINT base_dt_diagnosis_default_pkey PRIMARY KEY (base_dt_diagnosis_default_id);


--
-- Name: base_dt_diagnosis_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_dt_diagnosis
    ADD CONSTRAINT base_dt_diagnosis_pkey PRIMARY KEY (base_dt_diagnosis_id);


--
-- Name: base_dt_item_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_dt_item
    ADD CONSTRAINT base_dt_item_pkey PRIMARY KEY (base_dt_item_id);


--
-- Name: base_dt_symbol_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_dt_symbol
    ADD CONSTRAINT base_dt_symbol_pkey PRIMARY KEY (base_dt_symbol_id);


--
-- Name: base_dt_tx_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_dt_tx_type
    ADD CONSTRAINT base_dt_tx_type_pkey PRIMARY KEY (base_dt_tx_type_id);


--
-- Name: base_employee_role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_employee_role
    ADD CONSTRAINT base_employee_role_pkey PRIMARY KEY (base_employee_role_id);


--
-- Name: base_er_victim_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_er_victim_type
    ADD CONSTRAINT base_er_victim_type_pkey PRIMARY KEY (base_er_victim_type_id);


--
-- Name: base_forensic_death_behaviour_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_forensic_death_behaviour
    ADD CONSTRAINT base_forensic_death_behaviour_pkey PRIMARY KEY (base_forensic_death_behaviour_id);


--
-- Name: base_forensic_in_exam_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_forensic_in_exam_detail
    ADD CONSTRAINT base_forensic_in_exam_detail_pkey PRIMARY KEY (base_forensic_in_exam_detail_id);


--
-- Name: base_forensic_in_exam_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_forensic_in_exam_group
    ADD CONSTRAINT base_forensic_in_exam_group_pkey PRIMARY KEY (base_forensic_in_exam_group_id);


--
-- Name: base_forensic_out_exam_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_forensic_out_exam
    ADD CONSTRAINT base_forensic_out_exam_pkey PRIMARY KEY (base_forensic_out_exam_id);


--
-- Name: base_generation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_generation
    ADD CONSTRAINT base_generation_pkey PRIMARY KEY (base_generation_id);


--
-- Name: base_group_practice_employee_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_group_practice_doctor
    ADD CONSTRAINT base_group_practice_employee_pkey PRIMARY KEY (base_group_practice_doctor_id);


--
-- Name: base_group_practice_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_group_practice
    ADD CONSTRAINT base_group_practice_pkey PRIMARY KEY (base_group_practice_id);


--
-- Name: base_hp_checkup_sugges_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_hp_checkup_sugges
    ADD CONSTRAINT base_hp_checkup_sugges_pkey PRIMARY KEY (base_hp_checkup_sugges_id);


--
-- Name: base_intake_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_intake
    ADD CONSTRAINT base_intake_pkey PRIMARY KEY (base_intake_id);


--
-- Name: base_invoice_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_invoice_group
    ADD CONSTRAINT base_invoice_group_pkey PRIMARY KEY (base_invoice_group_id);


--
-- Name: base_item_set_sub_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_item_set_sub_group
    ADD CONSTRAINT base_item_set_sub_group_pkey PRIMARY KEY (base_item_set_sub_group_id);


--
-- Name: base_item_xray_film_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_item_xray_film
    ADD CONSTRAINT base_item_xray_film_pkey PRIMARY KEY (base_item_xray_film_id);


--
-- Name: base_lab_ignore_report_reason_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_lab_ignore_report_reason
    ADD CONSTRAINT base_lab_ignore_report_reason_pkey PRIMARY KEY (base_lab_ignore_report_reason_id);


--
-- Name: base_lab_micro_agent_set_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_lab_micro_agent_set
    ADD CONSTRAINT base_lab_micro_agent_set_pkey PRIMARY KEY (base_lab_micro_agent_set_id);


--
-- Name: base_lab_seq_ln_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_lab_seq_ln_type
    ADD CONSTRAINT base_lab_seq_ln_type_pkey PRIMARY KEY (base_lab_seq_ln_type_id);


--
-- Name: base_lab_service_point_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_lab_service_point
    ADD CONSTRAINT base_lab_service_point_pkey PRIMARY KEY (base_lab_service_point_id);


--
-- Name: base_lab_thalas_item_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_lab_thalas_item
    ADD CONSTRAINT base_lab_thalas_item_pkey PRIMARY KEY (base_lab_thalas_item_id);


--
-- Name: base_lab_thalas_result_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_lab_thalas_result
    ADD CONSTRAINT base_lab_thalas_result_pkey PRIMARY KEY (base_lab_thalas_result_id);


--
-- Name: base_lab_tube_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_lab_tube
    ADD CONSTRAINT base_lab_tube_pkey PRIMARY KEY (base_lab_tube_id);


--
-- Name: base_lab_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_lab_type
    ADD CONSTRAINT base_lab_type_pkey PRIMARY KEY (base_lab_type_id);


--
-- Name: base_ldap_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_ldap
    ADD CONSTRAINT base_ldap_pkey PRIMARY KEY (base_ldap_id);


--
-- Name: base_lis_machine_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_lis_automate
    ADD CONSTRAINT base_lis_machine_pkey PRIMARY KEY (base_lis_automate_id);


--
-- Name: base_lis_test_sort_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_lis_test_sort
    ADD CONSTRAINT base_lis_test_sort_pkey PRIMARY KEY (base_lis_test_sort_id);


--
-- Name: base_long_waiting_reason_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_long_waiting_reason
    ADD CONSTRAINT base_long_waiting_reason_pkey PRIMARY KEY (base_long_waiting_reason_id);


--
-- Name: base_lr_child_health_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_lr_child_health
    ADD CONSTRAINT base_lr_child_health_pkey PRIMARY KEY (base_lr_child_health_id);


--
-- Name: base_lr_delivery_abnormal_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_lr_delivery_abnormal
    ADD CONSTRAINT base_lr_delivery_abnormal_pkey PRIMARY KEY (base_lr_delivery_abnormal_id);


--
-- Name: base_lr_delivery_medication_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_lr_delivery_medication
    ADD CONSTRAINT base_lr_delivery_medication_pkey PRIMARY KEY (base_lr_delivery_medication_id);


--
-- Name: base_lr_delivery_method_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_lr_delivery_method
    ADD CONSTRAINT base_lr_delivery_method_pkey PRIMARY KEY (base_lr_delivery_method_id);


--
-- Name: base_lr_episiotomy_degree_of_tear_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_lr_episiotomy_degree_of_tear
    ADD CONSTRAINT base_lr_episiotomy_degree_of_tear_pkey PRIMARY KEY (base_lr_episiotomy_degree_of_tear_id);


--
-- Name: base_lr_episiotomy_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_lr_episiotomy_type
    ADD CONSTRAINT base_lr_episiotomy_type_pkey PRIMARY KEY (base_lr_episiotomy_type_id);


--
-- Name: base_lr_exam_organ_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_lr_exam_organ
    ADD CONSTRAINT base_lr_exam_organ_pkey PRIMARY KEY (base_lr_exam_organ_id);


--
-- Name: base_lr_fetal_present_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_lr_fetal_present
    ADD CONSTRAINT base_lr_fetal_present_pkey PRIMARY KEY (base_lr_fetal_present_id);


--
-- Name: base_lr_fluid_appearance_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_lr_fluid_appearance
    ADD CONSTRAINT base_lr_fluid_appearance_pkey PRIMARY KEY (base_lr_fluid_appearance_id);


--
-- Name: base_lr_fundal_ht_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_lr_fundal_ht
    ADD CONSTRAINT base_lr_fundal_ht_pkey PRIMARY KEY (base_lr_fundal_ht_id);


--
-- Name: base_lr_ga_deliver_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_lr_ga_deliver
    ADD CONSTRAINT base_lr_ga_deliver_pkey PRIMARY KEY (base_lr_ga_deliver_id);


--
-- Name: base_lr_laceration_specify_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_lr_laceration_specify
    ADD CONSTRAINT base_lr_laceration_specify_pkey PRIMARY KEY (base_lr_laceration_specify_id);


--
-- Name: base_lr_newborn_medication_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_lr_newborn_medication
    ADD CONSTRAINT base_lr_newborn_medication_pkey PRIMARY KEY (base_lr_newborn_medication_id);


--
-- Name: base_lr_operation_method_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_lr_operation_method
    ADD CONSTRAINT base_lr_operation_method_pkey PRIMARY KEY (base_lr_operation_method_id);


--
-- Name: base_lr_placenta_delivery_method_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_lr_placenta_delivery_method
    ADD CONSTRAINT base_lr_placenta_delivery_method_pkey PRIMARY KEY (base_lr_placenta_delivery_method_id);


--
-- Name: base_lr_postpartum_abnormal_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_lr_postpartum_abnormal
    ADD CONSTRAINT base_lr_postpartum_abnormal_pkey PRIMARY KEY (base_lr_postpartum_abnormal_id);


--
-- Name: base_lr_pregnancy_abnormal_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_lr_pregnancy_abnormal
    ADD CONSTRAINT base_lr_pregnancy_abnormal_pkey PRIMARY KEY (base_lr_pregnancy_abnormal_id);


--
-- Name: base_lr_pregnancy_risk_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_lr_pregnancy_risk
    ADD CONSTRAINT base_lr_pregnancy_risk_pkey PRIMARY KEY (base_lr_pregnancy_risk_id);


--
-- Name: base_lr_resuscitation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_lr_resuscitation
    ADD CONSTRAINT base_lr_resuscitation_pkey PRIMARY KEY (base_lr_resuscitation_id);


--
-- Name: base_lr_risk_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_lr_risk
    ADD CONSTRAINT base_lr_risk_pkey PRIMARY KEY (base_lr_risk_id);


--
-- Name: base_lr_spont_position_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_lr_spont_position
    ADD CONSTRAINT base_lr_spont_position_pkey PRIMARY KEY (base_lr_spont_position_id);


--
-- Name: base_lr_umbilical_cord_insertion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_lr_umbilical_cord_insertion
    ADD CONSTRAINT base_lr_umbilical_cord_insertion_pkey PRIMARY KEY (base_lr_umbilical_cord_insertion_id);


--
-- Name: base_lr_us_gynae_exam_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_lr_us_gynae_exam
    ADD CONSTRAINT base_lr_us_gynae_exam_pkey PRIMARY KEY (base_lr_us_gynae_exam_id);


--
-- Name: base_lr_us_obs_exam_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_lr_us_obs_exam
    ADD CONSTRAINT base_lr_us_obs_exam_pkey PRIMARY KEY (base_lr_us_obs_exam_id);


--
-- Name: base_med_department_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_med_department
    ADD CONSTRAINT base_med_department_pkey PRIMARY KEY (base_med_department_id);


--
-- Name: base_med_device_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_med_device
    ADD CONSTRAINT base_med_device_pkey PRIMARY KEY (base_med_device_id);


--
-- Name: base_med_device_sub_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_med_device_sub_type
    ADD CONSTRAINT base_med_device_sub_type_pkey PRIMARY KEY (base_med_device_sub_type_id);


--
-- Name: base_micro_result_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_micro_result
    ADD CONSTRAINT base_micro_result_pkey PRIMARY KEY (base_micro_result_id);


--
-- Name: base_narcotic_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_narcotic
    ADD CONSTRAINT base_narcotic_pkey PRIMARY KEY (base_narcotic_id);


--
-- Name: base_nt_nutrition_facts_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_nt_nutrition_facts
    ADD CONSTRAINT base_nt_nutrition_facts_pkey PRIMARY KEY (base_nt_nutrition_facts_id);


--
-- Name: base_nt_special_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_nt_special_group
    ADD CONSTRAINT base_nt_special_group_pkey PRIMARY KEY (base_nt_special_group_id);


--
-- Name: base_nt_supplement_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_nt_supplement_type
    ADD CONSTRAINT base_nt_supplement_type_pkey PRIMARY KEY (base_nt_supplement_type_id);


--
-- Name: base_nt_therapeutic_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_nt_therapeutic_type
    ADD CONSTRAINT base_nt_therapeutic_type_pkey PRIMARY KEY (base_nt_therapeutic_type_id);


--
-- Name: base_nt_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_nt_type
    ADD CONSTRAINT base_nt_type_pkey PRIMARY KEY (base_nt_type_id);


--
-- Name: base_office_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_office
    ADD CONSTRAINT base_office_pkey PRIMARY KEY (base_office_id);


--
-- Name: base_official_holiday_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_official_holiday
    ADD CONSTRAINT base_official_holiday_pkey PRIMARY KEY (base_official_holiday_id);


--
-- Name: base_op_anes_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_op_anes_type
    ADD CONSTRAINT base_op_anes_type_pkey PRIMARY KEY (base_op_anes_type_id);


--
-- Name: base_op_clinic_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_op_clinic
    ADD CONSTRAINT base_op_clinic_pkey PRIMARY KEY (base_op_clinic_id);


--
-- Name: base_op_delay_reason_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_op_delay_reason
    ADD CONSTRAINT base_op_delay_reason_pkey PRIMARY KEY (base_op_delay_reason_id);


--
-- Name: base_op_operation_posture_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_op_operation_posture
    ADD CONSTRAINT base_op_operation_posture_pkey PRIMARY KEY (base_op_operation_posture_id);


--
-- Name: base_op_role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_op_role
    ADD CONSTRAINT base_op_role_pkey PRIMARY KEY (base_op_role_id);


--
-- Name: base_op_room_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_op_room
    ADD CONSTRAINT base_op_room_pkey PRIMARY KEY (base_op_room_id);


--
-- Name: base_op_set_extra_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_op_set_extra
    ADD CONSTRAINT base_op_set_extra_pkey PRIMARY KEY (base_op_set_extra_id);


--
-- Name: base_op_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_op_type
    ADD CONSTRAINT base_op_type_pkey PRIMARY KEY (base_op_type_id);


--
-- Name: base_op_wound_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_op_wound
    ADD CONSTRAINT base_op_wound_pkey PRIMARY KEY (base_op_wound_id);


--
-- Name: base_operation_employee_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_operation_employee
    ADD CONSTRAINT base_operation_employee_pkey PRIMARY KEY (base_operation_employee_id);


--
-- Name: base_operation_set_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_operation_set
    ADD CONSTRAINT base_operation_set_pkey PRIMARY KEY (base_operation_set_id);


--
-- Name: base_order_drug_reason_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_order_drug_reason
    ADD CONSTRAINT base_order_drug_reason_pkey PRIMARY KEY (base_order_drug_reason_id);


--
-- Name: base_organic_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_organic
    ADD CONSTRAINT base_organic_pkey PRIMARY KEY (base_organic_id);


--
-- Name: base_organisms_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_organisms
    ADD CONSTRAINT base_organisms_pkey PRIMARY KEY (base_organisms);


--
-- Name: base_other_allergy_level_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_other_allergy_level
    ADD CONSTRAINT base_other_allergy_level_pkey PRIMARY KEY (base_other_allergy_level_id);


--
-- Name: base_other_allergy_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_other_allergy
    ADD CONSTRAINT base_other_allergy_pkey PRIMARY KEY (base_other_allergy_id);


--
-- Name: base_out_doctor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_out_doctor
    ADD CONSTRAINT base_out_doctor_pkey PRIMARY KEY (base_out_doctor_id);


--
-- Name: base_output_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_output
    ADD CONSTRAINT base_output_pkey PRIMARY KEY (base_output_id);


--
-- Name: base_paid_method_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_paid_method
    ADD CONSTRAINT base_paid_method_pkey PRIMARY KEY (base_paid_method_id);


--
-- Name: base_patho_contraceptive_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_patho_contraceptive
    ADD CONSTRAINT base_patho_contraceptive_pkey PRIMARY KEY (base_patho_contraceptive_id);


--
-- Name: base_patho_hormonal_status_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_patho_hormonal_status
    ADD CONSTRAINT base_patho_hormonal_status_pkey PRIMARY KEY (base_patho_hormonal_status_id);


--
-- Name: base_patho_snomed_morphology_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_patho_snomed_morphology
    ADD CONSTRAINT base_patho_snomed_morphology_pkey PRIMARY KEY (base_patho_snomed_morphology_id);


--
-- Name: base_patho_snomed_topography_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_patho_snomed_topography
    ADD CONSTRAINT base_patho_snomed_topography_pkey PRIMARY KEY (base_patho_snomed_topography_id);


--
-- Name: base_patho_stain_routine_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_patho_stain_routine
    ADD CONSTRAINT base_patho_stain_routine_pkey PRIMARY KEY (base_patho_stain_routine_id);


--
-- Name: base_patient_classification_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_patient_classification
    ADD CONSTRAINT base_patient_classification_pkey PRIMARY KEY (base_patient_classification_id);


--
-- Name: base_patient_classify_determiners_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_patient_classify_determiners
    ADD CONSTRAINT base_patient_classify_determiners_pkey PRIMARY KEY (base_patient_classify_determiners_id);


--
-- Name: base_patient_classify_standard_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_patient_classify_standard
    ADD CONSTRAINT base_patient_classify_standard_pkey PRIMARY KEY (base_patient_classify_standard_id);


--
-- Name: base_patient_file_location_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_patient_file_location
    ADD CONSTRAINT base_patient_file_location_pkey PRIMARY KEY (base_patient_file_location_id);


--
-- Name: base_patient_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_patient_group
    ADD CONSTRAINT base_patient_group_pkey PRIMARY KEY (base_patient_group_id);


--
-- Name: base_patient_image_folder_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_patient_image_folder
    ADD CONSTRAINT base_patient_image_folder_pkey PRIMARY KEY (base_patient_image_folder_id);


--
-- Name: base_patient_media_perspective_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_patient_media_perspective
    ADD CONSTRAINT base_patient_media_perspective_pkey PRIMARY KEY (base_patient_media_perspective_id);


--
-- Name: base_patient_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_patient_type
    ADD CONSTRAINT base_patient_type_pkey PRIMARY KEY (base_patient_type_id);


--
-- Name: base_patient_type_special_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_patient_type_special
    ADD CONSTRAINT base_patient_type_special_pkey PRIMARY KEY (base_patient_type_special_id);


--
-- Name: base_patient_unit_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_patient_unit
    ADD CONSTRAINT base_patient_unit_pkey PRIMARY KEY (base_patient_unit_id);


--
-- Name: base_plan_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_plan_group
    ADD CONSTRAINT base_plan_group_pkey PRIMARY KEY (base_plan_group_id);


--
-- Name: base_police_station_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_police_station
    ADD CONSTRAINT base_police_station_pkey PRIMARY KEY (base_police_station_id);


--
-- Name: base_prename_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_prename
    ADD CONSTRAINT base_prename_pkey PRIMARY KEY (base_prename_id);


--
-- Name: base_receipt_category_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_receipt_category
    ADD CONSTRAINT base_receipt_category_pkey PRIMARY KEY (base_receipt_category_id);


--
-- Name: base_refer_out_reason_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_refer_out_reason
    ADD CONSTRAINT base_refer_out_reason_pkey PRIMARY KEY (base_refer_out_reason_id);


--
-- Name: base_religion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_religion
    ADD CONSTRAINT base_religion_pkey PRIMARY KEY (base_religion_id);


--
-- Name: base_room_item_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_room_item
    ADD CONSTRAINT base_room_item_pkey PRIMARY KEY (base_room_item_id);


--
-- Name: base_room_med_department_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_room_med_department
    ADD CONSTRAINT base_room_med_department_pkey PRIMARY KEY (base_room_med_department_id);


--
-- Name: base_room_observe_item_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_room_observe_item
    ADD CONSTRAINT base_room_observe_item_pkey PRIMARY KEY (base_room_observe_item_id);


--
-- Name: base_room_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_room
    ADD CONSTRAINT base_room_pkey PRIMARY KEY (base_room_id);


--
-- Name: base_room_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_room_type
    ADD CONSTRAINT base_room_type_pkey PRIMARY KEY (base_room_type_id);


--
-- Name: base_service_point_ip_address_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_service_point_ip_address
    ADD CONSTRAINT base_service_point_ip_address_pkey PRIMARY KEY (base_service_point_ip_address_id);


--
-- Name: base_service_point_patient_no_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_service_point_patient_no
    ADD CONSTRAINT base_service_point_patient_no_pkey PRIMARY KEY (base_service_point_patient_no_id);


--
-- Name: base_service_point_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_service_point
    ADD CONSTRAINT base_service_point_pkey PRIMARY KEY (base_service_point_id);


--
-- Name: base_service_point_role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_service_point_role
    ADD CONSTRAINT base_service_point_role_pkey PRIMARY KEY (base_service_point_role_id);


--
-- Name: base_site_app_server_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_site_app_server
    ADD CONSTRAINT base_site_app_server_pkey PRIMARY KEY (base_site_app_server_id);


--
-- Name: base_site_branch_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_site_branch
    ADD CONSTRAINT base_site_branch_pkey PRIMARY KEY (base_site_branch_id);


--
-- Name: base_site_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_site
    ADD CONSTRAINT base_site_pkey PRIMARY KEY (base_site_id);


--
-- Name: base_skin_color_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_skin_color
    ADD CONSTRAINT base_skin_color_pkey PRIMARY KEY (base_skin_color_id);


--
-- Name: base_special_nurse_care_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_special_nurse_care
    ADD CONSTRAINT base_special_nurse_care_pkey PRIMARY KEY (base_special_nurse_care_id);


--
-- Name: base_specimen_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_specimen
    ADD CONSTRAINT base_specimen_pkey PRIMARY KEY (base_specimen_id);


--
-- Name: base_sql_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_sql_group
    ADD CONSTRAINT base_sql_group_pkey PRIMARY KEY (base_sql_group_id);


--
-- Name: base_state_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_state
    ADD CONSTRAINT base_state_pkey PRIMARY KEY (base_state_id);


--
-- Name: base_stock_exchange_note_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_stock_exchange_note
    ADD CONSTRAINT base_stock_exchange_note_pkey PRIMARY KEY (base_stock_exchange_note_id);


--
-- Name: base_tariff_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_tariff
    ADD CONSTRAINT base_tariff_pkey PRIMARY KEY (base_tariff_id);


--
-- Name: base_template_advice_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_template_advice
    ADD CONSTRAINT base_template_advice_pkey PRIMARY KEY (base_template_advice_id);


--
-- Name: base_template_app_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_template_app_detail
    ADD CONSTRAINT base_template_app_detail_pkey PRIMARY KEY (base_template_app_detail_id);


--
-- Name: base_template_diagnosis_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_template_diagnosis
    ADD CONSTRAINT base_template_diagnosis_pkey PRIMARY KEY (base_template_diagnosis_id);


--
-- Name: base_template_doctor_note_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_template_doctor_note
    ADD CONSTRAINT base_template_doctor_note_pkey PRIMARY KEY (base_template_doctor_note_id);


--
-- Name: base_template_illness_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_template_illness
    ADD CONSTRAINT base_template_illness_pkey PRIMARY KEY (base_template_illness_id);


--
-- Name: base_template_item_set_multi_visit_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_template_item_set_multi_visit
    ADD CONSTRAINT base_template_item_set_multi_visit_pkey PRIMARY KEY (base_template_item_set_multi_visit_id);


--
-- Name: base_template_lab_result_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_template_lab_result
    ADD CONSTRAINT base_template_lab_result_pkey PRIMARY KEY (base_template_lab_result_id);


--
-- Name: base_template_mc_approve_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_template_mc_approve
    ADD CONSTRAINT base_template_mc_approve_pkey PRIMARY KEY (base_template_mc_approve_id);


--
-- Name: base_template_operation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_template_operation
    ADD CONSTRAINT base_template_operation_pkey PRIMARY KEY (base_template_operation_id);


--
-- Name: base_template_phyex_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_template_phyex
    ADD CONSTRAINT base_template_phyex_pkey PRIMARY KEY (base_template_phyex_id);


--
-- Name: base_template_symptom_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_template_symptom
    ADD CONSTRAINT base_template_symptom_pkey PRIMARY KEY (base_template_symptom_id);


--
-- Name: base_template_xray_result_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_template_xray_result
    ADD CONSTRAINT base_template_xray_result_pkey PRIMARY KEY (base_template_xray_result_id);


--
-- Name: base_time_stamp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_time_stamp
    ADD CONSTRAINT base_time_stamp_pkey PRIMARY KEY (base_time_stamp_id);


--
-- Name: base_unit_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_unit
    ADD CONSTRAINT base_unit_pkey PRIMARY KEY (base_unit_id);


--
-- Name: base_vs_coma_scale_eye_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_vs_coma_scale_eye
    ADD CONSTRAINT base_vs_coma_scale_eye_pkey PRIMARY KEY (base_vs_coma_scale_eye_id);


--
-- Name: base_vs_coma_scale_motion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_vs_coma_scale_motion
    ADD CONSTRAINT base_vs_coma_scale_motion_pkey PRIMARY KEY (base_vs_coma_scale_motion_id);


--
-- Name: base_vs_coma_scale_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_vs_coma_scale
    ADD CONSTRAINT base_vs_coma_scale_pkey PRIMARY KEY (base_vs_coma_scale_id);


--
-- Name: base_vs_coma_scale_verbal_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_vs_coma_scale_verbal
    ADD CONSTRAINT base_vs_coma_scale_verbal_pkey PRIMARY KEY (base_vs_coma_scale_verbal_id);


--
-- Name: base_vs_ext_advice_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_vs_ext_advice
    ADD CONSTRAINT base_vs_ext_advice_pkey PRIMARY KEY (base_vs_ext_advice_id);


--
-- Name: base_vs_motor_power_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_vs_motor_power
    ADD CONSTRAINT base_vs_motor_power_pkey PRIMARY KEY (base_vs_motor_power_id);


--
-- Name: base_vs_pupil_score_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_vs_pupil_score
    ADD CONSTRAINT base_vs_pupil_score_pkey PRIMARY KEY (base_vs_pupil_score_id);


--
-- Name: base_xray_film_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_xray_film
    ADD CONSTRAINT base_xray_film_pkey PRIMARY KEY (base_xray_film_id);


--
-- Name: base_xray_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.base_xray_type
    ADD CONSTRAINT base_xray_type_pkey PRIMARY KEY (base_xray_type_id);


--
-- Name: bb_base_product_pack_cell_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.bb_base_product_pack_cell
    ADD CONSTRAINT bb_base_product_pack_cell_pkey PRIMARY KEY (bb_base_product_pack_cell_id);


--
-- Name: bb_cross_matching_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.bb_cross_matching
    ADD CONSTRAINT bb_cross_matching_pkey PRIMARY KEY (bb_cross_matching_id);


--
-- Name: bb_donate_data_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.bb_donate_data
    ADD CONSTRAINT bb_donate_data_pkey PRIMARY KEY (bb_donate_data_id);


--
-- Name: bb_donor_social_data_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.bb_donor_social_data
    ADD CONSTRAINT bb_donor_social_data_pkey PRIMARY KEY (bb_donor_social_data_id);


--
-- Name: bb_pack_cell_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.bb_pack_cell
    ADD CONSTRAINT bb_pack_cell_pkey PRIMARY KEY (bb_pack_cell_id);


--
-- Name: bb_stock_card_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.bb_stock_card
    ADD CONSTRAINT bb_stock_card_pkey PRIMARY KEY (bb_stock_card_id);


--
-- Name: bed_booking_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.bed_booking
    ADD CONSTRAINT bed_booking_pkey PRIMARY KEY (bed_booking_id);


--
-- Name: bed_management_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.bed_management
    ADD CONSTRAINT bed_management_pkey PRIMARY KEY (bed_management_id);


--
-- Name: bed_mgnt_order_continue_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.bed_mgnt_order_continue
    ADD CONSTRAINT bed_mgnt_order_continue_pkey PRIMARY KEY (bed_mgnt_order_continue_id);


--
-- Name: connection_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.connection_profile
    ADD CONSTRAINT connection_profile_pkey PRIMARY KEY (connection_profile_id);


--
-- Name: constraint_hn; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.patient
    ADD CONSTRAINT constraint_hn UNIQUE (hn);


--
-- Name: consult_case_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.consult_case
    ADD CONSTRAINT consult_case_pkey PRIMARY KEY (consult_case_id);


--
-- Name: consult_case_result_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.consult_case_result
    ADD CONSTRAINT consult_case_result_pkey PRIMARY KEY (consult_case_result_id);


--
-- Name: consult_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.consult
    ADD CONSTRAINT consult_pkey PRIMARY KEY (consult_id);


--
-- Name: consult_treatment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.consult_treatment
    ADD CONSTRAINT consult_treatment_pkey PRIMARY KEY (consult_treatment_id);


--
-- Name: current_drug_use_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.current_drug_use
    ADD CONSTRAINT current_drug_use_pkey PRIMARY KEY (current_drug_use_id);


--
-- Name: custom_report_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.custom_report
    ADD CONSTRAINT custom_report_pkey PRIMARY KEY (id);


--
-- Name: daily_record_ipd_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.daily_record_ipd
    ADD CONSTRAINT daily_record_ipd_pkey PRIMARY KEY (daily_record_ipd_id);


--
-- Name: death_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.death
    ADD CONSTRAINT death_pkey PRIMARY KEY (death_id);


--
-- Name: dental_order_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.dental_order
    ADD CONSTRAINT dental_order_pkey PRIMARY KEY (dental_order_id);


--
-- Name: dental_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.dental
    ADD CONSTRAINT dental_pkey PRIMARY KEY (dental_id);


--
-- Name: dental_plan_order_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.dental_plan_order
    ADD CONSTRAINT dental_plan_order_pkey PRIMARY KEY (dental_plan_order_id);


--
-- Name: dental_teeth_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.dental_teeth
    ADD CONSTRAINT dental_teeth_pkey PRIMARY KEY (dental_teeth_id);


--
-- Name: department_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock_department
    ADD CONSTRAINT department_pkey PRIMARY KEY (stock_department_id);


--
-- Name: df_approve_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.df_approve
    ADD CONSTRAINT df_approve_pkey PRIMARY KEY (df_approve_id);


--
-- Name: df_order_item_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.df_order_item
    ADD CONSTRAINT df_order_item_pkey PRIMARY KEY (df_order_item_id);


--
-- Name: df_order_item_set_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.df_order_item_set
    ADD CONSTRAINT df_order_item_set_pkey PRIMARY KEY (df_order_item_set_id);


--
-- Name: diagnosis_icd10_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.diagnosis_icd10
    ADD CONSTRAINT diagnosis_icd10_pkey PRIMARY KEY (diagnosis_icd10_id);


--
-- Name: diagnosis_icd9_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.diagnosis_icd9
    ADD CONSTRAINT diagnosis_icd9_pkey PRIMARY KEY (diagnosis_icd9_id);


--
-- Name: dialysis_operator_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.dialysis_operator
    ADD CONSTRAINT dialysis_operator_pkey PRIMARY KEY (dialysis_operator_id);


--
-- Name: dialysis_registered_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.dialysis_registered
    ADD CONSTRAINT dialysis_registered_pkey PRIMARY KEY (dialysis_registered_id);


--
-- Name: distributor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.distributor
    ADD CONSTRAINT distributor_pkey PRIMARY KEY (distributor_id);


--
-- Name: doc_scan_template_message_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.doc_scan_template_message
    ADD CONSTRAINT doc_scan_template_message_pkey PRIMARY KEY (doc_scan_template_message_id);


--
-- Name: doctor_absent_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.doctor_absent
    ADD CONSTRAINT doctor_absent_pkey PRIMARY KEY (doctor_absent_id);


--
-- Name: doctor_diagnosis_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.doctor_diagnosis
    ADD CONSTRAINT doctor_diagnosis_pkey PRIMARY KEY (doctor_diagnosis_id);


--
-- Name: doctor_discharge_ipd_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.doctor_discharge_ipd
    ADD CONSTRAINT doctor_discharge_ipd_pkey PRIMARY KEY (doctor_discharge_ipd_id);


--
-- Name: doctor_discharge_opd_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.doctor_discharge_opd
    ADD CONSTRAINT doctor_discharge_opd_pkey PRIMARY KEY (doctor_discharge_opd_id);


--
-- Name: doctor_parttime_time_stamp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.doctor_parttime_time_stamp
    ADD CONSTRAINT doctor_parttime_time_stamp_pkey PRIMARY KEY (doctor_parttime_time_stamp_id);


--
-- Name: doctor_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.doctor_profile
    ADD CONSTRAINT doctor_profile_pkey PRIMARY KEY (doctor_profile_id);


--
-- Name: doctor_schedule_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.doctor_schedule
    ADD CONSTRAINT doctor_schedule_pkey PRIMARY KEY (doctor_schedule_id);


--
-- Name: document_scan_comments_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.document_scan_comments
    ADD CONSTRAINT document_scan_comments_pkey PRIMARY KEY (document_scan_comments_id);


--
-- Name: document_scan_drawing_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.document_scan_drawing
    ADD CONSTRAINT document_scan_drawing_pkey PRIMARY KEY (document_scan_drawing_id);


--
-- Name: document_scan_dx_date_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.document_scan_dx_date
    ADD CONSTRAINT document_scan_dx_date_pkey PRIMARY KEY (document_scan_dx_date_id);


--
-- Name: document_scan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.document_scan
    ADD CONSTRAINT document_scan_pkey PRIMARY KEY (document_scan_id);


--
-- Name: drawing_details_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.drawing_details
    ADD CONSTRAINT drawing_details_pkey PRIMARY KEY (drawing_details_id);


--
-- Name: drawing_physical_exam_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.drawing_physical_exam
    ADD CONSTRAINT drawing_physical_exam_pkey PRIMARY KEY (drawing_physical_exam_id);


--
-- Name: drug_allergy_naranjo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.drug_allergy_naranjo
    ADD CONSTRAINT drug_allergy_naranjo_pkey PRIMARY KEY (drug_allergy_naranjo_id);


--
-- Name: drug_allergy_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.drug_allergy
    ADD CONSTRAINT drug_allergy_pkey PRIMARY KEY (drug_allergy_id);


--
-- Name: drug_allergy_temp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.drug_allergy_temp
    ADD CONSTRAINT drug_allergy_temp_pkey PRIMARY KEY (drug_allergy_temp_id);


--
-- Name: drug_specialist_only_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.drug_specialist_only
    ADD CONSTRAINT drug_specialist_only_pkey PRIMARY KEY (drug_specialist_only_id);


--
-- Name: dt_diagnosis_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.dt_diagnosis
    ADD CONSTRAINT dt_diagnosis_pkey PRIMARY KEY (dt_diagnosis_id);


--
-- Name: dt_teeth_chart_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.dt_teeth_chart
    ADD CONSTRAINT dt_teeth_chart_pkey PRIMARY KEY (dt_teeth_chart_id);


--
-- Name: dt_treatment_external_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.dt_treatment_external
    ADD CONSTRAINT dt_treatment_external_pkey PRIMARY KEY (dt_treatment_external_id);


--
-- Name: dt_treatment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.dt_treatment
    ADD CONSTRAINT dt_treatment_pkey PRIMARY KEY (dt_treatment_id);


--
-- Name: dt_treatment_plan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.dt_treatment_plan
    ADD CONSTRAINT dt_treatment_plan_pkey PRIMARY KEY (dt_treatment_plan_id);


--
-- Name: emergency_accident_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.emergency_accident
    ADD CONSTRAINT emergency_accident_pkey PRIMARY KEY (emergency_accident_id);


--
-- Name: emergency_data_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.emergency_data
    ADD CONSTRAINT emergency_data_pkey PRIMARY KEY (emergency_data_id);


--
-- Name: emergency_vital_sign_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.emergency_vital_sign
    ADD CONSTRAINT emergency_vital_sign_pkey PRIMARY KEY (emergency_vital_sign_id);


--
-- Name: employee_department_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.employee_department
    ADD CONSTRAINT employee_department_pkey PRIMARY KEY (employee_department_id);


--
-- Name: employee_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.employee
    ADD CONSTRAINT employee_pkey PRIMARY KEY (employee_id);


--
-- Name: employee_report_role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.employee_report_role
    ADD CONSTRAINT employee_report_role_pkey PRIMARY KEY (employee_report_role_id);


--
-- Name: employee_role_group_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.employee_role_group_detail
    ADD CONSTRAINT employee_role_group_detail_pkey PRIMARY KEY (employee_role_group_detail_id);


--
-- Name: employee_role_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.employee_role_group
    ADD CONSTRAINT employee_role_group_pkey PRIMARY KEY (employee_role_group_id);


--
-- Name: employee_role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.employee_role
    ADD CONSTRAINT employee_role_pkey PRIMARY KEY (employee_role_id);


--
-- Name: employee_type_default_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.employee_type_default
    ADD CONSTRAINT employee_type_default_pkey PRIMARY KEY (employee_type_default_id);


--
-- Name: family_history_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.family_history
    ADD CONSTRAINT family_history_pkey PRIMARY KEY (family_history_id);


--
-- Name: feature_cash_doc_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.feature_cash_doc
    ADD CONSTRAINT feature_cash_doc_pkey PRIMARY KEY (feature_cash_doc_id);


--
-- Name: feature_doctor_assign_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.feature_doctor_assign
    ADD CONSTRAINT feature_doctor_assign_pkey PRIMARY KEY (feature_doctor_assign_id);


--
-- Name: feature_exception_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.feature_exception
    ADD CONSTRAINT feature_exception_pkey PRIMARY KEY (feature_exception_id);


--
-- Name: feature_option_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.feature_option
    ADD CONSTRAINT feature_option_pkey PRIMARY KEY (feature_option_id);


--
-- Name: feature_order_short_key_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.feature_order_short_key
    ADD CONSTRAINT feature_order_short_key_pkey PRIMARY KEY (feature_order_short_key_id);


--
-- Name: file4_chronic_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.file4_chronic
    ADD CONSTRAINT file4_chronic_pkey PRIMARY KEY (file4_chronic_id);


--
-- Name: file4_death_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.file4_death
    ADD CONSTRAINT file4_death_pkey PRIMARY KEY (file4_death_id);


--
-- Name: file4_promote_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.file4_promote
    ADD CONSTRAINT file4_promote_pkey PRIMARY KEY (file4_promote_id);


--
-- Name: file4_surveil_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.file4_surveil
    ADD CONSTRAINT file4_surveil_pkey PRIMARY KEY (file4_surveil_id);


--
-- Name: finger_print_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.finger_print
    ADD CONSTRAINT finger_print_pkey PRIMARY KEY (finger_print_id);


--
-- Name: fix_bed_status_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.fix_bed_status
    ADD CONSTRAINT fix_bed_status_pkey PRIMARY KEY (fix_bed_status_id);


--
-- Name: fix_dt_surface_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.fix_dt_surface
    ADD CONSTRAINT fix_dt_surface_pkey PRIMARY KEY (fix_dt_surface_id);


--
-- Name: fix_dt_tooth_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.fix_dt_tooth
    ADD CONSTRAINT fix_dt_tooth_pkey PRIMARY KEY (fix_dt_tooth_id);


--
-- Name: fix_dt_tooth_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.fix_dt_tooth_type
    ADD CONSTRAINT fix_dt_tooth_type_pkey PRIMARY KEY (fix_dt_tooth_type_id);


--
-- Name: fix_emergency_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.fix_emergency_type
    ADD CONSTRAINT fix_emergency_type_pkey PRIMARY KEY (fix_emergency_type_id);


--
-- Name: fix_film_status_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.fix_film_status
    ADD CONSTRAINT fix_film_status_pkey PRIMARY KEY (fix_film_status_id);


--
-- Name: fix_gender_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.fix_gender
    ADD CONSTRAINT fix_gender_pkey PRIMARY KEY (fix_gender_id);


--
-- Name: fix_icd10_4_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.fix_icd10_4
    ADD CONSTRAINT fix_icd10_4_pkey PRIMARY KEY (fix_icd10_4_id);


--
-- Name: fix_icd10_5_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.fix_icd10_5
    ADD CONSTRAINT fix_icd10_5_pkey PRIMARY KEY (fix_icd10_5_id);


--
-- Name: fix_icd10_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.fix_icd10
    ADD CONSTRAINT fix_icd10_pkey PRIMARY KEY (fix_icd10_id);


--
-- Name: fix_icd9_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.fix_icd9
    ADD CONSTRAINT fix_icd9_pkey PRIMARY KEY (fix_icd9_id);


--
-- Name: fix_idx10v3_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.fix_idx10v3
    ADD CONSTRAINT fix_idx10v3_pkey PRIMARY KEY (fix_idx10v3_id);


--
-- Name: fix_ipd_discharge_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.fix_ipd_discharge_type
    ADD CONSTRAINT fix_ipd_discharge_type_pkey PRIMARY KEY (fix_ipd_discharge_type_id);


--
-- Name: fix_item_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.fix_item_type
    ADD CONSTRAINT fix_item_type_pkey PRIMARY KEY (fix_item_type_id);


--
-- Name: fix_lab_normal_valeu_type_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.fix_lab_normal_value_type
    ADD CONSTRAINT fix_lab_normal_valeu_type_pk PRIMARY KEY (fix_lab_normal_value_type_id);


--
-- Name: fix_lab_test_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.fix_lab_test_type
    ADD CONSTRAINT fix_lab_test_type_pkey PRIMARY KEY (fix_lab_test_type_id);


--
-- Name: fix_marriage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.fix_marriage
    ADD CONSTRAINT fix_marriage_pkey PRIMARY KEY (fix_marriage_id);


--
-- Name: fix_nationality_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.fix_nationality
    ADD CONSTRAINT fix_nationality_pkey PRIMARY KEY (fix_nationality_id);


--
-- Name: fix_occupation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.fix_occupation
    ADD CONSTRAINT fix_occupation_pkey PRIMARY KEY (fix_occupation_id);


--
-- Name: fix_pindex99_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.fix_pindex99
    ADD CONSTRAINT fix_pindex99_pkey PRIMARY KEY (fix_pindex99_id);


--
-- Name: fix_race_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.fix_race
    ADD CONSTRAINT fix_race_pkey PRIMARY KEY (fix_race_id);


--
-- Name: fix_visit_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.fix_visit_type
    ADD CONSTRAINT fix_visit_type_pkey PRIMARY KEY (fix_visit_type_id);


--
-- Name: fix_xray_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.fix_xray_type
    ADD CONSTRAINT fix_xray_type_pkey PRIMARY KEY (fix_xray_type_id);


--
-- Name: forensic_data_forensic_number_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.forensic_data
    ADD CONSTRAINT forensic_data_forensic_number_key UNIQUE (forensic_number);


--
-- Name: forensic_data_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.forensic_data
    ADD CONSTRAINT forensic_data_pkey PRIMARY KEY (forensic_data_id);


--
-- Name: forensic_in_exam_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.forensic_in_exam
    ADD CONSTRAINT forensic_in_exam_pkey PRIMARY KEY (forensic_in_exam_id);


--
-- Name: forensic_out_exam_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.forensic_out_exam
    ADD CONSTRAINT forensic_out_exam_pkey PRIMARY KEY (forensic_out_exam_id);


--
-- Name: forensic_team_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.forensic_team
    ADD CONSTRAINT forensic_team_pkey PRIMARY KEY (forensic_team_id);


--
-- Name: freport_advice_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.freport_advice
    ADD CONSTRAINT freport_advice_pkey PRIMARY KEY (freport_advice_id);


--
-- Name: freport_appointstatus_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.freport_appointstatus
    ADD CONSTRAINT freport_appointstatus_pkey PRIMARY KEY (freport_appointstatus_id);


--
-- Name: freport_child_age_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.freport_child_age
    ADD CONSTRAINT freport_child_age_pkey PRIMARY KEY (freport_child_age_id);


--
-- Name: freport_doctor_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.freport_doctor_type
    ADD CONSTRAINT freport_doctor_type_pkey PRIMARY KEY (freport_doctor_type_id);


--
-- Name: freport_patient_age_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.freport_patient_age
    ADD CONSTRAINT freport_patient_age_pkey PRIMARY KEY (freport_patient_age_id);


--
-- Name: health_promotion_visit_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.health_promotion_visit
    ADD CONSTRAINT health_promotion_visit_pkey PRIMARY KEY (health_promotion_visit_id);


--
-- Name: health_promotion_xray_result_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.health_promotion_xray_result
    ADD CONSTRAINT health_promotion_xray_result_pkey PRIMARY KEY (health_promotion_xray_result_id);


--
-- Name: help_admit_physician_data_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.help_admit_physician_data
    ADD CONSTRAINT help_admit_physician_data_pkey PRIMARY KEY (help_admit_physician_data_id);


--
-- Name: hilo_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.hilosequences
    ADD CONSTRAINT hilo_pk PRIMARY KEY (sequencename);


--
-- Name: hist_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.hist
    ADD CONSTRAINT hist_pkey PRIMARY KEY (hist_id);


--
-- Name: hp_checkup_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.hp_checkup
    ADD CONSTRAINT hp_checkup_pkey PRIMARY KEY (hp_checkup_id);


--
-- Name: hp_checkup_sugges_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.hp_checkup_sugges
    ADD CONSTRAINT hp_checkup_sugges_pkey PRIMARY KEY (hp_checkup_sugges_id);


--
-- Name: hp_lab_commit_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.hp_lab_commit
    ADD CONSTRAINT hp_lab_commit_pkey PRIMARY KEY (hp_lab_commit_id);


--
-- Name: hp_medical_history_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.hp_medical_history
    ADD CONSTRAINT hp_medical_history_pkey PRIMARY KEY (hp_medical_history_id);


--
-- Name: imed_config_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.imed_config
    ADD CONSTRAINT imed_config_pkey PRIMARY KEY (imed_config_id);


--
-- Name: imed_language_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.imed_language
    ADD CONSTRAINT imed_language_pkey PRIMARY KEY (imed_language_id);


--
-- Name: imed_version_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.imed_version
    ADD CONSTRAINT imed_version_pkey PRIMARY KEY (imed_version_id);


--
-- Name: ipd_attending_physician_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.ipd_attending_physician
    ADD CONSTRAINT ipd_attending_physician_pkey PRIMARY KEY (ipd_attending_physician_id);


--
-- Name: ipensook_billing_group_category_mapping_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.ipensook_billing_group_category_mapping
    ADD CONSTRAINT ipensook_billing_group_category_mapping_pkey PRIMARY KEY (base_category_group_id);


--
-- Name: ipensook_interface_item_update_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.ipensook_interface_item_update
    ADD CONSTRAINT ipensook_interface_item_update_pkey PRIMARY KEY (id);


--
-- Name: ipensook_interface_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.ipensook_interface_log
    ADD CONSTRAINT ipensook_interface_log_pkey PRIMARY KEY (ipensook_interface_log_id);


--
-- Name: ipensook_interface_order_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.ipensook_interface_order
    ADD CONSTRAINT ipensook_interface_order_pkey PRIMARY KEY (imed_id);


--
-- Name: ipensook_item_category_mapping_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.ipensook_item_category_mapping
    ADD CONSTRAINT ipensook_item_category_mapping_pkey PRIMARY KEY (base_category_group_id);


--
-- Name: item_discount_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.item_discount
    ADD CONSTRAINT item_discount_pkey PRIMARY KEY (item_discount_id);


--
-- Name: item_dispensed_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.item_dispensed
    ADD CONSTRAINT item_dispensed_pkey PRIMARY KEY (item_dispense_id);


--
-- Name: item_image_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.item_image
    ADD CONSTRAINT item_image_pkey PRIMARY KEY (item_image_id);


--
-- Name: item_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.item
    ADD CONSTRAINT item_pkey PRIMARY KEY (item_id);


--
-- Name: item_plan_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.item_plan_group
    ADD CONSTRAINT item_plan_group_pkey PRIMARY KEY (item_plan_group_id);


--
-- Name: item_price_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.item_price
    ADD CONSTRAINT item_price_pkey PRIMARY KEY (item_price_id);


--
-- Name: item_repack_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.item_repack
    ADD CONSTRAINT item_repack_pkey PRIMARY KEY (item_repack_id);


--
-- Name: item_set_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.item_set
    ADD CONSTRAINT item_set_pkey PRIMARY KEY (item_set_id);


--
-- Name: item_set_print_out_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.item_set_print_out
    ADD CONSTRAINT item_set_print_out_pkey PRIMARY KEY (item_set_print_out_id);


--
-- Name: itemwetcha_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.itemwetcha
    ADD CONSTRAINT itemwetcha_pkey PRIMARY KEY (item_id);


--
-- Name: jbm_counter_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.jbm_counter
    ADD CONSTRAINT jbm_counter_pkey PRIMARY KEY (name);


--
-- Name: jbm_dual_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.jbm_dual
    ADD CONSTRAINT jbm_dual_pkey PRIMARY KEY (dummy);


--
-- Name: jbm_id_cache_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.jbm_id_cache
    ADD CONSTRAINT jbm_id_cache_pkey PRIMARY KEY (node_id, cntr);


--
-- Name: jbm_msg_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.jbm_msg
    ADD CONSTRAINT jbm_msg_pkey PRIMARY KEY (message_id);


--
-- Name: jbm_msg_ref_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.jbm_msg_ref
    ADD CONSTRAINT jbm_msg_ref_pkey PRIMARY KEY (message_id, channel_id);


--
-- Name: jbm_postoffice_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.jbm_postoffice
    ADD CONSTRAINT jbm_postoffice_pkey PRIMARY KEY (postoffice_name, node_id, queue_name);


--
-- Name: jbm_role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.jbm_role
    ADD CONSTRAINT jbm_role_pkey PRIMARY KEY (user_id, role_id);


--
-- Name: jbm_tx_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.jbm_tx
    ADD CONSTRAINT jbm_tx_pkey PRIMARY KEY (transaction_id);


--
-- Name: jbm_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.jbm_user
    ADD CONSTRAINT jbm_user_pkey PRIMARY KEY (user_id);


--
-- Name: lab_microbiology_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.lab_microbiology
    ADD CONSTRAINT lab_microbiology_pkey PRIMARY KEY (lab_microbiology_id);


--
-- Name: lab_result_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.lab_result
    ADD CONSTRAINT lab_result_pkey PRIMARY KEY (lab_result_id);


--
-- Name: lab_test_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.lab_test
    ADD CONSTRAINT lab_test_pkey PRIMARY KEY (lab_test_id);


--
-- Name: lab_thalas_result_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.lab_thalas_result
    ADD CONSTRAINT lab_thalas_result_pkey PRIMARY KEY (lab_thalas_result_id);


--
-- Name: lis_order_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.lis_order
    ADD CONSTRAINT lis_order_pkey PRIMARY KEY (lis_order_id);


--
-- Name: lis_req_res_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.lis_req_res
    ADD CONSTRAINT lis_req_res_pkey PRIMARY KEY (lis_req_res_id);


--
-- Name: location_record_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.location_record
    ADD CONSTRAINT location_record_pkey PRIMARY KEY (location_record_id);


--
-- Name: lr_anc_exam_organ_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.lr_anc_exam_organ
    ADD CONSTRAINT lr_anc_exam_organ_pkey PRIMARY KEY (lr_anc_exam_organ_id);


--
-- Name: lr_anc_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.lr_anc
    ADD CONSTRAINT lr_anc_pkey PRIMARY KEY (lr_anc_id);


--
-- Name: lr_apgar_score_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.lr_apgar_score
    ADD CONSTRAINT lr_apgar_score_pkey PRIMARY KEY (lr_apgar_score_id);


--
-- Name: lr_delivery_abnormal_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.lr_delivery_abnormal
    ADD CONSTRAINT lr_delivery_abnormal_pkey PRIMARY KEY (lr_delivery_abnormal_id);


--
-- Name: lr_delivery_infant_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.lr_delivery_infant
    ADD CONSTRAINT lr_delivery_infant_pkey PRIMARY KEY (lr_delivery_infant_id);


--
-- Name: lr_delivery_medication_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.lr_delivery_medication
    ADD CONSTRAINT lr_delivery_medication_pkey PRIMARY KEY (lr_delivery_medication_id);


--
-- Name: lr_delivery_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.lr_delivery
    ADD CONSTRAINT lr_delivery_pkey PRIMARY KEY (lr_delivery_id);


--
-- Name: lr_first_anc_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.lr_first_anc
    ADD CONSTRAINT lr_first_anc_pkey PRIMARY KEY (lr_first_anc_id);


--
-- Name: lr_newborn_medication_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.lr_newborn_medication
    ADD CONSTRAINT lr_newborn_medication_pkey PRIMARY KEY (lr_newborn_medication_id);


--
-- Name: lr_newborn_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.lr_newborn
    ADD CONSTRAINT lr_newborn_pkey PRIMARY KEY (lr_newborn_id);


--
-- Name: lr_other_labor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.lr_other_labor
    ADD CONSTRAINT lr_other_labor_pkey PRIMARY KEY (lr_other_labor_id);


--
-- Name: lr_postpartum_abnormal_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.lr_postpartum_abnormal
    ADD CONSTRAINT lr_postpartum_abnormal_pkey PRIMARY KEY (lr_postpartum_abnormal_id);


--
-- Name: lr_pregnancy_abnormal_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.lr_pregnancy_abnormal
    ADD CONSTRAINT lr_pregnancy_abnormal_pkey PRIMARY KEY (lr_pregnancy_abnormal_id);


--
-- Name: lr_pregnancy_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.lr_pregnancy
    ADD CONSTRAINT lr_pregnancy_pkey PRIMARY KEY (lr_pregnancy_id);


--
-- Name: lr_tetanus_vaccine_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.lr_tetanus_vaccine
    ADD CONSTRAINT lr_tetanus_vaccine_pkey PRIMARY KEY (lr_tetanus_vaccine_id);


--
-- Name: lr_us_gynae_exam_result_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.lr_us_gynae_exam_result
    ADD CONSTRAINT lr_us_gynae_exam_result_pkey PRIMARY KEY (lr_us_gynae_exam_result_id);


--
-- Name: lr_us_gynae_result_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.lr_us_gynae_result
    ADD CONSTRAINT lr_us_gynae_result_pkey PRIMARY KEY (lr_us_gynae_result_id);


--
-- Name: lr_us_obs_exam_result_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.lr_us_obs_exam_result
    ADD CONSTRAINT lr_us_obs_exam_result_pkey PRIMARY KEY (lr_us_obs_exam_result_id);


--
-- Name: lr_us_obs_fetus_result_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.lr_us_obs_fetus_result
    ADD CONSTRAINT lr_us_obs_fetus_result_pkey PRIMARY KEY (lr_us_obs_fetus_result_id);


--
-- Name: lr_us_obs_result_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.lr_us_obs_result
    ADD CONSTRAINT lr_us_obs_result_pkey PRIMARY KEY (lr_us_obs_result_id);


--
-- Name: lr_us_result_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.lr_us_result
    ADD CONSTRAINT lr_us_result_pkey PRIMARY KEY (lr_us_result_id);


--
-- Name: manufacturer_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.manufacturer
    ADD CONSTRAINT manufacturer_pkey PRIMARY KEY (manufacturer_id);


--
-- Name: medical_certificate_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.medical_certificate
    ADD CONSTRAINT medical_certificate_pkey PRIMARY KEY (medical_certificate_id);


--
-- Name: medical_device_used_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.medical_device_used
    ADD CONSTRAINT medical_device_used_pkey PRIMARY KEY (medical_device_used_id);


--
-- Name: medical_history_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.medical_history
    ADD CONSTRAINT medical_history_pkey PRIMARY KEY (medical_history_id);


--
-- Name: medication_error_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.medication_error
    ADD CONSTRAINT medication_error_pkey PRIMARY KEY (medication_error_id);


--
-- Name: nhso_506_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.nhso_506
    ADD CONSTRAINT nhso_506_pkey PRIMARY KEY (icd10_code);


--
-- Name: nhso_anc_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.nhso_anc
    ADD CONSTRAINT nhso_anc_pkey PRIMARY KEY (nhso_anc_id);


--
-- Name: nhso_appoint_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.nhso_appoint
    ADD CONSTRAINT nhso_appoint_pkey PRIMARY KEY (nhso_appoint_id);


--
-- Name: nhso_btype_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.nhso_btype
    ADD CONSTRAINT nhso_btype_pkey PRIMARY KEY (nhso_btype_id);


--
-- Name: nhso_card_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.nhso_card
    ADD CONSTRAINT nhso_card_pkey PRIMARY KEY (nhso_card_id);


--
-- Name: nhso_chronic_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.nhso_chronic
    ADD CONSTRAINT nhso_chronic_pkey PRIMARY KEY (nhso_chronic_id);


--
-- Name: nhso_death_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.nhso_death
    ADD CONSTRAINT nhso_death_pkey PRIMARY KEY (nhso_death_id);


--
-- Name: nhso_diag_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.nhso_diag
    ADD CONSTRAINT nhso_diag_pkey PRIMARY KEY (nhso_diag_id);


--
-- Name: nhso_drug_code_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.nhso_drug_code
    ADD CONSTRAINT nhso_drug_code_pkey PRIMARY KEY (nhso_drug_code_id);


--
-- Name: nhso_drug_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.nhso_drug
    ADD CONSTRAINT nhso_drug_pkey PRIMARY KEY (nhso_drug_id);


--
-- Name: nhso_epi_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.nhso_epi
    ADD CONSTRAINT nhso_epi_pkey PRIMARY KEY (nhso_epi_id);


--
-- Name: nhso_fp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.nhso_fp
    ADD CONSTRAINT nhso_fp_pkey PRIMARY KEY (nhso_fp_id);


--
-- Name: nhso_fptype_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.nhso_fptype
    ADD CONSTRAINT nhso_fptype_pkey PRIMARY KEY (nhso_fptype_id);


--
-- Name: nhso_home_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.nhso_home
    ADD CONSTRAINT nhso_home_pkey PRIMARY KEY (nhso_home_id);


--
-- Name: nhso_mch_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.nhso_mch
    ADD CONSTRAINT nhso_mch_pkey PRIMARY KEY (nhso_mch_id);


--
-- Name: nhso_nutri_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.nhso_nutri
    ADD CONSTRAINT nhso_nutri_pkey PRIMARY KEY (nhso_nutri_id);


--
-- Name: nhso_person_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.nhso_person
    ADD CONSTRAINT nhso_person_pkey PRIMARY KEY (nhso_person_id);


--
-- Name: nhso_pp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.nhso_pp
    ADD CONSTRAINT nhso_pp_pkey PRIMARY KEY (nhso_pp_id);


--
-- Name: nhso_proced_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.nhso_proced
    ADD CONSTRAINT nhso_proced_pkey PRIMARY KEY (nhso_proced_id);


--
-- Name: nhso_service_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.nhso_service
    ADD CONSTRAINT nhso_service_pkey PRIMARY KEY (nhso_service_id);


--
-- Name: nhso_surveil_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.nhso_surveil
    ADD CONSTRAINT nhso_surveil_pkey PRIMARY KEY (nhso_surveil_id);


--
-- Name: nhso_vaccine_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.nhso_vaccine
    ADD CONSTRAINT nhso_vaccine_pkey PRIMARY KEY (nhso_vaccine_id);


--
-- Name: nhso_women_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.nhso_women
    ADD CONSTRAINT nhso_women_pkey PRIMARY KEY (nhso_women_id);


--
-- Name: nled_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.nled_group
    ADD CONSTRAINT nled_group_pkey PRIMARY KEY (nled_group_id);


--
-- Name: nt_change_order_sheet_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.nt_change_order_sheet
    ADD CONSTRAINT nt_change_order_sheet_pkey PRIMARY KEY (nt_change_order_sheet_id);


--
-- Name: nt_food_order_sheet_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.nt_food_order_sheet
    ADD CONSTRAINT nt_food_order_sheet_pkey PRIMARY KEY (nt_food_order_sheet_id);


--
-- Name: nt_nutrition_facts_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.nt_nutrition_facts
    ADD CONSTRAINT nt_nutrition_facts_pkey PRIMARY KEY (nt_nutrition_facts_id);


--
-- Name: nt_order_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.nt_order
    ADD CONSTRAINT nt_order_pkey PRIMARY KEY (nt_order_id);


--
-- Name: nt_patient_nutrition_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.nt_patient_nutrition
    ADD CONSTRAINT nt_patient_nutrition_pkey PRIMARY KEY (nt_patient_nutrition_id);


--
-- Name: nt_special_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.nt_special_group
    ADD CONSTRAINT nt_special_group_pkey PRIMARY KEY (nt_special_group_id);


--
-- Name: nt_supplement_diet_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.nt_supplement_diet
    ADD CONSTRAINT nt_supplement_diet_pkey PRIMARY KEY (nt_supplement_diet_id);


--
-- Name: nt_therapeutic_diet_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.nt_therapeutic_diet
    ADD CONSTRAINT nt_therapeutic_diet_pkey PRIMARY KEY (nt_therapeutic_diet_id);


--
-- Name: nursing_care_plan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.nursing_care_plan
    ADD CONSTRAINT nursing_care_plan_pkey PRIMARY KEY (nursing_care_plan_id);


--
-- Name: old_hn_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.old_hn
    ADD CONSTRAINT old_hn_pkey PRIMARY KEY (old_hn_id);


--
-- Name: omega_general_data_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.omega_general_data
    ADD CONSTRAINT omega_general_data_pkey PRIMARY KEY (omega_general_data_id);


--
-- Name: omega_order_and_result_data_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.omega_order_and_result_data
    ADD CONSTRAINT omega_order_and_result_data_pkey PRIMARY KEY (omega_order_and_result_data_id);


--
-- Name: op_doctor_schedule_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.op_doctor_schedule
    ADD CONSTRAINT op_doctor_schedule_pkey PRIMARY KEY (op_doctor_schedule_id);


--
-- Name: op_operation_posture_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.op_operation_posture
    ADD CONSTRAINT op_operation_posture_pkey PRIMARY KEY (op_operation_posture_id);


--
-- Name: op_registered_anes_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.op_registered_anes_type
    ADD CONSTRAINT op_registered_anes_type_pkey PRIMARY KEY (op_registered_anes_type_id);


--
-- Name: op_registered_diagnosis_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.op_registered_diagnosis
    ADD CONSTRAINT op_registered_diagnosis_pkey PRIMARY KEY (op_registered_diagnosis_id);


--
-- Name: op_registered_operation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.op_registered_operation
    ADD CONSTRAINT op_registered_operation_pkey PRIMARY KEY (op_registered_operation_id);


--
-- Name: op_registered_physician_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.op_registered_physician
    ADD CONSTRAINT op_registered_physician_pkey PRIMARY KEY (op_registered_physician_id);


--
-- Name: op_registered_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.op_registered
    ADD CONSTRAINT op_registered_pkey PRIMARY KEY (op_registered_id);


--
-- Name: op_room_schedule_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.op_room_schedule
    ADD CONSTRAINT op_room_schedule_pkey PRIMARY KEY (op_room_schedule_id);


--
-- Name: op_set_anes_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.op_set_anes_type
    ADD CONSTRAINT op_set_anes_type_pkey PRIMARY KEY (op_set_anes_type_id);


--
-- Name: op_set_delay_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.op_set_delay
    ADD CONSTRAINT op_set_delay_pkey PRIMARY KEY (op_set_delay_id);


--
-- Name: op_set_diagnosis_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.op_set_diagnosis
    ADD CONSTRAINT op_set_diagnosis_pkey PRIMARY KEY (op_set_diagnosis_id);


--
-- Name: op_set_extra_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.op_set_extra
    ADD CONSTRAINT op_set_extra_pkey PRIMARY KEY (op_set_extra_id);


--
-- Name: op_set_operation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.op_set_operation
    ADD CONSTRAINT op_set_operation_pkey PRIMARY KEY (op_set_operation_id);


--
-- Name: op_set_physician_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.op_set_physician
    ADD CONSTRAINT op_set_physician_pkey PRIMARY KEY (op_set_physician_id);


--
-- Name: op_set_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.op_set
    ADD CONSTRAINT op_set_pkey PRIMARY KEY (op_set_id);


--
-- Name: opd_stat_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.opd_stat
    ADD CONSTRAINT opd_stat_pkey PRIMARY KEY (id);


--
-- Name: order_continue_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.order_continue
    ADD CONSTRAINT order_continue_pkey PRIMARY KEY (order_continue_id);


--
-- Name: order_continue_prepared_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.order_continue_prepared
    ADD CONSTRAINT order_continue_prepared_pkey PRIMARY KEY (order_continue_prepared_id);


--
-- Name: order_drug_interaction_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.order_drug_interaction
    ADD CONSTRAINT order_drug_interaction_pkey PRIMARY KEY (order_drug_interaction_id);


--
-- Name: order_item_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.order_item
    ADD CONSTRAINT order_item_pkey PRIMARY KEY (order_item_id);


--
-- Name: order_set_multi_visit_child_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.order_set_multi_visit_child
    ADD CONSTRAINT order_set_multi_visit_child_pkey PRIMARY KEY (order_set_multi_visit_child_id);


--
-- Name: order_set_multi_visit_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.order_set_multi_visit_detail
    ADD CONSTRAINT order_set_multi_visit_detail_pkey PRIMARY KEY (order_set_multi_visit_detail_id);


--
-- Name: order_set_multi_visit_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.order_set_multi_visit
    ADD CONSTRAINT order_set_multi_visit_pkey PRIMARY KEY (order_set_multi_visit_id);


--
-- Name: pacs_mwlwl_accession_no_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.mwlwl
    ADD CONSTRAINT pacs_mwlwl_accession_no_key UNIQUE (accession_no);


--
-- Name: pacs_mwlwl_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.mwlwl
    ADD CONSTRAINT pacs_mwlwl_pkey PRIMARY KEY (mwl_key);


--
-- Name: patho_assign_lab_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.patho_assign_lab
    ADD CONSTRAINT patho_assign_lab_pkey PRIMARY KEY (patho_assign_lab_id);


--
-- Name: patho_assign_pap_contraceptive_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.patho_assign_pap_contraceptive
    ADD CONSTRAINT patho_assign_pap_contraceptive_pkey PRIMARY KEY (patho_assign_pap_contraceptive_id);


--
-- Name: patho_autopsy_result_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.patho_autopsy_result
    ADD CONSTRAINT patho_autopsy_result_pkey PRIMARY KEY (patho_autopsy_result_id);


--
-- Name: patho_pap_result_abnormal_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.patho_pap_result_abnormal
    ADD CONSTRAINT patho_pap_result_abnormal_pkey PRIMARY KEY (patho_pap_result_abnormal_id);


--
-- Name: patho_pap_result_negative_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.patho_pap_result_negative
    ADD CONSTRAINT patho_pap_result_negative_pkey PRIMARY KEY (patho_pap_result_negative_id);


--
-- Name: patho_pap_result_others_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.patho_pap_result_others
    ADD CONSTRAINT patho_pap_result_others_pkey PRIMARY KEY (patho_pap_result_others_id);


--
-- Name: patho_pap_smear_result_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.patho_pap_smear_result
    ADD CONSTRAINT patho_pap_smear_result_pkey PRIMARY KEY (patho_pap_smear_result_id);


--
-- Name: patho_result_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.patho_result
    ADD CONSTRAINT patho_result_pkey PRIMARY KEY (patho_result_id);


--
-- Name: patho_snomed_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.patho_snomed
    ADD CONSTRAINT patho_snomed_pkey PRIMARY KEY (patho_snomed_id);


--
-- Name: patho_specimen_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.patho_specimen
    ADD CONSTRAINT patho_specimen_pkey PRIMARY KEY (patho_specimen_id);


--
-- Name: patient_address_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.patient_address
    ADD CONSTRAINT patient_address_pkey PRIMARY KEY (patient_address_id);


--
-- Name: patient_contact_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.patient_contact
    ADD CONSTRAINT patient_contact_pkey PRIMARY KEY (patient_contact_id);


--
-- Name: patient_file_borrow_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.patient_file_borrow
    ADD CONSTRAINT patient_file_borrow_pkey PRIMARY KEY (patient_file_borrow_id);


--
-- Name: patient_film_borrow_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.patient_film_borrow
    ADD CONSTRAINT patient_film_borrow_pkey PRIMARY KEY (patient_film_borrow_id);


--
-- Name: patient_film_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.patient_film
    ADD CONSTRAINT patient_film_pkey PRIMARY KEY (patient_film_id);


--
-- Name: patient_image_seq_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.patient_image_seq
    ADD CONSTRAINT patient_image_seq_pkey PRIMARY KEY (patient_image_seq_id);


--
-- Name: patient_media_perspective_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.patient_media_perspective
    ADD CONSTRAINT patient_media_perspective_pkey PRIMARY KEY (patient_media_perspective_id);


--
-- Name: patient_name_history_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.patient_name_history
    ADD CONSTRAINT patient_name_history_pkey PRIMARY KEY (patient_name_history_id);


--
-- Name: patient_number_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.patient_number
    ADD CONSTRAINT patient_number_pkey PRIMARY KEY (patient_number_id);


--
-- Name: patient_other_allergy_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.patient_other_allergy
    ADD CONSTRAINT patient_other_allergy_pkey PRIMARY KEY (patient_other_allergy_id);


--
-- Name: patient_payment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.patient_payment
    ADD CONSTRAINT patient_payment_pkey PRIMARY KEY (patient_payment_id);


--
-- Name: patient_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.patient
    ADD CONSTRAINT patient_pkey PRIMARY KEY (patient_id);


--
-- Name: patient_relation_number_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.patient_relation_number
    ADD CONSTRAINT patient_relation_number_pkey PRIMARY KEY (patient_relation_number_id);


--
-- Name: payer_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.payer
    ADD CONSTRAINT payer_pkey PRIMARY KEY (payer_id);


--
-- Name: payment_voucher_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.payment_voucher
    ADD CONSTRAINT payment_voucher_pkey PRIMARY KEY (payment_voucher_id);


--
-- Name: personal_illness_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.personal_illness
    ADD CONSTRAINT personal_illness_pkey PRIMARY KEY (personal_illness_id);


--
-- Name: physical_examination_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.physical_examination
    ADD CONSTRAINT physical_examination_pkey PRIMARY KEY (physical_examination_id);


--
-- Name: plan_auto_order_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.plan_auto_order
    ADD CONSTRAINT plan_auto_order_pkey PRIMARY KEY (plan_auto_order_id);


--
-- Name: plan_billing_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.plan_billing_group
    ADD CONSTRAINT plan_billing_group_pkey PRIMARY KEY (plan_billing_group_id);


--
-- Name: plan_billing_share_except_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.plan_billing_share_except
    ADD CONSTRAINT plan_billing_share_except_pkey PRIMARY KEY (plan_billing_share_except_id);


--
-- Name: plan_document_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.plan_document
    ADD CONSTRAINT plan_document_pkey PRIMARY KEY (plan_document_id);


--
-- Name: plan_dx_except_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.plan_dx_except
    ADD CONSTRAINT plan_dx_except_pkey PRIMARY KEY (plan_dx_except_id);


--
-- Name: plan_external_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.plan_external
    ADD CONSTRAINT plan_external_pkey PRIMARY KEY (plan_external_id);


--
-- Name: plan_external_visit_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.plan_external_visit
    ADD CONSTRAINT plan_external_visit_pkey PRIMARY KEY (plan_external_visit_id);


--
-- Name: plan_item_share_limit_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.plan_item_share_limit
    ADD CONSTRAINT plan_item_share_limit_pkey PRIMARY KEY (plan_item_share_limit_id);


--
-- Name: plan_payer_other_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.plan_payer_other
    ADD CONSTRAINT plan_payer_other_pkey PRIMARY KEY (plan_payer_other_id);


--
-- Name: plan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.plan
    ADD CONSTRAINT plan_pkey PRIMARY KEY (plan_id);


--
-- Name: plan_set_billing_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.plan_set_billing_group
    ADD CONSTRAINT plan_set_billing_group_pkey PRIMARY KEY (plan_set_billing_group_id);


--
-- Name: prepack_item_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.prepack_item
    ADD CONSTRAINT prepack_item_pkey PRIMARY KEY (prepack_item_id);


--
-- Name: prescription_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.prescription
    ADD CONSTRAINT prescription_pkey PRIMARY KEY (prescription_id);


--
-- Name: previous_drug_used_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.previous_drug_used
    ADD CONSTRAINT previous_drug_used_pkey PRIMARY KEY (previous_drug_used_id);


--
-- Name: previous_test_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.previous_test
    ADD CONSTRAINT previous_test_pkey PRIMARY KEY (previous_test_id);


--
-- Name: progress_note_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.progress_note
    ADD CONSTRAINT progress_note_pkey PRIMARY KEY (progress_note_id);


--
-- Name: queue_mgnt_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.queue_mgnt
    ADD CONSTRAINT queue_mgnt_pkey PRIMARY KEY (queue_mgnt_id);


--
-- Name: receipt_attach_bill_grp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.receipt_attach_bill_grp
    ADD CONSTRAINT receipt_attach_bill_grp_pkey PRIMARY KEY (receipt_attach_bill_grp_id);


--
-- Name: receipt_attach_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.receipt_attach
    ADD CONSTRAINT receipt_attach_pkey PRIMARY KEY (receipt_attach_id);


--
-- Name: receipt_billing_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.receipt_billing_group
    ADD CONSTRAINT receipt_billing_group_pkey PRIMARY KEY (receipt_billing_group_id);


--
-- Name: receipt_order_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.receipt_order_detail
    ADD CONSTRAINT receipt_order_detail_pkey PRIMARY KEY (receipt_order_detail_id);


--
-- Name: receipt_order_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.receipt_order
    ADD CONSTRAINT receipt_order_pkey PRIMARY KEY (receipt_order_id);


--
-- Name: receipt_paid_method_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.receipt_paid_method
    ADD CONSTRAINT receipt_paid_method_pkey PRIMARY KEY (receipt_paid_method_id);


--
-- Name: receipt_pay_invoice_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.receipt_pay_invoice
    ADD CONSTRAINT receipt_pay_invoice_pkey PRIMARY KEY (receipt_pay_invoice_id);


--
-- Name: receipt_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.receipt
    ADD CONSTRAINT receipt_pkey PRIMARY KEY (receipt_id);


--
-- Name: receipt_print_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.receipt_print_group
    ADD CONSTRAINT receipt_print_group_pkey PRIMARY KEY (receipt_print_group_id);


--
-- Name: receipt_print_history_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.receipt_print_history
    ADD CONSTRAINT receipt_print_history_pkey PRIMARY KEY (receipt_print_history_id);


--
-- Name: receipt_revenue_order_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.receipt_revenue_order
    ADD CONSTRAINT receipt_revenue_order_pkey PRIMARY KEY (receipt_revenue_order_id);


--
-- Name: refer_in_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.refer_in
    ADD CONSTRAINT refer_in_pkey PRIMARY KEY (refer_in_id);


--
-- Name: refer_out_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.refer_out
    ADD CONSTRAINT refer_out_pkey PRIMARY KEY (refer_out_id);


--
-- Name: refer_out_reason_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.refer_out_reason
    ADD CONSTRAINT refer_out_reason_pkey PRIMARY KEY (refer_out_reason_id);


--
-- Name: report_drug_interaction_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.report_drug_interaction
    ADD CONSTRAINT report_drug_interaction_pkey PRIMARY KEY (report_drug_interaction_id);


--
-- Name: report_drug_pregnancy_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.report_drug_pregnancy
    ADD CONSTRAINT report_drug_pregnancy_pkey PRIMARY KEY (report_drug_pregnancy_id);


--
-- Name: report_group_billing_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.report_group_billing
    ADD CONSTRAINT report_group_billing_pkey PRIMARY KEY (report_group_billing_id);


--
-- Name: report_group_item_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.report_group_item
    ADD CONSTRAINT report_group_item_pkey PRIMARY KEY (report_group_item_id);


--
-- Name: report_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.report_group
    ADD CONSTRAINT report_group_pkey PRIMARY KEY (report_group_id);


--
-- Name: report_group_plan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.report_group_plan
    ADD CONSTRAINT report_group_plan_pkey PRIMARY KEY (report_group_plan_id);


--
-- Name: report_icd10_group_accident_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.report_icd10_group_accident
    ADD CONSTRAINT report_icd10_group_accident_pkey PRIMARY KEY (report_icd10_group_accident_id);


--
-- Name: report_range_time_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.report_range_time
    ADD CONSTRAINT report_range_time_pkey PRIMARY KEY (report_range_time);


--
-- Name: reportwl_exam_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.reportwl
    ADD CONSTRAINT reportwl_exam_id_key UNIQUE (exam_id);


--
-- Name: reportwl_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.reportwl
    ADD CONSTRAINT reportwl_pkey PRIMARY KEY (reportwl_key);


--
-- Name: return_drug_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.return_drug
    ADD CONSTRAINT return_drug_pkey PRIMARY KEY (return_drug_id);


--
-- Name: risk_factor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.risk_factor
    ADD CONSTRAINT risk_factor_pkey PRIMARY KEY (risk_factor_id);


--
-- Name: seq_an_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.seq_an
    ADD CONSTRAINT seq_an_pkey PRIMARY KEY (seq_an_id);


--
-- Name: seq_axn_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.seq_axn
    ADD CONSTRAINT seq_axn_pkey PRIMARY KEY (seq_axn_id);


--
-- Name: seq_en_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.seq_en
    ADD CONSTRAINT seq_en_pkey PRIMARY KEY (seq_en_id);


--
-- Name: seq_finger_print_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.seq_finger_print
    ADD CONSTRAINT seq_finger_print_pkey PRIMARY KEY (seq_finger_print_id);


--
-- Name: seq_hn_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.seq_hn
    ADD CONSTRAINT seq_hn_pkey PRIMARY KEY (seq_hn_id);


--
-- Name: seq_ln_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.seq_ln
    ADD CONSTRAINT seq_ln_pkey PRIMARY KEY (seq_ln_id);


--
-- Name: seq_medical_certificate_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.seq_medical_certificate
    ADD CONSTRAINT seq_medical_certificate_pkey PRIMARY KEY (seq_medical_certificate_id);


--
-- Name: seq_pacs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.seq_pacs
    ADD CONSTRAINT seq_pacs_pkey PRIMARY KEY (seq_pacs_id);


--
-- Name: seq_patient_number_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.seq_patient_number
    ADD CONSTRAINT seq_patient_number_pkey PRIMARY KEY (seq_patient_number_id);


--
-- Name: seq_pn_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.seq_pn
    ADD CONSTRAINT seq_pn_pkey PRIMARY KEY (seq_pn_id);


--
-- Name: seq_receipt_number_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.seq_receipt_number
    ADD CONSTRAINT seq_receipt_number_pkey PRIMARY KEY (seq_receipt_number_id);


--
-- Name: seq_refer_out_no_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.seq_refer_out_no
    ADD CONSTRAINT seq_refer_out_no_pkey PRIMARY KEY (seq_refer_out_no_id);


--
-- Name: seq_return_drug_no_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.seq_return_drug_no
    ADD CONSTRAINT seq_return_drug_no_pkey PRIMARY KEY (seq_return_drug_no_id);


--
-- Name: seq_un_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.seq_un
    ADD CONSTRAINT seq_un_pkey PRIMARY KEY (seq_un_id);


--
-- Name: seq_vn_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.seq_vn
    ADD CONSTRAINT seq_vn_pkey PRIMARY KEY (seq_vn_id);


--
-- Name: seq_xn_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.seq_xn
    ADD CONSTRAINT seq_xn_pkey PRIMARY KEY (seq_xn_id);


--
-- Name: service_point_schedule_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.service_point_schedule
    ADD CONSTRAINT service_point_schedule_pkey PRIMARY KEY (service_point_schedule_id);


--
-- Name: service_time_stamp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.service_time_stamp
    ADD CONSTRAINT service_time_stamp_pkey PRIMARY KEY (service_time_stamp_id);


--
-- Name: special_nurse_care_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.special_nurse_care_detail
    ADD CONSTRAINT special_nurse_care_detail_pkey PRIMARY KEY (nurse_care_detail_id);


--
-- Name: special_nurse_care_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.special_nurse_care
    ADD CONSTRAINT special_nurse_care_pkey PRIMARY KEY (special_nurse_care_id);


--
-- Name: sql_except_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.sql_except
    ADD CONSTRAINT sql_except_pkey PRIMARY KEY (sql_except_id);


--
-- Name: sql_group_role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.sql_group_role
    ADD CONSTRAINT sql_group_role_pkey PRIMARY KEY (sql_group_role_id);


--
-- Name: sql_statement_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.sql_statement
    ADD CONSTRAINT sql_statement_pkey PRIMARY KEY (sql_statement_id);


--
-- Name: stock_adjust_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock_adjust
    ADD CONSTRAINT stock_adjust_pkey PRIMARY KEY (stock_adjust_id);


--
-- Name: stock_auth_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock_auth
    ADD CONSTRAINT stock_auth_pkey PRIMARY KEY (stock_auth_id);


--
-- Name: stock_auto_receive_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock_auto_receive
    ADD CONSTRAINT stock_auto_receive_pkey PRIMARY KEY (stock_auto_receive_id);


--
-- Name: stock_base_exchange_note_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock_base_exchange_note
    ADD CONSTRAINT stock_base_exchange_note_pkey PRIMARY KEY (stock_base_exchange_note_id);


--
-- Name: stock_base_reason_of_request_order_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock_base_reason_of_request_order
    ADD CONSTRAINT stock_base_reason_of_request_order_pkey PRIMARY KEY (stock_base_reason_of_request_order_id);


--
-- Name: stock_budget_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock_budget
    ADD CONSTRAINT stock_budget_pkey PRIMARY KEY (stock_budget_id);


--
-- Name: stock_card_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock_card
    ADD CONSTRAINT stock_card_pkey PRIMARY KEY (stock_card_id);


--
-- Name: stock_chk_on_hand_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock_chk_on_hand
    ADD CONSTRAINT stock_chk_on_hand_pkey PRIMARY KEY (stock_chk_on_hand_id);


--
-- Name: stock_cssd_machine_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock_cssd_machine
    ADD CONSTRAINT stock_cssd_machine_pkey PRIMARY KEY (stock_cssd_machine_id);


--
-- Name: stock_cssd_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock_cssd
    ADD CONSTRAINT stock_cssd_pkey PRIMARY KEY (stock_cssd_id);


--
-- Name: stock_cssd_produce_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock_cssd_produce
    ADD CONSTRAINT stock_cssd_produce_pkey PRIMARY KEY (stock_cssd_produce_id);


--
-- Name: stock_cssd_sub_item_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock_cssd_sub_item
    ADD CONSTRAINT stock_cssd_sub_item_pkey PRIMARY KEY (stock_cssd_sub_item_id);


--
-- Name: stock_custom_po_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock_custom_po
    ADD CONSTRAINT stock_custom_po_pkey PRIMARY KEY (stock_custom_po_id);


--
-- Name: stock_dispense_other_item_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock_dispense_other_item
    ADD CONSTRAINT stock_dispense_other_item_pkey PRIMARY KEY (stock_dispense_other_item_id);


--
-- Name: stock_dispense_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock_dispense
    ADD CONSTRAINT stock_dispense_pkey PRIMARY KEY (stock_dispense_id);


--
-- Name: stock_exchange_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock_exchange_detail
    ADD CONSTRAINT stock_exchange_detail_pkey PRIMARY KEY (stock_exchange_detail_id);


--
-- Name: stock_exchange_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock_exchange
    ADD CONSTRAINT stock_exchange_pkey PRIMARY KEY (stock_exchange_id);


--
-- Name: stock_free_item_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock_free_item
    ADD CONSTRAINT stock_free_item_pkey PRIMARY KEY (stock_free_item_id);


--
-- Name: stock_item_set_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock_item_set
    ADD CONSTRAINT stock_item_set_pkey PRIMARY KEY (stock_item_set_id);


--
-- Name: stock_item_trade_name_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock_item_trade_name
    ADD CONSTRAINT stock_item_trade_name_pkey PRIMARY KEY (stock_item_trade_name_id);


--
-- Name: stock_merge_po_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock_merge_po_detail
    ADD CONSTRAINT stock_merge_po_detail_pkey PRIMARY KEY (stock_merge_po_detail_id);


--
-- Name: stock_merge_po_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock_merge_po
    ADD CONSTRAINT stock_merge_po_pkey PRIMARY KEY (stock_merge_po_id);


--
-- Name: stock_mgnt_period_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock_mgnt_period
    ADD CONSTRAINT stock_mgnt_period_pkey PRIMARY KEY (stock_mgnt_period_id);


--
-- Name: stock_mgnt_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock_mgnt
    ADD CONSTRAINT stock_mgnt_pkey PRIMARY KEY (stock_mgnt_id);


--
-- Name: stock_order_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock_order
    ADD CONSTRAINT stock_order_pkey PRIMARY KEY (stock_order_id);


--
-- Name: stock_order_tmp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock_order_tmp
    ADD CONSTRAINT stock_order_tmp_pkey PRIMARY KEY (stock_order_tmp_id);


--
-- Name: stock_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock
    ADD CONSTRAINT stock_pkey PRIMARY KEY (stock_id);


--
-- Name: stock_place_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock_place
    ADD CONSTRAINT stock_place_pkey PRIMARY KEY (stock_place_id);


--
-- Name: stock_prepack_item_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock_prepack_item
    ADD CONSTRAINT stock_prepack_item_pkey PRIMARY KEY (stock_prepack_item_id);


--
-- Name: stock_request_order_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock_request_order
    ADD CONSTRAINT stock_request_order_pkey PRIMARY KEY (stock_request_order_id);


--
-- Name: stock_request_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock_request
    ADD CONSTRAINT stock_request_pkey PRIMARY KEY (stock_request_id);


--
-- Name: stock_request_produce_material_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock_request_produce_material
    ADD CONSTRAINT stock_request_produce_material_pkey PRIMARY KEY (stock_request_produce_material_id);


--
-- Name: stock_request_produce_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock_request_produce
    ADD CONSTRAINT stock_request_produce_pkey PRIMARY KEY (stock_request_produce_id);


--
-- Name: stock_return_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock_return
    ADD CONSTRAINT stock_return_pkey PRIMARY KEY (stock_return_id);


--
-- Name: stock_setup_order_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock_setup_order_detail
    ADD CONSTRAINT stock_setup_order_detail_pkey PRIMARY KEY (stock_setup_order_detail_id);


--
-- Name: stock_setup_order_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock_setup_order
    ADD CONSTRAINT stock_setup_order_pkey PRIMARY KEY (stock_setup_order_id);


--
-- Name: stock_sterile_method_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock_sterile_method
    ADD CONSTRAINT stock_sterile_method_pkey PRIMARY KEY (stock_sterile_method_id);


--
-- Name: stock_template_produce_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock_template_produce
    ADD CONSTRAINT stock_template_produce_pkey PRIMARY KEY (stock_template_produce_id);


--
-- Name: stock_used_in_stock_or_produce_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock_used_in_stock_or_produce
    ADD CONSTRAINT stock_used_in_stock_or_produce_pkey PRIMARY KEY (stock_used_in_stock_or_produce_id);


--
-- Name: sub_drawing_details_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.sub_drawing_details
    ADD CONSTRAINT sub_drawing_details_pkey PRIMARY KEY (sub_drawing_details_id);


--
-- Name: sum_cost_billing_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.sum_cost_billing_group
    ADD CONSTRAINT sum_cost_billing_group_pkey PRIMARY KEY (sum_cost_billing_group_id);


--
-- Name: sum_cost_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.sum_cost
    ADD CONSTRAINT sum_cost_pkey PRIMARY KEY (sum_cost_id);


--
-- Name: team_work_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.stock_team_work
    ADD CONSTRAINT team_work_pkey PRIMARY KEY (stock_team_work_id);


--
-- Name: template_appointment_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.template_appointment_detail
    ADD CONSTRAINT template_appointment_detail_pkey PRIMARY KEY (template_appointment_detail_id);


--
-- Name: template_appointment_order_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.template_appointment_order
    ADD CONSTRAINT template_appointment_order_pkey PRIMARY KEY (template_appointment_order_id);


--
-- Name: template_appointment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.template_appointment
    ADD CONSTRAINT template_appointment_pkey PRIMARY KEY (template_appointment_id);


--
-- Name: template_dent_operation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.template_dent_operation
    ADD CONSTRAINT template_dent_operation_pkey PRIMARY KEY (template_dent_operation_id);


--
-- Name: template_discount_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.template_discount
    ADD CONSTRAINT template_discount_pkey PRIMARY KEY (template_discount_id);


--
-- Name: template_drug_allergy_note_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.template_drug_allergy_note
    ADD CONSTRAINT template_drug_allergy_note_pkey PRIMARY KEY (template_drug_allergy_note_id);


--
-- Name: template_drug_usage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.template_drug_usage
    ADD CONSTRAINT template_drug_usage_pkey PRIMARY KEY (template_drug_usage_id);


--
-- Name: template_dt_tx_note_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.template_dt_tx_note
    ADD CONSTRAINT template_dt_tx_note_pkey PRIMARY KEY (template_dt_tx_note_id);


--
-- Name: template_forensic_out_exam_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.template_forensic_out_exam
    ADD CONSTRAINT template_forensic_out_exam_pkey PRIMARY KEY (template_forensic_out_exam_id);


--
-- Name: template_item_set_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.template_item_set
    ADD CONSTRAINT template_item_set_pkey PRIMARY KEY (template_item_set_id);


--
-- Name: template_lab_choice_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.template_lab_choice
    ADD CONSTRAINT template_lab_choice_pkey PRIMARY KEY (template_lab_choice_id);


--
-- Name: template_lab_item_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.template_lab_item
    ADD CONSTRAINT template_lab_item_pkey PRIMARY KEY (template_lab_item_id);


--
-- Name: template_lab_item_tube_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.template_lab_item_tube
    ADD CONSTRAINT template_lab_item_tube_pkey PRIMARY KEY (template_lab_item_tube_id);


--
-- Name: template_lab_normal_value_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.template_lab_normal_value
    ADD CONSTRAINT template_lab_normal_value_pkey PRIMARY KEY (template_lab_normal_value_id);


--
-- Name: template_lab_special_request_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.template_lab_special_request
    ADD CONSTRAINT template_lab_special_request_pkey PRIMARY KEY (template_lab_special_request_id);


--
-- Name: template_lab_sub_test_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.template_lab_sub_test
    ADD CONSTRAINT template_lab_sub_test_pkey PRIMARY KEY (template_lab_sub_test_id);


--
-- Name: template_lab_test_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.template_lab_test
    ADD CONSTRAINT template_lab_test_pkey PRIMARY KEY (template_lab_test_id);


--
-- Name: template_lr_indication_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.template_lr_indication
    ADD CONSTRAINT template_lr_indication_pkey PRIMARY KEY (template_lr_indication_id);


--
-- Name: template_lr_pp_med_treatment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.template_lr_pp_med_treatment
    ADD CONSTRAINT template_lr_pp_med_treatment_pkey PRIMARY KEY (template_lr_pp_med_treatment_id);


--
-- Name: template_ncp_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.template_ncp_group
    ADD CONSTRAINT template_ncp_group_pkey PRIMARY KEY (template_ncp_group_id);


--
-- Name: template_ncp_plan_details_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.template_ncp_plan_details
    ADD CONSTRAINT template_ncp_plan_details_pkey PRIMARY KEY (template_ncp_plan_details_id);


--
-- Name: template_ncp_problem_details_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.template_ncp_problem_details
    ADD CONSTRAINT template_ncp_problem_details_pkey PRIMARY KEY (template_ncp_problem_details_id);


--
-- Name: template_ncp_problem_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.template_ncp_problem
    ADD CONSTRAINT template_ncp_problem_pkey PRIMARY KEY (template_ncp_problem_id);


--
-- Name: template_ncp_result_details_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.template_ncp_result_details
    ADD CONSTRAINT template_ncp_result_details_pkey PRIMARY KEY (template_ncp_result_details_id);


--
-- Name: template_nt_allergy_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.template_nt_allergy
    ADD CONSTRAINT template_nt_allergy_pkey PRIMARY KEY (template_nt_allergy_id);


--
-- Name: template_nt_change_order_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.template_nt_change_order
    ADD CONSTRAINT template_nt_change_order_pkey PRIMARY KEY (template_nt_change_order_id);


--
-- Name: template_nt_default_order_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.template_nt_default_order
    ADD CONSTRAINT template_nt_default_order_pkey PRIMARY KEY (template_nt_default_order_id);


--
-- Name: template_op_position_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.template_op_position
    ADD CONSTRAINT template_op_position_pkey PRIMARY KEY (template_op_position_id);


--
-- Name: template_patho_operation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.template_patho_operation
    ADD CONSTRAINT template_patho_operation_pkey PRIMARY KEY (template_patho_operation_id);


--
-- Name: template_patho_result_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.template_patho_result
    ADD CONSTRAINT template_patho_result_pkey PRIMARY KEY (template_patho_result_id);


--
-- Name: template_personal_illness_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.template_personal_illness
    ADD CONSTRAINT template_personal_illness_pkey PRIMARY KEY (template_personal_illness_id);


--
-- Name: template_phyex_default_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.template_phyex_default
    ADD CONSTRAINT template_phyex_default_pkey PRIMARY KEY (template_phyex_default_id);


--
-- Name: template_phyex_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.template_phyex_detail
    ADD CONSTRAINT template_phyex_detail_pkey PRIMARY KEY (template_phyex_detail_id);


--
-- Name: template_phyex_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.template_phyex
    ADD CONSTRAINT template_phyex_pkey PRIMARY KEY (template_phyex_id);


--
-- Name: template_return_drug_reason_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.template_return_drug_reason
    ADD CONSTRAINT template_return_drug_reason_pkey PRIMARY KEY (template_return_drug_reason_id);


--
-- Name: template_risk_factor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.template_risk_factor
    ADD CONSTRAINT template_risk_factor_pkey PRIMARY KEY (template_risk_factor_id);


--
-- Name: template_symptom_default_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.template_symptom_default
    ADD CONSTRAINT template_symptom_default_pkey PRIMARY KEY (template_symptom_default_id);


--
-- Name: template_symptom_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.template_symptom_group
    ADD CONSTRAINT template_symptom_group_pkey PRIMARY KEY (template_symptom_group_id);


--
-- Name: template_symptom_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.template_symptom
    ADD CONSTRAINT template_symptom_pkey PRIMARY KEY (template_symptom_id);


--
-- Name: template_xray_result_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.template_xray_result
    ADD CONSTRAINT template_xray_result_pkey PRIMARY KEY (template_xray_result_id);


--
-- Name: time_stamp_details_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.time_stamp_details
    ADD CONSTRAINT time_stamp_details_pkey PRIMARY KEY (time_stamp_details_id);


--
-- Name: time_stamp_receive_file_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.time_stamp_receive_file
    ADD CONSTRAINT time_stamp_receive_file_pkey PRIMARY KEY (time_stamp_receive_file_id);


--
-- Name: timers_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.timers
    ADD CONSTRAINT timers_pk PRIMARY KEY (timerid, targetid);


--
-- Name: user_audit_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.user_audit
    ADD CONSTRAINT user_audit_pkey PRIMARY KEY (user_audit_id);


--
-- Name: visit_clinic_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.visit_clinic
    ADD CONSTRAINT visit_clinic_pkey PRIMARY KEY (visit_clinic_id);


--
-- Name: visit_deliver_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.visit_deliver
    ADD CONSTRAINT visit_deliver_pkey PRIMARY KEY (visit_deliver_id);


--
-- Name: visit_payment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.visit_payment
    ADD CONSTRAINT visit_payment_pkey PRIMARY KEY (visit_payment_id);


--
-- Name: visit_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.visit
    ADD CONSTRAINT visit_pkey PRIMARY KEY (visit_id);


--
-- Name: visit_queue_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.visit_queue
    ADD CONSTRAINT visit_queue_pkey PRIMARY KEY (visit_queue_id);


--
-- Name: visit_sound_file_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.visit_sound_file
    ADD CONSTRAINT visit_sound_file_pkey PRIMARY KEY (visit_sound_file_id);


--
-- Name: visit_special_card_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.visit_special_card
    ADD CONSTRAINT visit_special_card_pkey PRIMARY KEY (visit_special_card_id);


--
-- Name: visit_waiting_reason_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.visit_waiting_reason
    ADD CONSTRAINT visit_waiting_reason_pkey PRIMARY KEY (visit_waiting_reason_id);


--
-- Name: vital_sign_extend_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.vital_sign_extend
    ADD CONSTRAINT vital_sign_extend_pkey PRIMARY KEY (vital_sign_extend_id);


--
-- Name: vital_sign_ipd_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.vital_sign_ipd
    ADD CONSTRAINT vital_sign_ipd_pkey PRIMARY KEY (vital_sign_ipd_id);


--
-- Name: vital_sign_opd_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.vital_sign_opd
    ADD CONSTRAINT vital_sign_opd_pkey PRIMARY KEY (vital_sign_opd_id);


--
-- Name: vs_coma_scale_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.vs_coma_scale
    ADD CONSTRAINT vs_coma_scale_pkey PRIMARY KEY (vs_coma_scale_id);


--
-- Name: vs_ext_adv_check_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.vs_ext_adv_check
    ADD CONSTRAINT vs_ext_adv_check_pkey PRIMARY KEY (vs_ext_adv_check_id);


--
-- Name: vs_ext_advice_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.vs_ext_advice
    ADD CONSTRAINT vs_ext_advice_pkey PRIMARY KEY (vs_ext_advice_id);


--
-- Name: vs_ipd_intake_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.vs_ipd_intake
    ADD CONSTRAINT vs_ipd_intake_pkey PRIMARY KEY (vs_ipd_intake_id);


--
-- Name: vs_ipd_output_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.vs_ipd_output
    ADD CONSTRAINT vs_ipd_output_pkey PRIMARY KEY (vs_ipd_output_id);


--
-- Name: vs_patient_classification_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.vs_patient_classification_detail
    ADD CONSTRAINT vs_patient_classification_detail_pkey PRIMARY KEY (vs_patient_classification_detail_id);


--
-- Name: vs_patient_classification_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.vs_patient_classification
    ADD CONSTRAINT vs_patient_classification_pkey PRIMARY KEY (vs_patient_classification_id);


--
-- Name: xray_execute_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.xray_execute_detail
    ADD CONSTRAINT xray_execute_detail_pkey PRIMARY KEY (xray_execute_detail_id);


--
-- Name: xray_film_depository_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.xray_film_depository
    ADD CONSTRAINT xray_film_depository_pkey PRIMARY KEY (xray_film_depository_id);


--
-- Name: xray_film_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.xray_film
    ADD CONSTRAINT xray_film_pkey PRIMARY KEY (xray_film_id);


--
-- Name: xray_result_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY imed.xray_result
    ADD CONSTRAINT xray_result_pkey PRIMARY KEY (xray_result_id);


--
-- Name: admission_note_admit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX admission_note_admit_id ON imed.admission_note USING btree (admit_id);


--
-- Name: admit_active; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX admit_active ON imed.admit USING btree (active);


--
-- Name: admit_an; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX admit_an ON imed.admit USING btree (an);


--
-- Name: admit_ipd_discharge; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX admit_ipd_discharge ON imed.admit USING btree (ipd_discharge);


--
-- Name: admit_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX admit_visit_id ON imed.admit USING btree (visit_id);


--
-- Name: anes_assessment_main_data_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX anes_assessment_main_data_id ON imed.anes_assessment_data USING btree (anes_data_id);


--
-- Name: anes_complication_main_data_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX anes_complication_main_data_id ON imed.anes_complication USING btree (anes_data_id);


--
-- Name: anes_general_main_data_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX anes_general_main_data_id ON imed.anes_general_data USING btree (anes_data_id);


--
-- Name: anes_inhalation_main_data_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX anes_inhalation_main_data_id ON imed.anes_inhalation USING btree (anes_data_id);


--
-- Name: anes_lab_main_data_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX anes_lab_main_data_id ON imed.anes_lab_investigation USING btree (anes_data_id);


--
-- Name: anes_local_agent_main_data_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX anes_local_agent_main_data_id ON imed.anes_local_agent USING btree (anes_data_id);


--
-- Name: anes_monitor_main_data_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX anes_monitor_main_data_id ON imed.anes_monitor_technique USING btree (anes_data_id);


--
-- Name: anes_monitors_main_data_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX anes_monitors_main_data_id ON imed.anes_monitors USING btree (anes_data_id);


--
-- Name: anes_pacu_main_data_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX anes_pacu_main_data_id ON imed.anes_pacu_data USING btree (anes_data_id);


--
-- Name: anes_physical_exam_main_data_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX anes_physical_exam_main_data_id ON imed.anes_physical_exam_data USING btree (anes_data_id);


--
-- Name: anes_po_main_data_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX anes_po_main_data_id ON imed.anes_po_pain_management USING btree (anes_data_id);


--
-- Name: anes_pre_op_condition_main_data_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX anes_pre_op_condition_main_data_id ON imed.anes_pre_op_condition USING btree (anes_data_id);


--
-- Name: anes_premedication_main_data_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX anes_premedication_main_data_id ON imed.anes_premedication USING btree (anes_data_id);


--
-- Name: anes_service_main_data_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX anes_service_main_data_id ON imed.anes_service USING btree (anes_data_id);


--
-- Name: anes_special_tech_data_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX anes_special_tech_data_id ON imed.anes_special_tech USING btree (anes_data_id);


--
-- Name: anes_team_main_data_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX anes_team_main_data_id ON imed.anes_team USING btree (anes_data_id);


--
-- Name: anes_technique_main_data_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX anes_technique_main_data_id ON imed.anes_technique USING btree (anes_data_id);


--
-- Name: appointment_appoint_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX appointment_appoint_date ON imed.appointment USING btree (appoint_date);


--
-- Name: appointment_appoint_time; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX appointment_appoint_time ON imed.appointment USING btree (appoint_time);


--
-- Name: appointment_base_spid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX appointment_base_spid ON imed.appointment USING btree (base_spid);


--
-- Name: appointment_continue_ref_no; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX appointment_continue_ref_no ON imed.appointment USING btree (continue_ref_no);


--
-- Name: appointment_doctor_appoin; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX appointment_doctor_appoin ON imed.appointment_doctor USING btree (appointment_id);


--
-- Name: appointment_doctor_assigner_eid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX appointment_doctor_assigner_eid ON imed.appointment USING btree (doctor_assigner_eid);


--
-- Name: appointment_doctor_eid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX appointment_doctor_eid ON imed.appointment USING btree (doctor_eid);


--
-- Name: appointment_order_item_ap; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX appointment_order_item_ap ON imed.appointment_order_item USING btree (appointment_id);


--
-- Name: appointment_order_item_it; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX appointment_order_item_it ON imed.appointment_order_item USING btree (item_id);


--
-- Name: appointment_patient_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX appointment_patient_id ON imed.appointment USING btree (patient_id);


--
-- Name: assign_lab_assign_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX assign_lab_assign_date ON imed.assign_lab USING btree (assign_date);


--
-- Name: assign_lab_assign_status; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX assign_lab_assign_status ON imed.assign_lab USING btree (assign_order_status);


--
-- Name: assign_lab_assign_time; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX assign_lab_assign_time ON imed.assign_lab USING btree (assign_time);


--
-- Name: assign_lab_complete_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX assign_lab_complete_date ON imed.assign_lab USING btree (complete_date);


--
-- Name: assign_lab_doctor_eid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX assign_lab_doctor_eid ON imed.assign_lab USING btree (assign_doctor_eid);


--
-- Name: assign_lab_ln; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX assign_lab_ln ON imed.assign_lab USING btree (ln);


--
-- Name: assign_lab_rc_all_specimen; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX assign_lab_rc_all_specimen ON imed.assign_lab USING btree (receive_all_specimen);


--
-- Name: assign_lab_receive_specimen_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX assign_lab_receive_specimen_date ON imed.assign_lab USING btree (receive_specimen_date);


--
-- Name: assign_lab_receive_specimen_time; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX assign_lab_receive_specimen_time ON imed.assign_lab USING btree (receive_specimen_time);


--
-- Name: assign_lab_recieve_specimen_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX assign_lab_recieve_specimen_date ON imed.assign_lab USING btree (receive_specimen_date);


--
-- Name: assign_lab_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX assign_lab_visit_id ON imed.assign_lab USING btree (visit_id);


--
-- Name: assign_times_patient_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX assign_times_patient_id ON imed.assign_times USING btree (patient_id);


--
-- Name: assign_xray_assign_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX assign_xray_assign_date ON imed.assign_xray USING btree (assign_date);


--
-- Name: assign_xray_assign_status; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX assign_xray_assign_status ON imed.assign_xray USING btree (assign_order_status);


--
-- Name: assign_xray_complete_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX assign_xray_complete_date ON imed.assign_xray USING btree (complete_date);


--
-- Name: assign_xray_doctor_eid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX assign_xray_doctor_eid ON imed.assign_xray USING btree (assign_doctor_eid);


--
-- Name: assign_xray_execute_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX assign_xray_execute_date ON imed.assign_xray USING btree (complete_execute_date);


--
-- Name: assign_xray_receive_spid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX assign_xray_receive_spid ON imed.assign_xray USING btree (receive_spid);


--
-- Name: assign_xray_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX assign_xray_visit_id ON imed.assign_xray USING btree (visit_id);


--
-- Name: att_physician_doctor_status; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX att_physician_doctor_status ON imed.attending_physician USING btree (fix_doctor_visit_status);


--
-- Name: atten_phy_eid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX atten_phy_eid ON imed.attending_physician USING btree (employee_id);


--
-- Name: atten_priority; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX atten_priority ON imed.attending_physician USING btree (priority);


--
-- Name: b_base_country_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX b_base_country_id ON imed.base_state USING btree (base_country_id);


--
-- Name: base_address_ampcode; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX base_address_ampcode ON imed.base_address USING btree (ampcode);


--
-- Name: base_address_cgwcode; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX base_address_cgwcode ON imed.base_address USING btree (cgwcode);


--
-- Name: base_address_description; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX base_address_description ON imed.base_address USING btree (description);


--
-- Name: base_address_type; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX base_address_type ON imed.base_address USING btree (type);


--
-- Name: base_antibiotic_desc_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX base_antibiotic_desc_idx ON imed.base_antibiotic USING btree (description);


--
-- Name: base_branch_bank_amphur_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX base_branch_bank_amphur_id ON imed.base_branch_bank USING btree (fix_amphur_id);


--
-- Name: base_branch_bank_bank_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX base_branch_bank_bank_id ON imed.base_branch_bank USING btree (base_bank_id);


--
-- Name: base_branch_bank_changwat_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX base_branch_bank_changwat_id ON imed.base_branch_bank USING btree (fix_changwat_id);


--
-- Name: base_branch_bank_tambol_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX base_branch_bank_tambol_id ON imed.base_branch_bank USING btree (fix_tambol_id);


--
-- Name: base_diagnosis_employee_clinic_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX base_diagnosis_employee_clinic_id ON imed.base_diagnosis_employee USING btree (base_clinic_id);


--
-- Name: base_diagnosis_employee_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX base_diagnosis_employee_id ON imed.base_diagnosis_employee USING btree (employee_id);


--
-- Name: base_drug_usag_ebase_drug_usage_code; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX base_drug_usag_ebase_drug_usage_code ON imed.base_drug_usage USING btree (base_drug_usage_code);


--
-- Name: base_dt_item_item_id_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX base_dt_item_item_id_idx ON imed.base_dt_item USING btree (item_id);


--
-- Name: base_lab_seq_lab_type_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX base_lab_seq_lab_type_id ON imed.base_lab_seq_ln_type USING btree (base_lab_type_id);


--
-- Name: base_lab_sp_type_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX base_lab_sp_type_idx ON imed.base_lab_service_point USING btree (base_lab_type_id);


--
-- Name: base_obs_item_room_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX base_obs_item_room_idx ON imed.base_room_observe_item USING btree (base_room_id);


--
-- Name: base_office_amphur_key; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX base_office_amphur_key ON imed.base_office USING btree (amphur);


--
-- Name: base_office_changwat_key; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX base_office_changwat_key ON imed.base_office USING btree (changwat);


--
-- Name: base_op_emp_clinic_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX base_op_emp_clinic_id ON imed.base_operation_employee USING btree (base_clinic_id);


--
-- Name: base_pt_cs_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX base_pt_cs_id ON imed.base_patient_classify_determiners USING btree (base_patient_classify_standard_id);


--
-- Name: base_room_base_service_point_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX base_room_base_service_point_id ON imed.base_room USING btree (base_service_point_id);


--
-- Name: base_room_bed_number; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX base_room_bed_number ON imed.base_room USING btree (bed_number);


--
-- Name: base_room_fix_bed_status_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX base_room_fix_bed_status_id ON imed.base_room USING btree (fix_bed_status_id);


--
-- Name: base_room_item_room_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX base_room_item_room_id ON imed.base_room_item USING btree (base_room_id);


--
-- Name: base_room_room_number; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX base_room_room_number ON imed.base_room USING btree (room_number);


--
-- Name: base_room_service_status; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX base_room_service_status ON imed.base_room USING btree (fix_service_status_id);


--
-- Name: base_room_type_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX base_room_type_id ON imed.base_room USING btree (base_room_type_id);


--
-- Name: base_sp_ip_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX base_sp_ip_idx ON imed.base_service_point_ip_address USING btree (ip_address);


--
-- Name: base_template_advice_description; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX base_template_advice_description ON imed.base_template_advice USING btree (description);


--
-- Name: base_template_app_description; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX base_template_app_description ON imed.base_template_app_detail USING btree (description);


--
-- Name: base_template_illness_clinic; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX base_template_illness_clinic ON imed.base_template_illness USING btree (base_clinic_id);


--
-- Name: base_template_illness_description; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX base_template_illness_description ON imed.base_template_illness USING btree (description);


--
-- Name: base_template_item_set_multi_visit_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX base_template_item_set_multi_visit_item_id ON imed.base_template_item_set_multi_visit USING btree (item_set_id);


--
-- Name: base_template_symptom_clinic; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX base_template_symptom_clinic ON imed.base_template_symptom USING btree (base_clinic_id);


--
-- Name: base_template_symptom_description; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX base_template_symptom_description ON imed.base_template_symptom USING btree (description);


--
-- Name: bb_cross_matching_order_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX bb_cross_matching_order_idx ON imed.bb_cross_matching USING btree (order_item_id);


--
-- Name: bdggn_generic_name; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX bdggn_generic_name ON imed.base_drug_generic_group_name USING btree (base_drug_generic_name);


--
-- Name: bdggn_group_name; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX bdggn_group_name ON imed.base_drug_generic_group_name USING btree (base_drug_group_name);


--
-- Name: bdgtn_generic_name; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX bdgtn_generic_name ON imed.base_drug_generic_trade_name USING btree (base_drug_generic_name);


--
-- Name: bdgtn_trade_name; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX bdgtn_trade_name ON imed.base_drug_generic_trade_name USING btree (base_drug_trade_name);


--
-- Name: bdi_base_drug_interaction_id_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX bdi_base_drug_interaction_id_idx ON imed.base_drug_interaction USING btree (base_drug_interaction_id);


--
-- Name: bdi_drug_generic_name_1_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX bdi_drug_generic_name_1_idx ON imed.base_drug_interaction USING btree (drug_generic_name_1);


--
-- Name: bed_booking_booking_name; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX bed_booking_booking_name ON imed.bed_booking USING btree (booking_name);


--
-- Name: bed_booking_patient_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX bed_booking_patient_id ON imed.bed_booking USING btree (patient_id);


--
-- Name: bed_management_admit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX bed_management_admit_id ON imed.bed_management USING btree (admit_id);


--
-- Name: bed_management_base_sp_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX bed_management_base_sp_id ON imed.bed_management USING btree (base_service_point_id);


--
-- Name: bed_management_current_bed; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX bed_management_current_bed ON imed.bed_management USING btree (current_bed);


--
-- Name: bed_mgnt_order_cont_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX bed_mgnt_order_cont_id ON imed.bed_mgnt_order_continue USING btree (bed_management_id);


--
-- Name: consult_case_consult_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX consult_case_consult_idx ON imed.consult_case USING btree (consult_id);


--
-- Name: consult_case_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX consult_case_idx ON imed.consult_case USING btree (base_consult_case_id);


--
-- Name: consult_case_result_case_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX consult_case_result_case_idx ON imed.consult_case_result USING btree (consult_case_id);


--
-- Name: consult_receive_eid_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX consult_receive_eid_idx ON imed.consult USING btree (receive_eid);


--
-- Name: consult_request_date_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX consult_request_date_idx ON imed.consult USING btree (request_date);


--
-- Name: consult_request_eid_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX consult_request_eid_idx ON imed.consult USING btree (request_eid);


--
-- Name: consult_request_spid_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX consult_request_spid_idx ON imed.consult USING btree (request_spid);


--
-- Name: consult_status_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX consult_status_idx ON imed.consult USING btree (fix_consult_status_id);


--
-- Name: consult_to_eid_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX consult_to_eid_idx ON imed.consult USING btree (consult_to_eid);


--
-- Name: consult_to_spid_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX consult_to_spid_idx ON imed.consult USING btree (consult_to_spid);


--
-- Name: consult_treatment_case_result_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX consult_treatment_case_result_idx ON imed.consult_treatment USING btree (consult_case_result_id);


--
-- Name: daily_record_ipd_admit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX daily_record_ipd_admit_id ON imed.daily_record_ipd USING btree (admit_id);


--
-- Name: dental_cont_ref_no; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX dental_cont_ref_no ON imed.dental USING btree (continue_ref_no);


--
-- Name: dental_continue_type; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX dental_continue_type ON imed.dental USING btree (continue_type);


--
-- Name: dental_dent_organic; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX dental_dent_organic ON imed.dental USING btree (base_dent_organic_id);


--
-- Name: dental_dental_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX dental_dental_date ON imed.dental USING btree (dental_date);


--
-- Name: dental_dental_time; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX dental_dental_time ON imed.dental USING btree (dental_time);


--
-- Name: dental_order_cont_ref_no; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX dental_order_cont_ref_no ON imed.dental_order USING btree (continue_ref_no);


--
-- Name: dental_order_dental_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX dental_order_dental_id ON imed.dental_order USING btree (dental_id);


--
-- Name: dental_order_operation_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX dental_order_operation_date ON imed.dental_order USING btree (operation_date);


--
-- Name: dental_order_operation_time; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX dental_order_operation_time ON imed.dental_order USING btree (operation_time);


--
-- Name: dental_order_order_item; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX dental_order_order_item ON imed.dental_order USING btree (order_item_id);


--
-- Name: dental_patient_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX dental_patient_id ON imed.dental USING btree (patient_id);


--
-- Name: dental_plan_order_dental_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX dental_plan_order_dental_id ON imed.dental_plan_order USING btree (dental_id);


--
-- Name: dental_teeth_dental_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX dental_teeth_dental_id ON imed.dental_teeth USING btree (dental_id);


--
-- Name: dental_teeth_tooth_number; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX dental_teeth_tooth_number ON imed.dental_teeth USING btree (tooth_number);


--
-- Name: dental_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX dental_visit_id ON imed.dental USING btree (visit_id);


--
-- Name: df_approve_order_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX df_approve_order_item_id ON imed.df_approve USING btree (order_item_id);


--
-- Name: df_order_item_employee_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX df_order_item_employee_id ON imed.df_order_item USING btree (employee_id);


--
-- Name: df_order_item_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX df_order_item_item_id ON imed.df_order_item USING btree (item_id);


--
-- Name: df_order_item_set_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX df_order_item_set_item_id ON imed.df_order_item_set USING btree (item_id);


--
-- Name: df_order_item_set_item_set_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX df_order_item_set_item_set_id ON imed.df_order_item_set USING btree (item_set_id);


--
-- Name: diagnosis_icd10_fix_diagn; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX diagnosis_icd10_fix_diagn ON imed.diagnosis_icd10 USING btree (fix_diagnosis_type_id);


--
-- Name: diagnosis_icd10_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX diagnosis_icd10_visit_id ON imed.diagnosis_icd10 USING btree (visit_id);


--
-- Name: diagnosis_icd9_fix_operat; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX diagnosis_icd9_fix_operat ON imed.diagnosis_icd9 USING btree (fix_operation_type_id);


--
-- Name: diagnosis_icd9_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX diagnosis_icd9_visit_id ON imed.diagnosis_icd9 USING btree (visit_id);


--
-- Name: dialysis_regist_med_device_used; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX dialysis_regist_med_device_used ON imed.dialysis_registered USING btree (medical_device_used_id);


--
-- Name: docscan_comments_docscan_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX docscan_comments_docscan_id ON imed.document_scan_comments USING btree (document_scan_id);


--
-- Name: docscan_comments_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX docscan_comments_visit_id ON imed.document_scan_comments USING btree (visit_id);


--
-- Name: docscan_drawing_docscan_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX docscan_drawing_docscan_id ON imed.document_scan_drawing USING btree (document_scan_id);


--
-- Name: docscan_drawing_x; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX docscan_drawing_x ON imed.document_scan_drawing USING btree (x);


--
-- Name: docscan_drawing_y; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX docscan_drawing_y ON imed.document_scan_drawing USING btree (y);


--
-- Name: docscan_dx_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX docscan_dx_date ON imed.document_scan_dx_date USING btree (dx_date);


--
-- Name: docscan_dx_docscan_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX docscan_dx_docscan_id ON imed.document_scan_dx_date USING btree (document_scan_id);


--
-- Name: docscan_patient_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX docscan_patient_id ON imed.document_scan USING btree (patient_id);


--
-- Name: docscan_prescription_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX docscan_prescription_id ON imed.document_scan USING btree (prescription_id);


--
-- Name: docscan_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX docscan_visit_id ON imed.document_scan USING btree (visit_id);


--
-- Name: doctor_abs_dow; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX doctor_abs_dow ON imed.doctor_absent USING btree (fix_day_of_week);


--
-- Name: doctor_abs_employee; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX doctor_abs_employee ON imed.doctor_absent USING btree (employee_id);


--
-- Name: doctor_abs_schedule; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX doctor_abs_schedule ON imed.doctor_absent USING btree (doctor_schedule_id);


--
-- Name: doctor_abs_spid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX doctor_abs_spid ON imed.doctor_absent USING btree (spid);


--
-- Name: doctor_diagnosis_doctor_eid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX doctor_diagnosis_doctor_eid ON imed.doctor_diagnosis USING btree (doctor_eid);


--
-- Name: doctor_discharge_opd_visi; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX doctor_discharge_opd_visi ON imed.doctor_discharge_opd USING btree (visit_id);


--
-- Name: doctor_ds_ipd_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX doctor_ds_ipd_visit_id ON imed.doctor_discharge_ipd USING btree (visit_id);


--
-- Name: doctor_note_base_clinic_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX doctor_note_base_clinic_id ON imed.base_template_doctor_note USING btree (base_clinic_id);


--
-- Name: doctor_note_description; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX doctor_note_description ON imed.base_template_doctor_note USING btree (description);


--
-- Name: doctor_note_employee_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX doctor_note_employee_id ON imed.base_template_doctor_note USING btree (employee_id);


--
-- Name: doctor_sche_dow; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX doctor_sche_dow ON imed.doctor_schedule USING btree (fix_day_of_week);


--
-- Name: doctor_sche_employee; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX doctor_sche_employee ON imed.doctor_schedule USING btree (employee_id);


--
-- Name: doctor_sche_spid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX doctor_sche_spid ON imed.doctor_schedule USING btree (spid);


--
-- Name: drawing_details_phyex_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX drawing_details_phyex_id ON imed.drawing_details USING btree (drawing_physical_exam_id);


--
-- Name: drawing_phyex_attphy_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX drawing_phyex_attphy_id ON imed.drawing_physical_exam USING btree (attending_physician_id);


--
-- Name: drawing_phyex_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX drawing_phyex_visit_id ON imed.drawing_physical_exam USING btree (visit_id);


--
-- Name: drug_allergy_adr_group; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX drug_allergy_adr_group ON imed.drug_allergy USING btree (adr_group_name);


--
-- Name: drug_allergy_generic; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX drug_allergy_generic ON imed.drug_allergy USING btree (generic_name);


--
-- Name: drug_allergy_patient_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX drug_allergy_patient_id ON imed.drug_allergy USING btree (patient_id);


--
-- Name: drug_allergy_temp_patient; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX drug_allergy_temp_patient ON imed.drug_allergy_temp USING btree (patient_id);


--
-- Name: drug_allergy_trade; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX drug_allergy_trade ON imed.drug_allergy USING btree (trade_name);


--
-- Name: drug_specialist_item_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX drug_specialist_item_idx ON imed.drug_specialist_only USING btree (item_id);


--
-- Name: drug_sub_major_class; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX drug_sub_major_class ON imed.base_drug_sub_class USING btree (base_drug_major_class_id);


--
-- Name: dt_diagnosis_patient_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX dt_diagnosis_patient_id ON imed.dt_diagnosis USING btree (patient_id);


--
-- Name: dt_diagnosis_tooth_number; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX dt_diagnosis_tooth_number ON imed.dt_diagnosis USING btree (tooth_number);


--
-- Name: dt_diagnosis_vs_extend_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX dt_diagnosis_vs_extend_id ON imed.dt_diagnosis USING btree (vital_sign_extend_id);


--
-- Name: dt_external_patient_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX dt_external_patient_id ON imed.dt_treatment_external USING btree (patient_id);


--
-- Name: dt_external_tooth_number; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX dt_external_tooth_number ON imed.dt_treatment_external USING btree (tooth_number);


--
-- Name: dt_external_tx_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX dt_external_tx_date ON imed.dt_treatment_external USING btree (treatment_date);


--
-- Name: dt_external_tx_time; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX dt_external_tx_time ON imed.dt_treatment_external USING btree (treatment_time);


--
-- Name: dt_teeth_chart_patient_id_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX dt_teeth_chart_patient_id_idx ON imed.dt_teeth_chart USING btree (patient_id);


--
-- Name: dt_treatment_course_completed; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX dt_treatment_course_completed ON imed.dt_treatment USING btree (is_course_completed);


--
-- Name: dt_treatment_course_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX dt_treatment_course_id ON imed.dt_treatment USING btree (course_treatment_id);


--
-- Name: dt_treatment_item_type_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX dt_treatment_item_type_id ON imed.dt_treatment USING btree (fix_item_type_id);


--
-- Name: dt_treatment_order_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX dt_treatment_order_item_id ON imed.dt_treatment USING btree (order_item_id);


--
-- Name: dt_treatment_patient_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX dt_treatment_patient_id ON imed.dt_treatment USING btree (patient_id);


--
-- Name: dt_treatment_plan_no; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX dt_treatment_plan_no ON imed.dt_treatment_plan USING btree (plan_no);


--
-- Name: dt_treatment_plan_patient_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX dt_treatment_plan_patient_id ON imed.dt_treatment_plan USING btree (patient_id);


--
-- Name: dt_treatment_tooth_number; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX dt_treatment_tooth_number ON imed.dt_treatment USING btree (tooth_number);


--
-- Name: dt_treatment_tx_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX dt_treatment_tx_date ON imed.dt_treatment USING btree (treatment_date);


--
-- Name: dt_treatment_tx_time; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX dt_treatment_tx_time ON imed.dt_treatment USING btree (treatment_time);


--
-- Name: dt_treatment_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX dt_treatment_visit_id ON imed.dt_treatment USING btree (visit_id);


--
-- Name: emergency_accident_emergency_data; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX emergency_accident_emergency_data ON imed.emergency_accident USING btree (emergency_data_id);


--
-- Name: emergency_accident_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX emergency_accident_visit_id ON imed.emergency_accident USING btree (visit_id);


--
-- Name: emergency_accident_zone_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX emergency_accident_zone_id ON imed.emergency_accident USING btree (base_accident_zone_id);


--
-- Name: emergency_data_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX emergency_data_visit_id ON imed.emergency_data USING btree (visit_id);


--
-- Name: emergency_vs_emergency_data; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX emergency_vs_emergency_data ON imed.emergency_vital_sign USING btree (emergency_data_id);


--
-- Name: employee_department_department_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX employee_department_department_idx ON imed.employee_department USING btree (base_department_id);


--
-- Name: employee_department_employee_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX employee_department_employee_idx ON imed.employee_department USING btree (employee_id);


--
-- Name: employee_employee_code; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX employee_employee_code ON imed.employee USING btree (employee_code);


--
-- Name: employee_fix_employee_type_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX employee_fix_employee_type_id ON imed.employee USING btree (fix_employee_type_id);


--
-- Name: employee_role_employee_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX employee_role_employee_id ON imed.employee_role USING btree (employee_id);


--
-- Name: fda_base_service_point_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fda_base_service_point_id ON imed.feature_doctor_assign USING btree (base_service_point_id);


--
-- Name: feature_exception_type_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX feature_exception_type_id ON imed.feature_exception USING btree (fix_exception_type_id);


--
-- Name: fix_icd9_code; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fix_icd9_code ON imed.fix_icd9 USING btree (code);


--
-- Name: fix_icd9_nick_name; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fix_icd9_nick_name ON imed.fix_icd9 USING btree (nick_name);


--
-- Name: fix_idx10v3_description; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fix_idx10v3_description ON imed.fix_idx10v3 USING btree (description);


--
-- Name: fix_pindex99_description; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fix_pindex99_description ON imed.fix_pindex99 USING btree (description);


--
-- Name: forensic_data_forensic_data_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX forensic_data_forensic_data_id ON imed.forensic_data USING btree (forensic_data_id);


--
-- Name: forensic_data_forensic_number; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX forensic_data_forensic_number ON imed.forensic_data USING btree (forensic_number);


--
-- Name: ft_cash_doc_rcpt_type_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX ft_cash_doc_rcpt_type_id ON imed.feature_cash_doc USING btree (fix_receipt_type_id);


--
-- Name: ft_cash_doc_visit_type_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX ft_cash_doc_visit_type_id ON imed.feature_cash_doc USING btree (fix_visit_type_id);


--
-- Name: help_admit_physician_data_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX help_admit_physician_data_visit_id ON imed.help_admit_physician_data USING btree (visit_id);


--
-- Name: hp_checkup_checkup_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX hp_checkup_checkup_id ON imed.hp_checkup USING btree (hp_checkup_id);


--
-- Name: hp_checkup_sugges_hp_checkup_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX hp_checkup_sugges_hp_checkup_id ON imed.hp_checkup_sugges USING btree (hp_checkup_id);


--
-- Name: hp_checkup_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX hp_checkup_visit_id ON imed.hp_checkup USING btree (visit_id);


--
-- Name: hp_medical_history_hp_checkup_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX hp_medical_history_hp_checkup_id ON imed.hp_medical_history USING btree (hp_checkup_id);


--
-- Name: hp_medical_history_hp_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX hp_medical_history_hp_id ON imed.hp_medical_history USING btree (hp_medical_history_id);


--
-- Name: hp_patient_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX hp_patient_idx ON imed.health_promotion_visit USING btree (patient_id);


--
-- Name: hp_visit_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX hp_visit_idx ON imed.health_promotion_visit USING btree (visit_id);


--
-- Name: hp_xray_visit_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX hp_xray_visit_idx ON imed.health_promotion_xray_result USING btree (health_promotion_visit_id);


--
-- Name: idx_appoint_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_appoint_visit_id ON imed.appointment USING btree (visit_id);


--
-- Name: idx_bed_mgnt_room_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_bed_mgnt_room_id ON imed.bed_management USING btree (base_room_id);


--
-- Name: idx_employee_report_role_eid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_employee_report_role_eid ON imed.employee_report_role USING btree (employee_id);


--
-- Name: idx_imed_config_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_imed_config_date ON imed.imed_config USING btree (modify_date);


--
-- Name: idx_imed_config_time; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_imed_config_time ON imed.imed_config USING btree (modify_time);


--
-- Name: idx_imed_lang_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_imed_lang_date ON imed.imed_language USING btree (modify_date);


--
-- Name: idx_imed_lang_time; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_imed_lang_time ON imed.imed_language USING btree (modify_time);


--
-- Name: idx_item_billgrp_home_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_item_billgrp_home_id ON imed.item USING btree (base_billing_group_home_id);


--
-- Name: idx_item_billgrp_ipd_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_item_billgrp_ipd_id ON imed.item USING btree (base_billing_group_ipd_id);


--
-- Name: idx_item_billgrp_opd_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_item_billgrp_opd_id ON imed.item USING btree (base_billing_group_opd_id);


--
-- Name: idx_order_continue_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_order_continue_item_id ON imed.order_continue USING btree (item_id);


--
-- Name: idx_order_item_accession_number; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_order_item_accession_number ON imed.order_item USING btree (accession_number);


--
-- Name: idx_report_drug_interaction_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_report_drug_interaction_visit_id ON imed.report_drug_interaction USING btree (visit_id);


--
-- Name: idx_report_drug_pregnancy_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_report_drug_pregnancy_visit_id ON imed.report_drug_pregnancy USING btree (visit_id);


--
-- Name: idx_return_drug_pat_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_return_drug_pat_id ON imed.return_drug USING btree (patient_id);


--
-- Name: idx_service_point_schedule_sp_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_service_point_schedule_sp_id ON imed.service_point_schedule USING btree (base_service_point_id);


--
-- Name: idx_servtimestamp_assign; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_servtimestamp_assign ON imed.service_time_stamp USING btree (assign_date, assign_time);


--
-- Name: idx_servtimestamp_begin; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_servtimestamp_begin ON imed.service_time_stamp USING btree (begin_date, begin_time);


--
-- Name: idx_servtimestamp_doctor_eid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_servtimestamp_doctor_eid ON imed.service_time_stamp USING btree (doctor_eid);


--
-- Name: idx_servtimestamp_finish; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_servtimestamp_finish ON imed.service_time_stamp USING btree (finish_date, finish_time);


--
-- Name: idx_servtimestamp_spid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_servtimestamp_spid ON imed.service_time_stamp USING btree (spid);


--
-- Name: idx_servtimestamp_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_servtimestamp_visit_id ON imed.service_time_stamp USING btree (visit_id);


--
-- Name: idx_stk_order_distributor_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_stk_order_distributor_id ON imed.stock_order USING btree (distributor_id);


--
-- Name: idx_stk_order_from_stock_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_stk_order_from_stock_id ON imed.stock_order USING btree (order_from_stock_id);


--
-- Name: idx_stk_order_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_stk_order_item_id ON imed.stock_order USING btree (item_id);


--
-- Name: idx_stk_order_status; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_stk_order_status ON imed.stock_order USING btree (fix_stock_order_status_id);


--
-- Name: idx_stk_poffer_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_stk_poffer_date ON imed.stock_order USING btree (purchasing_offer_date);


--
-- Name: idx_stk_poffer_number; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_stk_poffer_number ON imed.stock_order USING btree (purchasing_offer_number);


--
-- Name: idx_stk_poffer_time; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_stk_poffer_time ON imed.stock_order USING btree (purchasing_offer_time);


--
-- Name: idx_timestampdetail_basetimestampid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_timestampdetail_basetimestampid ON imed.time_stamp_details USING btree (base_time_stamp_id);


--
-- Name: idx_visit_spccard_disc_spccard_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_visit_spccard_disc_spccard_id ON imed.visit_special_card USING btree (base_disc_special_card_id);


--
-- Name: index_lab_result_assign_lab_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_lab_result_assign_lab_id ON imed.lab_result USING btree (assign_lab_id);


--
-- Name: index_lab_result_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_lab_result_item_id ON imed.lab_result USING btree (item_id);


--
-- Name: index_lab_result_order_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_lab_result_order_item_id ON imed.lab_result USING btree (order_item_id);


--
-- Name: index_lab_result_patient_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_lab_result_patient_id ON imed.lab_result USING btree (patient_id);


--
-- Name: index_xray_execute_detail_assign_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_xray_execute_detail_assign_id ON imed.xray_execute_detail USING btree (assign_xray_id);


--
-- Name: index_xray_execute_detail_order_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_xray_execute_detail_order_item_id ON imed.xray_execute_detail USING btree (order_item_id);


--
-- Name: index_xray_film_execute_detail; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_xray_film_execute_detail ON imed.xray_film USING btree (xray_execute_detail_id);


--
-- Name: index_xray_film_status; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_xray_film_status ON imed.xray_film USING btree (fix_film_status_id);


--
-- Name: index_xray_reported; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_xray_reported ON imed.xray_result USING btree (reported);


--
-- Name: index_xray_result_assign_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_xray_result_assign_id ON imed.xray_result USING btree (assign_xray_id);


--
-- Name: index_xray_result_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_xray_result_item_id ON imed.xray_result USING btree (item_id);


--
-- Name: index_xray_result_order_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_xray_result_order_item_id ON imed.xray_result USING btree (order_item_id);


--
-- Name: index_xray_result_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_xray_result_visit_id ON imed.xray_result USING btree (visit_id);


--
-- Name: ipd_att_physician_admit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX ipd_att_physician_admit_id ON imed.ipd_attending_physician USING btree (admit_id);


--
-- Name: ipd_intake_vs_ipd_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX ipd_intake_vs_ipd_id ON imed.vs_ipd_intake USING btree (vital_sign_ipd_id);


--
-- Name: ipd_output_vs_ipd_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX ipd_output_vs_ipd_id ON imed.vs_ipd_output USING btree (vital_sign_ipd_id);


--
-- Name: ipd_physician_current_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX ipd_physician_current_idx ON imed.ipd_attending_physician USING btree (is_current);


--
-- Name: item_active; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX item_active ON imed.item USING btree (active);


--
-- Name: item_base_category_group; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX item_base_category_group ON imed.item USING btree (base_category_group_id);


--
-- Name: item_base_invoice_group; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX item_base_invoice_group ON imed.item USING btree (base_invoice_group_id);


--
-- Name: item_base_xray_type; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX item_base_xray_type ON imed.item USING btree (base_xray_type_id);


--
-- Name: item_common_name; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX item_common_name ON imed.item USING btree (common_name);


--
-- Name: item_discount_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX item_discount_item_id ON imed.item_discount USING btree (item_id);


--
-- Name: item_discount_plan_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX item_discount_plan_id ON imed.item_discount USING btree (plan_id);


--
-- Name: item_dispensed_active; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX item_dispensed_active ON imed.item_dispensed USING btree (active);


--
-- Name: item_dispensed_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX item_dispensed_item_id ON imed.item_dispensed USING btree (item_id);


--
-- Name: item_dispensed_stock_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX item_dispensed_stock_id ON imed.item_dispensed USING btree (stock_id);


--
-- Name: item_drug_generic_name; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX item_drug_generic_name ON imed.item USING btree (drug_generic_name);


--
-- Name: item_drug_group_name; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX item_drug_group_name ON imed.item USING btree (drug_group_name);


--
-- Name: item_drug_trade_name; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX item_drug_trade_name ON imed.item USING btree (drug_trade_name);


--
-- Name: item_fix_chrgitem_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX item_fix_chrgitem_id ON imed.item USING btree (fix_chrgitem_id);


--
-- Name: item_fix_item_type_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX item_fix_item_type_id ON imed.item USING btree (fix_item_type_id);


--
-- Name: item_image_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX item_image_item_id ON imed.item_image USING btree (item_id);


--
-- Name: item_item_code; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX item_item_code ON imed.item USING btree (item_code);


--
-- Name: item_nhso_drug_code_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX item_nhso_drug_code_id ON imed.item USING btree (nhso_drug_code_id);


--
-- Name: item_nhso_vaccine_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX item_nhso_vaccine_id ON imed.item USING btree (nhso_vaccine_id);


--
-- Name: item_nick_name; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX item_nick_name ON imed.item USING btree (nick_name);


--
-- Name: item_plan_group_group_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX item_plan_group_group_id ON imed.item_plan_group USING btree (base_plan_group_id);


--
-- Name: item_plan_grp_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX item_plan_grp_item_id ON imed.item_plan_group USING btree (item_id);


--
-- Name: item_plan_grp_plan_grp_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX item_plan_grp_plan_grp_id ON imed.item_plan_group USING btree (base_plan_group_id);


--
-- Name: item_price_base_tariff_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX item_price_base_tariff_id ON imed.item_price USING btree (base_tariff_id);


--
-- Name: item_price_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX item_price_item_id ON imed.item_price USING btree (item_id);


--
-- Name: item_price_modify_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX item_price_modify_date ON imed.item_price USING btree (modify_date);


--
-- Name: item_price_modify_eid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX item_price_modify_eid ON imed.item_price USING btree (modify_eid);


--
-- Name: item_price_modify_time; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX item_price_modify_time ON imed.item_price USING btree (modify_time);


--
-- Name: item_rpk_item_dest_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX item_rpk_item_dest_id ON imed.item_repack USING btree (item_dest_id);


--
-- Name: item_rpk_item_src_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX item_rpk_item_src_id ON imed.item_repack USING btree (item_src_id);


--
-- Name: item_set_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX item_set_item_id ON imed.item_set USING btree (item_id);


--
-- Name: item_set_set_type; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX item_set_set_type ON imed.item_set USING btree (fix_order_set_type_id);


--
-- Name: item_sub_class_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX item_sub_class_id ON imed.item USING btree (base_drug_sub_class_id);


--
-- Name: ix_bb_donate_data_collect_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX ix_bb_donate_data_collect_date ON imed.bb_donate_data USING btree (collect_date);


--
-- Name: ix_bb_pack_cell_collect_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX ix_bb_pack_cell_collect_date ON imed.bb_pack_cell USING btree (collect_date);


--
-- Name: ix_bb_pack_cell_expire_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX ix_bb_pack_cell_expire_date ON imed.bb_pack_cell USING btree (expire_date);


--
-- Name: jbm_msg_ref_tx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX jbm_msg_ref_tx ON imed.jbm_msg_ref USING btree (transaction_id, state);


--
-- Name: lab_micro_lab_result; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lab_micro_lab_result ON imed.lab_microbiology USING btree (lab_result_id);


--
-- Name: lab_micro_organisms_position; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lab_micro_organisms_position ON imed.lab_microbiology USING btree (organisms_position);


--
-- Name: lab_micro_set_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lab_micro_set_idx ON imed.base_lab_micro_agent_set USING btree (base_lab_micro_agent_set_name);


--
-- Name: lab_nv_lab_test_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lab_nv_lab_test_id ON imed.template_lab_normal_value USING btree (template_lab_test_id);


--
-- Name: lab_nv_type_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lab_nv_type_id ON imed.template_lab_normal_value USING btree (fix_lab_normal_value_type_id);


--
-- Name: lab_result_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lab_result_visit_id ON imed.lab_result USING btree (visit_id);


--
-- Name: lab_test_active; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lab_test_active ON imed.lab_test USING btree (active);


--
-- Name: lab_test_lab_result_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lab_test_lab_result_id ON imed.lab_test USING btree (lab_result_id);


--
-- Name: lab_test_modify_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lab_test_modify_date ON imed.lab_test USING btree (modify_date);


--
-- Name: lab_test_modify_time; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lab_test_modify_time ON imed.lab_test USING btree (modify_time);


--
-- Name: lab_test_name; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lab_test_name ON imed.lab_test USING btree (name);


--
-- Name: lab_test_order_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lab_test_order_item_id ON imed.lab_test USING btree (order_item_id);


--
-- Name: lab_test_reported; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lab_test_reported ON imed.lab_test USING btree (reported);


--
-- Name: lab_test_template_lab_test_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lab_test_template_lab_test_id ON imed.lab_test USING btree (template_lab_test_id);


--
-- Name: lae_base_leo_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lae_base_leo_id ON imed.lr_anc_exam_organ USING btree (base_lr_exam_organ_id);


--
-- Name: lae_first_anc; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lae_first_anc ON imed.lr_anc_exam_organ USING btree (lr_first_anc_id);


--
-- Name: lfa_pregnancy_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lfa_pregnancy_id ON imed.lr_first_anc USING btree (lr_pregnancy_id);


--
-- Name: lis_order_ln_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lis_order_ln_idx ON imed.lis_order USING btree (ln);


--
-- Name: lis_order_test_no_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lis_order_test_no_idx ON imed.lis_order USING btree (test_no);


--
-- Name: location_record_location_spid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX location_record_location_spid ON imed.location_record USING btree (location_spid);


--
-- Name: location_record_operate_eid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX location_record_operate_eid ON imed.location_record USING btree (operate_eid);


--
-- Name: location_record_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX location_record_visit_id ON imed.location_record USING btree (visit_id);


--
-- Name: lr_anc_pregnancy_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lr_anc_pregnancy_id ON imed.lr_anc USING btree (lr_pregnancy_id);


--
-- Name: lr_anc_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lr_anc_visit_id ON imed.lr_anc USING btree (visit_id);


--
-- Name: lr_apgarscore_newborn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lr_apgarscore_newborn_id ON imed.lr_apgar_score USING btree (lr_newborn_id);


--
-- Name: lr_delivery_infant_lr_delivery_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lr_delivery_infant_lr_delivery_id ON imed.lr_delivery_infant USING btree (lr_delivery_id);


--
-- Name: lr_delivery_pregnancy_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lr_delivery_pregnancy_id ON imed.lr_delivery USING btree (lr_pregnancy_id);


--
-- Name: lr_delivery_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lr_delivery_visit_id ON imed.lr_delivery USING btree (visit_id);


--
-- Name: lr_med_newborn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lr_med_newborn_id ON imed.lr_newborn_medication USING btree (lr_newborn_id);


--
-- Name: lr_newborn_pregnancy_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lr_newborn_pregnancy_id ON imed.lr_newborn USING btree (lr_pregnancy_id);


--
-- Name: lr_newborn_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lr_newborn_visit_id ON imed.lr_newborn USING btree (visit_id);


--
-- Name: lr_other_labor_patient_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lr_other_labor_patient_id ON imed.lr_other_labor USING btree (patient_id);


--
-- Name: lr_pregnancy_abnormal_pregnancy_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lr_pregnancy_abnormal_pregnancy_id ON imed.lr_pregnancy_abnormal USING btree (lr_pregnancy_id);


--
-- Name: lr_pregnancy_active; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lr_pregnancy_active ON imed.lr_pregnancy USING btree (active);


--
-- Name: lr_pregnancy_patient_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lr_pregnancy_patient_id ON imed.lr_pregnancy USING btree (patient_id);


--
-- Name: lr_pregnancy_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lr_pregnancy_visit_id ON imed.lr_pregnancy USING btree (visit_id);


--
-- Name: lr_tet_vcn_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lr_tet_vcn_id ON imed.lr_tetanus_vaccine USING btree (lr_pregnancy_id);


--
-- Name: lr_us_gynae_exam_result_gynae_result_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lr_us_gynae_exam_result_gynae_result_id ON imed.lr_us_gynae_exam_result USING btree (lr_us_gynae_result_id);


--
-- Name: lr_us_gynae_result_lr_pregnancy_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lr_us_gynae_result_lr_pregnancy_id ON imed.lr_us_gynae_result USING btree (lr_pregnancy_id);


--
-- Name: lr_us_obs_exam_result_obs_fetus_result_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lr_us_obs_exam_result_obs_fetus_result_id ON imed.lr_us_obs_exam_result USING btree (lr_us_obs_fetus_result_id);


--
-- Name: lr_us_obs_fetus_result_lr_us_obs_result_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lr_us_obs_fetus_result_lr_us_obs_result_id ON imed.lr_us_obs_fetus_result USING btree (lr_us_obs_result_id);


--
-- Name: lr_us_obs_result_lr_pregnancy_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lr_us_obs_result_lr_pregnancy_id ON imed.lr_us_obs_result USING btree (lr_pregnancy_id);


--
-- Name: lr_us_result_lr_pregnancy_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lr_us_result_lr_pregnancy_id ON imed.lr_us_result USING btree (lr_pregnancy_id);


--
-- Name: lrdelmed_delivery_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX lrdelmed_delivery_id ON imed.lr_delivery_medication USING btree (lr_delivery_id);


--
-- Name: med_err_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX med_err_visit_id ON imed.medication_error USING btree (visit_id);


--
-- Name: medical_certificate_type_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX medical_certificate_type_id ON imed.medical_certificate USING btree (fix_medcert_type_id);


--
-- Name: medical_certificate_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX medical_certificate_visit_id ON imed.medical_certificate USING btree (visit_id);


--
-- Name: naranjo_drug_allergy; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX naranjo_drug_allergy ON imed.drug_allergy_naranjo USING btree (drug_allergy_id);


--
-- Name: nhso_506_group_506; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX nhso_506_group_506 ON imed.nhso_506 USING btree (group_506);


--
-- Name: nt_assign_sheet_type_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX nt_assign_sheet_type_id ON imed.nt_food_order_sheet USING btree (fix_nt_order_sheet_type_id);


--
-- Name: nt_assign_status_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX nt_assign_status_id ON imed.nt_food_order_sheet USING btree (assign_order_status);


--
-- Name: nt_assign_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX nt_assign_visit_id ON imed.nt_food_order_sheet USING btree (visit_id);


--
-- Name: nt_chage_order_status_food_order_sheet_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX nt_chage_order_status_food_order_sheet_id ON imed.nt_change_order_sheet USING btree (nt_food_order_sheet_id);


--
-- Name: nt_nutrition_facts_nt_order_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX nt_nutrition_facts_nt_order_id ON imed.nt_nutrition_facts USING btree (nt_order_id);


--
-- Name: nt_order_assign_order_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX nt_order_assign_order_id ON imed.nt_order USING btree (nt_food_order_sheet_id);


--
-- Name: nt_order_meal_type_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX nt_order_meal_type_id ON imed.nt_order USING btree (fix_nt_meal_type_id);


--
-- Name: nt_order_nt_type_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX nt_order_nt_type_id ON imed.nt_order USING btree (base_nt_type_id);


--
-- Name: nt_order_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX nt_order_visit_id ON imed.nt_order USING btree (visit_id);


--
-- Name: nt_patient_nutrition_patient_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX nt_patient_nutrition_patient_id ON imed.nt_patient_nutrition USING btree (patient_id);


--
-- Name: nt_special_group_order_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX nt_special_group_order_id ON imed.nt_special_group USING btree (nt_order_id);


--
-- Name: nt_spm_diet_order_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX nt_spm_diet_order_id ON imed.nt_supplement_diet USING btree (nt_order_id);


--
-- Name: nt_therap_diet_order_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX nt_therap_diet_order_id ON imed.nt_therapeutic_diet USING btree (nt_order_id);


--
-- Name: nursing_care_plan_admit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX nursing_care_plan_admit_id ON imed.nursing_care_plan USING btree (admit_id);


--
-- Name: nursing_care_plan_begin_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX nursing_care_plan_begin_date ON imed.nursing_care_plan USING btree (start_date);


--
-- Name: nursing_care_plan_finish_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX nursing_care_plan_finish_date ON imed.nursing_care_plan USING btree (finish_date);


--
-- Name: nursing_care_plan_ncp_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX nursing_care_plan_ncp_id ON imed.nursing_care_plan USING btree (nursing_care_plan_id);


--
-- Name: odi_order_date_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX odi_order_date_idx ON imed.order_drug_interaction USING btree (order_date);


--
-- Name: odi_order_drug_interaction_id_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX odi_order_drug_interaction_id_idx ON imed.order_drug_interaction USING btree (order_drug_interaction_id);


--
-- Name: odi_order_time_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX odi_order_time_idx ON imed.order_drug_interaction USING btree (order_time);


--
-- Name: old_hn_new_hn_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX old_hn_new_hn_idx ON imed.old_hn USING btree (new_hn);


--
-- Name: old_hn_old_hn_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX old_hn_old_hn_idx ON imed.old_hn USING btree (old_hn);


--
-- Name: old_hn_patient_id_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX old_hn_patient_id_idx ON imed.old_hn USING btree (patient_id);


--
-- Name: omg_general_hn; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX omg_general_hn ON imed.omega_general_data USING btree (hn);


--
-- Name: omg_general_ln; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX omg_general_ln ON imed.omega_general_data USING btree (ln);


--
-- Name: omg_general_seq; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX omg_general_seq ON imed.omega_general_data USING btree (sequence_number);


--
-- Name: omg_order_rlt_hn; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX omg_order_rlt_hn ON imed.omega_order_and_result_data USING btree (omega_general_data_id);


--
-- Name: omg_order_rlt_ln; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX omg_order_rlt_ln ON imed.omega_order_and_result_data USING btree (item_name);


--
-- Name: omg_order_rlt_seq; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX omg_order_rlt_seq ON imed.omega_order_and_result_data USING btree (sequence_number);


--
-- Name: op_doctor_schedule_employee_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX op_doctor_schedule_employee_id ON imed.op_doctor_schedule USING btree (employee_id);


--
-- Name: op_posture_op_reg_id_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX op_posture_op_reg_id_idx ON imed.op_operation_posture USING btree (op_registered_operation_id);


--
-- Name: op_registered_anes_type_registered_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX op_registered_anes_type_registered_id ON imed.op_registered_anes_type USING btree (op_registered_id);


--
-- Name: op_registered_diagnosis_registered_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX op_registered_diagnosis_registered_id ON imed.op_registered_diagnosis USING btree (op_registered_id);


--
-- Name: op_registered_finish_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX op_registered_finish_date ON imed.op_registered USING btree (finish_date);


--
-- Name: op_registered_finish_time; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX op_registered_finish_time ON imed.op_registered USING btree (finish_time);


--
-- Name: op_registered_operation_registered_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX op_registered_operation_registered_id ON imed.op_registered_operation USING btree (op_registered_id);


--
-- Name: op_registered_patient_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX op_registered_patient_id ON imed.op_registered USING btree (patient_id);


--
-- Name: op_registered_physician_employee_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX op_registered_physician_employee_id ON imed.op_registered_physician USING btree (employee_id);


--
-- Name: op_registered_physician_registered_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX op_registered_physician_registered_id ON imed.op_registered_physician USING btree (op_registered_id);


--
-- Name: op_registered_start_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX op_registered_start_date ON imed.op_registered USING btree (start_date);


--
-- Name: op_registered_start_time; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX op_registered_start_time ON imed.op_registered USING btree (start_time);


--
-- Name: op_registered_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX op_registered_visit_id ON imed.op_registered USING btree (visit_id);


--
-- Name: op_room_schedule_base_op_room_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX op_room_schedule_base_op_room_id ON imed.op_room_schedule USING btree (base_op_room_id);


--
-- Name: op_set_fix_op_set_status; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX op_set_fix_op_set_status ON imed.op_set USING btree (fix_op_set_status_id);


--
-- Name: op_set_op_appoint_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX op_set_op_appoint_date ON imed.op_set USING btree (op_appoint_date);


--
-- Name: op_set_op_appoint_time; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX op_set_op_appoint_time ON imed.op_set USING btree (op_appoint_time);


--
-- Name: op_set_patient; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX op_set_patient ON imed.op_set USING btree (patient_id);


--
-- Name: opset_anes_opset_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX opset_anes_opset_id ON imed.op_set_anes_type USING btree (op_set_id);


--
-- Name: opset_delay_opset_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX opset_delay_opset_id ON imed.op_set_delay USING btree (op_set_id);


--
-- Name: opset_delay_status_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX opset_delay_status_id ON imed.op_set_delay USING btree (fix_op_set_status_id);


--
-- Name: opset_diag_opset_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX opset_diag_opset_id ON imed.op_set_diagnosis USING btree (op_set_id);


--
-- Name: opset_extra_opset_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX opset_extra_opset_id ON imed.op_set_extra USING btree (op_set_id);


--
-- Name: opset_op_opset_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX opset_op_opset_id ON imed.op_set_operation USING btree (op_set_id);


--
-- Name: opset_phy_opset_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX opset_phy_opset_id ON imed.op_set_physician USING btree (op_set_id);


--
-- Name: order_assigned_ref_no; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_assigned_ref_no ON imed.order_item USING btree (assigned_ref_no);


--
-- Name: order_continue_order_continue_status; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_continue_order_continue_status ON imed.order_continue USING btree (fix_order_continue_status_id);


--
-- Name: order_continue_prepared_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_continue_prepared_item_id ON imed.order_continue_prepared USING btree (item_id);


--
-- Name: order_continue_prepared_order_continue_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_continue_prepared_order_continue_id ON imed.order_continue_prepared USING btree (order_continue_id);


--
-- Name: order_continue_prepared_order_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_continue_prepared_order_date ON imed.order_continue_prepared USING btree (order_date);


--
-- Name: order_continue_prepared_order_time; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_continue_prepared_order_time ON imed.order_continue_prepared USING btree (order_time);


--
-- Name: order_continue_prepared_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_continue_prepared_visit_id ON imed.order_continue_prepared USING btree (visit_id);


--
-- Name: order_continue_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_continue_visit_id ON imed.order_continue USING btree (visit_id);


--
-- Name: order_is_urgent; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_is_urgent ON imed.order_item USING btree (is_urgent);


--
-- Name: order_item_assigned_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_item_assigned_date ON imed.order_item USING btree (assigned_date);


--
-- Name: order_item_assigned_time; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_item_assigned_time ON imed.order_item USING btree (assigned_time);


--
-- Name: order_item_base_lab_type_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_item_base_lab_type_id ON imed.order_item USING btree (base_lab_type_id);


--
-- Name: order_item_bill_grp_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_item_bill_grp_id ON imed.order_item USING btree (base_billing_group_id);


--
-- Name: order_item_dental_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_item_dental_id ON imed.order_item USING btree (dental_id);


--
-- Name: order_item_df_eid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_item_df_eid ON imed.order_item USING btree (doctor_fee_eid);


--
-- Name: order_item_df_mode; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_item_df_mode ON imed.order_item USING btree (base_df_mode_id);


--
-- Name: order_item_dispense_date_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_item_dispense_date_idx ON imed.order_item USING btree (dispense_date);


--
-- Name: order_item_dispense_spid_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_item_dispense_spid_idx ON imed.order_item USING btree (dispense_spid);


--
-- Name: order_item_dispense_time_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_item_dispense_time_idx ON imed.order_item USING btree (dispense_time);


--
-- Name: order_item_doctor_eid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_item_doctor_eid ON imed.order_item USING btree (order_doctor_eid);


--
-- Name: order_item_execute_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_item_execute_date ON imed.order_item USING btree (execute_date);


--
-- Name: order_item_execute_time; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_item_execute_time ON imed.order_item USING btree (execute_time);


--
-- Name: order_item_fix_item_type_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_item_fix_item_type_id ON imed.order_item USING btree (fix_item_type_id);


--
-- Name: order_item_fix_item_type_id_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_item_fix_item_type_id_idx ON imed.order_item USING btree (fix_item_type_id);


--
-- Name: order_item_fix_order_stat; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_item_fix_order_stat ON imed.order_item USING btree (fix_order_status_id);


--
-- Name: order_item_fix_set_type; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_item_fix_set_type ON imed.order_item USING btree (fix_set_type_id);


--
-- Name: order_item_ignore_report_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_item_ignore_report_date ON imed.order_item USING btree (ignore_report_date);


--
-- Name: order_item_ignore_report_eid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_item_ignore_report_eid ON imed.order_item USING btree (ignore_report_eid);


--
-- Name: order_item_ignore_report_reason; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_item_ignore_report_reason ON imed.order_item USING btree (base_lab_ignore_report_reason_id);


--
-- Name: order_item_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_item_item_id ON imed.order_item USING btree (item_id);


--
-- Name: order_item_mix_drug_no; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_item_mix_drug_no ON imed.order_item USING btree (mix_drug_no);


--
-- Name: order_item_mix_drug_type; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_item_mix_drug_type ON imed.order_item USING btree (fix_mix_drug_type_id);


--
-- Name: order_item_op_regist; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_item_op_regist ON imed.order_item USING btree (op_registered_id);


--
-- Name: order_item_order_doctor_eid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_item_order_doctor_eid ON imed.order_item USING btree (order_doctor_eid);


--
-- Name: order_item_order_spid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_item_order_spid ON imed.order_item USING btree (order_spid);


--
-- Name: order_item_pat_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_item_pat_id ON imed.order_item USING btree (patient_id);


--
-- Name: order_item_payment_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_item_payment_id ON imed.order_item USING btree (visit_payment_id);


--
-- Name: order_item_set_order_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_item_set_order_id ON imed.order_item USING btree (set_order_id);


--
-- Name: order_item_spid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_item_spid ON imed.order_item USING btree (order_spid);


--
-- Name: order_item_verify_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_item_verify_date ON imed.order_item USING btree (verify_date);


--
-- Name: order_item_verify_time; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_item_verify_time ON imed.order_item USING btree (verify_time);


--
-- Name: order_item_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_item_visit_id ON imed.order_item USING btree (visit_id);


--
-- Name: order_item_visit_payment; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_item_visit_payment ON imed.order_item USING btree (visit_payment_id);


--
-- Name: order_item_xray_type; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_item_xray_type ON imed.order_item USING btree (base_xray_type_id);


--
-- Name: order_lab_result_special; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_lab_result_special ON imed.order_item USING btree (fix_lab_result_special_id);


--
-- Name: order_set_multi_visit_detail_order_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_set_multi_visit_detail_order_item_id ON imed.order_set_multi_visit_detail USING btree (order_item_id);


--
-- Name: order_set_multi_visit_detail_order_set_multi_visit_detail_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_set_multi_visit_detail_order_set_multi_visit_detail_id ON imed.order_set_multi_visit_detail USING btree (order_set_multi_visit_detail_id);


--
-- Name: order_set_multi_visit_detail_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_set_multi_visit_detail_visit_id ON imed.order_set_multi_visit_detail USING btree (visit_id);


--
-- Name: order_set_multi_visit_is_complete; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_set_multi_visit_is_complete ON imed.order_set_multi_visit USING btree (is_complete);


--
-- Name: order_set_multi_visit_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_set_multi_visit_item_id ON imed.order_set_multi_visit USING btree (item_id);


--
-- Name: order_set_multi_visit_patient_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX order_set_multi_visit_patient_id ON imed.order_set_multi_visit USING btree (patient_id);


--
-- Name: orderset_mv_child_mvvisit_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX orderset_mv_child_mvvisit_idx ON imed.order_set_multi_visit_child USING btree (order_set_multi_visit_id);


--
-- Name: out_doctor_firstname; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX out_doctor_firstname ON imed.base_out_doctor USING btree (firstname);


--
-- Name: patho_assign_lab_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patho_assign_lab_idx ON imed.patho_assign_lab USING btree (assign_lab_id);


--
-- Name: patho_autopsy_presult_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patho_autopsy_presult_idx ON imed.patho_autopsy_result USING btree (patho_result_id);


--
-- Name: patho_contra_assign_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patho_contra_assign_idx ON imed.patho_assign_pap_contraceptive USING btree (patho_assign_lab_id);


--
-- Name: patho_pap_res_abnormal_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patho_pap_res_abnormal_idx ON imed.patho_pap_result_abnormal USING btree (patho_pap_smear_result_id);


--
-- Name: patho_pap_res_neg_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patho_pap_res_neg_idx ON imed.patho_pap_result_negative USING btree (patho_pap_smear_result_id);


--
-- Name: patho_pap_res_other_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patho_pap_res_other_idx ON imed.patho_pap_result_others USING btree (patho_pap_smear_result_id);


--
-- Name: patho_pap_result_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patho_pap_result_idx ON imed.patho_pap_smear_result USING btree (patho_result_id);


--
-- Name: patho_result_ln_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patho_result_ln_idx ON imed.patho_result USING btree (ln);


--
-- Name: patho_result_visit_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patho_result_visit_idx ON imed.patho_result USING btree (visit_id);


--
-- Name: patho_snomed_patho_result_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patho_snomed_patho_result_id ON imed.patho_snomed USING btree (patho_result_id);


--
-- Name: patho_specimen_order_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patho_specimen_order_idx ON imed.patho_specimen USING btree (order_item_id);


--
-- Name: patient_address_patient_i; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_address_patient_i ON imed.patient_address USING btree (patient_id);


--
-- Name: patient_any_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_any_id ON imed.patient USING btree (any_id);


--
-- Name: patient_base_patient_group_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_base_patient_group_id ON imed.patient USING btree (base_patient_group_id);


--
-- Name: patient_base_patient_unit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_base_patient_unit_id ON imed.patient USING btree (base_patient_unit_id);


--
-- Name: patient_contact_patient_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_contact_patient_id ON imed.patient_contact USING btree (patient_id);


--
-- Name: patient_file_borrow_base_med_department_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_file_borrow_base_med_department_id ON imed.patient_file_borrow USING btree (base_med_department_id);


--
-- Name: patient_file_borrow_borrow_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_file_borrow_borrow_date ON imed.patient_file_borrow USING btree (borrow_date);


--
-- Name: patient_file_borrow_borrow_eid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_file_borrow_borrow_eid ON imed.patient_file_borrow USING btree (borrow_eid);


--
-- Name: patient_file_borrow_borrow_file_type; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_file_borrow_borrow_file_type ON imed.patient_file_borrow USING btree (fix_borrow_file_type_id);


--
-- Name: patient_file_borrow_borrow_spid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_file_borrow_borrow_spid ON imed.patient_file_borrow USING btree (borrow_spid);


--
-- Name: patient_file_borrow_file_type; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_file_borrow_file_type ON imed.patient_file_borrow USING btree (file_type);


--
-- Name: patient_file_borrow_hn; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_file_borrow_hn ON imed.patient_file_borrow USING btree (hn);


--
-- Name: patient_file_borrow_pat_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_file_borrow_pat_id ON imed.patient_file_borrow USING btree (patient_id);


--
-- Name: patient_film_borrow_borrow_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_film_borrow_borrow_date ON imed.patient_film_borrow USING btree (borrow_date);


--
-- Name: patient_film_borrow_borrow_eid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_film_borrow_borrow_eid ON imed.patient_film_borrow USING btree (borrow_eid);


--
-- Name: patient_film_borrow_borrow_spid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_film_borrow_borrow_spid ON imed.patient_film_borrow USING btree (borrow_spid);


--
-- Name: patient_film_borrow_film_status; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_film_borrow_film_status ON imed.patient_film_borrow USING btree (fix_film_status_id);


--
-- Name: patient_film_borrow_patient_film_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_film_borrow_patient_film_id ON imed.patient_film_borrow USING btree (patient_film_id);


--
-- Name: patient_film_film_number; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_film_film_number ON imed.patient_film USING btree (film_number);


--
-- Name: patient_film_film_status; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_film_film_status ON imed.patient_film USING btree (fix_film_status_id);


--
-- Name: patient_film_film_type; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_film_film_type ON imed.patient_film USING btree (fix_film_type_id);


--
-- Name: patient_film_hn; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_film_hn ON imed.patient_film USING btree (hn);


--
-- Name: patient_film_patient_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_film_patient_id ON imed.patient_film USING btree (patient_id);


--
-- Name: patient_film_person_type; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_film_person_type ON imed.patient_film_borrow USING btree (fix_person_type_id);


--
-- Name: patient_firstname; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_firstname ON imed.patient USING btree (firstname);


--
-- Name: patient_fix_amphur_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_fix_amphur_id ON imed.patient USING btree (fix_amphur_id);


--
-- Name: patient_fix_changwat_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_fix_changwat_id ON imed.patient USING btree (fix_changwat_id);


--
-- Name: patient_fix_occupation_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_fix_occupation_id ON imed.patient USING btree (fix_occupation_id);


--
-- Name: patient_hn; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_hn ON imed.patient USING btree (hn);


--
-- Name: patient_id_current_drug_use_key; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_id_current_drug_use_key ON imed.current_drug_use USING btree (patient_id);


--
-- Name: patient_id_family_history_key; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_id_family_history_key ON imed.family_history USING btree (patient_id);


--
-- Name: patient_id_medical_history_key; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_id_medical_history_key ON imed.medical_history USING btree (patient_id);


--
-- Name: patient_id_personal_illness_key; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_id_personal_illness_key ON imed.personal_illness USING btree (patient_id);


--
-- Name: patient_id_previous_drug_used_key; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_id_previous_drug_used_key ON imed.previous_drug_used USING btree (patient_id);


--
-- Name: patient_id_previous_test_key; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_id_previous_test_key ON imed.previous_test USING btree (patient_id);


--
-- Name: patient_id_risk_factor_key; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_id_risk_factor_key ON imed.risk_factor USING btree (patient_id);


--
-- Name: patient_lastname; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_lastname ON imed.patient USING btree (lastname);


--
-- Name: patient_lsd; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_lsd ON imed.patient USING btree (lsd);


--
-- Name: patient_media_perspective_base_patient_media_perspective_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_media_perspective_base_patient_media_perspective_id ON imed.patient_media_perspective USING btree (base_patient_media_perspective_id);


--
-- Name: patient_media_perspective_patient_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_media_perspective_patient_id ON imed.patient_media_perspective USING btree (patient_id);


--
-- Name: patient_name_history_patient_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_name_history_patient_id ON imed.patient_name_history USING btree (patient_id);


--
-- Name: patient_nickname; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_nickname ON imed.patient USING btree (nickname);


--
-- Name: patient_nsd; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_nsd ON imed.patient USING btree (nsd);


--
-- Name: patient_number_idno_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_number_idno_idx ON imed.patient_number USING btree (identity_number);


--
-- Name: patient_number_patient_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_number_patient_idx ON imed.patient_number USING btree (patient_id);


--
-- Name: patient_number_type_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_number_type_idx ON imed.patient_number USING btree (base_patient_number_type);


--
-- Name: patient_other_allergy_pid_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_other_allergy_pid_idx ON imed.patient_other_allergy USING btree (patient_id);


--
-- Name: patient_payment_patient_i; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_payment_patient_i ON imed.patient_payment USING btree (patient_id);


--
-- Name: patient_payment_plan_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_payment_plan_id ON imed.patient_payment USING btree (plan_id);


--
-- Name: patient_pid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_pid ON imed.patient USING btree (pid);


--
-- Name: patient_relation_number_pat_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_relation_number_pat_idx ON imed.patient_relation_number USING btree (patient_id);


--
-- Name: patient_un; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_un ON imed.patient USING btree (un);


--
-- Name: patient_xn; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX patient_xn ON imed.patient USING btree (xn);


--
-- Name: physical_examination_position; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX physical_examination_position ON imed.physical_examination USING btree (result_position);


--
-- Name: physical_examination_vs_extend_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX physical_examination_vs_extend_id ON imed.physical_examination USING btree (vital_sign_extend_id);


--
-- Name: plan_0110_5; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX plan_0110_5 ON imed.plan USING btree (fix_0110_5_id);


--
-- Name: plan_auto_order_begin_time; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX plan_auto_order_begin_time ON imed.plan_auto_order USING btree (begin_order_time);


--
-- Name: plan_auto_order_finish_time; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX plan_auto_order_finish_time ON imed.plan_auto_order USING btree (finish_order_time);


--
-- Name: plan_auto_order_order_day; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX plan_auto_order_order_day ON imed.plan_auto_order USING btree (order_day);


--
-- Name: plan_auto_order_plan_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX plan_auto_order_plan_id ON imed.plan_auto_order USING btree (plan_id);


--
-- Name: plan_billing_except_billing_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX plan_billing_except_billing_idx ON imed.plan_billing_share_except USING btree (base_billing_group_id);


--
-- Name: plan_billing_except_plan_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX plan_billing_except_plan_idx ON imed.plan_billing_share_except USING btree (plan_id);


--
-- Name: plan_document_plan_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX plan_document_plan_id ON imed.plan_document USING btree (plan_id);


--
-- Name: plan_dx_except_plan_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX plan_dx_except_plan_id ON imed.plan_dx_except USING btree (plan_id);


--
-- Name: plan_ext_pat_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX plan_ext_pat_id ON imed.plan_external USING btree (hn);


--
-- Name: plan_ext_plan_code; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX plan_ext_plan_code ON imed.plan_external USING btree (plan_code);


--
-- Name: plan_external_active; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX plan_external_active ON imed.plan_external USING btree (active);


--
-- Name: plan_external_employee_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX plan_external_employee_id ON imed.plan_external USING btree (plan_external_id);


--
-- Name: plan_external_fsoundex; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX plan_external_fsoundex ON imed.plan_external USING btree (nsd);


--
-- Name: plan_external_lsoundex; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX plan_external_lsoundex ON imed.plan_external USING btree (lsd);


--
-- Name: plan_external_pid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX plan_external_pid ON imed.plan_external USING btree (pid);


--
-- Name: plan_external_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX plan_external_visit_id ON imed.plan_external_visit USING btree (visit_id);


--
-- Name: plan_fix_contract_type; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX plan_fix_contract_type ON imed.plan USING btree (fix_contract_type_id);


--
-- Name: plan_fix_plan_type; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX plan_fix_plan_type ON imed.plan USING btree (fix_plan_type_id);


--
-- Name: plan_inscl; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX plan_inscl ON imed.plan USING btree (inscl);


--
-- Name: plan_payer_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX plan_payer_id ON imed.plan USING btree (payer_id);


--
-- Name: plan_payer_other_plan_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX plan_payer_other_plan_idx ON imed.plan_payer_other USING btree (plan_id);


--
-- Name: plan_plan_group; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX plan_plan_group ON imed.plan USING btree (base_plan_group_id);


--
-- Name: plan_pttype; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX plan_pttype ON imed.plan USING btree (pttype);


--
-- Name: plan_share_limit_item_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX plan_share_limit_item_idx ON imed.plan_item_share_limit USING btree (item_id);


--
-- Name: plan_share_limit_plan_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX plan_share_limit_plan_idx ON imed.plan_item_share_limit USING btree (plan_id);


--
-- Name: position_base_template_operation; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX position_base_template_operation ON imed.template_op_position USING btree (base_template_operation_id);


--
-- Name: prepack_item_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX prepack_item_item_id ON imed.prepack_item USING btree (item_id);


--
-- Name: pres_ass_doctor_eid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX pres_ass_doctor_eid ON imed.prescription USING btree (assign_doctor_eid);


--
-- Name: pres_assign_eid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX pres_assign_eid ON imed.prescription USING btree (assign_eid);


--
-- Name: pres_assign_spid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX pres_assign_spid ON imed.prescription USING btree (assign_spid);


--
-- Name: prescription_assign_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX prescription_assign_date ON imed.prescription USING btree (assign_date);


--
-- Name: prescription_assign_status; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX prescription_assign_status ON imed.prescription USING btree (assign_order_status);


--
-- Name: prescription_patient_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX prescription_patient_id ON imed.prescription USING btree (patient_id);


--
-- Name: prescription_pn; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX prescription_pn ON imed.prescription USING btree (pn);


--
-- Name: prescription_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX prescription_visit_id ON imed.prescription USING btree (visit_id);


--
-- Name: print_out_item_set_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX print_out_item_set_id ON imed.item_set_print_out USING btree (item_set_id);


--
-- Name: progress_note_admit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX progress_note_admit_id ON imed.progress_note USING btree (admit_id);


--
-- Name: pt_contact_firstname_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX pt_contact_firstname_idx ON imed.patient_contact USING btree (firstname);


--
-- Name: pt_contact_lastname_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX pt_contact_lastname_idx ON imed.patient_contact USING btree (lastname);


--
-- Name: pv_patient_id_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX pv_patient_id_idx ON imed.payment_voucher USING btree (patient_id);


--
-- Name: pv_visit_id_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX pv_visit_id_idx ON imed.payment_voucher USING btree (visit_id);


--
-- Name: queue_mgnt_base_service_point_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX queue_mgnt_base_service_point_id ON imed.queue_mgnt USING btree (base_service_point_id);


--
-- Name: queue_mgnt_employee_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX queue_mgnt_employee_id ON imed.queue_mgnt USING btree (employee_id);


--
-- Name: rcpt_atch_grp_rcpt_atch_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX rcpt_atch_grp_rcpt_atch_id ON imed.receipt_attach_bill_grp USING btree (receipt_attach_id);


--
-- Name: rcpt_bill_grp_grpid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX rcpt_bill_grp_grpid ON imed.receipt_billing_group USING btree (base_billing_group_id);


--
-- Name: rcpt_bill_grp_pat; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX rcpt_bill_grp_pat ON imed.receipt_billing_group USING btree (patient_id);


--
-- Name: rcpt_bill_grp_rcptid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX rcpt_bill_grp_rcptid ON imed.receipt_billing_group USING btree (receipt_id);


--
-- Name: rcpt_bill_grp_visit; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX rcpt_bill_grp_visit ON imed.receipt_billing_group USING btree (visit_id);


--
-- Name: rcpt_category_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX rcpt_category_id ON imed.receipt USING btree (base_receipt_category_id);


--
-- Name: rcpt_com_number; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX rcpt_com_number ON imed.receipt USING btree (com_number);


--
-- Name: rcpt_debt_change; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX rcpt_debt_change ON imed.receipt USING btree (debt_change);


--
-- Name: rcpt_invoice_receipt_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX rcpt_invoice_receipt_id ON imed.receipt_pay_invoice USING btree (invoice_receipt_id);


--
-- Name: rcpt_invoice_status; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX rcpt_invoice_status ON imed.receipt USING btree (invoice_status);


--
-- Name: rcpt_paid_mtd_rcpt_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX rcpt_paid_mtd_rcpt_id ON imed.receipt_paid_method USING btree (receipt_id);


--
-- Name: rcpt_patient_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX rcpt_patient_id ON imed.receipt USING btree (patient_id);


--
-- Name: rcpt_payment_type_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX rcpt_payment_type_id ON imed.receipt USING btree (fix_payment_type_id);


--
-- Name: rcpt_plan_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX rcpt_plan_id ON imed.receipt USING btree (plan_id);


--
-- Name: rcpt_prn_grp_rcptid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX rcpt_prn_grp_rcptid ON imed.receipt_print_group USING btree (receipt_id);


--
-- Name: rcpt_receipt_number; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX rcpt_receipt_number ON imed.receipt USING btree (receipt_number);


--
-- Name: rcpt_receipt_status_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX rcpt_receipt_status_id ON imed.receipt USING btree (fix_receipt_status_id);


--
-- Name: rcpt_receipt_type_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX rcpt_receipt_type_id ON imed.receipt USING btree (fix_receipt_type_id);


--
-- Name: rcpt_receive_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX rcpt_receive_date ON imed.receipt USING btree (receive_date);


--
-- Name: rcpt_receive_time; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX rcpt_receive_time ON imed.receipt USING btree (receive_time);


--
-- Name: rcpt_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX rcpt_visit_id ON imed.receipt USING btree (visit_id);


--
-- Name: receipt_attach_rcpt_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX receipt_attach_rcpt_id ON imed.receipt_attach USING btree (receipt_id);


--
-- Name: receipt_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX receipt_item_id ON imed.receipt_order USING btree (item_id);


--
-- Name: receipt_order_billing_grp_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX receipt_order_billing_grp_idx ON imed.receipt_order USING btree (receipt_billing_group_id);


--
-- Name: receipt_order_receipt_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX receipt_order_receipt_idx ON imed.receipt_order USING btree (receipt_id);


--
-- Name: receipt_print_history_receipt_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX receipt_print_history_receipt_id ON imed.receipt_print_history USING btree (receipt_id);


--
-- Name: refer_reason_refer_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX refer_reason_refer_id ON imed.refer_out_reason USING btree (refer_out_id);


--
-- Name: report_drug_pregnancy_order_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX report_drug_pregnancy_order_item_id ON imed.report_drug_pregnancy USING btree (order_item_id);


--
-- Name: return_drug_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX return_drug_item_id ON imed.return_drug USING btree (item_id);


--
-- Name: return_drug_order_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX return_drug_order_id ON imed.return_drug USING btree (order_item_id);


--
-- Name: return_drug_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX return_drug_visit_id ON imed.return_drug USING btree (visit_id);


--
-- Name: revenue_order_order_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX revenue_order_order_idx ON imed.receipt_revenue_order USING btree (order_item_id);


--
-- Name: revenue_order_receipt_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX revenue_order_receipt_idx ON imed.receipt_revenue_order USING btree (receipt_id);


--
-- Name: rtd_dispense_unit_price; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX rtd_dispense_unit_price ON imed.return_drug USING btree (dispense_unit_price);


--
-- Name: rtd_dispense_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX rtd_dispense_visit_id ON imed.return_drug USING btree (dispense_visit_id);


--
-- Name: sc_bill_grp_deid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX sc_bill_grp_deid ON imed.sum_cost_billing_group USING btree (sum_cost_id);


--
-- Name: sc_bill_grp_grpid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX sc_bill_grp_grpid ON imed.sum_cost_billing_group USING btree (base_billing_group_id);


--
-- Name: sc_bill_grp_pat; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX sc_bill_grp_pat ON imed.sum_cost_billing_group USING btree (patient_id);


--
-- Name: sc_bill_grp_visit; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX sc_bill_grp_visit ON imed.sum_cost_billing_group USING btree (visit_id);


--
-- Name: sc_patient_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX sc_patient_id ON imed.sum_cost USING btree (patient_id);


--
-- Name: sc_receipt_number; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX sc_receipt_number ON imed.sum_cost USING btree (sum_cost_number);


--
-- Name: sc_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX sc_visit_id ON imed.sum_cost USING btree (visit_id);


--
-- Name: snc_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX snc_date ON imed.special_nurse_care USING btree (special_nurse_care_date);


--
-- Name: snc_nurse_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX snc_nurse_id ON imed.special_nurse_care USING btree (nurse_id);


--
-- Name: snc_sp_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX snc_sp_id ON imed.special_nurse_care USING btree (base_service_point_id);


--
-- Name: snc_time; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX snc_time ON imed.special_nurse_care USING btree (special_nurse_care_time);


--
-- Name: snc_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX snc_visit_id ON imed.special_nurse_care USING btree (visit_id);


--
-- Name: sncd_bsnc_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX sncd_bsnc_id ON imed.special_nurse_care_detail USING btree (base_special_nurse_care_id);


--
-- Name: sncd_fsncd_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX sncd_fsncd_id ON imed.special_nurse_care_detail USING btree (fix_special_nurse_care_detail);


--
-- Name: sncd_snc_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX sncd_snc_id ON imed.special_nurse_care_detail USING btree (special_nurse_care_id);


--
-- Name: sp_patient_no_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX sp_patient_no_idx ON imed.base_service_point_patient_no USING btree (base_service_point_id);


--
-- Name: special_card_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX special_card_visit_id ON imed.visit_special_card USING btree (visit_id);


--
-- Name: stk_card_dispense_number; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_card_dispense_number ON imed.stock_card USING btree (stock_dispense_number);


--
-- Name: stk_card_in_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_card_in_id ON imed.stock_card USING btree (in_id);


--
-- Name: stk_card_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_card_item_id ON imed.stock_card USING btree (item_id);


--
-- Name: stk_card_lot_number; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_card_lot_number ON imed.stock_card USING btree (lot_number);


--
-- Name: stk_card_method_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_card_method_id ON imed.stock_card USING btree (fix_stock_method_id);


--
-- Name: stk_card_mgnt_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_card_mgnt_id ON imed.stock_card USING btree (stock_mgnt_id);


--
-- Name: stk_card_order_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_card_order_item_id ON imed.stock_card USING btree (order_item_id);


--
-- Name: stk_card_out_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_card_out_id ON imed.stock_card USING btree (out_id);


--
-- Name: stk_card_receive_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_card_receive_date ON imed.stock_card USING btree (receive_date);


--
-- Name: stk_card_receive_time; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_card_receive_time ON imed.stock_card USING btree (receive_time);


--
-- Name: stk_card_req_or_order_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_card_req_or_order_id ON imed.stock_card USING btree (request_or_order_id);


--
-- Name: stk_card_stock_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_card_stock_id ON imed.stock_card USING btree (stock_id);


--
-- Name: stk_card_update_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_card_update_date ON imed.stock_card USING btree (update_date);


--
-- Name: stk_card_update_date_time; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_card_update_date_time ON imed.stock_card USING btree (update_date_time);


--
-- Name: stk_card_update_time; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_card_update_time ON imed.stock_card USING btree (update_time);


--
-- Name: stk_death_base_death_status_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_death_base_death_status_id ON imed.base_death_status USING btree (base_death_status_id);


--
-- Name: stk_death_patient_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_death_patient_id ON imed.patient USING btree (patient_id);


--
-- Name: stk_death_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_death_visit_id ON imed.visit USING btree (visit_id);


--
-- Name: stk_distributor_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_distributor_id ON imed.stock_item_trade_name USING btree (distributor_id);


--
-- Name: stk_drug_format_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_drug_format_id ON imed.stock_mgnt USING btree (base_drug_format_id);


--
-- Name: stk_ex_de_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_ex_de_item_id ON imed.stock_exchange_detail USING btree (item_id);


--
-- Name: stk_ex_de_stock_exchange_detail_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_ex_de_stock_exchange_detail_id ON imed.stock_exchange_detail USING btree (stock_exchange_detail_id);


--
-- Name: stk_ex_de_stock_exchange_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_ex_de_stock_exchange_id ON imed.stock_exchange_detail USING btree (stock_exchange_id);


--
-- Name: stk_ex_exchange_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_ex_exchange_date ON imed.stock_exchange USING btree (exchange_date);


--
-- Name: stk_ex_exchange_eid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_ex_exchange_eid ON imed.stock_exchange USING btree (exchange_eid);


--
-- Name: stk_ex_exchange_from_stock_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_ex_exchange_from_stock_id ON imed.stock_exchange USING btree (exchange_from_stock_id);


--
-- Name: stk_ex_exchange_number; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_ex_exchange_number ON imed.stock_exchange USING btree (exchange_number);


--
-- Name: stk_ex_exchange_time; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_ex_exchange_time ON imed.stock_exchange USING btree (exchange_time);


--
-- Name: stk_ex_exchange_to_distributor_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_ex_exchange_to_distributor_id ON imed.stock_exchange USING btree (exchange_to_distributor_id);


--
-- Name: stk_ex_fix_stock_exchange_status_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_ex_fix_stock_exchange_status_id ON imed.stock_exchange USING btree (fix_stock_exchange_status_id);


--
-- Name: stk_ex_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_ex_item_id ON imed.stock_exchange USING btree (item_id);


--
-- Name: stk_ex_lot_number; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_ex_lot_number ON imed.stock_exchange USING btree (lot_number);


--
-- Name: stk_ex_stock_exchange_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_ex_stock_exchange_id ON imed.stock_exchange USING btree (stock_exchange_id);


--
-- Name: stk_free_item_stock_order_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_free_item_stock_order_id ON imed.stock_free_item USING btree (stock_order_id);


--
-- Name: stk_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_item_id ON imed.stock_item_trade_name USING btree (item_id);


--
-- Name: stk_mgnt_dist_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_mgnt_dist_id ON imed.stock_mgnt USING btree (distributor_id);


--
-- Name: stk_mgnt_expire_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_mgnt_expire_date ON imed.stock_mgnt USING btree (expire_date);


--
-- Name: stk_mgnt_fix_abc_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_mgnt_fix_abc_id ON imed.stock_mgnt USING btree (fix_abc_id);


--
-- Name: stk_mgnt_fix_ven_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_mgnt_fix_ven_id ON imed.stock_mgnt USING btree (fix_ven_id);


--
-- Name: stk_mgnt_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_mgnt_item_id ON imed.stock_mgnt USING btree (item_id);


--
-- Name: stk_mgnt_lot_number; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_mgnt_lot_number ON imed.stock_mgnt USING btree (lot_number);


--
-- Name: stk_mgnt_manufact_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_mgnt_manufact_id ON imed.stock_mgnt USING btree (manufacturer_id);


--
-- Name: stk_mgnt_period_dist_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_mgnt_period_dist_id ON imed.stock_mgnt_period USING btree (distributor_id);


--
-- Name: stk_mgnt_period_drug_format_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_mgnt_period_drug_format_id ON imed.stock_mgnt_period USING btree (base_drug_format_id);


--
-- Name: stk_mgnt_period_expire_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_mgnt_period_expire_date ON imed.stock_mgnt_period USING btree (expire_date);


--
-- Name: stk_mgnt_period_fix_abc_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_mgnt_period_fix_abc_id ON imed.stock_mgnt_period USING btree (fix_abc_id);


--
-- Name: stk_mgnt_period_fix_ven_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_mgnt_period_fix_ven_id ON imed.stock_mgnt_period USING btree (fix_ven_id);


--
-- Name: stk_mgnt_period_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_mgnt_period_item_id ON imed.stock_mgnt_period USING btree (item_id);


--
-- Name: stk_mgnt_period_lot_number; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_mgnt_period_lot_number ON imed.stock_mgnt_period USING btree (lot_number);


--
-- Name: stk_mgnt_period_manufact_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_mgnt_period_manufact_id ON imed.stock_mgnt_period USING btree (manufacturer_id);


--
-- Name: stk_mgnt_period_produce_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_mgnt_period_produce_date ON imed.stock_mgnt_period USING btree (produce_date);


--
-- Name: stk_mgnt_period_receive_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_mgnt_period_receive_date ON imed.stock_mgnt_period USING btree (receive_date);


--
-- Name: stk_mgnt_period_stock_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_mgnt_period_stock_id ON imed.stock_mgnt_period USING btree (stock_id);


--
-- Name: stk_mgnt_produce_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_mgnt_produce_date ON imed.stock_mgnt USING btree (produce_date);


--
-- Name: stk_mgnt_receive_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_mgnt_receive_date ON imed.stock_mgnt USING btree (receive_date);


--
-- Name: stk_mgnt_stock_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_mgnt_stock_id ON imed.stock_mgnt USING btree (stock_id);


--
-- Name: stk_order_purchasing_offer_date_time; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_order_purchasing_offer_date_time ON imed.stock_order USING btree (purchasing_offer_date_time);


--
-- Name: stk_prequest_number; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_prequest_number ON imed.stock_order USING btree (purchasing_request_number);


--
-- Name: stk_receive_order_number; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_receive_order_number ON imed.stock_card USING btree (receive_order_number);


--
-- Name: stk_req_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_req_date ON imed.stock_request USING btree (req_date);


--
-- Name: stk_req_eid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_req_eid ON imed.stock_request USING btree (req_eid);


--
-- Name: stk_req_from_stock_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_req_from_stock_id ON imed.stock_request USING btree (disp_stock_id);


--
-- Name: stk_req_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_req_item_id ON imed.stock_request USING btree (item_id);


--
-- Name: stk_req_number; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_req_number ON imed.stock_request USING btree (req_number);


--
-- Name: stk_req_order_date_time; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_req_order_date_time ON imed.stock_request_order USING btree (req_order_date_time);


--
-- Name: stk_req_order_distributor_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_req_order_distributor_id ON imed.stock_request_order USING btree (distributor_id);


--
-- Name: stk_req_order_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_req_order_item_id ON imed.stock_request_order USING btree (item_id);


--
-- Name: stk_req_order_item_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_req_order_item_idx ON imed.stock_request USING btree (order_item_id);


--
-- Name: stk_req_order_manufacturer_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_req_order_manufacturer_id ON imed.stock_request_order USING btree (manufacturer_id);


--
-- Name: stk_req_status; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_req_status ON imed.stock_request USING btree (fix_stock_request_status_id);


--
-- Name: stk_req_time; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_req_time ON imed.stock_request USING btree (req_time);


--
-- Name: stk_req_to_stock_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_req_to_stock_id ON imed.stock_request USING btree (recv_stock_id);


--
-- Name: stk_request_req_date_time; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_request_req_date_time ON imed.stock_request USING btree (req_date_time);


--
-- Name: stk_ret_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_ret_date ON imed.stock_return USING btree (return_date);


--
-- Name: stk_ret_eid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_ret_eid ON imed.stock_return USING btree (return_eid);


--
-- Name: stk_ret_from_stock_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_ret_from_stock_id ON imed.stock_return USING btree (disp_stock_id);


--
-- Name: stk_ret_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_ret_item_id ON imed.stock_return USING btree (item_id);


--
-- Name: stk_ret_number; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_ret_number ON imed.stock_return USING btree (return_number);


--
-- Name: stk_ret_status; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_ret_status ON imed.stock_return USING btree (fix_stock_return_status_id);


--
-- Name: stk_ret_time; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_ret_time ON imed.stock_return USING btree (return_time);


--
-- Name: stk_ret_to_stock_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_ret_to_stock_id ON imed.stock_return USING btree (recv_stock_id);


--
-- Name: stk_setup_order_active; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_setup_order_active ON imed.stock_setup_order USING btree (active);


--
-- Name: stk_setup_order_active_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_setup_order_active_date ON imed.stock_setup_order_detail USING btree (active_date);


--
-- Name: stk_setup_order_big_unit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_setup_order_big_unit_id ON imed.stock_setup_order_detail USING btree (big_unit_id);


--
-- Name: stk_setup_order_detail_setup_order_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_setup_order_detail_setup_order_id ON imed.stock_setup_order_detail USING btree (stock_setup_order_id);


--
-- Name: stk_setup_order_distributor_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_setup_order_distributor_id ON imed.stock_setup_order USING btree (distributor_id);


--
-- Name: stk_setup_order_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_setup_order_item_id ON imed.stock_setup_order USING btree (item_id);


--
-- Name: stk_setup_order_manufacturer_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_setup_order_manufacturer_id ON imed.stock_setup_order USING btree (manufacturer_id);


--
-- Name: stk_setup_order_modify_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_setup_order_modify_date ON imed.stock_setup_order_detail USING btree (modify_date);


--
-- Name: stk_setup_order_modify_eid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_setup_order_modify_eid ON imed.stock_setup_order_detail USING btree (modify_eid);


--
-- Name: stk_setup_order_modify_time; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_setup_order_modify_time ON imed.stock_setup_order_detail USING btree (modify_time);


--
-- Name: stk_setup_order_small_unit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_setup_order_small_unit_id ON imed.stock_setup_order_detail USING btree (small_unit_id);


--
-- Name: stk_stock_order_tmp_fix_stock_method_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_stock_order_tmp_fix_stock_method_id ON imed.stock_order_tmp USING btree (fix_stock_method_id);


--
-- Name: stk_stock_order_tmp_is_received; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_stock_order_tmp_is_received ON imed.stock_order_tmp USING btree (is_received);


--
-- Name: stk_stock_order_tmp_order_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stk_stock_order_tmp_order_id ON imed.stock_order_tmp USING btree (stock_order_id);


--
-- Name: stock_auth_employee_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stock_auth_employee_id ON imed.stock_auth USING btree (employee_id);


--
-- Name: stock_auth_stock_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stock_auth_stock_id ON imed.stock_auth USING btree (stock_id);


--
-- Name: stock_card_stock_request_produce_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stock_card_stock_request_produce_id ON imed.stock_card USING btree (stock_request_produce_id);


--
-- Name: stock_cssd_disp_stock_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stock_cssd_disp_stock_idx ON imed.stock_cssd USING btree (disp_stock_id);


--
-- Name: stock_cssd_fix_stock_cssd_status_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stock_cssd_fix_stock_cssd_status_idx ON imed.stock_cssd USING btree (fix_stock_cssd_status_id);


--
-- Name: stock_cssd_fix_stock_method_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stock_cssd_fix_stock_method_idx ON imed.stock_cssd USING btree (fix_stock_method_id);


--
-- Name: stock_cssd_item_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stock_cssd_item_idx ON imed.stock_cssd USING btree (item_id);


--
-- Name: stock_cssd_produce_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stock_cssd_produce_item_id ON imed.stock_cssd_produce USING btree (item_id);


--
-- Name: stock_cssd_produce_produce_number; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stock_cssd_produce_produce_number ON imed.stock_cssd_produce USING btree (stock_cssd_produce_number);


--
-- Name: stock_cssd_produce_stock_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stock_cssd_produce_stock_id ON imed.stock_cssd_produce USING btree (stock_id);


--
-- Name: stock_cssd_recv_stock_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stock_cssd_recv_stock_idx ON imed.stock_cssd USING btree (recv_stock_id);


--
-- Name: stock_cssd_sub_item_stock_cssd_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stock_cssd_sub_item_stock_cssd_idx ON imed.stock_cssd_sub_item USING btree (stock_cssd_id);


--
-- Name: stock_dispense_dispense_number_id_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stock_dispense_dispense_number_id_idx ON imed.stock_dispense USING btree (dispense_number);


--
-- Name: stock_dispense_expire_date_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stock_dispense_expire_date_idx ON imed.stock_dispense USING btree (expire_date);


--
-- Name: stock_dispense_item_id_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stock_dispense_item_id_idx ON imed.stock_dispense USING btree (item_id);


--
-- Name: stock_dispense_lot_number_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stock_dispense_lot_number_idx ON imed.stock_dispense USING btree (lot_number);


--
-- Name: stock_dispense_other_dispense_item_id_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stock_dispense_other_dispense_item_id_idx ON imed.stock_dispense_other_item USING btree (dispense_item_id);


--
-- Name: stock_dispense_other_item_id_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stock_dispense_other_item_id_idx ON imed.stock_dispense_other_item USING btree (item_id);


--
-- Name: stock_dispense_other_stock_id_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stock_dispense_other_stock_id_idx ON imed.stock_dispense_other_item USING btree (stock_id);


--
-- Name: stock_dispense_request_number_id_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stock_dispense_request_number_id_idx ON imed.stock_dispense USING btree (request_number);


--
-- Name: stock_dispense_stock_id_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stock_dispense_stock_id_idx ON imed.stock_dispense USING btree (stock_id);


--
-- Name: stock_dispense_stock_request_id_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stock_dispense_stock_request_id_idx ON imed.stock_dispense USING btree (stock_request_id);


--
-- Name: stock_item_set_root_item_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stock_item_set_root_item_idx ON imed.stock_item_set USING btree (root_item_id);


--
-- Name: stock_merge_po_merge_po_number; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stock_merge_po_merge_po_number ON imed.stock_merge_po USING btree (merge_po_number);


--
-- Name: stock_merge_po_stock_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stock_merge_po_stock_id ON imed.stock_merge_po USING btree (stock_id);


--
-- Name: stock_merge_po_stock_merge_po_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stock_merge_po_stock_merge_po_idx ON imed.stock_merge_po_detail USING btree (stock_merge_po_id);


--
-- Name: stock_prepack_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stock_prepack_item_id ON imed.stock_prepack_item USING btree (item_id);


--
-- Name: stock_prepack_stock_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stock_prepack_stock_id ON imed.stock_prepack_item USING btree (stock_id);


--
-- Name: stock_prepack_stock_prepack_numer; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stock_prepack_stock_prepack_numer ON imed.stock_prepack_item USING btree (stock_prepack_numer);


--
-- Name: stock_request_produce_is_cut_off_material; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stock_request_produce_is_cut_off_material ON imed.stock_request_produce USING btree (is_cut_off_material);


--
-- Name: stock_request_produce_material_material_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stock_request_produce_material_material_id ON imed.stock_request_produce_material USING btree (material_id);


--
-- Name: stock_request_produce_material_stock_request_produce_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stock_request_produce_material_stock_request_produce_id ON imed.stock_request_produce_material USING btree (stock_request_produce_id);


--
-- Name: stock_request_produce_number; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stock_request_produce_number ON imed.stock_request_produce USING btree (stock_request_produce_number);


--
-- Name: stock_template_produce_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stock_template_produce_item_id ON imed.stock_template_produce USING btree (item_id);


--
-- Name: stock_template_produce_material_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stock_template_produce_material_id ON imed.stock_template_produce USING btree (material_id);


--
-- Name: stock_used_in_stock_or_produce_department_department_id_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stock_used_in_stock_or_produce_department_department_id_idx ON imed.stock_used_in_stock_or_produce USING btree (department_id);


--
-- Name: stock_used_in_stock_or_produce_item_id_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stock_used_in_stock_or_produce_item_id_idx ON imed.stock_used_in_stock_or_produce USING btree (item_id);


--
-- Name: stock_used_in_stock_or_produce_used_in_stock_or_produce_number_; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX stock_used_in_stock_or_produce_used_in_stock_or_produce_number_ ON imed.stock_used_in_stock_or_produce USING btree (used_in_stock_or_produce_number);


--
-- Name: template_appointment_active; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX template_appointment_active ON imed.template_appointment USING btree (active);


--
-- Name: template_appointment_detail_template_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX template_appointment_detail_template_id ON imed.template_appointment_detail USING btree (template_appointment_id);


--
-- Name: template_appointment_order_detail_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX template_appointment_order_detail_id ON imed.template_appointment_order USING btree (template_appointment_detail_id);


--
-- Name: template_appointment_order_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX template_appointment_order_item_id ON imed.template_appointment_order USING btree (item_id);


--
-- Name: template_appointment_order_priority; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX template_appointment_order_priority ON imed.template_appointment_order USING btree (priority);


--
-- Name: template_diagnosis_description; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX template_diagnosis_description ON imed.base_template_diagnosis USING btree (description);


--
-- Name: template_diagnosis_icd10_code; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX template_diagnosis_icd10_code ON imed.base_template_diagnosis USING btree (icd10_code);


--
-- Name: template_diagnosis_set; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX template_diagnosis_set ON imed.base_template_diagnosis USING btree (base_diagnosis_set_id);


--
-- Name: template_item_set_employee; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX template_item_set_employee ON imed.template_item_set USING btree (employee_id);


--
-- Name: template_item_set_service_point; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX template_item_set_service_point ON imed.template_item_set USING btree (base_service_point_id);


--
-- Name: template_lab_choice_test; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX template_lab_choice_test ON imed.template_lab_choice USING btree (template_lab_test_id);


--
-- Name: template_lab_item_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX template_lab_item_item_id ON imed.template_lab_item USING btree (item_id);


--
-- Name: template_lab_result_description; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX template_lab_result_description ON imed.base_template_lab_result USING btree (description);


--
-- Name: template_lab_sub_labtest_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX template_lab_sub_labtest_idx ON imed.template_lab_sub_test USING btree (template_lab_test_id);


--
-- Name: template_ncp_group_template_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX template_ncp_group_template_id ON imed.template_ncp_group USING btree (template_ncp_group_id);


--
-- Name: template_ncp_plan_details_problem_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX template_ncp_plan_details_problem_id ON imed.template_ncp_plan_details USING btree (template_ncp_problem_id);


--
-- Name: template_ncp_plan_details_template_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX template_ncp_plan_details_template_id ON imed.template_ncp_plan_details USING btree (template_ncp_plan_details_id);


--
-- Name: template_ncp_problem_details_problem_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX template_ncp_problem_details_problem_id ON imed.template_ncp_problem_details USING btree (template_ncp_problem_id);


--
-- Name: template_ncp_problem_details_template_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX template_ncp_problem_details_template_id ON imed.template_ncp_problem_details USING btree (template_ncp_problem_details_id);


--
-- Name: template_ncp_problem_template_group_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX template_ncp_problem_template_group_id ON imed.template_ncp_problem USING btree (template_ncp_group_id);


--
-- Name: template_ncp_problem_template_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX template_ncp_problem_template_id ON imed.template_ncp_problem USING btree (template_ncp_problem_id);


--
-- Name: template_ncp_result_details_problem_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX template_ncp_result_details_problem_id ON imed.template_ncp_result_details USING btree (template_ncp_problem_id);


--
-- Name: template_ncp_result_details_template_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX template_ncp_result_details_template_id ON imed.template_ncp_result_details USING btree (template_ncp_result_details_id);


--
-- Name: template_nt_allergy_desc; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX template_nt_allergy_desc ON imed.template_nt_allergy USING btree (description);


--
-- Name: template_nt_change_order_template_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX template_nt_change_order_template_id ON imed.template_nt_change_order USING btree (template_nt_change_order_id);


--
-- Name: template_nt_default_order_base_room_type; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX template_nt_default_order_base_room_type ON imed.template_nt_default_order USING btree (base_room_type_id);


--
-- Name: template_nt_default_order_fix_meal_type; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX template_nt_default_order_fix_meal_type ON imed.template_nt_default_order USING btree (fix_nt_meal_type_id);


--
-- Name: template_operation_description; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX template_operation_description ON imed.base_template_operation USING btree (description);


--
-- Name: template_operation_icd9_code; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX template_operation_icd9_code ON imed.base_template_operation USING btree (icd9_code);


--
-- Name: template_operation_item_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX template_operation_item_id ON imed.base_template_operation USING btree (item_id);


--
-- Name: template_operation_set; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX template_operation_set ON imed.base_template_operation USING btree (base_operation_set_id);


--
-- Name: template_patho_result_template_name; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX template_patho_result_template_name ON imed.template_patho_result USING btree (upper((template_name)::text));


--
-- Name: template_phyex_clinic; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX template_phyex_clinic ON imed.base_template_phyex USING btree (base_clinic_id);


--
-- Name: template_phyex_description; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX template_phyex_description ON imed.base_template_phyex USING btree (description);


--
-- Name: template_phyex_employee_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX template_phyex_employee_id ON imed.base_template_phyex USING btree (employee_id);


--
-- Name: template_phyex_organic; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX template_phyex_organic ON imed.base_template_phyex USING btree (base_organic_id);


--
-- Name: template_pi_description; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX template_pi_description ON imed.template_personal_illness USING btree (description);


--
-- Name: template_rf_description; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX template_rf_description ON imed.template_risk_factor USING btree (description);


--
-- Name: template_xray_result_code; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX template_xray_result_code ON imed.template_xray_result USING btree (code);


--
-- Name: template_xray_result_description; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX template_xray_result_description ON imed.base_template_xray_result USING btree (description);


--
-- Name: thalas_result_assign_date_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX thalas_result_assign_date_idx ON imed.lab_thalas_result USING btree (assign_date);


--
-- Name: thalas_result_hospital_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX thalas_result_hospital_idx ON imed.lab_thalas_result USING btree (assign_hospital_id);


--
-- Name: thalas_result_is_out_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX thalas_result_is_out_idx ON imed.lab_thalas_result USING btree (is_assign_out_hospital);


--
-- Name: thalas_result_type_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX thalas_result_type_idx ON imed.lab_thalas_result USING btree (fix_lab_thalas_type_id);


--
-- Name: tp_default_clinic_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX tp_default_clinic_id ON imed.template_phyex_default USING btree (base_clinic_id);


--
-- Name: tp_default_employee_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX tp_default_employee_id ON imed.template_phyex_default USING btree (employee_id);


--
-- Name: ts_default_clinic_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX ts_default_clinic_id ON imed.template_symptom_default USING btree (base_clinic_id);


--
-- Name: ts_default_group_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX ts_default_group_id ON imed.template_symptom_default USING btree (template_symptom_group_id);


--
-- Name: ts_receive_file_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX ts_receive_file_date ON imed.time_stamp_receive_file USING btree (receive_date);


--
-- Name: ts_receive_file_sp_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX ts_receive_file_sp_id ON imed.time_stamp_receive_file USING btree (receive_spid);


--
-- Name: ts_symptom_desc; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX ts_symptom_desc ON imed.template_symptom USING btree (description);


--
-- Name: ts_symptom_group_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX ts_symptom_group_id ON imed.template_symptom USING btree (template_symptom_group_id);


--
-- Name: user_audit_fix_audit_type_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX user_audit_fix_audit_type_idx ON imed.user_audit USING btree (fix_audit_type_id);


--
-- Name: user_audit_referent_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX user_audit_referent_idx ON imed.user_audit USING btree (referent_id);


--
-- Name: visit_an; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_an ON imed.visit USING btree (an);


--
-- Name: visit_assign_lab; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_assign_lab ON imed.visit USING btree (assign_lab_status);


--
-- Name: visit_assign_xray; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_assign_xray ON imed.visit USING btree (assign_xray_status);


--
-- Name: visit_base_patient_group_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_base_patient_group_id ON imed.visit USING btree (base_patient_group_id);


--
-- Name: visit_base_patient_unit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_base_patient_unit_id ON imed.visit USING btree (base_patient_unit_id);


--
-- Name: visit_clinic_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_clinic_visit_id ON imed.visit_clinic USING btree (visit_id);


--
-- Name: visit_deliver_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_deliver_visit_id ON imed.visit_deliver USING btree (visit_id);


--
-- Name: visit_doctor_discharge; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_doctor_discharge ON imed.visit USING btree (doctor_discharge);


--
-- Name: visit_fdiscd; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_fdiscd ON imed.visit USING btree (financial_discharge_date);


--
-- Name: visit_fdisct; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_fdisct ON imed.visit USING btree (financial_discharge_time);


--
-- Name: visit_financial_discharge; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_financial_discharge ON imed.visit USING btree (financial_discharge);


--
-- Name: visit_hn; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_hn ON imed.visit USING btree (hn);


--
-- Name: visit_id_accident_emergency_report_key; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_id_accident_emergency_report_key ON imed.accident_emergency_report USING btree (visit_id);


--
-- Name: visit_id_attending_physician_ke; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_id_attending_physician_ke ON imed.attending_physician USING btree (visit_id);


--
-- Name: visit_id_doctor_diagnosis_key; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_id_doctor_diagnosis_key ON imed.doctor_diagnosis USING btree (visit_id);


--
-- Name: visit_id_file4_chronic_key; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_id_file4_chronic_key ON imed.file4_chronic USING btree (visit_id);


--
-- Name: visit_id_file4_death_key; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_id_file4_death_key ON imed.file4_death USING btree (visit_id);


--
-- Name: visit_id_file4_promote_key; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_id_file4_promote_key ON imed.file4_promote USING btree (visit_id);


--
-- Name: visit_id_file4_surveil_key; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_id_file4_surveil_key ON imed.file4_surveil USING btree (visit_id);


--
-- Name: visit_id_refer_in_key; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_id_refer_in_key ON imed.refer_in USING btree (visit_id);


--
-- Name: visit_id_refer_out_key; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_id_refer_out_key ON imed.refer_out USING btree (visit_id);


--
-- Name: visit_old_pat_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_old_pat_id ON imed.visit USING btree (old_patient_id);


--
-- Name: visit_patient_age; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_patient_age ON imed.visit USING btree (patient_age);


--
-- Name: visit_patient_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_patient_id ON imed.visit USING btree (patient_id);


--
-- Name: visit_payment_cont_active; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_payment_cont_active ON imed.visit_payment USING btree (cont_credit_active);


--
-- Name: visit_payment_credit_type; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_payment_credit_type ON imed.visit_payment USING btree (fix_credit_type_id);


--
-- Name: visit_payment_expire_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_payment_expire_date ON imed.visit_payment USING btree (expire_date);


--
-- Name: visit_payment_main_hosp; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_payment_main_hosp ON imed.visit_payment USING btree (main_hospital_code);


--
-- Name: visit_payment_payer_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_payment_payer_id ON imed.visit_payment USING btree (payer_id);


--
-- Name: visit_payment_plan_code; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_payment_plan_code ON imed.visit_payment USING btree (plan_code);


--
-- Name: visit_payment_plan_group; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_payment_plan_group ON imed.visit_payment USING btree (base_plan_group_id);


--
-- Name: visit_payment_plan_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_payment_plan_id ON imed.visit_payment USING btree (plan_id);


--
-- Name: visit_payment_priority; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_payment_priority ON imed.visit_payment USING btree (priority);


--
-- Name: visit_payment_start_payment; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_payment_start_payment ON imed.visit_payment USING btree (start_payment_id);


--
-- Name: visit_payment_sub_hosp; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_payment_sub_hosp ON imed.visit_payment USING btree (sub_hospital_code);


--
-- Name: visit_payment_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_payment_visit_id ON imed.visit_payment USING btree (visit_id);


--
-- Name: visit_proxy_status; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_proxy_status ON imed.visit USING btree (proxy_status);


--
-- Name: visit_q_assign_loca_spid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_q_assign_loca_spid ON imed.visit_queue USING btree (assign_location_spid);


--
-- Name: visit_q_assign_oper_eid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_q_assign_oper_eid ON imed.visit_queue USING btree (assign_operate_eid);


--
-- Name: visit_q_next_clinic; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_q_next_clinic ON imed.visit_queue USING btree (next_department_id);


--
-- Name: visit_q_next_location_spid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_q_next_location_spid ON imed.visit_queue USING btree (next_location_spid);


--
-- Name: visit_q_next_operate_eid; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_q_next_operate_eid ON imed.visit_queue USING btree (next_operate_eid);


--
-- Name: visit_q_patient_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_q_patient_id ON imed.visit_queue USING btree (patient_id);


--
-- Name: visit_q_queue_status; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_q_queue_status ON imed.visit_queue USING btree (queue_status);


--
-- Name: visit_q_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_q_visit_id ON imed.visit_queue USING btree (visit_id);


--
-- Name: visit_q_visit_type; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_q_visit_type ON imed.visit_queue USING btree (fix_visit_type_id);


--
-- Name: visit_sound_file_visit_id_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_sound_file_visit_id_idx ON imed.visit_sound_file USING btree (visit_id);


--
-- Name: visit_status_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_status_idx ON imed.visit USING btree (fix_visit_status);


--
-- Name: visit_visit_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_visit_date ON imed.visit USING btree (visit_date);


--
-- Name: visit_visit_time; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_visit_time ON imed.visit USING btree (visit_time);


--
-- Name: visit_vn; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_vn ON imed.visit USING btree (vn);


--
-- Name: visit_waiting_reason_employee; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_waiting_reason_employee ON imed.visit_waiting_reason USING btree (employee_id);


--
-- Name: visit_waiting_reason_visit; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX visit_waiting_reason_visit ON imed.visit_waiting_reason USING btree (visit_id);


--
-- Name: vital_sign_extend_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX vital_sign_extend_visit_id ON imed.vital_sign_extend USING btree (visit_id);


--
-- Name: vital_sign_ipd_admit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX vital_sign_ipd_admit_id ON imed.vital_sign_ipd USING btree (admit_id);


--
-- Name: vital_sign_opd_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX vital_sign_opd_visit_id ON imed.vital_sign_opd USING btree (visit_id);


--
-- Name: vs_coma_visit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX vs_coma_visit_id ON imed.vs_coma_scale USING btree (visit_id);


--
-- Name: vs_patient_cs_admit_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX vs_patient_cs_admit_id ON imed.vs_patient_classification USING btree (admit_id);


--
-- Name: vs_patient_cs_classify_date; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX vs_patient_cs_classify_date ON imed.vs_patient_classification USING btree (classify_date);


--
-- Name: vs_patient_cs_detail_patient_classification; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX vs_patient_cs_detail_patient_classification ON imed.vs_patient_classification_detail USING btree (vs_patient_classification_id);


--
-- Name: vs_patient_cs_patient_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX vs_patient_cs_patient_id ON imed.vs_patient_classification USING btree (patient_id);


--
-- Name: xray_result_patient_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX xray_result_patient_id ON imed.xray_result USING btree (patient_id);




------------------------------------------------- Insert iMed Base Data -------------------------------------------------
INSERT INTO imed.abc_ven VALUES ('BE', 'B', 'E', '75', '90', '60', '21');
INSERT INTO imed.abc_ven VALUES ('AE', 'A', 'E', '30', '30', '60', '21');
INSERT INTO imed.abc_ven VALUES ('AN', 'A', 'N', '45', '45', '60', '21');
INSERT INTO imed.abc_ven VALUES ('BV', 'B', 'V', '60', '60', '90', '21');
INSERT INTO imed.abc_ven VALUES ('BN', 'B', 'N', '90', '90', '90', '21');
INSERT INTO imed.abc_ven VALUES ('CV', 'C', 'V', '105', '105', '120', '21');
INSERT INTO imed.abc_ven VALUES ('CE', 'C', 'E', '120', '120', '120', '21');
INSERT INTO imed.abc_ven VALUES ('CN', 'C', 'N', '135', '135', '120', '21');
INSERT INTO imed.abc_ven VALUES ('AV', 'A', 'V', '15', '20', '60', '21');

INSERT INTO imed.imed_config VALUES ('iMedMAP.properties', '#Wed Feb 01 15:09:32 ICT 2012\\015\\012MAP.SITE_LAT=13.764201\\015\\012MAP.SITE_LNG=100.680068\\015\\012MAP.GOOGLE_MAP_KEY=AIzaSyDTWaKt7Ksz0BKkqDS3NHh0mgDR0TZiNhE\\015\\012', 'admin', '2012-02-01', '15:09:32', 'MAP', 'iMed Maps');
INSERT INTO imed.imed_config VALUES ('imedIPSI.properties', '#Thu Feb 09 10:09:15 ICT 2012\\015\\012IPSI_SYNC_ITEM_SCHEDULE_TIME=13\\\\:45\\015\\012IPSI_IS_ENABLE_ITEM_NOT_ENOUGH=0\\015\\012IPSI.WS.URL=http\\\\://61.19.253.36\\\\:8080/service\\015\\012IPSI_TIMEOUT=5\\015\\012IPSI_TIMEOUT_COUNT=1\\015\\012IPSI_IS_ENABLE_ITEM_IN_STOCK=1\\015\\012IPSI_IS_INCOMPLETE_INTERFACE=0\\015\\012', 'admin', '2012-02-09', '10:09:15', 'IPSI', 'Interface iPensook');
INSERT INTO imed.imed_config VALUES ('imedLEARNING.properties', '#Thu Nov 24 13:32:03 ICT 2011\\015\\012IML.IS_ENABLE_DX_STAT=1\\015\\012IML.IS_ENABLE_DRUG_USAGE_STAT=1\\015\\012IML.LEARNING_SCHEDULE_TIME=17:42\\015\\012', 'admin', '2011-11-24', '13:32:03', 'LEARNING', 'iMed Learning');
INSERT INTO imed.imed_config VALUES ('imedSystem.properties', '#Wed Jun 06 16:05:05 ICT 2012\\015\\012SYS.IS_ENABLE_IPSI=0\\015\\012SYS.CHECK_DEFAULT_SP_BY_IP=0\\015\\012SYS.IS_ENABLE_PACS=0\\015\\012SYS.DEFAULT_SERVER_LANGUAGE=\\015\\012SYS.IS_ENABLE_LIS=0\\015\\012imedBOI.properties=1\\015\\012SYS.LOCK_TIMEOUT=60\\015\\012SYS.IS_ENABLE_LEARNING=1\\015\\012SYS.IS_ENABLE_MAP=1\\015\\012SYS.DATE_FORMAT=BUDDHIST_ERA\\015\\012SYS.IS_ENABLE_BOI=0\\015\\012SYS.IS_ENABLE_DOCSCAN=1\\015\\012DT_SYMBOL_PATH=http\\\\://192.168.1.161\\\\:8080/dentalImage/\\015\\012JNDI_CACHE_ENABLE=0\\015\\012PRINT_SERVER_ENABLE=0\\015\\012SYS.SHUTDOWN_TIMEOUT=90\\015\\012SYS.IS_ENABLE_POS=1\\015\\012', 'admin', '2012-06-06', '16:05:05', NULL, NULL);
INSERT INTO imed.imed_config VALUES ('iMedPOS.properties', '#Tue Feb 28 19:07:53 ICT 2012\\015\\012POS.GENERAL_CUST_HN=99-55\\015\\012POS.BARCODE_KEY_DELAY_MS=50\\015\\012', 'admin', '2012-02-28', '19:07:53', 'POS', 'Point of Sale');
INSERT INTO imed.imed_config VALUES ('imed.properties', '#Tue Apr 10 18:11:08 ICT 2012\\015\\012LAB.AUTO_PRINT_RESULT_AFTER_COMPLETE=0\\015\\012RESET_AFTER_DOCTOR_DISCHARGE=1\\015\\012FN.EDIT_DECIMAL_DISCOUNT=0\\015\\012NT.NOT_DEL_FOOD_SHEET_WHEN_DOCTOR_ALLOW=0\\015\\012VS_CC_BT_MAX=41\\015\\012GE.CHECK_DOCTOR_KEY_ORDER=1\\015\\012VS_SHOW_CHK_ABPR_ABPRES=1\\015\\012EXT.USE_FINGER_PRINT=0\\015\\012NT.NOT_ALLOW_MANY_SHEETS_PER_DAY=0\\015\\012VS_BT_MAX=36.5\\015\\012BB_LAB_ITEM_ID_DONATE=labitems_694\\015\\012LAB.CHECK_LABTEST_ALERT_TEXT=0\\015\\012DEFAULT_AUTO_OF_SEARCH=1\\015\\012REGIST.SHOW_EXPIRE_PAYMENT=1\\015\\012RX.OPD_PRINT_SUPPLY=1\\015\\012IS_SEARCH_EMPLOYEE_AUTO_COMPLETE=1\\015\\012VS_INFANT_RR_MAX=60\\015\\012IS_SHOW_OUT_HOSPITAL_DATA=0\\015\\012FN.EMPLOYEE_CREDIT_CUT_PAID=0\\015\\012OLD_HN_PATTERN=\\015\\012RX.CHECK_DRUG_PREGNANCY=1\\015\\012VS_CC_RR_MIN=12\\015\\012FN.CREATE_PAY_INVOICE_VOUCHER=01.01.01.00.00.00\\015\\012VS_CHILD_BP_SYS_MAX=110\\015\\012REGIST.CHECK_DOCTOR_DISCHARGE=0\\015\\012ORDER_SHOW_OUT_OF_STOCK=0\\015\\012FN.USE_TEMPLATE_DISCOUNT_ONLY=0\\015\\012VS_CC_WEIGHT_MAX=200\\015\\012FN.DEFAULT_SUMCOST_ALL=0\\015\\012REGIST.AUTO_REQUEST_PAT_FILE=0\\015\\012VS_SHOW_DTX_AND_HCT=1\\015\\012DEFAULT_ORDER_DRUG_CONTINUE_INTERVAL=6\\015\\012VS_CC_HEIGTH_MAX=250\\015\\012BILLING_DEBT_NUM_DATE=3\\015\\012IPD.DISCONTINUE_ORDER_WHEN=FD\\015\\012IPD.IPD_DOCTOR_ALLOW=0\\015\\012VS_OLDER_CHILD_BP_SYS_MIN=90\\015\\012RX.DOSE_DISPLAY_FORMAT=1\\015\\012IS_DOCTOR_NOT_SHOW_OTHER_DF=1\\015\\012MD.CHECK_DX_BEFORE_ORDER_DF=0\\015\\012MC_DEFAULT_SHOW_DIAG=1\\015\\012VS_CC_PR_MAX=160\\015\\012BB_SERVICE_POINT_ID=038.1\\015\\012FN.CREATE_DEBT_IGNORE_COM_NUMBER=1\\015\\012SCHEDULE_NIGHT_BEGIN=20\\\\:00\\015\\012VS_ADULT_RR_MIN=18\\015\\012RX.AUTO_DISPENSE_DRUG_FORMAT=1\\015\\012LAB.PROTECT_MODIFY_ORDER_WHEN_RECEIVE_SPECIMEN=1\\015\\012ORDER_CONT_DEFAULT_TIME=20\\\\:00\\015\\012VS_OLDER_CHILD_RR_MIN=12\\015\\012FN.AUTO_FNDC_SELF_PAY=1\\015\\012LAB_CHILD_AGE_LESS=15\\015\\012RX.INPUT_DOSE_MIX_DRUG=0\\015\\012FN.CHECK_TARIFF_WHEN_ADMIT=0\\015\\012REGIST.CHECK_PATIENT_DUPLICATE_NAME=1\\015\\012GE.AUTO_REFRESH_QUEUE=10000\\015\\012IN_TIME_END=19\\\\:30\\015\\012VS_CHILD_BP_DIAS_MAX=75\\015\\012IS_SHOW_DOCTOR_DRUG_ALLERGY=1\\015\\012VS_CHILD_RR_MIN=20\\015\\012GE.CHECK_DF_ORDER_SET=0\\015\\012LIMIT_APPOINTMENT=7\\015\\012IPD.AUTO_REFRESH_PATIENT_WAITING_BED_QUEUE=10000\\015\\012FN.MONEY_START_SUMCOST=30000\\015\\012RX.IPD_PRINT_DRUG=1\\015\\012VS_ADULT_PR_MAX=120\\015\\012VS_CC_BP_DIAS_MIN=0\\015\\012NT.GET_DEFAULT_NT_ORDER_BY_BASE=0\\015\\012VS_ADULT_BP_DIAS_MIN=50\\015\\012VS_OLDER_CHILD_PR_MAX=100\\015\\012RX.PRINT_DRUG_DOC_OPD_IPD_EACH_PRINTER=1\\015\\012GE.DF_ADDITION_DOCTOR=1\\015\\012VS_OLDER_CHILD_BP_DIAS_MIN=60\\015\\012RX.NOT_PRINT_DRUG_STICKER_FOR_IPD=0\\015\\012GE.WARNING_PATIENT_WAIT_TIME=10\\015\\012VS_INFANT_BP_SYS_MAX=90\\015\\012VS_ADULT_BP_SYS_MAX=140\\015\\012VS_CC_BT_MIN=35\\015\\012IPD.AUTO_ORDER_CONT_ADMIT=0\\015\\012VS_CHILD_PR_MAX=100\\015\\012IPD.AUTO_REFRESH_IPD_QUEUE=10000\\015\\012VS_BT_MIN=30\\015\\012IS_ALERT_CRM=1\\015\\012MD.AUTO_SELECT_NEXTPATIENT_INQUEUE=0\\015\\012ANES_CASE_NUMBER=AC\\015\\012RX.CHECK_DRUG_INTERACTION=1\\015\\012EXT.DEFAULT_PICTURE_FORMAT=PICTURE\\015\\012BB_PATIENT_NUMBER=BN\\015\\012RX.IPD_PRINT_SUPPLY=1\\015\\012VS_INFANT_RR_MIN=25\\015\\012FN.AUTO_REFRESH_NOT_PAY_QUEUE=10000\\015\\012FN.RECEIPT_NUMBER_FORMAT=1\\015\\012RX.NOT_PRINT_CAUTION=0\\015\\012FN.AUTO_USE_DISCOUNT_CARD=1\\015\\012MD.DISPLAY_ASSIGN_PATIENT_DIALOG=0\\015\\012REGIST.AUTO_PRINT_PRESCRIPTION=0\\015\\012VS_CHILD_BP_SYS_MIN=80\\015\\012IS_ENABLE_SUPPLY_EXTRA_PRICE=1\\015\\012FN.DECIMAL_DISCOUNT=1\\015\\012REGIST.DEFAULT_VISIT_EID=1\\015\\012GE.CHECK_RECEIVE_PATIENT_FILE=0\\015\\012IS_ENABLE_SUPPLY_TAKE_HOME=0\\015\\012MD.SAVE_DIAGNOSIS_BEFORE_FINISH=0\\015\\012VS_CC_WEIGHT_MIN=2\\015\\012HP_PATIENT_TYPE_CODE=HP\\015\\012LAB_THALAS_PATIENT_NUMBER=TN\\015\\012GE.AUTO_VERIFY_ORDER=1\\015\\012VS_INFANT_BP_DIAS_MAX=60\\015\\012VS_CC_HEIGTH_MIN=100\\015\\012LAB.LN_FORMAT=2\\015\\012VS_INFANT_PR_MAX=160\\015\\012GE.SEARCH_ITEM_CATEGORY=1\\015\\012RX.AUTO_PRINT_RX_PHARM_WHEN_ASSIGN=0\\015\\012XR.CHECK_CANCEL_XRAY_4_PRINT_SLIP=0\\015\\012LAB.DISPLAY_RESULT_LAB_MICRO_STYLE=1\\015\\012RX.AUTO_ASSIGN_EXE_PRESCRIPTION=0\\015\\012FN.PRINT_CASH_DOC_OPD_IPD_EACH_PRINTER=0\\015\\012FN.CASH_INVOICE_PRINT_DIAGNOSIS=0\\015\\012IS_PRINT_ITEM_SET_PRICE=0\\015\\012VS_CC_PR_MIN=40\\015\\012RX.ENABLE_ORDER_DRUG_ZERO=0\\015\\012FN.CREDIT_PERCASE_CUT_PAID=0\\015\\012IPD.AUTO_DC_BED_WHEN_FNDC=0\\015\\012RX.AUTO_DISPENSE_DRUG=1\\015\\012VS_CC_BP_SYS_MAX=300\\015\\012RX.CHECK_DRUG_REPEAT_ORDER=1\\015\\012IS_DEFAULT_PRINT_MC=0\\015\\012IS_DEFAULT_PRINT_DOCTOR_OPDCARD=0\\015\\012LAB.AUTO_REFRESH_LAB_QUEUE=10000\\015\\012RX.CHECK_DRUG_ALLERGY=1\\015\\012GE.SHOW_COST_IN_TAB_ORDER=1\\015\\012FN.DEFAULT_CREDIT_TYPE=1\\015\\012VS_INFANT_AGE=1\\015\\012PRINT_MEDCERT_QTY=1\\015\\012REGIST.AUTO_RETURN_PAT_FILE=0\\015\\012SCHEDULE_MORNING_BEGIN=08\\\\:00\\015\\012RX.DRUG_QTY_INT_ONLY=0\\015\\012VS_CHILD_BP_DIAS_MIN=60\\015\\012MD.DOCTOR_AUTO_SAVE=1\\015\\012IPD_MED_RECORD_ROW=5\\015\\012GE.SHOW_DIAGNOSIS_IN_PATIENTINFO=0\\015\\012LAB.CHECK_ASSIGN_REPEAT_LAB=1\\015\\012GE.HN_FORMAT=2\\015\\012VS_ADULT_PR_MIN=60\\015\\012IS_DEFAULT_PRINT_DOCTOR_PRESCRIPTION=0\\015\\012RN.CHECK_NURSE_EDIT_VS=0\\015\\012FN.CASH_RECEIPT_PRINT_DIAGNOSIS=0\\015\\012VS_CC_RR_MAX=60\\015\\012VS_OLDER_CHILD_PR_MIN=60\\015\\012REGIST.AUTO_PRINT_OPD_CARD=0\\015\\012GE.LAST_ORDER_ON_TOP=0\\015\\012REGIST.USE_MORE_DETAIL_REFER=0\\015\\012FISCAL_YEAR_MONTH=10\\015\\012RX.NOT_PRINT_DESCRIPTION=0\\015\\012BB_RESERVE_BLOOD_ITEM_ID=labitems_334\\015\\012GUI_HIDE_ER_RISK=0\\015\\012VS_OLDER_CHILD_AGE=15\\015\\012VS_INFANT_BP_SYS_MIN=60\\015\\012VS_ADULT_BP_SYS_MIN=80\\015\\012FN.DEFAULT_PLAN_ORDER_EXCEPT=0\\015\\012LAB_OLD_MAN_AGE_MORE=60\\015\\012VS_CHILD_AGE=9\\015\\012VS_CHILD_PR_MIN=80\\015\\012FN.LOCK_ORDER_WHEN_FAX_CLAIM=0\\015\\012IN_TIME_BEGIN=07\\\\:00\\015\\012NUMBER_FOR_SEARCH_BILL_GOV=300\\015\\012VS_OLDER_CHILD_BP_SYS_MAX=140\\015\\012RX.AUTO_PRINT_PRESCRIPTION_WHEN_COMMIT_PRESCRIPTION=0\\015\\012REGIST.AUTO_PRINT_PATIENT_CARD=0\\015\\012FN.PRINT_INVOICE_COST_WITH_INVOICE=1\\015\\012BB_PLAN_ID_ON_DONATE=A0020\\015\\012FN.AUTO_BILLING_DISCOUNT=1\\015\\012MD.CHECK_DX_BEFORE_ORDER_DRUG=0\\015\\012VS_ADULT_RR_MAX=30\\015\\012RX.CHECK_DRUG_SPECIALIST_ONLY=1\\015\\012ORDER_SHOW_DRUG_TAKE_HOME=0\\015\\012REGIST.CHECK_PATIENT_DUPLICATE_PID=1\\015\\012APPOINT_TIME_TEMPLATE=\\015\\012RX.OPD_PRINT_DRUG=1\\015\\012VS_OLDER_CHILD_RR_MAX=25\\015\\012PRINT_MED_PROFILE_COLUMN_COUNT=10\\015\\012TAB_MENU_AUTH=<mappers>\\\\n  <mapper guiseq\\\\="1" dbseq\\\\="1" tabname\\\\="patientServicePoint"/>\\\\n  <mapper guiseq\\\\="2" dbseq\\\\="35" tabname\\\\="clinicQueue"/>\\\\n  <mapper guiseq\\\\="3" dbseq\\\\="2" tabname\\\\="prescriptionQueue"/>\\\\n  <mapper guiseq\\\\="4" dbseq\\\\="3" tabname\\\\="patientWard"/>\\\\n  <mapper guiseq\\\\="5" dbseq\\\\="4" tabname\\\\="remainDoctorDischarge"/>\\\\n  <mapper guiseq\\\\="6" dbseq\\\\="5" tabname\\\\="listVisitQueueForLab"/>\\\\n  <mapper guiseq\\\\="7" dbseq\\\\="6" tabname\\\\="listVisitQueueForXray"/>\\\\n  <mapper guiseq\\\\="8" dbseq\\\\="7" tabname\\\\="listVisit"/>\\\\n  <mapper guiseq\\\\="9" dbseq\\\\="8" tabname\\\\="queueOperate"/>\\\\n  <mapper guiseq\\\\="10" dbseq\\\\="9" tabname\\\\="queueNutrition"/>\\\\n  <mapper guiseq\\\\="11" dbseq\\\\="10" tabname\\\\="doctorRoom"/>\\\\n  <mapper guiseq\\\\="12" dbseq\\\\="11" tabname\\\\="patientSocialData"/>\\\\n  <mapper guiseq\\\\="13" dbseq\\\\="12" tabname\\\\="operation"/>\\\\n  <mapper guiseq\\\\="14" dbseq\\\\="13" tabname\\\\="labor"/>\\\\n  <mapper guiseq\\\\="15" dbseq\\\\="14" tabname\\\\="nutrition"/>\\\\n  <mapper guiseq\\\\="16" dbseq\\\\="15" tabname\\\\="patientMedicalData"/>\\\\n  <mapper guiseq\\\\="17" dbseq\\\\="16" tabname\\\\="labResult"/>\\\\n  <mapper guiseq\\\\="18" dbseq\\\\="17" tabname\\\\="xray"/>\\\\n  <mapper guiseq\\\\="19" dbseq\\\\="18" tabname\\\\="vitalsign"/>\\\\n  <mapper guiseq\\\\="20" dbseq\\\\="19" tabname\\\\="orderItem"/>\\\\n  <mapper guiseq\\\\="21" dbseq\\\\="36" tabname\\\\="clinicPrescription"/>\\\\n  <mapper guiseq\\\\="22" dbseq\\\\="20" tabname\\\\="diagnosisAndOperation"/>\\\\n  <mapper guiseq\\\\="23" dbseq\\\\="21" tabname\\\\="dental"/>\\\\n  <mapper guiseq\\\\="24" dbseq\\\\="22" tabname\\\\="billing"/>\\\\n  <mapper guiseq\\\\="25" dbseq\\\\="23" tabname\\\\="billingIpd"/>\\\\n  <mapper guiseq\\\\="26" dbseq\\\\="24" tabname\\\\="admit"/>\\\\n  <mapper guiseq\\\\="27" dbseq\\\\="25" tabname\\\\="dialysis"/>\\\\n  <mapper guiseq\\\\="28" dbseq\\\\="26" tabname\\\\="queueLIS"/>\\\\n  <mapper guiseq\\\\="29" dbseq\\\\="27" tabname\\\\="billingGov"/>\\\\n  <mapper guiseq\\\\="30" dbseq\\\\="28" tabname\\\\="healthPromotion"/>\\\\n  <mapper guiseq\\\\="31" dbseq\\\\="29" tabname\\\\="anesthesia"/>\\\\n  <mapper guiseq\\\\="32" dbseq\\\\="30" tabname\\\\="bloodBank"/>\\\\n  <mapper guiseq\\\\="33" dbseq\\\\="31" tabname\\\\="pathology"/>\\\\n  <mapper guiseq\\\\="34" dbseq\\\\="32" tabname\\\\="forensic"/>\\\\n  <mapper guiseq\\\\="35" dbseq\\\\="33" tabname\\\\="patientClassify"/>\\\\n  <mapper guiseq\\\\="36" dbseq\\\\="34" tabname\\\\="queuePathology"/>\\\\n</mappers>\\015\\012RX.AUTO_REFRESH_PHARM_QUEUE=10000\\015\\012DEFAULT_APPOINTMENT_TIME=09\\\\:00\\\\:00\\015\\012GE.REVERSE_DISCHARGE_TYPE=0\\015\\012DEFAULT_ASSIGN_TO_PHARM=0\\015\\012GE.AUTO_ASSIGN_LAB_XRAY=1\\015\\012FN.ENABLE_NOT_PAY_QUEUE=0\\015\\012GE.ASSIGNER_ADDITION_DOCTOR=0\\015\\012LAB_NO_BLANK_RESULT=1\\015\\012LAB.CHECK_CANCEL_LAB_4_PRINT_SLIP=0\\015\\012GE.DEFAULT_DOCTOR_ON_ASSIGN_LAB=1\\015\\012REGIST.AUTO_PRINT_VISIT_SCREEN_SLIP=0\\015\\012IPD.CHECK_BED_SERVICEABLE=0\\015\\012DEFAULT_CONSIST_OF_SEARCH=0\\015\\012IS_ALERT_READ_PATIENT_HAVE_DEBT=0\\015\\012VS_INFANT_BP_DIAS_MIN=50\\015\\012MD.DOCTOR_VERIFY_PASSWORD_PRESCRIPTION=0\\015\\012VS_INFANT_PR_MIN=100\\015\\012VS_CHILD_RR_MAX=40\\015\\012IS_ALERT_DRUG_BLOOD_ALLERGY=1\\015\\012VS_CC_BP_DIAS_MAX=120\\015\\012XR.AUTO_REFRESH_XRAY_QUEUE=10000\\015\\012SCHEDULE_EVENING_BEGIN=17\\\\:00\\015\\012FN.DEFAULT_CASH_IPD_DISCHARGE=1\\015\\012FN.DEFAULT_PRINT_EXPENSE=1\\015\\012LAB.DEFAULT_ASSIGN_LAB_SPID=0\\015\\012VS_ADULT_BP_DIAS_MAX=90\\015\\012XR.IS_ENABLE_SCROLL_BAR=0\\015\\012RX.INPUT_NURSE_IN_PRESCRIPTION=0\\015\\012REGIST.CHECK_PLAN_EXTERNAL=0\\015\\012VS_CC_BP_SYS_MIN=10\\015\\012VS_OLDER_CHILD_BP_DIAS_MAX=90\\015\\012LAB_BLOOD_GROUP_TEST_CODE=[object]\\015\\012IPD.AUTO_ORDER_ROOM_MIDNIGHT=0\\015\\012', 'admin', '2012-04-10', '18:11:08', NULL, NULL);
INSERT INTO imed.imed_config VALUES ('imedDOCSCAN.properties', '#Tue Apr 10 15:56:49 ICT 2012\\015\\012IDENTIFIED_PICTURE_SERVER_PATH=D\\\\:/jboss-4.2.3.GA/server/default/deploy/imed-image-folder.war/\\015\\012SYS_ITEM_PICTURE_DEFAULT_URL=http\\\\://192.168.1.161\\\\:8080/imed-image-folder/ITEM/\\015\\012IDENTIFIED_PICTURE_STORE_URL=http\\\\://192.168.1.161\\\\:8080/imed-image-folder/\\015\\012DOCSCAN.OPDCARD_FOLDERNAME=OPD\\015\\012DOS_COLUMN_IN_QUEUE=0\\015\\012IDENTIFIED_PICTURE_SERVICE_URL=http\\\\://192.168.1.161\\\\:8080/imed/\\015\\012DOCSCAN.LABRESULT_FOLDERNAME=LAB\\015\\012SYS_ITEM_PICTURE_SERVICE_URL=http\\\\://192.168.1.161\\\\:8080/imed-image/\\015\\012DOCSCAN.DISCHARGE_PLAN_FOLDERNAME=IPS\\015\\012DOCSCAN.CREATE_DOCUMENT_SCAN_AFTER_PRINT=0\\015\\012DOCSCAN.DISCHARGE_SUMMARY_FOLDERNAME=DCS\\015\\012SYS_ITEM_PICTURE_DEFAULT_PATH=D\\\\:/jboss-4.2.3.GA/server/default/deploy/imed-image-folder.war/ITEM\\015\\012', 'admin', '2012-04-10', '15:56:49', 'DOCSCAN', 'Document Scan');

INSERT INTO imed.imed_language VALUES ('languageStock.properties', '#Fri Sep 09 08:57:53 GMT 2011\\015\\012FX_STK_MTD_ADJUST_TO_INCREASE=\\\\u0E1B\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E40\\\\u0E1E\\\\u0E34\\\\u0E48\\\\u0E21\\015\\012FX_STK_MTD_RECEIVE_STOCK_TO_STOCK_BY_RETURN=\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E04\\\\u0E37\\\\u0E19/\\\\u0E22\\\\u0E49\\\\u0E32\\\\u0E22\\015\\012RP_PRINT_LIST_ITEM_DETAIL_BY_DISTRIBUTOR_ID=\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\\\u0E41\\\\u0E2A\\\\u0E14\\\\u0E07\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\\\u0E17\\\\u0E35\\\\u0E48\\\\u0E0B\\\\u0E37\\\\u0E49\\\\u0E2D\\\\u0E08\\\\u0E32\\\\u0E01\\\\u0E1C\\\\u0E39\\\\u0E49\\\\u0E41\\\\u0E17\\\\u0E19\\\\u0E08\\\\u0E33\\\\u0E2B\\\\u0E19\\\\u0E48\\\\u0E32\\\\u0E22\\015\\012FX_STOCK_SUPPLY_PUSH_PREFIX=SP\\015\\012RP_PRINT_LIST_NOT_CHANGE_VALUE_IN_STOCK=\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\\\u0E17\\\\u0E35\\\\u0E48\\\\u0E44\\\\u0E21\\\\u0E48\\\\u0E40\\\\u0E04\\\\u0E25\\\\u0E37\\\\u0E48\\\\u0E2D\\\\u0E19\\\\u0E44\\\\u0E2B\\\\u0E27\\015\\012RP_PRINT_LIST_RETURN_DETAIL_RECORD_SHORT=\\\\u0E04\\\\u0E37\\\\u0E19/\\\\u0E22\\\\u0E49\\\\u0E32\\\\u0E22\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\\\u0E15\\\\u0E32\\\\u0E21\\\\u0E2B\\\\u0E19\\\\u0E48\\\\u0E27\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\015\\012RP_PRINT_ITEM_DETAIL_IN_STOCK_SHORT=\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E25\\\\u0E30\\\\u0E40\\\\u0E2D\\\\u0E35\\\\u0E22\\\\u0E14\\\\u0E02\\\\u0E49\\\\u0E2D\\\\u0E21\\\\u0E39\\\\u0E25\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\015\\012FX_STK_MTD_DISPENSE_STOCK_TO_STOCK_BY_RETURN=\\\\u0E04\\\\u0E37\\\\u0E19/\\\\u0E22\\\\u0E49\\\\u0E32\\\\u0E22\\015\\012RP_PRINT_LIST_ITEM_CHANGE_ITEM_PRICE_SHORT=\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E1B\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E40\\\\u0E1B\\\\u0E25\\\\u0E35\\\\u0E48\\\\u0E22\\\\u0E19\\\\u0E23\\\\u0E32\\\\u0E04\\\\u0E32\\\\u0E02\\\\u0E32\\\\u0E22\\015\\012FX_STK_MTD_RECEIVE_PATIENT_TO_STOCK=\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E04\\\\u0E37\\\\u0E19\\\\u0E08\\\\u0E32\\\\u0E01\\\\u0E1C\\\\u0E39\\\\u0E49\\\\u0E1B\\\\u0E48\\\\u0E27\\\\u0E22\\015\\012RP_PRINT_LIST_NEARLY_EXPIRE_IN_STOCK_SHORT=\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\\\u0E43\\\\u0E01\\\\u0E25\\\\u0E49\\\\u0E2B\\\\u0E21\\\\u0E14\\\\u0E2D\\\\u0E32\\\\u0E22\\\\u0E38\\015\\012FX_TYPE_IS_DEPARTMENT=\\\\u0E04\\\\u0E25\\\\u0E31\\\\u0E07\\015\\012FX_STK_MTD_RECEIVE_DISTRIBUTOR_TO_STOCK_BY_EXCHANGE=\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E08\\\\u0E32\\\\u0E01\\\\u0E41\\\\u0E25\\\\u0E01\\015\\012RP_PRINT_LIST_SUMMARY_VALUE_IN_STOCK=\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\\\u0E2A\\\\u0E23\\\\u0E38\\\\u0E1B\\\\u0E21\\\\u0E39\\\\u0E25\\\\u0E04\\\\u0E48\\\\u0E32\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\\\u0E04\\\\u0E07\\\\u0E04\\\\u0E25\\\\u0E31\\\\u0E07\\015\\012FX_STK_MTD_DISPENSE_STOCK_TO_DISTRIBUTOR=\\\\u0E04\\\\u0E37\\\\u0E19\\\\u0E1C\\\\u0E39\\\\u0E49\\\\u0E02\\\\u0E32\\\\u0E22\\015\\012FX_STK_MTD_DISPENSE_STOCK_TO_STOCK_BY_TRANSFER=\\\\u0E42\\\\u0E2D\\\\u0E19\\015\\012RP_PRINT_RECV_TO_STOCK=\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\\\u0E40\\\\u0E02\\\\u0E49\\\\u0E32\\\\u0E04\\\\u0E25\\\\u0E31\\\\u0E07\\015\\012FX_STK_MTD_DISPENSE_RE_STERILED_TO_STOCK=\\\\u0E2A\\\\u0E48\\\\u0E07 Re-Sterile\\015\\012FX_STK_MTD_REVEIVE_RE_STERILED_TO_STOCK=\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E08\\\\u0E32\\\\u0E01 Re-Sterile\\015\\012FX_STK_MTD_DISPENSE_EO_TO_STOCK=\\\\u0E2A\\\\u0E48\\\\u0E07 EO\\015\\012FX_STK_MTD_DISPENSE_STOCK_TO_DISAPPEAR=\\\\u0E2A\\\\u0E39\\\\u0E0D\\\\u0E2B\\\\u0E32\\\\u0E22\\015\\012RP_PRINT_LIST_STOCK_ORDER_HISTORY_SHORT=\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\\\u0E1B\\\\u0E23\\\\u0E30\\\\u0E27\\\\u0E31\\\\u0E15\\\\u0E34\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E2A\\\\u0E31\\\\u0E48\\\\u0E07\\\\u0E0B\\\\u0E37\\\\u0E49\\\\u0E2D\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\015\\012FX_STK_MTD_RECEIVE_PRODUCE_TO_STOCK=\\\\u0E1C\\\\u0E25\\\\u0E34\\\\u0E15\\015\\012FX_STK_MTD_RECEIVE_ITEM_EMERGENCY=\\\\u0E40\\\\u0E1E\\\\u0E34\\\\u0E48\\\\u0E21\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\\\u0E09\\\\u0E38\\\\u0E01\\\\u0E40\\\\u0E09\\\\u0E34\\\\u0E19\\015\\012RP_PRINT_LIST_TRANSFER_DETAIL_RECORD_SHORT=\\\\u0E42\\\\u0E2D\\\\u0E19/\\\\u0E08\\\\u0E48\\\\u0E32\\\\u0E22\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\\\u0E15\\\\u0E32\\\\u0E21\\\\u0E2B\\\\u0E19\\\\u0E48\\\\u0E27\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\015\\012FX_STK_MTD_RECEIVE_REPACK_TO_STOCK=\\\\u0E41\\\\u0E1A\\\\u0E48\\\\u0E07\\\\u0E1A\\\\u0E23\\\\u0E23\\\\u0E08\\\\u0E38\\015\\012FX_STOCK_DISPENSE_PREFIX=TF\\015\\012FX_STK_MTD_RECEIVE_OTHER_TO_STOCK=\\\\u0E2D\\\\u0E37\\\\u0E48\\\\u0E19\\\\u0E46\\015\\012RP_PRINT_STOCK_MASTER_LIST=\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\\\u0E04\\\\u0E07\\\\u0E40\\\\u0E2B\\\\u0E25\\\\u0E37\\\\u0E2D \\\\u0E13 \\\\u0E1B\\\\u0E31\\\\u0E08\\\\u0E38\\\\u0E1A\\\\u0E31\\\\u0E19\\015\\012RP_PRINT_RECV_TO_STOCK_SHORT=\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E40\\\\u0E02\\\\u0E49\\\\u0E32\\015\\012FX_STK_MTD_DISPENSE_STOCK_TO_REPACK=\\\\u0E19\\\\u0E33\\\\u0E44\\\\u0E1B\\\\u0E41\\\\u0E1A\\\\u0E48\\\\u0E07\\\\u0E1A\\\\u0E23\\\\u0E23\\\\u0E08\\\\u0E38\\015\\012FX_STK_MTD_REVEIVE_DISINFECTIONED_TO_STOCK=\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E08\\\\u0E32\\\\u0E01 Disinfection\\015\\012FX_STOCK_PREPACK_ITEM_PREFIX=PI\\015\\012FX_STK_MTD_DISPENSE_STOCK_TO_EXPIRE=\\\\u0E2B\\\\u0E21\\\\u0E14\\\\u0E2D\\\\u0E32\\\\u0E22\\\\u0E38\\015\\012RP_PRINT_DISP_FROM_STOCK_SHORT=\\\\u0E08\\\\u0E48\\\\u0E32\\\\u0E22\\\\u0E2D\\\\u0E2D\\\\u0E01\\015\\012FX_REQUEST_ORDER_PREFIX=PR\\015\\012RP_PRINT_LIST_ADD_ORDER_EMERGENCY=\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E40\\\\u0E1E\\\\u0E34\\\\u0E48\\\\u0E21\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\\\u0E09\\\\u0E38\\\\u0E01\\\\u0E40\\\\u0E09\\\\u0E34\\\\u0E19\\015\\012RP_PRINT_LIST_REQUEST_ORDER_SHORT=\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E02\\\\u0E2D\\\\u0E08\\\\u0E31\\\\u0E14\\\\u0E0B\\\\u0E37\\\\u0E49\\\\u0E2D\\015\\012RP_PRINT_LIST_RETURN_DETAIL_RECORD=\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\\\u0E04\\\\u0E37\\\\u0E19/\\\\u0E22\\\\u0E49\\\\u0E32\\\\u0E22\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\\\u0E15\\\\u0E32\\\\u0E21\\\\u0E2B\\\\u0E19\\\\u0E48\\\\u0E27\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\015\\012FX_STK_MTD_DISPENSE_WASH_TO_STOCK=\\\\u0E2A\\\\u0E48\\\\u0E07\\\\u0E0B\\\\u0E31\\\\u0E01\\\\u0E1F\\\\u0E2D\\\\u0E01\\015\\012RP_PRINT_LIST_RECEIVE_TO_STOCK_BY_OTHER_METHOD=\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\\\u0E40\\\\u0E02\\\\u0E49\\\\u0E32\\\\u0E04\\\\u0E25\\\\u0E31\\\\u0E07\\\\u0E14\\\\u0E49\\\\u0E27\\\\u0E22\\\\u0E27\\\\u0E34\\\\u0E18\\\\u0E35\\\\u0E2D\\\\u0E37\\\\u0E48\\\\u0E19\\\\u0E46\\015\\012RP_PRINT_LIST_WORTHLESS_QTY_IN_STOCK=\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\\\u0E2B\\\\u0E21\\\\u0E14\\015\\012RP_PRINT_LIST_ADJUST_IN_STOCK_SHORT=\\\\u0E1B\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E22\\\\u0E2D\\\\u0E14\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\015\\012RP_PRINT_LIST_ORDER_DETAIL_RECORD=\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\\\u0E1A\\\\u0E31\\\\u0E19\\\\u0E17\\\\u0E36\\\\u0E01\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E25\\\\u0E30\\\\u0E40\\\\u0E2D\\\\u0E35\\\\u0E22\\\\u0E14\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E02\\\\u0E2D\\\\u0E0B\\\\u0E37\\\\u0E49\\\\u0E2D\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\015\\012FX_STOCK_ADJUST_PREFIX=TR\\015\\012FX_STK_MTD_DISPENSE_STERILED_TO_STOCK=\\\\u0E2A\\\\u0E48\\\\u0E07 Sterile\\015\\012RP_PRINT_DISP_FROM_STOCK=\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E08\\\\u0E48\\\\u0E32\\\\u0E22\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\\\u0E2D\\\\u0E2D\\\\u0E01\\\\u0E08\\\\u0E32\\\\u0E01\\\\u0E04\\\\u0E25\\\\u0E31\\\\u0E07\\015\\012RP_PRINT_ITEM_DETAIL_IN_STOCK=\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E25\\\\u0E30\\\\u0E40\\\\u0E2D\\\\u0E35\\\\u0E22\\\\u0E14\\\\u0E02\\\\u0E49\\\\u0E2D\\\\u0E21\\\\u0E39\\\\u0E25\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\015\\012FIX_IS_NOT_UPDATE_CUR_QTY=\\\\u0E44\\\\u0E21\\\\u0E48\\\\u0E15\\\\u0E49\\\\u0E2D\\\\u0E07 Update \\\\u0E08\\\\u0E33\\\\u0E19\\\\u0E27\\\\u0E19\\\\u0E04\\\\u0E07\\\\u0E04\\\\u0E25\\\\u0E31\\\\u0E07\\015\\012RP_PRINT_LIST_ITEM_USE_RATE_IN_STOCK=\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E0A\\\\u0E37\\\\u0E48\\\\u0E2D\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\\\u0E43\\\\u0E19\\\\u0E04\\\\u0E25\\\\u0E31\\\\u0E07\\015\\012RP_PRINT_LIST_ITEM_DETAIL_BY_DISTRIBUTOR_ID_SHORT=\\\\u0E41\\\\u0E2A\\\\u0E14\\\\u0E07\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\\\u0E17\\\\u0E35\\\\u0E48\\\\u0E0B\\\\u0E37\\\\u0E49\\\\u0E2D\\\\u0E08\\\\u0E32\\\\u0E01\\\\u0E1C\\\\u0E39\\\\u0E49\\\\u0E41\\\\u0E17\\\\u0E19\\\\u0E08\\\\u0E33\\\\u0E2B\\\\u0E19\\\\u0E48\\\\u0E32\\\\u0E22\\015\\012RP_PRINT_LIST_NOT_CHANGE_VALUE_IN_STOCK_SHORT=\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\\\u0E17\\\\u0E35\\\\u0E48\\\\u0E44\\\\u0E21\\\\u0E48\\\\u0E40\\\\u0E04\\\\u0E25\\\\u0E37\\\\u0E48\\\\u0E2D\\\\u0E19\\\\u0E44\\\\u0E2B\\\\u0E27\\015\\012RP_PRINT_ITEM_OVER_MAX_IN_STOCK_SHORT=\\\\u0E22\\\\u0E32\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\\\u0E04\\\\u0E25\\\\u0E31\\\\u0E07\\\\u0E17\\\\u0E35\\\\u0E48\\\\u0E21\\\\u0E32\\\\u0E01\\\\u0E01\\\\u0E27\\\\u0E48\\\\u0E32\\\\u0E01\\\\u0E33\\\\u0E2B\\\\u0E19\\\\u0E14\\\\u0E04\\\\u0E48\\\\u0E32\\\\u0E2A\\\\u0E39\\\\u0E07\\\\u0E2A\\\\u0E38\\\\u0E14\\015\\012FX_STK_MTD_REVEIVE_ITEM_NEW_LOT_TO_STOCK=\\\\u0E40\\\\u0E1E\\\\u0E34\\\\u0E48\\\\u0E21 Lot \\\\u0E43\\\\u0E2B\\\\u0E21\\\\u0E48\\015\\012RP_PRINT_DISPENSE_DRUG_GROUP_BY_PLAN_GROUP=\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E43\\\\u0E0A\\\\u0E49\\\\u0E22\\\\u0E32\\\\u0E41\\\\u0E22\\\\u0E01\\\\u0E15\\\\u0E32\\\\u0E21\\\\u0E01\\\\u0E25\\\\u0E38\\\\u0E48\\\\u0E21\\\\u0E2A\\\\u0E34\\\\u0E17\\\\u0E18\\\\u0E34\\015\\012FX_STK_MTD_RECEIVE_DISTRIBUTOR_TO_STOCK=\\\\u0E0B\\\\u0E37\\\\u0E49\\\\u0E2D\\015\\012FIX_IS_INSERT_TO_ORDER=\\\\u0E40\\\\u0E1E\\\\u0E34\\\\u0E48\\\\u0E21\\\\u0E40\\\\u0E2D\\\\u0E07\\015\\012FX_STK_MTD_DISPENSE_STOCK_TO_OTHER=\\\\u0E1B\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E25\\\\u0E14\\\\u0E40\\\\u0E2B\\\\u0E15\\\\u0E38\\\\u0E1C\\\\u0E25\\\\u0E2D\\\\u0E37\\\\u0E48\\\\u0E19 \\\\u0E46\\015\\012FX_STK_MTD_DISPENSE_STOCK_TO_PRODUCE=\\\\u0E19\\\\u0E33\\\\u0E44\\\\u0E1B\\\\u0E1C\\\\u0E25\\\\u0E34\\\\u0E15\\015\\012RP_PRINT_LIST_STOCK_EXCHANGE=\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E41\\\\u0E25\\\\u0E01\\\\u0E40\\\\u0E1B\\\\u0E25\\\\u0E35\\\\u0E48\\\\u0E22\\\\u0E19\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\015\\012RP_PRINT_LIST_RECV_TO_STOCK_BY_ORDER=\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\\\u0E40\\\\u0E02\\\\u0E49\\\\u0E32\\\\u0E04\\\\u0E25\\\\u0E31\\\\u0E07\\\\u0E08\\\\u0E32\\\\u0E01\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E02\\\\u0E2D\\\\u0E0B\\\\u0E37\\\\u0E49\\\\u0E2D\\015\\012FX_STK_MTD_DISPENSE_DISINFECTIONED_TO_STOCK=\\\\u0E2A\\\\u0E48\\\\u0E07 Disinfection\\015\\012ACT_ITEM_STK_USE=\\\\u0E43\\\\u0E0A\\\\u0E49\\\\u0E43\\\\u0E19\\\\u0E2B\\\\u0E19\\\\u0E48\\\\u0E27\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\015\\012RP_PRINT_LIST_RECV_TO_STOCK_BY_ORDER_GROUP_BY_DISTRIBUTOR_ID_SHORT=\\\\u0E2A\\\\u0E23\\\\u0E38\\\\u0E1B\\\\u0E22\\\\u0E2D\\\\u0E14\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E40\\\\u0E02\\\\u0E49\\\\u0E32\\\\u0E08\\\\u0E32\\\\u0E01\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E0B\\\\u0E37\\\\u0E49\\\\u0E2D\\015\\012RP_PRINT_LIST_CANCEL_REQUEST=\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\\\u0E43\\\\u0E1A\\\\u0E40\\\\u0E1A\\\\u0E34\\\\u0E01\\\\u0E17\\\\u0E35\\\\u0E48\\\\u0E21\\\\u0E35\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E22\\\\u0E01\\\\u0E40\\\\u0E25\\\\u0E34\\\\u0E01\\015\\012DOCUMENT_CANCEL_DESCRIPTION=\\\\u0E22\\\\u0E01\\\\u0E40\\\\u0E25\\\\u0E34\\\\u0E01\\015\\012FX_STOCK_CSSD_PRODUCE_PREFIX=P\\015\\012FX_STOCK_RETURN_PREFIX=RT\\015\\012FX_STOCK_REQUEST_FROM_STOCK_ORDER_PREFIX=O\\015\\012RP_PRINT_LIST_ITEM_CHANGE_ITEM_PRICE=\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E1B\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E40\\\\u0E1B\\\\u0E25\\\\u0E35\\\\u0E48\\\\u0E22\\\\u0E19\\\\u0E23\\\\u0E32\\\\u0E04\\\\u0E32\\\\u0E02\\\\u0E32\\\\u0E22\\015\\012NO_LOT_NUMBER_DISPLAY=Default\\015\\012FX_STK_MTD_DISPENSE_STOCK_TO_STOCK_BY_BORROW=\\\\u0E08\\\\u0E48\\\\u0E32\\\\u0E22\\\\u0E08\\\\u0E32\\\\u0E01\\\\u0E22\\\\u0E37\\\\u0E21\\015\\012DOCUMENT_ORDER_RECEIVE_COMPLETE_DESCRIPTION=\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E40\\\\u0E02\\\\u0E49\\\\u0E32\\\\u0E2A\\\\u0E21\\\\u0E1A\\\\u0E39\\\\u0E23\\\\u0E13\\\\u0E4C - \\\\u0E17\\\\u0E38\\\\u0E01\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E01\\\\u0E32\\\\u0E23\\015\\012RP_PRINT_DISPENSE_DRUG_GROUP_BY_PLAN_GROUP_SHORT=\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E43\\\\u0E0A\\\\u0E49\\\\u0E22\\\\u0E32\\\\u0E41\\\\u0E22\\\\u0E01\\\\u0E15\\\\u0E32\\\\u0E21\\\\u0E01\\\\u0E25\\\\u0E38\\\\u0E48\\\\u0E21\\\\u0E2A\\\\u0E34\\\\u0E17\\\\u0E18\\\\u0E34\\015\\012RP_PRINT_LIST_ADD_ORDER_EMERGENCY_SHORT=\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E40\\\\u0E1E\\\\u0E34\\\\u0E48\\\\u0E21\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\\\u0E09\\\\u0E38\\\\u0E01\\\\u0E40\\\\u0E09\\\\u0E34\\\\u0E19\\015\\012DOCUMENT_ORDER_IN_PROCESS_DESCRIPTION=\\\\u0E43\\\\u0E1A\\\\u0E40\\\\u0E2A\\\\u0E19\\\\u0E2D\\\\u0E0B\\\\u0E37\\\\u0E49\\\\u0E2D/\\\\u0E08\\\\u0E49\\\\u0E32\\\\u0E07\\015\\012BTN_PRINT_REPORT_BY_DEPARTMENT_CONFIRM=\\\\u0E15\\\\u0E49\\\\u0E2D\\\\u0E07\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E1E\\\\u0E34\\\\u0E21\\\\u0E1E\\\\u0E4C\\\\u0E43\\\\u0E1A\\\\u0E41\\\\u0E22\\\\u0E01\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E25\\\\u0E30\\\\u0E40\\\\u0E2D\\\\u0E35\\\\u0E22\\\\u0E14 \\\\u0E41\\\\u0E22\\\\u0E01\\\\u0E15\\\\u0E32\\\\u0E21\\\\u0E2B\\\\u0E19\\\\u0E48\\\\u0E27\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\\\u0E14\\\\u0E49\\\\u0E27\\\\u0E22\\\\u0E2B\\\\u0E23\\\\u0E37\\\\u0E2D\\\\u0E44\\\\u0E21\\\\u0E48?\\015\\012RP_PRINT_LIST_RECV_TO_STOCK_BY_ORDER_SHORT=\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\\\u0E40\\\\u0E02\\\\u0E49\\\\u0E32\\\\u0E04\\\\u0E25\\\\u0E31\\\\u0E07\\\\u0E08\\\\u0E32\\\\u0E01\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E02\\\\u0E2D\\\\u0E0B\\\\u0E37\\\\u0E49\\\\u0E2D\\015\\012FX_STOCK_OFFER_PREFIX=B\\015\\012RP_PRINT_DISP_FROM_STOCK_GROUP_BY_STOCK_ID_SHORT=\\\\u0E2A\\\\u0E23\\\\u0E38\\\\u0E1B\\\\u0E22\\\\u0E2D\\\\u0E14\\\\u0E02\\\\u0E32\\\\u0E22\\\\u0E1B\\\\u0E23\\\\u0E30\\\\u0E08\\\\u0E33\\\\u0E07\\\\u0E27\\\\u0E14\\\\u0E41\\\\u0E22\\\\u0E01\\\\u0E15\\\\u0E32\\\\u0E21\\\\u0E25\\\\u0E39\\\\u0E01\\\\u0E04\\\\u0E49\\\\u0E32\\015\\012FX_STK_MTD_DISPENSE_STOCK_TO_STOCK_FOR_USE=\\\\u0E43\\\\u0E0A\\\\u0E49\\\\u0E43\\\\u0E19\\\\u0E2B\\\\u0E19\\\\u0E48\\\\u0E27\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\015\\012FX_STK_MTD_RECEIVE_STOCK_TO_STOCK_BY_TRANSFER=\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E42\\\\u0E2D\\\\u0E19\\015\\012RP_PRINT_SUMMARY_CUR_VALUE_GROUP_BY_STOCK_ID_SHORT=\\\\u0E2A\\\\u0E23\\\\u0E38\\\\u0E1B\\\\u0E21\\\\u0E39\\\\u0E25\\\\u0E04\\\\u0E48\\\\u0E32\\\\u0E02\\\\u0E2D\\\\u0E07\\\\u0E41\\\\u0E15\\\\u0E48\\\\u0E25\\\\u0E30\\\\u0E04\\\\u0E25\\\\u0E31\\\\u0E07\\015\\012FX_STK_MTD_DISPENSE_STOCK_TO_STOCK_BY_EXCHANGE=\\\\u0E41\\\\u0E25\\\\u0E01\\015\\012RP_PRINT_LIST_BIDDING_ITEM=\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\\\u0E1B\\\\u0E23\\\\u0E30\\\\u0E21\\\\u0E39\\\\u0E25\\015\\012RP_PRINT_LIST_NEARLY_EXPIRE_IN_STOCK=\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\\\u0E43\\\\u0E01\\\\u0E25\\\\u0E49\\\\u0E2B\\\\u0E21\\\\u0E14\\\\u0E2D\\\\u0E32\\\\u0E22\\\\u0E38\\015\\012FX_STOCK_DISPENSE_TO_PRODUCE_PREFIX=DP\\015\\012RP_PRINT_LIST_CANCEL_REQUEST_SHORT=\\\\u0E43\\\\u0E1A\\\\u0E40\\\\u0E1A\\\\u0E34\\\\u0E01\\\\u0E17\\\\u0E35\\\\u0E48\\\\u0E21\\\\u0E35\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E22\\\\u0E01\\\\u0E40\\\\u0E25\\\\u0E34\\\\u0E01\\015\\012FX_STOCK_FAX_DESCRIPTION=Fax\\015\\012RP_PRINT_LIST_SUMMARY_VALUE_IN_STOCK_SHORT=\\\\u0E2A\\\\u0E23\\\\u0E38\\\\u0E1B\\\\u0E21\\\\u0E39\\\\u0E25\\\\u0E04\\\\u0E48\\\\u0E32\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\\\u0E04\\\\u0E07\\\\u0E04\\\\u0E25\\\\u0E31\\\\u0E07\\015\\012FX_STK_MTD_DISPENSE_STOCK_TO_STOCK_FOR_SALE=\\\\u0E08\\\\u0E48\\\\u0E32\\\\u0E22\\\\u0E15\\\\u0E32\\\\u0E21\\\\u0E43\\\\u0E1A\\\\u0E40\\\\u0E1A\\\\u0E34\\\\u0E01\\015\\012FX_STOCK_PR_DESCRIPTION=\\\\u0E02\\\\u0E2D\\\\u0E0B\\\\u0E37\\\\u0E49\\\\u0E2D\\015\\012DOCUMENT_ORDER_RECEIVE_NOT_YET_DESCRIPTION=\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E40\\\\u0E02\\\\u0E49\\\\u0E32 - \\\\u0E1A\\\\u0E32\\\\u0E07\\\\u0E2A\\\\u0E48\\\\u0E27\\\\u0E19\\015\\012FX_STK_MTD_CANCEL=\\\\u0E22\\\\u0E01\\\\u0E40\\\\u0E25\\\\u0E34\\\\u0E01\\015\\012FX_STK_MTD_RECEIVE_BORROW_TO_STOCK=\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E08\\\\u0E32\\\\u0E01\\\\u0E22\\\\u0E37\\\\u0E21\\015\\012FX_STOCK_ORDER_TO_REQUEST_PREFIX=GR\\015\\012RP_PRINT_LIST_STOCK_EXCHANGE_SHORT=\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E41\\\\u0E25\\\\u0E01\\\\u0E40\\\\u0E1B\\\\u0E25\\\\u0E35\\\\u0E48\\\\u0E22\\\\u0E19\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\015\\012RP_PRINT_LIST_ADJUST_IN_STOCK=\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E1B\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E22\\\\u0E2D\\\\u0E14\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\015\\012FX_STOCK_PO_DESCRIPTION=\\\\u0E2A\\\\u0E31\\\\u0E48\\\\u0E07\\\\u0E0B\\\\u0E37\\\\u0E49\\\\u0E2D\\015\\012FX_BUDGET_NUMBER=\\\\u0E40\\\\u0E25\\\\u0E02\\\\u0E17\\\\u0E35\\\\u0E48\\\\u0E07\\\\u0E1A\\\\u0E1B\\\\u0E23\\\\u0E30\\\\u0E21\\\\u0E32\\\\u0E13\\015\\012ACT_ITEM_STK_SAVE=\\\\u0E1A\\\\u0E31\\\\u0E19\\\\u0E17\\\\u0E36\\\\u0E01\\015\\012RP_PRINT_SUMMARY_CUR_VALUE_GROUP_BY_STOCK_ID=\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\\\u0E2A\\\\u0E23\\\\u0E38\\\\u0E1B\\\\u0E21\\\\u0E39\\\\u0E25\\\\u0E04\\\\u0E48\\\\u0E32\\\\u0E02\\\\u0E2D\\\\u0E07\\\\u0E41\\\\u0E15\\\\u0E48\\\\u0E25\\\\u0E30\\\\u0E04\\\\u0E25\\\\u0E31\\\\u0E07\\015\\012FX_STOCK_PRODUCE_DESCRIPTION=\\\\u0E02\\\\u0E2D\\\\u0E1C\\\\u0E25\\\\u0E34\\\\u0E15\\015\\012ACT_ITEM_STK_REPACK=\\\\u0E41\\\\u0E1A\\\\u0E48\\\\u0E07\\\\u0E43\\\\u0E0A\\\\u0E49/\\\\u0E1A\\\\u0E23\\\\u0E23\\\\u0E08\\\\u0E38\\015\\012FX_BALANCE_QTY_IN_STOCK=On Hand\\015\\012FX_STOCK_EXCHANGE_PREFIX=EX\\015\\012RP_PRINT_LIST_REQUEST_DETAIL_RECORD=\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E40\\\\u0E1A\\\\u0E34\\\\u0E01/\\\\u0E08\\\\u0E48\\\\u0E32\\\\u0E22\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\\\u0E15\\\\u0E32\\\\u0E21\\\\u0E2B\\\\u0E19\\\\u0E48\\\\u0E27\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\015\\012FIX_PRINT_RECEIVE_ITEM=\\\\u0E1E\\\\u0E34\\\\u0E21\\\\u0E1E\\\\u0E4C\\\\u0E43\\\\u0E1A\\\\u0E15\\\\u0E23\\\\u0E27\\\\u0E08\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\015\\012BTN_PRINT_REPORT_BY_DEPARTMENT=\\\\u0E1E\\\\u0E34\\\\u0E21\\\\u0E1E\\\\u0E4C\\\\u0E2A\\\\u0E23\\\\u0E38\\\\u0E1B\\\\u0E21\\\\u0E39\\\\u0E25\\\\u0E04\\\\u0E48\\\\u0E32\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E43\\\\u0E0A\\\\u0E49\\\\u0E08\\\\u0E48\\\\u0E32\\\\u0E22\\\\u0E41\\\\u0E22\\\\u0E01\\\\u0E15\\\\u0E32\\\\u0E21\\\\u0E2B\\\\u0E19\\\\u0E48\\\\u0E27\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\015\\012DOCUMENT_ORDER_RECEIVE_NOT_COMPLETE_DESCRIPTION=\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E40\\\\u0E02\\\\u0E49\\\\u0E32\\\\u0E2A\\\\u0E21\\\\u0E1A\\\\u0E39\\\\u0E23\\\\u0E13\\\\u0E4C - \\\\u0E1A\\\\u0E32\\\\u0E07\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E01\\\\u0E32\\\\u0E23\\015\\012RP_PRINT_ITEM_OVER_MAX_IN_STOCK=\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\\\u0E04\\\\u0E07\\\\u0E04\\\\u0E25\\\\u0E31\\\\u0E07\\\\u0E17\\\\u0E35\\\\u0E48\\\\u0E21\\\\u0E32\\\\u0E01\\\\u0E01\\\\u0E27\\\\u0E48\\\\u0E32\\\\u0E01\\\\u0E33\\\\u0E2B\\\\u0E19\\\\u0E14\\\\u0E04\\\\u0E48\\\\u0E32\\\\u0E2A\\\\u0E39\\\\u0E07\\\\u0E2A\\\\u0E38\\\\u0E14\\015\\012FX_STK_MTD_REVEIVE_WASH_TO_STOCK=\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E08\\\\u0E32\\\\u0E01\\\\u0E0B\\\\u0E31\\\\u0E01\\\\u0E1F\\\\u0E2D\\\\u0E01\\015\\012RP_PRINT_LIST_BIDDING_ITEM_SHORT=\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\\\u0E1B\\\\u0E23\\\\u0E30\\\\u0E21\\\\u0E39\\\\u0E25\\015\\012DOCUMENT_ORDER_NOT_RECEIVE_DESCRIPTION=\\\\u0E44\\\\u0E21\\\\u0E48\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E40\\\\u0E02\\\\u0E49\\\\u0E32\\015\\012FX_STOCK_RECEIVE_OTHER_PREFIX=RO\\015\\012FX_STOCK_USE_IN_STOCK_PREFIX=CO\\015\\012ACT_ITEM_STK_ADJUST=\\\\u0E1B\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E22\\\\u0E2D\\\\u0E14\\015\\012RP_PRINT_SUMMARY_RECV_AND_DISP_ITEM_IN_STOCK_SHORT=\\\\u0E2A\\\\u0E23\\\\u0E38\\\\u0E1B\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\\\u0E41\\\\u0E25\\\\u0E30\\\\u0E40\\\\u0E1A\\\\u0E34\\\\u0E01\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\015\\012FX_STK_MTD_RECEIVE_RETURN_ITEM_USE_IN_STOCK=\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E04\\\\u0E37\\\\u0E19\\\\u0E08\\\\u0E32\\\\u0E01\\\\u0E2B\\\\u0E19\\\\u0E48\\\\u0E27\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\015\\012RP_PRINT_LIST_ITEM_TRANSPORT_LATE=\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E2A\\\\u0E48\\\\u0E07\\\\u0E02\\\\u0E2D\\\\u0E07\\\\u0E0A\\\\u0E49\\\\u0E32\\015\\012FX_STK_MTD_ADJUST_TO_DECREASE=\\\\u0E1B\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E25\\\\u0E14\\015\\012RP_PRINT_LIST_REQUEST_DETAIL_RECORD_SHORT=\\\\u0E40\\\\u0E1A\\\\u0E34\\\\u0E01/\\\\u0E08\\\\u0E48\\\\u0E32\\\\u0E22\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\\\u0E15\\\\u0E32\\\\u0E21\\\\u0E2B\\\\u0E19\\\\u0E48\\\\u0E27\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\015\\012FX_STK_MTD_DISPENSE_STOCK_TO_DAMAGE=\\\\u0E40\\\\u0E2A\\\\u0E35\\\\u0E22\\\\u0E2B\\\\u0E32\\\\u0E22\\015\\012RP_PRINT_DISP_FROM_STOCK_GROUP_BY_STOCK_ID=\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\\\u0E2A\\\\u0E23\\\\u0E38\\\\u0E1B\\\\u0E22\\\\u0E2D\\\\u0E14\\\\u0E02\\\\u0E32\\\\u0E22\\\\u0E1B\\\\u0E23\\\\u0E30\\\\u0E08\\\\u0E33\\\\u0E07\\\\u0E27\\\\u0E14\\\\u0E41\\\\u0E22\\\\u0E01\\\\u0E15\\\\u0E32\\\\u0E21\\\\u0E25\\\\u0E39\\\\u0E01\\\\u0E04\\\\u0E49\\\\u0E32\\015\\012RP_PRINT_LIST_ITEM_TRANSPORT_LATE_SHORT=\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E2A\\\\u0E48\\\\u0E07\\\\u0E02\\\\u0E2D\\\\u0E07\\\\u0E0A\\\\u0E49\\\\u0E32\\015\\012FX_STK_MTD_RECEIVE_DISTRIBUTOR_TO_STOCK_BY_FREE=\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E41\\\\u0E16\\\\u0E21\\015\\012RP_PRINT_LIST_RECEIVE_TO_STOCK_BY_OTHER_METHOD_SHORT=\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\\\u0E40\\\\u0E02\\\\u0E49\\\\u0E32\\\\u0E04\\\\u0E25\\\\u0E31\\\\u0E07\\\\u0E14\\\\u0E49\\\\u0E27\\\\u0E22\\\\u0E27\\\\u0E34\\\\u0E18\\\\u0E35\\\\u0E2D\\\\u0E37\\\\u0E48\\\\u0E19\\\\u0E46\\015\\012FX_STOCK_TRANSFER_PREFIX=T\\015\\012FX_TYPE_IS_STOCK=\\\\u0E04\\\\u0E25\\\\u0E31\\\\u0E07\\015\\012RP_PRINT_LIST_SUMMARY_USE_DETAIL_RECORD_SHORT=\\\\u0E2A\\\\u0E23\\\\u0E38\\\\u0E1B\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E43\\\\u0E0A\\\\u0E49\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\\\u0E02\\\\u0E2D\\\\u0E07\\\\u0E41\\\\u0E15\\\\u0E48\\\\u0E25\\\\u0E30\\\\u0E2B\\\\u0E19\\\\u0E48\\\\u0E27\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\015\\012FX_STK_MTD_REVEIVE_EO_TO_STOCK=\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E08\\\\u0E32\\\\u0E01 EO\\015\\012RP_PRINT_LIST_TRANSFER_DETAIL_RECORD=\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E42\\\\u0E2D\\\\u0E19/\\\\u0E08\\\\u0E48\\\\u0E32\\\\u0E22\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\\\u0E15\\\\u0E32\\\\u0E21\\\\u0E2B\\\\u0E19\\\\u0E48\\\\u0E27\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\015\\012FX_ON_HAND_QTY=Total Stock\\015\\012FX_STOCK_BORROW_PREFIX=BR\\015\\012FX_STOCK_DRUG_AND_SUPPLY=\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\015\\012RP_PRINT_LIST_ITEM_USE_RATE_IN_STOCK_SHORT=\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E0A\\\\u0E37\\\\u0E48\\\\u0E2D\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\\\u0E43\\\\u0E19\\\\u0E04\\\\u0E25\\\\u0E31\\\\u0E07\\015\\012RP_PRINT_SUMMARY_RECV_AND_DISP_ITEM_IN_STOCK=\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\\\u0E2A\\\\u0E23\\\\u0E38\\\\u0E1B\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\\\u0E41\\\\u0E25\\\\u0E30\\\\u0E40\\\\u0E1A\\\\u0E34\\\\u0E01\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\015\\012FX_STK_MTD_DISPENSE_STOCK_TO_PATIENT=\\\\u0E02\\\\u0E32\\\\u0E22\\\\u0E43\\\\u0E2B\\\\u0E49\\\\u0E1C\\\\u0E39\\\\u0E49\\\\u0E1B\\\\u0E48\\\\u0E27\\\\u0E22\\015\\012FX_STOCK_PRODUCE_PREFIX=P\\015\\012FX_STK_MTD_DISPENSE_DRIED_TO_STOCK=\\\\u0E2A\\\\u0E48\\\\u0E07\\\\u0E40\\\\u0E1B\\\\u0E48\\\\u0E32 \\\\u0E2D\\\\u0E1A\\\\u0E41\\\\u0E2B\\\\u0E49\\\\u0E07\\015\\012RP_PRINT_LIST_RECV_TO_STOCK_BY_ORDER_GROUP_BY_DISTRIBUTOR_ID=\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\\\u0E2A\\\\u0E23\\\\u0E38\\\\u0E1B\\\\u0E22\\\\u0E2D\\\\u0E14\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E40\\\\u0E02\\\\u0E49\\\\u0E32\\\\u0E08\\\\u0E32\\\\u0E01\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E0B\\\\u0E37\\\\u0E49\\\\u0E2D\\015\\012RP_PRINT_LIST_ORDER_DETAIL_RECORD_SHORT=\\\\u0E1A\\\\u0E31\\\\u0E19\\\\u0E17\\\\u0E36\\\\u0E01\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E25\\\\u0E30\\\\u0E40\\\\u0E2D\\\\u0E35\\\\u0E22\\\\u0E14\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E02\\\\u0E2D\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\015\\012RP_PRINT_LIST_REQUEST_ORDER=\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E02\\\\u0E2D\\\\u0E08\\\\u0E31\\\\u0E14\\\\u0E0B\\\\u0E37\\\\u0E49\\\\u0E2D\\015\\012ACT_ITEM_STK_MANU=\\\\u0E43\\\\u0E0A\\\\u0E49\\\\u0E40\\\\u0E1E\\\\u0E37\\\\u0E48\\\\u0E2D\\\\u0E1C\\\\u0E25\\\\u0E34\\\\u0E15\\015\\012RP_PRINT_LIST_WORTHLESS_QTY_IN_STOCK_SHORT=\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\\\u0E2B\\\\u0E21\\\\u0E14\\015\\012FX_STK_MTD_DISPENSE_STREAM_TO_STOCK=\\\\u0E2A\\\\u0E48\\\\u0E07 Stream\\015\\012FX_STK_MTD_REVEIVE_STREAM_TO_STOCK=\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E08\\\\u0E32\\\\u0E01 Stream\\015\\012IS_TO_PERIOD=\\\\u0E1A\\\\u0E31\\\\u0E19\\\\u0E17\\\\u0E36\\\\u0E01\\\\u0E02\\\\u0E49\\\\u0E2D\\\\u0E21\\\\u0E39\\\\u0E25\\\\u0E04\\\\u0E07\\\\u0E04\\\\u0E25\\\\u0E31\\\\u0E07\\\\u0E40\\\\u0E1B\\\\u0E47\\\\u0E19\\\\u0E23\\\\u0E2D\\\\u0E1A\\\\u0E40\\\\u0E14\\\\u0E37\\\\u0E2D\\\\u0E19\\015\\012FX_STOCK_REQUEST_PREFIX=R\\015\\012RP_PRINT_STOCK_MASTER_LIST_SHORT=Stock Master List\\015\\012RP_PRINT_LIST_SUMMARY_USE_DETAIL_RECORD=\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\\\u0E2A\\\\u0E23\\\\u0E38\\\\u0E1B\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E43\\\\u0E0A\\\\u0E49\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\\\u0E02\\\\u0E2D\\\\u0E07\\\\u0E41\\\\u0E15\\\\u0E48\\\\u0E25\\\\u0E30\\\\u0E2B\\\\u0E19\\\\u0E48\\\\u0E27\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\015\\012FX_STK_MTD_RECEIVE_STOCK_TO_STOCK_FOR_SALE=\\\\u0E40\\\\u0E1A\\\\u0E34\\\\u0E01\\015\\012RP_PRINT_LIST_STOCK_ORDER_HISTORY=\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\\\u0E1B\\\\u0E23\\\\u0E30\\\\u0E27\\\\u0E31\\\\u0E15\\\\u0E34\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E2A\\\\u0E31\\\\u0E48\\\\u0E07\\\\u0E0B\\\\u0E37\\\\u0E49\\\\u0E2D\\\\u0E2A\\\\u0E34\\\\u0E19\\\\u0E04\\\\u0E49\\\\u0E32\\015\\012', 'admin', '2011-09-09', '08:57:53');
INSERT INTO imed.imed_language VALUES ('languageReport.properties', '#Fri Sep 09 09:45:04 GMT 2011\\015\\012BasePaidMethod=\\\\u0E27\\\\u0E34\\\\u0E18\\\\u0E35\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E0A\\\\u0E33\\\\u0E23\\\\u0E30\\015\\012ServicePointIdXray=\\\\u0E08\\\\u0E38\\\\u0E14\\\\u0E1A\\\\u0E23\\\\u0E34\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E2B\\\\u0E49\\\\u0E2D\\\\u0E07 Xray\\015\\012BaseSiteBranch=\\\\u0E2A\\\\u0E32\\\\u0E02\\\\u0E32\\015\\012ReceiptType=\\\\u0E1B\\\\u0E23\\\\u0E30\\\\u0E40\\\\u0E20\\\\u0E17\\\\u0E43\\\\u0E1A\\\\u0E40\\\\u0E2A\\\\u0E23\\\\u0E47\\\\u0E08\\015\\012Refer=Refer\\015\\012BeginTime=\\\\u0E40\\\\u0E27\\\\u0E25\\\\u0E32\\\\u0E40\\\\u0E23\\\\u0E34\\\\u0E48\\\\u0E21\\\\u0E15\\\\u0E49\\\\u0E19\\015\\012Tariff=\\\\u0E2D\\\\u0E31\\\\u0E15\\\\u0E23\\\\u0E32\\\\u0E23\\\\u0E32\\\\u0E04\\\\u0E32\\015\\012TypeItemLab=\\\\u0E1B\\\\u0E23\\\\u0E30\\\\u0E40\\\\u0E20\\\\u0E17\\015\\012BeginDate=\\\\u0E27\\\\u0E31\\\\u0E19\\\\u0E17\\\\u0E35\\\\u0E48\\\\u0E40\\\\u0E23\\\\u0E34\\\\u0E48\\\\u0E21\\\\u0E15\\\\u0E49\\\\u0E19\\015\\012ItemType=\\\\u0E1B\\\\u0E23\\\\u0E30\\\\u0E40\\\\u0E20\\\\u0E17 Item\\015\\012EmployeeCash=\\\\u0E40\\\\u0E08\\\\u0E49\\\\u0E32\\\\u0E2B\\\\u0E19\\\\u0E49\\\\u0E32\\\\u0E17\\\\u0E35\\\\u0E48\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E40\\\\u0E07\\\\u0E34\\\\u0E19\\015\\012ServicePointIdLab=\\\\u0E08\\\\u0E38\\\\u0E14\\\\u0E1A\\\\u0E23\\\\u0E34\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E2B\\\\u0E49\\\\u0E2D\\\\u0E07 Lab\\015\\012Icd10Code=\\\\u0E23\\\\u0E2B\\\\u0E31\\\\u0E2A ICD10\\015\\012BaseClinicId=\\\\u0E04\\\\u0E25\\\\u0E35\\\\u0E19\\\\u0E34\\\\u0E04\\015\\012Limit=Limit\\015\\012Ward=Ward\\015\\012ItemCode=ItemCode\\015\\012EndTime=\\\\u0E40\\\\u0E27\\\\u0E25\\\\u0E32\\\\u0E2A\\\\u0E34\\\\u0E49\\\\u0E19\\\\u0E2A\\\\u0E38\\\\u0E14\\015\\012CategoryReceipt=\\\\u0E01\\\\u0E25\\\\u0E38\\\\u0E48\\\\u0E21\\\\u0E43\\\\u0E1A\\\\u0E40\\\\u0E2A\\\\u0E23\\\\u0E47\\\\u0E08\\015\\012ItemXray=\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E01\\\\u0E32\\\\u0E23 Xray\\015\\012EndDate=\\\\u0E27\\\\u0E31\\\\u0E19\\\\u0E17\\\\u0E35\\\\u0E48\\\\u0E2A\\\\u0E34\\\\u0E49\\\\u0E19\\\\u0E2A\\\\u0E38\\\\u0E14\\015\\012SpAll=SpAll\\015\\012BaseDepartment=\\\\u0E41\\\\u0E1C\\\\u0E19\\\\u0E01\\015\\012DeathStatus=\\\\u0E1B\\\\u0E23\\\\u0E30\\\\u0E40\\\\u0E20\\\\u0E17\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E40\\\\u0E2A\\\\u0E35\\\\u0E22\\\\u0E0A\\\\u0E35\\\\u0E27\\\\u0E34\\\\u0E15\\015\\012EmpCash=EmpCash\\015\\012SearchTime=\\\\u0E40\\\\u0E27\\\\u0E25\\\\u0E32\\015\\012Hospital=\\\\u0E42\\\\u0E23\\\\u0E07\\\\u0E1E\\\\u0E22\\\\u0E32\\\\u0E1A\\\\u0E32\\\\u0E25\\015\\012Doctor=\\\\u0E41\\\\u0E1E\\\\u0E17\\\\u0E22\\\\u0E4C\\015\\012VisitTypeId=\\\\u0E1B\\\\u0E23\\\\u0E30\\\\u0E40\\\\u0E20\\\\u0E17\\\\u0E1C\\\\u0E39\\\\u0E49\\\\u0E1B\\\\u0E48\\\\u0E27\\\\u0E22\\015\\012OrderInOut=\\\\u0E2A\\\\u0E31\\\\u0E48\\\\u0E07\\\\u0E08\\\\u0E32\\\\u0E01\\015\\012Plan=\\\\u0E2A\\\\u0E34\\\\u0E17\\\\u0E18\\\\u0E34\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E23\\\\u0E31\\\\u0E01\\\\u0E29\\\\u0E32\\015\\012TEST=\\\\u0E17\\\\u0E14\\\\u0E2A\\\\u0E2D\\\\u0E1A\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\\\u0E04\\\\u0E23\\\\u0E31\\\\u0E1A\\015\\012OutTime=\\\\u0E0A\\\\u0E48\\\\u0E27\\\\u0E07\\\\u0E40\\\\u0E27\\\\u0E25\\\\u0E32\\015\\012SearchDate=\\\\u0E27\\\\u0E31\\\\u0E19\\\\u0E17\\\\u0E35\\\\u0E48\\015\\012BaseLabTypeId=\\\\u0E1B\\\\u0E23\\\\u0E30\\\\u0E40\\\\u0E20\\\\u0E17 Lab\\015\\012DischargeType=\\\\u0E1B\\\\u0E23\\\\u0E30\\\\u0E40\\\\u0E20\\\\u0E17\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E08\\\\u0E33\\\\u0E2B\\\\u0E19\\\\u0E48\\\\u0E32\\\\u0E22\\015\\012OutHos=\\\\u0E2A\\\\u0E48\\\\u0E07\\\\u0E21\\\\u0E32\\\\u0E08\\\\u0E32\\\\u0E01\\015\\012Pathologist=\\\\u0E1E\\\\u0E22\\\\u0E32\\\\u0E18\\\\u0E34\\\\u0E41\\\\u0E1E\\\\u0E17\\\\u0E22\\\\u0E4C\\015\\012ItemLab=\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E01\\\\u0E32\\\\u0E23 Lab\\015\\012EmployeeName=\\\\u0E0A\\\\u0E37\\\\u0E48\\\\u0E2D\\\\u0E1E\\\\u0E19\\\\u0E31\\\\u0E01\\\\u0E07\\\\u0E32\\\\u0E19\\015\\012DynamicGroup=\\\\u0E08\\\\u0E31\\\\u0E14\\\\u0E01\\\\u0E25\\\\u0E38\\\\u0E48\\\\u0E21\\\\u0E15\\\\u0E32\\\\u0E21\\015\\012PlanGroup=\\\\u0E01\\\\u0E25\\\\u0E38\\\\u0E48\\\\u0E21\\\\u0E2A\\\\u0E34\\\\u0E17\\\\u0E18\\\\u0E34\\\\u0E4C\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E23\\\\u0E31\\\\u0E01\\\\u0E29\\\\u0E32\\015\\012WardId=\\\\u0E23\\\\u0E2B\\\\u0E31\\\\u0E2A Ward\\015\\012DoctorInOut=\\\\u0E41\\\\u0E1E\\\\u0E17\\\\u0E22\\\\u0E4C\\\\u0E19\\\\u0E2D\\\\u0E01\\\\u0E42\\\\u0E23\\\\u0E07\\\\u0E1E\\\\u0E22\\\\u0E32\\\\u0E1A\\\\u0E32\\\\u0E25\\015\\012Address=\\\\u0E17\\\\u0E35\\\\u0E48\\\\u0E2D\\\\u0E22\\\\u0E39\\\\u0E48\\015\\012ServicePointId=\\\\u0E08\\\\u0E38\\\\u0E14\\\\u0E1A\\\\u0E23\\\\u0E34\\\\u0E01\\\\u0E32\\\\u0E23\\015\\012VisitTypeID=\\\\u0E1B\\\\u0E23\\\\u0E30\\\\u0E40\\\\u0E20\\\\u0E17\\\\u0E1C\\\\u0E39\\\\u0E49\\\\u0E1B\\\\u0E48\\\\u0E27\\\\u0E22\\015\\012VisitPeriod=\\\\u0E0A\\\\u0E48\\\\u0E27\\\\u0E07\\\\u0E40\\\\u0E27\\\\u0E25\\\\u0E32\\015\\012VisitPaid=\\\\u0E22\\\\u0E2D\\\\u0E14\\\\u0E04\\\\u0E48\\\\u0E32\\\\u0E43\\\\u0E0A\\\\u0E49\\\\u0E08\\\\u0E48\\\\u0E32\\\\u0E22\\\\u0E21\\\\u0E32\\\\u0E01\\\\u0E01\\\\u0E27\\\\u0E48\\\\u0E32\\015\\012Icd9Code=\\\\u0E23\\\\u0E2B\\\\u0E31\\\\u0E2A ICD9\\015\\012Days=\\\\u0E08\\\\u0E33\\\\u0E19\\\\u0E27\\\\u0E19\\\\u0E27\\\\u0E31\\\\u0E19\\015\\012', 'admin', '2011-09-09', '09:45:04');
INSERT INTO imed.imed_language VALUES ('language.properties', '#Wed Nov 30 10:51:28 ICT 2011\\012CODE_PAY_INVOICE_OPD=OPI\\012ULTRASOUND=Ultrasound\\012DOCTOR_CLEAR_ORDER_ITEM_FORM=\\\\u0E25\\\\u0E49\\\\u0E32\\\\u0E07\\012RC_RECEIPT_ONLY_TEXT=\\\\u0E0A\\\\u0E33\\\\u0E23\\\\u0E30\\\\u0E40\\\\u0E07\\\\u0E34\\\\u0E19\\\\u0E42\\\\u0E14\\\\u0E22 (PAY BY)\\012VS_LBL_SEMI_URGENT_EMERGENCY=Semi-Urgent\\012NO_LOT_NUMBER_DISPLAY=Default\\012BTN_PRINT_FRONT_PAGE=\\\\u0E1E\\\\u0E34\\\\u0E21\\\\u0E1E\\\\u0E4C OPD \\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E4C\\\\u0E14\\012LBL_CLINIC=\\\\u0E04\\\\u0E25\\\\u0E34\\\\u0E19\\\\u0E34\\\\u0E01\\012PM_CASH_DESCRIPTION_EN=Cash\\012DISCOUNT_REASON_COMMAND_DESCRIPTION=\\\\u0E42\\\\u0E14\\\\u0E22\\\\u0E04\\\\u0E33\\\\u0E2A\\\\u0E31\\\\u0E48\\\\u0E07\\012LR_HBS_AG=HBs Ag\\012LBL_DRUG_DOSE_NOTE=Dose \\\\u0E17\\\\u0E35\\\\u0E48\\\\u0E08\\\\u0E48\\\\u0E32\\\\u0E22\\\\u0E1C\\\\u0E39\\\\u0E49\\\\u0E1B\\\\u0E48\\\\u0E27\\\\u0E22\\012RC_COVERPAGE_TITLE=\\\\u0E43\\\\u0E1A\\\\u0E2A\\\\u0E23\\\\u0E38\\\\u0E1B\\\\u0E2B\\\\u0E19\\\\u0E49\\\\u0E32\\\\u0E07\\\\u0E1A (STATEMENT)\\012PM_CREDIT_CARD_DESCRIPTION_EN=Credit Card\\012LR_SUGAR=Sugar\\012LBL_APGAR_STATE_THREE=10 MIN\\012EMPTY_NAME=\\\\u0E04\\\\u0E48\\\\u0E32\\\\u0E27\\\\u0E48\\\\u0E32\\\\u0E07\\012DISCOUNT_REASON_CASH_COMPANY_DESCRIPTION=\\\\u0E1A\\\\u0E23\\\\u0E34\\\\u0E29\\\\u0E31\\\\u0E17\\\\u0E40\\\\u0E07\\\\u0E34\\\\u0E19\\\\u0E2A\\\\u0E14\\012OTHER_ANES_TYPE=OTH\\012BTN_RESET=\\\\u0E25\\\\u0E49\\\\u0E32\\\\u0E07\\012TEST=\\\\u0E17\\\\u0E14\\\\u0E2A\\\\u0E2D\\\\u0E1A\\012PM_CHEQUE_DESCRIPTION_EN=Cheque\\012BTN_PRINT_APPOINT_LIST_CUSTOM=\\\\u0E1E\\\\u0E34\\\\u0E21\\\\u0E1E\\\\u0E4C\\\\u0E41\\\\u0E22\\\\u0E01\\\\u0E15\\\\u0E32\\\\u0E21\\\\u0E41\\\\u0E1C\\\\u0E19\\\\u0E01\\012LR_RH=Rh Group\\012MENU_PRINT_ADMISSION_SLIP=\\\\u0E1E\\\\u0E34\\\\u0E21\\\\u0E1E\\\\u0E4C Admission Slip\\012DISCOUNT_REASON_COMPANY_DESCRIPTION=\\\\u0E1A\\\\u0E23\\\\u0E34\\\\u0E29\\\\u0E31\\\\u0E17\\\\u0E01\\\\u0E23\\\\u0E21\\\\u0E41\\\\u0E23\\\\u0E07\\\\u0E07\\\\u0E32\\\\u0E19\\012RC_IPD_FAX_CLAIM_TITLE=\\\\u0E43\\\\u0E1A\\\\u0E41\\\\u0E08\\\\u0E49\\\\u0E07\\\\u0E04\\\\u0E48\\\\u0E32\\\\u0E23\\\\u0E31\\\\u0E01\\\\u0E29\\\\u0E32\\012CODE_PAY_INVOICE_IPD=IPI\\012DEFAULT_COUNTRY_ID=100\\012LBL_SEARCH_DRUG_DOSE=\\\\u0E04\\\\u0E49\\\\u0E19\\\\u0E2B\\\\u0E32\\012PM_OTHER_DESCRIPTION_EN=Other\\012RECEIPT_TYPE_CLAIM_DESC=\\\\u0E40\\\\u0E04\\\\u0E25\\\\u0E21\\012PLAN_TYPE_SPECIAL_CARD_CONDITION=\\\\u0E40\\\\u0E07\\\\u0E37\\\\u0E48\\\\u0E2D\\\\u0E19\\\\u0E44\\\\u0E02\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E43\\\\u0E0A\\\\u0E49\\\\u0E1A\\\\u0E31\\\\u0E15\\\\u0E23\\012RC_TITLE_DEPOSIT_PAYMENT=\\\\u0E43\\\\u0E1A\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E40\\\\u0E07\\\\u0E34\\\\u0E19\\\\u0E0A\\\\u0E31\\\\u0E48\\\\u0E27\\\\u0E04\\\\u0E23\\\\u0E32\\\\u0E27\\012MENU_PRINT_PATIENT_DETAIL=\\\\u0E1E\\\\u0E34\\\\u0E21\\\\u0E1E\\\\u0E4C\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E25\\\\u0E30\\\\u0E40\\\\u0E2D\\\\u0E35\\\\u0E22\\\\u0E14\\\\u0E1C\\\\u0E39\\\\u0E49\\\\u0E1B\\\\u0E48\\\\u0E27\\\\u0E22\\012LBL_BS=BS\\012RC_INVOICE_DEBT_TITLE=\\\\u0E43\\\\u0E1A\\\\u0E41\\\\u0E08\\\\u0E49\\\\u0E07\\\\u0E40\\\\u0E23\\\\u0E35\\\\u0E22\\\\u0E01\\\\u0E40\\\\u0E01\\\\u0E47\\\\u0E1A\\012PM_DEPOSIT_BANK_DESCRIPTION_TH=\\\\u0E40\\\\u0E07\\\\u0E34\\\\u0E19\\\\u0E42\\\\u0E2D\\\\u0E19\\012LR_HCT=Hct\\012LBL_CAN_NOT_READ_SECRET_DX_ICD10=\\\\u0E1B\\\\u0E01\\\\u0E1B\\\\u0E34\\\\u0E14\\012BTN_PRINT_VISIT_SCREEN_SLIP=\\\\u0E1E\\\\u0E34\\\\u0E21\\\\u0E1E\\\\u0E4C Visit Slip\\012EKG=EKG\\012MENU_PRINT_DC_PLAN=\\\\u0E1E\\\\u0E34\\\\u0E21\\\\u0E1E\\\\u0E4C Inpatient Summary\\012LBL_APGAR_STATE_TWO=5 MIN\\012XN=XN\\012LBL_DEPARTMENT=\\\\u0E41\\\\u0E1C\\\\u0E19\\\\u0E01\\012DOCTOR_LOGOUT_TIMEOUT=60\\012LBL_MAIN_SYMPTOM=\\\\u0E2D\\\\u0E32\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E2A\\\\u0E33\\\\u0E04\\\\u0E31\\\\u0E0D\\012MENU_PRINT_LABEL_ADULT=\\\\u0E1E\\\\u0E34\\\\u0E21\\\\u0E1E\\\\u0E4C Sticker \\\\u0E2B\\\\u0E49\\\\u0E2D\\\\u0E07\\\\u0E41\\\\u0E1F\\\\u0E49\\\\u0E21\\\\u0E40\\\\u0E27\\\\u0E0A\\\\u0E23\\\\u0E30\\\\u0E40\\\\u0E1A\\\\u0E35\\\\u0E22\\\\u0E19\\012MENU_LR_INFANT_DATA=\\\\u0E1E\\\\u0E34\\\\u0E21\\\\u0E1E\\\\u0E4C\\\\u0E43\\\\u0E1A\\\\u0E2A\\\\u0E23\\\\u0E38\\\\u0E1B\\\\u0E04\\\\u0E25\\\\u0E2D\\\\u0E14\\\\u0E17\\\\u0E32\\\\u0E23\\\\u0E01w\\012LR_PROTEIN=Protein\\012MENU_LR_DELIVERY_SUMMARY=\\\\u0E1E\\\\u0E34\\\\u0E21\\\\u0E1E\\\\u0E4C\\\\u0E43\\\\u0E1A\\\\u0E2A\\\\u0E23\\\\u0E38\\\\u0E1B\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E04\\\\u0E25\\\\u0E2D\\\\u0E14w\\012RC_TITLE_PAY_INVOICE=\\\\u0E43\\\\u0E1A\\\\u0E40\\\\u0E2A\\\\u0E23\\\\u0E47\\\\u0E08\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E40\\\\u0E07\\\\u0E34\\\\u0E19\\012DT_ITEM_SET_OTHER_NAME=Other\\012MENU_PRINT_PRESCRIPTION=\\\\u0E1E\\\\u0E34\\\\u0E21\\\\u0E1E\\\\u0E4C\\\\u0E43\\\\u0E1A\\\\u0E2A\\\\u0E31\\\\u0E48\\\\u0E07\\\\u0E22\\\\u0E32 OPD\\012DOCTOR_ALLOW=\\\\u0E41\\\\u0E1E\\\\u0E17\\\\u0E22\\\\u0E4C\\\\u0E2A\\\\u0E31\\\\u0E48\\\\u0E07\\\\u0E01\\\\u0E25\\\\u0E31\\\\u0E1A\\\\u0E1A\\\\u0E49\\\\u0E32\\\\u0E19\\012EN=CT\\012LBL_SECRET_LAB_RESULT=\\\\u0E41\\\\u0E2A\\\\u0E14\\\\u0E07\\\\u0E1C\\\\u0E25\\\\u0E15\\\\u0E23\\\\u0E27\\\\u0E08\\\\u0E41\\\\u0E25\\\\u0E1B\\\\u0E1B\\\\u0E01\\\\u0E1B\\\\u0E34\\\\u0E14\\012PM_FUND_DESCRIPTION_TH=\\\\u0E40\\\\u0E07\\\\u0E34\\\\u0E19\\\\u0E01\\\\u0E2D\\\\u0E07\\\\u0E17\\\\u0E38\\\\u0E19\\012DISCOUNT_REASON_CREDIT_COMPANY_CARD_DESCRIPTION=\\\\u0E1A\\\\u0E23\\\\u0E34\\\\u0E29\\\\u0E31\\\\u0E17\\\\u0E40\\\\u0E04\\\\u0E23\\\\u0E14\\\\u0E34\\\\u0E15\\012BTN_MAKE_PRESCRIPTION_NAME=\\\\u0E1E\\\\u0E34\\\\u0E21\\\\u0E1E\\\\u0E4C\\\\u0E09\\\\u0E25\\\\u0E32\\\\u0E01\\\\u0E22\\\\u0E32\\012SUBMENU_SERVICE_CANCEL_PATIENT=\\\\u0E22\\\\u0E01\\\\u0E40\\\\u0E25\\\\u0E34\\\\u0E01\\\\u0E1C\\\\u0E39\\\\u0E49\\\\u0E1B\\\\u0E48\\\\u0E27\\\\u0E22\\012RC_IPD_INVOICE_COST_TITLE=\\\\u0E43\\\\u0E1A\\\\u0E41\\\\u0E08\\\\u0E49\\\\u0E07\\\\u0E04\\\\u0E48\\\\u0E32\\\\u0E23\\\\u0E31\\\\u0E01\\\\u0E29\\\\u0E32\\012PM_DEPOSIT_DESCRIPTION_TH=\\\\u0E40\\\\u0E07\\\\u0E34\\\\u0E19\\\\u0E21\\\\u0E31\\\\u0E14\\\\u0E08\\\\u0E33\\012BTN_PREPARE_CANCEL_RECEIPT=\\\\u0E22\\\\u0E01\\\\u0E40\\\\u0E25\\\\u0E34\\\\u0E01\\\\u0E43\\\\u0E1A\\\\u0E40\\\\u0E2A\\\\u0E23\\\\u0E47\\\\u0E08\\012MENU_PRINT_OR_DOCTOR_FEE=\\\\u0E04\\\\u0E48\\\\u0E32\\\\u0E41\\\\u0E1E\\\\u0E17\\\\u0E22\\\\u0E4C\\\\u0E1C\\\\u0E48\\\\u0E32\\\\u0E15\\\\u0E31\\\\u0E14\\012LBL_NT_ALERT_CHANGE_ORDER=\\\\u0E15\\\\u0E49\\\\u0E2D\\\\u0E07\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E40\\\\u0E25\\\\u0E37\\\\u0E2D\\\\u0E01\\\\u0E2D\\\\u0E32\\\\u0E2B\\\\u0E32\\\\u0E23\\\\u0E40\\\\u0E2B\\\\u0E21\\\\u0E37\\\\u0E2D\\\\u0E19\\\\u0E21\\\\u0E37\\\\u0E49\\\\u0E2D\\\\u0E2D\\\\u0E37\\\\u0E48\\\\u0E19\\\\u0E2B\\\\u0E23\\\\u0E37\\\\u0E2D\\\\u0E44\\\\u0E21\\\\u0E48\\012BASE_ANY=\\\\u0E2D\\\\u0E32\\\\u0E2B\\\\u0E32\\\\u0E23\\012LIMIT_TIME_FOR_WAIT=15\\012LBL_CREATE_PATIENT_DUMMY=\\\\u0E40\\\\u0E1E\\\\u0E34\\\\u0E48\\\\u0E21\\\\u0E1C\\\\u0E39\\\\u0E49\\\\u0E1B\\\\u0E48\\\\u0E27\\\\u0E22\\\\u0E44\\\\u0E21\\\\u0E48\\\\u0E2A\\\\u0E23\\\\u0E49\\\\u0E32\\\\u0E07\\\\u0E41\\\\u0E1F\\\\u0E49\\\\u0E21\\012BTN_CANCEL_RECEIPT_IN_MENU=\\\\u0E22\\\\u0E37\\\\u0E19\\\\u0E22\\\\u0E31\\\\u0E19\\\\u0E22\\\\u0E01\\\\u0E40\\\\u0E25\\\\u0E34\\\\u0E01\\\\u0E43\\\\u0E1A\\\\u0E40\\\\u0E2A\\\\u0E23\\\\u0E47\\\\u0E08/\\\\u0E41\\\\u0E08\\\\u0E49\\\\u0E07\\\\u0E2B\\\\u0E19\\\\u0E35\\\\u0E49\\012MENU_PRINT_LABEL_CHILD=\\\\u0E1B\\\\u0E49\\\\u0E32\\\\u0E22\\\\u0E0A\\\\u0E37\\\\u0E48\\\\u0E2D\\\\u0E04\\\\u0E25\\\\u0E49\\\\u0E2D\\\\u0E07\\\\u0E41\\\\u0E02\\\\u0E19\\\\u0E2A\\\\u0E33\\\\u0E2B\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E40\\\\u0E14\\\\u0E47\\\\u0E01\\012LR_ANTI_HIV=ANTI HIV\\012MENU_PRINT_VISITSLIP=\\\\u0E1E\\\\u0E34\\\\u0E21\\\\u0E1E\\\\u0E4C\\\\u0E43\\\\u0E1A\\\\u0E2A\\\\u0E23\\\\u0E38\\\\u0E1B\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E23\\\\u0E31\\\\u0E01\\\\u0E29\\\\u0E32\\012RC_COPY_TEXT=\\\\u0E2A\\\\u0E33\\\\u0E40\\\\u0E19\\\\u0E32\\012RC_IPD_GENERAL_TITLE=\\\\u0E43\\\\u0E1A\\\\u0E40\\\\u0E2A\\\\u0E23\\\\u0E47\\\\u0E08\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E40\\\\u0E07\\\\u0E34\\\\u0E19 (RECEIPT)\\012DEFAULT_COUNTRY_NAME=\\\\u0E44\\\\u0E17\\\\u0E22\\012RC_OPD_GENERAL_TITLE=\\\\u0E43\\\\u0E1A\\\\u0E40\\\\u0E2A\\\\u0E23\\\\u0E47\\\\u0E08\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E40\\\\u0E07\\\\u0E34\\\\u0E19\\012BTN_CREATE_PATIENT_DUMMY=\\\\u0E1A\\\\u0E31\\\\u0E19\\\\u0E17\\\\u0E36\\\\u0E01(\\\\u0E44\\\\u0E21\\\\u0E48\\\\u0E2A\\\\u0E23\\\\u0E49\\\\u0E32\\\\u0E07HN)\\012LBL_VS_MAIN_SYMPTOM=\\\\u0E2D\\\\u0E32\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E2A\\\\u0E33\\\\u0E04\\\\u0E31\\\\u0E0D\\012PM_CASH_DESCRIPTION_TH=\\\\u0E40\\\\u0E07\\\\u0E34\\\\u0E19\\\\u0E2A\\\\u0E14\\012LBL_VISIT_PAYMENT_USED=\\\\u0E2A\\\\u0E34\\\\u0E17\\\\u0E18\\\\u0E34\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E23\\\\u0E31\\\\u0E01\\\\u0E29\\\\u0E32\\\\u0E17\\\\u0E35\\\\u0E48\\\\u0E2A\\\\u0E32\\\\u0E21\\\\u0E32\\\\u0E23\\\\u0E16\\\\u0E43\\\\u0E0A\\\\u0E49\\\\u0E44\\\\u0E14\\\\u0E49\\012PATIENTINFO_SPECIALCARE_SIGN=\\\\!\\012BTN_REPORT_ALL_TEST=\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\\\u0E1C\\\\u0E25\\\\u0E17\\\\u0E31\\\\u0E49\\\\u0E07\\\\u0E2B\\\\u0E21\\\\u0E14\\012VS_LBL_EMERGENCY_EMERGENCY=Emergency\\012LBL_SYMPTOM_HISTORY=\\\\u0E15\\\\u0E23\\\\u0E27\\\\u0E08\\\\u0E23\\\\u0E48\\\\u0E32\\\\u0E07\\\\u0E01\\\\u0E32\\\\u0E22\\012PM_DEPOSIT_BANK_DESCRIPTION_EN=Deposit Bank\\012LBL_VITALSIGN=\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E15\\\\u0E23\\\\u0E27\\\\u0E08\\\\u0E23\\\\u0E48\\\\u0E32\\\\u0E07\\\\u0E01\\\\u0E32\\\\u0E22\\012OLD_HN=HN \\\\u0E40\\\\u0E01\\\\u0E48\\\\u0E32\\012PM_CREDIT_CARD_DESCRIPTION_TH=\\\\u0E1A\\\\u0E31\\\\u0E15\\\\u0E23\\\\u0E40\\\\u0E04\\\\u0E23\\\\u0E14\\\\u0E34\\\\u0E15\\012LR_HBE_AG=HBe Ag\\012LR_ABO=ABO Group\\012LBL_PRINT_NO_QUANTITY=\\\\u0E1E\\\\u0E34\\\\u0E21\\\\u0E1E\\\\u0E4C\\\\u0E44\\\\u0E21\\\\u0E48\\\\u0E23\\\\u0E30\\\\u0E1A\\\\u0E38\\\\u0E08\\\\u0E33\\\\u0E19\\\\u0E27\\\\u0E19\\012LR_ANTI_HBS=Anti HBs\\012VS_LBL_CRITICAL_EMERGENCY=Critical\\012MENU_PRINT_OPDCARD=\\\\u0E1E\\\\u0E34\\\\u0E21\\\\u0E1E\\\\u0E4C OPD Card\\012XRAY=Xray\\012PM_CHEQUE_DESCRIPTION_TH=\\\\u0E40\\\\u0E0A\\\\u0E47\\\\u0E04\\012LR_VDRL=DTX\\012SPECIMEN_RECEIVER=\\\\u0E1C\\\\u0E39\\\\u0E49\\\\u0E40\\\\u0E01\\\\u0E47\\\\u0E1A\\\\u0E2A\\\\u0E34\\\\u0E48\\\\u0E07\\\\u0E2A\\\\u0E48\\\\u0E07\\\\u0E15\\\\u0E23\\\\u0E27\\\\u0E08\\012RC_OPD_INVOICE_COST_TITLE=\\\\u0E43\\\\u0E1A\\\\u0E41\\\\u0E08\\\\u0E49\\\\u0E07\\\\u0E40\\\\u0E23\\\\u0E35\\\\u0E22\\\\u0E01\\\\u0E40\\\\u0E01\\\\u0E47\\\\u0E1A\\012BTN_DISPENSE=\\\\u0E08\\\\u0E48\\\\u0E32\\\\u0E22\\012PM_OTHER_DESCRIPTION_TH=\\\\u0E2D\\\\u0E37\\\\u0E48\\\\u0E19\\\\u0E46\\012PM_FUND_DESCRIPTION_EN=Fund\\012DISCOUNT_REASON_SPECIAL_CARD_DESCRIPTION=\\\\u0E1A\\\\u0E31\\\\u0E15\\\\u0E23\\\\u0E15\\\\u0E32\\\\u0E21\\\\u0E2A\\\\u0E34\\\\u0E17\\\\u0E18\\\\u0E34\\012VS_LBL_NON_URGENT_EMERGENCY=Non-Urgent\\012PM_DEPOSIT_DESCRIPTION_EN=Deposit\\012PREPARE_CANCEL_RECEIPT_STATUS_DESCRIPTION=\\\\u0E22\\\\u0E01\\\\u0E40\\\\u0E25\\\\u0E34\\\\u0E01\\012LBL_NT_ADDITION_SUPPLYMENT=(\\\\u0E0A\\\\u0E33\\\\u0E23\\\\u0E30\\\\u0E2A\\\\u0E48\\\\u0E27\\\\u0E19\\\\u0E40\\\\u0E01\\\\u0E34\\\\u0E19\\\\u0E40\\\\u0E2D\\\\u0E07)\\012MENU_PRINT_OR_ITEM_COST=\\\\u0E43\\\\u0E1A\\\\u0E41\\\\u0E22\\\\u0E01\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E25\\\\u0E30\\\\u0E40\\\\u0E2D\\\\u0E35\\\\u0E22\\\\u0E14\\\\u0E04\\\\u0E48\\\\u0E32\\\\u0E43\\\\u0E0A\\\\u0E49\\\\u0E08\\\\u0E48\\\\u0E32\\\\u0E22\\\\u0E1C\\\\u0E48\\\\u0E32\\\\u0E15\\\\u0E31\\\\u0E14\\\\u0E41\\\\u0E22\\\\u0E01\\\\u0E15\\\\u0E32\\\\u0E21\\\\u0E41\\\\u0E1C\\\\u0E19\\\\u0E01\\012BTN_PRINT_RX_PHARM=\\\\u0E1E\\\\u0E34\\\\u0E21\\\\u0E1E\\\\u0E4C\\\\u0E43\\\\u0E1A\\\\u0E2A\\\\u0E31\\\\u0E48\\\\u0E07\\\\u0E22\\\\u0E32\\012VS_LBL_URGENT_EMERGENCY=Urgent\\012SUBMENU_SERVICE_CANCEL_VISIT=\\\\u0E22\\\\u0E01\\\\u0E40\\\\u0E25\\\\u0E34\\\\u0E01\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E40\\\\u0E02\\\\u0E49\\\\u0E32\\\\u0E23\\\\u0E31\\\\u0E1A\\\\u0E1A\\\\u0E23\\\\u0E34\\\\u0E01\\\\u0E32\\\\u0E23\\012BTN_REPORT_SOME_TEST=\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E07\\\\u0E32\\\\u0E19\\\\u0E1C\\\\u0E25\\\\u0E1A\\\\u0E32\\\\u0E07\\\\u0E2A\\\\u0E48\\\\u0E27\\\\u0E19\\012BTN_ASSIGN_LAB_XRAY=\\\\u0E2A\\\\u0E48\\\\u0E07\\\\u0E15\\\\u0E23\\\\u0E27\\\\u0E08/\\\\u0E22\\\\u0E32\\012LR_RUBELLA_IGM=Rubella IgM\\012MENU_PRINT_BARCODE=\\\\u0E1E\\\\u0E34\\\\u0E21\\\\u0E1E\\\\u0E4C Barcode \\\\u0E1C\\\\u0E39\\\\u0E49\\\\u0E1B\\\\u0E48\\\\u0E27\\\\u0E22\\012CANCEL_RECEIPT_STATUS_DESCRIPTION=\\\\u0E22\\\\u0E37\\\\u0E19\\\\u0E22\\\\u0E31\\\\u0E19\\\\u0E22\\\\u0E01\\\\u0E40\\\\u0E25\\\\u0E34\\\\u0E01\\012CMB_SURGEON=\\\\u0E41\\\\u0E1E\\\\u0E17\\\\u0E22\\\\u0E4C\\\\u0E1C\\\\u0E39\\\\u0E49\\\\u0E1C\\\\u0E48\\\\u0E32\\\\u0E15\\\\u0E31\\\\u0E14\\012LR_RUBELLA_IGG=Rubella IgG\\012LBL_NT_INFECTION=\\\\u0E15\\\\u0E34\\\\u0E14\\\\u0E40\\\\u0E0A\\\\u0E37\\\\u0E49\\\\u0E2D\\012RC_IPD_INVOICE_TITLE=\\\\u0E43\\\\u0E1A\\\\u0E41\\\\u0E08\\\\u0E49\\\\u0E07\\\\u0E40\\\\u0E23\\\\u0E35\\\\u0E22\\\\u0E01\\\\u0E40\\\\u0E01\\\\u0E47\\\\u0E1A\\012LBL_APGAR_STATE_ONE=1 MIN\\012PLAN_TYPE_SPECIAL_CARD=\\\\u0E1A\\\\u0E31\\\\u0E15\\\\u0E23\\\\u0E2A\\\\u0E48\\\\u0E27\\\\u0E19\\\\u0E25\\\\u0E14\\012MENU_LIST_PREPARE_CANCEL_RECEIPT=\\\\u0E23\\\\u0E32\\\\u0E22\\\\u0E01\\\\u0E32\\\\u0E23\\\\u0E43\\\\u0E1A\\\\u0E40\\\\u0E2A\\\\u0E23\\\\u0E47\\\\u0E08/\\\\u0E41\\\\u0E08\\\\u0E49\\\\u0E07\\\\u0E2B\\\\u0E19\\\\u0E35\\\\u0E49\\\\u0E23\\\\u0E2D\\\\u0E22\\\\u0E37\\\\u0E19\\\\u0E22\\\\u0E31\\\\u0E19\\\\u0E22\\\\u0E01\\\\u0E40\\\\u0E25\\\\u0E34\\\\u0E01\\012RC_OPD_INVOICE_TITLE=\\\\u0E43\\\\u0E1A\\\\u0E41\\\\u0E08\\\\u0E49\\\\u0E07\\\\u0E40\\\\u0E23\\\\u0E35\\\\u0E22\\\\u0E01\\\\u0E40\\\\u0E01\\\\u0E47\\\\u0E1A\\012', 'admin', '2011-11-30', '10:51:28');


------------------------------------------------- Create Hos Stock Table -------------------------------------------------
CREATE TABLE t_stock_drug_return_sp (
    t_stock_drug_return_sp_id varchar(255) NOT NULL,
    t_order_drug_return_id varchar(255),
    b_service_point_id varchar(255),
    t_order_item_id varchar(255)
);

ALTER TABLE ONLY t_stock_drug_return_sp
    ADD CONSTRAINT t_stock_drug_return_sp_pkey PRIMARY KEY (t_stock_drug_return_sp_id);




-------------------------------------------------- Hos Rule b_employee ---------------------------------------------------
DROP RULE b_employee_on_ins ON b_employee;
DROP RULE b_employee_on_ins_for_role_admin ON b_employee;
DROP RULE b_employee_on_ins_for_role_other ON b_employee;
DROP RULE b_employee_on_ins_for_role ON b_employee;
DROP RULE b_employee_on_upd ON b_employee;
DROP RULE b_employee_on_upd_role ON b_employee;
DROP RULE b_employee_on_del ON b_employee;
DROP RULE b_employee_on_del_role ON b_employee;


CREATE RULE b_employee_on_ins AS ON INSERT TO b_employee
DO
	INSERT INTO imed.employee(employee_id, password, firstname, lastname, base_service_point_id, active)
	VALUES(NEW.employee_login, NEW.employee_password, NEW.employee_firstname, NEW.employee_lastname, NEW.b_service_point_id, NEW.employee_active);

CREATE RULE b_employee_on_ins_for_role_admin AS ON INSERT TO b_employee
WHERE NEW.f_employee_authentication_id = '9'
DO
	INSERT INTO imed.employee_role(employee_role_id, employee_id, page_default, admin_module_auth, admin_user_and_sp_auth, admin_drug_detail_auth, admin_stock_auth, admin_stock_manage_auth, admin_stock_report_auth, admin_other_auth, admin_manage_item_auth, admin_manage_base_auth)
	VALUES(NEW.employee_login, NEW.employee_login, '0', '1111', '11', '11', '111111111111', '11111', '111111111111111111111111111111111', '1', '10001', '000111');

CREATE RULE b_employee_on_ins_for_role_other AS ON INSERT TO b_employee
WHERE NEW.f_employee_authentication_id <> '9'
DO
	INSERT INTO imed.employee_role(employee_role_id ,employee_id)
	VALUES(NEW.employee_login, NEW.employee_login);

CREATE RULE b_employee_on_upd AS ON UPDATE TO b_employee
DO
	UPDATE imed.employee
	SET employee_id = NEW.employee_login,
	password = NEW.employee_password,
	firstname = NEW.employee_firstname,
	lastname = NEW.employee_lastname,
	base_service_point_id = NEW.b_service_point_id
	WHERE employee_id = OLD.employee_login;

CREATE RULE b_employee_on_upd_role AS ON UPDATE TO b_employee
DO
	UPDATE imed.employee_role
	SET employee_role_id = NEW.employee_login,
	employee_id = NEW.employee_login
	WHERE employee_role_id = OLD.employee_login;

CREATE RULE b_employee_on_del AS ON DELETE TO b_employee
DO
	DELETE FROM imed.employee WHERE employee_id = OLD.employee_login;

CREATE RULE b_employee_on_del_role AS ON DELETE TO b_employee
DO
	DELETE FROM imed.employee_role WHERE employee_role_id = OLD.employee_login;




-------------------------------------------------- Hos Rule b_item -------------------------------------------------------------
DROP RULE b_item_on_del ON b_item;
DROP RULE b_item_on_ins ON b_item;
DROP RULE b_item_on_upd ON b_item;


CREATE RULE b_item_on_ins AS ON INSERT TO b_item
DO
	INSERT INTO imed.item(item_id, item_code, common_name, active, nick_name,fix_item_type_id,base_unit_id,hospital_item,pack_size,stock_critical,print_report)
	VALUES (NEW.b_item_id, NEW.item_number, NEW.item_common_name, NEW.item_active, NEW.item_nick_name, 
		(select case when f_item_group_id = '1'
		then '0'
		else f_item_group_id
		end as f_item_group
		from b_item inner join b_item_subgroup on b_item.b_item_subgroup_id = b_item_subgroup.b_item_subgroup_id
		where b_item.b_item_id =  NEW.b_item_id),
		(select case when b_item_drug_uom.item_drug_uom_number <> ''
		and b_item_drug_uom.item_drug_uom_number is not null
		then b_item_drug_uom.item_drug_uom_number
		else ''
		end as item_drug_uom
		from b_item left join b_item_drug on b_item.b_item_id = b_item_drug.b_item_id
		left join b_item_drug_uom on b_item_drug.item_drug_purch_uom = b_item_drug_uom.b_item_drug_uom_id
		where b_item.b_item_id =  NEW.b_item_id),
	'1','1','0','1');

CREATE RULE b_item_on_upd AS ON UPDATE TO b_item
DO
	UPDATE imed.item
	SET item_code = NEW.item_number, 
	common_name = NEW.item_common_name, 
	active = NEW.item_active, 
	nick_name = NEW.item_nick_name,
	fix_item_type_id = (select case when f_item_group_id  = '1' 
					then '0'
					else f_item_group_id
					end as f_item_group
					from b_item inner join b_item_subgroup on b_item.b_item_subgroup_id = b_item_subgroup.b_item_subgroup_id 
					where b_item.b_item_id = NEW.b_item_id),
	base_unit_id = (select case when b_item_drug_uom.item_drug_uom_number <> ''
					and b_item_drug_uom.item_drug_uom_number is not null
					then b_item_drug_uom.item_drug_uom_number
					else ''
					end as item_drug_uom
					from b_item left join b_item_drug on b_item.b_item_id = b_item_drug.b_item_id
					left join b_item_drug_uom on b_item_drug.item_drug_purch_uom = b_item_drug_uom.b_item_drug_uom_id
					where b_item.b_item_id =  NEW.b_item_id)
	WHERE item_id = OLD.b_item_id;

CREATE RULE b_item_on_del AS ON DELETE TO b_item
DO
	DELETE FROM imed.item WHERE item_id = OLD.b_item_id;




-------------------------------------------------- Hos Rule b_item_price -------------------------------------------------------------
DROP RULE b_item_price_on_del ON b_item_price;
DROP RULE b_item_price_on_ins ON b_item_price;
DROP RULE b_item_price_on_upd ON b_item_price;


CREATE RULE b_item_price_on_ins AS ON INSERT TO b_item_price
DO
	INSERT INTO imed.item_price(item_price_id, item_id, base_tariff_id, active_date, unit_price) 
	VALUES (NEW.b_item_price_id, NEW.b_item_id, '1', (select cast(substring(NEW.item_price_active_date,0,5) as integer)-543
											|| '-' ||substring(NEW.item_price_active_date,6,2)
											|| '-' ||substring(NEW.item_price_active_date,9,2) as dt), NEW.item_price);

CREATE RULE b_item_price_on_upd AS ON UPDATE TO b_item_price
DO
	UPDATE imed.item_price
	SET item_price_id = NEW.b_item_price_id, 
	item_id = NEW.b_item_id, 
	base_tariff_id = '1', 
	active_date = (select cast(substring(NEW.item_price_active_date,0,5) as integer)-543
				|| '-' ||substring(NEW.item_price_active_date,6,2)
				|| '-' ||substring(NEW.item_price_active_date,9,2) as dt), 
	unit_price = NEW.item_price
	WHERE item_price_id = OLD.b_item_price_id;

CREATE RULE b_item_price_on_del AS ON DELETE TO b_item_price
DO
	DELETE FROM imed.item_price WHERE item_price_id = OLD.b_item_price_id;




-------------------------------------------------- Hos Rule b_service_point -------------------------------------------------------------
DROP RULE b_service_point_on_del ON b_service_point;
DROP RULE b_service_point_on_ins ON b_service_point;
DROP RULE b_service_point_on_upd  ON b_service_point;


CREATE RULE b_service_point_on_ins AS ON INSERT TO b_service_point
DO
	INSERT INTO imed.base_service_point(base_service_point_id, description, active) 
	VALUES (NEW.b_service_point_id, NEW.service_point_description, NEW.service_point_active);

CREATE RULE b_service_point_on_upd AS ON UPDATE TO b_service_point
DO
	UPDATE imed.base_service_point
	SET base_service_point_id = NEW.b_service_point_id, 
	description = NEW.service_point_description, 
	active = NEW.service_point_active
	WHERE base_service_point_id = OLD.b_service_point_id;

CREATE RULE b_service_point_on_del AS ON DELETE TO b_service_point
DO
	DELETE FROM imed.base_service_point WHERE base_service_point_id = OLD.b_service_point_id;




-------------------------------------------------- Hos Rule b_visit_ward -------------------------------------------------------------
DROP RULE b_visit_ward_on_del ON b_visit_ward;
DROP RULE b_visit_ward_on_ins ON b_visit_ward;
DROP RULE b_visit_ward_on_upd ON b_visit_ward;

CREATE RULE b_visit_ward_on_ins AS ON INSERT TO b_visit_ward
DO
	INSERT INTO imed.base_service_point(base_service_point_id, description, active) 
	VALUES (NEW.b_visit_ward_id, NEW.visit_ward_description, NEW.visit_ward_active);

CREATE RULE b_visit_ward_on_upd AS ON UPDATE TO b_service_point
DO
	UPDATE imed.base_service_point
	SET base_service_point_id = NEW.b_service_point_id, 
	description = NEW.service_point_description, 
	active = NEW.service_point_active
	WHERE base_service_point_id = OLD.b_service_point_id;

CREATE RULE b_visit_ward_on_del AS ON DELETE TO b_service_point
DO
	DELETE FROM imed.base_service_point WHERE base_service_point_id = OLD.b_service_point_id;




---------------------------------------------- Hos Rule b_item_drug_uom ----------------------------------------------------------
DROP RULE b_item_drug_uom_on_del ON b_item_drug_uom;
DROP RULE b_item_drug_uom_on_ins ON b_item_drug_uom;
DROP RULE b_item_drug_uom_on_upd ON b_item_drug_uom;


CREATE RULE b_item_drug_uom_on_ins AS ON INSERT TO b_item_drug_uom
DO
	INSERT INTO imed.base_unit(base_unit_id, description_th) 
	VALUES (NEW.item_drug_uom_number, NEW.item_drug_uom_description);

CREATE RULE b_item_drug_uom_on_upd AS ON UPDATE TO b_item_drug_uom
DO
	UPDATE imed.base_unit 
	SET base_unit_id = NEW.item_drug_uom_number, 
	description_th = NEW.item_drug_uom_description
	WHERE base_unit_id = OLD.item_drug_uom_number;

CREATE RULE b_item_drug_uom_on_del AS ON DELETE TO b_item_drug_uom
DO
	DELETE FROM imed.base_unit WHERE base_unit_id = OLD.item_drug_uom_number;




----------------------------------------------- Hos Rule base_site -------------------------------------------------------------------
DROP RULE b_site_on_del ON b_site;
DROP RULE b_site_on_ins ON b_site;
DROP RULE b_site_on_upd ON b_site;


CREATE RULE b_site_on_ins AS ON INSERT TO b_site
DO
	INSERT INTO imed.base_site(base_site_id, site_name, address,village, tambol, amphur, changwat, postcode, tel, login_message_active) 
	VALUES (NEW.b_visit_office_id, NEW.site_full_name, NEW.site_phone_number, NEW.site_house ,NEW.site_moo,substring(NEW.site_tambon,5,2),substring(New.site_amphur,3,2),substring(NEW.site_changwat,1,2),NEW.site_postcode,'1');

CREATE RULE b_site_on_upd AS ON UPDATE TO b_site
DO
	UPDATE imed.base_site 
	SET base_site_id = NEW.b_visit_office_id, 
	site_name = NEW.site_full_name, 
	address = NEW.site_house ,
	village =  NEW.site_moo,
	tambol = substring(NEW.site_tambon,5,2),
	amphur = substring(New.site_amphur,3,2),
	changwat = substring(NEW.site_changwat,1,2),
	postcode = NEW.site_postcode,
	tel = NEW.site_phone_number, 
	login_message_active = '1'
	WHERE base_site_id = OLD.b_visit_office_id;

CREATE RULE b_site_on_del AS ON DELETE TO b_site
DO
	DELETE FROM imed.base_site WHERE base_site_id = OLD.b_visit_office_id;




----------------------------------------------- Hos Rule base_address -----------------------------------------------------------------
DROP RULE f_address_on_del ON f_address;
DROP RULE f_address_on_ins ON f_address;
DROP RULE f_address_on_upd ON f_address;


CREATE RULE f_address_on_del AS ON DELETE TO f_address
DO
	DELETE FROM imed.base_address WHERE base_address.fullcode::text = old.f_address_id::text;

CREATE RULE f_address_on_ins AS ON INSERT TO f_address
DO
	INSERT INTO imed.base_address (fullcode, description, type, ampcode, cgwcode, region) 
	VALUES (new.f_address_id, new.address_description, substring(new.address_tambon_type,5,2), substring(new.address_amphur_id,3,2), substring(new.address_changwat_id,1,2), new.address_region);

CREATE RULE f_address_on_upd AS  ON UPDATE TO f_address
DO
	UPDATE imed.base_address SET fullcode = new.f_address_id, description = new.address_description, type = substring(new.address_tambon_type,5,2), ampcode = substring(new.address_amphur_id,3,2), cgwcode = substring(new.address_changwat_id,1,2), region = new.address_region
	WHERE base_address.fullcode = old.f_address_id;


CREATE TABLE s_stock_version (
    s_stock_version_id character varying(255) NOT NULL,
    stock_version_number character varying(255) DEFAULT ''::character varying,
    stock_version_description character varying(255) DEFAULT ''::character varying,
    stock_version_application_number character varying(255) DEFAULT ''::character varying,
    stock_version_database_number character varying(255) DEFAULT ''::character varying,
    stock_version_update_time character varying(255) DEFAULT ''::character varying
);
ALTER TABLE ONLY s_stock_version
    ADD CONSTRAINT s_stock_version_pkey PRIMARY KEY (s_stock_version_id);

CREATE UNIQUE INDEX st_version_index ON s_stock_version USING btree (s_stock_version_id);
INSERT INTO s_stock_version VALUES ('st00100000001', '01', 'Stock Module', '1.3.20120612', '1.0.20120612', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','imed_stock_db.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'iMed Stock');
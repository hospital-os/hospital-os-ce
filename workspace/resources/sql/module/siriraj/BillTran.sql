select 

        '131' as spid
        , b_site.b_visit_office_id as HCODE
        , b_site.site_full_name as HNAME        
        , '' as DATETIME
        , '' as SESSNO
        , '' as RECCOUNT

--BILLTRAN

        ,'131'  as Station
        , ''as AuthCode
        , (substr(t_visit.visit_begin_visit_time,1,4)::int - 543)||substr(t_visit.visit_begin_visit_time,5,6)
            ||' '||substr(t_visit.visit_begin_visit_time,12,8)  as DTTran
        , b_site.b_visit_office_id as HCode_bill
        , lpad(t_visit.visit_vn,9,'0') ||0|| replace(substr(t_billing_invoice.t_billing_invoice_date_time,12,19),':','')  as InvNo


        , substr( t_billing_invoice.t_billing_invoice_date_time,3,2) 
            ||substr( t_billing_invoice.t_billing_invoice_date_time,6,2)
            ||substr( t_billing_invoice.t_billing_invoice_date_time,9,2)
            ||'-'|| replace(substr(t_billing_invoice.t_billing_invoice_date_time,12,19),':','')  as BillNo 

        , t_patient.patient_hn as HN
        , '' as MemberNo
        , t_billing_invoice.billing_invoice_payer_share as Amount
        , t_billing_invoice.billing_invoice_patient_share as Paid 
        , '' as VerCode 
        , '' as Tflag


from 
        t_billing_invoice inner join t_visit on t_billing_invoice.t_visit_id = t_visit.t_visit_id
                and t_billing_invoice.billing_invoice_active = '1'
                and cast(t_billing_invoice.billing_invoice_payer_share as float) > 0.0
                and t_visit.f_visit_type_id = '0'
                and f_visit_status_id in  ('2','3')
        inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
        inner join t_visit_payment on t_billing_invoice.t_payment_id = t_visit_payment.t_visit_payment_id
                 and t_visit_payment.visit_payment_active = '1'
                and t_visit_payment.b_contract_plans_id in (select  b_welfare_direct_draw_map_plan.b_contract_plans_id   from    b_welfare_direct_draw_map_plan ) 
       
        cross join b_site

where
        lpad(t_visit.visit_vn,9,'0') ||0|| replace(substr(t_billing_invoice.t_billing_invoice_date_time,12,19),':','')  = ?




/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.utility;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Somprasong
 */
public class ResourceBundle {

    private static final Logger LOG = Logger.getLogger(ResourceBundle.class.getName());
    public static final int GLOBAL = 0;
    public static final int TEXT = 1;
    public static final int LABEL = 2;
    public static final int BUTTON = 3;
    public static final int MENU = 4;

    public static String getBundleGlobal(String key) {
        return getBundle(GLOBAL, key);
    }

    public static String getBundleText(String key) {
        return getBundle(TEXT, key);
    }

    public static String getBundleLabel(String key) {
        return getBundle(LABEL, key);
    }

    public static String getBundleMenu(String key) {
        return getBundle(BUTTON, key);
    }

    public static String getBundleButton(String key) {
        return getBundle(MENU, key);
    }

    public static String getBundle(int type, String key) {
        try {
            switch (type) {
                case GLOBAL:
                    return java.util.ResourceBundle.getBundle("com/hosos/language/core/Global").getString(key);
                case TEXT:
                    return java.util.ResourceBundle.getBundle("com/hosos/language/core/Text").getString(key);
                case LABEL:
                    return java.util.ResourceBundle.getBundle("com/hosos/language/core/Label").getString(key);
                case BUTTON:
                    return java.util.ResourceBundle.getBundle("com/hosos/language/core/Button").getString(key);
                case MENU:
                    return java.util.ResourceBundle.getBundle("com/hosos/language/core/Menu").getString(key);
                default:
                    return "";
            }
        } catch (Exception ex) {
//            LOG.log(Level.SEVERE, ex.getMessage(), ex);
            return key;
        }
    }
}

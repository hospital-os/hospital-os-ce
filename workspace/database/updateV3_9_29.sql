CREATE TABLE t_person (
    t_person_id CHARACTER VARYING(255)  NOT NULL,
    person_hcis CHARACTER VARYING(255)  NOT NULL,
    t_health_home_id CHARACTER VARYING(255)  NOT NULL,
    person_family_number CHARACTER VARYING(3),
    f_prefix_id CHARACTER VARYING(255)  NOT NULL,
    person_firstname CHARACTER VARYING(255),
    person_lastname CHARACTER VARYING(255),
    person_pid CHARACTER VARYING(13),
    person_passport CHARACTER VARYING(255),
    person_birthday CHARACTER VARYING(19),
    person_birthday_true CHARACTER VARYING(1) DEFAULT '0'  NOT NULL,
    f_sex_id CHARACTER VARYING(1)  NOT NULL,
    f_patient_blood_group_id CHARACTER VARYING(1),
    f_rh_group_id CHARACTER VARYING(1),
    f_patient_nation_id CHARACTER VARYING(255),
    f_patient_race_id CHARACTER VARYING(255),
    f_patient_religion_id CHARACTER VARYING(255),
    father_person_id CHARACTER VARYING(255),
    mother_person_id CHARACTER VARYING(255),
    couple_person_id CHARACTER VARYING(255),
    f_patient_marriage_status_id CHARACTER VARYING(255),
    f_patient_education_type_id CHARACTER VARYING(255),
    f_patient_occupation_id CHARACTER VARYING(255),
    f_patient_family_status_id CHARACTER VARYING(255),
    f_person_village_status_id CHARACTER VARYING(2) DEFAULT '5',
    f_patient_area_status_id CHARACTER VARYING(40),
    f_person_foreigner_id CHARACTER VARYING(255),
    person_foreigner_card_no CHARACTER VARYING(255),
    person_revenu CHARACTER VARYING(255),
    person_move_in_date_time CHARACTER VARYING(19),
    active CHARACTER VARYING(1) DEFAULT '1'  NOT NULL,
    user_record_id CHARACTER VARYING(255)  NOT NULL,
    record_date_time CHARACTER VARYING(19)  NOT NULL,
    user_modify_id CHARACTER VARYING(255)  NOT NULL,
    modify_date_time CHARACTER VARYING(19)  NOT NULL,
    user_cancel_id CHARACTER VARYING(255),
    cancel_date_time CHARACTER VARYING(19),	
    CONSTRAINT t_person_pkey PRIMARY KEY (t_person_id),
	CONSTRAINT t_person_unique1 UNIQUE (person_hcis)
);

ALTER TABLE t_patient ADD COLUMN t_person_id CHARACTER VARYING(255) NOT NULL default '' ;
update t_patient set t_person_id = t_health_family_id ;

CREATE TABLE f_person_foreigner (
    f_person_foreigner_id CHARACTER VARYING(2)  NOT NULL,
    person_foreigner_description CHARACTER VARYING(255)  NOT NULL,
    CONSTRAINT f_person_foreigner_pkey PRIMARY KEY (f_person_foreigner_id)
);
--insert f_person_foreigner
INSERT INTO f_person_foreigner VALUES ('0','ไม่เป็นต่างด้าว');
INSERT INTO f_person_foreigner VALUES ('01','ต่างด้าวขึ้นทะเบียน ในกลุ่มนักเรียนในสถานศึกษาที่รับสวัสดิการจากรัฐด้านการศึกษา');
INSERT INTO f_person_foreigner VALUES ('02','ต่างด้าวขึ้นทะเบียน ในกลุ่มคนไร้รากเหง้า');
INSERT INTO f_person_foreigner VALUES ('03','ต่างด้าวขึ้นทะเบียน ในกลุ่มคนที่มีประโยชน์แก่ประเทศชาติ');
INSERT INTO f_person_foreigner VALUES ('11','ที่มีเลข 13 หลัก  ที่ขึ้นต้นด้วย 6');
INSERT INTO f_person_foreigner VALUES ('12','ที่มีรหัสที่ขึ้นต้นด้วยเลข 0 และได้ Workpermitted');
INSERT INTO f_person_foreigner VALUES ('13','ขึ้นทะเบียนกับกระทรวงมหาดไทยที่มีรหัสที่ขึ้นต้นด้วยเลข 0');
INSERT INTO f_person_foreigner VALUES ('14','กระทรวงแรงงาน  โดยมี Passport/Visa');
INSERT INTO f_person_foreigner VALUES ('15','ที่มีรหัสที่ขึ้นต้นด้วยเลข 3 และ 4 และไม่ใช่สัญชาติไทย');
INSERT INTO f_person_foreigner VALUES ('16','ที่มีรหัสที่ขึ้นต้นด้วยเลข 5 และไม่ใช่สัญชาติไทย');
INSERT INTO f_person_foreigner VALUES ('17','ที่มีรหัสที่ขึ้นต้นด้วยเลข 8 และ ไม่ใช่สัญชาติไทย');
INSERT INTO f_person_foreigner VALUES ('18','ที่มีรหัสที่ขึ้นต้นด้วยเลข 7');
INSERT INTO f_person_foreigner VALUES ('21','ต่างด้าวที่อพยพและอยู่ในค่าย/ศูนย์พักพิง');
INSERT INTO f_person_foreigner VALUES ('22','ต่างด้าวที่เป็นผู้ติดตามของรหัส 11 ,12 ,15,16,17 ข้างต้นได้แก่ สามี/ภรรยา/บุตร/ญาติ');
INSERT INTO f_person_foreigner VALUES ('23','หมายถึงกลุ่มอื่นๆ');


CREATE TABLE t_person_address (
    t_person_address_id CHARACTER VARYING(255)  NOT NULL,
    t_person_id CHARACTER VARYING(255)  NOT NULL,
    f_address_housetype_id CHARACTER VARYING(2)  NOT NULL,
    person_address_houseid CHARACTER VARYING(255) DEFAULT '',
    person_address_roomno CHARACTER VARYING(10) DEFAULT '',
    person_address_building CHARACTER VARYING(255) DEFAULT '',
    person_address_house CHARACTER VARYING(255) DEFAULT '',
    person_address_moo CHARACTER VARYING(50) DEFAULT '',
    person_address_villaname CHARACTER VARYING(255) DEFAULT '',
    person_address_soisub CHARACTER VARYING(255) DEFAULT '',
    person_address_soimain CHARACTER VARYING(255) DEFAULT '',
    person_address_road CHARACTER VARYING(255) DEFAULT '',
    person_address_tambon CHARACTER VARYING(6) DEFAULT '',
    person_address_amphur CHARACTER VARYING(6) DEFAULT '',
    person_address_changwat CHARACTER VARYING(6) DEFAULT '',
    person_address_telephone CHARACTER VARYING(255) DEFAULT '',
    person_address_mobile CHARACTER VARYING(255) DEFAULT '',
    person_address_latitude DOUBLE PRECISION,
    person_address_longitude DOUBLE PRECISION,
    person_address_other_country CHARACTER VARYING(255) DEFAULT '',
    person_address_other_country_active CHARACTER VARYING(1) DEFAULT '0',
    f_address_type_id CHARACTER VARYING(2)  NOT NULL,
    active CHARACTER VARYING(1) DEFAULT '1',
    user_record_id CHARACTER VARYING(255)  NOT NULL,
    record_date_time CHARACTER VARYING(19)  NOT NULL,
    user_modify_id CHARACTER VARYING(255)  NOT NULL,
    modify_date_time CHARACTER VARYING(19)  NOT NULL,
    CONSTRAINT t_person_address_pkey PRIMARY KEY (t_person_address_id)
);

CREATE TABLE t_patient_contact (
    t_patient_contact_id CHARACTER VARYING(255)  NOT NULL,
    t_patient_id CHARACTER VARYING(255)  NOT NULL,
    patient_contact_office_name CHARACTER VARYING(255) DEFAULT '',
    patient_contact_office_tel CHARACTER VARYING(255) DEFAULT '',
    patient_contact_email CHARACTER VARYING(255) DEFAULT '',
    patient_contact_firstname CHARACTER VARYING(255) DEFAULT '',
    patient_contact_lastname CHARACTER VARYING(255) DEFAULT '',
    f_patient_relation_id CHARACTER VARYING(2)  NOT NULL,
    patient_contact_sex_id CHARACTER VARYING(255) DEFAULT '',
    patient_contact_house CHARACTER VARYING(255) DEFAULT '',
    patient_contact_moo CHARACTER VARYING(50) DEFAULT '',
    patient_contact_road CHARACTER VARYING(255) DEFAULT '',	
    patient_contact_tambon CHARACTER VARYING(6) DEFAULT '',
    patient_contact_amphur CHARACTER VARYING(6) DEFAULT '',
    patient_contact_changwat CHARACTER VARYING(6) DEFAULT '',
    patient_contact_telephone CHARACTER VARYING(255) DEFAULT '',
    patient_contact_mobile CHARACTER VARYING(255) DEFAULT '',
    patient_contact_detail_etc TEXT DEFAULT '',
    active CHARACTER VARYING(1) DEFAULT '1'  NOT NULL,
    user_record_id CHARACTER VARYING(255)  NOT NULL,
    record_date_time CHARACTER VARYING(19)  NOT NULL,
    user_modify_id CHARACTER VARYING(255)  NOT NULL,
    modify_date_time CHARACTER VARYING(19)  NOT NULL,
    CONSTRAINT t_patient_contact_pkey PRIMARY KEY (t_patient_contact_id)
);


CREATE TABLE t_visit_illness_address (
    t_visit_illness_address_id CHARACTER VARYING(255)  NOT NULL,
    t_visit_id CHARACTER VARYING(255)  NOT NULL,
    f_address_housetype_id CHARACTER VARYING(2) NOT NULL,
    visit_illness_address_roomno CHARACTER VARYING(10),
    visit_illness_address_building CHARACTER VARYING(255),
    visit_illness_address_house CHARACTER VARYING(255),
    visit_illness_address_moo CHARACTER VARYING(3),
    visit_illness_address_villaname CHARACTER VARYING(255),
    visit_illness_address_soisub CHARACTER VARYING(255),
    visit_illness_address_soimain CHARACTER VARYING(255),
    visit_illness_address_road CHARACTER VARYING(255),
    visit_illness_address_tambon CHARACTER VARYING(6),
    visit_illness_address_amphur CHARACTER VARYING(6),
    visit_illness_address_changwat CHARACTER VARYING(6),
    visit_illness_address_latitude DOUBLE PRECISION,
    visit_illness_address_longitude DOUBLE PRECISION,
    active CHARACTER VARYING(1) DEFAULT '1'  NOT NULL,
    user_record_id CHARACTER VARYING(255)  NOT NULL,
    record_date_time CHARACTER VARYING(19)  NOT NULL,
    user_modify_id CHARACTER VARYING(255)  NOT NULL,
    modify_date_time CHARACTER VARYING(19)  NOT NULL,
    CONSTRAINT t_visit_illness_address_pkey PRIMARY KEY (t_visit_illness_address_id)
);

CREATE TABLE f_address_type (
    f_address_type_id CHARACTER VARYING(2)  NOT NULL,
    address_type_description CHARACTER VARYING(255) NOT NULL,
    CONSTRAINT f_address_type_pkey PRIMARY KEY (f_address_type_id)
);
--insert f_address_type
INSERT INTO f_address_type VALUES ('1','ที่อยู่ตามทะเบียนบ้าน');
INSERT INTO f_address_type VALUES ('2','ที่อยู่ที่ติดต่อได้');


CREATE TABLE f_address_housetype (
    f_address_housetype_id CHARACTER VARYING(2)  NOT NULL,
    address_housetype_description CHARACTER VARYING(255) NOT NULL,
    CONSTRAINT f_address_housetype_pkey PRIMARY KEY (f_address_housetype_id)
);
--insert f_address_housetype
INSERT INTO f_address_housetype VALUES ('1','บ้านเดี่ยว บ้านแฝด');
INSERT INTO f_address_housetype VALUES ('2','ทาวน์เฮาส์ ทาวน์โฮม');
INSERT INTO f_address_housetype VALUES ('3','คอนโดมิเนียม');
INSERT INTO f_address_housetype VALUES ('4','อพาร์ทเมนท์ หอพัก');
INSERT INTO f_address_housetype VALUES ('5','บ้านพักคนงาน');
INSERT INTO f_address_housetype VALUES ('8',' อื่นๆ');
INSERT INTO f_address_housetype VALUES ('9',' ไม่ทราบ');

CREATE TABLE f_person_village_status (
    f_person_village_status_id CHARACTER VARYING(2)  NOT NULL,
    person_village_status_description CHARACTER VARYING(255) NOT NULL,
    CONSTRAINT f_person_village_status_pkey PRIMARY KEY (f_person_village_status_id)
);
--insert f_person_village_status
INSERT INTO f_person_village_status VALUES ('1','กำนัน ผู้ใหญ่บ้าน');
INSERT INTO f_person_village_status VALUES ('2','อสม.');
INSERT INTO f_person_village_status VALUES ('3','แพทย์ประจำตำบล');
INSERT INTO f_person_village_status VALUES ('4','สมาชิก อบต.');
INSERT INTO f_person_village_status VALUES ('5','อื่นๆ');

--drug_allergy
CREATE TABLE f_allergy_level (
    f_allergy_level_id CHARACTER VARYING(2)  NOT NULL,
    allergy_level_description CHARACTER VARYING(255)  NOT NULL,
    CONSTRAINT f_allergy_level_pkey PRIMARY KEY (f_allergy_level_id)
);
--insert f_allergy_level
INSERT INTO f_allergy_level VALUES ('1','1. ไม่ร้ายแรง (Non-serious)');
INSERT INTO f_allergy_level VALUES ('2','2. ร้ายแรง - เสียชีวิต (Death)');
INSERT INTO f_allergy_level VALUES ('3','3. ร้ายแรง - อัตรายถึงชีวิต (Life-threatening)');
INSERT INTO f_allergy_level VALUES ('4','4. ร้ายแรง - ต้องเข้ารับการรักษาในโรงพยาบาล (Hospitalization-initial)');
INSERT INTO f_allergy_level VALUES ('5','5. ร้ายแรง - ทำให้เพิ่มระยะเวลาในการรักษานานขึ้น (Hospitalization-prolonged)');
INSERT INTO f_allergy_level VALUES ('6','6. ร้ายแรง - พิการ (Disability)');
INSERT INTO f_allergy_level VALUES ('7','7. ร้ายแรง - เป็นเหตุให้เกิดความผิดปกติแต่กำเนิด (Congenital anomaly)');
INSERT INTO f_allergy_level VALUES ('8','8. ร้ายแรง-อื่นๆ ');


CREATE TABLE f_allergy_informant (
    f_allergy_informant_id CHARACTER VARYING(2)  NOT NULL,
    allergy_informant_description CHARACTER VARYING(255) NOT NULL,
    CONSTRAINT f_allergy_informant_pkey PRIMARY KEY (f_allergy_informant_id)
);
--insert f_allergy_informant
INSERT INTO f_allergy_informant VALUES ('1','ผู้ป่วยให้ประวัติเอง');
INSERT INTO f_allergy_informant VALUES ('2','ผู้ป่วยให้ประวัติจากการให้ข้อมูลของสถานพยาบาลอื่น');
INSERT INTO f_allergy_informant VALUES ('3','สถานพยาบาลอื่นเป็นผู้ให้ข้อมูล');
INSERT INTO f_allergy_informant VALUES ('4','สถานพยาบาลแห่งนี้เป็นผู้พบการแพ้ยาเอง');

--f_accident
CREATE TABLE f_accident_place (
    f_accident_place_id CHARACTER VARYING(2)  NOT NULL,
    f_accident_place_description CHARACTER VARYING(255) NOT NULL,
    CONSTRAINT f_accident_place_pkey PRIMARY KEY (f_accident_place_id)
);
--insert f_accident_place
INSERT INTO f_accident_place VALUES ('1','ที่บ้าน หรืออาคารที่พัก');
INSERT INTO f_accident_place VALUES ('2','ในสถานที่ทำงาน ยกเว้นโรงงานหรือก่อสร้าง');
INSERT INTO f_accident_place VALUES ('3','ในโรงงานอุตสาหกรรม หรือบริเวณก่อสร้าง');
INSERT INTO f_accident_place VALUES ('4','ภายในอาคารอื่นๆ');
INSERT INTO f_accident_place VALUES ('5','ในสถานที่สาธารณะ');
INSERT INTO f_accident_place VALUES ('6','ในชุมชน และไร่นา');
INSERT INTO f_accident_place VALUES ('7','บนถนนสายหลัก');
INSERT INTO f_accident_place VALUES ('8','บนถนนสายรอง');
INSERT INTO f_accident_place VALUES ('9','ในแม่น้ำ ลำคลอง หนองน้ำ');
INSERT INTO f_accident_place VALUES ('10','ในทะเล');
INSERT INTO f_accident_place VALUES ('11','ในป่า/ภูเขา');
INSERT INTO f_accident_place VALUES ('98','อื่นๆ');
INSERT INTO f_accident_place VALUES ('99','ไม่ทราบ');

CREATE TABLE f_accident_visit_type (
    f_accident_visit_type_id CHARACTER(2)  NOT NULL,
    accident_visit_type_description CHARACTER(255) NOT NULL,
    CONSTRAINT f_accident_visit_type_pkey PRIMARY KEY (f_accident_visit_type_id)
);
--insert f_accident_visit_type
INSERT INTO f_accident_visit_type VALUES ('1','มารับบริการเอง');
INSERT INTO f_accident_visit_type VALUES ('2','ได้รับการส่งตัวโดย First responder');
INSERT INTO f_accident_visit_type VALUES ('3','ได้รับการส่งตัวโดย BLS');
INSERT INTO f_accident_visit_type VALUES ('4','ได้รับการส่งตัวโดย ILS');
INSERT INTO f_accident_visit_type VALUES ('5','ได้รับการส่งตัวโดย ALS');
INSERT INTO f_accident_visit_type VALUES ('6','ได้รับการส่งต่อจากสถานพยาบาลอื่น');
INSERT INTO f_accident_visit_type VALUES ('7','อื่น ๆ');
INSERT INTO f_accident_visit_type VALUES ('9','ไม่ทราบ');

CREATE TABLE f_accident_symptom_eye (
    f_accident_symptom_eye_id CHARACTER VARYING(2)  NOT NULL,
    accident_symptom_eye_description CHARACTER VARYING(255)  NOT NULL,
    accident_symptom_eye_score INTEGER  NOT NULL,
    CONSTRAINT f_accident_symptom_eye_pkey PRIMARY KEY (f_accident_symptom_eye_id)
);
--insert f_accident_symptom_eye
INSERT INTO f_accident_symptom_eye VALUES ('1','ไม่ลืมตาเลย(ไม่มีการตอบสนอง) ',1);
INSERT INTO f_accident_symptom_eye VALUES ('2','ลืมตาเมื่อได้รับการกระตุ้น ',2);
INSERT INTO f_accident_symptom_eye VALUES ('3','ลืมตาเมื่อถูกเรียก',3);
INSERT INTO f_accident_symptom_eye VALUES ('4','ลืมตาได้เอง ',4);

CREATE TABLE f_accident_symptom_speak (
    f_accident_symptom_speak_id CHARACTER VARYING(2)  NOT NULL,
    accident_symptom_speak_description CHARACTER VARYING(255)  NOT NULL,
    accident_symptom_speak_score INTEGER  NOT NULL,
    CONSTRAINT f_accident_symptom_speak_pkey PRIMARY KEY (f_accident_symptom_speak_id)
);
--insert f_accident_symptom_speak
INSERT INTO f_accident_symptom_speak VALUES ('1','ไม่เป็นคำพูดเลย',1);
INSERT INTO f_accident_symptom_speak VALUES ('2','เปร่งเสียงได้แต่ไม่เป็นคำพูด เช่น อื่ม หรือเสียงอยู่ในคอ ',2);
INSERT INTO f_accident_symptom_speak VALUES ('3','พูดเป็นคำ ๆ',3);
INSERT INTO f_accident_symptom_speak VALUES ('4','พูดสับสนไม่รู้เรื่อง',4);
INSERT INTO f_accident_symptom_speak VALUES ('5','พูดได้เองถูกต้องเป็นประโยค',5);

CREATE TABLE f_accident_symptom_movement (
    f_accident_symptom_movement_id CHARACTER VARYING(2)  NOT NULL,
    accident_symptom_movement_description CHARACTER VARYING(255)  NOT NULL,
    accident_symptom_movement_score INTEGER  NOT NULL,
    CONSTRAINT f_accident_symptom_movement_pkey PRIMARY KEY (f_accident_symptom_movement_id)
);
--insert f_accident_symptom_movement
INSERT INTO f_accident_symptom_movement VALUES ('1','ไม่เคลื่อนไหวโต้ตอบ',1);
INSERT INTO f_accident_symptom_movement VALUES ('2','แขนเหยีอดผิดปกติ',2);
INSERT INTO f_accident_symptom_movement VALUES ('3','แขนงอผิดปกติ (ชัก)',3);
INSERT INTO f_accident_symptom_movement VALUES ('4','ชักแขนขาหนีเมื่อได้รับการกระตุ้น',4);
INSERT INTO f_accident_symptom_movement VALUES ('5','ทราบตำแหน่งที่ได้รับบาดเจ็บ',5);
INSERT INTO f_accident_symptom_movement VALUES ('6','ทำตามคำสั่งได้',6);

--ALTER TABLE

--ALTER TABLE t_patient_drug_allergy
ALTER TABLE t_patient_drug_allergy ADD COLUMN f_allergy_level_id character varying(2) NOT NULL default '1';
ALTER TABLE t_patient_drug_allergy ADD COLUMN f_allergy_informant_id character varying(2) NOT NULL default '1';
ALTER TABLE t_patient_drug_allergy ADD COLUMN allergy_informant_hospital_id character varying(5) default '';

INSERT INTO f_naranjo_interpretation VALUES ('5', 'ไม่ระบุ่ (Unclassified)','-1000','-1000');

--ALTER TABLE t_health_home
ALTER TABLE t_health_home ADD COLUMN f_address_housetype_id character varying(2)  NOT NULL default '9';
ALTER TABLE t_health_home ADD COLUMN health_home_roomno character varying(10) default '';
ALTER TABLE t_health_home ADD COLUMN health_home_building character varying(255) default '';
ALTER TABLE t_health_home ADD COLUMN health_home_villaname character varying(255) default '';
ALTER TABLE t_health_home ADD COLUMN health_home_soisub character varying(255) default '';
ALTER TABLE t_health_home ADD COLUMN health_home_soimain character varying(255) default '';
ALTER TABLE t_health_home ADD COLUMN health_home_telephone CHARACTER(20) DEFAULT '';
ALTER TABLE t_health_home ADD COLUMN health_home_latitude DOUBLE PRECISION;
ALTER TABLE t_health_home ADD COLUMN health_home_longitude DOUBLE PRECISION;



--ALTER TABLE t_accident
ALTER TABLE t_accident ADD COLUMN f_accident_place_id character varying(2)  NOT NULL default '99';
ALTER TABLE t_accident ADD COLUMN f_accident_visit_type_id character varying(2)  NOT NULL default '9';
ALTER TABLE t_accident ADD COLUMN accident_narcotic character varying(2)  NOT NULL default '9';
ALTER TABLE t_accident ADD COLUMN accident_airway character varying(2)  NOT NULL default '3';
ALTER TABLE t_accident ADD COLUMN accident_stopbleed character varying(2)  NOT NULL default '3';
ALTER TABLE t_accident ADD COLUMN accident_splint character varying(2)  NOT NULL default '3';
ALTER TABLE t_accident ADD COLUMN accident_fluid character varying(2)  NOT NULL default '3';
ALTER TABLE t_accident ADD COLUMN f_accident_symptom_eye_id character varying(2) NOT NULL default '4';
ALTER TABLE t_accident ADD COLUMN f_accident_symptom_speak_id character varying(2)  NOT NULL default '5';
ALTER TABLE t_accident ADD COLUMN f_accident_symptom_movement_id character varying(2)  NOT NULL default '6';

--UPDATE t_accident
update t_accident set f_accident_place_id = case when f_accident_road_type_id = '0'
                                                                            then '8' 
                                                                        when f_accident_road_type_id = '1'
                                                                            then '9' 
                                                                            else '99' end ;


--DROP f_accident_road_type 
ALTER TABLE t_accident DROP COLUMN f_accident_road_type_id;
DROP TABLE f_accident_road_type;

INSERT INTO f_accident_highway_inout_type VALUES ('9', 'ไม่ระบุ่');
INSERT INTO f_accident_victim_type VALUES ('8', 'อื่นๆ ');
INSERT INTO f_accident_patient_vechicle_type VALUES ('14', 'รถตู้');
INSERT INTO f_accident_patient_vechicle_type VALUES ('15', 'เรือโดยสาร');
INSERT INTO f_accident_patient_vechicle_type VALUES ('16', ' เรืออื่นๆ');
INSERT INTO f_accident_patient_vechicle_type VALUES ('17', ' อากาศยาน');
INSERT INTO f_accident_patient_vechicle_type VALUES ('98', ' อื่นๆ');

INSERT INTO f_diag_icd10_type VALUES ('6', 'Additional Code');
INSERT INTO f_diag_icd10_type VALUES ('7', 'Morphology Code');

CREATE TABLE f_visit_service_type (
    f_visit_service_type_id CHARACTER VARYING(2)  NOT NULL,
    visit_service_type_description CHARACTER VARYING(255),
    CONSTRAINT f_visit_service_type_pkey PRIMARY KEY (f_visit_service_type_id)
);
--insert f_transportation_type 
INSERT INTO f_visit_service_type VALUES ('1','มารับบริการเอง');
INSERT INTO f_visit_service_type VALUES ('2','มารับบริการตามนัดหมาย');
INSERT INTO f_visit_service_type VALUES ('3','ได้รับการส่งต่อจากสถานพยาบาลอื่น');
INSERT INTO f_visit_service_type VALUES ('4','ได้รับการส่งตัวจากบริการ EMS');

ALTER TABLE t_visit ADD COLUMN f_visit_service_type_id  character varying(2)  NOT NULL default '1';

update t_visit
set f_visit_service_type_id =  case when t_visit_refer_in_out.visit_refer_in_out_refer_hospital <> '' 
                                            and t_visit_refer_in_out.f_visit_refer_type_id = '0'
                                       then '3'
                                       else f_visit_service_type_id 
                                     end
from t_visit_refer_in_out 
where 
        t_visit_refer_in_out.visit_refer_in_out_refer_hospital <> ''
        and t_visit.t_visit_id = t_visit_refer_in_out.t_visit_id 
        and t_visit_refer_in_out.f_visit_refer_type_id = '0'
        and t_visit_refer_in_out.visit_refer_in_out_active = '1';



--nutrition
ALTER TABLE t_health_nutrition ADD COLUMN food character varying(255) default '';
ALTER TABLE t_health_nutrition ADD COLUMN bottle character varying(255) default '';
ALTER TABLE t_health_grow_history ADD COLUMN childdevelop character varying(1) default '1'; --1 = ปกติ, 2 = สงสัยช้ากว่าปกติ, 3 = ช้ากว่าปกติ

--newborn
ALTER TABLE t_health_pp ADD COLUMN tsh character varying(1) default '9'; --1 = ได้รับการตรวจ , 2 = ไม่ได้ตรวจ, 9 = ไม่ทราบ
ALTER TABLE t_health_pp ADD COLUMN tshresult decimal(5,1) ;

--newborncare
ALTER TABLE t_health_pp_care ADD COLUMN food character varying(1) default ''; --1 = นมแม่อย่างเดียว, 2 = นมแม่และน้ำ, 3 = นมแม่และนมผสม, 4 = นมผสมอย่างเดียว

--dental
ALTER TABLE t_health_dental ADD COLUMN f_dent_type_id character varying(2) default '5'; 
CREATE TABLE f_dent_type (
    f_dent_type_id character varying(2) NOT NULL,
    description character varying(255) NOT NULL,
    CONSTRAINT f_dent_type_pkey PRIMARY KEY (f_dent_type_id));
insert into f_dent_type values ('1', 'กลุ่มหญิงตั้งครรภ์');
insert into f_dent_type values ('2', 'กลุ่มเด็กก่อนวัยเรียน');
insert into f_dent_type values ('3', 'กลุ่มเด็กวัยเรียน');
insert into f_dent_type values ('4', 'กลุ่มผู้สูงอายุ');
insert into f_dent_type values ('5', 'กลุ่มอื่นๆ(นอกเหนือจาก 4 กลุ่มแรก)');
ALTER TABLE t_health_dental ADD COLUMN pfilling character varying(255) default ''; 
ALTER TABLE t_health_dental ADD COLUMN pextract character varying(255) default ''; 
ALTER TABLE t_health_dental ADD COLUMN dfilling character varying(255) default ''; 
ALTER TABLE t_health_dental ADD COLUMN dextract character varying(255) default ''; 
ALTER TABLE t_health_dental ADD COLUMN need_fluoride character varying(1) default '2'; 
ALTER TABLE t_health_dental ADD COLUMN need_scaling character varying(1) default '2'; 
ALTER TABLE t_health_dental ADD COLUMN need_sealant character varying(255) default ''; 
ALTER TABLE t_health_dental ADD COLUMN need_pfilling character varying(255) default ''; 
ALTER TABLE t_health_dental ADD COLUMN need_dfilling character varying(255) default ''; 
ALTER TABLE t_health_dental ADD COLUMN need_pextract character varying(255) default ''; 
ALTER TABLE t_health_dental ADD COLUMN need_dextract character varying(255) default ''; 
ALTER TABLE t_health_dental ADD COLUMN nprosthesis character varying(1) default '4'; 
ALTER TABLE t_health_dental ADD COLUMN permanent_perma character varying(255) default ''; 
ALTER TABLE t_health_dental ADD COLUMN permanent_prost character varying(255) default ''; 
ALTER TABLE t_health_dental ADD COLUMN prosthesis_prost character varying(255) default ''; 
ALTER TABLE t_health_dental ADD COLUMN f_gum_id character varying(2) default '9'; 
CREATE TABLE f_gum (
    f_gum_id character varying(2) NOT NULL,
    description character varying(255) NOT NULL,
    CONSTRAINT f_gum_pkey PRIMARY KEY (f_gum_id));
insert into f_gum values ('0', 'ปกติ');
insert into f_gum values ('1', 'มีเลือดออกภายหลังจากการตรวจด้วยเครื่องมือตรวจปริทันต์');
insert into f_gum values ('2', 'มีหินน้ำลาย แต่ยังเห็นแถบดำบนเครื่องมือ');
insert into f_gum values ('3', 'มีร่องลึกปริทันต์ 4–5 ม.ม.(ขอบเหงือกอยู่ภายในแถบดำ)');
insert into f_gum values ('4', 'มีร่องลึกปริทันต์ 6 ม.ม.หรือมากกว่า (มองไม่เห็นแถบดำบนเครื่องมือ)');
insert into f_gum values ('5', 'มีหินน้ำลายและมีเลือดออกภายหลังการจากตรวจด้วยเครื่องมือตรวจปริทันต์');
insert into f_gum values ('9', 'ตรวจไม่ได้/ไม่ตรวจ');

ALTER TABLE t_health_dental ADD COLUMN f_school_type_id character varying(2) default '0'; 

CREATE TABLE f_school_type (
    f_school_type_id character varying(2) NOT NULL,
    description character varying(255) NOT NULL,
    CONSTRAINT f_school_type_pkey PRIMARY KEY (f_school_type_id));

insert into f_school_type values ('0', 'ไม่ทราบ');	
insert into f_school_type values ('1', 'ศพด.');
insert into f_school_type values ('2', 'ประถมศึกษารัฐบาล');
insert into f_school_type values ('3', 'ประถมศึกษาเทศบาล');
insert into f_school_type values ('4', 'ประถมศึกษาท้องถิ่น');
insert into f_school_type values ('5', 'ประถมศึกษาเอกชน');
insert into f_school_type values ('6', 'มัธยมศึกษารัฐบาล');
insert into f_school_type values ('7', 'มัธยมศึกษาเทศบาล');
insert into f_school_type values ('8', 'มัธยมศึกษาท้องถิ่น');
insert into f_school_type values ('9', 'มัธยมศึกษาเอกชน');

ALTER TABLE t_health_dental ADD COLUMN school_class character varying(255) default ''; 

--community_service--
CREATE TABLE t_health_community_service (
    t_health_community_service_id character varying(255) NOT NULL,
    t_visit_id character varying(255) NOT NULL,
    f_comservice_id character varying(10) NOT NULL,
    start_date character varying(10) NOT NULL,
    record_date_time character varying(19) NOT NULL,
    staff_record character varying(255) NOT NULL,
    modify_date_time character varying(19)  NULL,
    staff_modify character varying(255)  NULL,
    cancel_date_time character varying(19)  NULL,
    staff_cancel character varying(255)  NULL,
    active character varying(1) NOT NULL,
CONSTRAINT t_health_community_service_pkey PRIMARY KEY (t_health_community_service_id));
--f_comservice--
CREATE TABLE f_comservice (
    f_comservice_id character varying(10) NOT NULL,
    code character varying(10) NOT NULL,
    description character varying(255) NOT NULL,
CONSTRAINT f_comservice_pkey PRIMARY KEY (f_comservice_id));
--insert comservice
insert into f_comservice values ('000001','1A01101','เยี่ยมผู้ป่วยโรคความดันโลหิตสูง');
insert into f_comservice values ('000002','1A01102','เยี่ยมผู้ป่วยโรคเบาหวาน ');
insert into f_comservice values ('000003','1A01103','เยี่ยมผู้ป่วยโรคมะเร็ง');
insert into f_comservice values ('000004','1A01104','เยี่ยมผู้ป่วยโรคระบบทางเดินหายใจ');
insert into f_comservice values ('000005','1A01105','เยี่ยมผู้ป่วยโรคสมองเสื่อม');
insert into f_comservice values ('000006','1A01106','เยี่ยมผู้ป่วยโรคหลอดเลือดสมอง');
insert into f_comservice values ('000007','1A01108','เยี่ยมผู้ป่วยโรคเรื้อรังอื่น ๆ');
insert into f_comservice values ('000008','1A01201','เยี่ยมผู้ป่วยโรคไข้เลือดออก');
insert into f_comservice values ('000009','1A01202','เยี่ยมผู้ป่วยโรคอุจจาระร่วง ');
insert into f_comservice values ('000010','1A01203','เยี่ยมผู้ป่วยโรคไข้หวัดใหญ่ 2009');
insert into f_comservice values ('000011','1A01204','เยี่ยมผู้ป่วยโรควัณโรค ');
insert into f_comservice values ('000012','1A01205','เยี่ยมผู้ป่วยโรคกามโรค/สัมผัสกามโรค');
insert into f_comservice values ('000013','1A01206','เยี่ยมผู้ป่วยโรคเอดส์');
insert into f_comservice values ('000014','1A01208','เยี่ยมผู้ป่วยโรคติดต่ออื่น ๆ');
insert into f_comservice values ('000015','1A01301','เยี่ยมผู้ป่วยโรคจิต');
insert into f_comservice values ('000016','1A01302','เยี่ยมผู้ป่วยโรควิตกกังวล');
insert into f_comservice values ('000017','1A01303','เยี่ยมผู้ป่วยโรคซึมเศร้า');
insert into f_comservice values ('000018','1A01304','เยี่ยมผู้ป่วยโรคปัญญาอ่อน');
insert into f_comservice values ('000019','1A01305','เยี่ยมผู้ป่วยโรคลมชัก');
insert into f_comservice values ('000020','1A01306','เยี่ยมผู้พยายามฆ่าตัวตาย');
insert into f_comservice values ('000021','1A01308','เยี่ยมผู้ป่วยโรคจิตและปัญหาสุขภาพจิตอื่นๆ');
insert into f_comservice values ('000022','1A01401','เยี่ยมผู้ป่วยที่ได้รับบาดเจ็บจากอุบัติเหตุ');
insert into f_comservice values ('000023','1A01402','เยี่ยมผู้ป่วยที่ได้รับบาดเจ็บจากการถูกทำร้ายโดยคน');
insert into f_comservice values ('000024','1A01403','เยี่ยมผู้ป่วยที่ได้รับบาดเจ็บจากการถูกทำร้ายโดยสัตว์');
insert into f_comservice values ('000025','1A01408','เยี่ยมผู้ป่วยที่ได้รับบาดเจ็บจากสาเหตุภายนอกอื่น ๆ');
insert into f_comservice values ('000026','1A02100','เยี่ยมผู้ป่วยหลังผ่าตัด');
insert into f_comservice values ('000027','1A02200','เยี่ยมผู้ป่วยที่มีใบติดตามจากโรงพยาบาล');
insert into f_comservice values ('000028','1A02300','เยี่ยมผู้ป่วยที่ต้องการดูแลอย่างต่อเนื่องจากโรงพยาบาล');
insert into f_comservice values ('000029','1A02400','เยี่ยมติดตามผู้ป่วยขาดนัด');
insert into f_comservice values ('000030','1A02500','เยี่ยมผู้ป่วยหลังจากรับ/ส่งต่อ (refer) จากโรงพยาบาล');
insert into f_comservice values ('000031','1A02800','เยี่ยมผู้ป่วยหลังได้รับการรักษาอื่นๆ');
insert into f_comservice values ('000032','1A03101','เยี่ยมหญิงตั้งครรภ์แรก');
insert into f_comservice values ('000033','1A03102','เยี่ยมหญิงตั้งครรภ์น้ำหนักน้อยกว่าเกณฑ์');
insert into f_comservice values ('000034','1A03103','เยี่ยมหญิงตั้งครรภ์ที่มีภาวะเสี่ยง ');
insert into f_comservice values ('000035','1A03104','เยี่ยมหญิงตั้งครรภ์ที่แยกทางกับสามี');
insert into f_comservice values ('000036','1A03108','เยี่ยมหญิงตั้งครรภ์กิจกรรมอื่นๆ');
insert into f_comservice values ('000037','1A03101','เยี่ยมหญิงตั้งครรภ์แรก');
insert into f_comservice values ('000038','1A03102','เยี่ยมหญิงตั้งครรภ์น้ำหนักน้อยกว่าเกณฑ์');
insert into f_comservice values ('000039','1A03103','เยี่ยมหญิงตั้งครรภ์ที่มีภาวะเสี่ยง');
insert into f_comservice values ('000040','1A03104','เยี่ยมหญิงตั้งครรภ์ที่แยกทางกับสามี');
insert into f_comservice values ('000041','1A03108','เยี่ยมหญิงตั้งครรภ์กิจกรรมอื่นๆ');
insert into f_comservice values ('000042','1A03201','เยี่ยมติดตามหญิงหลังคลอด ครั้งที่ 1 (1-2 สัปดาห์)');
insert into f_comservice values ('000043','1A03202','เยี่ยมติดตามหญิงหลังคลอด ครั้งที่ 2 (4-6 สัปดาห์)');
insert into f_comservice values ('000044','1A03203','เยี่ยมติดตามหญิงหลังคลอดที่มีภาวะผิดปกติ');
insert into f_comservice values ('000045','1A03208','เยี่ยมติดตามหญิงหลังคลอด กิจกรรมอื่นๆ');
insert into f_comservice values ('000046','1A03201','เยี่ยมติดตามทารกแรกเกิด ครั้งที่ 1 (1-2 สัปดาห์)');
insert into f_comservice values ('000047','1A03202','เยี่ยมติดตามทารกแรกเกิด ครั้งที่ 2 (4-6 สัปดาห์)');
insert into f_comservice values ('000048','1A03203','เยี่ยมติดตามทารกแรกเกิดที่มีภาวะผิดปกติ');
insert into f_comservice values ('000049','1A03208','เยี่ยมติดตามทารกแรกเกิด กิจกรรมอื่นๆ');
insert into f_comservice values ('000050','1A04100','เยี่ยมผู้พิการทางการมองเห็น');
insert into f_comservice values ('000051','1A04200','เยี่ยมผู้พิการทางการได้ยินหรือสื่อความหมาย');
insert into f_comservice values ('000052','1A04300','เยี่ยมผู้พิการทางการเคลื่อนไหวหรือทางร่างกาย');
insert into f_comservice values ('000053','1A04400','เยี่ยมผู้พิการทางจิตใจหรือพฤติกรรม หรือออทิสติก');
insert into f_comservice values ('000054','1A04500','เยี่ยมผู้พิการทางสติปัญญา');
insert into f_comservice values ('000055','1A04600','เยี่ยมผู้พิการทางการเรียนรู้');
insert into f_comservice values ('000056','1A04900','เยี่ยมผู้ที่ความพิการยังไม่ได้รับการวินิจฉัยยืนยัน');
insert into f_comservice values ('000057','1A05101','เยี่ยมเด็กอายุ 0 – 5 ปี ขาดอาหาร เด็กอ้วน กลุ่มเสี่ยง(ค่อนข้างเตี้ย ค่อนข้างผอม น้ำหนักค่อนข้างน้อย ท้วม)');
insert into f_comservice values ('000058','1A05102','เยี่ยมเด็กอ้วนอายุ 0 – 5 ปี ');
insert into f_comservice values ('000059','1A05103','เยี่ยมเด็กกลุ่มเสี่ยง(ค่อนข้างเตี้ย ค่อนข้างผอม น้ำหนักค่อนข้างน้อย ท้วม)');
insert into f_comservice values ('000060','1A05104','เยี่ยมเด็กอายุ 0 – 5 ปี ที่มีการเจริญเติบโตดีแต่แนวโน้มการเจริญเติบโตไม่ดี');
insert into f_comservice values ('000061','1A05105','เยี่ยมเด็กอายุ 0 – 5 ปี เพื่อติดตามพัฒนาการเด็กที่บกพร่อง / พัฒนาการไม่สมวัย');
insert into f_comservice values ('000062','1A05106','เยี่ยมเด็กอายุ 0 – 5 ปี เพื่อติดตามเด็กที่มีโรคประจำตัว/ โรคร้ายแรง/ โรคที่ต้องเฝ้าระวัง');
insert into f_comservice values ('000063','1A05107','เยี่ยมเด็กอายุ 0 – 5 ปี เพื่อติดตามเด็กให้ได้รับวัคซีนครบตามเกณฑ์/ เด็กขาดการรับวัคซีนตามนัด');
insert into f_comservice values ('000064','1A05108','เยี่ยมเด็กอายุ 0 – 5 ปี เพื่อแนะนำผู้ปกครอง เรื่อง การแปรงฟันที่ถูกต้อง ด้วยยาสีฟันผสมฟลูออไรด์');
insert into f_comservice values ('000065','1A05109','เยี่ยมเด็กอายุ 0 – 5 ปี กิจกรรมการให้บริการอื่น');
insert into f_comservice values ('000066','1A05201','เยี่ยมเด็กกลุ่มวัยเรียนอายุ 6 - 18 ปี และเด็กวัยรุ่นขาดอาหาร ');
insert into f_comservice values ('000067','1A05202','เยี่ยมเด็กกลุ่มวัยเรียนอายุ 6 - 18 ปี และเด็กวัยรุ่น ที่เป็นเด็กอ้วน');
insert into f_comservice values ('000068','1A05203','เยี่ยมเด็กกลุ่มวัยเรียนอายุ 6 - 18 ปี และเด็กวัยรุ่น อยู่ในกลุ่มเสี่ยง (ค่อนข้างเตี้ย ค่อนข้างผอม น้ำหนักค่อนข้างน้อย ท้วม)');
insert into f_comservice values ('000069','1A05204','เยี่ยมเด็กกลุ่มวัยเรียนอายุ 6 - 18 ปี และเด็กวัยรุ่น ที่มีการเจริญเติบโตดีแต่แนวโน้มการเจริญเติบโตไม่ดี');
insert into f_comservice values ('000070','1A05208','เยี่ยมเด็กกลุ่มวัยเรียนอายุ 6 - 18 ปี และเด็กวัยรุ่น กิจกรรมการให้บริการอื่น');
insert into f_comservice values ('000071','1A05301','เยี่ยมกลุ่มวัยแรงงานเพื่อติดตามภาวะเสี่ยงในการทำงาน ด้านการเกษตร');
insert into f_comservice values ('000072','1A05302','เยี่ยมกลุ่มวัยแรงงานเพื่อติดตามภาวะเสี่ยงในการทำงาน ในโรงงานอุตสาหกรรม');
insert into f_comservice values ('000073','1A05303','เยี่ยมกลุ่มวัยแรงงานเพื่อติดตามภาวะเสี่ยงในการทำงานด้านอุตสาหกรรมในครัวเรือน');
insert into f_comservice values ('000074','1A05308','เยี่ยมกลุ่มวัยแรงงานเพื่อติดตามภาวะเสี่ยงในการทำงานด้านอื่น ๆ');
insert into f_comservice values ('000075','1A05401','เยี่ยมผู้สูงอายุที่อยู่เพียงลำพัง');
insert into f_comservice values ('000076','1A05409','เยี่ยมผู้สูงอายุทั่วไป');
insert into f_comservice values ('000077','1A06100','เยี่ยมและเยียวยาผู้ประสบภัยพิบัติทางธรรมชาติ');
insert into f_comservice values ('000078','1A06200','เยี่ยมและเยียวยาผู้ประสบภัยพิบัติที่เกิดจากไฟ');
insert into f_comservice values ('000079','1A06300','เยี่ยมและเยียวยาผู้ประสบภัยพิบัติทางสงคราม');
insert into f_comservice values ('000080','1A06400','เยี่ยมและเยียวยาผู้ประสบภัยพิบัติจากการเข้าแทรกแซงตามกฎหมาย');
insert into f_comservice values ('000081','1A06500','เยี่ยมและเยียวยาผู้ประสบภัยพิบัติจากการก่อการร้าย/จราจล');
insert into f_comservice values ('000082','1A06800','เยี่ยมและเยียวยาผู้ประสบภัยพิบัติอื่น ๆ');
insert into f_comservice values ('000083','1A06900','เยี่ยมและเยียวยาผู้ประสบภัยพิบัติที่ไม่ระบุรายละเอียด  ');
insert into f_comservice values ('000084','1A09100','เยี่ยมบ้านประชาชนสุขภาพดีเพื่อกิจกรรมการให้บริการเฉพาะเรื่อง');
insert into f_comservice values ('000085','1A09900','เยี่ยมบ้านประชาชนสุขภาพดีทั่วไป  ');
insert into f_comservice values ('000086','1B01101','การตรวจคัดกรองความเสี่ยง / โรคเบาหวาน');
insert into f_comservice values ('000087','1B01102','การตรวจคัดกรองความเสี่ยง / โรคความดันโลหิตสูง');
insert into f_comservice values ('000088','1B01103','การตรวจคัดกรองความเสี่ยง / โรคหัวใจและหลอดเลือด');
insert into f_comservice values ('000089','1B01104','การตรวจคัดกรองความเสี่ยง / โรคมะเร็งเต้านม');
insert into f_comservice values ('000090','1B01105','การตรวจคัดกรองความเสี่ยง / โรคมะเร็งปากมดลูก');
insert into f_comservice values ('000091','1B01106','การตรวจคัดกรองความเสี่ยง / โรคธาลาสซีเมีย');
insert into f_comservice values ('000092','1B01108','การตรวจคัดกรองความเสี่ยง / โรคเรื้อรัง อื่นๆ');
insert into f_comservice values ('000093','1B01200','การตรวจคัดกรองความเสี่ยง / โรคติดต่อ');
insert into f_comservice values ('000094','1B01301','การตรวจคัดกรองโรคจิต');
insert into f_comservice values ('000095','1B01302','การตรวจคัดกรองโรคซึมเศร้า');
insert into f_comservice values ('000096','1B01303','การตรวจคัดกรองโรคออทิสติก ในเด็กอายุ 0 – 6 ปี');
insert into f_comservice values ('000097','1B01304','การตรวจคัดกรองภาวะเครียด');
insert into f_comservice values ('000098','1B01305','ดัชนีชี้วัดสุขภาพจิตคนไทย');
insert into f_comservice values ('000099','1B01306','ดัชนีชี้วัดความสุขคนไทย');
insert into f_comservice values ('000100','1B01308','การตรวจคัดกรองความเสี่ยง / โรคทางจิตเวช อื่น ๆ');
insert into f_comservice values ('000101','1B02101','การตรวจคัดกรองเพื่อค้นหาเด็กพิการทางกายและทางจิต');
insert into f_comservice values ('000102','1B02102','การตรวจคัดกรองเพื่อค้นหาเด็กพัฒนาการช้า');
insert into f_comservice values ('000103','1B02103','การตรวจคัดกรองเพื่อค้นหาเด็กกระดูกสันหลังคด');
insert into f_comservice values ('000104','1B02104','การตรวจคัดกรองเพื่อค้นหาเด็กอ้วนที่มีกิจกรรมทางกายต่ำ');
insert into f_comservice values ('000105','1B02108','การตรวจคัดกรองความเสี่ยง / เฝ้าระวัง ในเด็ก อื่น ๆ');
insert into f_comservice values ('000106','1B02201','การตรวจคัดกรองเพื่อค้นหาผู้ที่อาจมีภาวะเสี่ยงต่อการบาดเจ็บจากการทำงาน');
insert into f_comservice values ('000107','1B02202','การตรวจคัดกรองเพื่อค้นหาคนทำงานที่มีกิจกรรมทางกายต่ำ');
insert into f_comservice values ('000108','1B02203','การตรวจคัดกรองเพื่อติดตามโรคทางจิต');
insert into f_comservice values ('000109','1B02208','การตรวจคัดกรองความเสี่ยง / เฝ้าระวัง ในประชากรวัยแรงงาน อื่น ๆ');
insert into f_comservice values ('000110','1B02301','การตรวจคัดกรองเพื่อค้นหาผู้สูงอายุที่เสี่ยงต่อการหกล้ม');
insert into f_comservice values ('000111','1B02302','การตรวจคัดกรองเพื่อค้นหาผู้สูงอายุที่มีกิจกรรมทางกายต่ำ');
insert into f_comservice values ('000112','1B02308','การตรวจคัดกรองความเสี่ยง / เฝ้าระวังในผู้สูงอายุ  อื่น ๆ');
insert into f_comservice values ('000113','1C01100','การปรับเปลี่ยนพฤติกรรมเพื่อการเลิกสุรา');
insert into f_comservice values ('000114','1C01200','การปรับเปลี่ยนพฤติกรรมเพื่อการเลิกบุหรี่');
insert into f_comservice values ('000115','1C01300','การปรับเปลี่ยนพฤติกรรมเพื่อการเลิกยาเสพติด');
insert into f_comservice values ('000116','1C02100','การฝึกทักษะการออกกำลังกาย');
insert into f_comservice values ('000117','1C02200','การฝึกทักษะการออกกำลังกายเพื่อการฟื้นฟูสภาพ');
insert into f_comservice values ('000118','1C03100','การติดตามพฤติกรรมเสี่ยงในการบริโภคของกลุ่มวัยแรงงาน');
insert into f_comservice values ('000119','1C04000','การปรับเปลี่ยนพฤติกรรมความเสี่ยงโรคความดันโลหิตสูง');
insert into f_comservice values ('000120','1C05000','การปรับเปลี่ยนพฤติกรรมความเสี่ยงโรคเบาหวาน');
insert into f_comservice values ('000121','1D01100','ทาฟลูออไรด์ในเด็กอายุ 0-2 ปี');
insert into f_comservice values ('000122','1D01200','แจกแปรงสีฟันในเด็กอายุ 18 เดือน');
insert into f_comservice values ('000123','1D01300','ให้ทันตสุขศึกษาหญิงตั้งครรภ์');
insert into f_comservice values ('000124','1D01400','สอน/แนะนำ ให้ผู้ดูแลหรือผู้พิการ แปรงฟัน ถูกวิธี ตามสภาพของตัวผู้พิการด้วยยาสีฟัน ผสมฟลูออไรด์');
insert into f_comservice values ('000125','1D01500','สอน/แนะนำ ให้ผู้ดูแลหรือผู้สูงอายุ แปรงฟัน ถูกวิธี ตามสภาพของตัวผู้สูงอายุด้วยยาสีฟันผสมฟลูออไรด์');
insert into f_comservice values ('000126','1D01600','ฝึกแปรงฟันด้วยยาสีฟันผสมฟลูออไรด์โดยผู้ปกครองแปรงให้เด็กเป็นรายคน');
insert into f_comservice values ('000127','1D01800','การให้บริการทันตสาธารณสุขอื่น ๆ');
insert into f_comservice values ('000128','1D02100','ตรวจรอยโรคฟันผุในเด็กอายุ 3-5 ปี');
insert into f_comservice values ('000129','1D02200','ตรวจความสะอาดช่องปากในเด็กอายุ 3-5 ปี');
insert into f_comservice values ('000130','1D02300','ตรวจรอยโรคฟันผุหญิงตั้งครรภ์');
insert into f_comservice values ('000131','1D02400','ตรวจสภาพเหงือกหญิงตั้งครรภ์');
insert into f_comservice values ('000132','1D02800','ตรวจสุขภาพช่องปากอื่น ๆ');
insert into f_comservice values ('000133','1D03100','การตรวจ และคัดกรองรอยโรคก่อมะเร็งในช่องปาก');
insert into f_comservice values ('000134','1D03200','การตรวจรอยโรคมะเร็งในช่องปาก');
insert into f_comservice values ('000135','1D03800','การตรวจ และคัดกรองมะเร็งช่องปากอื่น ๆ');
insert into f_comservice values ('000136','1E01100','การเฝ้าระวังโภชนาการในหญิงตั้งครรภ์');
insert into f_comservice values ('000137','1E01200','การประเมินสารไอโอดีนในหญิงตั้งครรภ์');
insert into f_comservice values ('000138','1E02100','การเฝ้าระวังการเจริญเติบโตของเด็กในกลุ่มอายุ 0-5 ปี');
insert into f_comservice values ('000139','1E02200','การตรวจคัดกรองภาวะอ้วนในประชาชนอายุ 15 ปีขึ้นไป โดยการวัดเส้นรอบเอวหรือประเมินค่าดัชนีมวลกาย');
insert into f_comservice values ('000140','1E02300','การตรวจคัดกรองภาวะอ้วนในประชาชนอายุ 15 ปีขึ้นไป ที่มีภาวะอ้วนลงพุง (โดยการวัดเส้นรอบเอว)');
insert into f_comservice values ('000141','1E02400','การตรวจคัดกรองภาวะอ้วนในประชาชนอายุ 15 ปีขึ้นไป ที่มีภาวะอ้วน (ประเมินค่าดัชนีมวลกาย)');
insert into f_comservice values ('000142','1E02800','กิจกรรมโภชนาการเฉพาะเรื่อง / เฉพาะกลุ่มอื่น ๆ');
insert into f_comservice values ('000143','1F01100','การให้บริการปรึกษาทางด้านสุขภาพจิตทางโทรศัพท์');
insert into f_comservice values ('000144','1F01800','การให้บริการปรึกษาทางด้านจิตเวชอื่น ๆ');
insert into f_comservice values ('000145','1F02100','การให้บริการปรึกษาผู้ที่ติดสุราเพื่อลด/เลิกสุรา');
insert into f_comservice values ('000146','1F02200','การให้บริการปรึกษาผู้ที่ติดบุหรี่เพื่อลด/เลิกบุหรี่');
insert into f_comservice values ('000147','1F02300','การให้บริการปรึกษาผู้ที่ติดยาเสพติดเพื่อลด/เลิกยาเสพติด');
insert into f_comservice values ('000148','1F02800','การให้บริการปรึกษาทางด้านสารเสพติดอื่น ๆ');
insert into f_comservice values ('000149','1F03100','การให้บริการปรึกษาเรื่องเพศสัมพันธ์ ');
insert into f_comservice values ('000150','1F03200','การให้บริการปรึกษาเรื่องปัญหาครอบครัว');
insert into f_comservice values ('000151','1F03800','การให้บริการปรึกษาในวัยรุ่นเรื่องสุขภาพอื่น ๆ');
insert into f_comservice values ('000152','1F04000','การให้บริการปรึกษาทางการแพทย์และสังคมแก่ผู้ป่วยกามโรค');
insert into f_comservice values ('000153','1F05000','การให้บริการปรึกษาทางด้านการวางแผนครอบครัว');
insert into f_comservice values ('000154','1F08000','การให้บริการปรึกษาทางด้านสุขภาพอื่น');
insert into f_comservice values ('000155','1G01100','การให้บริการกิจกรรมกายภาพบำบัด');
insert into f_comservice values ('000156','1G01200','การให้บริการผู้ป่วยร่วมกับหน่วยงานอื่น / สหวิชาชีพ');
insert into f_comservice values ('000157','1G02101','การให้คำแนะนำญาติที่ดูแลผู้ป่วยใส่สายให้อาหาร (NG tube)');
insert into f_comservice values ('000158','1G02102','การให้คำแนะนำญาติที่ดูแลผู้ป่วยใส่ท่อหลอดลม คอ และการดูดเสมหะ');
insert into f_comservice values ('000159','1G02103','การให้คำแนะนำญาติที่ดูแลผู้ป่วยใส่สายสวนปัสสาวะ');
insert into f_comservice values ('000160','1G02104','การให้คำแนะนำญาติที่ดูแลผู้ป่วยที่มียาฉีด เช่น อินซูลิน ฯลฯ');
insert into f_comservice values ('000161','1G02105','การให้คำแนะนำญาติที่ดูแลผู้ป่วยที่มีบาดแผล');
insert into f_comservice values ('000162','1G02108','การให้คำแนะนำญาติที่ดูแลผู้ป่วยอื่น ๆ');
insert into f_comservice values ('000163','1H01101','ตรวจรอยโรคฟันผุในนักเรียนชั้นประถมศึกษาปีที่ 1 (อายุ 6-7 ปี)');
insert into f_comservice values ('000164','1H01102','ตรวจรอยโรคฟันผุในนักเรียนชั้นประถมศึกษาปีที่ 3 (อายุ 7-8 ปี)');
insert into f_comservice values ('000165','1H01201','การให้บริการ Need for Sealant ในนักเรียนชั้นประถมศึกษาปีที่ 1 (อายุ 6-7 ปี)');
insert into f_comservice values ('000166','1H01301','การให้บริการ Need for E-application ในนักเรียนชั้นประถมศึกษาปีที่ 1 (อายุ 6-7 ปี)');
insert into f_comservice values ('000167','1H01401','ตรวจการยึดติดไม่สมบูรณ์ของสารเคลือบหลุมร่องฟัน ในนักเรียนชั้นประถมศึกษา (อายุ 6-12 ปี)');
insert into f_comservice values ('000168','1H01501','ตรวจเหงือกตามระบบเฝ้าระวังทันตสุขภาพ ในนักเรียนชั้นประถมศึกษา (อายุ 6-12 ปี)');
insert into f_comservice values ('000169','1H01502','ตรวจเหงือกตามระบบเฝ้าระวังทันตสุขภาพ ในนักเรียนชั้นมัธยมศึกษา (อายุ 13-18 ปี)');
insert into f_comservice values ('000170','1H01601','ตรวจฟันผุตามระบบเฝ้าระวังทันตสุขภาพ ในนักเรียนชั้นประถมศึกษา (อายุ 6-12 ปี)');
insert into f_comservice values ('000171','1H01602','ตรวจฟันผุตามระบบเฝ้าระวังทันตสุขภาพ ในนักเรียนชั้นมัธยมศึกษา (อายุ 13-18 ปี)');
insert into f_comservice values ('000172','1H01701','ตรวจฟันตกกระตามระบบเฝ้าระวังทันตสุขภาพ ในนักเรียนชั้นประถมศึกษา (อายุ 6-12 ปี)');
insert into f_comservice values ('000173','1H01702','ตรวจฟันตกกระตามระบบเฝ้าระวังทันตสุขภาพ ในนักเรียนชั้นมัธยมศึกษา (อายุ 13-18 ปี)');
insert into f_comservice values ('000174','1H01801','ตรวจสุขภาพช่องปากตามเกณฑ์โรงเรียนส่งเสริมสุขภาพในนักเรียนชั้นประถมศึกษา (อายุ 6-12 ปี)');
insert into f_comservice values ('000175','1H01802','ตรวจสุขภาพช่องปากตามเกณฑ์โรงเรียนส่งเสริมสุขภาพในนักเรียนชั้นมัธยมศึกษา (อายุ 13-18 ปี)');
insert into f_comservice values ('000176','1H01900','การให้บริการสุขภาพช่องปากอื่น ๆ');
insert into f_comservice values ('000177','1H02100','ติดตามภาวะทางจิตในกลุ่มวัยเรียนอายุ 6 - 18 ปี และเด็กวัยรุ่น ');
insert into f_comservice values ('000178','1H02800','การให้บริการทางด้านจิตเวชอื่น ๆ');
insert into f_comservice values ('000179','1H03101','ตรวจหูในนักเรียนชั้นประถมศึกษา');
insert into f_comservice values ('000180','1H03102','ตรวจหูในนักเรียนชั้นมัธยมศึกษา');
insert into f_comservice values ('000181','1H03103','แก้ไขโรคของหูในนักเรียนชั้นประถมศึกษา');
insert into f_comservice values ('000182','1H03104','แก้ไขโรคของหูในนักเรียนชั้นมัธยมศึกษา');
insert into f_comservice values ('000183','1H03105','ตรวจการได้ยินในนักเรียนชั้นประถมศึกษา');
insert into f_comservice values ('000184','1H03106','ตรวจการได้ยินในนักเรียนชั้นมัธยมศึกษา');
insert into f_comservice values ('000185','1H03107','แก้ไขอาการผิดปกติของการได้ยินในนักเรียนชั้นประถมศึกษา');
insert into f_comservice values ('000186','1H03108','แก้ไขอาการผิดปกติของการได้ยินในนักเรียนชั้นมัธยมศึกษา');
insert into f_comservice values ('000187','1H03201','ตรวจตาในนักเรียนชั้นประถมศึกษา');
insert into f_comservice values ('000188','1H03202','ตรวจตาในนักเรียนชั้นมัธยมศึกษา');
insert into f_comservice values ('000189','1H03203','แก้ไขโรคตาในนักเรียนชั้นประถมศึกษา');
insert into f_comservice values ('000190','1H03204','แก้ไขโรคตาในนักเรียนชั้นมัธยมศึกษา');
insert into f_comservice values ('000191','1H03205','ตรวจสายตาในนักเรียนชั้นประถมศึกษา');
insert into f_comservice values ('000192','1H03206','ตรวจสายตาในนักเรียนชั้นมัธยมศึกษา');
insert into f_comservice values ('000193','1H03207','แก้ไขอาการสายตาผิดปกติในนักเรียนชั้นประถมศึกษา');
insert into f_comservice values ('000194','1H03208','แก้ไขอาการสายตาผิดปกติในนักเรียนชั้นมัธยมศึกษา');
insert into f_comservice values ('000195','1H03301','นักเรียนที่ได้รับยาถ่ายพยาธิ');
insert into f_comservice values ('000196','1H03302','นักเรียนที่ได้รับการตรวจเหาและได้รับการแก้ไข');
insert into f_comservice values ('000197','1H03308','นักเรียนที่ได้รับการตรวจสุขภาพประจำปีอื่น ๆ');
insert into f_comservice values ('000198','1H04101','นักเรียนได้รับการเฝ้าระวังการเจริญเติบโต');
insert into f_comservice values ('000199','1H04102','นักเรียนได้รับการตรวจภาวะโลหิตจาง');
insert into f_comservice values ('000200','1H04103','นักเรียนได้รับการตรวจคอพอก');
insert into f_comservice values ('000201','1H04108','นักเรียนได้รับการเฝ้าระวังการเจริญเติบโตอื่น ๆ');
insert into f_comservice values ('000202','1I01100','ผู้ป่วยได้รับการนวดเพื่อการรักษาที่บ้าน');
insert into f_comservice values ('000203','1I01200','ประชาชนได้รับการนวดเพื่อการส่งเสริมสุขภาพที่บ้าน');
insert into f_comservice values ('000204','1I01300','ผู้ป่วยได้รับการประคบสมุนไพรเพื่อการรักษาที่บ้าน');
insert into f_comservice values ('000205','1I01400','ผู้ป่วยได้รับการอบสมุนไพรเพื่อการรักษาที่บ้าน');
insert into f_comservice values ('000206','1I01500','ประชาชนได้รับการอบสมุนไพรเพื่อการส่งเสริมสุขภาพที่บ้าน');
insert into f_comservice values ('000207','1I01600','หญิงหลังคลอดได้รับการอบสมุนไพรที่บ้าน');
insert into f_comservice values ('000208','1I01700','หญิงหลังคลอดได้รับการทับหม้อเกลือที่บ้าน');
insert into f_comservice values ('000209','1I01800','การให้คำแนะนำ การสอน สาธิตด้านการแพทย์แผนไทย');
insert into f_comservice values ('000210','1I01900','การให้บริการการแพทย์แผนไทยอื่น ๆ ที่บ้าน');
insert into f_comservice values ('000211','1J01100','ให้ความรู้/ สุขศึกษาเกี่ยวกับโรคและการป้องกันโรคต่าง ๆ ');
insert into f_comservice values ('000212','1J01200','ให้ความรู้/ สุขศึกษาเกี่ยวกับโรคตามฤดูกาล');
insert into f_comservice values ('000213','1J01300','แนะนำ / ให้ความรู้ เรื่องการใช้ยา');
insert into f_comservice values ('000214','1J01400','ให้ความรู้และจัดกิจกรรมการออกกำลังกายที่ถูกต้องและเหมาะสม');
insert into f_comservice values ('000215','1J01500','ให้สุขศึกษาในแหล่งแพร่กามโรค');
insert into f_comservice values ('000216','1J01600','ให้ความรู้ด้านโภชนาการ');
insert into f_comservice values ('000217','1J01800','ให้ความรู้/ สุขศึกษาเกี่ยวกับสุขภาพอื่น ๆ');
insert into f_comservice values ('000218','1J02100','ให้ความรู้/ สุขศึกษาแก่หญิงขายบริการ');
insert into f_comservice values ('000219','1J02200','ให้ความรู้เกี่ยวกับการดูแลเท้าในผู้ป่วยเบาหวาน');
insert into f_comservice values ('000220','1J02800','ให้ความรู้/ สุขศึกษาบุคคลเฉพาะกลุ่มอื่น ๆ');
insert into f_comservice values ('000221','1J03100','ให้ความรู้และจัดกิจกรรมออกกำลังกายที่เหมาะสมสำหรับผู้ป่วยโรคเรื้อรัง');
insert into f_comservice values ('000222','1J03200','ให้ความรู้และจัดกิจกรรมออกกำลังกายที่เหมาะสมสำหรับผู้ป่วยที่มีปัญหาทางระบบทางเดินหายใจ เช่น ปอดอุดกั้น รื้อรัง หอบหืด');
insert into f_comservice values ('000223','1J03300','ให้ความรู้และจัดกิจกรรมออกกำลังกายที่เหมาะสมสำหรับผู้ป่วยที่มีปัญหาทางระบบหัวใจหลอดเลือด');
insert into f_comservice values ('000224','1J03400','ให้ความรู้และจัดกิจกรรมออกกำลังกายที่เหมาะสมสำหรับเด็กอ้วนที่มีกิจกรรมทางกายต่ำ');
insert into f_comservice values ('000225','1J03500','ให้ความรู้และจัดกิจกรรมออกกำลังกายที่เหมาะสมสำหรับหญิงตั้งครรภ์และหลังคลอด');
insert into f_comservice values ('000226','1J03600','ให้ความรู้และจัดกิจกรรมออกกำลังกายที่เหมาะสมสำหรับประชากรกลุ่มเสี่ยงต่อการบาดเจ็บจากการทำงาน');
insert into f_comservice values ('000227','1J03700','ให้ความรู้และจัดกิจกรรมออกกำลังกายที่เหมาะสมสำหรับกลุ่มคนทำงานที่มีกิจกรรมทางกายต่ำ');
insert into f_comservice values ('000228','1J03800','ให้ความรู้และจัดกิจกรรมออกกำลังกายที่เหมาะสมสำหรับผู้สูงอายุที่เสี่ยงต่อการหกล้ม');
insert into f_comservice values ('000229','1J03900','ให้ความรู้บุคคลทั่วไปที่มีสุขภาพดีเกี่ยวกับท่าทางการทำงานและท่าทางในชีวิตประจำวัน');

delete from b_health_maim;
insert into b_health_maim values('785000000000000001','1','ความพิการทางการเห็น','1');
insert into b_health_maim values('785000000000000002','2','ความพิการทางการได้ยินหรือการสื่อความหมาย','1');
insert into b_health_maim values('785000000000000003','3','ความพิการการเคลื่อนไหวหรือทางร่างกาย','1');
insert into b_health_maim values('785000000000000004','4','ความพิการทางจิตใจหรือพฤติกรรมหรือออทิสติก','1');
insert into b_health_maim values('785000000000000005','5','ความพิการทางสติปัญญา','1');
insert into b_health_maim values('785000000000000006','6','ความพิการทางการเรียนรู้','1');

update t_health_maim set b_health_maim_id =case when b_health_maim_id = '785000000000000002'
                                                                                then '785000000000000003' 
                                                                            when b_health_maim_id = '785000000000000003'
                                                                                then '785000000000000004' 
                                                                            when b_health_maim_id = '785000000000000004'
                                                                                then '785000000000000005' end ;


--community_activity
CREATE TABLE t_health_community_activity (
    t_health_community_activity_id character varying(255) NOT NULL,
    vid character varying(255) NOT NULL,
    f_comactivity_id character varying(10) NOT NULL,
    date_start character varying(10) NOT NULL,
    date_finish character varying(10) NULL,
    moo character varying(3) NOT NULL,
    amphur character varying(6) NOT NULL,
    tambon character varying(6) NOT NULL,
    changwat character varying(6) NOT NULL,
    record_date_time character varying(19) NOT NULL,
    staff_record character varying(255)  NOT NULL,
    modify_date_time character varying(19)  NULL,
    staff_modify character varying(255)  NULL,
    cancel_date_time character varying(19)  NULL,
    staff_cancel character varying(255)  NULL,
    active character varying(1) NOT NULL,
CONSTRAINT t_health_community_activity_pkey PRIMARY KEY (t_health_community_activity_id));

--f_comactivity
CREATE TABLE f_comactivity (
    f_comactivity_id character varying(10) NOT NULL,
    code character varying(10) NOT NULL,
    description character varying(255) NOT NULL,
CONSTRAINT f_comactivity_pkey PRIMARY KEY (f_comactivity_id));
--insert comactivity
insert into f_comactivity values ('000001','2A01100','เก็บตัวอย่าง/ตรวจสารปนเปื้อนในอาหาร ผัก และผลไม้ทุกชนิด');
insert into f_comactivity values ('000002','2A01200','ตรวจร้านค้าของชำ');
insert into f_comactivity values ('000003','2A01300','ตรวจให้คำแนะนำสถานที่ผลิตอาหาร');
insert into f_comactivity values ('000004','2A01400','ตรวจสถานที่จำหน่ายบุหรี่/แอลกอฮอล์');
insert into f_comactivity values ('000005','2A01500','ตรวจร้านก๋วยเตี๋ยว/หม้อก๋วยเตี๋ยว');
insert into f_comactivity values ('000006','2A01800','กิจกรรมการให้บริการชุมชนด้านการคุ้มครองผู้บริโภคอื่น ๆ');
insert into f_comactivity values ('000007','2B01100','การกำจัดน้ำเสีย/ขยะ/มูลสัตว์');
insert into f_comactivity values ('000008','2B01200','ตรวจโรงฆ่าสัตว์');
insert into f_comactivity values ('000009','2B01300','ตรวจเรือนจำ');
insert into f_comactivity values ('000010','2B01400','ตรวจคุณภาพน้ำ');
insert into f_comactivity values ('000011','2B01500','ตรวจโรงงาน / ตรวจสุขภาพพนักงาน');
insert into f_comactivity values ('000012','2B01600','ตรวจเยี่ยมสถานประกอบกิจการ');
insert into f_comactivity values ('000013','2B01700','การพัฒนา/การจัดการส้วมให้ถูกสุขลักษณะ');
insert into f_comactivity values ('000014','2B01800','กิจกรรมการให้บริการชุมชนอื่น ๆ ด้านการสุขาภิบาลสิ่งแวดล้อม');
insert into f_comactivity values ('000015','2C01100','ตรวจสุขาภิบาลร้านอาหารในตลาดสด');
insert into f_comactivity values ('000016','2C01200','ตรวจสุขาภิบาลร้านอาหารแผงลอย');
insert into f_comactivity values ('000017','2C01300','ตรวจสุขาภิบาลร้านอาหารในโรงเรียน');
insert into f_comactivity values ('000018','2C01800','ตรวจสุขาภิบาลอาหารอื่น ๆ');
insert into f_comactivity values ('000019','2D01101','การพ่นหมอกควันเพื่อกำจัดยุงลาย');
insert into f_comactivity values ('000020','2C01102','การหยอดทรายอะเบทเพื่อกำจัดยุงลาย');
insert into f_comactivity values ('000021','2C01103','รณรงค์การเลี้ยงปลาหางนกยูงการกำจัดลูกน้ำ ยุงลาย');
insert into f_comactivity values ('000022','2C01104','รณรงค์การปิดภาชนะ/คว่ำภาชนะ เพื่อกำจัดยุงลาย');
insert into f_comactivity values ('000023','2D01200','การกำจัดแมลงและสัตว์นำโรคต่าง ๆ');
insert into f_comactivity values ('000024','2D01300','การสอบสวนโรคและควบคุมโรคระบาดในชุมชน');
insert into f_comactivity values ('000025','2D01400','ตรวจประเมินสถานการณ์การประสบภัยพิบัติในชุมชน');
insert into f_comactivity values ('000026','2D01800','กิจกรรมการให้บริการชุมชนอื่น ๆ ด้านการควบคุมป้องกันโรค');
insert into f_comactivity values ('000027','2E01100','การตรวจสอบคุณภาพเกลือเสริมไอโอดีนในชุมชน');
insert into f_comactivity values ('000028','2E01200','จัดนิทรรศการด้านโภชนาการ');
insert into f_comactivity values ('000029','2E01300','รณรงค์ด้านโภชนาการ');
insert into f_comactivity values ('000030','2E01400','ประกวดด้านโภชนาการ');
insert into f_comactivity values ('000031','2F01100','การให้ความรู้/ข้อมูลด้านโภชนาการ');
insert into f_comactivity values ('000032','2F01800','การให้ความรู้และสุขศึกษาเรื่องสุขภาพอื่น ๆ');
insert into f_comactivity values ('000033','2F02000','ออกหน่วยสุขศึกษาและหน่วยเคลื่อนที่ในชุมชน');
insert into f_comactivity values ('000034','2F03100','จัดอบรมให้ความรู้ด้านการส่งเสริมป้องกันโรคแก่ อสม. / แกนนำชุมชน');
insert into f_comactivity values ('000035','2F03200','จัดอบรมให้ความรู้ด้านการส่งเสริมป้องกันโรคแก่ประชาชนกลุ่มเป้าหมาย');
insert into f_comactivity values ('000036','2F03300','จัดอบรมให้ความรู้ด้านโภชนาการ');
insert into f_comactivity values ('000037','2F03400','จัดอบรมให้ความรู้ด้านทันตสุขภาพแก่แกนนำ/ กลุ่มผู้ป่วย/ ชมรม');
insert into f_comactivity values ('000038','2F03500','จัดอบรมให้ความรู้ด้านทันตสุขภาพแก่ /อสม./ผู้ดูแลเด็ก (ผดด.)');
insert into f_comactivity values ('000039','2F03600','จัดอบรมให้ความรู้ด้านทันตสุขภาพแก่แกนนำนักเรียน/ครู');
insert into f_comactivity values ('000040','2F03800','จัดอบรมให้ความรู้เรื่องสุขภาพอื่น ๆ');
insert into f_comactivity values ('000041','2G01101','ออกหน่วยบริการทางการแพทย์/ทันตกรรม เคลื่อนที่ในชุมชน');
insert into f_comactivity values ('000042','2G01102','ออกหน่วยบริการทางการแพทย์/ทันตกรรม พอ.สว. เคลื่อนที่ในชุมชน');
insert into f_comactivity values ('000043','2G02000','ออกหน่วยบริการสุขภาพในพื้นที่');





--provider
ALTER TABLE b_employee ADD COLUMN t_person_id character varying(255) default ''; 
ALTER TABLE b_employee ADD COLUMN provider character varying(255) default ''; 
ALTER TABLE b_employee ADD COLUMN council character varying(255) default ''; 
ALTER TABLE b_employee ADD COLUMN f_provider_type_id character varying(255) default ''; 
ALTER TABLE b_employee ADD COLUMN start_date character varying(255) default ''; 
ALTER TABLE b_employee ADD COLUMN out_date character varying(255) default ''; 
ALTER TABLE b_employee ADD COLUMN move_from character varying(255) default ''; 
ALTER TABLE b_employee ADD COLUMN move_to character varying(255) default '';


--create f_provider_type
CREATE TABLE f_provider_type (
    f_provider_type_id character varying(10) NOT NULL,
    code character varying(10) NOT NULL,
    description character varying(255) NOT NULL,
CONSTRAINT f_provider_type_pkey PRIMARY KEY (f_provider_type_id));

--insert f_provider_type
insert into f_provider_type values('000001','01','แพทย์');
insert into f_provider_type values('000002','02','ทันตแพทย์,');
insert into f_provider_type values('000003','03','พยาบาลวิชาชีพ (ที่ทำหน้าที่ตรวจรักษา)');
insert into f_provider_type values('000004','04',' เจ้าพนักงานสาธารณสุขชุมชน');
insert into f_provider_type values('000005','05',' นักวิชาการสาธารณสุข');
insert into f_provider_type values('000006','06','เจ้าพนักงานทันตสาธารณสุข');
insert into f_provider_type values('000007','07','อสม. (ผู้ให้บริการในชุมชน)');
insert into f_provider_type values('000008','08','บุคลากรแพทย์แผนไทย แพทย์พื้นบ้าน แพทย์ทางเลือก (ที่มีวุฒิการศึกษาหรือผ่านการอบรมตามเกณฑ์)');
insert into f_provider_type values('000009','09','อื่นๆ');

--create women
CREATE TABLE t_health_women (
    t_health_women_id character varying(255) NOT NULL,
    t_health_family_id character varying(255) NOT NULL,
    totalson character varying(255)  NULL,
    numberson character varying(255)  NULL,
    abortion character varying(255)  NULL,
    stillbirth character varying(255)  NULL,
    f_health_family_planing_method_id character varying(255)  NULL,
    f_health_family_planing_id character varying(255)  NULL,
    record_date_time character varying(19)  NOT NULL,
    staff_record character varying(255)  NOT NULL,
    update_date_time character varying(19)   NULL,
    staff_update character varying(255)   NULL,
    service_date character varying(10)   NULL,
    service_time character varying(10)   NULL,
    active character varying(1)  NOT NULL,
CONSTRAINT t_health_women_pkey PRIMARY KEY (t_health_women_id));

ALTER TABLE t_surveil ADD COLUMN user_record_id character varying(255)  default '';
ALTER TABLE t_surveil ADD COLUMN record_date_time character varying(19)  default '';
ALTER TABLE t_surveil ADD COLUMN user_modify_id character varying(255)  default '';
ALTER TABLE t_surveil ADD COLUMN modify_date_time character varying(19)  default '';

update t_health_village 
set village_record_date_time = 
				(case 
                    when village_record_date_time   <> ''
                        then   village_record_date_time
                    when village_record_date_time = '' and village_modify_date_time <> ''
                        then village_modify_date_time
                    when village_record_date_time = '' and village_modify_date_time = ''
                         then (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS')
                     end)
    ,village_modify_date_time = 
                (case
                    when village_modify_date_time   <> ''
                        then village_modify_date_time 
                    when village_record_date_time <> '' and village_modify_date_time = ''
                        then village_record_date_time
                    when village_record_date_time = '' and village_modify_date_time = ''
                         then (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS')
                     end);
					 

update t_health_home 
set home_record_date_time = 
                (case 
                    when home_record_date_time   <> ''
                        then home_record_date_time
                    when home_record_date_time = '' and home_modify_date_time <> ''
                        then home_modify_date_time
                    when home_record_date_time = '' and home_modify_date_time = ''
                         then (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS')
                     end) 
    ,home_modify_date_time = 
               (case
                    when home_modify_date_time   <> ''
                        then home_modify_date_time 
                    when home_record_date_time <> '' and home_modify_date_time = ''
                        then home_record_date_time
                    when home_record_date_time = '' and home_modify_date_time = ''
                         then (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS')
                     end);

update b_employee 
set record_date_time = 
                (case 
                    when record_date_time   <> ''
                        then record_date_time
                    when record_date_time = '' and update_date_time <> ''
                        then update_date_time
                    when record_date_time = '' and update_date_time = ''
                         then (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS')
                     end) 
    ,update_date_time = 
               (case
                    when update_date_time <> ''
                        then update_date_time 
                    when record_date_time <> '' and update_date_time = ''
                        then record_date_time
                    when record_date_time = '' and update_date_time = ''
                         then (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS')
                     end);


INSERT INTO s_version VALUES ('9701000000062', '62', 'Hospital OS, Community Edition', '3.9.29', '3.20.180212', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_29.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.29');
ALTER TABLE t_billing_receipt ADD COLUMN total_print integer DEFAULT 0;
ALTER TABLE t_billing_receipt ADD COLUMN cancel_reason text DEFAULT '';

alter table b_receipt_sequence drop column receipt_sequence_day; 
alter table b_receipt_sequence drop column receipt_sequence_month; 
alter table b_receipt_sequence drop column receipt_sequence_service_point_seq; 
alter table b_receipt_sequence drop column receipt_sequence_service_point;
alter table b_receipt_sequence drop column receipt_sequence_description;

ALTER TABLE b_service_point ADD COLUMN service_point_color character varying(255) DEFAULT 'r=255,g=255,b=255';


update t_billing_receipt set t_patient_id = substring(t_patient_id, 0, char_length(t_patient_id)-5), total_print = 1 where position('สำเนา' in t_patient_id) > 0;

update t_patient_risk_factor set risk_alcoho_advise = '0' where risk_alcoho_advise is null;
update t_patient_risk_factor set risk_cigarette_advise = '0' where risk_cigarette_advise is null;

update b_icd10 set active53 = '1' where icd10_number like 'U%';
update b_icd10 set active53 = '0' where icd10_number = 'U50';
update b_icd10 set active53 = '0' where icd10_number = 'U51';
update b_icd10 set active53 = '0' where icd10_number = 'U51.0';
update b_icd10 set active53 = '0' where icd10_number = 'U51.1';
update b_icd10 set active53 = '0' where icd10_number = 'U51.3';
update b_icd10 set active53 = '0' where icd10_number = 'U51.4';
update b_icd10 set active53 = '0' where icd10_number = 'U52';
update b_icd10 set active53 = '0' where icd10_number = 'U52.3';
update b_icd10 set active53 = '0' where icd10_number = 'U54';
update b_icd10 set active53 = '0' where icd10_number = 'U55';
update b_icd10 set active53 = '0' where icd10_number = 'U56';
update b_icd10 set active53 = '0' where icd10_number = 'U56.0';
update b_icd10 set active53 = '0' where icd10_number = 'U56.1';
update b_icd10 set active53 = '0' where icd10_number = 'U56.2';
update b_icd10 set active53 = '0' where icd10_number = 'U56.3';
update b_icd10 set active53 = '0' where icd10_number = 'U56.4';
update b_icd10 set active53 = '0' where icd10_number = 'U56.5';
update b_icd10 set active53 = '0' where icd10_number = 'U57';
update b_icd10 set active53 = '0' where icd10_number = 'U57.2';
update b_icd10 set active53 = '0' where icd10_number = 'U57.3';
update b_icd10 set active53 = '0' where icd10_number = 'U57.4';
update b_icd10 set active53 = '0' where icd10_number = 'U57.5';
update b_icd10 set active53 = '0' where icd10_number = 'U57.6';
update b_icd10 set active53 = '0' where icd10_number = 'U58';
update b_icd10 set active53 = '0' where icd10_number = 'U58.0';
update b_icd10 set active53 = '0' where icd10_number = 'U58.1';
update b_icd10 set active53 = '0' where icd10_number = 'U58.2';
update b_icd10 set active53 = '0' where icd10_number = 'U58.3';
update b_icd10 set active53 = '0' where icd10_number = 'U58.4';
update b_icd10 set active53 = '0' where icd10_number = 'U58.49';
update b_icd10 set active53 = '0' where icd10_number = 'U58.6';
update b_icd10 set active53 = '0' where icd10_number = 'U58.9';
update b_icd10 set active53 = '0' where icd10_number = 'U59';
update b_icd10 set active53 = '0' where icd10_number = 'U59.0';
update b_icd10 set active53 = '0' where icd10_number = 'U59.1';
update b_icd10 set active53 = '0' where icd10_number = 'U59.2';
update b_icd10 set active53 = '0' where icd10_number = 'U59.3';
update b_icd10 set active53 = '0' where icd10_number = 'U59.4';
update b_icd10 set active53 = '0' where icd10_number = 'U59.5';
update b_icd10 set active53 = '0' where icd10_number = 'U59.6';
update b_icd10 set active53 = '0' where icd10_number = 'U59.8';
update b_icd10 set active53 = '0' where icd10_number = 'U60';
update b_icd10 set active53 = '0' where icd10_number = 'U60.0';
update b_icd10 set active53 = '0' where icd10_number = 'U60.1';
update b_icd10 set active53 = '0' where icd10_number = 'U60.2';
update b_icd10 set active53 = '0' where icd10_number = 'U61';
update b_icd10 set active53 = '0' where icd10_number = 'U61.1';
update b_icd10 set active53 = '0' where icd10_number = 'U61.3';
update b_icd10 set active53 = '0' where icd10_number = 'U62';
update b_icd10 set active53 = '0' where icd10_number = 'U63';
update b_icd10 set active53 = '0' where icd10_number = 'U64';
update b_icd10 set active53 = '0' where icd10_number = 'U65';
update b_icd10 set active53 = '0' where icd10_number = 'U65.3';
update b_icd10 set active53 = '0' where icd10_number = 'U65.7';
update b_icd10 set active53 = '0' where icd10_number = 'U65.8';
update b_icd10 set active53 = '0' where icd10_number = 'U66';
update b_icd10 set active53 = '0' where icd10_number = 'U66.2';
update b_icd10 set active53 = '0' where icd10_number = 'U66.3';
update b_icd10 set active53 = '0' where icd10_number = 'U66.4';
update b_icd10 set active53 = '0' where icd10_number = 'U66.5';
update b_icd10 set active53 = '0' where icd10_number = 'U66.6';
update b_icd10 set active53 = '0' where icd10_number = 'U66.7';
update b_icd10 set active53 = '0' where icd10_number = 'U66.8';
update b_icd10 set active53 = '0' where icd10_number = 'U67';
update b_icd10 set active53 = '0' where icd10_number = 'U68';
update b_icd10 set active53 = '0' where icd10_number = 'U69';
update b_icd10 set active53 = '0' where icd10_number = 'U69.0';
update b_icd10 set active53 = '0' where icd10_number = 'U69.8';
update b_icd10 set active53 = '0' where icd10_number = 'U70';
update b_icd10 set active53 = '0' where icd10_number = 'U70.0';
update b_icd10 set active53 = '0' where icd10_number = 'U70.1';
update b_icd10 set active53 = '0' where icd10_number = 'U70.2';
update b_icd10 set active53 = '0' where icd10_number = 'U70.3';
update b_icd10 set active53 = '0' where icd10_number = 'U70.4';
update b_icd10 set active53 = '0' where icd10_number = 'U70.5';
update b_icd10 set active53 = '0' where icd10_number = 'U70.6';
update b_icd10 set active53 = '0' where icd10_number = 'U70.7';
update b_icd10 set active53 = '0' where icd10_number = 'U70.8';
update b_icd10 set active53 = '0' where icd10_number = 'U71';
update b_icd10 set active53 = '0' where icd10_number = 'U71.0';
update b_icd10 set active53 = '0' where icd10_number = 'U71.1';
update b_icd10 set active53 = '0' where icd10_number = 'U71.2';
update b_icd10 set active53 = '0' where icd10_number = 'U71.3';
update b_icd10 set active53 = '0' where icd10_number = 'U71.4';
update b_icd10 set active53 = '0' where icd10_number = 'U71.5';
update b_icd10 set active53 = '0' where icd10_number = 'U71.6';
update b_icd10 set active53 = '0' where icd10_number = 'U71.7';
update b_icd10 set active53 = '0' where icd10_number = 'U71.8';
update b_icd10 set active53 = '0' where icd10_number = 'U72';
update b_icd10 set active53 = '0' where icd10_number = 'U74';
update b_icd10 set active53 = '0' where icd10_number = 'U74.0';
update b_icd10 set active53 = '0' where icd10_number = 'U75';
update b_icd10 set active53 = '0' where icd10_number = 'U75.0';
update b_icd10 set active53 = '0' where icd10_number = 'U75.1';
update b_icd10 set active53 = '0' where icd10_number = 'U75.2';
update b_icd10 set active53 = '0' where icd10_number = 'U75.4';

CREATE TABLE t_billing_receipt_book_seq
(
   book_number integer NOT NULL, 
   current_seq integer NOT NULL, 
   CONSTRAINT t_billing_receipt_book_seq_pkey PRIMARY KEY (book_number)
);

CREATE INDEX billing_billing_id
  ON t_billing
  USING btree
  (t_billing_id);
CREATE INDEX billing_patient_id
  ON t_billing
  USING btree
  (t_patient_id);
CREATE INDEX billing_visit_id
  ON t_billing
  USING btree
  (t_visit_id);
CREATE INDEX billing_invoice_billing_invoice
  ON t_billing_invoice
  USING btree
  (t_billing_invoice_id);
CREATE INDEX billing_invoice_invoice_no_key
  ON t_billing_invoice
  USING btree
  (billing_invoice_number);
CREATE INDEX billing_invoice_patient_id_key
  ON t_billing_invoice
  USING btree
  (t_patient_id);
CREATE INDEX billing_invoice_visit_id_key
  ON t_billing_invoice
  USING btree
  (t_visit_id);
CREATE INDEX billing_invoice_subgroup_billin
  ON t_billing_invoice_billing_subgroup
  USING btree
  (b_item_billing_subgroup_id);
CREATE INDEX billing_invoice_subgroup_catego
  ON t_billing_invoice_billing_subgroup
  USING btree
  (b_item_subgroup_id);
CREATE INDEX billing_invoice_subgroup_patien
  ON t_billing_invoice_billing_subgroup
  USING btree
  (t_patient_id);
CREATE INDEX billing_invoice_subgroup_visit_
  ON t_billing_invoice_billing_subgroup
  USING btree
  (t_visit_id);
CREATE INDEX billing_invoice_item_billing_in
  ON t_billing_invoice_item
  USING btree
  (t_billing_invoice_id);
CREATE INDEX billing_invoice_item_item_id_ke
  ON t_billing_invoice_item
  USING btree
  (b_item_id);
CREATE INDEX billing_invoice_item_order_item
  ON t_billing_invoice_item
  USING btree
  (t_order_item_id);
CREATE INDEX billing_invoice_item_patient_id
  ON t_billing_invoice_item
  USING btree
  (t_patient_id);
CREATE INDEX billing_invoice_item_visit_id_k
  ON t_billing_invoice_item
  USING btree
  (t_visit_id);
CREATE UNIQUE INDEX tc_receipt581
  ON t_billing_receipt
  USING btree
  (t_billing_receipt_id);
CREATE UNIQUE INDEX tc_receipt_subgroup582
  ON t_billing_receipt_billing_subgroup
  USING btree
  (t_billing_receipt_billing_subgroup_id);
CREATE INDEX receipt_item_receipt_item_id_key
  ON t_billing_receipt_item
  USING btree
  (t_billing_receipt_item_id);
CREATE INDEX cronic_cronic_id_key
  ON t_chronic
  USING btree
  (t_chronic_id);
CREATE INDEX cronic_id_cronic_key
  ON t_chronic
  USING btree
  (t_chronic_id);
CREATE INDEX death_id_death_key
  ON t_death
  USING btree
  (t_death_id);
CREATE UNIQUE INDEX tc_death526
  ON t_death
  USING btree
  (t_death_id);
CREATE INDEX diag_icd10_diag_icd10_id
  ON t_diag_icd10
  USING btree
  (t_diag_icd10_id);
CREATE INDEX diag_icd10_icd10_code
  ON t_diag_icd10
  USING btree
  (diag_icd10_number);
CREATE INDEX diag_icd10_type
  ON t_diag_icd10
  USING btree
  (f_diag_icd10_type_id);
CREATE INDEX diag_icd10_vn
  ON t_diag_icd10
  USING btree
  (diag_icd10_vn);
CREATE UNIQUE INDEX tc_diag_icd10527
  ON t_diag_icd10
  USING btree
  (t_diag_icd10_id);
CREATE INDEX diag_icd9_diag_icd9_id
  ON t_diag_icd9
  USING btree
  (t_diag_icd9_id);
CREATE INDEX diag_icd9_icd9_code
  ON t_diag_icd9
  USING btree
  (diag_icd9_icd9_number);
CREATE INDEX diag_icd9_vn
  ON t_diag_icd9
  USING btree
  (diag_icd9_vn);
CREATE UNIQUE INDEX tc_diag_icd9528
  ON t_diag_icd9
  USING btree
  (t_diag_icd9_id);
CREATE INDEX icd9_id_participate_or_key
  ON t_diag_icd9_participate_or
  USING btree
  (b_icd9_id);
CREATE INDEX participate_or_employee_key
  ON t_diag_icd9_participate_or
  USING btree
  (diag_icd9_participate_or_staff_participate);
CREATE INDEX participate_or_vn_key
  ON t_diag_icd9_participate_or
  USING btree
  (diag_icd9_participate_or_vn);
CREATE INDEX drugfund_drugfund_id_key
  ON t_drugfund
  USING btree
  (t_drugfund_id);
CREATE INDEX drugfund_billing_drugfund_billi
  ON t_drugfund_billing
  USING btree
  (t_drugfund_billing_id);
CREATE INDEX drugfund_billing_item_drugfund_
  ON t_drugfund_billing_item
  USING btree
  (t_drugfund_billing_item_id);
CREATE INDEX drugfund_receipt_drugfund_recei
  ON t_drugfund_billing_receipt
  USING btree
  (t_drugfund_billing_receipt_id);
CREATE INDEX drugfund_receipt_item_drugfund_
  ON t_drugfund_billing_receipt_item
  USING btree
  (t_drugfund_billing_receipt_item_id);
CREATE INDEX drugfund_order_drugfund_order_i
  ON t_drugfund_order_item
  USING btree
  (t_drugfund_order_item_id);
CREATE INDEX drugfund_visit_drugfund_visit_i
  ON t_drugfund_visit
  USING btree
  (t_drugfund_visit_id);
CREATE UNIQUE INDEX agr_primary_key
  ON t_health_agr
  USING btree
  (t_health_agr_id);
CREATE INDEX agr_village_id
  ON t_health_agr
  USING btree
  (t_health_village_id);
CREATE UNIQUE INDEX agr_history_primary_key
  ON t_health_agr_history
  USING btree
  (t_health_agr_history_id);
CREATE INDEX agr_id
  ON t_health_agr_history
  USING btree
  (t_health_agr_id);
CREATE INDEX anc_family_id
  ON t_health_anc
  USING btree
  (t_health_family_id);
CREATE INDEX anc_patient_id
  ON t_health_anc
  USING btree
  (t_patient_id);
CREATE INDEX anc_pregnancy_id
  ON t_health_anc
  USING btree
  (t_health_pregnancy_id);
CREATE UNIQUE INDEX anc_primary_key
  ON t_health_anc
  USING btree
  (t_health_anc_id);
CREATE INDEX anc_visit_id
  ON t_health_anc
  USING btree
  (t_visit_id);
CREATE INDEX anc_detail_anc_id
  ON t_health_anc_detail
  USING btree
  (t_health_anc_id);
CREATE INDEX anc_detail_family_id
  ON t_health_anc_detail
  USING btree
  (t_health_family_id);
CREATE INDEX anc_detail_patient_id
  ON t_health_anc_detail
  USING btree
  (t_patient_id);
CREATE INDEX anc_detail_pregnancy_id
  ON t_health_anc_detail
  USING btree
  (t_health_pregnancy_id);
CREATE UNIQUE INDEX anc_detail_primary_key
  ON t_health_anc_detail
  USING btree
  (t_health_anc_detail_id);
CREATE INDEX anc_detail_visit_id
  ON t_health_anc_detail
  USING btree
  (t_visit_id);
CREATE INDEX check_body_office_id
  ON t_health_check_body
  USING btree
  (b_visit_refer_office_id);
CREATE UNIQUE INDEX check_body_primary_key
  ON t_health_check_body
  USING btree
  (t_health_check_body_id);
CREATE INDEX check_body_school_id
  ON t_health_check_body
  USING btree
  (t_health_visit_school_id);
CREATE INDEX check_body_student_id
  ON t_health_check_body
  USING btree
  (t_health_student_id);
CREATE INDEX check_ears_office_id
  ON t_health_check_ears
  USING btree
  (b_visit_refer_office_id);
CREATE UNIQUE INDEX check_ears_primary_key
  ON t_health_check_ears
  USING btree
  (t_health_check_ears_id);
CREATE INDEX check_ears_school_id
  ON t_health_check_ears
  USING btree
  (t_health_visit_school_id);
CREATE INDEX check_ears_student_id
  ON t_health_check_ears
  USING btree
  (t_health_student_id);
CREATE INDEX check_eyes_office_id
  ON t_health_check_eyes
  USING btree
  (b_visit_refer_office_id);
CREATE UNIQUE INDEX check_eyes_primary_key
  ON t_health_check_eyes
  USING btree
  (t_health_check_eyes_id);
CREATE INDEX check_eyes_school_id
  ON t_health_check_eyes
  USING btree
  (t_health_visit_school_id);
CREATE INDEX check_eyes_student_id
  ON t_health_check_eyes
  USING btree
  (t_health_student_id);
CREATE INDEX check_blood_office_id
  ON t_health_check_fe_blood
  USING btree
  (b_visit_refer_office_id);
CREATE UNIQUE INDEX check_blood_primary_key
  ON t_health_check_fe_blood
  USING btree
  (t_health_check_fe_blood_id);
CREATE INDEX check_blood_school_id
  ON t_health_check_fe_blood
  USING btree
  (t_health_visit_school_id);
CREATE INDEX check_blood_student_id
  ON t_health_check_fe_blood
  USING btree
  (t_health_student_id);
CREATE INDEX check_goiter_office_id
  ON t_health_check_goiter
  USING btree
  (b_visit_refer_office_id);
CREATE UNIQUE INDEX check_goiter_primary_key
  ON t_health_check_goiter
  USING btree
  (t_health_check_goiter_id);
CREATE INDEX check_goiter_school_id
  ON t_health_check_goiter
  USING btree
  (t_health_visit_school_id);
CREATE INDEX check_goiter_student_id
  ON t_health_check_goiter
  USING btree
  (t_health_student_id);
CREATE INDEX check_health_year_activity_id
  ON t_health_check_health_year
  USING btree
  (b_health_check_health_year_activity_id);
CREATE INDEX check_health_year_family_id
  ON t_health_check_health_year
  USING btree
  (t_health_family_id);
CREATE INDEX check_health_year_patient_id
  ON t_health_check_health_year
  USING btree
  (t_patient_id);
CREATE UNIQUE INDEX check_health_year_primary_key
  ON t_health_check_health_year
  USING btree
  (t_health_check_health_year_id);
CREATE INDEX check_health_year_visit_id
  ON t_health_check_health_year
  USING btree
  (t_visit_id);
CREATE INDEX check_healthy_age_survey_id
  ON t_health_check_healthy
  USING btree
  (b_health_age_survey_id);
CREATE INDEX check_healthy_family_id
  ON t_health_check_healthy
  USING btree
  (t_health_family_id);
CREATE INDEX check_healthy_patient_id
  ON t_health_check_healthy
  USING btree
  (t_patient_id);
CREATE UNIQUE INDEX check_healthy_primary_key
  ON t_health_check_healthy
  USING btree
  (t_health_check_healthy_id);
CREATE INDEX check_healthy_survey_id
  ON t_health_check_healthy
  USING btree
  (b_health_survey_id);
CREATE INDEX check_healthy_visit_id
  ON t_health_check_healthy
  USING btree
  (t_visit_id);
CREATE INDEX check_lice_office_id
  ON t_health_check_lice
  USING btree
  (b_visit_refer_office_id);
CREATE UNIQUE INDEX check_lice_primary_key
  ON t_health_check_lice
  USING btree
  (t_health_check_lice_id);
CREATE INDEX check_lice_school_id
  ON t_health_check_lice
  USING btree
  (t_health_visit_school_id);
CREATE INDEX check_lice_student_id
  ON t_health_check_lice
  USING btree
  (t_health_student_id);
CREATE INDEX check_nutrition_office_id
  ON t_health_check_nutrition
  USING btree
  (b_visit_refer_office_id);
CREATE UNIQUE INDEX check_nutrition_primary_key
  ON t_health_check_nutrition
  USING btree
  (t_health_check_nutrition_id);
CREATE INDEX check_nutrition_school_id
  ON t_health_check_nutrition
  USING btree
  (t_health_visit_school_id);
CREATE INDEX check_nutrition_student_id
  ON t_health_check_nutrition
  USING btree
  (t_health_student_id);
CREATE INDEX other_office_id
  ON t_health_check_other
  USING btree
  (b_visit_refer_office_id);
CREATE UNIQUE INDEX other_primary_key
  ON t_health_check_other
  USING btree
  (t_health_check_other_id);
CREATE INDEX other_school_id
  ON t_health_check_other
  USING btree
  (t_health_visit_school_id);
CREATE INDEX other_student_id
  ON t_health_check_other
  USING btree
  (t_health_student_id);
CREATE INDEX student_dental_office_id
  ON t_health_check_student_dental
  USING btree
  (b_visit_refer_office_id);
CREATE UNIQUE INDEX student_dental_primary_key
  ON t_health_check_student_dental
  USING btree
  (t_health_check_student_dental_id);
CREATE INDEX student_dental_school_id
  ON t_health_check_student_dental
  USING btree
  (t_health_visit_school_id);
CREATE INDEX student_dental_student_id
  ON t_health_check_student_dental
  USING btree
  (t_health_student_id);
CREATE INDEX student_health_office_id
  ON t_health_check_student_health
  USING btree
  (b_visit_refer_office_id);
CREATE UNIQUE INDEX student_health_primary_key
  ON t_health_check_student_health
  USING btree
  (t_health_check_student_health_id);
CREATE INDEX student_health_school_id
  ON t_health_check_student_health
  USING btree
  (t_health_visit_school_id);
CREATE INDEX student_health_student_id
  ON t_health_check_student_health
  USING btree
  (t_health_student_id);
CREATE INDEX thalassemia_office_id
  ON t_health_check_thalassemia
  USING btree
  (b_visit_refer_office_id);
CREATE UNIQUE INDEX thalassemia_primary_key
  ON t_health_check_thalassemia
  USING btree
  (t_health_check_thalassemia_id);
CREATE INDEX thalassemia_school_id
  ON t_health_check_thalassemia
  USING btree
  (t_health_visit_school_id);
CREATE INDEX thalassemia_student_id
  ON t_health_check_thalassemia
  USING btree
  (t_health_student_id);
CREATE INDEX worm_office_id
  ON t_health_check_worm
  USING btree
  (b_visit_refer_office_id);
CREATE UNIQUE INDEX worm_primary_key
  ON t_health_check_worm
  USING btree
  (t_health_check_worm_id);
CREATE INDEX worm_school_id
  ON t_health_check_worm
  USING btree
  (t_health_visit_school_id);
CREATE INDEX worm_student_id
  ON t_health_check_worm
  USING btree
  (t_health_student_id);
CREATE UNIQUE INDEX company_primary_key
  ON t_health_company
  USING btree
  (t_health_company_id);
CREATE INDEX company_village_id
  ON t_health_company
  USING btree
  (t_health_village_id);
CREATE INDEX company_history_company_id
  ON t_health_company_history
  USING btree
  (t_health_company_id);
CREATE INDEX company_history_company_type_id
  ON t_health_company_history
  USING btree
  (b_health_company_type_id);
CREATE UNIQUE INDEX company_history_primary_key
  ON t_health_company_history
  USING btree
  (t_health_company_history_id);
CREATE INDEX counsel_family_id
  ON t_health_counsel
  USING btree
  (t_health_family_id);
CREATE INDEX counsel_patient_id
  ON t_health_counsel
  USING btree
  (t_patient_id);
CREATE UNIQUE INDEX counsel_primary_key
  ON t_health_counsel
  USING btree
  (t_health_counsel_id);
CREATE INDEX counsel_type_id
  ON t_health_counsel
  USING btree
  (b_health_counsel_type_id);
CREATE INDEX counsel_visit_id
  ON t_health_counsel
  USING btree
  (t_visit_id);
CREATE INDEX delivery_family_id
  ON t_health_delivery
  USING btree
  (t_health_family_id);
CREATE INDEX delivery_icd10_id
  ON t_health_delivery
  USING btree
  (b_icd10_id);
CREATE INDEX delivery_patient_id
  ON t_health_delivery
  USING btree
  (t_patient_id);
CREATE UNIQUE INDEX delivery_primary_key
  ON t_health_delivery
  USING btree
  (t_health_delivery_id);
CREATE INDEX delivery_visit_id
  ON t_health_delivery
  USING btree
  (t_visit_id);
CREATE INDEX dental_family_id
  ON t_health_dental
  USING btree
  (t_health_family_id);
CREATE INDEX dental_patient_id
  ON t_health_dental
  USING btree
  (t_patient_id);
CREATE UNIQUE INDEX dental_primary_key
  ON t_health_dental
  USING btree
  (t_health_dental_id);
CREATE INDEX dental_visit_id
  ON t_health_dental
  USING btree
  (t_visit_id);
CREATE INDEX drug_history_drug_id
  ON t_health_drug_history
  USING btree
  (t_health_drug_id);
 CREATE UNIQUE INDEX drug_history_primary_key
  ON t_health_drug_history
  USING btree
  (t_health_drug_history_id);
CREATE INDEX drug_stock_item_id
  ON t_health_drug_stock
  USING btree
  (b_item_id);
CREATE INDEX drug_stock_office_id
  ON t_health_drug_stock
  USING btree
  (b_visit_office_id);
CREATE UNIQUE INDEX drug_stock_primary_key
  ON t_health_drug_stock
  USING btree
  (t_health_drug_stock_id);
CREATE INDEX efficiency_family_id
  ON t_health_efficiency
  USING btree
  (t_health_family_id);
CREATE INDEX efficiency_patient_id
  ON t_health_efficiency
  USING btree
  (t_patient_id);
CREATE UNIQUE INDEX efficiency_primary_key
  ON t_health_efficiency
  USING btree
  (t_health_efficiency_id);
CREATE INDEX efficiency_visit_id
  ON t_health_efficiency
  USING btree
  (t_visit_id);
CREATE INDEX epi_family_id
  ON t_health_epi
  USING btree
  (t_health_family_id);
CREATE INDEX epi_office_id
  ON t_health_epi
  USING btree
  (b_visit_office_id);
CREATE INDEX epi_patient_id
  ON t_health_epi
  USING btree
  (t_patient_id);
CREATE UNIQUE INDEX epi_primary_key
  ON t_health_epi
  USING btree
  (t_health_epi_id);
CREATE INDEX epi_visit_id
  ON t_health_epi
  USING btree
  (t_visit_id);
CREATE INDEX epi_detail_epi_id
  ON t_health_epi_detail
  USING btree
  (t_health_epi_id);
CREATE INDEX epi_detail_epi_set_id
  ON t_health_epi_detail
  USING btree
  (b_health_epi_set_id);
CREATE INDEX epi_detail_family_id
  ON t_health_epi_detail
  USING btree
  (t_health_family_id);
CREATE INDEX epi_detail_patient_id
  ON t_health_epi_detail
  USING btree
  (t_patient_id);
CREATE UNIQUE INDEX epi_detail_primary_key
  ON t_health_epi_detail
  USING btree
  (t_health_epi_detail_id);
CREATE INDEX epi_detail_visit_id
  ON t_health_epi_detail
  USING btree
  (t_visit_id);
CREATE INDEX epi_outsite_family_id
  ON t_health_epi_outsite
  USING btree
  (t_health_family_id);
CREATE INDEX epi_outsite_office_id
  ON t_health_epi_outsite
  USING btree
  (b_epi_outsite_office_id);
CREATE INDEX epi_outsite_patient_id
  ON t_health_epi_outsite
  USING btree
  (t_patient_id);
CREATE UNIQUE INDEX epi_outsite_primary_key
  ON t_health_epi_outsite
  USING btree
  (t_health_epi_outsite_id);
CREATE INDEX epi_visit_outsite_office_id
  ON t_health_epi_outsite
  USING btree
  (b_visit_office_id);
CREATE UNIQUE INDEX family_hcis
  ON t_health_family
  USING btree
  (health_family_hn_hcis);
CREATE INDEX family_health_family_active
  ON t_health_family
  USING btree
  (health_family_active);
CREATE INDEX family_home_id
  ON t_health_family
  USING btree
  (t_health_home_id);
CREATE INDEX patient_id
  ON t_health_family
  USING btree
  (t_patient_id);
CREATE INDEX pid
  ON t_health_family
  USING btree
  (patient_pid);
CREATE INDEX primary_key
  ON t_health_family
  USING btree
  (t_health_family_id);
CREATE INDEX family_planing_family_id
  ON t_health_family_planing
  USING btree
  (t_health_family_id);
CREATE INDEX family_planing_patient_id
  ON t_health_family_planing
  USING btree
  (t_patient_id);
CREATE UNIQUE INDEX family_planing_primary_key
  ON t_health_family_planing
  USING btree
  (t_health_family_planing_id);
CREATE INDEX family_planing_visit_id
  ON t_health_family_planing
  USING btree
  (t_visit_id);
CREATE INDEX grow_family_id
  ON t_health_grow
  USING btree
  (t_health_family_id);
CREATE INDEX grow_office_id
  ON t_health_grow
  USING btree
  (b_visit_office_id);
CREATE INDEX grow_patient_id
  ON t_health_grow
  USING btree
  (t_patient_id);
CREATE UNIQUE INDEX grow_primary_key
  ON t_health_grow
  USING btree
  (t_health_grow_id);
CREATE INDEX grow_visit_id
  ON t_health_grow
  USING btree
  (t_visit_id);
CREATE INDEX grow_history_family_id
  ON t_health_grow_history
  USING btree
  (t_health_family_id);
CREATE INDEX grow_history_grow_id
  ON t_health_grow_history
  USING btree
  (t_health_grow_id);
CREATE INDEX grow_history_patient_id
  ON t_health_grow_history
  USING btree
  (t_patient_id);
CREATE UNIQUE INDEX grow_history_primary_key
  ON t_health_grow_history
  USING btree
  (t_health_grow_history_id);
CREATE INDEX grow_history_visit_id
  ON t_health_grow_history
  USING btree
  (t_visit_id);
CREATE INDEX home_office_id
  ON t_health_home
  USING btree
  (b_visit_office_id);
CREATE UNIQUE INDEX home_primary_key
  ON t_health_home
  USING btree
  (t_health_home_id);
CREATE INDEX home_village_id
  ON t_health_home
  USING btree
  (t_health_village_id);
CREATE UNIQUE INDEX bug_control_primary_key
  ON t_health_home_bug_control
  USING btree
  (t_health_home_bug_control_id);
CREATE INDEX bug_control_sub_home_id
  ON t_health_home_bug_control
  USING btree
  (t_health_sub_home_id);
CREATE UNIQUE INDEX food_primary_key
  ON t_health_home_food_standard
  USING btree
  (t_health_home_food_standard_id);
CREATE INDEX food_sub_home_id
  ON t_health_home_food_standard
  USING btree
  (t_health_sub_home_id);
CREATE INDEX herb_home_id
  ON t_health_home_herb
  USING btree
  (t_health_home_id);
CREATE UNIQUE INDEX herb_primary_key
  ON t_health_home_herb
  USING btree
  (t_health_home_herb_id);
CREATE UNIQUE INDEX house_standard_primary_key
  ON t_health_home_house_standard
  USING btree
  (t_health_home_house_standard_id);
CREATE INDEX house_standard_sub_home_id
  ON t_health_home_house_standard
  USING btree
  (t_health_sub_home_id);
CREATE INDEX pet_home_id
  ON t_health_home_pet
  USING btree
  (t_health_home_id);
CREATE UNIQUE INDEX pet_primary_key
  ON t_health_home_pet
  USING btree
  (t_health_home_pet_id);
CREATE UNIQUE INDEX water_eradicate_primary_key
  ON t_health_home_water_eradicate
  USING btree
  (t_health_home_water_eradicate_id);
CREATE INDEX water_sub_home_id
  ON t_health_home_water_eradicate
  USING btree
  (t_health_sub_home_id);
CREATE INDEX maim_family_id
  ON t_health_maim
  USING btree
  (t_health_family_id);
CREATE INDEX maim_patient_id
  ON t_health_maim
  USING btree
  (t_patient_id);
CREATE UNIQUE INDEX maim_primary_key
  ON t_health_maim
  USING btree
  (t_health_maim_id);
CREATE INDEX maim_visit_id
  ON t_health_maim
  USING btree
  (t_visit_id);
CREATE UNIQUE INDEX market_primary_key
  ON t_health_market
  USING btree
  (t_health_market_id);
CREATE INDEX market_village_id
  ON t_health_market
  USING btree
  (t_health_village_id);
CREATE INDEX history_market_id
  ON t_health_market_history
  USING btree
  (t_health_market_id);
CREATE UNIQUE INDEX market_history_primary_key
  ON t_health_market_history
  USING btree
  (t_health_market_history_id);
CREATE INDEX nutrition_family_id
  ON t_health_nutrition
  USING btree
  (t_health_family_id);
CREATE INDEX nutrition_office_id
  ON t_health_nutrition
  USING btree
  (b_visit_office_id);
CREATE INDEX nutrition_patient_id
  ON t_health_nutrition
  USING btree
  (t_patient_id);
CREATE UNIQUE INDEX nutrition_primary_key
  ON t_health_nutrition
  USING btree
  (t_health_nutrition_id);
CREATE INDEX nutrition_visit_id
  ON t_health_nutrition
  USING btree
  (t_visit_id);
CREATE INDEX postpartum_delivery_id
  ON t_health_postpartum
  USING btree
  (t_health_delivery_id);
CREATE INDEX postpartum_family_id
  ON t_health_postpartum
  USING btree
  (t_health_family_id);
CREATE INDEX postpartum_patient_id
  ON t_health_postpartum
  USING btree
  (t_patient_id);
CREATE UNIQUE INDEX postpartum_primary_key
  ON t_health_postpartum
  USING btree
  (t_health_postpartum_id);
CREATE INDEX postpartum_visit_id
  ON t_health_postpartum
  USING btree
  (t_visit_id);
CREATE INDEX pp_family_id
  ON t_health_pp
  USING btree
  (t_health_family_id);
CREATE INDEX pp_patient_id
  ON t_health_pp
  USING btree
  (t_patient_id);
CREATE UNIQUE INDEX pp_primary_key
  ON t_health_pp
  USING btree
  (t_health_pp_id);
CREATE INDEX pp_visit_id
  ON t_health_pp
  USING btree
  (t_visit_id);
CREATE INDEX pp_care_family_id
  ON t_health_pp_care
  USING btree
  (t_health_family_id);
CREATE INDEX pp_care_patient_id
  ON t_health_pp_care
  USING btree
  (t_patient_id);
CREATE UNIQUE INDEX pp_care_primary_key
  ON t_health_pp_care
  USING btree
  (t_health_pp_care_id);
CREATE INDEX pp_care_visit_id
  ON t_health_pp_care
  USING btree
  (t_visit_id);
CREATE INDEX pregnancy_family_id
  ON t_health_pregnancy
  USING btree
  (t_health_family_id);
CREATE INDEX pregnancy_office_id
  ON t_health_pregnancy
  USING btree
  (b_visit_office_id);
CREATE INDEX pregnancy_patient_id
  ON t_health_pregnancy
  USING btree
  (t_patient_id);
CREATE UNIQUE INDEX pregnancy_primary_key
  ON t_health_pregnancy
  USING btree
  (t_health_pregnancy_id);
CREATE INDEX pregnancy_visit_id
  ON t_health_pregnancy
  USING btree
  (t_visit_id);
CREATE UNIQUE INDEX resource_primary_key
  ON t_health_resource
  USING btree
  (t_health_resource_id);
CREATE INDEX resource_village_id
  ON t_health_resource
  USING btree
  (t_health_village_id);
CREATE INDEX history_resource_id
  ON t_health_resource_history
  USING btree
  (t_health_resource_id);
CREATE UNIQUE INDEX resource_history_primary_key
  ON t_health_resource_history
  USING btree
  (t_health_resource_history_id);
CREATE UNIQUE INDEX school_primary_key
  ON t_health_school
  USING btree
  (t_health_school_id);
CREATE INDEX school_village_id
  ON t_health_school
  USING btree
  (t_health_village_id);
CREATE INDEX history_school_id
  ON t_health_school_history
  USING btree
  (t_health_school_id);
CREATE INDEX school_history_max_class_id
  ON t_health_school_history
  USING btree
  (b_health_school_max_class_id);
CREATE UNIQUE INDEX school_history_primary_key
  ON t_health_school_history
  USING btree
  (t_health_school_history_id);
CREATE UNIQUE INDEX student_primary_key
  ON t_health_student
  USING btree
  (t_health_student_id);
CREATE INDEX student_visit_school_id
  ON t_health_student
  USING btree
  (t_health_visit_school_id);
CREATE INDEX sub_home_home_id
  ON t_health_sub_home
  USING btree
  (t_health_home_id);
CREATE UNIQUE INDEX sub_home_primary_key
  ON t_health_sub_home
  USING btree
  (t_health_sub_home_id);
CREATE UNIQUE INDEX temple_primary_key
  ON t_health_temple
  USING btree
  (t_health_temple_id);
CREATE INDEX temple_village_id
  ON t_health_temple
  USING btree
  (t_health_village_id);
CREATE INDEX history_temple_id
  ON t_health_temple_history
  USING btree
  (t_health_temple_id);
CREATE UNIQUE INDEX temple_history_primary_key
  ON t_health_temple_history
  USING btree
  (t_health_temple_history_id);
CREATE INDEX detail_temple_history_id
  ON t_health_temple_history_detail
  USING btree
  (t_health_temple_history_id);
CREATE UNIQUE INDEX history_detail_primary_key
  ON t_health_temple_history_detail
  USING btree
  (t_health_temple_history_detail_id);
CREATE INDEX uncontagious_disease_id
  ON t_health_uncontagious
  USING btree
  (b_health_disease_id);
CREATE INDEX uncontagious_family_id
  ON t_health_uncontagious
  USING btree
  (t_health_family_id);
CREATE INDEX uncontagious_patient_id
  ON t_health_uncontagious
  USING btree
  (t_patient_id);
CREATE UNIQUE INDEX uncontagious_primary_key
  ON t_health_uncontagious
  USING btree
  (t_health_uncontagious_id);
CREATE INDEX uncontagious_visit_id
  ON t_health_uncontagious
  USING btree
  (t_visit_id);
CREATE INDEX village_location_id
  ON t_health_village
  USING btree
  (b_health_village_location_id);
CREATE UNIQUE INDEX village_primary_key
  ON t_health_village
  USING btree
  (t_health_village_id);
CREATE INDEX visit_home_family_id
  ON t_health_visit_home
  USING btree
  (t_health_family_id);
CREATE INDEX visit_home_patient_id
  ON t_health_visit_home
  USING btree
  (t_patient_id);
CREATE UNIQUE INDEX visit_home_primary_key
  ON t_health_visit_home
  USING btree
  (t_health_visit_home_id);
CREATE INDEX homevisit_vitalsign
  ON t_health_visit_home_vitalsign
  USING btree
  (t_health_visit_home_id);
CREATE UNIQUE INDEX homevisit_vitalsign_pk
  ON t_health_visit_home_vitalsign
  USING btree
  (t_health_visit_home_vitalsign_id);
CREATE UNIQUE INDEX visit_school_primary_key
  ON t_health_visit_school
  USING btree
  (t_health_visit_school_id);
CREATE INDEX visit_school_school_id
  ON t_health_visit_school
  USING btree
  (t_health_school_id);
CREATE UNIQUE INDEX water_primary_key
  ON t_health_water
  USING btree
  (t_health_water_id);
CREATE INDEX water_village_id
  ON t_health_water
  USING btree
  (t_health_village_id);
CREATE INDEX history_water_id
  ON t_health_water_history
  USING btree
  (t_health_water_id);
CREATE UNIQUE INDEX water_history_primary_key
  ON t_health_water_history
  USING btree
  (t_health_water_history_id);
CREATE INDEX order_item_labreferin_order_ite
  ON t_lab_refer_in_order
  USING btree
  (t_lab_refer_in_order_id);
CREATE INDEX order_result_labreferin_order_r
  ON t_lab_refer_in_order_result
  USING btree
  (t_lab_refer_in_order_result_id);
CREATE INDEX order_item_status_labreferin_or
  ON t_lab_refer_in_order_status
  USING btree
  (t_lab_refer_in_order_status_id);
CREATE INDEX patient_labreferin_patient_id_k
  ON t_lab_refer_in_patient
  USING btree
  (t_lab_refer_in_patient_id);
CREATE INDEX visit_labreferin_visit_labrefer
  ON t_lab_refer_in_visit
  USING btree
  (t_lab_refer_in_visit_id);
CREATE INDEX visit_status_labreferin_visit_s
  ON t_lab_refer_in_visit_status
  USING btree
  (t_lab_refer_in_visit_status_id);
CREATE INDEX order_item_labreferout_order_it
  ON t_lab_refer_out_order
  USING btree
  (t_lab_refer_out_order_id);
CREATE INDEX form_labreferout_form_labrefero
  ON t_lab_refer_out_order_form
  USING btree
  (t_lab_refer_out_order_form_id);
CREATE INDEX t_lis_ln_exec_datetime_idx
  ON t_lis_ln
  USING btree
  (exec_datetime);
CREATE INDEX t_lis_ln_lab_number_idx
  ON t_lis_ln
  USING btree
  (lab_number);
CREATE INDEX t_lis_ln_t_visit_id_idx
  ON t_lis_ln
  USING btree
  (t_visit_id);

CREATE INDEX t_lis_order_t_order_id_idx
  ON t_lis_order
  USING btree
  (t_order_id);
CREATE INDEX order_item_16_group
  ON t_order
  USING btree
  (b_item_16_group_id);
CREATE INDEX order_item_hn
  ON t_order
  USING btree
  (t_patient_id);
CREATE INDEX order_verify_date_time
  ON t_order
  USING btree
  (order_verify_date_time);
CREATE INDEX t_visit_id
  ON t_order
  USING btree
  (t_visit_id);
CREATE UNIQUE INDEX tc_order_item568
  ON t_order
  USING btree
  (t_order_id);
CREATE INDEX order_continue_order_continue_i
  ON t_order_continue
  USING btree
  (t_order_continue_id);
CREATE INDEX order_drug_active
  ON t_order_drug
  USING btree
  (order_drug_active);
CREATE INDEX t_order_id
  ON t_order_drug
  USING btree
  (t_order_id);
CREATE UNIQUE INDEX tc_order_item_drug569
  ON t_order_drug
  USING btree
  (t_order_drug_id);
CREATE INDEX hn_patient_key
  ON t_patient
  USING btree
  (patient_hn);
CREATE INDEX pid_patient_key
  ON t_patient
  USING btree
  (patient_pid);
CREATE INDEX pin_patient_key
  ON t_patient
  USING btree
  (patient_pid);
CREATE INDEX t_health_family_key
  ON t_patient
  USING btree
  (t_health_family_id);
CREATE UNIQUE INDEX tc_patient573
  ON t_patient
  USING btree
  (t_patient_id);
CREATE UNIQUE INDEX tc_appointment506
  ON t_patient_appointment
  USING btree
  (t_patient_appointment_id);
CREATE UNIQUE INDEX drug_allergy_id_drug_allergy_uk
  ON t_patient_drug_allergy
  USING btree
  (t_patient_drug_allergy_id);
CREATE INDEX t_patient_id
  ON t_patient_drug_allergy
  USING btree
  (t_patient_id);
CREATE INDEX hn_payment_patient_key
  ON t_patient_payment
  USING btree
  (t_patient_id);
CREATE INDEX patient_payment_contract_kid
  ON t_patient_payment
  USING btree
  (b_contact_id);
CREATE INDEX patient_payment_family_id
  ON t_patient_payment
  USING btree
  (t_health_family_id);
CREATE INDEX patient_payment_patient_id
  ON t_patient_payment
  USING btree
  (t_patient_id);
CREATE INDEX patient_payment_patient_payment_id
  ON t_patient_payment
  USING btree
  (t_patient_payment_id);
CREATE INDEX patient_payment_plan_kid
  ON t_patient_payment
  USING btree
  (b_contact_plans_id);
CREATE INDEX patient_xn_patient_id
  ON t_patient_xn
  USING btree
  (t_patient_id);
CREATE INDEX patient_xn_year
  ON t_patient_xn
  USING btree
  (patient_xn_year);
CREATE INDEX patient_xn_year
  ON t_patient_xn
  USING btree
  (patient_xn_year);
CREATE INDEX patient_xray_number
  ON t_patient_xn
  USING btree
  (patient_xray_number);
CREATE INDEX t_patient_xn_patient_id
  ON t_patient_xn
  USING btree
  (t_patient_id);
CREATE INDEX t_print_drug_pkey
  ON t_print_drug_sticker
  USING btree
  (print_drug_sticker_description);
CREATE INDEX t_print_point_pkey
  ON t_print_point
  USING btree
  (print_point_description);
CREATE INDEX result_lab_order
  ON t_result_lab
  USING btree
  (t_order_id, b_item_lab_result_id);
CREATE INDEX result_lab_result_item_id
  ON t_result_lab
  USING btree
  (b_item_id);
CREATE UNIQUE INDEX result_lab_result_lab_id_key
  ON t_result_lab
  USING btree
  (t_result_lab_id);
CREATE INDEX result_lab_result_visit_id
  ON t_result_lab
  USING btree
  (t_visit_id);
CREATE UNIQUE INDEX result_xray_result_xray_id_key
  ON t_result_xray
  USING btree
  (t_result_xray_id);
CREATE INDEX t_result_xray_patient_xn_id
  ON t_result_xray
  USING btree
  (t_patient_xn_id);
CREATE INDEX order_item_id_result_xray_posit
  ON t_result_xray_position
  USING btree
  (t_order_id);
CREATE INDEX result_xray_position_order_resu
  ON t_result_xray_position
  USING btree
  (t_result_xray_id);
CREATE INDEX result_xray_position_result_xra
  ON t_result_xray_position
  USING btree
  (t_result_xray_position_id);
CREATE INDEX result_xray_position_visit_id_k
  ON t_result_xray_position
  USING btree
  (t_visit_id);
CREATE INDEX surveil_id_surveil_key
  ON t_surveil
  USING btree
  (t_surveil_id);
CREATE INDEX t_patient_id_transfer_id
  ON t_transfer_file_patient
  USING btree
  (t_patient_id);
CREATE INDEX t_visit_id_tranfer_file_index
  ON t_transfer_file_patient
  USING btree
  (t_visit_id);
CREATE INDEX t_transfer_group_pkey
  ON t_transfer_group_file_patient
  USING btree
  (t_transfer_group_file_patient_id);
CREATE INDEX an_visit_key
  ON t_visit
  USING btree
  (visit_an);
CREATE UNIQUE INDEX tc_visit598
  ON t_visit
  USING btree
  (t_visit_id);
CREATE INDEX visit_an
  ON t_visit
  USING btree
  (visit_an);
CREATE INDEX visit_begin_visit_time
  ON t_visit
  USING btree
  (visit_begin_visit_time);
CREATE INDEX visit_doctor_discharge_datetime
  ON t_visit
  USING btree
  (visit_staff_doctor_discharge_date_time);
CREATE INDEX visit_hn
  ON t_visit
  USING btree
  (visit_hn);
CREATE INDEX visit_patient_id
  ON t_visit
  USING btree
  (t_patient_id);
CREATE INDEX visit_queue
  ON t_visit
  USING btree
  (visit_queue);
CREATE INDEX visit_visit_id
  ON t_visit
  USING btree
  (t_visit_id);
CREATE INDEX visit_visit_type
  ON t_visit
  USING btree
  (f_visit_type_id);
CREATE INDEX visit_vn
  ON t_visit
  USING btree
  (visit_vn);
CREATE INDEX map_visit_dx_dx_template_id_key
  ON t_visit_diag_map
  USING btree
  (b_template_dx_id);
CREATE INDEX map_visit_dx_visit_id_key
  ON t_visit_diag_map
  USING btree
  (t_visit_id);
CREATE INDEX visit_advice_id
  ON t_visit_diag_map
  USING btree
  (t_visit_discharge_advice_id);
CREATE INDEX payment_contract_kid
  ON t_visit_payment
  USING btree
  (b_contract_id);
CREATE INDEX payment_payment_id
  ON t_visit_payment
  USING btree
  (t_visit_payment_id);
CREATE INDEX payment_plan_kid
  ON t_visit_payment
  USING btree
  (b_contract_plans_id);
CREATE INDEX payment_visit_id
  ON t_visit_payment
  USING btree
  (t_visit_id);
CREATE UNIQUE INDEX tc_payment575
  ON t_visit_payment
  USING btree
  (t_visit_payment_id);
CREATE UNIQUE INDEX physical_exam_physical_exam_key
  ON t_visit_physical_exam
  USING btree
  (t_visit_physical_exam_id);
CREATE INDEX t_visit_primary_symptom_t_patient_id_key
  ON t_visit_primary_symptom
  USING btree
  (t_patient_id);
CREATE INDEX t_visit_primary_symptom_t_visit_id_key
  ON t_visit_primary_symptom
  USING btree
  (t_visit_id);
CREATE INDEX t_visit_primary_symptom_t_visit_primary_symptom_id_key
  ON t_visit_primary_symptom
  USING btree
  (t_visit_primary_symptom_id);
CREATE INDEX t_visit_primary_symptom_visit_primary_symptom_staff_cancel_key
  ON t_visit_primary_symptom
  USING btree
  (visit_primary_symptom_staff_cancel);
CREATE INDEX t_visit_primary_symptom_visit_primary_symptom_staff_modify_key
  ON t_visit_primary_symptom
  USING btree
  (visit_primary_symptom_staff_modify);
CREATE INDEX t_visit_primary_symptom_visit_primary_symptom_staff_record_key
  ON t_visit_primary_symptom
  USING btree
  (visit_primary_symptom_staff_record);
CREATE INDEX queue_icd_queue_icd_id_key
  ON t_visit_queue_coding
  USING btree
  (t_visit_queue_coding_id);
CREATE INDEX queue_despense_queue_despense_i
  ON t_visit_queue_despense
  USING btree
  (t_visit_queue_despense_id);
CREATE INDEX queue_lab_queue_lab_id_key
  ON t_visit_queue_lab
  USING btree
  (t_visit_queue_lab_id);
CREATE INDEX map_queue_visit_map_queue_visit
  ON t_visit_queue_map
  USING btree
  (t_visit_queue_map_id);
CREATE INDEX t_visit_id_map_q_i
  ON t_visit_queue_map
  USING btree
  (t_visit_id);
CREATE INDEX visit_queue_map_visit_id
  ON t_visit_queue_map
  USING btree
  (t_visit_id);
CREATE INDEX b_service_point_id_idex
  ON t_visit_queue_transfer
  USING btree
  (b_service_point_id);
CREATE INDEX t_patient_id_ide
  ON t_visit_queue_transfer
  USING btree
  (t_patient_id);
CREATE INDEX t_visit_id_idex
  ON t_visit_queue_transfer
  USING btree
  (t_visit_id);
CREATE INDEX t_visit_queue_transfer_index
  ON t_visit_queue_transfer
  USING btree
  (t_visit_queue_transfer_id);
CREATE INDEX visit_service_staff_doctor
  ON t_visit_queue_transfer
  USING btree
  (visit_service_staff_doctor);
CREATE INDEX queue_xray_queue_xray_id_key
  ON t_visit_queue_xray
  USING btree
  (t_visit_queue_xray_id);
CREATE INDEX refer_refer_id_key
  ON t_visit_refer_in_out
  USING btree
  (t_visit_refer_in_out_id);
CREATE INDEX reverse_admit_reverse_admit_id_
  ON t_visit_reverse_admit
  USING btree
  (t_visit_reverse_admit_id);
CREATE INDEX b_service_point_id_t_visit_service_key
  ON t_visit_service
  USING btree
  (b_service_point_id);
CREATE INDEX t_patient_id_t_visit_service_key
  ON t_visit_service
  USING btree
  (t_patient_id);
CREATE INDEX t_visit_id_t_visit_service_key
  ON t_visit_service
  USING btree
  (t_visit_id);
CREATE UNIQUE INDEX tc_tranferance594
  ON t_visit_service
  USING btree
  (t_visit_service_id);
CREATE UNIQUE INDEX tc_vital_sign601
  ON t_visit_vital_sign
  USING btree
  (t_visit_vital_sign_id);
CREATE INDEX visit_year
  ON t_visit_year
  USING btree
  (visit_year);
CREATE INDEX t_billing_id_fkey
  ON t_welfare_billtrans
  USING btree
  (t_billing_id);
CREATE INDEX t_patient_id_fkey
  ON t_welfare_directdraw_patient
  USING btree
  (t_patient_id);
CREATE INDEX t_welfare_billtrans_id_fkey
  ON t_welfare_opbills
  USING btree
  (t_welfare_billtrans_id);
CREATE INDEX wound_patient_id
  ON t_wound
  USING btree
  (t_visit_id);
CREATE INDEX accident_id_accident_key
  ON t_accident
  USING btree
  (t_accident_id);
CREATE UNIQUE INDEX tc_accident499
  ON t_accident
  USING btree
  (t_accident_id);

INSERT INTO s_version VALUES ('9701000000050', '50', 'Hospital OS, Community Edition', '3.9.17', '3.18.251011', '2554-10-25 00:00:00');

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_17.sql',(select current_date) || ','|| (select current_time),'ปรับแก้สำหรับhospitalOS3.9.17');
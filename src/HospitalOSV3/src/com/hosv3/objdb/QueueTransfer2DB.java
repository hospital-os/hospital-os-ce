/*
 * QueueTransfer2DB.java
 *
 * Created on 11 �ԧ�Ҥ� 2548, 18:56 �.
 */
package com.hosv3.objdb;

import com.hospital_os.objdb.QueueTransferDB;
import com.hospital_os.object.ListTransfer;
import com.hospital_os.object.VisitType;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.Gutil;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

/**
 *
 * @author kingland
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class QueueTransfer2DB extends QueueTransferDB {

    public ListTransfer dbObj2;

    /**
     * Creates a new instance of QueueTransfer2DB
     */
    public QueueTransfer2DB(ConnectionInf db) {
        super(db);
        initConfig();
    }

    public boolean initConfig() {
        dbObj.table = "t_visit_queue_transfer";
        dbObj.pk_field = "t_visit_queue_transfer_id";
        dbObj.assign_time = "assign_date_time";
        dbObj.description = "visit_queue_setup_description";
        dbObj.fname = "patient_firstname";
        dbObj.hn = "visit_hn";
        dbObj.lname = "patient_lastname";
        dbObj.locking = "visit_locking";
        dbObj.name = "service_point_description";
        dbObj.color = "visit_queue_setup_queue_color";
        dbObj.patient_id = "t_patient_id";
        dbObj.doctor = "visit_service_staff_doctor";
        dbObj.visit_id = "t_visit_id";
        dbObj.vn = "visit_vn";
        dbObj.queue = "visit_queue_map_queue";
        dbObj.visit_type = "f_visit_type_id";
        dbObj.servicepoint_id = "b_service_point_id";
        dbObj.patient_allergy = "patient_drugallergy";
        dbObj.sex = "f_sex_id";
        dbObj.prefix = "f_patient_prefix_id";

        dbObj2 = new ListTransfer();
        dbObj2.table = "t_visit_queue_transfer";
        dbObj2.pk_field = "t_visit_queue_transfer_id";
        dbObj2.assign_time = "assign_date_time";
        dbObj2.description = "visit_queue_setup_description";
        dbObj2.fname = "patient_firstname";
        dbObj2.hn = "visit_hn";
        dbObj2.lname = "patient_lastname";
        dbObj2.locking = "visit_locking";
        dbObj2.name = "service_point_description";
        dbObj2.color = "visit_queue_setup_queue_color";
        dbObj2.doctor = "visit_service_staff_doctor";
        dbObj2.visit_id = "t_visit_id";
        dbObj2.patient_id = "t_patient_id";
        dbObj2.vn = "visit_vn";
        dbObj2.queue = "visit_queue_map_queue";
        dbObj2.visit_type = "f_visit_type_id";
        dbObj2.servicepoint_id = "b_service_point_id";
        dbObj2.patient_allergy = "patient_drugallergy";
        dbObj2.sex = "f_sex_id";
        dbObj2.prefix = "f_patient_prefix_id";
        dbObj2.labstatus = "visit_queue_transfer_lab_status";
        dbObj2.xraystatus = "visit_queue_transfer_xray_status";
        return true;
    }

    @Override
    public int insert(ListTransfer p) throws Exception {
        p.generateOID(idtable);
        String sql = "insert into " + dbObj.table + " ("
                + dbObj2.pk_field + " ,"
                + dbObj2.assign_time + " ,"
                + dbObj.description + " ,"
                + dbObj2.fname + " ,"
                + dbObj2.hn + " ,"
                + dbObj2.lname + " ,"
                + dbObj.locking + " ,"
                + dbObj2.name + " ,"
                + dbObj2.color + " ,"
                + dbObj2.patient_id + " ,"
                + dbObj2.doctor + " ,"
                + dbObj2.visit_id + " ,"
                + dbObj2.vn + " ,"
                + dbObj2.queue + " ,"
                + dbObj2.visit_type + " ,"
                + dbObj2.servicepoint_id + " ,"
                + dbObj2.sex + " ,"
                + dbObj2.prefix + " ,"
                + dbObj2.patient_allergy + " ,"
                + dbObj2.labstatus + " "
                + " ) values ('"
                + p.getObjectId() + "','"
                + p.assign_time + "','"
                + p.description + "','"
                + p.fname + "','"
                + p.hn + "','"
                + p.lname + "','"
                + p.locking + "','"
                + p.name + "','"
                + p.color + "','"
                + p.patient_id + "','"
                + p.doctor + "','"
                + p.visit_id + "','"
                + p.vn + "','"
                + p.queue + "','"
                + p.visit_type + "','"
                + p.servicepoint_id + "','"
                + p.sex + "','"
                + p.prefix + "','"
                + p.patient_allergy + "','"
                + p.labstatus
                + "')";
        sql = Gutil.convertSQLToMySQL(sql, theConnectionInf.gettypeDatabase());
        return theConnectionInf.eUpdate(sql);
    }

    @Override
    public int updateMapQueueTransferByVisitID(ListTransfer o) throws Exception {
        String sql = "UPDATE " + dbObj.table + ""
                + " set " + dbObj.color + " = '" + o.color + "'"
                + " , " + dbObj.description + " = '" + o.description + "'"
                + " , " + dbObj.queue + " = '" + o.queue + "'"
                + " where " + dbObj.visit_id + "='" + o.visit_id + "'";
        return theConnectionInf.eUpdate(sql);
    }

    public int update(ListTransfer p) throws Exception {
        String sql = "update t_visit_queue_transfer set "
                + "patient_firstname = ?, "
                + "visit_queue_setup_description = ?, "
                + "assign_date_time = ?, "
                + "visit_vn = ?, "
                + "t_visit_id = ?, "
                + "visit_service_staff_doctor = ?, "
                + "t_patient_id = ?, "
                + "visit_queue_setup_queue_color = ?, "
                + "service_point_description = ?, "
                + "visit_locking = ?, "
                + "patient_lastname = ?, "
                + "visit_hn = ?, "
                + "patient_drugallergy = ?, "
                + "f_patient_prefix_id = ?, "
                + "f_sex_id = ?, "
                + "b_service_point_id = ?, "
                + "f_visit_type_id = ?, "
                + "visit_queue_transfer_lab_status = ?, "
                + "visit_queue_map_queue = ?,"
                + "visit_queue_transfer_xray_status= ?,"
                + "arrived_datetime=?,"
                + "arrived_status=?"
                + " where t_visit_queue_transfer_id = ?";

        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, p.fname);
            ePQuery.setString(index++, p.description);
            ePQuery.setString(index++, p.assign_time);
            ePQuery.setString(index++, p.vn);
            ePQuery.setString(index++, p.visit_id);
            ePQuery.setString(index++, p.doctor);
            ePQuery.setString(index++, p.patient_id);
            ePQuery.setString(index++, p.color);
            ePQuery.setString(index++, p.name);
            ePQuery.setString(index++, p.locking);
            ePQuery.setString(index++, p.lname);
            ePQuery.setString(index++, p.hn);
            ePQuery.setString(index++, p.patient_allergy);
            ePQuery.setString(index++, p.prefix);
            ePQuery.setString(index++, p.sex);
            ePQuery.setString(index++, p.servicepoint_id);
            ePQuery.setString(index++, p.visit_type);
            ePQuery.setString(index++, p.labstatus);
            ePQuery.setString(index++, p.queue);
            ePQuery.setString(index++, p.xraystatus);
            ePQuery.setTimestamp(index++, p.arrived_datetime != null ? new java.sql.Timestamp(p.arrived_datetime.getTime()) : null);
            ePQuery.setInt(index++, Integer.parseInt(p.arrived_status));
            ePQuery.setString(index++, p.getObjectId());
            return ePQuery.executeUpdate();
        }
    }

    @Override
    public int delete(ListTransfer o) throws Exception {
        String sql = "delete from " + dbObj.table
                + " where " + dbObj.pk_field + "='" + o.getObjectId() + "'";
        return theConnectionInf.eUpdate(sql);
    }

    public int updateTransferPatientDenyAllergy(String patient_id, String allergy)
            throws Exception {
        String sql = "UPDATE " + dbObj.table + ""
                + " set " + dbObj.patient_allergy + " = '" + allergy + "'"
                + " where " + dbObj.patient_id + "='" + patient_id + "'";
        return theConnectionInf.eUpdate(sql);
    }

    @Override
    public Vector listTransferVisitQueueByServicePoint(String servicePointId, String doctorId, String visitType) throws Exception {
        String sql = "select t_visit_queue_transfer.*"
                + "         ,t_visit.xray_urgent_status "
                + "         ,t_visit.lab_urgent_status "
                + "         ,t_visit.drug_stat_status \n"
                + "         ,t_visit.f_emergency_status_id \n"
                + "         ,b_visit_range_age.description as range_age\n"
                + "         ,t_visit.visit_vital_sign_score\n"
                + "         ,t_visit.visit_vital_sign_notify_datetime\n"
                + "         ,t_visit.visit_vital_sign_newspews_type"
                + "     from t_visit_queue_transfer\n"
                + "     inner join t_visit on t_visit.t_visit_id  =t_visit_queue_transfer.t_visit_id \n"
                + "     left join b_visit_range_age on t_visit.b_visit_range_age_id = b_visit_range_age.b_visit_range_age_id \n"
                + "          and b_visit_range_age.active = '1' \n"
                + "     where true \n";
        if (servicePointId != null && !servicePointId.trim().isEmpty()) {
            sql = sql + "and t_visit_queue_transfer.b_service_point_id = ?\n";
        }
        if (doctorId != null && !doctorId.trim().isEmpty()) {
            sql = sql + "and t_visit_queue_transfer.visit_service_staff_doctor = ?\n";
        }
        if (visitType != null && !visitType.isEmpty() && (visitType.equals(VisitType.IPD) || visitType.equals(VisitType.OPD))) {
            sql = sql + "and t_visit_queue_transfer.f_visit_type_id = ?\n";
        }
        sql = sql + " order by t_visit_queue_transfer.arrived_datetime\n"
                + "    ,t_visit_queue_transfer.assign_date_time";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;

            if (servicePointId != null && !servicePointId.trim().isEmpty()) {
                ePQuery.setString(index++, servicePointId);
            }
            if (doctorId != null && !doctorId.trim().isEmpty()) {
                ePQuery.setString(index++, doctorId == null
                        || doctorId.isEmpty()
                        || doctorId.equals("")
                        || doctorId.equals("null") ? null : doctorId);
            }
            if (visitType != null && !visitType.isEmpty() && (visitType.equals(VisitType.IPD) || visitType.equals(VisitType.OPD))) {
                ePQuery.setString(index++, visitType);
            }
            return veQuery(ePQuery.toString());
        }
    }

    public Vector selectAll() throws Exception {
        String SQL = "select * from " + dbObj.table
                + " order by " + dbObj.assign_time;
        return veQuery(SQL);
    }

    public Vector selectIPD() throws Exception {
        String SQL = "select * from " + dbObj.table
                + " where " + dbObj.vn + " like '1%' "
                + " order by " + dbObj.assign_time;
        return veQuery(SQL);
    }

    @Override
    public Vector veQuery(String sql) throws Exception {
        ListTransfer p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            p = new ListTransfer();
            p.setObjectId(rs.getString(dbObj.pk_field));
            p.assign_time = rs.getString(dbObj.assign_time);
            p.description = rs.getString(dbObj.description);
            p.fname = rs.getString(dbObj.fname);
            p.hn = rs.getString(dbObj.hn);
            p.lname = rs.getString(dbObj.lname);
            p.locking = rs.getString(dbObj.locking);
            p.name = rs.getString(dbObj.name);
            p.patient_id = rs.getString(dbObj.patient_id);
            p.doctor = rs.getString(dbObj.doctor);
            p.color = rs.getString(dbObj.color);
            p.visit_id = rs.getString(dbObj.visit_id);
            p.vn = rs.getString(dbObj.vn);
            p.visit_type = rs.getString(dbObj.visit_type);
            p.queue = rs.getString(dbObj.queue);
            p.servicepoint_id = rs.getString(dbObj.servicepoint_id);
            p.patient_allergy = rs.getString(dbObj.patient_allergy);
            p.sex = rs.getString(dbObj.sex);
            p.prefix = rs.getString(dbObj.prefix);
            p.labstatus = rs.getString(dbObj2.labstatus);
            p.xraystatus = rs.getString(dbObj2.xraystatus);
            try {
                p.lab_urgent_status = rs.getString("lab_urgent_status");
                p.xray_urgent_status = rs.getString("xray_urgent_status");
                p.drug_stat_status = rs.getString("drug_stat_status");
            } catch (SQLException e) {
            }
            try {
                p.emergency_status = rs.getString("f_emergency_status_id");
            } catch (SQLException e) {
            }
            p.arrived_datetime = rs.getTimestamp("arrived_datetime");
            try {
                p.range_age = rs.getString("range_age");
            } catch (SQLException e) {
            }
            p.arrived_status = String.valueOf(rs.getInt("arrived_status"));
            try {
                p.visit_vital_sign_score = rs.getString("visit_vital_sign_score");
                p.visit_vital_sign_notify_datetime = rs.getTimestamp("visit_vital_sign_notify_datetime");
                p.visit_vital_sign_newspews_type = rs.getString("visit_vital_sign_newspews_type");
            } catch (SQLException e) {
            }
            list.add(p);
        }

        rs.close();
        return list;
    }

    public ListTransfer select2ByVisitID(String visit_id) throws Exception {
        String SQL = "select * from " + dbObj.table
                + " where " + dbObj.visit_id + "='" + visit_id + "'";

        Vector v = veQuery(SQL);

        if (v.isEmpty()) {
            return null;
        } else {
            return (ListTransfer) v.get(0);
        }
    }

    public int updateLockByVid(String vid) throws Exception {
        String sql = "UPDATE " + dbObj.table + ""
                + " set " + dbObj.locking + " = '" + 0 + "'"
                + " where " + dbObj.visit_id + "='" + vid
                + "'";
        return theConnectionInf.eUpdate(sql);
    }

    /**
     * @Author: tuk @date: 08/08/2549
     * @see: update �ӹ�˹�Ҫ���㹤�� Transfer
     * @param: patient id ��� �ӹ�˹�Ҫ���
     */
    public int updatePrefix(String patient_id, String prefix_id) throws Exception {
        String sql = "UPDATE " + dbObj.table + ""
                + " set " + dbObj.prefix + " = '" + prefix_id + "'"
                + " where " + dbObj.patient_id + "='" + patient_id
                + "'";
        return theConnectionInf.eUpdate(sql);
    }

    public int updateArrivedStatus(String visitId, int status) throws Exception {
        String sql = "UPDATE t_visit_queue_transfer set arrived_status = ?\n"
                + " where t_visit_id = ?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setInt(index++, status);
            ePQuery.setString(index++, visitId);
            return ePQuery.executeUpdate();
        }
    }
}

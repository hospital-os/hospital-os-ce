/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.gui.panel.detail;

/**
 *
 * @author Somprasong
 */
public interface UIActionInf {

    public void setParameters(Object... object);
    
    public void initailUI(Object... object);

    public void resetUI();

    public boolean setUI();
    
    public void validateUI();

    public boolean doNewAction();

    public boolean doSaveAction();

    public boolean doDeleteAction();
}

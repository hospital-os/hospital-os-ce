DROP FUNCTION IF EXISTS moph_phr_V39(text);
DROP FUNCTION IF EXISTS moph_phr_update(text);
CREATE OR REPLACE FUNCTION moph_phr_update(t_visit_id text)
RETURNS TABLE (patient_phr jsonb) AS $$
    DECLARE
    BEGIN
return query 
select jsonb_build_object(
            'managingOrganization',phr.managingorganization
            ,'Patient',jsonb_build_object(
                'identifier',COALESCE(phr.patient_identifier, '[]'::jsonb)
                ,'active',phr.patient_active
                ,'name',COALESCE(phr.patient_name, '[]'::jsonb)
                ,'telecom',COALESCE(phr.patient_telecom, '[]'::jsonb)
                ,'gender',phr.patient_gender
                ,'birthDate',phr.patient_birthdate
                ,'deceasedBoolean',phr.patient_deceased
                ,'nationality',COALESCE(phr.patient_nation, '{}'::jsonb)
                ,'address',COALESCE(phr.patient_address, '[]'::jsonb)
                ,'maritalStatus',COALESCE(phr.patient_maritalstatus, '{}'::jsonb)
                ,'contact',COALESCE(phr.patient_contact, '[]'::jsonb)
            )
            ,'AllergyIntolerance',COALESCE(phr.allergy_intolerance,'[]'::jsonb)
            ,'ChronicDiseaseRegister',COALESCE(phr.chronic_disease,'[]'::jsonb)
            ,'Encounter',jsonb_build_array(jsonb_build_object(
                'managingOrganization',phr.encounter_managingorganization
                ,'identifier',COALESCE(phr.encounter_identifier, '[]'::jsonb)
                ,'status','finished'
                ,'class',COALESCE(phr.encounter_class, '{}'::jsonb)
                ,'subclass',COALESCE(phr.encounter_subclass, '{}'::jsonb)
                ,'division',COALESCE(phr.encounter_division, '{}'::jsonb)
                ,'type',COALESCE(phr.encounter_type, '{}'::jsonb)
                ,'priority',COALESCE(phr.encounter_priority, '{}'::jsonb)
                ,'period',COALESCE(phr.encounter_period, '{}'::jsonb)
                ,'subject',COALESCE(phr.encounter_subject, '{}'::jsonb)
                ,'screen_allergy',COALESCE(phr.encounter_screen_allergy, '{}'::jsonb)
                ,'screen_smoking',COALESCE(phr.encounter_screen_smoking, '{}'::jsonb)
                ,'screen_drinking',COALESCE(phr.encounter_screen_drinking, '{}'::jsonb)
                ,'participant',COALESCE(encounter_participant, '[]'::jsonb)
                ,'reason',COALESCE(phr.encounter_reason, '[]'::jsonb)
                ,'financeTotalAmount',phr.encounter_total
                ,'financeReimbursementAmount',phr.encounter_reimbursement
                ,'financePaidAmount',phr.encounter_paid
                ,'Coverage',COALESCE(phr.encounter_coverage, '[]'::jsonb)
                ,'vital_signs',COALESCE(phr.encounter_vital_signs, '{}'::jsonb)
                ,'Observation',COALESCE(phr.encounter_observation, '[]'::jsonb)
                ,'Condition',COALESCE(phr.encounter_condition, '[]'::jsonb)
                ,'Medication',COALESCE(phr.encounter_medication, '[]'::jsonb)
                ,'Claim',COALESCE(NULL, '[]'::jsonb)
                ,'Appointment',COALESCE(phr.encounter_appointment, '[]'::jsonb)
                ,'Immunization',COALESCE(phr.encounter_immunization, '[]'::jsonb)
                ,'DiagnosticReport',COALESCE(phr.encounter_xray, '[]'::jsonb)
            ))
        )
from (select jsonb_build_object(
                'type','Organization'
                ,'identifier',jsonb_build_object(
                    'use','official'
                    ,'system','https://bps.moph.go.th/hcode/5'
                    ,'value',b_site.b_visit_office_id
                )
                ,'display',b_site.site_full_name
                ,'scope',''
				,'agent', 'HospitalOS 3.9'
            ) as managingorganization 
            ,jsonb_build_array(jsonb_build_object(
                    'use','official'
                    ,'system','https://www.dopa.go.th'
                    ,'type','CID'
                    ,'value',t_patient.patient_pid
                    ,'period',jsonb_build_object(
                        'start',to_char(text_to_timestamp(t_patient.patient_record_date_time),'YYYY-MM-DD')
                    ) 
            )
            ,jsonb_build_object(
                    'use','official'
                    ,'system','https://sil-th.org/hn'
                    ,'assigner',jsonb_build_object(
                        'use','official'
                        ,'system','https://bps.moph.go.th/hcode/5'
                        ,'value',b_site.b_visit_office_id
                        ,'display',b_site.site_full_name
                    )
                    ,'type','HN'
                    ,'value',t_patient.patient_hn
                    ,'period',jsonb_build_object(
                        'start',to_char(text_to_timestamp(t_patient.patient_record_date_time),'YYYY-MM-DD')
                    )
            )) as patient_identifier
            ,case when t_patient.patient_active = '1' then true else false end as patient_active
            ,jsonb_build_array(jsonb_build_object(
                'use','official'
                ,'text',(case when (f_patient_prefix.f_patient_prefix_id <> '000' and f_patient_prefix.f_patient_prefix_id is not null) then f_patient_prefix.patient_prefix_description else '' end)
                        || t_patient.patient_firstname || ' ' || t_patient.patient_lastname
                ,'languageCode','TH'
                ,'family',t_patient.patient_lastname
                ,'given',COALESCE(jsonb_agg(t_patient.patient_firstname) FILTER (WHERE t_patient.patient_firstname is not null), '[]'::jsonb)
                ,'prefix',COALESCE(jsonb_agg(case when (f_patient_prefix.f_patient_prefix_id <> '000' and f_patient_prefix.f_patient_prefix_id is not null) then f_patient_prefix.patient_prefix_description else '' end) 
                            FILTER (WHERE f_patient_prefix.f_patient_prefix_id <> '000' and f_patient_prefix.f_patient_prefix_id is not null), '[]'::jsonb)
                ,'suffix','[]'::jsonb
                ,'period',jsonb_build_object(
                    'start',to_char(text_to_timestamp(t_patient.patient_record_date_time),'YYYY-MM-DDT') || to_char(text_to_timestamp(t_patient.patient_record_date_time),'HH24:MI:SS.MSZ')
                )
            )
            ,jsonb_build_object(
                'use','official'
                ,'text', case when t_health_family.patient_firstname_eng <> '' 
                              then f_patient_prefix.patient_prefix_description_eng || ' ' || t_health_family.patient_firstname_eng || ' ' || t_health_family.patient_lastname_eng
                              else '' end
                ,'languageCode','EN'
                ,'family',t_health_family.patient_lastname_eng
                ,'given',COALESCE(jsonb_agg(t_health_family.patient_firstname_eng) FILTER (WHERE t_health_family.patient_firstname_eng <> ''), '[]'::jsonb)
                ,'prefix',COALESCE(jsonb_agg(case when (f_patient_prefix.f_patient_prefix_id <> '000' and f_patient_prefix.f_patient_prefix_id is not null) then f_patient_prefix.patient_prefix_description_eng else '' end) 
                            FILTER (WHERE f_patient_prefix.f_patient_prefix_id <> '000' and f_patient_prefix.f_patient_prefix_id is not null), '[]'::jsonb)
                ,'suffix','[]'::jsonb
                ,'period',jsonb_build_object(
                    'start',to_char(text_to_timestamp(t_patient.patient_record_date_time),'YYYY-MM-DDT') || to_char(text_to_timestamp(t_patient.patient_record_date_time),'HH24:MI:SS.MSZ')
                )
            )) as patient_name
            ,telecom.patient_telecom
            ,case when t_patient.f_sex_id = '1' then 'male'
                  when t_patient.f_sex_id = '2' then 'female'
                  else '' end as patient_gender
            ,case when t_patient.patient_birthday <> '' then to_char(text_to_timestamp(t_patient.patient_birthday),'YYYY-MM-DD') else '' end as patient_birthdate
            ,case when t_health_family.f_patient_discharge_status_id in ('1','2','3') then true else false end as patient_deceased
            ,jsonb_build_object(
                'coding',jsonb_build_array(jsonb_build_object(
                    'system','http://www.thcc.or.th/download/nationalitycode.xls'
                    ,'code',case when f_patient_nation.f_patient_nation_id is not null then f_patient_nation.r_rp1853_nation_id else '' end
                    ,'display',case when f_patient_nation.f_patient_nation_id is not null then f_patient_nation.patient_nation_description else '' end
                ))
                ,'text',case when f_patient_nation.f_patient_nation_id is not null then f_patient_nation.patient_nation_description else '' end
            ) as patient_nation
            ,jsonb_build_array(jsonb_build_object(
                'use','home'
                 ,'type','both'
                 ,'text','ที่อยู่'
                 ,'line',array_to_json(array_remove(ARRAY[
                    case when t_patient.patient_house <> '' then 'บ้านเลขที่ '|| t_patient.patient_house else null end
                    ,case when t_patient.patient_moo <> '' then 'หมู่ที่ '|| t_patient.patient_moo else null end
                    ,case when t_patient.patient_road <> '' then 'ถนน '|| t_patient.patient_road else null end
                    ], null))
                 ,'city',case when city_address.f_address_id is null then ''
                              when t_patient.patient_changwat = '100000' then 'แขวง' || city_address.address_description
                              when t_patient.patient_changwat <> '100000' then 'ตำบล' || city_address.address_description end
                 ,'district',case when district_address.f_address_id is null then ''
                                  when t_patient.patient_changwat = '100000' then 'เขต' || district_address.address_description
                                  when t_patient.patient_changwat <> '100000' then 'อำเภอ' || district_address.address_description end
                 ,'state',case when state_address.f_address_id is null then ''
                               when t_patient.patient_changwat = '100000' then state_address.address_description
                               when t_patient.patient_changwat <> '100000' then 'จังหวัด' || state_address.address_description end
                 ,'postalCode',case when t_patient.patient_postcode <> '' then t_patient.patient_postcode else '' end
                 ,'country',case when patient_is_other_country = '0' then 'TH' else '' end
                 ,'period',jsonb_build_object(
                    'start',to_char(text_to_timestamp(t_patient.patient_record_date_time),'YYYY-MM-DDT') || to_char(text_to_timestamp(t_patient.patient_record_date_time),'HH24:MI:SS.MSZ')
                 )
                 ,'address_code',case when t_patient.patient_tambon <> '' then t_patient.patient_tambon else '' end
            )
           ) as patient_address
           ,jsonb_build_object(
                'coding',jsonb_build_array(jsonb_build_object(
                    'system','http://terminology.hl7.org/CodeSystem/v3-MaritalStatus'
                    ,'code',case when f_patient_marriage_status.f_patient_marriage_status_id in ('1','6') then 'S'
                                 when f_patient_marriage_status.f_patient_marriage_status_id = '2' then 'M'
                                 when f_patient_marriage_status.f_patient_marriage_status_id = '3' then 'L'
                                 when f_patient_marriage_status.f_patient_marriage_status_id = '4' then 'D'
                                 when f_patient_marriage_status.f_patient_marriage_status_id = '5' then 'W'
                                 else '' end
                    ,'display',case when f_patient_marriage_status.f_patient_marriage_status_id in ('1','2','3','4','5','6')
                                    then f_patient_marriage_status.patient_marriage_status_description
                                    else '' end
                    )
                )
                ,'text',case when f_patient_marriage_status.f_patient_marriage_status_id in ('1','2','3','4','5','6') then f_patient_marriage_status.patient_marriage_status_description else '' end
            ) as patient_maritalstatus
            ,jsonb_build_array(jsonb_build_object(
                'relationship',case when t_patient.patient_contact_firstname = '' then '[]'::jsonb
                                    else jsonb_build_array(jsonb_build_object(
                    'coding',jsonb_build_array(jsonb_build_object(
                        'system','https://www.this.or.th'
                         ,'code',case when f_patient_relation.f_patient_relation_id is not null then cast(cast(f_patient_relation.f_patient_relation_id as integer) as text) else '' end
                         ,'display',case when f_patient_relation.f_patient_relation_id is not null then f_patient_relation.patient_relation_description else '' end
                    ))
                    ,'text',case when f_patient_relation.f_patient_relation_id is not null then f_patient_relation.patient_relation_description else '' end
                )) end
                ,'name',case when t_patient.patient_contact_firstname = '' then '[]'::jsonb
                             else jsonb_build_array(jsonb_build_object(
                    'use','official'
                     ,'text',case when t_patient.patient_contact_sex_id = '1' then 'นาย' when t_patient.patient_contact_sex_id = '2' then 'นางสาว' else '' end
                            || t_patient.patient_contact_firstname || ' ' || t_patient.patient_contact_lastname
                     ,'family',t_patient.patient_contact_lastname
                     ,'languageCode','TH'
                     ,'given',COALESCE(jsonb_agg(t_patient.patient_contact_firstname) FILTER (WHERE t_patient.patient_contact_firstname is not null), '[]'::jsonb)
                     ,'prefix',COALESCE(jsonb_agg(case when t_patient.patient_contact_sex_id = '1' then 'นาย'
                                    when t_patient.patient_contact_sex_id = '2' then 'นางสาว'
                                    else null end
                                    ) FILTER (WHERE t_patient.patient_contact_firstname <> ''), '[]'::jsonb)
                     ,'suffix','[]'::jsonb
                     ,'period',jsonb_build_object(
                        'start',to_char(text_to_timestamp(t_patient.patient_record_date_time),'YYYY-MM-DDT') || to_char(text_to_timestamp(t_patient.patient_record_date_time),'HH24:MI:SS.MSZ')
                        )
                )) end
                ,'telecom',case when t_patient.patient_contact_firstname = '' then '[]'::jsonb
                                else contact_tele.contact_telecom end
                ,'address',case when t_patient.patient_contact_firstname = '' then '[]'::jsonb
                                else jsonb_build_array(jsonb_build_object(
                    'use','home'
                    ,'type','both'
                    ,'text','ที่อยู่'
                    ,'line',array_to_json(array_remove(ARRAY[
                        case when t_patient.patient_contact_house <> '' then 'บ้านเลขที่ '|| t_patient.patient_contact_house else null end
                        ,case when t_patient.patient_contact_moo <> '' then 'หมู่ที่ '|| t_patient.patient_contact_moo else null end
                        ,case when t_patient.patient_contact_road <> '' then 'ถนน '|| t_patient.patient_contact_road else null end
                                ], null))
                            ,'city',case when city_contact.f_address_id is null then ''
                                         when t_patient.patient_contact_changwat = '100000' then 'แขวง' || city_contact.address_description
                                         when t_patient.patient_contact_changwat <> '100000' then 'ตำบล' || city_contact.address_description end
                            ,'district',case when district_contact.f_address_id is null then ''
                                         when t_patient.patient_contact_changwat = '100000' then 'เขต' || district_contact.address_description
                                         when t_patient.patient_contact_changwat <> '100000' then 'อำเภอ' || district_contact.address_description end
                            ,'state',case when state_contact.f_address_id is null then ''
                                         when t_patient.patient_contact_changwat = '100000' then state_contact.address_description
                                         when t_patient.patient_contact_changwat <> '100000' then 'จังหวัด' || state_contact.address_description end
                            ,'postalCode',case when t_patient.patient_contact_postcode <> '' then t_patient.patient_contact_postcode else '' end
                            ,'country','TH'
                            ,'period',jsonb_build_object(
                                    'start',to_char(text_to_timestamp(t_patient.patient_record_date_time),'YYYY-MM-DDT') || to_char(text_to_timestamp(t_patient.patient_record_date_time),'HH24:MI:SS.MSZ')
                                )
                            ,'address_code',case when t_patient.patient_contact_tambon <> '' then t_patient.patient_contact_tambon else '' end
                        )) end
                        ,'gender',case when t_patient.f_sex_id = '1' then 'male' when t_patient.f_sex_id = '2' then 'female' else '' end
            )) as patient_contact
            ,jsonb_build_object(
                'type','Organization'
                ,'identifier',jsonb_build_object(
                    'use','official'
                    ,'system','https://bps.moph.go.th/hcode/5'
                    ,'value',b_site.b_visit_office_id
                )
                ,'display',b_site.site_full_name
            ) as encounter_managingorganization
            ,jsonb_build_array(jsonb_build_object(
                    'use','official'
                    ,'system','https://bps.moph.go.th/vn'
                    ,'value',t_visit.visit_vn
                ),jsonb_build_object(
                    'use','official'
                    ,'system','https://sil-th.org/hn'
                    ,'value',t_patient.patient_hn
                    ,'period',jsonb_build_object(
                        'start',to_char(text_to_timestamp(t_visit.visit_begin_visit_time),'YYYY')
                    )
                )
            ) as encounter_identifier
            ,jsonb_build_object(
                'system','https://terminology.hl7.org/CodeSystem/v3-ActCode'
                ,'code',case when t_visit.f_emergency_status_id = '4' then 'EMER'
                             when t_visit.f_visit_type_id = '1' then 'IMP'
                             else 'AMB' end
                ,'display',case when t_visit.f_emergency_status_id = '4' then 'emergency'
                             when t_visit.f_visit_type_id = '1' then 'inpatient encounter'
                             else 'ambulatory' end
            ) as encounter_class
            ,jsonb_build_object(
                'system','https://bps.moph.go.th/subclass'
                ,'code','1'
                ,'display','ผู้ป่วยตรวจโรคทั่วไป'
            ) as encounter_subclass
            ,jsonb_build_object(
                'system','https://bps.moph.go.th/division'
                ,'code',case when b_report_12files_std_clinic.b_report_12files_std_clinic_id is not null
                             then b_report_12files_std_clinic.b_report_12files_std_clinic_id
                             else '' end
                ,'display',case when b_report_12files_std_clinic.b_report_12files_std_clinic_id is not null
                             then b_report_12files_std_clinic.report_clinic_12files_description
                             else '' end
            ) as encounter_division
            ,jsonb_build_object(
                'coding',jsonb_build_array(jsonb_build_object(
                    'system','https://spd.moph.go.th/new_bps/43file_version2.3'
                    ,'code',case when f_visit_service_type.f_visit_service_type_id is not null then f_visit_service_type.f_visit_service_type_id else '' end
                    ,'display',case when f_visit_service_type.f_visit_service_type_id is not null then f_visit_service_type.visit_service_type_description else '' end
                    )
                )
                ,'text',case when f_visit_service_type.f_visit_service_type_id is not null then f_visit_service_type.visit_service_type_description else '' end
            ) as encounter_type
            ,jsonb_build_object(
                'coding',jsonb_build_array(jsonb_build_object(
                    'system','http://terminology.hl7.org/CodeSystem/v3-ActPriority'
                    ,'code',case when t_visit.f_emergency_status_id in ('0','1') then 'R'
                                 when t_visit.f_emergency_status_id = '2' then 'EL'
                                 when t_visit.f_emergency_status_id = '3' then 'UR'
                                 when t_visit.f_emergency_status_id = '4' then 'EM'
                                 when t_visit.f_emergency_status_id = '5' then 'T' else '' end
                    ,'display',case when t_visit.f_emergency_status_id in ('0','1') then 'routine'
                                 when t_visit.f_emergency_status_id = '2' then 'elective'
                                 when t_visit.f_emergency_status_id = '3' then 'urgent'
                                 when t_visit.f_emergency_status_id = '4' then 'emergency'
                                 when t_visit.f_emergency_status_id = '5' then 'timing critical' else '' end
                    )
                )
                ,'text',case when t_visit.f_emergency_status_id in ('0','1') then 'ไม่เร่งด่วน'
                                 when t_visit.f_emergency_status_id = '2' then 'กึ่งเร่งด่วน'
                                 when t_visit.f_emergency_status_id = '3' then 'เร่งด่วน'
                                 when t_visit.f_emergency_status_id = '4' then 'ฉุกเฉิน'
                                 when t_visit.f_emergency_status_id = '5' then 'วิกฤต' else '' end
            ) as encounter_priority
            ,jsonb_build_object(
                'start',case when t_visit.visit_begin_visit_time <> '' 
							then to_char(text_to_timestamp(t_visit.visit_begin_visit_time),'YYYY-MM-DDT') || to_char(text_to_timestamp(t_visit.visit_begin_visit_time),'HH24:MI:SS.MSZ')
                             else '' end
                ,'end',case when t_visit.visit_staff_doctor_discharge_date_time <> '' 
                            then to_char(text_to_timestamp(t_visit.visit_staff_doctor_discharge_date_time),'YYYY-MM-DDT') || to_char(text_to_timestamp(t_visit.visit_staff_doctor_discharge_date_time),'HH24:MI:SS.MSZ')
                            else '' end
            ) as encounter_period
            ,jsonb_build_object(
                'reference','Patient/' || t_visit.visit_hn
                ,'display',(case when (f_patient_prefix.f_patient_prefix_id <> '000' and f_patient_prefix.f_patient_prefix_id is not null) then f_patient_prefix.patient_prefix_description else '' end)
                        || t_patient.patient_firstname || ' ' || t_patient.patient_lastname
            ) as encounter_subject
            ,jsonb_build_object(
                'system','https://bps.moph.go.th/screen_allergy'
                ,'code',case when t_patient.deny_allergy in ('1','2','3') then t_patient.deny_allergy else '9' end
                ,'display',case when t_patient.deny_allergy = '1' then 'ปฏิเสธการแพ้ยา'
                                when t_patient.deny_allergy = '2' then 'ไม่ทราบประวัติการแพ้ยา'
                                when t_patient.deny_allergy = '3' then 'มีประวัติการแพ้ยา'
                                else 'ไม่ระบุ' end
            ) as encounter_screen_allergy
            ,jsonb_build_object(
                'system','https://bps.moph.go.th/screen_smoking'
                ,'code',case when smoking_drinking.risk_cigarette_result = '0' then '1'
                             when smoking_drinking.risk_cigarette_result = '1' then '2'
                             when smoking_drinking.risk_cigarette_result = '2' then '3'
                             when smoking_drinking.risk_cigarette_result = '3' then '4' else '9' end
                ,'display',case when smoking_drinking.risk_cigarette_result = '0' then 'ไม่สูบ'
                                when smoking_drinking.risk_cigarette_result = '1' then 'สูบนานๆครั้ง'
                                when smoking_drinking.risk_cigarette_result = '2' then 'สูบเป็นครั้งคราว'
                                when smoking_drinking.risk_cigarette_result = '3' then 'สูบเป็นประจำ'
                                else 'ไม่ทราบ' end
            ) as encounter_screen_smoking
            ,jsonb_build_object(
                'system','https://bps.moph.go.th/screen_drinking'
                ,'code',case when smoking_drinking.risk_alcoho_result = '0' then '1'
                             when smoking_drinking.risk_alcoho_result = '1' then '2'
                             when smoking_drinking.risk_alcoho_result = '2' then '3'
                             when smoking_drinking.risk_alcoho_result = '3' then '4' else '9' end
                ,'display',case when smoking_drinking.risk_alcoho_result = '0' then 'ไม่ดื่ม'
                                when smoking_drinking.risk_alcoho_result = '1' then 'ดื่มนานๆครั้ง'
                                when smoking_drinking.risk_alcoho_result = '2' then 'ดื่มเป็นครั้งคราว'
                                when smoking_drinking.risk_alcoho_result = '3' then 'ดื่มเป็นประจำ'
                                else 'ไม่ทราบ' end
            ) as encounter_screen_drinking
            ,jsonb_build_array(jsonb_build_object(
                'individual',jsonb_build_object('type',jsonb_build_object('text',f_provider_type.description))
                ,'reference',case when participant.f_employee_authentication_id = '3' and participant.employee_number  <> '' 
                                and (case when participant_person.t_person_id is not null
                                          then participant_prefix.patient_prefix_description not ilike 'ทพ.%' or participant_prefix.patient_prefix_description not ilike 'ทพญ.%'
                                          else participant.employee_firstname not ilike 'ทพ.%' or participant.employee_firstname not ilike 'ทพญ.%' end)
                             then  'ว'||participant.employee_number
                             when participant.f_employee_authentication_id = '3' and participant.employee_number  <> ''
                                and (case when participant_person.t_person_id is not null
                                          then participant_prefix.patient_prefix_description ilike 'ทพ.%' or participant_prefix.patient_prefix_description ilike 'ทพญ.%'
                                          else participant.employee_firstname ilike 'ทพ.%' or participant.employee_firstname ilike 'ทพญ.%' end)
                             then  'ท'||participant.employee_number
                             when participant.f_employee_authentication_id = '2' and participant.employee_number  <> ''  then  'พ'||participant.employee_number
                             when participant.f_employee_authentication_id = '6' and participant.employee_number  <> ''  then  'ภ'||participant.employee_number
                             when participant.f_employee_authentication_id not in ('2','3','6') and participant.employee_number  <> ''  then participant.employee_number
                             else ''  end
                ,'display',(case when (participant_prefix.f_patient_prefix_id <> '000' and participant_prefix.f_patient_prefix_id is not null) then participant_prefix.patient_prefix_description else '' end)
                        || participant_person.person_firstname || ' ' || participant_person.person_lastname
                )
            ) as encounter_participant
            ,jsonb_build_array(jsonb_build_object(
                'text',f_visit_service_type.visit_service_type_description
				)
            ) as encounter_reason
            ,cast(t_billing.billing_total as decimal(10,2)) as encounter_total
            ,cast(t_billing.billing_payer_share as decimal(10,2)) as encounter_reimbursement
            ,cast(t_billing.billing_patient_share as decimal(10,2)) as encounter_paid
            ,case when plans.t_visit_id is not null 
                  then jsonb_build_array(jsonb_build_object(
                        'identifier',plans.identifier
                        ,'subscriberId',plans.subscriberid
                        ,'status',plans.status
                        ,'type',plans.type_coding
                        ,'relationship',plans.relationship_coding
                        ,'period',plans.period
                        ,'payor',plans.payor
                        ,'class',plans.coverage_class
                        ,'reimbursementAmount',plans.coverage_reimbursement
                        ,'contract',plans.contract
						)
					)
                   else null end as encounter_coverage
            ,case when vital_sign.t_visit_id is not null 
                  then jsonb_build_object(
                        'body_weight',vital_sign.body_weight
                        ,'body_height',vital_sign.body_height
                        ,'body_temp',vital_sign.body_temp
                        ,'bp_systolic',vital_sign.bp_systolic
                        ,'bp_diastolic',vital_sign.bp_diastolic
                     )
                   else null end as encounter_vital_signs
            ,case when lab.t_visit_id is not null 
                  then lab.lab_observation
                  else null end as encounter_observation
            ,case when diag.t_visit_id is not null 
                  then diag.encounter_condition
                  else null end as encounter_condition
            ,case when drugs.t_visit_id is not null 
                  then drugs.encounter_med
                  else null end as encounter_medication
            ,case when appointment.t_visit_id is not null 
                  then appointment.encounter_appointment
                  else null end as encounter_appointment
            ,case when xray.t_visit_id is not null 
                  then xray.result_xray
                  else null end as encounter_xray
            ,case when vaccine.t_visit_id is not null 
                  then vaccine.immunization
                  else null end as encounter_immunization
            ,case when allergy.t_patient_id is not null 
                  then allergy.allergy_intolerance
                  else null end as allergy_intolerance
            ,case when chronic.t_patient_id is not null 
                  then chronic.chronic_disease
                  else null end as chronic_disease
    from t_visit
        inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
        inner join t_health_family on t_patient.t_health_family_id = t_health_family.t_health_family_id
        left join f_patient_prefix on t_health_family.f_prefix_id = f_patient_prefix.f_patient_prefix_id
        left join (select t_patient.t_patient_id
                        ,max(screen_smoking.risk_cigarette_result) as risk_cigarette_result
                        ,max(screen_drinking.risk_alcoho_result) as risk_alcoho_result
                    from t_visit
                        inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
                        left join t_patient_risk_factor as screen_smoking on t_patient.t_patient_id = screen_smoking.t_patient_id
                                                                      and screen_smoking.patient_risk_factor_topic = 'สูบบุหรี่'
                        left join t_patient_risk_factor as screen_drinking on t_patient.t_patient_id = screen_drinking.t_patient_id
                                                                      and screen_drinking.patient_risk_factor_topic = 'ดื่มแอลกอฮอล์'
                    where t_visit.t_visit_id = $1
                    group by t_patient.t_patient_id
                    ) as smoking_drinking on t_patient.t_patient_id = smoking_drinking.t_patient_id
        left join (select t_billing.t_visit_id
                        ,sum(cast(t_billing.billing_total as decimal(10,2))) as billing_total
                        ,sum(cast(t_billing.billing_payer_share as decimal(10,2))) as billing_payer_share
                        ,sum(cast(t_billing.billing_patient_share as decimal(10,2))) as billing_patient_share
                    from t_billing
                    where t_billing.billing_active = '1'
                        and t_billing.t_visit_id = $1
                    group by t_billing.t_visit_id
                  ) as t_billing on t_visit.t_visit_id = t_billing.t_visit_id                     
        left join (select telecom.t_patient_id
                    ,COALESCE(jsonb_agg(telecom.patient_telecom) FILTER (WHERE telecom.t_patient_id is not null), '[]'::jsonb)as patient_telecom
                from (select telecom.t_patient_id
                        ,jsonb_build_object(
                        'system',telecom.telecom_system
                        ,'value',telecom.telecom_value
                        ,'use',telecom.telecom_use
                        ,'rank',row_number() over (order by telecom.telecom_rank)
                        ,'period',jsonb_build_object(
                            'start',to_char(telecom.telecom_period,'YYYY-MM-DDT') || to_char(telecom.telecom_period,'HH24:MI:SS.MSZ')
                            )
                        ) as patient_telecom
                    from (select t_patient.t_patient_id
                            ,'phone' as telecom_system
                            ,t_patient.patient_phone_number as telecom_value
                            ,'home' as telecom_use
                            ,1 as telecom_rank
                            ,text_to_timestamp(t_patient.patient_record_date_time) as telecom_period
                        from t_visit
                            inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
                        where t_visit.f_visit_status_id <> '4'
                            and t_patient.patient_phone_number <> ''
                            and t_patient.patient_active = '1'
                            and t_visit.t_visit_id = $1
                        group by t_patient.t_patient_id
                        union
                        select t_patient.t_patient_id
                            ,'phone' as telecom_system
                            ,t_patient.patient_patient_mobile_phone as telecom_value
                            ,'mobile' as telecom_use
                            ,2 as telecom_rank
                            ,text_to_timestamp(t_patient.patient_record_date_time) as telecom_period
                        from t_visit
                            inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
                        where t_visit.f_visit_status_id <> '4'
                            and t_patient.patient_patient_mobile_phone <> ''
                            and t_patient.patient_active = '1'
                            and t_visit.t_visit_id = $1
                        group by t_patient.t_patient_id
                        union
                        select t_patient.t_patient_id
                            ,'email' as telecom_system
                            ,t_patient.patient_patient_email as telecom_value
                            ,'work' as telecom_use
                            ,3 as telecom_rank
                            ,text_to_timestamp(t_patient.patient_record_date_time) as telecom_period
                        from t_visit
                            inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
                        where t_visit.f_visit_status_id <> '4'
                            and t_patient.patient_patient_email <> ''
                            and t_patient.patient_active = '1'
                            and t_visit.t_visit_id = $1
                        group by t_patient.t_patient_id
                        ) as telecom
                    ) as telecom
                    group by telecom.t_patient_id) as telecom on t_patient.t_patient_id = telecom.t_patient_id
        left join f_patient_nation on t_patient.f_patient_nation_id = f_patient_nation.f_patient_nation_id
        left join f_patient_marriage_status on t_patient.f_patient_marriage_status_id = f_patient_marriage_status.f_patient_marriage_status_id
        left join f_address as city_address on t_patient.patient_tambon = city_address.f_address_id
        left join f_address as district_address on t_patient.patient_amphur = district_address.f_address_id
        left join f_address as state_address on t_patient.patient_changwat = state_address.f_address_id
        left join f_patient_relation on t_patient.f_patient_relation_id = f_patient_relation.f_patient_relation_id
        left join f_address as city_contact on t_patient.patient_contact_tambon = city_contact.f_address_id
        left join f_address as district_contact on t_patient.patient_contact_amphur = district_contact.f_address_id
        left join f_address as state_contact on t_patient.patient_contact_changwat = state_contact.f_address_id
        left join (select contact_telecom.t_patient_id
                    ,COALESCE(jsonb_agg(contact_telecom.contact_telecom) FILTER (WHERE contact_telecom.t_patient_id is not null), '[]'::jsonb)as contact_telecom
                from (select contact_telecom.t_patient_id
                        ,jsonb_build_object(
                        'system',contact_telecom.telecom_system
                        ,'value',contact_telecom.telecom_value
                        ,'use',contact_telecom.telecom_use
                        ,'rank',row_number() over (order by contact_telecom.telecom_rank)
                        ,'period',jsonb_build_object(
                            'start',to_char(contact_telecom.telecom_period,'YYYY-MM-DDT') || to_char(contact_telecom.telecom_period,'HH24:MI:SS.MSZ')
                            )
                        ) as contact_telecom
                    from (select t_patient.t_patient_id
                            ,'phone' as telecom_system
                            ,t_patient.patient_contact_phone_number as telecom_value
                            ,'home' as telecom_use
                            ,1 as telecom_rank
                            ,text_to_timestamp(t_patient.patient_record_date_time) as telecom_period
                        from t_visit
                            inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
                        where t_visit.f_visit_status_id <> '4'
                            and t_patient.patient_contact_phone_number <> ''
                            and t_patient.patient_active = '1'
                            and t_visit.t_visit_id = $1
                        group by t_patient.t_patient_id
                        union
                        select t_patient.t_patient_id
                            ,'phone' as telecom_system
                            ,t_patient.patient_contact_mobile_phone as telecom_value
                            ,'mobile' as telecom_use
                            ,2 as telecom_rank
                            ,text_to_timestamp(t_patient.patient_record_date_time) as telecom_period
                        from t_visit
                            inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
                        where t_visit.f_visit_status_id <> '4'
                            and t_patient.patient_contact_mobile_phone <> ''
                            and t_patient.patient_active = '1'
                            and t_visit.t_visit_id = $1
                        group by t_patient.t_patient_id
                        union
                        select t_patient.t_patient_id
                            ,'email' as telecom_system
                            ,t_patient.patient_contact_email as telecom_value
                            ,'work' as telecom_use
                            ,3 as telecom_rank
                            ,text_to_timestamp(t_patient.patient_record_date_time) as telecom_period
                        from t_visit
                            inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
                        where t_visit.f_visit_status_id <> '4'
                            and t_patient.patient_contact_email <> ''
                            and t_patient.patient_active = '1'
                            and t_visit.t_visit_id = $1
                        group by t_patient.t_patient_id
                        ) as contact_telecom
                    ) as contact_telecom
                    group by contact_telecom.t_patient_id) as contact_tele on t_patient.t_patient_id = contact_tele.t_patient_id
    left join (select t_diag_icd10.diag_icd10_vn as t_visit_id
                      ,jsonb_agg(jsonb_build_object(
                        'clinicalStatus',jsonb_build_object(
                            'coding',jsonb_build_array(jsonb_build_object(
                                'system','http://terminology.hl7.org/CodeSystem/condition-clinical'
                                ,'code','active'
                                ,'display','Active'
                            ))
                          )
                        ,'verificationStatus',jsonb_build_object(
                            'coding',jsonb_build_array(jsonb_build_object(
                                'system','http://terminology.hl7.org/CodeSystem/condition-ver-status'
                                ,'code','confirmed'
                                ,'display','Confirmed'
                            ))
                          )
                        ,'category',jsonb_build_array(jsonb_build_object(
                            'coding',jsonb_build_array(jsonb_build_object(
                                'system','http://snomed.info/sct'
                                ,'code','439401001'
                                ,'display','Diagnosis'
                            ))
                          ))
                        ,'severity',jsonb_build_object(
                            'coding',jsonb_build_array(jsonb_build_object(
                                'system','http://snomed.info/sct'
                                ,'code','255604002'
                                ,'display','Mild'
                            ))
                          )
                        ,'code',jsonb_build_object(
                            'coding',jsonb_build_array(jsonb_build_object(
                                'system','http://hl7.org/fhir/sid/icd-10'
                                ,'code',t_diag_icd10.diag_icd10_number
                                ,'display',b_icd10.icd10_description
                            ))
                            ,'text',b_icd10.icd10_description
                          )
                        ,'bodySite',COALESCE(null, '[]'::jsonb)
                        ,'recordedDate',to_char(text_to_timestamp(t_diag_icd10.diag_icd10_record_date_time),'YYYY-MM-DDT') || to_char(text_to_timestamp(t_diag_icd10.diag_icd10_record_date_time),'HH24:MI:SS.MSZ')
                       )) as encounter_condition
                    ,t_diag_icd10.b_visit_clinic_id
                    ,t_diag_icd10.diag_icd10_staff_doctor
               from t_diag_icd10
                   inner join t_visit on t_diag_icd10.diag_icd10_vn = t_visit.t_visit_id
                   inner join b_icd10 on t_diag_icd10.diag_icd10_number = b_icd10.icd10_number
               where t_visit.t_visit_id = $1
                   and t_diag_icd10.diag_icd10_active = '1'
               group by t_diag_icd10.diag_icd10_vn
                ,t_diag_icd10.b_visit_clinic_id
                ,t_diag_icd10.diag_icd10_staff_doctor
            ) as diag on diag.t_visit_id = t_visit.t_visit_id
    left join (select t_diag_icd10.*
                      ,row_number() OVER (partition by  t_diag_icd10.diag_icd10_vn,t_diag_icd10.f_diag_icd10_type_id order by text_to_timestamp(t_diag_icd10.diag_icd10_record_date_time) asc) as seq                                    
               from t_diag_icd10
                   inner join b_icd10 on t_diag_icd10.diag_icd10_number = b_icd10.icd10_number
                   inner join (select t_diag_icd10.diag_icd10_vn                                                             
                                      ,max(text_to_timestamp(t_diag_icd10.diag_icd10_record_date_time)) as record_date_time
                              from t_diag_icd10 
                                    inner join t_visit on t_diag_icd10.diag_icd10_vn = t_visit.t_visit_id
                              where t_diag_icd10.f_diag_icd10_type_id = '1'
                                    and t_diag_icd10.diag_icd10_active = '1'
                                    and t_visit.f_visit_status_id <> '4'
                                    and t_visit.t_visit_id = $1
                              group by t_diag_icd10.diag_icd10_vn) as max_diag_icd10 
                                    on t_diag_icd10.diag_icd10_vn = max_diag_icd10.diag_icd10_vn                                                 
                                    and text_to_timestamp(t_diag_icd10.diag_icd10_record_date_time) = max_diag_icd10.record_date_time
                                    and t_diag_icd10.f_diag_icd10_type_id = '1') as t_diag_icd10  
                on t_diag_icd10.diag_icd10_vn = t_visit.t_visit_id
                and t_diag_icd10.seq = 1
        left join b_report_12files_map_clinic on t_diag_icd10.b_visit_clinic_id = b_report_12files_map_clinic.b_visit_clinic_id
        left join b_report_12files_std_clinic on b_report_12files_map_clinic.b_report_12files_std_clinic_id = b_report_12files_std_clinic.b_report_12files_std_clinic_id
        left join b_employee as participant on t_diag_icd10.diag_icd10_staff_doctor = participant.b_employee_id
        left join t_person as participant_person on participant.t_person_id = participant_person.t_person_id
        left join f_patient_prefix as participant_prefix on participant_person.f_prefix_id = participant_prefix.f_patient_prefix_id
        left join f_provider_type on participant.f_provider_type_id = f_provider_type.f_provider_type_id
        left join f_visit_service_type on t_visit.f_visit_service_type_id = f_visit_service_type.f_visit_service_type_id
        left join (select t_billing_invoice.t_visit_id
                        ,jsonb_build_array(jsonb_build_object(
                                'system','https://www.nhso.go.th/certificate'
                                ,'value',t_visit_payment.visit_payment_card_number
                            )
                            ,jsonb_build_object(
                                'system','https://www.nhso.go.th/authcode'
                                ,'value',case when t_nhso_authencode.claim_code is not null then t_nhso_authencode.claim_code else '' end
                            )
                        ) as identifier
                        ,t_visit_payment.visit_payment_card_number as subscriberid
                        ,case when (text_to_timestamp(t_visit_payment.visit_payment_card_issue_date)::date is not null and text_to_timestamp(t_visit_payment.visit_payment_card_expire_date)::date is not null)
                                    then case when (text_to_timestamp(t_visit.visit_begin_visit_time)::date 
                                                        between text_to_timestamp(t_visit_payment.visit_payment_card_issue_date)::date and text_to_timestamp(t_visit_payment.visit_payment_card_expire_date)::date)
                                              then 'active'
                                              else 'inactive' end
                              else 'active' end as status
                        ,jsonb_build_object(
                            'coding',jsonb_build_array(jsonb_build_object(
                                'system','http://terminology.hl7.org/CodeSystem/v3-ActCode'
                                ,'code','PUBLICPOL'
                                ,'display','public healthcare'
                            ))
                        ) as type_coding
                        ,jsonb_build_object(
                            'coding',jsonb_build_array(jsonb_build_object(
                                'system','http://terminology.hl7.org/CodeSystem/subscriber-relationship'
                                ,'code','self'
                                ,'display','Self'
                            ))
                        ) as relationship_coding
                        ,jsonb_build_object(
                            'start',case when t_visit_payment.visit_payment_card_issue_date is not null 
                                         then to_char(text_to_timestamp(t_visit_payment.visit_payment_card_issue_date)::date,'YYYY-MM-DD')
                                         else '' end
                            ,'end',case when t_visit_payment.visit_payment_card_issue_date is not null 
                                         then to_char(text_to_timestamp(t_visit_payment.visit_payment_card_expire_date)::date,'YYYY-MM-DD')
                                         else '' end
                        ) as period
                        ,jsonb_build_object(
                            'reference',case when b_contract_payer.b_contract_payer_id is not null 
                                         then b_contract_payer.contract_payer_description
                                         else '' end
                        ) as payor
                        ,jsonb_build_array(jsonb_build_object(
                            'type',jsonb_build_object(
                                'coding',jsonb_build_array(jsonb_build_object(
                                    'system','http://terminology.hl7.org/CodeSystem/coverage-class'
                                    ,'code','group'
                                ))
                            )
                            ,'value',r_rp1853_instype.maininscl
                            ,'name',f_nhso_main_inscl.nhso_main_seq
                        ),jsonb_build_object(
                            'type',jsonb_build_object(
                                'coding',jsonb_build_array(jsonb_build_object(
                                    'system','http://terminology.hl7.org/CodeSystem/coverage-class'
                                    ,'code','subgroup'
                                ))
                            )
                            ,'value',r_rp1853_instype.id
                            ,'name',r_rp1853_instype.inscl_name
                        )) as coverage_class
                        ,sum(cast(t_billing_invoice.billing_invoice_payer_share as decimal(10,2))) as coverage_reimbursement
                        ,jsonb_build_array(jsonb_build_object(
                            'reference','MainHospital'
                            ,'identifier',main_hospital.b_visit_office_id
                            ,'display',main_hospital.visit_office_name1
                        ),
                        jsonb_build_object(
                            'reference','SubHospital'
                            ,'identifier',sub_hospital.b_visit_office_id
                            ,'display',sub_hospital.visit_office_name1
                        )) as contract
                from t_billing_invoice
                    inner join t_visit_payment on t_billing_invoice.t_payment_id = t_visit_payment.t_visit_payment_id
                    inner join t_visit on t_billing_invoice.t_visit_id = t_visit.t_visit_id
                    left join t_nhso_authencode on t_visit.visit_vn = t_nhso_authencode.vn
                    left join b_contract_plans on t_visit_payment.b_contract_plans_id = b_contract_plans.b_contract_plans_id
                    left join r_rp1853_instype on b_contract_plans.r_rp1853_instype_id = r_rp1853_instype.id
                    left join f_nhso_main_inscl on r_rp1853_instype.maininscl = f_nhso_main_inscl.nhso_main_inscl_name
                    left join b_contract_payer on b_contract_plans.b_contract_payer_id = b_contract_payer.b_contract_payer_id
                    left join b_visit_office as main_hospital on t_visit_payment.visit_payment_main_hospital = main_hospital.b_visit_office_id
                    left join b_visit_office as sub_hospital on t_visit_payment.visit_payment_sub_hospital = sub_hospital.b_visit_office_id
                where t_billing_invoice.billing_invoice_active = '1'
                    and t_billing_invoice.t_visit_id = $1
                group by t_billing_invoice.t_billing_invoice_id
                    ,t_visit_payment.t_visit_payment_id
                    ,t_nhso_authencode.t_nhso_authencode_id
                    ,t_visit.t_visit_id
                    ,b_contract_payer.b_contract_payer_id
                    ,r_rp1853_instype.id
                    ,f_nhso_main_inscl.f_nhso_main_inscl_id
                    ,main_hospital.b_visit_office_id
                    ,sub_hospital.b_visit_office_id
            ) as plans on t_visit.t_visit_id = plans.t_visit_id
        left join (select t_visit_vital_sign.t_visit_id
                        ,jsonb_build_object(
                            'status','final'
                            ,'valueQuantity',jsonb_build_object(
                                'value',case when t_visit_vital_sign.visit_vital_sign_weight <> '' then cast(t_visit_vital_sign.visit_vital_sign_weight as numeric) else null end
                                ,'unit','kg'
                            )
                        ) as body_weight
                        ,jsonb_build_object(
                            'status','final'
                            ,'valueQuantity',jsonb_build_object(
                                'value',case when t_visit_vital_sign.visit_vital_sign_height <> '' then cast(t_visit_vital_sign.visit_vital_sign_height as numeric) else null end
                                ,'unit','cm'
                            )
                        ) as body_height
                        ,jsonb_build_object(
                            'status','final'
                            ,'valueQuantity',jsonb_build_object(
                                'value',case when t_visit_vital_sign.visit_vital_sign_temperature <> '' then cast(t_visit_vital_sign.visit_vital_sign_temperature as numeric) else null end
                                ,'unit','cel'
                            )
                        ) as body_temp
                        ,jsonb_build_object(
                            'status','final'
                            ,'valueQuantity',jsonb_build_object(
                                'value',case when t_visit_vital_sign.visit_vital_sign_blood_presure <> '' 
                                             then cast(substring(t_visit_vital_sign.visit_vital_sign_blood_presure,1,(position('/' in t_visit_vital_sign.visit_vital_sign_blood_presure)-1)) as numeric) else null end
                                ,'unit','mmHg'
                            )
                            ,'interpretation',jsonb_build_object(
                                'text',case when t_visit_vital_sign.visit_vital_sign_blood_presure <> '' 
                                            then case when cast(substring(t_visit_vital_sign.visit_vital_sign_blood_presure,1,(position('/' in t_visit_vital_sign.visit_vital_sign_blood_presure)-1)) as numeric) between 90 and 120
                                                      then 'Normal' else 'Abnormal' end
                                            else null end
                            )
                        ) as bp_systolic
                        ,jsonb_build_object(
                            'status','final'
                            ,'valueQuantity',jsonb_build_object(
                                'value',case when t_visit_vital_sign.visit_vital_sign_blood_presure <> '' 
                                             then cast(substring(t_visit_vital_sign.visit_vital_sign_blood_presure,(position('/' in t_visit_vital_sign.visit_vital_sign_blood_presure)+1)) as numeric) else null end
                                ,'unit','mmHg'
                            )
                            ,'interpretation',jsonb_build_object(
                                'text',case when t_visit_vital_sign.visit_vital_sign_blood_presure <> '' 
                                            then case when cast(substring(t_visit_vital_sign.visit_vital_sign_blood_presure,(position('/' in t_visit_vital_sign.visit_vital_sign_blood_presure)+1)) as numeric) between 60 and 80
                                                      then 'Normal' else 'Abnormal' end
                                            else null end
                            )
                        ) as bp_diastolic
                    from t_visit_vital_sign
                        inner join (select t_visit_vital_sign.t_visit_id
                                        ,max(case when (t_visit_vital_sign.visit_vital_sign_check_date <> '' and t_visit_vital_sign.visit_vital_sign_check_time <> '')
                                                 then (text_to_timestamp(t_visit_vital_sign.record_date)::date ||','|| t_visit_vital_sign.record_time)::timestamp with time zone
                                                 else (text_to_timestamp(t_visit_vital_sign.visit_vital_sign_check_date)::date  ||','|| t_visit_vital_sign.visit_vital_sign_check_time)::timestamp with time zone 
                                                 end) as vital_sign_date
                                    from t_visit_vital_sign
                                    where t_visit_vital_sign.visit_vital_sign_active = '1'
                                        and t_visit_vital_sign.t_visit_id = $1
                                    group by t_visit_vital_sign.t_visit_id
                                    ) as max_vital_sign on t_visit_vital_sign.t_visit_id = max_vital_sign.t_visit_id
                                                       and (case when (t_visit_vital_sign.visit_vital_sign_check_date <> '' and t_visit_vital_sign.visit_vital_sign_check_time <> '')
                                                                 then (text_to_timestamp(t_visit_vital_sign.record_date)::date ||','|| t_visit_vital_sign.record_time)::timestamp with time zone
                                                                 else (text_to_timestamp(t_visit_vital_sign.visit_vital_sign_check_date)::date  ||','|| t_visit_vital_sign.visit_vital_sign_check_time)::timestamp with time zone end) 
                                                            = max_vital_sign.vital_sign_date
                    where t_visit_vital_sign.visit_vital_sign_active = '1'
                        and t_visit_vital_sign.t_visit_id = $1
            ) as vital_sign on t_visit.t_visit_id = vital_sign.t_visit_id
        -- lab
        left join (select t_result_lab.t_visit_id
                    ,jsonb_agg(jsonb_build_object(
                                    'status','final'
                                    ,'issued',to_char(text_to_timestamp(t_result_lab.record_date_time),'YYYY-MM-DDT') || to_char(text_to_timestamp(t_result_lab.record_date_time),'HH24:MI:SS.MSZ')
                                    ,'code',jsonb_build_object(
                                                'coding',jsonb_build_array(jsonb_build_object(
                                                    'system','https://tmlt.this.or.th/tmlt/'
                                                    ,'code',b_lab_tmlt.tmltcode
                                                    ,'display',b_lab_tmlt.tmlt_name
                                                ))
                                                ,'text',t_result_lab.result_lab_name
                                            )
                                    ,'valueQuantity',jsonb_build_object(
                                        'value',case when t_result_lab.result_lab_value <> '' and regexp_replace(t_result_lab.result_lab_value , '[^0-9]*', '', 'g') <> ''
                                                     then t_result_lab.result_lab_value::numeric else null end
                                        ,'unit',t_result_lab.result_lab_unit
                                    )
                                    ,'profile_group',t_order.order_common_name
                                    ,'valueString',t_result_lab.result_lab_value
                                    ,'referenceRange',jsonb_build_array(jsonb_build_object(
                                        'low',jsonb_build_object('value',case when t_result_lab.result_lab_min <> '' then t_result_lab.result_lab_min::numeric else null end)
                                        ,'high',jsonb_build_object('value',case when t_result_lab.result_lab_max <> '' then t_result_lab.result_lab_max::numeric else null end)
                                        ,'type',jsonb_build_object(
                                            'coding',COALESCE((case when ((t_result_lab.result_lab_value <> '' and t_result_lab.result_lab_min <> '' and t_result_lab.result_lab_max <> '')
                                                                and (t_result_lab.result_lab_value::numeric between t_result_lab.result_lab_min::numeric and t_result_lab.result_lab_max::numeric))
                                                          then jsonb_build_array(jsonb_build_object(
                                                                    'system','http://terminology.hl7.org/CodeSystem/referencerange-meaning'
                                                                    ,'code','normal'
                                                                    ,'display','Normal Range'
                                                               ))
                                                          else null end), '[]'::jsonb)
                                        )
                                    ))
                                )) as lab_observation
                from t_result_lab
                    inner join t_order on t_result_lab.t_order_id = t_order.t_order_id
                    left join b_phr_map_lab on t_result_lab.b_item_id = b_phr_map_lab.b_item_id
                    left join b_phr_item_lab on b_phr_map_lab.b_phr_item_lab_id = b_phr_item_lab.b_phr_item_lab_id
                    left join b_map_lab_tmlt on t_result_lab.b_item_id = b_map_lab_tmlt.b_item_id
                    left join b_lab_tmlt on b_map_lab_tmlt.b_lab_tmlt_tmltcode = b_lab_tmlt.tmltcode
                where result_lab_active = '1'
                    and t_result_lab.t_visit_id = $1
                group by t_result_lab.t_visit_id
            ) as lab on t_visit.t_visit_id = lab.t_visit_id
      -- drug 
        left join (select t_visit.t_visit_id
                    ,jsonb_agg(jsonb_build_object(
                        'code',jsonb_build_object(
                            'coding',jsonb_build_array(jsonb_build_object(
                                'system','https://www.this.or.th/tmt/gp'
                                ,'code',case when b_drug_tmt.tpucode is not null then b_drug_tmt.tpucode else '' end
                                ,'display',case when b_drug_tmt.tpucode is not null then b_drug_tmt.fsn else '' end
                             ))
                             ,'text',t_order.order_common_name
                         )
                         ,'form',COALESCE(case when map_purch_uom.b_phr_map_unit_id is not null
                                               then jsonb_build_object(
                                                        'coding',jsonb_build_array(jsonb_build_object(
                                                            'system','http://snomed.info/sct'
                                                            ,'code',sct_purch_uom.code
                                                            ,'display',sct_purch_uom.description
                                                        ))
                                                    )
                                                else null end, '{}'::jsonb)
                         ,'finance',jsonb_build_object(
                            'qty',cast(t_order.order_qty as decimal(10,1))
                            ,'unitPrice',cast(t_order.order_price as decimal(10,1))
                         )
                        ,'statement',jsonb_build_object(
                            'status','active'
                            ,'category',jsonb_build_object(
                                'coding',jsonb_build_array(jsonb_build_object(
                                    'system','http://terminology.hl7.org/CodeSystem/medication-statement-category'
                                    ,'code',case when t_visit.f_visit_type_id = '1' then 'inpatient' else 'outpatient' end
                                    ,'display',case when t_visit.f_visit_type_id = '1' then 'Inpatient' else 'Outpatient' end
                                 ))
                             )
                             ,'effectiveDateTime',to_char(text_to_timestamp(t_order.order_executed_date_time),'YYYY-MM-DDT') || to_char(text_to_timestamp(t_order.order_executed_date_time),'HH24:MI:SS.MSZ')
                             ,'note',COALESCE(null, '[]'::jsonb)
                             ,'dosage',jsonb_build_array(jsonb_build_object(
                                'sequence',1
                                ,'text',case when (t_order_drug.order_drug_special_prescription = '0' 
                                                    and b_item_drug_instruction.b_item_drug_instruction_id is not null
                                                    and b_item_drug_frequency.b_item_drug_frequency_id is not null)
                                             then b_item_drug_instruction.item_drug_instruction_number || ' ' || t_order_drug.order_drug_dose || ' ' || b_item_drug_frequency.item_drug_frequency_number
                                             else '' end
                                ,'patientInstruction',case when (t_order_drug.order_drug_special_prescription = '0' 
                                                                and b_item_drug_instruction.b_item_drug_instruction_id is not null
                                                                and b_item_drug_frequency.b_item_drug_frequency_id is not null)
                                                         then b_item_drug_instruction.item_drug_instruction_description 
                                                                || ' ' || t_order_drug.order_drug_dose 
                                                                || ' ' || use_uom.item_drug_uom_description
                                                                || ' ' || b_item_drug_frequency.item_drug_frequency_description
                                                         else '' end
                                ,'timing',COALESCE(case when b_item_drug_frequency.b_item_drug_frequency_id is not null
                                               then jsonb_build_object(
                                                        'repeat',jsonb_build_object(
                                                            'frequency',b_item_drug_frequency.item_drug_frequency_factor::int
                                                            ,'period',1
                                                            ,'periodUnit','d'
                                                        )
                                                    )
                                               else null end, '{}'::jsonb)
                                ,'route',COALESCE(case when b_item_drug_instruction.edqm_route_code <> '0'
                                                       then jsonb_build_object(
                                                            'coding',jsonb_build_array(jsonb_build_object(
                                                                'system','http://standardterms.edqm.eu'
                                                                ,'code',f_edqm_item_route.code
                                                                ,'display',f_edqm_item_route.description
                                                           ))
                                                    )
                                                  else null end, '{}'::jsonb)
                                ,'doseAndRate',jsonb_build_array(jsonb_build_object(
                                    'type',jsonb_build_object(
                                        'coding',jsonb_build_array(jsonb_build_object(
                                            'system','http://terminology.hl7.org/CodeSystem/dose-rate-type'
                                            ,'code','ordered'
                                            ,'display','Ordered'
                                        ))
                                    )
                                    ,'doseQuantity',jsonb_build_object(
                                        'value',t_order_drug.order_drug_dose
                                        ,'unit',use_uom.item_drug_uom_description
                                        ,'system','http://http://snomed.info/sct'
                                        ,'code',case when sct_use_uom.code is not null then sct_use_uom.code else '' end
                                    )
                                ))
                             ))
                         )
                    )) as encounter_med
                from t_order
                    inner join t_order_drug on t_order.t_order_id = t_order_drug.t_order_id
                                           and t_order_drug.order_drug_active = '1'
                    inner join t_visit on t_order.t_visit_id = t_visit.t_visit_id
                    left join b_item_drug_instruction on t_order_drug.b_item_drug_instruction_id = b_item_drug_instruction.b_item_drug_instruction_id
                    left join f_edqm_item_route on b_item_drug_instruction.edqm_route_code = f_edqm_item_route.code
                    left join b_item_drug_frequency on t_order_drug.b_item_drug_frequency_id = b_item_drug_frequency.b_item_drug_frequency_id
                    left join b_item_drug_uom as purch_uom on t_order_drug.b_item_drug_uom_id_purch = purch_uom.b_item_drug_uom_id
                    left join b_phr_map_unit as map_purch_uom on purch_uom.b_item_drug_uom_id = map_purch_uom.b_item_drug_uom_id
                    left join f_sct_item_unit as sct_purch_uom on map_purch_uom.sct_item_unit_code = sct_purch_uom.code
                    left join b_item_drug_uom as use_uom on t_order_drug.b_item_drug_uom_id_use = use_uom.b_item_drug_uom_id
                    left join b_phr_map_unit as map_use_uom on use_uom.b_item_drug_uom_id = map_use_uom.b_item_drug_uom_id
                    left join f_sct_item_unit as sct_use_uom on map_use_uom.sct_item_unit_code = sct_use_uom.code
                    left join b_map_drug_tmt on t_order.b_item_id = b_map_drug_tmt.b_item_id
                    left join b_drug_tmt on b_map_drug_tmt.b_drug_tmt_tpucode = b_drug_tmt.tpucode
                where t_order.f_order_status_id not in ('0','3')
                    and t_order.order_charge_complete = '1'
                    and t_order.f_item_group_id = '1'
                    and t_visit.t_visit_id = $1
                group by t_visit.t_visit_id
        ) as drugs on t_visit.t_visit_id = drugs.t_visit_id
      -- appointment
        left join (SELECT appointment.t_visit_id
        				,jsonb_agg(jsonb_build_object(
                        'status',appointment.appoint_status
                        ,'serviceCategory',jsonb_build_array(appointment.appoint_servicecategory)
                        ,'serviceType',jsonb_build_array(appointment.appoint_servicetype)
                        ,'specialty',jsonb_build_array(appointment.appoint_specialty)
                        ,'appointmentType',appointment.appoint_type
                        ,'reason',appointment.appoint_reason
                        ,'description',appointment.appoint_description
                        ,'start',appointment.appoint_start
                        ,'end',appointment.appoint_end
                        ,'created',appointment.appoint_created
                        ,'note',jsonb_build_array(appointment.appoint_note)
                        ,'patientInstruction',COALESCE(null, '[]'::jsonb)
                        ,'basedOn',COALESCE(null, '[]'::jsonb)
                        ,'subject',appointment.appoint_subject
                        ,'participant',appointment.appoint_participant
                       )) AS encounter_appointment
                    FROM (select t_patient_appointment.visit_id_make_appointment as t_visit_id
                    ,case when t_patient_appointment.patient_appointment_status in ('0','6') then 'booked'
                          when t_patient_appointment.patient_appointment_status in ('1','4','5') then 'arrived'
                          when t_patient_appointment.patient_appointment_status = '2' then 'noshow'
                          when t_patient_appointment.patient_appointment_status = '3' then 'cancelled'
                          end as appoint_status
                    ,jsonb_build_object(
                        'coding',jsonb_build_array(jsonb_build_object(
                            'system','http://terminology.hl7.org/CodeSystem/service-category'
                            ,'code','17'
                            ,'display','General Practice'
                        ))
                    ) as appoint_servicecategory
                    ,jsonb_build_object(
                        'coding',jsonb_build_array(jsonb_build_object(
                            'system','http://terminology.hl7.org/CodeSystem/service-type'
                            ,'code','124'
                            ,'display','General Practice'
                        ))
                    ) as appoint_servicetype
                    ,jsonb_build_object(
                        'coding',jsonb_build_array(jsonb_build_object(
                            'system','http://snomed.info/sct'
                            ,'code','394814009'
                            ,'display','General Practice'
                        ))
                    ) as appoint_specialty
                    ,jsonb_build_object(
                        'coding',jsonb_build_array(jsonb_build_object(
                            'system','http://terminology.hl7.org/CodeSystem/v2-0276'
                            ,'code','FOLLOWUP'
                            ,'display','A follow up visit from a previous appointment'
                        ))
                    ) as appoint_type
					,jsonb_build_array(jsonb_build_object(
						'reference',jsonb_build_object(
							'reference','Condition/example'
							,'display',t_patient_appointment.patient_appointment
						)
					)) as appoint_reason
                    ,t_patient_appointment.patient_appointment_notice as appoint_description
                    ,to_char(text_to_timestamp(t_patient_appointment.patient_appointment_date)::date,'YYYY-MM-DDT')
                        || to_char((text_to_timestamp(t_patient_appointment.patient_appointment_date)::date || ',' || t_patient_appointment.patient_appointment_time)::timestamp with time zone,'HH24:MI:SS.MSZ') as appoint_start
                    ,to_char(text_to_timestamp(t_patient_appointment.patient_appointment_date)::date,'YYYY-MM-DDT')
                        || to_char((text_to_timestamp(t_patient_appointment.patient_appointment_date)::date || ',' || t_patient_appointment.patient_appointment_end_time)::timestamp with time zone,'HH24:MI:SS.MSZ') as appoint_end
                    ,to_char(text_to_timestamp(t_patient_appointment.patient_appointment_record_date_time),'YYYY-MM-DD') as appoint_created
                    ,jsonb_build_object(
                        'text',t_patient_appointment.patient_appointment_notice
                    ) as appoint_note
                    ,jsonb_build_object(
                        'reference','Patient/example'
                        ,'display',case when (f_patient_prefix.f_patient_prefix_id <> '000' and f_patient_prefix.f_patient_prefix_id is not null) then f_patient_prefix.patient_prefix_description else '' end 
                                        || t_patient.patient_firstname || ' ' || t_patient.patient_lastname
                    ) as appoint_subject
                    ,jsonb_build_array(jsonb_build_object(
                        'actor',jsonb_build_object(
                            'reference','Patient/example'
                            ,'display',case when (f_patient_prefix.f_patient_prefix_id <> '000' and f_patient_prefix.f_patient_prefix_id is not null) then f_patient_prefix.patient_prefix_description else '' end 
                                        || t_patient.patient_firstname || ' ' || t_patient.patient_lastname
                        )
                        ,'required',true
                        ,'status','accepted'
                    ),jsonb_build_object(
                        'type',jsonb_build_array(jsonb_build_object(
                            'coding',jsonb_build_array(jsonb_build_object(
                                'system','http://terminology.hl7.org/CodeSystem/v3-ParticipationType'
                                ,'code','ATND'
                            ))
                        ))
                    ),jsonb_build_object(
                        'actor',jsonb_build_object(
                            'reference','Practitioner/example'
                            ,'display',case when (doctor_prefix.f_patient_prefix_id <> '000' and doctor_prefix.f_patient_prefix_id is not null) then doctor_prefix.patient_prefix_description else '' end 
                                        || doctor.person_firstname || ' ' || doctor.person_lastname
                        )
                        ,'required',true
                        ,'status','accepted'
                    ),jsonb_build_object(
                        'actor',jsonb_build_object(
                            'reference','Location/1'
                            ,'display',b_service_point.service_point_description
                        )
                        ,'required',true
                        ,'status','accepted'
                    )) as appoint_participant
                from t_patient_appointment
                    inner join t_visit on t_patient_appointment.visit_id_make_appointment = t_visit.t_visit_id
                    inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
                    left join f_patient_prefix on t_patient.f_patient_prefix_id = f_patient_prefix.f_patient_prefix_id
                    left join b_employee on t_patient_appointment.patient_appointment_doctor = b_employee.b_employee_id
                    left join t_person as doctor on b_employee.t_person_id = doctor.t_person_id
                    left join f_patient_prefix as doctor_prefix on doctor.f_prefix_id = doctor_prefix.f_patient_prefix_id
                    left join b_service_point on t_patient_appointment.patient_appointment_servicepoint = b_service_point.b_service_point_id
                where t_patient_appointment.patient_appointment_active = '1'
                    and t_patient_appointment.visit_id_make_appointment = $1
        ) as appointment
        	GROUP BY appointment.t_visit_id) AS appointment on t_visit.t_visit_id = appointment.t_visit_id
      -- vaccines 
        left join (select vaccines.t_visit_id
                ,jsonb_agg(jsonb_build_object(
					'managingOrganization',jsonb_build_object(
						'type','Organization'
						,'identifier',jsonb_build_object(
							'use','official'
							,'system','https://bps.moph.go.th/hcode/5'
							,'value',b_site.b_visit_office_id
						)
						,'display',b_site.site_full_name
					),
                    'status','completed'
                    ,'vaccineCode',jsonb_build_object(
                        'coding',jsonb_build_array(jsonb_build_object(
                            'system','https://refcode.moph.go.th/vaccine'
                            ,'code',vaccines.vaccinecode_code
							,'display',vaccines.vaccinecode_text
                        ))
                        ,'text',vaccines.vaccinecode_text
                    )
					,'encounterRefCode',vaccines.refcode
                    ,'occurrenceDateTime',to_char(vaccines.occurrencedatetime,'YYYY-MM-DDT') || to_char(vaccines.occurrencedatetime,'HH24:MI:SS.MSZ')
                    ,'primarySource',true
                    ,'location',vaccines.vaccine_location
                    ,'manufacturer',vaccines.manufacturer
                    ,'lotNumber',vaccines.vaccine_lot
                    ,'expirationDate',to_char(vaccines.expirationdate,'YYYY-MM-DD')
                    ,'site',COALESCE(case when (vaccines.act_site_code <> '0' and vaccines.act_site_code is not null)
                        then jsonb_build_object(
                            'coding',jsonb_build_array(jsonb_build_object(
                                'system','http://terminology.hl7.org/CodeSystem/v3-ActSite'
                                ,'code',vaccines.act_site_code
                                ,'display',vaccines.act_site_desc
                            ))
                        ) else null end, '{}'::jsonb)
                    ,'route',COALESCE(case when (vaccines.immunization_route_code <> '0' and vaccines.immunization_route_code is not null)
                        then jsonb_build_object(
                            'coding',jsonb_build_array(jsonb_build_object(
                                'system','http://terminology.hl7.org/CodeSystem/v3-RouteOfAdministration'
                                ,'code',vaccines.immunization_route_code
                                ,'display',vaccines.immunization_route_desc
                            ))
                        ) else null end, '{}'::jsonb)
                    ,'doseQuantity',jsonb_build_object(
                            'value',vaccines.dose_value
                            ,'system','http://unitsofmeasure.org'
                            ,'code',vaccines.dose_code
                    )
                    ,'note',jsonb_build_array(jsonb_build_object(
                        'text',vaccines.vaccine_note
                    ))
                    ,'reasonCode',jsonb_build_array(jsonb_build_object(
                        'coding',jsonb_build_array(jsonb_build_object(
                            'system','http://snomed.info/sct'
                            ,'code','429060002'
                        ))
                    ))
                    ,'performer',jsonb_build_object(
                        'license_no',vaccines.license_no
                        ,'name',vaccines.performer_name
                    )
                )) as immunization
            from (select t_health_epi.t_visit_id
                    ,t_health_epi_detail.t_health_epi_detail_id
                    ,case when position(':' in b_health_epi_group.health_epi_group_description_particular) > 0
                                        then substring(b_health_epi_group.health_epi_group_description_particular,1,(position(':' in b_health_epi_group.health_epi_group_description_particular)-1))
                                        else b_health_epi_group.health_epi_group_description_particular end as vaccinecode_code
                    ,b_health_epi_group.health_epi_group_description as vaccinecode_text
					,t_patient.patient_pid || t_health_epi_detail.ref_code as refcode
                    ,text_to_timestamp(t_health_epi_detail.record_date_time) as occurrencedatetime
                    ,b_site.site_full_name as vaccine_location
                    ,case when b_item_manufacturer.b_item_manufacturer_id is not null then b_item_manufacturer.item_manufacturer_description else '' end as manufacturer
                    ,t_health_epi_detail.health_epi_detail_lot as vaccine_lot
                    ,text_to_timestamp(t_health_epi_detail.health_epi_exp)::date as expirationdate
                    ,t_health_epi_detail.act_site_code as act_site_code
                    ,f_hl7_act_site.description as act_site_desc
                    ,t_health_epi_detail.immunization_route_code as immunization_route_code
                    ,f_hl7_immunization_route.description as immunization_route_desc
                    ,t_order_drug.order_drug_dose as dose_value
                    ,case when b_item_drug_uom.ucum_unit_code <> '0' then b_item_drug_uom.ucum_unit_code else '' end as dose_code
                    ,t_health_epi.health_epi_notice as vaccine_note
                    ,case when b_employee.f_employee_authentication_id = '3' and b_employee.employee_number  <> '' 
                               and (case when t_person.t_person_id is not null
                                         then f_patient_prefix.patient_prefix_description not ilike 'ทพ.%' or f_patient_prefix.patient_prefix_description not ilike 'ทพญ.%'
                                         else b_employee.employee_firstname not ilike 'ทพ.%' or b_employee.employee_firstname not ilike 'ทพญ.%' end)
                          then  'ว'||b_employee.employee_number
                          when b_employee.f_employee_authentication_id = '3' and b_employee.employee_number  <> ''
                               and (case when t_person.t_person_id is not null
                                         then f_patient_prefix.patient_prefix_description ilike 'ทพ.%' or f_patient_prefix.patient_prefix_description ilike 'ทพญ.%'
                                         else b_employee.employee_firstname ilike 'ทพ.%' or b_employee.employee_firstname ilike 'ทพญ.%' end)
                          then  'ท'||b_employee.employee_number
                          when b_employee.f_employee_authentication_id = '2' and b_employee.employee_number  <> ''  then  'พ'||b_employee.employee_number
                          when b_employee.f_employee_authentication_id = '6' and b_employee.employee_number  <> ''  then  'ภ'||b_employee.employee_number
                          when b_employee.f_employee_authentication_id not in ('2','3','6') and b_employee.employee_number  <> ''  then b_employee.employee_number
                          else ''  end as license_no
                    ,(case when (f_patient_prefix.f_patient_prefix_id <> '000' and f_patient_prefix.f_patient_prefix_id is not null) then f_patient_prefix.patient_prefix_description else '' end)
                     || t_person.person_firstname || ' ' || t_person.person_lastname as performer_name
                from t_health_epi
					inner join t_visit on t_health_epi.t_visit_id = t_visit.t_visit_id
                    inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
                    inner join t_health_epi_detail on t_health_epi.t_health_epi_id = t_health_epi_detail.t_health_epi_id
                    inner join b_health_epi_group on t_health_epi_detail.b_health_epi_set_id = b_health_epi_group.b_health_epi_group_id
                    inner join b_health_epi_item on b_health_epi_group.b_health_epi_group_id = b_health_epi_item.b_health_epi_group_id
                    inner join t_order on b_health_epi_item.b_item_id = t_order.b_item_id
                                      and t_health_epi.t_visit_id = t_order.t_visit_id
                                      and t_order.f_order_status_id not in ('0','3')
                    inner join t_order_drug on t_order.t_order_id = t_order_drug.t_order_id
                                           and t_order_drug.order_drug_active = '1'
                    left join b_item_drug_uom on t_order_drug.b_item_drug_uom_id_use = b_item_drug_uom.b_item_drug_uom_id
                    left join f_hl7_vaccine_code on b_health_epi_group.hl7_vaccine_code = f_hl7_vaccine_code.code
                    left join b_item_manufacturer on t_health_epi_detail.b_item_manufacturer_id = b_item_manufacturer.b_item_manufacturer_id
                    left join f_hl7_act_site on t_health_epi_detail.act_site_code = f_hl7_act_site.code
                    left join f_hl7_immunization_route on t_health_epi_detail.immunization_route_code = f_hl7_immunization_route.code
                    left join b_employee on t_health_epi.health_epi_staff_record = b_employee.b_employee_id
                    left join t_person on b_employee.t_person_id = t_person.t_person_id
                    left join f_patient_prefix on t_person.f_prefix_id = f_patient_prefix.f_patient_prefix_id
                    cross join b_site
                where t_health_epi.health_epi_active = '1'
                    and t_health_epi.t_visit_id = $1
                group by t_health_epi.t_health_epi_id
					,t_patient.patient_pid
					,t_health_epi_detail.ref_code
                    ,t_health_epi_detail.t_health_epi_detail_id
                    ,b_health_epi_group.b_health_epi_group_id
                    ,b_site.site_full_name
                    ,b_item_manufacturer.b_item_manufacturer_id
                    ,f_hl7_act_site.id
                    ,f_hl7_immunization_route.id
                    ,t_order_drug.t_order_drug_id
                    ,b_item_drug_uom.ucum_unit_code
                    ,b_employee.b_employee_id
                    ,t_person.t_person_id
                    ,f_patient_prefix.f_patient_prefix_id
                union
                select t_health_vaccine_covid19.t_visit_id
                    ,t_health_vaccine_covid19.t_health_vaccine_covid19_id
                    ,case when position(':' in b_health_epi_group.health_epi_group_description_particular) > 0
                                        then substring(b_health_epi_group.health_epi_group_description_particular,1,(position(':' in b_health_epi_group.health_epi_group_description_particular)-1))
                                        else b_health_epi_group.health_epi_group_description_particular end as vaccinecode_code
                    ,b_health_epi_group.health_epi_group_description as vaccinecode_text
					,t_patient.patient_pid || t_health_vaccine_covid19.ref_code as refcode
                    ,(t_health_vaccine_covid19.receive_date || ',' || t_health_vaccine_covid19.receive_time)::timestamp with time zone as occurrencedatetime
                    ,b_site.site_full_name as vaccine_location
                    ,case when f_vaccine_manufacturer.f_vaccine_manufacturer_id is not null then f_vaccine_manufacturer.vaccine_manufacturer_name else '' end as manufacturer
                    ,b_health_vaccine.lot_number as vaccine_lot
                    ,b_health_vaccine.expire_date as expirationdate
                    ,t_health_vaccine_covid19.act_site_code as act_site_code
                    ,f_hl7_act_site.description as act_site_desc
                    ,t_health_vaccine_covid19.immunization_route_code as immunization_route_code
                    ,f_hl7_immunization_route.description as immunization_route_desc
                    ,t_order_drug.order_drug_dose as dose_value
                    ,case when b_item_drug_uom.ucum_unit_code <> '0' then b_item_drug_uom.ucum_unit_code else '' end as dose_code
                    ,t_health_vaccine_covid19.description as vaccine_note
                    ,case when b_employee.f_employee_authentication_id = '3' and b_employee.employee_number  <> '' 
                               and (case when t_person.t_person_id is not null
                                         then f_patient_prefix.patient_prefix_description not ilike 'ทพ.%' or f_patient_prefix.patient_prefix_description not ilike 'ทพญ.%'
                                         else b_employee.employee_firstname not ilike 'ทพ.%' or b_employee.employee_firstname not ilike 'ทพญ.%' end)
                          then  'ว'||b_employee.employee_number
                          when b_employee.f_employee_authentication_id = '3' and b_employee.employee_number  <> ''
                               and (case when t_person.t_person_id is not null
                                         then f_patient_prefix.patient_prefix_description ilike 'ทพ.%' or f_patient_prefix.patient_prefix_description ilike 'ทพญ.%'
                                         else b_employee.employee_firstname ilike 'ทพ.%' or b_employee.employee_firstname ilike 'ทพญ.%' end)
                          then  'ท'||b_employee.employee_number
                          when b_employee.f_employee_authentication_id = '2' and b_employee.employee_number  <> ''  then  'พ'||b_employee.employee_number
                          when b_employee.f_employee_authentication_id = '6' and b_employee.employee_number  <> ''  then  'ภ'||b_employee.employee_number
                          when b_employee.f_employee_authentication_id not in ('2','3','6') and b_employee.employee_number  <> ''  then b_employee.employee_number
                          else ''  end as license_no
                    ,(case when (f_patient_prefix.f_patient_prefix_id <> '000' and f_patient_prefix.f_patient_prefix_id is not null) then f_patient_prefix.patient_prefix_description else '' end)
                     || t_person.person_firstname || ' ' || t_person.person_lastname as performer_name
                from t_health_vaccine_covid19
					inner join t_visit on t_health_vaccine_covid19.t_visit_id = t_visit.t_visit_id
                    inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
                    inner join b_health_epi_group on t_health_vaccine_covid19.b_health_epi_group_id = b_health_epi_group.b_health_epi_group_id
                    inner join b_health_epi_item on b_health_epi_group.b_health_epi_group_id = b_health_epi_item.b_health_epi_group_id
                    inner join t_order on b_health_epi_item.b_item_id = t_order.b_item_id
                                      and t_health_vaccine_covid19.t_visit_id = t_order.t_visit_id
                                      and t_order.f_order_status_id not in ('0','3')
                    inner join t_order_drug on t_order.t_order_id = t_order_drug.t_order_id
                                           and t_order_drug.order_drug_active = '1'
                    left join b_item_drug_uom on t_order_drug.b_item_drug_uom_id_use = b_item_drug_uom.b_item_drug_uom_id
                    left join f_hl7_vaccine_code on b_health_epi_group.hl7_vaccine_code = f_hl7_vaccine_code.code
                    inner join b_health_vaccine on t_health_vaccine_covid19.b_health_vaccine_id = b_health_vaccine.b_health_vaccine_id
                    left join f_vaccine_manufacturer on b_health_vaccine.f_vaccine_manufacturer_id = f_vaccine_manufacturer.f_vaccine_manufacturer_id
                    left join f_hl7_act_site on t_health_vaccine_covid19.act_site_code = f_hl7_act_site.code
                    left join f_hl7_immunization_route on t_health_vaccine_covid19.immunization_route_code = f_hl7_immunization_route.code
                    left join b_employee on t_health_vaccine_covid19.user_procedure_id = b_employee.b_employee_id
                    left join t_person on b_employee.t_person_id = t_person.t_person_id
                    left join f_patient_prefix on t_person.f_prefix_id = f_patient_prefix.f_patient_prefix_id
                    cross join b_site
                where t_health_vaccine_covid19.active = '1'
                    and t_health_vaccine_covid19.t_visit_id = $1
                group by t_health_vaccine_covid19.t_visit_id
					,t_patient.patient_pid
					,t_health_vaccine_covid19.ref_code
                    ,t_health_vaccine_covid19.t_health_vaccine_covid19_id
                    ,b_health_epi_group.b_health_epi_group_id
                    ,b_site.site_full_name
                    ,f_vaccine_manufacturer.f_vaccine_manufacturer_id
                    ,b_health_vaccine.b_health_vaccine_id
                    ,f_hl7_act_site.id
                    ,f_hl7_immunization_route.id
                    ,t_order_drug.t_order_drug_id
                    ,b_item_drug_uom.ucum_unit_code
                    ,b_employee.b_employee_id
                    ,t_person.t_person_id
                    ,f_patient_prefix.f_patient_prefix_id
            ) as vaccines
			cross join b_site
            group by vaccines.t_visit_id
        ) as vaccine on t_visit.t_visit_id = vaccine.t_visit_id
      -- allergy 
        left join (select allergy.t_patient_id
                    ,jsonb_agg(jsonb_build_object(
                        'managingOrganization',jsonb_build_object(
                            'type','Organization'
                            ,'identifier',jsonb_build_object(
                                'use','official'
                                ,'system','https://bps.moph.go.th/hcode/5'
                                ,'value',allergy.b_visit_office_id
                            )
                            ,'display',allergy.site_full_name
                        )
                        ,'identifier',jsonb_build_array(jsonb_build_object(
                            'system','http://acme.com/ids/patients/risks'
                            ,'value','49476535'
                        ))
                        ,'clinicalStatus',jsonb_build_object(
                            'coding',jsonb_build_array(jsonb_build_object(
                                'system','http://terminology.hl7.org/CodeSystem/allergyintolerance-clinical'
                                ,'code','active'
                                ,'display','Active'
                            ))
                        )
                        ,'verificationStatus',jsonb_build_object(
                            'coding',jsonb_build_array(jsonb_build_object(
                                'system','http://terminology.hl7.org/CodeSystem/allergyintolerance-verification'
                                ,'code','confirmed'
                                ,'display','Confirmed'
                            ))
                        )
                        ,'type','allergy'
                        ,'category','{medication}'::text[]
                        ,'criticality',case when allergy.f_allergy_level_id <> '1' then 'high' else 'low' end
                        ,'code',jsonb_build_object(
                            'coding',jsonb_build_array(jsonb_build_object(
                                'system','https://www.this.or.th/tmt/substance'
                                ,'code',case when allergy.b_drug_tmt_tpucode is null then '' else allergy.b_drug_tmt_tpucode end
                                ,'display',case when allergy.item_drug_standard_description is null then '' else allergy.item_drug_standard_description end
                            ))
                        )
                        ,'seriousness',jsonb_build_object(
                            'coding',jsonb_build_array(jsonb_build_object(
                                'system','https://www.code.com/seriousness'
                                ,'code',case when allergy.f_allergy_level_id is null then '' else allergy.f_allergy_level_id end
                                ,'display',case when allergy.allergy_level_description is null then '' else allergy.allergy_level_description end
                            ))
                        )
                        ,'allergyGroup',jsonb_build_object(
                            'coding',jsonb_build_array(jsonb_build_object(
                                'system','https://www.code.com/allergy_group'
                                ,'code',''
                                ,'display',''
                            ))
                        )
                        ,'allergyInformationSource',jsonb_build_object(
                            'coding',jsonb_build_array(jsonb_build_object(
                                'system','https://www.code.com/allergy_source'
                                ,'code',case when allergy.f_allergy_informant_id is null then '' else allergy.f_allergy_informant_id end
                                ,'display',case when allergy.allergy_informant_description is null then '' else allergy.allergy_informant_description end
                            ))
                        )
                        ,'allergyResult',jsonb_build_object(
                            'coding',jsonb_build_array(jsonb_build_object(
                                'system','https://www.code.com/allergy_result'
                                ,'code','1'
                                ,'display','หายเป็นปกติ'
                            ))
                        )
                        ,'naranjoResult',jsonb_build_object(
                            'coding',jsonb_build_array(jsonb_build_object(
                                'system','https://www.code.com/allergy_naranjo_result'
                                ,'code',case when allergy.f_naranjo_interpretation_id is null then '' else allergy.f_naranjo_interpretation_id end
                                ,'display',case when allergy.naranjo_interpretation_detail is null then '' else allergy.naranjo_interpretation_detail end
                            ))
                        )
                        ,'preventable',false
                        ,'preventableScore',allergy.naranjo_total
                        ,'recordedDate',allergy.record_date_time
                        ,'recordOfficer',jsonb_build_object(
                            'reference','Practitioner/' || allergy.participant_identifier
                            ,'identifier',allergy.participant_identifier
                            ,'display',allergy.participant_display
                        )
                        ,'reaction',jsonb_build_array(jsonb_build_object(
                            'manifestation',jsonb_build_array(jsonb_build_object(
                                'coding',jsonb_build_array(jsonb_build_object(
                                    'system','http://snomed.info/sct'
                                    ,'code','404684003'
                                    ,'display',allergy.drug_allergy_symtom
                                ))
                            ))
                        ))
                    )) as allergy_intolerance
                from (select t_patient_drug_allergy.t_patient_id
                        ,b_site.b_visit_office_id
                        ,b_site.site_full_name
                        ,f_allergy_level.f_allergy_level_id
                        ,f_allergy_level.allergy_level_description
                        ,max(b_map_drug_tmt.b_drug_tmt_tpucode) as b_drug_tmt_tpucode
                        ,b_item_drug_standard.item_drug_standard_description
                        ,f_allergy_informant.f_allergy_informant_id
                        ,f_allergy_informant.allergy_informant_description
                        ,f_naranjo_interpretation.f_naranjo_interpretation_id
                        ,f_naranjo_interpretation.naranjo_interpretation_detail
                        ,case when t_naranjo.naranjo_total is not null then t_naranjo.naranjo_total else 0 end as naranjo_total
                        ,to_char(text_to_timestamp(t_patient_drug_allergy.record_date_time),'YYYY-MM-DDT') || to_char(text_to_timestamp(t_patient_drug_allergy.record_date_time),'HH24:MI:SS.MSZ') as record_date_time
                        ,case when b_employee.f_employee_authentication_id = '3' and b_employee.employee_number  <> '' 
                                  and (case when t_person.t_person_id is not null
                                            then f_patient_prefix.patient_prefix_description not ilike 'ทพ.%' or f_patient_prefix.patient_prefix_description not ilike 'ทพญ.%'
                                            else b_employee.employee_firstname not ilike 'ทพ.%' or b_employee.employee_firstname not ilike 'ทพญ.%' end)
                              then  'ว'||b_employee.employee_number
                              when b_employee.f_employee_authentication_id = '3' and b_employee.employee_number  <> ''
                                  and (case when t_person.t_person_id is not null
                                            then f_patient_prefix.patient_prefix_description ilike 'ทพ.%' or f_patient_prefix.patient_prefix_description ilike 'ทพญ.%'
                                            else b_employee.employee_firstname ilike 'ทพ.%' or b_employee.employee_firstname ilike 'ทพญ.%' end)
                              then  'ท'||b_employee.employee_number
                              when b_employee.f_employee_authentication_id = '2' and b_employee.employee_number  <> ''  then  'พ'||b_employee.employee_number
                              when b_employee.f_employee_authentication_id = '6' and b_employee.employee_number  <> ''  then  'ภ'||b_employee.employee_number
                              when b_employee.f_employee_authentication_id not in ('2','3','6') and b_employee.employee_number  <> ''  then b_employee.employee_number
                              else '' end as participant_identifier
                        ,(case when (f_patient_prefix.f_patient_prefix_id <> '000' and f_patient_prefix.f_patient_prefix_id is not null) then f_patient_prefix.patient_prefix_description else '' end)
                                    || t_person.person_firstname || ' ' || t_person.person_lastname as participant_display
                        ,t_patient_drug_allergy.drug_allergy_symtom
                    from t_patient_drug_allergy
                        inner join b_item_drug_standard on t_patient_drug_allergy.b_item_drug_standard_id = b_item_drug_standard.b_item_drug_standard_id
                        inner join b_item_drug_standard_map_item on b_item_drug_standard.b_item_drug_standard_id = b_item_drug_standard_map_item.b_item_drug_standard_id
                        inner join b_item on b_item_drug_standard_map_item.b_item_id = b_item.b_item_id
                        inner join b_map_drug_tmt on b_item.b_item_id = b_map_drug_tmt.b_item_id
                        inner join t_visit on t_patient_drug_allergy.t_patient_id = t_visit.t_patient_id
                        left join t_naranjo on t_patient_drug_allergy.t_patient_drug_allergy_id = t_naranjo.t_patient_drug_allergy_id
                        left join f_allergy_level on t_patient_drug_allergy.f_allergy_level_id = f_allergy_level.f_allergy_level_id
                        left join f_naranjo_interpretation on t_patient_drug_allergy.f_naranjo_interpretation_id = f_naranjo_interpretation.f_naranjo_interpretation_id
                        left join f_allergy_informant on t_patient_drug_allergy.f_allergy_informant_id = f_allergy_informant.f_allergy_informant_id
                        left join b_employee on t_patient_drug_allergy.doctor_diag_id = b_employee.b_employee_id
                        left join t_person on b_employee.t_person_id = t_person.t_person_id
                        left join f_patient_prefix on t_person.f_prefix_id = f_patient_prefix.f_patient_prefix_id
                        cross join b_site
                    where t_patient_drug_allergy.active = '1'
                        and t_visit.t_visit_id = $1
                    group by t_patient_drug_allergy.t_patient_id
                        ,b_site.b_visit_office_id
                        ,b_site.site_full_name
                        ,f_allergy_level.f_allergy_level_id
                        ,f_allergy_informant.f_allergy_informant_id
                        ,f_naranjo_interpretation.f_naranjo_interpretation_id
                        ,b_item_drug_standard.item_drug_standard_description
                        ,t_naranjo.naranjo_total
                        ,t_patient_drug_allergy.record_date_time
                        ,b_employee.b_employee_id
                        ,t_person.t_person_id
                        ,f_patient_prefix.f_patient_prefix_id
                        ,t_patient_drug_allergy.drug_allergy_symtom
                ) as allergy
                group by allergy.t_patient_id
        ) as allergy on allergy.t_patient_id = t_patient.t_patient_id
        -- chronic
        left join (select t_chronic.t_patient_id
                ,jsonb_agg(jsonb_build_object(
                    'managingOrganization',jsonb_build_object(
                        'type','Organization'
                        ,'identifier',jsonb_build_object(
                            'use','official'
                            ,'system','https://bps.moph.go.th/hcode/5'
                            ,'value',t_chronic.b_visit_office_id
                        )
                        ,'display',t_chronic.site_full_name
                    )
                   ,'code',jsonb_build_object(
                        'coding',jsonb_build_array(jsonb_build_object(
                            'system','http://terminology.hl7.org/CodeSystem/code'
                            ,'code',case when regexp_replace(t_chronic.group_chronic_number , '[^0-9]*', '', 'g') <> ''
                                         then lpad(regexp_replace(t_chronic.group_chronic_number , '[^0-9]*', '', 'g'),3,'0')
                                         else '' end
                            ,'display',t_chronic.group_chronic_description_th
                        ))
                    )
                    ,'clinicalStatus',jsonb_build_object(
                        'coding',jsonb_build_array(jsonb_build_object(
                            'system','http://terminology.hl7.org/CodeSystem/code'
                            ,'code',case when t_chronic.f_chronic_discharge_status_id is not null
                                         then t_chronic.f_chronic_discharge_status_id
                                         else '' end
                            ,'display',case when t_chronic.f_chronic_discharge_status_id is not null
                                          then t_chronic.chronic_discharge_status_description
                                          else '' end
                        ))
                    )
                    ,'clinicalText',t_chronic.icd10_description
                    ,'registerDate',case when t_chronic.chronic_diagnosis_date is not null
                                          then to_char(t_chronic.chronic_diagnosis_date,'YYYY-MM-DD')
                                          else '' end
                    ,'dischargeDate',case when t_chronic.chronic_discharge_date is not null
                                          then to_char(t_chronic.chronic_discharge_date,'YYYY-MM-DD')
                                          else '' end
                    ,'recordedDate',to_char(t_chronic.record_date_time,'YYYY-MM-DDT') || to_char(t_chronic.record_date_time,'HH24:MI:SS.MSZ')
                    ,'description',jsonb_build_array(t_chronic.chronic_notice)
                )) as chronic_disease
            from(select t_chronic.t_patient_id
                ,b_site.b_visit_office_id
                ,b_site.site_full_name
                ,b_group_chronic.group_chronic_number
                ,b_group_chronic.group_chronic_description_th
                ,b_icd10.icd10_description
                ,f_chronic_discharge_status.f_chronic_discharge_status_id
                ,f_chronic_discharge_status.chronic_discharge_status_description
                ,min(text_to_timestamp(t_chronic.chronic_diagnosis_date)) as chronic_diagnosis_date
                ,max(text_to_timestamp(t_chronic.chronic_discharge_date)) as chronic_discharge_date
                ,max(text_to_timestamp(t_chronic.record_date_time)) as record_date_time
                ,t_chronic.chronic_notice
            from t_chronic 
                inner join t_patient on t_chronic.t_patient_id = t_patient.t_patient_id
                inner join t_visit on t_patient.t_patient_id = t_visit.t_patient_id
                inner join b_group_icd10 on t_chronic.chronic_icd10 = b_group_icd10.group_icd10_number
                inner join b_group_chronic on b_group_icd10.b_group_chronic_id = b_group_chronic.b_group_chronic_id
                inner join b_icd10 on t_chronic.chronic_icd10 = b_icd10.icd10_number
                left join f_chronic_discharge_status on t_chronic.f_chronic_discharge_status_id = f_chronic_discharge_status.f_chronic_discharge_status_id
                cross join b_site
            where t_chronic.chronic_active = '1'
                and t_visit.t_visit_id = $1
            group by t_chronic.t_patient_id
                ,b_site.b_visit_office_id
                ,b_site.site_full_name
                ,b_group_chronic.group_chronic_number
                ,b_group_chronic.group_chronic_description_th
                ,b_icd10.icd10_description
                ,f_chronic_discharge_status.f_chronic_discharge_status_id
                ,f_chronic_discharge_status.chronic_discharge_status_description
                ,t_chronic.chronic_notice
           ) as t_chronic
           group by t_chronic.t_patient_id
        ) as chronic on chronic.t_patient_id = t_patient.t_patient_id
        -- xray
        left join (select t_visit.t_visit_id
                    ,jsonb_agg(jsonb_build_object(
                        'status','final'
                        ,'category',jsonb_build_array(jsonb_build_object(
                            'coding',jsonb_build_array(jsonb_build_object(
                                'system','http://snomed.info/sct'
                                ,'code','394914008'
                                ,'display','Radiology'
                            ))
                        ))
                        ,'code',jsonb_build_object(
                            'coding',jsonb_build_array(jsonb_build_object(
                                'system','http://snomed.info/sct'
                                ,'code','429858000'
                                ,'display',t_order.order_common_name
                            ))
                            ,'text',t_order.order_common_name
                        )
                        ,'subject',jsonb_build_object(
                            'display',t_order.order_common_name
                        )
                        ,'effectiveDateTime',to_char(text_to_timestamp(t_order.order_executed_date_time),'YYYY-MM-DDT') || to_char(text_to_timestamp(t_order.order_executed_date_time),'HH24:MI:SS.MSZ')
                        ,'issued',to_char(text_to_timestamp(t_result_xray.record_date_time),'YYYY-MM-DDT') || to_char(text_to_timestamp(t_result_xray.record_date_time),'HH24:MI:SS.MSZ')
                        ,'performer',jsonb_build_array(jsonb_build_object(
                            'display',(case when (prefix_verify.f_patient_prefix_id <> '000' and prefix_verify.f_patient_prefix_id is not null) then prefix_verify.patient_prefix_description else '' end)
                                     || person_verify.person_firstname || ' ' || person_verify.person_lastname
                        ))
                        ,'imagingStudy',jsonb_build_array(jsonb_build_object(
                            'display',t_order.order_common_name
                        ))
                        ,'resultsInterpreter',jsonb_build_array(jsonb_build_object(
                            'display',(case when (prefix_execute.f_patient_prefix_id <> '000' and prefix_execute.f_patient_prefix_id is not null) then prefix_execute.patient_prefix_description else '' end)
                                     || person_execute.person_firstname || ' ' || person_execute.person_lastname
                        ))
                        ,'conclusion',t_result_xray.result_xray_description
                        ,'conclusionCode',jsonb_build_array(jsonb_build_object(
                            'coding',jsonb_build_array(jsonb_build_object(
                                'system','http://snomed.info/sct'
                                ,'code','188340000'
                                ,'display',t_order.order_common_name
                            ))
                        ))
                    )) as result_xray
                from t_result_xray
                    inner join t_order on t_result_xray.t_order_id = t_order.t_order_id
                    inner join t_visit on t_order.t_visit_id = t_visit.t_visit_id
                    left join b_employee as employee_verify on t_order.order_staff_verify = employee_verify.b_employee_id
                    left join t_person as person_verify on employee_verify.t_person_id = person_verify.t_person_id
                    left join f_patient_prefix as prefix_verify on person_verify.f_prefix_id = prefix_verify.f_patient_prefix_id
                    left join b_employee as employee_execute on t_result_xray.result_xray_staff_execute = employee_execute.b_employee_id
                    left join t_person as person_execute on employee_execute.t_person_id = person_execute.t_person_id
                    left join f_patient_prefix as prefix_execute on person_execute.f_prefix_id = prefix_execute.f_patient_prefix_id
                where t_order.f_order_status_id not in ('0','3')
                    and t_order.f_item_group_id = '3'
                    and t_visit.t_visit_id = $1
                group by t_visit.t_visit_id
        ) as xray on xray.t_visit_id = t_visit.t_visit_id 
        cross join b_site
    where t_visit.f_visit_status_id <> '4'
        and t_visit.t_visit_id = $1
    group by b_site.b_site_id
        ,t_patient.t_patient_id
        ,t_health_family.t_health_family_id
        ,f_patient_prefix.f_patient_prefix_id
		,smoking_drinking.risk_cigarette_result
		,smoking_drinking.risk_alcoho_result
        ,city_address.f_address_id
        ,district_address.f_address_id
        ,state_address.f_address_id
        ,f_patient_relation.f_patient_relation_id
        ,telecom.patient_telecom
        ,contact_tele.contact_telecom
        ,city_contact.f_address_id
        ,district_contact.f_address_id
        ,state_contact.f_address_id
        ,f_patient_nation.f_patient_nation_id
        ,f_patient_marriage_status.f_patient_marriage_status_id
        ,t_visit.t_visit_id
        ,b_report_12files_std_clinic.b_report_12files_std_clinic_id
        ,f_visit_service_type.f_visit_service_type_id
        ,f_provider_type.f_provider_type_id
        ,participant.b_employee_id
        ,participant_person.t_person_id
        ,participant_prefix.f_patient_prefix_id
        ,t_billing.billing_total
        ,t_billing.billing_payer_share
        ,t_billing.billing_patient_share
        ,plans.t_visit_id
        ,plans.identifier
        ,plans.subscriberid
        ,plans.status
        ,plans.type_coding
        ,plans.relationship_coding
        ,plans.period
        ,plans.payor
        ,plans.coverage_class
        ,plans.coverage_reimbursement
        ,plans.contract
        ,vital_sign.t_visit_id
        ,vital_sign.body_weight
        ,vital_sign.body_height
        ,vital_sign.body_temp
        ,vital_sign.bp_systolic
        ,vital_sign.bp_diastolic
        ,lab.t_visit_id
        ,lab.lab_observation
        ,diag.t_visit_id
        ,diag.encounter_condition
        ,drugs.t_visit_id
        ,drugs.encounter_med
        ,appointment.t_visit_id
        ,appointment.encounter_appointment
        ,xray.t_visit_id
        ,xray.result_xray
        ,vaccine.t_visit_id
        ,vaccine.immunization
        ,allergy.t_patient_id
        ,allergy.allergy_intolerance
        ,chronic.t_patient_id
        ,chronic.chronic_disease
) as phr
group by phr.managingorganization
    ,phr.patient_identifier
    ,phr.patient_active
    ,phr.patient_name
    ,phr.patient_telecom
    ,phr.patient_gender
    ,phr.patient_birthdate
    ,phr.patient_deceased
    ,phr.patient_nation
    ,phr.patient_address
    ,phr.patient_maritalstatus
    ,phr.patient_contact
    ,phr.encounter_managingorganization
    ,phr.encounter_identifier
    ,phr.encounter_class
    ,phr.encounter_subclass
    ,phr.encounter_division
    ,phr.encounter_type
    ,phr.encounter_priority
    ,phr.encounter_period
    ,phr.encounter_subject
    ,phr.encounter_screen_allergy
    ,phr.encounter_screen_smoking
    ,phr.encounter_screen_drinking
    ,phr.encounter_participant
    ,phr.encounter_reason
    ,phr.encounter_total
    ,phr.encounter_reimbursement
    ,phr.encounter_paid
    ,phr.encounter_coverage
    ,phr.encounter_vital_signs
    ,phr.encounter_observation
    ,phr.encounter_condition
    ,phr.encounter_medication
    ,phr.encounter_appointment
    ,phr.encounter_immunization
    ,phr.encounter_xray
    ,phr.allergy_intolerance
    ,phr.chronic_disease;
END;
$$
LANGUAGE 'plpgsql';

-- AllergyIntolerance
DROP FUNCTION IF EXISTS moph_phr_allergy(text);
CREATE OR REPLACE FUNCTION moph_phr_allergy(t_visit_id text)
RETURNS TABLE (patient_phr jsonb) AS $$
    DECLARE
    BEGIN
return query 
select jsonb_build_object(
            'managingOrganization',phr.managingorganization
            ,'Patient',jsonb_build_object(
                'identifier',COALESCE(phr.patient_identifier, '[]'::jsonb)
                ,'active',phr.patient_active
                ,'name',COALESCE(phr.patient_name, '[]'::jsonb)
                ,'telecom',COALESCE(phr.patient_telecom, '[]'::jsonb)
                ,'gender',phr.patient_gender
                ,'birthDate',phr.patient_birthdate
                ,'deceasedBoolean',phr.patient_deceased
                ,'nationality',COALESCE(phr.patient_nation, '{}'::jsonb)
                ,'address',COALESCE(phr.patient_address, '[]'::jsonb)
                ,'maritalStatus',COALESCE(phr.patient_maritalstatus, '{}'::jsonb)
                ,'contact',COALESCE(phr.patient_contact, '[]'::jsonb)
            )
            ,'AllergyIntolerance',COALESCE(phr.allergy_intolerance,'[]'::jsonb)
        )
from (select jsonb_build_object(
                'type','Organization'
                ,'identifier',jsonb_build_object(
                    'use','official'
                    ,'system','https://bps.moph.go.th/hcode/5'
                    ,'value',b_site.b_visit_office_id
                )
                ,'display',b_site.site_full_name
                ,'scope',''
				,'agent', 'HospitalOS 3.9'
            ) as managingorganization 
            ,jsonb_build_array(jsonb_build_object(
                    'use','official'
                    ,'system','https://www.dopa.go.th'
                    ,'type','CID'
                    ,'value',t_patient.patient_pid
                    ,'period',jsonb_build_object(
                        'start',to_char(text_to_timestamp(t_patient.patient_record_date_time),'YYYY-MM-DD')
                    ) 
            )
            ,jsonb_build_object(
                    'use','official'
                    ,'system','https://sil-th.org/hn'
                    ,'assigner',jsonb_build_object(
                        'use','official'
                        ,'system','https://bps.moph.go.th/hcode/5'
                        ,'value',b_site.b_visit_office_id
                        ,'display',b_site.site_full_name
                    )
                    ,'type','HN'
                    ,'value',t_patient.patient_hn
                    ,'period',jsonb_build_object(
                        'start',to_char(text_to_timestamp(t_patient.patient_record_date_time),'YYYY-MM-DD')
                    )
            )) as patient_identifier
            ,case when t_patient.patient_active = '1' then true else false end as patient_active
            ,jsonb_build_array(jsonb_build_object(
                'use','official'
                ,'text',(case when (f_patient_prefix.f_patient_prefix_id <> '000' and f_patient_prefix.f_patient_prefix_id is not null) then f_patient_prefix.patient_prefix_description else '' end)
                        || t_patient.patient_firstname || ' ' || t_patient.patient_lastname
                ,'languageCode','TH'
                ,'family',t_patient.patient_lastname
                ,'given',COALESCE(jsonb_agg(t_patient.patient_firstname) FILTER (WHERE t_patient.patient_firstname is not null), '[]'::jsonb)
                ,'prefix',COALESCE(jsonb_agg(case when (f_patient_prefix.f_patient_prefix_id <> '000' and f_patient_prefix.f_patient_prefix_id is not null) then f_patient_prefix.patient_prefix_description else '' end) 
                            FILTER (WHERE f_patient_prefix.f_patient_prefix_id <> '000' and f_patient_prefix.f_patient_prefix_id is not null), '[]'::jsonb)
                ,'suffix','[]'::jsonb
                ,'period',jsonb_build_object(
                    'start',to_char(text_to_timestamp(t_patient.patient_record_date_time),'YYYY-MM-DDT') || to_char(text_to_timestamp(t_patient.patient_record_date_time),'HH24:MI:SS.MSZ')
                )
            )
            ,jsonb_build_object(
                'use','official'
                ,'text', case when t_health_family.patient_firstname_eng <> '' 
                              then f_patient_prefix.patient_prefix_description_eng || ' ' || t_health_family.patient_firstname_eng || ' ' || t_health_family.patient_lastname_eng
                              else '' end
                ,'languageCode','EN'
                ,'family',t_health_family.patient_lastname_eng
                ,'given',COALESCE(jsonb_agg(t_health_family.patient_firstname_eng) FILTER (WHERE t_health_family.patient_firstname_eng <> ''), '[]'::jsonb)
                ,'prefix',COALESCE(jsonb_agg(case when (f_patient_prefix.f_patient_prefix_id <> '000' and f_patient_prefix.f_patient_prefix_id is not null) then f_patient_prefix.patient_prefix_description_eng else '' end) 
                            FILTER (WHERE f_patient_prefix.f_patient_prefix_id <> '000' and f_patient_prefix.f_patient_prefix_id is not null), '[]'::jsonb)
                ,'suffix','[]'::jsonb
                ,'period',jsonb_build_object(
                    'start',to_char(text_to_timestamp(t_patient.patient_record_date_time),'YYYY-MM-DDT') || to_char(text_to_timestamp(t_patient.patient_record_date_time),'HH24:MI:SS.MSZ')
                )
            )) as patient_name
            ,telecom.patient_telecom
            ,case when t_patient.f_sex_id = '1' then 'male'
                  when t_patient.f_sex_id = '2' then 'female'
                  else '' end as patient_gender
            ,case when t_patient.patient_birthday <> '' then to_char(text_to_timestamp(t_patient.patient_birthday),'YYYY-MM-DD') else '' end as patient_birthdate
            ,case when t_health_family.f_patient_discharge_status_id in ('1','2','3') then true else false end as patient_deceased
            ,jsonb_build_object(
                'coding',jsonb_build_array(jsonb_build_object(
                    'system','http://www.thcc.or.th/download/nationalitycode.xls'
                    ,'code',case when f_patient_nation.f_patient_nation_id is not null then f_patient_nation.r_rp1853_nation_id else '' end
                    ,'display',case when f_patient_nation.f_patient_nation_id is not null then f_patient_nation.patient_nation_description else '' end
                ))
                ,'text',case when f_patient_nation.f_patient_nation_id is not null then f_patient_nation.patient_nation_description else '' end
            ) as patient_nation
            ,jsonb_build_array(jsonb_build_object(
                'use','home'
                 ,'type','both'
                 ,'text','ที่อยู่'
                 ,'line',array_to_json(array_remove(ARRAY[
                    case when t_patient.patient_house <> '' then 'บ้านเลขที่ '|| t_patient.patient_house else null end
                    ,case when t_patient.patient_moo <> '' then 'หมู่ที่ '|| t_patient.patient_moo else null end
                    ,case when t_patient.patient_road <> '' then 'ถนน '|| t_patient.patient_road else null end
                    ], null))
                 ,'city',case when city_address.f_address_id is null then ''
                              when t_patient.patient_changwat = '100000' then 'แขวง' || city_address.address_description
                              when t_patient.patient_changwat <> '100000' then 'ตำบล' || city_address.address_description end
                 ,'district',case when district_address.f_address_id is null then ''
                                  when t_patient.patient_changwat = '100000' then 'เขต' || district_address.address_description
                                  when t_patient.patient_changwat <> '100000' then 'อำเภอ' || district_address.address_description end
                 ,'state',case when state_address.f_address_id is null then ''
                               when t_patient.patient_changwat = '100000' then state_address.address_description
                               when t_patient.patient_changwat <> '100000' then 'จังหวัด' || state_address.address_description end
                 ,'postalCode',case when t_patient.patient_postcode <> '' then t_patient.patient_postcode else '' end
                 ,'country',case when patient_is_other_country = '0' then 'TH' else '' end
                 ,'period',jsonb_build_object(
                    'start',to_char(text_to_timestamp(t_patient.patient_record_date_time),'YYYY-MM-DDT') || to_char(text_to_timestamp(t_patient.patient_record_date_time),'HH24:MI:SS.MSZ')
                 )
                 ,'address_code',case when t_patient.patient_tambon <> '' then t_patient.patient_tambon else '' end
            )
           ) as patient_address
           ,jsonb_build_object(
                'coding',jsonb_build_array(jsonb_build_object(
                    'system','http://terminology.hl7.org/CodeSystem/v3-MaritalStatus'
                    ,'code',case when f_patient_marriage_status.f_patient_marriage_status_id in ('1','6') then 'S'
                                 when f_patient_marriage_status.f_patient_marriage_status_id = '2' then 'M'
                                 when f_patient_marriage_status.f_patient_marriage_status_id = '3' then 'L'
                                 when f_patient_marriage_status.f_patient_marriage_status_id = '4' then 'D'
                                 when f_patient_marriage_status.f_patient_marriage_status_id = '5' then 'W'
                                 else '' end
                    ,'display',case when f_patient_marriage_status.f_patient_marriage_status_id in ('1','2','3','4','5','6')
                                    then f_patient_marriage_status.patient_marriage_status_description
                                    else '' end
                    )
                )
                ,'text',case when f_patient_marriage_status.f_patient_marriage_status_id in ('1','2','3','4','5','6') then f_patient_marriage_status.patient_marriage_status_description else '' end
            ) as patient_maritalstatus
            ,jsonb_build_array(jsonb_build_object(
                'relationship',case when t_patient.patient_contact_firstname = '' then '[]'::jsonb
                                    else jsonb_build_array(jsonb_build_object(
                    'coding',jsonb_build_array(jsonb_build_object(
                        'system','https://www.this.or.th'
                         ,'code',case when f_patient_relation.f_patient_relation_id is not null then cast(cast(f_patient_relation.f_patient_relation_id as integer) as text) else '' end
                         ,'display',case when f_patient_relation.f_patient_relation_id is not null then f_patient_relation.patient_relation_description else '' end
                    ))
                    ,'text',case when f_patient_relation.f_patient_relation_id is not null then f_patient_relation.patient_relation_description else '' end
                )) end
                ,'name',case when t_patient.patient_contact_firstname = '' then '[]'::jsonb
                             else jsonb_build_array(jsonb_build_object(
                    'use','official'
                     ,'text',case when t_patient.patient_contact_sex_id = '1' then 'นาย' when t_patient.patient_contact_sex_id = '2' then 'นางสาว' else '' end
                            || t_patient.patient_contact_firstname || ' ' || t_patient.patient_contact_lastname
                     ,'family',t_patient.patient_contact_lastname
                     ,'languageCode','TH'
                     ,'given',COALESCE(jsonb_agg(t_patient.patient_contact_firstname) FILTER (WHERE t_patient.patient_contact_firstname is not null), '[]'::jsonb)
                     ,'prefix',COALESCE(jsonb_agg(case when t_patient.patient_contact_sex_id = '1' then 'นาย'
                                    when t_patient.patient_contact_sex_id = '2' then 'นางสาว'
                                    else null end
                                    ) FILTER (WHERE t_patient.patient_contact_firstname <> ''), '[]'::jsonb)
                     ,'suffix','[]'::jsonb
                     ,'period',jsonb_build_object(
                        'start',to_char(text_to_timestamp(t_patient.patient_record_date_time),'YYYY-MM-DDT') || to_char(text_to_timestamp(t_patient.patient_record_date_time),'HH24:MI:SS.MSZ')
                        )
                )) end
                ,'telecom',case when t_patient.patient_contact_firstname = '' then '[]'::jsonb
                                else contact_tele.contact_telecom end
                ,'address',case when t_patient.patient_contact_firstname = '' then '[]'::jsonb
                                else jsonb_build_array(jsonb_build_object(
                    'use','home'
                    ,'type','both'
                    ,'text','ที่อยู่'
                    ,'line',array_to_json(array_remove(ARRAY[
                        case when t_patient.patient_contact_house <> '' then 'บ้านเลขที่ '|| t_patient.patient_contact_house else null end
                        ,case when t_patient.patient_contact_moo <> '' then 'หมู่ที่ '|| t_patient.patient_contact_moo else null end
                        ,case when t_patient.patient_contact_road <> '' then 'ถนน '|| t_patient.patient_contact_road else null end
                                ], null))
                            ,'city',case when city_contact.f_address_id is null then ''
                                         when t_patient.patient_contact_changwat = '100000' then 'แขวง' || city_contact.address_description
                                         when t_patient.patient_contact_changwat <> '100000' then 'ตำบล' || city_contact.address_description end
                            ,'district',case when district_contact.f_address_id is null then ''
                                         when t_patient.patient_contact_changwat = '100000' then 'เขต' || district_contact.address_description
                                         when t_patient.patient_contact_changwat <> '100000' then 'อำเภอ' || district_contact.address_description end
                            ,'state',case when state_contact.f_address_id is null then ''
                                         when t_patient.patient_contact_changwat = '100000' then state_contact.address_description
                                         when t_patient.patient_contact_changwat <> '100000' then 'จังหวัด' || state_contact.address_description end
                            ,'postalCode',case when t_patient.patient_contact_postcode <> '' then t_patient.patient_contact_postcode else '' end
                            ,'country','TH'
                            ,'period',jsonb_build_object(
                                    'start',to_char(text_to_timestamp(t_patient.patient_record_date_time),'YYYY-MM-DDT') || to_char(text_to_timestamp(t_patient.patient_record_date_time),'HH24:MI:SS.MSZ')
                                )
                            ,'address_code',case when t_patient.patient_contact_tambon <> '' then t_patient.patient_contact_tambon else '' end
                        )) end
                        ,'gender',case when t_patient.f_sex_id = '1' then 'male' when t_patient.f_sex_id = '2' then 'female' else '' end
            )) as patient_contact
            ,case when allergy.t_patient_id is not null 
                  then allergy.allergy_intolerance
                  else null end as allergy_intolerance

    from t_visit
        inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
        inner join t_health_family on t_patient.t_health_family_id = t_health_family.t_health_family_id
        left join f_patient_prefix on t_health_family.f_prefix_id = f_patient_prefix.f_patient_prefix_id
        left join (select t_patient.t_patient_id
                        ,max(screen_smoking.risk_cigarette_result) as risk_cigarette_result
                        ,max(screen_drinking.risk_alcoho_result) as risk_alcoho_result
                    from t_visit
                        inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
                        left join t_patient_risk_factor as screen_smoking on t_patient.t_patient_id = screen_smoking.t_patient_id
                                                                      and screen_smoking.patient_risk_factor_topic = 'สูบบุหรี่'
                        left join t_patient_risk_factor as screen_drinking on t_patient.t_patient_id = screen_drinking.t_patient_id
                                                                      and screen_drinking.patient_risk_factor_topic = 'ดื่มแอลกอฮอล์'
                    where t_visit.t_visit_id = $1
                    group by t_patient.t_patient_id
                    ) as smoking_drinking on t_patient.t_patient_id = smoking_drinking.t_patient_id             
        left join (select telecom.t_patient_id
                    ,COALESCE(jsonb_agg(telecom.patient_telecom) FILTER (WHERE telecom.t_patient_id is not null), '[]'::jsonb)as patient_telecom
                from (select telecom.t_patient_id
                        ,jsonb_build_object(
                        'system',telecom.telecom_system
                        ,'value',telecom.telecom_value
                        ,'use',telecom.telecom_use
                        ,'rank',row_number() over (order by telecom.telecom_rank)
                        ,'period',jsonb_build_object(
                            'start',to_char(telecom.telecom_period,'YYYY-MM-DDT') || to_char(telecom.telecom_period,'HH24:MI:SS.MSZ')
                            )
                        ) as patient_telecom
                    from (select t_patient.t_patient_id
                            ,'phone' as telecom_system
                            ,t_patient.patient_phone_number as telecom_value
                            ,'home' as telecom_use
                            ,1 as telecom_rank
                            ,text_to_timestamp(t_patient.patient_record_date_time) as telecom_period
                        from t_visit
                            inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
                        where t_visit.f_visit_status_id <> '4'
                            and t_patient.patient_phone_number <> ''
                            and t_patient.patient_active = '1'
                            and t_visit.t_visit_id = $1
                        group by t_patient.t_patient_id
                        union
                        select t_patient.t_patient_id
                            ,'phone' as telecom_system
                            ,t_patient.patient_patient_mobile_phone as telecom_value
                            ,'mobile' as telecom_use
                            ,2 as telecom_rank
                            ,text_to_timestamp(t_patient.patient_record_date_time) as telecom_period
                        from t_visit
                            inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
                        where t_visit.f_visit_status_id <> '4'
                            and t_patient.patient_patient_mobile_phone <> ''
                            and t_patient.patient_active = '1'
                            and t_visit.t_visit_id = $1
                        group by t_patient.t_patient_id
                        union
                        select t_patient.t_patient_id
                            ,'email' as telecom_system
                            ,t_patient.patient_patient_email as telecom_value
                            ,'work' as telecom_use
                            ,3 as telecom_rank
                            ,text_to_timestamp(t_patient.patient_record_date_time) as telecom_period
                        from t_visit
                            inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
                        where t_visit.f_visit_status_id <> '4'
                            and t_patient.patient_patient_email <> ''
                            and t_patient.patient_active = '1'
                            and t_visit.t_visit_id = $1
                        group by t_patient.t_patient_id
                        ) as telecom
                    ) as telecom
                    group by telecom.t_patient_id) as telecom on t_patient.t_patient_id = telecom.t_patient_id
        left join f_patient_nation on t_patient.f_patient_nation_id = f_patient_nation.f_patient_nation_id
        left join f_patient_marriage_status on t_patient.f_patient_marriage_status_id = f_patient_marriage_status.f_patient_marriage_status_id
        left join f_address as city_address on t_patient.patient_tambon = city_address.f_address_id
        left join f_address as district_address on t_patient.patient_amphur = district_address.f_address_id
        left join f_address as state_address on t_patient.patient_changwat = state_address.f_address_id
        left join f_patient_relation on t_patient.f_patient_relation_id = f_patient_relation.f_patient_relation_id
        left join f_address as city_contact on t_patient.patient_contact_tambon = city_contact.f_address_id
        left join f_address as district_contact on t_patient.patient_contact_amphur = district_contact.f_address_id
        left join f_address as state_contact on t_patient.patient_contact_changwat = state_contact.f_address_id
        left join (select contact_telecom.t_patient_id
                    ,COALESCE(jsonb_agg(contact_telecom.contact_telecom) FILTER (WHERE contact_telecom.t_patient_id is not null), '[]'::jsonb)as contact_telecom
                from (select contact_telecom.t_patient_id
                        ,jsonb_build_object(
                        'system',contact_telecom.telecom_system
                        ,'value',contact_telecom.telecom_value
                        ,'use',contact_telecom.telecom_use
                        ,'rank',row_number() over (order by contact_telecom.telecom_rank)
                        ,'period',jsonb_build_object(
                            'start',to_char(contact_telecom.telecom_period,'YYYY-MM-DDT') || to_char(contact_telecom.telecom_period,'HH24:MI:SS.MSZ')
                            )
                        ) as contact_telecom
                    from (select t_patient.t_patient_id
                            ,'phone' as telecom_system
                            ,t_patient.patient_contact_phone_number as telecom_value
                            ,'home' as telecom_use
                            ,1 as telecom_rank
                            ,text_to_timestamp(t_patient.patient_record_date_time) as telecom_period
                        from t_visit
                            inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
                        where t_visit.f_visit_status_id <> '4'
                            and t_patient.patient_contact_phone_number <> ''
                            and t_patient.patient_active = '1'
                            and t_visit.t_visit_id = $1
                        group by t_patient.t_patient_id
                        union
                        select t_patient.t_patient_id
                            ,'phone' as telecom_system
                            ,t_patient.patient_contact_mobile_phone as telecom_value
                            ,'mobile' as telecom_use
                            ,2 as telecom_rank
                            ,text_to_timestamp(t_patient.patient_record_date_time) as telecom_period
                        from t_visit
                            inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
                        where t_visit.f_visit_status_id <> '4'
                            and t_patient.patient_contact_mobile_phone <> ''
                            and t_patient.patient_active = '1'
                            and t_visit.t_visit_id = $1
                        group by t_patient.t_patient_id
                        union
                        select t_patient.t_patient_id
                            ,'email' as telecom_system
                            ,t_patient.patient_contact_email as telecom_value
                            ,'work' as telecom_use
                            ,3 as telecom_rank
                            ,text_to_timestamp(t_patient.patient_record_date_time) as telecom_period
                        from t_visit
                            inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
                        where t_visit.f_visit_status_id <> '4'
                            and t_patient.patient_contact_email <> ''
                            and t_patient.patient_active = '1'
                            and t_visit.t_visit_id = $1
                        group by t_patient.t_patient_id
                        ) as contact_telecom
                    ) as contact_telecom
                    group by contact_telecom.t_patient_id) as contact_tele on t_patient.t_patient_id = contact_tele.t_patient_id
    left join (select t_diag_icd10.*
                      ,row_number() OVER (partition by  t_diag_icd10.diag_icd10_vn,t_diag_icd10.f_diag_icd10_type_id order by text_to_timestamp(t_diag_icd10.diag_icd10_record_date_time) asc) as seq                                    
               from t_diag_icd10
                   inner join b_icd10 on t_diag_icd10.diag_icd10_number = b_icd10.icd10_number
                   inner join (select t_diag_icd10.diag_icd10_vn                                                             
                                      ,max(text_to_timestamp(t_diag_icd10.diag_icd10_record_date_time)) as record_date_time
                              from t_diag_icd10 
                                    inner join t_visit on t_diag_icd10.diag_icd10_vn = t_visit.t_visit_id
                              where t_diag_icd10.f_diag_icd10_type_id = '1'
                                    and t_diag_icd10.diag_icd10_active = '1'
                                    and t_visit.f_visit_status_id <> '4'
                                    and t_visit.t_visit_id = $1
                              group by t_diag_icd10.diag_icd10_vn) as max_diag_icd10 
                                    on t_diag_icd10.diag_icd10_vn = max_diag_icd10.diag_icd10_vn                                                 
                                    and text_to_timestamp(t_diag_icd10.diag_icd10_record_date_time) = max_diag_icd10.record_date_time
                                    and t_diag_icd10.f_diag_icd10_type_id = '1') as t_diag_icd10  
                on t_diag_icd10.diag_icd10_vn = t_visit.t_visit_id
                and t_diag_icd10.seq = 1
        left join b_report_12files_map_clinic on t_diag_icd10.b_visit_clinic_id = b_report_12files_map_clinic.b_visit_clinic_id
        left join b_report_12files_std_clinic on b_report_12files_map_clinic.b_report_12files_std_clinic_id = b_report_12files_std_clinic.b_report_12files_std_clinic_id
        left join b_employee as participant on t_diag_icd10.diag_icd10_staff_doctor = participant.b_employee_id
        left join t_person as participant_person on participant.t_person_id = participant_person.t_person_id
        left join f_patient_prefix as participant_prefix on participant_person.f_prefix_id = participant_prefix.f_patient_prefix_id
        left join f_provider_type on participant.f_provider_type_id = f_provider_type.f_provider_type_id
        left join f_visit_service_type on t_visit.f_visit_service_type_id = f_visit_service_type.f_visit_service_type_id
		-- allergy
        inner join (select allergy.t_patient_id
                    ,jsonb_agg(jsonb_build_object(
                        'managingOrganization',jsonb_build_object(
                            'type','Organization'
                            ,'identifier',jsonb_build_object(
                                'use','official'
                                ,'system','https://bps.moph.go.th/hcode/5'
                                ,'value',allergy.b_visit_office_id
                            )
                            ,'display',allergy.site_full_name
                        )
                        ,'identifier',jsonb_build_array(jsonb_build_object(
                            'system','http://acme.com/ids/patients/risks'
                            ,'value','49476535'
                        ))
                        ,'clinicalStatus',jsonb_build_object(
                            'coding',jsonb_build_array(jsonb_build_object(
                                'system','http://terminology.hl7.org/CodeSystem/allergyintolerance-clinical'
                                ,'code','active'
                                ,'display','Active'
                            ))
                        )
                        ,'verificationStatus',jsonb_build_object(
                            'coding',jsonb_build_array(jsonb_build_object(
                                'system','http://terminology.hl7.org/CodeSystem/allergyintolerance-verification'
                                ,'code','confirmed'
                                ,'display','Confirmed'
                            ))
                        )
                        ,'type','allergy'
                        ,'category','{medication}'::text[]
                        ,'criticality',case when allergy.f_allergy_level_id <> '1' then 'high' else 'low' end
                        ,'code',jsonb_build_object(
                            'coding',jsonb_build_array(jsonb_build_object(
                                'system','https://www.this.or.th/tmt/substance'
                                ,'code',case when allergy.b_drug_tmt_tpucode is null then '' else allergy.b_drug_tmt_tpucode end
                                ,'display',case when allergy.item_drug_standard_description is null then '' else allergy.item_drug_standard_description end
                            ))
                        )
                        ,'seriousness',jsonb_build_object(
                            'coding',jsonb_build_array(jsonb_build_object(
                                'system','https://www.code.com/seriousness'
                                ,'code',case when allergy.f_allergy_level_id is null then '' else allergy.f_allergy_level_id end
                                ,'display',case when allergy.allergy_level_description is null then '' else allergy.allergy_level_description end
                            ))
                        )
                        ,'allergyGroup',jsonb_build_object(
                            'coding',jsonb_build_array(jsonb_build_object(
                                'system','https://www.code.com/allergy_group'
                                ,'code',''
                                ,'display',''
                            ))
                        )
                        ,'allergyInformationSource',jsonb_build_object(
                            'coding',jsonb_build_array(jsonb_build_object(
                                'system','https://www.code.com/allergy_source'
                                ,'code',case when allergy.f_allergy_informant_id is null then '' else allergy.f_allergy_informant_id end
                                ,'display',case when allergy.allergy_informant_description is null then '' else allergy.allergy_informant_description end
                            ))
                        )
                        ,'allergyResult',jsonb_build_object(
                            'coding',jsonb_build_array(jsonb_build_object(
                                'system','https://www.code.com/allergy_result'
                                ,'code','1'
                                ,'display','หายเป็นปกติ'
                            ))
                        )
                        ,'naranjoResult',jsonb_build_object(
                            'coding',jsonb_build_array(jsonb_build_object(
                                'system','https://www.code.com/allergy_naranjo_result'
                                ,'code',case when allergy.f_naranjo_interpretation_id is null then '' else allergy.f_naranjo_interpretation_id end
                                ,'display',case when allergy.naranjo_interpretation_detail is null then '' else allergy.naranjo_interpretation_detail end
                            ))
                        )
                        ,'preventable',false
                        ,'preventableScore',allergy.naranjo_total
                        ,'recordedDate',allergy.record_date_time
                        ,'recordOfficer',jsonb_build_object(
                            'reference','Practitioner/' || allergy.participant_identifier
                            ,'identifier',allergy.participant_identifier
                            ,'display',allergy.participant_display
                        )
                        ,'reaction',jsonb_build_array(jsonb_build_object(
                            'manifestation',jsonb_build_array(jsonb_build_object(
                                'coding',jsonb_build_array(jsonb_build_object(
                                    'system','http://snomed.info/sct'
                                    ,'code','404684003'
                                    ,'display',drug_allergy_symtom
                                ))
                            ))
                        ))
                    )) as allergy_intolerance
                from (select t_patient_drug_allergy.t_patient_id
                        ,b_site.b_visit_office_id
                        ,b_site.site_full_name
                        ,f_allergy_level.f_allergy_level_id
                        ,f_allergy_level.allergy_level_description
                        ,max(b_map_drug_tmt.b_drug_tmt_tpucode) as b_drug_tmt_tpucode
                        ,b_item_drug_standard.item_drug_standard_description
                        ,f_allergy_informant.f_allergy_informant_id
                        ,f_allergy_informant.allergy_informant_description
                        ,f_naranjo_interpretation.f_naranjo_interpretation_id
                        ,f_naranjo_interpretation.naranjo_interpretation_detail
                        ,case when t_naranjo.naranjo_total is not null then t_naranjo.naranjo_total else 0 end as naranjo_total
                        ,to_char(text_to_timestamp(t_patient_drug_allergy.record_date_time),'YYYY-MM-DDT') || to_char(text_to_timestamp(t_patient_drug_allergy.record_date_time),'HH24:MI:SS.MSZ') as record_date_time
                        ,case when b_employee.f_employee_authentication_id = '3' and b_employee.employee_number  <> '' 
                                  and (case when t_person.t_person_id is not null
                                            then f_patient_prefix.patient_prefix_description not ilike 'ทพ.%' or f_patient_prefix.patient_prefix_description not ilike 'ทพญ.%'
                                            else b_employee.employee_firstname not ilike 'ทพ.%' or b_employee.employee_firstname not ilike 'ทพญ.%' end)
                              then  'ว'||b_employee.employee_number
                              when b_employee.f_employee_authentication_id = '3' and b_employee.employee_number  <> ''
                                  and (case when t_person.t_person_id is not null
                                            then f_patient_prefix.patient_prefix_description ilike 'ทพ.%' or f_patient_prefix.patient_prefix_description ilike 'ทพญ.%'
                                            else b_employee.employee_firstname ilike 'ทพ.%' or b_employee.employee_firstname ilike 'ทพญ.%' end)
                              then  'ท'||b_employee.employee_number
                              when b_employee.f_employee_authentication_id = '2' and b_employee.employee_number  <> ''  then  'พ'||b_employee.employee_number
                              when b_employee.f_employee_authentication_id = '6' and b_employee.employee_number  <> ''  then  'ภ'||b_employee.employee_number
                              when b_employee.f_employee_authentication_id not in ('2','3','6') and b_employee.employee_number  <> ''  then b_employee.employee_number
                              else '' end as participant_identifier
                        ,(case when (f_patient_prefix.f_patient_prefix_id <> '000' and f_patient_prefix.f_patient_prefix_id is not null) then f_patient_prefix.patient_prefix_description else '' end)
                                    || t_person.person_firstname || ' ' || t_person.person_lastname as participant_display
                        ,t_patient_drug_allergy.drug_allergy_symtom
                    from t_patient_drug_allergy
                        inner join b_item_drug_standard on t_patient_drug_allergy.b_item_drug_standard_id = b_item_drug_standard.b_item_drug_standard_id
                        inner join b_item_drug_standard_map_item on b_item_drug_standard.b_item_drug_standard_id = b_item_drug_standard_map_item.b_item_drug_standard_id
                        inner join b_item on b_item_drug_standard_map_item.b_item_id = b_item.b_item_id
                        inner join b_map_drug_tmt on b_item.b_item_id = b_map_drug_tmt.b_item_id
                        inner join t_visit on t_patient_drug_allergy.t_patient_id = t_visit.t_patient_id
                        left join t_naranjo on t_patient_drug_allergy.t_patient_drug_allergy_id = t_naranjo.t_patient_drug_allergy_id
                        left join f_allergy_level on t_patient_drug_allergy.f_allergy_level_id = f_allergy_level.f_allergy_level_id
                        left join f_naranjo_interpretation on t_patient_drug_allergy.f_naranjo_interpretation_id = f_naranjo_interpretation.f_naranjo_interpretation_id
                        left join f_allergy_informant on t_patient_drug_allergy.f_allergy_informant_id = f_allergy_informant.f_allergy_informant_id
                        left join b_employee on t_patient_drug_allergy.doctor_diag_id = b_employee.b_employee_id
                        left join t_person on b_employee.t_person_id = t_person.t_person_id
                        left join f_patient_prefix on t_person.f_prefix_id = f_patient_prefix.f_patient_prefix_id
                        cross join b_site
                    where t_patient_drug_allergy.active = '1'
                        and t_visit.t_visit_id = $1
                    group by t_patient_drug_allergy.t_patient_id
                        ,b_site.b_visit_office_id
                        ,b_site.site_full_name
                        ,f_allergy_level.f_allergy_level_id
                        ,f_allergy_informant.f_allergy_informant_id
                        ,f_naranjo_interpretation.f_naranjo_interpretation_id
                        ,b_item_drug_standard.item_drug_standard_description
                        ,t_naranjo.naranjo_total
                        ,t_patient_drug_allergy.record_date_time
                        ,b_employee.b_employee_id
                        ,t_person.t_person_id
                        ,f_patient_prefix.f_patient_prefix_id
                        ,t_patient_drug_allergy.drug_allergy_symtom
                ) as allergy
                group by allergy.t_patient_id
        ) as allergy on allergy.t_patient_id = t_patient.t_patient_id
        cross join b_site
    where t_visit.f_visit_status_id <> '4'
        and t_visit.t_visit_id = $1
    group by b_site.b_site_id
        ,t_patient.t_patient_id
        ,t_health_family.t_health_family_id
        ,f_patient_prefix.f_patient_prefix_id
		,smoking_drinking.risk_cigarette_result
		,smoking_drinking.risk_alcoho_result
        ,city_address.f_address_id
        ,district_address.f_address_id
        ,state_address.f_address_id
        ,f_patient_relation.f_patient_relation_id
        ,telecom.patient_telecom
        ,contact_tele.contact_telecom
        ,city_contact.f_address_id
        ,district_contact.f_address_id
        ,state_contact.f_address_id
        ,f_patient_nation.f_patient_nation_id
        ,f_patient_marriage_status.f_patient_marriage_status_id
        ,t_visit.t_visit_id
        ,b_report_12files_std_clinic.b_report_12files_std_clinic_id
        ,f_visit_service_type.f_visit_service_type_id
        ,f_provider_type.f_provider_type_id
        ,participant.b_employee_id
        ,participant_person.t_person_id
        ,participant_prefix.f_patient_prefix_id
        ,allergy.t_patient_id
        ,allergy.allergy_intolerance
) as phr
group by phr.managingorganization
    ,phr.patient_identifier
    ,phr.patient_active
    ,phr.patient_name
    ,phr.patient_telecom
    ,phr.patient_gender
    ,phr.patient_birthdate
    ,phr.patient_deceased
    ,phr.patient_nation
    ,phr.patient_address
    ,phr.patient_maritalstatus
    ,phr.patient_contact
    ,phr.allergy_intolerance;
END;
$$
LANGUAGE 'plpgsql';

-- Immunization
DROP FUNCTION IF EXISTS moph_phr_immunization(text);
CREATE OR REPLACE FUNCTION moph_phr_immunization(t_visit_id text)
RETURNS TABLE (patient_phr jsonb) AS $$
    DECLARE
    BEGIN
return query 
select jsonb_build_object(
            'managingOrganization',phr.managingorganization
            ,'Patient',jsonb_build_object(
                'identifier',COALESCE(phr.patient_identifier, '[]'::jsonb)
                ,'active',phr.patient_active
                ,'name',COALESCE(phr.patient_name, '[]'::jsonb)
                ,'telecom',COALESCE(phr.patient_telecom, '[]'::jsonb)
                ,'gender',phr.patient_gender
                ,'birthDate',phr.patient_birthdate
                ,'deceasedBoolean',phr.patient_deceased
                ,'nationality',COALESCE(phr.patient_nation, '{}'::jsonb)
                ,'address',COALESCE(phr.patient_address, '[]'::jsonb)
                ,'maritalStatus',COALESCE(phr.patient_maritalstatus, '{}'::jsonb)
                ,'contact',COALESCE(phr.patient_contact, '[]'::jsonb)
            )
            ,'Immunization',COALESCE(phr.encounter_immunization, '[]'::jsonb)
        )
from (select jsonb_build_object(
                'type','Organization'
                ,'identifier',jsonb_build_object(
                    'use','official'
                    ,'system','https://bps.moph.go.th/hcode/5'
                    ,'value',b_site.b_visit_office_id
                )
                ,'display',b_site.site_full_name
                ,'scope',''
				,'agent', 'HospitalOS 3.9'
            ) as managingorganization 
            ,jsonb_build_array(jsonb_build_object(
                    'use','official'
                    ,'system','https://www.dopa.go.th'
                    ,'type','CID'
                    ,'value',t_patient.patient_pid
                    ,'period',jsonb_build_object(
                        'start',to_char(text_to_timestamp(t_patient.patient_record_date_time),'YYYY-MM-DD')
                    ) 
            )
            ,jsonb_build_object(
                    'use','official'
                    ,'system','https://sil-th.org/hn'
                    ,'assigner',jsonb_build_object(
                        'use','official'
                        ,'system','https://bps.moph.go.th/hcode/5'
                        ,'value',b_site.b_visit_office_id
                        ,'display',b_site.site_full_name
                    )
                    ,'type','HN'
                    ,'value',t_patient.patient_hn
                    ,'period',jsonb_build_object(
                        'start',to_char(text_to_timestamp(t_patient.patient_record_date_time),'YYYY-MM-DD')
                    )
            )) as patient_identifier
            ,case when t_patient.patient_active = '1' then true else false end as patient_active
            ,jsonb_build_array(jsonb_build_object(
                'use','official'
                ,'text',(case when (f_patient_prefix.f_patient_prefix_id <> '000' and f_patient_prefix.f_patient_prefix_id is not null) then f_patient_prefix.patient_prefix_description else '' end)
                        || t_patient.patient_firstname || ' ' || t_patient.patient_lastname
                ,'languageCode','TH'
                ,'family',t_patient.patient_lastname
                ,'given',COALESCE(jsonb_agg(t_patient.patient_firstname) FILTER (WHERE t_patient.patient_firstname is not null), '[]'::jsonb)
                ,'prefix',COALESCE(jsonb_agg(case when (f_patient_prefix.f_patient_prefix_id <> '000' and f_patient_prefix.f_patient_prefix_id is not null) then f_patient_prefix.patient_prefix_description else '' end) 
                            FILTER (WHERE f_patient_prefix.f_patient_prefix_id <> '000' and f_patient_prefix.f_patient_prefix_id is not null), '[]'::jsonb)
                ,'suffix','[]'::jsonb
                ,'period',jsonb_build_object(
                    'start',to_char(text_to_timestamp(t_patient.patient_record_date_time),'YYYY-MM-DDT') || to_char(text_to_timestamp(t_patient.patient_record_date_time),'HH24:MI:SS.MSZ')
                )
            )
            ,jsonb_build_object(
                'use','official'
                ,'text', case when t_health_family.patient_firstname_eng <> '' 
                              then f_patient_prefix.patient_prefix_description_eng || ' ' || t_health_family.patient_firstname_eng || ' ' || t_health_family.patient_lastname_eng
                              else '' end
                ,'languageCode','EN'
                ,'family',t_health_family.patient_lastname_eng
                ,'given',COALESCE(jsonb_agg(t_health_family.patient_firstname_eng) FILTER (WHERE t_health_family.patient_firstname_eng <> ''), '[]'::jsonb)
                ,'prefix',COALESCE(jsonb_agg(case when (f_patient_prefix.f_patient_prefix_id <> '000' and f_patient_prefix.f_patient_prefix_id is not null) then f_patient_prefix.patient_prefix_description_eng else '' end) 
                            FILTER (WHERE f_patient_prefix.f_patient_prefix_id <> '000' and f_patient_prefix.f_patient_prefix_id is not null), '[]'::jsonb)
                ,'suffix','[]'::jsonb
                ,'period',jsonb_build_object(
                    'start',to_char(text_to_timestamp(t_patient.patient_record_date_time),'YYYY-MM-DDT') || to_char(text_to_timestamp(t_patient.patient_record_date_time),'HH24:MI:SS.MSZ')
                )
            )) as patient_name
            ,telecom.patient_telecom
            ,case when t_patient.f_sex_id = '1' then 'male'
                  when t_patient.f_sex_id = '2' then 'female'
                  else '' end as patient_gender
            ,case when t_patient.patient_birthday <> '' then to_char(text_to_timestamp(t_patient.patient_birthday),'YYYY-MM-DD') else '' end as patient_birthdate
            ,case when t_health_family.f_patient_discharge_status_id in ('1','2','3') then true else false end as patient_deceased
            ,jsonb_build_object(
                'coding',jsonb_build_array(jsonb_build_object(
                    'system','http://www.thcc.or.th/download/nationalitycode.xls'
                    ,'code',case when f_patient_nation.f_patient_nation_id is not null then f_patient_nation.r_rp1853_nation_id else '' end
                    ,'display',case when f_patient_nation.f_patient_nation_id is not null then f_patient_nation.patient_nation_description else '' end
                ))
                ,'text',case when f_patient_nation.f_patient_nation_id is not null then f_patient_nation.patient_nation_description else '' end
            ) as patient_nation
            ,jsonb_build_array(jsonb_build_object(
                'use','home'
                 ,'type','both'
                 ,'text','ที่อยู่'
                 ,'line',array_to_json(array_remove(ARRAY[
                    case when t_patient.patient_house <> '' then 'บ้านเลขที่ '|| t_patient.patient_house else null end
                    ,case when t_patient.patient_moo <> '' then 'หมู่ที่ '|| t_patient.patient_moo else null end
                    ,case when t_patient.patient_road <> '' then 'ถนน '|| t_patient.patient_road else null end
                    ], null))
                 ,'city',case when city_address.f_address_id is null then ''
                              when t_patient.patient_changwat = '100000' then 'แขวง' || city_address.address_description
                              when t_patient.patient_changwat <> '100000' then 'ตำบล' || city_address.address_description end
                 ,'district',case when district_address.f_address_id is null then ''
                                  when t_patient.patient_changwat = '100000' then 'เขต' || district_address.address_description
                                  when t_patient.patient_changwat <> '100000' then 'อำเภอ' || district_address.address_description end
                 ,'state',case when state_address.f_address_id is null then ''
                               when t_patient.patient_changwat = '100000' then state_address.address_description
                               when t_patient.patient_changwat <> '100000' then 'จังหวัด' || state_address.address_description end
                 ,'postalCode',case when t_patient.patient_postcode <> '' then t_patient.patient_postcode else '' end
                 ,'country',case when patient_is_other_country = '0' then 'TH' else '' end
                 ,'period',jsonb_build_object(
                    'start',to_char(text_to_timestamp(t_patient.patient_record_date_time),'YYYY-MM-DDT') || to_char(text_to_timestamp(t_patient.patient_record_date_time),'HH24:MI:SS.MSZ')
                 )
                 ,'address_code',case when t_patient.patient_tambon <> '' then t_patient.patient_tambon else '' end
            )
           ) as patient_address
           ,jsonb_build_object(
                'coding',jsonb_build_array(jsonb_build_object(
                    'system','http://terminology.hl7.org/CodeSystem/v3-MaritalStatus'
                    ,'code',case when f_patient_marriage_status.f_patient_marriage_status_id in ('1','6') then 'S'
                                 when f_patient_marriage_status.f_patient_marriage_status_id = '2' then 'M'
                                 when f_patient_marriage_status.f_patient_marriage_status_id = '3' then 'L'
                                 when f_patient_marriage_status.f_patient_marriage_status_id = '4' then 'D'
                                 when f_patient_marriage_status.f_patient_marriage_status_id = '5' then 'W'
                                 else '' end
                    ,'display',case when f_patient_marriage_status.f_patient_marriage_status_id in ('1','2','3','4','5','6')
                                    then f_patient_marriage_status.patient_marriage_status_description
                                    else '' end
                    )
                )
                ,'text',case when f_patient_marriage_status.f_patient_marriage_status_id in ('1','2','3','4','5','6') then f_patient_marriage_status.patient_marriage_status_description else '' end
            ) as patient_maritalstatus
            ,jsonb_build_array(jsonb_build_object(
                'relationship',case when t_patient.patient_contact_firstname = '' then '[]'::jsonb
                                    else jsonb_build_array(jsonb_build_object(
                    'coding',jsonb_build_array(jsonb_build_object(
                        'system','https://www.this.or.th'
                         ,'code',case when f_patient_relation.f_patient_relation_id is not null then cast(cast(f_patient_relation.f_patient_relation_id as integer) as text) else '' end
                         ,'display',case when f_patient_relation.f_patient_relation_id is not null then f_patient_relation.patient_relation_description else '' end
                    ))
                    ,'text',case when f_patient_relation.f_patient_relation_id is not null then f_patient_relation.patient_relation_description else '' end
                )) end
                ,'name',case when t_patient.patient_contact_firstname = '' then '[]'::jsonb
                             else jsonb_build_array(jsonb_build_object(
                    'use','official'
                     ,'text',case when t_patient.patient_contact_sex_id = '1' then 'นาย' when t_patient.patient_contact_sex_id = '2' then 'นางสาว' else '' end
                            || t_patient.patient_contact_firstname || ' ' || t_patient.patient_contact_lastname
                     ,'family',t_patient.patient_contact_lastname
                     ,'languageCode','TH'
                     ,'given',COALESCE(jsonb_agg(t_patient.patient_contact_firstname) FILTER (WHERE t_patient.patient_contact_firstname is not null), '[]'::jsonb)
                     ,'prefix',COALESCE(jsonb_agg(case when t_patient.patient_contact_sex_id = '1' then 'นาย'
                                    when t_patient.patient_contact_sex_id = '2' then 'นางสาว'
                                    else null end
                                    ) FILTER (WHERE t_patient.patient_contact_firstname <> ''), '[]'::jsonb)
                     ,'suffix','[]'::jsonb
                     ,'period',jsonb_build_object(
                        'start',to_char(text_to_timestamp(t_patient.patient_record_date_time),'YYYY-MM-DDT') || to_char(text_to_timestamp(t_patient.patient_record_date_time),'HH24:MI:SS.MSZ')
                        )
                )) end
                ,'telecom',case when t_patient.patient_contact_firstname = '' then '[]'::jsonb
                                else contact_tele.contact_telecom end
                ,'address',case when t_patient.patient_contact_firstname = '' then '[]'::jsonb
                                else jsonb_build_array(jsonb_build_object(
                    'use','home'
                    ,'type','both'
                    ,'text','ที่อยู่'
                    ,'line',array_to_json(array_remove(ARRAY[
                        case when t_patient.patient_contact_house <> '' then 'บ้านเลขที่ '|| t_patient.patient_contact_house else null end
                        ,case when t_patient.patient_contact_moo <> '' then 'หมู่ที่ '|| t_patient.patient_contact_moo else null end
                        ,case when t_patient.patient_contact_road <> '' then 'ถนน '|| t_patient.patient_contact_road else null end
                                ], null))
                            ,'city',case when city_contact.f_address_id is null then ''
                                         when t_patient.patient_contact_changwat = '100000' then 'แขวง' || city_contact.address_description
                                         when t_patient.patient_contact_changwat <> '100000' then 'ตำบล' || city_contact.address_description end
                            ,'district',case when district_contact.f_address_id is null then ''
                                         when t_patient.patient_contact_changwat = '100000' then 'เขต' || district_contact.address_description
                                         when t_patient.patient_contact_changwat <> '100000' then 'อำเภอ' || district_contact.address_description end
                            ,'state',case when state_contact.f_address_id is null then ''
                                         when t_patient.patient_contact_changwat = '100000' then state_contact.address_description
                                         when t_patient.patient_contact_changwat <> '100000' then 'จังหวัด' || state_contact.address_description end
                            ,'postalCode',case when t_patient.patient_contact_postcode <> '' then t_patient.patient_contact_postcode else '' end
                            ,'country','TH'
                            ,'period',jsonb_build_object(
                                    'start',to_char(text_to_timestamp(t_patient.patient_record_date_time),'YYYY-MM-DDT') || to_char(text_to_timestamp(t_patient.patient_record_date_time),'HH24:MI:SS.MSZ')
                                )
                            ,'address_code',case when t_patient.patient_contact_tambon <> '' then t_patient.patient_contact_tambon else '' end
                        )) end
                        ,'gender',case when t_patient.f_sex_id = '1' then 'male' when t_patient.f_sex_id = '2' then 'female' else '' end
            )) as patient_contact
            ,case when vaccine.t_patient_id is not null 
                  then vaccine.immunization
                  else null end as encounter_immunization
    from t_visit
        inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
        inner join t_health_family on t_patient.t_health_family_id = t_health_family.t_health_family_id
        left join f_patient_prefix on t_health_family.f_prefix_id = f_patient_prefix.f_patient_prefix_id
        left join (select t_patient.t_patient_id
                        ,max(screen_smoking.risk_cigarette_result) as risk_cigarette_result
                        ,max(screen_drinking.risk_alcoho_result) as risk_alcoho_result
                    from t_visit
                        inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
                        left join t_patient_risk_factor as screen_smoking on t_patient.t_patient_id = screen_smoking.t_patient_id
                                                                      and screen_smoking.patient_risk_factor_topic = 'สูบบุหรี่'
                        left join t_patient_risk_factor as screen_drinking on t_patient.t_patient_id = screen_drinking.t_patient_id
                                                                      and screen_drinking.patient_risk_factor_topic = 'ดื่มแอลกอฮอล์'
                    where t_visit.t_visit_id = $1
                    group by t_patient.t_patient_id
                    ) as smoking_drinking on t_patient.t_patient_id = smoking_drinking.t_patient_id                
        left join (select telecom.t_patient_id
                    ,COALESCE(jsonb_agg(telecom.patient_telecom) FILTER (WHERE telecom.t_patient_id is not null), '[]'::jsonb)as patient_telecom
                from (select telecom.t_patient_id
                        ,jsonb_build_object(
                        'system',telecom.telecom_system
                        ,'value',telecom.telecom_value
                        ,'use',telecom.telecom_use
                        ,'rank',row_number() over (order by telecom.telecom_rank)
                        ,'period',jsonb_build_object(
                            'start',to_char(telecom.telecom_period,'YYYY-MM-DDT') || to_char(telecom.telecom_period,'HH24:MI:SS.MSZ')
                            )
                        ) as patient_telecom
                    from (select t_patient.t_patient_id
                            ,'phone' as telecom_system
                            ,t_patient.patient_phone_number as telecom_value
                            ,'home' as telecom_use
                            ,1 as telecom_rank
                            ,text_to_timestamp(t_patient.patient_record_date_time) as telecom_period
                        from t_visit
                            inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
                        where t_visit.f_visit_status_id <> '4'
                            and t_patient.patient_phone_number <> ''
                            and t_patient.patient_active = '1'
                            and t_visit.t_visit_id = $1
                        group by t_patient.t_patient_id
                        union
                        select t_patient.t_patient_id
                            ,'phone' as telecom_system
                            ,t_patient.patient_patient_mobile_phone as telecom_value
                            ,'mobile' as telecom_use
                            ,2 as telecom_rank
                            ,text_to_timestamp(t_patient.patient_record_date_time) as telecom_period
                        from t_visit
                            inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
                        where t_visit.f_visit_status_id <> '4'
                            and t_patient.patient_patient_mobile_phone <> ''
                            and t_patient.patient_active = '1'
                            and t_visit.t_visit_id = $1
                        group by t_patient.t_patient_id
                        union
                        select t_patient.t_patient_id
                            ,'email' as telecom_system
                            ,t_patient.patient_patient_email as telecom_value
                            ,'work' as telecom_use
                            ,3 as telecom_rank
                            ,text_to_timestamp(t_patient.patient_record_date_time) as telecom_period
                        from t_visit
                            inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
                        where t_visit.f_visit_status_id <> '4'
                            and t_patient.patient_patient_email <> ''
                            and t_patient.patient_active = '1'
                            and t_visit.t_visit_id = $1
                        group by t_patient.t_patient_id
                        ) as telecom
                    ) as telecom
                    group by telecom.t_patient_id) as telecom on t_patient.t_patient_id = telecom.t_patient_id
        left join f_patient_nation on t_patient.f_patient_nation_id = f_patient_nation.f_patient_nation_id
        left join f_patient_marriage_status on t_patient.f_patient_marriage_status_id = f_patient_marriage_status.f_patient_marriage_status_id
        left join f_address as city_address on t_patient.patient_tambon = city_address.f_address_id
        left join f_address as district_address on t_patient.patient_amphur = district_address.f_address_id
        left join f_address as state_address on t_patient.patient_changwat = state_address.f_address_id
        left join f_patient_relation on t_patient.f_patient_relation_id = f_patient_relation.f_patient_relation_id
        left join f_address as city_contact on t_patient.patient_contact_tambon = city_contact.f_address_id
        left join f_address as district_contact on t_patient.patient_contact_amphur = district_contact.f_address_id
        left join f_address as state_contact on t_patient.patient_contact_changwat = state_contact.f_address_id
        left join (select contact_telecom.t_patient_id
                    ,COALESCE(jsonb_agg(contact_telecom.contact_telecom) FILTER (WHERE contact_telecom.t_patient_id is not null), '[]'::jsonb)as contact_telecom
                from (select contact_telecom.t_patient_id
                        ,jsonb_build_object(
                        'system',contact_telecom.telecom_system
                        ,'value',contact_telecom.telecom_value
                        ,'use',contact_telecom.telecom_use
                        ,'rank',row_number() over (order by contact_telecom.telecom_rank)
                        ,'period',jsonb_build_object(
                            'start',to_char(contact_telecom.telecom_period,'YYYY-MM-DDT') || to_char(contact_telecom.telecom_period,'HH24:MI:SS.MSZ')
                            )
                        ) as contact_telecom
                    from (select t_patient.t_patient_id
                            ,'phone' as telecom_system
                            ,t_patient.patient_contact_phone_number as telecom_value
                            ,'home' as telecom_use
                            ,1 as telecom_rank
                            ,text_to_timestamp(t_patient.patient_record_date_time) as telecom_period
                        from t_visit
                            inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
                        where t_visit.f_visit_status_id <> '4'
                            and t_patient.patient_contact_phone_number <> ''
                            and t_patient.patient_active = '1'
                            and t_visit.t_visit_id = $1
                        group by t_patient.t_patient_id
                        union
                        select t_patient.t_patient_id
                            ,'phone' as telecom_system
                            ,t_patient.patient_contact_mobile_phone as telecom_value
                            ,'mobile' as telecom_use
                            ,2 as telecom_rank
                            ,text_to_timestamp(t_patient.patient_record_date_time) as telecom_period
                        from t_visit
                            inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
                        where t_visit.f_visit_status_id <> '4'
                            and t_patient.patient_contact_mobile_phone <> ''
                            and t_patient.patient_active = '1'
                            and t_visit.t_visit_id = $1
                        group by t_patient.t_patient_id
                        union
                        select t_patient.t_patient_id
                            ,'email' as telecom_system
                            ,t_patient.patient_contact_email as telecom_value
                            ,'work' as telecom_use
                            ,3 as telecom_rank
                            ,text_to_timestamp(t_patient.patient_record_date_time) as telecom_period
                        from t_visit
                            inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
                        where t_visit.f_visit_status_id <> '4'
                            and t_patient.patient_contact_email <> ''
                            and t_patient.patient_active = '1'
                            and t_visit.t_visit_id = $1
                        group by t_patient.t_patient_id
                        ) as contact_telecom
                    ) as contact_telecom
                    group by contact_telecom.t_patient_id) as contact_tele on t_patient.t_patient_id = contact_tele.t_patient_id
		left join (select t_diag_icd10.*
                      ,row_number() OVER (partition by  t_diag_icd10.diag_icd10_vn,t_diag_icd10.f_diag_icd10_type_id order by text_to_timestamp(t_diag_icd10.diag_icd10_record_date_time) asc) as seq                                    
               from t_diag_icd10
                   inner join b_icd10 on t_diag_icd10.diag_icd10_number = b_icd10.icd10_number
                   inner join (select t_diag_icd10.diag_icd10_vn                                                             
                                      ,max(text_to_timestamp(t_diag_icd10.diag_icd10_record_date_time)) as record_date_time
                              from t_diag_icd10 
                                    inner join t_visit on t_diag_icd10.diag_icd10_vn = t_visit.t_visit_id
                              where t_diag_icd10.f_diag_icd10_type_id = '1'
                                    and t_diag_icd10.diag_icd10_active = '1'
                                    and t_visit.f_visit_status_id <> '4'
                                    and t_visit.t_visit_id = $1
                              group by t_diag_icd10.diag_icd10_vn) as max_diag_icd10 
                                    on t_diag_icd10.diag_icd10_vn = max_diag_icd10.diag_icd10_vn                                                 
                                    and text_to_timestamp(t_diag_icd10.diag_icd10_record_date_time) = max_diag_icd10.record_date_time
                                    and t_diag_icd10.f_diag_icd10_type_id = '1') as t_diag_icd10  
                on t_diag_icd10.diag_icd10_vn = t_visit.t_visit_id
                and t_diag_icd10.seq = 1
        left join b_report_12files_map_clinic on t_diag_icd10.b_visit_clinic_id = b_report_12files_map_clinic.b_visit_clinic_id
        left join b_report_12files_std_clinic on b_report_12files_map_clinic.b_report_12files_std_clinic_id = b_report_12files_std_clinic.b_report_12files_std_clinic_id
        left join b_employee as participant on t_diag_icd10.diag_icd10_staff_doctor = participant.b_employee_id
        left join t_person as participant_person on participant.t_person_id = participant_person.t_person_id
        left join f_patient_prefix as participant_prefix on participant_person.f_prefix_id = participant_prefix.f_patient_prefix_id
        left join f_provider_type on participant.f_provider_type_id = f_provider_type.f_provider_type_id
        left join f_visit_service_type on t_visit.f_visit_service_type_id = f_visit_service_type.f_visit_service_type_id
		-- vaccine
        inner join (select vaccines.t_patient_id
                ,jsonb_agg(jsonb_build_object(
					'managingOrganization',jsonb_build_object(
						'type','Organization'
						,'identifier',jsonb_build_object(
							'use','official'
							,'system','https://bps.moph.go.th/hcode/5'
							,'value',b_site.b_visit_office_id
						)
						,'display',b_site.site_full_name
					),
                    'status','completed'
                    ,'vaccineCode',jsonb_build_object(
                        'coding',jsonb_build_array(jsonb_build_object(
                            'system','https://refcode.moph.go.th/vaccine'
                            ,'code',vaccines.vaccinecode_code
							,'display',vaccines.vaccinecode_text
                        ))
                        ,'text',vaccines.vaccinecode_text
                    )
					,'encounterRefCode',vaccines.refcode
                    ,'occurrenceDateTime',to_char(vaccines.occurrencedatetime,'YYYY-MM-DDT') || to_char(vaccines.occurrencedatetime,'HH24:MI:SS.MSZ')
                    ,'primarySource',true
                    ,'location',vaccines.vaccine_location
                    ,'manufacturer',vaccines.manufacturer
                    ,'lotNumber',vaccines.vaccine_lot
                    ,'expirationDate',to_char(vaccines.expirationdate,'YYYY-MM-DD')
                    ,'site',COALESCE(case when (vaccines.act_site_code <> '0' and vaccines.act_site_code is not null)
                        then jsonb_build_object(
                            'coding',jsonb_build_array(jsonb_build_object(
                                'system','http://terminology.hl7.org/CodeSystem/v3-ActSite'
                                ,'code',vaccines.act_site_code
                                ,'display',vaccines.act_site_desc
                            ))
                        ) else null end, '{}'::jsonb)
                    ,'route',COALESCE(case when (vaccines.immunization_route_code <> '0' and vaccines.immunization_route_code is not null)
                        then jsonb_build_object(
                            'coding',jsonb_build_array(jsonb_build_object(
                                'system','http://terminology.hl7.org/CodeSystem/v3-RouteOfAdministration'
                                ,'code',vaccines.immunization_route_code
                                ,'display',vaccines.immunization_route_desc
                            ))
                        ) else null end, '{}'::jsonb)
                    ,'doseQuantity',jsonb_build_object(
                            'value',vaccines.dose_value
                            ,'system','http://unitsofmeasure.org'
                            ,'code',vaccines.dose_code
                    )
                    ,'note',jsonb_build_array(jsonb_build_object(
                        'text',vaccines.vaccine_note
                    ))
                    ,'reasonCode',jsonb_build_array(jsonb_build_object(
                        'coding',jsonb_build_array(jsonb_build_object(
                            'system','http://snomed.info/sct'
                            ,'code','429060002'
                        ))
                    ))
                    ,'performer',jsonb_build_object(
                        'license_no',vaccines.license_no
                        ,'name',vaccines.performer_name
                    )
                )) as immunization
            from (select t_patient.t_patient_id
                    ,t_health_epi.t_visit_id
                    ,t_health_epi_detail.t_health_epi_detail_id
                    ,case when position(':' in b_health_epi_group.health_epi_group_description_particular) > 0
                                        then substring(b_health_epi_group.health_epi_group_description_particular,1,(position(':' in b_health_epi_group.health_epi_group_description_particular)-1))
                                        else b_health_epi_group.health_epi_group_description_particular end as vaccinecode_code
                    ,b_health_epi_group.health_epi_group_description as vaccinecode_text
					,t_patient.patient_pid || t_health_epi_detail.ref_code as refcode
                    ,text_to_timestamp(t_health_epi_detail.record_date_time) as occurrencedatetime
                    ,b_site.site_full_name as vaccine_location
                    ,case when b_item_manufacturer.b_item_manufacturer_id is not null then b_item_manufacturer.item_manufacturer_description else '' end as manufacturer
                    ,t_health_epi_detail.health_epi_detail_lot as vaccine_lot
                    ,text_to_timestamp(t_health_epi_detail.health_epi_exp)::date as expirationdate
                    ,t_health_epi_detail.act_site_code as act_site_code
                    ,f_hl7_act_site.description as act_site_desc
                    ,t_health_epi_detail.immunization_route_code as immunization_route_code
                    ,f_hl7_immunization_route.description as immunization_route_desc
                    ,t_order_drug.order_drug_dose as dose_value
                    ,case when b_item_drug_uom.ucum_unit_code <> '0' then b_item_drug_uom.ucum_unit_code else '' end as dose_code
                    ,t_health_epi.health_epi_notice as vaccine_note
                    ,case when b_employee.f_employee_authentication_id = '3' and b_employee.employee_number  <> '' 
                               and (case when t_person.t_person_id is not null
                                         then f_patient_prefix.patient_prefix_description not ilike 'ทพ.%' or f_patient_prefix.patient_prefix_description not ilike 'ทพญ.%'
                                         else b_employee.employee_firstname not ilike 'ทพ.%' or b_employee.employee_firstname not ilike 'ทพญ.%' end)
                          then  'ว'||b_employee.employee_number
                          when b_employee.f_employee_authentication_id = '3' and b_employee.employee_number  <> ''
                               and (case when t_person.t_person_id is not null
                                         then f_patient_prefix.patient_prefix_description ilike 'ทพ.%' or f_patient_prefix.patient_prefix_description ilike 'ทพญ.%'
                                         else b_employee.employee_firstname ilike 'ทพ.%' or b_employee.employee_firstname ilike 'ทพญ.%' end)
                          then  'ท'||b_employee.employee_number
                          when b_employee.f_employee_authentication_id = '2' and b_employee.employee_number  <> ''  then  'พ'||b_employee.employee_number
                          when b_employee.f_employee_authentication_id = '6' and b_employee.employee_number  <> ''  then  'ภ'||b_employee.employee_number
                          when b_employee.f_employee_authentication_id not in ('2','3','6') and b_employee.employee_number  <> ''  then b_employee.employee_number
                          else ''  end as license_no
                    ,(case when (f_patient_prefix.f_patient_prefix_id <> '000' and f_patient_prefix.f_patient_prefix_id is not null) then f_patient_prefix.patient_prefix_description else '' end)
                     || t_person.person_firstname || ' ' || t_person.person_lastname as performer_name
                from t_visit
                    inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
                    inner join t_health_epi on t_patient.t_patient_id = t_health_epi.t_patient_id
                    inner join t_health_epi_detail on t_health_epi.t_health_epi_id = t_health_epi_detail.t_health_epi_id
                    inner join b_health_epi_group on t_health_epi_detail.b_health_epi_set_id = b_health_epi_group.b_health_epi_group_id
                    inner join b_health_epi_item on b_health_epi_group.b_health_epi_group_id = b_health_epi_item.b_health_epi_group_id
                    inner join t_order on b_health_epi_item.b_item_id = t_order.b_item_id
                                      and t_health_epi.t_visit_id = t_order.t_visit_id
                                      and t_order.f_order_status_id not in ('0','3')
                    inner join t_order_drug on t_order.t_order_id = t_order_drug.t_order_id
                                           and t_order_drug.order_drug_active = '1'
                    left join b_item_drug_uom on t_order_drug.b_item_drug_uom_id_use = b_item_drug_uom.b_item_drug_uom_id
                    left join f_hl7_vaccine_code on b_health_epi_group.hl7_vaccine_code = f_hl7_vaccine_code.code
                    left join b_item_manufacturer on t_health_epi_detail.b_item_manufacturer_id = b_item_manufacturer.b_item_manufacturer_id
                    left join f_hl7_act_site on t_health_epi_detail.act_site_code = f_hl7_act_site.code
                    left join f_hl7_immunization_route on t_health_epi_detail.immunization_route_code = f_hl7_immunization_route.code
                    left join b_employee on t_health_epi.health_epi_staff_record = b_employee.b_employee_id
                    left join t_person on b_employee.t_person_id = t_person.t_person_id
                    left join f_patient_prefix on t_person.f_prefix_id = f_patient_prefix.f_patient_prefix_id
                    cross join b_site
                where t_health_epi.health_epi_active = '1'
                    and t_visit.t_visit_id = $1
                group by t_patient.t_patient_id
                    ,t_health_epi.t_health_epi_id
					,t_patient.patient_pid
                    ,t_health_epi_detail.ref_code
                    ,t_health_epi_detail.t_health_epi_detail_id
                    ,b_health_epi_group.b_health_epi_group_id
                    ,b_site.site_full_name
                    ,b_item_manufacturer.b_item_manufacturer_id
                    ,f_hl7_act_site.id
                    ,f_hl7_immunization_route.id
                    ,t_order_drug.t_order_drug_id
                    ,b_item_drug_uom.ucum_unit_code
                    ,b_employee.b_employee_id
                    ,t_person.t_person_id
                    ,f_patient_prefix.f_patient_prefix_id
                union
                select t_patient.t_patient_id
                    ,t_health_vaccine_covid19.t_visit_id
                    ,t_health_vaccine_covid19.t_health_vaccine_covid19_id
                    ,case when position(':' in b_health_epi_group.health_epi_group_description_particular) > 0
                                        then substring(b_health_epi_group.health_epi_group_description_particular,1,(position(':' in b_health_epi_group.health_epi_group_description_particular)-1))
                                        else b_health_epi_group.health_epi_group_description_particular end as vaccinecode_code
                    ,b_health_epi_group.health_epi_group_description as vaccinecode_text
					,t_patient.patient_pid || t_health_vaccine_covid19.ref_code as refcode
                    ,(t_health_vaccine_covid19.receive_date || ',' || t_health_vaccine_covid19.receive_time)::timestamp with time zone as occurrencedatetime
                    ,b_site.site_full_name as vaccine_location
                    ,case when f_vaccine_manufacturer.f_vaccine_manufacturer_id is not null then f_vaccine_manufacturer.vaccine_manufacturer_name else '' end as manufacturer
                    ,b_health_vaccine.lot_number as vaccine_lot
                    ,b_health_vaccine.expire_date as expirationdate
                    ,t_health_vaccine_covid19.act_site_code as act_site_code
                    ,f_hl7_act_site.description as act_site_desc
                    ,t_health_vaccine_covid19.immunization_route_code as immunization_route_code
                    ,f_hl7_immunization_route.description as immunization_route_desc
                    ,t_order_drug.order_drug_dose as dose_value
                    ,case when b_item_drug_uom.ucum_unit_code <> '0' then b_item_drug_uom.ucum_unit_code else '' end as dose_code
                    ,t_health_vaccine_covid19.description as vaccine_note
                    ,case when b_employee.f_employee_authentication_id = '3' and b_employee.employee_number  <> '' 
                               and (case when t_person.t_person_id is not null
                                         then f_patient_prefix.patient_prefix_description not ilike 'ทพ.%' or f_patient_prefix.patient_prefix_description not ilike 'ทพญ.%'
                                         else b_employee.employee_firstname not ilike 'ทพ.%' or b_employee.employee_firstname not ilike 'ทพญ.%' end)
                          then  'ว'||b_employee.employee_number
                          when b_employee.f_employee_authentication_id = '3' and b_employee.employee_number  <> ''
                               and (case when t_person.t_person_id is not null
                                         then f_patient_prefix.patient_prefix_description ilike 'ทพ.%' or f_patient_prefix.patient_prefix_description ilike 'ทพญ.%'
                                         else b_employee.employee_firstname ilike 'ทพ.%' or b_employee.employee_firstname ilike 'ทพญ.%' end)
                          then  'ท'||b_employee.employee_number
                          when b_employee.f_employee_authentication_id = '2' and b_employee.employee_number  <> ''  then  'พ'||b_employee.employee_number
                          when b_employee.f_employee_authentication_id = '6' and b_employee.employee_number  <> ''  then  'ภ'||b_employee.employee_number
                          when b_employee.f_employee_authentication_id not in ('2','3','6') and b_employee.employee_number  <> ''  then b_employee.employee_number
                          else ''  end as license_no
                    ,(case when (f_patient_prefix.f_patient_prefix_id <> '000' and f_patient_prefix.f_patient_prefix_id is not null) then f_patient_prefix.patient_prefix_description else '' end)
                     || t_person.person_firstname || ' ' || t_person.person_lastname as performer_name
                from t_visit
                    inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
                    inner join t_health_vaccine_covid19 on t_patient.t_patient_id = t_health_vaccine_covid19.t_patient_id
                    inner join b_health_epi_group on t_health_vaccine_covid19.b_health_epi_group_id = b_health_epi_group.b_health_epi_group_id
                    inner join b_health_epi_item on b_health_epi_group.b_health_epi_group_id = b_health_epi_item.b_health_epi_group_id
                    inner join t_order on b_health_epi_item.b_item_id = t_order.b_item_id
                                      and t_health_vaccine_covid19.t_visit_id = t_order.t_visit_id
                                      and t_order.f_order_status_id not in ('0','3')
                    inner join t_order_drug on t_order.t_order_id = t_order_drug.t_order_id
                                           and t_order_drug.order_drug_active = '1'
                    left join b_item_drug_uom on t_order_drug.b_item_drug_uom_id_use = b_item_drug_uom.b_item_drug_uom_id
                    left join f_hl7_vaccine_code on b_health_epi_group.hl7_vaccine_code = f_hl7_vaccine_code.code
                    inner join b_health_vaccine on t_health_vaccine_covid19.b_health_vaccine_id = b_health_vaccine.b_health_vaccine_id
                    left join f_vaccine_manufacturer on b_health_vaccine.f_vaccine_manufacturer_id = f_vaccine_manufacturer.f_vaccine_manufacturer_id
                    left join f_hl7_act_site on t_health_vaccine_covid19.act_site_code = f_hl7_act_site.code
                    left join f_hl7_immunization_route on t_health_vaccine_covid19.immunization_route_code = f_hl7_immunization_route.code
                    left join b_employee on t_health_vaccine_covid19.user_procedure_id = b_employee.b_employee_id
                    left join t_person on b_employee.t_person_id = t_person.t_person_id
                    left join f_patient_prefix on t_person.f_prefix_id = f_patient_prefix.f_patient_prefix_id
                    cross join b_site
                where t_health_vaccine_covid19.active = '1'
                    and t_visit.t_visit_id = $1
                group by t_patient.t_patient_id
                    ,t_health_vaccine_covid19.t_visit_id
					,t_patient.patient_pid
                    ,t_health_vaccine_covid19.ref_code
                    ,t_health_vaccine_covid19.t_health_vaccine_covid19_id
                    ,b_health_epi_group.b_health_epi_group_id
                    ,b_site.site_full_name
                    ,f_vaccine_manufacturer.f_vaccine_manufacturer_id
                    ,b_health_vaccine.b_health_vaccine_id
                    ,f_hl7_act_site.id
                    ,f_hl7_immunization_route.id
                    ,t_order_drug.t_order_drug_id
                    ,b_item_drug_uom.ucum_unit_code
                    ,b_employee.b_employee_id
                    ,t_person.t_person_id
                    ,f_patient_prefix.f_patient_prefix_id
            ) as vaccines
			cross join b_site
            group by vaccines.t_patient_id
        ) as vaccine on t_patient.t_patient_id = vaccine.t_patient_id
        cross join b_site
    where t_visit.f_visit_status_id <> '4'
        and t_visit.t_visit_id = $1
    group by b_site.b_site_id
        ,t_patient.t_patient_id
        ,t_health_family.t_health_family_id
        ,f_patient_prefix.f_patient_prefix_id
		,smoking_drinking.risk_cigarette_result
		,smoking_drinking.risk_alcoho_result
        ,city_address.f_address_id
        ,district_address.f_address_id
        ,state_address.f_address_id
        ,f_patient_relation.f_patient_relation_id
        ,telecom.patient_telecom
        ,contact_tele.contact_telecom
        ,city_contact.f_address_id
        ,district_contact.f_address_id
        ,state_contact.f_address_id
        ,f_patient_nation.f_patient_nation_id
        ,f_patient_marriage_status.f_patient_marriage_status_id
        ,t_visit.t_visit_id
        ,b_report_12files_std_clinic.b_report_12files_std_clinic_id
        ,f_visit_service_type.f_visit_service_type_id
        ,f_provider_type.f_provider_type_id
        ,participant.b_employee_id
        ,participant_person.t_person_id
        ,participant_prefix.f_patient_prefix_id
        ,vaccine.t_patient_id
        ,vaccine.immunization
) as phr
group by phr.managingorganization
    ,phr.patient_identifier
    ,phr.patient_active
    ,phr.patient_name
    ,phr.patient_telecom
    ,phr.patient_gender
    ,phr.patient_birthdate
    ,phr.patient_deceased
    ,phr.patient_nation
    ,phr.patient_address
    ,phr.patient_maritalstatus
    ,phr.patient_contact
    ,phr.encounter_immunization;
END;
$$
LANGUAGE 'plpgsql';

-- ChronicDisease
DROP FUNCTION IF EXISTS moph_phr_chronic(text);
CREATE OR REPLACE FUNCTION moph_phr_chronic(t_visit_id text)
RETURNS TABLE (patient_phr jsonb) AS $$
    DECLARE
    BEGIN
return query 
select jsonb_build_object(
            'managingOrganization',phr.managingorganization
            ,'Patient',jsonb_build_object(
                'identifier',COALESCE(phr.patient_identifier, '[]'::jsonb)
                ,'active',phr.patient_active
                ,'name',COALESCE(phr.patient_name, '[]'::jsonb)
                ,'telecom',COALESCE(phr.patient_telecom, '[]'::jsonb)
                ,'gender',phr.patient_gender
                ,'birthDate',phr.patient_birthdate
                ,'deceasedBoolean',phr.patient_deceased
                ,'nationality',COALESCE(phr.patient_nation, '{}'::jsonb)
                ,'address',COALESCE(phr.patient_address, '[]'::jsonb)
                ,'maritalStatus',COALESCE(phr.patient_maritalstatus, '{}'::jsonb)
                ,'contact',COALESCE(phr.patient_contact, '[]'::jsonb)
            )
            ,'ChronicDiseaseRegister',COALESCE(phr.chronic_disease,'[]'::jsonb)
        )
from (select jsonb_build_object(
                'type','Organization'
                ,'identifier',jsonb_build_object(
                    'use','official'
                    ,'system','https://bps.moph.go.th/hcode/5'
                    ,'value',b_site.b_visit_office_id
                )
                ,'display',b_site.site_full_name
                ,'scope',''
				,'agent', 'HospitalOS 3.9'
            ) as managingorganization 
            ,jsonb_build_array(jsonb_build_object(
                    'use','official'
                    ,'system','https://www.dopa.go.th'
                    ,'type','CID'
                    ,'value',t_patient.patient_pid
                    ,'period',jsonb_build_object(
                        'start',to_char(text_to_timestamp(t_patient.patient_record_date_time),'YYYY-MM-DD')
                    ) 
            )
            ,jsonb_build_object(
                    'use','official'
                    ,'system','https://sil-th.org/hn'
                    ,'assigner',jsonb_build_object(
                        'use','official'
                        ,'system','https://bps.moph.go.th/hcode/5'
                        ,'value',b_site.b_visit_office_id
                        ,'display',b_site.site_full_name
                    )
                    ,'type','HN'
                    ,'value',t_patient.patient_hn
                    ,'period',jsonb_build_object(
                        'start',to_char(text_to_timestamp(t_patient.patient_record_date_time),'YYYY-MM-DD')
                    )
            )) as patient_identifier
            ,case when t_patient.patient_active = '1' then true else false end as patient_active
            ,jsonb_build_array(jsonb_build_object(
                'use','official'
                ,'text',(case when (f_patient_prefix.f_patient_prefix_id <> '000' and f_patient_prefix.f_patient_prefix_id is not null) then f_patient_prefix.patient_prefix_description else '' end)
                        || t_patient.patient_firstname || ' ' || t_patient.patient_lastname
                ,'languageCode','TH'
                ,'family',t_patient.patient_lastname
                ,'given',COALESCE(jsonb_agg(t_patient.patient_firstname) FILTER (WHERE t_patient.patient_firstname is not null), '[]'::jsonb)
                ,'prefix',COALESCE(jsonb_agg(case when (f_patient_prefix.f_patient_prefix_id <> '000' and f_patient_prefix.f_patient_prefix_id is not null) then f_patient_prefix.patient_prefix_description else '' end) 
                            FILTER (WHERE f_patient_prefix.f_patient_prefix_id <> '000' and f_patient_prefix.f_patient_prefix_id is not null), '[]'::jsonb)
                ,'suffix','[]'::jsonb
                ,'period',jsonb_build_object(
                    'start',to_char(text_to_timestamp(t_patient.patient_record_date_time),'YYYY-MM-DDT') || to_char(text_to_timestamp(t_patient.patient_record_date_time),'HH24:MI:SS.MSZ')
                )
            )
            ,jsonb_build_object(
                'use','official'
                ,'text', case when t_health_family.patient_firstname_eng <> '' 
                              then f_patient_prefix.patient_prefix_description_eng || ' ' || t_health_family.patient_firstname_eng || ' ' || t_health_family.patient_lastname_eng
                              else '' end
                ,'languageCode','EN'
                ,'family',t_health_family.patient_lastname_eng
                ,'given',COALESCE(jsonb_agg(t_health_family.patient_firstname_eng) FILTER (WHERE t_health_family.patient_firstname_eng <> ''), '[]'::jsonb)
                ,'prefix',COALESCE(jsonb_agg(case when (f_patient_prefix.f_patient_prefix_id <> '000' and f_patient_prefix.f_patient_prefix_id is not null) then f_patient_prefix.patient_prefix_description_eng else '' end) 
                            FILTER (WHERE f_patient_prefix.f_patient_prefix_id <> '000' and f_patient_prefix.f_patient_prefix_id is not null), '[]'::jsonb)
                ,'suffix','[]'::jsonb
                ,'period',jsonb_build_object(
                    'start',to_char(text_to_timestamp(t_patient.patient_record_date_time),'YYYY-MM-DDT') || to_char(text_to_timestamp(t_patient.patient_record_date_time),'HH24:MI:SS.MSZ')
                )
            )) as patient_name
            ,telecom.patient_telecom
            ,case when t_patient.f_sex_id = '1' then 'male'
                  when t_patient.f_sex_id = '2' then 'female'
                  else '' end as patient_gender
            ,case when t_patient.patient_birthday <> '' then to_char(text_to_timestamp(t_patient.patient_birthday),'YYYY-MM-DD') else '' end as patient_birthdate
            ,case when t_health_family.f_patient_discharge_status_id in ('1','2','3') then true else false end as patient_deceased
            ,jsonb_build_object(
                'coding',jsonb_build_array(jsonb_build_object(
                    'system','http://www.thcc.or.th/download/nationalitycode.xls'
                    ,'code',case when f_patient_nation.f_patient_nation_id is not null then f_patient_nation.r_rp1853_nation_id else '' end
                    ,'display',case when f_patient_nation.f_patient_nation_id is not null then f_patient_nation.patient_nation_description else '' end
                ))
                ,'text',case when f_patient_nation.f_patient_nation_id is not null then f_patient_nation.patient_nation_description else '' end
            ) as patient_nation
            ,jsonb_build_array(jsonb_build_object(
                'use','home'
                 ,'type','both'
                 ,'text','ที่อยู่'
                 ,'line',array_to_json(array_remove(ARRAY[
                    case when t_patient.patient_house <> '' then 'บ้านเลขที่ '|| t_patient.patient_house else null end
                    ,case when t_patient.patient_moo <> '' then 'หมู่ที่ '|| t_patient.patient_moo else null end
                    ,case when t_patient.patient_road <> '' then 'ถนน '|| t_patient.patient_road else null end
                    ], null))
                 ,'city',case when city_address.f_address_id is null then ''
                              when t_patient.patient_changwat = '100000' then 'แขวง' || city_address.address_description
                              when t_patient.patient_changwat <> '100000' then 'ตำบล' || city_address.address_description end
                 ,'district',case when district_address.f_address_id is null then ''
                                  when t_patient.patient_changwat = '100000' then 'เขต' || district_address.address_description
                                  when t_patient.patient_changwat <> '100000' then 'อำเภอ' || district_address.address_description end
                 ,'state',case when state_address.f_address_id is null then ''
                               when t_patient.patient_changwat = '100000' then state_address.address_description
                               when t_patient.patient_changwat <> '100000' then 'จังหวัด' || state_address.address_description end
                 ,'postalCode',case when t_patient.patient_postcode <> '' then t_patient.patient_postcode else '' end
                 ,'country',case when patient_is_other_country = '0' then 'TH' else '' end
                 ,'period',jsonb_build_object(
                    'start',to_char(text_to_timestamp(t_patient.patient_record_date_time),'YYYY-MM-DDT') || to_char(text_to_timestamp(t_patient.patient_record_date_time),'HH24:MI:SS.MSZ')
                 )
                 ,'address_code',case when t_patient.patient_tambon <> '' then t_patient.patient_tambon else '' end
            )
           ) as patient_address
           ,jsonb_build_object(
                'coding',jsonb_build_array(jsonb_build_object(
                    'system','http://terminology.hl7.org/CodeSystem/v3-MaritalStatus'
                    ,'code',case when f_patient_marriage_status.f_patient_marriage_status_id in ('1','6') then 'S'
                                 when f_patient_marriage_status.f_patient_marriage_status_id = '2' then 'M'
                                 when f_patient_marriage_status.f_patient_marriage_status_id = '3' then 'L'
                                 when f_patient_marriage_status.f_patient_marriage_status_id = '4' then 'D'
                                 when f_patient_marriage_status.f_patient_marriage_status_id = '5' then 'W'
                                 else '' end
                    ,'display',case when f_patient_marriage_status.f_patient_marriage_status_id in ('1','2','3','4','5','6')
                                    then f_patient_marriage_status.patient_marriage_status_description
                                    else '' end
                    )
                )
                ,'text',case when f_patient_marriage_status.f_patient_marriage_status_id in ('1','2','3','4','5','6') then f_patient_marriage_status.patient_marriage_status_description else '' end
            ) as patient_maritalstatus
            ,jsonb_build_array(jsonb_build_object(
                'relationship',case when t_patient.patient_contact_firstname = '' then '[]'::jsonb
                                    else jsonb_build_array(jsonb_build_object(
                    'coding',jsonb_build_array(jsonb_build_object(
                        'system','https://www.this.or.th'
                         ,'code',case when f_patient_relation.f_patient_relation_id is not null then cast(cast(f_patient_relation.f_patient_relation_id as integer) as text) else '' end
                         ,'display',case when f_patient_relation.f_patient_relation_id is not null then f_patient_relation.patient_relation_description else '' end
                    ))
                    ,'text',case when f_patient_relation.f_patient_relation_id is not null then f_patient_relation.patient_relation_description else '' end
                )) end
                ,'name',case when t_patient.patient_contact_firstname = '' then '[]'::jsonb
                             else jsonb_build_array(jsonb_build_object(
                    'use','official'
                     ,'text',case when t_patient.patient_contact_sex_id = '1' then 'นาย' when t_patient.patient_contact_sex_id = '2' then 'นางสาว' else '' end
                            || t_patient.patient_contact_firstname || ' ' || t_patient.patient_contact_lastname
                     ,'family',t_patient.patient_contact_lastname
                     ,'languageCode','TH'
                     ,'given',COALESCE(jsonb_agg(t_patient.patient_contact_firstname) FILTER (WHERE t_patient.patient_contact_firstname is not null), '[]'::jsonb)
                     ,'prefix',COALESCE(jsonb_agg(case when t_patient.patient_contact_sex_id = '1' then 'นาย'
                                    when t_patient.patient_contact_sex_id = '2' then 'นางสาว'
                                    else null end
                                    ) FILTER (WHERE t_patient.patient_contact_firstname <> ''), '[]'::jsonb)
                     ,'suffix','[]'::jsonb
                     ,'period',jsonb_build_object(
                        'start',to_char(text_to_timestamp(t_patient.patient_record_date_time),'YYYY-MM-DDT') || to_char(text_to_timestamp(t_patient.patient_record_date_time),'HH24:MI:SS.MSZ')
                        )
                )) end
                ,'telecom',case when t_patient.patient_contact_firstname = '' then '[]'::jsonb
                                else contact_tele.contact_telecom end
                ,'address',case when t_patient.patient_contact_firstname = '' then '[]'::jsonb
                                else jsonb_build_array(jsonb_build_object(
                    'use','home'
                    ,'type','both'
                    ,'text','ที่อยู่'
                    ,'line',array_to_json(array_remove(ARRAY[
                        case when t_patient.patient_contact_house <> '' then 'บ้านเลขที่ '|| t_patient.patient_contact_house else null end
                        ,case when t_patient.patient_contact_moo <> '' then 'หมู่ที่ '|| t_patient.patient_contact_moo else null end
                        ,case when t_patient.patient_contact_road <> '' then 'ถนน '|| t_patient.patient_contact_road else null end
                                ], null))
                            ,'city',case when city_contact.f_address_id is null then ''
                                         when t_patient.patient_contact_changwat = '100000' then 'แขวง' || city_contact.address_description
                                         when t_patient.patient_contact_changwat <> '100000' then 'ตำบล' || city_contact.address_description end
                            ,'district',case when district_contact.f_address_id is null then ''
                                         when t_patient.patient_contact_changwat = '100000' then 'เขต' || district_contact.address_description
                                         when t_patient.patient_contact_changwat <> '100000' then 'อำเภอ' || district_contact.address_description end
                            ,'state',case when state_contact.f_address_id is null then ''
                                         when t_patient.patient_contact_changwat = '100000' then state_contact.address_description
                                         when t_patient.patient_contact_changwat <> '100000' then 'จังหวัด' || state_contact.address_description end
                            ,'postalCode',case when t_patient.patient_contact_postcode <> '' then t_patient.patient_contact_postcode else '' end
                            ,'country','TH'
                            ,'period',jsonb_build_object(
                                    'start',to_char(text_to_timestamp(t_patient.patient_record_date_time),'YYYY-MM-DDT') || to_char(text_to_timestamp(t_patient.patient_record_date_time),'HH24:MI:SS.MSZ')
                                )
                            ,'address_code',case when t_patient.patient_contact_tambon <> '' then t_patient.patient_contact_tambon else '' end
                        )) end
                        ,'gender',case when t_patient.f_sex_id = '1' then 'male' when t_patient.f_sex_id = '2' then 'female' else '' end
            )) as patient_contact
            ,case when chronic.t_patient_id is not null 
                  then chronic.chronic_disease
                  else null end as chronic_disease
    from t_visit
        inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
        inner join t_health_family on t_patient.t_health_family_id = t_health_family.t_health_family_id
        left join f_patient_prefix on t_health_family.f_prefix_id = f_patient_prefix.f_patient_prefix_id
        left join (select t_patient.t_patient_id
                        ,max(screen_smoking.risk_cigarette_result) as risk_cigarette_result
                        ,max(screen_drinking.risk_alcoho_result) as risk_alcoho_result
                    from t_visit
                        inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
                        left join t_patient_risk_factor as screen_smoking on t_patient.t_patient_id = screen_smoking.t_patient_id
                                                                      and screen_smoking.patient_risk_factor_topic = 'สูบบุหรี่'
                        left join t_patient_risk_factor as screen_drinking on t_patient.t_patient_id = screen_drinking.t_patient_id
                                                                      and screen_drinking.patient_risk_factor_topic = 'ดื่มแอลกอฮอล์'
                    where t_visit.t_visit_id = $1
                    group by t_patient.t_patient_id
                    ) as smoking_drinking on t_patient.t_patient_id = smoking_drinking.t_patient_id                
        left join (select telecom.t_patient_id
                    ,COALESCE(jsonb_agg(telecom.patient_telecom) FILTER (WHERE telecom.t_patient_id is not null), '[]'::jsonb)as patient_telecom
                from (select telecom.t_patient_id
                        ,jsonb_build_object(
                        'system',telecom.telecom_system
                        ,'value',telecom.telecom_value
                        ,'use',telecom.telecom_use
                        ,'rank',row_number() over (order by telecom.telecom_rank)
                        ,'period',jsonb_build_object(
                            'start',to_char(telecom.telecom_period,'YYYY-MM-DDT') || to_char(telecom.telecom_period,'HH24:MI:SS.MSZ')
                            )
                        ) as patient_telecom
                    from (select t_patient.t_patient_id
                            ,'phone' as telecom_system
                            ,t_patient.patient_phone_number as telecom_value
                            ,'home' as telecom_use
                            ,1 as telecom_rank
                            ,text_to_timestamp(t_patient.patient_record_date_time) as telecom_period
                        from t_visit
                            inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
                        where t_visit.f_visit_status_id <> '4'
                            and t_patient.patient_phone_number <> ''
                            and t_patient.patient_active = '1'
                            and t_visit.t_visit_id = $1
                        group by t_patient.t_patient_id
                        union
                        select t_patient.t_patient_id
                            ,'phone' as telecom_system
                            ,t_patient.patient_patient_mobile_phone as telecom_value
                            ,'mobile' as telecom_use
                            ,2 as telecom_rank
                            ,text_to_timestamp(t_patient.patient_record_date_time) as telecom_period
                        from t_visit
                            inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
                        where t_visit.f_visit_status_id <> '4'
                            and t_patient.patient_patient_mobile_phone <> ''
                            and t_patient.patient_active = '1'
                            and t_visit.t_visit_id = $1
                        group by t_patient.t_patient_id
                        union
                        select t_patient.t_patient_id
                            ,'email' as telecom_system
                            ,t_patient.patient_patient_email as telecom_value
                            ,'work' as telecom_use
                            ,3 as telecom_rank
                            ,text_to_timestamp(t_patient.patient_record_date_time) as telecom_period
                        from t_visit
                            inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
                        where t_visit.f_visit_status_id <> '4'
                            and t_patient.patient_patient_email <> ''
                            and t_patient.patient_active = '1'
                            and t_visit.t_visit_id = $1
                        group by t_patient.t_patient_id
                        ) as telecom
                    ) as telecom
                    group by telecom.t_patient_id) as telecom on t_patient.t_patient_id = telecom.t_patient_id
        left join f_patient_nation on t_patient.f_patient_nation_id = f_patient_nation.f_patient_nation_id
        left join f_patient_marriage_status on t_patient.f_patient_marriage_status_id = f_patient_marriage_status.f_patient_marriage_status_id
        left join f_address as city_address on t_patient.patient_tambon = city_address.f_address_id
        left join f_address as district_address on t_patient.patient_amphur = district_address.f_address_id
        left join f_address as state_address on t_patient.patient_changwat = state_address.f_address_id
        left join f_patient_relation on t_patient.f_patient_relation_id = f_patient_relation.f_patient_relation_id
        left join f_address as city_contact on t_patient.patient_contact_tambon = city_contact.f_address_id
        left join f_address as district_contact on t_patient.patient_contact_amphur = district_contact.f_address_id
        left join f_address as state_contact on t_patient.patient_contact_changwat = state_contact.f_address_id
        left join (select contact_telecom.t_patient_id
                    ,COALESCE(jsonb_agg(contact_telecom.contact_telecom) FILTER (WHERE contact_telecom.t_patient_id is not null), '[]'::jsonb)as contact_telecom
                from (select contact_telecom.t_patient_id
                        ,jsonb_build_object(
                        'system',contact_telecom.telecom_system
                        ,'value',contact_telecom.telecom_value
                        ,'use',contact_telecom.telecom_use
                        ,'rank',row_number() over (order by contact_telecom.telecom_rank)
                        ,'period',jsonb_build_object(
                            'start',to_char(contact_telecom.telecom_period,'YYYY-MM-DDT') || to_char(contact_telecom.telecom_period,'HH24:MI:SS.MSZ')
                            )
                        ) as contact_telecom
                    from (select t_patient.t_patient_id
                            ,'phone' as telecom_system
                            ,t_patient.patient_contact_phone_number as telecom_value
                            ,'home' as telecom_use
                            ,1 as telecom_rank
                            ,text_to_timestamp(t_patient.patient_record_date_time) as telecom_period
                        from t_visit
                            inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
                        where t_visit.f_visit_status_id <> '4'
                            and t_patient.patient_contact_phone_number <> ''
                            and t_patient.patient_active = '1'
                            and t_visit.t_visit_id = $1
                        group by t_patient.t_patient_id
                        union
                        select t_patient.t_patient_id
                            ,'phone' as telecom_system
                            ,t_patient.patient_contact_mobile_phone as telecom_value
                            ,'mobile' as telecom_use
                            ,2 as telecom_rank
                            ,text_to_timestamp(t_patient.patient_record_date_time) as telecom_period
                        from t_visit
                            inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
                        where t_visit.f_visit_status_id <> '4'
                            and t_patient.patient_contact_mobile_phone <> ''
                            and t_patient.patient_active = '1'
                            and t_visit.t_visit_id = $1
                        group by t_patient.t_patient_id
                        union
                        select t_patient.t_patient_id
                            ,'email' as telecom_system
                            ,t_patient.patient_contact_email as telecom_value
                            ,'work' as telecom_use
                            ,3 as telecom_rank
                            ,text_to_timestamp(t_patient.patient_record_date_time) as telecom_period
                        from t_visit
                            inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
                        where t_visit.f_visit_status_id <> '4'
                            and t_patient.patient_contact_email <> ''
                            and t_patient.patient_active = '1'
                            and t_visit.t_visit_id = $1
                        group by t_patient.t_patient_id
                        ) as contact_telecom
                    ) as contact_telecom
                    group by contact_telecom.t_patient_id) as contact_tele on t_patient.t_patient_id = contact_tele.t_patient_id
    left join (select t_diag_icd10.*
                      ,row_number() OVER (partition by  t_diag_icd10.diag_icd10_vn,t_diag_icd10.f_diag_icd10_type_id order by text_to_timestamp(t_diag_icd10.diag_icd10_record_date_time) asc) as seq                                    
               from t_diag_icd10
                   inner join b_icd10 on t_diag_icd10.diag_icd10_number = b_icd10.icd10_number
                   inner join (select t_diag_icd10.diag_icd10_vn                                                             
                                      ,max(text_to_timestamp(t_diag_icd10.diag_icd10_record_date_time)) as record_date_time
                              from t_diag_icd10 
                                    inner join t_visit on t_diag_icd10.diag_icd10_vn = t_visit.t_visit_id
                              where t_diag_icd10.f_diag_icd10_type_id = '1'
                                    and t_diag_icd10.diag_icd10_active = '1'
                                    and t_visit.f_visit_status_id <> '4'
                                    and t_visit.t_visit_id = $1
                              group by t_diag_icd10.diag_icd10_vn) as max_diag_icd10 
                                    on t_diag_icd10.diag_icd10_vn = max_diag_icd10.diag_icd10_vn                                                 
                                    and text_to_timestamp(t_diag_icd10.diag_icd10_record_date_time) = max_diag_icd10.record_date_time
                                    and t_diag_icd10.f_diag_icd10_type_id = '1') as t_diag_icd10  
                on t_diag_icd10.diag_icd10_vn = t_visit.t_visit_id
                and t_diag_icd10.seq = 1
        left join b_report_12files_map_clinic on t_diag_icd10.b_visit_clinic_id = b_report_12files_map_clinic.b_visit_clinic_id
        left join b_report_12files_std_clinic on b_report_12files_map_clinic.b_report_12files_std_clinic_id = b_report_12files_std_clinic.b_report_12files_std_clinic_id
        left join b_employee as participant on t_diag_icd10.diag_icd10_staff_doctor = participant.b_employee_id
        left join t_person as participant_person on participant.t_person_id = participant_person.t_person_id
        left join f_patient_prefix as participant_prefix on participant_person.f_prefix_id = participant_prefix.f_patient_prefix_id
        left join f_provider_type on participant.f_provider_type_id = f_provider_type.f_provider_type_id
        left join f_visit_service_type on t_visit.f_visit_service_type_id = f_visit_service_type.f_visit_service_type_id
        -- chronic
        inner join (select t_chronic.t_patient_id
                ,jsonb_agg(jsonb_build_object(
                    'managingOrganization',jsonb_build_object(
                        'type','Organization'
                        ,'identifier',jsonb_build_object(
                            'use','official'
                            ,'system','https://bps.moph.go.th/hcode/5'
                            ,'value',t_chronic.b_visit_office_id
                        )
                        ,'display',t_chronic.site_full_name
                    )
                   ,'code',jsonb_build_object(
                        'coding',jsonb_build_array(jsonb_build_object(
                            'system','http://terminology.hl7.org/CodeSystem/code'
                            ,'code',case when regexp_replace(t_chronic.group_chronic_number , '[^0-9]*', '', 'g') <> ''
                                         then lpad(regexp_replace(t_chronic.group_chronic_number , '[^0-9]*', '', 'g'),3,'0')
                                         else '' end
                            ,'display',t_chronic.group_chronic_description_th
                        ))
                    )
                    ,'clinicalStatus',jsonb_build_object(
                        'coding',jsonb_build_array(jsonb_build_object(
                            'system','http://terminology.hl7.org/CodeSystem/code'
                            ,'code',case when t_chronic.f_chronic_discharge_status_id is not null
                                         then t_chronic.f_chronic_discharge_status_id
                                         else '' end
                            ,'display',case when t_chronic.f_chronic_discharge_status_id is not null
                                          then t_chronic.chronic_discharge_status_description
                                          else '' end
                        ))
                    )
                    ,'clinicalText',t_chronic.icd10_description
                    ,'registerDate',case when t_chronic.chronic_diagnosis_date is not null
                                          then to_char(t_chronic.chronic_diagnosis_date,'YYYY-MM-DD')
                                          else '' end
                    ,'dischargeDate',case when t_chronic.chronic_discharge_date is not null
                                          then to_char(t_chronic.chronic_discharge_date,'YYYY-MM-DD')
                                          else '' end
                    ,'recordedDate',to_char(t_chronic.record_date_time,'YYYY-MM-DDT') || to_char(t_chronic.record_date_time,'HH24:MI:SS.MSZ')
                    ,'description',jsonb_build_array(t_chronic.chronic_notice)
                )) as chronic_disease
            from(select t_chronic.t_patient_id
                ,b_site.b_visit_office_id
                ,b_site.site_full_name
                ,b_group_chronic.group_chronic_number
                ,b_group_chronic.group_chronic_description_th
                ,b_icd10.icd10_description
                ,f_chronic_discharge_status.f_chronic_discharge_status_id
                ,f_chronic_discharge_status.chronic_discharge_status_description
                ,min(text_to_timestamp(t_chronic.chronic_diagnosis_date)) as chronic_diagnosis_date
                ,max(text_to_timestamp(t_chronic.chronic_discharge_date)) as chronic_discharge_date
                ,max(text_to_timestamp(t_chronic.record_date_time)) as record_date_time
                ,t_chronic.chronic_notice
            from t_chronic 
                inner join t_patient on t_chronic.t_patient_id = t_patient.t_patient_id
                inner join t_visit on t_patient.t_patient_id = t_visit.t_patient_id
                inner join b_group_icd10 on t_chronic.chronic_icd10 = b_group_icd10.group_icd10_number
                inner join b_group_chronic on b_group_icd10.b_group_chronic_id = b_group_chronic.b_group_chronic_id
                inner join b_icd10 on t_chronic.chronic_icd10 = b_icd10.icd10_number
                left join f_chronic_discharge_status on t_chronic.f_chronic_discharge_status_id = f_chronic_discharge_status.f_chronic_discharge_status_id
                cross join b_site
            where t_chronic.chronic_active = '1'
                and t_visit.t_visit_id = $1
            group by t_chronic.t_patient_id
                ,b_site.b_visit_office_id
                ,b_site.site_full_name
                ,b_group_chronic.group_chronic_number
                ,b_group_chronic.group_chronic_description_th
                ,b_icd10.icd10_description
                ,f_chronic_discharge_status.f_chronic_discharge_status_id
                ,f_chronic_discharge_status.chronic_discharge_status_description
                ,t_chronic.chronic_notice
           ) as t_chronic
           group by t_chronic.t_patient_id
        ) as chronic on chronic.t_patient_id = t_patient.t_patient_id
        cross join b_site
    where t_visit.f_visit_status_id <> '4'
        and t_visit.t_visit_id = $1
    group by b_site.b_site_id
        ,t_patient.t_patient_id
        ,t_health_family.t_health_family_id
        ,f_patient_prefix.f_patient_prefix_id
		,smoking_drinking.risk_cigarette_result
		,smoking_drinking.risk_alcoho_result
        ,city_address.f_address_id
        ,district_address.f_address_id
        ,state_address.f_address_id
        ,f_patient_relation.f_patient_relation_id
        ,telecom.patient_telecom
        ,contact_tele.contact_telecom
        ,city_contact.f_address_id
        ,district_contact.f_address_id
        ,state_contact.f_address_id
        ,f_patient_nation.f_patient_nation_id
        ,f_patient_marriage_status.f_patient_marriage_status_id
        ,t_visit.t_visit_id
        ,b_report_12files_std_clinic.b_report_12files_std_clinic_id
        ,f_visit_service_type.f_visit_service_type_id
        ,f_provider_type.f_provider_type_id
        ,participant.b_employee_id
        ,participant_person.t_person_id
        ,participant_prefix.f_patient_prefix_id
        ,chronic.t_patient_id
        ,chronic.chronic_disease
) as phr
group by phr.managingorganization
    ,phr.patient_identifier
    ,phr.patient_active
    ,phr.patient_name
    ,phr.patient_telecom
    ,phr.patient_gender
    ,phr.patient_birthdate
    ,phr.patient_deceased
    ,phr.patient_nation
    ,phr.patient_address
    ,phr.patient_maritalstatus
    ,phr.patient_contact
    ,phr.chronic_disease;
END;
$$
LANGUAGE 'plpgsql';

INSERT INTO public.s_moph_version
(version_app, version_db, description, update_datetime)
VALUES('1.3.0', '1.1.0', 'Update Module for MOPH v1.3.0', current_timestamp);
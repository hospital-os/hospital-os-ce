package com.hospital_os.object;

import com.hospital_os.usecase.connection.*;

public class DxTemplate extends Persistent implements CommonInf {

    public String description = "";
    public String icd_type = "";
    public String icd_code = "";
    public String guide_after_dx = "";
    public String clinic_code = "";
    protected String use_icd10 = "";
    public String thaidescription = "";

    /**
     * @roseuid 3F658BBB036E
     */
    public DxTemplate() {
    }

    public DxTemplate(String str) {
        description = str;
    }

    @Override
    public String getCode() {
        return this.getObjectId();
    }

    @Override
    public String getName() {
        return this.description;
    }

    @Override
    public String toString() {
        return getName();
    }
}

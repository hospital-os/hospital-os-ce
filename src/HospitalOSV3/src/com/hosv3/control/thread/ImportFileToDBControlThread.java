/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.control.thread;

import com.hospital_os.objdb.DrugCode24DB;
import com.hospital_os.objdb.DrugTMTDB;
import com.hospital_os.objdb.LabTMLTDB;
import com.hospital_os.objdb.MapDrugTMTDB;
import com.hospital_os.object.DrugCode24;
import com.hospital_os.object.DrugTMT;
import com.hospital_os.object.LabTMLT;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.usecase.connection.UpdateStatus;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Somprasong
 */
public class ImportFileToDBControlThread {

    private static final Logger LOG = Logger.getLogger(ImportFileToDBControlThread.class.getName());
    protected UpdateStatus theUS;
    protected ConnectionInf theConnectionInf;

    public ImportFileToDBControlThread(ConnectionInf theConnectionInf) {
        this.theConnectionInf = theConnectionInf;
    }

    public void setUpdateStatus(UpdateStatus us) {
        this.theUS = us;
    }

    public void importMapDrug(final File file) {
        if (file != null && file.isFile()
                && (file.getName().toLowerCase().endsWith(".xls") || file.getName().toLowerCase().endsWith(".xlsx"))) {

            theUS.setStatus("��س����ѡ���� . . .", UpdateStatus.WARNING);
            Thread t = new Thread() {
                @Override
                @SuppressWarnings("deprecation")
                public void run() {
                    boolean isXls = file.getName().toLowerCase().endsWith(".xls");
                    ConnectionInf clone = theConnectionInf.getClone();
                    DrugCode24DB drugCode24DB = new DrugCode24DB(clone);
                    clone.open();
                    try {
                        clone.getConnection().setAutoCommit(false);
                        int total;
                        InputStream myxls = new FileInputStream(file);
                        Sheet sheet = null;
                        if (isXls) {
                            HSSFWorkbook wb = new HSSFWorkbook(myxls);
                            sheet = wb.getSheetAt(0);       // first sheet
                        } else {
                            XSSFWorkbook wb = new XSSFWorkbook(myxls);
                            sheet = wb.getSheetAt(0);       // first sheet
                        }
                        total = sheet.getLastRowNum();
                        for (int i = 0; i < 1; i++) {
                            Row row = isXls ? (HSSFRow) sheet.getRow(i) : (XSSFRow) sheet.getRow(i);
                            if (row.getLastCellNum() < 18) {
                                theUS.setStatus("������ç��� format", UpdateStatus.WARNING);
                                stop();
                                return;
                            }
                            /*
                             * column name 0 regno
                             column name 1 T_Code
                             column name 2 GPOcode
                             column name 3 GPOcheck
                             column name 4 comp
                             column name 5 tradename
                             column name 6 dosage_form
                             column name 7 dgdsfnm
                             column name 8 unit
                             column name 9 drugname
                             column name 10 STRENGTH
                             column name 11 manufacturer
                             column name 12 country
                             column name 13 STD_CODE
                             column name 14 UN_SPSC
                             column name 15 OK_record
                             column name 16 select
                             column name 17 version
                             column name 18 findCOMP
                             */
                            for (int j = 0; j < (int) row.getLastCellNum(); j++) {
                                Cell cell = row.getCell(j);
                                String data = "";
                                if (cell == null) {
                                    data = "";
                                } else if (cell.getCellType() == CellType.STRING) {
                                    data = cell.getStringCellValue();
                                }
                                if (j == 0) {
                                    if (!data.equalsIgnoreCase("regno")) {
                                        theUS.setStatus("������ç��� format", UpdateStatus.WARNING);
                                        stop();
                                        return;
                                    }

                                }
                                if (j == 1) {
                                    if (!data.equalsIgnoreCase("T_Code")) {
                                        theUS.setStatus("������ç��� format", UpdateStatus.WARNING);
                                        stop();
                                        return;
                                    }
                                }
                                if (j == 2) {
                                    if (!data.equalsIgnoreCase("GPOcode")) {
                                        theUS.setStatus("������ç��� format", UpdateStatus.WARNING);
                                        stop();
                                        return;
                                    }
                                }
                                if (j == 3) {
                                    if (!data.equalsIgnoreCase("GPOcheck")) {
                                        theUS.setStatus("������ç��� format", UpdateStatus.WARNING);
                                        stop();
                                        return;
                                    }
                                }
                                if (j == 4) {
                                    if (!data.equalsIgnoreCase("comp")) {
                                        theUS.setStatus("������ç��� format", UpdateStatus.WARNING);
                                        stop();
                                        return;
                                    }
                                }
                                if (j == 5) {
                                    if (!data.equalsIgnoreCase("tradename")) {
                                        theUS.setStatus("������ç��� format", UpdateStatus.WARNING);
                                        stop();
                                        return;
                                    }
                                }
                                if (j == 6) {
                                    if (!data.equalsIgnoreCase("dosage_form")) {
                                        theUS.setStatus("������ç��� format", UpdateStatus.WARNING);
                                        stop();
                                        return;
                                    }
                                }
                                if (j == 7) {
                                    if (!data.equalsIgnoreCase("dgdsfnm")) {
                                        theUS.setStatus("������ç��� format", UpdateStatus.WARNING);
                                        stop();
                                        return;
                                    }
                                }
                                if (j == 8) {
                                    if (!data.equalsIgnoreCase("unit")) {
                                        theUS.setStatus("������ç��� format", UpdateStatus.WARNING);
                                        stop();
                                        return;
                                    }
                                }
                                if (j == 9) {
                                    if (!data.equalsIgnoreCase("drugname")) {
                                        theUS.setStatus("������ç��� format", UpdateStatus.WARNING);
                                        stop();
                                        return;
                                    }
                                }
                                if (j == 10) {
                                    if (!data.equalsIgnoreCase("STRENGTH")) {
                                        theUS.setStatus("������ç��� format", UpdateStatus.WARNING);
                                        stop();
                                        return;
                                    }
                                }
                                if (j == 11) {
                                    if (!data.equalsIgnoreCase("manufacturer")) {
                                        theUS.setStatus("������ç��� format", UpdateStatus.WARNING);
                                        stop();
                                        return;
                                    }
                                }
                                if (j == 12) {
                                    if (!data.equalsIgnoreCase("country")) {
                                        theUS.setStatus("������ç��� format", UpdateStatus.WARNING);
                                        stop();
                                        return;
                                    }
                                }
                                if (j == 13) {
                                    if (!data.equalsIgnoreCase("STD_CODE")) {
                                        theUS.setStatus("������ç��� format", UpdateStatus.WARNING);
                                        stop();
                                        return;
                                    }
                                }
                                if (j == 14) {
                                    if (!data.equalsIgnoreCase("UN_SPSC")) {
                                        theUS.setStatus("������ç��� format", UpdateStatus.WARNING);
                                        stop();
                                        return;
                                    }
                                }
                                if (j == 15) {
                                    if (!data.equalsIgnoreCase("OK_record")) {
                                        theUS.setStatus("������ç��� format", UpdateStatus.WARNING);
                                        stop();
                                        return;
                                    }
                                }
                                if (j == 16) {
                                    if (!data.equalsIgnoreCase("select")) {
                                        theUS.setStatus("������ç��� format", UpdateStatus.WARNING);
                                        stop();
                                        return;
                                    }

                                }
                                if (j == 17) {
                                    if (!data.equalsIgnoreCase("version")) {
                                        theUS.setStatus("������ç��� format", UpdateStatus.WARNING);
                                        stop();
                                        return;
                                    }
                                }
                                if (j == 18) {
                                    if (!data.equalsIgnoreCase("findCOMP")) {
                                        theUS.setStatus("������ç��� format", UpdateStatus.WARNING);
                                        stop();
                                        return;
                                    }
                                }
                            }
                        }
                        for (int i = 1; i <= sheet.getLastRowNum(); i++) {
                            Row row = sheet.getRow(i);
                            DrugCode24 drugCode24 = new DrugCode24();
                            for (int j = 0; j < (int) row.getLastCellNum(); j++) {
                                Cell cell = row.getCell(j);
                                String data = "";
                                if (cell == null) {
                                    data = "";
                                } else if (cell.getCellType() == CellType.STRING) {
                                    data = cell.getStringCellValue();
                                } else if (cell.getCellType() == CellType.NUMERIC) {
                                    DecimalFormat df = new DecimalFormat("########################");
                                    data = df.format(cell.getNumericCellValue());
                                }
//                            else {
//                                if(i==9 || j==0 || j==5 || j==4 || j==13)
//                                JOptionPane.showMessageDialog(theUS.getJFrame(),
//                                        "�ջ������ͧ�����źҧ��Ŵ���������� Text \n" +
//                                        "��سҵ�駤�һ����������ŷء��Ŵ��� Text ��͹\n" +
//                                        "���Ǩ֧����ö������������ҵðҹ���Ѻ������������",
//                                        "�������ö����Ң����ŵ����", JOptionPane.ERROR_MESSAGE);
//                                theUS.setStatus("����ö�������������� 24 ��ѡ���Ѻ���Ѻ��������к����ǼԴ��Ҵ", theUS.ERROR);
//                                stop();
//                                return;
//                            }

                                if (j == 9) {
                                    drugCode24.itemname = data;
//                                drugCode24.itemname = drugCode24.itemname.replace("\'", "\\'");
//                                nd.std_id = data;
                                } else if (j == 0) {
                                    drugCode24.regno = data;
//                                drugCode24.regno = drugCode24.regno.replace("\'", "\\'");
//                                nd.drugcode40 = data;
                                } else if (j == 5) {
                                    drugCode24.tradename = data;
//                                drugCode24.tradename = drugCode24.tradename.replace("\'", "\\'");
//                                nd.regno = data;
                                } else if (j == 4) {
                                    drugCode24.company = data;
//                                drugCode24.company = drugCode24.company.replace("\'", "\\'");
//                                nd.regno = data;
                                } else if (j == 13) {
                                    drugCode24.drugcode24 = data;
//                                drugCode24.drugcode24 = drugCode24.drugcode24.replace("\'", "\\'");
//                                nd.regno = data;
                                }
                            }
                            DrugCode24 selectByCode24 = drugCode24DB.selectByCode24(drugCode24.drugcode24);
                            if (selectByCode24 == null) {
                                drugCode24DB.insert(drugCode24);
                            } else {
                                selectByCode24.itemname = drugCode24.itemname;
                                selectByCode24.regno = drugCode24.regno;
                                selectByCode24.tradename = drugCode24.tradename;
                                selectByCode24.company = drugCode24.company;
                                drugCode24DB.update(selectByCode24);
                            }
                            theUS.setStatus("��Ѻ��ا����¹�� " + i + "/" + total, UpdateStatus.WARNING);
                            LOG.log(Level.INFO, "��Ѻ��ا����¹�� {0}/{1}", new Object[]{i, total});
                        }
                        clone.getConnection().commit();
                        theUS.setStatus("����ö�������������� 24 ��ѡ���Ѻ���Ѻ��������к������������", UpdateStatus.COMPLETE);
                    } catch (Exception ex) {
                        theUS.setStatus("��Ѻ��ا����¹�ҼԴ��Ҵ", UpdateStatus.ERROR);
                        LOG.log(Level.SEVERE, ex.getMessage(), ex);
                        try {
                            clone.getConnection().rollback();
                        } catch (SQLException ex1) {
                            LOG.log(Level.SEVERE, null, ex1);
                        }
                    } finally {
                        try {
                            clone.getConnection().close();
                        } catch (SQLException ex) {
                            LOG.log(Level.SEVERE, null, ex);
                        }
                    }
                    stop();
                }
            };
            t.start();
        } else {
            theUS.setStatus("������������١��ͧ (�ͧ�Ѻ��� *.xls ���� *.xlsx ��ҹ��)", UpdateStatus.WARNING);
        }
    }

    public void importMapDrugTMT(final File file, final String userId) {
        if (file != null && file.isFile()
                && (file.getName().toLowerCase().endsWith(".xls") || file.getName().toLowerCase().endsWith(".xlsx"))) {

            theUS.setStatus("��س����ѡ���� . . .", UpdateStatus.WARNING);
            Thread t = new Thread() {
                @Override
                @SuppressWarnings("deprecation")
                public void run() {
                    boolean isXls = file.getName().toLowerCase().endsWith(".xls");
                    ConnectionInf clone = theConnectionInf.getClone();
                    DrugTMTDB drugTMTDB = new DrugTMTDB(clone);
                    MapDrugTMTDB mapDrugTMTDB = new MapDrugTMTDB(clone);
                    clone.open();
                    try {
                        clone.getConnection().setAutoCommit(false);
                        // check file version
                        String sql = "select  *\n"
                                + "from t_log_import_drug_tmt\n"
                                + "where\n"
                                + "substr(drug_tmt_version,6)::date >= ('" + file.getName().substring(5, 13) + "'::date)\n"
                                + "order by record_date_time desc limit 1";
                        List<Object[]> versionChecks = theConnectionInf.eComplexQuery(sql);
                        if (versionChecks.isEmpty()) {
                            sql = "INSERT INTO t_log_import_drug_tmt(\n"
                                    + " file_import, drug_tmt_version, user_record_id)\n"
                                    + "    VALUES (?, ?, ?)";
                            PreparedStatement ePQuery = theConnectionInf.ePQuery(sql);
                            ePQuery.setString(1, file.getName());
                            ePQuery.setString(2, file.getName().substring(0, 13));
                            ePQuery.setString(3, userId);
                            ePQuery.executeUpdate();
                            StringBuilder msg = new StringBuilder("���������������ҵðҹ TMT �����");
                            InputStream myxls = new FileInputStream(file);
                            Sheet sheet = null;
                            Workbook workbook = isXls ? new HSSFWorkbook(myxls) : new XSSFWorkbook(myxls);
                            for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
                                sheet = workbook.getSheetAt(i);
                                LOG.log(Level.INFO, "Start process sheet name : {0}", sheet.getSheetName());
                                if (sheet.getSheetName().toLowerCase().contains("add")) {
                                    int[] cols = new int[]{0, 1, 2, 3};
                                    List<Object[]> datas = readDataBySelectedCols(sheet, cols);
                                    int success = 0;
                                    int fail = 0;
                                    int total = datas.size();
                                    for (int j = 0; j < datas.size(); j++) {
                                        DrugTMT drugTMT = new DrugTMT();
                                        drugTMT.setObjectId(String.valueOf(
                                                datas.get(j)[0] instanceof Double
                                                ? ((Double) datas.get(j)[0]).intValue()
                                                : datas.get(j)[0]));
                                        drugTMT.fsn = String.valueOf(datas.get(j)[1]);
                                        drugTMT.manufacturer = String.valueOf(datas.get(j)[2]);
                                        drugTMT.changedate = String.valueOf(
                                                datas.get(j)[3] instanceof Double
                                                ? ((Double) datas.get(j)[3]).intValue()
                                                : datas.get(j)[3]);
                                        try {
                                            theUS.setStatus("����������� (Add) " + j + "/" + total, UpdateStatus.WARNING);
                                            success += drugTMTDB.insert(drugTMT);
                                        } catch (Exception ex) {
                                            fail++;
                                            LOG.log(Level.INFO, "����������� (Add) �������� {0} (TMTID : {1})", new Object[]{fail, drugTMT.getObjectId()});
                                        }
                                    }
                                    String log = String.format("����������� (Add) ����� %d �ҡ������ %d", success, total);
                                    msg.append("\n").append(log);
                                    LOG.log(Level.INFO, log);
                                } else if (sheet.getSheetName().toLowerCase().contains("rectify")) {
                                    int[] cols = new int[]{0, 1, 2, 3};
                                    List<Object[]> datas = readDataBySelectedCols(sheet, cols);
                                    int success = 0;
                                    int fail = 0;
                                    int total = datas.size();
                                    for (int j = 0; j < datas.size(); j++) {
                                        DrugTMT drugTMT = new DrugTMT();
                                        drugTMT.setObjectId(String.valueOf(
                                                datas.get(j)[0] instanceof Double
                                                ? ((Double) datas.get(j)[0]).intValue()
                                                : datas.get(j)[0]));
                                        drugTMT.fsn = String.valueOf(datas.get(j)[1]);
                                        drugTMT.manufacturer = String.valueOf(datas.get(j)[2]);
                                        drugTMT.changedate = String.valueOf(
                                                datas.get(j)[3] instanceof Double
                                                ? ((Double) datas.get(j)[3]).intValue()
                                                : datas.get(j)[3]);
                                        try {
                                            theUS.setStatus("����Թ������ (Rectify) " + j + "/" + total, UpdateStatus.WARNING);
                                            success += drugTMTDB.update(drugTMT);
                                        } catch (Exception ex) {
                                            fail++;
                                            LOG.log(Level.INFO, "����Թ������ (Rectify) �������� {0} (TMTID : {1})", new Object[]{fail, drugTMT.getObjectId()});
                                        }
                                    }
                                    String log = String.format("����Թ������ (Rectify) ����� %d �ҡ������ %d", success, total);
                                    msg.append("\n").append(log);
                                    LOG.log(Level.INFO, log);
                                } else if (sheet.getSheetName().toLowerCase().contains("deprecate")) {
                                    int[] cols = new int[]{0, 2};
                                    List<Object[]> datas = readDataBySelectedCols(sheet, cols);
                                    int success = 0;
                                    int fail = 0;
                                    int total = datas.size();
                                    for (int j = 0; j < datas.size(); j++) {
                                        String tmtId = String.valueOf(
                                                datas.get(j)[0] instanceof Double
                                                ? ((Double) datas.get(j)[0]).intValue()
                                                : datas.get(j)[0]);
                                        String tmtIdChange = String.valueOf(
                                                datas.get(j)[1] instanceof Double
                                                ? ((Double) datas.get(j)[1]).intValue()
                                                : datas.get(j)[1]);
                                        try {
                                            theUS.setStatus("����Թ���᷹�������������� (Deprecate) " + j + "/" + total, UpdateStatus.WARNING);
                                            success += mapDrugTMTDB.updateChangeTPUCode(tmtId, tmtIdChange);
                                        } catch (Exception ex) {
                                            fail++;
                                            LOG.log(Level.INFO, "����Թ���᷹�������������� (Deprecate) �������� {0} (TMTID : {1})", new Object[]{fail, tmtId});
                                        }
                                    }
                                    String log = String.format("����Թ���᷹�������������� (Deprecate) ����� %d �ҡ������ %d", success, total);
                                    msg.append("\n").append(log);
                                    LOG.log(Level.INFO, log);
                                } else if (sheet.getSheetName().toLowerCase().contains("reject")) {
                                    int[] cols = new int[]{0};
                                    List<Object[]> datas = readDataBySelectedCols(sheet, cols);
                                    int success = 0;
                                    int fail = 0;
                                    int total = datas.size();
                                    for (int j = 0; j < datas.size(); j++) {
                                        String tmtId = String.valueOf(
                                                datas.get(j)[0] instanceof Double
                                                ? ((Double) datas.get(j)[0]).intValue()
                                                : datas.get(j)[0]);
                                        try {
                                            theUS.setStatus("����Թ���¡��ԡ (Reject) " + j + "/" + total, UpdateStatus.WARNING);
                                            success += drugTMTDB.delete(tmtId);
                                        } catch (Exception ex) {
                                            fail++;
                                            LOG.log(Level.INFO, "����Թ���¡��ԡ (Reject) �������� {0} (TMTID : {1})", new Object[]{fail, tmtId});
                                        }
                                    }
                                    String log = String.format("����Թ���¡��ԡ (Reject) ����� %d �ҡ������ %d", success, total);
                                    msg.append("\n").append(log);
                                    LOG.log(Level.INFO, log);
                                }
                                LOG.log(Level.INFO, "Finish process sheet name : {0}", sheet.getSheetName());
                            }
                            theUS.setStatus("���������������ҵðҹ TMT �����", UpdateStatus.COMPLETE);
                            JOptionPane.showMessageDialog(null, msg.toString());
                        } else {
                            theUS.setStatus(" �������ö������� ���ͧ�ҡ��������͡�����ѹ��ӡ���������ҡѺ ��������������ش (" + String.valueOf(versionChecks.get(0)[1]) + ")", UpdateStatus.WARNING);
                        }
                        clone.getConnection().commit();
                    } catch (Exception ex) {
                        theUS.setStatus("���������������ҵðҹ TMT �Դ��Ҵ", UpdateStatus.ERROR);
                        LOG.log(Level.SEVERE, ex.getMessage(), ex);
                        try {
                            clone.getConnection().rollback();
                        } catch (SQLException ex1) {
                            LOG.log(Level.SEVERE, null, ex1);
                        }
                    } finally {
                        try {
                            clone.getConnection().close();
                        } catch (SQLException ex) {
                            LOG.log(Level.SEVERE, null, ex);
                        }
                    }
                    interrupt();
                }
            };
            t.start();
        } else {
            theUS.setStatus("������������١��ͧ (�ͧ�Ѻ��� *.xls ���� *.xlsx ��ҹ��)", UpdateStatus.WARNING);
        }
    }

    private List<Object[]> readDataBySelectedCols(Sheet sheet, int[] cols) {
        List<Object[]> list = new ArrayList<Object[]>();
        try {
            for (int i = 1; i <= sheet.getLastRowNum(); i++) {
                Row row = sheet.getRow(i);
                Object[] objects = new Object[cols.length];
                for (int j = 0; j < cols.length; j++) {
                    Cell cell = row.getCell(cols[j]);
                    objects[j] = cell == null ? "" : (cell.getCellType() == CellType.NUMERIC
                            ? cell.getNumericCellValue()
                            : cell.getStringCellValue());
                }
                list.add(objects);
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Can not read sheet " + sheet.getSheetName() + " : " + ex.getMessage(), ex);
        }
        return list;
    }

    public void importMapLabTMLT(final File file, final String userId) {
        if (file != null && file.isFile()
                && (file.getName().toLowerCase().endsWith(".xls") || file.getName().toLowerCase().endsWith(".xlsx"))) {

            theUS.setStatus("��س����ѡ���� . . .", UpdateStatus.WARNING);
            Thread t = new Thread() {
                @Override
                @SuppressWarnings("deprecation")
                public void run() {
                    boolean isXls = file.getName().toLowerCase().endsWith(".xls");
                    ConnectionInf clone = theConnectionInf.getClone();
                    LabTMLTDB labTMLTDB = new LabTMLTDB(clone);
                    clone.open();
                    try {
                        clone.getConnection().setAutoCommit(false);
                        // check file version
                        String sql = "select *\n"
                                + "from t_log_import_lab_tmlt\n"
                                + "where \n"
                                + "substr(lab_tmlt_version,7)::date >= ('" + file.getName().substring(6, 14) + "'::date)\n"
                                + "order by record_date_time desc limit 1";
                        List<Object[]> versionChecks = theConnectionInf.eComplexQuery(sql);
                        if (versionChecks.isEmpty()) {
                            sql = "INSERT INTO t_log_import_lab_tmlt("
                                    + "        file_import, lab_tmlt_version, user_record_id) \n"
                                    + " VALUES(?, ?, ?)";
                            PreparedStatement ePQuery = theConnectionInf.ePQuery(sql);
                            ePQuery.setString(1, file.getName());
                            ePQuery.setString(2, file.getName().substring(0, 14));
                            ePQuery.setString(3, userId);
                            ePQuery.executeUpdate();
                            StringBuilder msg = new StringBuilder("���������������ҵðҹ TMLT �����");
                            InputStream myxls = new FileInputStream(file);
                            Sheet sheet = null;
                            Workbook workbook = isXls ? new HSSFWorkbook(myxls) : new XSSFWorkbook(myxls);
                            for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
                                sheet = workbook.getSheetAt(i);
                                LOG.log(Level.INFO, "Start process sheet name : {0}", sheet.getSheetName());
                                if (sheet.getSheetName().toLowerCase().contains("add")) {
                                    int[] cols = new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
                                    List<Object[]> datas = readDataBySelectedCols(sheet, cols);
                                    int success = 0;
                                    int fail = 0;
                                    int total = datas.size();
                                    for (int j = 0; j < datas.size(); j++) {
                                        LabTMLT labTMLT = new LabTMLT();
                                        labTMLT.setObjectId(String.valueOf(
                                                datas.get(j)[0] instanceof Double
                                                ? ((Double) datas.get(j)[0]).intValue()
                                                : datas.get(j)[0]));
                                        labTMLT.tmlt_name = String.valueOf(datas.get(j)[1]);
                                        labTMLT.component = String.valueOf(datas.get(j)[2]);
                                        labTMLT.scale = String.valueOf(datas.get(j)[3]);
                                        labTMLT.unit = String.valueOf(datas.get(j)[4]);
                                        labTMLT.specimen = String.valueOf(datas.get(j)[5]);
                                        labTMLT.method = String.valueOf(datas.get(j)[6]);
                                        labTMLT.order_type = String.valueOf(datas.get(j)[7]);
                                        labTMLT.loinc = String.valueOf(datas.get(j)[8]);
                                        labTMLT.status = String.valueOf(datas.get(j)[9]);
                                        labTMLT.first_release_date = (String.valueOf(
                                                datas.get(j)[10] instanceof Double
                                                ? ((Double) datas.get(j)[10]).intValue()
                                                : datas.get(j)[10]));
                                        labTMLT.last_release_date = (String.valueOf(
                                                datas.get(j)[11] instanceof Double
                                                ? ((Double) datas.get(j)[11]).intValue()
                                                : datas.get(j)[11]));
                                        labTMLT.cscode = String.valueOf(datas.get(j)[12]);
                                        labTMLT.cs_name = String.valueOf(datas.get(j)[13]);
                                        labTMLT.cs_price = Double.valueOf(String.valueOf(datas.get(j)[14]));
                                        try {
                                            theUS.setStatus("����������� (Add) " + j + "/" + total, UpdateStatus.WARNING);
                                            success += labTMLTDB.insert(labTMLT);
                                        } catch (Exception ex) {
                                            fail++;
                                            LOG.log(Level.INFO, "����������� (Add) �������� {0} (TMTID : {1})", new Object[]{fail, labTMLT.getObjectId()});
                                        }
                                    }
                                    String log = String.format("����������� (Add) ����� %d �ҡ������ %d", success, total);
                                    msg.append("\n").append(log);
                                    LOG.log(Level.INFO, log);
                                } else if (sheet.getSheetName().toLowerCase().contains("rectify")) {
                                    int[] cols = new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
                                    List<Object[]> datas = readDataBySelectedCols(sheet, cols);
                                    int success = 0;
                                    int fail = 0;
                                    int total = datas.size();
                                    for (int j = 0; j < datas.size(); j++) {
                                        LabTMLT labTMLT = new LabTMLT();
                                        labTMLT.setObjectId(String.valueOf(
                                                datas.get(j)[0] instanceof Double
                                                ? ((Double) datas.get(j)[0]).intValue()
                                                : datas.get(j)[0]));
                                        labTMLT.tmlt_name = String.valueOf(datas.get(j)[1]);
                                        labTMLT.component = String.valueOf(datas.get(j)[2]);
                                        labTMLT.scale = String.valueOf(datas.get(j)[3]);
                                        labTMLT.unit = String.valueOf(datas.get(j)[4]);
                                        labTMLT.specimen = String.valueOf(datas.get(j)[5]);
                                        labTMLT.method = String.valueOf(datas.get(j)[6]);
                                        labTMLT.order_type = String.valueOf(datas.get(j)[7]);
                                        labTMLT.loinc = String.valueOf(datas.get(j)[8]);
                                        labTMLT.status = String.valueOf(datas.get(j)[9]);
                                        labTMLT.first_release_date = (String.valueOf(
                                                datas.get(j)[10] instanceof Double
                                                ? ((Double) datas.get(j)[10]).intValue()
                                                : datas.get(j)[10]));
                                        labTMLT.last_release_date = (String.valueOf(
                                                datas.get(j)[11] instanceof Double
                                                ? ((Double) datas.get(j)[11]).intValue()
                                                : datas.get(j)[11]));
                                        labTMLT.cscode = String.valueOf(datas.get(j)[12]);
                                        labTMLT.cs_name = String.valueOf(datas.get(j)[13]);
                                        labTMLT.cs_price = Double.valueOf(String.valueOf(datas.get(j)[14]));
                                        try {
                                            theUS.setStatus("����Թ������ (Rectify) " + j + "/" + total, UpdateStatus.WARNING);
                                            success += labTMLTDB.update(labTMLT);
                                        } catch (Exception ex) {
                                            fail++;
                                            LOG.log(Level.INFO, "����Թ������ (Rectify) �������� {0} (TMTID : {1})", new Object[]{fail, labTMLT.getObjectId()});
                                        }
                                    }
                                    String log = String.format("����Թ������ (Rectify) ����� %d �ҡ������ %d", success, total);
                                    msg.append("\n").append(log);
                                    LOG.log(Level.INFO, log);
                                } else if (sheet.getSheetName().toLowerCase().contains("deprecate")) {
                                    int[] cols = new int[]{0};

                                    List<Object[]> datas = readDataBySelectedCols(sheet, cols);
                                    int success = 0;
                                    int fail = 0;
                                    int total = datas.size();
                                    for (int j = 0; j < datas.size(); j++) {
                                        String tmtId = String.valueOf(
                                                datas.get(j)[0] instanceof Double
                                                ? ((Double) datas.get(j)[0]).intValue()
                                                : datas.get(j)[0]);
                                        try {
                                            theUS.setStatus("����Թ���¡��ԡ (deprecate) " + j + "/" + total, UpdateStatus.WARNING);
                                            success += labTMLTDB.delete(tmtId);
                                        } catch (Exception ex) {
                                            fail++;
                                            LOG.log(Level.INFO, "����Թ���¡��ԡ (deprecate) �������� {0} (TMTID : {1})", new Object[]{fail, tmtId});
                                        }
                                    }
                                    String log = String.format("����Թ���¡��ԡ (deprecate) ����� %d �ҡ������ %d", success, total);
                                    msg.append("\n").append(log);
                                    LOG.log(Level.INFO, log);
                                }
                                LOG.log(Level.INFO, "Finish process sheet name : {0}", sheet.getSheetName());
                            }
                            theUS.setStatus("����������������ҵðҹ TMLT �����", UpdateStatus.COMPLETE);
                            JOptionPane.showMessageDialog(null, msg.toString());
                        } else {
                            theUS.setStatus(" �������ö������� ���ͧ�ҡ��������͡�����ѹ��ӡ���������ҡѺ ��������������ش (" + String.valueOf(versionChecks.get(0)[1]) + ")", UpdateStatus.WARNING);
                        }
                        clone.getConnection().commit();
                    } catch (Exception ex) {
                        theUS.setStatus("����������������ҵðҹ TMLT �Դ��Ҵ", UpdateStatus.ERROR);
                        LOG.log(Level.SEVERE, ex.getMessage(), ex);
                        try {
                            clone.getConnection().rollback();
                        } catch (SQLException ex1) {
                            LOG.log(Level.SEVERE, null, ex1);
                        }
                    } finally {
                        try {
                            clone.getConnection().close();
                        } catch (SQLException ex) {
                            LOG.log(Level.SEVERE, null, ex);
                        }
                    }
                    interrupt();
                }
            };
            t.start();
        } else {
            theUS.setStatus("������������١��ͧ (�ͧ�Ѻ��� *.xls ���� *.xlsx ��ҹ��)", UpdateStatus.WARNING);
        }
    }
}

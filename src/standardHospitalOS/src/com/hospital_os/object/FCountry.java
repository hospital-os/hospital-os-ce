/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong Damyos <somprasong@hospital-os.com>
 */
public class FCountry extends Persistent implements CommonInf {

    public String continent_en = "";
    public String continent_th = "";
    public String common_name_en = "";
    public String common_name_th = "";
    public String official_name_en = "";
    public String official_name_th = "";
    public String capital_name_en = "";
    public String capital_name_th = "";

    @Override
    public String getCode() {
        return getObjectId();
    }

    @Override
    public String getName() {
        return common_name_th;
    }

    @Override
    public String toString() {
        return common_name_th;
    }
}

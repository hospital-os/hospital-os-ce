package com.hospital_os.objdb;

import com.hospital_os.object.*;
import com.hospital_os.usecase.connection.*;
import com.hospital_os.utility.*;
import com.pcu.objdb.objdbclass.FamilyDB;
import com.pcu.object.Family;
import java.sql.*;
import java.text.*;
import java.util.*;

@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public class PatientDB {

    public ConnectionInf theConnectionInf;
    public Patient dbObj;
    final public String idtable = "206";

    protected String sqlcon = "";
    protected ResultSet rs;
    protected int icount;
    protected boolean bresult = false;
    protected Vector vHN;
    private FamilyDB theFamilyDB;

    public PatientDB(ConnectionInf db) {
        theConnectionInf = db;
        initConfig();
    }

    public PatientDB(ConnectionInf db, FamilyDB fdb) {
        theConnectionInf = db;
        initConfig();
        theFamilyDB = fdb;
    }

    private boolean initConfig() {
        dbObj = getMapObject();
        return true;
    }

    public static Patient getMapObject() {
        Patient dbObj = new Patient();
        dbObj.table = "t_patient";
        dbObj.pk_field = "t_patient_id";
        dbObj.hn = "patient_hn";
        dbObj.prefix_id = "f_patient_prefix_id";
        dbObj.fname = "patient_firstname";
        dbObj.lname = "patient_lastname";
        dbObj.xn = "patient_xn";
        dbObj.sex = "f_sex_id";
        dbObj.birthday = "patient_birthday";
        dbObj.house = "patient_house";
        dbObj.road = "patient_road";
        dbObj.village = "patient_moo";
        dbObj.tambon = "patient_tambon";
        dbObj.ampur = "patient_amphur";
        dbObj.changwat = "patient_changwat";
        dbObj.patient_postcode = "patient_postcode";
        dbObj.patient_address_eng = "patient_addrees_eng";
        dbObj.mstatus = "f_patient_marriage_status_id";
        dbObj.occupa = "f_patient_occupation_id";
        dbObj.race = "f_patient_race_id";
        dbObj.nation = "f_patient_nation_id";
        dbObj.religion = "f_patient_religion_id";
        dbObj.education = "f_patient_education_type_id";
        dbObj.fstatus = "f_patient_family_status_id";
        dbObj.father_fname = "patient_father_firstname";
        dbObj.mother_fname = "patient_mother_firstname";
        dbObj.couple_fname = "patient_couple_firstname";
        dbObj.move_in = "patient_move_in_date_time";
        dbObj.dischar = "f_patient_discharge_status_id";
        dbObj.ddisch = "patient_discharge_date_time";
        dbObj.bgroup = "f_patient_blood_group_id";
        dbObj.typearea = "f_patient_area_status_id";
        dbObj.cid_f = "patient_father_pid";
        dbObj.cif_m = "patient_mather_pid";
        dbObj.cid_couple = "patient_couple_pid";
        dbObj.p_type = "patient_community_status";
        dbObj.private_doc = "patient_private_doctor";
        dbObj.pid = "patient_pid";
        dbObj.mother_lname = "patient_mother_lastname";
        dbObj.father_lname = "patient_father_lastname";
        dbObj.couple_lname = "patient_couple_lastname";
        dbObj.phone = "patient_phone_number";
        dbObj.relation = "f_patient_relation_id";
        dbObj.phone_contact = "patient_contact_phone_number";
        dbObj.sex_contact = "patient_contact_sex_id";
        dbObj.house_contact = "patient_contact_house";
        dbObj.village_contact = "patient_contact_moo";
        dbObj.changwat_contact = "patient_contact_changwat";
        dbObj.ampur_contact = "patient_contact_amphur";
        dbObj.tambon_contact = "patient_contact_tambon";
        dbObj.patient_contact_postcode = "patient_contact_postcode";
        dbObj.road_contact = "patient_contact_road";
        dbObj.contact_fname = "patient_contact_firstname";
        dbObj.contact_lname = "patient_contact_lastname";
        dbObj.true_birthday = "patient_birthday_true";

        dbObj.record_date_time = "patient_record_date_time";
        dbObj.update_date_time = "patient_update_date_time";
        dbObj.staff_record = "patient_staff_record";
        dbObj.staff_modify = "patient_staff_modify";
        dbObj.staff_cancel = "patient_staff_cancel";
        dbObj.patient_drugallergy = "patient_drugallergy";
        dbObj.active = "patient_active";
        dbObj.family_id = "t_health_family_id";

        dbObj.mobile_phone = "patient_patient_mobile_phone";
        dbObj.contact_mobile_phone = "patient_contact_mobile_phone";
        dbObj.other_address = "patient_other_country_address";
        dbObj.is_other_country = "patient_is_other_country";
        dbObj.deny_allergy = "deny_allergy";
        dbObj.patient_email = "patient_patient_email";
        dbObj.contact_email = "patient_contact_email";
        dbObj.sent_email_invitation = "sent_email_invitation";
        return dbObj;
    }

    public int updateFidByFid(String family_id, String family_from) throws Exception {
        String sql = "update " + dbObj.table + " set "
                + dbObj.family_id + "='" + family_id + "',"
                + dbObj.staff_cancel + "='" + dbObj.staff_cancel + "'||-||'" + family_from + "'"
                + " where "
                + dbObj.family_id + "='" + family_from + "'";
        return theConnectionInf.eUpdate(sql);
    }

    public int updateFidByPtid(String family_id, String patient_id) throws Exception {
        StringBuffer sql = new StringBuffer("update ").append(dbObj.table).append(" set ").append(dbObj.family_id).append("='").append(family_id).append("' where ").append(dbObj.pk_field).append("='").append(patient_id).append("'");
        return theConnectionInf.eUpdate(sql.toString());
    }

    public int insert(Patient p) throws Exception {
        p.updateF2P();
        p.generateOID(idtable);
        StringBuffer sql = new StringBuffer("insert into ").append(dbObj.table).
                append(" (").append(dbObj.pk_field).
                append(" ,").append(dbObj.hn).
                append(" ,").append(dbObj.prefix_id).
                append(" ,").append(dbObj.fname).
                append(" ,").append(dbObj.lname).
                append(" ,").append(dbObj.xn).
                append(" ,").append(dbObj.sex).
                append(" ,").append(dbObj.birthday).
                append(" ,").append(dbObj.house).
                append(" ,").append(dbObj.road).
                append(" ,").append(dbObj.village).
                append(" ,").append(dbObj.tambon).
                append(" ,").append(dbObj.ampur).
                append(" ,").append(dbObj.changwat).
                append(" ,").append(dbObj.patient_postcode).
                append(" ,").append(dbObj.patient_address_eng).
                append(" ,").append(dbObj.mstatus).
                append(" ,").append(dbObj.occupa).
                append(" ,").append(dbObj.race).
                append(" ,").append(dbObj.pid).
                append(" ,").append(dbObj.nation).
                append(" ,").append(dbObj.religion).
                append(" ,").append(dbObj.education).
                append(" ,").append(dbObj.fstatus).
                append(" ,").append(dbObj.father_fname).
                append(" ,").append(dbObj.mother_fname).
                append(" ,").append(dbObj.father_lname).
                append(" ,").append(dbObj.mother_lname).
                append(" ,").append(dbObj.couple_fname).
                append(" ,").append(dbObj.couple_lname).
                append(" ,").append(dbObj.move_in).
                append(" ,").append(dbObj.dischar).
                append(" ,").append(dbObj.ddisch).
                append(" ,").append(dbObj.bgroup).
                append(" ,").append(dbObj.typearea).
                append(" ,").append(dbObj.cid_f).
                append(" ,").append(dbObj.cif_m).
                append(" ,").append(dbObj.cid_couple).
                append(" ,").append(dbObj.p_type).
                append(" ,").append(dbObj.private_doc).
                append(" ,").append(dbObj.phone).
                append(" ,").append(dbObj.relation).
                append(" ,").append(dbObj.sex_contact).
                append(" ,").append(dbObj.house_contact).
                append(" ,").append(dbObj.village_contact).
                append(" ,").append(dbObj.road_contact).
                append(" ,").append(dbObj.phone_contact).
                append(" ,").append(dbObj.changwat_contact).
                append(" ,").append(dbObj.ampur_contact).
                append(" ,").append(dbObj.tambon_contact).
                append(" ,").append(dbObj.patient_contact_postcode).
                append(" ,").append(dbObj.contact_fname).
                append(" ,").append(dbObj.contact_lname).
                append(" ,").append(dbObj.patient_drugallergy).
                append(" ,").append(dbObj.true_birthday).
                append(" ,").append(dbObj.record_date_time).
                append(" ,").append(dbObj.update_date_time).
                append(" ,").append(dbObj.staff_record).
                append(" ,").append(dbObj.staff_modify).
                append(" ,").append(dbObj.staff_cancel).
                append(" ,").append(dbObj.active).
                append(" ,").append(dbObj.family_id).
                append(" ,").append(dbObj.mobile_phone).
                append(" ,").append(dbObj.contact_mobile_phone).
                append(" ,").append(dbObj.patient_email).
                append(" ,").append(dbObj.contact_email).
                append(" ,").append(dbObj.other_address).
                append(" ,").append(dbObj.is_other_country).
                append(" ,").append(dbObj.deny_allergy).
                append(" ,latitude").
                append(" ,longitude").
                append(" , t_person_id").
                append(" , sent_email_invitation").
                append(" ) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"
                        + "?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"
                        + "?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"
                        + "?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"
                        + "?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"
                        + "?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"
                        + "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        PreparedStatement ePQuery = null;
        try {
            ePQuery = theConnectionInf.ePQuery(sql.toString());
            int index = 1;
            ePQuery.setString(index++, p.getObjectId());
            ePQuery.setString(index++, p.hn);
            ePQuery.setString(index++, p.prefix_id);
            ePQuery.setString(index++, p.fname);
            ePQuery.setString(index++, p.lname);
            ePQuery.setString(index++, p.xn);
            ePQuery.setString(index++, p.sex);
            ePQuery.setString(index++, p.birthday);
            ePQuery.setString(index++, p.house);
            ePQuery.setString(index++, p.road);
            ePQuery.setString(index++, p.village);
            ePQuery.setString(index++, p.tambon);
            ePQuery.setString(index++, p.ampur);
            ePQuery.setString(index++, p.changwat);
            ePQuery.setString(index++, p.patient_postcode);
            ePQuery.setString(index++, p.patient_address_eng);
            ePQuery.setString(index++, p.mstatus);
            ePQuery.setString(index++, p.occupa);
            ePQuery.setString(index++, p.race);
            ePQuery.setString(index++, p.pid.trim());
            ePQuery.setString(index++, p.nation);
            ePQuery.setString(index++, p.religion);
            ePQuery.setString(index++, p.education);
            ePQuery.setString(index++, p.fstatus);
            ePQuery.setString(index++, p.father_fname);
            ePQuery.setString(index++, p.mother_fname);
            ePQuery.setString(index++, p.father_lname);
            ePQuery.setString(index++, p.mother_lname);
            ePQuery.setString(index++, p.couple_fname);
            ePQuery.setString(index++, p.couple_lname);
            ePQuery.setString(index++, p.move_in);
            ePQuery.setString(index++, p.dischar);
            ePQuery.setString(index++, p.ddisch);
            ePQuery.setString(index++, p.bgroup);
            ePQuery.setString(index++, p.typearea);
            ePQuery.setString(index++, p.cid_f);
            ePQuery.setString(index++, p.cif_m);
            ePQuery.setString(index++, p.cid_couple);
            ePQuery.setString(index++, p.p_type);
            ePQuery.setString(index++, p.private_doc);
            ePQuery.setString(index++, p.phone);
            ePQuery.setString(index++, p.relation);
            ePQuery.setString(index++, p.sex_contact);
            ePQuery.setString(index++, p.house_contact);
            ePQuery.setString(index++, p.village_contact);
            ePQuery.setString(index++, p.road_contact);
            ePQuery.setString(index++, p.phone_contact);
            ePQuery.setString(index++, p.changwat_contact);
            ePQuery.setString(index++, p.ampur_contact);
            ePQuery.setString(index++, p.tambon_contact);
            ePQuery.setString(index++, p.patient_contact_postcode);
            ePQuery.setString(index++, p.contact_fname);
            ePQuery.setString(index++, p.contact_lname);
            ePQuery.setString(index++, p.patient_drugallergy);
            ePQuery.setString(index++, p.true_birthday);
            ePQuery.setString(index++, p.record_date_time);
            ePQuery.setString(index++, p.update_date_time);
            ePQuery.setString(index++, p.staff_record);
            ePQuery.setString(index++, p.staff_modify);
            ePQuery.setString(index++, p.staff_cancel);
            ePQuery.setString(index++, p.active);
            ePQuery.setString(index++, p.family_id);
            ePQuery.setString(index++, p.mobile_phone);
            ePQuery.setString(index++, p.contact_mobile_phone);
            ePQuery.setString(index++, p.patient_email);
            ePQuery.setString(index++, p.contact_email);
            ePQuery.setString(index++, p.other_address);
            ePQuery.setString(index++, p.is_other_country);
            ePQuery.setString(index++, p.deny_allergy);
            ePQuery.setDouble(index++, p.latitude);
            ePQuery.setDouble(index++, p.longitude);
            ePQuery.setString(index++, p.family_id);
            ePQuery.setString(index++, p.sent_email_invitation);
            return ePQuery.executeUpdate();
        } finally {
            if (ePQuery != null) {
                ePQuery.close();
            }
        }
    }

    public int updateParent(Patient o) throws Exception {
        update(o);
        if (o.getFamily() == null) {
            return 0;
        }
        return theFamilyDB.update(o.getFamily());
    }

    public int update(Patient p) throws Exception {
        p.updateF2P();
        StringBuffer sql = new StringBuffer("update ").append(dbObj.table).
                append(" set ").append(dbObj.hn).append("=?"
                + ", ").append(dbObj.prefix_id).append("=?"
                + ", ").append(dbObj.fname).append("=?"
                + ", ").append(dbObj.lname).append("=?"
                + ", ").append(dbObj.xn).append("=?"
                + ", ").append(dbObj.sex).append("=?"
                + ", ").append(dbObj.birthday).append("=?"
                + ", ").append(dbObj.house).append("=?"
                + ", ").append(dbObj.road).append("=?"
                + ", ").append(dbObj.village).append("=?"
                + ", ").append(dbObj.tambon).append("=?"
                + ", ").append(dbObj.ampur).append("=?"
                + ", ").append(dbObj.changwat).append("=?"
                + ", ").append(dbObj.patient_postcode).append("=?"
                + ", ").append(dbObj.patient_address_eng).append("=?"
                + ", ").append(dbObj.mstatus).append("=?"
                + ", ").append(dbObj.occupa).append("=?"
                + ", ").append(dbObj.race).append("=?"
                + ", ").append(dbObj.pid).append("=?"
                + ", ").append(dbObj.nation).append("=?"
                + ", ").append(dbObj.religion).append("=?"
                + ", ").append(dbObj.education).append("=?"
                + ", ").append(dbObj.fstatus).append("=?"
                + ", ").append(dbObj.father_fname).append("=?"
                + ", ").append(dbObj.mother_fname).append("=?"
                + ", ").append(dbObj.father_lname).append("=?"
                + ", ").append(dbObj.mother_lname).append("=?"
                + ", ").append(dbObj.couple_fname).append("=?"
                + ", ").append(dbObj.couple_lname).append("=?"
                + ", ").append(dbObj.move_in).append("=?"
                + ", ").append(dbObj.dischar).append("=?"
                + ", ").append(dbObj.ddisch).append("=?"
                + ", ").append(dbObj.bgroup).append("=?"
                + ", ").append(dbObj.typearea).append("=?"
                + ", ").append(dbObj.cid_f).append("=?"
                + ", ").append(dbObj.cif_m).append("=?"
                + ", ").append(dbObj.cid_couple).append("=?"
                + ", ").append(dbObj.p_type).append("=?"
                + ", ").append(dbObj.private_doc).append("=?"
                + ", ").append(dbObj.phone).append("=?"
                + ", ").append(dbObj.relation).append("=?"
                + ", ").append(dbObj.sex_contact).append("=?"
                + ", ").append(dbObj.house_contact).append("=?"
                + ", ").append(dbObj.village_contact).append("=?"
                + ", ").append(dbObj.road_contact).append("=?"
                + ", ").append(dbObj.phone_contact).append("=?"
                + ", ").append(dbObj.changwat_contact).append("=?"
                + ", ").append(dbObj.ampur_contact).append("=?"
                + ", ").append(dbObj.tambon_contact).append("=?"
                + ", ").append(dbObj.patient_contact_postcode).append("=?"
                + ", ").append(dbObj.contact_fname).append("=?"
                + ", ").append(dbObj.contact_lname).append("=?"
                + ", ").append(dbObj.patient_drugallergy).append("=?"
                + ", ").append(dbObj.true_birthday).append("=?"
                + ", ").append(dbObj.record_date_time).append("=?"
                + ", ").append(dbObj.update_date_time).append("=?"
                + ", ").append(dbObj.staff_record).append("=?"
                + ", ").append(dbObj.staff_modify).append("=?"
                + ", ").append(dbObj.staff_cancel).append("=?"
                + ", ").append(dbObj.active).append("=?"
                + ", ").append(dbObj.family_id).append("=?"
                + ", ").append(dbObj.mobile_phone).append("=?"
                + ", ").append(dbObj.contact_mobile_phone).append("=?"
                + ", ").append(dbObj.patient_email).append("=?"
                + ", ").append(dbObj.contact_email).append("=?"
                + ", ").append(dbObj.other_address).append("=?"
                + ", ").append(dbObj.is_other_country).append("=?"
                + ", ").append(dbObj.deny_allergy).append("=?"
                + ", ").append(dbObj.sent_email_invitation).append("=?"
                + ", latitude=?"
                + ", longitude=?").
                append(" where ").append(dbObj.pk_field).append("=?");

        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql.toString())) {
            int index = 1;
            ePQuery.setString(index++, p.hn);
            ePQuery.setString(index++, p.prefix_id);
            ePQuery.setString(index++, p.fname);
            ePQuery.setString(index++, p.lname);
            ePQuery.setString(index++, p.xn);
            ePQuery.setString(index++, p.sex);
            ePQuery.setString(index++, p.birthday);
            ePQuery.setString(index++, p.house);
            ePQuery.setString(index++, p.road);
            ePQuery.setString(index++, p.village);
            ePQuery.setString(index++, p.tambon);
            ePQuery.setString(index++, p.ampur);
            ePQuery.setString(index++, p.changwat);
            ePQuery.setString(index++, p.patient_postcode);
            ePQuery.setString(index++, p.patient_address_eng);
            ePQuery.setString(index++, p.mstatus);
            ePQuery.setString(index++, p.occupa);
            ePQuery.setString(index++, p.race);
            ePQuery.setString(index++, p.pid.trim());
            ePQuery.setString(index++, p.nation);
            ePQuery.setString(index++, p.religion);
            ePQuery.setString(index++, p.education);
            ePQuery.setString(index++, p.fstatus);
            ePQuery.setString(index++, p.father_fname);
            ePQuery.setString(index++, p.mother_fname);
            ePQuery.setString(index++, p.father_lname);
            ePQuery.setString(index++, p.mother_lname);
            ePQuery.setString(index++, p.couple_fname);
            ePQuery.setString(index++, p.couple_lname);
            ePQuery.setString(index++, p.move_in);
            ePQuery.setString(index++, p.dischar);
            ePQuery.setString(index++, p.ddisch);
            ePQuery.setString(index++, p.bgroup);
            ePQuery.setString(index++, p.typearea);
            ePQuery.setString(index++, p.cid_f);
            ePQuery.setString(index++, p.cif_m);
            ePQuery.setString(index++, p.cid_couple);
            ePQuery.setString(index++, p.p_type);
            ePQuery.setString(index++, p.private_doc);
            ePQuery.setString(index++, p.phone);
            ePQuery.setString(index++, p.relation);
            ePQuery.setString(index++, p.sex_contact);
            ePQuery.setString(index++, p.house_contact);
            ePQuery.setString(index++, p.village_contact);
            ePQuery.setString(index++, p.road_contact);
            ePQuery.setString(index++, p.phone_contact);
            ePQuery.setString(index++, p.changwat_contact);
            ePQuery.setString(index++, p.ampur_contact);
            ePQuery.setString(index++, p.tambon_contact);
            ePQuery.setString(index++, p.patient_contact_postcode);
            ePQuery.setString(index++, p.contact_fname);
            ePQuery.setString(index++, p.contact_lname);
            ePQuery.setString(index++, p.patient_drugallergy);
            ePQuery.setString(index++, p.true_birthday);
            ePQuery.setString(index++, p.record_date_time);
            ePQuery.setString(index++, p.update_date_time);
            ePQuery.setString(index++, p.staff_record);
            ePQuery.setString(index++, p.staff_modify);
            ePQuery.setString(index++, p.staff_cancel);
            ePQuery.setString(index++, p.active);
            ePQuery.setString(index++, p.family_id);
            ePQuery.setString(index++, p.mobile_phone);
            ePQuery.setString(index++, p.contact_mobile_phone);
            ePQuery.setString(index++, p.patient_email);
            ePQuery.setString(index++, p.contact_email);
            ePQuery.setString(index++, p.other_address);
            ePQuery.setString(index++, p.is_other_country);
            ePQuery.setString(index++, p.deny_allergy);
            ePQuery.setString(index++, p.sent_email_invitation);
            ePQuery.setDouble(index++, p.latitude);
            ePQuery.setDouble(index++, p.longitude);
            ePQuery.setString(index++, p.getObjectId());
            return ePQuery.executeUpdate();
        }
    }

    public int updateMobileNo(Patient p) throws Exception {
        String sql = "UPDATE t_patient\n"
                + "   SET patient_patient_mobile_phone=?\n"
                + " WHERE t_patient_id=?";
        PreparedStatement ePQuery = null;
        try {
            ePQuery = theConnectionInf.ePQuery(sql);
            int index = 1;
            ePQuery.setString(index++, p.mobile_phone.trim());
            ePQuery.setString(index++, p.getObjectId());
            return ePQuery.executeUpdate();
        } finally {
            if (ePQuery != null) {
                ePQuery.close();
            }
        }
    }

    public int updatePID(Patient p) throws Exception {
        String sql = "UPDATE t_patient\n"
                + "   SET patient_pid=?\n"
                + " WHERE t_patient_id=?";
        PreparedStatement ePQuery = null;
        try {
            ePQuery = theConnectionInf.ePQuery(sql);
            int index = 1;
            ePQuery.setString(index++, p.pid.trim());
            ePQuery.setString(index++, p.getObjectId());
            return ePQuery.executeUpdate();
        } finally {
            if (ePQuery != null) {
                ePQuery.close();
            }
        }
    }

    public int updateXN(String xn) throws Exception {
        StringBuffer sql = new StringBuffer("update ").append(dbObj.table).
                append(" set ").append(dbObj.xn).append("='").append(Gutil.CheckReservedWords(xn)).append("'");
        return theConnectionInf.eUpdate(sql.toString());
    }

    public int updateXN(Patient pat) throws Exception {
        StringBuffer sql = new StringBuffer("update ").append(dbObj.table).
                append(" set ").append(dbObj.xn).append("='").append(Gutil.CheckReservedWords(pat.xn)).append("'").
                append(" where ").append(dbObj.pk_field).append("= '").append(pat.getObjectId()).append("'");
        return theConnectionInf.eUpdate(sql.toString());
    }

    public int updateFamilyHome(Patient p) throws Exception {
        StringBuffer sql = new StringBuffer("update ").append(dbObj.table).
                append(" set ").append(dbObj.family_id).append("='").append(p.family_id //).append( "', " ).append( dbObj.has_health_home ).append( "='" ).append( p.has_health_home
        ).append("' where ").append(dbObj.pk_field).append("='").append(p.getObjectId()).append("'");

        return theConnectionInf.eUpdate(sql.toString());
    }

    public int updatePatientDischar(Patient o) throws Exception {
        o.updateF2P();
        StringBuffer sql = new StringBuffer("update ").append(dbObj.table).
                append(" set ").append(dbObj.dischar).append(" = '").append(o.dischar).append("'").
                append(",").append(dbObj.ddisch).append(" = '").append(o.ddisch).append("'").
                append(" where ").append(dbObj.pk_field).append("='").append(o.getObjectId()).append("'");
        return theConnectionInf.eUpdate(sql.toString());
    }

    public int updateFamilyID(Patient o) throws Exception {
        StringBuffer sql = new StringBuffer("update ").append(dbObj.table).append(
                " set ").append(dbObj.family_id).append(" = '").append(o.family_id).append("'").append(
                " where ").append(dbObj.pk_field).append("='").append(o.getObjectId()).append("'");
        return theConnectionInf.eUpdate(sql.toString());
    }

    public String selectPatientSexByPatientID(String patient_id) throws Exception {
        StringBuffer sql = new StringBuffer("Select ").append(dbObj.sex).append(" From ").append(dbObj.table).append("").append(
                " Where ").append(dbObj.pk_field).append("='").append(patient_id).append("'");
        Vector v = eQuerySex(sql.toString());

        if (v.isEmpty()) {
            return "1";
        } else {
            String sex = ((Patient) v.get(0)).sex;
            return sex;
        }
    }

    public int delete(Patient o) throws Exception {
        StringBuffer sql = new StringBuffer("delete from ").append(dbObj.table).
                append(" where ").append(dbObj.pk_field).append("='").append(o.getObjectId()).append("'");
        return theConnectionInf.eUpdate(sql.toString());
    }

    public int updateAllergy(String patient_id, String allergy) throws Exception {
        StringBuffer sql = new StringBuffer("UPDATE ").append(dbObj.table).append(" SET ").append(
                " ").append(dbObj.patient_drugallergy).append(" = '").append(allergy).append("'").append(
                " WHERE ").append(dbObj.pk_field).append("= '").append(patient_id).append("'");
        return theConnectionInf.eUpdate(sql.toString());
    }

    /**
     * @deprecated henbe unused
     */
    public int setPatientAllergyByPatientID(String patient_id, String allergy) throws Exception {
        StringBuffer sql = new StringBuffer("UPDATE ").append(dbObj.table).append(" SET ").append(
                " ").append(dbObj.patient_drugallergy).append(" = '").append(allergy).append("'").append(
                " WHERE ").append(dbObj.pk_field).append("= '").append(patient_id).append("'");
        return theConnectionInf.eUpdate(sql.toString());
    }

    public ResultSet selectData() throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.active).append(" ='1'");

        return theConnectionInf.eQuery(sql.toString());

    }

    /**
     *
     * �鹼����¨ҡ�Ţ��Ъҡ�
     */
    public Patient selectByFid(String family_id) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.family_id).append(" = '").append(family_id).append("' and ").append(dbObj.active).append(" = '1'");

        Vector v = eQuery(sql.toString());
        if (v.isEmpty()) {
            return null;
        }
        Patient p = (Patient) v.get(0);
        Family fm = theFamilyDB.selectByPK(p.family_id);
        p.setFamily(fm, true);
        return p;
    }

    public Patient selectByPatientID(String patientid) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.pk_field).append(" = '").append(patientid).append("'");

        Vector v = eQuery(sql.toString());
        if (v.isEmpty()) {
            return null;
        }
        Patient p = (Patient) v.get(0);
        Family fm = theFamilyDB.selectByPK(p.family_id);
        p.setFamily(fm, true);
        return p;
    }

    public String selectMaxHN() throws Exception {
        StringBuffer sql = new StringBuffer("Select MAX(").append(dbObj.hn).append(") as max From ").append(dbObj.table);
        ResultSet resultSet = theConnectionInf.eQuery(sql.toString());
        if (resultSet.next()) {
            return resultSet.getString(1);
        }
        return "0";
    }

    /**
     * @deprecated henbe unused ��������� �Թ˹��¤������٧�ҡ�ѧ�ѹ���
     */
    public Vector select() throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.active).append(" ='1'");
        Vector v = eQuery(sql.toString());
        if (v.isEmpty()) {
            return null;
        } else {
            return v;
        }
    }

    public int updateActiveByPatientID(Patient patient) throws Exception {
        StringBuffer sql = new StringBuffer("UPDATE ").append(dbObj.table).
                append(" Set ").append(dbObj.active).append("='").append(patient.active).append("'").append(
                ",").append(dbObj.update_date_time).append("='").append(patient.update_date_time).append("'").append(
                ",").append(dbObj.staff_cancel).append("='").append(patient.staff_cancel).append("'").append(
                ",").append(dbObj.p_type).append("='").append(patient.p_type).append("'").append(
                " WHERE ").append(dbObj.pk_field).append("='").append(patient.getObjectId()).append("'");
        return theConnectionInf.eUpdate(sql.toString());
    }

    public Vector selectByPKV(String pk) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.pk_field).append(" = '").append(pk).append("'").append(
                " and ").append(dbObj.active).append(" ='1'");
        Vector v = eQuery(sql.toString());
        if (v.isEmpty()) {
            return null;
        } else {
            return v;
        }
    }

    // ��Ѻ�����Ҵ��� %hn sumo 18/7/2549
    public Vector queryByHn(String hn) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.hn).append(" like '%").append(Gutil.CheckReservedWords(hn)).append("'").append(
                " and ").append(dbObj.active).append(" ='1'");

        return eQuery(sql.toString());
    }

    /**
     * @deprecated henbe used public Vector selectLikeHN(String hn,String
     * active) throws Exception
     *
     */
    public Vector selectLikeHN(String hn) throws Exception {
        return selectLikeHN(hn, "1");
    }

    public Vector selectLikeHN(String hn, String active) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.hn).append(" like '").append(Gutil.CheckReservedWords(hn)).append("'");
        if (!active.isEmpty()) {
            sql.append(" and ").append(dbObj.active).append(" ='").append(active).append("'");
        }
        return eQuery(sql.toString());
    }

    public Vector queryByName(String pname, String fname, String lname) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.prefix_id).append(" like '").append(Gutil.CheckReservedWords(pname)).
                append("' and ").append(dbObj.fname).append(" like '").append(Gutil.CheckReservedWords(fname)).
                append("' and ").append(dbObj.lname).append(" like '").append(Gutil.CheckReservedWords(lname)).append("' ").append(
                " and ").append(dbObj.active).append(" ='1' ").append(
                "order by ").append(dbObj.fname);

        return eQuery(sql.toString());
    }

    public Vector queryByFName(String fname) throws Exception {
        StringBuffer sql = new StringBuffer("select ").append(dbObj.hn).append(",").append(dbObj.fname).
                append(",").append(dbObj.lname).append(",").append(dbObj.mother_fname).append(" from ").append(dbObj.table).
                append(" where ").append(dbObj.fname).append(" like '").append(Gutil.CheckReservedWords(fname)).append("' ").append(
                " and ").append(dbObj.active).append(" ='1' ").append("order by ").append(dbObj.fname);

        return eQuery(sql.toString());
    }

    public Vector queryBySName(String lname) throws Exception {
        StringBuffer sql = new StringBuffer("select ").append(dbObj.hn).
                append(",").append(dbObj.fname).append(",").append(dbObj.lname).
                append(",").append(dbObj.mother_fname).
                append(" from ").append(dbObj.table).
                append(" where ").append(dbObj.lname).append(" like '").append(Gutil.CheckReservedWords(lname)).append("' ").append(
                " and ").append(dbObj.active).append(" ='1' ");

        return eQuery(sql.toString());
    }

    public Vector eQuerySex(String sql) throws Exception {
        Patient p;
        Vector list = new Vector();
        rs = theConnectionInf.eQuery(sql.toString());
        int i = 0;
        while (rs.next()) {
            p = new Patient();
            p.sex = rs.getString(dbObj.sex);

            list.add(p);
            i = i + 1;
            if (i == 100) {
                break;
            }
        }
        rs.close();
        return list;
    }

    public String selectNamePatientbyPatintID(String patient_id) throws Exception {
        StringBuffer sql = new StringBuffer("Select ").append(dbObj.fname).append(",").append(dbObj.lname).append(" FROM ").append(
                "").append(dbObj.table).append(" WHERE ").append(dbObj.pk_field).append(" ='").append(patient_id).append("'");

        return eQueryNamePatient(sql.toString());

    }

    public String selectHNPatientbyPatintID(String patient_id) throws Exception {
        StringBuffer sql = new StringBuffer("Select ").append(dbObj.hn).append(" FROM ").append(
                "").append(dbObj.table).append(" WHERE ").append(dbObj.pk_field).append(" ='").append(patient_id).append("'");

        return eQueryHNPatient(sql.toString());

    }

    public String eQueryNamePatient(String sql) throws Exception {
        Patient p = null;

        ResultSet resultSet = theConnectionInf.eQuery(sql);
        while (resultSet.next()) {
            p = new Patient();

            p.fname = resultSet.getString(dbObj.fname);
            p.lname = resultSet.getString(dbObj.lname);

        }
        if (p != null) {
            return p.fname + " " + p.lname;
        } else {
            return null;
        }
    }

    public String eQueryHNPatient(String sql) throws Exception {
        Patient p = null;
        ResultSet resultSet = theConnectionInf.eQuery(sql);
        int i = 0;
        while (resultSet.next()) {
            p = new Patient();
            p.hn = resultSet.getString(dbObj.hn);
        }
        if (p != null) {
            return p.hn;
        } else {
            return null;
        }
    }

    public static String getStringFromRS(ResultSet rs, String field_name) throws Exception {
        String str = rs.getString(field_name);
        if (str != null) {
            return str;
        } else {
            return "";
        }
    }

    public Vector eQuery(String sql) throws Exception {
        Vector list = new Vector();
        ResultSet resultSet = theConnectionInf.eQuery(sql);
        int i = 0;
        while (resultSet.next()) {
            Patient p = new Patient();
            getObject(dbObj, p, resultSet);
            list.add(p);
            i = i + 1;
            if (i == 100) {
                break;
            }
        }
        resultSet.close();
        return list;
    }

    public Vector eQueryNolimit(String sql) throws Exception {
        Vector list = new Vector();
        ResultSet resultSet = theConnectionInf.eQuery(sql);
        while (resultSet.next()) {
            Patient p = new Patient();
            getObject(dbObj, p, resultSet);
            Family fm = theFamilyDB.selectByPK(p.family_id);
            p.setFamily(fm);
            list.add(p);
        }
        resultSet.close();
        return list;
    }

    public boolean getPatient(Patient p, ResultSet rs) throws Exception {
        return getObject(dbObj, p, rs);
    }

    public static boolean getObject(Patient dbObj, Patient p, ResultSet rs) throws Exception {
        p.setObjectId(getStringFromRS(rs, dbObj.pk_field));
        p.hn = getStringFromRS(rs, dbObj.hn);
        p.prefix_id = getStringFromRS(rs, dbObj.prefix_id);
        p.fname = getStringFromRS(rs, dbObj.fname);
        p.patient_name = getStringFromRS(rs, dbObj.fname);
        p.lname = getStringFromRS(rs, dbObj.lname);
        p.patient_last_name = getStringFromRS(rs, dbObj.lname);
        p.xn = getStringFromRS(rs, dbObj.xn);
        p.sex = getStringFromRS(rs, dbObj.sex);
        p.birthday = getStringFromRS(rs, dbObj.birthday);
        p.patient_birthday = getStringFromRS(rs, dbObj.birthday);
        p.mother_firstname = getStringFromRS(rs, dbObj.mother_fname);
        p.house = getStringFromRS(rs, dbObj.house);
        p.road = getStringFromRS(rs, dbObj.road);
        p.village = getStringFromRS(rs, dbObj.village);
        p.tambon = getStringFromRS(rs, dbObj.tambon);
        p.ampur = getStringFromRS(rs, dbObj.ampur);
        p.changwat = getStringFromRS(rs, dbObj.changwat);
        p.patient_postcode = getStringFromRS(rs, dbObj.patient_postcode);
        p.patient_address_eng = getStringFromRS(rs, dbObj.patient_address_eng);
        p.mstatus = getStringFromRS(rs, dbObj.mstatus);
        p.occupa = getStringFromRS(rs, dbObj.occupa);
        p.race = getStringFromRS(rs, dbObj.race);
        p.pid = getStringFromRS(rs, dbObj.pid);
        p.nation = getStringFromRS(rs, dbObj.nation);
        p.religion = getStringFromRS(rs, dbObj.religion);
        p.education = getStringFromRS(rs, dbObj.education);
        p.fstatus = getStringFromRS(rs, dbObj.fstatus);
        p.father_fname = getStringFromRS(rs, dbObj.father_fname);
        p.mother_fname = getStringFromRS(rs, dbObj.mother_fname);
        p.father_lname = getStringFromRS(rs, dbObj.father_lname);
        p.mother_lname = getStringFromRS(rs, dbObj.mother_lname);
        p.couple_fname = getStringFromRS(rs, dbObj.couple_fname);
        p.couple_lname = getStringFromRS(rs, dbObj.couple_lname);
        p.move_in = getStringFromRS(rs, dbObj.move_in);
        p.dischar = getStringFromRS(rs, dbObj.dischar);
        p.ddisch = getStringFromRS(rs, dbObj.ddisch);
        p.bgroup = getStringFromRS(rs, dbObj.bgroup);
        p.typearea = getStringFromRS(rs, dbObj.typearea);
        p.cid_f = getStringFromRS(rs, dbObj.cid_f);
        p.cif_m = getStringFromRS(rs, dbObj.cif_m);
        p.cid_couple = getStringFromRS(rs, dbObj.cid_couple);
        p.p_type = getStringFromRS(rs, dbObj.p_type);
        p.private_doc = getStringFromRS(rs, dbObj.private_doc);
        p.phone = getStringFromRS(rs, dbObj.phone);
        p.relation = getStringFromRS(rs, dbObj.relation);
        p.sex_contact = getStringFromRS(rs, dbObj.sex_contact);
        p.house_contact = getStringFromRS(rs, dbObj.house_contact);
        p.village_contact = getStringFromRS(rs, dbObj.village_contact);
        p.road_contact = getStringFromRS(rs, dbObj.road_contact);
        p.phone_contact = getStringFromRS(rs, dbObj.phone_contact);
        p.changwat_contact = getStringFromRS(rs, dbObj.changwat_contact);
        p.ampur_contact = getStringFromRS(rs, dbObj.ampur_contact);
        p.tambon_contact = getStringFromRS(rs, dbObj.tambon_contact);
        p.patient_contact_postcode = getStringFromRS(rs, dbObj.patient_contact_postcode);
        p.contact_fname = getStringFromRS(rs, dbObj.contact_fname);
        p.contact_lname = getStringFromRS(rs, dbObj.contact_lname);
        p.true_birthday = getStringFromRS(rs, dbObj.true_birthday);
        p.patient_drugallergy = getStringFromRS(rs, dbObj.patient_drugallergy);
        p.record_date_time = getStringFromRS(rs, dbObj.record_date_time);
        p.update_date_time = getStringFromRS(rs, dbObj.update_date_time);
        p.staff_record = getStringFromRS(rs, dbObj.staff_record);
        p.staff_modify = getStringFromRS(rs, dbObj.staff_modify);
        p.staff_cancel = getStringFromRS(rs, dbObj.staff_cancel);
        p.active = getStringFromRS(rs, dbObj.active);
        p.family_id = getStringFromRS(rs, dbObj.family_id);
        p.mobile_phone = getStringFromRS(rs, dbObj.mobile_phone);
        p.contact_mobile_phone = getStringFromRS(rs, dbObj.contact_mobile_phone);
        p.patient_email = getStringFromRS(rs, dbObj.patient_email);
        p.contact_email = getStringFromRS(rs, dbObj.contact_email);
        p.other_address = getStringFromRS(rs, dbObj.other_address);
        p.is_other_country = getStringFromRS(rs, dbObj.is_other_country);
        p.deny_allergy = getStringFromRS(rs, dbObj.deny_allergy);
        p.sent_email_invitation = getStringFromRS(rs, dbObj.sent_email_invitation);
        p.latitude = rs.getDouble("latitude");
        p.longitude = rs.getDouble("longitude");
        return true;
    }

    public Vector queryByFLName(String fname, String lname) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.fname).append(" like '").append(Gutil.CheckReservedWords(fname)).
                append("' and ").append(dbObj.lname).append(" like '").append(Gutil.CheckReservedWords(lname)).append("' ").append(
                " and ").append(dbObj.active).append(" ='1' ");

        return eQuery(sql.toString());
    }

    public Vector queryPid(String pid) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.pid).append(" = '").append(Gutil.CheckReservedWords(pid)).append("'").append(
                " and ").append(dbObj.active).append(" ='1' ");
        return eQuery(sql.toString());
    }

    public Vector queryFPid(String fpid) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.pid).append(" = '").append(Gutil.CheckReservedWords(fpid)).append("'").append(
                " and ").append(dbObj.active).append(" ='1' ");
        return eQuery(sql.toString());
    }

    public Vector queryMPid(String mpid) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.pid).append(" = '").append(Gutil.CheckReservedWords(mpid)).append("'").append(
                " and ").append(dbObj.active).append(" ='1' ");
        return eQuery(sql.toString());
    }

    public Vector queryCPid(String cpid) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.pid).append(" = '").append(Gutil.CheckReservedWords(cpid)).append("'").append(
                " and ").append(dbObj.active).append(" ='1' ");
        return eQuery(sql.toString());
    }

    public int countPatientByHN(String HN) throws Exception {
        StringBuffer sql = new StringBuffer("SELECT COUNT(").append(dbObj.hn).append(") as count").append(
                " FROM ").append(dbObj.table).append("").append(
                " WHERE ").append(dbObj.hn).append(" LIKE '%").append(Gutil.CheckReservedWords(HN)).append("'");

        ResultSet resultSet = theConnectionInf.eQuery(sql.toString());
        int count = 0;

        while (resultSet.next()) {
            count = resultSet.getInt("count");
        }
        resultSet.close();
        return count;

    }

    public int selectCountHN(String hn) throws Exception {
        StringBuffer sql = new StringBuffer("select count(*) as cnt from ").append(dbObj.table).
                append(" where ").append(dbObj.hn).append(" = '").append(Gutil.CheckReservedWords(hn)).append("'").
                append(" and ").append(dbObj.active).append(" = '1'");
        ResultSet resultSet = theConnectionInf.eQuery(sql.toString());
        if (resultSet.next()) {
            return resultSet.getInt("cnt");
        }
        return 0;
    }

    public int selectCountPID(String pid) throws Exception {
        StringBuffer sql = new StringBuffer("select count(*) as cnt from ").append(dbObj.table).
                append(" where ").append(dbObj.pid).append(" = '").append(Gutil.CheckReservedWords(pid)).append("'").
                append(" and ").append(dbObj.active).append(" = '1'");
        ResultSet resultSet = theConnectionInf.eQuery(sql.toString());
        if (resultSet.next()) {
            return resultSet.getInt("cnt");
        }
        return 0;
    }

    public Patient selectByHnEqual(String hn) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.hn).append(" = '").append(Gutil.CheckReservedWords(hn)).append("'");
        Vector v = eQuery(sql.toString());
        if (v.isEmpty()) {
            return null;
        }
        Patient p = (Patient) v.get(0);
        Family fm = theFamilyDB.selectByPK(p.family_id);
        p.setFamily(fm, true);
        return p;
    }

    public Patient selectByHn(String hn) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.hn).append(" like '%").append(Gutil.CheckReservedWords(hn)).append("'").append(
                " and ").append(dbObj.active).append(" ='1'");

        Vector v = eQuery(sql.toString());
        if (v.isEmpty()) {
            return null;
        }
        Patient p = (Patient) v.get(0);
        Family fm = theFamilyDB.selectByPK(p.family_id);
        p.setFamily(fm, true);
        return p;
    }

    public Vector selectByXN(String xn) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.xn).append(" = '").append(Gutil.CheckReservedWords(xn)).
                append("' and ").append(dbObj.active).append(" ='1'");
        return eQuery(sql.toString());
    }

    public Patient selectByPK(String pk) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.pk_field).append(" = '").append(pk).append("'");
        Vector v = eQuery(sql.toString());
        if (v.isEmpty()) {
            return null;
        }
        Patient p = (Patient) v.get(0);
        Family fm = theFamilyDB.selectByPK(p.family_id);
        p.setFamily(fm, true);
        return p;

    }

    public Patient selectByFamilyID(String fid) throws Exception {
        String sql = "select * from " + dbObj.table + " where " + dbObj.family_id + " = '" + fid + "'";
        Vector v = eQuery(sql.toString());
        if (v.isEmpty()) {
            return null;
        }
        Patient p = (Patient) v.get(0);
        Family fm = theFamilyDB.selectByPK(p.family_id);
        p.setFamily(fm, true);
        return p;
    }

    /**
     * @deprecated henbe unused
     * @param pk
     * @return
     * @throws Exception
     */
    public Patient selectParentByPK(String pk) throws Exception {
        Patient pt = selectByPK(pk);
        Family fm = null;
        if (!pt.family_id.isEmpty()) {
            fm = theFamilyDB.selectByPK(pt.family_id);
        }
        pt.setFamily(fm);
        return pt;
    }

    /**
     * ��㹡������� xn �������������������ի�ӡѺ�ͧ��������������ѧ
     * ��ҫ�����ǡ���� return �� false ����ѧ��� ��ӡ���� return �� true
     *
     * @param xn �� String �ͧ�Ţ xn ������
     * @return �� boolean ��ҫ�����ǡ���� return �� false ����ѧ��� ��ӡ����
     * return �� true
     * @author padungrat(tong)
     * @date 21/04/2549,11:22
     */
    public Vector checkSamePatientXn(String xn, String patient_id) throws Exception {
        StringBuffer sql = new StringBuffer("SELECT ").append(dbObj.hn).append(" as count FROM ").append(dbObj.table).
                append(" WHERE UPPER(").append(dbObj.xn).append(") LIKE UPPER('").append(Gutil.CheckReservedWords(xn)).append("') ").
                append(" AND ").append(dbObj.active).append(" = '").append(Active.isEnable()).append("'").
                append(" AND ").append(dbObj.pk_field).append(" <> '").append(patient_id).append("'");
        ResultSet resultSet = theConnectionInf.eQuery(sql.toString());
        bresult = false;
        vHN = new Vector();
        while (resultSet.next()) {
            vHN.add(resultSet.getString(1));
        }
        resultSet.close();
        return vHN;
    }

    public Vector selectLikeXn(String pk) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.xn).append(" like '").append(Gutil.CheckReservedWords(pk)).append("'").
                append(" and ").append(dbObj.active).append(" ='1' ");
        return eQuery(sql.toString());
    }

    public Patient selectByHnToBorrowFilm(String hn) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.hn).append(" like '%").append(Gutil.CheckReservedWords(hn)).append("'").append(
                " and ").append(dbObj.active).append(" ='1'").append(
                " and ").append(dbObj.xn).append(" !=''");

        Vector v = eQuery(sql.toString());
        if (v.isEmpty()) {
            return null;
        }
        Patient p = (Patient) v.get(0);
        Family fm = theFamilyDB.selectByPK(p.family_id);
        p.setFamily(fm, true);
        return p;
    }

    public Patient selectByXnToBorrowFilm(String xn) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.xn).append(" = '").append(Gutil.CheckReservedWords(xn)).append("'").
                append(" and ").append(dbObj.active).append(" ='1'");
        Vector v = eQuery(sql.toString());
        if (v.isEmpty()) {
            return null;
        }
        Patient p = (Patient) v.get(0);
        Family fm = theFamilyDB.selectByPK(p.family_id);
        p.setFamily(fm, true);
        return p;
    }

    public Vector selectLocking(String str1, String str2) throws Exception {
        StringBuffer sql = new StringBuffer("select * from t_patient").append(
                " left join t_visit on t_patient.t_patient_id = t_visit.t_patient_id").append(
                        " where (f_visit_status_id like '").append(str1).append("'").append(
                " or f_visit_status_id like '").append(str2).append("')").append(
                " and visit_locking = '1' ").append(
                        " order by visit_begin_visit_time desc  limit 500");

        return eQuery(sql.toString());
    }

    public Vector selectLockingByHN(String hn) throws Exception {
        StringBuffer sql = new StringBuffer("select * from t_patient").append(
                " left join t_visit on t_patient.t_patient_id = t_visit.t_patient_id").append(
                        " where visit_locking = '1' ").append(
                        " and t_visit.visit_hn like '").append(Gutil.CheckReservedWords(hn)).append("'").append(
                " order by t_visit.visit_vn desc ");

        return eQuery(sql.toString());
    }

    /**
     * @author henbe css �ͧ���
     */
    public Vector selectByHnXnFnameLnamePid(String name) throws Exception {
        boolean is_number = false;
        boolean is_pid = false;
        boolean is_xray = false;
        long value = 0;
        try {
            value = Integer.parseInt(name);
            is_number = true;
        } catch (Exception e) {
        }
        if (name.length() == 13) {
            is_pid = true;
        }
        if (name.startsWith("X") || name.startsWith("x")) {
            is_xray = true;
        }

        DecimalFormat d = new DecimalFormat();
        d.applyPattern("000000000");
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).append(" where (");
        if (is_number) {
            sql.append(dbObj.hn).append(" = '").append(d.format(value)).append("'");
        } else if (is_pid) {
            sql.append(dbObj.pid).append(" = '").append(Gutil.CheckReservedWords(name)).append("'");
        } else if (is_xray) {
            sql.append(dbObj.xn).append(" = '").append(Gutil.CheckReservedWords(name)).append("'");
        } else {
            sql.append(dbObj.fname).append(" like '").append(Gutil.CheckReservedWords(name)).append("%'").append(
                    " or ").append(dbObj.lname).append(" like '").append(Gutil.CheckReservedWords(name)).append("%'");
        }
        sql.append(") and ").append(dbObj.active).append(" ='1' ");
        return eQuery(sql.toString());
    }

    public boolean selectByPrefix(String prefix_id) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.prefix_id).append(" = '").append(prefix_id).append("'").
                append(" and ").append(dbObj.active).append(" ='1'");
        Vector v = eQuery(sql.toString());
        if (v.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    public Vector selectByPID(String pid_dep) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.pid).append(" = '").append(Gutil.CheckReservedWords(pid_dep)).append("'").
                append(" and ").append(dbObj.active).append(" ='1'");
        return eQuery(sql.toString());
    }

    public int updatePidByPtid(String family_pid, String patient_id) throws Exception {
        StringBuffer sql = new StringBuffer("update ").append(dbObj.table).append(
                " set ").append(dbObj.pid).append("='").append(family_pid).append("'").append(
                " where ").append(dbObj.pk_field).append("='").append(patient_id).append("'");
        return theConnectionInf.eUpdate(sql.toString());
    }

    public String selectMaxXN(String pattern) throws Exception {
        StringBuffer sql = new StringBuffer("Select MAX(").append(dbObj.xn).append(") as max From ").append(dbObj.table).
                append(" where length(").append(dbObj.xn).append(") >=").append(pattern.length());
        ResultSet resultSet = theConnectionInf.eQuery(sql.toString());
        String ret = "0";
        if (resultSet.next()) {
            ret = resultSet.getString(1);
        }
        resultSet.close();
        return ret;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class FinanceInsuranceDiscountsCondition extends Persistent {
    
    public String b_finance_insurance_plan_id = "";
    public String b_item_billing_subgroup_id = "";
    public String condition_adjustment = "";
    // in object only
    public String b_item_billing_subgroup_name = "";
}

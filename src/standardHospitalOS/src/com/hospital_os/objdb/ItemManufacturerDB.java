/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.ItemManufacturer;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class ItemManufacturerDB {

    public ConnectionInf connectionInf;
    final public String tableId = "856";

    public ItemManufacturerDB(ConnectionInf db) {
        connectionInf = db;
    }

    public int insert(ItemManufacturer obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO b_item_manufacturer(\n"
                    + "            b_item_manufacturer_id, item_manufacturer_number, item_manufacturer_description,\n"
                    + "            active, user_record,user_modify)\n"
                    + "    VALUES (?, ?, ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.getGenID(tableId));
            preparedStatement.setString(index++, obj.item_manufacturer_number);
            preparedStatement.setString(index++, obj.item_manufacturer_description);
            preparedStatement.setString(index++, obj.active);
            preparedStatement.setString(index++, obj.user_record);
            preparedStatement.setString(index++, obj.user_modify);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(ItemManufacturer obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE b_item_manufacturer\n"
                    + "   SET item_manufacturer_number=?, \n"
                    + "       item_manufacturer_description=?, active=?, modify_date_time=current_timestamp, user_modify=?\n"
                    + " WHERE b_item_manufacturer_id=?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.item_manufacturer_number);
            preparedStatement.setString(index++, obj.item_manufacturer_description);
            preparedStatement.setString(index++, obj.active);
            preparedStatement.setString(index++, obj.user_modify);
            preparedStatement.setString(index++, obj.getObjectId());
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(ItemManufacturer obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM b_item_manufacturer\n");
            sql.append(" WHERE b_item_manufacturer_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public ItemManufacturer selectById(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_item_manufacturer\n"
                    + "where b_item_manufacturer_id = ?\n";
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, id);
            List<ItemManufacturer> executeQuery = executeQuery(preparedStatement);
            return executeQuery.isEmpty() ? null : executeQuery.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<ItemManufacturer> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_item_manufacturer where active = '1' order by item_manufacturer_number, item_manufacturer_description";
            preparedStatement = connectionInf.ePQuery(sql);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<ItemManufacturer> listByKeyword(String keyword, String active) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_item_manufacturer\n"
                    + "where (item_manufacturer_number ilike ? or item_manufacturer_description ilike ?)\n"
                    + "and active = ?\n"
                    + "order by item_manufacturer_number, item_manufacturer_description";
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, "%" + keyword + "%");
            preparedStatement.setString(index++, "%" + keyword + "%");
            preparedStatement.setString(index++, active);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<ItemManufacturer> listByCode(String code) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_item_manufacturer\n"
                    + "where item_manufacturer_number = ? \n"
                    + "and active = '1'\n"
                    + "order by item_manufacturer_number, item_manufacturer_description";
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, code);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<ItemManufacturer> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<ItemManufacturer> list = new ArrayList<ItemManufacturer>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                ItemManufacturer obj = new ItemManufacturer();
                obj.setObjectId(rs.getString("b_item_manufacturer_id"));
                obj.item_manufacturer_description = rs.getString("item_manufacturer_description");
                obj.item_manufacturer_number = rs.getString("item_manufacturer_number");
                obj.active = rs.getString("active");
                obj.record_date_time = rs.getTimestamp("record_date_time");
                obj.user_record = rs.getString("user_record");
                obj.modify_date_time = rs.getTimestamp("modify_date_time");
                obj.user_modify = rs.getString("user_modify");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (ItemManufacturer obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }

    public List<CommonInf> getComboboxDatasource(String keyword) throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (ItemManufacturer obj : listByKeyword(keyword, "1")) {
            list.add((CommonInf) obj);
        }
        return list;
    }
}

-- จับคู่สิทธิประกันสังคม
CREATE TABLE b_map_contract_plans_socialsec(
    b_map_contract_plans_socialsec_id    character varying(30)   NOT NULL,
    b_contract_plans_id                   character varying(30)   NOT NULL,
CONSTRAINT b_map_contract_plans_socialsec_pkey PRIMARY KEY (b_map_contract_plans_socialsec_id)
);

CREATE TABLE t_visit_socialsec_plan(
    t_visit_socialsec_plan_id    character varying(30)   NOT NULL,
    t_visit_payment_id           character varying(30)   NOT NULL,
    socialsec_number             character varying(50)   NULL,
    approve_status               character varying(2) NOT NULL default '',
    active                       character varying(1) NOT NULL default '1', 
    record_datetime              timestamp without time zone NOT NULL DEFAULT current_timestamp,
    user_record_id               character varying(30)   NOT NULL,
    update_datetime              timestamp without time zone NOT NULL DEFAULT current_timestamp,
    user_update_id               character varying(30)   NOT NULL,
CONSTRAINT t_visit_socialsec_plan_pkey PRIMARY KEY (t_visit_socialsec_plan_id)
);

-- เพิ่มหมวดค่ารักษาพยาบาล
BEGIN; 
UPDATE b_item_16_group SET item_16_group_description = 'ค่าห้องผ่าตัด / ห้องคลอด' WHERE item_16_group_number = 'H'; 
INSERT INTO b_item_16_group SELECT  '3120000000017','H','ค่าห้องผ่าตัด / ห้องคลอด','1'
WHERE NOT EXISTS (SELECT 1 FROM b_item_16_group WHERE item_16_group_number = 'H'); 
COMMIT;
BEGIN; 
UPDATE b_item_16_group SET item_16_group_description = 'ค่าธรรมเนียมบุคลากรทางการแพทย์' WHERE item_16_group_number = 'I'; 
INSERT INTO b_item_16_group SELECT  '3120000000018','I','ค่าธรรมเนียมบุคลากรทางการแพทย์','1'
WHERE NOT EXISTS (SELECT 1 FROM b_item_16_group WHERE item_16_group_number = 'I'); 
COMMIT;

-- EDC
ALTER TABLE t_visit_govoffical_plan ADD COLUMN approve_status character varying(2) default ''; 
ALTER TABLE t_visit_govoffical_plan ADD COLUMN active character varying(1) NOT NULL default '1'; 
ALTER TABLE t_visit_govoffical_plan ADD COLUMN f_govoffical_plan_transaction_code_id  character varying(2); 
ALTER TABLE t_visit_govoffical_plan ADD COLUMN response_message text; 
ALTER TABLE t_visit_govoffical_plan ADD COLUMN invoice_number text; 
ALTER TABLE t_visit_govoffical_plan ADD COLUMN terminal_id text;
ALTER TABLE t_visit_govoffical_plan ADD COLUMN merchant_id text; 
ALTER TABLE t_visit_govoffical_plan ADD COLUMN response_date text; 
ALTER TABLE t_visit_govoffical_plan ADD COLUMN response_time text;

CREATE INDEX t_visit_payment_id ON t_visit_govoffical_plan (t_visit_payment_id);
CREATE INDEX govoffical_number ON t_visit_govoffical_plan (govoffical_number);

CREATE TABLE IF NOT EXISTS f_govoffical_plan_transaction_code (
   id character varying(2) NOT NULL,
   description character varying(255) NOT NULL,
   tx_type character varying(1) NOT NULL, -- 1 = sale, 2 = void, 3 = settlement, 4 = re-print
   CONSTRAINT govoffical_plan_transaction_code_pk PRIMARY KEY (id)
);

INSERT INTO  f_govoffical_plan_transaction_code VALUES ('11','ผู้ป่วยนอกทั่วไป สิทธิตนเองและครอบครัว', '1');
INSERT INTO  f_govoffical_plan_transaction_code VALUES ('12','ผู้ป่วยนอกทั่วไป สิทธิบุตร 0-7 ปี', '1');
INSERT INTO  f_govoffical_plan_transaction_code VALUES ('13','ผู้ป่วยนอกทั่วไป สิทธิคู่สมรสต่างชาติ', '1');
INSERT INTO  f_govoffical_plan_transaction_code VALUES ('14','ผู้ป่วยนอกทั่วไป ไม่สามารถใช้บัตรได้', '1');
INSERT INTO  f_govoffical_plan_transaction_code VALUES ('21','หน่วยไตเทียม สิทธิตนเองและครอบครัว', '1');
INSERT INTO  f_govoffical_plan_transaction_code VALUES ('22','หน่วยไตเทียม สิทธิบุตร 0-7 ปี', '1');
INSERT INTO  f_govoffical_plan_transaction_code VALUES ('23','หน่วยไตเทียม สิทธิคู่สมรสต่างชาติ', '1');
INSERT INTO  f_govoffical_plan_transaction_code VALUES ('24','หน่วยไตเทียม ไม่สามารถใช้บัตรได้', '1');
INSERT INTO  f_govoffical_plan_transaction_code VALUES ('31','หน่วยรังสีผู้เป็นมะเร็ง สิทธิตนเองและครอบครัว', '1');
INSERT INTO  f_govoffical_plan_transaction_code VALUES ('32','หน่วยรังสีผู้เป็นมะเร็ง สิทธิบุตร 0-7 ปี', '1');
INSERT INTO  f_govoffical_plan_transaction_code VALUES ('33','หน่วยรังสีผู้เป็นมะเร็ง สิทธิคู่สมรสต่างชาติ', '1');
INSERT INTO  f_govoffical_plan_transaction_code VALUES ('34','หน่วยรังสีผู้เป็นมะเร็ง ไม่สามารถใช้บัตรได้', '1');
INSERT INTO  f_govoffical_plan_transaction_code VALUES ('26','รายการยกเลิก', '2');
INSERT INTO  f_govoffical_plan_transaction_code VALUES ('50','รายการโอนยอด', '3');
INSERT INTO  f_govoffical_plan_transaction_code VALUES ('92','รายการพิมพ์สลิปซ้ำ', '4');

ALTER TABLE b_item ADD COLUMN item_local_name character varying(255);

ALTER TABLE t_person ADD COLUMN person_prefix_eng character varying(255) default '';
ALTER TABLE t_person ADD COLUMN person_firstname_eng character varying(255) default '';
ALTER TABLE t_person ADD COLUMN person_lastname_eng character varying(255) default '';

ALTER TABLE b_item_auto ADD COLUMN item_auto_holiday_show VARCHAR(255) DEFAULT '0';
ALTER TABLE b_item_auto ADD COLUMN item_auto_holiday_show_start_time VARCHAR(255) DEFAULT '00:00';
ALTER TABLE b_item_auto ADD COLUMN item_auto_holiday_show_stop_time VARCHAR(255) DEFAULT '00:00';
ALTER TABLE b_item_auto ADD COLUMN item_auto_holiday_show_all_time VARCHAR(255) DEFAULT '0';


ALTER TABLE b_item_drug ADD COLUMN indication text;

update b_printing set default_jrxml = 'drug-sticker.jrxml' where b_printing_id = '6';
insert into b_printing values ('9', 'ใบพิมพ์รายการนัด', 'appointment-list.jrxml', '0', '0');

-- update db version
INSERT INTO s_version VALUES ('9701000000085', '85', 'Hospital OS, Community Edition', '3.9.52', '3.33.0', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_52.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.52');
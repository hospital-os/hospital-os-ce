package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;
import java.util.Date;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author tanakrit
 */
public class NewsPewsScore extends Persistent {

    public int score = 0;
    public int hours = 0;
    public int mins = 0;
    public String newspews_type;
    public String user_record_id;
    public Date record_date_time;
    public String user_update_id;
    public Date update_date_time;

}

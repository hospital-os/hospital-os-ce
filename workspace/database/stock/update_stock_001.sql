CREATE TABLE f_hstock_option (
	f_hstock_option_id	  	CHARACTER VARYING(4),
	hstock_option					CHARACTER VARYING(255) NOT NULL,
	CONSTRAINT f_hstock_option_pkey PRIMARY KEY (f_hstock_option_id)
);

INSERT INTO f_hstock_option VALUES ('0101','ยอมให้จำนวนคงคลังติดลบได้');


CREATE TABLE b_hstock (
	b_hstock_id			  CHARACTER VARYING(255),
	hstock_code			  CHARACTER VARYING(255) NOT NULL,
    hstock_name		      TEXT NOT NULL,
	f_hstock_option_ids	  CHARACTER VARYING[] ,
	active                CHARACTER VARYING(1) NOT NULL default '1',
    record_datetime       timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    user_record_id        CHARACTER VARYING(255) NOT NULL,
    update_datetime       timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    user_update_id        CHARACTER VARYING(255) NOT NULL,
	cancel_datetime       timestamp with time zone ,
    user_cancel_id        CHARACTER VARYING(255),
	CONSTRAINT b_hstock_pkey PRIMARY KEY (b_hstock_id),
	CONSTRAINT b_hstock_unique UNIQUE (hstock_code)
);

CREATE TABLE f_hstock_user_authen (
	f_hstock_user_authen_id	  	CHARACTER VARYING(4),
	user_authen					CHARACTER VARYING(255) NOT NULL,
	CONSTRAINT f_hstock_user_authen_pkey PRIMARY KEY (f_hstock_user_authen_id)
);
INSERT INTO f_hstock_user_authen VALUES ('0200','จัดการระบบคลัง');
INSERT INTO f_hstock_user_authen VALUES ('0201','ยกเลิกรายการ');
INSERT INTO f_hstock_user_authen VALUES ('0202','นำกลับมาใช้');
INSERT INTO f_hstock_user_authen VALUES ('0203','แก้ไขรายละเอียดรายการในแต่ละ Lot');
INSERT INTO f_hstock_user_authen VALUES ('0204','ปรับยอด');
INSERT INTO f_hstock_user_authen VALUES ('0205','รับเข้าจากการสั่งซื้อ/อื่น');
INSERT INTO f_hstock_user_authen VALUES ('0300','รายงาน');
INSERT INTO f_hstock_user_authen VALUES ('0301','รายงานการจ่ายออกยาและเวชภัณฑ์');
INSERT INTO f_hstock_user_authen VALUES ('0302','รายงานการรับเข้ายาและเวชภัณฑ์');
INSERT INTO f_hstock_user_authen VALUES ('0303','รายงานยาและเวชภัณฑ์ใกล้หมดอายุ');
INSERT INTO f_hstock_user_authen VALUES ('0304','รายงานสรุปมูลค่าและจำนวนยาและเวชภัณฑ์คงคลัง');
INSERT INTO f_hstock_user_authen VALUES ('0305','รายงานรายการยาและเวชภัณฑ์ที่ไม่เคลื่อนไหว');
INSERT INTO f_hstock_user_authen VALUES ('0306','รายงานการปรับยอดยาและเวชภัณฑ์');
INSERT INTO f_hstock_user_authen VALUES ('0307','รายงานรับเข้า/จ่ายออก/ปรับยอด/คงเหลือ ของยาและเวชภัณฑ์');

CREATE TABLE b_hstock_user (
	b_hstock_user_id	  		CHARACTER VARYING(255),
	b_hstock_id			  		CHARACTER VARYING(255) NOT NULL,
	b_employee_id	      		CHARACTER VARYING(255) NOT NULL,
	f_hstock_user_authen_ids	CHARACTER VARYING[] ,
	active                		CHARACTER VARYING(1) NOT NULL default '1',
    record_datetime       		timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    user_record_id        		CHARACTER VARYING(255) NOT NULL,
    update_datetime       		timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    user_update_id        		CHARACTER VARYING(255) NOT NULL,
	cancel_datetime       		timestamp with time zone,
    user_cancel_id        		CHARACTER VARYING(255),
	CONSTRAINT b_hstock_user_pkey PRIMARY KEY (b_hstock_user_id),
	CONSTRAINT b_hstock_user_unique UNIQUE (b_hstock_id, b_employee_id)
);

CREATE TABLE b_hstock_map_stock (
	b_service_point_id	  CHARACTER VARYING(255),
	b_hstock_id	  		  CHARACTER VARYING(255) NOT NULL,
    update_datetime       timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    user_update_id        CHARACTER VARYING(255) NOT NULL,
	CONSTRAINT b_hstock_map_stock_pkey PRIMARY KEY (b_service_point_id),
	CONSTRAINT b_hstock_map_stock_unique UNIQUE (b_service_point_id,b_hstock_id)
);

CREATE TABLE b_hstock_item (
	b_hstock_item_id	  CHARACTER VARYING(255),
	b_hstock_id			  CHARACTER VARYING(255) NOT NULL,
	b_item_id			  CHARACTER VARYING(255) NOT NULL,
	max_qty				  INTEGER default 0,
	min_qty				  INTEGER default 0,
	critical_qty		  INTEGER default 0,
	active                CHARACTER VARYING(1) NOT NULL default '1',
    record_datetime       timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    user_record_id        CHARACTER VARYING(255) NOT NULL,
    update_datetime       timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    user_update_id        CHARACTER VARYING(255) NOT NULL,
	cancel_datetime       timestamp with time zone ,
    user_cancel_id        CHARACTER VARYING(255),
	CONSTRAINT b_hstock_item_pkey PRIMARY KEY (b_hstock_item_id),
	CONSTRAINT b_hstock_item_unique UNIQUE (b_hstock_id,b_item_id)
);

CREATE TABLE t_hstock_mgnt (
	t_hstock_mgnt_id	  			CHARACTER VARYING(255),
	b_hstock_item_id	  			CHARACTER VARYING(255) NOT NULL,
	invoice_number		  			CHARACTER VARYING(255) NOT NULL,
	receive_date_time	  			timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP ,
	lot_number			  			CHARACTER VARYING(255) NOT NULL,
	produce_date     	  			DATE,
	expire_date		      			DATE,
	b_item_manufacturer_id 			CHARACTER VARYING(255),    
	b_item_distributor_id  			CHARACTER VARYING(255),  
	b_item_drug_uom_big_id			CHARACTER VARYING(255),  
	b_item_drug_uom_small_id		CHARACTER VARYING(255),  
	big_unit_rate		 			INTEGER NOT NULL default 1,
	small_unit_rate		 			INTEGER NOT NULL default 1,
	receive_qty			 			INTEGER NOT NULL default 0,
	free_qty			 			INTEGER NOT NULL default 0,
	inventory_qty		 			INTEGER NOT NULL default 0,
	big_unit_price		 			DOUBLE PRECISION NOT NULL,
	vat					 			INTEGER NOT NULL,	
	vat_action			 			CHARACTER VARYING(1) NOT NULL default '1', -- 1 = รวม VAT , 0 = ยังไม่รวม VAT
	total_before_discount			DOUBLE PRECISION NOT NULL,
	discount			 			DOUBLE PRECISION NOT NULL,
	discount_option		 			CHARACTER VARYING(1) NOT NULL default '1', -- 1 = ส่วนลดเงิน , 2 =  ส่วนลด %
	total_after_discount   			DOUBLE PRECISION NOT NULL,
	avg_free_qty_action  			CHARACTER VARYING(1) NOT NULL default '1', -- 1 = นำของแถมมาคิดราคาทุนเฉลี่ย , 0 =  ไม่นำของแถมมาคิดราคาทุนเฉลี่ย
	big_unit_cost		 			DOUBLE PRECISION NOT NULL,
	small_unit_cost		 			DOUBLE PRECISION NOT NULL,
	reason				 			TEXT,
    record_datetime       			timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    user_record_id        			CHARACTER VARYING(255) NOT NULL,
    update_datetime       			timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    user_update_id        			CHARACTER VARYING(255) NOT NULL,

	CONSTRAINT t_hstock_mgnt_pkey PRIMARY KEY (t_hstock_mgnt_id)

);

CREATE TABLE f_hstock_adjust_type (
	f_hstock_adjust_type_id	  	CHARACTER VARYING(2),
        adjust_type			CHARACTER VARYING(255) NOT NULL,
	adjust_active		  	CHARACTER VARYING(1) NOT NULL default '1',
	import				CHARACTER VARYING(1) NOT NULL , 	
	export				CHARACTER VARYING(1) NOT NULL	,	
	CONSTRAINT f_hstock_adjust_type_pkey PRIMARY KEY (f_hstock_adjust_type_id)
);
INSERT INTO f_hstock_adjust_type VALUES ('0','รอจ่าย','0','0','1');
INSERT INTO f_hstock_adjust_type VALUES ('1','ยอดยกมา','0','1','0');
INSERT INTO f_hstock_adjust_type VALUES ('2','รับเข้าจากการสั่งซื้อ/อื่น ๆ','0','1','0');
INSERT INTO f_hstock_adjust_type VALUES ('3','ปรับเพิ่ม','1','1','0');
INSERT INTO f_hstock_adjust_type VALUES ('4','ยกเลิก','0','1','0');
INSERT INTO f_hstock_adjust_type VALUES ('5','ปรับเพิ่มเหตุผลอื่น ๆ','1','1','0');
INSERT INTO f_hstock_adjust_type VALUES ('6','ขายให้ผู้ป่วย','0','0','1');
INSERT INTO f_hstock_adjust_type VALUES ('7','เสียหาย','1','0','1');
INSERT INTO f_hstock_adjust_type VALUES ('8','สูญหาย','1','0','1');
INSERT INTO f_hstock_adjust_type VALUES ('9','หมดอายุ','1','0','1');
INSERT INTO f_hstock_adjust_type VALUES ('10','ปรับลดเหตุผลอื่น ๆ','1','0','1');
INSERT INTO f_hstock_adjust_type VALUES ('11','ยกเลิกรอจ่าย','0','1','0');


CREATE TABLE t_hstock_card (
	t_hstock_card_id	  CHARACTER VARYING(255),
	t_hstock_mgnt_id	  CHARACTER VARYING(255) NOT NULL,
	previous_qty		  INTEGER NOT NULL,
	previous_qty_lot	  INTEGER NOT NULL,
	qty			  INTEGER NOT NULL,
	update_qty  		  INTEGER NOT NULL,
	update_qty_lot		  INTEGER NOT NULL,	
	small_unit_cost		  DOUBLE PRECISION NOT NULL,
	f_hstock_adjust_type_id	  CHARACTER VARYING(2) NOT NULL,	
	t_order_id		  CHARACTER VARYING(255),
        order_seq                 INTEGER NOT NULL default 1,
	expire_date_adjust        DATE,
	reason_adjust		  TEXT,
        update_datetime           timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP ,
        user_update_id            CHARACTER VARYING(255) NOT NULL,
	CONSTRAINT t_hstock_card_pkey PRIMARY KEY (t_hstock_card_id)

);

-- update 2
--Bug #2755
ALTER TABLE b_hstock_item ALTER COLUMN max_qty TYPE double precision ;
ALTER TABLE b_hstock_item ALTER COLUMN min_qty TYPE double precision ;
ALTER TABLE b_hstock_item ALTER COLUMN critical_qty TYPE double precision ;

ALTER TABLE t_hstock_mgnt ALTER COLUMN receive_qty TYPE double precision ;
ALTER TABLE t_hstock_mgnt ALTER COLUMN free_qty TYPE double precision ;
ALTER TABLE t_hstock_mgnt ALTER COLUMN inventory_qty TYPE double precision ;

ALTER TABLE t_hstock_card ALTER COLUMN previous_qty TYPE double precision ;
ALTER TABLE t_hstock_card ALTER COLUMN previous_qty_lot TYPE double precision ;
ALTER TABLE t_hstock_card ALTER COLUMN qty TYPE double precision ;
ALTER TABLE t_hstock_card ALTER COLUMN update_qty TYPE double precision ;
ALTER TABLE t_hstock_card ALTER COLUMN update_qty_lot TYPE double precision ;

--Bug #2756
--f_order_status_id = '5' จ่าย
CREATE OR REPLACE FUNCTION update_hstock_order_status5() RETURNS trigger AS $$
DECLARE 
         hstock_card_count_order_id boolean :=  (select count(*)
                                                 from t_hstock_card
                                                where t_hstock_card.t_order_id = NEW.t_order_id) > 0 ;
BEGIN
        IF (hstock_card_count_order_id) THEN
        UPDATE t_hstock_card SET 
            f_hstock_adjust_type_id = '6'
            ,user_update_id = NEW.order_staff_dispense
        WHERE t_order_id = NEW.t_order_id;
        END IF;
        RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

CREATE TRIGGER update_hstock_order_status5
AFTER UPDATE ON t_order
FOR EACH ROW 
WHEN (OLD.f_order_status_id ='2' AND NEW.f_order_status_id = '5')
EXECUTE PROCEDURE update_hstock_order_status5();

--f_order_status_id = '3' ยกเลิก
CREATE OR REPLACE FUNCTION insert_t_hstock_card(
  hstock_mgnt_id text
 ,  previous_qty float8
 ,  previous_qty_lot float8
 ,  qty float8
 ,  update_qty float8
 ,  update_qty_lot float8
 ,  small_unit_cost float8
 ,  f_hstock_adjust_type_id text
 ,  t_order_id text
 ,  order_seq integer
 ,  expire_date_adjust date
 ,  reason_adjust text
 ,  update_datetime timestamp
 ,  user_id text   
) RETURNS VOID AS
$$
DECLARE 
       item_inventory_qty float8 := (select sum(t_hstock_mgnt.inventory_qty) as sum_inventory_qty
                                                    from t_hstock_mgnt 
                                                    where t_hstock_mgnt.b_hstock_item_id in (select t_hstock_mgnt.b_hstock_item_id
                                                                                            from t_hstock_mgnt
                                                                                            where  t_hstock_mgnt.t_hstock_mgnt_id = hstock_mgnt_id));
       lot_inventory_qty float8 := (select t_hstock_mgnt.inventory_qty as sum_inventory_qty
                                                    from t_hstock_mgnt 
                                                    where t_hstock_mgnt.t_hstock_mgnt_id = hstock_mgnt_id);
BEGIN
    LOOP
        DECLARE 
            hstock_card_id text := 't02'||(select b_site.b_visit_office_id||substr((random()*10^15)::text,1,10) from b_site);            
        BEGIN
            INSERT INTO t_hstock_card ( t_hstock_card_id
             ,  t_hstock_mgnt_id
             ,  previous_qty
             ,  previous_qty_lot
             ,  qty
             ,  update_qty
             ,  update_qty_lot
             ,  small_unit_cost
             ,  f_hstock_adjust_type_id
             ,  t_order_id
             ,  order_seq
             ,  expire_date_adjust
             ,  reason_adjust
             ,  update_datetime
             ,  user_update_id )
            VALUES ( hstock_card_id
             ,  hstock_mgnt_id
             ,  item_inventory_qty
             ,  lot_inventory_qty
             ,  qty
             ,  item_inventory_qty + qty
             ,  lot_inventory_qty + qty
             ,  small_unit_cost
             ,  f_hstock_adjust_type_id
             ,  t_order_id
             ,  order_seq
             ,  expire_date_adjust
             ,  reason_adjust
             ,  update_datetime
             ,  user_id);
			
            UPDATE t_hstock_mgnt
            SET inventory_qty = inventory_qty + qty
                ,update_datetime = current_timestamp
                ,user_update_id = user_id
            WHERE
                    t_hstock_mgnt.t_hstock_mgnt_id = hstock_mgnt_id;
		
            RETURN;
        EXCEPTION WHEN unique_violation THEN
        END;
      END LOOP;           
END;
$$
LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION update_hstock_order_status3() RETURNS trigger AS $$
DECLARE 
         hstock_card_count_order_id boolean :=  (select count(*)
                                                                    from t_hstock_card
                                                                    where t_hstock_card.t_order_id = NEW.t_order_id) > 0 ;
        views RECORD;
BEGIN
    IF (hstock_card_count_order_id AND NEW.f_order_status_id = '3') THEN
    FOR views IN 
    (select
             t_hstock_card.t_hstock_mgnt_id as hstock_mgnt_id
             , null::float8 as previous_qty
             , null::float8 as previous_qty_lot
             , t_hstock_card.qty as qty
             , null::float8 as update_qty
             , null::float8 as update_qty_lot
             , t_hstock_card.small_unit_cost as small_unit_cost
             , case when OLD.f_order_status_id = '5' AND NEW.f_order_status_id = '3'
                            then '4' else '11' end as f_hstock_adjust_type_id 
             , t_hstock_card.t_order_id as t_order_id
             , t_hstock_card.order_seq as order_seq
             , null::date as expire_date_adjust
             , '' as reason_adjust
             , current_timestamp as update_datetime
             , NEW.order_staff_discontinue as user_update_id    
    from t_hstock_card inner join t_hstock_mgnt on t_hstock_card.t_hstock_mgnt_id = t_hstock_mgnt.t_hstock_mgnt_id
    where
            t_hstock_card.t_order_id = NEW.t_order_id
    order by
            order_seq asc)
    LOOP
        EXECUTE insert_t_hstock_card( views.hstock_mgnt_id::text
             , views.previous_qty::float8
             , views.previous_qty_lot::float8
             , views.qty::float8
             , views.update_qty::float8
             , views.update_qty_lot::float8
             , views.small_unit_cost::float8
             , views.f_hstock_adjust_type_id::text
             , views.t_order_id::text
             , views.order_seq::integer
             , views.expire_date_adjust::date
             , views.reason_adjust::text
             , views.update_datetime::timestamp
             , views.user_update_id::text);
        END LOOP;
        END IF;
        RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';


CREATE TRIGGER update_hstock_order_status3
AFTER UPDATE ON t_order
FOR EACH ROW 
WHEN (NEW.f_order_status_id = '3')
EXECUTE PROCEDURE update_hstock_order_status3();

-- update 3
DROP TRIGGER update_hstock_order_status3 ON t_order;

CREATE TRIGGER update_hstock_order_status3
AFTER UPDATE ON t_order
FOR EACH ROW 
WHEN (OLD.f_order_status_id != '3' AND NEW.f_order_status_id = '3')
EXECUTE PROCEDURE update_hstock_order_status3();

UPDATE t_hstock_card SET t_order_id = null WHERE t_order_id = '';

--t_hstock_card set unique key hstock_card_order_status
UPDATE t_hstock_card SET t_order_id = 'BUG'||lpad(q.seq::text,2,'0')||q.t_order_id
FROM 
(select 
        t_hstock_card.t_order_id
        ,t_hstock_card.update_datetime
        ,t_hstock_card.t_hstock_card_id
        ,row_number() over (partition by t_hstock_card.t_order_id order by t_hstock_card.update_datetime asc ,t_hstock_card.t_hstock_card_id asc) as seq
from t_hstock_card
where
        t_hstock_card.t_order_id in (
                select t_order_id 
                from t_hstock_card 
                where
                        t_hstock_card.t_order_id <> ''
                        or t_hstock_card.t_order_id is not null
                group by
                t_order_id 
                        ,t_hstock_mgnt_id
                        ,f_hstock_adjust_type_id
                having count(*) >1)) as q
WHERE 
        t_hstock_card.t_hstock_card_id = q.t_hstock_card_id
        and q.seq != 1;		
ALTER TABLE t_hstock_card ADD CONSTRAINT hstock_card_order_status_unique UNIQUE (t_hstock_mgnt_id,t_order_id,f_hstock_adjust_type_id);

-- update 4
CREATE OR REPLACE FUNCTION "public"."update_hstock_order_status5" () RETURNS trigger AS
$BODY$
DECLARE 
         hstock_card_count_order_id boolean :=  (select count(*)
                                                 from t_hstock_card
                                                where t_hstock_card.t_order_id = NEW.t_order_id) > 0 ;
BEGIN
        IF (hstock_card_count_order_id) THEN
        UPDATE t_hstock_card SET 
            f_hstock_adjust_type_id = '6'
            ,user_update_id = NEW.order_staff_dispense
        WHERE t_order_id = NEW.t_order_id
             and f_hstock_adjust_type_id = '0';
        END IF;
        RETURN NEW;
END;
$BODY$
LANGUAGE 'plpgsql';


CREATE OR REPLACE FUNCTION "public"."update_hstock_order_status3" () RETURNS trigger AS
$BODY$
DECLARE 
         hstock_card_count_order_id boolean :=  (select count(*)
                                                                    from t_hstock_card
                                                                    where t_hstock_card.t_order_id = NEW.t_order_id) > 0 ;
        views RECORD;
BEGIN
    IF (hstock_card_count_order_id AND NEW.f_order_status_id = '3') THEN
    FOR views IN 
    (select
             t_hstock_card.t_hstock_mgnt_id as hstock_mgnt_id
             , null::float8 as previous_qty
             , null::float8 as previous_qty_lot
             , t_hstock_card.qty as qty
             , null::float8 as update_qty
             , null::float8 as update_qty_lot
             , t_hstock_card.small_unit_cost as small_unit_cost
             , case when OLD.f_order_status_id = '5' AND NEW.f_order_status_id = '3'
                            then '4' else '11' end as f_hstock_adjust_type_id 
             , t_hstock_card.t_order_id as t_order_id
             , t_hstock_card.order_seq as order_seq
             , null::date as expire_date_adjust
             , '' as reason_adjust
             , current_timestamp as update_datetime
             , NEW.order_staff_discontinue as user_update_id    
    from t_hstock_card inner join t_hstock_mgnt on t_hstock_card.t_hstock_mgnt_id = t_hstock_mgnt.t_hstock_mgnt_id
    where
            t_hstock_card.t_order_id = NEW.t_order_id
    order by
            order_seq asc)
    LOOP
        EXECUTE insert_t_hstock_card( views.hstock_mgnt_id::text
             , views.previous_qty::float8
             , views.previous_qty_lot::float8
             , views.qty::float8
             , views.update_qty::float8
             , views.update_qty_lot::float8
             , views.small_unit_cost::float8
             , views.f_hstock_adjust_type_id::text
             , views.t_order_id::text
             , views.order_seq::integer
             , views.expire_date_adjust::date
             , views.reason_adjust::text
             , views.update_datetime::timestamp
             , views.user_update_id::text);
        END LOOP;
        END IF;
        RETURN NEW;
END;
$BODY$
LANGUAGE 'plpgsql';

INSERT INTO f_hstock_user_authen VALUES('0206','ขอเบิก'),('0207','จ่ายออกจากการเบิก'),('0208','โอน'),('0209','รับเข้าระหว่างคลัง');

DELETE FROM b_hstock_item;
DELETE FROM t_hstock_mgnt;
DELETE FROM t_hstock_card;


ALTER TABLE t_hstock_card ADD CONSTRAINT t_hstock_card_mgnt_id_fk FOREIGN KEY(t_hstock_mgnt_id)
	REFERENCES t_hstock_mgnt (t_hstock_mgnt_id) ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE t_hstock_card ADD CONSTRAINT t_hstock_card_order_id_fk FOREIGN KEY(t_order_id)
	REFERENCES t_order (t_order_id) ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE t_hstock_card ADD CONSTRAINT t_hstock_card_adjust_type_fk FOREIGN KEY(f_hstock_adjust_type_id)
	REFERENCES f_hstock_adjust_type (f_hstock_adjust_type_id) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE t_order ADD COLUMN b_hstock_item_id CHARACTER VARYING(50);

ALTER TABLE public.b_hstock_map_stock
  DROP CONSTRAINT b_hstock_map_stock_pkey;

ALTER TABLE public.b_hstock_map_stock
  ADD CONSTRAINT b_hstock_map_stock_pkey PRIMARY KEY (b_service_point_id, b_hstock_id);

-- update 5
INSERT INTO f_hstock_user_authen VALUES('0308','รายงานยาคงเหลือ'),('0309','รายงานจำนวนยาคงคลัง');


CREATE TABLE b_hstock_item_price
(	
    b_hstock_item_price_id 		CHARACTER VARYING(50) NOT NULL,
    b_hstock_item_id			CHARACTER VARYING(50) NOT NULL,
    b_contract_plans_id         CHARACTER VARYING(50),
    item_price					float8 NOT NULL default 0,
    item_price_cost             float8 NOT NULL default 0,
    item_price_cgd              float8 NOT NULL default 0,
    active						CHARACTER VARYING(1) NOT NULL  default '1',
    record_datetime				timestamp with time zone NOT NULL DEFAULT current_timestamp,
    user_record_id				CHARACTER VARYING(50) NOT NULL,
    cancel_datetime				timestamp with time zone,
    user_cancel_id				CHARACTER VARYING(50),

    CONSTRAINT b_hstock_item_price_pk PRIMARY KEY (b_hstock_item_price_id),  
    CONSTRAINT b_hstock_item_price_item_id_fk FOREIGN KEY (b_hstock_item_id) 
        REFERENCES b_hstock_item (b_hstock_item_id) ON UPDATE NO ACTION ON DELETE NO ACTION,  
    CONSTRAINT b_hstock_item_price_contract_plans_fk FOREIGN KEY (b_contract_plans_id) 
        REFERENCES b_contract_plans (b_contract_plans_id) ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE OR REPLACE FUNCTION public.get_item_price(
    IN item_id text,
    IN contract_plans_id text)
  RETURNS TABLE(item_price numeric, item_price_cost numeric, item_price_cgd numeric) AS
$BODY$
DECLARE 
BEGIN
        RETURN QUERY 
         select
                 b_item_price_view_active.item_price::numeric    
                ,b_item_price_view_active.item_price_cost::numeric 
                ,b_item_price_view_active.item_price_cgd::numeric 
         from (select
                        b_item_price.b_item_id
                        ,b_contract_plans.b_contract_plans_id as payment             
                        ,row_number() over (partition by 
                            b_item_price.b_item_id 
                            ,b_contract_plans.b_contract_plans_id     
                        order by text_to_timestamp(b_item_price.item_price_active_date) desc
                            ,b_item_price.record_datetime desc 
                            ,b_item_price.item_price desc) as rank
                        ,b_item_price.item_price     
                        ,b_item_price.item_price_cost
                        ,b_item_price.item_price_cgd
                from b_item_price left join b_contract_plans on b_item_price.item_price_number =  b_contract_plans.b_contract_plans_id 
                where 
                        b_item_id = item_id
union 
select
          b_hstock_item_price.b_hstock_item_id
          ,b_contract_plans.b_contract_plans_id as payment             
          ,row_number() over (partition by 
                             b_hstock_item_price.b_hstock_item_id 
                            ,b_contract_plans.b_contract_plans_id     
                        order by b_hstock_item_price.record_datetime desc
                            ,b_hstock_item_price.record_datetime desc 
                            ,b_hstock_item_price.item_price desc) as rank
            ,b_hstock_item_price.item_price     
            ,b_hstock_item_price.item_price_cost
            ,b_hstock_item_price.item_price_cgd
from b_hstock_item_price left join b_contract_plans on b_hstock_item_price.b_contract_plans_id =  b_contract_plans.b_contract_plans_id
where
        b_hstock_item_price.b_hstock_item_id = item_id
        and b_hstock_item_price.active = '1') as b_item_price_view_active 
         where
                 b_item_price_view_active.b_item_id = item_id         
                and b_item_price_view_active.payment = contract_plans_id
                and b_item_price_view_active.rank = 1;
        IF NOT FOUND THEN
        RETURN QUERY
        select
                 b_item_price_view_active.item_price::numeric    
                ,b_item_price_view_active.item_price_cost::numeric 
                ,b_item_price_view_active.item_price_cgd::numeric   
         from (select
                        b_item_price.b_item_id
                        ,b_contract_plans.b_contract_plans_id as payment             
                        ,row_number() over (partition by 
                            b_item_price.b_item_id 
                            ,b_contract_plans.b_contract_plans_id     
                        order by text_to_timestamp(b_item_price.item_price_active_date) desc
                            ,b_item_price.record_datetime desc 
                            ,b_item_price.item_price desc) as rank
                        ,b_item_price.item_price     
                        ,b_item_price.item_price_cost
                        ,b_item_price.item_price_cgd
                from b_item_price left join b_contract_plans on b_item_price.item_price_number =  b_contract_plans.b_contract_plans_id 
                where 
                        b_item_id = item_id
union 
select
          b_hstock_item_price.b_hstock_item_id
          ,b_contract_plans.b_contract_plans_id as payment             
          ,row_number() over (partition by 
                             b_hstock_item_price.b_hstock_item_id 
                            ,b_contract_plans.b_contract_plans_id     
                        order by b_hstock_item_price.record_datetime desc
                            ,b_hstock_item_price.record_datetime desc 
                            ,b_hstock_item_price.item_price desc) as rank
            ,b_hstock_item_price.item_price     
            ,b_hstock_item_price.item_price_cost
            ,b_hstock_item_price.item_price_cgd
from b_hstock_item_price left join b_contract_plans on b_hstock_item_price.b_contract_plans_id =  b_contract_plans.b_contract_plans_id
where
        b_hstock_item_price.b_hstock_item_id = item_id
        and b_hstock_item_price.active = '1') as b_item_price_view_active        
         where
                b_item_price_view_active.b_item_id = item_id
                and b_item_price_view_active.payment is null
                and b_item_price_view_active.rank = 1;  
        END IF;  
        IF NOT FOUND THEN
            RETURN QUERY (select 0::numeric , 0::numeric ,0::numeric); 
        END IF;              
END;$BODY$
  LANGUAGE 'plpgsql';



CREATE OR REPLACE FUNCTION order_charge_overtime() RETURNS trigger AS $$
DECLARE 
        visit_contract_plans_id text := (select 
                                            t_visit_payment.b_contract_plans_id 
                                         from t_visit_payment
                                         where
                                            t_visit_payment.visit_payment_active = '1'
                                            and t_visit_payment.visit_payment_priority = '0'
                                            and t_visit_payment.t_visit_id = NEW.t_visit_id );       
          option_charge_overtime boolean := (select 
                                                option_detail_name ='1'
                                            from b_option_detail
                                            where
                                                b_option_detail_id = 'charge_overtime'); 
          visit_time_type_overtime boolean := (select t_visit.f_visit_time_type_id = '2'
                                            from t_visit 
                                            where t_visit.t_visit_id = NEW.t_visit_id); 
       new_order_price_cgd numeric := (select item_price_cgd from get_item_price((case when NEW.b_hstock_item_id is not null then NEW.b_hstock_item_id else NEW.b_item_id end),visit_contract_plans_id));
          get_charge_overtime_percent integer := (case when visit_time_type_overtime and option_charge_overtime then (select get_charge_overtime_percent(NEW.t_visit_id,NEW.b_item_id)) else 0 end);
BEGIN
        UPDATE t_order SET order_price_cgd = new_order_price_cgd
    ,charge_overtime_percent = get_charge_overtime_percent
  WHERE 
    t_order.t_order_id = NEW.t_order_id;
        RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

-- update 6
INSERT INTO f_hstock_adjust_type VALUES ('12','จ่ายออกจากการเบิก','1','0','1')
,('13','โอนยาระหว่างคลัง','1','0','1')
,('14','รอรับเข้าจากเบิก','1','1','0')
,('15','รอรับเข้าจากโอน','1','1','0')
,('16','รับเข้าจากเบิก','1','1','0')
,('17','รับเข้าจากโอน','1','1','0')
,('18','ปฏิเสธรับเข้าจากเบิก','1','0','1')
,('19','ปฏิเสธรับเข้าจากโอน','1','0','1');


CREATE TABLE f_hstock_transfer_method
(	
	f_hstock_transfer_method_id 			CHARACTER VARYING(4) NOT NULL,
	description 						text NOT NULL,	
	CONSTRAINT f_hstock_transfer_method_pk PRIMARY KEY (f_hstock_transfer_method_id)
);

INSERT INTO f_hstock_transfer_method VALUES ('1','ขอเบิก'),('2','โอนยาระหว่างคลัง');


CREATE TABLE f_hstock_transfer_status
(	
	f_hstock_transfer_status_id 			CHARACTER VARYING(4) NOT NULL,
	description 						text NOT NULL,	
	CONSTRAINT f_hstock_transfer_status_pk PRIMARY KEY (f_hstock_transfer_status_id)
);

INSERT INTO f_hstock_transfer_status VALUES ('1','ขอเบิก'),('2','จ่ายออกจากการเบิก'),('3','โอนระหว่างคลัง'),('4','รับจากการขอเบิก'),('5','รับจากการโอนระหว่างคลัง'),('6','ยกเลิก'),('7','ปฏิเสธรับเข้าจากเบิก'),('8','ปฏิเสธรับเข้าจากโอน');

CREATE TABLE t_hstock_transfer
(	
	t_hstock_transfer_id 			CHARACTER VARYING(50) NOT NULL,
	recv_hstock_id					CHARACTER VARYING(50) NOT NULL, -- b_hstock_id
	disp_hstock_id 					CHARACTER VARYING(50) NOT NULL, -- b_hstock_id
	transfer_number					text NOT NULL,
	automated_stock					CHARACTER VARYING(1) NOT NULL  default '0', 	--1 รับเข้าอัตโนมัติ
	f_hstock_transfer_method_id		CHARACTER VARYING(4) NOT NULL,
	f_hstock_transfer_status_id		CHARACTER VARYING(4) NOT NULL,
	request_datetime 				timestamp with time zone NOT NULL DEFAULT current_timestamp,
	record_datetime					timestamp with time zone NOT NULL DEFAULT current_timestamp,
	user_record_id					CHARACTER VARYING(50) NOT NULL,
	update_datetime					timestamp with time zone NOT NULL DEFAULT current_timestamp,
	user_update_id					CHARACTER VARYING(50) NOT NULL,
	cancel_datetime					timestamp with time zone,
	user_cancel_id					CHARACTER VARYING(50),
	
	CONSTRAINT t_hstock_transfer_pk PRIMARY KEY (t_hstock_transfer_id),  
	CONSTRAINT t_hstock_transfer_recv_hstock_fk FOREIGN KEY (recv_hstock_id) 
		REFERENCES b_hstock (b_hstock_id) ON UPDATE NO ACTION ON DELETE NO ACTION,  
	CONSTRAINT t_hstock_transfer_disp_hstock_fk FOREIGN KEY (disp_hstock_id) 
		REFERENCES b_hstock (b_hstock_id) ON UPDATE NO ACTION ON DELETE NO ACTION,  
	CONSTRAINT t_hstock_transfer_hstock_method_fk FOREIGN KEY (f_hstock_transfer_method_id) 
		REFERENCES f_hstock_transfer_method (f_hstock_transfer_method_id) ON UPDATE NO ACTION ON DELETE NO ACTION,  
	CONSTRAINT t_hstock_transfer_hstock_status_fk FOREIGN KEY (f_hstock_transfer_status_id) 
		REFERENCES f_hstock_transfer_status (f_hstock_transfer_status_id) ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE SEQUENCE transfer_number_request;
CREATE SEQUENCE transfer_number_move;

CREATE OR REPLACE FUNCTION t_hstock_generate_transfer_number() RETURNS trigger AS $$
DECLARE 
BEGIN
		
        IF (select max(substr(transfer_number,2,4)) from t_hstock_transfer) = (EXTRACT(YEAR  FROM current_date) + 543)::text THEN
            IF (NEW.f_hstock_transfer_method_id = '1') THEN
					NEW.transfer_number := 'R'||(EXTRACT(YEAR  FROM current_date) + 543)::text||TO_CHAR(current_date,'-MM-')|| lpad( nextval('transfer_number_request'::regclass)::text,5,'0');        
			ELSEIF (NEW.f_hstock_transfer_method_id = '2') THEN    
					NEW.transfer_number := 'M'||(EXTRACT(YEAR  FROM current_date) + 543)::text||TO_CHAR(current_date,'-MM-')|| lpad( nextval('transfer_number_move'::regclass)::text,5,'0');
			END IF;
		ELSE
            ALTER SEQUENCE transfer_number_request RESTART WITH 1;
			ALTER SEQUENCE transfer_number_move RESTART WITH 1;
            IF (NEW.f_hstock_transfer_method_id = '1') THEN
					NEW.transfer_number := 'R'||(EXTRACT(YEAR  FROM current_date) + 543)::text||TO_CHAR(current_date,'-MM-')|| lpad( nextval('transfer_number_request'::regclass)::text,5,'0');        
			ELSEIF (NEW.f_hstock_transfer_method_id = '2') THEN    
					NEW.transfer_number := 'M'||(EXTRACT(YEAR  FROM current_date) + 543)::text||TO_CHAR(current_date,'-MM-')|| lpad( nextval('transfer_number_move'::regclass)::text,5,'0');
			END IF;
		END IF;
        RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

CREATE TRIGGER t_hstock_generate_transfer_number
BEFORE INSERT ON t_hstock_transfer
FOR EACH ROW 
EXECUTE PROCEDURE t_hstock_generate_transfer_number();


CREATE TABLE t_hstock_transfer_item
(	
	t_hstock_transfer_item_id 		CHARACTER VARYING(50) NOT NULL,
	t_hstock_transfer_id			CHARACTER VARYING(50) NOT NULL,
	b_item_id						CHARACTER VARYING(50) NOT NULL,		
	b_hstock_item_id				CHARACTER VARYING(50) NOT NULL,	
	req_qty							float8 NOT NULL default 0,
	partial_pay						CHARACTER VARYING(1) NOT NULL  default '0', 	--1 จ่ายบางส่วน
	record_datetime					timestamp with time zone NOT NULL DEFAULT current_timestamp,
	user_record_id					CHARACTER VARYING(50) NOT NULL,
	update_datetime					timestamp with time zone NOT NULL DEFAULT current_timestamp,
	user_update_id					CHARACTER VARYING(50) NOT NULL,
	
	CONSTRAINT t_hstock_transfer_item_pk PRIMARY KEY (t_hstock_transfer_item_id),  
	CONSTRAINT t_hstock_transfer_item_id_fk FOREIGN KEY (b_hstock_item_id) 
		REFERENCES b_hstock_item (b_hstock_item_id) ON UPDATE NO ACTION ON DELETE NO ACTION,  
	CONSTRAINT t_hstock_transfer_item_transfer_id_fk FOREIGN KEY (t_hstock_transfer_id) 
		REFERENCES t_hstock_transfer (t_hstock_transfer_id) ON UPDATE NO ACTION ON DELETE NO ACTION
);

ALTER TABLE t_hstock_card ADD COLUMN t_hstock_transfer_item_id CHARACTER VARYING(50);
ALTER TABLE t_hstock_card ADD CONSTRAINT t_hstock_card_transfer_item_id_fk FOREIGN KEY(t_hstock_transfer_item_id)
	REFERENCES t_hstock_transfer_item (t_hstock_transfer_item_id) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE t_hstock_mgnt ADD CONSTRAINT t_hstock_mgnt_item_id_fk FOREIGN KEY(b_hstock_item_id)
	REFERENCES b_hstock_item (b_hstock_item_id) ON UPDATE NO ACTION ON DELETE NO ACTION;



--รายการยาชุด
CREATE TABLE b_hstock_item_set
(	
	b_hstock_item_set_id 			CHARACTER VARYING(50) NOT NULL,
	item_set_name					CHARACTER VARYING(50) NOT NULL,
	user_set_id						CHARACTER VARYING(50) NOT NULL,
	active							CHARACTER VARYING(1) NOT NULL  default '1', 	
	record_datetime					timestamp with time zone NOT NULL DEFAULT current_timestamp,
	user_record_id					CHARACTER VARYING(50) NOT NULL,
	update_datetime					timestamp with time zone NOT NULL DEFAULT current_timestamp,
	user_update_id					CHARACTER VARYING(50) NOT NULL,
	
	CONSTRAINT b_hstock_item_set_pk PRIMARY KEY (b_hstock_item_set_id),  
	CONSTRAINT b_hstock_item_set_user_set_fk FOREIGN KEY (user_set_id) 
		REFERENCES b_employee (b_employee_id) ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE b_hstock_item_set_detail
(	
	b_hstock_item_set_detail_id 	CHARACTER VARYING(50) NOT NULL,
	b_hstock_item_set_id			CHARACTER VARYING(50) NOT NULL,
	b_item_id						CHARACTER VARYING(50) NOT NULL,
	f_item_group_id					CHARACTER VARYING(50) NOT NULL,
	b_hstock_item_id				CHARACTER VARYING(50),	
	item_dose_use					float8,
	b_item_drug_uom_id_use			CHARACTER VARYING(50),
	item_qty_purch 					float8,	
	b_item_drug_uom_id_purch		CHARACTER VARYING(50),	
	b_item_drug_instruction_id		CHARACTER VARYING(50),
	b_item_drug_frequency_id		CHARACTER VARYING(50),
	item_special_prescription		CHARACTER VARYING(1),
	item_special_prescription_text	text,
	b_item_drug_caution_id			CHARACTER VARYING(50),
	b_item_drug_description_id		CHARACTER VARYING(50),
	printable						CHARACTER VARYING(1),
	record_datetime					timestamp with time zone NOT NULL DEFAULT current_timestamp,
	user_record_id					CHARACTER VARYING(50) NOT NULL,
	update_datetime					timestamp with time zone NOT NULL DEFAULT current_timestamp,
	user_update_id					CHARACTER VARYING(50) NOT NULL,
	
	CONSTRAINT b_hstock_item_set_detail_pk PRIMARY KEY (b_hstock_item_set_detail_id),  
	CONSTRAINT b_hstock_item_set_detail_set_id_fk FOREIGN KEY (b_hstock_item_set_id) 
		REFERENCES b_hstock_item_set (b_hstock_item_set_id) ON UPDATE NO ACTION ON DELETE NO ACTION,  
	CONSTRAINT b_hstock_item_set_detail_item_id_fk FOREIGN KEY (b_item_id) 
		REFERENCES b_item (b_item_id) ON UPDATE NO ACTION ON DELETE NO ACTION,  
	CONSTRAINT b_hstock_item_set_detail_hstock_item_id_fk FOREIGN KEY (b_hstock_item_id) 
		REFERENCES b_hstock_item (b_hstock_item_id) ON UPDATE NO ACTION ON DELETE NO ACTION,  
	CONSTRAINT b_hstock_item_set_detail_item_group_fk FOREIGN KEY (f_item_group_id) 
		REFERENCES f_item_group (f_item_group_id) ON UPDATE NO ACTION ON DELETE NO ACTION
);

ALTER TABLE t_patient_appointment_order ADD COLUMN b_hstock_item_id CHARACTER VARYING(50);

-- update 7
--f_order_status_id = '5' จ่าย
CREATE OR REPLACE FUNCTION update_hstock_order_status5() RETURNS trigger AS $$
DECLARE 
         hstock_card_count_order_id boolean :=  (select count(*)
                                                 from t_hstock_card
                                                where t_hstock_card.t_order_id = NEW.t_order_id) > 0 ;
BEGIN
        IF (hstock_card_count_order_id) THEN
        UPDATE t_hstock_card SET 
            f_hstock_adjust_type_id = '6'
            ,user_update_id = NEW.order_staff_dispense
            ,update_datetime = current_timestamp
        WHERE t_order_id = NEW.t_order_id;
        END IF;
        RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

ALTER TABLE public.s_stock_version RENAME TO s_stock_version_backup;
ALTER TABLE public.s_stock_version_backup RENAME CONSTRAINT s_stock_version_pk TO s_stock_version_pk_bk;
ALTER TABLE public.s_stock_version_backup RENAME CONSTRAINT s_stock_version_uk TO s_stock_version_uk_bk;


-- create s_stock_version
CREATE TABLE s_stock_version
(
   id			serial,
   version_app 		text NOT NULL, 
   version_db 		text NOT NULL, 
   description		text NOT NULL, 
   update_datetime 	timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
   CONSTRAINT s_stock_version_pk PRIMARY KEY (id),
   CONSTRAINT s_stock_version_uk UNIQUE (version_db)
);
INSERT INTO s_stock_version (version_app, version_db, description)VALUES ('2.0.0', '2.0.0', 'Stock Module');

INSERT INTO s_script_update_log values ('Stock_Module','update_stock_001.sql',(select current_date) || ','|| (select current_time),'Initialize Stock Module');


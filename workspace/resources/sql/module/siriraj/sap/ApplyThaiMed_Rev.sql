select  distinct
        'AR' as record_type
        , '01' as data_type
        , 'S001' as company_code
        , t_billing_receipt.billing_receipt_number as receipt_no
        , case when t_billing_receipt.billing_receipt_active = '1' then 'A' else 'C' end as status
        , case when length(t_patient.patient_pid) =13
                    then t_patient.patient_pid
                    else '' end as "Id Card No"
        , '' as an
        ,  t_visit.visit_vn as vn
        , substr(t_visit.visit_begin_visit_time,9,2)||substr(t_visit.visit_begin_visit_time,6,2)||substr(t_visit.visit_begin_visit_time,1,4)  as service_date
        , substr(t_billing_receipt.billing_receipt_date_time,9,2)||substr(t_billing_receipt.billing_receipt_date_time,6,2)||substr(t_billing_receipt.billing_receipt_date_time,1,4) as posting_date
        , 'OPD'  as patient_type
        , t_billing_invoice_item.patient_group as patient_group

        , '' as organization
        , '' as debtor
        , '' as receipt_type
        , '' as receipt_amount
        , '' as discount_code
        , '' as bank_code
        , '' as cheque_or_creditcard_no 
        , '' as cheque_date

        , f_patient_prefix.patient_prefix_description||t_person.person_firstname||'  '||t_person.person_lastname as cashier_code_or_name
        , '' as description
        , '' as requesting_unit
        , '' as performing_unit
        , t_billing_invoice_item.service_group as service_group
        , (1::decimal(6,3))::text  as amount_of_treatment
        , (0::decimal(6,3))::text  as amount_of_cost
        , 'PCS' as unit_of_treatment
        , (t_billing_invoice_item.billing_invoice_item_payer_share::decimal(10,2))::text as total_amount_of_treatment

from
        t_billing_receipt inner join t_visit on t_billing_receipt.t_visit_id = t_visit.t_visit_id
        inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
        inner join (select
                                    t_billing_receipt.billing_receipt_number as billing_receipt_number
                                    ,case when b_item_subgroup.f_item_group_id = '5'
                                                then '0001'
                                                else '0002'
                                             end  as service_group
                                    ,case when b_srj_contract_plans.contract_plans_number = '16' then '03'
                                                when b_srj_contract_plans.contract_plans_number = '18' then '04'
                                                    end as patient_group
                                    ,sum(t_billing_invoice_item.billing_invoice_item_payer_share) as billing_invoice_item_payer_share
                            from 
                                    t_billing_receipt inner join t_billing_receipt_item on t_billing_receipt.t_billing_receipt_id = t_billing_receipt_item.t_billing_receipt_id
                                    inner join t_billing_invoice_item on t_billing_receipt_item.t_billing_invoice_item_id = t_billing_invoice_item.t_billing_invoice_item_id
                                    left join b_item on t_billing_invoice_item.b_item_id = b_item.b_item_id
                                    left join b_item_subgroup on b_item.b_item_subgroup_id = b_item_subgroup.b_item_subgroup_id
                                    inner join t_visit_payment on t_billing_receipt_item.t_payment_id = t_visit_payment.t_visit_payment_id
                                                    and t_visit_payment.visit_payment_active = '1'
                                    inner join b_contract_plans on t_visit_payment.b_contract_plans_id =  b_contract_plans.b_contract_plans_id
                                    inner join b_siriraj_direct_draw_map_plan on b_contract_plans.b_contract_plans_id = b_siriraj_direct_draw_map_plan.b_contract_plans_id 
                                    inner join b_map_hos_siriraj_plan on b_siriraj_direct_draw_map_plan.b_contract_plans_id = b_map_hos_siriraj_plan.b_contract_plans_id
                                    inner join b_srj_contract_plans on b_map_hos_siriraj_plan.b_srj_contract_plans_id = b_srj_contract_plans.b_srj_contract_plans_id
                            where
                                    t_billing_invoice_item.billing_invoice_item_payer_share > 0
                            group by
                                    billing_receipt_number
                                    ,service_group
                                    ,patient_group) as t_billing_invoice_item  

        on t_billing_receipt.billing_receipt_number = t_billing_invoice_item.billing_receipt_number 
            

        left join b_employee on t_billing_receipt.billing_receipt_staff_record = b_employee.b_employee_id
        left join t_person on b_employee.t_person_id = t_person.t_person_id
        left join f_patient_prefix on t_person.f_prefix_id = f_patient_prefix.f_patient_prefix_id

where       
        substr(t_billing_receipt.billing_receipt_date_time,1,10) = ':exportdate'
     

order by
        receipt_no asc
        ,record_type desc    

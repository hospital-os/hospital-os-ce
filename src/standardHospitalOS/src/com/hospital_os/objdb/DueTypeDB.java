/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.DueType;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.ComboFix;
import com.hospital_os.utility.Gutil;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 *
 * @author Somprasong
 */
public class DueTypeDB {

    private final ConnectionInf theConnectionInf;
    private final String idtable = "983";

    public DueTypeDB(ConnectionInf theConnectionInf) {
        this.theConnectionInf = theConnectionInf;
    }

    public int insert(DueType o) throws Exception {
        String sql = "INSERT INTO b_due_type (b_due_type_id, due_code, "
                + "due_name, active, user_record_id, record_date_time, user_update_id, update_date_time) "
                + "VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')";
        return theConnectionInf.eUpdate(String.format(sql,
                o.getGenID(idtable),
                Gutil.CheckReservedWords(o.due_code),
                Gutil.CheckReservedWords(o.due_name),
                o.active,
                o.user_record_id,
                o.record_date_time,
                o.user_update_id,
                o.update_date_time));
    }

    public int updateActive(DueType o) throws Exception {
        String sql = "UPDATE b_due_type SET active = '%s', user_update_id = '%s', update_date_time = '%s' "
                + "WHERE b_due_type_id = '%s'";
        return theConnectionInf.eUpdate(String.format(sql,
                o.active,
                o.user_update_id,
                o.update_date_time,
                o.getObjectId()));
    }

    public DueType selectById(String id) throws Exception {
        String sql = "select * from b_due_type where b_due_type_id = '%s'";
        List<DueType> list = eQuery(String.format(sql, id));
        return (DueType) (list.isEmpty() ? null : list.get(0));
    }

    public List<DueType> listAll() throws Exception {
        return this.listAll(null, "1");
    }

    public List<DueType> listAll(String keyword, String active) throws Exception {
        String sql = "select * from b_due_type where active = '%s' ";
        if (keyword != null && !keyword.isEmpty()) {
            sql += "and (UPPER(b_due_type.due_code) like UPPER('%s') "
                    + "or UPPER(b_due_type.due_name) like UPPER('%s'))";
        }
        sql += " order by b_due_type.due_name";
        List<DueType> list = eQuery(keyword != null && !keyword.isEmpty() ? String.format(sql, active, "%" + Gutil.CheckReservedWords(keyword) + "%", "%" + Gutil.CheckReservedWords(keyword) + "%") : String.format(sql, active));
        return list;
    }

    public List<DueType> eQuery(String sql) throws Exception {
        List<DueType> list = new ArrayList<DueType>();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        while (rs.next()) {
            DueType p = new DueType();
            p.setObjectId(rs.getString("b_due_type_id"));
            p.due_code = rs.getString("due_code");
            p.due_name = rs.getString("due_name");
            p.active = rs.getString("active");
            p.user_record_id = rs.getString("user_record_id");
            p.record_date_time = rs.getString("record_date_time");
            p.user_update_id = rs.getString("user_update_id");
            p.update_date_time = rs.getString("update_date_time");
            list.add(p);
        }
        rs.close();
        return list;
    }

    @SuppressWarnings("UseOfObsoleteCollectionType")
    public Vector<ComboFix> getComboboxDatasources() throws Exception {
        Vector<ComboFix> list = new Vector<ComboFix>();
        List<DueType> listAll = listAll();
        for (DueType obj : listAll) {
            list.add(new ComboFix(obj.getObjectId(), obj.due_name));
        }
        return list;
    }
}

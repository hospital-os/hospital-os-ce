/*
 * To change this template;public String choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class PatientContact extends Persistent {

    public String t_patient_id;
    public String patient_contact_office_name;
    public String patient_contact_office_tel;
    public String patient_contact_email;
    public String patient_contact_firstname;
    public String patient_contact_lastname;
    public String f_patient_relation_id;
    public String patient_contact_sex_id;
    public String patient_contact_house;
    public String patient_contact_moo;
    public String patient_contact_road;
    public String patient_contact_tambon;
    public String patient_contact_amphur;
    public String patient_contact_changwat;
    public String patient_contact_telephone;
    public String patient_contact_mobile;
    public String patient_contact_detail_etc;
    public String active = "1";
    public String user_record_id;
    public String record_date_time;
    public String user_modify_id;
    public String modify_date_time;
    
}

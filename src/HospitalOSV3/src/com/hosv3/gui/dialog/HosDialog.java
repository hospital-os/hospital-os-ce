/*
 * HosDialog.java
 *
 * Created on 18 ����Ҥ� 2548, 10:32 �.
 *
 */
package com.hosv3.gui.dialog;

import com.hospital_os.object.*;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hosv3.control.HosControl;
import com.hosv3.object.HosObject;
import com.hosv3.object.LabList;
import com.hosv3.utility.Constant;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.Vector;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.text.JTextComponent;

/**
 *
 * @author administrator
 */
@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public class HosDialog {

    HosControl theHC;
    UpdateStatus theUS;
    DialogSearchPatient theDialogSearchPatient;
    DialogOffDrug theDialogOffDrug, theDialogContinueDrug;
    DialogDoctorClinic theDialogUseDoctor;
    DialogShowLabResult theDialogShowLabResult;
    DialogOffice theDialogOffice;
    DialogHistoryBilling theDialogHistoryBilling;
    DialogOrderHistory theDialogOrderHistory;
    DialogAccident theDialogAccident;
    DialogAdmit theDialogAdmit;
    DialogSurveil theDialogSurveil;
    DialogAppointment theDialogAppointment;
    DialogAppointment theDialogAppointmentlist;
//     DialogCause theDialogCause;
    DialogChronic theDialogChronic;
    DialogReferIn theDialogReferIn;
    DialogViewSequence theDialogViewSequence;
    DialogGuideAfterDx dialogGuideAfterDx;
    /**
     * add by sumo
     */
    DialogDischarge theDialogDischarge;
    DialogSeeIcd10 theDialogSeeIcd10;
    DialogDeath theDialogDeath;
    DialogBorrowFilmXray theDialogBorrowFilmXray;
    /**
     * add by jao
     */
    DialogOrderItemLabByLab theDialogOrderItemLabByLab;
    //tong
    DialogOrderItemXRayByXRay theDialogOrderItemXRayByXRay;
    DialogSelectLabPrint theDialogSelectLabPrint;
    //tong 18/04/2549
    DialogShowHistoryXN theDialogShowHistoryXN;
    //DialogChooseICD10FromTemplate theDialogChooseICD10FromTemplate;
    /**
     * @author tong
     * @
     */
    DialogDetailHospitalOS theDialogDetailHospitalOS;
    DialogSetPrefix theDialogSetPrefix;
    DialogHistoryPatient theDialogHistoryPatient;
    DialogOrderSet theDialogOrderSet;
    DialogOrderOldVisit theDialogOrderOldVisit;
    DialogReceiveDrug theDialogReceiveDrug;
    DialogHistoryOrderContinue theDialogHistoryOrderContinue;
    PanelResultXray thePanelResultXray;
    DialogCalBilling2 theDialogCalBilling;
    //add code by noom
    DialogPatientPaid theDialogPatientPaid;
    DialogPasswdForCancelReceipt theDialogPasswdForCancelReceipt;
    //Add by Neung
    DialogQueueVisit theDialogQueueVisit;
    PanelSetupSearchSub thePanelSetupSearchSub3;
    PanelSetupSearchSub thePanelSetupSearchSub4;
    PanelSetupSearchSub thePanelSetupSearchSub5;
    PanelSetupSearchSub thePanelSetupSearchSub7;
    PanelSetupSearchSub thePanelSetupSearchSub8;
    //neung Print Dialog
    DialogChooseOrderItemPrint theDialogChooseOrderItemPrint;
    //-neung Dialog Labrefer
    DialogLabReferIn theDialogLabReferIn;
    //neung Dialog ReverseAdmit
    DialogReverseAdmit theDialogReverseAdmit;
    //neung Dialog SelectPrintIpdNameCard
    DialogSelectPatientPrintNameCard theDialogSelectPatientPrintNameCard;
    // Somprasong add 20101007
    DialogNewNotifyNote theDialogNewNotifyNote;
    DialogListNotifyNote theDialogListNotifyNote;
    PanelNCD thePanelNCD;//amp:15/06/2549
    PanelLabList thePanelLabList;
    // Somprasong 171111 3.9.18
    DialogAdmitLeaveHomeInfo theDialogLeaveTheHouseInfo;
    DialogEyeExam theDialogEyeExam;
    DialogFootExam theDialogFootExam;
    // 3.9.19
    private DialogInformIntolerance theDialogInformIntolerance;
    private DialogInformAnemia theDialogInformAnemia;
    private DialogEmployeeList theDialogEmployeeList;
    private DialogAuthenList theDialogAuthenList;
    private DialogPatientHistoryReport theDialogPatientHistoryReport;
    // 3.9.29
    private PanelSearchPlan thePanelSearchPlan;
    private DialogInsuranceClaim theDialogInsuranceClaim;
    private DialogClaimReport theDialogClaimReport;
    private DialogClaimPreparePayment theDialogClaimPayment;
    //3.9.36
    private DialogInactiveOPDCard theDialogInactiveOPDCard;
    private DialogDrugFavorite theDialogDrugFavorite;
    private DialogAppointmentLimit theDialogAppointmentLimit;
    private DialogBorrowOPDCard2 theDialogBorrowOPDCard;
    // 3.9.37
    private DialogPatientNCD theDialogPatientNCD;
    private DialogAdmitEdit theDialogAdmitEdit;
    private WebcamSnapshotDialog webcamSnapshotDialog;
    private DialogDischargeIPD theDialogDischargeIPD;
    private DialogLISResult theDialogLISResult;
    private DialogEKGResult theDialogEKGResult;
    private DialogEDC theDialogEDC;
    private DialogCreateMedicalCerticate theDialogCreateMedicalCerticate;
    private DialogHistoryMedicalCerticate theDialogHistoryMedicalCerticate;
    private DialogAudiometry theDialogAudiometry;
    private DialogProgressNote theDialogProgressNote;
    private DialogHistoryLab theDialogHistpryLab;
    private DialogAppointmentTemplate theDialogAppointmentTemplate;
    public JFrame theDialogAppointmentStock;

    public HosDialog(HosControl hc, UpdateStatus us) {
        setControl(hc, us);
    }

    private void setControl(HosControl hc, UpdateStatus us) {
        theHC = hc;
        theUS = us;
    }

    public void showDialogParticipateOr(JTable jt, Vector parOrV) {
        if (thePanelSetupSearchSub5 == null) {
            thePanelSetupSearchSub5 = new PanelSetupSearchSub(theHC, theUS, 5);
            thePanelSetupSearchSub5.setTitle("���Ҫ��ͼ�����������ҵѴ");
        }
        thePanelSetupSearchSub5.showSearch(jt, parOrV);
    }

    public boolean showDialogSearchPatient(String fname, String lname, String hn, String pid, String dob) {
        if (theDialogSearchPatient == null) {
            theDialogSearchPatient = new DialogSearchPatient(theHC, theUS, 1);
        }
        if ((!fname.isEmpty() || !lname.isEmpty()) && (dob != null && !dob.isEmpty())) {
            theDialogSearchPatient.setFnameLnameDOB(fname, lname, dob);
        } else if (!fname.isEmpty() || !lname.isEmpty()) {
            theDialogSearchPatient.setFnameLname(fname, lname);
        } else if (!hn.isEmpty() || !pid.isEmpty()) {
            theDialogSearchPatient.setHNPID(hn, pid);
        } else if (dob != null && !dob.isEmpty()) {
            theDialogSearchPatient.setDOB(dob);
        }
        return theDialogSearchPatient.showDialog();

    }

    public boolean showDialogSearchPatientByPassportNo(String passportNo) {
        if (theDialogSearchPatient == null) {
            theDialogSearchPatient = new DialogSearchPatient(theHC, theUS, 1);
        }
        if (passportNo != null && !passportNo.isEmpty()) {
            theDialogSearchPatient.setPassportNo(passportNo);
        }
        return theDialogSearchPatient.showDialog();

    }

    public void showDialogShowLabResult(OrderItem oi) {
        if (theDialogShowLabResult == null) {
            theDialogShowLabResult = new DialogShowLabResult(theUS.getJFrame(), theHC);
        }
        theDialogShowLabResult.showDialog(oi);
    }

    public void showDialogViewSequence() {
        /**
         * tong comment becouse can not run date 24/05/48
         */
        if (theDialogViewSequence == null) {
            theDialogViewSequence = new DialogViewSequence(theHC, theUS);
        }
        theDialogViewSequence.showDialog();

    }

    /**
     * @author tong
     * @date 24/05/48
     * @param v Object Visit in transaction
     * @return void ���ʴ� Dialog referIn ����͵�ͧ��èС�˹���Ң����š�� refer
     * In/Out �ͧ������
     */
    public boolean showDialogReferIn(Visit v) {
        if (v == null || v.getObjectId() == null) {
            theUS.setStatus("��س����͡�����·������㹡�кǹ���", UpdateStatus.WARNING);
            return false;
        }
        if (theDialogReferIn == null) {
            theDialogReferIn = new DialogReferIn(theHC, theUS);
        }
        return theDialogReferIn.showDialogReferIn(v);
    }

    /**
     * @param vprefix
     * @param pf
     * @author tong
     * @date 24/05/48 ���ʴ� Dialog �ͧ��á�˹��ӹ�˹��
     */
    public void showDialogSetPrefix(Vector vprefix, String pf) {

        if (theDialogSetPrefix == null) {
            theDialogSetPrefix = new DialogSetPrefix(theHC, theUS);
        }
        theDialogSetPrefix.showDialog(vprefix, pf);

    }

    /**
     * @author tong
     * @date 24/05/48
     * @param thePatient Object Patient
     * @param theVisit Object Visit
     */
    public void showDialogAppointment(Patient thePatient, Visit theVisit) {
        if (thePatient == null) {
            theUS.setStatus("�ѧ��������͡������", UpdateStatus.WARNING);
            return;
        }

        if (theHC.theHP.theHD.theDialogAppointmentStock != null) {
            theDialogAppointment = (DialogAppointment) theHC.theHP.theHD.theDialogAppointmentStock;
            theDialogAppointment.setExtendedState(JFrame.MAXIMIZED_BOTH);
        }

        if (theDialogAppointment == null) {
            theDialogAppointment = new DialogAppointment(theHC, theUS, false, this);
            theDialogAppointment.setExtendedState(JFrame.MAXIMIZED_BOTH);
        }
        theDialogAppointment.showDialog(thePatient, theVisit, false);
    }

    /**
     * @author tong
     * @date 24/05/48
     * @param thePatient Object Patient ���ʴ� Dialog
     * ����ѵԡ���Ѻ��ԡ�âͧ������
     */
    public void showDialogHistoryPatient(Patient thePatient) {
//        if(theDialogHistoryPatient == null)
        theDialogHistoryPatient = new DialogHistoryPatient(theHC, theUS);
        theDialogHistoryPatient.showDialog(thePatient);

    }

//    /**
//     * @author LionHeart
//     * @date 27/10/53
//     * @param Object Patient
//     * @return void ���ʴ� Dialog �кػ����� Visit
//     */
//    public void showPanelServiceLocation(Visit visit) {
//
//        if (thePanelServiceLocation == null) {
//            thePanelServiceLocation = new PanelServiceLocation();
//        }
//        thePanelServiceLocation.showDialog(theHC.theUS.getJFrame(), visit);
//
//    }
    /**
     * @author LionHeart
     * @date 04/10/53
     * @param hc
     * @param panelOrder ���ʴ� Dialog ����� Sticker �Դ tube
     */
    public void showPanelLabList(HosControl hc, JPanel panelOrder) {
        if (thePanelLabList == null) {
            thePanelLabList = new PanelLabList();
            thePanelLabList.setControl(hc);
            thePanelLabList.setCheck(false);
        }
        // ����??? Sompraosng 19012015
//        hc.theVisitControl.readVisitPatientByVn(hc.theHO.theVisit.vn);
        if (panelOrder != null) {
            hc.theHP.theJTabbedPane.setSelectedComponent(panelOrder);
        }
        Vector v = hc.theLabControl.listLabOrderByVn(hc.theHO.theVisit.getObjectId());
        if (v == null || v.isEmpty()) {
            theHC.theUS.setStatus("��辺������", UpdateStatus.WARNING);
            return;
        }
        thePanelLabList.setCheck(false);
        thePanelLabList.showDialog(v, LabList.TYPE_NEW);
    }

    /**
     * @author tong
     * @date 24/05/48
     * @param theVisit ���ʴ� Dialog ��¡���Ҫش
     */
    public void showDialogOrderSet(Visit theVisit) {
        if (theDialogOrderSet == null) {
            theDialogOrderSet = new DialogOrderSet(theHC, theUS);
        }
        theDialogOrderSet.showDialog(theVisit);

    }

    public void showDialogOrderSet(PropertyChangeListener listener) {
        if (theDialogOrderSet == null) {
            theDialogOrderSet = new DialogOrderSet(theHC, theUS);
        }
        theDialogOrderSet.showDialog(listener);
    }

    /**
     * @author tong
     * @date 24/05/48 ���ʴ� Dialog ��¡��������͹���駷������ ��� vn
     * @param thePatient Object Patient
     * @param theVisit
     */
    public void showDialogOrderOldVisitByVn(Patient thePatient, Visit theVisit) {
        if (theDialogOrderOldVisit == null) {
            theDialogOrderOldVisit = new DialogOrderOldVisit(theHC, theUS);
        }
        theDialogOrderOldVisit.showDialog(thePatient, theVisit, true);

    }

    /**
     * @author tong
     * @date 24/05/48
     * @param thePatient
     * @param theVisit ���ʴ� Dialog ��¡��������͹���駷������ ��� �ѹ���
     */
    public void showDialogOrderOldVisitByDate(Patient thePatient, Visit theVisit) {
        if (theDialogOrderOldVisit == null) {
            theDialogOrderOldVisit = new DialogOrderOldVisit(theHC, theUS);
        }
        theDialogOrderOldVisit.showDialog(thePatient, theVisit, false);

    }

    /**
     * @author tong
     * @date 24/05/48
     * @param theVisit ���ʴ� ��§ҹ��������ͧ��èз���§ҹ
     */
    public void showDialogReceiveDrug(Visit theVisit) {
        if (theDialogReceiveDrug == null) {
            theDialogReceiveDrug = new DialogReceiveDrug(theHC, theUS);
        }
        theDialogReceiveDrug.showDialog(theVisit);

    }

    /**
     * @author tong
     * @date 27/05/48
     * @param theVisit ���ʴ� ��§ҹ��������ͧ��èз���§ҹ
     */
    public void showDialogHistoryOrderContinue(Visit theVisit) {
        if (theDialogHistoryOrderContinue == null) {
            theDialogHistoryOrderContinue = new DialogHistoryOrderContinue(theHC, theUS);
        }
        theDialogHistoryOrderContinue.showDialog(theVisit);

    }

    public void showDialogChronic(Visit theVisit, Chronic theChronic) {
        if (theDialogChronic == null) {
            theDialogChronic = new DialogChronic(theHC, theUS);
        }
        theDialogChronic.showDialog(theVisit, theChronic);
    }

    public void showDialogOrderHistory(Patient thePatient) {
        if (theDialogOrderHistory == null) {
            theDialogOrderHistory = new DialogOrderHistory(theHC, theUS);
        }
        theDialogOrderHistory.showDialog(thePatient);

    }

    public void showDialogOrderHistory(Patient thePatient, String datefrom, String dateto, String categoryGroupID) {
        DialogHistoryOrder d = new DialogHistoryOrder(theHC, theUS, this);
        d.showDialog(thePatient);
    }

    public void showDialogAccident() {
        if (theDialogAccident == null) {
            theDialogAccident = new DialogAccident(theHC, theUS);
        }
        theDialogAccident.showDialog(theHC.theHO);
    }

    public void showDialogAppointmentlist(Patient thePatient, Visit theVisit) {
        if (theDialogAppointmentlist == null) {
            theDialogAppointmentlist = new DialogAppointment(theHC, theUS, true, this);
            theDialogAppointmentlist.setExtendedState(JFrame.MAXIMIZED_BOTH);
        }
        theDialogAppointmentlist.showDialog(thePatient, theVisit, true);
    }

    public void showDialogAdmit(Visit theVisit) {
        if (theVisit == null) {
            theUS.setStatus("��س����͡�����·������Ѻ��ԡ������", UpdateStatus.WARNING);
            return;
        }
        if (theVisit.visit_type.equals(VisitType.OPD)) {
            if (theDialogAdmit == null) {
                theDialogAdmit = new DialogAdmit(theHC, theUS);
            }
            theDialogAdmit.showDialog(theVisit);
        } else {
            if (theDialogAdmitEdit == null) {
                theDialogAdmitEdit = new DialogAdmitEdit(theUS.getJFrame(), true);
                theDialogAdmitEdit.setControl(theHC);
            }
            theDialogAdmitEdit.openDialog(theVisit);
        }
    }

    public void showDialogHistoryBilling(Patient thePatient) {
        if (theDialogHistoryBilling == null) {
            theDialogHistoryBilling = new DialogHistoryBilling(theHC, theUS);
        }
        theDialogHistoryBilling.showDialog(thePatient);
    }

    public void showDialogHistoryBilling(Patient thePatient, String billing_id) {
        if (theDialogHistoryBilling == null) {
            theDialogHistoryBilling = new DialogHistoryBilling(theHC, theUS);
        }
        theDialogHistoryBilling.showDialog(thePatient, billing_id);
    }

    public void showDialogOrderItemLabByLab(Vector orderlab, Visit theVisit) {
        if (theDialogOrderItemLabByLab == null) {
            theDialogOrderItemLabByLab = new DialogOrderItemLabByLab(theHC, theUS);
        }
        theDialogOrderItemLabByLab.showDialog(orderlab, theVisit);
    }

    /**
     * �ʴ� Dialog ������� Item Xray
     *
     * @param orderxray Orderxray �� vector �ͧ Object Order
     * @param theVisit �� Object �ͧ Visit
     * @author padungrat(tong)
     */
    public void showDialogOrderItemXrayByXray(Vector orderxray, Visit theVisit) {
        if (theDialogOrderItemXRayByXRay == null) {
            theDialogOrderItemXRayByXRay = new DialogOrderItemXRayByXRay(theHC, theUS);
        }
        theDialogOrderItemXRayByXRay.showDialog(orderxray, theVisit);
    }

    public void showDialogSelectLabPrint(Vector orderlab, Visit theVisit, Patient thePatient, Vector vResultLab) {
        if (theDialogSelectLabPrint == null) {
            theDialogSelectLabPrint = new DialogSelectLabPrint(theUS.getJFrame(), theHC, theUS);
        }
        theDialogSelectLabPrint.showDialog(orderlab, theVisit, thePatient, orderlab, vResultLab);
    }

    private void closeDialog() {
        if (theDialogHistoryBilling != null) {
            theDialogHistoryBilling.setVisible(false);
            theDialogHistoryBilling = null;
        }
        if (theDialogOrderHistory != null) {
            theDialogOrderHistory.setVisible(false);
            theDialogOrderHistory = null;
        }
        if (theDialogAccident != null)//DialogAccident
        {
            theDialogAccident.setVisible(false);
            theDialogAccident = null;
        }
        if (theDialogAdmit != null)//DialogAdmit
        {
            theDialogAdmit.setVisible(false);
            theDialogAdmit = null;
        }
        if (theDialogAppointment != null)//DialogAppointment
        {
            theDialogAppointment.setVisible(false);
            theDialogAppointment = null;
        }
//        if(theDialogCause!=null)//DialogCause
//        {
//            theDialogCause.setVisible(false);
//            theDialogCause = null;
//        }
        if (theDialogChronic != null) {
            theDialogChronic.setVisible(false);
            theDialogChronic = null;
        }
        if (theDialogOrderItemLabByLab != null) {
            theDialogOrderItemLabByLab.setVisible(false);
            theDialogOrderItemLabByLab = null;
        }
        if (theDialogOrderItemXRayByXRay != null) {
            theDialogOrderItemXRayByXRay.setVisible(false);
            theDialogOrderItemXRayByXRay = null;
        }

        if (theDialogSelectLabPrint != null) {
            theDialogSelectLabPrint.setVisible(false);
            theDialogSelectLabPrint = null;
        }
    }

    /**
     * add code by noom
     *
     * @param billing
     * @param visit
     */
    public void showDialogPatientPaid(Billing billing, Visit visit) {
        DialogPatientPaid.showDialog(theHC, theUS, billing, visit);
    }

    //add code by noom
    public boolean showDialogPasswdForCancelReceipt() {
        if (theDialogPasswdForCancelReceipt == null) {
            theDialogPasswdForCancelReceipt = new DialogPasswdForCancelReceipt(theHC, theUS);
        }
        return theDialogPasswdForCancelReceipt.showDialog();
    }

    public boolean showDialogOffice(Office office) {
        if (office == null) {
            return false;
        }
        if (theDialogOffice == null) {
            theDialogOffice = new DialogOffice(theHC, theUS);
        }
        return theDialogOffice.showDialog(office);
    }

    public void showDialogSurveil(Surveil s) {
        if (theDialogSurveil == null) {
            theDialogSurveil = new DialogSurveil(theHC, theUS, s);
        }
        theDialogSurveil.showDialog(s);
    }

    public void showDialogDetailHospitalOS(Vector v) {
        if (theDialogDetailHospitalOS == null) {
            theDialogDetailHospitalOS = new DialogDetailHospitalOS(theHC, theUS);
        }
        theDialogDetailHospitalOS.showDialog(v);
    }

    private void showPanelSetupSearchSub(int mode) {

        if (mode == 7) {
            if (thePanelSetupSearchSub7 == null) {
                thePanelSetupSearchSub7 = new PanelSetupSearchSub(theHC, theUS, mode);
            }
            thePanelSetupSearchSub7.setTitle(Constant.getTextBundle("��Ǫ����ҡ�����ͧ��"));
            thePanelSetupSearchSub7.showSearch();
        } else if (mode == 8) {
            if (thePanelSetupSearchSub8 == null) {
                thePanelSetupSearchSub8 = new PanelSetupSearchSub(theHC, theUS, mode);
            }
            thePanelSetupSearchSub8.setTitle(Constant.getTextBundle("��Ǫ����ҡ�����ͧ��"));
            thePanelSetupSearchSub8.showSearch();
        }

    }

    public void showDialogDrugAllergyTemplate(JTable jt, Vector v) {
        if (thePanelSetupSearchSub3 == null) {
            thePanelSetupSearchSub3 = new PanelSetupSearchSub(theHC, theUS, 3);
        }
        thePanelSetupSearchSub3.setTitle(Constant.getTextBundle("��Ǫ���������¡���ҷ����"));
        thePanelSetupSearchSub3.showSearchAllergy(jt, v);
    }

    public void showDialogDxTemplate(JTextComponent jf, Vector v) {
        PanelSetupSearchSub.showDialogDx(theHC, theUS, 4, Constant.getTextBundle("��Ǫ��¡���ԹԨ���"), jf, v);
    }
//     public void showDialogPrimarySymptomTemplate()
//     {
//        showPanelSetupSearchSub(7);
//     }

    public void showDialogPrimarySymptomTemplate(JTextComponent component) {
        if (thePanelSetupSearchSub8 == null) {
            thePanelSetupSearchSub8 = new PanelSetupSearchSub(theHC, theUS, 7);
        }
        thePanelSetupSearchSub8.setTitle(Constant.getTextBundle("��Ǫ����ҡ�����ͧ��"));
        thePanelSetupSearchSub8.setComponent(component);
        thePanelSetupSearchSub8.showSearch();

    }

    public void showDialogResultXray(OrderItem theOrderItem) {
        if (thePanelResultXray == null) {
            thePanelResultXray = new PanelResultXray(theHC.theUS.getJFrame());
            thePanelResultXray.setHosControl(theHC);
        }
        thePanelResultXray.showSearch(theOrderItem);
    }

    public void showDialogCalBilling(Vector vOrderItem, boolean all) {
        if (theDialogCalBilling == null) {
            theDialogCalBilling = new DialogCalBilling2(theHC, theUS);
        }
        theDialogCalBilling.showDialog(vOrderItem);
    }

    public void showDialogGuideAfterDx(Visit theVisit, Vector vGuide) {
        if (dialogGuideAfterDx == null) {
            dialogGuideAfterDx = new DialogGuideAfterDx(theHC, theUS, theVisit, vGuide);
        }
        dialogGuideAfterDx.showDialog(theVisit, vGuide);
    }

    /**
     * ��㹡���ʴ�����ѵԢͧ�Ţ XN
     *
     * @param hosObject
     * @return �� boolean
     * @author padungrat(tong)
     * @date 18/04/2549,10:50
     */
    public boolean showDialogShowHistoryXN(HosObject hosObject) {
        if (theDialogShowHistoryXN == null) {
            theDialogShowHistoryXN = new DialogShowHistoryXN(theHC, theUS);
        }
        return theDialogShowHistoryXN.showDialog(hosObject);
    }

    public boolean showDialogDischarge(Visit theVisit) {
        if (theDialogDischarge == null) {
            theDialogDischarge = new DialogDischarge(theUS.getJFrame(), true);
            theDialogDischarge.setHosControl(theHC, theUS);
            theDialogDischarge.setLocationRelativeTo(theUS.getJFrame());
        }
        return theDialogDischarge.showDialog(theVisit);
    }

    public boolean showDialogDischargeIPD(Visit theVisit) {
        if (theDialogDischargeIPD == null) {
            theDialogDischargeIPD = new DialogDischargeIPD(theUS.getJFrame(), true);
            theDialogDischargeIPD.setHosControl(theHC, theUS);
            theDialogDischargeIPD.setLocationRelativeTo(theUS.getJFrame());
        }
        return theDialogDischargeIPD.showDialog(theVisit);
    }

    public void showDialogSeeIcd10(@SuppressWarnings("UseOfObsoleteCollectionType") Vector see, JTextField jt) {
        if (theDialogSeeIcd10 == null) {
            theDialogSeeIcd10 = new DialogSeeIcd10(theHC, theUS, see, jt);
        }
        theDialogSeeIcd10.showDialog(see, jt);
    }

    public boolean showDialogQueueVisit(Visit theVisit,
            QueueVisit mqv,
            int mod,
            boolean useQueue,
            Vector vWaitAppointment,
            Vector vCh) {
        if (theDialogQueueVisit == null) {
            theDialogQueueVisit = new DialogQueueVisit(theUS.getJFrame(), true);
            theDialogQueueVisit.setControl(theHC, theUS);
        }
        return theDialogQueueVisit.showDialog(theVisit, mqv, mod, useQueue, vWaitAppointment, vCh);
    }

    public void showDialogChooseOrderItemPrint(JFrame jframe, int preview) {
        if (theDialogChooseOrderItemPrint == null) {
            theDialogChooseOrderItemPrint = new DialogChooseOrderItemPrint(theUS.getJFrame(), theHC, preview);
        } else {
            theDialogChooseOrderItemPrint.setPreview(preview);
        }
    }

    /**
     * ��㹡���ʴ� Dialog ���͡ᾷ�� ��� clinic �����������ä
     *
     * @param ddc �� Object �ͧ Clinic �����ҡ������͡ (call by value)
     * @return
     * @author padungrat(tong)
     * @date 19/04/2549,10:55
     */
    public boolean showDialogDoctorDiag(DiagDoctorClinic ddc) {
        if (theDialogUseDoctor == null) {
            theDialogUseDoctor = new DialogDoctorClinic(theHC, theUS);
        }
        theDialogUseDoctor.setTitle(Constant.getTextBundle("���͡ᾷ�����������"));
        return theDialogUseDoctor.showDialog(ddc);
    }

    /**
     * ��㹡���ʴ� Dialog ���͡ᾷ�� ��� clinic �����������ä
     *
     * @param ddc �� Object �ͧ Clinic �����ҡ������͡ (call by value)
     * @return
     * @author henbe
     * @date 5/9/2549,10:55
     * @not deprecated ��ͧ�������ҹ�Ѻ theDiagDoctorClinic ����� local
     * ��е�ͧ����� employee �Ѻ clinic �������
     */
    public boolean showDialogDiag(DiagDoctorClinic ddc) {
        if (theDialogUseDoctor == null) {
            theDialogUseDoctor = new DialogDoctorClinic(theHC, theUS);
        }
        theDialogUseDoctor.setTitle(Constant.getTextBundle("���͡ᾷ�����������"));
        return theDialogUseDoctor.showDialog(ddc);
    }

    /**
     * @deprecated henbe unused ��㹡���ʴ� Dialog ���͡Clinic������������ä
     * @param ddc �� Object �ͧ Clinic �����ҡ������͡ (call by value)
     * @return
     * @author padungrat(tong)
     * @date 19/04/2549,10:55
     */
    public boolean showDialogClinicDiag(DiagDoctorClinic ddc) {
        if (theDialogUseDoctor == null) {
            theDialogUseDoctor = new DialogDoctorClinic(theHC, theUS);
        }
        theDialogUseDoctor.setTitle(Constant.getTextBundle("���͡ᾷ�����������"));
        return theDialogUseDoctor.showDialog(ddc);
    }

    public boolean showDialogContinueDrug(DiagDoctorClinic ddc) {
        if (theDialogContinueDrug == null) {
            theDialogContinueDrug = new DialogOffDrug(theUS.getJFrame(), true, theHC, theUS);
        }
        theDialogContinueDrug.setTitle(Constant.getTextBundle("���͡ᾷ������觵�Ǩ������ͧ"));
        theDialogContinueDrug.setUseContinueOrOff(true);
        return theDialogContinueDrug.showDialog(ddc);
    }

    public boolean showDialogOffDrug(DiagDoctorClinic ddc) {
        if (theDialogOffDrug == null) {
            theDialogOffDrug = new DialogOffDrug(theUS.getJFrame(), true, theHC, theUS);
        }
        theDialogOffDrug.setTitle(Constant.getTextBundle("���͡ᾷ���� OFF"));
        theDialogOffDrug.setUseContinueOrOff(false);
        return theDialogOffDrug.showDialog(ddc);
    }

    public void showDialogDeath() {
        if (theDialogDeath == null) {
            theDialogDeath = new DialogDeath(theHC, theUS);
        }
        theDialogDeath.showDialog(theHC.theHO.theFamily, theHC.theHO.thePatient, theHC.theHO.theVisit);
    }

    public boolean showDialogDeath(Death dt) {
        if (theDialogDeath == null) {
            theDialogDeath = new DialogDeath(theHC, theUS);
        }
        return theDialogDeath.showDialog(dt);
    }

    public void showDialogLabReferIn(HosControl theHC, UpdateStatus theUS) {
        if (theDialogLabReferIn == null) {
            theDialogLabReferIn = new DialogLabReferIn(theHC, theUS);
        }
        theDialogLabReferIn.showDialog(theHC, theUS);
    }

    public void showDialogReverseAdmit(HosControl theHC, UpdateStatus theUS) {
        if (theDialogReverseAdmit == null) {
            theDialogReverseAdmit = new DialogReverseAdmit(theHC, theUS, true);
        }
        theDialogReverseAdmit.showDialog(theHC.theHO.theVisit);
    }

    public void showDialogSelectPatient(HosControl theHC, UpdateStatus theUS) {
        if (theDialogSelectPatientPrintNameCard == null) {
            theDialogSelectPatientPrintNameCard = new DialogSelectPatientPrintNameCard(theHC, theUS);
        }
        theDialogSelectPatientPrintNameCard.showDialog();
    }

    public void showDialogBorrowFilmXray(HosControl theHC, UpdateStatus theUS) {
        if (theDialogBorrowFilmXray == null) {
            theDialogBorrowFilmXray = new DialogBorrowFilmXray(theHC, theUS);
        }
        DialogBorrowFilmXray.showDialog(theHC, theUS);
    }

    public void showDialogBorrowOpdCard(HosControl theHC, UpdateStatus theUS) {
        if (theDialogBorrowOPDCard == null) {
            theDialogBorrowOPDCard = new DialogBorrowOPDCard2();
            theDialogBorrowOPDCard.setHosControl(theHC);
            theDialogBorrowOPDCard.setSize(900, 600);
        }
        theDialogBorrowOPDCard.openDialog();
    }

    public void showDialogNewNotifyNote(String id) {
        if (theDialogNewNotifyNote == null) {
            theDialogNewNotifyNote = new DialogNewNotifyNote(theUS.getJFrame(), true);
            theDialogNewNotifyNote.setControl(theHC, this);
        }
        theDialogNewNotifyNote.openDialog(id);
    }

    public void showDialogListNotifyNote(String hn) {
        if (theDialogListNotifyNote == null) {
            theDialogListNotifyNote = new DialogListNotifyNote(theUS.getJFrame(), true);
            theDialogListNotifyNote.setControl(theHC, this);
        }
        theDialogListNotifyNote.openDialog(hn);
    }

    public void showDialogListNotifyNote(String hn, String visitId, int mode) {
        if (theDialogListNotifyNote == null) {
            theDialogListNotifyNote = new DialogListNotifyNote(theUS.getJFrame(), true);
            theDialogListNotifyNote.setControl(theHC, this);
        }
        theDialogListNotifyNote.openDialog(hn, visitId, mode);
    }

    public void showLeaveTheHouseDialog(Visit visit) {
        if (theDialogLeaveTheHouseInfo == null) {
            theDialogLeaveTheHouseInfo = new DialogAdmitLeaveHomeInfo(theUS.getJFrame(), true);
            theDialogLeaveTheHouseInfo.setControl(theHC);
        }
        theDialogLeaveTheHouseInfo.openDialog(visit);
    }

    public void showEyeExamDialog(Visit visit) {
        if (theDialogEyeExam == null) {
            theDialogEyeExam = new DialogEyeExam(theUS.getJFrame(), true);
            theDialogEyeExam.setControl(theHC);
        }
        theDialogEyeExam.openDialog(visit.getObjectId());
    }

    public void showAudiometryDialog(Visit visit) {
        if (theDialogAudiometry == null) {
            theDialogAudiometry = new DialogAudiometry(theUS.getJFrame(), true);
            theDialogAudiometry.setControl(theHC);
        }
        theDialogAudiometry.openDialog(visit.getObjectId());
    }

    public void showFootExamDialog(Visit visit) {
        if (theDialogFootExam == null) {
            theDialogFootExam = new DialogFootExam(theUS.getJFrame(), true);
            theDialogFootExam.setControl(theHC);
        }
        theDialogFootExam.openDialog(visit.getObjectId());
    }

    public void showInformIntoleranceDialog(Patient patient) {
        if (theDialogInformIntolerance == null) {
            theDialogInformIntolerance = new DialogInformIntolerance(theUS.getJFrame(), true);
            theDialogInformIntolerance.setControl(theHC);
        }
        theDialogInformIntolerance.openDialog(patient);
    }

    public void showUncheckInformIntoleranceDialog(Patient patient) {
        if (theDialogInformIntolerance == null) {
            theDialogInformIntolerance = new DialogInformIntolerance(theUS.getJFrame(), true);
            theDialogInformIntolerance.setControl(theHC);
        }
        theDialogInformIntolerance.openDialog(patient, true);
    }

    public void showInformAnemiaDialog(Visit theVisit) {
        if (theDialogInformAnemia == null) {
            theDialogInformAnemia = new DialogInformAnemia(theUS.getJFrame(), true);
            theDialogInformAnemia.setControl(theHC);
        }
        theDialogInformAnemia.openDialog(theVisit);
    }

    public List<Employee> showDialogEmployeeList() {
        if (theDialogEmployeeList == null) {
            theDialogEmployeeList = new DialogEmployeeList(theUS.getJFrame(), true);
            theDialogEmployeeList.setControl(theHC);
        }
        return theDialogEmployeeList.openDialog();
    }

    public String showDialogAuthenList(String authens) {
        if (theDialogAuthenList == null) {
            theDialogAuthenList = new DialogAuthenList(theUS.getJFrame(), true);
            theDialogAuthenList.setControl(theHC);
        }
        return theDialogAuthenList.openDialog(authens);
    }

    public void showDialogPatientHistoryReport() {
        if (theHC.theHO.thePatient == null) {
            theUS.setStatus("��س����͡�����¡�͹", UpdateStatus.WARNING);
            return;
        }
        if (theDialogPatientHistoryReport == null) {
            theDialogPatientHistoryReport = new DialogPatientHistoryReport(theUS.getJFrame(), true);
            theDialogPatientHistoryReport.setControl(theHC);
            theDialogPatientHistoryReport.setSize(820, 640);
        }
        theDialogPatientHistoryReport.openDialog();
    }

    public List<Plan> showPlanChooserDialog() {
        if (thePanelSearchPlan == null) {
            thePanelSearchPlan = new PanelSearchPlan();
            thePanelSearchPlan.setControl(theHC, theUS);
        }
        return thePanelSearchPlan.showChooserDialog();
    }

    public void showDialogIllnessAddress() {
        if (theHC.theHO.theVisit == null) {
            theUS.setStatus("��س����͡�����·������Ѻ��ԡ������", UpdateStatus.WARNING);
            return;
        }
        DialogVisitIllnessAddress theDialogIllnessAddress = new DialogVisitIllnessAddress(theUS.getJFrame(), true);
        theDialogIllnessAddress.setHosControl(theHC);
        theDialogIllnessAddress.openDialog(theHC.theHO.thePatient, theHC.theHO.theVisit);
    }

    public void showDialogInsuranceClaim(BillingInvoice bi, Visit visit) {
        if (theDialogInsuranceClaim == null) {
            theDialogInsuranceClaim = new DialogInsuranceClaim(theUS.getJFrame(), true);
            theDialogInsuranceClaim.setControl(theHC);
        }
        theDialogInsuranceClaim.openDialog(bi, visit);
    }

    public void showDialogClaimPayment() {
        if (theDialogClaimPayment == null) {
            theDialogClaimPayment = new DialogClaimPreparePayment();
            theDialogClaimPayment.setControl(theHC);
            theDialogClaimPayment.setSize(800, 600);
        }
        theDialogClaimPayment.openDialog();
    }

    public void showDialogClaimReport() {
        if (theDialogClaimReport == null) {
            theDialogClaimReport = new DialogClaimReport();
            theDialogClaimReport.setControl(theHC);
            theDialogClaimReport.setSize(800, 600);
        }
        theDialogClaimReport.openDialog();
    }

    public void showDialogInactiveOPDCard() {
        if (theDialogInactiveOPDCard == null) {
            theDialogInactiveOPDCard = new DialogInactiveOPDCard();
            theDialogInactiveOPDCard.setHosControl(theHC);
            theDialogInactiveOPDCard.setSize(900, 600);
        }
        theDialogInactiveOPDCard.openDialog();
    }

    public void showDialogDrugFavorite() {
        if (theHC.theHO.theVisit == null) {
            theUS.setStatus("��س����͡�����·������㹡�кǹ���", UpdateStatus.WARNING);
            return;
        }
        if (theDialogDrugFavorite == null) {
            theDialogDrugFavorite = new DialogDrugFavorite(theUS.getJFrame(), true);
            theDialogDrugFavorite.setControl(theHC);
            theDialogDrugFavorite.setSize(300, 400);
        }
        theDialogDrugFavorite.showDialog();

    }

    public void showDialogAppointmentLimit() {
        if (theDialogAppointmentLimit == null) {
            theDialogAppointmentLimit = new DialogAppointmentLimit();
            theDialogAppointmentLimit.setControl(theHC);
            theDialogAppointmentLimit.setSize(900, 400);
        }
        theDialogAppointmentLimit.showDialog();
    }

    public void showDialogPatientNCD(Patient patient) {
        if (theDialogPatientNCD == null) {
            theDialogPatientNCD = new DialogPatientNCD(theUS.getJFrame(), true);
            theDialogPatientNCD.setControl(theHC);
            theDialogPatientNCD.setSize(400, 400);
        }
        theDialogPatientNCD.openDialog(patient);
    }

    public BufferedImage doOpenWebcamSnapshotDialog(Patient patient) {
        if (patient == null) {
            theUS.setStatus("��س����͡�����¡�͹", UpdateStatus.WARNING);
            return null;
        }
        if (webcamSnapshotDialog == null) {
            webcamSnapshotDialog = new WebcamSnapshotDialog(theUS.getJFrame(), true);
//            webcamSnapshotDialog.setControl(theHC);
        }
        return webcamSnapshotDialog.openDialog();
//        return image == null ? null : ImageUtils.toByteArray(ImageUtils.fit(image, 80, 80));
    }

    public void showDialogLISResult() {
        if (theHC.theHO.thePatient == null) {
            theUS.setStatus("��س����͡�����¡�͹", UpdateStatus.WARNING);
            return;
        }
        if (theDialogLISResult == null) {
            theDialogLISResult = new DialogLISResult(theUS.getJFrame(), true);
            theDialogLISResult.setControl(theHC);
            theDialogLISResult.setSize(800, 450);
        }
        theDialogLISResult.openDialog();
    }

    public void showDialogEKGResult() {
        if (theHC.theHO.thePatient == null) {
            theUS.setStatus("��س����͡�����¡�͹", UpdateStatus.WARNING);
            return;
        }
        if (theDialogEKGResult == null) {
            theDialogEKGResult = new DialogEKGResult(theUS.getJFrame(), true);
            theDialogEKGResult.setControl(theHC);
            theDialogEKGResult.setSize(800, 450);
        }
        theDialogEKGResult.openDialog();
    }

    public void showDialogEDC() {
        if (theHC.theHO.theVisit == null) {
            theUS.setStatus("��س����͡�����·������Ѻ��ԡ������", UpdateStatus.WARNING);
            return;
        }
        if (theDialogEDC == null) {
            theDialogEDC = new DialogEDC(theUS.getJFrame(), true);
            theDialogEDC.setControl(theHC);
        }
        theDialogEDC.openDialog(theHC.theHO.theVisit.getObjectId());
    }

    public void showDialogCreateMedicalCertoficate(Visit visit) {
        if (visit == null) {
            theUS.setStatus("��س����͡�����·������Ѻ��ԡ������", UpdateStatus.WARNING);
            return;
        }
        if (theDialogCreateMedicalCerticate == null) {
            theDialogCreateMedicalCerticate = new DialogCreateMedicalCerticate();
            theDialogCreateMedicalCerticate.setControl(theHC);
        }
        theDialogCreateMedicalCerticate.openDialog(visit.getObjectId());
    }

    public void showDialogHistoryMedicalCerticate(Patient patient, Visit visit) {
        if (theDialogHistoryMedicalCerticate == null) {
            theDialogHistoryMedicalCerticate = new DialogHistoryMedicalCerticate(theUS.getJFrame(), false);
            theDialogHistoryMedicalCerticate.setControl(theHC);
        }
        theDialogHistoryMedicalCerticate.openDialog(patient, visit);
    }

    public void showDialogProgressNote(Visit visit) {
        if (visit == null) {
            theUS.setStatus("��س����͡�����·������Ѻ��ԡ������", UpdateStatus.WARNING);
            return;
        }
        if (theDialogProgressNote == null) {
            theDialogProgressNote = new DialogProgressNote(theHC, theUS);
        }
        theDialogProgressNote.openDialog(visit);
    }

    public void showDialogHistoryLab(Visit visit) {
        if (visit == null) {
            theUS.setStatus("��س����͡�����·������Ѻ��ԡ������", UpdateStatus.WARNING);
            return;
        }
        if (theDialogHistpryLab == null) {
            theDialogHistpryLab = new DialogHistoryLab(theUS.getJFrame(), true);
            theDialogHistpryLab.setControl(theHC);
        }
        theDialogHistpryLab.openDialog(visit);
    }

    public void showDialogAppointmentTemplate() {
        if (theDialogAppointmentTemplate == null) {
            theDialogAppointmentTemplate = new DialogAppointmentTemplate(theHC, theUS, this);
        }
        theDialogAppointmentTemplate.showDialog();
    }
}

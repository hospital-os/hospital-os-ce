/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.InsuranceClaimBillingReceipt;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class InsuranceClaimBillingReceiptDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "823";

    public InsuranceClaimBillingReceiptDB(ConnectionInf c) {
        this.connectionInf = c;
    }

    public int insert(InsuranceClaimBillingReceipt obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO t_insurance_claim_receipt(\n"
                    + "            t_insurance_claim_receipt_id, t_insurance_claim_billing_id, receipt_number, receipt_display_name, receipt_type, t_patient_id, user_record_id, record_datetime)\n"
                    + "    VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getGenID(tableId));
            preparedStatement.setString(2, obj.t_insurance_claim_billing_id);
            preparedStatement.setString(3, obj.receipt_number);
            preparedStatement.setString(4, obj.receipt_display_name);
            preparedStatement.setString(5, obj.receipt_type);
            preparedStatement.setString(6, obj.t_patient_id);
            preparedStatement.setString(7, obj.user_record_id);
            preparedStatement.setString(8, obj.record_datetime);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<InsuranceClaimBillingReceipt> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<InsuranceClaimBillingReceipt> list = new ArrayList<InsuranceClaimBillingReceipt>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                InsuranceClaimBillingReceipt obj = new InsuranceClaimBillingReceipt();
                obj.setObjectId(rs.getString("t_insurance_claim_receipt_id"));
                obj.t_insurance_claim_billing_id = rs.getString("t_insurance_claim_billing_id");
                obj.receipt_number = rs.getString("receipt_number");
                obj.receipt_display_name = rs.getString("receipt_display_name");
                obj.t_patient_id = rs.getString("t_patient_id");
                obj.receipt_type = rs.getString("receipt_type");
                obj.user_record_id = rs.getString("user_record_id");
                obj.record_datetime = rs.getString("record_datetime");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public int updatePrintCount(String... receiptNumbers) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String numbers = "";
            for (String receiptNumber : receiptNumbers) {
                numbers += ",'" + receiptNumber + "'";
            }
            String sql = "UPDATE t_insurance_claim_receipt SET total_print = total_print + 1 where receipt_number in (%s)";
            preparedStatement = connectionInf.ePQuery(String.format(sql, numbers.substring(1)));
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class DrugDosageForm extends Persistent implements CommonInf{

    private static final long serialVersionUID = 1L;
    public String form_name;
    public String active = "1";
    public String user_record_id;
    public String record_date_time;
    public String user_update_id;
    public String update_date_time;

    @Override
    public String getCode() {
        return getObjectId();
    }

    @Override
    public String getName() {
        return form_name;
    }
    
    @Override
    public String toString() {
        return getName();
    }
}

ALTER TABLE public.t_lis_ln ADD exec_user_id varchar(30) NULL;
ALTER TABLE public.t_lis_ln ADD exec_location_id varchar(30) NULL;

ALTER TABLE b_item_drug ADD COLUMN pregnancy_category varchar(1) NOT NULL DEFAULT ''; -- A/B/C/D/X
ALTER TABLE t_order_drug ADD COLUMN pregnancy_category varchar(1) NOT NULL DEFAULT ''; -- A/B/C/D/X
ALTER TABLE b_printing ADD COLUMN enable_choose_language varchar(1) NOT NULL DEFAULT '0';

ALTER TABLE b_service_point ADD COLUMN service_time_per_person int4 NOT NULL DEFAULT 0;
ALTER TABLE b_restful_url ADD is_system bool NOT NULL DEFAULT false;

INSERT INTO b_restful_url (b_restful_url_id, restful_code, restful_url, user_update_id, is_system)
VALUES  ('999' || (select b_visit_office_id from b_site) || 'smart.hos.vq','smart.hos.vq','http://' || host(inet_server_addr()) || ':8080',(select rpad('157'||b_visit_office_id,18,'0')  from b_site), true);

insert into f_gui_action (f_gui_action_id, gui_action_name) VALUES ('0815', 'พิมพ์ใบนำทาง');

-- set default value
ALTER TABLE public.t_health_family DROP COLUMN t_patient_id;
ALTER TABLE public.t_health_family ALTER COLUMN f_prefix_id SET DEFAULT '000'::character varying;
ALTER TABLE public.t_health_family ALTER COLUMN patient_birthday TYPE varchar(10) USING patient_birthday::varchar;
ALTER TABLE public.t_health_family ALTER COLUMN patient_birthday_true TYPE varchar(1) USING patient_birthday_true::varchar;
ALTER TABLE public.t_health_family ALTER COLUMN patient_birthday_true SET NOT NULL;
ALTER TABLE public.t_health_family ALTER COLUMN patient_birthday_true SET DEFAULT '1'::character varying;
ALTER TABLE public.t_health_family ALTER COLUMN f_sex_id TYPE varchar(1) USING f_sex_id::varchar;
ALTER TABLE public.t_health_family ALTER COLUMN f_sex_id SET NOT NULL;
ALTER TABLE public.t_health_family ALTER COLUMN f_sex_id SET DEFAULT '3'::character varying;
ALTER TABLE public.t_health_family ALTER COLUMN f_patient_education_type_id TYPE varchar(2) USING f_patient_education_type_id::varchar;
ALTER TABLE public.t_health_family ALTER COLUMN f_patient_education_type_id SET NOT NULL;
ALTER TABLE public.t_health_family ALTER COLUMN f_patient_education_type_id SET DEFAULT '11'::character varying;
ALTER TABLE public.t_health_family ALTER COLUMN f_patient_marriage_status_id TYPE varchar(2) USING f_patient_marriage_status_id::varchar;
ALTER TABLE public.t_health_family ALTER COLUMN f_patient_marriage_status_id SET NOT NULL;
ALTER TABLE public.t_health_family ALTER COLUMN f_patient_marriage_status_id SET DEFAULT '9'::character varying;
ALTER TABLE public.t_health_family ALTER COLUMN f_patient_occupation_id TYPE varchar(5) USING f_patient_occupation_id::varchar;
ALTER TABLE public.t_health_family ALTER COLUMN f_patient_occupation_id SET NOT NULL;
ALTER TABLE public.t_health_family ALTER COLUMN f_patient_occupation_id SET DEFAULT '000'::character varying;
ALTER TABLE public.t_health_family ALTER COLUMN f_patient_nation_id TYPE varchar(3) USING f_patient_nation_id::varchar;
ALTER TABLE public.t_health_family ALTER COLUMN f_patient_nation_id SET NOT NULL;
ALTER TABLE public.t_health_family ALTER COLUMN f_patient_nation_id SET DEFAULT '99'::character varying;
ALTER TABLE public.t_health_family ALTER COLUMN f_patient_race_id TYPE varchar(3) USING f_patient_race_id::varchar;
ALTER TABLE public.t_health_family ALTER COLUMN f_patient_race_id SET NOT NULL;
ALTER TABLE public.t_health_family ALTER COLUMN f_patient_race_id SET DEFAULT '99'::character varying;
ALTER TABLE public.t_health_family ALTER COLUMN f_patient_religion_id TYPE varchar(2) USING f_patient_religion_id::varchar;
ALTER TABLE public.t_health_family ALTER COLUMN f_patient_religion_id SET NOT NULL;
ALTER TABLE public.t_health_family ALTER COLUMN f_patient_religion_id SET DEFAULT '9'::character varying;
ALTER TABLE public.t_health_family ALTER COLUMN f_patient_family_status_id TYPE varchar(1) USING f_patient_family_status_id::varchar;
ALTER TABLE public.t_health_family ALTER COLUMN f_patient_family_status_id SET NOT NULL;
ALTER TABLE public.t_health_family ALTER COLUMN f_patient_family_status_id SET DEFAULT '2'::character varying;
ALTER TABLE public.t_health_family ALTER COLUMN f_patient_blood_group_id TYPE varchar(1) USING f_patient_blood_group_id::varchar;
ALTER TABLE public.t_health_family ALTER COLUMN f_patient_blood_group_id SET NOT NULL;
ALTER TABLE public.t_health_family ALTER COLUMN f_patient_blood_group_id SET DEFAULT '1'::character varying;
ALTER TABLE public.t_health_family ALTER COLUMN f_patient_area_status_id TYPE varchar(1) USING f_patient_area_status_id::varchar;
ALTER TABLE public.t_health_family ALTER COLUMN f_patient_area_status_id SET NOT NULL;
ALTER TABLE public.t_health_family ALTER COLUMN f_patient_area_status_id SET DEFAULT '1'::character varying;
ALTER TABLE public.t_health_family ALTER COLUMN record_date_time TYPE varchar(19) USING record_date_time::varchar;
ALTER TABLE public.t_health_family ALTER COLUMN record_date_time SET NOT NULL;
ALTER TABLE public.t_health_family ALTER COLUMN record_date_time SET DEFAULT ((to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'));
ALTER TABLE public.t_health_family ALTER COLUMN health_family_staff_record TYPE varchar(30) USING health_family_staff_record::varchar;
ALTER TABLE public.t_health_family ALTER COLUMN health_family_staff_record SET NOT NULL;
ALTER TABLE public.t_health_family ALTER COLUMN health_family_staff_record SET DEFAULT '1570000000001'::character varying;
ALTER TABLE public.t_health_family ALTER COLUMN health_family_active TYPE varchar(1) USING health_family_active::varchar;
ALTER TABLE public.t_health_family ALTER COLUMN health_family_active SET NOT NULL;
ALTER TABLE public.t_health_family ALTER COLUMN health_family_active SET DEFAULT '1'::character varying;
ALTER TABLE public.t_health_family ALTER COLUMN health_family_rh TYPE varchar(1) USING health_family_rh::varchar;
ALTER TABLE public.t_health_family ALTER COLUMN health_family_rh SET NOT NULL;
ALTER TABLE public.t_health_family ALTER COLUMN health_family_rh SET DEFAULT '9'::character varying;
ALTER TABLE public.t_health_family ALTER COLUMN f_person_village_status_id SET NOT NULL;
ALTER TABLE public.t_health_family ALTER COLUMN patient_couple_id SET NOT NULL;
ALTER TABLE public.t_health_family ALTER COLUMN patient_couple_id SET DEFAULT ''::character varying;
ALTER TABLE public.t_health_family ALTER COLUMN patient_mother_pid SET NOT NULL;
ALTER TABLE public.t_health_family ALTER COLUMN patient_mother_pid SET DEFAULT ''::character varying;
ALTER TABLE public.t_health_family ALTER COLUMN patient_father_pid SET NOT NULL;
ALTER TABLE public.t_health_family ALTER COLUMN patient_father_pid SET DEFAULT ''::character varying;


ALTER TABLE public.t_patient ALTER COLUMN f_patient_prefix_id SET NOT NULL;
ALTER TABLE public.t_patient ALTER COLUMN f_patient_prefix_id SET DEFAULT '000';
ALTER TABLE public.t_patient ALTER COLUMN f_sex_id SET NOT NULL;
ALTER TABLE public.t_patient ALTER COLUMN f_sex_id SET DEFAULT '3';
ALTER TABLE public.t_patient ALTER COLUMN f_patient_education_type_id SET NOT NULL;
ALTER TABLE public.t_patient ALTER COLUMN f_patient_education_type_id SET DEFAULT '11';
ALTER TABLE public.t_patient ALTER COLUMN f_patient_marriage_status_id SET NOT NULL;
ALTER TABLE public.t_patient ALTER COLUMN f_patient_marriage_status_id SET DEFAULT '9';
ALTER TABLE public.t_patient ALTER COLUMN f_patient_occupation_id SET NOT NULL;
ALTER TABLE public.t_patient ALTER COLUMN f_patient_occupation_id SET DEFAULT '000';
ALTER TABLE public.t_patient ALTER COLUMN f_patient_race_id SET NOT NULL;
ALTER TABLE public.t_patient ALTER COLUMN f_patient_race_id SET DEFAULT '99';
ALTER TABLE public.t_patient ALTER COLUMN f_patient_nation_id SET NOT NULL;
ALTER TABLE public.t_patient ALTER COLUMN f_patient_nation_id SET DEFAULT '99';
ALTER TABLE public.t_patient ALTER COLUMN f_patient_religion_id SET NOT NULL;
ALTER TABLE public.t_patient ALTER COLUMN f_patient_religion_id SET DEFAULT '9';
ALTER TABLE public.t_patient ALTER COLUMN f_patient_family_status_id SET NOT NULL;
ALTER TABLE public.t_patient ALTER COLUMN f_patient_family_status_id SET DEFAULT '2';
ALTER TABLE public.t_patient ALTER COLUMN f_patient_area_status_id TYPE varchar(1) USING f_patient_area_status_id::varchar;
ALTER TABLE public.t_patient ALTER COLUMN f_patient_area_status_id SET NOT NULL;
ALTER TABLE public.t_patient ALTER COLUMN f_patient_area_status_id SET DEFAULT '1';
ALTER TABLE public.t_patient ALTER COLUMN f_patient_relation_id SET DEFAULT '00';
ALTER TABLE public.t_patient ALTER COLUMN patient_contact_sex_id SET DEFAULT '3';
ALTER TABLE public.t_patient ALTER COLUMN patient_birthday_true TYPE varchar(1) USING patient_birthday_true::varchar;
ALTER TABLE public.t_patient ALTER COLUMN patient_birthday_true SET NOT NULL;
ALTER TABLE public.t_patient ALTER COLUMN patient_birthday_true SET DEFAULT '1';
ALTER TABLE public.t_patient ALTER COLUMN patient_record_date_time TYPE varchar(19) USING patient_record_date_time::varchar;
ALTER TABLE public.t_patient ALTER COLUMN patient_record_date_time SET NOT NULL;
ALTER TABLE public.t_patient ALTER COLUMN patient_record_date_time SET DEFAULT ((to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'));
ALTER TABLE public.t_patient ALTER COLUMN patient_staff_record TYPE varchar(30) USING patient_staff_record::varchar;
ALTER TABLE public.t_patient ALTER COLUMN patient_staff_record SET NOT NULL;
ALTER TABLE public.t_patient ALTER COLUMN patient_staff_record SET DEFAULT '1570000000001';
ALTER TABLE public.t_patient ALTER COLUMN patient_active TYPE varchar(1) USING patient_active::varchar;
ALTER TABLE public.t_patient ALTER COLUMN patient_active SET NOT NULL;
ALTER TABLE public.t_patient ALTER COLUMN patient_active SET DEFAULT '1';
ALTER TABLE public.t_patient ALTER COLUMN t_health_family_id SET NOT NULL;
ALTER TABLE public.t_patient ALTER COLUMN patient_is_other_country TYPE varchar(1) USING patient_is_other_country::varchar;
ALTER TABLE public.t_patient ALTER COLUMN patient_is_other_country SET NOT NULL;
ALTER TABLE public.t_patient ALTER COLUMN patient_is_other_country SET DEFAULT '0';
ALTER TABLE public.t_patient ALTER COLUMN patient_other_country_address SET DEFAULT '';
ALTER TABLE public.t_patient ALTER COLUMN patient_xn SET NOT NULL;
ALTER TABLE public.t_patient ALTER COLUMN patient_xn SET DEFAULT ''::character varying;
ALTER TABLE public.t_patient ALTER COLUMN patient_father_pid SET NOT NULL;
ALTER TABLE public.t_patient ALTER COLUMN patient_father_pid SET DEFAULT ''::character varying;
ALTER TABLE public.t_patient ALTER COLUMN patient_mather_pid SET NOT NULL;
ALTER TABLE public.t_patient ALTER COLUMN patient_mather_pid SET DEFAULT ''::character varying;
ALTER TABLE public.t_patient ALTER COLUMN patient_couple_pid SET NOT NULL;
ALTER TABLE public.t_patient ALTER COLUMN patient_couple_pid SET DEFAULT ''::character varying;



ALTER TABLE public.t_visit ALTER COLUMN f_visit_type_id TYPE varchar(1) USING f_visit_type_id::varchar;
ALTER TABLE public.t_visit ALTER COLUMN f_visit_type_id SET NOT NULL;
ALTER TABLE public.t_visit ALTER COLUMN f_visit_type_id SET DEFAULT '0';
ALTER TABLE public.t_visit ALTER COLUMN visit_begin_visit_time TYPE varchar(19) USING visit_begin_visit_time::varchar;
ALTER TABLE public.t_visit ALTER COLUMN visit_begin_visit_time SET NOT NULL;
ALTER TABLE public.t_visit ALTER COLUMN visit_begin_visit_time SET DEFAULT ((to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'));
ALTER TABLE public.t_visit ALTER COLUMN visit_locking TYPE varchar(1) USING visit_locking::varchar;
ALTER TABLE public.t_visit ALTER COLUMN visit_locking SET NOT NULL;
ALTER TABLE public.t_visit ALTER COLUMN visit_locking SET DEFAULT '0';
ALTER TABLE public.t_visit ALTER COLUMN f_visit_status_id TYPE varchar(1) USING f_visit_status_id::varchar;
ALTER TABLE public.t_visit ALTER COLUMN f_visit_status_id SET NOT NULL;
ALTER TABLE public.t_visit ALTER COLUMN f_visit_status_id SET DEFAULT '1';
ALTER TABLE public.t_visit ALTER COLUMN visit_deny_allergy TYPE varchar(1) USING visit_deny_allergy::varchar;
ALTER TABLE public.t_visit ALTER COLUMN visit_first_visit TYPE varchar(1) USING visit_first_visit::varchar;
ALTER TABLE public.t_visit ALTER COLUMN visit_first_visit SET NOT NULL;
ALTER TABLE public.t_visit ALTER COLUMN visit_pcu_service SET NOT NULL;
ALTER TABLE public.t_visit ALTER COLUMN visit_hospital_service SET NOT NULL;
ALTER TABLE public.t_visit ALTER COLUMN visit_lab_status_id SET NOT NULL;
ALTER TABLE public.t_visit ALTER COLUMN visit_ncd SET NOT NULL;
ALTER TABLE public.t_visit ALTER COLUMN f_refer_cause_id SET NOT NULL;
ALTER TABLE public.t_visit ALTER COLUMN f_emergency_status_id SET NOT NULL;
ALTER TABLE public.t_visit ALTER COLUMN visit_have_appointment SET NOT NULL;
ALTER TABLE public.t_visit ALTER COLUMN visit_have_admit SET NOT NULL;
ALTER TABLE public.t_visit ALTER COLUMN visit_have_refer SET NOT NULL;
ALTER TABLE public.t_visit ALTER COLUMN visit_pcu_service TYPE varchar(1) USING visit_pcu_service::varchar;
ALTER TABLE public.t_visit ALTER COLUMN visit_hospital_service TYPE varchar(1) USING visit_hospital_service::varchar;
ALTER TABLE public.t_visit ALTER COLUMN visit_lab_status_id TYPE varchar(1) USING visit_lab_status_id::varchar;
ALTER TABLE public.t_visit ALTER COLUMN visit_ncd TYPE varchar(1) USING visit_ncd::varchar;
ALTER TABLE public.t_visit ALTER COLUMN f_refer_cause_id TYPE varchar(1) USING f_refer_cause_id::varchar;
ALTER TABLE public.t_visit ALTER COLUMN f_emergency_status_id TYPE varchar(1) USING f_emergency_status_id::varchar;
ALTER TABLE public.t_visit ALTER COLUMN visit_have_appointment TYPE varchar(1) USING visit_have_appointment::varchar;
ALTER TABLE public.t_visit ALTER COLUMN visit_have_admit TYPE varchar(1) USING visit_have_admit::varchar;
ALTER TABLE public.t_visit ALTER COLUMN visit_have_refer TYPE varchar(1) USING visit_have_refer::varchar;
ALTER TABLE public.t_visit ALTER COLUMN visit_record_date_time TYPE varchar(19) USING visit_record_date_time::varchar;
ALTER TABLE public.t_visit ALTER COLUMN visit_record_date_time SET NOT NULL;
ALTER TABLE public.t_visit ALTER COLUMN visit_record_date_time SET DEFAULT ((to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'));
ALTER TABLE public.t_visit ALTER COLUMN visit_record_staff TYPE varchar(30) USING visit_record_staff::varchar;
ALTER TABLE public.t_visit ALTER COLUMN visit_record_staff SET NOT NULL;
ALTER TABLE public.t_visit ALTER COLUMN visit_record_staff SET DEFAULT '1570000000001';
ALTER TABLE public.t_visit ALTER COLUMN service_location TYPE varchar(1) USING service_location::varchar;
ALTER TABLE public.t_visit ALTER COLUMN service_location SET NOT NULL;
ALTER TABLE public.t_visit ALTER COLUMN service_location SET DEFAULT '1'::character varying;
ALTER TABLE public.t_visit ALTER COLUMN visit_ipd_discharge_status TYPE varchar(1) USING visit_ipd_discharge_status::varchar;
ALTER TABLE public.t_visit ALTER COLUMN visit_ipd_discharge_status SET NOT NULL;
ALTER TABLE public.t_visit ALTER COLUMN visit_ipd_discharge_status SET DEFAULT '';
ALTER TABLE public.t_visit ALTER COLUMN visit_money_discharge_status SET NOT NULL;
ALTER TABLE public.t_visit ALTER COLUMN visit_money_discharge_status SET DEFAULT '';
ALTER TABLE public.t_visit ALTER COLUMN visit_doctor_discharge_status TYPE varchar(1) USING visit_doctor_discharge_status::varchar;
ALTER TABLE public.t_visit ALTER COLUMN visit_doctor_discharge_status SET NOT NULL;
ALTER TABLE public.t_visit ALTER COLUMN visit_doctor_discharge_status SET DEFAULT '';
ALTER TABLE public.t_visit ALTER COLUMN visit_dx SET NOT NULL;
ALTER TABLE public.t_visit ALTER COLUMN visit_dx SET DEFAULT '';
ALTER TABLE public.t_visit ALTER COLUMN visit_observe SET NOT NULL;
ALTER TABLE public.t_visit ALTER COLUMN visit_observe SET DEFAULT '';

ALTER TABLE public.t_visit_payment ALTER COLUMN visit_payment_active TYPE varchar(1) USING visit_payment_active::varchar;
ALTER TABLE public.t_visit_payment ALTER COLUMN visit_payment_active SET NOT NULL;
ALTER TABLE public.t_visit_payment ALTER COLUMN visit_payment_staff_record TYPE varchar(30) USING visit_payment_staff_record::varchar;
ALTER TABLE public.t_visit_payment ALTER COLUMN visit_payment_staff_record SET NOT NULL;
ALTER TABLE public.t_visit_payment ALTER COLUMN visit_payment_staff_record SET DEFAULT '1570000000001'::character varying;
ALTER TABLE public.t_visit_payment ALTER COLUMN visit_payment_record_date_time TYPE varchar(19) USING visit_payment_record_date_time::varchar;
ALTER TABLE public.t_visit_payment ALTER COLUMN visit_payment_record_date_time SET NOT NULL;
ALTER TABLE public.t_visit_payment ALTER COLUMN visit_payment_record_date_time SET DEFAULT ((to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'));
ALTER TABLE public.t_visit_payment ALTER COLUMN visit_payment_priority TYPE varchar(1) USING visit_payment_priority::varchar;
ALTER TABLE public.t_visit_payment ALTER COLUMN visit_payment_priority SET NOT NULL;
ALTER TABLE public.t_visit_payment ALTER COLUMN visit_payment_priority SET DEFAULT '0';
ALTER TABLE public.t_visit_payment ALTER COLUMN visit_payment_used_money_limit TYPE varchar(1) USING visit_payment_used_money_limit::varchar;
ALTER TABLE public.t_visit_payment ALTER COLUMN visit_payment_used_money_limit SET NOT NULL;
ALTER TABLE public.t_visit_payment ALTER COLUMN visit_payment_used_money_limit SET DEFAULT '0';
ALTER TABLE public.t_visit_payment ALTER COLUMN visit_payment_card_issue_date TYPE varchar(10) USING visit_payment_card_issue_date::varchar;
ALTER TABLE public.t_visit_payment ALTER COLUMN visit_payment_card_issue_date SET NOT NULL;
ALTER TABLE public.t_visit_payment ALTER COLUMN visit_payment_card_issue_date SET DEFAULT ((to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD'));
ALTER TABLE public.t_visit_payment ALTER COLUMN visit_payment_main_hospital TYPE varchar(5) USING visit_payment_main_hospital::varchar;
ALTER TABLE public.t_visit_payment ALTER COLUMN visit_payment_sub_hospital TYPE varchar(5) USING visit_payment_sub_hospital::varchar;

ALTER TABLE public.t_visit_service ALTER COLUMN sender_id TYPE varchar(30) USING sender_id::varchar;
ALTER TABLE public.t_visit_service ALTER COLUMN sender_id SET NOT NULL;
ALTER TABLE public.t_visit_service ALTER COLUMN sender_id SET DEFAULT '1570000000001';
ALTER TABLE public.t_visit_service ALTER COLUMN assign_date_time TYPE varchar(19) USING assign_date_time::varchar;
ALTER TABLE public.t_visit_service ALTER COLUMN assign_date_time SET NOT NULL;
ALTER TABLE public.t_visit_service ALTER COLUMN assign_date_time SET DEFAULT ((to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'));
ALTER TABLE public.t_visit_service ALTER COLUMN f_visit_service_status_id TYPE varchar(1) USING f_visit_service_status_id::varchar;
ALTER TABLE public.t_visit_service ALTER COLUMN f_visit_service_status_id SET NOT NULL;
ALTER TABLE public.t_visit_service ALTER COLUMN f_visit_service_status_id SET DEFAULT '1';
ALTER TABLE public.t_visit_service ALTER COLUMN visit_service_treatment_date_time TYPE varchar(19) USING visit_service_treatment_date_time::varchar;
ALTER TABLE public.t_visit_service ALTER COLUMN visit_service_treatment_date_time SET NOT NULL;
ALTER TABLE public.t_visit_service ALTER COLUMN visit_service_treatment_date_time SET DEFAULT ((to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'));
ALTER TABLE public.t_visit_service ALTER COLUMN visit_service_staff_doctor SET DEFAULT '';

ALTER TABLE public.t_visit_queue_map ALTER COLUMN visit_queue_map_active TYPE varchar(1) USING visit_queue_map_active::varchar;
ALTER TABLE public.t_visit_queue_map ALTER COLUMN visit_queue_map_active SET NOT NULL;
ALTER TABLE public.t_visit_queue_map ALTER COLUMN visit_queue_map_active SET DEFAULT '1';

ALTER TABLE public.t_visit_queue_transfer ALTER COLUMN patient_drugallergy TYPE varchar(1) USING patient_drugallergy::varchar;
ALTER TABLE public.t_visit_queue_transfer ALTER COLUMN patient_drugallergy SET NOT NULL;
ALTER TABLE public.t_visit_queue_transfer ALTER COLUMN patient_drugallergy SET DEFAULT '0';
ALTER TABLE public.t_visit_queue_transfer ALTER COLUMN visit_locking TYPE varchar(1) USING visit_locking::varchar;
ALTER TABLE public.t_visit_queue_transfer ALTER COLUMN visit_locking SET NOT NULL;
ALTER TABLE public.t_visit_queue_transfer ALTER COLUMN visit_locking SET DEFAULT '0';
ALTER TABLE public.t_visit_queue_transfer ALTER COLUMN f_visit_type_id TYPE varchar(1) USING f_visit_type_id::varchar;
ALTER TABLE public.t_visit_queue_transfer ALTER COLUMN f_visit_type_id SET NOT NULL;
ALTER TABLE public.t_visit_queue_transfer ALTER COLUMN f_visit_type_id SET DEFAULT '0';
ALTER TABLE public.t_visit_queue_transfer ALTER COLUMN assign_date_time TYPE varchar(19) USING assign_date_time::varchar;
ALTER TABLE public.t_visit_queue_transfer ALTER COLUMN assign_date_time SET NOT NULL;
ALTER TABLE public.t_visit_queue_transfer ALTER COLUMN assign_date_time SET DEFAULT ((to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'));

ALTER TABLE public.t_order ALTER COLUMN order_charge_complete TYPE varchar(1) USING order_charge_complete::varchar;
ALTER TABLE public.t_order ALTER COLUMN order_charge_complete SET NOT NULL;
ALTER TABLE public.t_order ALTER COLUMN order_charge_complete SET DEFAULT '0';
ALTER TABLE public.t_order ALTER COLUMN f_order_status_id SET NOT NULL;
ALTER TABLE public.t_order ALTER COLUMN f_order_status_id SET DEFAULT '0';
ALTER TABLE public.t_order ALTER COLUMN order_secret SET NOT NULL;
ALTER TABLE public.t_order ALTER COLUMN order_secret SET DEFAULT '0';
ALTER TABLE public.t_order ALTER COLUMN order_continue TYPE varchar(1) USING order_continue::varchar;
ALTER TABLE public.t_order ALTER COLUMN order_continue SET NOT NULL;
ALTER TABLE public.t_order ALTER COLUMN order_continue SET DEFAULT '0';
ALTER TABLE public.t_order ALTER COLUMN order_refer_out TYPE varchar(1) USING order_refer_out::varchar;
ALTER TABLE public.t_order ALTER COLUMN order_refer_out SET NOT NULL;
ALTER TABLE public.t_order ALTER COLUMN order_refer_out SET DEFAULT '0';
ALTER TABLE public.t_order ALTER COLUMN order_request TYPE varchar(1) USING order_request::varchar;
ALTER TABLE public.t_order ALTER COLUMN order_request SET NOT NULL;
ALTER TABLE public.t_order ALTER COLUMN order_request SET DEFAULT '0';
ALTER TABLE public.t_order ALTER COLUMN order_staff_order TYPE varchar(30) USING order_staff_order::varchar;
ALTER TABLE public.t_order ALTER COLUMN order_staff_order SET NOT NULL;
ALTER TABLE public.t_order ALTER COLUMN order_staff_order SET DEFAULT '1570000000001';
ALTER TABLE public.t_order ALTER COLUMN order_date_time TYPE varchar(19) USING order_date_time::varchar;
ALTER TABLE public.t_order ALTER COLUMN order_date_time SET NOT NULL;
ALTER TABLE public.t_order ALTER COLUMN order_date_time SET DEFAULT ((to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'));
ALTER TABLE public.t_order ALTER COLUMN order_complete TYPE varchar(1) USING order_complete::varchar;
ALTER TABLE public.t_order ALTER COLUMN order_complete SET NOT NULL;
ALTER TABLE public.t_order ALTER COLUMN order_complete SET DEFAULT '0';

ALTER TABLE public.b_item_price ALTER COLUMN item_price_number TYPE varchar(30) USING item_price_number::varchar;
ALTER TABLE public.b_item_price ALTER COLUMN item_price_number SET NOT NULL;
ALTER TABLE public.b_item_price ALTER COLUMN item_price_number SET DEFAULT '';

ALTER TABLE public.t_order_drug ALTER COLUMN order_drug_modifier TYPE varchar(30) USING order_drug_modifier::varchar;
ALTER TABLE public.t_order_drug ALTER COLUMN order_drug_modifier SET NOT NULL;
ALTER TABLE public.t_order_drug ALTER COLUMN order_drug_modifier SET DEFAULT 1570000000001;
ALTER TABLE public.t_order_drug ALTER COLUMN order_drug_modify_datetime TYPE varchar(19) USING order_drug_modify_datetime::varchar;
ALTER TABLE public.t_order_drug ALTER COLUMN order_drug_modify_datetime SET NOT NULL;
ALTER TABLE public.t_order_drug ALTER COLUMN order_drug_modify_datetime SET DEFAULT ((to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'));
ALTER TABLE public.t_order_drug ALTER COLUMN order_drug_order_status TYPE varchar(1) USING order_drug_order_status::varchar;
ALTER TABLE public.t_order_drug ALTER COLUMN order_drug_order_status SET NOT NULL;
ALTER TABLE public.t_order_drug ALTER COLUMN order_drug_order_status SET DEFAULT '0';

ALTER TABLE public.t_allergy_drug_order ALTER COLUMN user_record TYPE varchar(30) USING user_record::varchar;
ALTER TABLE public.t_allergy_drug_order ALTER COLUMN user_record SET DEFAULT '1570000000001';
ALTER TABLE public.t_allergy_drug_order ALTER COLUMN record_date_time SET DEFAULT ((to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'));

ALTER TABLE public.t_visit_queue_lab ALTER COLUMN visit_queue_lab_number_order TYPE varchar(4) USING visit_queue_lab_number_order::varchar;
ALTER TABLE public.t_visit_queue_lab ALTER COLUMN visit_queue_lab_number_order SET NOT NULL;
ALTER TABLE public.t_visit_queue_lab ALTER COLUMN visit_queue_lab_number_order SET DEFAULT '1';
ALTER TABLE public.t_visit_queue_lab ALTER COLUMN assign_date_time TYPE varchar(19) USING assign_date_time::varchar;
ALTER TABLE public.t_visit_queue_lab ALTER COLUMN assign_date_time SET NOT NULL;
ALTER TABLE public.t_visit_queue_lab ALTER COLUMN assign_date_time SET DEFAULT ((to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'));

ALTER TABLE public.t_visit_queue_xray ALTER COLUMN visit_queue_xray_number_order TYPE varchar(4) USING visit_queue_xray_number_order::varchar;
ALTER TABLE public.t_visit_queue_xray ALTER COLUMN visit_queue_xray_number_order SET NOT NULL;
ALTER TABLE public.t_visit_queue_xray ALTER COLUMN visit_queue_xray_number_order SET DEFAULT '1';
ALTER TABLE public.t_visit_queue_xray ALTER COLUMN assign_date_time TYPE varchar(19) USING assign_date_time::varchar;
ALTER TABLE public.t_visit_queue_xray ALTER COLUMN assign_date_time SET NOT NULL;
ALTER TABLE public.t_visit_queue_xray ALTER COLUMN assign_date_time SET DEFAULT ((to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'));

ALTER TABLE public.t_result_xray ALTER COLUMN record_date_time TYPE varchar(19) USING record_date_time::varchar;
ALTER TABLE public.t_result_xray ALTER COLUMN record_date_time SET NOT NULL;
ALTER TABLE public.t_result_xray ALTER COLUMN record_date_time SET DEFAULT ((to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'));
ALTER TABLE public.t_result_xray ALTER COLUMN result_xray_active TYPE varchar(1) USING result_xray_active::varchar;
ALTER TABLE public.t_result_xray ALTER COLUMN result_xray_active SET NOT NULL;
ALTER TABLE public.t_result_xray ALTER COLUMN result_xray_active SET DEFAULT '1';
ALTER TABLE public.t_result_xray ALTER COLUMN result_xray_complete TYPE varchar(1) USING result_xray_complete::varchar;
ALTER TABLE public.t_result_xray ALTER COLUMN result_xray_complete SET NOT NULL;
ALTER TABLE public.t_result_xray ALTER COLUMN result_xray_complete SET DEFAULT '0';
ALTER TABLE public.t_result_xray ALTER COLUMN result_xray_xn SET DEFAULT '';


ALTER TABLE public.t_result_lab DROP COLUMN t_patient_id;
ALTER TABLE public.t_result_lab ALTER COLUMN result_lab_active TYPE varchar(1) USING result_lab_active::varchar;
ALTER TABLE public.t_result_lab ALTER COLUMN result_lab_active SET NOT NULL;
ALTER TABLE public.t_result_lab ALTER COLUMN result_lab_active SET DEFAULT '1';
ALTER TABLE public.t_result_lab ALTER COLUMN result_lab_complete TYPE varchar(1) USING result_lab_complete::varchar;
ALTER TABLE public.t_result_lab ALTER COLUMN result_lab_complete SET NOT NULL;
ALTER TABLE public.t_result_lab ALTER COLUMN result_lab_complete SET DEFAULT '0';
ALTER TABLE public.t_result_lab ALTER COLUMN f_lab_result_type_id TYPE varchar(1) USING f_lab_result_type_id::varchar;
ALTER TABLE public.t_result_lab ALTER COLUMN f_lab_result_type_id SET NOT NULL;
ALTER TABLE public.t_result_lab ALTER COLUMN f_lab_result_type_id SET DEFAULT 2;
ALTER TABLE public.t_result_lab ALTER COLUMN normal_range_type SET DEFAULT '0';

-- report
insert into f_specialpp_code values ('1BD099','การควบคุมพฤติกรรมในเด็กโดยไม่ใช้ยา');
insert into f_specialpp_code values ('1BD199','การควบคุมพฤติกรรมในเด็กโดยใช้ยา');

-- U78.sql
insert into b_icd10 values ('550010000000','U78.000','Yindisease','','','0','1','โรคอิน','0');
insert into b_icd10 values ('550010000001','U78.001','Yangdisease','','','0','1','โรคหยาง','0');
insert into b_icd10 values ('550010000002','U78.002','Colddamage','','','0','1','โรคซางหาน','0');
insert into b_icd10 values ('550010000003','U78.003','Miscellaneousdisease','','','0','1','โรคอื่นๆ(จ๋าปิ้ง)','0');
insert into b_icd10 values ('550010000004','U78.004','Six-meridiandisease','','','0','1','โรคของเส้นลมปราณทั้งหก','0');
insert into b_icd10 values ('550010000005','U78.010','Dampnessdamage','','','0','1','โรคจากชื้น','0');
insert into b_icd10 values ('550010000006','U78.011','Stomachductpain','','','0','1','เจ็บกระเพาะอาหาร','0');
insert into b_icd10 values ('550010000007','U78.012','Gastricstuffness','','','0','1','ท้องอืด','0');
insert into b_icd10 values ('550010000008','U78.013','Vomiting','','','0','1','อาเจียน','0');
insert into b_icd10 values ('550010000009','U78.014','Acidvomiting','','','0','1','กรดไหลย้อน','0');
insert into b_icd10 values ('550010000010','U78.015','Gastricupset','','','0','1','ไม่สบายท้อง','0');
insert into b_icd10 values ('550010000011','U78.016','Hiccup','','','0','1','สะอึก','0');
insert into b_icd10 values ('550010000012','U78.017','Dysphagia-occlusion','','','0','1','กลืนอาหารไม่ลง','0');
insert into b_icd10 values ('550010000013','U78.018','Stomachreflux','','','0','1','กระเพาะอาหารอุดตัน(อาหารในกระเพาะไหลย้อน)','0');
insert into b_icd10 values ('550010000014','U78.019','Blockandrepulsion(disease)','','','0','1','ปัสสาวะไม่ออกอาเจียนไม่หยุด','0');
insert into b_icd10 values ('550010000015','U78.020','Dysentery','','','0','1','บิด','0');
insert into b_icd10 values ('550010000016','U78.021','Epidemictoxindysentery','','','0','1','บิดเป็นพิษ','0');
insert into b_icd10 values ('550010000017','U78.022','Intermittentdysentery','','','0','1','บิดเรื้อรัง','0');
insert into b_icd10 values ('550010000018','U78.023','Food-denyingdysentery','','','0','1','บิดไม่ยอมรับประทานอาหาร(โรคบิดที่มีอาการไม่อยากรับประทานอาหาร)','0');
insert into b_icd10 values ('550010000019','U78.024','Cholera','','','0','1','อหิวาตกโรค','0');
insert into b_icd10 values ('550010000020','U78.025','Drycholera','','','0','1','อหิวาตกโรคแบบแห้ง','0');
insert into b_icd10 values ('550010000021','U78.026','Diarrhea','','','0','1','อุจจาระร่วง','0');
insert into b_icd10 values ('550010000022','U78.027','Chronicdiarrhea','','','0','1','ท้องเสียเรื้อรัง','0');
insert into b_icd10 values ('550010000023','U78.028','Constipation','','','0','1','ท้องผูก','0');
insert into b_icd10 values ('550010000024','U78.029','Splenicconstipation','','','0','1','ท้องผูกที่มีสาเหตุจากม้าม','0');
insert into b_icd10 values ('550010000025','U78.030','Stuffinessofexcesstype','','','0','1','แน่นแบบแกร่ง','0');
insert into b_icd10 values ('550010000026','U78.031','Stuffinessofdeficiencytype','','','0','1','แน่นแบบพร่อง','0');
insert into b_icd10 values ('550010000027','U78.032','Visceralbind','','','0','1','แน่นตึงในอวัยวะภายใน','0');
insert into b_icd10 values ('550010000028','U78.033','Fooddamage','','','0','1','อาหารไม่ย่อย','0');
insert into b_icd10 values ('550010000029','U78.034','Liverfixity','','','0','1','ตับติดขัด','0');
insert into b_icd10 values ('550010000030','U78.035','Runningpiglet','','','0','1','ลมดันในท้องดันขึ้น','0');
insert into b_icd10 values ('550010000031','U78.036','Aggregation-accumulation','','','0','1','ก้อนบวมในท้อง','0');
insert into b_icd10 values ('550010000032','U78.037','Hernia','','','0','1','ไส้เลื่อน','0');
insert into b_icd10 values ('550010000033','U78.038','Abdominalpain','','','0','1','ปวดท้อง','0');
insert into b_icd10 values ('550010000034','U78.039','Coldabdominalcolic','','','0','1','หานซ่าน','0');
insert into b_icd10 values ('550010000035','U78.040','Malaria','','','0','1','มาลาเรีย','0');
insert into b_icd10 values ('550010000036','U78.041','Miasmicmalaria','','','0','1','มาเลเรียขึ้นสมอง','0');
insert into b_icd10 values ('550010000037','U78.050','Seasonalepidemic','','','0','1','โรคระบาดตามฤดูกาล','0');
insert into b_icd10 values ('550010000038','U78.051','Pestilence','','','0','1','โรคติดต่อร้ายแรง','0');
insert into b_icd10 values ('550010000039','U78.052','Erysipelasfacialis','','','0','1','โรคไฟลามทุ่งที่ใบหน้า','0');
insert into b_icd10 values ('550010000040','U78.053','Warmtoxin','','','0','1','เวินตู๋','0');
insert into b_icd10 values ('550010000041','U78.054','Warmdisease','','','0','1','เวินปิ้ง','0');
insert into b_icd10 values ('550010000042','U78.055','Springwarmth','','','0','1','ชุนเวิน','0');
insert into b_icd10 values ('550010000043','U78.056','Wind-warmth','','','0','1','เฟิงเวิน','0');
insert into b_icd10 values ('550010000044','U78.057','Dampness-warmth','','','0','1','ซือเวิน','0');
insert into b_icd10 values ('550010000045','U78.060','Summerheatstroke','','','0','1','เป็นลมแดด','0');
insert into b_icd10 values ('550010000046','U78.061','Summerheataffliction','','','0','1','หวัดในฤดูร้อน','0');
insert into b_icd10 values ('550010000047','U78.062','Summerheatdamage','','','0','1','โรคจากอากาศร้อน','0');
insert into b_icd10 values ('550010000048','U78.063','Latentsummerheat','','','0','1','ฝูสู่','0');
insert into b_icd10 values ('550010000049','U78.064','Summerheatconvulsions','','','0','1','เป็นลมหมดสติจากอากาศร้อน','0');
insert into b_icd10 values ('550010000050','U78.065','Summerheat-warmth','','','0','1','สู่เวิน','0');
insert into b_icd10 values ('550010000051','U78.066','Summerheat-wind','','','0','1','ชักจากไข้สูง','0');
insert into b_icd10 values ('550010000052','U78.070','Wheezinganddyspnea','','','0','1','โรคหอบหืด','0');
insert into b_icd10 values ('550010000053','U78.071','Wheezing','','','0','1','หืด','0');
insert into b_icd10 values ('550010000054','U78.072','Coldwheezing','','','0','1','หืดแบบภาวะเย็น','0');
insert into b_icd10 values ('550010000055','U78.073','Heatwheezing','','','0','1','หืดแบบภาวะร้อน','0');
insert into b_icd10 values ('550010000056','U78.074','Dyspnea','','','0','1','หอบ','0');
insert into b_icd10 values ('550010000057','U78.075','Dyspneaofexcesstype','','','0','1','หอบแบบภาวะแกร่ง','0');
insert into b_icd10 values ('550010000058','U78.076','Dyspneaofdeficiencytype','','','0','1','หอบแบบภาวะพร่อง','0');
insert into b_icd10 values ('550010000059','U78.077','Shortnessofbreath','','','0','1','หายใจสั้น','0');
insert into b_icd10 values ('550010000060','U78.078','Shortageofqi','','','0','1','ชี่ไม่พอ','0');
insert into b_icd10 values ('550010000061','U78.079','Pulm-pitqi','','','0','1','ความรู้สึกมีของติดคอ','0');
insert into b_icd10 values ('550010000062','U78.080','Cough','','','0','1','โรคไอ','0');
insert into b_icd10 values ('550010000063','U78.081','Drycough','','','0','1','ไอแห้ง','0');
insert into b_icd10 values ('550010000064','U78.082','Fifth-watchcough','','','0','1','อาการไอที่เกิดขึ้นหรืออาจเกิดขึ้นทุกวันก่อนรุ่งสาง','0');
insert into b_icd10 values ('550010000065','U78.083','Lungcough','','','0','1','ไอเรื้อรัง','0');
insert into b_icd10 values ('550010000066','U78.084','Lungdistention','','','0','1','ปอดโป่งพอง','0');
insert into b_icd10 values ('550010000067','U78.085','Lungabscess','','','0','1','ปอดอักเสบ(ฝีในปอด)','0');
insert into b_icd10 values ('550010000068','U78.086','Lungconsumption','','','0','1','วัณโรคปอด','0');
insert into b_icd10 values ('550010000069','U78.087','Lungatrophy','','','0','1','ปอดแฟบ','0');
insert into b_icd10 values ('550010000070','U78.090','Commoncold','','','0','1','หวัด','0');
insert into b_icd10 values ('550010000071','U78.091','Influenza','','','0','1','ไข้หวัดใหญ่','0');
insert into b_icd10 values ('550010000072','U78.092','Snoring','','','0','1','กรน','0');
insert into b_icd10 values ('550010000073','U78.100','Palpitations','','','0','1','ใจสั่น','0');
insert into b_icd10 values ('550010000074','U78.101','Chestimpediment','','','0','1','เจ็บหน้าอก','0');
insert into b_icd10 values ('550010000075','U78.102','Chestbind','','','0','1','แน่นในอก','0');
insert into b_icd10 values ('550010000076','U78.103','Heartpain','','','0','1','เจ็บที่หัวใจ','0');
insert into b_icd10 values ('550010000077','U78.104','Suddenheartpain','','','0','1','เจ็บที่หัวใจเฉียบพลัน','0');
insert into b_icd10 values ('550010000078','U78.105','Trueheartpain','','','0','1','เจ็บหน้าอกจากโรคของหัวใจโดยแท้','0');
insert into b_icd10 values ('550010000079','U78.110','Apoplecticwindstroke','','','0','1','โรคหลอดเลือดสมองที่เกิดจากลมภายใน','0');
insert into b_icd10 values ('550010000080','U78.111','Prodromeofwindstroke','','','0','1','อาการเตือนโรคหลอดเลือดสมอง','0');
insert into b_icd10 values ('550010000081','U78.112','Sequelaeofwindstroke','','','0','1','โรคตามมาจากโรคหลอดเลือดสมอง','0');
insert into b_icd10 values ('550010000082','U78.113','Collateralstroke','','','0','1','โรคหลอดเลือดสมองในระดับเส้นลมปราณแขนง(เส้นลั่ว)','0');
insert into b_icd10 values ('550010000083','U78.114','Meridianstroke','','','0','1','โรคหลอดเลือดสมองในระดับเส้นลมปราณหลัก(เส้นจิง)','0');
insert into b_icd10 values ('550010000084','U78.115','Bowelstroke','','','0','1','โรคหลอดเลือดสมองในระดับอวัยวะกลวง','0');
insert into b_icd10 values ('550010000085','U78.116','Visceralstroke','','','0','1','โรคหลอดเลือดสมองในระดับอวัยวะตัน','0');
insert into b_icd10 values ('550010000086','U78.117','Hemiplegia','','','0','1','อัมพาตครึ่งซีก','0');
insert into b_icd10 values ('550010000087','U78.120','Dizziness','','','0','1','เวียนศีรษะ','0');
insert into b_icd10 values ('550010000088','U78.121','Lossofconsciousness','','','0','1','หมดสติ','0');
insert into b_icd10 values ('550010000089','U78.122','Numbness','','','0','1','ชาไม่รู้สึก','0');
insert into b_icd10 values ('550010000090','U78.123','Insomnia','','','0','1','นอนไม่หลับ','0');
insert into b_icd10 values ('550010000091','U78.124','Forgetfulness','','','0','1','หลงลืม','0');
insert into b_icd10 values ('550010000092','U78.125','Dementia','','','0','1','ภาวะสมองเสื่อม','0');
insert into b_icd10 values ('550010000093','U78.126','Yangepilepsy','','','0','1','ลมบ้าหมูแบบหยาง','0');
insert into b_icd10 values ('550010000094','U78.127','Yinepilepsy','','','0','1','ลมบ้าหมูแบบอิน','0');
insert into b_icd10 values ('550010000095','U78.128','Tremor','','','0','1','โรคสั่น','0');
insert into b_icd10 values ('550010000096','U78.129','Tetanus','','','0','1','บาดทะยัก','0');
insert into b_icd10 values ('550010000097','U78.130','Depression','','','0','1','โรคซึมเศร้า','0');
insert into b_icd10 values ('550010000098','U78.131','Depressivepsychosis','','','0','1','โรคจิตแบบซึมเศร้า','0');
insert into b_icd10 values ('550010000099','U78.132','Manicpsychosis','','','0','1','โรคจิตแบบคลุ้มคลั่ง','0');
insert into b_icd10 values ('550010000100','U78.133','Lilydisease','','','0','1','โรคอยู่ไม่เป็นสุข','0');
insert into b_icd10 values ('550010000101','U78.134','Hysteria','','','0','1','ฮิสทีเรีย','0');
insert into b_icd10 values ('550010000102','U78.140','Jaundice','','','0','1','ดีซ่าน','0');
insert into b_icd10 values ('550010000103','U78.141','Acutejaundice','','','0','1','ดีซ่านเฉียบพลัน','0');
insert into b_icd10 values ('550010000104','U78.142','Yangjaundice','','','0','1','ดีซ่านแบบหยาง','0');
insert into b_icd10 values ('550010000105','U78.143','Yinjaundice','','','0','1','ดีซ่านแบบอิน','0');
insert into b_icd10 values ('550010000106','U78.144','Dietaryjaundice','','','0','1','ดีซ่านจากการรับประทานอาหาร','0');
insert into b_icd10 values ('550010000107','U78.145','Alcoholicjaundice','','','0','1','โรคดีซ่านจากสุรา','0');
insert into b_icd10 values ('550010000108','U78.146','Sallowdisease','','','0','1','โรคบวมเหลือง','0');
insert into b_icd10 values ('550010000109','U78.150','Gallbladderdistention','','','0','1','ภาวะแน่นท้องจากถุงน้ำดี','0');
insert into b_icd10 values ('550010000110','U78.151','Tympanites','','','0','1','ท้องมานน้ำ','0');
insert into b_icd10 values ('550010000111','U78.152','Cutaneousdistention','','','0','1','ผิวหนังบวม','0');
insert into b_icd10 values ('550010000112','U78.153','Distentionandfullness','','','0','1','ท้องแน่นตึง','0');
insert into b_icd10 values ('550010000113','U78.160','Edema','','','0','1','บวมน้ำ','0');
insert into b_icd10 values ('550010000114','U78.161','Yangedema','','','0','1','หยางสุ่ย','0');
insert into b_icd10 values ('550010000115','U78.162','Yinedema','','','0','1','อินสุ่ย','0');
insert into b_icd10 values ('550010000116','U78.163','Windedema','','','0','1','บวมน้ำแบบเฟิงสุ่ย(บวมน้ำเฉพาะที่)','0');
insert into b_icd10 values ('550010000117','U78.164','Skinedema','','','0','1','บวมน้ำแบบผีสุ่ย(บวมน้ำที่ผิวหนัง)','0');
insert into b_icd10 values ('550010000118','U78.165','Regularedema','','','0','1','บวมน้ำแบบเจิ้งสุ่ย(บวมน้ำทั้งตัวร่วมกับอาการหายใจขัด)','0');
insert into b_icd10 values ('550010000119','U78.166','Stonyedema','','','0','1','บวมน้ำแบบสือสุ่ย(บวมน้ำแบบแข็ง)','0');
insert into b_icd10 values ('550010000120','U78.170','Strangurydisease','','','0','1','โรคปัสสาวะขัด','0');
insert into b_icd10 values ('550010000121','U78.171','Heatstrangury','','','0','1','โรคปัสสาวะขัดจากพิษร้อน','0');
insert into b_icd10 values ('550010000122','U78.172','Stonestrangury','','','0','1','โรคนิ่วในทางเดินปัสสาวะ','0');
insert into b_icd10 values ('550010000123','U78.173','Qistrangury','','','0','1','ปัสสาวะขัดจากชี่ติดขัด','0');
insert into b_icd10 values ('550010000124','U78.174','Bloodstrangury','','','0','1','ปัสสาวะขัดปนเลือด','0');
insert into b_icd10 values ('550010000125','U78.175','Unctuousstrangury','','','0','1','ปัสสาวะขัดและขุ่นขาว','0');
insert into b_icd10 values ('550010000126','U78.176','Fatiguestrangury','','','0','1','ปัสสาวะขัดเรื้อรัง','0');
insert into b_icd10 values ('550010000127','U78.180','Spontaneousurination','','','0','1','ปัสสาวะปริมาณมากกว่าปกติ','0');
insert into b_icd10 values ('550010000128','U78.181','Difficulturination','','','0','1','ปัสสาวะลำบาก','0');
insert into b_icd10 values ('550010000129','U78.182','Inhibitedurination','','','0','1','ปัสสาวะไม่คล่อง','0');
insert into b_icd10 values ('550010000130','U78.183','Urinaryincontinence','','','0','1','กลั้นปัสสาวะไม่ได้','0');
insert into b_icd10 values ('550010000131','U78.184','Frequenturination','','','0','1','ปัสสาวะบ่อย','0');
insert into b_icd10 values ('550010000132','U78.185','Difficultpainfulurination','','','0','1','ปัสสาวะลำบากและมีอาการปวดเจ็บทางเดินปัสสาวะ','0');
insert into b_icd10 values ('550010000133','U78.186','Enuresis','','','0','1','ปัสสาวะรดที่นอน','0');
insert into b_icd10 values ('550010000134','U78.187','Whiteturbidity','','','0','1','ไป๋จั๋ว','0');
insert into b_icd10 values ('550010000135','U78.188','Turbidurine(disease)','','','0','1','ปัสสาวะขุ่น','0');
insert into b_icd10 values ('550010000136','U78.189','Dribblingurinaryblock(disease)','','','0','1','ปัสสาวะกะปริดกะปรอย','0');
insert into b_icd10 values ('550010000137','U78.190','Whiteooze','','','0','1','ไป๋อิ๋น','0');
insert into b_icd10 values ('550010000138','U78.191','Seminalemission(disease)','','','0','1','น้ำกามเคลื่อน','0');
insert into b_icd10 values ('550010000139','U78.192','Dreamemission(disease)','','','0','1','ฝันเปียก','0');
insert into b_icd10 values ('550010000140','U78.193','Spermatorrhea','','','0','1','น้ำกามเคลื่อนเอง','0');
insert into b_icd10 values ('550010000141','U78.194','Prematureejaculation(disease)','','','0','1','หลั่งเร็ว','0');
insert into b_icd10 values ('550010000142','U78.195','Impotence','','','0','1','เสื่อมสมรรถภาพทางเพศ','0');
insert into b_icd10 values ('550010000143','U78.196','Persistenterection','','','0','1','หยางเฉียง','0');
insert into b_icd10 values ('550010000144','U78.197','Seminalcold','','','0','1','จิงเหลิ่ง','0');
insert into b_icd10 values ('550010000145','U78.198','Seminalturbidity(disease)','','','0','1','ต่อมลูกหมากอักเสบ','0');
insert into b_icd10 values ('550010000146','U78.199','Yin-yangtransmission','','','0','1','อินหยางอี้','0');
insert into b_icd10 values ('550010000147','U78.200','Blooddisease','','','0','1','โรคเลือดคั่ง','0');
insert into b_icd10 values ('550010000148','U78.201','Nosebleed','','','0','1','เลือดกำเดาไหล','0');
insert into b_icd10 values ('550010000149','U78.202','Gumbleeding','','','0','1','เลือดออกบริเวณเหงือก','0');
insert into b_icd10 values ('550010000150','U78.203','Hematemesis','','','0','1','อาเจียนเป็นเลือด','0');
insert into b_icd10 values ('550010000151','U78.204','Hematochezia','','','0','1','ถ่ายอุจจาระเป็นเลือด','0');
insert into b_icd10 values ('550010000152','U78.205','Hematuria','','','0','1','ปัสสาวะปนเลือด','0');
insert into b_icd10 values ('550010000153','U78.206','Purpura','','','0','1','จ้ำเลือด','0');
insert into b_icd10 values ('550010000154','U78.207','Bloodamassment(disease)','','','0','1','โรคพิษร้อนเลือดคั่ง','0');
insert into b_icd10 values ('550010000155','U78.210','Phlegm-fluidretention','','','0','1','ถานอิ่น','0');
insert into b_icd10 values ('550010000156','U78.211','Pleuralfluidretention','','','0','1','เสวียนอิ่น','0');
insert into b_icd10 values ('550010000157','U78.212','Subcutaneousfluidretention','','','0','1','อี้อิ่น','0');
insert into b_icd10 values ('550010000158','U78.213','Thoracicfluidretention','','','0','1','จืออิ่น','0');
insert into b_icd10 values ('550010000159','U78.214','Recurrentfluidretention','','','0','1','ฝูอิ่น','0');
insert into b_icd10 values ('550010000160','U78.215','Mildfluidretention','','','0','1','เว่ยอิ่น','0');
insert into b_icd10 values ('550010000161','U78.216','Persistentfluidretention','','','0','1','หลิวอิ่น','0');
insert into b_icd10 values ('550010000162','U78.220','Wasting-thirst','','','0','1','เซียวเข่อ','0');
insert into b_icd10 values ('550010000163','U78.221','Upperwasting-thirst','','','0','1','ซ่างเซียว','0');
insert into b_icd10 values ('550010000164','U78.222','Middlewasting-thirst','','','0','1','จงเซียว','0');
insert into b_icd10 values ('550010000165','U78.223','Lowerwasting-thirst','','','0','1','เซี่ยเซียว','0');
insert into b_icd10 values ('550010000166','U78.224','Fatigueduetooverexertion','','','0','1','เหลาเจวี้ยน','0');
insert into b_icd10 values ('550010000167','U78.225','Consumptivedisease','','','0','1','ซวีเหลา','0');
insert into b_icd10 values ('550010000168','U78.230','Syncope','','','0','1','เจ๋ว/เจ๋วเจิ้ง','0');
insert into b_icd10 values ('550010000169','U78.231','Floppingsyncope','','','0','1','เป๋าเจ๋ว','0');
insert into b_icd10 values ('550010000170','U78.232','Visceralsyncope','','','0','1','ฉางเจ๋ว/จั้งเจ๋ว','0');
insert into b_icd10 values ('550010000171','U78.233','Crapulentsyncope','','','0','1','สือเจ๋ว','0');
insert into b_icd10 values ('550010000172','U78.234','Heatsyncope','','','0','1','เญ่อเจ๋ว/เจียนเจ๋ว','0');
insert into b_icd10 values ('550010000173','U78.235','Coldsyncope','','','0','1','หานเจ๋ว','0');
insert into b_icd10 values ('550010000174','U78.236','Windsyncope','','','0','1','เฟิงเจ๋ว','0');
insert into b_icd10 values ('550010000175','U78.237','Qisyncope','','','0','1','ชี่เจ๋ว','0');
insert into b_icd10 values ('550010000176','U78.238','Bloodsyncope','','','0','1','เซ่วเจ๋ว','0');
insert into b_icd10 values ('550010000177','U78.239','Phlegmsyncope','','','0','1','ถานเจ๋ว','0');
insert into b_icd10 values ('550010000178','U78.240','Trueheadache','','','0','1','ปวดศีรษะแท้','0');
insert into b_icd10 values ('550010000179','U78.241','Headwind','','','0','1','ปวดศีรษะ','0');
insert into b_icd10 values ('550010000180','U78.242','Hemilateralheadwind','','','0','1','ปวดศีรษะข้างเดียว','0');
insert into b_icd10 values ('550010000181','U78.243','Thunderheadwind','','','0','1','ปวดศีรษะแบบอสนีบาต','0');
insert into b_icd10 values ('550010000182','U78.244','Convulsivedisease','','','0','1','โรคชัก','0');
insert into b_icd10 values ('550010000183','U78.245','Febrileconvulsionwithoutchills','','','0','1','โหรวจิ้ง','0');
insert into b_icd10 values ('550010000184','U78.246','Febrileconvulsionwithchills','','','0','1','กังจิ้ง','0');
insert into b_icd10 values ('550010000185','U78.250','Impedimentdisease','','','0','1','ปี้ปิ้ง','0');
insert into b_icd10 values ('550010000186','U78.251','Movingimpediment','','','0','1','สิงปี้/เฟิงปี้','0');
insert into b_icd10 values ('550010000187','U78.252','Painfulimpediment','','','0','1','ท่งปี้/หานปี้','0');
insert into b_icd10 values ('550010000188','U78.253','Fixedimpediment','','','0','1','จั๋วปี้/ซือปี้','0');
insert into b_icd10 values ('550010000189','U78.254','Heatimpediment','','','0','1','เร่อปี้','0');
insert into b_icd10 values ('550010000190','U78.255','Joint-runningwind','','','0','1','ลี่เจี๋ยเฟิง(ปวดตามข้อทั่วร่างกาย)','0');
insert into b_icd10 values ('550010000191','U78.256','Boneimpediment','','','0','1','กู่ปี้(เจ็บตามกระดูกและข้อ)','0');
insert into b_icd10 values ('550010000192','U78.257','Fleshimpediment','','','0','1','จีปี้(ปวดตามกล้ามเนื้อ)','0');
insert into b_icd10 values ('550010000193','U78.258','Bloodimpediment','','','0','1','เซ่วปี้(เจ็บตามปลายนิ้วจากเลือดติดขัด)','0');
insert into b_icd10 values ('550010000194','U78.259','Vesselimpediment','','','0','1','ม่ายปี้(เจ็บแขนขาจากหลอดเลือดอุดตัน)','0');
insert into b_icd10 values ('550010000195','U78.260','Heartimpediment','','','0','1','ซินปี้(หลอดเลือดหัวใจอุดตัน)','0');
insert into b_icd10 values ('550010000196','U78.261','Intestinalimpediment','','','0','1','ฉางปี้(ลำไส้ไม่เคลื่อนไหว)','0');
insert into b_icd10 values ('550010000197','U78.270','Wiltingdisease','','','0','1','เหว่ยปิ้ง(ร่างกายอ่อนแรง)','0');
insert into b_icd10 values ('550010000198','U78.271','Bonewilting','','','0','1','กู๋เหว่ย/เซิ่นเหว่ย(กระดูกไม่แข็งแรง)','0');
insert into b_icd10 values ('550010000199','U78.272','Vesselwilting','','','0','1','ม่ายเหว่ย/ซินเหว่ย(แขนขาอ่อนแรงจากขาดเลือดมาหล่อเลี้ยง)','0');
insert into b_icd10 values ('550010000200','U78.273','Fleshywilting','','','0','1','โร่วเหว่ย(กล้ามเนื้ออ่อนแรง)','0');
insert into b_icd10 values ('550010000201','U78.274','Sinewwilting','','','0','1','จินเหว่ย(เส้นเอ็นหดเกร็ง)','0');
insert into b_icd10 values ('550010000202','U78.275','Legflaccidity','','','0','1','เหว่ยปี้(แขนขาอ่อนแรง)','0');
insert into b_icd10 values ('550010000203','U78.280','Abnormalsweating','','','0','1','เหงื่อออกผิดปกติ','0');
insert into b_icd10 values ('550010000204','U78.281','Shocksweating','','','0','1','เหงื่อออกในภาวะช็อค','0');
insert into b_icd10 values ('550010000205','U78.282','Chromhidrosis','','','0','1','เหงื่อเหลือง','0');
insert into b_icd10 values ('550010000206','U78.283','Profusesweating','','','0','1','เหงื่อออกมาก','0');
insert into b_icd10 values ('550010000207','U78.284','Spontaneoussweating','','','0','1','เหงื่อออกเองโดยไม่มีสาเหตุ','0');
insert into b_icd10 values ('550010000208','U78.285','Nightsweating','','','0','1','เหงื่อออกขณะหลับเวลากลางคืน','0');
insert into b_icd10 values ('550010000209','U78.286','Coldsweating','','','0','1','เหงื่อเย็น','0');
insert into b_icd10 values ('550010000210','U78.287','Sweatingfromthehead','','','0','1','เหงื่อออกเฉพาะศีรษะ','0');
insert into b_icd10 values ('550010000211','U78.288','Sweatingfromthepalmsandsoles','','','0','1','เหงื่อออกตามฝ่ามือฝ่าเท้า','0');
insert into b_icd10 values ('550010000212','U78.289','Absenceofsweating','','','0','1','เหงื่อออกมากจนแห้ง','0');
insert into b_icd10 values ('550010000213','U78.290','Fever','','','0','1','ไข้/ตัวร้อน','0');
insert into b_icd10 values ('550010000214','U78.291','Highfever','','','0','1','ไข้สูง','0');
insert into b_icd10 values ('550010000215','U78.292','Tidalfever','','','0','1','ไข้ขึ้นๆลงๆ','0');
insert into b_icd10 values ('550010000216','U78.293','Mildfever','','','0','1','ไข้ต่ำ','0');
insert into b_icd10 values ('550010000217','U78.294','Chestpain','','','0','1','เจ็บหน้าอก','0');
insert into b_icd10 values ('550010000218','U78.295','Fox-creeperdisease','','','0','1','หูฮั่ว(โรคBehcet’s)','0');
insert into b_icd10 values ('550010000219','U78.300','Soreandulcer','','','0','1','แผลที่ผิวหนัง','0');
insert into b_icd10 values ('550010000220','U78.301','Ulceratingsore','','','0','1','แผลเปื่อย','0');
insert into b_icd10 values ('550010000221','U78.302','Swollensore','','','0','1','แผลบวม','0');
insert into b_icd10 values ('550010000222','U78.303','Hairlinesore','','','0','1','ตุ่มอักเสบตามไรผม','0');
insert into b_icd10 values ('550010000223','U78.304','Seatsore','','','0','1','ตุ่มอักเสบที่ก้น','0');
insert into b_icd10 values ('550010000224','U78.305','Shanksore','','','0','1','แผลอักเสบเรื้อรังที่หน้าแข้ง','0');
insert into b_icd10 values ('550010000225','U78.306','Bedsore','','','0','1','แผลกดทับ','0');
insert into b_icd10 values ('550010000226','U78.310','Furuncle','','','0','1','ตุ่มฝีอักเสบ','0');
insert into b_icd10 values ('550010000227','U78.311','Molecricketboildisease','','','0','1','ตุ่มหนองบริเวณหนังศีรษะ','0');
insert into b_icd10 values ('550010000228','U78.312','Furunculosis','','','0','1','โรคตุ่มหนองกระจาย','0');
insert into b_icd10 values ('550010000229','U78.313','Deep-rootedboil','','','0','1','ฝีหัวตะปู','0');
insert into b_icd10 values ('550010000230','U78.314','Snake-headwhitlow','','','0','1','ฝีที่ปลายนิ้ว','0');
insert into b_icd10 values ('550010000231','U78.315','Cutaneousanthrax','','','0','1','โรคแอนแทร็กซ์ที่ผิวหนัง','0');
insert into b_icd10 values ('550010000232','U78.316','Poplitealinfection','','','0','1','ฝีที่ข้อพับเข่า','0');
insert into b_icd10 values ('550010000233','U78.320','Abscess','','','0','1','ฝี','0');
insert into b_icd10 values ('550010000234','U28321','Cervicalabscess','','','0','1','ฝีที่คอ','0');
insert into b_icd10 values ('550010000235','U78.322','Axillaryabscess','','','0','1','ฝีที่รักแร้','0');
insert into b_icd10 values ('550010000236','U78.323','Umbilicalabscess','','','0','1','ฝีที่สะดือ','0');
insert into b_icd10 values ('550010000237','U78.324','Glutealabscess','','','0','1','ฝีที่สะโพก','0');
insert into b_icd10 values ('550010000238','U78.325','Intestinalabscess','','','0','1','ฝีไส้ติ่ง','0');
insert into b_icd10 values ('550010000239','U78.326','Carbuncle','','','0','1','ฝีฝักบัว','0');
insert into b_icd10 values ('550010000240','U78.327','Deepmultipleabscess','','','0','1','ฝีเซาะลง','0');
insert into b_icd10 values ('550010000241','U78.330','Effusion','','','0','1','ภาวะอักเสบบวม','0');
insert into b_icd10 values ('550010000242','U78.331','Effusionofthebackofthehand','','','0','1','ภาวะอักเสบบวมที่หลังมือ','0');
insert into b_icd10 values ('550010000243','U78.332','Effusionofthedorsumofthefoot','','','0','1','ภาวะอักเสบบวมที่หลังเท้า','0');
insert into b_icd10 values ('550010000244','U78.340','Suppurativeosteomyelitis','','','0','1','กระดูกอักเสบเป็นหนอง','0');
insert into b_icd10 values ('550010000245','U78.341','Suppurativecoxitis','','','0','1','ฝีอักเสบที่จุดหวนเที่ยว','0');
insert into b_icd10 values ('550010000246','U78.342','Digitalgangrene','','','0','1','นิ้วตายเน่า','0');
insert into b_icd10 values ('550010000247','U78.343','Suppurativeparotitis','','','0','1','ฝีที่แก้ม','0');
insert into b_icd10 values ('550010000248','U78.344','','','','0','1','ฝีในกระดูก','0');
insert into b_icd10 values ('550010000249','U78.345','Scrofula','','','0','1','วัณโรคต่อมน้ำเหลืองที่คอ','0');
insert into b_icd10 values ('550010000250','U78.350','Acutemastitis','','','0','1','เต้านมอักเสบเฉียบพลัน','0');
insert into b_icd10 values ('550010000251','U78.351','Phlegmonousmastitis','','','0','1','เต้านมอักเสบเป็นแผลเปื่อย','0');
insert into b_icd10 values ('550010000252','U78.352','Mammaryphthisis','','','0','1','ก้อนอักเสบของเต้านม','0');
insert into b_icd10 values ('550010000253','U78.353','Gynecomastia','','','0','1','ภาวะนมโตในเพศชาย','0');
insert into b_icd10 values ('550010000254','U78.354','Mammaryfistula','','','0','1','แผลชอนทะลุที่เต้านม','0');
insert into b_icd10 values ('550010000255','U78.355','Thelorrhagia','','','0','1','เลือดออกจากหัวนม','0');
insert into b_icd10 values ('550010000256','U78.356','Mammaryhyperplasia','','','0','1','ก้อนของเต้านม','0');
insert into b_icd10 values ('550010000257','U78.357','Crackednipple','','','0','1','หัวนมแตก','0');
insert into b_icd10 values ('550010000258','U78.358','Breastpain','','','0','1','เจ็บเต้านม ','0');
insert into b_icd10 values ('550010000259','U78.360','Goiter','','','0','1','คอพอก','0');
insert into b_icd10 values ('550010000260','U78.361','Qigoiter','','','0','1','คอพอกธรรมดา','0');
insert into b_icd10 values ('550010000261','U78.362','Fleshygoiter','','','0','1','เนื้องอกต่อมไทรอยด์','0');
insert into b_icd10 values ('550010000262','U78.363','Stonygoiter','','','0','1','มะเร็งต่อมไทรอยด์','0');
insert into b_icd10 values ('550010000263','U78.370','Tumor','','','0','1','เนื้องอก','0');
insert into b_icd10 values ('550010000264','U78.371','Qitumor','','','0','1','ชี่หลิว(เนื้องอกชี่)','0');
insert into b_icd10 values ('550010000265','U78.372','Bloodtumor','','','0','1','เนื้องอกหลอดเลือด','0');
insert into b_icd10 values ('550010000266','U78.373','Sinewtumor','','','0','1','จินหลิว(เนื้องอกจิน)','0');
insert into b_icd10 values ('550010000267','U78.374','Fleshytumor','','','0','1','เนื้องอกกล้ามเนื้อ','0');
insert into b_icd10 values ('550010000268','U78.375','Bonetumor','','','0','1','เนื้องอกกระดูก','0');
insert into b_icd10 values ('550010000269','U78.376','Rockymass/cancer','','','0','1','ก้อนมะเร็ง','0');
insert into b_icd10 values ('550010000270','U78.377','Cocoonlip','','','0','1','เนื้องอกริมฝีปาก','0');
insert into b_icd10 values ('550010000271','U78.378','Cervicalmalignancywithcachexia','','','0','1','โรคผอมแห้งจากเนื้อร้าย','0');
insert into b_icd10 values ('550010000272','U78.379','Rockymassinthebreast/breastcancer','','','0','1','มะเร็งเต้านม','0');
insert into b_icd10 values ('550010000273','U78.400','Erysipelas','','','0','1','ไฟลามทุ่ง','0');
insert into b_icd10 values ('550010000274','U78.401','Wanderingerysipelas','','','0','1','ชื่อโหยวตัน','0');
insert into b_icd10 values ('550010000275','U78.402','Herpessimplex','','','0','1','เริม','0');
insert into b_icd10 values ('550010000276','U78.403','Herpeszoster','','','0','1','งูสวัด','0');
insert into b_icd10 values ('550010000277','U78.404','Verruca','','','0','1','หูด','0');
insert into b_icd10 values ('550010000278','U78.405','Corn','','','0','1','ตาปลา','0');
insert into b_icd10 values ('550010000279','U78.406','Fatsore(Tineacapitis)','','','0','1','เชื้อราที่ศีรษะ','0');
insert into b_icd10 values ('550010000280','U78.407','Tineamanuum','','','0','1','เชื้อรามือและเท้า','0');
insert into b_icd10 values ('550010000281','U78.408','Tineaversicolor','','','0','1','เกลื้อน','0');
insert into b_icd10 values ('550010000282','U78.410','Eruption','','','0','1','ผื่น','0');
insert into b_icd10 values ('550010000283','U78.411','Macula','','','0','1','จุดหรือปื้นผิวหนังที่เปลี่ยนสี','0');
insert into b_icd10 values ('550010000284','U78.412','Papule','','','0','1','ตุ่มนูน','0');
insert into b_icd10 values ('550010000285','U78.413','Pustule','','','0','1','ตุ่มหนอง','0');
insert into b_icd10 values ('550010000286','U78.414','Subcutaneousnode','','','0','1','ก้อนใต้ผิวหนัง','0');
insert into b_icd10 values ('550010000287','U78.415','Polyp','','','0','1','ติ่งเนื้อผิวหนัง','0');
insert into b_icd10 values ('550010000288','U78.416','Fistula','','','0','1','แผลชอนทะลุ ','0');
insert into b_icd10 values ('550010000289','U78.420','Vilitigo','','','0','1','ด่างขาว','0');
insert into b_icd10 values ('550010000290','U78.421','Scabies','','','0','1','หิด','0');
insert into b_icd10 values ('550010000291','U78.422','Urticaria','','','0','1','ลมพิษ','0');
insert into b_icd10 values ('550010000292','U78.423','Dryringworm','','','0','1','ผื่นผิวหนังติดเชื้อราอักเสบเรื้อรัง','0');
insert into b_icd10 values ('550010000293','U78.424','Psoriasis','','','0','1','โรคสะเก็ดเงิน(Psoriasis)','0');
insert into b_icd10 values ('550010000294','U78.425','Seborrheicdermatitis','','','0','1','ต่อมไขมันอักเสบ','0');
insert into b_icd10 values ('550010000295','U78.426','Acne','','','0','1','สิว','0');
insert into b_icd10 values ('550010000296','U78.427','Alopeciaareata','','','0','1','ผมร่วงเป็นหย่อม','0');
insert into b_icd10 values ('550010000297','U78.428','Erythemamultiforme','','','0','1','ผื่นตาแมว/ผื่นแพ้หลากรูปแบบ','0');
insert into b_icd10 values ('550010000298','U78.430','Erythemanodosum','','','0','1','ตุ่มแดงที่ขาจากหลอดเลือดผิวหนังอักเสบ','0');
insert into b_icd10 values ('550010000299','U78.431','Skinimpediment','','','0','1','โรคหนังแข็ง','0');
insert into b_icd10 values ('550010000300','U78.432','Generalizeditching','','','0','1','คันตามผิวหนัง','0');
insert into b_icd10 values ('550010000301','U78.433','Numbnessoftheskin','','','0','1','ชาตามผิวหนัง','0');
insert into b_icd10 values ('550010000302','U78.440','Hemorrhoid','','','0','1','ริดสีดวงทวาร','0');
insert into b_icd10 values ('550010000303','U78.441','Internalhemorrhoid','','','0','1','ริดสีดวงทวารภายใน','0');
insert into b_icd10 values ('550010000304','U78.442','Externalhemorrhoid','','','0','1','ริดสีดวงทวารภายนอก','0');
insert into b_icd10 values ('550010000305','U78.443','Analfistula','','','0','1','ฝีคัณฑสูตร','0');
insert into b_icd10 values ('550010000306','U78.444','Prolapseoftherectum','','','0','1','ทวารหย่อน','0');
insert into b_icd10 values ('550010000307','U78.445','Polypofrectum','','','0','1','ติ่งเนื้อในลำไส้','0');
insert into b_icd10 values ('550010000308','U78.450','Genitaldisease','','','0','1','ซ่าน/ซ่านชี่','0');
insert into b_icd10 values ('550010000309','U78.451','Abscessofthetesticle','','','0','1','อัณฑะอักเสบ','0');
insert into b_icd10 values ('550010000310','U78.452','Scrotalabscess','','','0','1','ถุงอัณฑะอักเสบ','0');
insert into b_icd10 values ('550010000311','U78.453','Hydrocele','','','0','1','ถุงน้ำในถุงอัณฑะ','0');
insert into b_icd10 values ('550010000312','U78.490','Generalizedpain','','','0','1','เจ็บปวดตามร่างกาย','0');
insert into b_icd10 values ('550010000313','U78.491','Stiffnessoftheneck','','','0','1','คอแข็ง','0');
insert into b_icd10 values ('550010000314','U78.492','Hypochondriacpain','','','0','1','เจ็บชายโครง','0');
insert into b_icd10 values ('550010000315','U78.493','Lumbago','','','0','1','ปวดเอว','0');
insert into b_icd10 values ('550010000316','U78.494','Backpain','','','0','1','ปวดหลัง','0');
insert into b_icd10 values ('550010000317','U78.495','Genitalpain','','','0','1','เจ็บบริเวณอวัยวะสืบพันธุ์','0');
insert into b_icd10 values ('550010000318','U78.496','Thighswelling','','','0','1','ต้นขาบวม','0');
insert into b_icd10 values ('550010000319','U78.497','Green-bluesnaketoxinsore','','','0','1','หลอดเลือดดำชั้นตื้นอักเสบมีลิ่มเลือด','0');
insert into b_icd10 values ('550010000320','U78.498','Heelpain','','','0','1','เจ็บส้นเท้า','0');
insert into b_icd10 values ('550010000321','U78.500','Mastitisduringpregnancy','','','0','1','เต้านมอักเสบระหว่างตั้งครรภ์','0');
insert into b_icd10 values ('550010000322','U78.501','Morningsickness','','','0','1','แพ้ท้อง','0');
insert into b_icd10 values ('550010000323','U78.502','Uterineobstruction','','','0','1','ปวดท้องขณะตั้งครรภ์','0');
insert into b_icd10 values ('550010000324','U78.503','Vaginalbleedingduringpregnancy','','','0','1','เลือดออกขณะตั้งครรภ์','0');
insert into b_icd10 values ('550010000325','U78.504','Threatenedabortion','','','0','1','แท้งคุกคาม','0');
insert into b_icd10 values ('550010000326','U78.505','Habitualabortion','','','0','1','แท้งเป็นอาจิณ','0');
insert into b_icd10 values ('550010000327','U78.506','Earlyabortion','','','0','1','แท้งบุตรระยะไตรมาสแรก','0');
insert into b_icd10 values ('550010000328','U78.507','Lateabortion','','','0','1','แท้งบุตรในไตรมาสที่สอง','0');
insert into b_icd10 values ('550010000329','U78.508','Pseudopregnancy','','','0','1','ตั้งครรภ์เทียม','0');
insert into b_icd10 values ('550010000330','U78.510','Hydramnios','','','0','1','ตึงแน่นขณะตั้งครรภ์','0');
insert into b_icd10 values ('550010000331','U78.511','Pregnancyswelling','','','0','1','บวมน้ำขณะตั้งครรภ์','0');
insert into b_icd10 values ('550010000332','U78.512','Pregnancyvexation','','','0','1','หงุดหงิดขณะตั้งครรภ์','0');
insert into b_icd10 values ('550010000333','U78.513','Dizzinessinpregnancy','','','0','1','เวียนศีรษะขณะตั้งครรภ์','0');
insert into b_icd10 values ('550010000334','U78.514','Eclampsiaofpregnancy','','','0','1','โรคพิษแห่งครรภ์ระยะชัก','0');
insert into b_icd10 values ('550010000335','U78.515','Coughduringpregnancy','','','0','1','ไอขณะตั้งครรภ์','0');
insert into b_icd10 values ('550010000336','U78.516','Stranguryofpregnancy','','','0','1','ปัสสาวะขัดขณะตั้งครรภ์','0');
insert into b_icd10 values ('550010000337','U78.517','Post-termpregnancy','','','0','1','ครรภ์เกินกำหนด','0');
insert into b_icd10 values ('550010000338','U78.518','Deadfetusintheuterus','','','0','1','ทารกเสียชีวิตในครรภ์','0');
insert into b_icd10 values ('550010000339','U78.520','Falselabor','','','0','1','เจ็บท้องคลอดลวง','0');
insert into b_icd10 values ('550010000340','U78.521','Earlyleakageofamnioticfluid','','','0','1','ถุงน้ำคร่ำแตกก่อนกำหนด','0');
insert into b_icd10 values ('550010000341','U78.524','Difficultdelivery','','','0','1','คลอดยาก','0');
insert into b_icd10 values ('550010000342','U78.525','Retentionofplacenta','','','0','1','รกค้าง','0');
insert into b_icd10 values ('550010000343','U78.530','Postpartummastitis','','','0','1','เต้านมอักเสบหลังคลอด','0');
insert into b_icd10 values ('550010000344','U78.531','Retentionoflochia','','','0','1','น้ำคาวปลาไม่ออกหรือออกน้อย','0');
insert into b_icd10 values ('550010000345','U78.532','Persistentflowofthelochia','','','0','1','น้ำคาวปลาออกไม่หยุด','0');
insert into b_icd10 values ('550010000346','U78.533','Oligogalactia','','','0','1','น้ำนมน้อย','0');
insert into b_icd10 values ('550010000347','U78.534','Agalactia','','','0','1','ไม่มีน้ำนม','0');
insert into b_icd10 values ('550010000348','U78.535','Postpartumgalactorrhea','','','0','1','น้ำนมไหลออกเองหลังคลอด','0');
insert into b_icd10 values ('550010000349','U78.540','Menstrualirregularities','','','0','1','ประจำเดือนผิดปกติ','0');
insert into b_icd10 values ('550010000350','U78.541','Bimonthlymenstruation','','','0','1','ประจำเดือนสองเดือนมาครั้ง','0');
insert into b_icd10 values ('550010000351','U78.542','Trimonthlymenstruation','','','0','1','ประจำเดือนสามเดือนมาครั้ง','0');
insert into b_icd10 values ('550010000352','U78.543','Annualmenstruation','','','0','1','ประจำเดือนปีละครั้ง','0');
insert into b_icd10 values ('550010000353','U78.544','Advancedmenstruation','','','0','1','ภาวะประจำเดือนมาเร็วกว่าปกติ','0');
insert into b_icd10 values ('550010000354','U78.545','Delayedmenstruation','','','0','1','ประจำเดือนมาช้ากว่าปกติ','0');
insert into b_icd10 values ('550010000355','U78.546','Menstruationatirregularintervals','','','0','1','ประจำเดือนคลาดเคลื่อน','0');
insert into b_icd10 values ('550010000356','U78.547','Scantmenstruation','','','0','1','ประจำเดือนมาน้อยกว่าปกติ','0');
insert into b_icd10 values ('550010000357','U78.548','Profusemenstruation','','','0','1','ประจำเดือนมามากกว่าปกติ','0');
insert into b_icd10 values ('550010000358','U78.550','Flooding','','','0','1','เลือดออกทางช่องคลอดมาก','0');
insert into b_icd10 values ('550010000359','U78.551','Spotting','','','0','1','เลือดออกทางช่องคลอดกะปริดกะปรอย','0');
insert into b_icd10 values ('550010000360','U78.552','Floodingandspotting','','','0','1','เลือดออกทางช่องคลอดมากสลับกะปริดกะปรอย','0');
insert into b_icd10 values ('550010000361','U78.553','Prolongedmenstruation','','','0','1','ประจำเดือนมานานกว่าปกติ','0');
insert into b_icd10 values ('550010000362','U78.554','Intermenstrualbleeding','','','0','1','เลือดออกกลางรอบประจำเดือน','0');
insert into b_icd10 values ('550010000363','U78.555','Amenorrhea','','','0','1','ประจำเดือนไม่มา','0');
insert into b_icd10 values ('550010000364','U78.556','Dysmenorrhea','','','0','1','ปวดประจำเดือน','0');
insert into b_icd10 values ('550010000365','U78.557','Invertedmenstruation','','','0','1','เลือดประจำเดือนไหลย้อนขึ้น','0');
insert into b_icd10 values ('550010000366','U78.558','Distendingpaininthebreastsduringmenstruation','','','0','1','คัดเต้านมขณะมีประจำเดือน','0');
insert into b_icd10 values ('550010000367','U78.559','Moodinessduringmenstruation','','','0','1','อารมณ์แปรปรวนขณะมีประจำเดือน','0');
insert into b_icd10 values ('550010000368','U78.560','Vaginaldischarge','','','0','1','ตกขาว','0');
insert into b_icd10 values ('550010000369','U78..561','Whitevaginaldischarge','','','0','1','ตกขาวชนิดสีขาว','0');
insert into b_icd10 values ('550010000370','U78.562','Yellowvaginaldischarge','','','0','1','ตกขาวชนิดสีเหลือง','0');
insert into b_icd10 values ('550010000371','U78.563','Galactorrhea','','','0','1','น้ำนมไหลออกเอง','0');
insert into b_icd10 values ('550010000372','U78.564','Infertility','','','0','1','ภาวะมีบุตรยาก','0');
insert into b_icd10 values ('550010000373','U78.565','Lowerabdominalmass(inwoman)','','','0','1','ก้อนในรังไข่','0');
insert into b_icd10 values ('550010000374','U78.566','Stonyconglomeration','','','0','1','ก้อนในมดลูก','0');
insert into b_icd10 values ('550010000375','U78.567','Prolapseoftheuterus','','','0','1','มดลูกหย่อน','0');
insert into b_icd10 values ('550010000376','U78.570','Pudendalitch','','','0','1','คันตามอวัยวะเพศ','0');
insert into b_icd10 values ('550010000377','U78.571','Pudendalswelling','','','0','1','อวัยวะเพศภายนอกบวม','0');
insert into b_icd10 values ('550010000378','U78.572','Pudendalsore','','','0','1','ฝีที่ปากช่องคลอด','0');
insert into b_icd10 values ('550010000379','U78.573','Pudendalpain','','','0','1','เจ็บช่องคลอด','0');
insert into b_icd10 values ('550010000380','U78.574','Vaginalflatus','','','0','1','ผายลมทางช่องคลอด','0');
insert into b_icd10 values ('550010000381','U78.600','Fetalweakness','','','0','1','ทารกอ่อนแอ','0');
insert into b_icd10 values ('550010000382','U78.601','Depressedfontanel','','','0','1','ขม่อมบุ๋ม','0');
insert into b_icd10 values ('550010000383','U78.602','Bulgingfontanel','','','0','1','ขม่อมนูน','0');
insert into b_icd10 values ('550010000384','U78.603','Growthfever','','','0','1','เด็กตัวร้อนระหว่างเจริญเติบโต','0');
insert into b_icd10 values ('550010000385','U78.604','Tortoiseback','','','0','1','หลังค่อม','0');
insert into b_icd10 values ('550010000386','U78.605','Pigeonchest','','','0','1','อกไก่','0');
insert into b_icd10 values ('550010000387','U78.606','Un-unitedskull','','','0','1','เด็กหัวโตผิดปกติ','0');
insert into b_icd10 values ('550010000388','U78.607','Fiveretardations','','','0','1','ภาวะพัฒนาการช้าห้าประการ','0');
insert into b_icd10 values ('550010000389','U78.608','Fivelimpnesses/flaccidity','','','0','1','ภาวะอ่อนห้าประการ','0');
insert into b_icd10 values ('550010000390','U78.609','Fivestiffnesses','','','0','1','ภาวะเกร็งห้าประการ','0');
insert into b_icd10 values ('550010000391','U78.610','(Infantile)malnutrition','','','0','1','ภาวะทุพโภชนาการ','0');
insert into b_icd10 values ('550010000392','U78.611','Lactationalmalnutrition','','','0','1','ภาวะทุพโภชนาการจากการขาดนมแม่','0');
/* insert into b_icd10 values ('550010000393','U78.612','Othermalnutrition','','','0','1','ภาวะทุพโภชนาการแบบอื่น','0');
insert into b_icd10 values ('550010000394','U78.612','Foodaccumulation','','','0','1','ภาวะโรคที่มีลักษณะของการสะสมอาหารที่ไม่ย่อย','0');
insert into b_icd10 values ('550010000395','U78.612','Mild(infantile)malnutrition','','','0','1','ภาวะทุโภชนาการในทารกเล็กน้อยในช่วงระยะแรก','0');
insert into b_icd10 values ('550010000396','U78.612','Mild(infantile)malnutritionwithaccumulation','','','0','1','ภาวะทุโภชนาการในทารกช่วงระยะกลาง','0');
insert into b_icd10 values ('550010000397','U78.612','T-shapedmalnutrition','','','0','1','ภาวะทุโภชนาการในทารกชนิดรุนแรง','0');
insert into b_icd10 values ('550010000398','U78.612','Dryness(infantile)malnutrition','','','0','1','ภาวะทุโภชนาการในทารกช่วงระยะท้าย','0');
insert into b_icd10 values ('550010000399','U78.612','Blood(infantile)malnutrition','','','0','1','ภาวะทุโภชนาการในทารกร่วมกับเลือดพร่องชัดเจน','0');
insert into b_icd10 values ('550010000400','U78.612','Spleen(infantile)malnutrition','','','0','1','ภาวะทุโภชนาการในทารกสาเหตุจากม้ามและกระเพาะอาหารอ่อนแอเกิดความร้อนชื้นสะสมหรือเรียกว่าภาวะทุโภชนาการจากอาหาร','0');
insert into b_icd10 values ('550010000401','U78.612','Heart(infantile)malnutrition','','','0','1','ภาวะทุโภชนาการในทารกสาเหตุจากม้ามและกระเพาะอาหารอ่อนแอเกิดความร้อนสะสมภายในเส้นลมปราณหัวใจหรือเรียกว่าภาวะทุโภชนาการเหตุตกใจกลัว','0');
insert into b_icd10 values ('550010000402','U78.612','Liver(infantile)malnutrition','','','0','1','ภาวะทุโภชนาการในทารกสาเหตุจากม้ามและกระเพาะอาหารอ่อนแอเกิดความร้อนสะสมภายในเส้นลมปราณตับหรือเรียกว่าภาวะทุโภชนาการเอ็น','0');
insert into b_icd10 values ('550010000403','U78.612','Lung(infantile)malnutrition','','','0','1','ภาวะทุโภชนาการในทารกสาเหตุจากม้ามและกระเพาะอาหารอ่อนแอเกิดความร้อนภายในทำลายปอดหรือเรียกว่าภาวะทุโภชนาการลมปราณ','0');
insert into b_icd10 values ('550010000404','U78.612','Kidney(infantile)malnutrition','','','0','1','ภาวะทุโภชนาการในทารกสาเหตุจากความบกพร่องทางพันธุกรรม','0'); 
insert into b_icd10 values ('550010000405','U78.612','Eye(infantile)malnutrition','','','0','1','ภาวะทุโภชนาการในเด็กซึ่งเกิดจากไฟตับรุกรานตา','0');
insert into b_icd10 values ('550010000406','U78.612','Ascaris(infantile)malnutrition','','','0','1','ภาวะทุโภชนาการจากพยาธิไส้เดือน','0');*/
insert into b_icd10 values ('550010000407','U78.630','Dribbling','','','0','1','น้ำลายไหลมากผิดปกติ','0');
insert into b_icd10 values ('550010000408','U78.631','Thrush','','','0','1','ฝ้าขาวในช่องปาก','0');
insert into b_icd10 values ('550010000409','U78.632','Aphtha','','','0','1','แผลร้อนใน','0');
insert into b_icd10 values ('550010000410','U78.633','Gingivaleruption','','','0','1','ฝ้าเหลืองที่เหงือก','0');
insert into b_icd10 values ('550010000411','U78.634','Woodentongue','','','0','1','ลิ้นบวมแข็ง','0');
insert into b_icd10 values ('550010000412','U78.635','Teethgrinding','','','0','1','นอนกัดฟัน','0');
insert into b_icd10 values ('550010000413','U78.636','Anorexia','','','0','1','เบื่ออาหาร','0');
insert into b_icd10 values ('550010000414','U78.637','Milkregurgitation','','','0','1','สำรอกนม','0');
insert into b_icd10 values ('550010000415','U78.638','Foodaccumulation','','','0','1','อาหารตกค้างสะสม','0');
insert into b_icd10 values ('550010000416','U78.640','Infantileconvulsion','','','0','1','ชักในเด็ก','0');
insert into b_icd10 values ('550010000417','U78.641','Convulsionwithupliftedeyes','','','0','1','ชักจากไข้สูง','0');
insert into b_icd10 values ('550010000418','U78.642','Convulsionwithabdominalpain','','','0','1','ชักร่วมกับอาการปวดท้อง','0');
insert into b_icd10 values ('550010000419','U78.643','Acuteinfantileconvulsion','','','0','1','ชักเฉียบพลัน','0');
insert into b_icd10 values ('550010000420','U78.644','Chronicinfantileconvulsion','','','0','1','ชักเรื้อรัง','0');
insert into b_icd10 values ('550010000421','U78.645','Chronicspleenwind','','','0','1','ชักจากม้ามพร่อง','0');
insert into b_icd10 values ('550010000422','U78.646','Epilepsy','','','0','1','โรคลมชักในเด็ก','0');
insert into b_icd10 values ('550010000423','U78.650','Smallpox','','','0','1','ไข้ทรพิษหรือฝีดาษ','0');
insert into b_icd10 values ('550010000424','U78.651','Measles','','','0','1','หัด','0');
insert into b_icd10 values ('550010000425','U78.652','Exanthem','','','0','1','ออกผื่น','0');
insert into b_icd10 values ('550010000426','U78.653','Rubella','','','0','1','หัดเยอรมัน','0');
insert into b_icd10 values ('550010000427','U78.654','Scarlatina','','','0','1','ไข้อีดำอีแดง','0');
insert into b_icd10 values ('550010000428','U78.655','Chickenpox','','','0','1','อีสุกอีใส','0');
insert into b_icd10 values ('550010000429','U78.656','Mumps','','','0','1','คางทูม','0');
insert into b_icd10 values ('550010000430','U78.657','Diphtheria','','','0','1','คอตีบ','0');
insert into b_icd10 values ('550010000431','U78.658','Whoopingcough','','','0','1','ไอกรน','0');
insert into b_icd10 values ('550010000432','U78.660','Umbilicalwind','','','0','1','บาดทะยักในเด็ก','0');
insert into b_icd10 values ('550010000433','U78.661','Umbilicaldampness','','','0','1','สะดือแฉะ','0');
insert into b_icd10 values ('550010000434','U78.662','Umbilicalsores','','','0','1','แผลสะดือ','0');
insert into b_icd10 values ('550010000435','U78.663','Umbilicalhernia','','','0','1','ไส้เลื่อนที่สะดือ','0');
insert into b_icd10 values ('550010000436','U78.690','Fetalheat','','','0','1','ร้อนในทารกแรกเกิด','0');
insert into b_icd10 values ('550010000437','U78.691','Neonatalcough','','','0','1','ไอในทารกแรกเกิด','0');
insert into b_icd10 values ('550010000438','U78.692','Infantileasthma','','','0','1','หืดในทารก','0');
insert into b_icd10 values ('550010000439','U78.693','Horse-spleenwind','','','0','1','หอบเฉียบพลันในเด็กเล็ก','0');
insert into b_icd10 values ('550010000440','U78.694','Summernon-acclimatization','','','0','1','ภาวะอ่อนเพลียมีไข้จากอากาศร้อน','0');
insert into b_icd10 values ('550010000441','U78.695','Nightcrying','','','0','1','กรีดร้องเวลากลางคืน','0');
insert into b_icd10 values ('550010000442','U78.696','Nightcryingduetofright','','','0','1','ร้องไห้เวลากลางคืนจากความกลัว','0');
insert into b_icd10 values ('550010000443','U78.697','Frightseizure','','','0','1','กลัวคนหรือสิ่งที่ไม่คุ้นเคย','0');
insert into b_icd10 values ('550010000444','U78.698','Neonataljaundice','','','0','1','ดีซ่านในทารก','0');
insert into b_icd10 values ('550010000445','U78.699','Fetalredness','','','0','1','ตัวแดงในทารกแรกเกิด','0');
insert into b_icd10 values ('550010000446','U78.700','Photophobia','','','0','1','ตากลัวแสง','0');
insert into b_icd10 values ('550010000447','U78.701','Dimvision','','','0','1','ตามัว','0');
insert into b_icd10 values ('550010000448','U78.702','Blurredvision','','','0','1','ตาฝ้าฟาง','0');
insert into b_icd10 values ('550010000449','U78.703','Doublevision','','','0','1','ตาเห็นภาพซ้อน','0');
insert into b_icd10 values ('550010000450','U78.704','Blindness','','','0','1','ตาบอด','0');
insert into b_icd10 values ('550010000451','U78.705','Suddenblindness','','','0','1','ตาบอดฉับพลัน','0');
insert into b_icd10 values ('550010000452','U78.706','Bluishblindness','','','0','1','ประสาทตาฝ่อ','0');
insert into b_icd10 values ('550010000453','U78.707','Nightblindness','','','0','1','ตาบอดกลางคืน','0');
insert into b_icd10 values ('550010000454','U78.708','Retinopathypigmentosa','','','0','1','จอตาเสื่อม','0');
insert into b_icd10 values ('550010000455','U78.710','Sty','','','0','1','ตากุ้งยิง','0');
insert into b_icd10 values ('550010000456','U78.711','Phlegmnodeoftheeyelid','','','0','1','ถุงน้ำในเปลือกตาด้านใน','0');
insert into b_icd10 values ('550010000457','U78.712','Trachoma','','','0','1','ริดสีดวงตา','0');
insert into b_icd10 values ('550010000458','U78.713','Heattearing','','','0','1','น้ำตาร้อน','0');
insert into b_icd10 values ('550010000459','U78.714','Coldtearing','','','0','1','น้ำตาเย็น','0');
insert into b_icd10 values ('550010000460','U78.715','Dacryopyorrhea','','','0','1','ถุงน้ำตาอักเสบเป็นหนอง','0');
insert into b_icd10 values ('550010000461','U78.720','Pterygium','','','0','1','ต้อเนื้อ','0');
insert into b_icd10 values ('550010000462','U78.721','Suddenattackofwind-heatontheeye','','','0','1','เยื่อบุตาอักเสบเฉียบพลัน','0');
insert into b_icd10 values ('550010000463','U78.722','Epidemicconjunctivitis','','','0','1','โรคตาแดง','0');
insert into b_icd10 values ('550010000464','U78.723','Acuteconjunctivitiswithnebula','','','0','1','เยื่อบุตาอักเสบเฉียบพลันและเกิดแผล','0');
insert into b_icd10 values ('550010000465','U78.724','Phlyctenularconjunctivitis','','','0','1','เยื่อบุตาอักเสบมีตุ่มใสเล็ก(ต้อข้าวสาร)','0');
insert into b_icd10 values ('550010000466','U78.725','Acutescleritis','','','0','1','ตาขาวอักเสบเฉียบพลัน','0');
insert into b_icd10 values ('550010000467','U78.726','Bluishdiscolorationofsclera','','','0','1','ตาขาวเป็นสีน้ำเงิน','0');
insert into b_icd10 values ('550010000468','U78.727','Subconjunctivalhemorrhage','','','0','1','เลือดออกใต้เยื่อตา','0');
insert into b_icd10 values ('550010000469','U78.730','Invasionofwhitemembraneintothecornea','','','0','1','ต้อเนื้อลามกระจกตา','0');
insert into b_icd10 values ('550010000470','U78.731','Superficialpunctuatekeratitis','','','0','1','กระจกตาอักเสบเป็นจุด','0');
insert into b_icd10 values ('550010000471','U78.732','Cornealulcer','','','0','1','แผลที่กระจกตา','0');
insert into b_icd10 values ('550010000472','U78.733','Cornealopacity','','','0','1','กระจกตาขุ่น','0');
insert into b_icd10 values ('550010000473','U78.734','Purulentkeratitis','','','0','1','กระจกตาอักเสบเป็นหนอง','0');
insert into b_icd10 values ('550010000474','U78.735','Droopingpannus','','','0','1','หลอดเลือดบังกระจกตาด้านบน','0');
insert into b_icd10 values ('550010000475','U78.736','Keraticpannus','','','0','1','หลอดเลือดบังกระจกตาทุกด้าน','0');
insert into b_icd10 values ('550010000476','U78.737','Pupillarymetamorphosis','','','0','1','รูม่านตาผิดรูป','0');
insert into b_icd10 values ('550010000477','U78.738','Cataract','','','0','1','ต้อกระจก','0');
insert into b_icd10 values ('550010000478','U78.739','Congenitalcataract','','','0','1','ต้อกระจกแต่กำเนิด','0');
insert into b_icd10 values ('550010000479','U78.740','Traumaticcataract','','','0','1','ต้อกระจกจากการบาดเจ็บ','0');
insert into b_icd10 values ('550010000480','U78.741','Foreignbodyintheeye','','','0','1','สิ่งแปลกปลอมเข้าตา','0');
insert into b_icd10 values ('550010000481','U78.742','Collisioneyeinjury','','','0','1','ตาบาดเจ็บจากแรงกระแทก','0');
insert into b_icd10 values ('550010000482','U78.743','Rupturedwoundoftheeyeball','','','0','1','ลูกตาแตก','0');
insert into b_icd10 values ('550010000483','U78.750','Greenishglaucoma','','','0','1','ต้อหินแบบมุมปิดเฉียบพลัน','0');
insert into b_icd10 values ('550010000484','U78.751','Hyalosis','','','0','1','วุ้นตาเสื่อม','0');
insert into b_icd10 values ('550010000485','U78.752','(Nutritional)keratomalacia','','','0','1','ลูกตาน่วม','0');
insert into b_icd10 values ('550010000486','U78.753','Paralyticstrabismus','','','0','1','ตาเหล่จากประสาทตาอัมพาต','0');
insert into b_icd10 values ('550010000487','U78.754','Fixedprotrudingeye','','','0','1','ตาโปน','0');
insert into b_icd10 values ('550010000488','U78.755','Suddenprotrusionoftheeyeball','','','0','1','ตาโปนฉับพลัน','0');
insert into b_icd10 values ('550010000489','U78.756','Eyedischarge','','','0','1','ตาแฉะ','0');
insert into b_icd10 values ('550010000490','U78.757','Itchyeyes','','','0','1','คันตา','0');
insert into b_icd10 values ('550010000491','U78.758','Deviatedeyeandmouth','','','0','1','หลับตาไม่สนิทและปากเบี้ยว','0');
insert into b_icd10 values ('550010000492','U78.800','Earboil','','','0','1','ตุ่มฝีในหู','0');
insert into b_icd10 values ('550010000493','U78.801','Earsore','','','0','1','แผลในหู','0');
insert into b_icd10 values ('550010000494','U78.802','Eczemaofexternalear','','','0','1','หูชั้นนอกอักเสบ','0');
insert into b_icd10 values ('550010000495','U78.803','Impactedcerumen','','','0','1','ขี้หูอุดตัน','0');
insert into b_icd10 values ('550010000496','U78.804','Eardistension','','','0','1','ตึงแน่นในหู','0');
insert into b_icd10 values ('550010000497','U78.805','Purulentear','','','0','1','หูน้ำหนวก','0');
insert into b_icd10 values ('550010000498','U78.806','Postauricularinfection','','','0','1','หลังหูอักเสบ','0');
insert into b_icd10 values ('550010000499','U78.807','Postauricularabscess','','','0','1','หลังหูเป็นฝี','0');
insert into b_icd10 values ('550010000500','U78.808','Earpile','','','0','1','ริดสีดวงหู','0');
insert into b_icd10 values ('550010000501','U78.809','Earpolyp','','','0','1','ติ่งเนื้อที่หู','0');
insert into b_icd10 values ('550010000502','U78.810','Earprotuberance','','','0','1','เนื้องอกในหู','0');
insert into b_icd10 values ('550010000503','U78.811','Earfistula','','','0','1','แผลชอนทะลุที่หู','0');
insert into b_icd10 values ('550010000504','U78.812','Hardnessofhearing','','','0','1','หูตึง','0');
insert into b_icd10 values ('550010000505','U78.813','Tinnitus','','','0','1','มีเสียงผิดปกติในหู','0');
insert into b_icd10 values ('550010000506','U78.814','Deafness','','','0','1','หูหนวก','0');
insert into b_icd10 values ('550010000507','U78.820','Nasalboil','','','0','1','ตุ่มฝีที่จมูก','0');
insert into b_icd10 values ('550010000508','U78.821','Drynose','','','0','1','โพรงจมูกแห้ง','0');
insert into b_icd10 values ('550010000509','U78.822','Nasalsore','','','0','1','แผลในจมูก','0');
insert into b_icd10 values ('550010000510','U78.823','Atrophicrhinitis','','','0','1','เยื่อบุจมูกอักเสบแบบฝ่อ','0');
insert into b_icd10 values ('550010000511','U78.824','Allergicrhinitis','','','0','1','เยื่อบุจมูกอักเสบจากภูมิแพ้','0');
insert into b_icd10 values ('550010000512','U78.825','Nasalpolyp','','','0','1','ริดสีดวงจมูก','0');
insert into b_icd10 values ('550010000513','U78.826','Sinusitis','','','0','1','ไซนัสอักเสบ','0');
insert into b_icd10 values ('550010000514','U78.827','Nasalcongestion','','','0','1','คัดจมูก','0');
insert into b_icd10 values ('550010000515','U78.828','Lossofsmell','','','0','1','จมูกไม่ได้กลิ่น','0');
insert into b_icd10 values ('550010000516','U78.829','Sneezing','','','0','1','จาม','0');
insert into b_icd10 values ('550010000517','U78.830','Tonsillitis','','','0','1','ทอนซิลอักเสบ','0');
insert into b_icd10 values ('550010000518','U78.831','Chronictonsillitis','','','0','1','ทอนซิลอักเสบเรื้อรัง','0');
insert into b_icd10 values ('550010000519','U78.832','Throatimpediment','','','0','1','เจ็บคอ','0');
insert into b_icd10 values ('550010000520','U78.833','Throatabscess','','','0','1','ฝีในคอ','0');
insert into b_icd10 values ('550010000521','U78.834','Ominousabscessofthethroat','','','0','1','ฝีในคออักเสบรุนแรง','0');
insert into b_icd10 values ('550010000522','U78.835','Lichenoiderosionofthethroat','','','0','1','แผลกร่อนในคอ','0');
insert into b_icd10 values ('550010000523','U78.836','Bonestuckinthethroat','','','0','1','กระดูกหรือสิ่งแปลกปลอมติดคอ','0');
insert into b_icd10 values ('550010000524','U78.837','Tumorofthethroat','','','0','1','เนื้องอกในคอ','0');
insert into b_icd10 values ('550010000525','U78.838','Throatcancer','','','0','1','มะเร็งในคอ','0');
insert into b_icd10 values ('550010000526','U78.840','Lossofvoice','','','0','1','เสียงหาย','0');
insert into b_icd10 values ('550010000527','U78.841','Hoarseness','','','0','1','เสียงแหบ','0');
insert into b_icd10 values ('550010000528','U78.842','Nausea','','','0','1','คลื่นไส้','0');
insert into b_icd10 values ('550010000529','U78.850','Throatwind','','','0','1','ลำคออักเสบ','0');
insert into b_icd10 values ('550010000530','U78.851','Fulminantthroatwind','','','0','1','หลอดคออักเสบเฉียบพลัน','0');
insert into b_icd10 values ('550010000531','U78.852','Entwiningthroatwind','','','0','1','หลอดคอติดเชื้อรุนแรง','0');
insert into b_icd10 values ('550010000532','U78.853','Obstructivethroatwind','','','0','1','หลอดคออักเสบมีอาการปวดบวม','0');
insert into b_icd10 values ('550010000533','U78.860','Doubletongue','','','0','1','ใต้ลิ้นบวม','0');
insert into b_icd10 values ('550010000534','U78.861','Ankyloglossia','','','0','1','ลิ้นยึด','0');
insert into b_icd10 values ('550010000535','U78.862','Tongueabscess','','','0','1','ฝีที่ลิ้น','0');
insert into b_icd10 values ('550010000536','U78.863','Tongueboil','','','0','1','ตุ่มฝีที่ลิ้น','0');
insert into b_icd10 values ('550010000537','U78.864','Tonguesore','','','0','1','แผลที่ลิ้น','0');
insert into b_icd10 values ('550010000538','U78.865','Tonguecancer','','','0','1','มะเร็งที่ลิ้น','0');
insert into b_icd10 values ('550010000539','U78.866','Phlegmcyst','','','0','1','ถุงน้ำใต้ลิ้น','0');
insert into b_icd10 values ('550010000540','U78.870','Mouthodor','','','0','1','มีกลิ่นปาก','0');
insert into b_icd10 values ('550010000541','U78.871','Drymouth','','','0','1','ปากแห้ง','0');
insert into b_icd10 values ('550010000542','U78.872','Oralerosion','','','0','1','แผลในปาก','0');
insert into b_icd10 values ('550010000543','U78.873','Maxillaryosteomyelitis','','','0','1','กระดูกขากรรไกรอักเสบ','0');
insert into b_icd10 values ('550010000544','U78.874','Exfoliativecheilitis','','','0','1','ริมฝีปากอักเสบ','0');
insert into b_icd10 values ('550010000545','U78.875','Lippustule','','','0','1','ตุ่มฝีที่ริมฝีปาก','0');
insert into b_icd10 values ('550010000546','U78.876','Lipcancer','','','0','1','มะเร็งที่ริมฝีปาก','0');
insert into b_icd10 values ('550010000547','U78.877','Deviatedmouth','','','0','1','ปากเบี้ยว(อัมพฤกษ์ใบหน้า)','0');
insert into b_icd10 values ('550010000548','U78.878','Tasteinthemouth','','','0','1','การรับรสผิดปกติ','0');
insert into b_icd10 values ('550010000549','U78.880','Toothache','','','0','1','ปวดฟัน','0');
insert into b_icd10 values ('550010000550','U78.881','Dentalcaries','','','0','1','ฟันผุ','0');
insert into b_icd10 values ('550010000551','U78.882','Gumatrophy','','','0','1','เหงือกร่น','0');
insert into b_icd10 values ('550010000552','U78.883','Ulcerativegingivitis','','','0','1','เหงือกอักเสบ','0');
insert into b_icd10 values ('550010000553','U78.900','Stiffneck','','','0','1','ตกหมอน','0');
insert into b_icd10 values ('550010000554','U78.901','Sinewinjury','','','0','1','เส้นเอ็นบาดเจ็บ','0');
insert into b_icd10 values ('550010000555','U78.902','Rupturedsinew','','','0','1','เส้นเอ็นขาด','0');
insert into b_icd10 values ('550010000556','U78.903','Contractedsinew','','','0','1','เส้นเอ็นหด','0');
insert into b_icd10 values ('550010000557','U78.904','Thickenedsinew','','','0','1','เส้นเอ็นหนาตัว','0');
insert into b_icd10 values ('550010000558','U78.905','Hypertonicityofthesinew','','','0','1','เส้นเอ็นเกร็ง','0');
insert into b_icd10 values ('550010000559','U78.906','Impedimentofthesinew','','','0','1','ปวดเส้นเอ็น','0');
insert into b_icd10 values ('550010000560','U78.907','Sprain','','','0','1','ข้อแพลง','0');
insert into b_icd10 values ('550010000561','U78.910','Spasm','','','0','1','อาการหดเกร็ง','0');
insert into b_icd10 values ('550010000562','U78.911','Contracture','','','0','1','ตะคริว','0');
insert into b_icd10 values ('550010000563','U78.912','Contractureofthenapeandneck','','','0','1','ตะคริวของกล้ามเนื้อคอและหลัง','0');
insert into b_icd10 values ('550010000564','U78.913','Contractureofthelimb','','','0','1','ตะคริวของแขนขา','0');

-- U79.sql
insert into b_icd10 values ('550010000565','U79.000','Yin pattern / syndrome01ภาวะ / กลุ่มอาการอิน','0');
insert into b_icd10 values ('550010000566','U79.001','Yang pattern / syndrome','','','0','1','ภาวะ / กลุ่มอาการหยาง','0');
insert into b_icd10 values ('550010000567','U79.002','Pattern / syndrome of yin-yang disharmony','','','0','1','ภาวะ / กลุ่มอาการอินหยางเสียสมดุล','0');
insert into b_icd10 values ('550010000568','U79.003','Yang damage pattern / syndrome','','','0','1','ภาวะ / กลุ่มอาการหยางถูกทำลาย ','0');
insert into b_icd10 values ('550010000569','U79.004','Yin damage pattern / syndrome','','','0','1','ภาวะ / กลุ่มอาการอินถูกทำลาย','0');
insert into b_icd10 values ('550010000570','U79.005','Yin deficiency pattern / syndrome','','','0','1','ภาวะ / กลุ่มอาการอินพร่อง','0');
insert into b_icd10 values ('550010000571','U79.006','Yang deficiency pattern / syndrome','','','0','1','ภาวะ / กลุ่มอาการหยางพร่อง ','0');
insert into b_icd10 values ('550010000572','U79.007','Pattern / syndrome of dual deficiency of yin and yang','','','0','1','ภาวะ / กลุ่มอาการอินและหยางล้วนพร่อง','0');
insert into b_icd10 values ('550010000573','U79.008','Pattern / syndrome of yin deficiency with internal heat','','','0','1','ภาวะ / กลุ่มอาการร้อนภายในจากอินพร่อง','0');
insert into b_icd10 values ('550010000574','U79.009','Pattern / syndrome of yin deficiency with effulgent fire','','','0','1','ภาวะ / กลุ่มอาการไฟกำเริบจากอินพร่อง','0');
insert into b_icd10 values ('550010000575','U79.010','Pattern / syndrome of yin deficiency with yang hyperactivity','','','0','1','ภาวะ / กลุ่มอาการหยางกำเริบจากอินพร่อง','0');
insert into b_icd10 values ('550010000576','U79.011','Pattern / syndrome of yin deficiency with fluid depletion','','','0','1','ภาวะ / กลุ่มอาการขาดสารน้ำจากอินพร่อง','0');
insert into b_icd10 values ('550010000577','U79.012','Pattern / syndrome of yin deficiency with water retention','','','0','1','ภาวะ / กลุ่มอาการน้ำคั่งค้างจากอินพร่อง','0');
insert into b_icd10 values ('550010000578','U79.013','Pattern / syndrome of yin deficiency and dampness-heat','','','0','1','ภาวะ / กลุ่มอาการอินพร่องที่มีภาวะร้อนชื้นแทรกซ้อน','0');
insert into b_icd10 values ('550010000579','U79.014','Pattern / syndrome of yin deficiency and blood stasis','','','0','1','ภาวะ / กลุ่มอาการอินพร่องที่มีภาวะเลือดคั่งแทรกซ้อน','0');
insert into b_icd10 values ('550010000580','U79.015','Pattern / syndrome of yang deficiency with qi stagnation','','','0','1','ภาวะ / กลุ่มอาการชี่ติดขัดจากหยางพร่อง','0');
insert into b_icd10 values ('550010000581','U79.016','Pattern / syndrome of yang deficiency with dampness obstruction','','','0','1','ภาวะ / กลุ่มอาการชื้นอุดกั้นจากหยางพร่อง','0');
insert into b_icd10 values ('550010000582','U79.017','Pattern / syndrome of yang deficiency with water flood','','','0','1','ภาวะ / กลุ่มอาการบวมน้ำจากหยางพร่อง','0');
insert into b_icd10 values ('550010000583','U79.018','Pattern / syndrome of yang deficiency with congealing phlegm','','','0','1','ภาวะ / กลุ่มอาการหยางพร่องที่มีภาวะเสลดจับเป็นก้อนแทรกซ้อน','0');
insert into b_icd10 values ('550010000584','U79.019','Pattern / syndrome of yang deficiency with congealing cold','','','0','1','ภาวะ / กลุ่มอาการหยางพร่องที่ชี่และเลือดติดขัดจากความเย็น ','0');
insert into b_icd10 values ('550010000585','U79.020','Yin-blood depletion pattern / syndrome','','','0','1','ภาวะอินและเลือดพร่อง','0');
insert into b_icd10 values ('550010000586','U79.021','Pattern / syndrome of yin exuberance with yang debilitation','','','0','1','ภาวะ / กลุ่มอาการอินแรงหยางอ่อน','0');
insert into b_icd10 values ('550010000587','U79.022','Pattern / syndrome of exuberant yin repelling yang','','','0','1','ภาวะ / กลุ่มอาการอินแรงขับหยางออกภายนอก','0');
insert into b_icd10 values ('550010000588','U79.023','Pattern / syndrome of detriment to yin affecting yang','','','0','1','ภาวะ / กลุ่มอาการอินพร่องทำให้หยางพร่องตาม','0');
insert into b_icd10 values ('550010000589','U79.024','Pattern / syndrome of detriment to yang affecting yin','','','0','1','ภาวะ / กลุ่มอาการหยางพร่องทำให้อินพร่องตาม','0');
insert into b_icd10 values ('550010000590','U79.025','Pattern / syndrome of yin exhaustion and yang collapse','','','0','1','ภาวะ / กลุ่มอาการอินและหยางวาย','0');
insert into b_icd10 values ('550010000591','U79.026','Pattern / syndrome of clear yang failing to ascend','','','0','1','ภาวะ / กลุ่มอาการหยางไม่ขึ้นสู่ส่วนบน','0');
insert into b_icd10 values ('550010000592','U79.027','Upcast yang pattern / syndrome','','','0','1','ภาวะ / กลุ่มอาการส่วนบนร้อนปลอม','0');
insert into b_icd10 values ('550010000593','U79.028','Yin collapse pattern / syndrome','','','0','1','ภาวะ / กลุ่มอาการอินวาย','0');
insert into b_icd10 values ('550010000594','U79.029','Yang collapse pattern / syndrome','','','0','1','ภาวะ / กลุ่มอาการหยางวาย','0');
insert into b_icd10 values ('550010000595','U79.030','Exterior pattern / syndrome','','','0','1','ภาวะ / กลุ่มอาการภายนอก','0');
insert into b_icd10 values ('550010000596','U79.031','Interior pattern / syndrome','','','0','1','ภาวะ / กลุ่มอาการภายใน','0');
insert into b_icd10 values ('550010000597','U79.032','Half-exterior half-interior pattern / syndrome','','','0','1','ภาวะ / กลุ่มอาการกึ่งภายนอกกับภายใน','0');
insert into b_icd10 values ('550010000598','U79.033','Exterior cold pattern / syndrome ','','','0','1','ภาวะ / กลุ่มอาการภายนอกจากพิษหนาว','0');
insert into b_icd10 values ('550010000599','U79.034','Exterior heat pattern / syndrome','','','0','1','ภาวะ / กลุ่มอาการภายนอกจากลมร้อน','0');
insert into b_icd10 values ('550010000600','U79.035','Exterior deficiency pattern / syndrome','','','0','1','ภาวะ / กลุ่มอาการภายนอกแบบพร่อง','0');
insert into b_icd10 values ('550010000601','U79.036','Exterior excess pattern / syndrome','','','0','1','ภาวะ / กลุ่มอาการภายนอกแบบแกร่ง','0');
insert into b_icd10 values ('550010000602','U79.037','Pattern / syndrome of wind-dampness assailing the exterior','','','0','1','ภาวะ / กลุ่มอาการภายนอกจากลมชื้น','0');
insert into b_icd10 values ('550010000603','U79.038','Pattern / syndrome of summerheat-dampness assailing the exterior','','','0','1','ภาวะ / กลุ่มอาการภายนอกจากอากาศร้อนชื้น','0');
insert into b_icd10 values ('550010000604','U79.039','Defense-exterior insecurity pattern / syndrome','','','0','1','ภาวะ / กลุ่มอาการเว่ยชี่ที่ส่วนภายนอกอ่อนแอ','0');
insert into b_icd10 values ('550010000605','U79.040','Interior cold pattern / syndrome','','','0','1','ภาวะ / กลุ่มอาการภายในเย็น','0');
insert into b_icd10 values ('550010000606','U79.041','Interior heat pattern / syndrome','','','0','1','ภาวะ / กลุ่มอาการภายในร้อน','0');
insert into b_icd10 values ('550010000607','U79.042','Pattern / syndrome of dual exterior and interior cold','','','0','1','ภาวะ / กลุ่มอาการทั้งภายนอกและภายในเย็น','0');
insert into b_icd10 values ('550010000608','U79.043','Interior excess pattern / syndrome','','','0','1','ภาวะ / กลุ่มอาการภายในแกร่งเกิน ','0');
insert into b_icd10 values ('550010000609','U79.044','Pattern / syndrome of dual exterior and interior cold','','','0','1','ภาวะ / กลุ่มอาการทั้งภายนอกและภายในเย็น','0');
insert into b_icd10 values ('550010000610','U79.045','Pattern / syndrome of dual exterior and interior heat','','','0','1','ภาวะ / กลุ่มอาการทั้งภายนอกและภายในร้อน','0');
insert into b_icd10 values ('550010000611','U79.046','Pattern / syndrome of dual exterior and interior excess','','','0','1','ภาวะ / กลุ่มอาการภายนอกและภายในล้วนแกร่งเกิน','0');
insert into b_icd10 values ('550010000612','U79.047','Pattern / syndrome of dual exterior and interior deficiency','','','0','1','ภาวะ/กลุ่มอาการภายนอกและภายในล้วนพร่อง','0');
insert into b_icd10 values ('550010000613','U79.048','Pattern / syndrome of exterior cold and interior heat','','','0','1','ภาวะ / กลุ่มอาการภายนอกเย็นภายในร้อน','0');
insert into b_icd10 values ('550010000614','U79.049','Pattern / syndrome of exterior heat and interior cold','','','0','1','ภาวะ / กลุ่มอาการภายนอกร้อนภายในเย็น','0');
insert into b_icd10 values ('550010000615','U79.050','Pattern / syndrome of exterior deficiency and interior excess','','','0','1','ภาวะ / กลุ่มอาการภายนอกพร่องภายในแกร่ง','0');
insert into b_icd10 values ('550010000616','U79.051','Pattern / syndrome of exterior excess and interior deficiency','','','0','1','ภาวะภายนอกแกร่งภายในพร่อง','0');
insert into b_icd10 values ('550010000617','U79.052','Pattern / syndrome of internal block and external collapse','','','0','1','ภาวะ / กลุ่มอาการภายในมีพิษภัยแกร่งปิดกั้น ภายนอกชี่พร่อง','0');
insert into b_icd10 values ('550010000618','U79.060','Qi deficiency pattern / syndrome','','','0','1','ภาวะ / กลุ่มอาการชี่พร่อง','0');
insert into b_icd10 values ('550010000619','U79.061','Qi sinking pattern / syndrome','','','0','1','ภาวะ / กลุ่มอาการชี่จม','0');
insert into b_icd10 values ('550010000620','U79.062','Qi stagnation pattern / syndrome','','','0','1','ภาวะ / กลุ่มอาการชี่ติดขัด','0');
insert into b_icd10 values ('550010000621','U79.063','Qi counterflow pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการชี่ทวนขึ้น','0');
insert into b_icd10 values ('550010000622','U79.064','Qi block pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการชี่ปิดกั้น','0');
insert into b_icd10 values ('550010000623','U79.065','Qi collapse pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการชี่ทรุดลง ','0');
insert into b_icd10 values ('550010000624','U79.066','Disordered qi movement pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการการไหลเวียนของชี่ผิดปกติ ','0');
insert into b_icd10 values ('550010000625','U79.067','Inhibited qi movement pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการชี่ไหลเวียนไม่คล่อง ','0');
insert into b_icd10 values ('550010000626','U79.068','Stagnant qi movement pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการการไหลเวียนของชี่ติดค้าง','0');
insert into b_icd10 values ('550010000627','U79.069','Qi depression pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการชี่คั่ง','0');
insert into b_icd10 values ('550010000628','U79.070','Deficiency pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการพร่อง ','0');
insert into b_icd10 values ('550010000629','U79.071','Excess pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการแกร่งเกิน','0');
insert into b_icd10 values ('550010000630','U79.072','Deficiency cold pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการเย็นจากพร่อง','0');
insert into b_icd10 values ('550010000631','U79.073','Deficiency heat pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการร้อนจากพร่อง','0');
insert into b_icd10 values ('550010000632','U79.074','Deficiency-excess complex pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการพร่องและแกร่งเกินทับซ้อนกัน','0');
insert into b_icd10 values ('550010000633','U79.075','Upper exuberance and lower deficiency pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการส่วนบนแกร่งเกิน ส่วนล่างพร่อง','0');
insert into b_icd10 values ('550010000634','U79.076','True deficiency and false excess pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการแกร่งไม่แท้','0');
insert into b_icd10 values ('550010000635','U79.077','True excess with false deficiency pattern / syndrome ','','','0','1','ภาวะ/กลุ่มอาการพร่องไม่แท้','0');
insert into b_icd10 values ('550010000636','U79.078','Essential qi deficiency pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการจิงชี่พร่อง','0');
insert into b_icd10 values ('550010000637','U79.100','External wind pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการจากลมภายนอก ','0');
insert into b_icd10 values ('550010000638','U79.101','Internal wind pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการจากลมภายใน ','0');
insert into b_icd10 values ('550010000639','U79.102','Wind-stroke block pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการสมองขาดเลือดแบบเกร็ง','0');
insert into b_icd10 values ('550010000640','U79.103','Wind-stroke collapse pattern / syndrome','','','0','1','ภาวะกลุ่มอาการสมองขาดเลือดแบบแขนขาอ่อนเปลี้ย','0');
insert into b_icd10 values ('550010000641','U79.104','Excess heat pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการร้อนแกร่ง','0');
insert into b_icd10 values ('550010000642','U79.105','Excess cold pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการหนาวแกร่ง','0');
insert into b_icd10 values ('550010000643','U79.106','Pattern / syndrome of cold in the middle ','','','0','1','ภาวะ/กลุ่มอาการท้องถูกความเย็น','0');
insert into b_icd10 values ('550010000644','U79.107','Summerheat pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการอากาศร้อนจัด','0');
insert into b_icd10 values ('550010000645','U79.108','Dampness pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการชื้น','0');
insert into b_icd10 values ('550010000646','U79.109','External dryness pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการแห้งจากภายนอก','0');
insert into b_icd10 values ('550010000647','U79.110','Internal dryness pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการแห้งจากภายใน','0');
insert into b_icd10 values ('550010000648','U79.111','Cold dryness pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการเย็นแห้ง','0');
insert into b_icd10 values ('550010000649','U79.112','Warm dryness pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการร้อนแห้ง','0');
insert into b_icd10 values ('550010000650','U79.113','Pattern / syndrome of dryness affecting the clear orifice','','','0','1','ภาวะ/กลุ่มอาการตา จมูกและปากแห้ง','0');
insert into b_icd10 values ('550010000651','U79.114','Dryness bind pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการท้องผูกจากความแห้ง','0');
insert into b_icd10 values ('550010000652','U79.115','Excess fire pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการไฟแกร่ง','0');
insert into b_icd10 values ('550010000653','U79.116','Fire-heat pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการจากไฟร้อน','0');
insert into b_icd10 values ('550010000654','U79.117','Pattern / syndrome of deficiency fire flaming upward','','','0','1','ภาวะ/กลุ่มอาการส่วนบนร้อนจากไฟของภาวะพร่อง','0');
insert into b_icd10 values ('550010000655','U79.118','Pus pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการหนอง','0');
insert into b_icd10 values ('550010000656','U79.119','Food accumulation pattern / syndrome ','','','0','1','ภาวะ/กลุ่มอาการอาหารไม่ย่อย','0');
insert into b_icd10 values ('550010000657','U79.120','Worm accumulation pattern / syndrome ','','','0','1','ภาวะ/กลุ่มอาการพยาธิอุดตัน ','0');
insert into b_icd10 values ('550010000658','U79.121','Wind-phlegm pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการลมเสมหะ','0');
insert into b_icd10 values ('550010000659','U79.122','Cold-phlegm pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการเสมหะเย็น','0');
insert into b_icd10 values ('550010000660','U79.123','Heat-phlegm pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการเสมหะร้อน ','0');
insert into b_icd10 values ('550010000661','U79.124','Dryness-phlegm pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการเสมหะแห้ง','0');
insert into b_icd10 values ('550010000662','U79.125','Blood stasis-phlegm pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการเสมหะร่วมกับเลือดคั่ง ','0');
insert into b_icd10 values ('550010000663','U79.126','Purulent phlegm pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการเสมหะเป็นหนอง','0');
insert into b_icd10 values ('550010000664','U79.127','Dampness-phlegm pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการเสมหะชื้น','0');
insert into b_icd10 values ('550010000665','U79.128','Pattern / syndrome of binding of phlegm and qi','','','0','1','ภาวะ/กลุ่มอาการเสมหะจับกับชี่','0');
insert into b_icd10 values ('550010000666','U79.129','Pattern / syndrome of internal harassment of phlegm-heat','','','0','1','ภาวะ/กลุ่มอาการเสมหะร้อนรบกวนภายใน ','0');
insert into b_icd10 values ('550010000667','U79.130','Pattern / syndrome of internal block of phlegm-heat','','','0','1','ภาวะ/กลุ่มอาการเสมหะร้อนปิดกั้นภายใน ','0');
insert into b_icd10 values ('550010000668','U79.131','Pattern / syndrome of phlegm-heatstirring wind','','','0','1','ภาวะ/กลุ่มอาการลมภายในจากเสมหะร้อน','0');
insert into b_icd10 values ('550010000669','U79.132','Pattern / syndrome of lingering phlegm nodule ','','','0','1','ภาวะ/กลุ่มอาการก้อนจากเสมหะ ','0');
insert into b_icd10 values ('550010000670','U79.133','Pattern / syndrome of blood stasis with wind-dryness','','','0','1','ภาวะ/กลุ่มอาการลมแห้งจากเลือดคั่ง','0');
insert into b_icd10 values ('550010000671','U79.134','Pattern / syndrome of blood stasis with water retention','','','0','1','ภาวะ/กลุ่มอาการเลือดคั่งและน้ำติดค้าง','0');
insert into b_icd10 values ('550010000672','U79.135','Pattern / syndrome of internal obstruction of cold-dampness','','','0','1','ภาวะ/กลุ่มอาการพิษเย็นชื้นอุดกั้นภายใน','0');
insert into b_icd10 values ('550010000673','U79.136','Pattern / syndrome of congealing cold with blood stasis ','','','0','1','กลุ่มอาการเลือดคั่งจากพิษเย็น','0');
insert into b_icd10 values ('550010000674','U79.137','Blood cold pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการเย็นในเลือด','0');
insert into b_icd10 values ('550010000675','U79.138','Pattern / syndrome ofretained dampness-heat toxin','','','0','1','ภาวะ/กลุ่มอาการจากพิษร้อนชื้นสั่งสม','0');
insert into b_icd10 values ('550010000676','U79.139','Pattern / syndrome ofdampness-heat pouring downward','','','0','1','ภาวะ/กลุ่มอาการร้อนชื้นลงสู่ส่วนล่าง','0');
insert into b_icd10 values ('550010000677','U79.140','Pattern / syndrome ofpestilential toxin pouring downward','','','0','1','ภาวะ/กลุ่มอาการอัณฑะร้อนในโรคคางทูม','0');
insert into b_icd10 values ('550010000678','U79.141','Wind-toxin pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการผื่นลมพิษ','0');
insert into b_icd10 values ('550010000679','U79.142','Wind-fire-heat toxin pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการพิษอักเสบจากลมและไฟร้อน','0');
insert into b_icd10 values ('550010000680','U79.143','Fire toxin pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการพิษอักเสบจากไฟร้อน','0');
insert into b_icd10 values ('550010000681','U79.144','Pattern / syndrome of inward invasion of fire toxin','','','0','1','ภาวะ/กลุ่มอาการพิษไฟแทรกเข้าภายใน ','0');
insert into b_icd10 values ('550010000682','U79.145','Yin toxin pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการพิษอักเสบที่สังกัดอิน','0');
insert into b_icd10 values ('550010000683','U79.146','Pattern / syndrome of inward attack of snake venom','','','0','1','ภาวะ/กลุ่มอาการจากพิษงู','0');
insert into b_icd10 values ('550010000684','U79.147','Calculus obstruction pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการจากนิ่วอุดตัน','0');
insert into b_icd10 values ('550010000685','U79.148','Wind-cold pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการถูกลมเย็น','0');
insert into b_icd10 values ('550010000686','U79.149','Wind-heat pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการถูกลมร้อน','0');
insert into b_icd10 values ('550010000687','U79.150','Wind-fire pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการถูกลมและไฟ ','0');
insert into b_icd10 values ('550010000688','U79.151','Wind-dampness pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการจากถูกลมชื้น','0');
insert into b_icd10 values ('550010000689','U79.152','Wind-dryness pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการจากถูกลมแห้ง','0');
insert into b_icd10 values ('550010000690','U79.153','Heat toxin pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการพิษอักเสบจากร้อน ','0');
insert into b_icd10 values ('550010000691','U79.154','Dampness toxin pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการจากพิษอักเสบจากชื้น','0');
insert into b_icd10 values ('550010000692','U79.155','Cold-dampness pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการจากเย็นและชื้น','0');
insert into b_icd10 values ('550010000693','U79.156','Dampness-heat pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการร้อนชื้น ','0');
insert into b_icd10 values ('550010000694','U79.157','Yin summerheat pattern / syndrome','','','0','1','ภาวะ / กลุ่มอาการถูกเย็นในขณะอากาศร้อนจัด','0');
insert into b_icd10 values ('550010000695','U79.158','Phlegm-dampness pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการเสมหะจากความชื้น','0');
insert into b_icd10 values ('550010000696','U79.210','Pattern / syndrome of depressed qi transforming into fire ','','','0','1','ภาวะ/กลุ่มอาการชี่คั่งทำให้เกิดไฟกำเริบ','0');
insert into b_icd10 values ('550010000697','U79.211','Pattern / syndrome of congealing cold with qi stagnation ','','','0','1','ภาวะ/กลุ่มอาการชี่ติดขัดจากพิษเย็น','0');
insert into b_icd10 values ('550010000698','U79.212','Sunken middle qi pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการจงชี่ทรุดลง','0');
insert into b_icd10 values ('550010000699','U79.213','Pattern / syndrome of qi deficiency with failure to constrain','','','0','1','ภาวะ/กลุ่มอาการชี่พร่องไม่สามารถเก็บกักของเหลว','0');
insert into b_icd10 values ('550010000700','U79.214','Qi deficiency fever pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการร้อนจากชี่พร่อง ','0');
insert into b_icd10 values ('550010000701','U79.215','Pattern / syndrome of qi deficiency with dampness obstruction','','','0','1','ภาวะ/กลุ่มอาการชื้นอุดกั้นจากชี่พร่อง ','0');
insert into b_icd10 values ('550010000702','U79.216','Pattern / syndrome of qi deficiency with water retention ','','','0','1','ภาวะ/กลุ่มอาการน้ำคั่งค้างจากชี่พร่อง ','0');
insert into b_icd10 values ('550010000703','U79.217','Pattern / syndrome of qi deficiency with external contraction','','','0','1','ภาวะ/กลุ่มอาการชี่พร่องและพิษภัยภายนอกแทรกเข้า ','0');
insert into b_icd10 values ('550010000704','U79.218','Pattern / syndrome of dual deficiency of qi and yin','','','0','1','ภาวะ/กลุ่มอาการชี่และอินพร่อง','0');
insert into b_icd10 values ('550010000705','U79.219','Blood deficiency pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการเลือดพร่อง','0');
insert into b_icd10 values ('550010000706','U79.220','Blood collapse pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการเสียเลือด','0');
insert into b_icd10 values ('550010000707','U79.221','Blood stasis pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการเลือดคั่งอุดกั้น','0');
insert into b_icd10 values ('550010000708','U79.222','Blood amassment pattern / syndrome ','','','0','1','ภาวะ/กลุ่มอาการเลือดคั่งสั่งสมภายใน','0');
insert into b_icd10 values ('550010000709','U79.223','Blood heat pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการร้อนในเลือด','0');
insert into b_icd10 values ('550010000710','U79.224','Pattern / syndrome of dual deficiency of qi and blood','','','0','1','ภาวะ/กลุ่มอาการชี่และเลือดพร่อง','0');
insert into b_icd10 values ('550010000711','U79.225','Qi-blood disharmony pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการชี่และเลือดเสียสมดุล','0');
insert into b_icd10 values ('550010000712','U79.226','Pattern / syndrome of qi deficiency with blood stasis','','','0','1','ภาวะ/กลุ่มอาการเลือดคั่งจากชี่พร่อง ','0');
insert into b_icd10 values ('550010000713','U79.227','Pattern / syndrome of qi stagnation and blood stasis','','','0','1','ภาวะ/กลุ่มอาการเลือดคั่งจากชี่ติดค้าง','0');
insert into b_icd10 values ('550010000714','U79.228','Pattern / syndrome of qi collapse following bleeding','','','0','1','ภาวะ/กลุ่มอาการชี่ออกข้างนอกตามการเสียเลือด ','0');
insert into b_icd10 values ('550010000715','U79.229','Pattern / syndrome of qi failing to control the blood','','','0','1','ภาวะ/กลุ่มอาการชี่ไม่เก็บกักเลือด','0');
insert into b_icd10 values ('550010000716','U79.230','Pattern / syndrome of blood deficiency complicated by stasis','','','0','1','ภาวะ/กลุ่มอาการเลือดพร่องร่วมกับเลือดคั่ง ','0');
insert into b_icd10 values ('550010000717','U79.231','Pattern / syndrome of blood deficiency and congealing cold','','','0','1','ภาวะ/กลุ่มอาการเลือดพร่องร่วมกับถูกเย็น','0');
insert into b_icd10 values ('550010000718','U79.232','Pattern / syndrome of blood deficiency and wind-dryness','','','0','1','ภาวะ/กลุ่มอาการเลือดพร่องร่วมกับลมแห้ง','0');
insert into b_icd10 values ('550010000719','U79.233','Pattern / syndrome of blood deficiency engendering wind','','','0','1','ภาวะ/กลุ่มอาการเกิดลมภายในจากเลือดพร่อง','0');
insert into b_icd10 values ('550010000720','U79.300','Phlegm pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการจากถาน(เสมหะขุ่นข้นหรือเสลด) ','0');
insert into b_icd10 values ('550010000721','U79.301','Fluid retention pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการอิ่น (เสมหะใส)คั่งค้าง','0');
insert into b_icd10 values ('550010000722','U79.302','Water retention pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการน้ำคั่งค้าง ','0');
insert into b_icd10 values ('550010000723','U79.303','Humor collapse pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการเสียน้ำ ','0');
insert into b_icd10 values ('550010000724','U79.304','Fluid-humor deficiency pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการของเหลว (จินเย่) พร่อง ','0');
insert into b_icd10 values ('550010000725','U79.305','Fluid-qi deficiency pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการของเหลวและชี่พร่อง ','0');
insert into b_icd10 values ('550010000726','U79.306','Pattern / syndrome of qi stagnation with water retention','','','0','1','ภาวะ/กลุ่มอาการน้ำคั่งค้างจากชี่ติดขัด','0');
insert into b_icd10 values ('550010000727','U79.307','Pattern / syndrome of fluid retention in the chest and hypochondrium','','','0','1','ภาวะ/กลุ่มอาการของเหลวคั่งค้างในปอด ','0');
insert into b_icd10 values ('550010000728','U79.308','Pattern / syndrome of mutual contention of wind and water','','','0','1','ภาวะ/กลุ่มอาการจากลมกระทบน้ำ ','0');
insert into b_icd10 values ('550010000729','U79.400','Heart qi deficiency pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการชี่ของหัวใจพร่อง','0');
insert into b_icd10 values ('550010000730','U79.401','Heart blood deficiency pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการเลือดของหัวใจพร่อง','0');
insert into b_icd10 values ('550010000731','U79.402','Pattern / syndrome of dual deficiency of heart qi and blood ','','','0','1','ภาวะ/กลุ่มอาการชี่และเลือดของหัวใจพร่อง','0');
insert into b_icd10 values ('550010000732','U79.403','Pattern / syndrome of heart deficiency with timidity','','','0','1','ภาวะ/กลุ่มอาการขลาดกลัวจากชี่หัวใจพร่อง','0');
insert into b_icd10 values ('550010000733','U79.404','Heart yin deficiency pattern / syndrome','','','0','1','ภาวะ / กลุ่มอาการอินของหัวใจพร่อง ','0');
insert into b_icd10 values ('550010000734','U79.405','Heart yang deficiency pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการหยางของหัวใจพร่อง','0');
insert into b_icd10 values ('550010000735','U79.406','Heart yang collapse pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการหยางของหัวใจพร่องจนหลุดออกภายนอก','0');
insert into b_icd10 values ('550010000736','U79.407','Pattern / syndrome of heart fire flaming upward ','','','0','1','ภาวะ/กลุ่มอาการไฟของหัวใจร้อนขึ้นข้างบน','0');
insert into b_icd10 values ('550010000737','U79.408','Intense heart fire pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการไฟของหัวใจกำเริบ ','0');
insert into b_icd10 values ('550010000738','U79.409','Pattern / syndrome of heat harassing the heart spirit','','','0','1','ภาวะ/กลุ่มอาการความร้อนก่อกวนจิตใจ','0');
insert into b_icd10 values ('550010000739','U79.410','Pattern / syndrome oftransmission of heart heat to the small intestine','','','0','1','ภาวะ/กลุ่มอาการความร้อนจากหัวใจย้ายสู่ลำไส้เล็ก ','0');
insert into b_icd10 values ('550010000740','U79.411','Heart blood stasis (obstruction) pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการเลือดคั่งอุดกั้นหัวใจ ','0');
insert into b_icd10 values ('550010000741','U79.412','Heart vessel obstruction pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการหลอดเลือดหัวใจอุดตัน','0');
insert into b_icd10 values ('550010000742','U79.413','Pattern / syndrome of phlegm clouding the heart spirit','','','0','1','ภาวะ/กลุ่มอาการเสมหะครอบคลุมจิต ','0');
insert into b_icd10 values ('550010000743','U79.414','Pattern / syndrome of phlegm-fire harassing the heart','','','0','1','ภาวะ/กลุ่มอาการไฟเสมหะรบกวนจิต','0');
insert into b_icd10 values ('550010000744','U79.415','Pattern / syndrome of water qi intimidating the heart','','','0','1','ภาวะ/กลุ่มอาการน้ำรุกเข้าหัวใจ ','0');
insert into b_icd10 values ('550010000745','U79.416','Pattern / syndrome of (blood) stasis obstructing the brain collateral','','','0','1','ภาวะ/กลุ่มอาการเลือดคั่งอุดกั้นหลอดเลือดสมอง ','0');
insert into b_icd10 values ('550010000746','U79.417','Pattern / syndrome of qi block with syncope','','','0','1','ภาวะ/กลุ่มอาการหมดสติจากชี่ปิดกั้น','0');
insert into b_icd10 values ('550010000747','U79.418','Pattern / syndrome of fluid retention in the pericardium','','','0','1','ภาวะ/กลุ่มอาการของเหลวคั่งค้างในเยื่อหุ้มหัวใจ ','0');
insert into b_icd10 values ('550010000748','U79.420','Lung qi deficiency pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการชี่ปอดพร่อง ','0');
insert into b_icd10 values ('550010000749','U79.421','Lung yin deficiency pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการอินปอดพร่อง','0');
insert into b_icd10 values ('550010000750','U79.422','Lung yang deficiency pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการหยางปอดพร่อง ','0');
insert into b_icd10 values ('550010000751','U79.423','Pattern / syndrome of wind-cold assailing the lung','','','0','1','ภาวะ/กลุ่มอาการลมเย็นรุกเข้าปอด','0');
insert into b_icd10 values ('550010000752','U79.424','Pattern / syndrome of wind-cold fettering the lung','','','0','1','ภาวะ/กลุ่มอาการลมเย็นปิดล้อมปอด ','0');
insert into b_icd10 values ('550010000753','U79.425','Pattern / syndrome of wind-heat invading the lung','','','0','1','ภาวะ/กลุ่มอาการลมร้อนคุกคามปอด','0');
insert into b_icd10 values ('550010000754','U79.426','Pattern / syndrome of dryness invading the lung','','','0','1','ภาวะ/กลุ่มอาการพิษแห้งคุกคามปอด ','0');
insert into b_icd10 values ('550010000755','U79.427','Lung heat pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการปอดร้อน ','0');
insert into b_icd10 values ('550010000756','U79.428','Intense lung heat pattern / syndrome ','','','0','1','ภาวะ/กลุ่มอาการปอดร้อนรุนแรง ','0');
insert into b_icd10 values ('550010000757','U79.429','Pattern / syndrome of phlegm-heat obstructing the lung','','','0','1','ภาวะ/กลุ่มอาการเสมหะร้อนอุดกั้นปอด ','0');
insert into b_icd10 values ('550010000758','U79.430','Pattern / syndrome of phlegm turbidity obstructing the lung','','','0','1','ภาวะ/กลุ่มอาการเสลดอุดกั้นปอด ','0');
insert into b_icd10 values ('550010000759','U79.431','Pattern / syndrome of cold-phlegm obstructing the lung','','','0','1','ภาวะ/กลุ่มอาการเสมหะเย็นอุดกั้นปอด','0');
insert into b_icd10 values ('550010000760','U79.432','Pattern / syndrome of summerheat damaging the lung vessel','','','0','1','ภาวะ/กลุ่มอาการอากาศร้อนทำลายหลอดเลือดปอด ','0');
insert into b_icd10 values ('550010000761','U79.433','Pattern / syndrome of heat toxin blocking the lung','','','0','1','ภาวะ/กลุ่มอาการพิษร้อนอุดกั้นปอด ','0');
insert into b_icd10 values ('550010000762','U79.434','Pattern / syndrome of lung dryness with intestinal obstruction','','','0','1','ภาวะ/กลุ่มอาการปอดแห้งกระทบลำไส้','0');
insert into b_icd10 values ('550010000763','U79.440','Spleen deficiency pattern / syndrome','','','0','1','ภาวะ / กลุ่มอาการม้ามพร่อง','0');
insert into b_icd10 values ('550010000764','U79.441','Spleen qi deficiency pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการชี่ม้ามพร่อง','0');
insert into b_icd10 values ('550010000765','U79.442','Pattern / syndrome of spleen failing in transportation','','','0','1','ภาวะ/กลุ่มอาการการลำเลียงของม้ามผิดปกติ','0');
insert into b_icd10 values ('550010000766','U79.443','Spleen yin deficiency pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการอินของม้ามพร่อง','0');
insert into b_icd10 values ('550010000767','U79.444','Spleen yang deficiency pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการหยางของม้ามพร่อง ','0');
insert into b_icd10 values ('550010000768','U79.445','Pattern / syndrome of spleen failing to control the blood','','','0','1','ภาวะ/กลุ่มอาการม้ามไม่สามารถคุมเลือด','0');
insert into b_icd10 values ('550010000769','U79.446','Sunken spleen qi pattern /syndrome','','','0','1','ภาวะ/กลุ่มอาการชี่ทรุดลงจากม้ามพร่อง','0');
insert into b_icd10 values ('550010000770','U79.447','Pattern / syndrome of spleen deficiency with dampness encumbrance','','','0','1','ภาวะ/กลุ่มอาการม้ามพร่องถูกชื้นปิดล้อม','0');
insert into b_icd10 values ('550010000771','U79.448','Pattern / syndrome of spleen deficiency withstirring of wind','','','0','1','ภาวะ/กลุ่มอาการม้ามพร่องทำให้เกิดลมภายใน','0');
insert into b_icd10 values ('550010000772','U79.449','Pattern / syndrome of spleen deficiency withwater flood','','','0','1','ภาวะ/กลุ่มอาการบวมน้ำจากม้ามพร่อง','0');
insert into b_icd10 values ('550010000773','U79.450','Pattern / syndrome of cold-dampness encumbering the spleen','','','0','1','ภาวะกลุ่มอาการเย็นชื้นปิดล้อมม้าม','0');
insert into b_icd10 values ('550010000774','U79.451','Pattern / syndrome of dampness-heat in the spleen','','','0','1','ภาวะ/กลุ่มอาการร้อนชื้นสั่งสมในม้าม','0');
insert into b_icd10 values ('550010000775','U79.452','Pattern / syndrome of dampness-heat in the spleen and stomach','','','0','1','ภาวะ/กลุ่มอาการม้ามและกระเพาะอาหารร้อนชื้น','0');
insert into b_icd10 values ('550010000776','U79.453','Spleen-stomach deficiency cold pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการม้ามและกระเพาะอาหารเย็นจากพร่อง','0');
insert into b_icd10 values ('550010000777','U79.454','Spleen-stomach weakness pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการม้ามและกระเพาะอาหารอ่อนแอ','0');
insert into b_icd10 values ('550010000778','U79.455','Spleen-stomach yin deficiency pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการอินของม้ามและกระเพาะอาหารพร่อง','0');
insert into b_icd10 values ('550010000779','U79.456','Spleen-stomach disharmony pattern / syndrome','','','0','1','ภาวะ / กลุ่มอาการม้ามและกระเพาะอาหารไม่ประสานกัน','0');
insert into b_icd10 values ('550010000780','U79.460','Stomach deficiency pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการกระเพาะอาหารพร่อง','0');
insert into b_icd10 values ('550010000781','U79.461','Stomach qi deficiency pattern / syndrome ','','','0','1','ภาวะ/กลุ่มอาการชี่กระเพาะอาหารพร่อง','0');
insert into b_icd10 values ('550010000782','U79.462','Stomach yang deficiency pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการหยางกระเพาะอาหารพร่อง','0');
insert into b_icd10 values ('550010000783','U79.463','Stomach yin deficiency pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการอินของกระเพาะอาหารพร่อง','0');
insert into b_icd10 values ('550010000784','U79.464','Stomach cold pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการกระเพาะอาหารเย็น','0');
insert into b_icd10 values ('550010000785','U79.465','Stomach excess cold pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการกระเพาะอาหารเย็นแกร่ง','0');
insert into b_icd10 values ('550010000786','U79.466','Stomach heat pattern / syndrome ','','','0','1','ภาวะ/กลุ่มอาการกระเพาะอาหารร้อน','0');
insert into b_icd10 values ('550010000787','U79.467','Pattern / syndrome of (blood) stasis in the stomach collateral','','','0','1','ภาวะ/กลุ่มอาการหลอดเลือดของกระเพาะอาหารอุดตัน','0');
insert into b_icd10 values ('550010000788','U79.468','Pattern / syndrome of intestinal dryness and fluid depletion','','','0','1','ภาวะ/กลุ่มอาการแห้งของลำไส้จากขาดน้ำ','0');
insert into b_icd10 values ('550010000789','U79.469','Pattern / syndrome of blood deficiency and intestinal dryness','','','0','1','ภาวะ/กลุ่มอาการแห้งของลำไส้จากเลือดพร่อง','0');
insert into b_icd10 values ('550010000790','U79.470','Pattern / syndrome of cold stagnating in stomach and intestines','','','0','1','ภาวะ/กลุ่มอาการความเย็นอุดกั้นกระเพาะอาหารและลำไส้','0');
insert into b_icd10 values ('550010000791','U79.471','Intestinal dampness-heat pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการทางเดินลำไส้ร้อนชื้น','0');
insert into b_icd10 values ('550010000792','U79.472','Pattern / syndrome of intestinal heat and bowel excess','','','0','1','ภาวะ/กลุ่มอาการลำไส้ร้อนแกร่ง','0');
insert into b_icd10 values ('550010000793','U79.473','Gastrointestinal qi stagnation pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการชี่กระเพาะอาหารและลำไส้ติดขัด','0');
insert into b_icd10 values ('550010000794','U79.474','Pattern / syndrome of yin deficiency with stirring wind','','','0','1','ภาวะ/กลุ่มอาการอินพร่องทำให้เกิดลมภายใน','0');
insert into b_icd10 values ('550010000795','U79.475','Pattern / syndrome of fluid retention in the stomach and intestines','','','0','1','ภาวะ/กลุ่มอาการของเหลวค้างในกระเพาะอาหารและลำไส้','0');
insert into b_icd10 values ('550010000796','U79.476','Pattern / syndrome of worms accumulating in the intestines','','','0','1','ภาวะ/กลุ่มอาการพยาธิในลำไส้ ','0');
insert into b_icd10 values ('550010000797','U79.477','Large intestinal fluid deficiency pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการลำไส้ใหญ่น้ำพร่อง','0');
insert into b_icd10 values ('550010000798','U79.478','Large intestinal heat bind pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการความร้อนจับในลำไส้ใหญ่','0');
insert into b_icd10 values ('550010000799','U79.479','Large intestinal dampness-heat pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการลำไส้ใหญ่ร้อนชื้น','0');
insert into b_icd10 values ('550010000800','U79.480','Small intestine qi stagnation pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการชี่ลำไส้เล็กติดขัด','0');
insert into b_icd10 values ('550010000801','U79.500','Liver qi depression pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการชี่ตับคั่งค้าง','0');
insert into b_icd10 values ('550010000802','U79.501','Liver blood deficiency pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการเลือดตับพร่อง','0');
insert into b_icd10 values ('550010000803','U79.502','Liver yin deficiency pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการอินตับพร่อง','0');
insert into b_icd10 values ('550010000804','U79.503','Liver yang deficiency pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการหยางตับพร่อง','0');
insert into b_icd10 values ('550010000805','U79.504','Pattern / syndrome of internal stirring of liver wind','','','0','1','ภาวะ/กลุ่มอาการลมตับรบกวนภายใน ','0');
insert into b_icd10 values ('550010000806','U79.505','Pattern / syndrome of liver yang, transforming into wind','','','0','1','ภาวะ/กลุ่มอาการหยางตับทำให้เกิดลมขึ้นภายใน','0');
insert into b_icd10 values ('550010000807','U79.06','Pattern / syndrome of liver depression and qi stagnation','','','0','1','ภาวะ/กลุ่มอาการชี่ตับติดขัด','0');
insert into b_icd10 values ('550010000808','U79.507','Pattern / syndrome of liver depression and blood stasis','','','0','1','ภาวะ/กลุ่มอาการเลือดคั่งจากชี่ตับติดขัด','0');
insert into b_icd10 values ('550010000809','U79.508','Pattern / syndrome ofdepressed liver qi transforming into fire','','','0','1','ภาวะ/กลุ่มอาการร้อนจากชี่ตับติดขัด','0');
insert into b_icd10 values ('550010000810','U79.509','Pattern / syndrome of liver fire flaming upward ','','','0','1','ภาวะ/กลุ่มอาการไฟตับเผาขึ้นบน','0');
insert into b_icd10 values ('550010000811','U79.510','Intense liver fire pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการไฟตับกำเริบ','0');
insert into b_icd10 values ('550010000812','U79.511','Pattern / syndrome of ascendant hyperactivity of liver yang','','','0','1','ภาวะ/กลุ่มอาการหยางตับกำเริบขึ้นบน','0');
insert into b_icd10 values ('550010000813','U79.512','Liver-gallbladder dampness-heat pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการตับและถุงน้ำดีร้อนชื้น','0');
insert into b_icd10 values ('550010000814','U79.513','Pattern / syndrome of cold stagnating in the liver meridian','','','0','1','ภาวะ/กลุ่มอาการเส้นลมปราณตับติดขัดจากความเย็น','0');
insert into b_icd10 values ('550010000815','U79.514','Pattern / syndrome of dampness-heat in the liver meridian','','','0','1','ภาวะ/กลุ่มอาการร้อนชื้นในเส้นตับ','0');
insert into b_icd10 values ('550010000816','U79.515','Gallbladder heat pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการร้อนในถุงน้ำดี','0');
insert into b_icd10 values ('550010000817','U79.516','Pattern / syndrome of depressed gallbladder with harassing phlegm','','','0','1','ภาวะ/กลุ่มอาการชี่ของถุงน้ำดีคั่งค้างจากเสมหะ','0');
insert into b_icd10 values ('550010000818','U79.517','Gallbladder qi deficiency pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการชี่ถุงน้ำดีพร่อง','0');
insert into b_icd10 values ('550010000819','U79.518','Pattern / syndrome of worms harassing the gallbladder','','','0','1','ภาวะ/กลุ่มอาการพยาธิอุดตันถุงน้ำดี','0');
insert into b_icd10 values ('550010000820','U79.520','Kidney deficiency pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการไตพร่อง','0');
insert into b_icd10 values ('550010000821','U79.521','Kidney essence insufficiency pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการสารจิงของไตพร่อง','0');
insert into b_icd10 values ('550010000822','U79.522','Kidney qi deficiency pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการชี่ไตพร่อง','0');
insert into b_icd10 values ('550010000823','U79.523','Kidney qi insecurity pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการชี่ไตไม่เก็บกัก','0');
insert into b_icd10 values ('550010000824','U79.524','Kidney yin deficiency pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการอินไตพร่อง','0');
insert into b_icd10 values ('550010000825','U79.525','Pattern / syndrome of kidney yin deficiency with fire effulgence','','','0','1','ภาวะ/กลุ่มอาการร้อนกำเริบจากอินไตพร่อง','0');
insert into b_icd10 values ('550010000826','U79.526','Kidney yang deficiency pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการหยางไตพร่อง','0');
insert into b_icd10 values ('550010000827','U79.527','Pattern / syndrome of kidney failing to receive qi ','','','0','1','ภาวะ/กลุ่มอาการไตไม่ดึงเก็บชี่','0');
insert into b_icd10 values ('550010000828','U79.528','Pattern / syndrome of kidney deficiency with water flood','','','0','1','ภาวะ/กลุ่มอาการน้ำคั่งค้างจากไตพร่อง','0');
insert into b_icd10 values ('550010000829','U79.529','Kidney meridian cold-dampness pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการเส้นลมปราณไตเย็นและชื้น','0');
insert into b_icd10 values ('550010000830','U79.530','Bladder deficiency cold pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการกระเพาะปัสสาวะเย็นจากพร่อง','0');
insert into b_icd10 values ('550010000831','U79.531','Bladder dampness-heat pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการกระเพาะปัสสาวะร้อนชื้น','0');
insert into b_icd10 values ('550010000832','U79.532','Pattern / syndrome of heat accumulating in the bladder','','','0','1','ภาวะ/กลุ่มอาการความร้อนสะสมในกระเพาะปัสสาวะ','0');
insert into b_icd10 values ('550010000833','U79.540','Heart-kidney non-interaction pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการหัวใจและไตไม่ประสานกัน','0');
insert into b_icd10 values ('550010000834','U79.541','Heart-kidney yang deficiency pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการหยางของหัวใจและไตพร่อง','0');
insert into b_icd10 values ('550010000835','U79.542','Heart-kidney qi deficiency pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการชี่ของหัวใจและปอดพร่อง','0');
insert into b_icd10 values ('550010000836','U79.543','Pattern / syndrome of dual deficiency of the heart and spleen','','','0','1','ภาวะ/กลุ่มอาการหัวใจและม้ามพร่อง ','0');
insert into b_icd10 values ('550010000837','U79.544','Heart-liver blood deficiency pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการเลือดของหัวใจและตับพร่อง','0');
insert into b_icd10 values ('550010000838','U79.545','Lung-kidney qi deficiency pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการชี่ของปอดและไตพร่อง ','0');
insert into b_icd10 values ('550010000839','U79.546','Lung-kidney yin deficiency pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการอินของปอดและไตพร่อง','0');
insert into b_icd10 values ('550010000840','U79.547','Lung-kidney yang deficiency pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการหยางของไตและปอดพร่อง','0');
insert into b_icd10 values ('550010000841','U79.548','Spleen-lung qi deficiency pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการชี่ของม้ามและปอดพร่อง','0');
insert into b_icd10 values ('550010000842','U79.549','Pattern / syndrome of liver fire invading the lung ','','','0','1','ภาวะ/กลุ่มอาการไฟตับคุกคามปอด','0');
insert into b_icd10 values ('550010000843','U79.550','Pattern / syndrome of liver qi invading the stomach','','','0','1','ภาวะ/กลุ่มอาการตับและกระเพาะอาหารไม่ประสานกัน','0');
insert into b_icd10 values ('550010000844','U79.551','Pattern / syndrome of liver depression and spleen deficiency','','','0','1','ภาวะ/กลุ่มอาการชี่ของตับติดขัดและม้ามพร่อง','0');
insert into b_icd10 values ('550010000845','U79.552','Liver-kidney yin deficiency pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการอินของตับและไตพร่อง','0');
insert into b_icd10 values ('550010000846','U79.553','Spleen-kidney yang deficiency pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการหยางของม้ามและไตพร่อง ','0');
insert into b_icd10 values ('550010000847','U79.600','Greater yang disease pattern / syndrome','','','0','1','ภาวะ / กลุ่มอาการโรคไท่หยาง','0');
insert into b_icd10 values ('550010000848','U79.601','Greater yang meridian pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการเส้นลมปราณไท่หยาง ','0');
insert into b_icd10 values ('550010000849','U79.602','Greater yang bowel pattern / syndrome ','','','0','1','ภาวะ/กลุ่มอาการอวัยวะกลวงไท่หยาง','0');
insert into b_icd10 values ('550010000850','U79.603','Greater yang cold damage pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการไท่หยางถูกลมหนาว','0');
insert into b_icd10 values ('550010000851','U79.604','Greater yang water-retention pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการไท่หยางน้ำสะสม ','0');
insert into b_icd10 values ('550010000852','U79.605','Yang brightness disease pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการโรคหยางหมิง','0');
insert into b_icd10 values ('550010000853','U79.606','Yang brightness meridian pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการของเส้นลมปราณหยางหมิง','0');
insert into b_icd10 values ('550010000854','U79.607','Yang brightness bowel pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการอวัยวะกลวงหยางหมิง ','0');
insert into b_icd10 values ('550010000855','U79.608','Lesser yang disease pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการโรคเส้าหยาง','0');
insert into b_icd10 values ('550010000856','U79.609','Lesser yang meridian pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการเส้นลมปราณเส้าหยาง ','0');
insert into b_icd10 values ('550010000857','U79.610','Lesser yang bowel pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการอวัยวะกลวงเส้าหยาง','0');
insert into b_icd10 values ('550010000858','U79.611','Greater yin disease pattern / syndrome','','','0','1','ภาวะ / กลุ่มอาการโรคไท่อิน ','0');
insert into b_icd10 values ('550010000859','U79.612','Greater yin wind stroke pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการเส้นลมปราณไท่อินถูกพิษลม','0');
insert into b_icd10 values ('550010000860','U79.613','Lesser yin disease pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการโรคเส้าอิน ','0');
insert into b_icd10 values ('550010000861','U79.614','Lesser yin exterior cold pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการเส้าอินภายนอกเย็น ','0');
insert into b_icd10 values ('550010000862','U79.615','Lesser yin cold transformation pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการเส้าอินเย็น ','0');
insert into b_icd10 values ('550010000863','U79.616','Lesser yin heat transformation pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการเส้าอินร้อน ','0');
insert into b_icd10 values ('550010000864','U79.617','Reverting yin disease pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการเจ๋วอิน','0');
insert into b_icd10 values ('550010000865','U79.618','Reverting yin heat reversal pattern / syndrome','','','0','1','ภาวะ/ กลุ่มอาการเจ๋วอินแบบภายนอกเย็นภายในร้อน ','0');
insert into b_icd10 values ('550010000866','U79.619','Reverting yin cold reversal pattern / syndrome','','','0','1','ภาวะ/ กลุ่มอาการเจ๋วอินเย็น ','0');
insert into b_icd10 values ('550010000867','U79..620','Greater yang blood amassment pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการไท่หยางมีเลือดสะสม ','0');
insert into b_icd10 values ('550010000868','U79.621','Pattern / syndrome of heat entering blood chamber','','','0','1','ภาวะ/กลุ่มอาการมดลูกร้อน','0');
insert into b_icd10 values ('550010000869','U79.700','Defense aspect pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการของส่วนเว่ย','0');
insert into b_icd10 values ('550010000870','U79.701','Qi aspect pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการของส่วนชี่ ','0');
insert into b_icd10 values ('550010000871','U79.702','Nutrient aspect pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการของส่วนอิ๋ง ','0');
insert into b_icd10 values ('550010000872','U79.703','Blood aspect pattern / syndrome','','','0','1','ภาวะ/ กลุ่มอาการของส่วนเลือด(เซ่ว)','0');
insert into b_icd10 values ('550010000873','U79.704','Pattern / syndrome of both defense-qi aspects disease','','','0','1','ภาวะ/กลุ่มอาการโรคของส่วนเว่ยเกิดร่วมกับส่วนชี่ ','0');
insert into b_icd10 values ('550010000874','U79.705','Pattern / syndrome of both defense-nutrient aspects disease','','','0','1','ภาวะ/กลุ่มอาการโรคของส่วนเว่ยเกิดร่วมกับส่วนอิ๋ง','0');
insert into b_icd10 values ('550010000875','U79.706','Pattern / syndrome of dual blaze of qi-nutrient aspects','','','0','1','ภาวะ/กลุ่มอาการส่วนชี่และส่วนอิ๋งร้อน','0');
insert into b_icd10 values ('550010000876','U79.707','Pattern / syndrome of dual blaze of qi-blood aspects','','','0','1','ภาวะ/กลุ่มอาการส่วนชี่และส่วนเลือดร้อน','0');
insert into b_icd10 values ('550010000877','U79.708','Pattern / syndrome of heat entering the blood aspect','','','0','1','ภาวะ/กลุ่มอาการร้อนเข้าสู่ส่วนเลือด','0');
insert into b_icd10 values ('550010000878','U79.709','Pattern / syndrome of heat entering nutrient-blood aspects','','','0','1','ภาวะ/กลุ่มอาการร้อนเข้าสู่ส่วนอิ๋งและส่วนเลือด ','0');
insert into b_icd10 values ('550010000879','U79.710','Pattern / syndrome of exuberant heatstirring wind','','','0','1','ภาวะ/กลุ่มอาการร้อนจัดกระตุ้นลมภายใน ','0');
insert into b_icd10 values ('550010000880','U79.711','Pattern / syndrome of exuberant heatwith bleeding','','','0','1','ภาวะ/กลุ่มอาการร้อนจัดกระตุ้นเลือด','0');
insert into b_icd10 values ('550010000881','U79.712','Pattern / syndrome of heat entering the pericardium','','','0','1','ภาวะ/กลุ่มอาการร้อนเข้าสู่เยื่อหุ้มหัวใจ','0');
insert into b_icd10 values ('550010000882','U79.713','Residual heat pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการจากความร้อนตกค้าง','0');
insert into b_icd10 values ('550010000883','U79.714','Pattern / syndrome of toxin congesting the upper energizer','','','0','1','ภาวะ/กลุ่มอาการพิษร้อนอุดกั้นซ่างเจียว','0');
insert into b_icd10 values ('550010000884','U79.715','Pattern / syndrome of wind-heat with epidemic toxin','','','0','1','ภาวะ/กลุ่มอาการพิษระบาดจากลมร้อน','0');
insert into b_icd10 values ('550010000885','U79.716','Pattern / syndrome of dampness obstructing defense yang','','','0','1','ภาวะ/กลุ่มอาการชื้นอุดกั้นส่วนเว่ยหยาง','0');
insert into b_icd10 values ('550010000886','U79.717','Pattern / syndrome of pathogen hidden in the pleurodiaphragmatic interspace','','','0','1','ภาวะ/กลุ่มอาการพิษภัยเข้าสู่โม้หยวน','0');
insert into b_icd10 values ('550010000887','U79.718','Qi aspect dampness-heat pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการส่วนชี่ร้อนชื้น ','0');
insert into b_icd10 values ('550010000888','U79.719','Pattern / syndrome of dampness-heat obstructing qi movement','','','0','1','ภาวะ/กลุ่มอาการการไหลเวียนของชี่ถูกร้อนชื้นอุดกั้น','0');
insert into b_icd10 values ('550010000889','U79.720','Pattern / syndrome of dampness predominating over heat','','','0','1','ภาวะ/กลุ่มอาการร้อนชื้นแบบชื้นเด่น ','0');
insert into b_icd10 values ('550010000890','U79.721','Pattern / syndrome of heat predominanting over dampness','','','0','1','ภาวะ/กลุ่มอาการร้อนชื้นแบบร้อนเด่น ','0');
insert into b_icd10 values ('550010000891','U79.722','Spreading dampness-heat pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการเป็นแผลร้อนชื้น','0');
insert into b_icd10 values ('550010000892','U79.723','Pattern / syndrome of summerheat with cold-dampness','','','0','1','ภาวะ/กลุ่มอาการพิษอากาศร้อนร่วมกับเย็นชื้น ','0');
insert into b_icd10 values ('550010000893','U79.724','Pattern / syndrome of summerheat-dampness encumbering the middle energizer','','','0','1','ภาวะ/กลุ่มอาการพิษอากาศร้อนชื้นปิดล้อมจงเจียว ','0');
insert into b_icd10 values ('550010000894','U79.725','Summerheat pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการจากอากาศร้อน','0');
insert into b_icd10 values ('550010000895','U79.726','Summerheat dampness pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการอากาศร้อนชื้น ','0');
insert into b_icd10 values ('550010000896','U79.727','Pattern / syndrome of summerheat entering yang brightness','','','0','1','ภาวะ/กลุ่มอาการพิษอากาศร้อนรุกเข้าหยางหมิง ','0');
insert into b_icd10 values ('550010000897','U79.728','Pattern / syndrome of summerheat damaging fluid and qi','','','0','1','ภาวะ/กลุ่มอาการชี่และน้ำสึกหรอจากอากาศร้อน','0');
insert into b_icd10 values ('550010000898','U79.729','Pattern / syndrome of summerheat-heat stirring wind','','','0','1','ภาวะ/กลุ่มอาการอากาศร้อนกระตุ้นลมภายใน ','0');
insert into b_icd10 values ('550010000899','U79.900','Upper energizer disease pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการโรคของซ่างเจียว ','0');
insert into b_icd10 values ('550010000900','U79.901','Middle energizer disease pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการโรคของจงเจียว ','0');
insert into b_icd10 values ('550010000901','U79.902','Lower energizer disease pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการโรคของเซี่ยเจียว ','0');
insert into b_icd10 values ('550010000902','U79.903','Triple energizer dampness-heat pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการซานเจียวร้อนชื้น','0');
insert into b_icd10 values ('550010000903','U79.904','Upper energizer dampness-heat pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการซ่างเจียวร้อนชื้น','0');
insert into b_icd10 values ('550010000904','U79.905','Lower energizer dampness-heat pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการเซี่ยเจียวร้อนชื้น ','0');
insert into b_icd10 values ('550010000905','U79.910','Pattern / syndrome of insecurity of thoroughfare and conception vessels','','','0','1','ภาวะ/กลุ่มอาการชงเริ่นไม่เก็บรักษาเลือด','0');
insert into b_icd10 values ('550010000906','U79.911','Pattern / syndrome of disharmony of thoroughfare and conception vessels','','','0','1','ภาวะ/กลุ่มอาการชงเริ่นเสียสมดุล','0');
insert into b_icd10 values ('550010000907','U79.912','Pattern / syndrome of cold congealing in the uterus','','','0','1','ภาวะ/กลุ่มอาการมดลูกเย็น ','0');
insert into b_icd10 values ('550010000908','U79.913','Pattern / syndrome of (blood) stasis obstructing the uterus','','','0','1','ภาวะ/กลุ่มอาการเลือดคั่งในมดลูก','0');
insert into b_icd10 values ('550010000909','U79.914','Uterine deficiency cold pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการมดลูกเย็นจากพร่อง ','0');
insert into b_icd10 values ('550010000910','U79.915','Uterine dampness-heat pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการมดลูกร้อนชื้น','0');
insert into b_icd10 values ('550010000911','U79..916','Pattern / syndrome of accumulated heat in the uterus','','','0','1','ภาวะ/กลุ่มอาการร้อนคั่งในมดลูก ','0');
insert into b_icd10 values ('550010000912','U79.917','Pattern / syndrome of dampness-heat obstructing the essence chamber','','','0','1','ภาวะ/กลุ่มอาการถุงอสุจิถูกอุดกั้นจากร้อนชื้น ','0');
insert into b_icd10 values ('550010000913','U79.918','Pattern / syndrome of phlegm obstructing the essence chamber','','','0','1','ภาวะ/กลุ่มอาการถุงอสุจิถูกอุดกั้นจากเสมหะ','0');
insert into b_icd10 values ('550010000914','U79.919','Pattern / syndrome of (blood) stasis obstructing the essence chamber','','','0','1','ภาวะ/กลุ่มอาการถุงอสุจิถูกอุดกั้นจากเลือดคั่ง','0');
insert into b_icd10 values ('550010000915','U79.920','Pattern / syndrome of wind striking the meridians and collaterals','','','0','1','ภาวะ/กลุ่มอาการเส้นลมปราณถูกลม','0');
insert into b_icd10 values ('550010000916','U79.921','Pattern / syndrome of wind-cold assailing the collaterals','','','0','1','ภาวะ/กลุ่มอาการเส้นลมปราณถูกลมเย็น ','0');
insert into b_icd10 values ('550010000917','U79.922','Pattern / syndrome of wind-cold obstructing the collaterals','','','0','1','ภาวะ/กลุ่มอาการเส้นลมปราณถูกลมเย็นอุดกั้น ','0');
insert into b_icd10 values ('550010000918','U79.923','Pattern / syndrome of wind-prevailing migratory arthralgia','','','0','1','ภาวะ/กลุ่มอาการปวดข้อจากพิษลม ','0');
insert into b_icd10 values ('550010000919','U79.924','Pattern / syndrome of cold-prevailing agonizing arthralgia ','','','0','1','ภาวะ/กลุ่มอาการปวดข้อจากพิษเย็น','0');
insert into b_icd10 values ('550010000920','U79.925','Pattern / syndrome of dampness-prevailing fixed arthralgia','','','0','1','ภาวะ/กลุ่มอาการปวดข้อจากพิษชื้น ','0');
insert into b_icd10 values ('550010000921','U79.926','Pattern / syndrome of heat-obstructing arthralgia','','','0','1','ภาวะ/กลุ่มอาการปวดข้อจากพิษร้อน ','0');
insert into b_icd10 values ('550010000922','U79.927','Pattern / syndrome of stasis and stagnation due to traumatic injury','','','0','1','ภาวะ/กลุ่มอาการเลือดคั่งจากการบาดเจ็บ','0');
insert into b_icd10 values ('550010000923','U79.928','Pattern / syndrome ofdamage to sinew and bone','','','0','1','ภาวะ/กลุ่มอาการเส้นเอ็นและกระดูกบาดเจ็บ','0');
insert into b_icd10 values ('550010000924','U79.929','Pattern / syndrome ofwind-cold invading the head','','','0','1','ภาวะ/กลุ่มอาการลมเย็นกระทบศีรษะ ','0');
insert into b_icd10 values ('550010000925','U79.930','Pattern / syndrome ofwind-heat invading the head','','','0','1','ภาวะ/กลุ่มอาการลมร้อนกระทบศีรษะ','0');
insert into b_icd10 values ('550010000926','U79.931','Pattern / syndrome ofwind-heat invading the head','','','0','1','ภาวะ/กลุ่มอาการลมร้อนกระทบศีรษะ','0');
insert into b_icd10 values ('550010000927','U79.932','Pattern / syndrome ofstatic blood invading the head','','','0','1','ภาวะ/กลุ่มอาการเลือดคั่งกระทบศีรษะ ','0');
insert into b_icd10 values ('550010000928','U79.933','Pattern / syndrome of phlegm turbidity invading the head','','','0','1','ภาวะ/กลุ่มอาการเสลดกระทบศีรษะ','0');
insert into b_icd10 values ('550010000929','U79.934','Pattern / syndrome of liver fire invading the head','','','0','1','ภาวะ/กลุ่มอาการไฟตับกระทบศีรษะ ','0');
insert into b_icd10 values ('550010000930','U79.935','Pattern / syndrome of wind-fire attacking the eyes','','','0','1','ภาวะ/กลุ่มอาการลมร้อนรุกเข้าตา','0');
insert into b_icd10 values ('550010000931','U79.936','Pattern / syndrome of wind-dampness insulting the eyes','','','0','1','ภาวะ/กลุ่มอาการลมชื้นรุกเข้าตา ','0');
insert into b_icd10 values ('550010000932','U79.937','Pattern / syndrome of traumatic injury of ocular vessel','','','0','1','ภาวะ/กลุ่มอาการตาช้ำเลือดจากการบาดเจ็บ','0');
insert into b_icd10 values ('550010000933','U79.938','Pattern / syndrome ofworm accumulation transforming into malnutrition','','','0','1','ภาวะ/กลุ่มอาการตาขาดการบำรุงจากพยาธิ','0');
insert into b_icd10 values ('550010000934','U79.939','Pattern / syndrome ofliver fire blazing the ear','','','0','1','ภาวะ/กลุ่มอาการไฟตับรุกเข้าหู','0');
insert into b_icd10 values ('550010000935','U79.940','Pattern / syndrome of wind-heat invading the ear','','','0','1','ภาวะ/กลุ่มอาการลมร้อนรุกเข้าหู','0');
insert into b_icd10 values ('550010000936','U79.941','Pattern / syndrome ofdampness-heat invading the ear','','','0','1','ภาวะ/กลุ่มอาการร้อนชื้นรุกเข้าหู','0');
insert into b_icd10 values ('550010000937','U79.942','Pattern / syndrome ofphlegm-dampness attacking the ear','','','0','1','ภาวะ/กลุ่มอาการเสมหะชื้นรุกเข้าหู','0');
insert into b_icd10 values ('550010000938','U79.943','Pattern / syndrome ofqi deficiency with hearing loss','','','0','1','ภาวะ/กลุ่มอาการการได้ยินลดลงจากชี่พร่อง','0');
insert into b_icd10 values ('550010000939','U79.944','Pattern / syndrome of wind-cold invading the nose','','','0','1','ภาวะ/กลุ่มอาการลมเย็นรุกเข้าจมูก ','0');
insert into b_icd10 values ('550010000940','U79.945','Pattern / syndrome of wind-heat invading the nose','','','0','1','ภาวะ/กลุ่มอาการลมร้อนรุกเข้าจมูก','0');
insert into b_icd10 values ('550010000941','U79.946','Pattern / syndrome of qi deficiency with loss of smell','','','0','1','ภาวะ/กลุ่มอาการจมูกไม่ได้กลิ่นจากชี่พร่อง','0');
insert into b_icd10 values ('550010000942','U79.947','Pattern / syndrome of yin deficiency withdryness of the nose','','','0','1','ภาวะ/กลุ่มอาการของจมูกแห้งจากอินพร่อง ','0');
insert into b_icd10 values ('550010000943','U79.950','Pattern / syndrome of wind-cold assailing the throat','','','0','1','ภาวะ/กลุ่มอาการลมเย็นรุกเข้าลำคอ','0');
insert into b_icd10 values ('550010000944','U79.951','Pattern / syndrome of wind-heat invading the throat','','','0','1','ภาวะ/กลุ่มอาการลมร้อนรุกเข้าลำคอ ','0');
insert into b_icd10 values ('550010000945','U79.952','Pattern / syndrome of toxic heat attacking the throat','','','0','1','ภาวะ/กลุ่มอาการพิษร้อนรุกเข้าลำคอ ','0');
insert into b_icd10 values ('550010000946','U79.953','Pattern / syndrome ofqi stagnating and phlegm congealing in the throat','','','0','1','ภาวะ/กลุ่มอาการเสมหะติดค้างในลำคอจากชี่ติดขัด','0');
insert into b_icd10 values ('550010000947','U79.954','Pattern / syndrome of yin deficiency with dryness of the throat','','','0','1','ภาวะ/กลุ่มอาการคอแห้งจากอินพร่อง','0');
insert into b_icd10 values ('550010000948','U79.955','Pattern / syndrome of stomach fire blazing the gums','','','0','1','ภาวะ/กลุ่มอาการเหงือกร้อนจากไฟของกระเพาะอาหาร','0');
insert into b_icd10 values ('550010000949','U79.956','Pattern / syndrome of dampness-heat steaming the teeth','','','0','1','ภาวะ/กลุ่มอาการของฟันจากร้อนชื้น ','0');
insert into b_icd10 values ('550010000950','U79.957','Pattern / syndrome of deficiency fire scorching the gums','','','0','1','ภาวะ/กลุ่มอาการของเหงือกจากความร้อนที่เกิดจากพร่อง','0');
insert into b_icd10 values ('550010000951','U79.958','Pattern / syndrome of toxic fire attacking the lips','','','0','1','ภาวะ/กลุ่มอาการพิษร้อนรุกเข้าริมฝีปาก ','0');
insert into b_icd10 values ('550010000952','U79.959','Pattern / syndrome ofdampness-heat steaming the mouth','','','0','1','ภาวะ/กลุ่มอาการร้อนชื้นรุกเข้าช่องปาก','0');
insert into b_icd10 values ('550010000953','U79.960','Pattern / syndrome of dampness-heat steaming the tongue','','','0','1','ภาวะ/กลุ่มอาการร้อนชื้นรุกเข้าลิ้น','0');
insert into b_icd10 values ('550010000954','U79.961','Pattern / syndrome of heat toxin attacking the tongue','','','0','1','ภาวะ/กลุ่มอาการพิษร้อนรุกเข้าลิ้น ','0');
insert into b_icd10 values ('550010000955','U79.962','Sublingual blood stasis pattern / syndrome','','','0','1','ภาวะ/กลุ่มอาการเลือดคั่งที่ใต้ลิ้น','0');

-- EClaim_HospitalOS_181228.sql
BEGIN; 
INSERT INTO r_rp1253_adpcode SELECT '22105','Blood group (ABO- Cell and serum grouping) - Tube method [22105]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22105'); 
COMMIT;
BEGIN; 
INSERT INTO r_rp1253_adpcode SELECT '22112','Blood group (ABO- Cell and serum grouping) - Gel method [22112]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22112'); 
COMMIT;
BEGIN; 
INSERT INTO r_rp1253_adpcode SELECT '22106','ABO Cell grouping - Slide method (ในกรณีตรวจหมู่เลือดซ้ำเท่านั้น) [22106]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22106'); 
COMMIT;
BEGIN; 
INSERT INTO r_rp1253_adpcode SELECT '22108','RH. (D) Typing - Tube method [22108]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22108'); 
COMMIT;
BEGIN; 
INSERT INTO r_rp1253_adpcode SELECT '22113','Rh. (D) Typing - Gel method [22113]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22113'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '22109','Rh. Typing (Complete) [22109]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22109'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '22103','Antibody screening (Indirect antiglobulin test) - Tube method [22103]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22103'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '22104','Antibody screening, (Indirect antiglobulin test) - Gel method [22104]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22104'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '22116','Antibody screening A cell (Indirect antiglobulin test) - Gel method [22116]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22116'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '22117','Antibody screening B cell (Indirect antiglobulin test) - Gel method [22117]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22117'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '22110','Direct antiglobulin test - Tube method [22110]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22110'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '22111','Direct antiglobulin test - Gel method [22111]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22111'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '22146','Direct antiglobulin (Coomb''s test) monospecific 5 ชนิด IgG, IgM, IgA, C3c และ C3d - Gel method [22146]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22146'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '22147','Direct antiglobulin (Coomb''s test) monospecific 2 ชนิด IgG และ C3c - Gel method [22147]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22147'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '22120','Type and screen (ABO + Rh + ab screening) Tube method [22120]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22120'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '22121','Type and screen (ABO + Rh + ab screening) Gel method [22121]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22121'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '22114','Cross matching - Tube method [22114]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22114'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '22115','Cross matching - Gel method [22115]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22115'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '22101','Antibody identification - Tube method [22101]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22101'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '22102','Antibody identification - Gel method [22102]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22102'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '22122','Antibody titration (ABO) [22122]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22122'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '22123','Antibody titration (Rh) [22123]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22123'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '22124','Adsorption test [22124]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22124'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '22125','Elution test [22125]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22125'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '22126','Antigen C [22126]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22126'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '22127','Antigen c [22127]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22127'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '22128','Antigen Di(a) [22128]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22128'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '22129','Antigen E [22129]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22129'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '22130','Antigen e [22130]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22130'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '22131','Antigen Fy(a) [22131]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22131'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '22132','Antigen Fy(b) [22132]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22132'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '22133','Antigen Jk(a) [22133]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22133'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '22134','Antigen Jk(b) [22134]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22134'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '22135','Antigen K [22135]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22135'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '22136','Antigen k [22136]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22136'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '22137','Antigen Le(a) [22137]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22137'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '22138','Antigen Le(b) [22138]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22138'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '22139','Antigen Le(a) + Le(b) [22139]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22139'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '22140','Antigen M [22140]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22140'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '22141','Antigen Mi(a) [22141]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22141'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '22142','Antigen N [22142]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22142'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '22143','Antigen P1 [22143]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22143'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '22144','Antigen S [22144]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22144'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '22145','Antigen s [22145]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22145'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '22148','Neutralization test: ABH substance in saliva [22148]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '22148'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23601','Platelet crossmatch (Flow cytometry) [23601]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23601'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23602','Crossmatch for HLA Compatible Platelets [23602]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23602'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30608','Transfusion reaction- Leukoagglutinin [30608]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30608'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23151','Whole Blood [23151]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23151'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23152','Whole Blood (NAT) [23152]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23152'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23101','Whole Blood (สภากาชาด) [23101]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23101'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23154','Leukocyte Depleted Whole Blood (LDWB) (NAT) [23154]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23154'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23102','Leukocyte Depleted Whole Blood (LDWB) (สภากาชาด) [23102]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23102'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23153','Preoperative Autologous Whole Blood Donation (PAD - รวมค่า LAB) [23153]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23153'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23251','PRC [23251]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23251'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23252','PRC (NAT) [23252]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23252'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23201','PRC (NAT) (สภากาชาด) [23201]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23201'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23255','Leukocyte Depleted PRC [23255]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23255'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23256','Leukocyte Depleted PRC (NAT) [23256]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23256'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23203','Leukocyte Depleted PRC (NAT) (สภากาชาด) [23203]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23203'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23253','Leukocyte Poor PRC [23253]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23253'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23254','Leukocyte Poor PRC (NAT) [23254]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23254'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23202','Leukocyte Poor PRC (NAT) (สภากาชาด) [23202]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23202'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23206','Single Donor Red Cell. (SDR) - Non Filtered (NAT) (สภากาชาด) [23206]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23206'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23207','Single Donor Red Cell. (SDR) - Filtered (NAT) (สภากาชาด) [23207]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23207'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23208','Single Donor Red Cell. (SDR) - Non Filtered (NAT) [23208]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23208'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23209','Single Donor Red Cell. (SDR) - Filtered (NAT) [23209]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23209'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23456','Single Donor Granocyte [23456]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23456'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23457','Single Donor Granocyte Concentrate (สภากาชาด) [23457]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23457'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23470','Wash and Frozen Red Cell [23470]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23470'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23351','Random Platelet Concentrate [23351]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23351'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23358','Random Platelet Concentrate (NAT) [23358]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23358'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23301','Random Platelet Concentrate (NAT) (สภากาชาด) [23301]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23301'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23359','Leukocyte Depleted Platelet Concentrate 1 unit [23359]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23359'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23353','Leukocyte Depleted Pooled Platelet Concentrate 4 units (NAT) (Filtration method) [23353]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23353'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23303','Leukocyte Depleted Pooled Platelet Concentrate 4 units (NAT) (สภากาชาด) [23303]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23303'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23361','Leukocyte Poor Platelet Concentrate (NAT) [23361]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23361'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23352','Leukocyte Poor Pooled Platelet Concentrate 4 units (NAT) [23352]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23352'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23302','Leukocyte Poor Pooled Platelet Concentrate 4 units (NAT) (สภากาชาด) [23302]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23302'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23354','Single Donor Platelet Concentrate - Non-Filtered, Open System (NAT) [23354]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23354'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23355','Single Donor Platelet Concentrate - Filtered, Open System (NAT) [23355]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23355'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23356','Single Donor Platelet Concentrate - Non - Filtered, Close System (NAT) [23356]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23356'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23304','Single Donor Platelet Concentrate - Non - Filtered, Close System (NAT) (สภากาชาด) [23304]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23304'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23357','Single Donor Platelet Concentrate - Filtered, Close System (NAT) [23357]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23357'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23360','Leukodepleted Single Donor Platelet Concentrate (SDP) [23360]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23360'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23362','Single Donor Platelet Concentrate PAS-C (สภากาชาด) [23362]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23362'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23363','Leukodepleted Single Donor Platelet Concentrate (SDP) (NAT) [23363]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23363'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23364','Single Donor Platelets PAS-C [23364]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23364'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23451','Fresh Frozen Plasma [23451]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23451'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23452','Fresh Frozen Plasma (NAT) [23452]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23452'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23401','Fresh Frozen Plasma (NAT) (สภากาชาด) [23401]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23401'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23453','Leukocyte Depleted Fresh Frozen Plasma (LDFFP) [23453]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23453'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23469','Leukocyte Depleted Fresh Frozen Plasma (LDFFP) (สภากาชาด) [23469]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23469'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23461','Leukocyte Depleted Cryo-Removed Plasma (LDCRP) [23461]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23461'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23462','Leukocyte Depleted Cryo-Removed Plasma (LDCRP) (NAT) [23462]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23462'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23468','Leukocyte Depleted Cryo-Removed Plasma (LDCRP) (NAT) (สภากาชาด) [23468]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23468'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23458','Cryo-Removed Plasma [23458]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23458'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23459','Cryo-Removed Plasma (NAT) [23459]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23459'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23402','Cryo-Removed Plasma (NAT) (สภากาชาด) [23402]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23402'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23454','Aged Plasma/Cryo-Removed Plasma [23454]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23454'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23460','Aged Plasma/Cryo-Removed Plasma (NAT) [23460]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23460'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23463','Bovine thrombin (1000 IU) [23463]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23463'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23455','Cryoprecipitate [23455]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23455'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23465','Cryoprecipitate (NAT) [23465]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23465'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23403','Cryoprecipitate (NAT) (สภากาชาด) [23403]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23403'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23464','Leukocyte Depleted Cryoprecipitate (NAT) (สภากาชาด) [23464]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23464'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23466','Leukocyte Depleted Cryoprecipitate (NAT) [23466]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23466'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23467','Heat Treat Freeze Dried Cryoprecipitate (HTFDC) [23467]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23467'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23365','Pl Psorelen-treated Plateletpheresis PAS-C (สภากาชาด) [23365]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23365'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23502','Leukapheresis (ใช้เครื่อง Apheresis) [23502]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23502'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23503','Blood Exchange (ใช้เครื่อง Apheresis) [23503]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23503'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23504','Plasma Exchange (ใช้เครื่อง Apheresis) [23504]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23504'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23511','Therapuetic Blood Letting [23511]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23511'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23505','Stem Cell Processing for Autologous Bone Marrow Collection [23505]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23505'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23506','Stem Cell Processing for Autologous PBSC Collection [23506]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23506'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23507','Stem Cell Processing for Bone Marrow Collection [23507]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23507'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23508','Stem Cell Processing for Cord Blood Collection [23508]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23508'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23512','Peripheral Blood Stem Cell Collection [23512]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23512'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23513','Peripheral Stem Cell Collection (สภากาชาด) [23513]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23513'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30701','Stem Cell Cuture for BFU-E Number [30701]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30701'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30702','Stem Cell Cuture for CFU-GM Number [30702]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30702'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23603','Intem [23603]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23603'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23604','Extem [23604]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23604'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23605','Fibtem [23605]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23605'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23606','Aptem [23606]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23606'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23607','Heptem [23607]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23607'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23509','การจัดการการรับบริจาคโลหิต [23509]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23509'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23510','ค่าอุปกรณ์เชื่อมถุงเลือดโดยเครื่องอัตโนมัติ [23510]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23510'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23501','ค่าบริการฉายแสงเลือด [23501]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23501'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23515','Transfer Bag 300 ml. [23515]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23515'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23516','Plasma Transfer Set [23516]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23516'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '23518','NAT for HIV + HCV + HBV สำหรับตรวจในผู้ป่วย [23518]T14','14','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '23518'); 
COMMIT;
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '?30106','Acid phosphatase with tartrate (hairy cell leukemia) (หรือ Tartrate Resistant Acid Phosphatase, Qualitative) [?30106]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '?30106'); 
COMMIT;                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33804','Carboxyhemoglobin, (Quantitative) [33804]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33804'); 
COMMIT;                                                                                                            
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37106','Cold agglutinin, (Qualitative) [37106]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37106'); 
COMMIT;                                                                                                               
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30105','Erythrocyte Sedimentation Rate (ESR) [30105]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30105'); 
COMMIT;                                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '31205','Fat stain, Stool (Sudan IV stain) [31205]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '31205'); 
COMMIT;                                                                                                            
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30309','Fetal hemoglobin, (Qualitative) [30309]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30309'); 
COMMIT;                                                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30312','Ham''s test, Acid hemolysis [Presence] of Blood [30312]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30312'); 
COMMIT;                                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30111','Heinz body [30111]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30111'); 
COMMIT;                                                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30104','Hematocrit (centrifuged) [30104]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30104'); 
COMMIT;                                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '31209','Hemoglobin detection, Stool (Immunochemical, Qualitative) [31209]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '31209'); 
COMMIT;                                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30313','Hemoglobin typing [30313]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30313'); 
COMMIT;                                                                                                                            
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30112','Hemosiderin test (Qualitative) [30112]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30112'); 
COMMIT;                                                                                                               
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30113','Erythrocyte Inclusion body [30113]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30113'); 
COMMIT;                                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30108','Iron stain, Bone marrow [30108]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30108'); 
COMMIT;                                                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30114','Leukocyte Alkaline Phosphatase (LAP) score [30114]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30114'); 
COMMIT;                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30124','LE cell preparation, stain, examination [30124]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30124'); 
COMMIT;                                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '31203','Occult blood, Stool [31203]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '31203'); 
COMMIT;                                                                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30133','Osmotic fragility test, quantitative [30133]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30133'); 
COMMIT;                                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30125','Osmotic fragility test, screening [30125]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30125'); 
COMMIT;                                                                                                            
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30131','Platelet count (Manual) [30131]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30131'); 
COMMIT;                                                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30103','Reticulocyte count [30103]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30103'); 
COMMIT;                                                                                                                           
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30316','Serum viscosity, (Quantitative) [30316]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30316'); 
COMMIT;                                                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30127','Sudan Black stain [30127]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30127'); 
COMMIT;                                                                                                                            
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '31301','Cell count and diff, Body fluid [31301]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '31301'); 
COMMIT;                                                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30101','Complete blood count (CBC) [30101]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30101'); 
COMMIT;                                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30102','Complete blood count without smear [30102]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30102'); 
COMMIT;                                                                                                           
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30109','Cytochemical profile stain (ชนิดละ) [30109]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30109'); 
COMMIT;                                                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '31201','Direct smear, Stool (Blood Cell Count Panel) [31201]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '31201'); 
COMMIT;                                                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30107','Wright stain, Bone marrow (Differential panel - Bone marrow) [30107]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30107'); 
COMMIT;                                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30110','Wright stain, Buffy coat (Differential panel) [30110]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30110'); 
COMMIT;                                                                                                
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30217','Activated Protein C Resistance assay [30217]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30217'); 
COMMIT;                                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30231','Alpha 2 antiplasmin [30231]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30231'); 
COMMIT;                                                                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37233','Anti-Beta-2 glycoprotein 1 IgG (Quantitative) [37233]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37233'); 
COMMIT;                                                                                                
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37234','Anti-Beta-2 glycoprotein 1 IgM (Quantitative) [37234]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37234'); 
COMMIT;                                                                                                
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37207','Anti-Cardiolipin IgG (Quantitative) [37207]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37207'); 
COMMIT;                                                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37208','Anti-Cardiolipin IgM (Quantitative) [37208]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37208'); 
COMMIT;                                                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37020','Anticardiolipin (Quantitative) [37020]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37020'); 
COMMIT;                                                                                                               
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30229','Antithrombin III activity (chromogenic) [30229]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30229'); 
COMMIT;                                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30210','Bleeding time [30210]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30210'); 
COMMIT;                                                                                                                                
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30205','Clot retraction time/Clot lysis time [30205]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30205'); 
COMMIT;                                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30208','D-dimer (Automate) (Quantitative) [30208]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30208'); 
COMMIT;                                                                                                            
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30206','Euglobulin Lysis Time (ELT) [30206]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30206'); 
COMMIT;                                                                                                                  
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30215','Factor assay - Factor II [30215]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30215'); 
COMMIT;                                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30221','Factor assay - Factor IX [30221]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30221'); 
COMMIT;                                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30222','Factor assay - Factor IX Inhibitor (Quantitative) [30222]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30222'); 
COMMIT;                                                                                            
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30216','Factor assay - Factor V [30216]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30216'); 
COMMIT;                                                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30218','Factor assay - Factor VII [30218]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30218'); 
COMMIT;                                                                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30219','Factor assay - Factor VIII [30219]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30219'); 
COMMIT;                                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30220','Factor assay - Factor VIII Inhibitor (Quantitative) [30220]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30220'); 
COMMIT;                                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30223','Factor assay - Factor X [30223]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30223'); 
COMMIT;                                                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30224','Factor assay - Factor XI [30224]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30224'); 
COMMIT;                                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30225','Factor assay - Factor XII [30225]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30225'); 
COMMIT;                                                                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30207','Fibrin Degradation Product (FDP) [30207]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30207'); 
COMMIT;                                                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30214','Fibrinogen level [30214]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30214'); 
COMMIT;                                                                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30230','Heparin anti Xa [30230]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30230'); 
COMMIT;                                                                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30234','Lupus anticoagulant (confirm), dRVVT (dilute Russell''s Viper Venom Time) [30234]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30234'); 
COMMIT;                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30233','Lupus anticoagulant (screening) [30233]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30233'); 
COMMIT;                                                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30202','Partial Thomboplastin Time (PTT) [30202]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30202'); 
COMMIT;                                                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30226','Platelet aggregration (อย่างน้อยต้องมี การตรวจ ADP, Collagen และ Adrenaline) [30226]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30226'); 
COMMIT;                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30227','Protein C (chromogenic assay) [30227]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30227'); 
COMMIT;                                                                                                                
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30228','Protein S free antigen or activity [30228]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30228'); 
COMMIT;                                                                                                           
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30201','Prothombin Time (PT) and International Normalize Ratio (INR) [30201]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30201'); 
COMMIT;                                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30211','Ristocetin cofactor activity [30211]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30211'); 
COMMIT;                                                                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30212','Ristocetin induced agglutination (Quantitative) [30212]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30212'); 
COMMIT;                                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30203','Thrombin Time (TT) [30203]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30203'); 
COMMIT;                                                                                                                           
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30204','Venous Clotting Time (VCT) [30204]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30204'); 
COMMIT;                                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30209','Von Willebrand factor (Activity) [30209]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30209'); 
COMMIT;                                                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30508','CD3 Count [30508]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30508'); 
COMMIT;                                                                                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30509','CD4 Count [30509]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30509'); 
COMMIT;                                                                                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30510','CD8 Count [30510]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30510'); 
COMMIT;                                                                                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30501','Flow cytometry for acute leukemia panel, Acute Myeloid Leukemia (AML), Acute Lymphoblastic Leukemia (ALL) PANEL.CELLMARKER [30501]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30501'); 
COMMIT;                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30503','Flow cytometry for detection of DAF (Diaminofluorescein) (CD55 and CD59) (blood) [30503]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30503'); 
COMMIT;                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30507','Flow cytometry for minimal residual disease, AML panel [30507]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30507'); 
COMMIT;                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30505','Flow cytometry for minimal residual disease, B-ALL panel (B Lymphocytes) [30505]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30505'); 
COMMIT;                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30506','Flow cytometry for minimal residual disease, T-ALL panel (T Lymphocytes) [30506]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30506'); 
COMMIT;                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30502','Flow cytometry for Non Hodgkin''s lymphoma panel [30502]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30502'); 
COMMIT;                                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30504','Flow cytometry for CD34 surface antigen (blood) [30504]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30504'); 
COMMIT;                                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30612','HLA - A DNA typing (low resolution) [30612]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30612'); 
COMMIT;                                                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30628','HLA - A DNA typing (high resolution) [30628]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30628'); 
COMMIT;                                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30609','HLA - A, B DNA typing [30609]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30609'); 
COMMIT;                                                                                                                        
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30614','HLA - A2 DNA subtyping [30614]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30614'); 
COMMIT;                                                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30615','HLA - ABC DNA typing [30615]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30615'); 
COMMIT;                                                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30616','HLA - B DNA typing (low resolution) [30616]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30616'); 
COMMIT;                                                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30629','HLA - B DNA typing (high resolution) [30629]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30629'); 
COMMIT;                                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30617','HLA - B15 DNA subtyping [30617]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30617'); 
COMMIT;                                                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30618','HLA - C DNA typing (low resolution) [30618]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30618'); 
COMMIT;                                                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30630','HLA - C DNA typing (high resolution) [30630]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30630'); 
COMMIT;                                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30619','HLA - DQA DNA typing [30619]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30619'); 
COMMIT;                                                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30620','HLA antibody screening - Luminex [30620]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30620'); 
COMMIT;                                                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30606','HLA class II DNA high resolution (DRB, DQB) typing [30606]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30606'); 
COMMIT;                                                                                           
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30611','HLA class II DNA low resolution (DRB, DQB) typing [30611]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30611'); 
COMMIT;                                                                                            
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30517','Lymphocyte Crossmatch (T, B cell) (Flow Cytometry) [30517]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30517'); 
COMMIT;                                                                                           
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30603','Lymphocyte crossmatch (T, B cell) ทั้งผู้ให้และผู้รับอวัยวะ [30603]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30603'); 
COMMIT;                                                                                  
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37263','MICA antibody [37263]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37263'); 
COMMIT;                                                                                                                                
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37264','MICA genotyping [37264]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37264'); 
COMMIT;                                                                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30623','Single Antigen Antibody HLA Class I - Luminex HLA-A & B & C (class I) Ab.IgG panel [30623]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30623'); 
COMMIT;                                                           
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30624','Single Antigen Antibody HLA Class II - Luminex HLA-DP & DQ & DR (class II) Ab.IgG panel [30624]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30624'); 
COMMIT;                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30626','Specific PRA HLA Class I - Luminex (HLA-A+B+C Ab) [30626]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30626'); 
COMMIT;                                                                                            
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30627','Specific PRA HLA Class II - Luminex (HLA-DP+DQ+DR Ab) [30627]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30627'); 
COMMIT;                                                                                        
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30602','Transplantation Autoantibody (T, B cell) ทั้งผู้ให้และผู้รับอวัยวะ [30602]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30602'); 
COMMIT;                                                                           
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30601','HLA-B27 Serologic typing [30601]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30601'); 
COMMIT;                                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30621','HLA-B*1502 allele -Realtime PCR (HLA-B*15:02) [30621]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30621'); 
COMMIT;                                                                                                
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30622','HLA-B*5801 allele -Realtime PCR (HLA-B*58:01) [30622]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30622'); 
COMMIT;                                                                                                
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30607','Transfusion reaction - HLA antibody [30607]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30607'); 
COMMIT;                                                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '31004','Albumin, Urine (Qualitative) [31004]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '31004'); 
COMMIT;                                                                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '31011','Alkaptonuria, Urine (Homogentisate) [31011]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '31011'); 
COMMIT;                                                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '31102','Bence-Jones protein, Urine (Immunoglobulin light chains) [31102]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '31102'); 
COMMIT;                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '31006','Bile, Urine [31006]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '31006'); 
COMMIT;                                                                                                                                  
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '31005','Glucose, Urine (Qualitative) [31005]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '31005'); 
COMMIT;                                                                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '31010','Hemosiderin, Urine [31010]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '31010'); 
COMMIT;                                                                                                                           
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '31007','Ketone, Urine (Test strip) [31007]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '31007'); 
COMMIT;                                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '31003','pH, Urine [31003]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '31003'); 
COMMIT;                                                                                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '31101','Pregnancy test, Urine [31101]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '31101'); 
COMMIT;                                                                                                                        
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '31002','Specific gravity [31002]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '31002'); 
COMMIT;                                                                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '31001','Urinalysis (Physical + Chemical + Microscopic) PANEL.UA [31001]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '31001'); 
COMMIT;                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '31009','Urobilinogen, Urine [31009]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '31009'); 
COMMIT;                                                                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32106','Calcium (Serum or Plasma, mg/dL) [32106]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32106'); 
COMMIT;                                                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '34118','Calcium, 24 hr Urine [34118]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '34118'); 
COMMIT;                                                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32110','Calcium, ionized [32110]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32110'); 
COMMIT;                                                                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '34006','Calcium, Urine [34006]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '34006'); 
COMMIT;                                                                                                                               
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32104','Chloride [32104]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32104'); 
COMMIT;                                                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32105','CO2 [32105]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32105'); 
COMMIT;                                                                                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '31012','Iodine, Urine [31012]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '31012'); 
COMMIT;                                                                                                                                
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30304','Iron, Serum [30304]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30304'); 
COMMIT;                                                                                                                                  
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32107','Magnesium [32107]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32107'); 
COMMIT;                                                                                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '34119','Magnesium, 24 hr Urine [34119]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '34119'); 
COMMIT;                                                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32101','Osmolality, Serum [32101]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32101'); 
COMMIT;                                                                                                                            
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '34001','Osmolality, Urine [34001]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '34001'); 
COMMIT;                                                                                                                            
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32109','Phosphorus (Phosphate) [32109]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32109'); 
COMMIT;                                                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '34120','Phosphorus, 24 hr Urine [34120]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '34120'); 
COMMIT;                                                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32103','Potassium [32103]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32103'); 
COMMIT;                                                                                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '34121','Potassium, 24 hr Urine [34121]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '34121'); 
COMMIT;                                                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '34004','Potassium, Urine [34004]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '34004'); 
COMMIT;                                                                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32102','Sodium [32102]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32102'); 
COMMIT;                                                                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '34122','Sodium, 24 hr Urine [34122]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '34122'); 
COMMIT;                                                                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '34002','Sodium, Urine [34002]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '34002'); 
COMMIT;                                                                                                                                
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32108','Zinc, Serum/Urine [32108]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32108'); 
COMMIT;                                                                                                                            
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32210','Ammonia [32210]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32210'); 
COMMIT;                                                                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37254','B-crosslaps (Collagen crosslinked C-telopeptide) [37254]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37254'); 
COMMIT;                                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32414','Beta carotene [32414]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32414'); 
COMMIT;                                                                                                                                
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32207','Bilirubin, Direct (Bilirubin.glucuronidated + Bilirubin.albumin bound) [32207]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32207'); 
COMMIT;                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32209','Bilirubin, Micro (Neonatal bilirubin panel) [32209]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32209'); 
COMMIT;                                                                                                  
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32208','Bilirubin, Total [32208]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32208'); 
COMMIT;                                                                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32201','BUN (Blood Urea Nitrogen) [32201]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32201'); 
COMMIT;                                                                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '34124','Citrate, 24 hr Urine [34124]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '34124'); 
COMMIT;                                                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32202','Creatinine [32202]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32202'); 
COMMIT;                                                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '34102','Creatinine, 24 hr Urine [34102]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '34102'); 
COMMIT;                                                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '34101','Creatinine, Urine [34101]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '34101'); 
COMMIT;                                                                                                                            
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30306','Ferritin [30306]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30306'); 
COMMIT;                                                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32415','Folate [32415]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32415'); 
COMMIT;                                                                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32206','Fructosamine [32206]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32206'); 
COMMIT;                                                                                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32203','Glucose (Blood, Urine, Other) (Quantitative) [32203]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32203'); 
COMMIT;                                                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30311','Homocysteine [30311]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30311'); 
COMMIT;                                                                                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32216','Ketones, Serum (Quantitative) [32216]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32216'); 
COMMIT;                                                                                                                
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32008','Lactate [32008]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32008'); 
COMMIT;                                                                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32501','Lipid - Cholesterol [32501]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32501'); 
COMMIT;                                                                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32503','Lipid - HDL - cholesterol Cholesterol in HDL [32503]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32503'); 
COMMIT;                                                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32502','Lipid - TG (Triglyceride) [32502]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32502'); 
COMMIT;                                                                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32504','Lipid - LDL - chol (direct) Cholesterol in LDL, Direct assay สั่งรายการเดียว [32504]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32504'); 
COMMIT;                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '34125','Oxalate, Urine (Quantitative) [34125]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '34125'); 
COMMIT;                                                                                                                
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '34115','Porphyrin, Urine (Quantitative) [34115]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '34115'); 
COMMIT;                                                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '34126','Sulfate, Urine (Quantitative) [34126]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '34126'); 
COMMIT;                                                                                                                
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32205','Uric acid (Urate)(Quantitative) [32205]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32205'); 
COMMIT;                                                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32409','Vitamin A (Retinol) [32409]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32409'); 
COMMIT;                                                                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32410','Vitamin B1 (Thiamine) [32410]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32410'); 
COMMIT;                                                                                                                        
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32416','Vitamin B2 (Riboflavin) [32416]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32416'); 
COMMIT;                                                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32417','Vitamin B6 (Pyridoxine) [32417]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32417'); 
COMMIT;                                                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32411','Vitamin B12 (Cobalamins) [32411]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32411'); 
COMMIT;                                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32412','Vitamin C (Ascorbic acid) [32412]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32412'); 
COMMIT;                                                                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32418','Vitamin D [32418]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32418'); 
COMMIT;                                                                                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32413','Vitamin E (Tocopherols) [32413]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32413'); 
COMMIT;                                                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32301','Acid phosphatase, Total [32301]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32301'); 
COMMIT;                                                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33801','Adenosine deaminase (body fluid) [33801]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33801'); 
COMMIT;                                                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32403','Albumin (Quantitative) [32403]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32403'); 
COMMIT;                                                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32314','Aldolase [32314]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32314'); 
COMMIT;                                                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32309','Alkaline phosphatase [32309]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32309'); 
COMMIT;                                                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37250','Alpha 1 antitrypsin (Quantitative) [37250]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37250'); 
COMMIT;                                                                                                           
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32303','Amylase, Serum [32303]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32303'); 
COMMIT;                                                                                                                               
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '34114','Amylase, Urine [34114]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '34114'); 
COMMIT;                                                                                                                               
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32505','Apo lipoprotein A [32505]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32505'); 
COMMIT;                                                                                                                            
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32506','Apo lipoprotein B [32506]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32506'); 
COMMIT;                                                                                                                            
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37304','Beta 2 microglobulin, serum/urine (Quantitative) [37304]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37304'); 
COMMIT;                                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37315','Beta 2 glycoprotein IgG (?2 GP1) (Quantitative) [37315]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37315'); 
COMMIT;                                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32406','Ceruloplasmin [32406]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32406'); 
COMMIT;                                                                                                                                
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33803','Cholinesterase, Plasma or red cell (Quantitative) [33803]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33803'); 
COMMIT;                                                                                            
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32305','Creatine Kinase-MB (CK-MB) [32305]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32305'); 
COMMIT;                                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32304','Creatinine Phosphokinase (CPK) (Creatine kinase) (Quantitative) [32304]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32304'); 
COMMIT;                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37103','C-reactive protein (CRP) (Quantitative) [37103]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37103'); 
COMMIT;                                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37218','C-reactive protein (CRP), High sensitivity [37218]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37218'); 
COMMIT;                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37219','Cryoglobulin (Qualitative) [37219]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37219'); 
COMMIT;                                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30310','G-6-PD Qualitative [30310]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30310'); 
COMMIT;                                                                                                                           
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30322','G-6-PD Quantitative [30322]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30322'); 
COMMIT;                                                                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32312','Gamma glutamyl transpeptidase [32312]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32312'); 
COMMIT;                                                                                                                
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30317','Haptoglobin (Quantitative) [30317]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30317'); 
COMMIT;                                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32401','Hb A1C [32401]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32401'); 
COMMIT;                                                                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32306','LDH Lactate dehydrogenase (Quantitative) [32306]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32306'); 
COMMIT;                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32315','LDH isoenzyme [32315]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32315'); 
COMMIT;                                                                                                                                
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32313','Lipase (Triacylglycerol lipase) [32313]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32313'); 
COMMIT;                                                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32507','Lipoprotein a [32507]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32507'); 
COMMIT;                                                                                                                                
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '34116','Microalbumin, Urine [34116]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '34116'); 
COMMIT;                                                                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32316','Myoglobin (Quantitative) [32316]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32316'); 
COMMIT;                                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32407','NT-pro BNP (Natriuretic peptide.B prohormone N-Terminal) [32407]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32407'); 
COMMIT;                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32614','Osteocalcin [32614]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32614'); 
COMMIT;                                                                                                                                  
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32408','Prealbumin [32408]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32408'); 
COMMIT;                                                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32310','SGOT (AST Aspartate aminotransferase) [32310]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32310'); 
COMMIT;                                                                                                        
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32311','SGPT (ALT Alanine aminotransferase) [32311]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32311'); 
COMMIT;                                                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37596','Thiopurine methyltransferase activity วิธี HPLC (TPMT gene targeted mutation analysis) [37596]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37596'); 
COMMIT;                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32630','Thyroglobulin [32630]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32630'); 
COMMIT;                                                                                                                                
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30305','TIBC (Iron binding capacity) [30305]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30305'); 
COMMIT;                                                                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '34301','Total protein (Quantitative) [34301]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '34301'); 
COMMIT;                                                                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '34104','Total protein, 24 hr Urine (Quantitative) [34104]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '34104'); 
COMMIT;                                                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '34103','Total protein, Urine random (Quantitative) [34103]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '34103'); 
COMMIT;                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30307','Transferrin [30307]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30307'); 
COMMIT;                                                                                                                                  
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32307','Troponin [32307]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32307'); 
COMMIT;                                                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '34112','17-KS, Urine [34112]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '34112'); 
COMMIT;                                                                                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32606','17-OH-progesterone (17-Hydroxyprogesterone Quantitative) [32606]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32606'); 
COMMIT;                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '34113','17-OHCS, Urine (17-Hydroxycorticosteroids) [34113]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '34113'); 
COMMIT;                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32601','ACTH (Corticotropin) [32601]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32601'); 
COMMIT;                                                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32604','Aldosterone [32604]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32604'); 
COMMIT;                                                                                                                                  
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32624','C-peptide [32624]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32624'); 
COMMIT;                                                                                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32615','Calcitonin [32615]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32615'); 
COMMIT;                                                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '34107','Catecholamine, Urine (Quantitative) [34107]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '34107'); 
COMMIT;                                                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32603','Cortisol [32603]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32603'); 
COMMIT;                                                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32607','DHEA-sulphate (Dehydroepiandrosterone sulfate) [32607]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32607'); 
COMMIT;                                                                                               
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30308','EPO (erythropoietin) [30308]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30308'); 
COMMIT;                                                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32618','Estradiol [32618]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32618'); 
COMMIT;                                                                                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '34111','Free cortisol, Urine [34111]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '34111'); 
COMMIT;                                                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32616','FSH (Follicle stimulating hormone) (Follitropin) [32616]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32616'); 
COMMIT;                                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32623','Growth hormone (Somatotropin) [32623]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32623'); 
COMMIT;                                                                                                                
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '34110','HIAA, Urine (5-Hydroxyindoleacetic acid) (Quantitative) [34110]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '34110'); 
COMMIT;                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '34109','HVA (Homovanillic acid), Urine (Quantitative) [34109]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '34109'); 
COMMIT;                                                                                                
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32625','Insulin (Quantitative) [32625]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32625'); 
COMMIT;                                                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32626','Insulin - IGF1 (Insulin-like growth factor-I) [32626]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32626'); 
COMMIT;                                                                                                
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32627','Insulin - IGF BP3 (Insulin-like growth factor binding protein 3) (Quantitative) [32627]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32627'); 
COMMIT;                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32617','LH (Lutropin) [32617]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32617'); 
COMMIT;                                                                                                                                
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33316','Metanephrine and normetanephrine, plasma [33316]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33316'); 
COMMIT;                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '34108','Metanephrine and normetanephrine, urine [34108]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '34108'); 
COMMIT;                                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37313','NSE (Neuron-specific enolase) [37313]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37313'); 
COMMIT;                                                                                                                
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32619','Progesterone [32619]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32619'); 
COMMIT;                                                                                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32622','Prolactin [32622]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32622'); 
COMMIT;                                                                                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32613','PTH (intact) (Parathyrin.intact) [32613]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32613'); 
COMMIT;                                                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32605','Renin [32605]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32605'); 
COMMIT;                                                                                                                                        
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32620','Testosterone (Quantitative) [32620]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32620'); 
COMMIT;                                                                                                                  
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32612','Thyroid hormone - Free T3 (Free Tri - iodothyroxine ) (Triiodothyronine.free) [32612]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32612'); 
COMMIT;                                                                
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32610','Thyroid hormone - Free T4 (Free Thyroxine) [32610]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32610'); 
COMMIT;                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32611','Thyroid hormone - T3 (Tri - iodothyroxine) (Triiodothyronine) [32611]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32611'); 
COMMIT;                                                                                
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32608','Thyroid hormone - TSH (Thyroid Stimulating Hormone) (Thyrotropin) (Quantitative) [32608]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32608'); 
COMMIT;                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32632','Thyroid hormone - TSH, Neonatal blood spot (IRMA) (Immunoradiometric assay) [32632]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32632'); 
COMMIT;                                                                  
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '34105','Vanillylmandelic acid (VMA), Urine [34105]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '34105'); 
COMMIT;                                                                                                           
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37302','Alpha Fetoprotein (AFP) (Alpha-1-Fetoprotein) [37302]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37302'); 
COMMIT;                                                                                                
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37303','Beta - HCG (Choriogonadotropin.beta subunit) (Quantitative) [37303]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37303'); 
COMMIT;                                                                                  
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37306','CA 125 (Cancer Ag 125) (Quantitative) [37306]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37306'); 
COMMIT;                                                                                                        
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37307','CA 19-9 (Cancer Ag 19-9) (Quantitative) [37307]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37307'); 
COMMIT;                                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37308','CEA (Carcinoembryonic antigen) [37308]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37308'); 
COMMIT;                                                                                                               
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37311','Free PSA (Prostate specific Ag.free) [37311]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37311'); 
COMMIT;                                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37305','HCG titer (Choriogonadotropin) [37305]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37305'); 
COMMIT;                                                                                                               
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37310','PSA (Prostate-specific antigen) [37310]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37310'); 
COMMIT;                                                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32002','Blood gas analysis [32002]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32002'); 
COMMIT;                                                                                                                           
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37505','Carnitine/acylcarnitine analysis [37505]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37505'); 
COMMIT;                                                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32001','Electrolyte (Na, K, Cl, CO2) [32001]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32001'); 
COMMIT;                                                                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32004','Lipid profile (Cholesterol, HDL-chol, LDL-chol, TG) [32004]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32004'); 
COMMIT;                                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32005','Lipoprotein electrophoresis [32005]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32005'); 
COMMIT;                                                                                                                  
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32003','Liver function test [32003]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32003'); 
COMMIT;                                                                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37501','Metabolic screen [37501]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37501'); 
COMMIT;                                                                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37502','Quantitative plasma amino acid analysis [37502]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37502'); 
COMMIT;                                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '31511','Stone composition analysis [31511]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '31511'); 
COMMIT;                                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37503','Urine organic acid analysis [37503]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37503'); 
COMMIT;                                                                                                                  
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37504','Urine Thin layer chromatography for MPS (Mucopolysaccharidosis) (ตรวจ Glycosaminoglycans pattern) [37504]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37504'); 
COMMIT;                                            
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32635','ACTH (1 mg) Stimulation (for cortisol) [32635]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32635'); 
COMMIT;                                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32636','ACTH (250 mcg) Stimulation (for cortisol) [32636]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32636'); 
COMMIT;                                                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37231','Bromocriptine test (for GH) [37231]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37231'); 
COMMIT;                                                                                                                  
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32637','Insulin Tolerance Test (for POCT, cortisol and GH) [32637]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32637'); 
COMMIT;                                                                                           
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32204','Oral Glucose Tolerance Test (OGTT) ระดับ plasma glucose 2 ครั้ง [32204]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32204'); 
COMMIT;                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32218','Oral Glucose Tolerance Test (OGTT) ระดับ plasma glucose ไม่ต่ำกว่า 4 ครั้ง [32218]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32218'); 
COMMIT;                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37248','OGTT for GH [37248]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37248'); 
COMMIT;                                                                                                                                  
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33107','Acetaminophen (quantitative) [33107]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33107'); 
COMMIT;                                                                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33550','Acetone (quantitative) [33550]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33550'); 
COMMIT;                                                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33901','Alcohols, blood (quantitative) [33901]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33901'); 
COMMIT;                                                                                                               
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33501','Aluminium (quantitative) [33501]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33501'); 
COMMIT;                                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33006','Amikacin (quantitative) [33006]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33006'); 
COMMIT;                                                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33112','Amitriptyline (quantitative) [33112]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33112'); 
COMMIT;                                                                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33701','Amphetamine [33701]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33701'); 
COMMIT;                                                                                                                                  
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33114','Antidepressants [33114]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33114'); 
COMMIT;                                                                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37239','Antihistamines (qualitative) [37239]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37239'); 
COMMIT;                                                                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33502','Arsenic (Gastric Lavage, Toxin sample) [33502]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33502'); 
COMMIT;                                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33503','Arsenic (quantitative จาก Urine, EDTA blood) [33503]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33503'); 
COMMIT;                                                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33118','Barbiturates (quantitative) [33118]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33118'); 
COMMIT;                                                                                                                  
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33552','Benzene (quantitative) [33552]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33552'); 
COMMIT;                                                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33123','Benzodiazepine (qualitative) [33123]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33123'); 
COMMIT;                                                                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33524','Cadmium (quantitative) [33524]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33524'); 
COMMIT;                                                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33702','Cannabinoid [33702]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33702'); 
COMMIT;                                                                                                                                  
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33602','Carbamate [33602]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33602'); 
COMMIT;                                                                                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33101','Carbamazepine (quantitative) [33101]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33101'); 
COMMIT;                                                                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33554','Chloroform [33554]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33554'); 
COMMIT;                                                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33505','Chromium (quantitative) [33505]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33505'); 
COMMIT;                                                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33506','Copper (quantitative) [33506]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33506'); 
COMMIT;                                                                                                                        
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33515','Cyanide (quantitative) [33515]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33515'); 
COMMIT;                                                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33201','Cyclosporin (quantitative) [33201]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33201'); 
COMMIT;                                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33302','Digoxin (quantitative) [33302]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33302'); 
COMMIT;                                                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33555','Ethanol (Ethyl alcohol) วิธี GC [33555]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33555'); 
COMMIT;                                                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33208','Everolimus (quantitative) [33208]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33208'); 
COMMIT;                                                                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '31104','Hippuric acid (qualitative) [31104]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '31104'); 
COMMIT;                                                                                                                  
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33151','Imipramine (qualitative) [33151]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33151'); 
COMMIT;                                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33556','Isopropanol (qualitative) [33556]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33556'); 
COMMIT;                                                                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33508','Lead (quantitative) [33508]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33508'); 
COMMIT;                                                                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33110','Lithium (quantitative) [33110]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33110'); 
COMMIT;                                                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33517','Manganese (quantitative) [33517]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33517'); 
COMMIT;                                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33705','Marijuana (Cannabinoid) วิธี immunoassay [33705]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33705'); 
COMMIT;                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33512','Mercury (quantitative) [33512]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33512'); 
COMMIT;                                                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33707','Methadone, Confirm test (quantitative) [33707]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33707'); 
COMMIT;                                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33708','Methamphetamine (qualitative) [33708]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33708'); 
COMMIT;                                                                                                                
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33709','Methamphetamine, Confirm test (quantitative) [33709]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33709'); 
COMMIT;                                                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33557','Methanol วิธี GC [33557]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33557'); 
COMMIT;                                                                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33710','Morphine (quantitative) [33710]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33710'); 
COMMIT;                                                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33727','Mycophenolate (quantitative) [33727]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33727'); 
COMMIT;                                                                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33513','Nickel (quantitative) [33513]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33513'); 
COMMIT;                                                                                                                        
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33725','Opiates (quantitative) [33725]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33725'); 
COMMIT;                                                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33610','Organochlorine (Type) [33610]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33610'); 
COMMIT;                                                                                                                        
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33611','Organonitrogen [33611]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33611'); 
COMMIT;                                                                                                                               
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33604','Organophosphate (qualitative) [33604]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33604'); 
COMMIT;                                                                                                                
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33605','Paraquat (qualitative) [33605]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33605'); 
COMMIT;                                                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33102','Phenobarbital (quantitative) [33102]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33102'); 
COMMIT;                                                                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33171','Phenothiazine (quantitative) [33171]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33171'); 
COMMIT;                                                                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33103','Phenytoin (quantitative) [33103]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33103'); 
COMMIT;                                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33319','Pyrethrins [33319]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33319'); 
COMMIT;                                                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33108','Salicylate (quantitative) [33108]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33108'); 
COMMIT;                                                                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33204','Sirolimus (quantitative) [33204]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33204'); 
COMMIT;                                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36387','Surfactant [36387]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36387'); 
COMMIT;                                                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33203','Tacrolimus (quantitative) [33203]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33203'); 
COMMIT;                                                                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33301','Theophyline (quantitative) [33301]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33301'); 
COMMIT;                                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33558','Thinner (Toluene) [33558]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33558'); 
COMMIT;                                                                                                                            
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33104','Valproic acid/Sodium valproate (quantitative) [33104]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33104'); 
COMMIT;                                                                                                
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33005','Vancomycin (quantitative) [33005]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33005'); 
COMMIT;                                                                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33560','Volatile Organic Compounds [33560]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33560'); 
COMMIT;                                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33607','Zinc Phosphide [33607]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33607'); 
COMMIT;                                                                                                                               
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '35101','Aerobic culture and sensitivity [35101]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '35101'); 
COMMIT;                                                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '35102','Anaerobic culture and sensitivity [35102]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '35102'); 
COMMIT;                                                                                                            
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36012','B. pseudomallei - Ab (Melioid titer) - Indirect hemagglutination assay (IHA) [36012]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36012'); 
COMMIT;                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36061','B. pseudomallei - Ab (Melioid titer) IgM/IgG quantification- Immunofluorescent assay (IFA) [36061]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36061'); 
COMMIT;                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36021','Bacterial Antigen (CSF), including H. influenzae, N. meningitidis, S. pneumoniae, S. agalactiae [36021]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36021'); 
COMMIT;                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36024','Brucella spp. Ab [36024]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36024'); 
COMMIT;                                                                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36017','Clostidium difficile toxin (Qualitative) [36017]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36017'); 
COMMIT;                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '35002','Gram stain [35002]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '35002'); 
COMMIT;                                                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36013','Haemophilus influenzae type b Ag [36013]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36013'); 
COMMIT;                                                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36015','Helicobacter pylori (CLO) [36015]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36015'); 
COMMIT;                                                                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36014','Helicobacter pylori/Ab (Quantitative) [36014]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36014'); 
COMMIT;                                                                                                        
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '35105','Hemoculture and sensitivity, Automate ต่อ 1 ขวด [35105]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '35105'); 
COMMIT;                                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36025','Legionella pneumophila DNA detection, NP wash [36025]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36025'); 
COMMIT;                                                                                                
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36026','Legionella pneumophila Ag, urine ด้วยวิธี IC assay [36026]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36026'); 
COMMIT;                                                                                           
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36007','Leptospira spp. Ab detection (Qualitative) [36007]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36007'); 
COMMIT;                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '35111','Minimum Bactericidal Concentration (MBC) [35111]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '35111'); 
COMMIT;                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '35109','Minimum Inhibitory Concentration (MIC) [35109]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '35109'); 
COMMIT;                                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36023','Mycoplasma pneumoniae Ab [36023]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36023'); 
COMMIT;                                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36046','Mycoplasma pneumoniae, quantitative DNA detection [36046]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36046'); 
COMMIT;                                                                                            
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36010','Neisseria meningitidis Ag detection (Qualitative) [36010]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36010'); 
COMMIT;                                                                                            
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36053','Orientia tsutsugamushi (Scub Typhus) Ab detection [36053]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36053'); 
COMMIT;                                                                                            
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36740','Rickettsia typhi Ab detection [36740]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36740'); 
COMMIT;                                                                                                                
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36001','Streptococcus gr A - Anti - Streptolysin O [36001]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36001'); 
COMMIT;                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36009','Streptococcus gr B Ag [36009]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36009'); 
COMMIT;                                                                                                                        
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36008','Streptococcus pneumoniae Ag [36008]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36008'); 
COMMIT;                                                                                                                  
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '31502','Treponema pallidum - Dark field examination [31502]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '31502'); 
COMMIT;                                                                                                  
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36004','Treponema pallidum - FTA - Abs [36004]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36004'); 
COMMIT;                                                                                                               
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36006','Treponema pallidum - TPHA [36006]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36006'); 
COMMIT;                                                                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36003','Treponema pallidum - VDRL (RPR) (Reagin Ab, D400) [36003]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36003'); 
COMMIT;                                                                                            
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '35001','AFB stain (Acid-Fast Bacilli stain) [35001]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '35001'); 
COMMIT;                                                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '35250','IFN- ? release assay for TB (Mycobacterium tuberculosis stimulated gamma interferon panel) [35250]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '35250'); 
COMMIT;                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '35005','Modified acid-fast stain [35005]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '35005'); 
COMMIT;                                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '35113','Mycobacteria: antimicrobial susceptibility test for NTM [35113]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '35113'); 
COMMIT;                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36036','Mycobacteria: antimicrobial susceptibility test for 1st line anti-TB ชื่อยา INH, RIF, EMB, PZN, SM [36036]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36036'); 
COMMIT;                                           
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36037','Mycobacteria: antimicrobial susceptibility test for 2nd line anti-TB ชื่อยา Kanamycin, Levofloxacin [36037]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36037'); 
COMMIT;                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '35103','Mycobacterium culture [35103]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '35103'); 
COMMIT;                                                                                                                        
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36018','Mycobacteria: direct PCR [36018]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36018'); 
COMMIT;                                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36552','Adenovirus Ag (Qualitative) [36552]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36552'); 
COMMIT;                                                                                                                  
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36553','Adenovirus DNA detection (Qualitative) [36553]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36553'); 
COMMIT;                                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36555','Adenovirus, viral load [36555]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36555'); 
COMMIT;                                                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36700','Avian influenza virus Ag (rapid) [36700]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36700'); 
COMMIT;                                                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36504','Avian influenza virus, qualitative RT-PCR [36504]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36504'); 
COMMIT;                                                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36603','Chikungunya virus RNA detection [36603]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36603'); 
COMMIT;                                                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36601','Chikungunya Ab (Qualitative) [36601]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36601'); 
COMMIT;                                                                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36710','Chlamydophila pneumoniae DNA detection [36710]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36710'); 
COMMIT;                                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36713','Chlamydia trachomatis Ag [36713]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36713'); 
COMMIT;                                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36580','Coxsackie B virus neutralizing Ab (NT) [36580]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36580'); 
COMMIT;                                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36440','Cytomegalovirus (CMV) Ab [36440]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36440'); 
COMMIT;                                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36443','Cytomegalovirus (CMV) viral load - Quantitative (Real time PCR) [36443]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36443'); 
COMMIT;                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36461','Cytomegalovirus (CMV) Ag [36461]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36461'); 
COMMIT;                                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36614','Dengue virus Ag NS1 [36614]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36614'); 
COMMIT;                                                                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36610','Dengue virus Ab (qualitative) [36610]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36610'); 
COMMIT;                                                                                                                
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36613','Dengue virus, qualitative RT - PCR [36613]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36613'); 
COMMIT;                                                                                                           
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36464','Echovirus Ag [36464]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36464'); 
COMMIT;                                                                                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36750','Enterovirus 71 Ag [36750]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36750'); 
COMMIT;                                                                                                                            
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36752','Enterovirus 71 RNA detection [36752]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36752'); 
COMMIT;                                                                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36755','Enterovirus Ag [36755]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36755'); 
COMMIT;                                                                                                                               
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36466','Enterovirus RNA detection [36466]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36466'); 
COMMIT;                                                                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36430','Epstein-Barr virus EBV Ab detection [36430]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36430'); 
COMMIT;                                                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36433','Epstein-Barr virus EBV, viral load RT - PCR [36433]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36433'); 
COMMIT;                                                                                                  
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '31302','Giemsa stain for Virus or Parasite [31302]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '31302'); 
COMMIT;                                                                                                           
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36302','Hepatitis A virus - Anti HAV IgM (ELISA) [36302]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36302'); 
COMMIT;                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36311','Hepatitis B virus HBc Ab (Hepatitis B virus core Ab) [36311]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36311'); 
COMMIT;                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36312','Hepatitis B virus HBc Ab (IgM) [36312]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36312'); 
COMMIT;                                                                                                               
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36315','Hepatitis B virus Hbe Ab [36315]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36315'); 
COMMIT;                                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36314','Hepatitis B virus Hbe Ag [36314]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36314'); 
COMMIT;                                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36317','Hepatitis B virus HBs Ab detection [36317]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36317'); 
COMMIT;                                                                                                           
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36319','Hepatitis B virus HBs Ag (Hepatitis B surface antigen) - ELISA, MEIA, ECLIA [36319]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36319'); 
COMMIT;                                                                  
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36318','Hepatitis B virus HBs Ag (Hepatitis B surface antigen) - PHA [36318]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36318'); 
COMMIT;                                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36321','Hepatitis B virus HBs - Quantitative Ag [36321]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36321'); 
COMMIT;                                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36323','Hepatitis B virus HBV Genotype, drug resistance [36323]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36323'); 
COMMIT;                                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36320','Hepatitis B virus HBV PCR - viral load [36320]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36320'); 
COMMIT;                                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36334','Hepatitis C virus HCV genotype (line-probe) [36334]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36334'); 
COMMIT;                                                                                                  
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36333','Hepatitis C virus HCV viral load - Quantitative (Real time PCR) [36333]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36333'); 
COMMIT;                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36331','Hepatitis C virus HCV Ab (Hepatitis C Antibody) [36331]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36331'); 
COMMIT;                                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36384','Hepatitis D virus HDV anti - HDV Ab [36384]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36384'); 
COMMIT;                                                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36385','Hepatitis E virus HEV Ab [36385]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36385'); 
COMMIT;                                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36450','HHV-6 (Human Herpesvirus type 6) PCR - Qualitative (Real time PCR) [36450]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36450'); 
COMMIT;                                                                           
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36451','HHV-7 PCR - Qualitative (Real time PCR) [36451]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36451'); 
COMMIT;                                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36452','HHV-8 PCR - Qualitative (Real time PCR) [36452]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36452'); 
COMMIT;                                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36362','HIV viral load - Quantitative (Real time PCR) [36362]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36362'); 
COMMIT;                                                                                                
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36372','HIV-1 Drug resistance genotype to protease inhibitors (PI) [36372]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36372'); 
COMMIT;                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36370','HIV-1 Drug resistance genotype (3-Drug Class Resistance, 3-DCR) [36370]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36370'); 
COMMIT;                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36371','HIV-1 Drug resistance genotype to reverse transcriptase inhibitors (NRTI, NNRTI) [36371]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36371'); 
COMMIT;                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36352','HIV Ab (confirm) - WESTERN BLOT [36352]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36352'); 
COMMIT;                                                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36351','HIV Ab (screening) [36351]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36351'); 
COMMIT;                                                                                                                           
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36350','HIV Ab (screening) - RAPID [36350]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36350'); 
COMMIT;                                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36360','HIV Ag (Qualitative) [36360]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36360'); 
COMMIT;                                                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '31503','HSV detection - Tzank''s smear (Wright''s stain) [31503]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '31503'); 
COMMIT;                                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '31501','HSV direct Ag detection with Immunofluoresence assay [31501]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '31501'); 
COMMIT;                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36405','HSV type 1&2 viral load - Quantitative (Real time PCR) [36405]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36405'); 
COMMIT;                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36402','HSV-1 and -2 Ab detection [36402]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36402'); 
COMMIT;                                                                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36406','HSV-1 and -2, isolation (culture) [36406]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36406'); 
COMMIT;                                                                                                            
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36353','HTLV-1 Ab [36353]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36353'); 
COMMIT;                                                                                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36760','Human Metapneumovirus Ag [36760]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36760'); 
COMMIT;                                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36762','Human metapneumovirus RNA detection [36762]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36762'); 
COMMIT;                                                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36506','Influenza A and B virus Ag (rapid test) [36506]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36506'); 
COMMIT;                                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36508','Influenza A virus RNA detection (Quantitative) [36508]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36508'); 
COMMIT;                                                                                               
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36514','Influenza A virus Ag [36514]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36514'); 
COMMIT;                                                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36516','Influenza B virus RNA detection (Quantitative) [36516]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36516'); 
COMMIT;                                                                                               
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36517','Influenza B virus Ag [36517]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36517'); 
COMMIT;                                                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37803','Japanese encephalitis virus (JEV) RNA detection [37803]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37803'); 
COMMIT;                                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36620','Japanese encephalitis virus Ab (Qualitative) [36620]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36620'); 
COMMIT;                                                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36671','JC Virus and BK Virus viral load - Quantitative (Real time PCR) [36671]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36671'); 
COMMIT;                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36655','Measles virus Ab IgG [36655]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36655'); 
COMMIT;                                                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36656','Measles virus Ab IgM [36656]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36656'); 
COMMIT;                                                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36650','Mumps Ab IgG (ELISA) [36650]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36650'); 
COMMIT;                                                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36684','Mumps Ab IgM (ELISA) [36684]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36684'); 
COMMIT;                                                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36521','Parainfluenza virus Ag (ชนิดละ) [36521]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36521'); 
COMMIT;                                                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36520','Parainfluenza virus 1, 2, 3 Ab IgG (ELISA) [36520]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36520'); 
COMMIT;                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36527','Parainfluenza virus 1, 2, 3 Ab IgM (ELISA) [36527]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36527'); 
COMMIT;                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36681','Parvo virus B19 PCR (Real time PCR) Quantitative [36681]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36681'); 
COMMIT;                                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36682','Parvo virus B19 Ab IgG (ELISA) [36682]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36682'); 
COMMIT;                                                                                                               
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36680','Parvo virus B19 Ab IgM (ELISA) [36680]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36680'); 
COMMIT;                                                                                                               
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36642','Rabies virus (NASBA) (Nucleic Acid Sequence Based Amplification) [36642]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36642'); 
COMMIT;                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36640','Rabies virus Ab [36640]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36640'); 
COMMIT;                                                                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36641','Rabies virus Ag (FTA) [36641]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36641'); 
COMMIT;                                                                                                                        
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36541','Respiratory syncytial virus (RSV) Ag [36541]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36541'); 
COMMIT;                                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36542','Respiratory syncytial virus Ab IgG (ELISA) [36542]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36542'); 
COMMIT;                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36540','Respiratory syncytial virus Ab IgM (ELISA) [36540]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36540'); 
COMMIT;                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36570','Rota virus Ag [36570]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36570'); 
COMMIT;                                                                                                                                
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36571','Rotavirus RNA detection [36571]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36571'); 
COMMIT;                                                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36660','Rubella Ab IgG [36660]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36660'); 
COMMIT;                                                                                                                               
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36661','Rubella Ab IgM [36661]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36661'); 
COMMIT;                                                                                                                               
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36420','Varicella zoster virus (VZV) Ab IgG (ELISA) [36420]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36420'); 
COMMIT;                                                                                                  
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36420','Varicella zoster virus (VZV) Ab IgM (ELISA) [36420]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36420'); 
COMMIT;                                                                                                  
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36421','Varicella zoster virus (VZV) Quantitative (Real time PCR) [36421]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36421'); 
COMMIT;                                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '35116','Aspergillus: galactomannan Ag detection (Quantitative) [35116]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '35116'); 
COMMIT;                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37227','Cryptococcal Ag, serum/CSF/ other body fluid (Qualitative) [37227]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37227'); 
COMMIT;                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '35107','Culture for fungus [35107]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '35107'); 
COMMIT;                                                                                                                           
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '35114','Fungus: antimicrobial susceptibility test (1 - 3 ชนิด) [35114]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '35114'); 
COMMIT;                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '35240','Fungus: Molecular identification [35240]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '35240'); 
COMMIT;                                                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '35003','Indian ink preparation [35003]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '35003'); 
COMMIT;                                                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '35004','KOH preparation [35004]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '35004'); 
COMMIT;                                                                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36106','Pythium Ab detection [36106]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36106'); 
COMMIT;                                                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '31223','Amoeba - Special stain for free-living amoeba (Giemsa''s stainning) [31223]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '31223'); 
COMMIT;                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '31224','Amoeba - Special stain for free-living amoeba (Trichrome stainning) [31224]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '31224'); 
COMMIT;                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '31225','Cryptosporidium (modified acid fast stain) [31225]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '31225'); 
COMMIT;                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36208','Entamoeba histolytica Ab [36208]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36208'); 
COMMIT;                                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36212','Leishmania NNE culture [36212]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36212'); 
COMMIT;                                                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '31214','Malaria, thick film [31214]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '31214'); 
COMMIT;                                                                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30126','Malaria, thin film [30126]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30126'); 
COMMIT;                                                                                                                           
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '35006','Microsporidia, special stain [35006]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '35006'); 
COMMIT;                                                                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '31217','Naegleria /Acanthamoeba NNE culture [31217]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '31217'); 
COMMIT;                                                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '31303','Pneumocystis carinii, special stain [31303]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '31303'); 
COMMIT;                                                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36202','Pneumocystis carinii - Fluorescent Assay [36202]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36202'); 
COMMIT;                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '31221','Protozoa detection, special stain [31221]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '31221'); 
COMMIT;                                                                                                            
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36103','Toxoplasma Ab IgG (Quantitative) [36103]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36103'); 
COMMIT;                                                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36103','Toxoplasma Ab IgM (Quantitative) [36103]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36103'); 
COMMIT;                                                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '31212','Angiostrongylus Ab [31212]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '31212'); 
COMMIT;                                                                                                                           
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37214','Arthropod identification [37214]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37214'); 
COMMIT;                                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36203','Cysticercosis Ab (Taenia solium larva Ab) [36203]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36203'); 
COMMIT;                                                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '31207','Enterobiasis - Scotch tape technique [31207]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '31207'); 
COMMIT;                                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '31307','Filariasis - Giemsa stain [31307]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '31307'); 
COMMIT;                                                                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37268','Filariasis - IgG4 Ab [37268]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37268'); 
COMMIT;                                                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37601','Filariasis - Real time PCR [37601]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37601'); 
COMMIT;                                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '31215','Gnathostomiasis Ab [31215]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '31215'); 
COMMIT;                                                                                                                           
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36217','Paragonimiasis Ab [36217]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36217'); 
COMMIT;                                                                                                                            
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '31218','Parasite identification (Ova & parasites identified) [31218]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '31218'); 
COMMIT;                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '35008','Scabiasis [35008]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '35008'); 
COMMIT;                                                                                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37906','Simple sedimentation (Stool) [37906]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37906'); 
COMMIT;                                                                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '31219','Stool concentration (Formalin-ethyl acetate technique/Kato''s Thick smear) [31219]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '31219'); 
COMMIT;                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36215','Trichinosis Ab [36215]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36215'); 
COMMIT;                                                                                                                               
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '35007','Wet smear for ova/parasite [35007]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '35007'); 
COMMIT;                                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37015','Antineutrophil Cytoplasmic Antibodies (ANCA) (Quantitative) [37015]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37015'); 
COMMIT;                                                                                  
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37004','Anti-dsDNA Ab (DNA double strand Ab, Quantitaive) [37004]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37004'); 
COMMIT;                                                                                            
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37010','Anti-La (SS-B) Ab (Sjogrens syndrome-B extractable nuclear Ab Quantitative) [37010]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37010'); 
COMMIT;                                                                  
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37018','Anti-LKM (Liver-Kidney-Microsome) Ab (Quantitative) [37018]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37018'); 
COMMIT;                                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37013','Anti-MPO (Myeloperoxidase) Ab (Quantitative) [37013]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37013'); 
COMMIT;                                                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37008','Anti-nRNP Ab (ELISA) (Ribonucleoprotein extractable nuclear Ab Quantitative) [37008]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37008'); 
COMMIT;                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37007','Anti-nRNP Ab (Immunodiffusion) (Ribonucleoprotein extractable nuclear Ab Qualitative) [37007]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37007'); 
COMMIT;                                                        
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37014','Anti-PR3 (Proteinase 3) Ab (Quantitative) [37014]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37014'); 
COMMIT;                                                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37009','Anti-Ro (SS-A) Ab (Sjogrens syndrome-A extractable nuclear Ab, Quantitative) [37009]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37009'); 
COMMIT;                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37011','Anti-Scl 70 Ab [37011]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37011'); 
COMMIT;                                                                                                                               
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37005','Anti-Sm Ab (Smith extractable nuclear Ab) [37005]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37005'); 
COMMIT;                                                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37006','Anti-Sm Ab (ELISA) [37006]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37006'); 
COMMIT;                                                                                                                           
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37017','Anti-Smooth muscle Ab [37017]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37017'); 
COMMIT;                                                                                                                        
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37211','Anti-Thyroglobulin Ab [37211]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37211'); 
COMMIT;                                                                                                                        
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37012','Anticentromere Ab [37012]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37012'); 
COMMIT;                                                                                                                            
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37016','Antimitochondrial Ab [37016]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37016'); 
COMMIT;                                                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37003','Antinuclear Ab (FANA, ANA) [37003]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37003'); 
COMMIT;                                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37001','Rheumatoid factor - Latex (Qualitative) [37001]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37001'); 
COMMIT;                                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37002','Rheumatoid factor - Nephelometry (Quantitative, Titer) [37002]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37002'); 
COMMIT;                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37107','Complement C4 level (Quantitative) [37107]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37107'); 
COMMIT;                                                                                                           
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37105','Complement CH50 (Complement total hemolytic CH50, Quantitative) [37105]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37105'); 
COMMIT;                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37101','Complement C3 level - Latex (Qualitative) [37101]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37101'); 
COMMIT;                                                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37102','Complement C3 level - Nephelometry (Quantitative) [37102]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37102'); 
COMMIT;                                                                                            
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37205','Immunoglobulin level IgA (Quantitative) [37205]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37205'); 
COMMIT;                                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37350','Immunoglobulin level IgE (total) [37350]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37350'); 
COMMIT;                                                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37201','Immunoglobulin level IgG (Quantitative) [37201]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37201'); 
COMMIT;                                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37206','Immunoglobulin level IgM (Quantitative) [37206]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37206'); 
COMMIT;                                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33154','Immunofixation electrophoresis [33154]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33154'); 
COMMIT;                                                                                                               
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32006','Protein electrophoresis, serum/urine [32006]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32006'); 
COMMIT;                                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37351','Specific IgE to mixture of food allergens (screening test) [37351]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37351'); 
COMMIT;                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37353','Specific IgE to mixture of respiratory (inhalant) allergens (screening test) [37353]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37353'); 
COMMIT;                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37355','Specific IgE, quantitative (1 allergen) [37355]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37355'); 
COMMIT;                                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37357','Specific IgE, quantitative (3 allergens) [37357]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37357'); 
COMMIT;                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37359','Specific IgE, quantitative (5 allergens) [37359]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37359'); 
COMMIT;                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37361','Specific IgE, quantitative (7 allergens) [37361]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37361'); 
COMMIT;                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37506','Chromosome breakage study [37506]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37506'); 
COMMIT;                                                                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30402','Chromosome analysis (Amniotic fluid/CVS/Tissue) [30402]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30402'); 
COMMIT;                                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30401','Chromosome analysis non-leukemia (Blood) [30401]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30401'); 
COMMIT;                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37576','Chromosome analysis in leukemia (Bone marrow/blood) [37576]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37576'); 
COMMIT;                                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37575','Chromosome analysis - FISH technique (per probe used) [37575]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37575'); 
COMMIT;                                                                                        
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37801','Achondroplasia/FGFR3 Mutation Analysis [37801]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37801'); 
COMMIT;                                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37552','Autosomal Dominant Polycystic Kidney Disease (ADPKD) - DNA analysis [37552]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37552'); 
COMMIT;                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37521','ApoE polymorphism PCR RFLP [37521]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37521'); 
COMMIT;                                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37540','ARX - DNA analysis [37540]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37540'); 
COMMIT;                                                                                                                           
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30405','BCR/ABL gene for CML - RT-PCR [30405]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30405'); 
COMMIT;                                                                                                                
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37573','BRAF Mutation - Realtime PCR [37573]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37573'); 
COMMIT;                                                                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37545','Bruton hypogammaglobulinemia - DNA analysis [37545]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37545'); 
COMMIT;                                                                                                  
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37558','CADASIL DNA analysis [37558]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37558'); 
COMMIT;                                                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37522','Charcot - Marie-Tooth type IA DNA analysis [37522]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37522'); 
COMMIT;                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37546','Chronic granulomatous disease DNA analysis [37546]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37546'); 
COMMIT;                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37580','CYP2C19 Genotyping [37580]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37580'); 
COMMIT;                                                                                                                           
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37581','CYP2C9 Genotyping [37581]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37581'); 
COMMIT;                                                                                                                            
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37544','Cystinosis DNA analysis [37544]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37544'); 
COMMIT;                                                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37556','Distal Myopathy with Rimmed Vacuole (DMRV) DNA analysis [37556]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37556'); 
COMMIT;                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37535','Dopa-responsive dystonia DYT1 - PCR-seq [37535]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37535'); 
COMMIT;                                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37510','Duchenne/Becker muscular dystrophy - Multiplex PCR [37510]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37510'); 
COMMIT;                                                                                           
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37511','Duchenne/Becker muscular dystrophy - MLPA (Multiplex Ligation-dependent Probe Amplification) [37511]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37511'); 
COMMIT;                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37555','Dysferlinopathy DNA analysis [37555]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37555'); 
COMMIT;                                                                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32217','EGFR mutation analysis in cancer - PCR [32217]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32217'); 
COMMIT;                                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37516','Factor V Leiden - DNA analysis [37516]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37516'); 
COMMIT;                                                                                                               
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37557','Familial adenomatous polyposis (FAP) DNA analysis (Adenomatous Polyposis Coli APC gene targeted mutation analysis) [37557]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37557'); 
COMMIT;                           
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37559','FGFR related disorders- Limited gene sequencing (Fibroblast growth factor receptor) [37559]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37559'); 
COMMIT;                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37509','Fragile X syndrome - Methylation PCR [37509]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37509'); 
COMMIT;                                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37533','GJB2 mutation DNA analysis [37533]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37533'); 
COMMIT;                                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37526','Glucocorticoid remediable aldosteronism - LongPCR (CYP11B1 gene) [37526]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37526'); 
COMMIT;                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37536','Hemochromatosis HFE DNA analysis [37536]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37536'); 
COMMIT;                                                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37554','Hemophilia A DNA analysis (F8 Gene) [37554]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37554'); 
COMMIT;                                                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37525','Hemophilia A intron 22 inversion - LongPCR (F8 Gene intron) [37525]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37525'); 
COMMIT;                                                                                  
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37550','Hereditary breast-ovarian cancer (BRCA1, 2) DNA analysis [37550]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37550'); 
COMMIT;                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37562','Hereditary pancreatitis SPINK1- Limited gene sequencing [37562]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37562'); 
COMMIT;                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37566','HNPCC - Microsatellite instability (MSI) from tissue [37566]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37566'); 
COMMIT;                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37551','HNPCC MSH2, MLH1, MSH6, PMS2 DNA analysis [37551]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37551'); 
COMMIT;                                                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37527','Huntington disease DNA analysis (HTT Gene) [37527]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37527'); 
COMMIT;                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37537','Idiopathic pancreatitis PRSS1 DNA analysis [37537]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37537'); 
COMMIT;                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37260','IgH (Immunoglobulin heavy chain gene) Rearrangement - Multiplex PCR [37260]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37260'); 
COMMIT;                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37261','IgK (Immunoglobulin kappa light chain gene) Rearrangement - Multiplex PCR [37261]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37261'); 
COMMIT;                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37564','Kearns Sayre/CPEO - Mitochondrial DNA analysis [37564]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37564'); 
COMMIT;                                                                                               
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37530','Kennedy disease - PCR-Seq (AR gene) [37530]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37530'); 
COMMIT;                                                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37805','KRAS Mutation - Limited gene sequencing [37805]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37805'); 
COMMIT;                                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37563','Leber optic atrophy - LHON (Leber hereditary optic neuropathy syndrome gene) Mitochondrial DNA analysis [37563]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37563'); 
COMMIT;                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37561','Liddle''s syndrome DNA analysis (Sodium Channel SCN1A gene) [37561]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37561'); 
COMMIT;                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37549','Marfan syndrome DNA analysis (FBN1 gene) [37549]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37549'); 
COMMIT;                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37565','MERFF Encephalomyopathy - Mitochondrial DNA analysis (MELAS gene Mitochondrial Encephalomyopathy, Lactic Acidosis, and Stroke-like episodes) [37565]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37565'); 
COMMIT; 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37538','Metachromatic leukodystrophy DNA analysis (Arylsulfatase A ARSA gene) [37538]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37538'); 
COMMIT;                                                                        
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37547','Methemoglobinemia (cyt b5R) DNA analysis (CYBA gene) [37547]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37547'); 
COMMIT;                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37512','Methylation analysis - Methylation PCR [37512]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37512'); 
COMMIT;                                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37517','MTHFR C677T - PCR-ASA [37517]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37517'); 
COMMIT;                                                                                                                        
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37528','OPMD (Oculopharyngeal muscular dystrophy, PABPN1 gene) DNA analysis [37528]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37528'); 
COMMIT;                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37543','Peutz-Jeghers syndrome DNA analysis (STK11 gene) [37543]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37543'); 
COMMIT;                                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30237','Prothrombin mutation - PCR (F2 gene) [30237]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30237'); 
COMMIT;                                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37524','Real time PCR - Others [37524]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37524'); 
COMMIT;                                                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37560','RET related disorders- Limited gene sequencing [37560]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37560'); 
COMMIT;                                                                                               
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37539','Rett syndrome (MECP2) DNA analysis [37539]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37539'); 
COMMIT;                                                                                                           
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37529','SCA type 1, 2, 3 DNA analysis (Spinocerebellar ataxia) [37529]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37529'); 
COMMIT;                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37513','Spinal muscular atrophy DNA analysis (SMN1 gene) [37513]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37513'); 
COMMIT;                                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37508','SRY gene - PCR [37508]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37508'); 
COMMIT;                                                                                                                               
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33805','TCRB Rearrangement-PCR [33805]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33805'); 
COMMIT;                                                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33806','TCRD Rearrangement-PCR [33806]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33806'); 
COMMIT;                                                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33807','TCRG Rearrangement-PCR [33807]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33807'); 
COMMIT;                                                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37519','Thalassemia, deletion analysis (alpha SEA and THAI) - PCR [37519]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37519'); 
COMMIT;                                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30404','Thalassemia, deletion (อย่างน้อยตรวจ alpha SEA, THAI, -3.7, -4.2) - Multiplex gap PCR (PANEL.MOLPATH) [30404]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30404'); 
COMMIT;                                        
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37520','Thalassemia, alpha non-deletion mutations - Multiplex PCR - ASA [37520]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37520'); 
COMMIT;                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37532','Thalassemia, beta mutations [37532]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37532'); 
COMMIT;                                                                                                                  
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37534','Von Hippel Lindau disease (VHL) DNA analysis [37534]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37534'); 
COMMIT;                                                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37548','Whole gene sequencing – Others [37548]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37548'); 
COMMIT;                                                                                                               
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37553','Wilson disease DNA analysis (ATP7B gene) [37553]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37553'); 
COMMIT;                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37542','Wiskott Aldrich syndrome DNA analysis (WAS gene) [37542]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37542'); 
COMMIT;                                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37541','X-linked adrenoleukodystrophy (ALD) DNA analysis ABCD1 (ATP-binding cassette, sub-family D) gene [37541]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37541'); 
COMMIT;                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37610','PCR 1 fragment [37610]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37610'); 
COMMIT;                                                                                                                               
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37611','PCR 3 fragments [37611]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37611'); 
COMMIT;                                                                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37612','PCR 5 fragments [37612]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37612'); 
COMMIT;                                                                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37613','PCR 10 fragments [37613]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37613'); 
COMMIT;                                                                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37614','PCR 15 fragments [37614]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37614'); 
COMMIT;                                                                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37615','PCR 30 fragments [37615]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37615'); 
COMMIT;                                                                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37616','Sequencing with dye 1 reaction [37616]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37616'); 
COMMIT;                                                                                                               
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37617','Sequencing with dye 5 reactions [37617]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37617'); 
COMMIT;                                                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37618','Sequencing with dye 10 reactions [37618]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37618'); 
COMMIT;                                                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37619','Sequencing with dye 30 reactions [37619]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37619'); 
COMMIT;                                                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30115','HIT antibody screening [30115]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30115'); 
COMMIT;                                                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30116','HIT antibody confirm [30116]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30116'); 
COMMIT;                                                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30235','ADAMTS13 activity [30235]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30235'); 
COMMIT;                                                                                                                            
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30236','ADAMTS13 antibody [30236]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30236'); 
COMMIT;                                                                                                                            
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30238','Factor assay - Factor IX Inhibitor (Qualititative) [30238]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30238'); 
COMMIT;                                                                                           
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30239','Factor assay - Factor VIII Inhibitor (Qualititative) [30239]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30239'); 
COMMIT;                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30240','Factor XIII (Urea solubility) [30240]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30240'); 
COMMIT;                                                                                                                
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30241','Von Willebrand factor (Ag, ELISA) [30241]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30241'); 
COMMIT;                                                                                                            
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30511','Flow cytometry for Hairy cell leukaemia (HCL) [30511]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30511'); 
COMMIT;                                                                                                
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30512','Flow cytometry for Lymphoma/Chronic lymphocytic leukaemia (CLL) [30512]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30512'); 
COMMIT;                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30513','Flow cytometry for Multiple myeloma (MM) [30513]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30513'); 
COMMIT;                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30514','Immunophenotyping NKT Cell [30514]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30514'); 
COMMIT;                                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30631','HLA-DRB1 DNA typing (low resolution) [30631]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30631'); 
COMMIT;                                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30632','HLA-DQB1 DNA typing (low resolution) [30632]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30632'); 
COMMIT;                                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30633','HLA-B*5701 allele -Realtime PCR (HLA-B*57:01) [30633]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30633'); 
COMMIT;                                                                                                
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '31013','Urine Eosinophils [31013]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '31013'); 
COMMIT;                                                                                                                            
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '31014','Urea nitrogen (Urine/dialysate) [31014]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '31014'); 
COMMIT;                                                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32404','Cryofibrinogen [32404]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32404'); 
COMMIT;                                                                                                                               
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32405','Procalcitonin (PCT) [32405]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32405'); 
COMMIT;                                                                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37309','Chromogranin A (CgA) [37309]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37309'); 
COMMIT;                                                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33703','Amphetamines Panel (GC-MS confirmation) [33703]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33703'); 
COMMIT;                                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '33606','Paraquat (quantitative) [33606]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '33606'); 
COMMIT;                                                                                                                      
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36022','PCR for Bacterial molecular identification [36022]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36022'); 
COMMIT;                                                                                                   
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36019','Mycobacteria: direct PCR for IR (INH, RIF) resistant [36019]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36019'); 
COMMIT;                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '36301','Hepatitis A virus - Anti HAV IgG [36301]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '36301'); 
COMMIT;                                                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37228','Cryptococcal Ag, serum/CSF/ other body fluid (Quantitative, Titer) [37228]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37228'); 
COMMIT;                                                                           
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '35115','Fungus: antimicrobial susceptibility test (4 - 9 ชนิด) [35115]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '35115'); 
COMMIT;                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '35117','Gomori-silver stain for fungus [35117]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '35117'); 
COMMIT;                                                                                                               
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37021','Antibody against glutamic Acid Decarboxylase (GAD)/Tyrosine Phosphatase(IA2)[GAD/IA2] [37021]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37021'); 
COMMIT;                                                        
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37022','Anti-CCP, Cyclic Citrullinated Peptide IgG Antibody [37022]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37022'); 
COMMIT;                                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37023','Anti Interferon Gamma Antibody [37023]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37023'); 
COMMIT;                                                                                                               
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37024','Anti thyroid peroxidase (TPO antibody) [37024]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37024'); 
COMMIT;                                                                                                       
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37025','Aquaporin 4 (NMOIgG) [37025]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37025'); 
COMMIT;                                                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37026','Autoimmune Encephalitis Assay (IFA) [37026]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37026'); 
COMMIT;                                                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37027','GBM Antibody (Glomerular Basement Membrane Antibody) [37027]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37027'); 
COMMIT;                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37028','Microsomal/Ab (Anti-TPO) [37028]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37028'); 
COMMIT;                                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37029','Paraneoplastic Syndromes Assay [37029]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37029'); 
COMMIT;                                                                                                               
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37030','TSH receptor Antibody (Thyroid Stimulating Hormone receptor Antibody) [37030]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37030'); 
COMMIT;                                                                        
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37108','Clostridium tetani IgG/ELISA (Antitetanus, IgG) [37108]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37108'); 
COMMIT;                                                                                              
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37109','DHR (Dihydrorhodamine flow cytometric test) [37109]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37109'); 
COMMIT;                                                                                                  
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37110','IgG4 [37110]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37110'); 
COMMIT;                                                                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37111','IgG subclass (1 - 4) [37111]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37111'); 
COMMIT;                                                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37112','Lymphocyte proliferation assay [37112]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37112'); 
COMMIT;                                                                                                               
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37113','Tryptase ELISA [37113]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37113'); 
COMMIT;                                                                                                                               
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '32007','Free light chain (FLC) Kappa/Lambda ใน serum ด้วยวิธี Nephelometry [32007]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '32007'); 
COMMIT;                                                                           
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '37354','Specific IgE, component resolved diagnosis (CRD) [37354]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '37354'); 
COMMIT;                                                                                             
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30406','BCR-ABL p210 by RQ-PCR (real-time quantitative polymerase chain reaction) [30406]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30406'); 
COMMIT;                                                                    
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30407','CALR mutation [30407]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30407'); 
COMMIT;                                                                                                                                
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30408','Direct sequencing of BCR-ABL mutation [30408]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30408'); 
COMMIT;                                                                                                        
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30409','Direct Sequencing ของ JAK2 exon 12 mutation [30409]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30409'); 
COMMIT;                                                                                                  
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30410','FLT3-ITD gene for AML (peripheral blood) [30410]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30410'); 
COMMIT;                                                                                                     
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30411','JAK2 V617F mutation [30411]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30411'); 
COMMIT;                                                                                                                          
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30412','MPL mutation [30412]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30412'); 
COMMIT;                                                                                                                                 
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30413','NPM1 gene for AML (peripheral blood) [30413]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30413'); 
COMMIT;                                                                                                         
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30414','PML-RARA fusion gene (Bone Marrow) [30414]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30414'); 
COMMIT;                                                                                                           
BEGIN;
INSERT INTO r_rp1253_adpcode SELECT '30415','T315I mutation of BCR-ABL gene [30415]T15','15','OFC/LGO'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1253_adpcode WHERE id = '30415'); 
COMMIT;

-- update db version
INSERT INTO s_version VALUES ('9701000000086', '86', 'Hospital OS, Community Edition', '3.9.53', '3.33.1', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_53.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.53');
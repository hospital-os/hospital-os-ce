-- item price for ipd
ALTER TABLE b_item_price ADD COLUMN item_price_ipd double precision NOT NULL default 0;
ALTER TABLE b_item_price DROP COLUMN user_modify; -- unused, insert new record only.
ALTER TABLE b_item_price DROP COLUMN modify_datetime; -- unused, insert new record only.

update b_item_price set item_price_cost = 0 where item_price_cost = '' or item_price_cost is null;
update b_item_price set item_price = 0 where item_price is null;
update b_item_price set item_price_ipd = item_price;

ALTER TABLE b_item_price ALTER COLUMN item_price_cost TYPE double precision USING (item_price_cost::numeric);

ALTER TABLE t_order ADD COLUMN order_price_type character varying(1) not null default '0'; -- 0 = opd, 1 = ipd

-- icd10 accident
ALTER TABLE t_diag_icd10 ADD COLUMN diag_icd10_accident character varying(1) not null default '0'; -- 0 = not accident, 1 = accident

update t_diag_icd10 set diag_icd10_accident = b_icd10.icd10_accident 
from b_icd10 
where b_icd10.icd10_number = t_diag_icd10.diag_icd10_number;

DELETE FROM b_map_rp1853_instype
WHERE
        r_rp1853_instype_id not in (select id from r_rp1853_instype)
        and b_contract_plans_id not in (select b_contract_plans_id from b_contract_plans);

		
		
INSERT INTO b_map_rp1853_education
select f_patient_education_type_id as id, r_rp1853_education_id, f_patient_education_type_id 
from f_patient_education_type 
where 
        r_rp1853_education_id is not null 
        and r_rp1853_education_id <> ''
        and f_patient_education_type_id not in (select f_patient_education_id from b_map_rp1853_education) ;

		
DELETE FROM b_map_rp1253_adpcode
WHERE r_rp1253_adpcode_id = '';

-- update db version
INSERT INTO s_version VALUES ('9701000000074', '74', 'Hospital OS, Community Edition', '3.9.41', '3.25.060515', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_41.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.41');
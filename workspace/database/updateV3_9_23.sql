ALTER TABLE t_notify_note ADD COLUMN show_on_patient character varying(1) default '0';
ALTER TABLE t_notify_note ADD COLUMN show_on_visit character varying(1) default '1';
ALTER TABLE t_notify_note ADD COLUMN other_remover text default '';

update f_notify_type set description = 'ครั้งถัดไปที่รับบริการเท่านั้น' where f_notify_type_id = '1';

ALTER TABLE t_patient_appointment ADD COLUMN patient_appointment_end_time character varying(5) default '';

ALTER TABLE t_visit_queue_despense ADD COLUMN visit_queue_name character varying(255) default '';
ALTER TABLE t_visit_queue_despense ADD COLUMN visit_queue_color character varying(255) default '';
ALTER TABLE t_visit_queue_despense ADD COLUMN visit_queue_number character varying(255) default '';

INSERT INTO f_gui_action (f_gui_action_id, gui_action_name, gui_action_note) 
	VALUES ('0408', 'ประวัติตามช่วงเวลา', NULL);

insert into f_employee_authentication values ('98','นักกายภาพบำบัด');

INSERT INTO s_version VALUES ('9701000000056', '56', 'Hospital OS, Community Edition', '3.9.23', '3.18.050712', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_23.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.23');
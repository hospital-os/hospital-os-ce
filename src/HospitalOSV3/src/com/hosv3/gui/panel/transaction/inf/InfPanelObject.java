/*
 * InfPanelObject.java
 *
 * Created on 8 ���Ҥ� 2549, 10:26 �.
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
package com.hosv3.gui.panel.transaction.inf;

/**
 *
 * @author Administrator
 */
public interface InfPanelObject {

    public int search(String str);

    public boolean setObjectV(@SuppressWarnings("UseOfObsoleteCollectionType") java.util.Vector v);

    public boolean add();

    public boolean select();

    public boolean save();

    public boolean delete();

    public void setEnabled(boolean b);
}

package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

@SuppressWarnings("ClassWithoutLogger")
public class LabResultItem extends Persistent {

    private static final long serialVersionUID = 1L;
    public String lab_result_item_id;
    public String name;
    public String unit;
    public String item_id;
    public String default_value;
    public String resultType;
    public String number;
    public String labresult_id;
    public String ncd_fbs = "0";
    public String ncd_hct = "0";
    // 1 = by gender, 2 = by interval age, 3 = text, 4 = sql
    public String normal_range_type = "1";
    // in object only not map to db
    public Object[] normalRanges = null;

    /**
     * @roseuid 3F658BBB036E
     */
    public LabResultItem() {
        lab_result_item_id = "";
        name = "";
        unit = "";
        item_id = "";
        default_value = "";
        resultType = "";
        number = "";
        labresult_id = "";
        normal_range_type = "1";
    }

    public void setNcdFbs(boolean b) {
        ncd_fbs = b ? "1" : "0";
    }

    public void setNcdHct(boolean b) {
        ncd_hct = b ? "1" : "0";
    }
}

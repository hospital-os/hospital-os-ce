/*
 * SpecialQueryAppointmentDB.java
 *
 * Created on 29 ���Ҥ� 2547, 11:11 �.
 */
package com.hosv3.objdb.squery;

import com.hospital_os.object.specialQuery.SpecialQueryAppointment;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

/**
 *
 * @author tong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class SpecialQueryAppointment2DB {

    /**
     * Creates a new instance of SpecialQueryAppointmentDB
     */
    public ConnectionInf theConnectionInf;
    private SpecialQueryAppointment dbObj;

    public SpecialQueryAppointment2DB(ConnectionInf db) {
        theConnectionInf = db;
        dbObj = new SpecialQueryAppointment();
        initConfig();
    }

    public boolean initConfig() {
        dbObj.patient_appointment_date = "patient_appointment_date";
        dbObj.patient_prefix = "f_patient_prefix_id";
        dbObj.patient_firstname = "patient_firstname";
        dbObj.patient_hn = "patient_hn";
        dbObj.patient_lastname = "patient_lastname";
        dbObj.service_point_description = "service_point_description";
        dbObj.t_patient_appointment_id = "t_patient_appointment_id";
        dbObj.t_patient_id = "t_patient_id";
        dbObj.t_visit_id = "t_visit_id";
        dbObj.patient_appointment_auto_visit = "patient_appointment_auto_visit";
        dbObj.b_visit_queue_setup_id = "b_visit_queue_setup_id";
        dbObj.patient_appointment_time = "patient_appointment_time";
        dbObj.patient_appointment = "patient_appointment";
        dbObj.patient_appointment_status = "patient_appointment_status";
        dbObj.patient_appointment_doctor = "patient_appointment_doctor";
        dbObj.patient_appointment_clinic = "patient_appointment_clinic";
        return true;
    }

    public Vector queryDataOrderbyDateHN(boolean all_period, String datefrom,
            String dateto, String servicepointid, String patientid,
            String appointstatus, String active, String doctor, String clinic, int limit) throws Exception {
        String sql = "SELECT \n"
                + "t_patient_appointment.t_patient_appointment_id,\n"
                + "t_patient.patient_hn,"
                + "t_patient.f_patient_prefix_id,\n"
                + "t_health_family.patient_name as patient_firstname,\n"
                + "t_health_family.patient_last_name as patient_lastname,\n"
                + "t_patient_appointment.patient_appointment_date,\n"
                + "prefix2.patient_prefix_description ||person2.person_firstname || ' ' || person2.person_lastname as patient_appointment_doctor,\n"
                + "b_service_point.service_point_description,\n"
                + "b_visit_clinic.visit_clinic_description as patient_appointment_clinic,\n"
                + "t_patient_appointment.t_patient_id,\n"
                + "t_patient_appointment.t_visit_id,\n"
                + "t_patient_appointment.patient_appointment_auto_visit,\n"
                + "t_patient_appointment.b_visit_queue_setup_id,\n"
                + "t_patient_appointment.patient_appointment_time,\n"
                + "t_patient_appointment.patient_appointment,\n"
                + "t_patient_appointment.patient_appointment_status \n"
                + " FROM t_patient_appointment \n"
                + "inner join t_patient on t_patient.t_patient_id = t_patient_appointment.t_patient_id\n"
                + "inner join t_health_family on t_health_family.t_health_family_id = t_patient.t_health_family_id\n"
                + "left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_health_family.f_prefix_id\n"
                + "left join b_employee on b_employee.b_employee_id = t_patient_appointment.patient_appointment_doctor\n"
                + "left join t_person as person2 on person2.t_person_id = b_employee.t_person_id\n"
                + "left join f_patient_prefix as prefix2 on prefix2.f_patient_prefix_id = person2.f_prefix_id\n"
                + "inner JOIN b_service_point ON t_patient_appointment.patient_appointment_servicepoint = b_service_point.b_service_point_id\n"
                + "inner JOIN b_visit_clinic ON t_patient_appointment.patient_appointment_clinic = b_visit_clinic.b_visit_clinic_id\n"
                + " WHERE true \n";
        if (patientid != null && !patientid.equals("")) {
            sql = sql + " AND t_patient_appointment.t_patient_id = '" + patientid + "' \n";
        }
        if (!all_period) {
            sql = sql
                    + " AND t_patient_appointment.patient_appointment_date >= '" + datefrom + "'"
                    + " AND t_patient_appointment.patient_appointment_date <= '" + dateto + "' \n";
        }
        if (active.equals("1")) {
            sql = sql
                    + " AND t_patient_appointment.patient_appointment_active = '" + active + "' \n";
        }
        if (doctor != null && !doctor.equals("")) {
            sql = sql + " AND t_patient_appointment.patient_appointment_doctor = '" + doctor + "' \n";
        }

        if (clinic != null && !clinic.equals("")) {
            sql = sql + " AND t_patient_appointment.patient_appointment_clinic = '" + clinic + "' \n";
        }

        if (servicepointid != null && !servicepointid.equals("")) {
            sql = sql + " AND t_patient_appointment.patient_appointment_servicepoint = '" + servicepointid + "' \n";
        }
        if (appointstatus != null && !appointstatus.equals("")) {
            sql = sql + " AND t_patient_appointment.patient_appointment_status = '" + appointstatus + "' \n";
        }

        sql = sql + " order by t_patient_appointment.patient_appointment_date desc, t_patient.patient_hn limit " + limit;
        return eQuery(sql);
    }

    public Vector queryDataAppointmentById(String... ids) throws Exception {
        String sql = "SELECT \n"
                + "t_patient_appointment.t_patient_appointment_id,\n"
                + "t_patient.patient_hn,"
                + "t_patient.f_patient_prefix_id,\n"
                + "t_health_family.patient_name as patient_firstname,\n"
                + "t_health_family.patient_last_name as patient_lastname,\n"
                + "t_patient_appointment.patient_appointment_date,\n"
                + "prefix2.patient_prefix_description ||person2.person_firstname || ' ' || person2.person_lastname as patient_appointment_doctor,\n"
                + "b_service_point.service_point_description,\n"
                + "b_visit_clinic.visit_clinic_description as patient_appointment_clinic,\n"
                + "t_patient_appointment.t_patient_id,\n"
                + "t_patient_appointment.t_visit_id,\n"
                + "t_patient_appointment.patient_appointment_auto_visit,\n"
                + "t_patient_appointment.b_visit_queue_setup_id,\n"
                + "t_patient_appointment.patient_appointment_time,\n"
                + "t_patient_appointment.patient_appointment,\n"
                + "t_patient_appointment.patient_appointment_status \n"
                + " FROM t_patient_appointment \n"
                + "inner join t_patient on t_patient.t_patient_id = t_patient_appointment.t_patient_id\n"
                + "inner join t_health_family on t_health_family.t_health_family_id = t_patient.t_health_family_id\n"
                + "left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_health_family.f_prefix_id\n"
                + "left join b_employee on b_employee.b_employee_id = t_patient_appointment.patient_appointment_doctor\n"
                + "left join t_person as person2 on person2.t_person_id = b_employee.t_person_id\n"
                + "left join f_patient_prefix as prefix2 on prefix2.f_patient_prefix_id = person2.f_prefix_id\n"
                + "inner JOIN b_service_point ON t_patient_appointment.patient_appointment_servicepoint = b_service_point.b_service_point_id\n"
                + "inner JOIN b_visit_clinic ON t_patient_appointment.patient_appointment_clinic = b_visit_clinic.b_visit_clinic_id\n"
                + " WHERE t_patient_appointment.t_patient_appointment_id = ANY(?) \n"
                + " order by t_patient_appointment.patient_appointment_date desc, t_patient.patient_hn ";
        PreparedStatement ePQuery = theConnectionInf.ePQuery(sql);
        ePQuery.setArray(1, theConnectionInf.getConnection().createArrayOf("text", ids));
        return eQuery(ePQuery.toString());
    }

    private Vector eQuery(String sql) throws Exception {
        SpecialQueryAppointment p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            p = new SpecialQueryAppointment();
            p.patient_appointment_date = rs.getString(dbObj.patient_appointment_date);
            p.patient_firstname = rs.getString(dbObj.patient_firstname);
            p.patient_prefix = rs.getString(dbObj.patient_prefix);
            p.patient_hn = rs.getString(dbObj.patient_hn);
            p.patient_lastname = rs.getString(dbObj.patient_lastname);
            p.service_point_description = rs.getString(dbObj.service_point_description);
            p.t_patient_appointment_id = rs.getString(dbObj.t_patient_appointment_id);
            p.t_patient_id = rs.getString(dbObj.t_patient_id);
            p.t_visit_id = rs.getString(dbObj.t_visit_id);
            p.patient_appointment_auto_visit = rs.getString(dbObj.patient_appointment_auto_visit);
            p.b_visit_queue_setup_id = rs.getString(dbObj.b_visit_queue_setup_id);
            p.patient_appointment_time = rs.getString(dbObj.patient_appointment_time);
            p.patient_appointment = rs.getString(dbObj.patient_appointment);
            p.patient_appointment_status = rs.getString(dbObj.patient_appointment_status); //amp:9/8/2549
            p.patient_appointment_doctor = rs.getString(dbObj.patient_appointment_doctor);
            p.patient_appointment_clinic = rs.getString(dbObj.patient_appointment_clinic);
            list.add(p);
        }
        rs.close();
        return list;
    }
}

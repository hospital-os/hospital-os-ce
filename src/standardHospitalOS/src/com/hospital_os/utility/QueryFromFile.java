/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sompr
 */
public class QueryFromFile {

    private static final Logger LOG = Logger.getLogger(QueryFromFile.class.getName());
    private final static String SQL_ENCODE = "UTF-8";

    public static QueryResult getQueryResultFromSqlFile(Connection connection, File file, Map<String, String> params) {
        return getQueryResultFromSqlFile(connection, file, params, true);
    }

    public static QueryResult getQueryResultFromSqlFile(Connection connection, File file, Map<String, String> params, boolean isCloseConnection) {
        // �纪��� column ������ select
        String[] columnNames = null;
        // �红�����������
        List<Object[]> resultList = new ArrayList<Object[]>();
        Statement stmt = null;
        ResultSet rs = null;
        try {
            // ��ҹ sql �ҡ File
            String sql = readFileToString(file.getAbsolutePath());
            stmt = connection.createStatement();
            sql = setParameters(sql, params);
//                    LOG.info("sql @ " + file.getName() + " \n "+ sql);
            rs = stmt.executeQuery(sql);
            // Get result set meta data
            ResultSetMetaData rsmd = rs.getMetaData();
            int numColumns = rsmd.getColumnCount();
            columnNames = new String[numColumns];
            // Get the column names; column indices start from 1
            for (int i = 1; i < numColumns + 1; i++) {
                columnNames[i - 1] = rsmd.getColumnName(i);
            }
            while (rs.next()) {
                Object[] objects = new Object[numColumns];
                for (int i = 0; i < numColumns; i++) {
                    objects[i] = rs.getObject(columnNames[i]);
                }
                resultList.add(objects);
            }
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, "An error occurred while export " + file.getName() + " cause by " + ex.getMessage(), ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOG.log(Level.SEVERE, null, ex);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    LOG.log(Level.SEVERE, null, ex);
                }
            }
            if (isCloseConnection && connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOG.log(Level.SEVERE, null, ex);
                }
            }
        }
        QueryResult queryResult = new QueryResult();
        queryResult.setColumnNames(columnNames);
        queryResult.setRowDatas(resultList);
        return queryResult;
    }

    public static List<Object[]> getDataOnlyFromSqlFile(Connection connection, File file, Map<String, String> params) {
        return getDataOnlyFromSqlFile(connection, file, params, true);
    }

    public static List<Object[]> getDataOnlyFromSqlFile(Connection connection, File file, Map<String, String> params, boolean isCloseConnection) {
        List<Object[]> resultList = new ArrayList<Object[]>();
        Statement stmt = null;
        ResultSet rs = null;
        try {
            // ��ҹ sql �ҡ File
            String sql = readFileToString(file.getAbsolutePath());
            stmt = connection.createStatement();
            sql = setParameters(sql, params);
//                    LOG.info("sql @ " + file.getName() + " \n "+ sql);
            rs = stmt.executeQuery(sql);
            // Get result set meta data
            ResultSetMetaData rsmd = rs.getMetaData();
            int numColumns = rsmd.getColumnCount();
            String[] columnNames = new String[numColumns];
            // Get the column names; column indices start from 1
            for (int i = 1; i < numColumns + 1; i++) {
                columnNames[i - 1] = rsmd.getColumnName(i);
            }
            while (rs.next()) {
                Object[] objects = new Object[numColumns];
                for (int i = 0; i < numColumns; i++) {
                    objects[i] = rs.getObject(columnNames[i]);
                }
                resultList.add(objects);
            }
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, "An error occurred while export " + file.getName() + " cause by " + ex.getMessage(), ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOG.log(Level.SEVERE, null, ex);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    LOG.log(Level.SEVERE, null, ex);
                }
            }
            if (isCloseConnection && connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOG.log(Level.SEVERE, null, ex);
                }
            }
        }
        return resultList;
    }

    public static List<LinkedHashMap<String, Object>> getDataFromSqlFile(Connection connection, File file, Map<String, String> params) {
        return getDataFromSqlFile(connection, file, params, true);
    }

    public static List<LinkedHashMap<String, Object>> getDataFromSqlFile(Connection connection, File file, Map<String, String> params, boolean isCloseConnection) {
        List<LinkedHashMap<String, Object>> resultList = new ArrayList<LinkedHashMap<String, Object>>();
        Statement stmt = null;
        ResultSet rs = null;
        try {
            // ��ҹ sql �ҡ File
            String sql = readFileToString(file.getAbsolutePath());
            stmt = connection.createStatement();
            sql = setParameters(sql, params);
//                    LOG.info("sql @ " + file.getName() + " \n "+ sql);
            rs = stmt.executeQuery(sql);
            // Get result set meta data
            ResultSetMetaData rsmd = rs.getMetaData();
            int numColumns = rsmd.getColumnCount();
            String[] columnNames = new String[numColumns];
            // Get the column names; column indices start from 1
            for (int i = 1; i < numColumns + 1; i++) {
                columnNames[i - 1] = rsmd.getColumnName(i);
            }
            while (rs.next()) {
                LinkedHashMap<String, Object> record = new LinkedHashMap<String, Object>();
                for (int i = 0; i < numColumns; i++) {
                    record.put(columnNames[i], rs.getObject(columnNames[i]));
                }
                resultList.add(record);
            }
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, "An error occurred while export " + file.getName() + " cause by " + ex.getMessage(), ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOG.log(Level.SEVERE, null, ex);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    LOG.log(Level.SEVERE, null, ex);
                }
            }
            if (isCloseConnection && connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOG.log(Level.SEVERE, null, ex);
                }
            }
        }
        return resultList;
    }

    protected static String readFileToString(String path) {
        File file = new File(path);
        if (!file.exists()) {
            return null;
        }
        String string = null;
        InputStreamReader isr = null;
        try {
            isr = new InputStreamReader(new java.io.FileInputStream(file), SQL_ENCODE);
            StringBuilder buffer;
            try (BufferedReader br = new BufferedReader(isr)) {
                String line;
                buffer = new StringBuilder();
                while ((line = br.readLine()) != null) {
                    buffer.append(line);
                    buffer.append("\n");
                }
            }
            //#############################################
            string = buffer.toString();
        } catch (FileNotFoundException ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        } catch (UnsupportedEncodingException ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        } finally {
            try {
                if (isr != null) {
                    isr.close();
                }
            } catch (IOException ex) {
                LOG.log(Level.SEVERE, ex.getMessage(), ex);
            }

        }
        return string;
    }

    protected static String setParameters(String sql, Map<String, String> params) {
        if (params == null) {
            return sql;
        }
        for (String key : params.keySet()) {
            String value = params.get(key);
            sql = sql.replaceAll(":" + key, value);
        }
        return sql;
    }
}

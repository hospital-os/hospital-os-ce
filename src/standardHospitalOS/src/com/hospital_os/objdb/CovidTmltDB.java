/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.CovidTmlt;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tanakrit
 */
public class CovidTmltDB {

    public ConnectionInf theConnectionInf;

    public CovidTmltDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public CovidTmlt selectByCode(String code) throws Exception {
        String sql = "select * from f_covid_tmlt"
                + " where tmlt_code= ?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            ePQuery.setString(1, code);
            List<CovidTmlt> list = executeQuery(ePQuery);
            return list.isEmpty() ? null : list.get(0);
        }
    }

    public List<CovidTmlt> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<CovidTmlt> list = new ArrayList<>();
        try (ResultSet rs = preparedStatement.executeQuery()) {
            while (rs.next()) {
                CovidTmlt p = new CovidTmlt();
                p.setObjectId(rs.getString("f_covid_tmlt_id"));
                p.tmlt_code = rs.getString("tmlt_code");
                p.tmlt_name = rs.getString("tmlt_name");
                p.component = rs.getString("component");
                p.specimen = rs.getString("specimen");
                p.method = rs.getString("method");
                list.add(p);
            }
            return list;
        }
    }
}

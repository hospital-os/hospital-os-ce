ALTER TABLE b_item_drug ALTER item_drug_caution TYPE text;
ALTER TABLE b_item_drug ALTER item_drug_description TYPE text;
ALTER TABLE b_item_drug ALTER item_drug_special_prescription_text TYPE text;

ALTER TABLE b_item_drug_frequency ALTER item_drug_frequency_description TYPE text;

ALTER TABLE b_item_drug_interaction ALTER item_drug_interaction_act TYPE text;
ALTER TABLE b_item_drug_interaction ALTER item_drug_interaction_repair TYPE text;

ALTER TABLE b_template_sign_symptom ALTER template_sign_symptom_description TYPE text;

ALTER TABLE t_result_xray ALTER result_xray_description TYPE text;
ALTER TABLE t_result_xray ALTER result_xray_notice TYPE text;

ALTER TABLE t_order ALTER order_notice TYPE text;
ALTER TABLE t_order ALTER order_cause_cancel_resultlab TYPE text;

ALTER TABLE t_order_drug ALTER order_drug_caution TYPE text;
ALTER TABLE t_order_drug ALTER order_drug_description TYPE text;
ALTER TABLE t_order_drug ALTER order_drug_special_prescription_text TYPE text;

ALTER TABLE t_visit ALTER visit_notice TYPE text;
ALTER TABLE t_visit ALTER visit_diagnosis_notice TYPE text;
ALTER TABLE t_visit ALTER visit_dx TYPE text;
ALTER TABLE t_visit ALTER visit_cause_appointment TYPE text;

ALTER TABLE t_visit_discharge_advice ALTER visit_discharge_advice_advice TYPE text;

ALTER TABLE t_visit_vital_sign ALTER visit_vital_sign_note TYPE text;

ALTER TABLE t_visit_primary_symptom ALTER visit_primary_symptom_main_symptom TYPE text;
ALTER TABLE t_visit_primary_symptom ALTER visit_primary_symptom_current_illness TYPE text;
ALTER TABLE t_visit_primary_symptom ALTER visit_primary_symptom_general_symptom TYPE text;

ALTER TABLE t_diag_icd10 ALTER diag_icd10_notice TYPE text;
ALTER TABLE t_diag_icd9 ALTER diag_icd9_notice TYPE text;

ALTER TABLE t_result_xray ALTER result_xray_notice TYPE text;
ALTER TABLE t_result_xray ALTER result_xray_description TYPE text;

ALTER TABLE t_patient_personal_disease ALTER patient_personal_disease_description TYPE text;

ALTER TABLE t_patient_risk_factor ALTER patient_risk_factor_description TYPE text;

ALTER TABLE t_patient_past_history ALTER patient_past_history_description TYPE text;

ALTER TABLE t_patient_past_vaccine ALTER patient_past_vaccine_no_comlete_name TYPE text;

ALTER TABLE t_patient_family_history ALTER patient_family_history_description TYPE text;

ALTER TABLE t_chronic ALTER chronic_notice TYPE text;

ALTER TABLE t_visit_refer_in_out ALTER visit_refer_in_out_othdetail TYPE text;
ALTER TABLE t_visit_refer_in_out ALTER visit_refer_in_out_cause TYPE text;
ALTER TABLE t_visit_refer_in_out ALTER visit_refer_in_out_summary_treatment TYPE text;
ALTER TABLE t_visit_refer_in_out ALTER visit_refer_in_out_summary_diagnosis TYPE text;
ALTER TABLE t_visit_refer_in_out ALTER visit_refer_in_out_summary_investigation TYPE text;
ALTER TABLE t_visit_refer_in_out ALTER visit_refer_in_out_current_symptom TYPE text;
ALTER TABLE t_visit_refer_in_out ALTER visit_refer_in_out_family_symptom TYPE text;
ALTER TABLE t_visit_refer_in_out ALTER visit_refer_in_out_diagnosis_final TYPE text;
ALTER TABLE t_visit_refer_in_out ALTER visit_refer_in_out_treatment_result TYPE text;
ALTER TABLE t_visit_refer_in_out ALTER visit_refer_in_out_lab_result TYPE text;
ALTER TABLE t_visit_refer_in_out ALTER visit_refer_in_out_treatment_continue TYPE text;
ALTER TABLE t_visit_refer_in_out ALTER visit_refer_in_out_notice TYPE text;

ALTER TABLE f_ned_reason ADD COLUMN ned_code character varying(255)  default '';

update f_ned_reason set ned_code = 'A' , description ='A. เกิดอาการไม่พึงประสงค์จากยาหรือแพ้ยาที่สามารถใช้ได้ในบัญชียาหลักแห่งชาติ' where f_ned_reason_id = '1';
update f_ned_reason set ned_code = 'B' , description ='B. ผลการรรักษาไม่บรรลุเป้าหมายแม้ว่าได้ใช้ยาในบัญชียาหลักแห่งชาติครบตามมาตรฐานการรรักษาแล้ว' where f_ned_reason_id = '2';
update f_ned_reason set ned_code = 'C' , description ='C. ไม่มีกลุ่มยาในบัญชียาหลักแห่งชาติให้ใช้ แต่ผู้ป่วยมีความจำเป็นในการใช้ยานี้ตามข้อบ่งใช้ที่ได้ขึ้นทะเบียนไว้กับสำนักงานคณะกรรมการอาหารและยา' where f_ned_reason_id = '3';
update f_ned_reason set ned_code = 'D' , description ='D. ผู้ป่วยมีภาวะหรือโรคที่ห้ามใช้ยาในบัญชีอย่างสัมบรูณ์ (absolute contraindication) หรือมีข้อห้ามการใช้ยาในบัญชีร่วมกับยาอื่น (contraindicated/serious/major drug interaction) ที่ผู้ป่วยจำเป็นต้องใช้อย่างหลีกเลี่ยงไม่ได้' where f_ned_reason_id = '4';
update f_ned_reason set ned_code = 'E' , description ='E. ยาในบัญชียาหลักแห่งชาติมีราคาแพงกว่า (ในเชิงความคุ้มค่า)' where f_ned_reason_id = '5';
update f_ned_reason set ned_code = 'F' , description ='F. ผู้ป่วยแสดงความจำนงต้องการ (เบิกไม่ได้)' where f_ned_reason_id = '6';

INSERT INTO s_version VALUES ('9701000000060', '60', 'Hospital OS, Community Edition', '3.9.27', '3.18.081012', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_27.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.27');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class FinanceInsurancePlan extends Persistent implements CommonInf {

    public String code = "";
    public String description = "";
    public String b_finance_insurance_company_id = "";
    public String record_datetime = "";
    public String update_datetime = "";
    public String user_record_id = "";
    public String user_update_id = "";
    public String active = "1";
    // in object only
    public String companyName = "";

    @Override
    public String getCode() {
        return getObjectId();
    }

    @Override
    public String getName() {
        return description;
    }

    @Override
    public String toString() {
        return description;
    }
}

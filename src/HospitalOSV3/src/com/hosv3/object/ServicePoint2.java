/*
 *
 ��������Ǩ��������� service point ��� 
 ServicePoint2.java
 *
 * Created on 1 �ԧ�Ҥ� 2548, 15:57 �.
 */
package com.hosv3.object;

import com.hospital_os.object.ServicePoint;
import com.hospital_os.usecase.connection.CommonInf;

/**
 *
 * @author sumo
 * @deprecated unused 161111 By Somprasong
 */
public class ServicePoint2 extends ServicePoint implements CommonInf {

    static final long serialVersionUID = 0;

    /**
     * Creates a new instance of ServicePoint2
     */
    public ServicePoint2(ServicePoint sp) {
        active = sp.active;
        name = sp.name;
        service_point_id = sp.service_point_id;
        service_sub_type_id = sp.service_sub_type_id;
        service_type_id = sp.service_type_id;
        setObjectId(sp.getObjectId());
    }

    @Override
    public String toString() {
        String str = getName();
        return str;
    }
}

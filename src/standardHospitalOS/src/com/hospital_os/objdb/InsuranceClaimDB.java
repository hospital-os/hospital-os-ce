/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.InsuranceClaim;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class InsuranceClaimDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "819";
    private final SequenceDataDB sequenceDataDB;

    public InsuranceClaimDB(ConnectionInf c) {
        this.connectionInf = c;
        sequenceDataDB = new SequenceDataDB(connectionInf);
    }

    public int insert(InsuranceClaim obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            obj.claim_code = sequenceDataDB.updateSequence("claim", true);
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO t_insurance_claim(\n"
                    + "            t_insurance_claim_id, t_billing_invoice_id, t_visit_insurance_plan_id, claim_code, status_insurance_approve, reason_not_approve, user_record_id, record_datetime, user_update_id, update_datetime, alert_date, status_insurance_receive)\n"
                    + "    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getGenID(tableId));
            preparedStatement.setString(2, obj.t_billing_invoice_id);
            preparedStatement.setString(3, obj.t_visit_insurance_plan_id);
            preparedStatement.setString(4, obj.claim_code);
            preparedStatement.setString(5, obj.status_insurance_approve);
            preparedStatement.setString(6, obj.reason_not_approve);
            preparedStatement.setString(7, obj.user_record_id);
            preparedStatement.setString(8, obj.record_datetime);
            preparedStatement.setString(9, obj.user_update_id);
            preparedStatement.setString(10, obj.update_datetime);
            preparedStatement.setDate(11, obj.alert_date == null ? null : new java.sql.Date(obj.alert_date.getTime()));
            preparedStatement.setString(12, obj.status_insurance_receive);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int updateApprove(InsuranceClaim obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_insurance_claim\n");
            sql.append("   SET status_insurance_approve=?, reason_not_approve=?, \n");
            sql.append("       user_update_id=?, update_datetime=?\n");
            sql.append(" WHERE t_insurance_claim_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.status_insurance_approve);
            preparedStatement.setString(2, obj.reason_not_approve);
            preparedStatement.setString(3, obj.user_update_id);
            preparedStatement.setString(4, obj.update_datetime);
            preparedStatement.setString(5, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int updateAlert(InsuranceClaim obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_insurance_claim\n");
            sql.append("   SET alert_date=?, \n");
            sql.append("       user_update_id=?, update_datetime=?\n");
            sql.append(" WHERE t_insurance_claim_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setDate(1, new java.sql.Date(obj.alert_date.getTime()));
            preparedStatement.setString(2, obj.user_update_id);
            preparedStatement.setString(3, obj.update_datetime);
            preparedStatement.setString(4, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int updateReceiveStatus(InsuranceClaim obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_insurance_claim\n");
            sql.append("   SET status_insurance_receive=?, \n");
            sql.append("       user_update_id=?, update_datetime=?\n");
            sql.append(" WHERE t_insurance_claim_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.status_insurance_receive);
            preparedStatement.setString(2, obj.user_update_id);
            preparedStatement.setString(3, obj.update_datetime);
            preparedStatement.setString(4, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public InsuranceClaim select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select t_insurance_claim.*, b_finance_insurance_company.company_name, b_finance_insurance_plan.description from t_insurance_claim "
                    + "inner join t_visit_insurance_plan on t_visit_insurance_plan.t_visit_insurance_plan_id = t_insurance_claim.t_visit_insurance_plan_id "
                    + "inner join b_finance_insurance_company on b_finance_insurance_company.b_finance_insurance_company_id = t_visit_insurance_plan.b_finance_insurance_company_id "
                    + "inner join b_finance_insurance_plan on b_finance_insurance_plan.b_finance_insurance_plan_id = t_visit_insurance_plan.b_finance_insurance_plan_id "
                    + "where t_insurance_claim_id = ? ";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<InsuranceClaim> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public InsuranceClaim selectClaimCode(String code) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select t_insurance_claim.*, b_finance_insurance_company.company_name, b_finance_insurance_plan.description from t_insurance_claim "
                    + "inner join t_visit_insurance_plan on t_visit_insurance_plan.t_visit_insurance_plan_id = t_insurance_claim.t_visit_insurance_plan_id "
                    + "inner join b_finance_insurance_company on b_finance_insurance_company.b_finance_insurance_company_id = t_visit_insurance_plan.b_finance_insurance_company_id "
                    + "inner join b_finance_insurance_plan on b_finance_insurance_plan.b_finance_insurance_plan_id = t_visit_insurance_plan.b_finance_insurance_plan_id "
                    + "where claim_code = ? ";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, code);
            List<InsuranceClaim> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public InsuranceClaim selectByInsurancePlanId(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select t_insurance_claim.*, b_finance_insurance_company.company_name, b_finance_insurance_plan.description from t_insurance_claim \n"
                    + "inner join t_visit_insurance_plan on t_visit_insurance_plan.t_visit_insurance_plan_id = t_insurance_claim.t_visit_insurance_plan_id \n"
                    + "inner join b_finance_insurance_company on b_finance_insurance_company.b_finance_insurance_company_id = t_visit_insurance_plan.b_finance_insurance_company_id \n"
                    + "inner join b_finance_insurance_plan on b_finance_insurance_plan.b_finance_insurance_plan_id = t_visit_insurance_plan.b_finance_insurance_plan_id \n"
                    + "inner join t_billing_invoice on t_billing_invoice.t_billing_invoice_id = t_insurance_claim.t_billing_invoice_id and t_billing_invoice.billing_invoice_active = '1' \n"
                    + "where t_visit_insurance_plan.t_visit_insurance_plan_id = ? ";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<InsuranceClaim> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<InsuranceClaim> listAlertWithCurrentDate() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select t_insurance_claim.*, b_finance_insurance_company.company_name, b_finance_insurance_plan.description from t_insurance_claim "
                    + "inner join t_visit_insurance_plan on t_visit_insurance_plan.t_visit_insurance_plan_id = t_insurance_claim.t_visit_insurance_plan_id "
                    + "inner join b_finance_insurance_company on b_finance_insurance_company.b_finance_insurance_company_id = t_visit_insurance_plan.b_finance_insurance_company_id "
                    + "inner join b_finance_insurance_plan on b_finance_insurance_plan.b_finance_insurance_plan_id = t_visit_insurance_plan.b_finance_insurance_plan_id "
                    + "where alert_date = current_date\n"
                    + "and t_insurance_claim.status_insurance_approve = '0'\n"
                    + "and t_insurance_claim.status_insurance_receive = '1'\n"
                    + "order by claim_code, claim_code_datetime";
            preparedStatement = connectionInf.ePQuery(sql);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<InsuranceClaim> listByClaimCode(String code) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select t_insurance_claim.*, b_finance_insurance_company.company_name, b_finance_insurance_plan.description from t_insurance_claim "
                    + "inner join t_visit_insurance_plan on t_visit_insurance_plan.t_visit_insurance_plan_id = t_insurance_claim.t_visit_insurance_plan_id "
                    + "inner join b_finance_insurance_company on b_finance_insurance_company.b_finance_insurance_company_id = t_visit_insurance_plan.b_finance_insurance_company_id "
                    + "inner join b_finance_insurance_plan on b_finance_insurance_plan.b_finance_insurance_plan_id = t_visit_insurance_plan.b_finance_insurance_plan_id "
                    + "where claim_code like ? order by claim_code, claim_code_datetime";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, "%" + code + "%");
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<InsuranceClaim> listByClaimDate(Date date1, Date date2) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select t_insurance_claim.*, b_finance_insurance_company.company_name, b_finance_insurance_plan.description from t_insurance_claim "
                    + "inner join t_visit_insurance_plan on t_visit_insurance_plan.t_visit_insurance_plan_id = t_insurance_claim.t_visit_insurance_plan_id "
                    + "inner join b_finance_insurance_company on b_finance_insurance_company.b_finance_insurance_company_id = t_visit_insurance_plan.b_finance_insurance_company_id "
                    + "inner join b_finance_insurance_plan on b_finance_insurance_plan.b_finance_insurance_plan_id = t_visit_insurance_plan.b_finance_insurance_plan_id "
                    + "where claim_code_datetime >= ? and claim_code_datetime <= ? order by claim_code, claim_code_datetime";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setTimestamp(1, new Timestamp(date1.getTime()));
            preparedStatement.setTimestamp(2, new Timestamp(date2.getTime()));
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<InsuranceClaim> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<InsuranceClaim> list = new ArrayList<InsuranceClaim>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                InsuranceClaim obj = new InsuranceClaim();
                obj.setObjectId(rs.getString("t_insurance_claim_id"));
                obj.t_billing_invoice_id = rs.getString("t_billing_invoice_id");
                obj.t_visit_insurance_plan_id = rs.getString("t_visit_insurance_plan_id");
                obj.claim_code_datetime = rs.getTimestamp("claim_code_datetime");
                obj.claim_code = rs.getString("claim_code");
                obj.reason_not_approve = rs.getString("reason_not_approve");
                obj.status_insurance_approve = rs.getString("status_insurance_approve");
                obj.user_record_id = rs.getString("user_record_id");
                obj.record_datetime = rs.getString("record_datetime");
                obj.user_update_id = rs.getString("user_update_id");
                obj.update_datetime = rs.getString("update_datetime");
                obj.alert_date = rs.getDate("alert_date");
                obj.status_insurance_receive = rs.getString("status_insurance_receive");
                try {
                    obj.insurancePlanName = rs.getString("description");
                } catch (Exception ex) {
                }
                try {
                    obj.insuranceCompanyName = rs.getString("company_name");
                } catch (Exception ex) {
                }
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }
}

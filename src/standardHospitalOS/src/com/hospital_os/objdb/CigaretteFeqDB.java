/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.CigaretteFeq;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.ResultSet;
import java.util.Vector;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class CigaretteFeqDB {

    public ConnectionInf theConnectionInf;
    public CigaretteFeq dbObj;
    private final String idtable = "303";

    public CigaretteFeqDB(ConnectionInf db) {
        theConnectionInf = db;
        dbObj = new CigaretteFeq();
        this.initConfig();
    }

    private boolean initConfig() {
        dbObj.table = "f_cigarette_feq";
        dbObj.pk_field = "f_cigarette_feq_id";
        dbObj.desc = "cigarette_feq_description";
        dbObj.active = "active";
        return true;
    }

    @SuppressWarnings("UseOfObsoleteCollectionType")
    public Vector selectAll() throws Exception {
        String sql = "select * from " + dbObj.table;

        Vector v = eQuery(sql);
        if (v.isEmpty()) {
            return null;
        } else {
            return v;
        }
    }

    @SuppressWarnings("UseOfObsoleteCollectionType")
    public Vector eQuery(String sql) throws Exception {
        CigaretteFeq p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            p = new CigaretteFeq();
            p.setObjectId(rs.getString(dbObj.pk_field));
            p.desc = rs.getString(dbObj.desc);
            p.active = rs.getString(dbObj.active);
            list.add(p);
        }
        rs.close();
        return list;
    }
}

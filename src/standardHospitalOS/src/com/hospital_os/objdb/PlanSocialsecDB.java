/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.PlanSocialsec;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.Gutil;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class PlanSocialsecDB {
    private final ConnectionInf theConnectionInf;
    private final String tableId = "849";

    public PlanSocialsecDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public int insert(PlanSocialsec obj) throws Exception {
        String sql = "INSERT INTO b_map_contract_plans_socialsec( "
                + "b_map_contract_plans_socialsec_id, b_contract_plans_id) "
                + "VALUES ('%s', '%s')";
        sql = String.format(sql, obj.getGenID(tableId), obj.b_contract_plans_id);
        return theConnectionInf.eUpdate(sql);
    }

    public int delete(PlanSocialsec itemDrugMapG6PD) throws Exception {
        String sql = "delete from b_map_contract_plans_socialsec where b_map_contract_plans_socialsec_id = '%s'";
        return theConnectionInf.eUpdate(String.format(sql, itemDrugMapG6PD.getObjectId()));
    }

    public PlanSocialsec selectById(String id) throws Exception {
        String sql = "select *, b_contract_plans.contract_plans_description from b_map_contract_plans_socialsec "
                + "inner join b_contract_plans on b_map_contract_plans_socialsec.b_contract_plans_id = b_contract_plans.b_contract_plans_id "
                + "where b_map_contract_plans_socialsec_id = '%s'";
        List<PlanSocialsec> eQuery = eQuery(String.format(sql, id));
        return (PlanSocialsec) (eQuery.isEmpty() ? null : eQuery.get(0));
    }

    public PlanSocialsec selectByPlanId(String id) throws Exception {
        String sql = "select *, b_contract_plans.contract_plans_description from b_map_contract_plans_socialsec "
                + "inner join b_contract_plans on b_map_contract_plans_socialsec.b_contract_plans_id = b_contract_plans.b_contract_plans_id "
                + "where b_contract_plans.b_contract_plans_id = '%s'";
        List<PlanSocialsec> eQuery = eQuery(String.format(sql, id));
        return (PlanSocialsec) (eQuery.isEmpty() ? null : eQuery.get(0));
    }

    public List<PlanSocialsec> listAll() throws Exception {
        String sql = "select *, b_contract_plans.contract_plans_description from b_map_contract_plans_socialsec "
                + "inner join b_contract_plans on b_map_contract_plans_socialsec.b_contract_plans_id = b_contract_plans.b_contract_plans_id ";
        return eQuery(sql);
    }

    public List<PlanSocialsec> eQuery(String sql) throws Exception {
        List<PlanSocialsec> list = new ArrayList<PlanSocialsec>();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        while (rs.next()) {
            PlanSocialsec p = new PlanSocialsec();
            p.setObjectId(rs.getString("b_map_contract_plans_socialsec_id"));
            p.b_contract_plans_id = rs.getString("b_contract_plans_id");
            try{
                p.description = rs.getString("contract_plans_description");
            }catch(Exception ex){
                
            }
            list.add(p);
        }
        rs.close();
        return list;
    }

    public List<Object[]> listByKeyword(String keyword) throws Exception {
        String sql = "select b_map_contract_plans_socialsec.b_map_contract_plans_socialsec_id"
                + ", b_contract_plans.contract_plans_description "
                + "from b_map_contract_plans_socialsec "
                + "inner join b_contract_plans on b_map_contract_plans_socialsec.b_contract_plans_id = b_contract_plans.b_contract_plans_id "
                + "where UPPER(b_contract_plans.contract_plans_description) like UPPER('%s') order by b_contract_plans.contract_plans_description";
        return theConnectionInf.eComplexQuery(String.format(sql, keyword == null || keyword.isEmpty() ? "%" : ("%" + Gutil.CheckReservedWords(keyword) + "%")));
    }

    public List<Object[]> listNotMapByKeyword(String keyword) throws Exception {
        String sql = "select b_contract_plans.b_contract_plans_id"
                + ", b_contract_plans.contract_plans_description "
                + "from b_contract_plans "
                + "where b_contract_plans.contract_plans_active = '1' "
                + "and b_contract_plans.b_contract_plans_id not in (select b_contract_plans_id from b_map_contract_plans_socialsec) "
                + "and UPPER(b_contract_plans.contract_plans_description) like UPPER('%s') order by b_contract_plans.contract_plans_description";
        return theConnectionInf.eComplexQuery(String.format(sql, keyword == null || keyword.isEmpty() ? "%" : ("%" + Gutil.CheckReservedWords(keyword) + "%")));
    }
}

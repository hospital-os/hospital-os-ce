/*
 * PrefixLookup.java
 *
 * Created on 28 �á�Ҥ� 2548, 13:58 �.
 */
package com.hosv3.control.lookup;

//import com.henbe.connection.*;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.LookupControlInf;
import com.hosv3.control.LookupControl;

/**
 *
 * @not deprecated because use henbe package
 *
 * @author kingland
 */
public class TambonLookup implements LookupControlInf {

    private LookupControl theLookup;

    /**
     * Creates a new instance of PrefixLookup
     */
    public TambonLookup(LookupControl lookup) {
        theLookup = lookup;
    }

    @Override
    @SuppressWarnings("UseOfObsoleteCollectionType")
    public java.util.Vector listData(String str) {
        return theLookup.listTambon(str, theLookup.getSAmphur());
    }

    @Override
    public CommonInf readHosData(String str) {
        return theLookup.readAddressById(str);
    }
}

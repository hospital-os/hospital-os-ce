/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.control;

import com.hospital_os.object.Authentication;
import com.hospital_os.object.Employee;
import com.hospital_os.object.FinanceInsurancePlan;
import com.hospital_os.object.GovCode;
import com.hospital_os.object.ICD10;
import com.hospital_os.object.Item;
import com.hospital_os.object.Nation;
import com.hospital_os.object.Person;
import com.hospital_os.object.Plan;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.DataSource;
import com.hosv3.utility.search.DialogSearchDatasource;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class AllDialogDatasourceControl {

    private static final Logger LOG = Logger.getLogger(AllDialogDatasourceControl.class.getName());
    private final HosControl theHC;
    private final ConnectionInf theConnectionInf;
    private final HosDB theHosDB;

    public AllDialogDatasourceControl(HosControl theHC) {
        this.theHC = theHC;
        this.theConnectionInf = this.theHC.theConnectionInf;
        this.theHosDB = this.theHC.theHosDB;
    }

    public DialogSearchDatasource getDatasourcePerson() {
        return datasourcePerson;
    }
    private DialogSearchDatasource datasourcePerson = new DialogSearchDatasource() {
        @Override
        public List<DataSource> search(String keyword, String active) {
            List<DataSource> dataSources = new ArrayList<DataSource>();
            theConnectionInf.open();
            try {
                theConnectionInf.getConnection().setAutoCommit(false);
                List<Person> list = theHosDB.thePersonDB.listByKeyword(keyword);
                for (Person person : list) {
                    dataSources.add(new DataSource(person.getObjectId(), person.prefix.description
                            + person.person_firstname
                            + " " + person.person_lastname));
                }
                theConnectionInf.getConnection().commit();
            } catch (Exception ex) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                try {
                    theConnectionInf.getConnection().rollback();
                } catch (SQLException ex1) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex1);
                }
            } finally {
                theConnectionInf.close();
            }
            return dataSources;
        }
    };

    public DialogSearchDatasource getDatasourceEmployee() {
        return datasourceEmployee;
    }
    private DialogSearchDatasource datasourceEmployee = new DialogSearchDatasource() {
        @Override
        public List<DataSource> search(String keyword, String active) {
            List<DataSource> dataSources = new ArrayList<DataSource>();
            theConnectionInf.open();
            try {
                theConnectionInf.getConnection().setAutoCommit(false);
                Vector<Employee> list = theHosDB.theEmployeeDB.selectAllByName(keyword, active);
                for (Employee employee : list) {
                    dataSources.add(new DataSource(employee.getObjectId(), employee.employee_id
                            + " - "
                            + employee.person.person_firstname
                            + " " + employee.person.person_lastname));
                }
                theConnectionInf.getConnection().commit();
            } catch (Exception ex) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                try {
                    theConnectionInf.getConnection().rollback();
                } catch (SQLException ex1) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex1);
                }
            } finally {
                theConnectionInf.close();
            }
            return dataSources;
        }
    };

    public DialogSearchDatasource getDatasourceEmployeeAuthen() {
        return datasourceEmployeeAuthen;
    }
    private DialogSearchDatasource datasourceEmployeeAuthen = new DialogSearchDatasource() {
        @Override
        public List<DataSource> search(String keyword, String active) {
            List<DataSource> dataSources = new ArrayList<>();
            theConnectionInf.open();
            try {
                theConnectionInf.getConnection().setAutoCommit(false);
                Vector<Authentication> list = theHosDB.theAuthenticationDB.selectAuthenAll(keyword);
                for (Authentication authentication : list) {
                    dataSources.add(new DataSource(authentication.getObjectId(), authentication.description));
                }
                theConnectionInf.getConnection().commit();
            } catch (Exception ex) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                try {
                    theConnectionInf.getConnection().rollback();
                } catch (SQLException ex1) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex1);
                }
            } finally {
                theConnectionInf.close();
            }
            return dataSources;
        }
    };

    public DialogSearchDatasource getDatasourceDoctor() {
        return datasourceDoctor;
    }
    private DialogSearchDatasource datasourceDoctor = new DialogSearchDatasource() {
        @Override
        public List<DataSource> search(String keyword, String active) {
            List<DataSource> dataSources = new ArrayList<DataSource>();
            theConnectionInf.open();
            try {
                theConnectionInf.getConnection().setAutoCommit(false);
                Vector<Employee> list = theHosDB.theEmployeeDB.selectAuthenAllByName(keyword, active, new String[]{
                    Authentication.DOCTOR,
                    Authentication.THAI_DOCTOR,
                    Authentication.PHYSICAL_THERAPY});
                for (Employee employee : list) {
                    dataSources.add(new DataSource(employee, employee.getName()));
                }
                theConnectionInf.getConnection().commit();
            } catch (Exception ex) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                try {
                    theConnectionInf.getConnection().rollback();
                } catch (SQLException ex1) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex1);
                }
            } finally {
                theConnectionInf.close();
            }
            return dataSources;
        }
    };

    private DialogSearchDatasource datasourceICD10 = new DialogSearchDatasource() {
        @Override
        public List<DataSource> search(String keyword, String active) {
            List<DataSource> dataSources = new ArrayList<DataSource>();
            theConnectionInf.open();
            try {
                theConnectionInf.getConnection().setAutoCommit(false);
                Vector<ICD10> selectByIdName = theHosDB.theICD10DB.selectByIdName(keyword);
                for (ICD10 icd10 : selectByIdName) {
                    dataSources.add(new DataSource(icd10.getObjectId(), icd10.icd10_id + " : " + icd10.description));
                }
                theConnectionInf.getConnection().commit();
            } catch (Exception ex) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                try {
                    theConnectionInf.getConnection().rollback();
                } catch (SQLException ex1) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex1);
                }
            } finally {
                theConnectionInf.close();
            }
            return dataSources;
        }
    };

    public DialogSearchDatasource getDatasourceICD10() {
        return datasourceICD10;
    }

    public DialogSearchDatasource getDatasourceItem() {
        return datasourceItem;
    }
    private DialogSearchDatasource datasourceItem = new DialogSearchDatasource() {
        @Override
        public List<DataSource> search(String keyword, String active) {
            List<DataSource> dataSources = new ArrayList<DataSource>();
            theConnectionInf.open();
            try {
                theConnectionInf.getConnection().setAutoCommit(false);
                Vector<Item> list = theHosDB.theItemDB.selectAllByPK(keyword, active);
                for (Item item : list) {
                    dataSources.add(new DataSource(item.getObjectId(), item.common_name + " (" + item.trade_name + ")"));
                }
                theConnectionInf.getConnection().commit();
            } catch (Exception ex) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                try {
                    theConnectionInf.getConnection().rollback();
                } catch (SQLException ex1) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex1);
                }
            } finally {
                theConnectionInf.close();
            }
            return dataSources;
        }
    };
    private DialogSearchDatasource datasourceLab = new DialogSearchDatasource() {
        @Override
        public List<DataSource> search(String keyword, String active) {
            List<DataSource> dataSources = new ArrayList<DataSource>();
            theConnectionInf.open();
            try {
                theConnectionInf.getConnection().setAutoCommit(false);
                Vector<Item> list = theHosDB.theItemDB.selectByItemGroupId("2", keyword, active, true);
                for (Item item : list) {
                    dataSources.add(new DataSource(item.getObjectId(), item.item_id + " : " + item.common_name));
                }
                theConnectionInf.getConnection().commit();
            } catch (Exception ex) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                try {
                    theConnectionInf.getConnection().rollback();
                } catch (SQLException ex1) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex1);
                }
            } finally {
                theConnectionInf.close();
            }
            return dataSources;
        }
    };

    public DialogSearchDatasource getDatasourceLab() {
        return datasourceLab;
    }
    private DialogSearchDatasource datasourceMed = new DialogSearchDatasource() {
        @Override
        public List<DataSource> search(String keyword, String active) {
            List<DataSource> dataSources = new ArrayList<DataSource>();
            theConnectionInf.open();
            try {
                theConnectionInf.getConnection().setAutoCommit(false);
                Vector<Item> list = theHosDB.theItemDB.selectByItemGroupId("1", keyword, active, true);
                for (Item item : list) {
                    dataSources.add(new DataSource(item.getObjectId(), item.item_id + " : " + item.common_name));
                }
                theConnectionInf.getConnection().commit();
            } catch (Exception ex) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                try {
                    theConnectionInf.getConnection().rollback();
                } catch (SQLException ex1) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex1);
                }
            } finally {
                theConnectionInf.close();
            }
            return dataSources;
        }
    };

    public DialogSearchDatasource getDatasourceMed() {
        return datasourceMed;
    }
    private DialogSearchDatasource datasourcePlan = new DialogSearchDatasource() {
        @Override
        public List<DataSource> search(String keyword, String active) {
            List<DataSource> dataSources = new ArrayList<DataSource>();
            theConnectionInf.open();
            try {
                theConnectionInf.getConnection().setAutoCommit(false);
                Vector<Plan> list = theHosDB.thePlanDB.selectByCNA(keyword, active);
                for (Plan plan : list) {
                    dataSources.add(new DataSource(plan.getObjectId(), plan.plan_id + " : " + plan.description));
                }
                theConnectionInf.getConnection().commit();
            } catch (Exception ex) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                try {
                    theConnectionInf.getConnection().rollback();
                } catch (SQLException ex1) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex1);
                }
            } finally {
                theConnectionInf.close();
            }
            return dataSources;
        }
    };

    public DialogSearchDatasource getDatasourcePlan() {
        return datasourcePlan;
    }
    private DialogSearchDatasource datasourceInsurancePlan = new DialogSearchDatasource() {
        @Override
        public List<DataSource> search(String keyword, String active) {
            List<DataSource> dataSources = new ArrayList<DataSource>();
            theConnectionInf.open();
            try {
                theConnectionInf.getConnection().setAutoCommit(false);
                List<FinanceInsurancePlan> list = theHosDB.theFinanceInsurancePlanDB.listByKeywordCompanyAndPlan(keyword, active);
                for (FinanceInsurancePlan plan : list) {
                    dataSources.add(new DataSource(plan, plan.companyName + " : " + plan.description));
                }
                theConnectionInf.getConnection().commit();
            } catch (Exception ex) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                try {
                    theConnectionInf.getConnection().rollback();
                } catch (SQLException ex1) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex1);
                }
            } finally {
                theConnectionInf.close();
            }
            return dataSources;
        }
    };

    public DialogSearchDatasource getDatasourceInsurancePlan() {
        return datasourceInsurancePlan;
    }

    public DialogSearchDatasource getDatasourceNation() {
        return datasourceNation;
    }
    private DialogSearchDatasource datasourceNation = new DialogSearchDatasource() {
        @Override
        public List<DataSource> search(String keyword, String active) {
            List<DataSource> dataSources = new ArrayList<DataSource>();
            theConnectionInf.open();
            try {
                theConnectionInf.getConnection().setAutoCommit(false);
                Vector<Nation> list = theHosDB.theNationDB.selectByCN(keyword);
                for (Nation nation : list) {
                    dataSources.add(new DataSource(nation, nation.description));
                }
                theConnectionInf.getConnection().commit();
            } catch (Exception ex) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                try {
                    theConnectionInf.getConnection().rollback();
                } catch (SQLException ex1) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex1);
                }
            } finally {
                theConnectionInf.close();
            }
            return dataSources;
        }
    };

    public DialogSearchDatasource getDatasourceGovCode() {
        return datasourceGovCode;
    }
    private DialogSearchDatasource datasourceGovCode = new DialogSearchDatasource() {
        @Override
        public List<DataSource> search(String keyword, String active) {
            List<DataSource> dataSources = new ArrayList<DataSource>();
            theConnectionInf.open();
            try {
                theConnectionInf.getConnection().setAutoCommit(false);
                List<GovCode> list = theHosDB.theGovCodeDB.listByKeyword(keyword);
                for (GovCode obj : list) {
                    dataSources.add(new DataSource(obj, obj.description));
                }
                theConnectionInf.getConnection().commit();
            } catch (Exception ex) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                try {
                    theConnectionInf.getConnection().rollback();
                } catch (SQLException ex1) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex1);
                }
            } finally {
                theConnectionInf.close();
            }
            return dataSources;
        }
    };
    private DialogSearchDatasource datasourceICD10Accident = new DialogSearchDatasource() {
        @Override
        public List<DataSource> search(String keyword, String limit) {
            List<DataSource> dataSources = new ArrayList<DataSource>();
            if (keyword == null || keyword.length() < 3) {
                return dataSources;
            }
            theConnectionInf.open();
            try {
                theConnectionInf.getConnection().setAutoCommit(false);
                Vector<ICD10> list = theHosDB.theICD10DB.selectAccidentByIdName(keyword, 0);
                for (ICD10 icd10 : list) {
                    dataSources.add(new DataSource(icd10.icd10_id, icd10.icd10_id + " : " + (icd10.icd10_description_th == null
                            || icd10.icd10_description_th.isEmpty() ? icd10.description : icd10.icd10_description_th)));
                }
                theConnectionInf.getConnection().commit();
            } catch (Exception ex) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                try {
                    theConnectionInf.getConnection().rollback();
                } catch (SQLException ex1) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex1);
                }
            } finally {
                theConnectionInf.close();
            }
            return dataSources;
        }
    };

    public DialogSearchDatasource getDatasourceICD10Accident() {
        return datasourceICD10Accident;
    }

    private DialogSearchDatasource datasourceICD10Chronic = new DialogSearchDatasource() {
        @Override
        public List<DataSource> search(String keyword, String limit) {
            List<DataSource> dataSources = new ArrayList<DataSource>();
            if (keyword == null || keyword.length() < 2) {
                return dataSources;
            }
            theConnectionInf.open();
            PreparedStatement ps = null;
            try {
                theConnectionInf.getConnection().setAutoCommit(false);
                String sql = "select b_icd10.icd10_number, b_icd10.icd10_description from b_icd10\n"
                        + "inner join b_group_icd10 on b_group_icd10.group_icd10_number = b_icd10.icd10_number\n"
                        + "inner join b_group_chronic on b_group_icd10.b_group_chronic_id = b_group_chronic.b_group_chronic_id and b_group_chronic.group_chronic_active = '1'\n"
                        + "where 1=1 \n";
                if (theHC.theHO.thePatient != null) {
                    sql += "and b_icd10.icd10_number not in (select chronic_icd10 from t_chronic where chronic_hn = ?)\n";
                }
                sql += "and (b_icd10.icd10_number ilike  ? or b_icd10.icd10_description ilike  ? or b_icd10.icd10_description_th ilike  ?)\n"
                        + "order by b_icd10.icd10_number";
                ps = theConnectionInf.ePQuery(sql);
                int index = 1;
                if (theHC.theHO.thePatient != null) {
                    ps.setString(index++, theHC.theHO.thePatient.hn);
                }
                ps.setString(index++, "%" + keyword + "%");
                ps.setString(index++, "%" + keyword + "%");
                ps.setString(index++, "%" + keyword + "%");

                List<Object[]> eComplexQuery = theConnectionInf.eComplexQuery(ps.toString());
//                System.out.println(ps.toString());
                for (Object[] objects : eComplexQuery) {
                    dataSources.add(new DataSource(objects[0], objects[0] + " : " + objects[1]));
                }
                theConnectionInf.getConnection().commit();
            } catch (Exception ex) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                try {
                    theConnectionInf.getConnection().rollback();
                } catch (SQLException ex1) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex1);
                }
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        LOG.log(Level.SEVERE, null, ex);
                    }
                }
                theConnectionInf.close();
            }
            return dataSources;
        }
    };

    public DialogSearchDatasource getDatasourceICD10Chronic() {
        return datasourceICD10Chronic;
    }
}

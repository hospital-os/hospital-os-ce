/*
 * ReadModule.java
 *
 * Created on 27 �ѹ��¹ 2548, 9:32 �.
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
package com.hosv3.utility;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.hospital_os.object.ModuleLicense;
import com.hospital_os.object.Version;
import com.hospital_os.usecase.connection.ModuleInfTool;
import com.hosv3.control.HosControl;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author tong(Padungrat) henbe modify ���ͺ������
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class ReadModule {

    private static String MODULE_DIR;
    private File file;
    private DefaultReadModule theDefaultReadModule;
    private String module;
    private int[] row;

    public ReadModule() {
        this(Config.MODULE_PATH);
    }

    public ReadModule(String moduleDir) {
        MODULE_DIR = moduleDir;
        file = new File(MODULE_DIR);
        module = "-module=";
    }

    public static Vector loadModule(String[] argc, String dir, String dir_rp) {
        LOG.info("Start load module...");
        ReadModule rm = new ReadModule(dir);
        String modules = "";
        for (String argc1 : argc) {
            // load from xml files.
            if (argc1.equals("-module_xml")) {
                modules = rm.listModule(MODULE_DIR, dir_rp);
            }
            // load from arguments
            if (argc1.startsWith("-module=")) {
                modules = argc1;
            }
        }
        LOG.log(Level.INFO, "List of modules\n{0}", modules);
        // add each module implemented to vector "vModules".
        Vector vModules = new Vector();
        int index = modules.indexOf('=');
        if (index == -1) {
            return vModules;
        }
        modules = modules.substring(index + 1) + ";";
        index = modules.indexOf(';');
        while (index != -1) {
            String mod = modules.substring(0, index);
            modules = modules.substring(index + 1);
            index = modules.indexOf(';');
            try {
                if (mod.trim().length() == 0) {
                    continue;
                }
                ModuleInfTool mi = (ModuleInfTool) Class.forName(mod).newInstance();
                vModules.add(mi);
                LOG.log(Level.INFO, "Add external module \"{0}\" complete.", mod);
            } catch (ClassNotFoundException e) {
                LOG.log(Level.INFO, "Class not found for external module : {0}", mod);
            } catch (IllegalAccessException e) {
                LOG.log(Level.INFO, "Class not found for external module : {0}", mod);
            } catch (InstantiationException e) {
                LOG.log(Level.INFO, "Class not found for external module : {0}", mod);
            }
        }
        return vModules;
    }

    private String listModule(String dir, String dir_rp) {
        StringBuilder sb = new StringBuilder("-module=");
        File mFile = new File(dir);
        File[] filemodules = mFile.listFiles();
        for (File filemodule : filemodules) {
            String filename = filemodule.getAbsolutePath();
            if (filename.toLowerCase().endsWith(".xml")) {
                theDefaultReadModule = new DefaultReadModule(filemodule);
                theDefaultReadModule.checkXMLFile();
                theDefaultReadModule.readXML();
                sb.append(theDefaultReadModule.getModule()).append(";");
                theDefaultReadModule.getIndex();
            }
        }
        if (dir_rp != null) {
            mFile = new File(dir_rp);
            filemodules = mFile.listFiles();
            for (File filemodule : filemodules) {
                String filename = filemodule.getAbsolutePath();
                if (filename.toLowerCase().endsWith(".xml")) {
                    theDefaultReadModule = new DefaultReadModule(filemodule);
                    theDefaultReadModule.checkXMLFile();
                    theDefaultReadModule.readXML();
                    sb.append(theDefaultReadModule.getModule()).append(";");
                    theDefaultReadModule.getIndex();
                }
            }
        }
        return sb.toString();
    }

    /**
     * ��㹡�� sort Index ��� array �ͧ row ������ ������仵�� Index
     * ������ config ������
     */
    private void sortIndex() {
        // �ӡ��ǹ�ٻ ����ӹǹ �ͧ array �ͧ row
        for (int i = 0; i < row.length; i++) {
            int temp = row[i];
            //��Ǩ�ͺ��� ��ͧ����繵���ش����
            if (i != row.length - 1) {
                //�ӡ������º���º��Ңͧ array row ������ ��ҹ��¡��� ���ӡ�� swap ��ҷ�鹷��
                if (temp > row[i + 1]) {
                    row[i] = row[i + 1];
                    row[i + 1] = temp;
                }
            }
        }
    }

    /**
     * �ӡ�õ�Ǩ�ͺ ��� Index ���������������ա�ë�ӡѹ�Ѻ Index
     * ���������������� ����ի�ӡѹ �зӡ��������Ң��价���� 1
     * ��зӡ�õ�Ǩ�ͺ�ա��� �ի�ӡѹ������� �������� ��� return
     * ��ҷ��١��ͧ�͡��
     *
     * @param newIndex �� int �繤�� Index ������Ǩ�ͺ��ҫ�ӡѹ�������
     * @return int �� Index ���� ��� function �ӡ�õ�Ǩ�ͺ ����觤�������
     */
    private int checkIndex(int newIndex) {
        int index = newIndex;
        //�ӡ��ǹ�ٺ��� �ӹǹ array �ͧ row ���������
        for (int i = 0; i < row.length; i++) {
            //��Ǩ�ͺ ��ҷ���Ѻ�������ҫ�ӡѹ�������
            if (newIndex == row[i]) {   //��ӡѹ�ӡ����������ա 1 ��зӡ�õ�Ǩ�ͺ ��ҫ�ӡѹ�Ѻ����������������
                newIndex = newIndex + 1;
                index = checkIndex(newIndex);
            }

        }

        //   LOG.info(index);
        return index;
    }
//
//    /**
//     * @deprecated unused 151111 By Somprasong
//     * @param filemodule
//     * @param hashModule
//     */
//    private void loadModule(File filemodule, HashMap hashModule) {
//        theDefaultReadModule = new DefaultReadModule(filemodule);
//        theDefaultReadModule.checkXMLFile();
//        theDefaultReadModule.readXML();
//        int index = checkIndex(Integer.parseInt(theDefaultReadModule.getIndex()));
//        row[count] = index;
//        hashModule.put(String.valueOf(index), theDefaultReadModule.getModule());
//        count++;
//
//
//    }

//    /**
//     * @deprecated unused 151111 By Somprasong ��㹡�õ�Ǩ�ͺ ����� Directory
//     * �������������� �������ըзӡ�����ҧ Directory ������
//     * @return boolean true ��Directory �����������, false ����� Directory ����
//     * ���� ����դ��(null)
//     */
//    private boolean checkFile(String dir) {
//        file = new File(MODULE_DIR);
//        boolean result = true;
//        if (file != null) {   // ������������
//            if (!file.exists()) {   //���ҧ Direcroty ���
//                createFile();
//                result = false;
//            }
//        } else {
//            result = false;
//        }
//        return result;
//    }
    /**
     * ��㹡�����ҧ Directory ������������� Directory
     */
    private void createFile() {
        java.io.File f = file;//new java.io.File(file.getParent());
        LOG.log(Level.INFO, "Create file {0} is Exists : {1}", new Object[]{f.toString(), f.exists()});
        java.io.File fi = new java.io.File(f.getParent());

        try {

            if (!f.exists()) {
                /**
                 * ��ͧ ���ҧ Folder gui ���ͧ��Ǩ�ͺ��͹����� Folder
                 * ��͹˹�ҹ������
                 */
                if (!f.exists()) {
                    /**
                     * ��ͧ ���ҧ Folder config
                     */
                    fi.mkdir();
                }
                f.mkdir();

            }

        } catch (Exception ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    /**
     * @deprecated unused 151111 By Somprasong
     * @param hashModule
     * @return
     */
    private String genModuleArgc(HashMap hashModule) {
        sortIndex();
        module = "-module";
        for (int i = 0; i < row.length; i++) {
            if (hashModule.get(String.valueOf(row[i])) == null) {
                continue;
            }

//                if(i==(row.length-1))
//                {   //�ӡ�� load ��� ����к�
//                    module = module+hashModule.get(String.valueOf(row[i]));
//                }
//                else
//                {   //�ӡ�� load ��� ����к�
//                    module = module+hashModule.get(String.valueOf(row[i])) + END_MODULE;
//                }
            // LOG.info(module);
            LOG.log(Level.INFO, "Index:  {0} : {1}", new Object[]{row[i], hashModule.get(String.valueOf(row[i]))});
            if (i == 0) {
                module = module + "=" + String.valueOf(hashModule.get(String.valueOf(row[i])));
            } else {
                module = module + ";" + String.valueOf(hashModule.get(String.valueOf(row[i])));
            }
        }
        LOG.info(module);
        return module;
    }

    public static boolean checkLicense(HosControl hc, ModuleInfTool mi) {
        Version v = (Version) mi.getObjectVersion();

        if (!v.requiredLicense) {
            return true;
        }
        Package pack = mi.getClass().getPackage();
        String packageName = pack.getName();
//        System.out.println("packageName: " + packageName);
        ModuleLicense ml = hc.theSystemControl.getModuleLicenseByModuleId(packageName);

        if (ml == null) {
            return false;
        }

        String license;

        try {
            String licenseHex = ml.license;
            byte[] bytes = Hex.decodeHex(licenseHex.toCharArray());

            license = new String(bytes, "UTF-8");
        } catch (UnsupportedEncodingException | DecoderException ex) {
            LOG.log(Level.SEVERE, null, ex);
            return false;
        }

        v.expiresAt = ml.expires_at;

        String hcode = hc.theHO.theSite.off_id;
        String secretKey = hcode + "-" + ml.getObjectId();
        secretKey = DigestUtils.sha256Hex(secretKey);

        try {
            DecodedJWT decode = JwtUtil.verify(license, hcode, secretKey);
            if (!decode.getSubject().equals(packageName)) {
                return false;
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, null, ex);
            return false;
        }

        return true;
    }

    private static final Logger LOG = Logger.getLogger(ReadModule.class.getName());
}

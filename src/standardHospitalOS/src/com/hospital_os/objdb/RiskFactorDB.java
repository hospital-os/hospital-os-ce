/*
 * RiskFactorDB.java
 *
 * Created on 10 ����Ҿѹ�� 2549, 16:47 �.
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.*;
import com.hospital_os.usecase.connection.*;
import com.hospital_os.utility.*;
import java.sql.*;
import java.util.*;

/**
 *
 * @author amp
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class RiskFactorDB {

    public ConnectionInf theConnectionInf;
    public RiskFactor dbObj;
    final public String idtable = "277";

    /**
     * Creates a new instance of RiskFactorDB
     */
    public RiskFactorDB(ConnectionInf db) {
        theConnectionInf = db;
        dbObj = new RiskFactor();
        initConfig();
    }

    private boolean initConfig() {
        dbObj.table = "t_patient_risk_factor";
        dbObj.pk_field = "t_patient_risk_factor_id";
        dbObj.patient_id = "t_patient_id";
        dbObj.topic = "patient_risk_factor_topic";
        dbObj.description = "patient_risk_factor_description";
        dbObj.staff_record = "patient_risk_factor_staff_record";
        dbObj.record_date_time = "patient_risk_factor_record_date_time";
        dbObj.risk_alcoho_result = "risk_alcoho_result";
        dbObj.risk_alcoho_about = "risk_alcoho_about";
        dbObj.risk_alcoho_about_week = "risk_alcoho_about_week";
        dbObj.risk_alcoho_advise = "risk_alcoho_advise";
        dbObj.risk_cigarette_result = "risk_cigarette_result";
        dbObj.risk_cigarette_about = "risk_cigarette_about";
        dbObj.risk_cigarette_advise = "risk_cigarette_advise";
        dbObj.risk_cigarette_about_week = "risk_cigarette_about_week";
        dbObj.risk_exercise_result = "risk_exercise_result";
//        dbObj.risk_exercise_advice = "risk_exercise_advice";
        return true;
    }

    public int insert(RiskFactor o) throws Exception {
        RiskFactor p = o;
        p.generateOID(idtable);
        String sql = "insert into " + dbObj.table + " ("
                + dbObj.pk_field
                + " ," + dbObj.patient_id
                + " ," + dbObj.topic
                + " ," + dbObj.description
                + " ," + dbObj.staff_record
                + " ," + dbObj.record_date_time
                + " ," + dbObj.risk_alcoho_result
                + " ," + dbObj.risk_alcoho_about
                + " ," + dbObj.risk_alcoho_about_week
                + " ," + dbObj.risk_alcoho_advise
                + " ," + dbObj.risk_cigarette_result
                + " ," + dbObj.risk_cigarette_about
                + " ," + dbObj.risk_cigarette_about_week
                + " ," + dbObj.risk_cigarette_advise
                + " ," + dbObj.risk_exercise_result
//                + " ," + dbObj.risk_exercise_advice
                + " ) values ('"
                + p.getObjectId()
                + "','" + p.patient_id
                + "','" + p.topic
                + "','" + Gutil.CheckReservedWords(p.description)
                + "','" + p.staff_record
                + "','" + p.record_date_time
                + "','" + p.risk_alcoho_result
                + "','" + p.risk_alcoho_about
                + "','" + p.risk_alcoho_about_week
                + "','" + Gutil.CheckReservedWords(p.risk_alcoho_advise)
                + "','" + p.risk_cigarette_result
                + "','" + p.risk_cigarette_about
                + "','" + p.risk_cigarette_about_week
                + "','" + Gutil.CheckReservedWords(p.risk_cigarette_advise)
                + "','" + p.risk_exercise_result
//                + "','" + Gutil.CheckReservedWords(p.risk_exercise_advice)
                + "')";
        sql = Gutil.convertSQLToMySQL(sql, theConnectionInf.gettypeDatabase());
        return theConnectionInf.eUpdate(sql);
    }

    public int update(RiskFactor o) throws Exception {
        String sql = "update " + dbObj.table + " set ";
        RiskFactor p = o;
        String field = ""
                + "', " + dbObj.patient_id + "='" + p.patient_id
                + "', " + dbObj.topic + "='" + p.topic
                + "', " + dbObj.description + "='" + Gutil.CheckReservedWords(p.description)
                + "', " + dbObj.staff_record + "='" + p.staff_record
                + "', " + dbObj.record_date_time + "='" + p.record_date_time
                + "', " + dbObj.risk_alcoho_result + "='" + p.risk_alcoho_result
                + "', " + dbObj.risk_alcoho_about + "='" + p.risk_alcoho_about
                + "', " + dbObj.risk_alcoho_about_week + "='" + p.risk_alcoho_about_week
                + "', " + dbObj.risk_alcoho_advise + "='" + Gutil.CheckReservedWords(p.risk_alcoho_advise)
                + "', " + dbObj.risk_cigarette_result + "='" + p.risk_cigarette_result
                + "', " + dbObj.risk_cigarette_about + "='" + p.risk_cigarette_about
                + "', " + dbObj.risk_cigarette_about_week + "='" + p.risk_cigarette_about_week
                + "', " + dbObj.risk_cigarette_advise + "='" + Gutil.CheckReservedWords(p.risk_cigarette_advise)
                + "', " + dbObj.risk_exercise_result + "='" + p.risk_exercise_result
//                + "', " + dbObj.risk_exercise_advice + "='" + Gutil.CheckReservedWords(p.risk_exercise_advice)
                + "' where " + dbObj.pk_field + "='" + p.getObjectId() + "'";
        sql = Gutil.convertSQLToMySQL(sql + field.substring(2), theConnectionInf.gettypeDatabase());
        return theConnectionInf.eUpdate(sql);
    }

    public int delete(RiskFactor o) throws Exception {
        String sql = "delete from " + dbObj.table
                + " where " + dbObj.pk_field + "='" + o.getObjectId() + "'";
        return theConnectionInf.eUpdate(sql);
    }

    public int deleteByPtid(String patient_id) throws Exception {
        String sql = "delete from " + dbObj.table
                + " where " + dbObj.patient_id + "='" + patient_id + "'";
        return theConnectionInf.eUpdate(sql);
    }

    public Vector selectByPatientId(String patientId) throws Exception {
        String sql = "select * from " + dbObj.table
                + " where " + dbObj.patient_id
                + " = '" + patientId + "'";

        return eQuery(sql);
    }

    public Vector eQuery(String sql) throws Exception {
        RiskFactor p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            p = new RiskFactor();
            p.setObjectId(rs.getString(dbObj.pk_field));
            p.patient_id = rs.getString(dbObj.patient_id);
            p.topic = rs.getString(dbObj.topic);
            p.description = rs.getString(dbObj.description);
            p.staff_record = rs.getString(dbObj.staff_record);
            p.record_date_time = rs.getString(dbObj.record_date_time);
            p.risk_alcoho_result = rs.getString(dbObj.risk_alcoho_result);
            p.risk_alcoho_about = rs.getString(dbObj.risk_alcoho_about);
            p.risk_alcoho_about_week = rs.getString(dbObj.risk_alcoho_about_week);
            p.risk_alcoho_advise = rs.getString(dbObj.risk_alcoho_advise);
            p.risk_cigarette_result = rs.getString(dbObj.risk_cigarette_result);
            p.risk_cigarette_about = rs.getString(dbObj.risk_cigarette_about);
            p.risk_cigarette_about_week = rs.getString(dbObj.risk_cigarette_about_week);
            p.risk_cigarette_advise = rs.getString(dbObj.risk_cigarette_advise);
            p.risk_exercise_result = rs.getString(dbObj.risk_exercise_result);
//            p.risk_exercise_advice = rs.getString(dbObj.risk_exercise_advice);
            list.add(p);
        }
        rs.close();
        return list;
    }
}

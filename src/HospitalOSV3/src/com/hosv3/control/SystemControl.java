/*
 * SystemControl.java
 *
 * Created on 25 ���Ҥ� 2546, 13:51 �.
 */
package com.hosv3.control;

import com.hospital_os.gui.connection.AbsVersionControl;
import com.hospital_os.gui.connection.AbsVersionControl3;
import com.hospital_os.object.*;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hospital_os.utility.*;
import com.hosv3.control.thread.*;
import com.hosv3.control.version.CheckVersionHealthy;
import com.hosv3.control.version.CheckVersionHos;
import com.hosv3.control.version.CheckVersionPcu;
import com.hosv3.control.version.CheckVersionReport;
import com.hosv3.object.*;
import com.hosv3.subject.HosSubject;
import com.hosv3.utility.Constant;
import com.hosv3.utility.ResourceBundle;
import com.hosv3.utility.Splash;
import java.io.*;
import java.sql.*;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author henbe
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class SystemControl {

    private static final Logger LOG = Logger.getLogger(BillingControl.class.getName());
    ConnectionInf theConnectionInf;
    HosDB theHosDB;
    HosObject theHO;
    HosSubject theHS;
    LookupControl theLookupControl;
    SolveEmptyFamilyThread sef = new SolveEmptyFamilyThread();
    SolveEmptyHCISThread solveEmpHCIS = new SolveEmptyHCISThread();
    SolveHNPatternThread solveHNPat = new SolveHNPatternThread();
    SolveDeprecatedVNThread solveDepVN = new SolveDeprecatedVNThread();
    SolveDepNameSNamePID solveDepPID = new SolveDepNameSNamePID();
    private UpdateStatus theUS;
    private int button_family;
    public static final String SQL_CHECK_PATIENT_NO_FAMILY = " select t_patient.* from t_patient "
            + " left join t_health_family on t_health_family.t_health_family_id = t_patient.t_health_family_id"
            + " where t_health_family.t_health_family_id is null";
    public static final String SQL_COUNT_PATIENT_NO_FAMILY = "select count(*) from ("
            + " select t_patient.patient_hn,t_patient.t_health_family_id from t_patient "
            + " left join t_health_family on t_health_family.t_health_family_id = t_patient.t_health_family_id"
            + " where t_health_family.t_health_family_id is null) q ";
    private HosControl hosControl;

    public SystemControl(ConnectionInf con, HosObject ho, HosDB hdb, HosSubject hs) {
        theConnectionInf = con;
        theHosDB = hdb;
        theHS = hs;
        theHO = ho;
    }

    public boolean saveNews(String[] value) {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHO.date_time = this.theLookupControl.intReadDateTime();
            String sql = "insert into b_site_news values (?"
                    + ",'" + theHO.date_time
                    + "','" + theHO.theEmployee.getObjectId()
                    + "',?,?)";
            PreparedStatement ps = theConnectionInf.getConnection().prepareStatement(sql);
            ps.setString(1, value[0] + theHO.date_time);
            ps.setString(2, value[0] + theHO.date_time);
            ps.setString(3, value[1]);
            ps.executeUpdate();
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            return true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            return false;
        } finally {
            theConnectionInf.close();
        }
    }

    public boolean deleteNews(String id) {
        if (!theUS.confirmBox(ResourceBundle.getBundleText("com.hosv3.control.SystemControl.CONFIRM.DELETE.NEWS"), UpdateStatus.WARNING)) {
            return false;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String sql = "delete from b_site_news where id like '" + id + "'";
            theConnectionInf.eUpdate(sql);
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INACTIVE);
            return true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INACTIVE);
            return false;
        } finally {
            theConnectionInf.close();
        }
    }

    public Vector listNews(String text) {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            //henbe comment 100253 kong �����÷������Ӥ������� sql �����������ǡ������١�����ѡ��ù��
            String key[] = text.split(" ");
            String sqllookup = "select id,topic as des,text||' '||topic as text from b_site_news";
            String sql = "select * from (" + sqllookup + ") as query where text ilike '%" + Gutil.CheckReservedWords(key[0]) + "%'";
            for (int i = 1; key.length > 1 && i < key.length; i++) {
                sql += " and text ilike '%" + Gutil.CheckReservedWords(key[i]) + "%' ";

            }
            sql += "order by des limit 50 ";
            ResultSet rs = theConnectionInf.eQuery(sql);
            Vector vData = new Vector();
            while (rs.next()) {
                String[] data = new String[]{
                    rs.getString(1),
                    rs.getString(2),
                    rs.getString(3)};
                vData.add(data);
            }
            theConnectionInf.getConnection().commit();
            return vData;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            return null;
        } finally {
            theConnectionInf.close();
        }
    }

    public void setDepControl(LookupControl lc) {
        theLookupControl = lc;
        XPersistent x = new XPersistent();
        x.setHospitalSiteId(theLookupControl.readSite().off_id);
    }

    public void setUpdateStatus(UpdateStatus us) {
        theUS = us;
    }

    public void setWardLogin(Object selectedItem) {
        theHO.theWard = (Ward) selectedItem;
    }

    /**
     * @param theUS
     * @return
     * @author henbe ��ѭ�� VN ���������������¹���Ţ VN ����ش����
     *
     */
    public int solveEmptyHCIS(UpdateStatus theUS) {
        try {
            if (!theUS.confirmBox(ResourceBundle.getBundleGlobal("TEXT.CONFIRM")
                    + ResourceBundle.getBundleText("com.hosv3.control.SystemControl.RESOLVE.EMPTY.HCIS"), UpdateStatus.WARNING)) {
                return 0;
            }
            if (solveEmpHCIS.isAlive()) {
                theUS.setStatus(ResourceBundle.getBundleGlobal("TEXT.PROCESSING"), UpdateStatus.WARNING);
                return 0;
            } else {
                solveEmpHCIS = new SolveEmptyHCISThread();
                solveEmpHCIS.setControl(theConnectionInf, theHosDB, theHO, theUS, null);
                solveEmpHCIS.setHosControl(hosControl);
            }

            solveEmpHCIS.start();

            return 1;
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.UNKONW, ResourceBundle.getBundleText("com.hosv3.control.SystemControl.RESOLVE.EMPTY.HCIS"));
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            return 0;
        }
    }

    /**
     * @param theUS
     * @return
     * @author henbe ��ѭ�� VN ���������������¹���Ţ VN ����ش����
     */
    public int solveDeprecatedVN(UpdateStatus theUS) {
        try {
            if (!theUS.confirmBox(ResourceBundle.getBundleGlobal("TEXT.CONFIRM")
                    + ResourceBundle.getBundleText("com.hosv3.control.SystemControl.RESOLVE.DUPLICATE.VN"), UpdateStatus.WARNING)) {
                return 0;
            }
            if (solveDepVN.isAlive()) {
                theUS.setStatus((ResourceBundle.getBundleGlobal("TEXT.PROCESSING")), UpdateStatus.WARNING);
                return 0;
            } else {
                solveDepVN = new SolveDeprecatedVNThread();
                solveDepVN.setControl(theConnectionInf, theHosDB, theHO, theUS, null);
                solveDepVN.setHosControl(hosControl);
            }
            solveDepVN.start();

            return 1;
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.UNKONW, ResourceBundle.getBundleText("com.hosv3.control.SystemControl.RESOLVE.DUPLICATE.VN"));
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            return 0;
        }
    }

    /**
     * @author henbe ��ѭ�� �����š�õ��������Ţ��Ъҡ�
     */
    public void solveEmptyFamily() {
        try {
            if (!theUS.confirmBox(ResourceBundle.getBundleGlobal("TEXT.CONFIRM")
                    + ResourceBundle.getBundleText("com.hosv3.control.SystemControl.RESOLVE.EMPTY.PERSON"), UpdateStatus.WARNING)) {
                return;
            }
            if (sef.isAlive()) {
                theUS.setStatus((ResourceBundle.getBundleGlobal("TEXT.PROCESSING")), UpdateStatus.WARNING);
                return;
            } else {
                sef = new SolveEmptyFamilyThread();
                sef.setControl(theConnectionInf, theHosDB, theHO, theUS, null);
                sef.setHosControl(hosControl);
                sef.setTotalNumber(button_family);
            }
            sef.start();

        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.UNKONW, ResourceBundle.getBundleText("com.hosv3.control.SystemControl.RESOLVE.EMPTY.PERSON"));
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
        }
    }

    /**
     * @param theUS
     * @return
     * @author henbe ��ѭ�� �����š�õ��������Ţ��Ъҡ�
     */
    public int solveHNPattern(UpdateStatus theUS) {
        try {
            if (!theUS.confirmBox(ResourceBundle.getBundleGlobal("TEXT.CONFIRM")
                    + ResourceBundle.getBundleText("com.hosv3.control.SystemControl.RESOLVE.HN.PATTERN"), UpdateStatus.WARNING)) {
                return 0;
            }
            if (solveHNPat.isAlive()) {
                theUS.setStatus(ResourceBundle.getBundleGlobal("TEXT.PROCESSING"), UpdateStatus.WARNING);
                return 0;
            } else {
                solveHNPat = new SolveHNPatternThread();
                solveHNPat.setControl(theConnectionInf, theHosDB, theHO, theUS, theLookupControl);
                solveHNPat.setHosControl(hosControl);
            }
            solveHNPat.start();
            return 1;
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.UNKONW, ResourceBundle.getBundleText("com.hosv3.control.SystemControl.RESOLVE.HN.PATTERN"));
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            return 0;
        }
    }

    /**
     * @param theUS
     * @return
     * @author henbe ��ѭ�� VN ���������������¹���Ţ VN ����ش����
     */
    public int solveDepNameSNamePID(UpdateStatus theUS) {
        try {
            if (!theUS.confirmBox(ResourceBundle.getBundleGlobal("TEXT.CONFIRM")
                    + ResourceBundle.getBundleText("com.hosv3.control.SystemControl.RESOLVE.DUPLICATE.PERSON"), UpdateStatus.WARNING)) {
                return 0;
            }
            if (solveDepPID.isAlive()) {
                theUS.setStatus((ResourceBundle.getBundleGlobal("TEXT.PROCESSING")), UpdateStatus.WARNING);
                return 0;
            } else {
                solveDepPID = new SolveDepNameSNamePID();
                solveDepPID.setControl(theConnectionInf, theHosDB, theHO, theUS, null);
                solveDepPID.setHosControl(hosControl);
            }
            solveDepPID.start();
            return 1;
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.UNKONW, ResourceBundle.getBundleText("com.hosv3.control.SystemControl.RESOLVE.DUPLICATE.PERSON"));
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            return 0;
        }
    }

    /*
     * @author Pongtorn (Henbe) @name
     * ��˹���������Ѻ�к������������������§�������
     */
    public void setSoundEnabled(boolean enable) {
        theHO.is_sound_enabled = enable;
    }

    /*
     * @author Pongtorn (Henbe) @name ��ҹ��ҵ���âͧ�к������������§�������
     */
    public boolean isSoundEnabled() {
        return theHO.is_sound_enabled;
    }

    /**
     * login to setup ui
     *
     * @param uname
     * @param passwd
     * @param sp
     * @return
     */
    public boolean loginSetup(String uname, char[] passwd, Object sp) {
        boolean ret = false;
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ret = intLogin(uname, passwd, sp);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            theHS.theSystemSubject.notifyLoginSetup(ResourceBundle.getBundleGlobal("TEXT.LOGON")
                    + " "
                    + ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theSystemSubject.notifyLoginSetup(ResourceBundle.getBundleGlobal("TEXT.LOGON")
                    + " "
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        }
        return ret;
    }

    /**
     * login to working ui
     *
     * @param uname
     * @param passwd
     * @param sp
     * @return
     */
    public boolean login(String uname, char[] passwd, Object sp) {
        boolean ret = false;
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ret = intLogin(uname, passwd, sp);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.confirmBox(ResourceBundle.getBundleText("com.hosv3.control.SystemControl.NOT.CONNECT.DB"), UpdateStatus.WARNING);
            System.exit(0);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theSystemSubject.notifyLogin(ResourceBundle.getBundleGlobal("TEXT.LOGON")
                    + " "
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        }
        return ret;
    }

    /**
     * ���� save ���� ��� logout ��лŴ��͡������
     */
    public void exitProgram() {
        // save befaore exit
        exitProgramWithCloseConnection();
    }

    public void restartProgram() {
        saveProgram();
    }

    /**
     * clear database connection before exit program
     */
    public void exitProgramWithCloseConnection() {
        saveProgram();
        try {
            if (!theConnectionInf.getConnection().isClosed()) {
                if (!theConnectionInf.getConnection().getAutoCommit()) {
                    theConnectionInf.getConnection().commit();
                }
                theConnectionInf.getConnection().close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(SystemControl.class.getName()).log(Level.SEVERE, null, ex);
        }
        theHS.theSystemSubject.notifyExit("", 0);
        System.exit(0);
    }

    /**
     * clear visit lcoking and save infomation befaore exit program
     */
    public void saveProgram() {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String date_time = theLookupControl.intReadDateTime();
            if (theHO.theVisit != null) {
                this.hosControl.theVisitControl.intSaveTransferThrow(theHO.theEmployee, date_time);
                if (theHO.theVisit.lock_user.equals(theHO.theEmployee.getObjectId())) {
                    if (theHO.theVisit != null) {
                        theHO.theVisit.locking = "0";
                        theHosDB.theVisitDB.updateLocking(theHO.theVisit);
                    }
                    if (theHO.theListTransfer != null) {
                        theHO.theListTransfer.locking = "0";
                        theHosDB.theQueueTransferDB.updateLock(theHO.theListTransfer);
                    }
                }
            }
            theHO.theEmployee.last_logout = date_time;
            theHosDB.theEmployeeDB.updateLogout(theHO.theEmployee);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        theHS.theSystemSubject.notifyExit("", 0);
    }

    private boolean intLogin(String uname, char[] passwd, Object sp)
            throws Exception {
        boolean ret;// = false;
        String date_time = theLookupControl.intReadDateTime();
        Employee e = null;
        if (uname == null && passwd == null && sp == null) {
            //�����ҹ���ҹ����礨ҡ˹�� login ��������ͧ���ա�����¡�� checkUser();
            e = theHO.theEmployee;
        }
        if (uname != null && uname.isEmpty()) {
            //�����ҹ���ҹ����礨ҡ˹�� login ��������ͧ���ա�����¡�� checkUser();
            e = theHO.theEmployee;
        }
        if (e == null) {
            e = theHosDB.theEmployeeDB.selectByUsername(uname);

        }
        if (sp == null) {
            sp = theHO.theServicePoint != null
                    ? theHO.theServicePoint
                    : (ServicePoint) theHosDB.theServicePointDB.selectByPK(e.default_service_id);

        }
        ret = true;
        e.last_login = date_time;
        theHosDB.theEmployeeDB.updateLogout(e);
        theHO.theServicePoint = (ServicePoint) sp;
        theHO.theEmployee = theHosDB.theEmployeeDB.selectByPK(e.getObjectId());
        Person person = theHosDB.thePersonDB.select(theHO.theEmployee.t_person_id);
        if (person != null) {
            theHO.theEmployee.person = person;
        }
        Vector v = theHosDB.theGActionAuthDB.selectByAid(e.authentication_id);
        theHO.theGActionAuthV = new GActionAuthV(v);
        return ret;
    }

    public Employee checkUser(String uname, char[] passwd, Object sp) {
        Employee e = this.readEmpByUser(uname);
        if (e != null) {
            theHO.theEmployee = e;
            theHO.theServicePoint = (ServicePoint) sp;
            String pass = CryptPassword.encryptText(String.valueOf(passwd));
            if (e.password.equals(pass)) {
                return e;
            } else {
                return null;

            }
        }
        return null;
    }

    public Version getDbVersion() {
        Version theVersion = null;
        Vector vc;// = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theVersionDB.selectAllVersion();
            if (vc != null) {
                theVersion = (Version) vc.get(0);
                theVersion.db_code = theVersion.db_code.substring(0, theVersion.db_code.lastIndexOf('.'));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theVersion;
    }

    public Version getAppVersion() {
        Version version = new Version();
        version.app_code = Constant.getTextBundleConfig("APP_CODE");
        version.db_code = Constant.getTextBundleConfig("DB_CODE");
        version.db_code = version.db_code.substring(0, version.db_code.lastIndexOf('.'));
        return version;
    }

    /**
     * check database version is match program version?
     *
     * @param frm
     * @param show
     * @param setup_mode
     * @return
     */
    public boolean checkVersion(javax.swing.JFrame frm, boolean show, boolean setup_mode) {
        Version app_version = getAppVersion();
        Version db_version = getDbVersion();
        if (!db_version.db_code.equals(app_version.db_code) || show) {
            javax.swing.JOptionPane.showMessageDialog(frm, MessageFormat.format(ResourceBundle.getBundleText("com.hosv3.control.SystemControl.CHECK.DB.VERSION"),
                    app_version.app_code, app_version.db_code, db_version.db_code), ResourceBundle.getBundleText("com.hosv3.control.SystemControl.DB.CONNECTION") + ResourceBundle.getBundleGlobal("TEXT.FAIL"), javax.swing.JOptionPane.OK_OPTION);
            return false;
        }
        LOG.info("*****************************************");
        LOG.log(Level.INFO, " Program Version : {0}", app_version.app_code);
        LOG.log(Level.INFO, " Match Database Version: {0}", app_version.db_code);
        LOG.log(Level.INFO, " Database Version: {0}", db_version.db_code);
        LOG.log(Level.INFO, " Match Program Version: {0}", db_version.app_code);
        LOG.log(Level.INFO, " Update Time      : {0}", db_version.update_time);
        LOG.info("*****************************************");
        app_version.update_time = db_version.update_time;
        theHO.theVersion = app_version;
        return true;
    }

    /**
     * check database structure is correct?
     *
     * @param frm
     * @return
     */
    public boolean checkAlert(JFrame frm) {
        int row = checkFamilyButton();
        if (row > 0) {
            javax.swing.JOptionPane.showMessageDialog(frm,
                    MessageFormat.format(ResourceBundle.getBundleText("com.hosv3.control.SystemControl.MSG.RESOLVE.EMPTY.PERSON"), row),
                    ResourceBundle.getBundleText("com.hosv3.control.SystemControl.CHECK.DB"), javax.swing.JOptionPane.OK_OPTION);
        }
        row = checkHCISButton();
        if (row > 0) {
            javax.swing.JOptionPane.showMessageDialog(frm,
                    MessageFormat.format(ResourceBundle.getBundleText("com.hosv3.control.SystemControl.MSG.RESOLVE.HCIS"), row),
                    ResourceBundle.getBundleText("com.hosv3.control.SystemControl.CHECK.DB"), javax.swing.JOptionPane.OK_OPTION);
        }
        row = checkHCISNumber();
        if (row > 0) {
            javax.swing.JOptionPane.showMessageDialog(frm,
                    ResourceBundle.getBundleText("com.hosv3.control.SystemControl.MSG.RESOLVE.DUPLICATE.HCIS"),
                    ResourceBundle.getBundleText("com.hosv3.control.SystemControl.CHECK.DB"), javax.swing.JOptionPane.OK_OPTION);
        }
        return true;
    }
    public StringBuffer updateResult = new StringBuffer();

    /**
     * ��Ѻ��ا�ҹ�����Ţͧ�������ѡ�ͧ hos ��觨л�ҡ������ login
     * �����͹䢴ѧ���仹�� admin �����Է������¹�ç���ҧ�ҹ����������ҹ��
     * user ��������Է������¹�ç���ҧ�ҹ������ ��ͧ�ѹ��� delete ���� drop
     * ���������ö����
     * �������¹�ŧ�ç���ҧ�ҹ�����Ũ��ռŵ�͡������¹�ŧ�����ѹ�ͧ����ͧ�١����
     *
     * @param frm
     * @param show
     * @param spl
     * @return
     */
    public boolean setVersion(javax.swing.JFrame frm, boolean show, Splash spl) {
        try {
            theConnectionInf.open();
            AbsVersionControl3 cvheal = new CheckVersionHealthy(theHosDB);
            AbsVersionControl3 cvpcu = new CheckVersionPcu(theHosDB);
            AbsVersionControl3 cvhos = new CheckVersionHos(theHosDB);
            AbsVersionControl3 cvreport = new CheckVersionReport(theConnectionInf);
            boolean update_version = false;
            boolean hos_complete = cvhos.getCurrentVersion().isEmpty();
            Vector vfile = new Vector();
            AbsVersionControl.getHeaderUpdate(updateResult, "");
            if (!cvheal.isVersionCorrect() && !hos_complete) {
                Vector v = cvheal.getFileUpdateV();
                for (int i = 0; i < v.size(); i++) {
                    vfile.add(v.get(i));
                }
                AbsVersionControl.executeSQL(theConnectionInf, v, updateResult);
                if (!v.isEmpty()) {
                    update_version = true;
                }
            }
            if (!cvpcu.isVersionCorrect() && !hos_complete) {
                Vector v = cvpcu.getFileUpdate(cvpcu.getCurrentVersion());
                for (int i = 0; i < v.size(); i++) {
                    vfile.add(v.get(i));
                }
                AbsVersionControl.executeSQL(theConnectionInf, v, updateResult);
                if (!v.isEmpty()) {
                    update_version = true;
                }
            }
            if (!cvreport.isVersionCorrect() && !hos_complete) {
                Vector v = cvreport.getFileUpdateV();
                for (int i = 0; i < v.size(); i++) {
                    vfile.add(v.get(i));
                }
                AbsVersionControl.executeSQL(theConnectionInf, v, updateResult);
                if (!v.isEmpty()) {
                    update_version = true;
                }
            }
            if (!cvhos.isVersionCorrect()) {
                Vector v = cvhos.getFileUpdate(cvhos.getCurrentVersion());
                for (int i = 0; i < v.size(); i++) {
                    vfile.add(v.get(i));
                }
                AbsVersionControl.executeSQL(theConnectionInf, v, updateResult);
                if (!v.isEmpty()) {
                    update_version = true;
                }
            }
            if (!update_version) {
                return true;
            }
            //�������ͧ update schema database �è� update ����
            boolean update_schema = false;
            if (cvhos.isSchemaUpdate(cvhos.getCurrentVersion())
                    || cvpcu.isSchemaUpdate(cvpcu.getCurrentVersion())
                    || cvreport.isSchemaUpdate(cvreport.getCurrentVersion())
                    || cvheal.isSchemaUpdate(cvheal.getCurrentVersion())) {
                update_schema = true;
            }
            String warning = MessageFormat.format(ResourceBundle.getBundleText("com.hosv3.control.SystemControl.MSG.CONFIRM.UPDATE.DB"),
                    cvhos.getWarningMessage(), cvpcu.getWarningMessage(), cvreport.getWarningMessage());
            String warning2 = MessageFormat.format(ResourceBundle.getBundleText("com.hosv3.control.SystemControl.MSG.UPDATE.DB"),
                    cvhos.getWarningMessage(), cvpcu.getWarningMessage(), cvreport.getWarningMessage());
            return updateScriptFile2(theConnectionInf, spl, vfile, update_schema, frm, warning + updateResult.toString(), warning2, theHO.theEmployee.authentication_id, true);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            return false;
        }
    }

    private int checkFamilyButton() {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            //��Ǩ�ͺ��Ъҡë�Өҡ�Ţ�ѵû�ЪҪ�
            String sql = SQL_COUNT_PATIENT_NO_FAMILY;
            ResultSet rs = theConnectionInf.eQuery(sql);
            button_family = 0;
            if (rs.next()) {
                button_family = rs.getInt(1);

            }
            theConnectionInf.getConnection().commit();
            return button_family;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            return 0;
        } finally {
            theConnectionInf.close();
        }
    }

    private int checkHCISButton() {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            //��Ǩ�ͺ��Ъҡë�Өҡ�Ţ�ѵû�ЪҪ�
            String sql = "select count(*) from t_health_family where (health_family_hn_hcis = ''"
                    + "    or health_family_hn_hcis is null "
                    + //                    "    or length(t_health_family.health_family_hn_hcis) <>6 " +
                    //����͡������ҵ͹ migrate �ӹǹ�����������ѡ���ͧ��Ǩ�ͺ�͹�͡��§ҹ��Ҷ١��ͧ��������
                    "   or t_health_family.health_family_hn_hcis='000000')";
            ResultSet rs = theConnectionInf.eQuery(sql);
            int num_row = 0;
            if (rs.next()) {
                num_row = rs.getInt(1);

            }
            theConnectionInf.getConnection().commit();
            return num_row;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            return 0;
        } finally {
            theConnectionInf.close();
        }
    }

    private int checkHCISNumber() {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            //pu: ��Ǩ�ͺ��Ъҡë�Өҡ�Ţ hn_hcis ¡��ѹ hn_hcis �繤����ҧ �������͹
            String sql = "select count(t_health_family.health_family_hn_hcis) "
                    + " from t_health_family "
                    + " where t_health_family.health_family_hn_hcis  <> '' "
                    + " group by t_health_family.health_family_hn_hcis "
                    + " having count(t_health_family.health_family_hn_hcis)  > 1 ";
            ResultSet rs = theConnectionInf.eQuery(sql);
            int num_row = 0;
            if (rs.next()) {
                num_row = rs.getInt(1);

            }
            theConnectionInf.getConnection().commit();
            return num_row;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            return 0;
        } finally {
            theConnectionInf.close();
        }
    }

    @SuppressWarnings("NestedAssignment")
    public static boolean executeSQLFile(ConnectionInf con, String filename, boolean change_schema) throws Exception {
        PrintStream fos = null;
        con.open();
        try {
            con.getConnection().setAutoCommit(false);
            Statement stmt;
            int ret = 0, count = 0;
            fos = new PrintStream(new FileOutputStream("updatedb.log", true));
            fos.println("\n\npublic void executeSQLFile " + filename);

            stmt = con.getConnection().createStatement();
            FileInputStream fis = new FileInputStream(filename);
            InputStreamReader isr = new InputStreamReader(fis, "UTF8");
            BufferedReader in = new BufferedReader(isr);
            String str;// = "";
            String exe = "";

            while ((str = in.readLine()) != null) {
                if (str.length() <= 0) {
                    continue;
                }
                // Check BOM
                int i_mob = (int) str.charAt(0);
                if (i_mob == 65279) {
                    str = str.substring(1);
                }
                if (str.startsWith("--")) {
                    continue;
                }
                exe = exe + " " + str;
                if (str.lastIndexOf(';') != -1) {
                    if (exe.toLowerCase().trim().startsWith("drop table t_") && !change_schema) {
                        throw new FileNotFoundException(filename + " must not remove data");
                    }
                    if (exe.toLowerCase().trim().startsWith("delete from t_") && !change_schema) {
                        throw new FileNotFoundException(filename + " must not remove data");
                    }
                    if (exe.toLowerCase().trim().startsWith("copy")) {
                        throw new FileNotFoundException(filename + " please execute with command line");
                    }
                    if (exe.length() > 5 && exe.substring(0, 5).compareToIgnoreCase("alter") == 0) {
                        LOG.info(exe);
                    }
                    ret += stmt.executeUpdate(exe);

                    exe = "";
                    count++;
                }
            }
            con.getConnection().commit();
        } catch (Exception ex) {
            Logger.getLogger(SystemControl.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            try {
                con.getConnection().rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(SystemControl.class.getName()).log(java.util.logging.Level.SEVERE, null, ex1);
            }
            throw ex;
        } finally {
            con.close();
            if (fos != null) {
                fos.close();
            }
        }
        return true;
    }

    public static Connection getConnectionFromFile() {
        String config = IOStream.readInputDefault(".hospital_os.cfg");
        try {
            String tmp, value;
            //�ʹ���ʵç���
            String conf = Secure.decode(config);
//            tmp = conf.substring(0, conf.indexOf('\n'));
            conf = conf.substring(conf.indexOf('\n') + 1);
//            value = tmp.substring(tmp.indexOf('=') + 1);
            tmp = conf.substring(0, conf.indexOf('\n'));
            conf = conf.substring(conf.indexOf('\n') + 1);
            value = tmp.substring(tmp.indexOf('=') + 1);
            String server = value.trim();

            tmp = conf.substring(0, conf.indexOf('\n'));
            conf = conf.substring(conf.indexOf('\n') + 1);
            value = tmp.substring(tmp.indexOf('=') + 1);
            //settings.put("DATABASE", value.trim());
            String database = value.trim();

//            tmp = conf.substring(0, conf.indexOf('\n'));
            conf = conf.substring(conf.indexOf('\n') + 1);
//            value = tmp.substring(tmp.indexOf('=') + 1);
            //settings.put("PORT", value.trim());

            tmp = conf.substring(0, conf.indexOf('\n'));
            conf = conf.substring(conf.indexOf('\n') + 1);
            value = tmp.substring(tmp.indexOf('=') + 1);
            //settings.put("USERNAME", value.trim());
            String user = value.trim();

            tmp = conf.substring(0, conf.indexOf('\n'));
//            conf = conf.substring(conf.indexOf('\n') + 1);
            value = tmp.substring(tmp.indexOf('=') + 1);
            //settings.put("PASSWORD", value.trim());
            String pass = value.trim();

//            tmp = conf.substring(0, conf.indexOf('\n'));
//            conf = conf.substring(conf.indexOf('\n') + 1);
//            value = tmp.substring(tmp.indexOf('=') + 1);
            String url = "jdbc:postgresql://" + server + ":5432/" + database;
//            String type = "0"; //0 postgres 1 mysql 2 sqlserver
            DriverManager.registerDriver(new org.postgresql.Driver());
            Connection con = DriverManager.getConnection(url, user, pass);
            return con;
        } catch (Exception ex) {
            Logger.getLogger(SystemControl.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            return null;
        }
    }

    public Employee readEmpByUser(String user) {
        Employee emp = null;
        theConnectionInf.open();
        try {

            theConnectionInf.getConnection().setAutoCommit(false);
            ResultSet rs = theLookupControl.theConnectionInf.eQuery(
                    " select b_employee_id,employee_login,employee_password,f_employee_authentication_id,b_service_point_id"
                    + " from b_employee"
                    + " where employee_login = '" + user + "' and employee_active = '1'");
            while (rs.next()) {
                emp = new Employee();
                emp.setObjectId(rs.getString(1));
                emp.employee_id = rs.getString(2);
                emp.password = rs.getString(3);
                emp.authentication_id = rs.getString(4);
                emp.default_service_id = rs.getString(5);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return emp;
    }

    public static boolean updateScriptFile2(ConnectionInf theConnectionInf,
            Splash spl, Vector vfile, boolean update_schema,
            JFrame frm, String warning, String warning2,
            String authen, boolean isCore) {
        if (isCore) {
            JOptionPane.showMessageDialog(frm, new javax.swing.JTextArea((warning2 == null
                    || warning2.isEmpty() ? ResourceBundle.getBundleText("com.hosv3.control.SystemControl.CONTACT.ADMIN.FOR.UPDATE.DB") : warning2) + (authen.equals(Authentication.ADMIN) ? "\n\n" + warning : "")));
            return false;
        } else {
            return updateScriptFile2(theConnectionInf, spl, vfile, update_schema, frm, warning, warning2, authen);
        }
    }

    /**
     * ��Ǩ�ͺ��û�Ѻ��ا�ҹ�����ŵ�ͧ�����¼������к���ҹ��
     *
     * @param theConnectionInf
     * @param spl
     * @param vfile
     * @param update_schema
     * @param frm
     * @param warning
     * @param warning2
     * @param authen
     * @return
     */
    @SuppressWarnings("SleepWhileInLoop")
    public static boolean updateScriptFile2(ConnectionInf theConnectionInf,
            Splash spl, Vector vfile, boolean update_schema,
            JFrame frm, String warning, String warning2,
            String authen) {
        // 210513 Somprasong comment disable auto update database by program(user manual only)
        if (!authen.equals(Authentication.ADMIN)) {
            JOptionPane.showMessageDialog(frm, new javax.swing.JTextArea(warning2 == null
                    || warning2.isEmpty() ? ResourceBundle.getBundleText("com.hosv3.control.SystemControl.CONTACT.ADMIN.FOR.UPDATE.DB") : warning2));
            return false;
        }
        String title = update_schema ? ResourceBundle.getBundleText("com.hosv3.control.SystemControl.UPDATE.DB2") : ResourceBundle.getBundleText("com.hosv3.control.SystemControl.UPDATE.DB1");
        int ret = JOptionPane.showConfirmDialog(frm, new javax.swing.JTextArea(warning), title, javax.swing.JOptionPane.YES_NO_OPTION);
        boolean returnResult = false;
        if (ret == JOptionPane.YES_OPTION) {
            if (spl != null) {
                spl.setVisible(true);

            }
            try {
                for (int i = 0; i < vfile.size(); i++) {
                    String filename = (String) vfile.get(i);
                    executeSQLFile(theConnectionInf, filename, update_schema);
                    Thread.sleep(2000);
                }
                returnResult = true;
            } catch (Exception ex) {
                Logger.getLogger(SystemControl.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                if (spl != null) {
                    spl.setVisible(false);
                }
                PrintStream fos;
                try {
                    fos = new PrintStream(new FileOutputStream("updatedb.log", true));
                    fos.println(ex.toString());
                    fos.close();
                } catch (Exception ex1) {
                    Logger.getLogger(SystemControl.class.getName()).log(Level.SEVERE, null, ex1);
                }
                JOptionPane.showMessageDialog(null, ResourceBundle.getBundleText("com.hosv3.control.SystemControl.MSG.UPDATE.DB.FAIL"));
            } finally {
                if (spl != null && spl.isVisible()) {
                    spl.setVisible(false);

                }
            }
        }
        return returnResult;
    }

    public List<InsuranceClaim> listInsuranceClaimAlertRepay() {
        List<InsuranceClaim> list = new ArrayList<InsuranceClaim>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            list.addAll(theHosDB.theInsuranceClaimDB.listAlertWithCurrentDate());
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public int updateInsuranceClaimAlertRepay(List<InsuranceClaim> insuranceClaims) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (InsuranceClaim ic : insuranceClaims) {
                ic.update_datetime = theLookupControl.intReadDateTime();
                ic.user_update_id = theHO.theEmployee.getObjectId();
                theHosDB.theInsuranceClaimDB.updateAlert(ic);
            }
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public void setHosControl(HosControl hosControl) {
        this.hosControl = hosControl;
    }

    public Employee getEmployeeById(String empId) {
        Employee employee = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            employee = theHosDB.theEmployeeDB.selectByPK(empId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return employee;
    }

    private final Map<String, Boolean> cacheCheckWarningInformIntolerance = new HashMap<String, Boolean>();

    public boolean isShowWarningInformIntolerance(String authId) {
        Boolean isShow = cacheCheckWarningInformIntolerance.get(authId);
        if (isShow != null) {
            return isShow;
        }

        isShow = false;

        if ("1".equals(hosControl.theLookupControl.readOption(false).alert_inform_intolerance)) {
            List<MapWarningInformIntolerance> list
                    = hosControl.theLookupControl.listMapWarningInformIntolerance(false);
            for (MapWarningInformIntolerance obj : list) {
                if (obj.f_employee_authentication_id.equals(authId)) {
                    isShow = true;
                    break;
                }
            }
        }
        cacheCheckWarningInformIntolerance.put(authId, isShow);
        return isShow;
    }

    private final Map<String, Boolean> cacheCheckWarningDrugAllergy = new HashMap<String, Boolean>();

    public boolean isShowWarningDrugAllergy(String authId) {
        Boolean isShow = cacheCheckWarningDrugAllergy.get(authId);
        if (isShow != null) {
            return isShow;
        }

        isShow = false;

        if ("1".equals(hosControl.theLookupControl.readOption(false).alert_drug_allergy)) {
            List<MapWarningDrugAllergy> list
                    = hosControl.theLookupControl.listMapWarningDrugAllergy(false);
            for (MapWarningDrugAllergy obj : list) {
                if (obj.f_employee_authentication_id.equals(authId)) {
                    isShow = true;
                    break;
                }
            }
        }
        cacheCheckWarningDrugAllergy.put(authId, isShow);
        return isShow;
    }

    public ModuleLicense getModuleLicenseByModuleId(String moduleId) {
        ModuleLicense ml = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ml = this.theHosDB.theModuleLicenseDB.select(moduleId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ml;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.MapFinanceInsurance;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class MapFinanceInsuranceDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "817";

    public MapFinanceInsuranceDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public int insert(MapFinanceInsurance obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "INSERT INTO b_map_finance_insurance(\n"
                    + "               b_map_finance_insurance_id, b_contract_plans_id)\n"
                    + "       VALUES (?, ?)";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, obj.getGenID(tableId));
            preparedStatement.setString(2, obj.b_contract_plans_id);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(MapFinanceInsurance obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "UPDATE b_map_finance_insurance\n"
                    + "      SET b_contract_plans_id=?\n"
                    + "    WHERE b_map_finance_insurance_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, obj.b_contract_plans_id);
            preparedStatement.setString(2, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(MapFinanceInsurance obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "DELETE FROM b_map_finance_insurance \n"
                    + " WHERE b_map_finance_insurance_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public MapFinanceInsurance select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_map_finance_insurance "
                    + "where b_map_finance_insurance_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<MapFinanceInsurance> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public MapFinanceInsurance selectByContractPlanId(String b_contract_plans_id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_map_finance_insurance "
                    + "where b_map_finance_insurance.b_contract_plans_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, b_contract_plans_id);
            List<MapFinanceInsurance> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<MapFinanceInsurance> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<MapFinanceInsurance> list = new ArrayList<>();
        try (ResultSet rs = preparedStatement.executeQuery()) {
            while (rs.next()) {
                MapFinanceInsurance obj = new MapFinanceInsurance();
                obj.setObjectId(rs.getString("b_map_finance_insurance_id"));
                obj.b_contract_plans_id = rs.getString("b_contract_plans_id");
                try {
                    obj.description = rs.getString("contract_plans_description");
                } catch (SQLException e) {
                }
                list.add(obj);
            }
            return list;
        }
    }

    public MapFinanceInsurance selectAll() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_map_finance_insurance ";
            preparedStatement = connectionInf.ePQuery(sql);
            List<MapFinanceInsurance> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<Object[]> listMapByKeyword(String keyword) throws Exception {
        String sql = "select b_map_finance_insurance.b_map_finance_insurance_id "
                + ", b_contract_plans.contract_plans_description "
                + " from b_map_finance_insurance "
                + " inner join b_contract_plans on b_map_finance_insurance.b_contract_plans_id = b_contract_plans.b_contract_plans_id "
                + " where b_contract_plans.contract_plans_description ilike ? order by b_contract_plans.contract_plans_description ";
        try (PreparedStatement ePQuery = connectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, keyword == null || keyword.isEmpty() ? "%" : ("%" + keyword + "%"));
            List<Object[]> list = connectionInf.eComplexQuery(ePQuery.toString());
            return list;
        }
    }

    public List<Object[]> listNotMapByKeyword(String keyword) throws Exception {
        String sql = "select b_contract_plans.b_contract_plans_id"
                + ", b_contract_plans.contract_plans_description "
                + "from b_contract_plans "
                + "where b_contract_plans.contract_plans_active = '1' "
                + "and b_contract_plans.b_contract_plans_id not in (select b_contract_plans_id from b_map_finance_insurance) "
                + "and b_contract_plans.contract_plans_description ilike ? order by b_contract_plans.contract_plans_description";
        try (PreparedStatement ePQuery = connectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, keyword == null || keyword.isEmpty() ? "%" : ("%" + keyword + "%"));
            List<Object[]> list = connectionInf.eComplexQuery(ePQuery.toString());
            return list;
        }
    }

    public List<MapFinanceInsurance> listAll() throws Exception {
        String sql = "select b_map_finance_insurance.b_map_finance_insurance_id"
                + ", b_map_finance_insurance .b_contract_plans_id"
                + ", b_contract_plans.contract_plans_description"
                + " from b_map_finance_insurance "
                + " inner join b_contract_plans on b_map_finance_insurance.b_contract_plans_id = b_contract_plans.b_contract_plans_id "
                + " order by b_contract_plans.contract_plans_description ";
        try (PreparedStatement ePQuery = connectionInf.ePQuery(sql)) {
            List<MapFinanceInsurance> list = executeQuery(ePQuery);
            return list;
        }
    }
}

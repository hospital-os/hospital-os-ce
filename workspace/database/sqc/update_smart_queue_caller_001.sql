--Install Extension pgsql-http Database PostgreSQL->> https://github.com/pramsey/pgsql-http/
-- UNIX > git clone https://github.com/pramsey/pgsql-http/
-- Windows > http://www.postgresonline.com/journal/archives/371-http-extension.html

CREATE EXTENSION http;	

CREATE TABLE b_smart_queue_api (
	code text NOT NULL,	
	method text NULL,      
  uri text NULL,
  headers text NULL,     
  content_type text NULL,
  content text null,
  CONSTRAINT b_smart_queue_api_pkey PRIMARY KEY (code)
);

insert into b_smart_queue_api 
values ('queue','POST','http://' || host(inet_server_addr()) || ':88/queue/api/queue/his','{"x-access-token-name":"Queue-HIS", "x-access-token":"06366c3d-140c-4dd9-aecf-7eb83fbfff07"}','application/json; charset=utf-8',null);


CREATE TABLE b_smart_queue_service_point (
	queue_name text NOT NULL,	
	b_service_point_id text null,
    CONSTRAINT b_smart_queue_service_point_pkey PRIMARY KEY (queue_name,b_service_point_id)
);
insert into b_smart_queue_service_point 
values ('pharmacy','2068197173050'),('financial','2064218952165');

CREATE TABLE t_smart_queue_logs (
	t_visit_queue_transfer_id text,
	http_request json null,	
	http_response json null,
	_id text null,
	status text not null,
	error json null,
	logs_date_time timestamp without time zone NOT NULL default current_timestamp,
    CONSTRAINT t_smart_queue_logs_pkey PRIMARY KEY (t_visit_queue_transfer_id),
    CONSTRAINT t_smart_queue_logs_id_unique UNIQUE(_id)
);


CREATE OR REPLACE FUNCTION smart_queue_post(p_content text , p_t_visit_queue_transfer_id text)
RETURNS json AS $$
DECLARE
	response_queue json;
	err_message text;
	err_context text;
	
BEGIN
	BEGIN	
		select row_to_json(http((
			          method,
			           uri,
			           ARRAY[http_header('x-access-token-name',headers::json->>'x-access-token-name'),http_header('x-access-token',headers::json->>'x-access-token')],
			           content_type,
					   p_content
			        )::http_request)) into response_queue
		from b_smart_queue_api
		where code = 'queue';
    exception when others then
    	GET STACKED DIAGNOSTICS err_message = MESSAGE_TEXT
							,err_context = PG_EXCEPTION_CONTEXT;
   	 	INSERT INTO t_smart_queue_logs (http_request,error,status,t_visit_queue_transfer_id)
            values (p_content::json,(json_build_object('MESSAGE_TEXT',err_message,'PG_EXCEPTION_CONTEXT',err_context)),'error',p_t_visit_queue_transfer_id)
        ON CONFLICT (t_visit_queue_transfer_id) DO
        UPDATE SET http_request = p_content::json
        			,error = (json_build_object('MESSAGE_TEXT',err_message,'PG_EXCEPTION_CONTEXT',err_context))
        			,status = 'error'
							,logs_date_time = current_timestamp;
    return null;
   END;
   INSERT INTO t_smart_queue_logs (http_request,http_response,_id,status,t_visit_queue_transfer_id,error)
        values (p_content::json,response_queue::json,(response_queue::json->>'content')::json->>'_id'
						,(case when response_queue->>'status' = '200' then 'active' else 'error' end)
						,p_t_visit_queue_transfer_id
						,(case when response_queue->>'status' != '200' then (response_queue->>'content')::json end))
   ON CONFLICT (t_visit_queue_transfer_id) DO
        UPDATE SET http_request = p_content::json
        			,http_response = response_queue::json
        			,_id = (response_queue::json->>'content')::json->>'_id'
        			,status = (case when response_queue->>'status' = '200' then 'active' else 'error' end)
							,error = (case when response_queue->>'status' != '200' then (response_queue->>'content')::json end)
							,logs_date_time = current_timestamp;
   RETURN response_queue;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION smart_queue_delete( p_id text)
RETURNS json AS $$
DECLARE
	response_queue json;
	err_message text;
	err_context text;
	
BEGIN
	BEGIN	
		select row_to_json(http((
			          'DELETE',
			           uri||'/'||p_id,
			           ARRAY[http_header('x-access-token-name',headers::json->>'x-access-token-name'),http_header('x-access-token',headers::json->>'x-access-token')],
			           null,
					   null
			        )::http_request)) into response_queue
		from b_smart_queue_api
		where code = 'queue';
    exception when others then
    	GET STACKED DIAGNOSTICS err_message = MESSAGE_TEXT
							,err_context = PG_EXCEPTION_CONTEXT;
   	 	update t_smart_queue_logs set error = (json_build_object('MESSAGE_TEXT',err_message,'PG_EXCEPTION_CONTEXT',err_context)), status = 'error', logs_date_time = current_timestamp where _id = p_id;
    return null;
   END;
  	IF response_queue->>'status' = '200' THEN
   		update t_smart_queue_logs set http_response = response_queue,status = 'delete' ,logs_date_time = current_timestamp where _id = p_id;
    END IF;
   RETURN response_queue;
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION smart_queue_manage()
RETURNS trigger AS $$
DECLARE		
	active_id	text := (select _id
							from t_smart_queue_logs 
						where 
							status = 'active'
							and t_smart_queue_logs.t_visit_queue_transfer_id = NEW.t_visit_queue_transfer_id);
BEGIN
	IF (TG_OP = 'INSERT') THEN 	
		PERFORM smart_queue_data(NEW.t_visit_queue_transfer_id, 'waiting',null);
		RETURN NEW;
	ELSIF (TG_OP = 'UPDATE') THEN
		active_id = (select _id
						from t_smart_queue_logs 
						where 
							status = 'active'
							and t_smart_queue_logs.t_visit_queue_transfer_id = NEW.t_visit_queue_transfer_id);
						
		IF ((OLD.assign_date_time <> NEW.assign_date_time) 
			and (active_id is not null)) THEN
			
			PERFORM smart_queue_delete(active_id);	
		END IF;
	
		IF (OLD.assign_date_time <> NEW.assign_date_time) then
			PERFORM smart_queue_data(NEW.t_visit_queue_transfer_id, 'waiting',null);
	   	END IF;
	    RETURN NEW;
  	END IF;   
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER smart_queue_manage
  AFTER INSERT OR UPDATE ON t_visit_queue_transfer  
  FOR EACH ROW EXECUTE PROCEDURE smart_queue_manage();

CREATE OR REPLACE FUNCTION smart_queue_cancel()
RETURNS trigger AS $$
DECLARE		
	active_id text := (select _id
						from t_smart_queue_logs 
						where 
							status = 'active'
							and t_smart_queue_logs.t_visit_queue_transfer_id = OLD.t_visit_queue_transfer_id);
BEGIN
					
		IF (active_id is not null) THEN
			 PERFORM smart_queue_delete(active_id);	
		END IF;
		RETURN OLD; 
END;
$$ LANGUAGE plpgsql;
 
CREATE TRIGGER smart_queue_cancel
  AFTER DELETE ON t_visit_queue_transfer  
  FOR EACH ROW EXECUTE PROCEDURE smart_queue_cancel();


CREATE OR REPLACE FUNCTION smart_queue_data (p_t_visit_queue_transfer_id text , p_status text ,p_channel text)
RETURNS VOID AS $$
DECLARE				
BEGIN
	PERFORM
		smart_queue_post((json_build_object('queue-name',"queue-name"
		-- กรณีเลขคิวเป็น vn เปลี่ยน "queue-code" เป็น vn 
		,'queue-code',"queue-code" 
		--,'queue-code',vn 
		,'queue-datetime',"queue-datetime"
		,'visit',json_build_object('hn',hn,'vn',vn,'name',name)
		,'language',language
		,'channel',channel
		,'status',status
		,'hcode',hcode))::text
		,t_visit_queue_transfer_id::text)
	from (
	select
		b_smart_queue_service_point.queue_name as "queue-name"
		,case when b_visit_queue_setup.visit_queue_setup_number is null then '-'
      		else (b_visit_queue_setup.visit_queue_setup_number || '-' || LPAD(t_visit_queue_transfer.visit_queue_map_queue, 4, '0'))
      		end as "queue-code"
		,current_timestamp as "queue-datetime"
		,t_patient.patient_hn as hn
		,t_visit.visit_vn as vn
		,case when f_patient_prefix.f_patient_prefix_id != '000'
				then f_patient_prefix.patient_prefix_description||t_patient.patient_firstname||' '||t_patient.patient_lastname
				else t_patient.patient_firstname||' '||t_patient.patient_lastname end as name
		,case when t_patient.f_patient_nation_id = '99'
			then 'th' else 'en' end as language
		,p_channel as channel
		,p_status as status
		,t_visit_queue_transfer.t_visit_queue_transfer_id 
		,b_visit_office_id as hcode
	from t_visit_queue_transfer inner join t_visit on t_visit_queue_transfer.t_visit_id = t_visit.t_visit_id
		inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
		left join f_patient_prefix on t_patient.f_patient_prefix_id = f_patient_prefix.f_patient_prefix_id
		inner join b_smart_queue_service_point on t_visit_queue_transfer.b_service_point_id = b_smart_queue_service_point.b_service_point_id
		left join t_visit_queue_map on t_visit_queue_map.t_visit_id = t_visit.t_visit_id
		left join b_visit_queue_setup on b_visit_queue_setup.b_visit_queue_setup_id = t_visit_queue_map.b_visit_queue_setup_id
		cross join b_site
	where
		t_visit_queue_transfer.t_visit_queue_transfer_id = p_t_visit_queue_transfer_id
	) as q;
END;
$$ LANGUAGE plpgsql;
 
					

-- create s_smart_queue_caller
CREATE TABLE s_smart_queue_caller_version (
    version_id            varchar(255) NOT NULL,
    version_number            	varchar(255) NULL,
    version_description       	varchar(255) NULL,
    version_application_number	varchar(255) NULL,
    version_database_number   	varchar(255) NULL,
    version_update_time       	varchar(255) NULL 
);

ALTER TABLE s_smart_queue_caller_version
	ADD CONSTRAINT s_smart_queue_caller_version_pkey
	PRIMARY KEY (version_id);

INSERT INTO s_smart_queue_caller_version VALUES ('1', '1', 'Smart Queue Caller Module', '1.0.0', '1.0.0', (select current_date) || ','|| (select current_time));

INSERT INTO s_script_update_log values ('Smart_Queue_Caller_Module','update_smart_queue_caller_001.sql',(select current_date) || ','|| (select current_time),'Initialize Smart Queue Caller Module');




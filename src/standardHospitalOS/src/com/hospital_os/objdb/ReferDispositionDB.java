/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.ReferDisposition;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.ComboFix;
import java.sql.ResultSet;
import java.util.Vector;

/**
 *
 * @author Khanate
 */
public class ReferDispositionDB {

    public ConnectionInf theConnectionInf;
    public ReferDisposition dbObj;

    public ReferDispositionDB(ConnectionInf db) {

        theConnectionInf = db;
        dbObj = new ReferDisposition();
        initConfig();
    }

    private boolean initConfig() {
        dbObj.table = "f_referral_disposition";
        dbObj.pk_field = "f_referral_disposition_id";
        dbObj.disposition_description = "disposition_description";
        dbObj.disposition_value = "disposition_value";
        return true;
    }

    public Vector selectAll() throws Exception {
        Vector vc = new Vector();
        String sql = "select * from " + dbObj.table + " order by " + dbObj.pk_field;
        vc = veQuery(sql);

        if (vc.isEmpty()) {
            return null;
        } else {
            return vc;
        }
    }

    public Vector veQuery(String sql) throws Exception {
        ComboFix p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        ComboFix f = new ComboFix("0", "����к�");
        list.add(f);
        while (rs.next()) {

            p = new ComboFix();
            p.code = rs.getString(dbObj.pk_field);
            p.name = rs.getString(dbObj.disposition_description);
            list.add(p);
        }
        rs.close();
        return list;
    }
}

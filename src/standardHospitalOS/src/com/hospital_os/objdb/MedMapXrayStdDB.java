/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.MedMapXrayStd;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.ResultSet;
import java.util.Vector;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class MedMapXrayStdDB {

    private final ConnectionInf theConnectionInf;

    public MedMapXrayStdDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public int delete(MedMapXrayStd MedMapXrayStd) throws Exception {
        String sql = "delete from b_med_map_xray_std where b_med_map_xray_std_id = '%s'";
        return theConnectionInf.eUpdate(String.format(sql, MedMapXrayStd.getObjectId()));
    }

    public MedMapXrayStd selectById(String id) throws Exception {
        String sql = "select * from b_med_map_xray_std where b_med_map_xray_std_id = '%s'";
        Vector eQuery = eQuery(String.format(sql, id));
        return (MedMapXrayStd) (eQuery.isEmpty() ? null : eQuery.get(0));
    }

    public Vector<MedMapXrayStd> listAll() throws Exception {
        String sql = "select * from b_med_map_xray_std";
        Vector eQuery = eQuery(sql);
        return eQuery;
    }

    public Vector eQuery(String sql) throws Exception {
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        while (rs.next()) {
            MedMapXrayStd p = new MedMapXrayStd();
            p.setObjectId(rs.getString("b_med_map_xray_std_id"));
            p.b_item_id = rs.getString("b_item_id");
            p.b_med_xray_std_id = rs.getString("b_med_xray_std_id");
            list.add(p);
        }
        rs.close();
        return list;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.MapProductCategory;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class MapProductCategoryDB {

    public ConnectionInf connectionInf;
    final public String tableId = "855";

    public MapProductCategoryDB(ConnectionInf db) {
        connectionInf = db;
    }

    public int insert(MapProductCategory obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO b_map_product_category(\n"
                    + "            b_map_product_category_id, b_item_id, f_product_category_id)\n"
                    + "    VALUES (?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.getGenID(tableId));
            preparedStatement.setString(index++, obj.b_item_id);
            preparedStatement.setString(index++, obj.f_product_category_id);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM b_map_product_category\n");
            sql.append(" WHERE b_map_product_category_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, id);
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(String... ids) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "DELETE FROM b_map_product_category WHERE b_map_product_category_id in (%s)";
            StringBuilder sb = new StringBuilder();
            for (String id : ids) {
                sb.append("'").append(id).append("',");
            }
            preparedStatement = connectionInf.ePQuery(String.format(sql,
                    sb.toString().substring(0, sb.toString().length() - 1)));
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<MapProductCategory> list(String keyword, String grpId, String type) throws Exception {
        String sql = "select b_item.item_common_name\n"
                + ", f_product_category.product_category_description\n"
                + ", b_item.b_item_id\n"
                + ", f_product_category.f_product_category_id\n"
                + ", b_map_product_category.b_map_product_category_id\n"
                + "from\n"
                + "b_item\n"
                + "inner join b_item_subgroup on b_item.b_item_subgroup_id = b_item_subgroup.b_item_subgroup_id and b_item_subgroup.f_item_group_id in ('1','4')\n"
                + "left join b_map_product_category on b_item.b_item_id = b_map_product_category.b_item_id\n"
                + "left join f_product_category on f_product_category.f_product_category_id = b_map_product_category.f_product_category_id\n"
                + "\n"
                + "where b_item.item_active = '1'\n";
        if (keyword != null && !keyword.isEmpty()) {
            sql += "and (b_item.item_common_name ilike ? or b_item.item_trade_name ilike ? or b_item.item_nick_name ilike ?)\n";
        }
        if (grpId != null && !grpId.isEmpty()) {
            sql += "and b_item_subgroup.b_item_subgroup_id = ?\n";
        }
        if (type != null && !type.isEmpty()) {
            if (type.equals("1")) {
                sql += "and b_map_product_category.b_map_product_category_id is not null\n";
            } else if (type.equals("2")) {
                sql += "and b_map_product_category.b_map_product_category_id is null\n";
            }
        }
        sql += "order by b_item.item_common_name";
        PreparedStatement preparedStatement = connectionInf.ePQuery(sql.toString());
        int index = 1;
        if (keyword != null && !keyword.isEmpty()) {
            preparedStatement.setString(index++, "%" + keyword + "%");
            preparedStatement.setString(index++, "%" + keyword + "%");
            preparedStatement.setString(index++, "%" + keyword + "%");
        }
        if (grpId != null && !grpId.isEmpty()) {
            preparedStatement.setString(index++, grpId);
        }
        return executeQuery(preparedStatement);
    }

    public List<MapProductCategory> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<MapProductCategory> list = new ArrayList<MapProductCategory>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                MapProductCategory obj = new MapProductCategory();
                obj.setObjectId(rs.getString("b_map_product_category_id"));
                obj.f_product_category_id = rs.getString("f_product_category_id");
                obj.b_item_id = rs.getString("b_item_id");
                obj.item_name = rs.getString("item_common_name");
                obj.product_category_name = rs.getString("product_category_description");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }
}

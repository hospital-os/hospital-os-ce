package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;
import java.util.Vector;

/**
 * Modify sumo 10/08/2549
 *
 **/ 
/**
 * Modify sumo 10/08/2549
 *
 **/@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"}) 

public class GuideAfterDxTransaction extends Persistent {
    private static final long serialVersionUID = 1L;

    public String visit_id = "";
    public String guide = "";
    public String health_head = "";

    /**
     * @roseuid 3F658BBB036E
     */
    public GuideAfterDxTransaction() {
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (health_head==null || health_head.isEmpty()) {
            return sb.append(guide).toString();
        } else {
            return sb.append(health_head).append(": ").append(guide).toString();
        }
    }

    public static String toString(Vector vHealthEdu) {

        StringBuilder health_edu = new StringBuilder();
        if (vHealthEdu == null) {
            return health_edu.toString();
        }

        for (int i = 0, size = vHealthEdu.size(); i < size; i++) {
            GuideAfterDxTransaction gu = (GuideAfterDxTransaction) vHealthEdu.get(i);
            if (health_edu.toString().equals("")) {
                health_edu.append(gu.toString());
            } else {
                health_edu.append("\n").append(gu.toString());
            }
        }
        return health_edu.toString();
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.object;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author sompr
 */
public class FIcfCode extends Persistent implements CommonInf {

    public String description = "";

    @Override
    public String getCode() {
        return getObjectId();
    }

    @Override
    public String getName() {
        return getObjectId() + " : " + description;
    }

    @Override
    public String toString() {
        return getObjectId() + " : " + description;
    }
}

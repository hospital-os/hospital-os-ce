/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.utility;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

/**
 *
 * @author Somprasong
 */
public class HosStdDatasource {

    private final Map<String, Object> mapKeys = new HashMap<String, Object>();
    private List<Object> datasources = new ArrayList<Object>();

    public void addMapKey(String key, Object value) {
        this.getMapKeys().put(key, value);
    }

    public Object getMapValue(String key) {
        return this.getMapKeys().get(key);
    }

    public Map<String, Object> getMapKeys() {
        return mapKeys;
    }

    public List<Object> getDatasources() {
        return datasources;
    }

    public void setDatasources(List<Object> datasources) {
        this.datasources = datasources;
    }

    public int size() {
        return datasources.size();
    }

    public boolean isEmpty() {
        return datasources.isEmpty();
    }

    public boolean contains(Object o) {
        return datasources.contains(o);
    }

    public Iterator<Object> iterator() {
        return datasources.iterator();
    }

    public Object[] toArray() {
        return datasources.toArray();
    }

    public <T> T[] toArray(T[] a) {
        return datasources.toArray(a);
    }

    public boolean add(Object e) {
        return datasources.add(e);
    }

    public boolean remove(Object o) {
        return datasources.remove(o);
    }

    public boolean containsAll(Collection<?> c) {
        return datasources.containsAll(c);
    }

    public boolean addAll(Collection<? extends Object> c) {
        return datasources.addAll(c);
    }

    public boolean addAll(int index, Collection<? extends Object> c) {
        return datasources.addAll(index, c);
    }

    public boolean removeAll(Collection<?> c) {
        return datasources.removeAll(c);
    }

    public boolean retainAll(Collection<?> c) {
        return datasources.retainAll(c);
    }

    public void clear() {
        datasources.clear();
    }

    @Override
    @SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
    public boolean equals(Object o) {
        return datasources.equals(o);
    }

    @Override
    public int hashCode() {
        return datasources.hashCode();
    }

    public Object get(int index) {
        return datasources.get(index);
    }

    public Object set(int index, Object element) {
        return datasources.set(index, element);
    }

    public void add(int index, Object element) {
        datasources.add(index, element);
    }

    public Object remove(int index) {
        return datasources.remove(index);
    }

    public int indexOf(Object o) {
        return datasources.indexOf(o);
    }

    public int lastIndexOf(Object o) {
        return datasources.lastIndexOf(o);
    }

    public ListIterator<Object> listIterator() {
        return datasources.listIterator();
    }

    public ListIterator<Object> listIterator(int index) {
        return datasources.listIterator(index);
    }

    public List<Object> subList(int fromIndex, int toIndex) {
        return datasources.subList(fromIndex, toIndex);
    }
}

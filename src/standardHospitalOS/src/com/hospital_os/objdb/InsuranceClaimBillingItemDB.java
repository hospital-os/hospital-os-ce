/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.InsuranceClaimBillingItem;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class InsuranceClaimBillingItemDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "822";

    public InsuranceClaimBillingItemDB(ConnectionInf c) {
        this.connectionInf = c;
    }

    public int insert(InsuranceClaimBillingItem obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO t_insurance_claim_billing_item(\n"
                    + "            t_insurance_claim_billing_item_id, t_insurance_claim_billing_id, t_insurance_claim_id, total_payment, user_record_id, record_datetime)\n"
                    + "    VALUES (?, ?, ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getGenID(tableId));
            preparedStatement.setString(2, obj.t_insurance_claim_billing_id);
            preparedStatement.setString(3, obj.t_insurance_claim_id);
            preparedStatement.setDouble(4, obj.total_payment);
            preparedStatement.setString(5, obj.user_record_id);
            preparedStatement.setString(6, obj.record_datetime);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
    
    public InsuranceClaimBillingItem select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select t_insurance_claim_billing_item from t_insurance_claim_billing_item \n"
                    + "where t_insurance_claim_billing_item_id = ? ";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<InsuranceClaimBillingItem> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
    
    public List<InsuranceClaimBillingItem> listByClaimBillingId(String claimBillingId) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select t_insurance_claim_billing_item from t_insurance_claim_billing_item \n"
                    + "where t_insurance_claim_billing_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, claimBillingId);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
    
    public List<InsuranceClaimBillingItem> listByClaimId(String claimId) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select t_insurance_claim_billing_item from t_insurance_claim_billing_item \n"
                    + "where t_insurance_claim_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, claimId);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<InsuranceClaimBillingItem> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<InsuranceClaimBillingItem> list = new ArrayList<InsuranceClaimBillingItem>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                InsuranceClaimBillingItem obj = new InsuranceClaimBillingItem();
                obj.setObjectId(rs.getString("t_insurance_claim_billing_item_id"));
                obj.t_insurance_claim_billing_id = rs.getString("t_insurance_claim_billing_id");
                obj.t_insurance_claim_id = rs.getString("t_insurance_claim_id");
                obj.total_payment = rs.getDouble("total_payment");
                obj.user_record_id = rs.getString("user_record_id");
                obj.record_datetime = rs.getString("record_datetime");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }
}

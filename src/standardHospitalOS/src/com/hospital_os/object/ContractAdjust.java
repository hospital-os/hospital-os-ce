package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

@SuppressWarnings("ClassWithoutLogger")
public class ContractAdjust extends Persistent {
    private static final long serialVersionUID = 1L;

    public String contract_id;
    public String covered_id;
    public String adjustment;
    public String draw;

    /**
     * @roseuid 3F658BBB036E
     */
    public ContractAdjust() {
    }
}

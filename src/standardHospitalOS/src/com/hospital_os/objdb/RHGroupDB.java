package com.hospital_os.objdb;

import com.hospital_os.usecase.connection.*;
import com.hospital_os.utility.*;
import java.util.*;
import java.sql.*;
import com.hospital_os.object.RHGroup;

@SuppressWarnings("UseOfObsoleteCollectionType")
public class RHGroupDB {

    public ConnectionInf theConnectionInf;
    public RHGroup dbObj;
    final public String idtable = "257";

    /**
     * @param db
     * @roseuid 3F65897F0326
     */
    public RHGroupDB(ConnectionInf db) {
        theConnectionInf = db;
        dbObj = new RHGroup();
        initConfig();
    }

    private boolean initConfig() {
        dbObj.table = "f_rh_group";
        dbObj.pk_field = "f_rh_group_id";
        dbObj.description = "rh_group_description";
        return true;
    }

    /**
     * @param o
     * @return int
     * @throws Exception
     * @roseuid 3F6574DE0394
     */
    public int insert(RHGroup o) throws Exception {
        RHGroup p = o;
        p.generateOID(idtable);
        String sql = "insert into " + dbObj.table + " ("
                + dbObj.pk_field
                + " ," + dbObj.description
                + " ) values ('"
                + p.getObjectId()
                + "','" + p.description
                + "')";
        sql = Gutil.convertSQLToMySQL(sql, theConnectionInf.gettypeDatabase());
        return theConnectionInf.eUpdate(sql);
    }

    public int update(RHGroup o) throws Exception {
        String sql = "update " + dbObj.table + " set ";
        RHGroup p = o;
        String field = ""
                + "', " + dbObj.description + "='" + p.description
                + "' where " + dbObj.pk_field + "='" + p.getObjectId() + "'";
        sql = Gutil.convertSQLToMySQL(sql + field.substring(2), theConnectionInf.gettypeDatabase());
        return theConnectionInf.eUpdate(sql);
    }

    public int delete(RHGroup o) throws Exception {
        String sql = "delete from " + dbObj.table
                + " where " + dbObj.pk_field + "='" + o.getObjectId() + "'";
        return theConnectionInf.eUpdate(sql);
    }

    public RHGroup selectByPK(String pk) throws Exception {
        String sql = "select * from " + dbObj.table
                + " where " + dbObj.pk_field
                + " = '" + pk + "'";

        Vector v = eQuery(sql);
        if (v.isEmpty()) {
            return null;
        } else {
            return (RHGroup) v.get(0);
        }
    }

    public Vector selectAllCombofix() throws Exception {
        String sql = "select * from " + dbObj.table
                + " order by " + dbObj.description;
        Vector vc = eQuery(sql);
        if (vc.isEmpty()) {
            return null;
        } else {
            return vc;
        }
    }

    public Vector selectAll() throws Exception {
        String sql = "select * from " + dbObj.table
                + " order by " + dbObj.pk_field;
        Vector vc = eQuery(sql);
        if (vc.isEmpty()) {
            return null;
        } else {
            return vc;
        }
    }

    public Vector selectOld() throws Exception {
        String sql = "select * from " + dbObj.table
                + " order by " + dbObj.pk_field;
        Vector vc = eQuery(sql);
        if (vc.isEmpty()) {
            return null;
        } else {
            return vc;
        }
    }

    public Vector eQuery(String sql) throws Exception {
        RHGroup p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            p = new RHGroup();
            p.setObjectId(rs.getString(dbObj.pk_field));
            p.description = rs.getString(dbObj.description);
            list.add(p);
        }
        rs.close();
        return list;
    }
}

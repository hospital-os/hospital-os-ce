/*
 * Maim.java
 *
 * Created on 22 ����Ҿѹ�� 2549, 10:07 �.
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
package com.pcu.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Administrator
 */
@SuppressWarnings("ClassWithoutLogger")
public class Maim extends Persistent {

    private static final long serialVersionUID = 1L;
    private static String init = "";
    public String family_id = init;
    public String maim_id = init;
//    public String patient_id = init; drop in patch 3.9.30
//    public String visit_id = init; drop in patch 3.9.30
    public String maim_treat = "0";
    public String maim_registry = init;
    public String staff_record = init;
    public String staff_modify = init;
    public String staff_cancel = init;
    public String record_date_time = init;
    public String modify_date_time = init;
    public String cancel_date_time = init;
    public String active = "1";
    public String survey_date = init;
    public String description = init;
//    public String disability_id = init; drop in patch 3.9.30
    public String disability_cause = "0";
    public String b_icd10_id = init;
    
    public String health_maim_date = "";
}

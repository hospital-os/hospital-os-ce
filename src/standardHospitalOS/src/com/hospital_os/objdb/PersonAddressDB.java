/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.PersonAddress;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class PersonAddressDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "801";

    public PersonAddressDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public int insert(PersonAddress obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO t_person_address(\n");
            sql.append("           t_person_address_id, t_person_id, f_address_housetype_id, person_address_houseid, \n");
            sql.append("           person_address_roomno, person_address_building, person_address_house, \n");
            sql.append("           person_address_moo, person_address_villaname, person_address_soisub, \n");
            sql.append("           person_address_soimain, person_address_road, person_address_tambon, \n");
            sql.append("           person_address_amphur, person_address_changwat, person_address_telephone, \n");
            sql.append("           person_address_mobile, person_address_latitude, person_address_longitude, \n");
            sql.append("           person_address_other_country, person_address_other_country_active, \n");
            sql.append("           f_address_type_id, active, user_record_id, record_date_time, \n");
            sql.append("           user_modify_id, modify_date_time)\n");
            sql.append("   VALUES (?, ?, ?, ?, \n");
            sql.append("           ?, ?, ?, \n");
            sql.append("           ?, ?, ?, \n");
            sql.append("           ?, ?, ?, \n");
            sql.append("           ?, ?, ?, \n");
            sql.append("           ?, ?, ?, \n");
            sql.append("           ?, ?, \n");
            sql.append("           ?, ?, ?, ?, \n");
            sql.append("           ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getGenID(tableId));
            preparedStatement.setString(2, obj.t_person_id);
            preparedStatement.setString(3, obj.f_address_housetype_id);
            preparedStatement.setString(4, obj.person_address_houseid);
            preparedStatement.setString(5, obj.person_address_roomno);
            preparedStatement.setString(6, obj.person_address_building);
            preparedStatement.setString(7, obj.person_address_house);
            preparedStatement.setString(8, obj.person_address_moo);
            preparedStatement.setString(9, obj.person_address_villaname);
            preparedStatement.setString(10, obj.person_address_soisub);
            preparedStatement.setString(11, obj.person_address_soimain);
            preparedStatement.setString(12, obj.person_address_road);
            preparedStatement.setString(13, obj.person_address_tambon);
            preparedStatement.setString(14, obj.person_address_amphur);
            preparedStatement.setString(15, obj.person_address_changwat);
            preparedStatement.setString(16, obj.person_address_telephone);
            preparedStatement.setString(17, obj.person_address_mobile);
            preparedStatement.setDouble(18, obj.person_address_latitude);
            preparedStatement.setDouble(19, obj.person_address_longitude);
            preparedStatement.setString(20, obj.person_address_other_country);
            preparedStatement.setString(21, obj.person_address_other_country_active);
            preparedStatement.setString(22, obj.f_address_type_id);
            preparedStatement.setString(23, obj.active);
            preparedStatement.setString(24, obj.user_record_id);
            preparedStatement.setString(25, obj.record_date_time);
            preparedStatement.setString(26, obj.user_modify_id);
            preparedStatement.setString(27, obj.modify_date_time);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(PersonAddress obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_person_address\n");
            sql.append("  SET t_person_id=?, f_address_housetype_id=?, \n");
            sql.append("      person_address_houseid=?, person_address_roomno=?, person_address_building=?, \n");
            sql.append("      person_address_house=?, person_address_moo=?, person_address_villaname=?, \n");
            sql.append("      person_address_soisub=?, person_address_soimain=?, person_address_road=?, \n");
            sql.append("      person_address_tambon=?, person_address_amphur=?, person_address_changwat=?, \n");
            sql.append("      person_address_telephone=?, person_address_mobile=?, person_address_latitude=?, \n");
            sql.append("      person_address_longitude=?, person_address_other_country=?, person_address_other_country_active=?, \n");
            sql.append("      f_address_type_id=?, active=?, \n");
            sql.append("      user_modify_id=?, modify_date_time=?\n");
            sql.append(" WHERE t_person_address_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.t_person_id);
            preparedStatement.setString(2, obj.f_address_housetype_id);
            preparedStatement.setString(3, obj.person_address_houseid);
            preparedStatement.setString(4, obj.person_address_roomno);
            preparedStatement.setString(5, obj.person_address_building);
            preparedStatement.setString(6, obj.person_address_house);
            preparedStatement.setString(7, obj.person_address_moo);
            preparedStatement.setString(8, obj.person_address_villaname);
            preparedStatement.setString(9, obj.person_address_soisub);
            preparedStatement.setString(10, obj.person_address_soimain);
            preparedStatement.setString(11, obj.person_address_road);
            preparedStatement.setString(12, obj.person_address_tambon);
            preparedStatement.setString(13, obj.person_address_amphur);
            preparedStatement.setString(14, obj.person_address_changwat);
            preparedStatement.setString(15, obj.person_address_telephone);
            preparedStatement.setString(16, obj.person_address_mobile);
            preparedStatement.setDouble(17, obj.person_address_latitude);
            preparedStatement.setDouble(18, obj.person_address_longitude);
            preparedStatement.setString(19, obj.person_address_other_country);
            preparedStatement.setString(20, obj.person_address_other_country_active);
            preparedStatement.setString(21, obj.f_address_type_id);
            preparedStatement.setString(22, obj.active);
            preparedStatement.setString(23, obj.user_modify_id);
            preparedStatement.setString(24, obj.modify_date_time);
            preparedStatement.setString(25, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int inactive(PersonAddress obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_person_address\n");
            sql.append("  SET active=?, user_modify_id=?, modify_date_time=?\n");
            sql.append(" WHERE t_person_address_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, "0");
            preparedStatement.setString(2, obj.user_modify_id);
            preparedStatement.setString(3, obj.modify_date_time);
            preparedStatement.setString(4, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "delete from t_person_address where t_person_address_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            // execute delete SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public PersonAddress select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from t_person_address where t_person_address_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<PersonAddress> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<PersonAddress> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<PersonAddress> list = new ArrayList<PersonAddress>();
        ResultSet rs = null;
        try {
            // execute insert SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                PersonAddress obj = new PersonAddress();
                obj.setObjectId(rs.getString("t_person_address_id"));
                obj.t_person_id = rs.getString("t_person_id");
                obj.f_address_housetype_id = rs.getString("f_address_housetype_id");
                obj.person_address_houseid = rs.getString("person_address_houseid");
                obj.person_address_roomno = rs.getString("person_address_roomno");
                obj.person_address_building = rs.getString("person_address_building");
                obj.person_address_house = rs.getString("person_address_house");
                obj.person_address_moo = rs.getString("person_address_moo");
                obj.person_address_villaname = rs.getString("person_address_villaname");
                obj.person_address_soisub = rs.getString("person_address_soisub");
                obj.person_address_soimain = rs.getString("person_address_soimain");
                obj.person_address_road = rs.getString("person_address_road");
                obj.person_address_tambon = rs.getString("person_address_tambon");
                obj.person_address_amphur = rs.getString("person_address_amphur");
                obj.person_address_changwat = rs.getString("person_address_changwat");
                obj.person_address_telephone = rs.getString("person_address_telephone");
                obj.person_address_mobile = rs.getString("person_address_mobile");
                obj.person_address_latitude = rs.getDouble("person_address_latitude");
                obj.person_address_longitude = rs.getDouble("person_address_longitude");
                obj.person_address_other_country = rs.getString("person_address_other_country");
                obj.person_address_other_country_active = rs.getString("person_address_other_country_active");
                obj.f_address_type_id = rs.getString("f_address_type_id");
                obj.active = rs.getString("active");
                obj.user_record_id = rs.getString("user_record_id");
                obj.record_date_time = rs.getString("record_date_time");
                obj.user_modify_id = rs.getString("user_modify_id");
                obj.modify_date_time = rs.getString("modify_date_time");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }
}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;
import java.util.Date;

/**
 *
 * @author Somprasong
 */
public class BUserApikey extends Persistent {

    public String b_employee_id = "";
    public String app_name = "";
    public String api_key = "";
    public Date created_at;
    public String created_by = "";
}

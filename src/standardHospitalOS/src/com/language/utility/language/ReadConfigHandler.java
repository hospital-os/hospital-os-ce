/*
 * ReadConfigHandler.java
 *
 * Created on 28 �Զع�¹ 2548, 13:16 �.
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
package com.language.utility.language;

import java.util.Properties;
import org.xml.sax.*;
import org.xml.sax.helpers.*;

/**
 *
 * @author tong(Padungrat)
 */
@SuppressWarnings("ClassWithoutLogger")
public class ReadConfigHandler extends DefaultHandler {

    String dataValue = null;
    String dataString = null;
    String oldDes = null;
    Properties data;

    /**
     * ��ҹ������ҷ���˹�
     */
    public ReadConfigHandler() {
        data = new Properties();
    }

    @Override
    public void startElement(String namespaceUri, String localName,
            String qualifiedName, Attributes attribut) throws SAXException {
        oldDes = "";
        dataString = "";
    }

    @Override
    public void endElement(String namespaceUri,
            String localName,
            String qualifiedName)
            throws SAXException {

        if (qualifiedName.equals("language-key")) {
            dataValue = dataString;
        } else if (qualifiedName.equals("language-des")) {
            this.data.setProperty(dataValue, dataString);
        }
    }

    @Override
    public void characters(char[] chars,
            int startIndex,
            int length) {
        oldDes = dataString;
        dataString = new String(chars, startIndex, length).trim();
        dataString = oldDes + dataString;
    }

    public Properties getData() {
        return data;
    }
}

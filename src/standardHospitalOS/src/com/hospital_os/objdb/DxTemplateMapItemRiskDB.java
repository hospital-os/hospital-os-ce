/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.DxTemplateMapItemRisk;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tanakrit
 */
public class DxTemplateMapItemRiskDB {

    public ConnectionInf theConnectionInf;
    final public String idtable = "858";

    public DxTemplateMapItemRiskDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public int insert(DxTemplateMapItemRisk p) throws Exception {
        String sql = "INSERT INTO b_template_dx_map_item_risk( \n"
                + "               b_template_dx_map_item_risk_id, b_template_dx_id, b_item_id, user_record_id) \n"
                + "        VALUES(?, ?, ?, ?)";
        try (PreparedStatement ps = theConnectionInf.ePQuery(sql)) {
            p.generateOID(idtable);
            int index = 1;
            ps.setString(index++, p.getObjectId());
            ps.setString(index++, p.b_template_dx_id);
            ps.setString(index++, p.b_item_id);
            ps.setString(index++, p.user_record_id);
            return ps.executeUpdate();
        }
    }

    public int update(DxTemplateMapItemRisk p) throws Exception {
        String sql = "UPDATE b_template_dx_map_item_risk \n"
                + "      SET b_template_dx_id = ?,b_item_id = ?,user_record_id = ?"
                + "    WHERE b_template_dx_map_item_risk_id = ?";
        try (PreparedStatement ps = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ps.setString(index++, p.b_template_dx_id);
            ps.setString(index++, p.b_item_id);
            ps.setString(index++, p.user_record_id);
            ps.setString(index++, p.getObjectId());
            return ps.executeUpdate();
        }
    }

    public int delete(DxTemplateMapItemRisk p) throws Exception {
        String sql = "Delete FROM b_template_dx_map_item_risk \n"
                + "    WHERE b_template_dx_map_item_risk_id = ?";
        try (PreparedStatement ps = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ps.setString(index++, p.getObjectId());
            return ps.executeUpdate();
        }
    }

    public int deleteItemDxByDxTemplate(String pk) throws Exception {
        String sql = "delete from b_template_dx_map_item_risk"
                + " where b_template_dx_map_item_risk_id=?";
        try (PreparedStatement ps = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ps.setString(index++, pk);
            return ps.executeUpdate();
        }
    }

    public List<DxTemplateMapItemRisk> selectByDxid(String pk) throws Exception {
        String sql = "select b_template_dx_map_item_risk.* from b_template_dx_map_item_risk \n"
                + "inner join b_item on b_template_dx_map_item_risk.b_item_id = b_item.b_item_id and b_item.item_active = '1'\n"
                + "where b_template_dx_map_item_risk.b_template_dx_id = ?";
        try (PreparedStatement ps = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ps.setString(index++, pk);
            return executeQuery(ps);
        }
    }

    public List<DxTemplateMapItemRisk> specialQueryItemDx(String pk) throws Exception {
        String sql = "select b_template_dx_map_item_risk.b_template_dx_map_item_risk_id as dx_map_item_risk_id "
                + "         ,b_template_dx_map_item_risk.b_template_dx_id as template_dx "
                + "         ,b_template_dx_map_item_risk.b_item_id as item_id "
                + "         ,b_item.item_number as code "
                + "         ,b_item.item_common_name as name "
                + "     from b_template_dx_map_item_risk "
                + "inner join b_item on b_template_dx_map_item_risk.b_item_id = b_item.b_item_id "
                + "     where b_template_dx_map_item_risk.b_template_dx_id = ?";
        try (PreparedStatement ps = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ps.setString(index++, pk);
            List<DxTemplateMapItemRisk> v = sEQuery(ps);
            if (v.isEmpty()) {
                return null;
            } else {
                return v;
            }
        }
    }

    public List<DxTemplateMapItemRisk> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<DxTemplateMapItemRisk> list = new ArrayList<>();
        try (ResultSet rs = preparedStatement.executeQuery()) {
            while (rs.next()) {
                DxTemplateMapItemRisk p = new DxTemplateMapItemRisk();
                p.setObjectId(rs.getString("b_template_dx_map_item_risk_id"));
                p.b_template_dx_id = rs.getString("b_template_dx_id");
                p.b_item_id = rs.getString("b_item_id");
                p.user_record_id = rs.getString("user_record_id");
                list.add(p);
            }
            return list;
        }
    }

    public List<DxTemplateMapItemRisk> sEQuery(PreparedStatement preparedStatement) throws Exception {
        List<DxTemplateMapItemRisk> list = new ArrayList<>();
        try (ResultSet rs = preparedStatement.executeQuery()) {
            while (rs.next()) {
                DxTemplateMapItemRisk p = new DxTemplateMapItemRisk();
                p.setObjectId(rs.getString("dx_map_item_risk_id"));
                p.b_template_dx_id = rs.getString("template_dx");
                p.b_item_id = rs.getString("item_id");
                p.code = rs.getString("code");
                p.description = rs.getString("name");
                list.add(p);
            }
        }
        return list;
    }
}

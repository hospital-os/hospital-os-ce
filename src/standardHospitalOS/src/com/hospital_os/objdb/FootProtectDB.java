/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.FootProtect;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class FootProtectDB {

    public ConnectionInf theConnectionInf;
    final public String idtable = "992";

    public FootProtectDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public int insert(FootProtect o) throws Exception {
        o.generateOID(idtable);
        String sql = "INSERT INTO t_foot_protect( "
                + "t_foot_protect_id, t_foot_result_id, f_foot_protect_type_id,  "
                + "record_date_time, user_record_id) "
                + "VALUES ('%s', '%s', '%s',  "
                + "'%s', '%s')";
        Object[] values = new Object[]{
            o.getObjectId(), o.t_foot_result_id, o.f_foot_protect_type_id,
            o.record_date_time, o.user_record_id};
        return theConnectionInf.eUpdate(String.format(sql, values));
    }

    public int delete(FootProtect o) throws Exception {
        String sql = "DELETE FROM t_foot_protect "
                + "WHERE t_foot_protect_id='%s'";
        Object[] values = new Object[]{o.getObjectId()};
        return theConnectionInf.eUpdate(String.format(sql, values));
    }

    public List<FootProtect> listByFootResultId(String id) throws Exception {
        String sql = "select * from t_foot_protect where t_foot_result_id = '%s'";
        return eQuery(String.format(sql, id));
    }

    private List<FootProtect> eQuery(String sql) throws Exception {
        List<FootProtect> list = new ArrayList<FootProtect>();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        while (rs.next()) {
            FootProtect o = new FootProtect();
            o.setObjectId(rs.getString("t_foot_protect_id"));
            o.t_foot_result_id = rs.getString("t_foot_result_id");
            o.f_foot_protect_type_id = rs.getString("f_foot_protect_type_id");
            o.record_date_time = rs.getString("record_date_time");
            o.user_record_id = rs.getString("user_record_id");
            list.add(o);
        }
        rs.close();
        return list;
    }
}

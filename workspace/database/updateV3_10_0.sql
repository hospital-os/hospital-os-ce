-- isses#673
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS public.t_telehealth (
	id uuid DEFAULT uuid_generate_v4(),
	t_patient_id varchar(30) NOT NULL,
	t_patient_appointment_id varchar(30) NULL,
	t_visit_id varchar(30) NULL,
	conference_link text NOT NULL,
	user_record_id varchar(30) NOT NULL,
	record_date_time timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
	CONSTRAINT t_telehealth_pk PRIMARY KEY (id)
);

ALTER TABLE t_patient_appointment ADD COLUMN patient_appointment_telehealth VARCHAR(1) NOT NULL DEFAULT '0';

-- issues#695
INSERT INTO b_option_detail (b_option_detail_id,option_detail_name,option_detail_note)
VALUES ('enable_api_payment_gateway','0','')
	,('cancel_receipt_payment_gateway','','')
ON CONFLICT (b_option_detail_id)
DO UPDATE SET option_detail_name = EXCLUDED.option_detail_name
			,option_detail_note = EXCLUDED.option_detail_note;

CREATE TABLE IF NOT EXISTS f_api_payment_status (
	id 				INTEGER NOT NULL,
	description		CHARACTER VARYING(255) NOT NULL,
    CONSTRAINT f_api_payment_status_pk PRIMARY KEY (id)
);

INSERT INTO f_api_payment_status (id, description)
VALUES (1, 'Pending') --  กำลังดำเนินการ
	  ,(2, 'Completed') -- รับชำระสำเร็จ
	  ,(3, 'Declined') -- ยกเลิกการรับชำระ
	  ,(4, 'Failed') -- เชื่อมต่อผิดพลาด
ON CONFLICT (id)
DO UPDATE SET description = EXCLUDED.description;

ALTER TABLE t_billing ADD COLUMN IF NOT EXISTS f_api_payment_status_id INTEGER NOT NULL DEFAULT 0;
ALTER TABLE t_billing ADD COLUMN IF NOT EXISTS api_payment_transaction_datetime TIMESTAMP WITH TIME ZONE DEFAULT NULL;
ALTER TABLE t_billing ADD COLUMN IF NOT EXISTS api_payment_note TEXT DEFAULT NULL;

-- เพิ่มวิธีการรับชำระเงิน  
INSERT INTO f_payment_type (f_payment_type_id, description, require_bank_info, require_account_name, require_account_no, limit_account_number
, is_auto_check_bank, require_card_type, require_payment_date, lbl_bank_info, lbl_account_name, lbl_account_no, lbl_card_type, lbl_payment_date)
VALUES ('6','ชำระเงินออนไลน์','0','0','0',0,'0','0','0','','','','','')
ON CONFLICT (f_payment_type_id)
DO UPDATE SET description=EXCLUDED.description
	, require_bank_info=EXCLUDED.require_bank_info
	, require_account_name=EXCLUDED.require_account_name
	, require_account_no=EXCLUDED.require_account_no
	, limit_account_number=EXCLUDED.limit_account_number
	, is_auto_check_bank=EXCLUDED.is_auto_check_bank
	, require_card_type=EXCLUDED.require_card_type
	, require_payment_date=EXCLUDED.require_payment_date
	, lbl_bank_info=EXCLUDED.lbl_bank_info
	, lbl_account_name=EXCLUDED.lbl_account_name
	, lbl_account_no=EXCLUDED.lbl_account_no
	, lbl_card_type=EXCLUDED.lbl_card_type
	, lbl_payment_date=EXCLUDED.lbl_payment_date;  


-- issues#696
CREATE TABLE IF NOT EXISTS b_item_picture (
    b_item_picture_id		serial4 NOT NULL,
    b_item_id 				VARCHAR(50) NOT NULL,
    item_picture			BYTEA NOT NULL,
    user_record_id			VARCHAR(50) NOT NULL,
    record_datetime			TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT b_item_picture_pk PRIMARY KEY (b_item_picture_id),
    CONSTRAINT b_item_picture_unique1 UNIQUE (b_item_id)
);

-- issues#700
ALTER TABLE b_visit_clinic ADD IF NOT EXISTS use_doctor_schedule VARCHAR(1) NOT NULL DEFAULT '0';

ALTER TABLE public.b_restful_url ADD IF NOT EXISTS use_basic_auth bool NOT NULL DEFAULT false;
ALTER TABLE public.b_restful_url ADD IF NOT EXISTS basic_auth_username varchar NULL;
ALTER TABLE public.b_restful_url ADD IF NOT EXISTS basic_auth_password varchar NULL; 

-- update db version
INSERT INTO s_version VALUES ('9701000000114', '114', 'Hospital OS, Community Edition', '3.10.0', '3.53.0', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_10_0.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.10.0');

-- require when use other postgres user
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA public to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA thai_ascvd to hospitalos;
-- GRANT ALL ON all sequences in schema thai_ascvd to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA rp50 to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA rp50nhso to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA rp50data to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA health_insurance to hospitalos;
-- GRANT ALL ON all sequences in schema public to hospitalos; 
-- GRANT ALL ON all sequences in schema rp50 to hospitalos; 
-- GRANT ALL ON all sequences in schema rp50nhso to hospitalos; 
-- GRANT ALL ON all sequences in schema rp50data to hospitalos; 
-- GRANT ALL ON all sequences in schema health_insurance to hospitalos;
-- GRANT ALL ON all sequences in schema thai_ascvd to hospitalos;
-- GRANT USAGE ON SCHEMA public TO hospitalos;
-- GRANT USAGE ON SCHEMA rp50 TO hospitalos;
-- GRANT USAGE ON SCHEMA rp50nhso TO hospitalos;
-- GRANT USAGE ON SCHEMA rp50data TO hospitalos;
-- GRANT USAGE ON SCHEMA health_insurance TO hospitalos;
-- GRANT USAGE ON SCHEMA thai_ascvd TO hospitalos;
/*
 * LabReferControl.java
 *
 * Created on 22 ����Ҥ� 2547, 09:34 �.
 */
package com.hosv3.control;

import com.hospital_os.object.*;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hosv3.object.HosObject;
import com.hosv3.object.LookupObject;
import com.hosv3.object.UseCase;
import com.hosv3.subject.HosSubject;
import com.hosv3.utility.Constant;
import com.hosv3.utility.ResourceBundle;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.Vector;
import java.util.logging.Logger;

/**
 *
 * @author Amp
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class LabReferControl {

    private static final Logger LOG = Logger.getLogger(BillingControl.class.getName());
    ConnectionInf theConnectionInf;
    UpdateStatus theUS;
    HosDB theHosDB;
    HosObject theHO;
    HosSubject theHS;
    LookupObject theLO;
    LookupControl theLookupControl;
    String theStatus;
    SystemControl theSystemControl;
    private HosControl hosControl;

    /**
     * Creates a new instance of BillingControl
     */
    public LabReferControl(ConnectionInf c, HosObject ho, HosDB hdb, HosSubject hs, LookupObject lo) {
        theConnectionInf = c;
        theHosDB = hdb;
        theHO = ho;
        theHS = hs;
        theLO = lo;
    }

    public void setSystemControl(SystemControl systemControl) {
        theSystemControl = systemControl;
    }
    
    public void setHosControl(HosControl hosControl) {
        this.hosControl = hosControl;
    }

    public void setDepControl(LookupControl lc) {
        theLookupControl = lc;
    }

    public void setUpdateStatus(UpdateStatus us) {
        theUS = us;
    }

    /**
     * ��Ǩ�ͺ������� pid �������¤����㹰ҹ���������������ѧ
     * �Ѵ�͡�����������´ uc ���� 27/11/47
     */
    public boolean checkPidPatientLabreferinWithPatient(String pid) {
        boolean result = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector answer = theHosDB.thePatientDB.queryPid(pid);
            if (answer == null || answer.isEmpty()) {
                result = false;
            } else {
                result = true;
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    public boolean checkPidPatientLabreferinWithPatientLabreferin(String pid) {
        boolean result = true;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector answer1;
            answer1 = theHosDB.thePatientLabreferinDB.queryPid(pid);
            if (answer1 == null || answer1.isEmpty()) {
                result = false;
            } else {
                result = true;
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    /**
     * ���������ż����·���ա���� labreferin �Ѵ�͡�����������´ uc ����
     * 27/11/47
     */
    /**
     * @Author: sumo
     * @date: 25/08/2549
     * @see: �ѹ�֡������ LabReferIn
     * @param: Object PatientLabreferin,Object UpdateStatus
     * @return: boolean
     */
    public boolean savePatientLabreferin(PatientLabreferin thePatientLabreferin, UpdateStatus theUS) {
        // ������Ǩ�ͺ trim �ͧ������й��ʡ�� sumo 25/08/2549
        if ((thePatientLabreferin.fname.trim().isEmpty()) || (thePatientLabreferin.lname.trim().isEmpty())) {
            theUS.setStatus(ResourceBundle.getBundleGlobal("VALIDATE.FULLNAME") + ResourceBundle.getBundleGlobal("TEXT.PATIENT"), UpdateStatus.WARNING);
            return false;
        }
        // ������Ǩ�ͺ�����Ţ��ЪҪ��ѵá�͡���ú sumo 25/08/2549
        if (thePatientLabreferin.pid.trim().length() > 0 && thePatientLabreferin.pid.trim().length() < 13) {
            theUS.setStatus(ResourceBundle.getBundleGlobal("VALIDATE.CID.LENGTH"), UpdateStatus.WARNING);
            return false;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (!thePatientLabreferin.pid.isEmpty()) {
                Vector pidv = theHosDB.thePatientDB.queryPid(thePatientLabreferin.pid);
                String hn_pid_many = "";
                for (int i = 0, size = pidv.size(); i < size; i++) {
                    Patient pt_pidv = (Patient) pidv.get(i);
                    hn_pid_many = hn_pid_many + " " + pt_pidv.hn;
                    //���Ţ�ѵû�ЪҪ� ����ѧ����բ����ż����¤���� ��ͧ���Ţ�ѵû�ЪҪ�
                }
                //���Ţ�ѵû�ЪҪ� ����ѧ����բ����ż����¤���� ��ͧ���Ţ�ѵû�ЪҪ�
                if (thePatientLabreferin.getObjectId() == null && !pidv.isEmpty()) {
                    theUS.setStatus(MessageFormat.format(ResourceBundle.getBundleText("com.hosv3.control.LabReferControl.DUP.CID.WITH.OTHER.HN"), 
                            hn_pid_many), UpdateStatus.WARNING);
                    return false;
                }
                //���Ţ�ѵû�ЪҪ� �ͧ��������ҷ���ӡѹ�ҡ���� 1 ��
                if (thePatientLabreferin.getObjectId() != null && pidv.size() > 1) {
                    theUS.setStatus(MessageFormat.format(ResourceBundle.getBundleText("com.hosv3.control.LabReferControl.DUP.CID.WITH.OTHER.HN"), 
                            hn_pid_many), UpdateStatus.WARNING);
                    return false;
                }
                //���Ţ�ѵû�ЪҪ� �ͧ����������Ţ�����������
                if (thePatientLabreferin.getObjectId() != null && !pidv.isEmpty()) {
                    Patient pt_pidv = (Patient) pidv.get(0);
                    if (!pt_pidv.getObjectId().equals(thePatientLabreferin.getObjectId())) {
                        theUS.setStatus(MessageFormat.format(ResourceBundle.getBundleText("com.hosv3.control.LabReferControl.DUP.CID.WITH.OTHER.HN"), 
                            hn_pid_many), UpdateStatus.WARNING);
                        return false;
                    }
                }
                Vector plridv = theHosDB.thePatientLabreferinDB.queryPid(thePatientLabreferin.pid);
                //���Ţ�ѵû�ЪҪ� ����ѧ����բ����ż����¤���� ��ͧ���Ţ�ѵû�ЪҪ�
                if (thePatientLabreferin.getObjectId() == null && !plridv.isEmpty()) {
                    theUS.setStatus(ResourceBundle.getBundleGlobal("VALIDATE.CID.DUPLICATE"), UpdateStatus.WARNING);
                    return false;
                }
                //���Ţ�ѵû�ЪҪ� �ͧ��������ҷ���ӡѹ�ҡ���� 1 ��
                if (thePatientLabreferin.getObjectId() != null && plridv.size() > 1) {
                    theUS.setStatus(ResourceBundle.getBundleGlobal("VALIDATE.CID.DUPLICATE"), UpdateStatus.WARNING);
                    return false;
                }
                //���Ţ�ѵû�ЪҪ� �ͧ����������Ţ�����������
                if (thePatientLabreferin.getObjectId() != null && !plridv.isEmpty()) {
                    PatientLabreferin pt_plridv = (PatientLabreferin) plridv.get(0);
                    if (!pt_plridv.getObjectId().equals(thePatientLabreferin.getObjectId())) {
                        theUS.setStatus(ResourceBundle.getBundleGlobal("VALIDATE.CID.DUPLICATE"), UpdateStatus.WARNING);
                        return false;
                    }
                }
                int result1 = Constant.isCorrectPID(thePatientLabreferin.pid);
                if (thePatientLabreferin.pid.length() == 13 && result1 != 1) { //incorrect pid standard
                    if (!theUS.confirmBox(ResourceBundle.getBundleText("com.hosv3.control.LabReferControl.CONFIRM.SAVE.INCORECT.CID"), UpdateStatus.WARNING)) {
                        return false;
                    }
                }
            }
            Prefix pfx = theLookupControl.readPrefixById(thePatientLabreferin.prefix_id);
            if (pfx != null
                    && (thePatientLabreferin.sex == null ? pfx.sex != null : !thePatientLabreferin.sex.equals(pfx.sex))
                    && (pfx.tlock == null ? Active.isEnable() == null : pfx.tlock.equals(Active.isEnable()))) {
                theUS.setStatus(ResourceBundle.getBundleGlobal("VALIDATE.PREFIX.WITH.SEX"), UpdateStatus.WARNING);
                return false;
            }
            if (thePatientLabreferin.getObjectId() == null) {
                theHosDB.thePatientLabreferinDB.insert(thePatientLabreferin);
            } else {
                theHosDB.thePatientLabreferinDB.update(thePatientLabreferin);
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
            return true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            return false;
            //theHS.theLabReferSubject.notifysavePatientLabreferin(null,"Found Error");
        } finally {
            theConnectionInf.close();
        }
    }

    /**
     * ���Ҽ����¨ҡ�������͹��ʡ�� �Ѵ�͡�����������´ uc ���� 27/11/47
     */
    public Vector listPatientLabreferinByName(String fname, String lname) {
        Vector result = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if ((!fname.isEmpty()) && (!lname.isEmpty())) {
                result = theHosDB.thePatientLabreferinDB.queryByFLName("%" + fname + "%", "%" + lname + "%");
            } else if (!fname.isEmpty()) {
                result = theHosDB.thePatientLabreferinDB.queryByFName("%" + fname + "%");
            } else if (!lname.isEmpty()) {
                result = theHosDB.thePatientLabreferinDB.queryBySName("%" + lname + "%");
            } else {
                result = theHosDB.thePatientLabreferinDB.queryByFName("%");
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    /**
     * ź�������͡�ҡ�ҹ������ �Ѵ�͡�����������´ uc ���� 27/11/47
     */
    public void deletePatientLabreferIn(PatientLabreferin patientLabreferin) {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.thePatientLabreferinDB.delete(patientLabreferin);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
    }

    /**
     * �ѹ�֡�����š������Ѻ��ԡ�âͧ������ �Ѵ�͡�����������´ uc ����
     * 29/11/47
     */
    public void saveVisitPatientLabreferin(VisitLabreferin visitLabreferin, UpdateStatus theUS) {
        if (visitLabreferin.getObjectId() != null) {
            return;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            visitLabreferin.begin_visit_time = theLookupControl.intReadDateTime();
            theHosDB.theVisitLabreferinDB.insert(visitLabreferin);
            theConnectionInf.getConnection().commit();
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.LabReferControl.VISIT.REFER") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.LabReferControl.VISIT.REFER") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
    }

    /**
     * ��Ǩ�ͺ��Ҽ�����������Ѻ��ԡ�����������ѧ
     * ���͹�������ª��㹡��ź�����ż������ա��˹�� �Ѵ�͡�����������´ uc
     * ���� 29/11/47
     */
    public boolean checkPatientInTableVisit(String patientLabreferinId) {
        boolean result = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector answer = theHosDB.theVisitLabreferinDB.queryPid(patientLabreferinId);
            if (answer == null || answer.isEmpty()) {
                result = false;
            } else {
                result = true;
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    /**
     * �ʴ���ª��ͼ����·����ѧ����Ѻ��ԡ�� �Ѵ�͡�����������´ uc ����
     * 29/11/47
     */
    public Vector listVisitLabreferin() {
        Vector result = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theVisitLabreferinDB.queryVisitInProcess();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    /**
     * ���Ң����ż����¨ҡ Primary Key �Ѵ�͡�����������´ uc ���� 29/11/47
     */
    public PatientLabreferin readPatientLabByPk(String plriid) {
        PatientLabreferin p = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            p = theHosDB.thePatientLabreferinDB.selectByPK(plriid);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return p;
    }

    /**
     * ���Ң����š���Ѻ��ԡ�ä�������ش����ѧ��ʶҹ�����Ѻ��ԡ������
     * �Ѵ�͡�����������´ uc ���� 29/11/47
     */
    public VisitLabreferin readVisitInProcessByPatientId(String plriid) {
        VisitLabreferin v = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            v = theHosDB.theVisitLabreferinDB.selectVisitByPId(plriid);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return v;
    }

    /**
     * ���Ң����š���Ѻ��ԡ�÷������ͧ������ �Ѵ�͡�����������´ uc ����
     * 29/11/47
     */
    public Vector listVisitLabreferinByPId(String pid) {
        Vector result = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theVisitLabreferinDB.queryVisitInProcessByPid(pid);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    /**
     * ������¡�� order �ҡ��ä�ԡ���͡���駡������Ѻ��ԡ�÷���ͧ��
     * �Ѵ�͡�����������´ uc ���� 29/11/47
     */
    public Vector listOrderByVisitId(String VId) {
        Vector result = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theOrderItemLabreferinDB.queryOrderByVid(VId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    /**
     * ź��¡�� order �Ѵ�͡�����������´ uc ���� 29/11/47
     */
    public void deleteOrderItemLabReferIn(OrderItemLabreferin orderItemLabreferin) {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theOrderItemLabreferinDB.delete(orderItemLabreferin);
            theHosDB.theOrderResultLabreferinDB.deleteById(orderItemLabreferin.getObjectId());

            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
    }

    /**
     * �ѹ�֡��¡�� order �Ѵ�͡�����������´ uc ���� 30/11/47
     */
    public void saveOrderItemLabReferin(Vector orderItemLabreferin) {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (orderItemLabreferin != null) {
                for (int i = 0; i < orderItemLabreferin.size(); i++) {
                    if (((OrderItemLabreferin) orderItemLabreferin.get(i)).getObjectId() == null) {
                        theHosDB.theOrderItemLabreferinDB.insert((OrderItemLabreferin) orderItemLabreferin.get(i));
                    } else {
                        theHosDB.theOrderItemLabreferinDB.update((OrderItemLabreferin) orderItemLabreferin.get(i));
                    }
                }
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
    }

    /**
     * �ʴ��� lab �����¡�� order �Ѵ�͡�����������´ uc ���� 30/11/47
     * �ѧ�������ҹ
     */
    public OrderResultLabreferin readResultLabReferinByOrderID(OrderResultLabreferin resultlab) {
        OrderResultLabreferin rl = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            rl = theHosDB.theOrderResultLabreferinDB.selectByOrderItem(resultlab);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return rl;
    }

    /**
     * �ʴ��� lab �����¡�� order �Ѵ�͡�����������´ uc ���� 30/11/47
     */
    public OrderItemLabreferin readOrderItemByItemIdAndVisitId(String itemID, String visitID) {
        OrderItemLabreferin oilri = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            oilri = theHosDB.theOrderItemLabreferinDB.selectByItemIDAndVID(itemID, visitID);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return oilri;
    }

    public String updateOrderItemLabreferin(OrderItemLabreferin orderItemLabreferin) {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theOrderItemLabreferinDB.update(orderItemLabreferin);
            theStatus = "Complete";
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theStatus = "Error";
        } finally {
            theConnectionInf.close();
        }
        return theStatus;
    }

    public void saveOrderResultLabrefrin(OrderResultLabreferin p) {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (p.getObjectId() == null) {
                theHosDB.theOrderResultLabreferinDB.insert(p);
                theConnectionInf.close();
            } else {
                theHosDB.theOrderResultLabreferinDB.update(p);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
    }

    public void deleteOrderResultLabReferIn(String oilriId) {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theOrderResultLabreferinDB.deleteById(oilriId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
    }

    public boolean checkVisitInResult(String visitLabreferinId) {
        boolean result = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector answer;
            answer = theHosDB.theOrderResultLabreferinDB.queryVid(visitLabreferinId);
            theConnectionInf.close();
            if (answer == null || answer.isEmpty()) {
                result = false;
            } else {
                result = true;
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    public Vector listResultByVId(String vId) {
        Vector result = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theOrderResultLabreferinDB.queryresultByVid(vId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    public String updateVisitLabreferin(VisitLabreferin visitLabreferin) {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theVisitLabreferinDB.update(visitLabreferin);
            theStatus = "Complete";
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theStatus = "Error";
        } finally {
            theConnectionInf.close();
        }
        return theStatus;
    }

    public boolean checkVisitInOrder(String visitLabreferinId) {
        boolean result = true;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector answer;
            answer = theHosDB.theOrderItemLabreferinDB.queryVid(visitLabreferinId);
            theConnectionInf.close();
            if (answer == null || answer.isEmpty()) {
                result = false;
            } else {
                result = true;
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    public OrderItemLabreferin readOrderItemByOrderItemIdAndVisitId(String orderItemID) {
        OrderItemLabreferin oilri = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            oilri = theHosDB.theOrderItemLabreferinDB.selectByPK(orderItemID);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return oilri;
    }

    public Vector listLabSetByItemId(String itemId) {
        Vector result = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theLabSetDB.selectByItemId(itemId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    public LabGroup readLabGroupByPk(String labGroupId) {
        LabGroup p = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            p = theHosDB.theLabGroupDB.selectByPK(labGroupId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return p;
    }

    public Vector listLabSetByLabGroupId(String lgId) {
        Vector result = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theLabSetDB.selectByLabGroupID(lgId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    public boolean checkOrderItemIdInResult(String orderItemLabreferinId) {
        boolean result = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector answer = theHosDB.theOrderResultLabreferinDB.queryOrderItemId(orderItemLabreferinId);
            theConnectionInf.close();
            if (answer == null || answer.isEmpty()) {
                result = false;
            } else {
                result = true;
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    public OrderItemLabreferin readOrderItemByPk(String pk) {
        OrderItemLabreferin p = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            p = theHosDB.theOrderItemLabreferinDB.selectByPK(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return p;
    }

    public LabGroup readLabGroupByItemId(String itemId) {
        LabGroup p = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            p = theHosDB.theLabGroupDB.selectByItemID(itemId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return p;
    }

    public void addLabReferOut(Vector vLabReferOut) {
        theHO.vLabReferOut = null;
        if (vLabReferOut == null || vLabReferOut.isEmpty()) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.LabReferControl.NO.LAB"), UpdateStatus.WARNING);
            return;
        }
        theHO.vLabReferOut = vLabReferOut;
        theHS.theResultSubject.notifyAddLabReferOut(ResourceBundle.getBundleText("com.hosv3.control.LabReferControl.ADD.LAB") + " "
                + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
    }

    public void addLabReferIn(Vector vLabReferIn) {
        theHO.vLabReferIn = null;
        if (vLabReferIn == null || vLabReferIn.isEmpty()) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.LabReferControl.NO.LAB"), UpdateStatus.WARNING);
            return;
        }
        theHO.vLabReferIn = vLabReferIn;
        theHS.theResultSubject.notifyAddLabReferIn(ResourceBundle.getBundleText("com.hosv3.control.LabReferControl.ADD.LAB") + " "
                + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
    }
}

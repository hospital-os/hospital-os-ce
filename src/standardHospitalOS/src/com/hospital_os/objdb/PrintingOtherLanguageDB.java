/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.PrintingOtherLanguage;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class PrintingOtherLanguageDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "829";

    public PrintingOtherLanguageDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public int insert(PrintingOtherLanguage obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO b_printing_other_language(\n"
                    + "            b_printing_other_language_id, b_printing_id, b_language_id, jrxml)\n"
                    + "    VALUES (?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getGenID(tableId));
            preparedStatement.setString(2, obj.b_printing_id);
            preparedStatement.setString(3, obj.b_language_id);
            preparedStatement.setString(4, obj.jrxml);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(PrintingOtherLanguage obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE b_printing_other_language\n");
            sql.append("   SET jrxml=?\n");
            sql.append(" WHERE b_printing_other_language_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.jrxml);
            preparedStatement.setString(2, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(PrintingOtherLanguage obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM b_printing_other_language\n");
            sql.append(" WHERE b_printing_other_language_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int deleteByPrintingId(String printingId) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM b_printing_other_language\n");
            sql.append(" WHERE b_printing_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, printingId);
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public PrintingOtherLanguage select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select b_printing_other_language.*, b_language.description from b_printing_other_language inner join b_language on b_language.b_language_id = b_printing_other_language.b_language_id where b_printing_other_language_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<PrintingOtherLanguage> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<PrintingOtherLanguage> listByPrintingId(String printingId) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select b_printing_other_language.*, b_language.description from b_printing_other_language inner join b_language on b_language.b_language_id = b_printing_other_language.b_language_id where b_printing_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, "printingId");
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<PrintingOtherLanguage> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<PrintingOtherLanguage> list = new ArrayList<PrintingOtherLanguage>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                PrintingOtherLanguage obj = new PrintingOtherLanguage();
                obj.setObjectId(rs.getString("b_printing_other_language_id"));
                obj.b_printing_id = rs.getString("b_printing_id");
                obj.b_language_id = rs.getString("b_language_id");
                obj.jrxml = rs.getString("jrxml");
                try {
                    obj.language = rs.getString("description");
                } catch (Exception ex) {
                }
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<Object[]> listMap(String printingId) throws Exception {
        List<Object[]> list = new ArrayList<Object[]>();
        ResultSet rs = null;
        PreparedStatement preparedStatement = null;
        try {
            // execute SQL stetement
            String sql = "select\n"
                    + "b_printing_other_language.b_printing_other_language_id as id\n"
                    + ", b_language.description as desc\n"
                    + "from\n"
                    + "b_printing_other_language\n"
                    //+ "inner join b_printing on b_printing.b_printing_id = b_printing_other_language.b_printing_id\n"
                    + "inner join b_language on b_language.b_language_id = b_printing_other_language.b_language_id\n"
                    + "where b_printing_other_language.b_printing_id = ?\n"
                    + "order by b_language.description";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, printingId);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Object[] obj = new Object[2];
                obj[0] = (rs.getString("id"));
                obj[1] = rs.getString("desc");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<Object[]> listUnmap(String printingId) throws Exception {
        List<Object[]> list = new ArrayList<Object[]>();
        ResultSet rs = null;
        PreparedStatement preparedStatement = null;
        try {
            // execute SQL stetement
            String sql = "select b_language.b_language_id, b_language.description from b_language where b_language.active = '1' and b_language.b_language_id not in (select\n"
                    + "b_language.b_language_id\n"
                    + "from\n"
                    + "b_printing_other_language\n"
                    //+ "inner join b_printing on b_printing.b_printing_id = b_printing_other_language.b_printing_id\n"
                    + "inner join b_language on b_language.b_language_id = b_printing_other_language.b_language_id\n"
                    + "where b_printing_other_language.b_printing_id = ?\n"
                    + ") order by b_language.description";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, printingId);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Object[] obj = new Object[2];
                obj[0] = (rs.getString("b_language_id"));
                obj[1] = rs.getString("description");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public Map<String, String> getJrxml(String printingId, String nationId) throws Exception {
        Map<String, String> map = null;
        ResultSet rs = null;
        PreparedStatement preparedStatement = null;
        try {
            // execute SQL stetement
            String sql = "select \n"
                    + "b_printing_other_language.jrxml, b_language.b_language_id\n"
                    + "from b_printing_other_language\n"
                    + "inner join b_printing on b_printing.b_printing_id = b_printing_other_language.b_printing_id\n"
                    + "inner join b_language on b_language.b_language_id = b_printing_other_language.b_language_id and b_language.active = '1'\n"
                    + "inner join b_language_mapping on b_language_mapping.b_language_id = b_language.b_language_id\n"
                    + "where b_printing_other_language.b_printing_id = ?\n"
                    + "and b_language_mapping.f_patient_nation_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, printingId);
            preparedStatement.setString(2, nationId);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                map = new HashMap<String, String>();
                map.put("JRXML", rs.getString("jrxml"));
                map.put("LANG_ID", rs.getString("b_language_id"));
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
        return map;
    }

    public List<Map<String, String>> listJrxmlLang(String printingId) throws Exception {
        List<Map<String, String>> list = new ArrayList<Map<String, String>>();
        ResultSet rs = null;
        PreparedStatement preparedStatement = null;
        try {
            // execute SQL stetement
            String sql = "select \n"
                    + "b_printing_other_language.jrxml \n"
                    + ", b_language.b_language_id \n"
                    + ", b_language.description\n"
                    + "from b_printing_other_language\n"
                    + "inner join b_language on b_language.b_language_id = b_printing_other_language.b_language_id and b_language.active = '1'\n"
                    + "where b_printing_other_language.b_printing_id = ?\n"
                    + "order by b_language.description";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, printingId);
            rs = preparedStatement.executeQuery();
            Map<String, String> map = null;
            while (rs.next()) {
                map = new HashMap<String, String>();
                map.put("JRXML", rs.getString("jrxml"));
                map.put("LANG_ID", rs.getString("b_language_id"));
                map.put("LANG", rs.getString("description"));
                list.add(map);
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
        return list;
    }
}

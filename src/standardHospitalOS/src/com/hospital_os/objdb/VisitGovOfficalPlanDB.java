/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.VisitGovOfficalPlan;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class VisitGovOfficalPlanDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "850";

    public VisitGovOfficalPlanDB(ConnectionInf c) {
        this.connectionInf = c;
    }

    public int insert(VisitGovOfficalPlan obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO t_visit_govoffical_plan(\n"
                    + "t_visit_govoffical_plan_id, t_visit_payment_id,\n"
                    + "govoffical_type, govoffical_number, f_govcode_id, ownname,\n"
                    + "ownrpid, f_subinscl_id, f_relinscl_id, user_record_id,\n"
                    + "user_update_id, approve_status, active, f_govoffical_plan_transaction_code_id,\n"
                    + "response_message, invoice_number, terminal_id, merchant_id,\n"
                    + "response_date, response_time)\n"
                    + "    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.getGenID(tableId));
            preparedStatement.setString(index++, obj.t_visit_payment_id);
            preparedStatement.setInt(index++, obj.govoffical_type);
            preparedStatement.setString(index++, obj.govoffical_number);
            preparedStatement.setString(index++, obj.f_govcode_id);
            preparedStatement.setString(index++, obj.ownname);
            preparedStatement.setString(index++, obj.ownrpid);
            preparedStatement.setString(index++, obj.f_subinscl_id);
            preparedStatement.setString(index++, obj.f_relinscl_id);
            preparedStatement.setString(index++, obj.user_record_id);
            preparedStatement.setString(index++, obj.user_update_id);
            preparedStatement.setString(index++, obj.approve_status);
            preparedStatement.setString(index++, obj.active);
            preparedStatement.setString(index++, obj.f_govoffical_plan_transaction_code_id);
            preparedStatement.setString(index++, obj.response_message);
            preparedStatement.setString(index++, obj.invoice_number);
            preparedStatement.setString(index++, obj.terminal_id);
            preparedStatement.setString(index++, obj.merchant_id);
            preparedStatement.setString(index++, obj.response_date);
            preparedStatement.setString(index++, obj.response_time);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(VisitGovOfficalPlan obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_visit_govoffical_plan\n");
            sql.append("   SET govoffical_type=?, govoffical_number=?, \n");
            sql.append("       f_govcode_id=?, ownname=?,\n");
            sql.append("       ownrpid=?,\n");
            sql.append("       f_subinscl_id=?, f_relinscl_id=?,user_update_id=?, update_datetime = current_timestamp,\n");
            sql.append("       approve_status=?, active=?,\n");
            sql.append("       response_message=?, invoice_number=?,\n");
            sql.append("       terminal_id=?, merchant_id=?,\n");
            sql.append("       response_date=?, response_time=?\n");
            sql.append(" WHERE t_visit_govoffical_plan_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setInt(index++, obj.govoffical_type);
            preparedStatement.setString(index++, obj.govoffical_number);
            preparedStatement.setString(index++, obj.f_govcode_id);
            preparedStatement.setString(index++, obj.ownname);
            preparedStatement.setString(index++, obj.ownrpid);
            preparedStatement.setString(index++, obj.f_subinscl_id);
            preparedStatement.setString(index++, obj.f_relinscl_id);
            preparedStatement.setString(index++, obj.user_update_id);
            preparedStatement.setString(index++, obj.approve_status);
            preparedStatement.setString(index++, obj.active);
            preparedStatement.setString(index++, obj.response_message);
            preparedStatement.setString(index++, obj.invoice_number);
            preparedStatement.setString(index++, obj.terminal_id);
            preparedStatement.setString(index++, obj.merchant_id);
            preparedStatement.setString(index++, obj.response_date);
            preparedStatement.setString(index++, obj.response_time);
            preparedStatement.setString(index++, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int updateApproveCode(VisitGovOfficalPlan obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_visit_govoffical_plan\n");
            sql.append("   SET govoffical_type=?, govoffical_number=?, \n");
            sql.append("       approve_status=?, active=?,\n");
            sql.append("       f_govoffical_plan_transaction_code_id=?,\n");
            sql.append("       response_message=?, invoice_number=?,user_update_id=?, update_datetime = current_timestamp,\n");
            sql.append("       terminal_id=?, merchant_id=?,\n");
            sql.append("       response_date=?, response_time=?\n");
            sql.append(" WHERE t_visit_govoffical_plan_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setInt(index++, obj.govoffical_type);
            preparedStatement.setString(index++, obj.govoffical_number);
            preparedStatement.setString(index++, obj.approve_status);
            preparedStatement.setString(index++, obj.active);
            preparedStatement.setString(index++, obj.f_govoffical_plan_transaction_code_id);
            preparedStatement.setString(index++, obj.response_message);
            preparedStatement.setString(index++, obj.invoice_number);
            preparedStatement.setString(index++, obj.user_update_id);
            preparedStatement.setString(index++, obj.terminal_id);
            preparedStatement.setString(index++, obj.merchant_id);
            preparedStatement.setString(index++, obj.response_date);
            preparedStatement.setString(index++, obj.response_time);
            preparedStatement.setString(index++, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int voidApproveCode(VisitGovOfficalPlan obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_visit_govoffical_plan\n");
            sql.append("   SET approve_status=?, active=?,\n");
            sql.append("       user_update_id=?, update_datetime = current_timestamp\n");
            sql.append(" WHERE t_visit_govoffical_plan_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.approve_status);
            preparedStatement.setString(index++, obj.active);
            preparedStatement.setString(index++, obj.user_update_id);
            preparedStatement.setString(index++, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int inActive(VisitGovOfficalPlan obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_visit_govoffical_plan\n");
            sql.append("   SET active=?, user_update_id=?, update_datetime = current_timestamp\n");
            sql.append(" WHERE t_visit_govoffical_plan_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.active);
            preparedStatement.setString(2, obj.user_update_id);
            preparedStatement.setString(3, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
//

    public VisitGovOfficalPlan select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from t_visit_govoffical_plan where t_visit_govoffical_plan_id = ? and active = '1'";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<VisitGovOfficalPlan> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public VisitGovOfficalPlan selectByVisitPaymentId(String t_visit_payment_id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from t_visit_govoffical_plan  where t_visit_payment_id = ? and active = '1'";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, t_visit_payment_id);
            List<VisitGovOfficalPlan> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<VisitGovOfficalPlan> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<VisitGovOfficalPlan> list = new ArrayList<VisitGovOfficalPlan>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                VisitGovOfficalPlan obj = new VisitGovOfficalPlan();
                obj.setObjectId(rs.getString("t_visit_govoffical_plan_id"));
                obj.t_visit_payment_id = rs.getString("t_visit_payment_id");
                obj.govoffical_number = rs.getString("govoffical_number");
                obj.govoffical_type = rs.getInt("govoffical_type");
                obj.ownname = rs.getString("ownname");
                obj.f_govcode_id = rs.getString("f_govcode_id");
                obj.ownrpid = rs.getString("ownrpid");
                obj.f_subinscl_id = rs.getString("f_subinscl_id");
                obj.f_relinscl_id = rs.getString("f_relinscl_id");
                obj.user_record_id = rs.getString("user_record_id");
                obj.record_datetime = rs.getTimestamp("record_datetime");
                obj.user_update_id = rs.getString("user_update_id");
                obj.update_datetime = rs.getTimestamp("update_datetime");
                obj.approve_status = rs.getString("approve_status");
                obj.active = rs.getString("active");
                obj.f_govoffical_plan_transaction_code_id = rs.getString("f_govoffical_plan_transaction_code_id");
                obj.response_message = rs.getString("response_message");
                obj.invoice_number = rs.getString("invoice_number");
                obj.terminal_id = rs.getString("terminal_id");
                obj.merchant_id = rs.getString("merchant_id");
                obj.response_date = rs.getString("response_date");
                obj.response_time = rs.getString("response_time");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }
}

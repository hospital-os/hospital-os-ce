package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;
import java.util.Date;

@SuppressWarnings("ClassWithoutLogger")
public class ModuleLicense extends Persistent {

    public String hcode;
    public String module_id;
    public String license;
    public Date expires_at;
    public Date record_date_time;
    public String user_record_id;

    @Override
    public String toString() {
        return module_id + " - " + expires_at.toLocaleString();
    }
}

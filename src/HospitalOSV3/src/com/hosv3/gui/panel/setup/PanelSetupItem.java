/*
 * PanelSetupOrderItem.java
 *
 * Created on 14 ���Ҥ� 2546, 14:44 �.
 */
package com.hosv3.gui.panel.setup;

import com.hospital_os.object.*;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hospital_os.utility.CellRendererHos;
import com.hospital_os.utility.ComboFix;
import com.hospital_os.utility.ComboboxModel;
import com.hospital_os.utility.ComplexDataSource;
import com.hospital_os.utility.DataSource;
import com.hospital_os.utility.Gutil;
import com.hospital_os.utility.TaBleModel;
import com.hospital_os.utility.TableModelComplexDataSource;
import com.hospital_os.utility.TableModelDataSource;
import com.hosv3.control.*;
import com.hosv3.control.lookup.*;
import com.hosv3.gui.dialog.*;
import com.hosv3.subject.*;
import com.hosv3.usecase.setup.*;
import com.hosv3.utility.*;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.DefaultTableCellRenderer;
import sd.comp.textfield.DoubleDocument;
import sd.comp.textfield.NumberDocument;
import sd.jtable.header.ColumnGroup;
import sd.jtable.header.GroupableTableColumnModel;
import sd.jtable.header.GroupableTableHeader;

/**
 *
 * @Panel author : amp
 */
@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public class PanelSetupItem extends javax.swing.JPanel implements ManageOptionReq {

    private static final long serialVersionUID = 1L;
    LookupControl theLookupControl;
    UpdateStatus theUS;
    SetupControl theSetupControl;
    SetupSubject theSetupSubject;
    ComboboxModel theComboboxModel;
    HosSubject theHS;
    Item theItem = new Item();
    ItemPrice theItemPrice;
    Drug theDrug = new Drug();
    LabResultItem theLabResultItem = new LabResultItem();
    LabGroup theLabGroup = new LabGroup();
    Vector vItemPrice = new Vector();
    Vector vItem_service = new Vector();
    ItemService theItemService = null;
    Vector vItem = new Vector();
    Vector vLabSet = new Vector();
    PanelSetupSearchSub psss;
    HosControl theHC;
    int offset = 20;
    int next = 0;
    int prev = 0;
    int saved = 0; // 0 ��� �������ö insert�� 1 ��� insert ��
    int category = 0;
    /**
     * pu : 25/07/2549 : �� Index �ͧ Item �����ҧ�ش�ͧ˹�һѨ�غѹ
     */
    int curNext = 0;
    /**
     * pu : 25/07/2549 : �� Index �ͧ Item �����ҧ�ش�ͧ˹�ҡ�͹˹�һѨ�غѹ
     */
    int curPrev = 0;
    String[] col = {"����", "����"};
    String[] col_list = {"��¡��"};
    int currentRow = 0;
    //amp:13/03/2549 �������Ǩ�ͺ����ա�����¡�����ҡ�������ҧ����������
    boolean is_drug = false;
    private final DoubleDocument doubleDocumentGenderM_Min = new DoubleDocument();
    private final DoubleDocument doubleDocumentGenderM_Max = new DoubleDocument();
    private final DoubleDocument doubleDocumentGenderWM_Min = new DoubleDocument();
    private final DoubleDocument doubleDocumentGenderWM_Max = new DoubleDocument();
    private final DoubleDocument doubleDocumentNRAgeM_Min = new DoubleDocument();
    private final DoubleDocument doubleDocumentNRAgeM_Max = new DoubleDocument();
    private final DoubleDocument doubleDocumentNRAgeWM_Min = new DoubleDocument();
    private final DoubleDocument doubleDocumentNRAgeWM_Max = new DoubleDocument();
    private final DoubleDocument doubleDocumentGenderM_MinCV = new DoubleDocument();
    private final DoubleDocument doubleDocumentGenderM_MaxCV = new DoubleDocument();
    private final DoubleDocument doubleDocumentGenderWM_MinCV = new DoubleDocument();
    private final DoubleDocument doubleDocumentGenderWM_MaxCV = new DoubleDocument();
    private final DoubleDocument doubleDocumentNRAgeM_MinCV = new DoubleDocument();
    private final DoubleDocument doubleDocumentNRAgeM_MaxCV = new DoubleDocument();
    private final DoubleDocument doubleDocumentNRAgeWM_MinCV = new DoubleDocument();
    private final DoubleDocument doubleDocumentNRAgeWM_MaxCV = new DoubleDocument();
    private ItemSupply theItemSupply;
    private BItemXray theItemXray;
    private final TableModelDataSource tmds = new TableModelDataSource("��һ���");
    private final DocumentListener textNormalRangeListener = new DocumentListener() {
        @Override
        public void insertUpdate(DocumentEvent e) {
            doValidateTextNormalRange();
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            doValidateTextNormalRange();
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            doValidateTextNormalRange();
        }
    };
    private final TableModelComplexDataSource tmcds = new TableModelComplexDataSource(new String[]{
        "Min", "Max", "Min CV", "Min", "Max", "Max CV", "Min CV", "Min", "Max", "Max CV"
    });
    private final DocumentListener ageNormalRangeListener = new DocumentListener() {
        @Override
        public void insertUpdate(DocumentEvent e) {
            doValidateAgeNormalRange();
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            doValidateAgeNormalRange();
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            doValidateAgeNormalRange();
        }
    };
    private final CellRendererHos cellRendererCurrency = new CellRendererHos(CellRendererHos.CURRENCY);

    public PanelSetupItem() {
        initComponents();
        initTable();
        setLanguage();
    }

    public PanelSetupItem(HosControl hc, UpdateStatus us) {
        this();
        setControl(hc, us);
    }

    private void initTable() {
        tableAgeNormalRange.setColumnModel(new GroupableTableColumnModel());
        tableAgeNormalRange.setTableHeader(new GroupableTableHeader((GroupableTableColumnModel) tableAgeNormalRange.getColumnModel()));
        tableAgeNormalRange.setModel(tmcds);
        // Setup Column Groups
        DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
        cellRenderer.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);
        GroupableTableColumnModel cm = (GroupableTableColumnModel) tableAgeNormalRange.getColumnModel();

        ColumnGroup grpAge = new ColumnGroup("��ǧ����");
        cm.getColumn(0).setPreferredWidth(20);
        cm.getColumn(0).setCellRenderer(cellRenderer);
        grpAge.add(cm.getColumn(0));

        cm.getColumn(1).setPreferredWidth(20);
        cm.getColumn(1).setCellRenderer(cellRenderer);
        grpAge.add(cm.getColumn(1));

        ColumnGroup grpMale = new ColumnGroup("��һ����Ȫ��");

        cm.getColumn(2).setPreferredWidth(30);
        cm.getColumn(2).setCellRenderer(cellRenderer);
        grpMale.add(cm.getColumn(2));

        cm.getColumn(3).setPreferredWidth(30);
        cm.getColumn(3).setCellRenderer(cellRenderer);
        grpMale.add(cm.getColumn(3));

        cm.getColumn(4).setPreferredWidth(30);
        cm.getColumn(4).setCellRenderer(cellRenderer);
        grpMale.add(cm.getColumn(4));

        cm.getColumn(5).setPreferredWidth(30);
        cm.getColumn(5).setCellRenderer(cellRenderer);
        grpMale.add(cm.getColumn(5));

        ColumnGroup grpFemale = new ColumnGroup("��һ�����˭ԧ");
        cm.getColumn(6).setPreferredWidth(30);
        cm.getColumn(6).setCellRenderer(cellRenderer);
        grpFemale.add(cm.getColumn(6));

        cm.getColumn(7).setPreferredWidth(30);
        cm.getColumn(7).setCellRenderer(cellRenderer);
        grpFemale.add(cm.getColumn(7));

        cm.getColumn(8).setPreferredWidth(30);
        cm.getColumn(8).setCellRenderer(cellRenderer);
        grpFemale.add(cm.getColumn(8));

        cm.getColumn(9).setPreferredWidth(30);
        cm.getColumn(9).setCellRenderer(cellRenderer);
        grpFemale.add(cm.getColumn(9));

        cm.addColumnGroup(grpAge);
        cm.addColumnGroup(grpMale);
        cm.addColumnGroup(grpFemale);
    }

    public void setControl(HosControl hc, UpdateStatus us) {
        theUS = us;
        theHC = hc;
        theHS = hc.theHS;
        this.jPanelDrugDescription.setControl(hc, us);
        theLookupControl = hc.theLookupControl;
        theSetupControl = hc.theSetupControl;
        theSetupSubject = hc.theHS.theSetupSubject;
        hc.theHS.theSetupSubject.addForLiftAttach(this);
        setupLookup();
        setEnabled(false);

        theSetupSubject.addItem(this);
        theSetupSubject.addItemLab(this);
        theSetupSubject.addpanelrefrash(this);

        jTabbedPane1.remove(jPanel9);
        dateTextField.setEditable(true);
        jTextFieldIcdCode.setControl(new Icd9Lookup(theHC.theLookupControl), theUS.getJFrame());
        balloonTextArea1.setJFrame(theUS.getJFrame());
        balloonTextArea1.setControl(new Icd9Lookup(theHC.theLookupControl));
        jTextFieldIcdCode.setEControl(new Icd9Lookup(theHC.theSetupControl));
        theHS.theBalloonSubject.attachBalloon(jTextFieldIcdCode);
        theHS.theBalloonSubject.attachBalloon(balloonTextArea1);
        jTextFieldDescription.setEditable(false);
        jTextFieldIcdCode.setVisible(false);
    }

    /**
     * @Author : amp
     * @date : 29/02/2549
     * @see : �Ѵ�������ǡѺ����
     */
    private void setLanguage() {
        for (int i = 0; i < this.getComponentCount(); i++) {
            GuiLang.setLanguage(this.getComponent(i));
        }
    }

    public void setupLookup() {
        Vector catagolyitemgroup = new Vector(theLookupControl.listCategoryGroupItemSpecial(CategoryGroup.isPackage(), false));
        ComboboxModel.initComboBox(jComboBoxSCategory, catagolyitemgroup);
        ComboboxModel.initComboBox(jComboBoxOrderList, catagolyitemgroup);
        Vector billingitemgroup = theLookupControl.listBillingGroupItem();
        ComboboxModel.initComboBox(jComboBoxReceiptList, billingitemgroup);
        Vector labresulttype = theLookupControl.listLabResultGroup();
        ComboboxModel.initComboBox(jComboBoxList, labresulttype);
        Vector standarditemgroup = theLookupControl.listItem16Group();
        ComboboxModel.initComboBox(jComboBoxStandardGroup, standarditemgroup);
        jComboBoxPlan.setControl(null, new PlanLookup(
                theHC.theLookupControl), new Plan());
        List tariffs = theLookupControl.listTariff();
        ComboboxModel.initComboBox(jComboBoxTariff, tariffs);
        Vector itemLabTypes = theLookupControl.listItemLabType();
        ItemLabType itemLabType = new ItemLabType();
        itemLabType.setObjectId("");
        itemLabType.name = "����к�";
        if (itemLabTypes == null) {
            itemLabTypes = new Vector();
        }
        itemLabTypes.add(0, itemLabType);
        ComboboxModel.initComboBox(jComboBoxLabRpGroup, itemLabTypes);
        // 3.9.31
        List<CommonInf> itemLabSpecimens = new ArrayList<CommonInf>();
        Specimen specimen = new Specimen();
        specimen.setObjectId("");
        specimen.description = "����к�";
        itemLabSpecimens.add(0, specimen);
        itemLabSpecimens.addAll(theLookupControl.listItemLabSpecimens());
        ComboboxModel.initComboBox(jComboBoxLabSpecimen, itemLabSpecimens);
        // 3.9.42
        List<CommonInf> modalitys = new ArrayList<CommonInf>();
        BModality modality = new BModality();
        modality.setObjectId("");
        modality.modality_code = "����к�";
        modalitys.add(0, modality);
        modalitys.addAll(theLookupControl.listXrayModality());
        ComboboxModel.initComboBox(combModality, modalitys);

        List<CommonInf> labAtkProducts = new ArrayList<>();
        LabAtkProduct theLabAtkProduct = new LabAtkProduct();
        theLabAtkProduct.setObjectId(null);
        theLabAtkProduct.lab_atk_device_name = "����к�";
        labAtkProducts.add(0, theLabAtkProduct);
        labAtkProducts.addAll(theLookupControl.listLabAtkProductByKeyword(""));
        ComboboxModel.initComboBox(cbAtkProduct, labAtkProducts);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroupLab = new javax.swing.ButtonGroup();
        buttonGroupLabResultType = new javax.swing.ButtonGroup();
        buttonGroupBeginWith = new javax.swing.ButtonGroup();
        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroupNormalValue = new javax.swing.ButtonGroup();
        buttonGroupLabLocation = new javax.swing.ButtonGroup();
        jPanel4 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jTextFieldSCode = new javax.swing.JTextField();
        jButtonSearch = new javax.swing.JButton();
        jCheckBoxS = new javax.swing.JCheckBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new com.hosv3.gui.component.HJTableSort();
        jRadioButtonBegin = new javax.swing.JRadioButton();
        jRadioButtonConsist = new javax.swing.JRadioButton();
        jPanel17 = new javax.swing.JPanel();
        jCheckBoxSearchGroup = new javax.swing.JCheckBox();
        jComboBoxSCategory = new javax.swing.JComboBox();
        jButtonPrev = new javax.swing.JButton();
        jButtonNext = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel5 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTextFieldCode = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextAreaCommonName = new javax.swing.JTextArea();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTextAreaTradeName = new javax.swing.JTextArea();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTextAreaNickName = new javax.swing.JTextArea();
        jLabel6 = new javax.swing.JLabel();
        jComboBoxOrderList = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        jComboBoxReceiptList = new javax.swing.JComboBox();
        jCheckBoxActive = new javax.swing.JCheckBox();
        jLabel37 = new javax.swing.JLabel();
        jComboBoxStandardGroup = new javax.swing.JComboBox();
        jLabel8 = new javax.swing.JLabel();
        jTextFieldGeneralNumber = new javax.swing.JTextField();
        jCheckBox2 = new javax.swing.JCheckBox();
        jPanel18 = new javax.swing.JPanel();
        lblStatusInactive = new javax.swing.JLabel();
        lblStatusNotMapTMT = new javax.swing.JLabel();
        lblStatusNotMap24 = new javax.swing.JLabel();
        jLabel42 = new javax.swing.JLabel();
        jScrollPane8 = new javax.swing.JScrollPane();
        jTextAreaLocalName = new javax.swing.JTextArea();
        jPanel28 = new javax.swing.JPanel();
        lblPicture = new javax.swing.JLabel();
        btnAddPicture = new javax.swing.JButton();
        btnDelPicture = new javax.swing.JButton();
        jLabel73 = new javax.swing.JLabel();
        jCheckBoxItemNotUseToOrder = new javax.swing.JCheckBox();
        jPanel6 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        tableItemPriceList = new com.hosv3.gui.component.HJTableSort();
        jPanel11 = new javax.swing.JPanel();
        jButtonAddPrice = new javax.swing.JButton();
        jButtonDelPrice = new javax.swing.JButton();
        panelItemPriceDetail = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        doubleTextFieldPriceCost = new com.hospital_os.utility.DoubleTextField();
        doubleTextFieldPriceOPD = new com.hospital_os.utility.DoubleTextField();
        dateTextField = new com.hospital_os.utility.DateComboBox();
        jCheckBoxPlan = new javax.swing.JCheckBox();
        jComboBoxPlan = new com.hosv3.gui.component.HosComboBox();
        jLabel34 = new javax.swing.JLabel();
        doubleTextFieldPriceIPD = new com.hospital_os.utility.DoubleTextField();
        jLabel43 = new javax.swing.JLabel();
        jLabel44 = new javax.swing.JLabel();
        jLabel45 = new javax.swing.JLabel();
        jLabel46 = new javax.swing.JLabel();
        doubleTextFieldPriceDF = new com.hospital_os.utility.DoubleTextField();
        jLabel47 = new javax.swing.JLabel();
        jLabel48 = new javax.swing.JLabel();
        jLabel49 = new javax.swing.JLabel();
        jLabel53 = new javax.swing.JLabel();
        doubleTextFieldPriceHos = new com.hospital_os.utility.DoubleTextField();
        jLabel54 = new javax.swing.JLabel();
        jPanel20 = new javax.swing.JPanel();
        chkbCanEditPrice = new javax.swing.JCheckBox();
        panelLimitPrice = new javax.swing.JPanel();
        jLabel50 = new javax.swing.JLabel();
        doubleTextFieldPriceMin = new com.hospital_os.utility.DoubleTextField();
        jLabel51 = new javax.swing.JLabel();
        doubleTextFieldPriceMax = new com.hospital_os.utility.DoubleTextField();
        jLabel52 = new javax.swing.JLabel();
        jLabel64 = new javax.swing.JLabel();
        doubleTextFieldPriceClaim = new com.hospital_os.utility.DoubleTextField();
        jLabel65 = new javax.swing.JLabel();
        jLabel66 = new javax.swing.JLabel();
        jComboBoxTariff = new com.hosv3.gui.component.HosComboBox();
        chkbUseClaimPrice = new javax.swing.JCheckBox();
        jPanel7 = new javax.swing.JPanel();
        jPanelBlank = new javax.swing.JPanel();
        jPanelLabDescription = new javax.swing.JPanel();
        jPanel23 = new javax.swing.JPanel();
        jRadioButtonLabDetail = new javax.swing.JRadioButton();
        jRadioButtonLabGroup = new javax.swing.JRadioButton();
        jComboBoxLabRpGroup = new javax.swing.JComboBox();
        jComboBoxLabSpecimen = new javax.swing.JComboBox();
        jLabel14 = new javax.swing.JLabel();
        jLabel68 = new javax.swing.JLabel();
        jRadioButtonLabIn = new javax.swing.JRadioButton();
        jRadioButtonLabOut = new javax.swing.JRadioButton();
        jPanel22 = new javax.swing.JPanel();
        jPanel21 = new javax.swing.JPanel();
        jPanel30 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        jTextFieldLabDetail = new javax.swing.JTextField();
        jLabel35 = new javax.swing.JLabel();
        jTextFieldLabUnit = new javax.swing.JTextField();
        jPanel31 = new javax.swing.JPanel();
        jRadioButtonNumber = new javax.swing.JRadioButton();
        jRadioButtonPointNumber = new javax.swing.JRadioButton();
        jRadioButtonText = new javax.swing.JRadioButton();
        jRadioButtonTexts = new javax.swing.JRadioButton();
        jPanelDefaultValue = new javax.swing.JPanel();
        jComboBoxList = new javax.swing.JComboBox();
        jScrollPane13 = new javax.swing.JScrollPane();
        jTableList = new com.hosv3.gui.component.HJTableSort();
        jRadioButtonList = new javax.swing.JRadioButton();
        panelNormalRange = new javax.swing.JPanel();
        jLabel33 = new javax.swing.JLabel();
        cbNormalrRangeType = new javax.swing.JComboBox();
        jScrollPane12 = new javax.swing.JScrollPane();
        panelCardNormalRange = new javax.swing.JPanel();
        panelNormalRangeByGender = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        txtGenderWM_Min = new javax.swing.JTextField();
        txtGenderWM_Max = new javax.swing.JTextField();
        txtGenderM_Max = new javax.swing.JTextField();
        txtGenderM_Min = new javax.swing.JTextField();
        jLabel55 = new javax.swing.JLabel();
        jLabel56 = new javax.swing.JLabel();
        jLabel57 = new javax.swing.JLabel();
        jLabel58 = new javax.swing.JLabel();
        txtGenderM_MinCV = new javax.swing.JTextField();
        txtGenderWM_MinCV = new javax.swing.JTextField();
        txtGenderM_MaxCV = new javax.swing.JTextField();
        txtGenderWM_MaxCV = new javax.swing.JTextField();
        panelNormalRangeByAges = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jLabel19 = new javax.swing.JLabel();
        jLabel40 = new javax.swing.JLabel();
        jLabel41 = new javax.swing.JLabel();
        txtAge_Min = new javax.swing.JTextField();
        txtAge_Max = new javax.swing.JTextField();
        jPanel24 = new javax.swing.JPanel();
        jLabel59 = new javax.swing.JLabel();
        txtNRAgeM_Min = new javax.swing.JTextField();
        jLabel39 = new javax.swing.JLabel();
        txtNRAgeM_Max = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        txtNRAgeWM_Min = new javax.swing.JTextField();
        jLabel31 = new javax.swing.JLabel();
        txtNRAgeWM_Max = new javax.swing.JTextField();
        jLabel60 = new javax.swing.JLabel();
        jLabel61 = new javax.swing.JLabel();
        jLabel62 = new javax.swing.JLabel();
        jLabel63 = new javax.swing.JLabel();
        txtNRAgeM_MinCV = new javax.swing.JTextField();
        txtNRAgeWM_MinCV = new javax.swing.JTextField();
        txtNRAgeM_MaxCV = new javax.swing.JTextField();
        txtNRAgeWM_MaxCV = new javax.swing.JTextField();
        jScrollPane7 = new javax.swing.JScrollPane();
        tableAgeNormalRange = new javax.swing.JTable();
        btnAddAgeNormalRange = new javax.swing.JButton();
        btnRemoveAgeNormalRange = new javax.swing.JButton();
        panelNormalRangeByTexts = new javax.swing.JPanel();
        txtNormalRangeText = new javax.swing.JTextField();
        btnAddTextNormalRange = new javax.swing.JButton();
        btnDeleteTextNormalRange = new javax.swing.JButton();
        jScrollPane6 = new javax.swing.JScrollPane();
        tableTextNormalRange = new javax.swing.JTable();
        panelNormalRangeBySql = new javax.swing.JPanel();
        jScrollPane11 = new javax.swing.JScrollPane();
        jTextFieldDefaultValue = new javax.swing.JTextArea();
        panelNA = new javax.swing.JPanel();
        lblNormalRangeMsg = new javax.swing.JLabel();
        jPanel15 = new javax.swing.JPanel();
        jCheckBoxSecret = new javax.swing.JCheckBox();
        jCheckBoxNCDFBS = new javax.swing.JCheckBox();
        jCheckBoxNCDHCT = new javax.swing.JCheckBox();
        jCheckBoxDuration = new javax.swing.JCheckBox();
        jTextFieldDurationDate = new com.hospital_os.utility.DoubleTextField();
        jLabelAmountDate = new javax.swing.JLabel();
        jPanel26 = new javax.swing.JPanel();
        jLabel69 = new javax.swing.JLabel();
        cbAtkProduct = new javax.swing.JComboBox<>();
        jPanel10 = new javax.swing.JPanel();
        jLabel32 = new javax.swing.JLabel();
        jScrollPane10 = new javax.swing.JScrollPane();
        jTable4 = new com.hosv3.gui.component.HJTableSort();
        jPanel51 = new javax.swing.JPanel();
        jButtonDelLabItem = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jPanelIcdCode = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        jTextFieldDescription = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jTextFieldIcdCode = new com.hosv3.gui.component.BalloonTextField();
        jScrollPane14 = new javax.swing.JScrollPane();
        balloonTextArea1 = new com.hosv3.gui.component.BalloonTextArea();
        jPanelDrugDescription = new com.hosv3.gui.panel.detail.PDItemDrug();
        panelSupply = new javax.swing.JPanel();
        cbSupplyPrintable = new javax.swing.JCheckBox();
        jPanel16 = new javax.swing.JPanel();
        chkbSupplyPrintMar = new javax.swing.JCheckBox();
        cbSupplyMarType = new javax.swing.JComboBox();
        jPanel25 = new javax.swing.JPanel();
        jLabel67 = new javax.swing.JLabel();
        jScrollPane15 = new javax.swing.JScrollPane();
        txtSupplement = new javax.swing.JTextArea();
        panelXrayDescription = new javax.swing.JPanel();
        jPanel19 = new javax.swing.JPanel();
        jLabel38 = new javax.swing.JLabel();
        combModality = new javax.swing.JComboBox();
        jPanel9 = new javax.swing.JPanel();
        jScrollPane9 = new javax.swing.JScrollPane();
        jTable3 = new com.hosv3.gui.component.HJTableSort();
        jPanel111 = new javax.swing.JPanel();
        jButtonAddPrice1 = new javax.swing.JButton();
        jButtonDelPrice1 = new javax.swing.JButton();
        jButtonSavePrice1 = new javax.swing.JButton();
        jPanel12 = new javax.swing.JPanel();
        jLabel21 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jLabel23 = new javax.swing.JLabel();
        jTextField3 = new javax.swing.JTextField();
        jButtonPrev1 = new javax.swing.JButton();
        jPanel13 = new javax.swing.JPanel();
        jLabel24 = new javax.swing.JLabel();
        jTextField4 = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        jTextField5 = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        jTextField6 = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        jTextField7 = new javax.swing.JTextField();
        jPanel14 = new javax.swing.JPanel();
        jLabel28 = new javax.swing.JLabel();
        jTextField8 = new javax.swing.JTextField();
        jComboBox2 = new javax.swing.JComboBox();
        jLabel29 = new javax.swing.JLabel();
        jTextField9 = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        jButtonAdd = new javax.swing.JButton();
        jButtonDel = new javax.swing.JButton();
        jButtonSave = new javax.swing.JButton();

        setLayout(new java.awt.GridBagLayout());

        jPanel4.setLayout(new java.awt.GridBagLayout());

        jLabel4.setFont(jLabel4.getFont().deriveFont(jLabel4.getFont().getSize()+7f));
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("com/hosv3/property/thai"); // NOI18N
        jLabel4.setText(bundle.getString("ITEM")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        jPanel4.add(jLabel4, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(jPanel4, gridBagConstraints);

        jPanel3.setMinimumSize(new java.awt.Dimension(300, 25));
        jPanel3.setPreferredSize(new java.awt.Dimension(300, 404));
        jPanel3.setLayout(new java.awt.GridBagLayout());

        jTextFieldSCode.setFont(jTextFieldSCode.getFont());
        jTextFieldSCode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldSCodeActionPerformed(evt);
            }
        });
        jTextFieldSCode.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldSCodeKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 3, 5);
        jPanel3.add(jTextFieldSCode, gridBagConstraints);

        jButtonSearch.setFont(jButtonSearch.getFont());
        jButtonSearch.setText(bundle.getString("SEARCH")); // NOI18N
        jButtonSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSearchActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        jPanel3.add(jButtonSearch, gridBagConstraints);

        jCheckBoxS.setFont(jCheckBoxS.getFont());
        jCheckBoxS.setSelected(true);
        jCheckBoxS.setText(bundle.getString("ACTIVE")); // NOI18N
        jCheckBoxS.setMaximumSize(new java.awt.Dimension(1, 1));
        jCheckBoxS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxSActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 5, 5);
        jPanel3.add(jCheckBoxS, gridBagConstraints);

        jScrollPane1.setMinimumSize(new java.awt.Dimension(100, 22));
        jScrollPane1.setPreferredSize(new java.awt.Dimension(100, 80));

        jTable1.setFillsViewportHeight(true);
        jTable1.setFont(jTable1.getFont());
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTable1MouseReleased(evt);
            }
        });
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTable1KeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 5, 5);
        jPanel3.add(jScrollPane1, gridBagConstraints);

        buttonGroupBeginWith.add(jRadioButtonBegin);
        jRadioButtonBegin.setFont(jRadioButtonBegin.getFont());
        jRadioButtonBegin.setText("��˹��");
        jRadioButtonBegin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonBeginActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel3.add(jRadioButtonBegin, gridBagConstraints);

        buttonGroupBeginWith.add(jRadioButtonConsist);
        jRadioButtonConsist.setFont(jRadioButtonConsist.getFont());
        jRadioButtonConsist.setSelected(true);
        jRadioButtonConsist.setText("��Сͺ");
        jRadioButtonConsist.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonConsistActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel3.add(jRadioButtonConsist, gridBagConstraints);

        jPanel17.setLayout(new java.awt.GridBagLayout());

        jCheckBoxSearchGroup.setFont(jCheckBoxSearchGroup.getFont());
        jCheckBoxSearchGroup.setText("�����");
        jCheckBoxSearchGroup.setMaximumSize(new java.awt.Dimension(1, 1));
        jCheckBoxSearchGroup.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jCheckBoxSearchGroupMouseClicked(evt);
            }
        });
        jCheckBoxSearchGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxSearchGroupActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        jPanel17.add(jCheckBoxSearchGroup, gridBagConstraints);

        jComboBoxSCategory.setFont(jComboBoxSCategory.getFont());
        jComboBoxSCategory.setEnabled(false);
        jComboBoxSCategory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxSCategoryActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        jPanel17.add(jComboBoxSCategory, gridBagConstraints);

        jButtonPrev.setFont(jButtonPrev.getFont());
        jButtonPrev.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/Back16.gif"))); // NOI18N
        jButtonPrev.setMaximumSize(new java.awt.Dimension(24, 24));
        jButtonPrev.setMinimumSize(new java.awt.Dimension(24, 24));
        jButtonPrev.setPreferredSize(new java.awt.Dimension(24, 24));
        jButtonPrev.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPrevActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel17.add(jButtonPrev, gridBagConstraints);

        jButtonNext.setFont(jButtonNext.getFont());
        jButtonNext.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/Forward16.gif"))); // NOI18N
        jButtonNext.setMaximumSize(new java.awt.Dimension(24, 24));
        jButtonNext.setMinimumSize(new java.awt.Dimension(24, 24));
        jButtonNext.setPreferredSize(new java.awt.Dimension(24, 24));
        jButtonNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNextActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        jPanel17.add(jButtonNext, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel3.add(jPanel17, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.4;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 5, 5);
        add(jPanel3, gridBagConstraints);

        jPanel2.setMinimumSize(new java.awt.Dimension(350, 400));
        jPanel2.setPreferredSize(new java.awt.Dimension(350, 400));
        jPanel2.setLayout(new java.awt.GridBagLayout());

        jTabbedPane1.setFont(jTabbedPane1.getFont());
        jTabbedPane1.setMaximumSize(new java.awt.Dimension(350, 420));
        jTabbedPane1.setMinimumSize(new java.awt.Dimension(350, 420));
        jTabbedPane1.setPreferredSize(new java.awt.Dimension(350, 420));

        jPanel5.setLayout(new java.awt.GridBagLayout());

        jLabel1.setFont(jLabel1.getFont());
        jLabel1.setText(bundle.getString("CODE")); // NOI18N
        jLabel1.setMaximumSize(new java.awt.Dimension(1, 1));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jLabel1, gridBagConstraints);

        jTextFieldCode.setBackground(new java.awt.Color(204, 255, 255));
        jTextFieldCode.setFont(jTextFieldCode.getFont());
        jTextFieldCode.setToolTipText("");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jTextFieldCode, gridBagConstraints);

        jLabel2.setFont(jLabel2.getFont());
        jLabel2.setText(bundle.getString("ITEM_COMMON_NAME")); // NOI18N
        jLabel2.setMaximumSize(new java.awt.Dimension(1, 1));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jLabel2, gridBagConstraints);

        jScrollPane2.setMaximumSize(new java.awt.Dimension(150, 48));
        jScrollPane2.setMinimumSize(new java.awt.Dimension(150, 48));
        jScrollPane2.setPreferredSize(new java.awt.Dimension(150, 45));

        jTextAreaCommonName.setBackground(new java.awt.Color(204, 255, 255));
        jTextAreaCommonName.setFont(jTextAreaCommonName.getFont());
        jTextAreaCommonName.setLineWrap(true);
        jTextAreaCommonName.setWrapStyleWord(true);
        jScrollPane2.setViewportView(jTextAreaCommonName);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jScrollPane2, gridBagConstraints);

        jLabel3.setFont(jLabel3.getFont());
        jLabel3.setText(bundle.getString("ITEM_TRADE_NAME")); // NOI18N
        jLabel3.setMaximumSize(new java.awt.Dimension(1, 1));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jLabel3, gridBagConstraints);

        jScrollPane3.setMaximumSize(new java.awt.Dimension(150, 48));
        jScrollPane3.setMinimumSize(new java.awt.Dimension(150, 48));
        jScrollPane3.setPreferredSize(new java.awt.Dimension(150, 45));

        jTextAreaTradeName.setFont(jTextAreaTradeName.getFont());
        jTextAreaTradeName.setLineWrap(true);
        jTextAreaTradeName.setWrapStyleWord(true);
        jScrollPane3.setViewportView(jTextAreaTradeName);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jScrollPane3, gridBagConstraints);

        jLabel5.setFont(jLabel5.getFont());
        jLabel5.setText(bundle.getString("ITEM_LOCAL_NAME")); // NOI18N
        jLabel5.setMaximumSize(new java.awt.Dimension(1, 1));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jLabel5, gridBagConstraints);

        jScrollPane4.setMaximumSize(new java.awt.Dimension(150, 48));
        jScrollPane4.setMinimumSize(new java.awt.Dimension(150, 48));
        jScrollPane4.setPreferredSize(new java.awt.Dimension(150, 45));

        jTextAreaNickName.setFont(jTextAreaNickName.getFont());
        jTextAreaNickName.setLineWrap(true);
        jTextAreaNickName.setWrapStyleWord(true);
        jScrollPane4.setViewportView(jTextAreaNickName);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jScrollPane4, gridBagConstraints);

        jLabel6.setFont(jLabel6.getFont());
        jLabel6.setText(bundle.getString("LIST_ORDER")); // NOI18N
        jLabel6.setMaximumSize(new java.awt.Dimension(1, 1));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jLabel6, gridBagConstraints);

        jComboBoxOrderList.setFont(jComboBoxOrderList.getFont());
        jComboBoxOrderList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jComboBoxOrderListMouseClicked(evt);
            }
        });
        jComboBoxOrderList.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBoxOrderListItemStateChanged(evt);
            }
        });
        jComboBoxOrderList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxOrderListActionPerformed(evt);
            }
        });
        jComboBoxOrderList.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jComboBoxOrderListFocusLost(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jComboBoxOrderList, gridBagConstraints);

        jLabel7.setFont(jLabel7.getFont());
        jLabel7.setText(bundle.getString("LIST_RECEIPT")); // NOI18N
        jLabel7.setMaximumSize(new java.awt.Dimension(1, 1));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jLabel7, gridBagConstraints);

        jComboBoxReceiptList.setFont(jComboBoxReceiptList.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jComboBoxReceiptList, gridBagConstraints);

        jCheckBoxActive.setFont(jCheckBoxActive.getFont());
        jCheckBoxActive.setText(bundle.getString("ACTIVE")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 15;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jCheckBoxActive, gridBagConstraints);

        jLabel37.setFont(jLabel37.getFont());
        jLabel37.setText("16 ������ҵðҹ");
        jLabel37.setMaximumSize(new java.awt.Dimension(1, 1));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 11;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jLabel37, gridBagConstraints);

        jComboBoxStandardGroup.setFont(jComboBoxStandardGroup.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jComboBoxStandardGroup, gridBagConstraints);

        jLabel8.setFont(jLabel8.getFont());
        jLabel8.setText("���ʡ���ѭ�ա�ҧ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jLabel8, gridBagConstraints);

        jTextFieldGeneralNumber.setFont(jTextFieldGeneralNumber.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jTextFieldGeneralNumber, gridBagConstraints);

        jCheckBox2.setFont(jCheckBox2.getFont());
        jCheckBox2.setText("��͹ᾷ�����ŧ  ICD10 ��͹�觼�����"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 13;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jCheckBox2, gridBagConstraints);

        jPanel18.setLayout(new java.awt.GridBagLayout());

        lblStatusInactive.setFont(lblStatusInactive.getFont());
        lblStatusInactive.setForeground(new java.awt.Color(255, 0, 0));
        lblStatusInactive.setText("*��¡�õ�Ǩ�ѡ�ҹ���������öź�͡�ҡ�ҹ�������� �ҡ����ͧ��� ������͡ active �͡");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        jPanel18.add(lblStatusInactive, gridBagConstraints);

        lblStatusNotMapTMT.setFont(lblStatusNotMapTMT.getFont());
        lblStatusNotMapTMT.setForeground(new java.awt.Color(255, 0, 0));
        lblStatusNotMapTMT.setText("*��¡�õ�Ǩ�ѡ�ҹ���ѧ�����Ѻ����������ҵðҹ TMT");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        jPanel18.add(lblStatusNotMapTMT, gridBagConstraints);

        lblStatusNotMap24.setFont(lblStatusNotMap24.getFont());
        lblStatusNotMap24.setForeground(new java.awt.Color(255, 0, 0));
        lblStatusNotMap24.setText("*��¡�õ�Ǩ�ѡ�ҹ���ѧ�����Ѻ��������� 24 ��ѡ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        jPanel18.add(lblStatusNotMap24, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 16;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jPanel18, gridBagConstraints);

        jLabel42.setFont(jLabel42.getFont());
        jLabel42.setText(bundle.getString("ITEM_NICK_NAME")); // NOI18N
        jLabel42.setMaximumSize(new java.awt.Dimension(1, 1));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jLabel42, gridBagConstraints);

        jScrollPane8.setMaximumSize(new java.awt.Dimension(150, 48));
        jScrollPane8.setMinimumSize(new java.awt.Dimension(150, 48));
        jScrollPane8.setPreferredSize(new java.awt.Dimension(150, 45));

        jTextAreaLocalName.setFont(jTextAreaLocalName.getFont());
        jTextAreaLocalName.setLineWrap(true);
        jTextAreaLocalName.setWrapStyleWord(true);
        jScrollPane8.setViewportView(jTextAreaLocalName);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jScrollPane8, gridBagConstraints);

        jPanel28.setLayout(new java.awt.GridBagLayout());

        lblPicture.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPicture.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        lblPicture.setMaximumSize(new java.awt.Dimension(90, 90));
        lblPicture.setMinimumSize(new java.awt.Dimension(90, 90));
        lblPicture.setPreferredSize(new java.awt.Dimension(90, 90));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel28.add(lblPicture, gridBagConstraints);

        btnAddPicture.setFont(btnAddPicture.getFont());
        btnAddPicture.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/plus16.gif"))); // NOI18N
        btnAddPicture.setMaximumSize(new java.awt.Dimension(28, 28));
        btnAddPicture.setMinimumSize(new java.awt.Dimension(28, 28));
        btnAddPicture.setPreferredSize(new java.awt.Dimension(28, 28));
        btnAddPicture.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddPictureActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        jPanel28.add(btnAddPicture, gridBagConstraints);

        btnDelPicture.setFont(btnDelPicture.getFont());
        btnDelPicture.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/minus16.gif"))); // NOI18N
        btnDelPicture.setMaximumSize(new java.awt.Dimension(28, 28));
        btnDelPicture.setMinimumSize(new java.awt.Dimension(28, 28));
        btnDelPicture.setPreferredSize(new java.awt.Dimension(28, 28));
        btnDelPicture.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDelPictureActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        jPanel28.add(btnDelPicture, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jPanel28, gridBagConstraints);

        jLabel73.setFont(jLabel73.getFont());
        jLabel73.setText("�ٻ�Ҿ��¡��");
        jLabel73.setMaximumSize(new java.awt.Dimension(1, 1));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jLabel73, gridBagConstraints);

        jCheckBoxItemNotUseToOrder.setFont(jCheckBoxItemNotUseToOrder.getFont());
        jCheckBoxItemNotUseToOrder.setText("���͹حҵ��������¡�õ�Ǩ�ѡ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 14;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jCheckBoxItemNotUseToOrder, gridBagConstraints);

        jTabbedPane1.addTab(bundle.getString("MEDSUPPLY"), jPanel5); // NOI18N

        jPanel6.setLayout(new java.awt.GridBagLayout());

        jScrollPane5.setMinimumSize(new java.awt.Dimension(250, 150));
        jScrollPane5.setPreferredSize(new java.awt.Dimension(250, 150));
        jScrollPane5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jScrollPane5MouseReleased(evt);
            }
        });

        tableItemPriceList.setFillsViewportHeight(true);
        tableItemPriceList.setFont(tableItemPriceList.getFont());
        tableItemPriceList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tableItemPriceListMouseReleased(evt);
            }
        });
        tableItemPriceList.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tableItemPriceListKeyReleased(evt);
            }
        });
        jScrollPane5.setViewportView(tableItemPriceList);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel6.add(jScrollPane5, gridBagConstraints);

        jPanel11.setLayout(new java.awt.GridBagLayout());

        jButtonAddPrice.setFont(jButtonAddPrice.getFont());
        jButtonAddPrice.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/plus16.gif"))); // NOI18N
        jButtonAddPrice.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonAddPrice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAddPriceActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel11.add(jButtonAddPrice, gridBagConstraints);

        jButtonDelPrice.setFont(jButtonDelPrice.getFont());
        jButtonDelPrice.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/minus16.gif"))); // NOI18N
        jButtonDelPrice.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonDelPrice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDelPriceActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel11.add(jButtonDelPrice, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel6.add(jPanel11, gridBagConstraints);

        panelItemPriceDetail.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        panelItemPriceDetail.setLayout(new java.awt.GridBagLayout());

        jLabel9.setFont(jLabel9.getFont());
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel9.setText(bundle.getString("DATE")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelItemPriceDetail.add(jLabel9, gridBagConstraints);

        jLabel13.setFont(jLabel13.getFont());
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel13.setText("�ҤҢ�� OPD*");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelItemPriceDetail.add(jLabel13, gridBagConstraints);

        jLabel30.setFont(jLabel30.getFont());
        jLabel30.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel30.setText("�Ҥҵ鹷ع");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelItemPriceDetail.add(jLabel30, gridBagConstraints);

        doubleTextFieldPriceCost.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        doubleTextFieldPriceCost.setText("0");
        doubleTextFieldPriceCost.setFont(doubleTextFieldPriceCost.getFont());
        doubleTextFieldPriceCost.setMinimumSize(new java.awt.Dimension(60, 21));
        doubleTextFieldPriceCost.setPreferredSize(new java.awt.Dimension(60, 21));
        doubleTextFieldPriceCost.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                doubleTextFieldPriceCostFocusGained(evt);
            }
        });
        doubleTextFieldPriceCost.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                doubleTextFieldPriceCostKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelItemPriceDetail.add(doubleTextFieldPriceCost, gridBagConstraints);

        doubleTextFieldPriceOPD.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        doubleTextFieldPriceOPD.setText("0");
        doubleTextFieldPriceOPD.setFont(doubleTextFieldPriceOPD.getFont());
        doubleTextFieldPriceOPD.setMinimumSize(new java.awt.Dimension(60, 21));
        doubleTextFieldPriceOPD.setPreferredSize(new java.awt.Dimension(60, 21));
        doubleTextFieldPriceOPD.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                doubleTextFieldPriceOPDFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                doubleTextFieldPriceOPDFocusLost(evt);
            }
        });
        doubleTextFieldPriceOPD.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                doubleTextFieldPriceOPDKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelItemPriceDetail.add(doubleTextFieldPriceOPD, gridBagConstraints);

        dateTextField.setFont(dateTextField.getFont());
        dateTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dateTextFieldActionPerformed(evt);
            }
        });
        dateTextField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                dateTextFieldFocusLost(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelItemPriceDetail.add(dateTextField, gridBagConstraints);

        jCheckBoxPlan.setFont(jCheckBoxPlan.getFont());
        jCheckBoxPlan.setText("����Է��");
        jCheckBoxPlan.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        jCheckBoxPlan.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jCheckBoxPlan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxPlanActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelItemPriceDetail.add(jCheckBoxPlan, gridBagConstraints);

        jComboBoxPlan.setEnabled(false);
        jComboBoxPlan.setFont(jComboBoxPlan.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelItemPriceDetail.add(jComboBoxPlan, gridBagConstraints);

        jLabel34.setFont(jLabel34.getFont());
        jLabel34.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel34.setText("�ҤҢ�� IPD*");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelItemPriceDetail.add(jLabel34, gridBagConstraints);

        doubleTextFieldPriceIPD.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        doubleTextFieldPriceIPD.setText("0");
        doubleTextFieldPriceIPD.setFont(doubleTextFieldPriceIPD.getFont());
        doubleTextFieldPriceIPD.setMinimumSize(new java.awt.Dimension(60, 21));
        doubleTextFieldPriceIPD.setPreferredSize(new java.awt.Dimension(60, 21));
        doubleTextFieldPriceIPD.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                doubleTextFieldPriceIPDFocusGained(evt);
            }
        });
        doubleTextFieldPriceIPD.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                doubleTextFieldPriceIPDKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelItemPriceDetail.add(doubleTextFieldPriceIPD, gridBagConstraints);

        jLabel43.setFont(jLabel43.getFont());
        jLabel43.setForeground(new java.awt.Color(255, 0, 0));
        jLabel43.setText("�����ǹ�觤��ᾷ�� ����ç��Һ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelItemPriceDetail.add(jLabel43, gridBagConstraints);

        jLabel44.setFont(jLabel44.getFont());
        jLabel44.setText("�ҷ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelItemPriceDetail.add(jLabel44, gridBagConstraints);

        jLabel45.setFont(jLabel45.getFont());
        jLabel45.setForeground(new java.awt.Color(255, 0, 0));
        jLabel45.setText("�����ǹ�觤��ᾷ�� ����ç��Һ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelItemPriceDetail.add(jLabel45, gridBagConstraints);

        jLabel46.setFont(jLabel46.getFont());
        jLabel46.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel46.setText("��ǹ�觤��ᾷ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelItemPriceDetail.add(jLabel46, gridBagConstraints);

        doubleTextFieldPriceDF.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        doubleTextFieldPriceDF.setText("0");
        doubleTextFieldPriceDF.setFont(doubleTextFieldPriceDF.getFont());
        doubleTextFieldPriceDF.setMinimumSize(new java.awt.Dimension(60, 21));
        doubleTextFieldPriceDF.setPreferredSize(new java.awt.Dimension(60, 21));
        doubleTextFieldPriceDF.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                doubleTextFieldPriceDFFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelItemPriceDetail.add(doubleTextFieldPriceDF, gridBagConstraints);

        jLabel47.setFont(jLabel47.getFont());
        jLabel47.setText("�ҷ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelItemPriceDetail.add(jLabel47, gridBagConstraints);

        jLabel48.setFont(jLabel48.getFont());
        jLabel48.setText("�ҷ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelItemPriceDetail.add(jLabel48, gridBagConstraints);

        jLabel49.setFont(jLabel49.getFont());
        jLabel49.setText("�ҷ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelItemPriceDetail.add(jLabel49, gridBagConstraints);

        jLabel53.setFont(jLabel53.getFont());
        jLabel53.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel53.setText("��ǹ���ç��Һ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelItemPriceDetail.add(jLabel53, gridBagConstraints);

        doubleTextFieldPriceHos.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        doubleTextFieldPriceHos.setText("0");
        doubleTextFieldPriceHos.setFont(doubleTextFieldPriceHos.getFont());
        doubleTextFieldPriceHos.setMinimumSize(new java.awt.Dimension(60, 21));
        doubleTextFieldPriceHos.setPreferredSize(new java.awt.Dimension(60, 21));
        doubleTextFieldPriceHos.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                doubleTextFieldPriceHosFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelItemPriceDetail.add(doubleTextFieldPriceHos, gridBagConstraints);

        jLabel54.setFont(jLabel54.getFont());
        jLabel54.setText("�ҷ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelItemPriceDetail.add(jLabel54, gridBagConstraints);

        jPanel20.setLayout(new java.awt.GridBagLayout());

        chkbCanEditPrice.setText("����ҤҢ�������¡����");
        chkbCanEditPrice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkbCanEditPriceActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel20.add(chkbCanEditPrice, gridBagConstraints);

        panelLimitPrice.setLayout(new java.awt.GridBagLayout());

        jLabel50.setFont(jLabel50.getFont());
        jLabel50.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel50.setText("��˹���ǧ�Ҥ�");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelLimitPrice.add(jLabel50, gridBagConstraints);

        doubleTextFieldPriceMin.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        doubleTextFieldPriceMin.setText("0");
        doubleTextFieldPriceMin.setFont(doubleTextFieldPriceMin.getFont());
        doubleTextFieldPriceMin.setMinimumSize(new java.awt.Dimension(60, 21));
        doubleTextFieldPriceMin.setPreferredSize(new java.awt.Dimension(60, 21));
        doubleTextFieldPriceMin.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                doubleTextFieldPriceMinFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelLimitPrice.add(doubleTextFieldPriceMin, gridBagConstraints);

        jLabel51.setFont(jLabel51.getFont());
        jLabel51.setText("�ҷ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelLimitPrice.add(jLabel51, gridBagConstraints);

        doubleTextFieldPriceMax.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        doubleTextFieldPriceMax.setText("0");
        doubleTextFieldPriceMax.setFont(doubleTextFieldPriceMax.getFont());
        doubleTextFieldPriceMax.setMinimumSize(new java.awt.Dimension(60, 21));
        doubleTextFieldPriceMax.setPreferredSize(new java.awt.Dimension(60, 21));
        doubleTextFieldPriceMax.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                doubleTextFieldPriceMaxFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelLimitPrice.add(doubleTextFieldPriceMax, gridBagConstraints);

        jLabel52.setFont(jLabel52.getFont());
        jLabel52.setText("-");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelLimitPrice.add(jLabel52, gridBagConstraints);

        jPanel20.add(panelLimitPrice, new java.awt.GridBagConstraints());

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        panelItemPriceDetail.add(jPanel20, gridBagConstraints);

        jLabel64.setFont(jLabel64.getFont());
        jLabel64.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel64.setText("�Ҥ��ԡ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelItemPriceDetail.add(jLabel64, gridBagConstraints);

        doubleTextFieldPriceClaim.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        doubleTextFieldPriceClaim.setText("0");
        doubleTextFieldPriceClaim.setFont(doubleTextFieldPriceClaim.getFont());
        doubleTextFieldPriceClaim.setMinimumSize(new java.awt.Dimension(60, 21));
        doubleTextFieldPriceClaim.setPreferredSize(new java.awt.Dimension(60, 21));
        doubleTextFieldPriceClaim.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                doubleTextFieldPriceClaimFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelItemPriceDetail.add(doubleTextFieldPriceClaim, gridBagConstraints);

        jLabel65.setFont(jLabel65.getFont());
        jLabel65.setText("�ҷ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelItemPriceDetail.add(jLabel65, gridBagConstraints);

        jLabel66.setFont(jLabel66.getFont());
        jLabel66.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel66.setText("�ѵ���Ҥ�");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelItemPriceDetail.add(jLabel66, gridBagConstraints);

        jComboBoxTariff.setEnabled(false);
        jComboBoxTariff.setFont(jComboBoxTariff.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelItemPriceDetail.add(jComboBoxTariff, gridBagConstraints);

        chkbUseClaimPrice.setText("���Ҥ��ԡ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelItemPriceDetail.add(chkbUseClaimPrice, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 0.9;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel6.add(panelItemPriceDetail, gridBagConstraints);

        jTabbedPane1.addTab(bundle.getString("PRICE"), jPanel6); // NOI18N

        jPanel7.setLayout(new java.awt.CardLayout());

        jPanelBlank.setLayout(new java.awt.GridBagLayout());
        jPanel7.add(jPanelBlank, "Blank");

        jPanelLabDescription.setLayout(new java.awt.BorderLayout());

        jPanel23.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel23.setMinimumSize(new java.awt.Dimension(10, 25));
        jPanel23.setLayout(new java.awt.GridBagLayout());

        buttonGroupLab.add(jRadioButtonLabDetail);
        jRadioButtonLabDetail.setFont(jRadioButtonLabDetail.getFont().deriveFont(jRadioButtonLabDetail.getFont().getStyle() | java.awt.Font.BOLD));
        jRadioButtonLabDetail.setSelected(true);
        jRadioButtonLabDetail.setText("LabDetail");
        jRadioButtonLabDetail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonLabDetailActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel23.add(jRadioButtonLabDetail, gridBagConstraints);

        buttonGroupLab.add(jRadioButtonLabGroup);
        jRadioButtonLabGroup.setFont(jRadioButtonLabGroup.getFont().deriveFont(jRadioButtonLabGroup.getFont().getStyle() | java.awt.Font.BOLD));
        jRadioButtonLabGroup.setText("LabGroup");
        jRadioButtonLabGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonLabGroupActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel23.add(jRadioButtonLabGroup, gridBagConstraints);

        jComboBoxLabRpGroup.setFont(jComboBoxLabRpGroup.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel23.add(jComboBoxLabRpGroup, gridBagConstraints);

        jComboBoxLabSpecimen.setFont(jComboBoxLabSpecimen.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 0.5;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel23.add(jComboBoxLabSpecimen, gridBagConstraints);

        jLabel14.setFont(jLabel14.getFont().deriveFont(jLabel14.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel14.setText("Specimen");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 13, 2, 2);
        jPanel23.add(jLabel14, gridBagConstraints);

        jLabel68.setFont(jLabel68.getFont().deriveFont(jLabel68.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel68.setText("ʶҹ����Ǩ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 13, 2, 2);
        jPanel23.add(jLabel68, gridBagConstraints);

        buttonGroupLabLocation.add(jRadioButtonLabIn);
        jRadioButtonLabIn.setFont(jRadioButtonLabIn.getFont());
        jRadioButtonLabIn.setSelected(true);
        jRadioButtonLabIn.setText("��Ǩ�ͧ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel23.add(jRadioButtonLabIn, gridBagConstraints);

        buttonGroupLabLocation.add(jRadioButtonLabOut);
        jRadioButtonLabOut.setFont(jRadioButtonLabOut.getFont());
        jRadioButtonLabOut.setText("�觵�Ǩ��¹͡");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel23.add(jRadioButtonLabOut, gridBagConstraints);

        jPanelLabDescription.add(jPanel23, java.awt.BorderLayout.NORTH);

        jPanel22.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel22.setMinimumSize(new java.awt.Dimension(462, 488));
        jPanel22.setLayout(new java.awt.CardLayout());

        jPanel21.setLayout(new java.awt.GridBagLayout());

        jPanel30.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel30.setLayout(new java.awt.GridBagLayout());

        jLabel12.setFont(jLabel12.getFont().deriveFont(jLabel12.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel12.setText("����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel30.add(jLabel12, gridBagConstraints);

        jTextFieldLabDetail.setFont(jTextFieldLabDetail.getFont());
        jTextFieldLabDetail.setMinimumSize(new java.awt.Dimension(77, 21));
        jTextFieldLabDetail.setPreferredSize(new java.awt.Dimension(77, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel30.add(jTextFieldLabDetail, gridBagConstraints);

        jLabel35.setFont(jLabel35.getFont().deriveFont(jLabel35.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel35.setText("˹���");
        jLabel35.setAutoscrolls(true);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel30.add(jLabel35, gridBagConstraints);

        jTextFieldLabUnit.setFont(jTextFieldLabUnit.getFont());
        jTextFieldLabUnit.setMinimumSize(new java.awt.Dimension(50, 21));
        jTextFieldLabUnit.setPreferredSize(new java.awt.Dimension(50, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel30.add(jTextFieldLabUnit, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel21.add(jPanel30, gridBagConstraints);

        jPanel31.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel31.setLayout(new java.awt.GridBagLayout());

        buttonGroupLabResultType.add(jRadioButtonNumber);
        jRadioButtonNumber.setFont(jRadioButtonNumber.getFont().deriveFont(jRadioButtonNumber.getFont().getStyle() | java.awt.Font.BOLD));
        jRadioButtonNumber.setText("����Ţ�ӹǹ���");
        jRadioButtonNumber.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonNumberActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel31.add(jRadioButtonNumber, gridBagConstraints);

        buttonGroupLabResultType.add(jRadioButtonPointNumber);
        jRadioButtonPointNumber.setFont(jRadioButtonPointNumber.getFont().deriveFont(jRadioButtonPointNumber.getFont().getStyle() | java.awt.Font.BOLD));
        jRadioButtonPointNumber.setText("����Ţ�ȹ���");
        jRadioButtonPointNumber.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonPointNumberActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel31.add(jRadioButtonPointNumber, gridBagConstraints);

        buttonGroupLabResultType.add(jRadioButtonText);
        jRadioButtonText.setFont(jRadioButtonText.getFont().deriveFont(jRadioButtonText.getFont().getStyle() | java.awt.Font.BOLD));
        jRadioButtonText.setSelected(true);
        jRadioButtonText.setText("��ͤ��� 1 ��÷Ѵ");
        jRadioButtonText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonTextActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel31.add(jRadioButtonText, gridBagConstraints);

        buttonGroupLabResultType.add(jRadioButtonTexts);
        jRadioButtonTexts.setFont(jRadioButtonTexts.getFont().deriveFont(jRadioButtonTexts.getFont().getStyle() | java.awt.Font.BOLD));
        jRadioButtonTexts.setText("���º�÷Ѵ");
        jRadioButtonTexts.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonTextsActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel31.add(jRadioButtonTexts, gridBagConstraints);

        jPanelDefaultValue.setLayout(new java.awt.GridBagLayout());

        jComboBoxList.setFont(jComboBoxList.getFont());
        jComboBoxList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxListActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelDefaultValue.add(jComboBoxList, gridBagConstraints);

        jTableList.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTableList.setFillsViewportHeight(true);
        jTableList.setFont(jTableList.getFont());
        jScrollPane13.setViewportView(jTableList);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelDefaultValue.add(jScrollPane13, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 15, 0, 0);
        jPanel31.add(jPanelDefaultValue, gridBagConstraints);

        buttonGroupLabResultType.add(jRadioButtonList);
        jRadioButtonList.setFont(jRadioButtonList.getFont().deriveFont(jRadioButtonList.getFont().getStyle() | java.awt.Font.BOLD));
        jRadioButtonList.setText("�ռ��ź����");
        jRadioButtonList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonListActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel31.add(jRadioButtonList, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel21.add(jPanel31, gridBagConstraints);

        panelNormalRange.setBorder(javax.swing.BorderFactory.createTitledBorder("��һ���"));
        panelNormalRange.setMinimumSize(new java.awt.Dimension(362, 125));
        panelNormalRange.setPreferredSize(new java.awt.Dimension(579, 122));
        panelNormalRange.setLayout(new java.awt.GridBagLayout());

        jLabel33.setFont(jLabel33.getFont().deriveFont(jLabel33.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel33.setText("��������һ���");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelNormalRange.add(jLabel33, gridBagConstraints);

        cbNormalrRangeType.setFont(cbNormalrRangeType.getFont());
        cbNormalrRangeType.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "����к�", "����㹪�ǧ �����", "����㹪�ǧ �����ǧ����", "�������ѡ��", "����㹪�ǧ ������͹� SQL" }));
        cbNormalrRangeType.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbNormalrRangeTypeActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelNormalRange.add(cbNormalrRangeType, gridBagConstraints);

        panelCardNormalRange.setLayout(new java.awt.CardLayout());

        panelNormalRangeByGender.setLayout(new java.awt.GridBagLayout());

        jLabel15.setFont(jLabel15.getFont().deriveFont(jLabel15.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel15.setText("�Ȫ��  ����㹪�ǧ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        panelNormalRangeByGender.add(jLabel15, gridBagConstraints);

        jLabel16.setFont(jLabel16.getFont().deriveFont(jLabel16.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel16.setText("��˭ԧ ����㹪�ǧ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(8, 5, 5, 5);
        panelNormalRangeByGender.add(jLabel16, gridBagConstraints);

        jLabel17.setFont(jLabel17.getFont().deriveFont(jLabel17.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel17.setText("�֧");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(8, 5, 5, 5);
        panelNormalRangeByGender.add(jLabel17, gridBagConstraints);

        jLabel18.setFont(jLabel18.getFont().deriveFont(jLabel18.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel18.setText("�֧");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        panelNormalRangeByGender.add(jLabel18, gridBagConstraints);

        txtGenderWM_Min.setDocument(doubleDocumentGenderWM_Min);
        txtGenderWM_Min.setFont(txtGenderWM_Min.getFont());
        txtGenderWM_Min.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtGenderWM_Min.setMaximumSize(new java.awt.Dimension(100, 24));
        txtGenderWM_Min.setMinimumSize(new java.awt.Dimension(100, 24));
        txtGenderWM_Min.setPreferredSize(new java.awt.Dimension(100, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        panelNormalRangeByGender.add(txtGenderWM_Min, gridBagConstraints);

        txtGenderWM_Max.setDocument(doubleDocumentGenderWM_Max);
        txtGenderWM_Max.setFont(txtGenderWM_Max.getFont());
        txtGenderWM_Max.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtGenderWM_Max.setMaximumSize(new java.awt.Dimension(100, 24));
        txtGenderWM_Max.setMinimumSize(new java.awt.Dimension(100, 24));
        txtGenderWM_Max.setPreferredSize(new java.awt.Dimension(100, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        panelNormalRangeByGender.add(txtGenderWM_Max, gridBagConstraints);

        txtGenderM_Max.setDocument(doubleDocumentGenderM_Max);
        txtGenderM_Max.setFont(txtGenderM_Max.getFont());
        txtGenderM_Max.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtGenderM_Max.setMaximumSize(new java.awt.Dimension(100, 24));
        txtGenderM_Max.setMinimumSize(new java.awt.Dimension(100, 24));
        txtGenderM_Max.setPreferredSize(new java.awt.Dimension(100, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        panelNormalRangeByGender.add(txtGenderM_Max, gridBagConstraints);

        txtGenderM_Min.setDocument(doubleDocumentGenderM_Min);
        txtGenderM_Min.setFont(txtGenderM_Min.getFont());
        txtGenderM_Min.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtGenderM_Min.setMaximumSize(new java.awt.Dimension(100, 24));
        txtGenderM_Min.setMinimumSize(new java.awt.Dimension(100, 24));
        txtGenderM_Min.setPreferredSize(new java.awt.Dimension(100, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        panelNormalRangeByGender.add(txtGenderM_Min, gridBagConstraints);

        jLabel55.setFont(jLabel55.getFont().deriveFont(jLabel55.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel55.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel55.setText("Min CV");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        panelNormalRangeByGender.add(jLabel55, gridBagConstraints);

        jLabel56.setFont(jLabel56.getFont().deriveFont(jLabel56.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel56.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel56.setText("Max");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        panelNormalRangeByGender.add(jLabel56, gridBagConstraints);

        jLabel57.setFont(jLabel57.getFont().deriveFont(jLabel57.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel57.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel57.setText("Min");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        panelNormalRangeByGender.add(jLabel57, gridBagConstraints);

        jLabel58.setFont(jLabel58.getFont().deriveFont(jLabel58.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel58.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel58.setText("Max CV");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 29, 5, 5);
        panelNormalRangeByGender.add(jLabel58, gridBagConstraints);

        txtGenderM_MinCV.setDocument(doubleDocumentGenderM_MinCV);
        txtGenderM_MinCV.setFont(txtGenderM_MinCV.getFont());
        txtGenderM_MinCV.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtGenderM_MinCV.setMaximumSize(new java.awt.Dimension(100, 24));
        txtGenderM_MinCV.setMinimumSize(new java.awt.Dimension(100, 24));
        txtGenderM_MinCV.setPreferredSize(new java.awt.Dimension(100, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        panelNormalRangeByGender.add(txtGenderM_MinCV, gridBagConstraints);

        txtGenderWM_MinCV.setDocument(doubleDocumentGenderWM_MinCV);
        txtGenderWM_MinCV.setFont(txtGenderWM_MinCV.getFont());
        txtGenderWM_MinCV.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtGenderWM_MinCV.setMaximumSize(new java.awt.Dimension(100, 24));
        txtGenderWM_MinCV.setMinimumSize(new java.awt.Dimension(100, 24));
        txtGenderWM_MinCV.setPreferredSize(new java.awt.Dimension(100, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        panelNormalRangeByGender.add(txtGenderWM_MinCV, gridBagConstraints);

        txtGenderM_MaxCV.setDocument(doubleDocumentGenderM_MaxCV);
        txtGenderM_MaxCV.setFont(txtGenderM_MaxCV.getFont());
        txtGenderM_MaxCV.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtGenderM_MaxCV.setMaximumSize(new java.awt.Dimension(100, 24));
        txtGenderM_MaxCV.setMinimumSize(new java.awt.Dimension(100, 24));
        txtGenderM_MaxCV.setPreferredSize(new java.awt.Dimension(100, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        panelNormalRangeByGender.add(txtGenderM_MaxCV, gridBagConstraints);

        txtGenderWM_MaxCV.setDocument(doubleDocumentGenderWM_MaxCV);
        txtGenderWM_MaxCV.setFont(txtGenderWM_MaxCV.getFont());
        txtGenderWM_MaxCV.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtGenderWM_MaxCV.setMaximumSize(new java.awt.Dimension(100, 24));
        txtGenderWM_MaxCV.setMinimumSize(new java.awt.Dimension(100, 24));
        txtGenderWM_MaxCV.setPreferredSize(new java.awt.Dimension(100, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        panelNormalRangeByGender.add(txtGenderWM_MaxCV, gridBagConstraints);

        panelCardNormalRange.add(panelNormalRangeByGender, "gender");

        panelNormalRangeByAges.setLayout(new java.awt.GridBagLayout());

        jPanel8.setBorder(javax.swing.BorderFactory.createTitledBorder("���͹�"));
        jPanel8.setLayout(new java.awt.GridBagLayout());

        jLabel19.setFont(jLabel19.getFont().deriveFont(jLabel19.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel19.setText("��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel8.add(jLabel19, gridBagConstraints);

        jLabel40.setFont(jLabel40.getFont().deriveFont(jLabel40.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel40.setText("��ǧ���� �����ҧ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_END;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel8.add(jLabel40, gridBagConstraints);

        jLabel41.setFont(jLabel41.getFont().deriveFont(jLabel41.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel41.setText("�֧");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_END;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel8.add(jLabel41, gridBagConstraints);

        txtAge_Min.setDocument(new NumberDocument());
        txtAge_Min.setFont(txtAge_Min.getFont());
        txtAge_Min.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtAge_Min.setMaximumSize(new java.awt.Dimension(70, 24));
        txtAge_Min.setMinimumSize(new java.awt.Dimension(70, 24));
        txtAge_Min.setPreferredSize(new java.awt.Dimension(70, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel8.add(txtAge_Min, gridBagConstraints);

        txtAge_Max.setDocument(new NumberDocument());
        txtAge_Max.setFont(txtAge_Max.getFont());
        txtAge_Max.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtAge_Max.setMaximumSize(new java.awt.Dimension(70, 24));
        txtAge_Max.setMinimumSize(new java.awt.Dimension(70, 24));
        txtAge_Max.setPreferredSize(new java.awt.Dimension(70, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel8.add(txtAge_Max, gridBagConstraints);

        jPanel24.setLayout(new java.awt.GridBagLayout());

        jLabel59.setFont(jLabel59.getFont().deriveFont(jLabel59.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel59.setText("�Ȫ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel24.add(jLabel59, gridBagConstraints);

        txtNRAgeM_Min.setDocument(doubleDocumentNRAgeM_Min);
        txtNRAgeM_Min.setFont(txtNRAgeM_Min.getFont());
        txtNRAgeM_Min.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtNRAgeM_Min.setMaximumSize(new java.awt.Dimension(100, 24));
        txtNRAgeM_Min.setMinimumSize(new java.awt.Dimension(100, 24));
        txtNRAgeM_Min.setPreferredSize(new java.awt.Dimension(100, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel24.add(txtNRAgeM_Min, gridBagConstraints);

        jLabel39.setFont(jLabel39.getFont().deriveFont(jLabel39.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel39.setText("�֧");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel24.add(jLabel39, gridBagConstraints);

        txtNRAgeM_Max.setDocument(doubleDocumentNRAgeM_Max);
        txtNRAgeM_Max.setFont(txtNRAgeM_Max.getFont());
        txtNRAgeM_Max.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtNRAgeM_Max.setMaximumSize(new java.awt.Dimension(100, 24));
        txtNRAgeM_Max.setMinimumSize(new java.awt.Dimension(100, 24));
        txtNRAgeM_Max.setPreferredSize(new java.awt.Dimension(100, 24));
        txtNRAgeM_Max.getDocument().addDocumentListener(ageNormalRangeListener);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel24.add(txtNRAgeM_Max, gridBagConstraints);

        jLabel20.setFont(jLabel20.getFont().deriveFont(jLabel20.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel20.setText("��˭ԧ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel24.add(jLabel20, gridBagConstraints);

        txtNRAgeWM_Min.setDocument(doubleDocumentNRAgeWM_Min);
        txtNRAgeWM_Min.setFont(txtNRAgeWM_Min.getFont());
        txtNRAgeWM_Min.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtNRAgeWM_Min.setMaximumSize(new java.awt.Dimension(100, 24));
        txtNRAgeWM_Min.setMinimumSize(new java.awt.Dimension(100, 24));
        txtNRAgeWM_Min.setPreferredSize(new java.awt.Dimension(100, 24));
        txtNRAgeWM_Min.getDocument().addDocumentListener(ageNormalRangeListener);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel24.add(txtNRAgeWM_Min, gridBagConstraints);

        jLabel31.setFont(jLabel31.getFont().deriveFont(jLabel31.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel31.setText("�֧");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel24.add(jLabel31, gridBagConstraints);

        txtNRAgeWM_Max.setDocument(doubleDocumentNRAgeWM_Max);
        txtNRAgeWM_Max.setFont(txtNRAgeWM_Max.getFont());
        txtNRAgeWM_Max.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtNRAgeWM_Max.setMaximumSize(new java.awt.Dimension(100, 24));
        txtNRAgeWM_Max.setMinimumSize(new java.awt.Dimension(100, 24));
        txtNRAgeWM_Max.setPreferredSize(new java.awt.Dimension(100, 24));
        txtNRAgeWM_Max.getDocument().addDocumentListener(ageNormalRangeListener);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel24.add(txtNRAgeWM_Max, gridBagConstraints);

        jLabel60.setFont(jLabel60.getFont().deriveFont(jLabel60.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel60.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel60.setText("Min CV");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel24.add(jLabel60, gridBagConstraints);

        jLabel61.setFont(jLabel61.getFont().deriveFont(jLabel61.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel61.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel61.setText("Min");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel24.add(jLabel61, gridBagConstraints);

        jLabel62.setFont(jLabel62.getFont().deriveFont(jLabel62.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel62.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel62.setText("Max");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel24.add(jLabel62, gridBagConstraints);

        jLabel63.setFont(jLabel63.getFont().deriveFont(jLabel63.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel63.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel63.setText("Max CV");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 27, 5, 5);
        jPanel24.add(jLabel63, gridBagConstraints);

        txtNRAgeM_MinCV.setDocument(doubleDocumentNRAgeM_MinCV);
        txtNRAgeM_MinCV.setFont(txtNRAgeM_MinCV.getFont());
        txtNRAgeM_MinCV.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtNRAgeM_MinCV.setMaximumSize(new java.awt.Dimension(100, 24));
        txtNRAgeM_MinCV.setMinimumSize(new java.awt.Dimension(100, 24));
        txtNRAgeM_MinCV.setPreferredSize(new java.awt.Dimension(100, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel24.add(txtNRAgeM_MinCV, gridBagConstraints);

        txtNRAgeWM_MinCV.setDocument(doubleDocumentNRAgeWM_MinCV);
        txtNRAgeWM_MinCV.setFont(txtNRAgeWM_MinCV.getFont());
        txtNRAgeWM_MinCV.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtNRAgeWM_MinCV.setMaximumSize(new java.awt.Dimension(100, 24));
        txtNRAgeWM_MinCV.setMinimumSize(new java.awt.Dimension(100, 24));
        txtNRAgeWM_MinCV.setPreferredSize(new java.awt.Dimension(100, 24));
        txtNRAgeWM_Min.getDocument().addDocumentListener(ageNormalRangeListener);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel24.add(txtNRAgeWM_MinCV, gridBagConstraints);

        txtNRAgeM_MaxCV.setDocument(doubleDocumentNRAgeM_MaxCV);
        txtNRAgeM_MaxCV.setFont(txtNRAgeM_MaxCV.getFont());
        txtNRAgeM_MaxCV.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtNRAgeM_MaxCV.setMaximumSize(new java.awt.Dimension(100, 24));
        txtNRAgeM_MaxCV.setMinimumSize(new java.awt.Dimension(100, 24));
        txtNRAgeM_MaxCV.setPreferredSize(new java.awt.Dimension(100, 24));
        txtNRAgeM_Max.getDocument().addDocumentListener(ageNormalRangeListener);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel24.add(txtNRAgeM_MaxCV, gridBagConstraints);

        txtNRAgeWM_MaxCV.setDocument(doubleDocumentNRAgeWM_MaxCV);
        txtNRAgeWM_MaxCV.setFont(txtNRAgeWM_MaxCV.getFont());
        txtNRAgeWM_MaxCV.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtNRAgeWM_MaxCV.setMaximumSize(new java.awt.Dimension(100, 24));
        txtNRAgeWM_MaxCV.setMinimumSize(new java.awt.Dimension(100, 24));
        txtNRAgeWM_MaxCV.setPreferredSize(new java.awt.Dimension(100, 24));
        txtNRAgeWM_Max.getDocument().addDocumentListener(ageNormalRangeListener);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel24.add(txtNRAgeWM_MaxCV, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.weightx = 1.0;
        jPanel8.add(jPanel24, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        panelNormalRangeByAges.add(jPanel8, gridBagConstraints);

        tableAgeNormalRange.setFont(tableAgeNormalRange.getFont());
        tableAgeNormalRange.setFillsViewportHeight(true);
        tableAgeNormalRange.setRowHeight(20);
        tableAgeNormalRange.setSelectionMode(javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        tableAgeNormalRange.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tableAgeNormalRangeMouseReleased(evt);
            }
        });
        tableAgeNormalRange.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tableAgeNormalRangeKeyReleased(evt);
            }
        });
        jScrollPane7.setViewportView(tableAgeNormalRange);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelNormalRangeByAges.add(jScrollPane7, gridBagConstraints);

        btnAddAgeNormalRange.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/btn_add24.png"))); // NOI18N
        btnAddAgeNormalRange.setEnabled(false);
        btnAddAgeNormalRange.setMaximumSize(new java.awt.Dimension(30, 30));
        btnAddAgeNormalRange.setMinimumSize(new java.awt.Dimension(30, 30));
        btnAddAgeNormalRange.setPreferredSize(new java.awt.Dimension(30, 30));
        btnAddAgeNormalRange.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddAgeNormalRangeActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelNormalRangeByAges.add(btnAddAgeNormalRange, gridBagConstraints);

        btnRemoveAgeNormalRange.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/btn_delete24.png"))); // NOI18N
        btnRemoveAgeNormalRange.setEnabled(false);
        btnRemoveAgeNormalRange.setMaximumSize(new java.awt.Dimension(30, 30));
        btnRemoveAgeNormalRange.setMinimumSize(new java.awt.Dimension(30, 30));
        btnRemoveAgeNormalRange.setPreferredSize(new java.awt.Dimension(30, 30));
        btnRemoveAgeNormalRange.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveAgeNormalRangeActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelNormalRangeByAges.add(btnRemoveAgeNormalRange, gridBagConstraints);

        panelCardNormalRange.add(panelNormalRangeByAges, "ages");

        panelNormalRangeByTexts.setLayout(new java.awt.GridBagLayout());

        txtNormalRangeText.setFont(txtNormalRangeText.getFont());
        txtNormalRangeText.getDocument().addDocumentListener(textNormalRangeListener);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelNormalRangeByTexts.add(txtNormalRangeText, gridBagConstraints);

        btnAddTextNormalRange.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/btn_add24.png"))); // NOI18N
        btnAddTextNormalRange.setEnabled(false);
        btnAddTextNormalRange.setMaximumSize(new java.awt.Dimension(30, 30));
        btnAddTextNormalRange.setMinimumSize(new java.awt.Dimension(30, 30));
        btnAddTextNormalRange.setPreferredSize(new java.awt.Dimension(30, 30));
        btnAddTextNormalRange.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddTextNormalRangeActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelNormalRangeByTexts.add(btnAddTextNormalRange, gridBagConstraints);

        btnDeleteTextNormalRange.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/btn_delete24.png"))); // NOI18N
        btnDeleteTextNormalRange.setEnabled(false);
        btnDeleteTextNormalRange.setMaximumSize(new java.awt.Dimension(30, 30));
        btnDeleteTextNormalRange.setMinimumSize(new java.awt.Dimension(30, 30));
        btnDeleteTextNormalRange.setPreferredSize(new java.awt.Dimension(30, 30));
        btnDeleteTextNormalRange.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteTextNormalRangeActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelNormalRangeByTexts.add(btnDeleteTextNormalRange, gridBagConstraints);

        tableTextNormalRange.setFont(tableTextNormalRange.getFont());
        tableTextNormalRange.setModel(tmds);
        tableTextNormalRange.setFillsViewportHeight(true);
        tableTextNormalRange.setRowHeight(20);
        tableTextNormalRange.setSelectionMode(javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        tableTextNormalRange.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tableTextNormalRangeMouseReleased(evt);
            }
        });
        tableTextNormalRange.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tableTextNormalRangeKeyReleased(evt);
            }
        });
        jScrollPane6.setViewportView(tableTextNormalRange);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelNormalRangeByTexts.add(jScrollPane6, gridBagConstraints);

        panelCardNormalRange.add(panelNormalRangeByTexts, "texts");

        panelNormalRangeBySql.setLayout(new java.awt.GridBagLayout());

        jTextFieldDefaultValue.setColumns(20);
        jTextFieldDefaultValue.setFont(jTextFieldDefaultValue.getFont());
        jTextFieldDefaultValue.setLineWrap(true);
        jTextFieldDefaultValue.setRows(5);
        jTextFieldDefaultValue.setText("select \ncase when visit_age<'10' then '0' \n        when visit_age > '10' then '1'\n        else '2' end as min\n,case when visit_age<'10' then '10' \n        when visit_age > '10' then '11'\n        else '20' end as max \nfrom t_visit where t_visit_id = ?");
        jTextFieldDefaultValue.setWrapStyleWord(true);
        jScrollPane11.setViewportView(jTextFieldDefaultValue);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelNormalRangeBySql.add(jScrollPane11, gridBagConstraints);

        panelCardNormalRange.add(panelNormalRangeBySql, "sql");

        panelNA.setLayout(new java.awt.GridBagLayout());

        lblNormalRangeMsg.setFont(lblNormalRangeMsg.getFont().deriveFont(lblNormalRangeMsg.getFont().getStyle() | java.awt.Font.BOLD));
        lblNormalRangeMsg.setForeground(new java.awt.Color(255, 0, 0));
        lblNormalRangeMsg.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        panelNA.add(lblNormalRangeMsg, gridBagConstraints);

        panelCardNormalRange.add(panelNA, "na");

        jScrollPane12.setViewportView(panelCardNormalRange);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelNormalRange.add(jScrollPane12, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel21.add(panelNormalRange, gridBagConstraints);

        jPanel15.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel15.setLayout(new java.awt.GridBagLayout());

        jCheckBoxSecret.setFont(jCheckBoxSecret.getFont().deriveFont(jCheckBoxSecret.getFont().getStyle() | java.awt.Font.BOLD));
        jCheckBoxSecret.setText("���Դ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel15.add(jCheckBoxSecret, gridBagConstraints);

        jCheckBoxNCDFBS.setFont(jCheckBoxNCDFBS.getFont().deriveFont(jCheckBoxNCDFBS.getFont().getStyle() | java.awt.Font.BOLD));
        jCheckBoxNCDFBS.setText("�дѺ��ӵ������ʹ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel15.add(jCheckBoxNCDFBS, gridBagConstraints);

        jCheckBoxNCDHCT.setFont(jCheckBoxNCDHCT.getFont().deriveFont(jCheckBoxNCDHCT.getFont().getStyle() | java.awt.Font.BOLD));
        jCheckBoxNCDHCT.setText("��������鹢ͧ������ʹᴧ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel15.add(jCheckBoxNCDHCT, gridBagConstraints);

        jCheckBoxDuration.setFont(jCheckBoxDuration.getFont().deriveFont(jCheckBoxDuration.getFont().getStyle() | java.awt.Font.BOLD));
        jCheckBoxDuration.setText("��Ǩ�ͺ��ǧ�������");
        jCheckBoxDuration.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxDurationActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel15.add(jCheckBoxDuration, gridBagConstraints);

        jTextFieldDurationDate.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldDurationDate.setToolTipText("");
        jTextFieldDurationDate.setEnabled(false);
        jTextFieldDurationDate.setFont(jTextFieldDurationDate.getFont());
        jTextFieldDurationDate.setMinimumSize(new java.awt.Dimension(50, 24));
        jTextFieldDurationDate.setPreferredSize(new java.awt.Dimension(50, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel15.add(jTextFieldDurationDate, gridBagConstraints);

        jLabelAmountDate.setFont(jLabelAmountDate.getFont().deriveFont(jLabelAmountDate.getFont().getStyle() | java.awt.Font.BOLD));
        jLabelAmountDate.setText("�ѹ");
        jLabelAmountDate.setEnabled(false);
        jLabelAmountDate.setMaximumSize(new java.awt.Dimension(70, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel15.add(jLabelAmountDate, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel21.add(jPanel15, gridBagConstraints);

        jPanel26.setBorder(javax.swing.BorderFactory.createTitledBorder("�����ż�Ե�ѳ��"));
        jPanel26.setFont(jPanel26.getFont());
        jPanel26.setLayout(new java.awt.GridBagLayout());

        jLabel69.setFont(jLabel69.getFont().deriveFont(jLabel69.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel69.setText("�Ţ����Ե�ѳ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel26.add(jLabel69, gridBagConstraints);

        cbAtkProduct.setEditable(true);
        cbAtkProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbAtkProductActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel26.add(cbAtkProduct, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel21.add(jPanel26, gridBagConstraints);

        jPanel22.add(jPanel21, "cardlabdetail");

        jPanel10.setLayout(new java.awt.GridBagLayout());

        jLabel32.setFont(jLabel32.getFont());
        jLabel32.setText(bundle.getString("SUB_LIST")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(9, 5, 5, 5);
        jPanel10.add(jLabel32, gridBagConstraints);

        jTable4.setFillsViewportHeight(true);
        jTable4.setFont(jTable4.getFont());
        jTable4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTable4MouseReleased(evt);
            }
        });
        jScrollPane10.setViewportView(jTable4);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 5, 5);
        jPanel10.add(jScrollPane10, gridBagConstraints);

        jPanel51.setLayout(new java.awt.GridBagLayout());

        jButtonDelLabItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/minus16.gif"))); // NOI18N
        jButtonDelLabItem.setMaximumSize(new java.awt.Dimension(13, 22));
        jButtonDelLabItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDelLabItemActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 0);
        jPanel51.add(jButtonDelLabItem, gridBagConstraints);

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/plus16.gif"))); // NOI18N
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel51.add(jButton3, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel10.add(jPanel51, gridBagConstraints);

        jPanel22.add(jPanel10, "cardlabgroup");

        jPanelLabDescription.add(jPanel22, java.awt.BorderLayout.CENTER);

        jPanel7.add(jPanelLabDescription, "LabDescription");

        jPanelIcdCode.setLayout(new java.awt.GridBagLayout());

        jLabel11.setFont(jLabel11.getFont());
        jLabel11.setText("�����ѵ����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelIcdCode.add(jLabel11, gridBagConstraints);

        jLabel36.setFont(jLabel36.getFont());
        jLabel36.setText("������¡���ѵ����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelIcdCode.add(jLabel36, gridBagConstraints);

        jTextFieldDescription.setFont(jTextFieldDescription.getFont());
        jTextFieldDescription.setMinimumSize(new java.awt.Dimension(70, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelIcdCode.add(jTextFieldDescription, gridBagConstraints);

        jLabel10.setFont(jLabel10.getFont());
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel10.setText("�Ѻ����Һ�ԡ�áѺ�����ѵ����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelIcdCode.add(jLabel10, gridBagConstraints);

        jTextFieldIcdCode.setFont(jTextFieldIcdCode.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelIcdCode.add(jTextFieldIcdCode, gridBagConstraints);

        jScrollPane14.setMinimumSize(new java.awt.Dimension(104, 61));
        jScrollPane14.setPreferredSize(new java.awt.Dimension(104, 61));

        balloonTextArea1.setWrapStyleWord(true);
        balloonTextArea1.setFont(balloonTextArea1.getFont());
        jScrollPane14.setViewportView(balloonTextArea1);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelIcdCode.add(jScrollPane14, gridBagConstraints);

        jPanel7.add(jPanelIcdCode, "ServiceDescription");
        jPanel7.add(jPanelDrugDescription, "DrugDescription");

        panelSupply.setLayout(new java.awt.GridBagLayout());

        cbSupplyPrintable.setFont(cbSupplyPrintable.getFont());
        cbSupplyPrintable.setText("�����ʵԡ����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        panelSupply.add(cbSupplyPrintable, gridBagConstraints);

        jPanel16.setLayout(new java.awt.GridBagLayout());

        chkbSupplyPrintMar.setFont(chkbSupplyPrintMar.getFont());
        chkbSupplyPrintMar.setText("������ Mar");
        chkbSupplyPrintMar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkbSupplyPrintMarActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel16.add(chkbSupplyPrintMar, gridBagConstraints);

        cbSupplyMarType.setFont(cbSupplyMarType.getFont());
        cbSupplyMarType.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "����Ѻ��", "����Ѻ��ù��" }));
        cbSupplyMarType.setEnabled(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel16.add(cbSupplyMarType, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        panelSupply.add(jPanel16, gridBagConstraints);

        jPanel25.setLayout(new java.awt.GridBagLayout());

        jLabel67.setFont(jLabel67.getFont());
        jLabel67.setText("��������´�����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel25.add(jLabel67, gridBagConstraints);

        jScrollPane15.setMaximumSize(new java.awt.Dimension(363, 55));
        jScrollPane15.setMinimumSize(new java.awt.Dimension(363, 53));

        txtSupplement.setFont(txtSupplement.getFont());
        txtSupplement.setLineWrap(true);
        txtSupplement.setMinimumSize(new java.awt.Dimension(360, 50));
        txtSupplement.setPreferredSize(new java.awt.Dimension(360, 50));
        jScrollPane15.setViewportView(txtSupplement);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel25.add(jScrollPane15, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        panelSupply.add(jPanel25, gridBagConstraints);

        jPanel7.add(panelSupply, "SUPPLY");

        panelXrayDescription.setLayout(new java.awt.GridBagLayout());

        jPanel19.setLayout(new java.awt.GridBagLayout());

        jLabel38.setText("Modality :");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel19.add(jLabel38, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel19.add(combModality, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        panelXrayDescription.add(jPanel19, gridBagConstraints);

        jPanel7.add(panelXrayDescription, "XrayDescription");

        jTabbedPane1.addTab(bundle.getString("PANEL_SETUP_ITEM_DETAIL"), jPanel7); // NOI18N

        jPanel9.setLayout(new java.awt.GridBagLayout());

        jTable3.setFillsViewportHeight(true);
        jTable3.setFont(jTable3.getFont());
        jScrollPane9.setViewportView(jTable3);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel9.add(jScrollPane9, gridBagConstraints);

        jPanel111.setLayout(new java.awt.GridBagLayout());

        jButtonAddPrice1.setFont(jButtonAddPrice1.getFont());
        jButtonAddPrice1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/plus16.gif"))); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 2);
        jPanel111.add(jButtonAddPrice1, gridBagConstraints);

        jButtonDelPrice1.setFont(jButtonDelPrice1.getFont());
        jButtonDelPrice1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/minus16.gif"))); // NOI18N
        jButtonDelPrice1.setEnabled(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 5, 5);
        jPanel111.add(jButtonDelPrice1, gridBagConstraints);

        jButtonSavePrice1.setFont(jButtonSavePrice1.getFont());
        jButtonSavePrice1.setText(bundle.getString("SAVE")); // NOI18N
        jButtonSavePrice1.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonSavePrice1.setPreferredSize(new java.awt.Dimension(60, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 5, 5);
        jPanel111.add(jButtonSavePrice1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 5, 5);
        jPanel9.add(jPanel111, gridBagConstraints);

        jPanel12.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel12.setLayout(new java.awt.GridBagLayout());

        jLabel21.setFont(jLabel21.getFont());
        jLabel21.setText(bundle.getString("RECEIVE")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel12.add(jLabel21, gridBagConstraints);

        jTextField1.setBackground(new java.awt.Color(204, 204, 204));
        jTextField1.setFont(jTextField1.getFont());
        jTextField1.setMaximumSize(new java.awt.Dimension(48, 24));
        jTextField1.setMinimumSize(new java.awt.Dimension(50, 21));
        jTextField1.setPreferredSize(new java.awt.Dimension(50, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 5, 2);
        jPanel12.add(jTextField1, gridBagConstraints);

        jLabel22.setFont(jLabel22.getFont());
        jLabel22.setText(bundle.getString("PAY")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 5, 2);
        jPanel12.add(jLabel22, gridBagConstraints);

        jTextField2.setBackground(new java.awt.Color(204, 204, 204));
        jTextField2.setFont(jTextField2.getFont());
        jTextField2.setMaximumSize(new java.awt.Dimension(48, 24));
        jTextField2.setMinimumSize(new java.awt.Dimension(50, 21));
        jTextField2.setPreferredSize(new java.awt.Dimension(50, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 5, 5);
        jPanel12.add(jTextField2, gridBagConstraints);

        jLabel23.setFont(jLabel23.getFont());
        jLabel23.setText(bundle.getString("REMAIN")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 5, 2);
        jPanel12.add(jLabel23, gridBagConstraints);

        jTextField3.setBackground(new java.awt.Color(204, 204, 204));
        jTextField3.setFont(jTextField3.getFont());
        jTextField3.setMaximumSize(new java.awt.Dimension(48, 24));
        jTextField3.setMinimumSize(new java.awt.Dimension(50, 21));
        jTextField3.setPreferredSize(new java.awt.Dimension(50, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 5, 2);
        jPanel12.add(jTextField3, gridBagConstraints);

        jButtonPrev1.setFont(jButtonPrev1.getFont());
        jButtonPrev1.setMinimumSize(new java.awt.Dimension(24, 24));
        jButtonPrev1.setPreferredSize(new java.awt.Dimension(24, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 5, 5);
        jPanel12.add(jButtonPrev1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 5, 5);
        jPanel9.add(jPanel12, gridBagConstraints);

        jPanel13.setLayout(new java.awt.GridBagLayout());

        jLabel24.setFont(jLabel24.getFont());
        jLabel24.setText(bundle.getString("DATE")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 5, 5);
        jPanel13.add(jLabel24, gridBagConstraints);

        jTextField4.setFont(jTextField4.getFont());
        jTextField4.setMaximumSize(new java.awt.Dimension(26, 24));
        jTextField4.setMinimumSize(new java.awt.Dimension(50, 21));
        jTextField4.setPreferredSize(new java.awt.Dimension(50, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 5, 5);
        jPanel13.add(jTextField4, gridBagConstraints);

        jLabel25.setFont(jLabel25.getFont());
        jLabel25.setText("/");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 5, 5);
        jPanel13.add(jLabel25, gridBagConstraints);

        jTextField5.setFont(jTextField5.getFont());
        jTextField5.setMaximumSize(new java.awt.Dimension(26, 24));
        jTextField5.setMinimumSize(new java.awt.Dimension(50, 21));
        jTextField5.setPreferredSize(new java.awt.Dimension(50, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 5, 5);
        jPanel13.add(jTextField5, gridBagConstraints);

        jLabel26.setFont(jLabel26.getFont());
        jLabel26.setText("/");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 5, 5);
        jPanel13.add(jLabel26, gridBagConstraints);

        jTextField6.setFont(jTextField6.getFont());
        jTextField6.setMaximumSize(new java.awt.Dimension(48, 24));
        jTextField6.setMinimumSize(new java.awt.Dimension(50, 21));
        jTextField6.setPreferredSize(new java.awt.Dimension(50, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 5, 5);
        jPanel13.add(jTextField6, gridBagConstraints);

        jLabel27.setFont(jLabel27.getFont());
        jLabel27.setText(bundle.getString("DATE_RECEIVE")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        jPanel13.add(jLabel27, gridBagConstraints);

        jTextField7.setBackground(new java.awt.Color(204, 204, 204));
        jTextField7.setFont(jTextField7.getFont());
        jTextField7.setBorder(null);
        jTextField7.setMaximumSize(new java.awt.Dimension(150, 24));
        jTextField7.setMinimumSize(new java.awt.Dimension(150, 24));
        jTextField7.setPreferredSize(new java.awt.Dimension(150, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        jPanel13.add(jTextField7, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 5, 5);
        jPanel9.add(jPanel13, gridBagConstraints);

        jPanel14.setLayout(new java.awt.GridBagLayout());

        jLabel28.setFont(jLabel28.getFont());
        jLabel28.setText(bundle.getString("DRUG_QTY")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 5, 5);
        jPanel14.add(jLabel28, gridBagConstraints);

        jTextField8.setFont(jTextField8.getFont());
        jTextField8.setMaximumSize(new java.awt.Dimension(150, 24));
        jTextField8.setMinimumSize(new java.awt.Dimension(50, 21));
        jTextField8.setPreferredSize(new java.awt.Dimension(50, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 5, 5);
        jPanel14.add(jTextField8, gridBagConstraints);

        jComboBox2.setFont(jComboBox2.getFont());
        jComboBox2.setMaximumSize(new java.awt.Dimension(150, 24));
        jComboBox2.setMinimumSize(new java.awt.Dimension(150, 24));
        jComboBox2.setPreferredSize(new java.awt.Dimension(150, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 5, 5);
        jPanel14.add(jComboBox2, gridBagConstraints);

        jLabel29.setFont(jLabel29.getFont());
        jLabel29.setText(bundle.getString("TOTAL")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        jPanel14.add(jLabel29, gridBagConstraints);

        jTextField9.setFont(jTextField9.getFont());
        jTextField9.setMaximumSize(new java.awt.Dimension(150, 24));
        jTextField9.setMinimumSize(new java.awt.Dimension(50, 21));
        jTextField9.setPreferredSize(new java.awt.Dimension(50, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        jPanel14.add(jTextField9, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 5, 5);
        jPanel9.add(jPanel14, gridBagConstraints);

        jTabbedPane1.addTab(bundle.getString("MED_STOCK"), jPanel9); // NOI18N

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel2.add(jTabbedPane1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        gridBagConstraints.weightx = 0.6;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 5, 5);
        add(jPanel2, gridBagConstraints);

        jPanel1.setLayout(new java.awt.GridBagLayout());

        jButtonAdd.setFont(jButtonAdd.getFont());
        jButtonAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/plus16.gif"))); // NOI18N
        jButtonAdd.setMaximumSize(new java.awt.Dimension(24, 24));
        jButtonAdd.setMinimumSize(new java.awt.Dimension(24, 24));
        jButtonAdd.setPreferredSize(new java.awt.Dimension(24, 24));
        jButtonAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAddActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel1.add(jButtonAdd, gridBagConstraints);

        jButtonDel.setFont(jButtonDel.getFont());
        jButtonDel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/minus16.gif"))); // NOI18N
        jButtonDel.setMaximumSize(new java.awt.Dimension(24, 24));
        jButtonDel.setMinimumSize(new java.awt.Dimension(24, 24));
        jButtonDel.setPreferredSize(new java.awt.Dimension(24, 24));
        jButtonDel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDelActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        jPanel1.add(jButtonDel, gridBagConstraints);

        jButtonSave.setFont(jButtonSave.getFont());
        jButtonSave.setText(bundle.getString("SAVE")); // NOI18N
        jButtonSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSaveActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        jPanel1.add(jButtonSave, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTHEAST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 5, 5);
        add(jPanel1, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jCheckBoxPlanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxPlanActionPerformed
        this.jComboBoxPlan.setEnabled(jCheckBoxPlan.isSelected());
    }//GEN-LAST:event_jCheckBoxPlanActionPerformed

    private void tableItemPriceListMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableItemPriceListMouseReleased
        this.doSelectedPrice();
    }//GEN-LAST:event_tableItemPriceListMouseReleased

    private void jScrollPane5MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jScrollPane5MouseReleased

    }//GEN-LAST:event_jScrollPane5MouseReleased

    private void jRadioButtonNumberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonNumberActionPerformed
        chooseLabType();
    }//GEN-LAST:event_jRadioButtonNumberActionPerformed

    private void jRadioButtonPointNumberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonPointNumberActionPerformed
        chooseLabType();
    }//GEN-LAST:event_jRadioButtonPointNumberActionPerformed

    private void jRadioButtonTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonTextActionPerformed
        chooseLabType();
    }//GEN-LAST:event_jRadioButtonTextActionPerformed

    private void jRadioButtonTextsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonTextsActionPerformed
        chooseLabType();
    }//GEN-LAST:event_jRadioButtonTextsActionPerformed

    private void jRadioButtonListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonListActionPerformed
        chooseLabType();
    }//GEN-LAST:event_jRadioButtonListActionPerformed

    private void jComboBoxListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxListActionPerformed
        this.doSelectedLabDetail();
    }//GEN-LAST:event_jComboBoxListActionPerformed

    private void doubleTextFieldPriceCostKeyReleased(java.awt.event.KeyEvent evt)//GEN-FIRST:event_doubleTextFieldPriceCostKeyReleased
    {//GEN-HEADEREND:event_doubleTextFieldPriceCostKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            jButtonSaveActionPerformed(null);
        }
    }//GEN-LAST:event_doubleTextFieldPriceCostKeyReleased

    private void doubleTextFieldPriceOPDKeyReleased(java.awt.event.KeyEvent evt)//GEN-FIRST:event_doubleTextFieldPriceOPDKeyReleased
    {//GEN-HEADEREND:event_doubleTextFieldPriceOPDKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_DOWN || evt.getKeyCode() == KeyEvent.VK_ENTER) {
            doubleTextFieldPriceIPD.requestFocus();
        }
    }//GEN-LAST:event_doubleTextFieldPriceOPDKeyReleased

    private void jCheckBoxSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxSActionPerformed
        //pu : 25/07/2549 : ��˹�������Ѻ Index ����Ѻ�кض֧˹�һѨ�غѹ�ͧ��¡�õ�Ǩ�ѡ��
        this.curNext = 0;
        this.curPrev = 0;
        searchItemGroup();
    }//GEN-LAST:event_jCheckBoxSActionPerformed

    private void jRadioButtonConsistActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonConsistActionPerformed
        //pu : 25/07/2549 : ��˹�������Ѻ Index ����Ѻ�кض֧˹�һѨ�غѹ�ͧ��¡�õ�Ǩ�ѡ��
        this.curNext = 0;
        this.curPrev = 0;
        searchItemGroup();
    }//GEN-LAST:event_jRadioButtonConsistActionPerformed

    private void jRadioButtonBeginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonBeginActionPerformed
        //pu : 25/07/2549 : ��˹�������Ѻ Index ����Ѻ�кض֧˹�һѨ�غѹ�ͧ��¡�õ�Ǩ�ѡ��
        this.curNext = 0;
        this.curPrev = 0;
        searchItemGroup();
    }//GEN-LAST:event_jRadioButtonBeginActionPerformed

    private void jTextFieldSCodeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldSCodeKeyReleased
        /**
         * ����ö������¡�õ�Ǩ�ѡ�� �¡���кؤӤ����ͧ����
         * ���Ǥ�������ѵ��ѵ� (����ͧ�� Enter ����) ���͡��¡�õ�Ǩ�ѡ��
         * ���¡�á������١�â��-ŧ ��ѧ�ҡ������¡�õ�Ǩ�ѡ������
         *
         * @Modifier pu
         * @Date 25/07/2549
         */
        if (evt.getKeyCode() == KeyEvent.VK_DOWN) {
            jTable1.requestFocus();
            jTable1.setRowSelectionInterval(0, 0);
            selectItemGroup();
        }
        if (jTextFieldSCode.getText().length() > 2) {
            this.jTextFieldSCodeActionPerformed(null);
        }
    }//GEN-LAST:event_jTextFieldSCodeKeyReleased

    private void jRadioButtonLabGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonLabGroupActionPerformed
        showLabProperty("cardlabgroup");
    }//GEN-LAST:event_jRadioButtonLabGroupActionPerformed

    private void dateTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dateTextFieldActionPerformed
    }//GEN-LAST:event_dateTextFieldActionPerformed

    private void dateTextFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_dateTextFieldFocusLost
    }//GEN-LAST:event_dateTextFieldFocusLost

    private void jComboBoxSCategoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxSCategoryActionPerformed
        //pu : 25/07/2549 : ��˹�������Ѻ Index ����Ѻ�кض֧˹�һѨ�غѹ�ͧ��¡�õ�Ǩ�ѡ��
        this.curNext = 0;
        this.curPrev = 0;
        searchItemGroup();
    }//GEN-LAST:event_jComboBoxSCategoryActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        addLabGroupItemGroup();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButtonAddPriceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAddPriceActionPerformed
        setItemPrice(new ItemPrice());
    }//GEN-LAST:event_jButtonAddPriceActionPerformed

    private void jTable4MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable4MouseReleased
    }//GEN-LAST:event_jTable4MouseReleased

    private void jButtonDelLabItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDelLabItemActionPerformed
        deleteLabSet();
    }//GEN-LAST:event_jButtonDelLabItemActionPerformed

    private void jRadioButtonLabDetailActionPerformed(java.awt.event.ActionEvent evt) {
        showLabProperty("cardlabdetail");
    }
    private void jComboBoxOrderListItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBoxOrderListItemStateChanged
    }//GEN-LAST:event_jComboBoxOrderListItemStateChanged

    private void jComboBoxOrderListMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jComboBoxOrderListMouseClicked
        selectOrderItemGroup();
    }//GEN-LAST:event_jComboBoxOrderListMouseClicked

    private void jComboBoxOrderListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxOrderListActionPerformed
        selectOrderItemGroup();
    }//GEN-LAST:event_jComboBoxOrderListActionPerformed

    private void jCheckBoxSearchGroupMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jCheckBoxSearchGroupMouseClicked
        checkItemGroup();
    }//GEN-LAST:event_jCheckBoxSearchGroupMouseClicked

    private void jCheckBoxSearchGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxSearchGroupActionPerformed
        //pu : 25/07/2549 : ��˹�������Ѻ Index ����Ѻ�кض֧˹�һѨ�غѹ�ͧ��¡�õ�Ǩ�ѡ��
        this.curNext = 0;
        this.curPrev = 0;
        searchItemGroup();
    }//GEN-LAST:event_jCheckBoxSearchGroupActionPerformed

    private void jButtonDelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDelActionPerformed
        deleteItemGroup();
    }//GEN-LAST:event_jButtonDelActionPerformed

    private void jTextFieldSCodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldSCodeActionPerformed
        //pu : 25/07/2549 : ��˹�������Ѻ Index ����Ѻ�кض֧˹�һѨ�غѹ�ͧ��¡�õ�Ǩ�ѡ��
        this.curNext = 0;
        this.curPrev = 0;
        searchItemGroup();
    }//GEN-LAST:event_jTextFieldSCodeActionPerformed

    private void jButtonSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSaveActionPerformed
        Item item = getItem();
        boolean isNew = item.getObjectId() == null;
        boolean isActive = item.active.equals("1");
        int ret = theSetupControl.saveItem(item,
                getItemPrice(),
                jPanelDrugDescription.getDrug(),
                getLabResultItem(),
                getLabGroup(),
                getLabSetV(),
                jRadioButtonLabGroup.isSelected(),
                getItemService(),
                getItemSupply(),
                getItemXray());
        if (ret > 0) {
            if (jTable1.getRowCount() > 0 && jTable1.getSelectedRow() >= 0) {
                int index = jTable1.getSelectedRow();
                int count = next - prev;
                this.curNext = next - count;
                this.curPrev = prev - offset;
                searchItemGroup();
                if (!isNew && isActive) {
                    jTable1.setRowSelectionInterval(index, index);
                    selectItemGroup();
                }
            }
        }
    }//GEN-LAST:event_jButtonSaveActionPerformed

    private void jButtonAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAddActionPerformed
        this.addNewItem();
        jTextFieldCode.requestFocus();
    }//GEN-LAST:event_jButtonAddActionPerformed

    private void jButtonNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonNextActionPerformed
        nextItemGroup();
    }//GEN-LAST:event_jButtonNextActionPerformed

    private void jButtonPrevActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPrevActionPerformed
        prevItemGroup();
    }//GEN-LAST:event_jButtonPrevActionPerformed

    private void jButtonSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSearchActionPerformed
        //pu : 25/07/2549 : ��˹�������Ѻ Index ����Ѻ�кض֧˹�һѨ�غѹ�ͧ��¡�õ�Ǩ�ѡ��
        this.curNext = 0;
        this.curPrev = 0;
        searchItemGroup();
    }//GEN-LAST:event_jButtonSearchActionPerformed

    private void jTable1MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseReleased
        selectItemGroup();
    }//GEN-LAST:event_jTable1MouseReleased

    private void jTable1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyReleased
        if ((evt.getKeyCode() == KeyEvent.VK_DOWN) || (evt.getKeyCode() == KeyEvent.VK_UP)) {
            selectItemGroup();
        }
	}//GEN-LAST:event_jTable1KeyReleased

    private void jButtonDelPriceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDelPriceActionPerformed

        //henbe comment 100253 kong ��ͧ��Ѻ��� pattern �ͧ��� setObject(null)
        this.deletePriceItemGroup();
        setItemPrice(null);
}//GEN-LAST:event_jButtonDelPriceActionPerformed

    private void jComboBoxOrderListFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jComboBoxOrderListFocusLost
        this.selectOrderItemGroup();
    }//GEN-LAST:event_jComboBoxOrderListFocusLost

    private void cbNormalrRangeTypeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbNormalrRangeTypeActionPerformed
        this.doSwitchNormalRangePanel();
    }//GEN-LAST:event_cbNormalrRangeTypeActionPerformed

    private void tableTextNormalRangeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tableTextNormalRangeKeyReleased
        btnDeleteTextNormalRange.setEnabled(tableTextNormalRange.getSelectedRowCount() > 0);
    }//GEN-LAST:event_tableTextNormalRangeKeyReleased

    private void tableTextNormalRangeMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableTextNormalRangeMouseReleased
        btnDeleteTextNormalRange.setEnabled(tableTextNormalRange.getSelectedRowCount() > 0);
    }//GEN-LAST:event_tableTextNormalRangeMouseReleased

    private void btnAddTextNormalRangeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddTextNormalRangeActionPerformed
        doAddTextNormalRange();
    }//GEN-LAST:event_btnAddTextNormalRangeActionPerformed

    private void btnDeleteTextNormalRangeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteTextNormalRangeActionPerformed
        doRemoveTextNormalRange();
    }//GEN-LAST:event_btnDeleteTextNormalRangeActionPerformed

    private void btnAddAgeNormalRangeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddAgeNormalRangeActionPerformed
        doAddAgeNormalRange();
    }//GEN-LAST:event_btnAddAgeNormalRangeActionPerformed

    private void btnRemoveAgeNormalRangeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveAgeNormalRangeActionPerformed
        doRemoveAgeNormalRange();
    }//GEN-LAST:event_btnRemoveAgeNormalRangeActionPerformed

    private void tableAgeNormalRangeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tableAgeNormalRangeKeyReleased
        btnRemoveAgeNormalRange.setEnabled(tableAgeNormalRange.getSelectedRowCount() > 0);
    }//GEN-LAST:event_tableAgeNormalRangeKeyReleased

    private void tableAgeNormalRangeMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableAgeNormalRangeMouseReleased
        btnRemoveAgeNormalRange.setEnabled(tableAgeNormalRange.getSelectedRowCount() > 0);
    }//GEN-LAST:event_tableAgeNormalRangeMouseReleased

    private void tableItemPriceListKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tableItemPriceListKeyReleased
        this.doSelectedPrice();
    }//GEN-LAST:event_tableItemPriceListKeyReleased

    private void chkbSupplyPrintMarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkbSupplyPrintMarActionPerformed
        cbSupplyMarType.setEnabled(chkbSupplyPrintMar.isSelected());
    }//GEN-LAST:event_chkbSupplyPrintMarActionPerformed

    private void doubleTextFieldPriceIPDKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_doubleTextFieldPriceIPDKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_DOWN || evt.getKeyCode() == KeyEvent.VK_ENTER) {
            doubleTextFieldPriceIPD.requestFocus();
        }
    }//GEN-LAST:event_doubleTextFieldPriceIPDKeyReleased

    private void doubleTextFieldPriceOPDFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_doubleTextFieldPriceOPDFocusLost
        if (doubleTextFieldPriceIPD.getText().trim().isEmpty() || Double.parseDouble(doubleTextFieldPriceIPD.getText().trim()) == 0.0d) {
            doubleTextFieldPriceIPD.setText(doubleTextFieldPriceOPD.getText());
        }
    }//GEN-LAST:event_doubleTextFieldPriceOPDFocusLost

    private void doubleTextFieldPriceOPDFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_doubleTextFieldPriceOPDFocusGained
        doubleTextFieldPriceOPD.selectAll();
    }//GEN-LAST:event_doubleTextFieldPriceOPDFocusGained

    private void doubleTextFieldPriceIPDFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_doubleTextFieldPriceIPDFocusGained
        doubleTextFieldPriceIPD.selectAll();
    }//GEN-LAST:event_doubleTextFieldPriceIPDFocusGained

    private void doubleTextFieldPriceCostFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_doubleTextFieldPriceCostFocusGained
        doubleTextFieldPriceCost.selectAll();
    }//GEN-LAST:event_doubleTextFieldPriceCostFocusGained

    private void doubleTextFieldPriceDFFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_doubleTextFieldPriceDFFocusGained
        doubleTextFieldPriceDF.selectAll();
    }//GEN-LAST:event_doubleTextFieldPriceDFFocusGained

    private void doubleTextFieldPriceMinFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_doubleTextFieldPriceMinFocusGained
        doubleTextFieldPriceMin.selectAll();
    }//GEN-LAST:event_doubleTextFieldPriceMinFocusGained

    private void chkbCanEditPriceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkbCanEditPriceActionPerformed
        panelLimitPrice.setVisible(chkbCanEditPrice.isSelected());
    }//GEN-LAST:event_chkbCanEditPriceActionPerformed

    private void doubleTextFieldPriceMaxFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_doubleTextFieldPriceMaxFocusGained
        doubleTextFieldPriceMax.selectAll();
    }//GEN-LAST:event_doubleTextFieldPriceMaxFocusGained

    private void doubleTextFieldPriceHosFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_doubleTextFieldPriceHosFocusGained
        doubleTextFieldPriceHos.selectAll();
    }//GEN-LAST:event_doubleTextFieldPriceHosFocusGained

    private void doubleTextFieldPriceClaimFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_doubleTextFieldPriceClaimFocusGained
        doubleTextFieldPriceClaim.selectAll();
    }//GEN-LAST:event_doubleTextFieldPriceClaimFocusGained

    private void jCheckBoxDurationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxDurationActionPerformed
        jTextFieldDurationDate.setEnabled(jCheckBoxDuration.isSelected());
        jLabelAmountDate.setEnabled(jCheckBoxDuration.isSelected());
        if (!jCheckBoxDuration.isSelected()) {
            jTextFieldDurationDate.setText("0");
        }
    }//GEN-LAST:event_jCheckBoxDurationActionPerformed

    private void cbAtkProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbAtkProductActionPerformed
        if (evt.getActionCommand().equals("comboBoxEdited")) {
            String keyword = String.valueOf(cbAtkProduct.getSelectedItem());
            if (cbAtkProduct.getSelectedIndex() >= 0) {
                CommonInf commonInf = (CommonInf) cbAtkProduct.getSelectedItem();
                if (commonInf.getName().trim().equals(keyword.trim())) {
                    return;
                }
            }
            List<CommonInf> v = theHC.theLookupControl.listLabAtkProductByKeyword(keyword.trim());
            if (v != null && !v.isEmpty() && v.size() > 1) {
                LabAtkProduct undefine = new LabAtkProduct();
                undefine.setObjectId(null);
                undefine.lab_atk_device_name = "����к�";
                if (keyword.trim().isEmpty()) {
                    v.add(0, undefine);
                }
            }
            if (v == null) {
                v = new ArrayList<>();
            }
            if (!v.isEmpty()) {
                ComboboxModel.initComboBox(cbAtkProduct, v);
            }
        }
    }//GEN-LAST:event_cbAtkProductActionPerformed

    private void btnAddPictureActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddPictureActionPerformed
        doAddItemPicture();
    }//GEN-LAST:event_btnAddPictureActionPerformed

    private void btnDelPictureActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDelPictureActionPerformed
        doDeleteItemPicture();
    }//GEN-LAST:event_btnDelPictureActionPerformed

    private void nextItemGroup() {
        setItemV(vItem, 1);
    }

    private void prevItemGroup() {
        setItemV(vItem, 0);
    }

    private void deleteLabSet() {
        int[] row = jTable4.getSelectedRows();
        if (row.length == 0) {
            theUS.setStatus("��س����͡��¡�÷���ͧ���ź", UpdateStatus.WARNING);
            return;
        }
        theSetupControl.deleteLabSet(vLabSet, row);
        setLabSetV(vLabSet);
    }

    private void deleteItemGroup() {
        int ret = theSetupControl.deleteItemByPk(theItem.getObjectId());
        if (ret == 0) {
            return;
        }
        setEnabled(false);
        clearAll();
        //pu : 25/07/2549 : �� Index �Ѩ�غѹ�ͧ˹����¡�õ�Ǩ�ѡ�ҷ����ѧ�ѹ�֡
        int count = next - prev;
        this.curNext = next - count;
        this.curPrev = prev - offset;
        searchItemGroup();
    }

    private void deletePriceItemGroup() {
        int row = tableItemPriceList.getSelectedRow();
        if (row != -1) {
            theItemPrice = (ItemPrice) vItemPrice.get(row);
            if (theItemPrice.getObjectId() != null) {
                int ret = theSetupControl.deleteItemPrice(theItemPrice);
                if (ret == 0) {
                    return;
                }
                vItemPrice.remove(row);
            } else {
                vItemPrice.remove(row);
            }
            setItemPriceV(vItemPrice);
        }
    }

    private void showLabProperty(String type) {
        CardLayout layout = (CardLayout) jPanel22.getLayout();
        layout.show(jPanel22, type);
    }

    private ItemPrice getItemPrice() {
        if (theItemPrice == null) {
            return null;
        }
        //pu:25/08/2549 : ������㹡�úѹ�֡��ҹ�Ҥ� Item ���� ������� sort ��١��ͧ�͹�ʴ�㹵��ҧ
        if (theItemPrice.getObjectId() == null) {
            theItemPrice = new ItemPrice();
            theItemPrice.item_price_id = this.jCheckBoxPlan.isSelected()
                    ? ComboboxModel.getCodeComboBox(jComboBoxPlan) : "";
            theItemPrice.b_tariff_id = ComboboxModel.getCodeComboBox(jComboBoxTariff);

            theItemPrice.active_date = dateTextField.getText() + "," + DateUtil.getTextCurrentTime(this.theHC.theConnectionInf);
            theItemPrice.price = Constant.toDouble(doubleTextFieldPriceOPD.getText());
            theItemPrice.price_ipd = Constant.toDouble(doubleTextFieldPriceIPD.getText());
            theItemPrice.price_cost = Constant.toDouble(doubleTextFieldPriceCost.getText());
            theItemPrice.item_price_claim = Constant.toDouble(doubleTextFieldPriceClaim.getText());
            theItemPrice.item_share_doctor = Constant.toDouble(doubleTextFieldPriceDF.getText());
            theItemPrice.item_share_hospital = Constant.toDouble(doubleTextFieldPriceHos.getText());
            theItemPrice.use_price_claim = chkbUseClaimPrice.isSelected();
            theItemPrice.item_editable_price = chkbCanEditPrice.isSelected() ? "1" : "0";
            theItemPrice.item_limit_price_min = chkbCanEditPrice.isSelected()
                    ? Constant.toDouble(doubleTextFieldPriceMin.getText()) : 0;
            theItemPrice.item_limit_price_max = chkbCanEditPrice.isSelected()
                    ? Constant.toDouble(doubleTextFieldPriceMax.getText()) : 0;
            return theItemPrice;
        } else {
            return null;
        }
    }

    private void setItemPriceV(Vector price) {
        vItemPrice = price;
        String[] column = {"�ѵ���Ҥ�", "�ѹ���", "�ҤҢ�� OPD", "�ҤҢ�� IPD", "�Ҥҷع", "�Ҥ��ԡ��", "��ǹ�觤��ᾷ��"};
        TaBleModel tm;
        if (price != null) {
            tm = new TaBleModel(column, price.size());
            for (int i = 0; i < price.size(); i++) {
                ItemPrice itemprice = (ItemPrice) price.get(i);
                tm.setValueAt(itemprice.tariffName, i, 0);
                try {
                    tm.setValueAt(DateUtil.getDateToString(DateUtil.getDateFromText(itemprice.active_date), false), i, 1);
                } catch (Exception ex) {
                    tm.setValueAt(itemprice.active_date, i, 1);
                }
                tm.setValueAt(itemprice.price, i, 2);
                tm.setValueAt(itemprice.price_ipd, i, 3);
                tm.setValueAt(itemprice.price_cost, i, 4);
                tm.setValueAt(itemprice.item_price_claim, i, 5);
                tm.setValueAt(itemprice.item_share_doctor, i, 6);
            }
        } else {
            tm = new TaBleModel(column, 0);
        }
        tableItemPriceList.setModel(tm);
        tableItemPriceList.getColumnModel().getColumn(0).setPreferredWidth(150);
        tableItemPriceList.getColumnModel().getColumn(1).setPreferredWidth(180);
        tableItemPriceList.getColumnModel().getColumn(2).setPreferredWidth(100);
        tableItemPriceList.getColumnModel().getColumn(2).setCellRenderer(cellRendererCurrency);
        tableItemPriceList.getColumnModel().getColumn(3).setPreferredWidth(100);
        tableItemPriceList.getColumnModel().getColumn(3).setCellRenderer(cellRendererCurrency);
        tableItemPriceList.getColumnModel().getColumn(4).setPreferredWidth(100);
        tableItemPriceList.getColumnModel().getColumn(4).setCellRenderer(cellRendererCurrency);
        tableItemPriceList.getColumnModel().getColumn(5).setPreferredWidth(100);
        tableItemPriceList.getColumnModel().getColumn(5).setCellRenderer(cellRendererCurrency);
        tableItemPriceList.getColumnModel().getColumn(6).setPreferredWidth(100);
        tableItemPriceList.getColumnModel().getColumn(6).setCellRenderer(cellRendererCurrency);
        tableItemPriceList.clearSelection();
        doSelectedPrice();
    }

    private void setEnableComponents(JPanel panel, boolean isEnable) {
        for (Component c : panel.getComponents()) {
            if (c instanceof JPanel) {
                JPanel p = (JPanel) c;
                setEnableComponents(p, isEnable);
                continue;
            }
            c.setEnabled(isEnable);
        }
    }

    private void setItemPrice(ItemPrice ip) {
        theItemPrice = ip;
        if (theItemPrice == null) {
            theItemPrice = new ItemPrice();
            theItemPrice.setObjectId("thisIdForFlagToCanNotSave");
            panelItemPriceDetail.setVisible(false);
            jButtonDelPrice.setEnabled(false);
            return;
        }
        panelItemPriceDetail.setVisible(true);
        jButtonDelPrice.setEnabled(true);
        boolean isEnable = theItemPrice.getObjectId() == null;
        setEnableComponents(panelItemPriceDetail, isEnable);
        jCheckBoxPlan.setSelected(theItemPrice.item_price_id != null
                && !theItemPrice.item_price_id.isEmpty());

        jComboBoxPlan.refresh();
        jComboBoxPlan.setEnabled(isEnable && jCheckBoxPlan.isSelected());
        ComboboxModel.setCodeComboBox(jComboBoxPlan, theItemPrice.item_price_id);

        boolean isEnableTariff = "1".equals(theHC.theLookupControl.readOption(true).enable_tariff);
        jComboBoxTariff.setEnabled(isEnable && isEnableTariff);
        ComboboxModel.setCodeComboBox(jComboBoxTariff, theItemPrice.b_tariff_id);

        if (theItemPrice.active_date == null) {
            dateTextField.setText(null);
        } else {
            String date = theItemPrice.active_date.substring(0, 10);
            String dateArr[] = date.split("-");
            date = dateArr[2] + "/" + dateArr[1] + "/" + dateArr[0];
            dateTextField.setText(date);
        }

        doubleTextFieldPriceCost.setText(Constant.doubleToDBString(theItemPrice.price_cost));
        doubleTextFieldPriceOPD.setText(Constant.doubleToDBString(theItemPrice.price));
        doubleTextFieldPriceIPD.setText(Constant.doubleToDBString(theItemPrice.price_ipd));
        doubleTextFieldPriceClaim.setText(Constant.doubleToDBString(theItemPrice.item_price_claim));
        doubleTextFieldPriceDF.setText(Constant.doubleToDBString(theItemPrice.item_share_doctor));
        doubleTextFieldPriceHos.setText(Constant.doubleToDBString(theItemPrice.item_share_hospital));

        chkbUseClaimPrice.setSelected(theItemPrice.use_price_claim);

        chkbCanEditPrice.setSelected(theItemPrice.item_editable_price.equals("1"));
        panelLimitPrice.setVisible(chkbCanEditPrice.isSelected());
        doubleTextFieldPriceMin.setText(Constant.doubleToDBString(theItemPrice.item_limit_price_min));
        doubleTextFieldPriceMax.setText(Constant.doubleToDBString(theItemPrice.item_limit_price_max));

        doubleTextFieldPriceOPD.requestFocus();
    }

//it is lookup of lab ���������ѡ�Ӥѭ����ͧʹ�
    private void setLabResultDetailV(Vector v) {
        TaBleModel tm;
        if (v != null) {
            tm = new TaBleModel(col_list, v.size());
            for (int i = 0; i < v.size(); i++) {
                LabResultDetail ld = (LabResultDetail) v.get(i);
                tm.setValueAt(ld.value, i, 0);
            }
        } else {
            tm = new TaBleModel(col_list, 0);
        }
        jTableList.setModel(tm);
    }

    private Vector getLabSetV() {
        for (int i = 0; i < vLabSet.size(); i++) {
            LabSet ls = (LabSet) vLabSet.get(i);
            ls.item_name = String.valueOf(jTable4.getValueAt(i, 1));
        }
        return vLabSet;
    }

    private boolean setLabSetV(Vector vls) {
        String[] column = {Constant.getTextBundle("Item"), Constant.getTextBundle("�ӴѺ"), Constant.getTextBundle("�����")};
        this.vLabSet = vls;
        if (vLabSet == null) {
            vLabSet = new Vector();
        }

        TaBleModel tm = new TaBleModel(column, vLabSet.size());
        for (int i = 0; i < vLabSet.size(); i++) {
            LabSet ls = (LabSet) vLabSet.get(i);
            tm.setValueAt(ls.item_common_name, i, 0);
            tm.setValueAt(ls.item_name, i, 1);
            tm.setValueAt(ls.has_sub, i, 2);
        }
        tm.setEditingCol(1);
        jTable4.setModel(tm);

        jTable4.getColumnModel().getColumn(0).setPreferredWidth(300);
        jTable4.getColumnModel().getColumn(1).setCellRenderer(TableRenderer.getRendererRight());
        jTable4.getColumnModel().getColumn(1).setPreferredWidth(100);
        jTable4.getColumnModel().getColumn(2).setCellRenderer(TableRenderer.getRendererRight());
        jTable4.getColumnModel().getColumn(2).setPreferredWidth(100);
        return true;
    }

    private void clearAll() {   //����
        jTextFieldCode.setText("");
        jTextAreaCommonName.setText("");
        jTextAreaTradeName.setText("");
        jTextAreaNickName.setText("");
        jTextAreaLocalName.setText("");
        jTextFieldGeneralNumber.setText("");
        jCheckBoxActive.setSelected(true);
        jCheckBox2.setSelected(false);
        jCheckBoxItemNotUseToOrder.setSelected(false);
        //price
        ComboboxModel.setCodeComboBox(jComboBoxTariff, "5707000000001");
        doubleTextFieldPriceOPD.setText("0");
        doubleTextFieldPriceIPD.setText("0");
        doubleTextFieldPriceCost.setText("0");
        doubleTextFieldPriceClaim.setText("0");
        doubleTextFieldPriceDF.setText("0");
        doubleTextFieldPriceHos.setText("0");
        doubleTextFieldPriceMin.setText("0");
        doubleTextFieldPriceMax.setText("0");
        chkbUseClaimPrice.setSelected(false);
        chkbCanEditPrice.setSelected(false);
        // Dose
        this.jPanelDrugDescription.clearAll();
        // Lab
        jTextFieldLabDetail.setText("");
        jTextFieldLabUnit.setText("");
        jRadioButtonLabGroup.setEnabled(true);
        jRadioButtonLabDetail.setEnabled(true);
        showLabProperty("cardlabdetail");
        vItemPrice = new Vector();
        vLabSet = new Vector();
        setLabResultItem(null);
        setLabSetV(null);
        setItemPriceV(null);
        setItemService(null);
        setItemSupply(null);
        setItemXray(null);
        theItem = new Item();
        theItemPrice = null;
        theLabGroup = new LabGroup();
        jCheckBoxSecret.setSelected(false);
        jCheckBoxNCDFBS.setSelected(false);
        jCheckBoxNCDHCT.setSelected(false);
        jCheckBoxDuration.setSelected(false);
        jTextFieldDurationDate.setEnabled(false);
        jLabelAmountDate.setEnabled(false);

        jRadioButtonText.setSelected(true);
        jTextFieldDefaultValue.setText("");
        this.jComboBoxLabRpGroup.setSelectedIndex(0);
        this.jComboBoxLabSpecimen.setSelectedIndex(0);
        this.cbNormalrRangeType.setSelectedIndex(0);
        txtGenderM_Min.setText("");
        txtGenderM_Max.setText("");
        txtGenderWM_Min.setText("");
        txtGenderWM_Max.setText("");
        txtGenderM_MinCV.setText("");
        txtGenderM_MaxCV.setText("");
        txtGenderWM_MinCV.setText("");
        txtGenderWM_MaxCV.setText("");
        txtNormalRangeText.setText("");
        tmds.clearTable();
        btnAddTextNormalRange.setEnabled(false);
        btnDeleteTextNormalRange.setEnabled(false);
        doValidateTextNormalRange();
        txtAge_Min.setText("");
        txtAge_Max.setText("");
        txtNRAgeM_Min.setText("");
        txtNRAgeM_Max.setText("");
        txtNRAgeWM_Min.setText("");
        txtNRAgeWM_Max.setText("");
        txtNRAgeM_MinCV.setText("");
        txtNRAgeM_MaxCV.setText("");
        txtNRAgeWM_MinCV.setText("");
        txtNRAgeWM_MaxCV.setText("");
        tmcds.clearTable();
        btnAddAgeNormalRange.setEnabled(false);
        btnRemoveAgeNormalRange.setEnabled(false);
        doValidateAgeNormalRange();
        this.combModality.setSelectedIndex(0);
        setItemPicture(null);
    }

    @Override
    public void setEnabled(boolean var) {
        jButtonDel.setEnabled(var);
        jTextFieldCode.setToolTipText("");
        jCheckBoxNCDFBS.setEnabled(var);
        jCheckBoxNCDHCT.setEnabled(var);
        jCheckBoxDuration.setEnabled(var);
    }

    /**
     * ����� combobox ����ö�ӧҹ�� ���� �����
     */
    private void checkItemGroup() {
        if (jCheckBoxSearchGroup.isSelected()) {
            jComboBoxSCategory.setEnabled(true);
        } else {
            jComboBoxSCategory.setEnabled(false);
        }
    }

    /**
     * �ӡ�ä��� item ������������ ��� ���ͷ���ͧ��ä���
     */
    private void searchItemGroup() {   //pu : 25/07/2549 : ��˹���� Index ���Ѻ˹�ҷ���ͧ����ʴ���¡�õ�Ǩ�ѡ��
        next = this.curNext;
        prev = this.curPrev;
        String itemname = jTextFieldSCode.getText();
        String itemgp = "";
        if (jCheckBoxSearchGroup.isSelected()) {
            itemgp = ComboboxModel.getCodeComboBox(jComboBoxSCategory);
        }
        String active = "0";
        if (jCheckBoxS.isSelected()) {
            active = "1";
        }

        if (itemname.isEmpty() && itemgp.isEmpty()) {
            theUS.setStatus("��سҡ�͡�Ӥ鹡�͹��ä���", UpdateStatus.WARNING);
            return;
        }
        boolean begin_with = this.jRadioButtonBegin.isSelected();
        vItem = theSetupControl.listItemByGroup(itemgp, itemname, active, begin_with, true);
        setItemV(vItem, 1);
    }

    /**
     * tong
     */
    private CategoryGroupItem selectOrderItemGroup() {
        String codeorderitem = ComboboxModel.getCodeComboBox(jComboBoxOrderList);
        CategoryGroupItem cgitem = theLookupControl.readCategoryGroupItemById(codeorderitem);
        if (cgitem != null) {
            category = Integer.parseInt(cgitem.category_group_code);
            if (cgitem.category_group_code.equals(CategoryGroup.isDrug())) {
                showOrderProperty("DrugDescription");
            } else if (cgitem.category_group_code.equals(CategoryGroup.isLab())) {//Lab
                showOrderProperty("LabDescription");
            } else if (cgitem.category_group_code.equals(CategoryGroup.isXray())) {//Xray
                showOrderProperty("XrayDescription");
            } else if (cgitem.category_group_code.equals(CategoryGroup.isService()) || cgitem.category_group_code.equals(CategoryGroup.isDental())) {//��Һ�ԡ�� ��� �ѹ�����
                showOrderProperty("ServiceDescription");
            } else if (cgitem.category_group_code.equals(CategoryGroup.isSupply())) {//�Ǫ�ѳ��
                showOrderProperty("SUPPLY");
            } else {// other such as xray
                showOrderProperty("Blank");
            }
        }
        return cgitem;
    }

    private void showOrderProperty(String type) {
        CardLayout layout = (CardLayout) jPanel7.getLayout();
        layout.show(jPanel7, type);
    }

    private void selectItemGroup() {
        setEnabled(true);
        clearAll();
        int row = jTable1.getSelectedRow();
        this.currentRow = jTable1.getSelectedRow();
        theItem = (Item) vItem.get(row + prev);
        setItem(theItem);
    }

    private void doAddItemPicture() {
        if (theItem != null) {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.addChoosableFileFilter(new FileFilter() {
                //Accept all directories and all gif, jpg, tiff, or png files.
                @Override
                public boolean accept(File f) {
                    if (f.isDirectory()) {
                        return true;
                    }
                    String extension = ImageUtils.getExtension(f);
                    if (extension != null) {
                        return extension.equals(ImageUtils.TIFF)
                                || extension.equals(ImageUtils.TIF)
                                || extension.equals(ImageUtils.GIF)
                                || extension.equals(ImageUtils.JPEG)
                                || extension.equals(ImageUtils.JPG)
                                || extension.equals(ImageUtils.PNG);
                    }
                    return false;
                }

                //The description of this filter
                @Override
                public String getDescription() {
                    return "���͡�Ҿ";
                }
            });
            fileChooser.setAcceptAllFileFilterUsed(false);
            int result = fileChooser.showOpenDialog(this);
            if (result == JFileChooser.APPROVE_OPTION) {
                File selectedFile = fileChooser.getSelectedFile();
                try {
                    BufferedImage inputImage = ImageIO.read(selectedFile);
                    inputImage = ImageUtils.fit(inputImage, 90, 90);
                    theItem.item_picture = ImageUtils.toByteArray(inputImage);
                    setItemPicture(inputImage);
                    if (theItem.getObjectId() != null) {
                        theHC.theSetupControl.updateItemPicture(theItem);
                    }
                } catch (IOException ex) {
                }
            }
        }
    }

    private void doDeleteItemPicture() {
        if (theItem != null) {
            if (theItem.getObjectId() == null) {
                theItem.item_picture = null;
                setItemPicture(null);
                return;
            }
            if (JOptionPane.showConfirmDialog(null, "�׹�ѹ ź�ٻ�Ҿ��¡��", "�׹�ѹ", JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION) {
                return;
            }
            theItem.item_picture = null;
            if (1 == theHC.theSetupControl.updateItemPicture(theItem)) {
                setItemPicture(null);
            }
        }
    }

    private void setItemPicture(BufferedImage image) {
        if (image == null) {
            lblPicture.setIcon(null);
        } else {
            byte[] imageInByte = ImageUtils.toByteArray(ImageUtils.fit(image, 90, 90));
            if (imageInByte != null) {
                lblPicture.setIcon(new ImageIcon(imageInByte));
            }
        }
    }

    private void setItem(Item it) {
        theItem = it;
        theItem = theSetupControl.listItemByPk(theItem.getObjectId());
        ComboboxModel.setCodeComboBox(jComboBoxOrderList, theItem.item_group_code_category);
        jTextFieldCode.setText(theItem.item_id);
        jTextAreaCommonName.setText(theItem.common_name);
        jTextAreaNickName.setText(theItem.nick_name);
        jTextAreaTradeName.setText(theItem.trade_name);
        jTextAreaLocalName.setText(theItem.local_name);
        ComboboxModel.setCodeComboBox(jComboBoxReceiptList, theItem.item_group_code_billing);
        ComboboxModel.setCodeComboBox(jComboBoxStandardGroup, theItem.item_16_group);
        jCheckBoxActive.setSelected(theItem.getActive() || theItem.active.equals("2"));
        jCheckBox2.setSelected(theItem.getWarningICD10());
        jCheckBoxItemNotUseToOrder.setSelected(theItem.getItemNotUseToOrder());
        jRadioButtonLabDetail.setEnabled(it.getObjectId() == null);
        jRadioButtonLabGroup.setEnabled(it.getObjectId() == null);
        jTextFieldGeneralNumber.setText(theItem.item_general_number);

        theItem.item_picture = theSetupControl.selectItemPictureByItemId(theItem.getObjectId());
        if (theItem.item_picture != null) {
            BufferedImage image = ImageUtils.toBufferedImage(theItem.item_picture);
            setItemPicture(image);
        }

        setItemPriceV(theSetupControl.listItemPrice(theItem.getObjectId()));
        CategoryGroupItem cgitem = selectOrderItemGroup();
        if (cgitem.category_group_code.equals(CategoryGroup.isDrug())) {
            jPanelDrugDescription.setDrug(theSetupControl.readDrug(theItem.getObjectId()));
        } else if (cgitem.category_group_code.equals(CategoryGroup.isService()) || cgitem.category_group_code.equals(CategoryGroup.isDental())) { //��Һ�ԡ�� ��� �ѹ�����
            setItemService(theSetupControl.readItemSeviceByItem(theItem.getObjectId()));
        } else if (cgitem.category_group_code.equals(CategoryGroup.isLab())) {
            // somprasong 20110825 ��ҡ�����������к����ѹ�֡ null ��з�����Ţ index ����¹
            try {
                this.jComboBoxLabRpGroup.setSelectedIndex(Integer.parseInt(theItem.rp_lab_type));
            } catch (Exception e) {
                this.jComboBoxLabRpGroup.setSelectedIndex(0);
            }
            Gutil.setGuiData(jComboBoxLabSpecimen, theItem.b_specimen_id);
            boolean ret = setLabGroup(theSetupControl.listLabGroupByItem(theItem.getObjectId()), null);
            if (ret) {
                jRadioButtonLabGroup.setSelected(true);
                this.jRadioButtonLabGroupActionPerformed(null);
                setLabSetV(theSetupControl.listLabSetByItemId(theItem.getObjectId(), "sort"));
                return;
            }
            jRadioButtonLabDetail.setSelected(true);
            this.jRadioButtonLabDetailActionPerformed(null);
            setLabResultItem(theSetupControl.readLabResultItem(theItem.getObjectId()));
            this.chooseLabType();
            jRadioButtonLabIn.setSelected(theItem.f_item_lab_location_id.equals("1"));
            jRadioButtonLabOut.setSelected(theItem.f_item_lab_location_id.equals("2"));
        } else if (cgitem.category_group_code.equals(CategoryGroup.isXray())) {
            setItemXray(theSetupControl.readItemXrayByItemId(theItem.getObjectId()));
        } else if (cgitem.category_group_code.equals(CategoryGroup.isSupply())) {
            setItemSupply(theSetupControl.readItemSupplyByItemId(theItem.getObjectId()));
        }
        if (cgitem.category_group_code.equals(CategoryGroup.isDrug())
                || cgitem.category_group_code.equals(CategoryGroup.isSupply())) {
            Object[] objs = theSetupControl.checkMapTMTAndCode24(theItem.getObjectId());
            if (objs != null && objs.length == 2) {
                if (objs[0] == null || String.valueOf(objs[0]).isEmpty()) {
                    lblStatusNotMapTMT.setVisible(true);
                } else {
                    lblStatusNotMapTMT.setVisible(false);
                }
                if (objs[1] == null || String.valueOf(objs[1]).isEmpty()) {
                    lblStatusNotMap24.setVisible(true);
                } else {
                    lblStatusNotMap24.setVisible(false);
                }
            } else {
                lblStatusNotMapTMT.setVisible(true);
                lblStatusNotMap24.setVisible(true);
            }
        } else {
            lblStatusNotMapTMT.setVisible(false);
            lblStatusNotMap24.setVisible(false);
        }
    }

    public void setItemXray(BItemXray itemXray) {
        theItemXray = itemXray;
        if (theItemXray == null) {
            theItemXray = new BItemXray();
        }
        if (theItemXray.b_modality_id == null || theItemXray.b_modality_id.isEmpty()) {
            combModality.setSelectedIndex(0);
        } else {
            ComboboxModel.setCodeComboBox(combModality, theItemXray.b_modality_id);
        }
    }

    public void setItemSupply(ItemSupply is) {
        theItemSupply = is;
        if (theItemSupply == null) {
            theItemSupply = new ItemSupply();
        }
        cbSupplyPrintable.setSelected(theItemSupply.item_supply_printable.equals("1"));
        chkbSupplyPrintMar.setSelected(!theItemSupply.print_mar_type.equals("0"));
        cbSupplyMarType.setEnabled(chkbSupplyPrintMar.isSelected());
        if (theItemSupply.print_mar_type.equals("1")) {
            cbSupplyMarType.setSelectedIndex(0);
        } else if (theItemSupply.print_mar_type.equals("2")) {
            cbSupplyMarType.setSelectedIndex(1);
        }
        txtSupplement.setText(theItemSupply.supplement_label);
    }

    public void setItemService(ItemService is) {
        theItemService = is;
        jTextFieldIcdCode.setText("");
        balloonTextArea1.setText("");
        jTextFieldDescription.setText("");
        if (theItemService == null) {
            theItemService = new ItemService();
        }

        jTextFieldDescription.setText(theItem.common_name);
        if (theItemService.icd9_code != null && !theItemService.icd9_code.isEmpty()) {
            ICD9 oicd9 = theSetupControl.listIcd9ByCode(theItemService.icd9_code);
            if (oicd9 != null) {
                jTextFieldIcdCode.setText(oicd9.getName());
                balloonTextArea1.setText(oicd9.getName());
            }
        }
    }

    public boolean setLabGroup(LabGroup lg, Vector vls) {
        theLabGroup = lg;
        if (theLabGroup == null) {
            setLabSetV(null);
            return false;
        }
        if (theLabGroup.getObjectId() == null) {
            setLabSetV(null);
            return false;
        }
        setLabSetV(vls);
        return true;
    }

    private Item getItem() {
        // set Item
        theItem.item_id = jTextFieldCode.getText();
        theItem.common_name = jTextAreaCommonName.getText();
        theItem.nick_name = jTextAreaNickName.getText();
        theItem.trade_name = jTextAreaTradeName.getText();
        theItem.local_name = jTextAreaLocalName.getText();
        theItem.item_group_code_billing = ComboboxModel.getCodeComboBox(jComboBoxReceiptList);
        theItem.item_group_code_category = ComboboxModel.getCodeComboBox(jComboBoxOrderList);
        theItem.item_16_group = ComboboxModel.getCodeComboBox(jComboBoxStandardGroup);
        theItem.setSecret(jCheckBoxSecret.isSelected());
        theItem.setActive(jCheckBoxActive.isSelected());
        theItem.setWarningICD10(jCheckBox2.isSelected());
        theItem.unit_pack53 = jPanelDrugDescription.getUnitPack();
        theItem.item_not_use_to_order = jCheckBoxItemNotUseToOrder.isSelected() ? "1" : "0";
        // somprasong 20110825 ��ҡ�����������к����ѹ�֡ null ��з�����Ţ index ����¹
        theItem.rp_lab_type = jComboBoxLabRpGroup.getSelectedIndex() == 0 ? null : String.valueOf(jComboBoxLabRpGroup.getSelectedIndex());
        theItem.item_general_number = jTextFieldGeneralNumber.getText();
        theItem.b_specimen_id = Gutil.getGuiData(jComboBoxLabSpecimen);
        CategoryGroupItem cgitem = theLookupControl.readCategoryGroupItemById(theItem.item_group_code_category);
        if (cgitem != null && cgitem.category_group_code.equals(CategoryGroup.isLab())) {
            theItem.f_item_lab_location_id = jRadioButtonLabIn.isSelected() ? "1" : "2";
            theItem.setLabDuration(jCheckBoxDuration.isSelected());
            theItem.item_lab_duration_day = jTextFieldDurationDate.getText();
            theItem.f_lab_atk_product_id = ComboboxModel.getCodeComboBox(cbAtkProduct);
        }
        return theItem;
    }

    private BItemXray getItemXray() {
        if (category != 3 || theItemXray == null) {
            return null;
        } else {
            theItemXray.b_item_id = theItem.getObjectId();
            theItemXray.b_modality_id = ComboboxModel.getCodeComboBox(combModality);
            return theItemXray;
        }
    }

    private ItemSupply getItemSupply() {
        if (category != 4 || theItemSupply == null) {
            return null;
        } else {
            theItemSupply.b_item_id = theItem.getObjectId();
            theItemSupply.item_supply_printable = cbSupplyPrintable.isSelected() ? "1" : "0";
            theItemSupply.print_mar_type = chkbSupplyPrintMar.isSelected()
                    ? String.valueOf(cbSupplyMarType.getSelectedIndex() + 1)
                    : "0";
            theItemSupply.supplement_label = txtSupplement.getText();
            return theItemSupply;
        }
    }

    public ItemService getItemService() {
        if (category != 6 && category != 5) {
            return null;
        }
        if (theItemService == null) {
            return null;
        }
        theItemService.item_id = theItem.getObjectId();
        theItemService.description = theItem.common_name;
        theItemService.active = "1";
        theItemService.icd9_code = "";
        String icd9 = balloonTextArea1.getText().trim();
        String icd9code;
        if (!icd9.isEmpty() && icd9.indexOf(":") > -1) {
            icd9code = icd9.substring(0, icd9.indexOf(":")).trim();
        } else {
            icd9code = icd9;
        }
        if (!icd9code.isEmpty()) {
            ICD9 i9 = theSetupControl.listIcd9ByCode(icd9code);
            if (i9 != null) {
                theItemService.icd9_code = icd9code;
            }
        }
        return theItemService;
    }

    public LabGroup getLabGroup() {
        if (theLabGroup == null) {
            theLabGroup = new LabGroup();
        }
        if (vLabSet == null) {
            vLabSet = new Vector();
        }
        return theLabGroup;
    }

    private LabResultItem getLabResultItem() {
        if (theLabResultItem == null) {
            theLabResultItem = new LabResultItem();
        }

        theLabResultItem.name = jTextFieldLabDetail.getText();
        theLabResultItem.unit = jTextFieldLabUnit.getText();
        theLabResultItem.normal_range_type = String.valueOf(cbNormalrRangeType.getSelectedIndex());
        theLabResultItem.default_value = "";

        switch (cbNormalrRangeType.getSelectedIndex()) {
            case 1:
                if (!jRadioButtonList.isSelected() && !jRadioButtonTexts.isSelected()) {
                    LabResultItemGender lrig = new LabResultItemGender();
                    lrig.male_result_min = txtGenderM_Min.getText().trim();
                    lrig.male_result_max = txtGenderM_Max.getText().trim();
                    lrig.female_result_min = txtGenderWM_Min.getText().trim();
                    lrig.female_result_max = txtGenderWM_Max.getText().trim();
                    lrig.male_result_critical_min = txtGenderM_MinCV.getText().trim();
                    lrig.male_result_critical_max = txtGenderM_MaxCV.getText().trim();
                    lrig.female_result_critical_min = txtGenderWM_MinCV.getText().trim();
                    lrig.female_result_critical_max = txtGenderWM_MaxCV.getText().trim();
                    theLabResultItem.normalRanges = new Object[]{lrig};
                }
                break;
            case 2:
                if (!jRadioButtonList.isSelected() && !jRadioButtonTexts.isSelected()) {
                    theLabResultItem.normalRanges = new Object[tmcds.getData().size()];
                    for (int i = 0; i < tmcds.getData().size(); i++) {
                        theLabResultItem.normalRanges[i] = tmcds.getData().get(i).getId();
                    }
                }
                break;
            case 3:
                if (jRadioButtonText.isSelected()) {
                    theLabResultItem.normalRanges = new Object[tmds.getData().size()];
                    for (int i = 0; i < tmds.getData().size(); i++) {
                        theLabResultItem.normalRanges[i] = tmds.getData().get(i).getId();
                    }
                }
                break;
            case 4:
                if (!jRadioButtonList.isSelected() && !jRadioButtonTexts.isSelected()) {
                    theLabResultItem.default_value = jTextFieldDefaultValue.getText().trim();
                }
                break;
            default:
                break;
        }

        if (jRadioButtonNumber.isSelected()) {
            theLabResultItem.resultType = LabResultType.INTEGER;
        }
        if (jRadioButtonPointNumber.isSelected()) {
            theLabResultItem.resultType = LabResultType.FLOAT;
        }
        if (jRadioButtonText.isSelected()) {
            theLabResultItem.resultType = LabResultType.TEXT_ONE;
        }
        if (jRadioButtonTexts.isSelected()) {
            theLabResultItem.resultType = LabResultType.TEXT_MANY;
        }
        if (jRadioButtonList.isSelected()) {
            theLabResultItem.resultType = LabResultType.LIST;
            ComboFix cf = (ComboFix) jComboBoxList.getSelectedItem();
            if (cf != null) {
                theLabResultItem.labresult_id = cf.getCode();
            }
        }

        //amp:19/06/2549
        if (jRadioButtonLabDetail.isSelected()) {
            theLabResultItem.setNcdFbs(jCheckBoxNCDFBS.isSelected());
            theLabResultItem.setNcdHct(jCheckBoxNCDHCT.isSelected());
        }
        return theLabResultItem;
    }

    private void setLabResultItem(LabResultItem lab) {
        theLabResultItem = lab;
        // clear normal range
        this.cbNormalrRangeType.setSelectedIndex(0);
        jTextFieldDefaultValue.setText("");
        txtGenderM_Min.setText("");
        txtGenderM_Max.setText("");
        txtGenderWM_Min.setText("");
        txtGenderWM_Max.setText("");
        txtGenderM_MinCV.setText("");
        txtGenderM_MaxCV.setText("");
        txtGenderWM_MinCV.setText("");
        txtGenderWM_MaxCV.setText("");
        txtNormalRangeText.setText("");
        tmds.clearTable();
        btnAddTextNormalRange.setEnabled(false);
        btnDeleteTextNormalRange.setEnabled(false);
        doValidateTextNormalRange();
        txtAge_Min.setText("");
        txtAge_Max.setText("");
        txtNRAgeM_Min.setText("");
        txtNRAgeM_Max.setText("");
        txtNRAgeWM_Min.setText("");
        txtNRAgeWM_Max.setText("");
        txtNRAgeM_MinCV.setText("");
        txtNRAgeM_MaxCV.setText("");
        txtNRAgeWM_MinCV.setText("");
        txtNRAgeWM_MaxCV.setText("");
        tmcds.clearTable();
        btnAddAgeNormalRange.setEnabled(false);
        btnRemoveAgeNormalRange.setEnabled(false);
        doValidateAgeNormalRange();
        if (theLabResultItem == null) {
            jTextFieldLabDetail.setText("");
            jTextFieldLabUnit.setText("");
            Gutil.setGuiData(jComboBoxList, "");
            Gutil.setGuiData(cbAtkProduct, "");
            jRadioButtonText.setSelected(true);
            jCheckBoxSecret.setSelected(false);
            jCheckBoxNCDFBS.setSelected(false);
            jCheckBoxNCDHCT.setSelected(false);
            jCheckBoxDuration.setSelected(false);
            return;
        }

        jTextFieldLabDetail.setText(theLabResultItem.name);
        jTextFieldLabUnit.setText(theLabResultItem.unit);
        Gutil.setGuiData(jComboBoxList, theLabResultItem.labresult_id);
        Gutil.setGuiData(cbAtkProduct, theItem.f_lab_atk_product_id);
        jCheckBoxSecret.setSelected(theItem.getSecret());
        jCheckBoxDuration.setSelected(theItem.getLabDuration());
        if (jCheckBoxDuration.isSelected()) {
            jTextFieldDurationDate.setEnabled(true);
            jLabelAmountDate.setEnabled(true);
        }
        jTextFieldDurationDate.setText(theItem.item_lab_duration_day);

        jRadioButtonList.setSelected(false);
        jRadioButtonTexts.setSelected(false);
        jRadioButtonText.setSelected(false);
        jRadioButtonPointNumber.setSelected(false);
        jRadioButtonNumber.setSelected(false);

        if (theLabResultItem.resultType.equals(LabResultType.INTEGER)) {
            jRadioButtonNumber.setSelected(true);
        } else if (theLabResultItem.resultType.equals(LabResultType.FLOAT)) {
            jRadioButtonPointNumber.setSelected(true);
        } else if (theLabResultItem.resultType.equals(LabResultType.TEXT_ONE)) {
            jRadioButtonText.setSelected(true);
        } else if (theLabResultItem.resultType.equals(LabResultType.TEXT_MANY)) {
            jRadioButtonTexts.setSelected(true);
        } else if (theLabResultItem.resultType.equals(LabResultType.LIST)) {
            jRadioButtonList.setSelected(true);
        }

        jCheckBoxNCDFBS.setSelected("1".equals(theLabResultItem.ncd_fbs));
        jCheckBoxNCDHCT.setSelected("1".equals(theLabResultItem.ncd_hct));

        if (theLabResultItem.resultType != null && theLabResultItem.resultType.equals("0")) {
            jRadioButtonNumber.setSelected(true);
        } else if (theLabResultItem.resultType != null && theLabResultItem.resultType.equals("1")) {
            jRadioButtonPointNumber.setSelected(true);
        } else if (theLabResultItem.resultType == null || theLabResultItem.resultType.equals("2")) {
            jRadioButtonText.setSelected(true);
        } else if (theLabResultItem.resultType != null && theLabResultItem.resultType.equals("3")) {
            jRadioButtonTexts.setSelected(true);
        } else if (theLabResultItem.resultType != null && theLabResultItem.resultType.equals("4")) {
            jRadioButtonList.setSelected(true);
            com.hospital_os.utility.ComboboxModel.setCodeComboBox(jComboBoxList, theLabResultItem.labresult_id);
        }

        cbNormalrRangeType.setSelectedIndex(theLabResultItem.normal_range_type.equals("") ? 0
                : Integer.parseInt(theLabResultItem.normal_range_type));

        if (theLabResultItem.getObjectId() != null) {
            switch (cbNormalrRangeType.getSelectedIndex()) {
                case 1:
                    LabResultItemGender itemGender = theHC.theSetupControl.getLabResultItemGenderByLabResultItemId(
                            theLabResultItem.getObjectId());
                    if (itemGender != null) {
                        txtGenderM_Min.setText(itemGender.male_result_min);
                        txtGenderM_Max.setText(itemGender.male_result_max);
                        txtGenderWM_Min.setText(itemGender.female_result_min);
                        txtGenderWM_Max.setText(itemGender.female_result_max);
                        txtGenderM_MinCV.setText(itemGender.male_result_critical_min);
                        txtGenderM_MaxCV.setText(itemGender.male_result_critical_max);
                        txtGenderWM_MinCV.setText(itemGender.female_result_critical_min);
                        txtGenderWM_MaxCV.setText(itemGender.female_result_critical_max);
                    }
                    break;
                case 2:
                    List<LabResultItemAge> list = theHC.theSetupControl.listLabResultItemAgeByLabResultItemId(
                            theLabResultItem.getObjectId());
                    for (LabResultItemAge itemAge : list) {
                        ComplexDataSource cds = new ComplexDataSource(itemAge, new Object[]{
                            itemAge.age_min,
                            itemAge.age_max,
                            itemAge.male_critical_min,
                            itemAge.male_min,
                            itemAge.male_max,
                            itemAge.male_critical_max,
                            itemAge.female_critical_min,
                            itemAge.female_min,
                            itemAge.female_max,
                            itemAge.female_critical_max
                        });
                        tmcds.addComplexDataSource(cds);
                    }
                    tmcds.fireTableDataChanged();
                    break;
                case 3:
                    List<LabResultItemText> lrits = theHC.theSetupControl.listLabResultItemTextByLabResultItemId(
                            theLabResultItem.getObjectId());
                    for (LabResultItemText itemText : lrits) {
                        tmds.addDataSource(new DataSource(itemText, itemText.result_text));
                    }
                    tmds.fireTableDataChanged();
                    break;
                case 4:
                    jTextFieldDefaultValue.setText(theLabResultItem.default_value);
                    break;
                default:
                    break;
            }
        }
    }

    public void chooseLabType() {
        if (jRadioButtonNumber.isSelected() || jRadioButtonPointNumber.isSelected()) {
            if (jRadioButtonNumber.isSelected()) {
                doubleDocumentGenderM_Min.setLimit(0);
                doubleDocumentGenderM_Max.setLimit(0);
                doubleDocumentGenderWM_Min.setLimit(0);
                doubleDocumentGenderWM_Max.setLimit(0);
                doubleDocumentNRAgeM_Min.setLimit(0);
                doubleDocumentNRAgeM_Max.setLimit(0);
                doubleDocumentNRAgeWM_Min.setLimit(0);
                doubleDocumentNRAgeWM_Max.setLimit(0);
                doubleDocumentGenderM_MinCV.setLimit(0);
                doubleDocumentGenderM_MaxCV.setLimit(0);
                doubleDocumentGenderWM_MinCV.setLimit(0);
                doubleDocumentGenderWM_MaxCV.setLimit(0);
                doubleDocumentNRAgeM_MinCV.setLimit(0);
                doubleDocumentNRAgeM_MaxCV.setLimit(0);
                doubleDocumentNRAgeWM_MinCV.setLimit(0);
                doubleDocumentNRAgeWM_MaxCV.setLimit(0);
            } else {
                doubleDocumentGenderM_Min.setLimit(-1);
                doubleDocumentGenderM_Max.setLimit(-1);
                doubleDocumentGenderWM_Min.setLimit(-1);
                doubleDocumentGenderWM_Max.setLimit(-1);
                doubleDocumentNRAgeM_Min.setLimit(-1);
                doubleDocumentNRAgeM_Max.setLimit(-1);
                doubleDocumentNRAgeWM_Min.setLimit(-1);
                doubleDocumentNRAgeWM_Max.setLimit(-1);
                doubleDocumentGenderM_MinCV.setLimit(-1);
                doubleDocumentGenderM_MaxCV.setLimit(-1);
                doubleDocumentGenderWM_MinCV.setLimit(-1);
                doubleDocumentGenderWM_MaxCV.setLimit(-1);
                doubleDocumentNRAgeM_MinCV.setLimit(-1);
                doubleDocumentNRAgeM_MaxCV.setLimit(-1);
                doubleDocumentNRAgeWM_MinCV.setLimit(-1);
                doubleDocumentNRAgeWM_MaxCV.setLimit(-1);
            }
            jComboBoxList.setEnabled(false);
            setLabResultDetailV(null);
        } else if (jRadioButtonList.isSelected()) {
            jComboBoxList.setEnabled(true);
            doSelectedLabDetail();
        } else if (jRadioButtonText.isSelected() || jRadioButtonTexts.isSelected()) {
            jComboBoxList.setEnabled(false);
            setLabResultDetailV(null);
        }
        doSwitchNormalRangePanel();
    }

    private void setItemV(Vector voffice, int off) {
        int count = offset;
        int p, n;
        if (voffice != null && !voffice.isEmpty()) {
            if (off == 1) {//������ next
                p = prev;
                n = next;
                prev = next;
                next += offset;
                if (next >= vItem.size()) {
                    next = vItem.size();
                    count = next - prev;
                }
                if (count == 0) {
                    prev = p;
                    next = n;
                    count = n - p;
                }
            } else {//������ previous
                next = prev;
                prev -= offset;
                if (prev <= 0) {
                    prev = 0;
                    next = offset;
                }
                if (next >= vItem.size()) {
                    next = vItem.size();
                    count = next;
                }
            }
            TaBleModel tm = new TaBleModel(col, count);
            for (int i = 0; i < count; i++) {
                Item of = (Item) voffice.get(i + prev);
                tm.setValueAt(of.item_id, i, 0);
                tm.setValueAt(of.common_name, i, 1);
            }
            jTable1.setModel(tm);
        } else {
            TaBleModel tm = new TaBleModel(col, 0);
            jTable1.setModel(tm);
        }
        jTable1.getColumnModel().getColumn(0).setPreferredWidth(60); // �������ѭ����Ѻ þ.�����, ���͡�ä�� ����Ѻ�ٹ���ä��
        jTable1.getColumnModel().getColumn(1).setPreferredWidth(300); // �ӹǹ
        if (jTable1.getRowCount() > 0) {
            jTable1.setRowSelectionInterval(0, 0);
            this.selectItemGroup();
        } else {
            this.addNewItem();
        }
    }

    public void addLabGroupItemGroup() {
        if (psss == null) {
            psss = new PanelSetupSearchSub(theHC, theUS, 2);
            psss.setTitle(Constant.getTextBundle("��˹���¡�� Lab �ش"));
            if (vLabSet == null) {
                vLabSet = new Vector();
            }
            psss.showSearchLabGroup(jTable4, vLabSet, null);
        }
        psss = null;
    }

    @Override
    public void setEnableForLift(boolean b) {
        jButtonDel.setVisible(b);
        jButtonDelLabItem.setVisible(b);
    }

    @Override
    public int saveOption(Option option) {
        return 0;
    }

    @Override
    public int editOption(Option option) {
        return 0;
    }

    @Override
    public void notifysetEnableForLift(boolean b) {
    }

    @Override
    public Option readOption() {
        return null;
    }

    @Override
    public void reFrashPanel() {
    }

    @Override
    public void notifyreFrashPanel() {
        setupLookup();
    }

    @Override
    public Vector listOptionAll() {
        return null;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.hosv3.gui.component.BalloonTextArea balloonTextArea1;
    private javax.swing.JButton btnAddAgeNormalRange;
    private javax.swing.JButton btnAddPicture;
    private javax.swing.JButton btnAddTextNormalRange;
    private javax.swing.JButton btnDelPicture;
    private javax.swing.JButton btnDeleteTextNormalRange;
    private javax.swing.JButton btnRemoveAgeNormalRange;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroupBeginWith;
    private javax.swing.ButtonGroup buttonGroupLab;
    private javax.swing.ButtonGroup buttonGroupLabLocation;
    private javax.swing.ButtonGroup buttonGroupLabResultType;
    private javax.swing.ButtonGroup buttonGroupNormalValue;
    private javax.swing.JComboBox<String> cbAtkProduct;
    private javax.swing.JComboBox cbNormalrRangeType;
    private javax.swing.JComboBox cbSupplyMarType;
    private javax.swing.JCheckBox cbSupplyPrintable;
    private javax.swing.JCheckBox chkbCanEditPrice;
    private javax.swing.JCheckBox chkbSupplyPrintMar;
    private javax.swing.JCheckBox chkbUseClaimPrice;
    private javax.swing.JComboBox combModality;
    private com.hospital_os.utility.DateComboBox dateTextField;
    private com.hospital_os.utility.DoubleTextField doubleTextFieldPriceClaim;
    private com.hospital_os.utility.DoubleTextField doubleTextFieldPriceCost;
    private com.hospital_os.utility.DoubleTextField doubleTextFieldPriceDF;
    private com.hospital_os.utility.DoubleTextField doubleTextFieldPriceHos;
    private com.hospital_os.utility.DoubleTextField doubleTextFieldPriceIPD;
    private com.hospital_os.utility.DoubleTextField doubleTextFieldPriceMax;
    private com.hospital_os.utility.DoubleTextField doubleTextFieldPriceMin;
    private com.hospital_os.utility.DoubleTextField doubleTextFieldPriceOPD;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButtonAdd;
    private javax.swing.JButton jButtonAddPrice;
    private javax.swing.JButton jButtonAddPrice1;
    private javax.swing.JButton jButtonDel;
    private javax.swing.JButton jButtonDelLabItem;
    private javax.swing.JButton jButtonDelPrice;
    private javax.swing.JButton jButtonDelPrice1;
    private javax.swing.JButton jButtonNext;
    private javax.swing.JButton jButtonPrev;
    private javax.swing.JButton jButtonPrev1;
    private javax.swing.JButton jButtonSave;
    private javax.swing.JButton jButtonSavePrice1;
    private javax.swing.JButton jButtonSearch;
    private javax.swing.JCheckBox jCheckBox2;
    private javax.swing.JCheckBox jCheckBoxActive;
    private javax.swing.JCheckBox jCheckBoxDuration;
    private javax.swing.JCheckBox jCheckBoxItemNotUseToOrder;
    private javax.swing.JCheckBox jCheckBoxNCDFBS;
    private javax.swing.JCheckBox jCheckBoxNCDHCT;
    private javax.swing.JCheckBox jCheckBoxPlan;
    private javax.swing.JCheckBox jCheckBoxS;
    private javax.swing.JCheckBox jCheckBoxSearchGroup;
    private javax.swing.JCheckBox jCheckBoxSecret;
    private javax.swing.JComboBox jComboBox2;
    private javax.swing.JComboBox jComboBoxLabRpGroup;
    private javax.swing.JComboBox jComboBoxLabSpecimen;
    private javax.swing.JComboBox jComboBoxList;
    private javax.swing.JComboBox jComboBoxOrderList;
    private com.hosv3.gui.component.HosComboBox jComboBoxPlan;
    private javax.swing.JComboBox jComboBoxReceiptList;
    private javax.swing.JComboBox jComboBoxSCategory;
    private javax.swing.JComboBox jComboBoxStandardGroup;
    private com.hosv3.gui.component.HosComboBox jComboBoxTariff;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel58;
    private javax.swing.JLabel jLabel59;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel60;
    private javax.swing.JLabel jLabel61;
    private javax.swing.JLabel jLabel62;
    private javax.swing.JLabel jLabel63;
    private javax.swing.JLabel jLabel64;
    private javax.swing.JLabel jLabel65;
    private javax.swing.JLabel jLabel66;
    private javax.swing.JLabel jLabel67;
    private javax.swing.JLabel jLabel68;
    private javax.swing.JLabel jLabel69;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel73;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelAmountDate;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel111;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel23;
    private javax.swing.JPanel jPanel24;
    private javax.swing.JPanel jPanel25;
    private javax.swing.JPanel jPanel26;
    private javax.swing.JPanel jPanel28;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel30;
    private javax.swing.JPanel jPanel31;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel51;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JPanel jPanelBlank;
    private javax.swing.JPanel jPanelDefaultValue;
    private com.hosv3.gui.panel.detail.PDItemDrug jPanelDrugDescription;
    private javax.swing.JPanel jPanelIcdCode;
    private javax.swing.JPanel jPanelLabDescription;
    private javax.swing.JRadioButton jRadioButtonBegin;
    private javax.swing.JRadioButton jRadioButtonConsist;
    private javax.swing.JRadioButton jRadioButtonLabDetail;
    private javax.swing.JRadioButton jRadioButtonLabGroup;
    private javax.swing.JRadioButton jRadioButtonLabIn;
    private javax.swing.JRadioButton jRadioButtonLabOut;
    private javax.swing.JRadioButton jRadioButtonList;
    private javax.swing.JRadioButton jRadioButtonNumber;
    private javax.swing.JRadioButton jRadioButtonPointNumber;
    private javax.swing.JRadioButton jRadioButtonText;
    private javax.swing.JRadioButton jRadioButtonTexts;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane12;
    private javax.swing.JScrollPane jScrollPane13;
    private javax.swing.JScrollPane jScrollPane14;
    private javax.swing.JScrollPane jScrollPane15;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JTabbedPane jTabbedPane1;
    private com.hosv3.gui.component.HJTableSort jTable1;
    private com.hosv3.gui.component.HJTableSort jTable3;
    private com.hosv3.gui.component.HJTableSort jTable4;
    private com.hosv3.gui.component.HJTableSort jTableList;
    private javax.swing.JTextArea jTextAreaCommonName;
    private javax.swing.JTextArea jTextAreaLocalName;
    private javax.swing.JTextArea jTextAreaNickName;
    private javax.swing.JTextArea jTextAreaTradeName;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JTextField jTextField5;
    private javax.swing.JTextField jTextField6;
    private javax.swing.JTextField jTextField7;
    private javax.swing.JTextField jTextField8;
    private javax.swing.JTextField jTextField9;
    private javax.swing.JTextField jTextFieldCode;
    private javax.swing.JTextArea jTextFieldDefaultValue;
    private javax.swing.JTextField jTextFieldDescription;
    private com.hospital_os.utility.DoubleTextField jTextFieldDurationDate;
    private javax.swing.JTextField jTextFieldGeneralNumber;
    private com.hosv3.gui.component.BalloonTextField jTextFieldIcdCode;
    private javax.swing.JTextField jTextFieldLabDetail;
    private javax.swing.JTextField jTextFieldLabUnit;
    private javax.swing.JTextField jTextFieldSCode;
    private javax.swing.JLabel lblNormalRangeMsg;
    private javax.swing.JLabel lblPicture;
    private javax.swing.JLabel lblStatusInactive;
    private javax.swing.JLabel lblStatusNotMap24;
    private javax.swing.JLabel lblStatusNotMapTMT;
    private javax.swing.JPanel panelCardNormalRange;
    private javax.swing.JPanel panelItemPriceDetail;
    private javax.swing.JPanel panelLimitPrice;
    private javax.swing.JPanel panelNA;
    private javax.swing.JPanel panelNormalRange;
    private javax.swing.JPanel panelNormalRangeByAges;
    private javax.swing.JPanel panelNormalRangeByGender;
    private javax.swing.JPanel panelNormalRangeBySql;
    private javax.swing.JPanel panelNormalRangeByTexts;
    private javax.swing.JPanel panelSupply;
    private javax.swing.JPanel panelXrayDescription;
    private javax.swing.JTable tableAgeNormalRange;
    private com.hosv3.gui.component.HJTableSort tableItemPriceList;
    private javax.swing.JTable tableTextNormalRange;
    private javax.swing.JTextField txtAge_Max;
    private javax.swing.JTextField txtAge_Min;
    private javax.swing.JTextField txtGenderM_Max;
    private javax.swing.JTextField txtGenderM_MaxCV;
    private javax.swing.JTextField txtGenderM_Min;
    private javax.swing.JTextField txtGenderM_MinCV;
    private javax.swing.JTextField txtGenderWM_Max;
    private javax.swing.JTextField txtGenderWM_MaxCV;
    private javax.swing.JTextField txtGenderWM_Min;
    private javax.swing.JTextField txtGenderWM_MinCV;
    private javax.swing.JTextField txtNRAgeM_Max;
    private javax.swing.JTextField txtNRAgeM_MaxCV;
    private javax.swing.JTextField txtNRAgeM_Min;
    private javax.swing.JTextField txtNRAgeM_MinCV;
    private javax.swing.JTextField txtNRAgeWM_Max;
    private javax.swing.JTextField txtNRAgeWM_MaxCV;
    private javax.swing.JTextField txtNRAgeWM_Min;
    private javax.swing.JTextField txtNRAgeWM_MinCV;
    private javax.swing.JTextField txtNormalRangeText;
    private javax.swing.JTextArea txtSupplement;
    // End of variables declaration//GEN-END:variables

    private void addNewItem() {
        saved = 1;
        setEnabled(true);
        jTextFieldCode.setEnabled(true);
        jTextAreaCommonName.setEnabled(true);
        String cat = ComboboxModel.getCodeComboBox(jComboBoxOrderList);
        String str = theHC.theSetupControl.readMaxItemCodeByCat(cat);
        this.jRadioButtonLabDetail.setSelected(true);
        this.jRadioButtonLabIn.setSelected(true);
        clearAll();
        this.jTextFieldCode.setText(str);
        jTextFieldCode.setToolTipText(Constant.getTextBundle("���ʷ���ʴ����繤���ش���� ��س����������ºǡ����ա1"));
        selectOrderItemGroup();
    }

    private void doSwitchNormalRangePanel() {
        CardLayout card = (CardLayout) panelCardNormalRange.getLayout();
        card.show(panelCardNormalRange, cbNormalrRangeType.getSelectedIndex() == 0
                ? "na" : cbNormalrRangeType.getSelectedIndex() == 1
                ? "gender" : cbNormalrRangeType.getSelectedIndex() == 2
                ? "ages" : cbNormalrRangeType.getSelectedIndex() == 2
                ? "texts" : "sql");
        lblNormalRangeMsg.setText("�������ö��˹���һ����� ���ͧ�ҡ���������Ż���ʹѺʹع��������һ��Է�����͡");
        switch (cbNormalrRangeType.getSelectedIndex()) {
            case 1:
                if (jRadioButtonList.isSelected() || jRadioButtonTexts.isSelected()) {
                    card.show(panelCardNormalRange, "na");
                } else {
                    card.show(panelCardNormalRange, "gender");
                }
                break;
            case 2:
                if (jRadioButtonList.isSelected() || jRadioButtonTexts.isSelected()) {
                    card.show(panelCardNormalRange, "na");
                } else {
                    card.show(panelCardNormalRange, "ages");
                }
                break;
            case 3:
                if (jRadioButtonText.isSelected()) {
                    card.show(panelCardNormalRange, "texts");
                } else {
                    card.show(panelCardNormalRange, "na");
                }
                break;
            case 4:
                if (jRadioButtonList.isSelected() || jRadioButtonTexts.isSelected()) {
                    card.show(panelCardNormalRange, "na");
                } else {
                    card.show(panelCardNormalRange, "sql");
                    if (jTextFieldDefaultValue.getText().trim().isEmpty()) {
                        this.jTextFieldDefaultValue.setText("select case when visit_age<'10' then '0' \n"
                                + "        when visit_age > '10' then '1'\n"
                                + "        else '2' end as min\n"
                                + ",case when visit_age<'10' then '10' \n"
                                + "        when visit_age > '10' then '11' \n"
                                + "        else '20' end as max from t_visit \n"
                                + "-- where t_visit_id = ? (����ͧ��� ������� default �������ͧ)");
                    }
                }
                break;
            default:
                card.show(panelCardNormalRange, "na");
                lblNormalRangeMsg.setText("");
                break;
        }
        lblNormalRangeMsg.setToolTipText(lblNormalRangeMsg.getText());
    }

    private void doSelectedLabDetail() {
        if (jRadioButtonList.isSelected()) {
            ComboFix cf = (ComboFix) jComboBoxList.getSelectedItem();
            Vector vd = null;
            if (cf != null) {
                vd = theHC.theSetupControl.listLabResultDetailByResultType(cf.getCode());
            }
            setLabResultDetailV(vd);
        }
    }

    private void doValidateTextNormalRange() {
        btnAddTextNormalRange.setEnabled(!txtNormalRangeText.getText().trim().isEmpty());
    }

    private void doAddTextNormalRange() {
        LabResultItemText itemText = new LabResultItemText();
        itemText.result_text = txtNormalRangeText.getText().trim();
        tmds.addDataSource(new DataSource(itemText, itemText.result_text));
        tmds.fireTableDataChanged();
        txtNormalRangeText.setText("");
        txtNormalRangeText.requestFocus();
    }

    private void doRemoveTextNormalRange() {
        tmds.removeDatas(tableTextNormalRange.getSelectedRows());
    }

    private void doValidateAgeNormalRange() {
        btnAddAgeNormalRange.setEnabled(!txtAge_Min.getText().trim().isEmpty()
                && !txtAge_Max.getText().trim().isEmpty()
                && ((!txtNRAgeM_Min.getText().trim().isEmpty() && !txtNRAgeM_Max.getText().trim().isEmpty()
                && !txtNRAgeM_MinCV.getText().trim().isEmpty() && !txtNRAgeM_MaxCV.getText().trim().isEmpty())
                || (!txtNRAgeWM_Min.getText().trim().isEmpty() && !txtNRAgeWM_Max.getText().trim().isEmpty()
                && !txtNRAgeWM_MinCV.getText().trim().isEmpty() && !txtNRAgeWM_MaxCV.getText().trim().isEmpty())));
    }

    private void doAddAgeNormalRange() {
        LabResultItemAge itemAge = new LabResultItemAge();
        itemAge.age_min = txtAge_Min.getText().trim();
        itemAge.age_max = txtAge_Max.getText().trim();
        itemAge.male_min = txtNRAgeM_Min.getText().trim();
        itemAge.male_max = txtNRAgeM_Max.getText().trim();
        itemAge.female_min = txtNRAgeWM_Min.getText().trim();
        itemAge.female_max = txtNRAgeWM_Max.getText().trim();
        itemAge.male_critical_min = txtNRAgeM_MinCV.getText().trim();
        itemAge.male_critical_max = txtNRAgeM_MaxCV.getText().trim();
        itemAge.female_critical_min = txtNRAgeWM_MinCV.getText().trim();
        itemAge.female_critical_max = txtNRAgeWM_MaxCV.getText().trim();
        ComplexDataSource cds = new ComplexDataSource(itemAge, new Object[]{
            itemAge.age_min,
            itemAge.age_max,
            itemAge.male_critical_min,
            itemAge.male_min,
            itemAge.male_max,
            itemAge.male_critical_max,
            itemAge.female_critical_min,
            itemAge.female_min,
            itemAge.female_max,
            itemAge.female_critical_max
        });
        tmcds.addComplexDataSource(cds);
        tmcds.fireTableDataChanged();
        txtAge_Min.setText("");
        txtAge_Max.setText("");
        txtNRAgeM_Min.setText("");
        txtNRAgeM_Max.setText("");
        txtNRAgeWM_Min.setText("");
        txtNRAgeWM_Max.setText("");
        txtNRAgeM_MinCV.setText("");
        txtNRAgeM_MaxCV.setText("");
        txtNRAgeWM_MinCV.setText("");
        txtNRAgeWM_MaxCV.setText("");
        txtAge_Min.requestFocus();
    }

    private void doRemoveAgeNormalRange() {
        tmcds.removeDatas(tableAgeNormalRange.getSelectedRows());
    }

    private void doSelectedPrice() {
        int row = tableItemPriceList.getSelectedRow();
        if (row >= 0) {
            theItemPrice = (ItemPrice) vItemPrice.get(row);
            setItemPrice(theItemPrice);
        } else {
            setItemPrice(null);
        }
    }
}

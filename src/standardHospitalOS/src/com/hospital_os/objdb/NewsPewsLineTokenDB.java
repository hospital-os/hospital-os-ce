/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.NewsPewsLineToken;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tanakrit
 */
public class NewsPewsLineTokenDB {

    public ConnectionInf theConnectionInf;
    final public String idtable = "980";

    public NewsPewsLineTokenDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public int insert(NewsPewsLineToken p) throws Exception {
        p.generateOID(idtable);
        String sql = "insert into b_news_pews_line_token ( \n"
                + "               b_news_pews_line_token_id , line_notify_token, line_notify_name,  \n"
                + "               b_service_point_id, b_visit_ward_id, user_record_id, user_update_id \n"
                + "     ) values (?, ?, ?, \n"
                + "               ?, ?, ?, ?)";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, p.getObjectId());
            ePQuery.setString(index++, p.line_notify_token);
            ePQuery.setString(index++, p.line_notify_name);
            ePQuery.setString(index++, p.b_service_point_id);
            ePQuery.setString(index++, p.b_visit_ward_id);
            ePQuery.setString(index++, p.user_record_id);
            ePQuery.setString(index++, p.user_update_id);
            return ePQuery.executeUpdate();
        }
    }

    public int update(NewsPewsLineToken p) throws Exception {
        String sql = "update b_news_pews_line_token \n"
                + "      set line_notify_token=?, line_notify_name=?, user_update_id=?, update_date_time=current_timestamp, \n"
                + "          b_service_point_id=?, b_visit_ward_id=? \n"
                + "    where b_news_pews_line_token_id=?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, p.line_notify_token);
            ePQuery.setString(index++, p.line_notify_name);
            ePQuery.setString(index++, p.user_update_id);
            ePQuery.setString(index++, p.b_service_point_id);
            ePQuery.setString(index++, p.b_visit_ward_id);
            ePQuery.setString(index++, p.getObjectId());
            return ePQuery.executeUpdate();
        }
    }

    public int delete(NewsPewsLineToken p) throws Exception {
        String sql = "DELETE FROM b_news_pews_line_token where b_news_pews_line_token_id=? \n";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, p.getObjectId());
            return ePQuery.executeUpdate();
        }
    }

    public NewsPewsLineToken selectByToken(String token, String sp) throws Exception {
        String sql = "select * from b_news_pews_line_token where line_notify_token = ? and b_service_point_id=? \n";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, token);
            ePQuery.setString(index++, sp);
            List<NewsPewsLineToken> list = executeQuery(ePQuery);
            return list.isEmpty() ? null : list.get(0);
        }
    }

    public List<NewsPewsLineToken> selectByServicepoint(String id, boolean isOPD) throws Exception {
        String sql = "select * from b_news_pews_line_token where ";
        if (isOPD) {
            sql += " b_service_point_id = ? ";
        } else {
            sql += " b_visit_ward_id = ?";
        }
        sql += " order by line_notify_name";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, id);
            return executeQuery(ePQuery);
        }
    }

    public List<NewsPewsLineToken> selectAll() throws Exception {
        String sql = "select * from b_news_pews_line_token order by line_notify_name";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            return executeQuery(ePQuery);
        }
    }

    public List<NewsPewsLineToken> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<NewsPewsLineToken> list = new ArrayList<>();
        try (ResultSet rs = preparedStatement.executeQuery()) {
            while (rs.next()) {
                NewsPewsLineToken p = new NewsPewsLineToken();
                p.setObjectId(rs.getString("b_news_pews_line_token_id"));
                p.line_notify_token = rs.getString("line_notify_token");
                p.line_notify_name = rs.getString("line_notify_name");
                p.b_service_point_id = rs.getString("b_service_point_id");
                p.b_visit_ward_id = rs.getString("b_visit_ward_id");
                p.user_record_id = rs.getString("user_record_id");
                p.record_date_time = rs.getTimestamp("record_date_time");
                p.user_update_id = rs.getString("user_update_id");
                p.update_date_time = rs.getTimestamp("update_date_time");
                list.add(p);
            }
            return list;
        }
    }
}

ALTER TABLE public.b_item_drug_dose_shortcut ALTER COLUMN b_item_drug_dose_shortcut_id TYPE varchar(30) USING b_item_drug_dose_shortcut_id::varchar;
ALTER TABLE public.b_item_drug_dose_shortcut ADD b_item_drug_uom_id varchar(30) NULL;
ALTER TABLE public.b_item_drug_dose_shortcut ADD item_drug_dose_shortcut_description_long text NULL DEFAULT '';

ALTER TABLE public.t_order_drug ADD order_drug_dose_abbreviation varchar NULL DEFAULT ''::character varying;

-- อัพเดทหลังอัพ patch เสร็จแล้ว
--  UPDATE 
--       t_order_drug t1
--  SET 
--       order_drug_dose_abbreviation = lower(order_drug_dose || t2.item_drug_uom_number || '*' || t3.item_drug_frequency_number)     
--       
--  FROM 
--       b_item_drug_uom t2,
--       b_item_drug_frequency t3
--  WHERE
--      order_drug_active = '1' 
--      and t1.order_drug_special_prescription <> '1'
--  	 and t1.b_item_drug_uom_id_use  = t2.b_item_drug_uom_id
--      and t1.b_item_drug_frequency_id  = t3.b_item_drug_frequency_id;

-- change age group = 10 when age >= 15
CREATE OR REPLACE FUNCTION calculate_agegroup(patient_id character varying)
	RETURNS integer AS $$
	declare
   		year int;
   		month int;
   		day int;
	BEGIN
		select substring(' ' || age(text_to_timestamp(t_patient.patient_birthday)::date) from '(...)year')::int as year
		    ,substring(' ' || age(text_to_timestamp(t_patient.patient_birthday)::date) from '(...)mon')::int as month
		    ,substring(' ' || age(text_to_timestamp(t_patient.patient_birthday)::date) from '(...)day')::int as day
		from t_patient
		into year,month,day
		where t_patient.t_patient_id = patient_id
		and t_patient.patient_birthday <> '';		

		/*raise notice '% % %',year,month,day;*/
		
		if(year is null) then
			if(month is null) then
				if(day <= 3) then
					return 1;
				else
					return 2;
				end if;
			else
				if(month = 1) then
					return 3;
				else
					return 4;
				end if;
			end if;
		else
			if(year <= 2) then
				return 5;
			elsif(year <= 5) then
				return 6;
			elsif(year <= 7) then
				return 7;
			elsif(year <= 9) then
				return 8;
			elsif(year < 15) then
				return 9;
			else
				return 10;
			end if;
		end if;
	END;
$$
LANGUAGE 'plpgsql';

-- update db version
INSERT INTO s_version VALUES ('9701000000106', '106', 'Hospital OS, Community Edition', '3.9.68', '3.48.0', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_68.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.68b01');

-- require when use other postgres user
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA public to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA thai_ascvd to hospitalos;
-- GRANT ALL ON all sequences in schema thai_ascvd to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA rp50 to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA rp50nhso to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA rp50data to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA health_insurance to hospitalos;
-- GRANT ALL ON all sequences in schema public to hospitalos; 
-- GRANT ALL ON all sequences in schema rp50 to hospitalos; 
-- GRANT ALL ON all sequences in schema rp50nhso to hospitalos; 
-- GRANT ALL ON all sequences in schema rp50data to hospitalos; 
-- GRANT ALL ON all sequences in schema health_insurance to hospitalos;
-- GRANT ALL ON all sequences in schema thai_ascvd to hospitalos;
-- GRANT USAGE ON SCHEMA public TO hospitalos;
-- GRANT USAGE ON SCHEMA rp50 TO hospitalos;
-- GRANT USAGE ON SCHEMA rp50nhso TO hospitalos;
-- GRANT USAGE ON SCHEMA rp50data TO hospitalos;
-- GRANT USAGE ON SCHEMA health_insurance TO hospitalos;
-- GRANT USAGE ON SCHEMA thai_ascvd TO hospitalos;
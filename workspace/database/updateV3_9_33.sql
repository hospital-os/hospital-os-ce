-- check item subgroup before calculate
insert into f_gui_action (f_gui_action_id, gui_action_name) VALUES ('5706', 'ตรวจสอบการดำเนินการก่อนคำนวณเงิน');

CREATE TABLE b_map_item_subgroup_check_calculate (
    b_map_item_subgroup_check_calculate_id    character varying(255)   NOT NULL,
    b_item_subgroup_id           character varying(255)   NOT NULL,
CONSTRAINT b_map_item_subgroup_check_calculate_pkey PRIMARY KEY (b_map_item_subgroup_check_calculate_id)
);
CREATE UNIQUE INDEX b_map_item_subgroup_check_calculate_index1
   ON b_map_item_subgroup_check_calculate (b_item_subgroup_id ASC NULLS LAST);

-- insurance
ALTER TABLE b_finance_insurance_company DROP COLUMN alert_repay;
ALTER TABLE b_finance_insurance_company DROP COLUMN alert_repay_day;
ALTER TABLE b_finance_insurance_company ADD COLUMN alert_repay_day integer NOT NULL DEFAULT 0;

CREATE TABLE b_map_finance_insurance (
    b_map_finance_insurance_id    character varying(255)   NOT NULL,
    b_contract_plans_id           character varying(255)   NOT NULL,
CONSTRAINT b_map_finance_insurance_pkey PRIMARY KEY (b_map_finance_insurance_id)
);
CREATE UNIQUE INDEX b_map_finance_insurance_index1
   ON b_map_finance_insurance (b_contract_plans_id ASC NULLS LAST);

-- change to b_finance_insurance_plan
drop table b_finance_insurance_discounts;

CREATE TABLE b_finance_insurance_plan (
    b_finance_insurance_plan_id    character varying(255)   NOT NULL,
    code                                character varying(255)   NOT NULL,
    description                         character varying(255)   NOT NULL,
    b_finance_insurance_company_id      character varying(255)   NOT NULL,
    active                              character varying(1)     NOT NULL,
    record_datetime                     character varying(19)    NOT NULL,
    update_datetime                     character varying(19)    NULL,
    user_record_id                      character varying(255)   NOT NULL,
    user_update_id                      character varying(255)   NULL,
CONSTRAINT b_finance_insurance_plan_pkey PRIMARY KEY (b_finance_insurance_plan_id)
);
CREATE UNIQUE INDEX b_finance_insurance_plan_index1
   ON b_finance_insurance_plan (code ASC NULLS LAST);

CREATE UNIQUE INDEX b_finance_insurance_company_index1
   ON b_finance_insurance_company (code ASC NULLS LAST);

--change structure
drop table b_finance_insurance_discounts_condition;

CREATE TABLE b_finance_insurance_discounts_condition  (
    b_finance_insurance_discounts_condition_id    character varying(255)   NOT NULL,
    b_finance_insurance_plan_id                   character varying(255)   NOT NULL,
    b_item_billing_subgroup_id                    character varying(255)   NOT NULL,
    condition_adjustment                          character varying(255)   NOT NULL,
CONSTRAINT b_finance_insurance_discounts_condition_pkey PRIMARY KEY (b_finance_insurance_discounts_condition_id)
);

CREATE TABLE t_visit_insurance_plan  (
    t_visit_insurance_plan_id                character varying(255)   NOT NULL,
    t_visit_payment_id                       character varying(255)   NOT NULL,
    b_finance_insurance_company_id           character varying(255)   NOT NULL,
    b_finance_insurance_plan_id              character varying(255)   NOT NULL,
    priority                                 character varying(2)   NOT NULL,
    active                                   character varying(1)   NOT NULL default '1',
    record_datetime                                      character varying(19)    NOT NULL,
    user_record_id                                       character varying(255)   NOT NULL,
    update_datetime                                      character varying(19)    NOT NULL,
    user_update_id                                       character varying(255)   NOT NULL,
CONSTRAINT t_visit_insurance_plan_pkey PRIMARY KEY (t_visit_insurance_plan_id)
);

delete from b_sequence_data where b_sequence_data_id = 'insurance';
INSERT INTO b_sequence_data VALUES ('claim', 'claim code', 'A000000', '1', '1');

-- change to t_insurance_claim
drop table t_finance_code;
CREATE TABLE t_insurance_claim  (
    t_insurance_claim_id            character varying(255)   NOT NULL,
    t_billing_invoice_id            character varying(255)   NOT NULL, 
    t_visit_insurance_plan_id       character varying(255)   NOT NULL, 
    claim_code                      character varying(255)   NOT NULL,
    claim_code_datetime             timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    status_insurance_approve        character varying(1)     NOT NULL,
    reason_not_approve              text                     NULL,
    record_datetime                 character varying(19)    NOT NULL,
    user_record_id                  character varying(255)   NOT NULL,
    update_datetime                 character varying(19)    NOT NULL,
    user_update_id                  character varying(255)   NOT NULL,
    alert_date                      date    NOT NULL DEFAULT CURRENT_DATE,
    status_insurance_receive        character varying(1)     NOT NULL,
CONSTRAINT t_insurance_claim_pkey PRIMARY KEY (t_insurance_claim_id)
);
CREATE UNIQUE INDEX t_insurance_claim_index1
   ON t_insurance_claim (claim_code ASC NULLS LAST);
CREATE INDEX t_insurance_claim_index2
   ON t_insurance_claim (t_visit_insurance_plan_id ASC NULLS LAST);

-- change to t_insurance_claim_subgroup
drop table t_finance_invoice_subgroup;

-- can not update
CREATE TABLE t_insurance_claim_subgroup  (
    t_insurance_claim_subgroup_id                  character varying(255)   NOT NULL,
    t_insurance_claim_id                           character varying(255)   NOT NULL,
    b_item_billing_subgroup_id                     character varying(255)   NOT NULL,
    t_billing_invoice_billing_subgroup_id          character varying(255)   NOT NULL,
    subgroup_total                                 double precision   NOT NULL DEFAULT 0.0,
    insurance_discount                             double precision   NOT NULL DEFAULT 0.0,
    insurance_pay                                  double precision   NOT NULL DEFAULT 0.0,
    subgroup_remain                                double precision   NOT NULL DEFAULT 0.0,
    record_datetime                                character varying(19)    NOT NULL,
    user_record_id                                 character varying(255)   NOT NULL,
CONSTRAINT t_insurance_claim_subgroup_pkey PRIMARY KEY (t_insurance_claim_subgroup_id)
);

-- repay
drop table t_finance_paid_life_insurance;
drop table t_finance_paid_life_insurance_bill;
drop table t_finance_paid_life_insurance_receipt;

CREATE TABLE t_insurance_claim_billing  (
    t_insurance_claim_billing_id            character varying(255)   NOT NULL,
    b_finance_insurance_company_id          character varying(255)   NOT NULL,
    total_debt                              double precision   NOT NULL DEFAULT 0.0,
    total_vat                               double precision   NOT NULL DEFAULT 0.0,
    total_tax                               double precision   NOT NULL DEFAULT 0.0,
    total_payment                           double precision   NOT NULL DEFAULT 0.0,
    f_payment_type_id                       character varying(2)   NOT NULL,
    b_bank_info_id                          character varying(255)   NOT NULL,
    bank_branch_name                        text,
    account_number                          character varying(255)   NOT NULL,
    payment_date                            date    NOT NULL DEFAULT CURRENT_DATE,
    record_datetime                 character varying(19)    NOT NULL,
    user_record_id                  character varying(255)   NOT NULL,
CONSTRAINT t_insurance_claim_billing_pkey PRIMARY KEY (t_insurance_claim_billing_id)
);

CREATE TABLE t_insurance_claim_billing_item  (
    t_insurance_claim_billing_item_id           character varying(255)   NOT NULL,
    t_insurance_claim_billing_id                character varying(255)   NOT NULL,
    t_insurance_claim_id                        character varying(255)   NOT NULL,
    total_payment                           double precision   NOT NULL DEFAULT 0.0,
    record_datetime                 character varying(19)    NOT NULL,
    user_record_id                  character varying(255)   NOT NULL,
CONSTRAINT t_insurance_claim_billing_item_pkey PRIMARY KEY (t_insurance_claim_billing_item_id)
);
CREATE INDEX t_insurance_claim_billing_item_index1
   ON t_insurance_claim_billing_item (t_insurance_claim_billing_id ASC NULLS LAST);
CREATE INDEX t_insurance_claim_billing_item_index2
   ON t_insurance_claim_billing_item (t_insurance_claim_id ASC NULLS LAST);

CREATE TABLE t_insurance_claim_receipt  (
    t_insurance_claim_receipt_id           character varying(255)   NOT NULL,
    t_insurance_claim_billing_id           character varying(255)   NOT NULL,
    receipt_number                         character varying(255)   NOT NULL,
    receipt_display_name                   character varying(255)   NOT NULL,
    receipt_type                           character varying(1)     NOT NULL default '0', -- 0 = Separate, 1 = Together
    t_patient_id                           character varying(255)   NULL, -- have data when receipt_type is 0
    total_print                            integer   NOT NULL default 0, 
    record_datetime                        character varying(19)    NOT NULL,
    user_record_id                         character varying(255)   NOT NULL,
CONSTRAINT t_insurance_claim_receipt_pkey PRIMARY KEY (t_insurance_claim_receipt_id)
);
CREATE INDEX t_insurance_claim_receipt_index1
   ON t_insurance_claim_receipt (t_insurance_claim_billing_id ASC NULLS LAST);
CREATE UNIQUE INDEX t_insurance_claim_receipt_index2
   ON t_insurance_claim_receipt (receipt_number ASC NULLS LAST);

-- setup
insert into f_gui_action (f_gui_action_id, gui_action_name) VALUES ('6003', 'จับคู่สิทธิ์ประกัน');

insert into f_gui_action (f_gui_action_id, gui_action_name) VALUES ('1100', 'เมนูการเงิน');
insert into f_gui_action (f_gui_action_id, gui_action_name) VALUES ('1101', 'รับชำระหนี้ประกัน');
insert into f_gui_action (f_gui_action_id, gui_action_name) VALUES ('1102', 'รายงานใบแจ้งหนี้ประกัน');

update f_gui_action set  gui_action_name = 'รายการแผนประกัน'where f_gui_action_id = '6002';
update b_option_detail set option_detail_name = 'nhso.png' where b_option_detail_id = 'b1_icon' and option_detail_name = 'b1.gif';

-- update db version
INSERT INTO s_version VALUES ('9701000000066', '66', 'Hospital OS, Community Edition', '3.9.33', '3.21.210513', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_33.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.33');
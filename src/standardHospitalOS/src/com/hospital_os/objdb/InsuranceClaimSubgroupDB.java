/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.InsuranceClaimSubgroup;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class InsuranceClaimSubgroupDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "820";

    public InsuranceClaimSubgroupDB(ConnectionInf c) {
        this.connectionInf = c;
    }

    public int insert(InsuranceClaimSubgroup obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO t_insurance_claim_subgroup(\n"
                    + "            t_insurance_claim_subgroup_id, t_insurance_claim_id, b_item_billing_subgroup_id, t_billing_invoice_billing_subgroup_id, subgroup_total, insurance_discount, insurance_pay, subgroup_remain, record_datetime, user_record_id)\n"
                    + "    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getGenID(tableId));
            preparedStatement.setString(2, obj.t_insurance_claim_id);
            preparedStatement.setString(3, obj.b_item_billing_subgroup_id);
            preparedStatement.setString(4, obj.t_billing_invoice_billing_subgroup_id);
            preparedStatement.setDouble(5, obj.subgroup_total);
            preparedStatement.setDouble(6, obj.insurance_discount);
            preparedStatement.setDouble(7, obj.insurance_pay);
            preparedStatement.setDouble(8, obj.subgroup_remain);
            preparedStatement.setString(9, obj.record_datetime);
            preparedStatement.setString(10, obj.user_record_id);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public InsuranceClaimSubgroup select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select t_insurance_claim_subgroup.*, b_item_billing_subgroup.item_billing_subgroup_description from t_insurance_claim_subgroup "
                    + "inner join b_item_billing_subgroup on b_item_billing_subgroup.b_item_billing_subgroup_id = t_insurance_claim_subgroup.b_item_billing_subgroup_id "
                    + "where t_insurance_claim_subgroup_id = ? ";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<InsuranceClaimSubgroup> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<InsuranceClaimSubgroup> listByClaimId(String t_insurance_claim_id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select t_insurance_claim_subgroup.*, b_item_billing_subgroup.item_billing_subgroup_description from t_insurance_claim_subgroup "
                    + "inner join b_item_billing_subgroup on b_item_billing_subgroup.b_item_billing_subgroup_id = t_insurance_claim_subgroup.b_item_billing_subgroup_id "
                    + "where t_insurance_claim_id = ? order by b_item_billing_subgroup.item_billing_subgroup_description";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, t_insurance_claim_id);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<Object[]> listClaimInfo(String t_insurance_claim_id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select\n"
                    + "t_billing_invoice_billing_subgroup.t_billing_invoice_billing_subgroup_id\n"
                    + ", b_item_billing_subgroup.b_item_billing_subgroup_id\n"
                    + ", b_item_billing_subgroup.item_billing_subgroup_description\n"
                    + ", t_billing_invoice_billing_subgroup.billing_invoice_billing_subgroup_patient_share\n"
                    + ", case when b_finance_insurance_discounts_condition.condition_adjustment is not null and b_finance_insurance_discounts_condition.condition_adjustment <> ''\n"
                    + "then (t_billing_invoice_billing_subgroup.billing_invoice_billing_subgroup_patient_share * (to_number(b_finance_insurance_discounts_condition.condition_adjustment, '999') / 100))\n"
                    + "else 0 end as dicount\n"
                    + ", case when t_insurance_claim_subgroup.insurance_pay is not null then t_insurance_claim_subgroup.insurance_pay\n"
                    + "else 0 end as paid\n"
                    + "from t_billing_invoice_billing_subgroup\n"
                    + "inner join t_insurance_claim on t_insurance_claim.t_billing_invoice_id = t_billing_invoice_billing_subgroup.t_billing_invoice_id\n"
                    + "inner join b_item_billing_subgroup on b_item_billing_subgroup.b_item_billing_subgroup_id = t_billing_invoice_billing_subgroup.b_item_billing_subgroup_id\n"
                    + "inner join t_visit_insurance_plan on t_visit_insurance_plan.t_visit_payment_id = t_billing_invoice_billing_subgroup.t_payment_id\n"
                    + "and t_visit_insurance_plan.t_visit_insurance_plan_id = t_insurance_claim.t_visit_insurance_plan_id\n"
                    + "left join b_finance_insurance_discounts_condition on b_finance_insurance_discounts_condition.b_finance_insurance_plan_id = t_visit_insurance_plan.b_finance_insurance_plan_id\n"
                    + "and b_finance_insurance_discounts_condition.b_item_billing_subgroup_id = t_billing_invoice_billing_subgroup.b_item_billing_subgroup_id\n"
                    + "left join t_insurance_claim_subgroup on t_insurance_claim_subgroup.b_item_billing_subgroup_id =  t_billing_invoice_billing_subgroup.b_item_billing_subgroup_id\n"
                    + "and t_insurance_claim_subgroup.t_insurance_claim_id = t_insurance_claim.t_insurance_claim_id\n"
                    + "where \n"
                    + "t_insurance_claim.t_insurance_claim_id = ?\n"
                    + "and billing_invoice_billing_subgroup_active = '1'\n"
                    + "order by b_item_billing_subgroup.item_billing_subgroup_description;\n"
                    + "";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, t_insurance_claim_id);
            return connectionInf.eComplexQuery(preparedStatement.toString());
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<InsuranceClaimSubgroup> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<InsuranceClaimSubgroup> list = new ArrayList<InsuranceClaimSubgroup>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                InsuranceClaimSubgroup obj = new InsuranceClaimSubgroup();
                obj.setObjectId(rs.getString("t_insurance_claim_subgroup_id"));
                obj.t_insurance_claim_id = rs.getString("t_insurance_claim_id");
                obj.subgroup_total = rs.getDouble("subgroup_total");
                obj.b_item_billing_subgroup_id = rs.getString("b_item_billing_subgroup_id");
                obj.t_billing_invoice_billing_subgroup_id = rs.getString("t_billing_invoice_billing_subgroup_id");
                obj.insurance_pay = rs.getDouble("insurance_pay");
                obj.insurance_discount = rs.getDouble("insurance_discount");
                obj.subgroup_remain = rs.getDouble("subgroup_remain");
                obj.record_datetime = rs.getString("record_datetime");
                obj.user_record_id = rs.getString("user_record_id");
                try {
                    obj.billingSubgroupName = rs.getString("item_billing_subgroup_description");
                } catch (Exception ex) {
                }
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }
}

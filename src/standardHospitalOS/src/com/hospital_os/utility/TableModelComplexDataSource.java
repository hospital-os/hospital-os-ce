/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.utility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class TableModelComplexDataSource extends AbstractTableModel {

    private static final long serialVersionUID = 1L;
    private final String[] columns;
    private final boolean isEnableCheck;
    private final boolean isShowIndex;
    private final List<ComplexDataSource> data = new ArrayList<ComplexDataSource>();
    private final List<Boolean> checked = new ArrayList<Boolean>();
    private final List<Integer> colEditables = new ArrayList<Integer>();
    private final Map<Integer, List<Integer>> cellDisables = new HashMap<Integer, List<Integer>>();
    private int startIndex = 1;

    public TableModelComplexDataSource(String[] columns) {
        this(columns, false);
    }

    public TableModelComplexDataSource(String[] columns, boolean isCheckable) {
        this(columns, isCheckable, false);
    }

    public TableModelComplexDataSource(String[] columns, boolean isCheckable, boolean isShowIndex) {
        this.isEnableCheck = isCheckable;
        this.isShowIndex = isShowIndex;
        if (this.isEnableCheck || this.isShowIndex) {
            this.columns = new String[columns.length
                    + (this.isEnableCheck ? 1 : 0)
                    + (this.isShowIndex ? 1 : 0)];
            int index = 0;
            if (this.isEnableCheck) {
                int col = index++;
                this.columns[col] = "";
                addColEditable(col);
            }
            if (this.isShowIndex) {
                this.columns[index++] = "#";
            }
            for (int i = index; i < this.columns.length; i++) {
                this.columns[i] = columns[i - index];
            }
        } else {
            this.columns = columns;
        }
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public ComplexDataSource getRow(int row) {
        return data.get(row);
    }

    public void addColEditable(int column) {
        colEditables.add(column);
    }

    public void removeColEditable(int column) {
        Collections.sort(colEditables);
        for (int i = 0; i < colEditables.size(); i++) {
            if (colEditables.get(i) == column) {
                colEditables.remove(i);
                break;
            }
        }
    }

    public void addDisableCellEditable(int row, int column) {
        boolean containsKey = cellDisables.containsKey(row);
        List<Integer> cols = new ArrayList<>();
        if (containsKey) {
            cols.addAll(cellDisables.get(row));
        }
        cols.add(column);
        cellDisables.put(row, cols);
    }

    public void removeDisableCellEditable(int row, int column) {
        boolean containsKey = cellDisables.containsKey(row);
        if (!containsKey) {
            return;
        }
        List<Integer> cols = cellDisables.get(row);

        Collections.sort(cols);
        for (int i = 0; i < cols.size(); i++) {
            if (cols.get(i) == column) {
                cols.remove(i);
                break;
            }
        }

        if (cols.isEmpty()) {
            cellDisables.remove(row);
        }
    }

    @Override
    public boolean isCellEditable(int row, int col) {
        if (!cellDisables.isEmpty()) {
            boolean containsKey = cellDisables.containsKey(row);
            if (containsKey) {
                if (cellDisables.get(row).contains(col)) {
                    return false;
                }
            }
        }

        return colEditables.contains(col);

    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return columns.length;
    }

    @Override
    public String getColumnName(int col) {
        return columns[col];
    }

    @Override
    public Object getValueAt(int row, int col) {
        if (isEnableCheck || isShowIndex) {
            if (col == 0 && isEnableCheck) {
                Boolean objects = checked.get(row);
                return objects;
            } else if (col == 0 && !isEnableCheck && isShowIndex) {
                return startIndex + row;
            } else if (col == 1 && isEnableCheck && isShowIndex) {
                return startIndex + row;
            } else {
                ComplexDataSource objects = data.get(row);
                Object[] values = objects.getValues();
                return values[col - (1 + (isEnableCheck && isShowIndex ? 1 : 0))];
            }
        } else {
            ComplexDataSource objects = data.get(row);
            Object[] values = objects.getValues();
            return values[col];
        }
    }

    @SuppressWarnings("UseOfObsoleteCollectionType")
    public List<ComplexDataSource> getData() {
        return data;
    }

    public List<Boolean> getChecked() {
        return checked;
    }

    public void setComplexDataSources(List<ComplexDataSource> datas) {
        setComplexDataSources(datas, false);
    }

    public void setComplexDataSources(List<ComplexDataSource> datas, boolean isChecked) {
        data.clear();
        checked.clear();
        if (datas != null && !datas.isEmpty()) {
            data.addAll(datas);
            for (int i = 0; i < datas.size(); i++) {
                checked.add(isChecked);
            }
        }
    }

    public void addComplexDataSource(ComplexDataSource objects) {
        addComplexDataSource(objects, false);
    }

    public void addComplexDataSource(ComplexDataSource objects, int index) {
        addComplexDataSource(objects, index, false);
    }

    public void addComplexDataSource(ComplexDataSource objects, boolean isChecked) {
        data.add(objects);
        checked.add(isChecked);
    }

    public void addComplexDataSource(ComplexDataSource objects, int index, boolean isChecked) {
        data.add(index, objects);
        checked.add(index, isChecked);
    }

    public void addNotComplexDataSource(ComplexDataSource objects) {
        addNotComplexDataSource(objects, false);
    }

    public void addNotComplexDataSource(ComplexDataSource objects, int index) {
        addNotComplexDataSource(objects, index, false);
    }

    public void addNotComplexDataSource(ComplexDataSource objects, boolean isChecked) {
        List<ComplexDataSource> dsClone = new ArrayList<ComplexDataSource>();
        dsClone.addAll(data);
        Collections.sort(dsClone, new ComplexDataSource());
        int binarySearch = Collections.binarySearch(dsClone, objects, new ComplexDataSource());
        if (binarySearch < 0) {
            data.add(objects);
            checked.add(isChecked);
        }
        dsClone.clear();
    }

    public void addNotComplexDataSource(ComplexDataSource objects, int index, boolean isChecked) {
        List<ComplexDataSource> dsClone = new ArrayList<ComplexDataSource>();
        dsClone.addAll(data);
        Collections.sort(dsClone, new ComplexDataSource());
        int binarySearch = Collections.binarySearch(dsClone, objects, new ComplexDataSource());
        if (binarySearch < 0) {
            data.add(index, objects);
            checked.add(index, isChecked);
        }
        dsClone.clear();
    }

    public void addNotComplexDataSource(List<ComplexDataSource> objects) {
        addNotComplexDataSource(objects, false);
    }

    public void addNotComplexDataSource(List<ComplexDataSource> objects, boolean isChecked) {
        for (ComplexDataSource dataSource : objects) {
            addNotComplexDataSource(dataSource);
        }
    }

    @Override
    public void setValueAt(Object value, int row, int col) {
        int offset = (isEnableCheck ? 1 : 0) + (isShowIndex ? 1 : 0);
        if (col == 0 && isEnableCheck) {
            checked.remove(row);
            checked.add(row, (Boolean) value);
            fireTableCellUpdated(row, col);
        } else {
            ComplexDataSource complexDataSource = data.get(row);
            complexDataSource.setValue(col - offset, value);
            fireTableCellUpdated(row, col - offset);
        }
    }

    public void clearTable() {
        getData().clear();
        getChecked().clear();
        cellDisables.clear();
        fireTableDataChanged();
    }

    public void removeData(int index) {
        data.remove(index);
        checked.remove(index);
        cellDisables.remove(index);
        fireTableDataChanged();
    }

    public void removeDatas(int[] indexs) {
        for (int i = indexs.length - 1; i >= 0; i--) {
            data.remove(indexs[i]);
            checked.remove(indexs[i]);
        }
        fireTableDataChanged();
    }

    public boolean isChecked(int row) {
        return checked.get(row);
    }

    public Object getSelectedId(int row) {
        ComplexDataSource objects = data.get(row);
        return objects.getId();
    }

    public Object[] getSelectedValues(int row) {
        ComplexDataSource objects = data.get(row);
        return objects.getValues();
    }

    public Object getSelectedValues(int row, int col) {
        ComplexDataSource objects = data.get(row);
        return objects.getValues()[col];
    }

    public ComplexDataSource getSelectedComplexDataSource(int row) {
        return data.get(row);
    }

    public int[] getCheckedRows() {
        List<Integer> indexs = new ArrayList<Integer>();
        for (int i = 0; i < checked.size(); i++) {
            if (checked.get(i)) {
                indexs.add(i);
            }
        }
        int[] rows = new int[indexs.size()];
        for (int i = 0; i < rows.length; i++) {
            rows[i] = indexs.get(i);
        }
        return rows;
    }

    public List<Boolean> listChecks(int[] rows) {
        List<Boolean> list = new ArrayList<Boolean>();
        for (int row : rows) {
            Boolean isChecked = checked.get(row);
            list.add(isChecked);
        }
        return list;
    }

    public List<Object> getCheckedIds() {
        List<Object> list = new ArrayList<Object>();
        for (int row : getCheckedRows()) {
            ComplexDataSource objects = data.get(row);
            list.add(objects.getId());
        }
        return list;
    }

    public List<Object> getSelectedIds(int[] rows) {
        List<Object> list = new ArrayList<Object>();
        for (int row : rows) {
            ComplexDataSource objects = data.get(row);
            list.add(objects.getId());
        }
        return list;
    }

    public List<Object[]> getCheckedValues() {
        List<Object[]> list = new ArrayList<Object[]>();
        for (int row : getCheckedRows()) {
            ComplexDataSource objects = data.get(row);
            list.add(objects.getValues());
        }
        return list;
    }

    public List<Object[]> getSelectedValues(int[] rows) {
        List<Object[]> list = new ArrayList<Object[]>();
        for (int row : rows) {
            ComplexDataSource objects = data.get(row);
            list.add(objects.getValues());
        }
        return list;
    }

    public List<ComplexDataSource> getCheckdComplexDataSources() {
        List<ComplexDataSource> list = new ArrayList<ComplexDataSource>();
        for (int row : getCheckedRows()) {
            ComplexDataSource objects = data.get(row);
            list.add(objects);
        }
        return list;
    }

    public List<ComplexDataSource> getSelectedComplexDataSources(int[] rows) {
        List<ComplexDataSource> list = new ArrayList<ComplexDataSource>();
        for (int row : rows) {
            ComplexDataSource objects = data.get(row);
            list.add(objects);
        }
        return list;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.usecase.transaction;

import javax.swing.JComponent;

/**
 *
 * @author Somprasong
 */
public interface NotificationObserver {

    public void add(String key, JComponent component);

    public void remove(String key);
    
    public void update();
    
    public void show(String key);
    
    public void hide(String key);
}

-- #531
ALTER TABLE t_health_epidem_report ADD IF NOT EXISTS sent_epidem BOOLEAN NOT NULL DEFAULT TRUE; -- ส่งข้อมูลกองระบาด
ALTER TABLE t_health_epidem_report ADD IF NOT EXISTS sent_moph BOOLEAN NOT NULL DEFAULT TRUE; -- ส่งข้อมูล MOPH
ALTER TABLE t_health_epidem_report ADD IF NOT EXISTS sent_moph_complete INTEGER NOT NULL DEFAULT 0; -- สถานะส่งข้อมูล MOPH (0 = not sent ,1 = sent completed, 2 = sent failed)

UPDATE f_epidem_person_status SET epidem_person_status_name = 'กำลังรักษาตัว' where f_epidem_person_status_id = '1';
UPDATE f_epidem_person_status SET epidem_person_status_name = 'หายจากโรคแล้ว' where f_epidem_person_status_id = '2';
UPDATE f_epidem_person_status SET epidem_person_status_name = 'เสียชีวิต' where f_epidem_person_status_id = '3';
UPDATE f_epidem_person_status SET epidem_person_status_name = 'ไม่ทราบ' where f_epidem_person_status_id = '4';

CREATE OR REPLACE FUNCTION moph_epidem_report_V39(epidem_report_id text)
RETURNS TABLE (epidem_query json) AS $$
    DECLARE
    BEGIN
return query 
select json_build_object(
        'hospital',jsonb_build_object(
            'hospital_code',epidem_report.hospital_code
            ,'hospital_name',epidem_report.hospital_name
            ,'his_identifier',epidem_report.his_identifier
            ),
        'person',jsonb_build_object(
            'cid',epidem_report.cid
            ,'passport_no',epidem_report.passport_no
            ,'prefix',epidem_report.prefix
            ,'first_name',epidem_report.first_name
            ,'last_name',epidem_report.last_name
            ,'nationality',epidem_report.nationality
            ,'gender',epidem_report.gender
            ,'birth_date',epidem_report.birth_date
            ,'mobile_phone',epidem_report.mobile_phone
            ,'address',epidem_report.address
            ,'moo',epidem_report.moo
            ,'road',epidem_report.road
            ,'chw_code',epidem_report.chw_code
            ,'amp_code',epidem_report.amp_code
            ,'tmb_code',epidem_report.tmb_code
            ),
        'epidem_report',jsonb_build_object(
            'epidem_report_guid',epidem_report.epidem_report_guid
            ,'epidem_report_group_id',epidem_report.epidem_report_group_id
            ,'treated_hospital_code',epidem_report.treated_hospital_code
            ,'principal_diagnosis_icd10',epidem_report.principal_diagnosis_icd10
            ,'report_datetime',epidem_report.report_datetime
            ,'informer_name',epidem_report.informer_name
            ,'onset_date',epidem_report.onset_date
            ,'treated_date',epidem_report.treated_date
            ,'diagnosis_date',epidem_report.diagnosis_date
            ,'address',epidem_report.epidem_address
            ,'address_moo',epidem_report.epidem_moo
            ,'address_road',epidem_report.epidem_road
            ,'address_chw_code',epidem_report.epidem_chw_code
            ,'address_amp_code',epidem_report.epidem_amp_code
            ,'address_tmb_code',epidem_report.epidem_tmb_code
            ,'location_gis_latitude',epidem_report.location_gis_latitude
            ,'location_gis_longitude',epidem_report.location_gis_longitude
            ,'pregnant_status',epidem_report.pregnant_status
            ,'respirator_status',epidem_report.respirator_status
            ,'vaccinated_status',epidem_report.vaccinated_status
            ,'epidem_person_status_id',epidem_report.epidem_person_status_id
            ,'epidem_symptom_type_id',epidem_report.epidem_symptom_type_id
            ,'epidem_accommodation_type_id',epidem_report.epidem_accommodation_type_id
            ,'epidem_lab_confirm_type_id',epidem_report.epidem_lab_confirm_type_id
            ,'exposure_epidemic_area_status',epidem_report.exposure_epidemic_area_status
            ,'exposure_healthcare_worker_status',epidem_report.exposure_healthcare_worker_status
            ,'exposure_closed_contact_status',epidem_report.exposure_closed_contact_status
            ,'exposure_occupation_status',epidem_report.exposure_occupation_status
            ,'exposure_travel_status',epidem_report.exposure_travel_status
            )) as epidem_report
from (select b_site.b_visit_office_id as hospital_code
        ,b_site.site_full_name as hospital_name
        ,'HospitalOS ' || (select version_application_number from s_version order by s_version.version_update_time desc limit 1) as his_identifier
        ,t_patient.patient_pid as cid
        ,t_health_family.passport_no as passport_no
        ,case when f_patient_prefix.patient_prefix_description is null then ''
              else f_patient_prefix.patient_prefix_description end as prefix
        ,t_patient.patient_firstname as first_name
        ,t_patient.patient_lastname as last_name
        ,f_patient_nation.r_rp1853_nation_id as nationality
        ,case when t_patient.f_sex_id in ('1','2') then t_patient.f_sex_id
              else '0' end::int as gender
        ,to_char(text_to_timestamp(t_patient.patient_birthday),'YYYY-MM-DD') as birth_date
        ,case when EXTRACT(YEAR FROM age(text_to_timestamp(t_patient.patient_birthday)))::int IS NOT NULL and EXTRACT(YEAR FROM age(text_to_timestamp(t_patient.patient_birthday)))::int > 0
              then EXTRACT(YEAR FROM age(text_to_timestamp(t_patient.patient_birthday)))::int else 0 end as age_y
        ,case when EXTRACT(MONTH FROM age(text_to_timestamp(t_patient.patient_birthday)))::int IS NOT NULL and EXTRACT(MONTH FROM age(text_to_timestamp(t_patient.patient_birthday)))::int > 0
              then EXTRACT(MONTH FROM age(text_to_timestamp(t_patient.patient_birthday)))::int else 0 end as age_m
        ,case when EXTRACT(DAY FROM age(text_to_timestamp(t_patient.patient_birthday)))::int IS NOT NULL and EXTRACT(DAY FROM age(text_to_timestamp(t_patient.patient_birthday)))::int > 0
              then EXTRACT(DAY FROM age(text_to_timestamp(t_patient.patient_birthday)))::int else 0 end as age_d
        ,case when t_patient.f_patient_marriage_status_id in ('1','2') then t_patient.f_patient_marriage_status_id::int
              when t_patient.f_patient_marriage_status_id in ('3','4') then '3'::int
              when t_patient.f_patient_marriage_status_id = '5' then '4'::int
              else null end as marital_status_id
        ,case when t_patient.patient_house <> '' then t_patient.patient_house
              else '' end as address
        ,case when t_patient.patient_moo <> '' then t_patient.patient_moo
              else '' end as moo
        ,case when t_patient.patient_road <> '' then t_patient.patient_road
              else '' end as road
        ,case when t_patient.patient_tambon <> '' then substr(t_patient.patient_tambon,1,2)
              else '' end as chw_code
        ,case when t_patient.patient_tambon <> '' then substr(t_patient.patient_tambon,3,2)
              else '' end as amp_code
        ,case when t_patient.patient_tambon <> '' then substr(t_patient.patient_tambon,5,2)
              else '' end as tmb_code
        ,case when t_patient.patient_patient_mobile_phone <> '' then t_patient.patient_patient_mobile_phone
              else '' end as mobile_phone
        ,case when r_rp1855_occupation.code is not null then r_rp1855_occupation.code
              else '' end as occupation
        ,case when t_health_epidem_report.epidem_report_guid is not null then '{' || upper(t_health_epidem_report.epidem_report_guid::text) || '}'
              else '' end epidem_report_guid
        ,92 as epidem_report_group_id
        ,t_health_epidem_report.treated_hospital_code
        ,to_char(t_health_epidem_report.report_datetime,'YYYY-MM-DDThh24:mi:ss.ms') as report_datetime
        ,to_char(t_health_epidem_report.onset_date,'YYYY-MM-DD') as onset_date
        ,to_char(t_health_epidem_report.treated_date,'YYYY-MM-DD') as treated_date
        ,case when icd10_primary.diagnosis_date is not null 
              then to_char(icd10_primary.diagnosis_date,'YYYY-MM-DD')
              else '' end as diagnosis_date
        ,case when employee_prefix.f_patient_prefix_id is not null and employee_prefix.f_patient_prefix_id <> '000'
              then employee_prefix.patient_prefix_description else '' end
         || employee_person.person_firstname || '  ' || employee_person.person_lastname as informer_name
        ,case when icd10_primary.icd10_code is not null then icd10_primary.icd10_code
              else '' end as principal_diagnosis_icd10
        ,t_health_epidem_report.f_epidem_person_status_id as epidem_person_status_id
        ,t_health_epidem_report.f_epidem_covid_symptom_type_id as epidem_symptom_type_id
        ,case when t_health_epidem_report.pregnant_status is true then 'Y'
              else 'N' end as pregnant_status
        ,case when t_health_epidem_report.respirator_status is true then 'Y'
              else 'N' end as respirator_status
        ,case when epidem_address.f_address_housetype_id in ('1','2') then epidem_address.f_address_housetype_id::int
              when epidem_address.f_address_housetype_id in ('3','4') then 3
              when epidem_address.f_address_housetype_id = '5' then 4
              when epidem_address.f_address_housetype_id = '8' then 5
              else 5 end as epidem_accommodation_type_id
        ,case when t_health_epidem_report.vaccinated_status is true then 'Y'
              else 'N' end as vaccinated_status
        ,case when t_health_epidem_report.exposure_epidemic_area_status is true then 'Y'
              else 'N' end as exposure_epidemic_area_status
        ,case when t_health_epidem_report.exposure_healthcare_worker_status is true then 'Y'
              else 'N' end as exposure_healthcare_worker_status
        ,case when t_health_epidem_report.exposure_closed_contact_status is true then 'Y'
              else 'N' end as exposure_closed_contact_status
        ,case when t_health_epidem_report.exposure_occupation_status is true then 'Y'
              else 'N' end as exposure_occupation_status
        ,case when t_health_epidem_report.exposure_travel_status is true then 'Y'
              else 'N' end as exposure_travel_status
        ,t_health_epidem_report.f_epidem_risk_history_type_id as risk_history_type_id
        ,case when epidem_address.house_no is not null then epidem_address.house_no
              else '' end as epidem_address
        ,case when epidem_address.village_no is not null then epidem_address.village_no
              else '' end as epidem_moo
        ,case when epidem_address.road is not null then epidem_address.road
              else '' end as epidem_road
        ,case when epidem_address.sub_district_id is not null then substr(epidem_address.sub_district_id,1,2)
              else '' end as epidem_chw_code
        ,case when epidem_address.sub_district_id is not null then substr(epidem_address.sub_district_id,3,2)
              else '' end as epidem_amp_code
        ,case when epidem_address.sub_district_id is not null then substr(epidem_address.sub_district_id,5,2)
              else '' end as epidem_tmb_code
        ,case when epidem_address.latitude is not null then epidem_address.latitude::float
              else 0 end as location_gis_latitude
        ,case when epidem_address.longitude is not null then epidem_address.longitude::float
              else 0 end as location_gis_longitude
        ,case when t_health_epidem_report.isolate_province_id is not null then substr(t_health_epidem_report.isolate_province_id,1,2)
              else '' end as isolate_chw_code
        ,t_health_epidem_report.f_epidem_covid_isolate_place_id as isolate_place_id
        ,case when t_visit.f_visit_type_id = '1' then 'IPD' else 'OPD' end as patient_type
        ,case when t_health_epidem_report.b_item_id is not null
              then t_health_epidem_report.f_epidem_covid_lab_confirm_type_id
              else null end as epidem_lab_confirm_type_id
        ,case when t_health_epidem_report.b_item_id is not null and t_health_epidem_report.lab_report_date is not null
              then to_char(t_health_epidem_report.lab_report_date,'YYYY-MM-DD')
              else null end as lab_report_date
        ,case when t_health_epidem_report.b_item_id is not null and t_health_epidem_report.lab_value is not null
              then t_health_epidem_report.lab_value
              else null end as lab_report_result
        ,case when t_health_epidem_report.b_item_id is not null and t_health_epidem_report.specimen_date is not null
              then to_char(t_health_epidem_report.specimen_date,'YYYY-MM-DD')
              else null end as specimen_date
        ,case when t_health_epidem_report.b_item_id is not null
              then t_health_epidem_report.f_epidem_covid_spcm_place_id
              else null end as specimen_place_id
        ,case when t_health_epidem_report.b_item_id is not null
              then t_health_epidem_report.f_epidem_covid_reason_type_id
              else null end as tests_reason_type_id
        ,b_item.b_item_id
    from t_health_epidem_report
        inner join t_visit on t_health_epidem_report.t_visit_id = t_visit.t_visit_id
        inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
        inner join t_health_family on t_patient.t_health_family_id = t_health_family.t_health_family_id
        left join f_patient_prefix on t_patient.f_patient_prefix_id = f_patient_prefix.f_patient_prefix_id
        left join f_patient_nation on t_patient.f_patient_nation_id = f_patient_nation.f_patient_nation_id
        left join f_patient_occupation on t_patient.f_patient_occupation_id = f_patient_occupation.f_patient_occupation_id
        left join b_map_rp1855_occupation on f_patient_occupation.f_patient_occupation_id = b_map_rp1855_occupation.f_patient_occupation_id
        left join r_rp1855_occupation on b_map_rp1855_occupation.r_rp1855_occupation_id = r_rp1855_occupation.id
        left join t_address as epidem_address on t_health_epidem_report.t_address_id = epidem_address.t_address_id
        left join (select t_diag_icd10.diag_icd10_vn as t_visit_id
                        ,text_to_timestamp(t_diag_icd10.diag_icd10_diagnosis_date) as diagnosis_date
                        ,replace(t_diag_icd10.diag_icd10_number,'.','') as icd10_code
                    from t_diag_icd10
                        inner join t_visit on t_diag_icd10.diag_icd10_vn = t_visit.t_visit_id
                        inner join t_health_epidem_report on t_health_epidem_report.t_visit_id = t_visit.t_visit_id
                    where t_diag_icd10.diag_icd10_active = '1'
                        and t_diag_icd10.f_diag_icd10_type_id = '1'
                        and t_health_epidem_report.t_health_epidem_report_id = $1
                )as icd10_primary on icd10_primary.t_visit_id = t_visit.t_visit_id
        left join b_item on t_health_epidem_report.b_item_id = b_item.b_item_id
        left join b_map_lab_tmlt on b_item.b_item_id = b_map_lab_tmlt.b_item_id
        left join b_employee on t_health_epidem_report.user_report_id = b_employee.b_employee_id
        left join t_person as employee_person on b_employee.t_person_id = employee_person.t_person_id
        left join f_patient_prefix as employee_prefix on employee_person.f_prefix_id = employee_prefix.f_patient_prefix_id
        cross join b_site
    where t_health_epidem_report.t_health_epidem_report_id = $1
        and t_health_epidem_report.active = '1'
    ) as epidem_report;
END;
$$
LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION doe_epidem_report_V39(epidem_report_id text)
RETURNS TABLE (epidem_query json) AS $$
    DECLARE
    BEGIN
return query 
select json_build_object(
        'hospital',jsonb_build_object(
            'hospital_code',epidem_report.hospital_code
            ,'hospital_name',epidem_report.hospital_name
            ,'his_identifier',epidem_report.his_identifier
            ),
        'person',jsonb_build_object(
            'cid',epidem_report.cid
            ,'passport_no',epidem_report.passport_no
            ,'prefix',epidem_report.prefix
            ,'first_name',epidem_report.first_name
            ,'last_name',epidem_report.last_name
            ,'nationality',epidem_report.nationality
            ,'gender',epidem_report.gender
            ,'birth_date',epidem_report.birth_date
            ,'age_y',epidem_report.age_y
            ,'age_m',epidem_report.age_m
            ,'age_d',epidem_report.age_d
            ,'marital_status_id',epidem_report.marital_status_id
            ,'address',epidem_report.address
            ,'moo',epidem_report.moo
            ,'road',epidem_report.road
            ,'chw_code',epidem_report.chw_code
            ,'amp_code',epidem_report.amp_code
            ,'tmb_code',epidem_report.tmb_code
            ,'mobile_phone',epidem_report.mobile_phone
            ,'occupation',epidem_report.occupation
            ),
        'epidem_report',jsonb_build_object(
            'epidem_report_guid',epidem_report.epidem_report_guid
            ,'epidem_report_group_id',epidem_report.epidem_report_group_id
            ,'treated_hospital_code',epidem_report.treated_hospital_code
            ,'report_datetime',epidem_report.report_datetime
            ,'onset_date',epidem_report.onset_date
            ,'treated_date',epidem_report.treated_date
            ,'diagnosis_date',epidem_report.diagnosis_date
            ,'informer_name',epidem_report.informer_name
            ,'principal_diagnosis_icd10',epidem_report.principal_diagnosis_icd10
            ,'diagnosis_icd10_list',epidem_report.diagnosis_icd10_list
            ,'epidem_person_status_id',epidem_report.epidem_person_status_id
            ,'epidem_symptom_type_id',epidem_report.epidem_symptom_type_id
            ,'pregnant_status',epidem_report.pregnant_status
            ,'respirator_status',epidem_report.respirator_status
            ,'epidem_accommodation_type_id',epidem_report.epidem_accommodation_type_id
            ,'vaccinated_status',epidem_report.vaccinated_status
            ,'exposure_epidemic_area_status',epidem_report.exposure_epidemic_area_status
            ,'exposure_healthcare_worker_status',epidem_report.exposure_healthcare_worker_status
            ,'exposure_closed_contact_status',epidem_report.exposure_closed_contact_status
            ,'exposure_occupation_status',epidem_report.exposure_occupation_status
            ,'exposure_travel_status',epidem_report.exposure_travel_status
            ,'risk_history_type_id',epidem_report.risk_history_type_id
            ,'epidem_address',epidem_report.epidem_address
            ,'epidem_moo',epidem_report.epidem_moo
            ,'epidem_road',epidem_report.epidem_road
            ,'epidem_chw_code',epidem_report.epidem_chw_code
            ,'epidem_amp_code',epidem_report.epidem_amp_code
            ,'epidem_tmb_code',epidem_report.epidem_tmb_code
            ,'location_gis_latitude',epidem_report.location_gis_latitude
            ,'location_gis_longitude',epidem_report.location_gis_longitude
            ,'isolate_chw_code',epidem_report.isolate_chw_code
            ,'isolate_place_id',epidem_report.isolate_place_id
            ,'patient_type',epidem_report.patient_type
            ),
        'lab_report',case when epidem_report.b_item_id IS NULL then '{}' 
                else jsonb_build_object(
            'epidem_lab_confirm_type_id',epidem_report.epidem_lab_confirm_type_id
            ,'lab_report_date',epidem_report.lab_report_date
            ,'lab_report_result',epidem_report.lab_report_result
            ,'specimen_date',epidem_report.specimen_date
            ,'specimen_place_id',epidem_report.specimen_place_id
            ,'tests_reason_type_id',epidem_report.tests_reason_type_id
            ,'lab_his_ref_code',epidem_report.lab_his_ref_code
            ,'lab_his_ref_name',epidem_report.lab_his_ref_name
            ,'tmlt_code',epidem_report.tmlt_code
            ) end) as epidem_report
from (select b_site.b_visit_office_id as hospital_code
        ,b_site.site_full_name as hospital_name
        ,'HospitalOS ' || (select version_application_number from s_version order by s_version.version_update_time desc limit 1) as his_identifier
        ,t_patient.patient_pid as cid
        ,t_health_family.passport_no as passport_no
        ,case when f_patient_prefix.patient_prefix_description is null then ''
              else f_patient_prefix.patient_prefix_description end as prefix
        ,t_patient.patient_firstname as first_name
        ,t_patient.patient_lastname as last_name
        ,f_patient_nation.r_rp1853_nation_id as nationality
        ,case when t_patient.f_sex_id in ('1','2') then t_patient.f_sex_id
              else '0' end::int as gender
        ,to_char(text_to_timestamp(t_patient.patient_birthday),'YYYY-MM-DD') as birth_date
        ,case when EXTRACT(YEAR FROM age(text_to_timestamp(t_patient.patient_birthday)))::int IS NOT NULL and EXTRACT(YEAR FROM age(text_to_timestamp(t_patient.patient_birthday)))::int > 0
              then EXTRACT(YEAR FROM age(text_to_timestamp(t_patient.patient_birthday)))::int else 0 end as age_y
        ,case when EXTRACT(MONTH FROM age(text_to_timestamp(t_patient.patient_birthday)))::int IS NOT NULL and EXTRACT(MONTH FROM age(text_to_timestamp(t_patient.patient_birthday)))::int > 0
              then EXTRACT(MONTH FROM age(text_to_timestamp(t_patient.patient_birthday)))::int else 0 end as age_m
        ,case when EXTRACT(DAY FROM age(text_to_timestamp(t_patient.patient_birthday)))::int IS NOT NULL and EXTRACT(DAY FROM age(text_to_timestamp(t_patient.patient_birthday)))::int > 0
              then EXTRACT(DAY FROM age(text_to_timestamp(t_patient.patient_birthday)))::int else 0 end as age_d
        ,case when t_patient.f_patient_marriage_status_id in ('1','2') then t_patient.f_patient_marriage_status_id::int
              when t_patient.f_patient_marriage_status_id in ('3','4') then '3'::int
              when t_patient.f_patient_marriage_status_id = '5' then '4'::int
              else null end as marital_status_id
        ,case when t_patient.patient_house <> '' then t_patient.patient_house
              else '' end as address
        ,case when t_patient.patient_moo <> '' then t_patient.patient_moo
              else '' end as moo
        ,case when t_patient.patient_road <> '' then t_patient.patient_road
              else '' end as road
        ,case when t_patient.patient_tambon <> '' then substr(t_patient.patient_tambon,1,2)
              else '' end as chw_code
        ,case when t_patient.patient_tambon <> '' then substr(t_patient.patient_tambon,3,2)
              else '' end as amp_code
        ,case when t_patient.patient_tambon <> '' then substr(t_patient.patient_tambon,5,2)
              else '' end as tmb_code
        ,case when t_patient.patient_patient_mobile_phone <> '' then t_patient.patient_patient_mobile_phone
              else '' end as mobile_phone
        ,case when r_rp1855_occupation.code is not null then r_rp1855_occupation.code
              else '' end as occupation
        ,case when t_health_epidem_report.epidem_report_guid is not null then '{' || upper(t_health_epidem_report.epidem_report_guid::text) || '}'
              else '' end epidem_report_guid
        ,92 as epidem_report_group_id
        ,t_health_epidem_report.treated_hospital_code
        ,to_char(t_health_epidem_report.report_datetime,'YYYY-MM-DDThh24:mi:ss.ms') as report_datetime
        ,to_char(t_health_epidem_report.onset_date,'YYYY-MM-DD') as onset_date
        ,to_char(t_health_epidem_report.treated_date,'YYYY-MM-DD') as treated_date
        ,case when icd10_primary.diagnosis_date is not null 
              then to_char(icd10_primary.diagnosis_date,'YYYY-MM-DD')
              else '' end as diagnosis_date
        ,case when employee_prefix.f_patient_prefix_id is not null and employee_prefix.f_patient_prefix_id <> '000'
              then employee_prefix.patient_prefix_description else '' end
         || employee_person.person_firstname || '  ' || employee_person.person_lastname as informer_name
        ,case when icd10_primary.icd10_code is not null then icd10_primary.icd10_code
              else '' end as principal_diagnosis_icd10
        ,case when icd10_list.icd10_code is not null then icd10_list.icd10_code
              else '' end as diagnosis_icd10_list
        ,case when t_health_epidem_report.f_epidem_person_status_id = 1 then 3
			  when t_health_epidem_report.f_epidem_person_status_id = 2 then 1
			  when t_health_epidem_report.f_epidem_person_status_id = 3 then 2
			  else t_health_epidem_report.f_epidem_person_status_id end as epidem_person_status_id
        ,t_health_epidem_report.f_epidem_covid_symptom_type_id as epidem_symptom_type_id
        ,case when t_health_epidem_report.pregnant_status is true then 'Y'
              else 'N' end as pregnant_status
        ,case when t_health_epidem_report.respirator_status is true then 'Y'
              else 'N' end as respirator_status
        ,case when epidem_address.f_address_housetype_id in ('1','2') then epidem_address.f_address_housetype_id::int
              when epidem_address.f_address_housetype_id in ('3','4') then 3
              when epidem_address.f_address_housetype_id = '5' then 4
              when epidem_address.f_address_housetype_id = '8' then 5
              else null end as epidem_accommodation_type_id
        ,case when t_health_epidem_report.vaccinated_status is true then 'Y'
              else 'N' end as vaccinated_status
        ,case when t_health_epidem_report.exposure_epidemic_area_status is true then 'Y'
              else 'N' end as exposure_epidemic_area_status
        ,case when t_health_epidem_report.exposure_healthcare_worker_status is true then 'Y'
              else 'N' end as exposure_healthcare_worker_status
        ,case when t_health_epidem_report.exposure_closed_contact_status is true then 'Y'
              else 'N' end as exposure_closed_contact_status
        ,case when t_health_epidem_report.exposure_occupation_status is true then 'Y'
              else 'N' end as exposure_occupation_status
        ,case when t_health_epidem_report.exposure_travel_status is true then 'Y'
              else 'N' end as exposure_travel_status
        ,t_health_epidem_report.f_epidem_risk_history_type_id as risk_history_type_id
        ,case when epidem_address.house_no is not null then epidem_address.house_no
              else '' end as epidem_address
        ,case when epidem_address.village_no is not null then epidem_address.village_no
              else '' end as epidem_moo
        ,case when epidem_address.road is not null then epidem_address.road
              else '' end as epidem_road
        ,case when epidem_address.sub_district_id is not null then substr(epidem_address.sub_district_id,1,2)
              else '' end as epidem_chw_code
        ,case when epidem_address.sub_district_id is not null then substr(epidem_address.sub_district_id,3,2)
              else '' end as epidem_amp_code
        ,case when epidem_address.sub_district_id is not null then substr(epidem_address.sub_district_id,5,2)
              else '' end as epidem_tmb_code
        ,case when epidem_address.latitude is not null then epidem_address.latitude::float
              else 0 end as location_gis_latitude
        ,case when epidem_address.longitude is not null then epidem_address.longitude::float
              else 0 end as location_gis_longitude
        ,case when t_health_epidem_report.isolate_province_id is not null then substr(t_health_epidem_report.isolate_province_id,1,2)
              else '' end as isolate_chw_code
        ,t_health_epidem_report.f_epidem_covid_isolate_place_id as isolate_place_id
        ,case when t_visit.f_visit_type_id = '1' then 'IPD' else 'OPD' end as patient_type
        ,case when t_health_epidem_report.b_item_id is not null
              then t_health_epidem_report.f_epidem_covid_lab_confirm_type_id
              else null end as epidem_lab_confirm_type_id
        ,case when t_health_epidem_report.b_item_id is not null and t_health_epidem_report.lab_report_date is not null
              then to_char(t_health_epidem_report.lab_report_date,'YYYY-MM-DD')
              else null end as lab_report_date
        ,case when t_health_epidem_report.b_item_id is not null and t_health_epidem_report.lab_value is not null
              then t_health_epidem_report.lab_value
              else null end as lab_report_result
        ,case when t_health_epidem_report.b_item_id is not null and t_health_epidem_report.specimen_date is not null
              then to_char(t_health_epidem_report.specimen_date,'YYYY-MM-DD')
              else null end as specimen_date
        ,case when t_health_epidem_report.b_item_id is not null
              then t_health_epidem_report.f_epidem_covid_spcm_place_id
              else null end as specimen_place_id
        ,case when t_health_epidem_report.b_item_id is not null
              then t_health_epidem_report.f_epidem_covid_reason_type_id
              else null end as tests_reason_type_id
        ,case when t_health_epidem_report.b_item_id is not null and b_item.item_number is not null
              then b_item.item_number
              else '' end as lab_his_ref_code
        ,case when t_health_epidem_report.b_item_id is not null and t_health_epidem_report.lab_name is not null
              then t_health_epidem_report.lab_name
              else '' end as lab_his_ref_name
        ,case when t_health_epidem_report.b_item_id is not null and b_map_lab_tmlt.b_lab_tmlt_tmltcode is not null
              then b_map_lab_tmlt.b_lab_tmlt_tmltcode
              else '' end as tmlt_code
        ,b_item.b_item_id
    from t_health_epidem_report
        inner join t_visit on t_health_epidem_report.t_visit_id = t_visit.t_visit_id
        inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
        inner join t_health_family on t_patient.t_health_family_id = t_health_family.t_health_family_id
        left join f_patient_prefix on t_patient.f_patient_prefix_id = f_patient_prefix.f_patient_prefix_id
        left join f_patient_nation on t_patient.f_patient_nation_id = f_patient_nation.f_patient_nation_id
        left join f_patient_occupation on t_patient.f_patient_occupation_id = f_patient_occupation.f_patient_occupation_id
        left join b_map_rp1855_occupation on f_patient_occupation.f_patient_occupation_id = b_map_rp1855_occupation.f_patient_occupation_id
        left join r_rp1855_occupation on b_map_rp1855_occupation.r_rp1855_occupation_id = r_rp1855_occupation.id
        left join t_address as epidem_address on t_health_epidem_report.t_address_id = epidem_address.t_address_id
        left join (select t_diag_icd10.diag_icd10_vn as t_visit_id
                        ,text_to_timestamp(t_diag_icd10.diag_icd10_diagnosis_date) as diagnosis_date
                        ,replace(t_diag_icd10.diag_icd10_number,'.','') as icd10_code
                    from t_diag_icd10
                        inner join t_visit on t_diag_icd10.diag_icd10_vn = t_visit.t_visit_id
                        inner join t_health_epidem_report on t_health_epidem_report.t_visit_id = t_visit.t_visit_id
                    where t_diag_icd10.diag_icd10_active = '1'
                        and t_diag_icd10.f_diag_icd10_type_id = '1'
                        and t_health_epidem_report.t_health_epidem_report_id = $1
                )as icd10_primary on icd10_primary.t_visit_id = t_visit.t_visit_id
        left join (select t_diag_icd10.diag_icd10_vn as t_visit_id
                        ,array_to_string(array_agg(replace(t_diag_icd10.diag_icd10_number,'.','')),',') as icd10_code
                    from t_diag_icd10
                        inner join t_visit on t_diag_icd10.diag_icd10_vn = t_visit.t_visit_id
                        inner join t_health_epidem_report on t_health_epidem_report.t_visit_id = t_visit.t_visit_id
                    where t_diag_icd10.diag_icd10_active = '1'
                        and t_diag_icd10.f_diag_icd10_type_id <> '1'
                        and t_health_epidem_report.t_health_epidem_report_id = $1
                    group by t_diag_icd10.diag_icd10_vn
                )as icd10_list on icd10_list.t_visit_id = t_visit.t_visit_id
        left join b_item on t_health_epidem_report.b_item_id = b_item.b_item_id
        left join b_map_lab_tmlt on b_item.b_item_id = b_map_lab_tmlt.b_item_id
        left join b_employee on t_health_epidem_report.user_report_id = b_employee.b_employee_id
        left join t_person as employee_person on b_employee.t_person_id = employee_person.t_person_id
        left join f_patient_prefix as employee_prefix on employee_person.f_prefix_id = employee_prefix.f_patient_prefix_id
        cross join b_site
    where t_health_epidem_report.t_health_epidem_report_id = $1
        and t_health_epidem_report.active = '1'
    ) as epidem_report;
END;
$$
LANGUAGE 'plpgsql';

-- update db version
INSERT INTO s_health_version VALUES ('9710000000011','11','PCU, Community Edition','1.34.0','1.5.0', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

insert into s_script_update_log values ('pcu','update_pcu_ph11.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'update PCU ph10 -> ph11');
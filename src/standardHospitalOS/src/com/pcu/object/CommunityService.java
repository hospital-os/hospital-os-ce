/*
 * To change this template;public String choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class CommunityService extends Persistent {

    public String t_visit_id;
    public String f_comservice_id;
    public String start_date;
    public String record_date_time;
    public String staff_record;
    public String modify_date_time;
    public String staff_modify;
    public String cancel_date_time;
    public String staff_cancel;
    public String active = "1";
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.object;

import com.hospital_os.usecase.connection.Persistent;
import java.util.Date;

/**
 *
 * @author sompr
 */
public class THealthRehabilitation extends Persistent {

    public String t_visit_id;
    public String t_person_id;
    public Date survey_date = new Date();
    public Date start_date;
    public Date finish_date;
    public String f_rehabilitation_code_id;
    public String f_rehabilitation_device_id;
    public int device_qty;
    public String hospital;
    public String active = "1";
    public Date record_date_time;
    public String user_record_id = "";
    public Date update_date_time;
    public String user_update_id = "";
    public Date cancel_date_time;
    public String user_cancel_id = "";
}

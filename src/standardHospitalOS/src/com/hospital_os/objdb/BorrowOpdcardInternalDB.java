/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.BorrowOpdcardInternal;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class BorrowOpdcardInternalDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "847";

    public BorrowOpdcardInternalDB(ConnectionInf c) {
        this.connectionInf = c;
    }

    public int insert(BorrowOpdcardInternal obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO t_borrow_opdcard_internal(\n"
                    + "            t_borrow_opdcard_internal_id, t_patient_id, t_visit_id, status, user_takeout_id)\n"
                    + "    VALUES (?, ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getGenID(tableId));
            preparedStatement.setString(2, obj.t_patient_id);
            preparedStatement.setString(3, obj.t_visit_id);
            preparedStatement.setString(4, obj.status);
            preparedStatement.setString(5, obj.user_takeout_id);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(BorrowOpdcardInternal obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_borrow_opdcard_internal SET \n"
                    + " status = ?, user_return_id = ?, return_datetime = ?\n"
                    + " WHERE t_borrow_opdcard_internal_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.status);
            preparedStatement.setString(2, obj.user_return_id);
            preparedStatement.setTimestamp(3, obj.return_datetime == null ? null : new Timestamp(obj.return_datetime.getTime()));
            preparedStatement.setString(4, obj.getObjectId());
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public BorrowOpdcardInternal getBorrowOpdcardInternalByPatientIdAndVisitId(String patientId, String visitId) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select *\n"
                    + "from t_borrow_opdcard_internal\n"
                    + "where t_patient_id = ? and t_visit_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, patientId);
            preparedStatement.setString(2, visitId);
            List<BorrowOpdcardInternal> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<BorrowOpdcardInternal> list(String hn, Date startDate, Date endDate, String status) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select\n"
                    + "t_borrow_opdcard_internal.*\n"
                    + ", t_patient.patient_hn as hn\n"
                    + ", f_patient_prefix.patient_prefix_description || t_health_family.patient_name || ' ' || t_health_family.patient_last_name as fullname\n"
                    + ", b_service_point.service_point_description as servicepoint_name\n"
                    + "from\n"
                    + "t_borrow_opdcard_internal\n"
                    + "inner join t_patient on t_patient.t_patient_id = t_borrow_opdcard_internal.t_patient_id\n"
                    + "inner join t_health_family on t_health_family.t_health_family_id = t_patient.t_health_family_id\n"
                    + "left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_health_family.f_prefix_id\n"
                    + "left join\n"
                    + "  (select\n"
                    + "  t_borrow_opdcard_internal_tracking.*\n"
                    + "  from t_borrow_opdcard_internal_tracking\n"
                    + "  inner join\n"
                    + "  (select\n"
                    + "		  t_borrow_opdcard_internal_tracking.t_borrow_opdcard_internal_id as t_borrow_opdcard_internal_id\n"
                    + "		  ,max(t_borrow_opdcard_internal_tracking.send_datetime) as max_send_datetime\n"
                    + "  from\n"
                    + "	  t_borrow_opdcard_internal_tracking\n";
            if (hn != null && !hn.isEmpty()) {
                sql += "	  inner join t_borrow_opdcard_internal on t_borrow_opdcard_internal_tracking.t_borrow_opdcard_internal_id = t_borrow_opdcard_internal.t_borrow_opdcard_internal_id\n"
                        + "	  inner join t_patient on t_borrow_opdcard_internal.t_patient_id = t_patient.t_patient_id\n"
                        + "			where  t_patient.patient_hn = ?\n";
            }
            if (startDate != null && endDate != null) {
                sql += "			where  t_borrow_opdcard_internal_tracking.send_datetime::date between ? and ?\n";
            }
            sql += "  group by\n"
                    + "		  t_borrow_opdcard_internal_tracking.t_borrow_opdcard_internal_id) as max_send_opdcard\n"
                    + "  on t_borrow_opdcard_internal_tracking.t_borrow_opdcard_internal_id = max_send_opdcard.t_borrow_opdcard_internal_id\n"
                    + "  and t_borrow_opdcard_internal_tracking.send_datetime = max_send_opdcard.max_send_datetime) as t_borrow_opdcard_internal_tracking\n"
                    + "  on t_borrow_opdcard_internal.t_borrow_opdcard_internal_id = t_borrow_opdcard_internal_tracking.t_borrow_opdcard_internal_id\n"
                    + "left join b_service_point on b_service_point.b_service_point_id = t_borrow_opdcard_internal_tracking.b_service_point_receiver_id\n"
                    + "where\n"
                    + "1=1\n";
            if (hn != null && !hn.isEmpty()) {
                sql += "and t_patient.patient_hn = ?\n";
            }
            if (startDate != null && endDate != null) {
                sql += "and t_borrow_opdcard_internal.takeout_datetime::date between ? and ?\n";
            }
            if (status != null && !status.isEmpty()) {
                sql += "and t_borrow_opdcard_internal.status = ?\n";
            }
            sql += "order by \n"
                    + "t_borrow_opdcard_internal.takeout_datetime\n"
                    + ", b_service_point.service_point_description\n"
                    + ", t_patient.patient_hn";
            preparedStatement = connectionInf.ePQuery(sql);
            int i = 1;
            if (hn != null && !hn.isEmpty()) {
                preparedStatement.setString(i++, hn);
                preparedStatement.setString(i++, hn);
            }
            if (startDate != null && endDate != null) {
                preparedStatement.setDate(i++, new java.sql.Date(startDate.getTime()));
                preparedStatement.setDate(i++, new java.sql.Date(endDate.getTime()));
                preparedStatement.setDate(i++, new java.sql.Date(startDate.getTime()));
                preparedStatement.setDate(i++, new java.sql.Date(endDate.getTime()));
            }
            if (status != null && !status.isEmpty()) {
                preparedStatement.setString(i++, status);
            }
            System.err.println(preparedStatement.toString());
            List<BorrowOpdcardInternal> list = executeQuery(preparedStatement);
            return list;
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<BorrowOpdcardInternal> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List< BorrowOpdcardInternal> list = new ArrayList< BorrowOpdcardInternal>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                BorrowOpdcardInternal obj = new BorrowOpdcardInternal();
                obj.setObjectId(rs.getString("t_borrow_opdcard_internal_id"));
                obj.t_patient_id = rs.getString("t_patient_id");
                obj.t_visit_id = rs.getString("t_visit_id");
                obj.status = rs.getString("status");
                obj.user_takeout_id = rs.getString("user_takeout_id");
                obj.takeout_datetime = rs.getTimestamp("takeout_datetime");
                obj.user_return_id = rs.getString("user_return_id");
                obj.return_datetime = rs.getTimestamp("return_datetime");
                try {
                    obj.hn = rs.getString("hn");
                } catch (Exception ex) {
                }
                try {
                    obj.name = rs.getString("fullname");
                } catch (Exception ex) {
                }
                try {
                    obj.lastestServicePoint = rs.getString("servicepoint_name");
                } catch (Exception ex) {
                }
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }
}

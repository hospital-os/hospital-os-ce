CREATE TABLE b_item_lab_ncd_std
(
  b_item_lab_ncd_std_id character varying(255) NOT NULL,
  lab_number character varying(255),
  item_lab_ncd_std_description character varying(255),  
  lab_ncd_note character varying(255),
  active character varying(1) NOT NULL,
  CONSTRAINT b_item_lab_ncd_std_pkey PRIMARY KEY (b_item_lab_ncd_std_id)
);

CREATE TABLE b_item_map_lab_ncd
(
  b_item_map_lab_ncd_id character varying(255) NOT NULL,
  b_item_lab_ncd_std_id character varying(255) NOT NULL,
  b_item_id character varying(255) NOT NULL,
  active character varying(1) NOT NULL,
  CONSTRAINT b_item_map_lab_ncd_pkey PRIMARY KEY (b_item_map_lab_ncd_id)
);

CREATE TABLE f_ncd_alcohol_feq
(
  f_ncd_alcohol_feq_id character varying(1) NOT NULL,
  alcohol_feq_description character varying(255),
  CONSTRAINT f_ncd_alcohol_feq_pkey PRIMARY KEY (f_ncd_alcohol_feq_id)
);

CREATE TABLE f_ncd_ans_result
(
  f_ncd_ans_result_id character varying(1) NOT NULL,
  ncd_ans_result_description character varying(255),
  CONSTRAINT f_ncd_ans_result_pkey PRIMARY KEY (f_ncd_ans_result_id)
);

CREATE TABLE f_ncd_cigarette_feq
(
  f_ncd_cigarette_feq_id character varying(1) NOT NULL,
  cigarette_feq_description character varying(255),
  CONSTRAINT f_ncd_cigarette_feq_pkey PRIMARY KEY (f_ncd_cigarette_feq_id)
);

CREATE TABLE f_ncd_cigarette_type
(
  f_ncd_cigarette_type_id character varying(1) NOT NULL,
  cigarette_type_description character varying(255),
  CONSTRAINT f_ncd_cigarette_type_pkey PRIMARY KEY (f_ncd_cigarette_type_id)
);

CREATE TABLE f_ncd_complications
(
  f_ncd_complications_id character varying(1) NOT NULL,
  ncd_complications_description character varying(255),
  CONSTRAINT f_ncd_complications_pkey PRIMARY KEY (f_ncd_complications_id)
);

CREATE TABLE f_ncd_drive_protect
(
  f_ncd_drive_protect_id character varying(1) NOT NULL,
  drive_protect_description character varying(255),
  CONSTRAINT f_ncd_drive_protect_pkey PRIMARY KEY (f_ncd_drive_protect_id)
);


CREATE TABLE f_ncd_exercise_feq
(
  f_ncd_exercise_feq_id character varying(1) NOT NULL,
  exercise_feq_description character varying(255),
  CONSTRAINT f_ncd_exercise_feq_pkey PRIMARY KEY (f_ncd_exercise_feq_id)
);

CREATE TABLE f_ncd_family_relation
(
  f_ncd_family_relation_id character varying(1) NOT NULL,
  ncd_family_relation_description character varying(255),
  CONSTRAINT f_ncd_family_relation_pkey PRIMARY KEY (f_ncd_family_relation_id)
);

CREATE TABLE f_ncd_food_taste
(
  f_ncd_food_taste_id character varying(1) NOT NULL,
  food_taste_description character varying(255),
  CONSTRAINT f_ncd_food_taste_pkey PRIMARY KEY (f_ncd_food_taste_id)
);

CREATE TABLE f_ncd_fruits_vegetable
(
  f_ncd_fruits_vegetable_id character varying(1) NOT NULL,
  fruits_vegetable_description character varying(255),
  CONSTRAINT f_ncd_fruits_vegetable_pkey PRIMARY KEY (f_ncd_fruits_vegetable_id)
);

CREATE TABLE f_ncd_history_conduct
(
  f_ncd_history_conduct_id character varying(1) NOT NULL,
  ncd_history_conduct_description character varying(255),
  CONSTRAINT f_ncd_history_conduct_pkey PRIMARY KEY (f_ncd_history_conduct_id)
);

CREATE TABLE f_ncd_operations_type
(
  f_ncd_operations_type_id character varying(1) NOT NULL,
  ncd_operations_type_description character varying(255),
  CONSTRAINT f_ncd_operations_type_pkey PRIMARY KEY (f_ncd_operations_type_id)
);

CREATE TABLE f_ncd_risk_group
(
  f_ncd_risk_group_id character varying(1) NOT NULL,
  ncd_risk_group_description character varying(255),
  CONSTRAINT f_ncd_risk_group_pkey PRIMARY KEY (f_ncd_risk_group_id)
);

CREATE TABLE f_ncd_risk_type
(
  f_ncd_risk_type_id character varying(1) NOT NULL,
  ncd_risk_type_description character varying(255),
  CONSTRAINT f_ncd_risk_type_pkey PRIMARY KEY (f_ncd_risk_type_id)
);

CREATE TABLE f_ncd_sex_protect
(
  f_ncd_sex_protect_id character varying(1) NOT NULL,
  sex_protect_description character varying(255),
  CONSTRAINT f_ncd_sex_protect_pkey PRIMARY KEY (f_ncd_sex_protect_id)
);

CREATE TABLE f_ncd_verbal_screening
(
  f_ncd_verbal_screening_id character varying(1) NOT NULL,
  ncd_verbal_screening_description character varying(255),
  CONSTRAINT f_ncd_verbal_screening_pkey PRIMARY KEY (f_ncd_verbal_screening_id)
);

CREATE TABLE f_person_ncd_group
(
  f_person_ncd_group_id character varying(1) NOT NULL,
  person_ncd_group_description character varying(255),
  CONSTRAINT f_person_ncd_group_pkey PRIMARY KEY (f_person_ncd_group_id)
);

CREATE TABLE f_person_ncd_screen_type
(
  f_person_ncd_screen_type_id character varying(1) NOT NULL,
  person_ncd_screen_type_description character varying(255),
  CONSTRAINT f_person_ncd_screen_type_pkey PRIMARY KEY (f_person_ncd_screen_type_id)
);

CREATE TABLE t_ncd_screen
(
  t_ncd_screen_id character varying(255) NOT NULL,
  t_health_family_id character varying(255) NOT NULL,
  t_visit_id character varying(255),
  height character varying(255),
  weight character varying(255),
  waist character varying(255),
  hips character varying(255),
  hbp_s1 character varying(255),
  hbp_d1 character varying(255),
  hbp_s2 character varying(255),
  hbp_d2 character varying(255),
  hbp_s_avg character varying(255),
  hbp_d_avg character varying(255),
  bmi character varying(255),
  f_person_ncd_screen_type_id character varying(1),
  f_person_ncd_group_id character varying(1),
  active character varying(1) NOT NULL,
  screen_date character varying(19) NOT NULL,
  record_date character varying(19) NOT NULL,
  d_update character varying(19) NOT NULL,
  cancel_date_time character varying(19),
  user_record_id character varying(255) NOT NULL,
  user_update_id character varying(255) NOT NULL,
  user_cancel_id character varying(255),
  CONSTRAINT ncd_screen_pkey PRIMARY KEY (t_ncd_screen_id)
);

CREATE TABLE t_ncd_complications
(
  t_ncd_complications_id character varying(255) NOT NULL,
  t_ncd_screen_id character varying(255) NOT NULL,
  f_ncd_complications_id character varying(1) NOT NULL,
  ncd_complications_other_detail character varying(255),
  record_date_time character varying(19) NOT NULL,
  update_date_time character varying(19) NOT NULL,
  user_record_id character varying(255) NOT NULL,
  user_update_id character varying(255) NOT NULL,
  CONSTRAINT t_ncd_complications_pkey PRIMARY KEY (t_ncd_complications_id)
);

CREATE TABLE t_ncd_screen_advice
(
  t_ncd_screen_advice_id character varying(255) NOT NULL,
  t_ncd_screen_id character varying(255) NOT NULL,
  discarding character varying(255),
  discarding_detail character varying(255),
  quit_drinking character varying(255),
  quit_drinking_detail character varying(255),
  eating character varying(255),
  eating_detail character varying(255),
  temperance character varying(255),
  temperance_detail character varying(255),
  exercise character varying(255),
  exercise_detail character varying(255),
  record_date_time character varying(19) NOT NULL,
  update_date_time character varying(19) NOT NULL,
  user_record_id character varying(255) NOT NULL,
  user_update_id character varying(255) NOT NULL,
  CONSTRAINT t_ncd_screen_advice_pkey PRIMARY KEY (t_ncd_screen_advice_id)
);

CREATE TABLE t_ncd_screen_behavior
(
  t_ncd_screen_behavior_id character varying(255) NOT NULL,
  t_ncd_screen_id character varying(255) NOT NULL,
  f_exercise_feq_id character varying(1),
  f_fruits_vegetable_id character varying(1),
  f_drive_protect_id character varying(1),
  f_sex_protect_id character varying(1),
  record_date_time character varying(19) NOT NULL,
  update_date_time character varying(19),
  user_record_id character varying(255) NOT NULL,
  user_update_id character varying(255),
  CONSTRAINT t_ncd_screen_behavior_pkey PRIMARY KEY (t_ncd_screen_behavior_id)
);

CREATE TABLE t_ncd_screen_family_history
(
  t_ncd_screen_family_history_id character varying(255) NOT NULL,
  t_ncd_screen_id character varying(255) NOT NULL,
  f_ncd_family_relation_id character varying(1) NOT NULL,
  dm_result character varying(255),
  ht_result character varying(255),
  gout_result character varying(255),
  crf_result character varying(255),
  mi_result character varying(255),
  stroke_result character varying(255),
  copd_result character varying(255),
  unknown_result character varying(255),
  other_detail character varying(255),
  record_date_time character varying(19) NOT NULL,
  update_date_time character varying(19),
  user_record_id character varying(255) NOT NULL,
  user_update_id character varying(255),
  CONSTRAINT t_ncd_screen_family_history_pkey PRIMARY KEY (t_ncd_screen_family_history_id)
);

CREATE TABLE t_ncd_screen_history
(
  t_ncd_screen_history_id character varying(25) NOT NULL,
  t_ncd_screen_id character varying(25) NOT NULL,
  high_blood_sugar character varying(25),
  dm character varying(25),
  ht character varying(25),
  hepatitis character varying(25),
  palsy character varying(25),
  heart character varying(25),
  lipid character varying(25),
  lesion_dm character varying(25),
  born_over_weigth character varying(25),
  plenty_of_water character varying(25),
  frequent_urination character varying(25),
  thinner character varying(25),
  loss_weight character varying(25),
  labia_slit character varying(25),
  scratch character varying(25),
  bleary character varying(25),
  feel_senseless character varying(25),
  snoring character varying(25),
  strain character varying(25),
  stroke character varying(25),
  f_ncd_history_conduct_id character varying(1),
  record_date_time character varying(19) NOT NULL,
  update_date_time character varying(19),
  user_record_id character varying(255) NOT NULL,
  user_update_id character varying(255),
  CONSTRAINT t_ncd_screen_history_pkey PRIMARY KEY (t_ncd_screen_history_id)
);

CREATE TABLE t_ncd_screen_result
(
  t_ncd_screen_result_id character varying(255) NOT NULL,
  t_ncd_screen_id character varying(255) NOT NULL,
  dm_risk character varying(1),
  ht_risk character varying(1),
  dm_ht_risk character varying(1),
  stroke_risk character varying(1),
  obesity_risk character varying(1),
  operations character varying(1),
  record_date_time character varying(19) NOT NULL,
  update_date_time character varying(19),
  user_record_id character varying(255) NOT NULL,
  user_update_id character varying(255),
  CONSTRAINT t_ncd_screen_result_pkey PRIMARY KEY (t_ncd_screen_result_id)
);

CREATE TABLE t_ncd_verbal_screening
(
  t_ncd_verbal_screening_id character varying(255) NOT NULL,
  t_ncd_screen_id character varying(255) NOT NULL,
  f_ncd_verbal_screening_id character varying(1) NOT NULL,
  record_date_time character varying(19) NOT NULL,
  update_date_time character varying(19),
  user_record_id character varying(255) NOT NULL,
  user_update_id character varying(255),
  CONSTRAINT t_ncd_verbal_screening_pkey PRIMARY KEY (t_ncd_verbal_screening_id)
);

CREATE TABLE t_ncd_risk_osteoporosis
(
  t_ncd_risk_osteoporosis_id character varying(255) NOT NULL,
  t_ncd_screen_id character varying(255) NOT NULL,
  osteoporosis_known character varying(1) NOT NULL,
  drug_inherent character varying(1) NOT NULL,
  uterus_ovary character varying(1) NOT NULL,
  menopause character varying(1) NOT NULL,
  record_date_time character varying(19) NOT NULL,
  update_date_time character varying(19),
  user_record_id character varying(255) NOT NULL,
  user_update_id character varying(255),
  CONSTRAINT t_risk_osteoporosis_pkey PRIMARY KEY (t_ncd_risk_osteoporosis_id)
);

CREATE TABLE t_ncd_risk_screen_cancer
(
  t_ncd_risk_screen_cancer_id character varying(255) NOT NULL,
  t_ncd_screen_id character varying(255) NOT NULL,
  cervical_cancer_year character varying(1) NOT NULL,
  breast_cancer_feq character varying(1) NOT NULL,
  breast_cancer_month character varying(1) NOT NULL,
  record_date_time character varying(19) NOT NULL,
  update_date_time character varying(19),
  user_record_id character varying(255) NOT NULL,
  user_update_id character varying(255),
  CONSTRAINT t_risk_screen_cancer_pkey PRIMARY KEY (t_ncd_risk_screen_cancer_id)
);

CREATE TABLE t_ncd_risk_screen_psychiatry
(
  t_ncd_risk_screen_psychiatry_id character varying(255) NOT NULL,
  t_ncd_screen_id character varying(255),
  q1 character varying(1) NOT NULL,
  q2 character varying(1) NOT NULL,
  record_date_time character varying(19) NOT NULL,
  update_date_time character varying(19),
  user_record_id character varying(255) NOT NULL,
  user_update_id character varying(255),
  CONSTRAINT t_risk_screen_psychiatry_pkey PRIMARY KEY (t_ncd_risk_screen_psychiatry_id)
);

CREATE TABLE t_ncd_risk_screen_tuberculosis
(
  t_ncd_risk_screen_tuberculosis_id character varying(255) NOT NULL,
  t_ncd_screen_id character varying(255) NOT NULL,
  cough character varying(1) NOT NULL,
  cough_blood character varying(1) NOT NULL,
  cough_phlegm character varying(1) NOT NULL,
  fever character varying(1) NOT NULL,
  loss_weight character varying(1) NOT NULL,
  others character varying(1) NOT NULL,
  record_date_time character varying(19) NOT NULL,
  update_date_time character varying(19),
  user_record_id character varying(255) NOT NULL,
  user_update_id character varying(255),
  CONSTRAINT t_risk_screen_tuberculosis_pkey PRIMARY KEY (t_ncd_risk_screen_tuberculosis_id)
);

CREATE TABLE t_ncd_screen_result_blood
(
  t_ncd_screen_result_blood_id character varying(255) NOT NULL,
  t_ncd_screen_id character varying(255) NOT NULL,
  "8hr_eat" character varying(1) NOT NULL,
  time_after character varying(255),
  fcg_result character varying(255),
  fpg_result character varying(255),
  ppg_result character varying(255),
  dtx_result character varying(255),
  fbs_result character varying(255),
  physical_examination character varying(1) NOT NULL,
  physical_examination_others character varying(255),
  b_employee_id character varying(255),
  record_date_time character varying(19) NOT NULL,
  update_date_time character varying(19),
  user_record_id character varying(255) NOT NULL,
  user_update_id character varying(255),
  CONSTRAINT t_screen_result_blood_pkey PRIMARY KEY (t_ncd_screen_result_blood_id)
);

CREATE TABLE t_ncd_screen_taste
(
  t_ncd_screen_taste_id character varying(255) NOT NULL,
  t_ncd_screen_id character varying(255) NOT NULL,
  f_food_taste_id character varying(1),
  record_date_time character varying(19) NOT NULL,
  update_date_time character varying(19),
  user_record_id character varying(255) NOT NULL,
  user_update_id character varying(255),
  CONSTRAINT t_screen_taste_pkey PRIMARY KEY (t_ncd_screen_taste_id)
);

CREATE TABLE t_ncd_screen_risk
(
  t_ncd_screen_risk_id character varying(255) NOT NULL,
  t_ncd_screen_id character varying(255),
  t_ncd_screen_disease_complications_id character varying(255),
  f_cigarette_feq_id character varying(1) NOT NULL,
  cigarette_amount_day character varying(255),
  cigarette_pack_year character varying(255),
  cigarette_count_year character varying(255),
  f_cigarette_type_id character varying(1),
  cigarette_longterm character varying(255),
  risk_cigarette_advice character varying(1) NOT NULL,
  f_alcohol_feq_id character varying(1) NOT NULL,
  alcohol_week character varying(255),
  alcohol_year character varying(255),
  risk_alcohol_advice character varying(1) NOT NULL,
  record_date_time character varying(19) NOT NULL,
  update_date_time character varying(19),
  user_record_id character varying(255) NOT NULL,
  user_update_id character varying(255),
  CONSTRAINT t_ncd_screen_risk_pkey PRIMARY KEY (t_ncd_screen_risk_id)
);

CREATE TABLE f_ncd_group_code
(
  f_ncd_group_code_id character varying(255) NOT NULL,
  code character varying(255) NOT NULL,
  ncd_group_code_description character varying(255),
  ncd_code_begin character varying(255),
  ncd_code_end character varying(255),
  sort character varying(255),
  b_ncd_group_id character varying(255),
  CONSTRAINT f_ncd_group_code_pkey PRIMARY KEY (f_ncd_group_code_id)
);

CREATE TABLE f_ncd_ans_normal_result
(
  f_ncd_ans_normal_result_id character varying(1) NOT NULL,
  ncd_ans_normal_result_description character varying(255),
  CONSTRAINT f_ncd_ans_normal_result_pkey PRIMARY KEY (f_ncd_ans_normal_result_id)
);

CREATE TABLE f_ncd_disease_complications
(
  f_ncd_disease_complications character varying(1) NOT NULL,
  ncd_disease_complications_description character varying(25),
  CONSTRAINT f_ncd_disease_complications_pkey PRIMARY KEY (f_ncd_disease_complications)
);

CREATE TABLE f_ncd_dm_treat_type
(
  f_ncd_dm_treat_type_id character varying(1) NOT NULL,
  ncd_dm_treat_type_description character varying(255),
  CONSTRAINT f_ncd_dm_treat_type_pkey PRIMARY KEY (f_ncd_dm_treat_type_id)
);

CREATE TABLE f_ncd_eyes_diagnosis_type
(
  f_ncd_eyes_diagnosis_type_id character varying(1) NOT NULL,
  ncd_eyes_diagnosis_type_description character varying(255),
  CONSTRAINT f_ncd_eyes_diagnosis_type_pkey PRIMARY KEY (f_ncd_eyes_diagnosis_type_id)
);

CREATE TABLE f_ncd_eyes_ma_type
(
  f_ncd_eyes_ma_type_id character varying(1) NOT NULL,
  ncd_eyes_ma_type_description character varying(255),
  CONSTRAINT f_ncd_eyes_ma_type_pkey PRIMARY KEY (f_ncd_eyes_ma_type_id)
);

CREATE TABLE f_ncd_eyes_result_type
(
  f_ncd_eyes_result_type_id character varying(2) NOT NULL,
  ncd_eyes_result_type_description character varying(255),
  CONSTRAINT f_ncd_eyes_result_type_pkey PRIMARY KEY (f_ncd_eyes_result_type_id)
);

CREATE TABLE f_ncd_foot_assess_type
(
  f_ncd_foot_assess_type_id character varying(1) NOT NULL,
  ncd_foot_assess_type_description character varying(255),
  CONSTRAINT f_ncd_foot_assess_type_pkey PRIMARY KEY (f_ncd_foot_assess_type_id)
);

CREATE TABLE f_ncd_foot_protect_type
(
  f_ncd_foot_protect_type character varying(255) NOT NULL,
  ncd_foot_protect_type_description character varying(255),
  CONSTRAINT f_ncd_foot_protect_type_pkey PRIMARY KEY (f_ncd_foot_protect_type)
);

CREATE TABLE f_ncd_foot_screen_result
(
  f_ncd_foot_screen_result_id character varying(1) NOT NULL,
  ncd_foot_screen_result_description character varying(255),
  CONSTRAINT f_ncd_foot_screen_result_pkey PRIMARY KEY (f_ncd_foot_screen_result_id)
);

CREATE TABLE f_ncd_foot_screen_skin_color
(
  f_ncd_foot_screen_skin_color_id character varying(1) NOT NULL,
  ncd_foot_screen_skin_color_description character varying(255) NOT NULL,
  CONSTRAINT f_ncd_foot_screen_skin_color_pkey PRIMARY KEY (f_ncd_foot_screen_skin_color_id)
);

CREATE TABLE f_ncd_foot_skin_temperature
(
  f_ncd_foot_skin_temperature_id character varying(1) NOT NULL,
  ncd_foot_skin_temperature_description character varying(255),
  CONSTRAINT f_ncd_foot_skin_temperature_pkey PRIMARY KEY (f_ncd_foot_skin_temperature_id)
);

CREATE TABLE f_ncd_lose_sensation_type
(
  f_ncd_lose_sensation_type_id character varying(1) NOT NULL,
  ncd_lose_sensation_type_description character varying(255),
  CONSTRAINT f_ncd_lose_sensation_type_pkey PRIMARY KEY (f_ncd_lose_sensation_type_id)
);

CREATE TABLE f_ncd_prodoscope_type
(
  f_ncd_prodoscope_type_id character varying(1) NOT NULL,
  ncd_prodoscope_type_description character varying(255),
  CONSTRAINT f_ncd_prodoscope_type_pkey PRIMARY KEY (f_ncd_prodoscope_type_id)
);

CREATE TABLE f_ncd_shoes_type
(
  f_ncd_shoes_type_id character varying(1) NOT NULL,
  ncd_shoes_type_description character varying(255),
  CONSTRAINT f_ncd_shoes_type_pkey PRIMARY KEY (f_ncd_shoes_type_id)
);

CREATE TABLE f_ncd_wound_position
(
  f_ncd_wound_position_id character varying(1) NOT NULL,
  ncd_wound_position_description character varying(255),
  CONSTRAINT f_ncd_wound_position_pkey PRIMARY KEY (f_ncd_wound_position_id)
);

CREATE TABLE f_ncd_dm_type
(
  f_ncd_dm_type_id character varying(1) NOT NULL,
  ncd_dm_type_description character varying(255),
  CONSTRAINT f_ncd_dm_type_pkey PRIMARY KEY (f_ncd_dm_type_id)
);

CREATE TABLE t_ncd_screen_disease_complications
(
  t_ncd_screen_disease_complications_id character varying(255) NOT NULL,
  f_ncd_group_code_id character varying(255) NOT NULL,
  diag_code character varying(255) NOT NULL,
  screen_date_time character varying(255) NOT NULL,
  t_health_family_id character varying(255) NOT NULL,
  t_visit_id character varying(255) NOT NULL,
  t_patient_id character varying(255) NOT NULL,
  active character varying(1) NOT NULL,
  record_date_time character varying(19) NOT NULL,
  update_date_time character varying(19) NOT NULL,
  cancel_date_time character varying(19),
  user_record_id character varying(255) NOT NULL,
  user_update_id character varying(255) NOT NULL,
  user_cancel_id character varying(255),
  CONSTRAINT t_ncd_screen_disease_complications_pkey PRIMARY KEY (t_ncd_screen_disease_complications_id)
);

CREATE TABLE t_ncd_history
(
  t_ncd_history_id character varying(255) NOT NULL,
  t_ncd_screen_disease_complications_id character varying(255) NOT NULL,
  f_dm_type_id character varying(1) NOT NULL,
  dm_long_term character varying(255),
  hbp_s character varying(255),
  hbp_d character varying(255),
  last_ha1c_result character varying(255),
  last_ha1c_date character varying(10),
  last_fbs_result character varying(255),
  last_fbs_date character varying(10),
  last_dtx_result character varying(255),
  last_dtx_date character varying(10),
  dm_treat character varying(255),
  dm_treat_other_detail character varying(255),
  record_date_time character varying(19) NOT NULL,
  update_date_time character varying(19) NOT NULL,
  user_record_id character varying(255) NOT NULL,
  user_update_id character varying(255) NOT NULL,
  CONSTRAINT t_ncd_history_pkey PRIMARY KEY (t_ncd_history_id)
);

CREATE TABLE t_ncd_disease_complications
(
  t_ncd_disease_complications_id character varying(255) NOT NULL,
  t_ncd_screen_disease_complications_id character varying(255) NOT NULL,
  f_ncd_disease_complications_id character varying(1) NOT NULL,
  ncd_disease_complications_detail character varying(255),
  screen_disease_complications character varying(255),
  found_disease_complications character varying(255),
  record_date_time character varying(19) NOT NULL,
  update_date_time character varying(19) NOT NULL,
  user_record_id character varying(255) NOT NULL,
  user_update_id character varying(255) NOT NULL,
  CONSTRAINT t_ncd_disease_complications_pkey PRIMARY KEY (t_ncd_disease_complications_id)
);

CREATE TABLE t_ncd_eyes_complications
(
  t_ncd_eyes_complications_id character varying(255) NOT NULL,
  t_ncd_disease_complications_id character varying(255) NOT NULL,
  left_eyes_result character varying(255),
  right_eyes_result character varying(255),
  left_eyes_diagnosis character varying(255),
  right_eyes_diagnosis character varying(255),
  left_eyes_csme character varying(255),
  right_eyes_csme character varying(255),
  left_eyes_opticnerve character varying(255),
  right_eyes_opticnerve character varying(255),
  left_eyes_opticnerve_detail character varying(255),
  right_eyes_opticnerve_detail character varying(255),
  left_eyes_haemorrhage character varying(255),
  right_eyes_haemorrhage character varying(255),
  left_eyes_exudates character varying(255),
  right_eyes_exudates character varying(255),
  left_eyes_bg character varying(255),
  right_eyes_bg character varying(255),
  left_eyes_macula_edema character varying(255),
  right_eyes_macula_edema character varying(255),
  other_diseases_found character varying(255),
  other_diseases_cataract character varying(255),
  other_diseases_glaucoma character varying(255),
  other_diseases_pterygium character varying(255),
  other_diseases_other character varying(255),
  other_diseases_other_detail character varying(255),
  doctor_recommend character varying(255),
  eyes_management character varying(255),
  appointment_date character varying(10),
  appointment_place character varying(255),
  doctor_diag character varying(25),
  screen_date character varying(10) NOT NULL,
  active character varying(1) NOT NULL,
  record_date_time character varying(19) NOT NULL,
  update_date_time character varying(19) NOT NULL,
  cancel_date_time character varying(19),
  user_record_id character varying(255) NOT NULL,
  user_update_id character varying(255) NOT NULL,
  user_cancel_id character varying(255),
  CONSTRAINT t_ncd_eyes_complications_pkey PRIMARY KEY (t_ncd_eyes_complications_id)
);

CREATE TABLE t_ncd_foot_complications
(
  t_ncd_foot_complications_id character varying(255) NOT NULL,
  t_ncd_disease_complications_id character varying(255) NOT NULL,
  foot_ulcers character varying(255),
  foot_ulcers_detail character varying(255),
  lose_sensation character varying(255),
  shoes_regularly character varying(255),
  shoes_regularly_other_detail character varying(255),
  active character varying(1) NOT NULL,
  record_date_time character varying(19) NOT NULL,
  update_date_time character varying(19) NOT NULL,
  cancel_date_time character varying(19),
  user_record_id character varying(255) NOT NULL,
  user_update_id character varying(255) NOT NULL,
  user_cancel_id character varying(255),
  CONSTRAINT t_ncd_foot_complications_pkey PRIMARY KEY (t_ncd_foot_complications_id)
);

CREATE TABLE t_ncd_foot_artery
(
  t_ncd_foot_artery_id character varying(255) NOT NULL,
  t_ncd_foot_complications_id character varying(255) NOT NULL,
  left_dorsalis_pedis character varying(255),
  right_dorsalis_pedis character varying(255),
  left_posterior_tibial character varying(255),
  right_posterior_tibial character varying(255),
  left_gangrene character varying(255),
  right_gangrene character varying(255),
  left_sensory character varying(255),
  left_sensory_count character varying(255),
  right_sensory character varying(255),
  right_sensory_count character varying(255),
  wound character varying(255),
  wound_position character varying(255),
  wound_area character varying(255),
  wound_size character varying(255),
  record_date_time character varying(19) NOT NULL,
  update_date_time character varying(19) NOT NULL,
  user_record_id character varying(255) NOT NULL,
  user_update_id character varying(255) NOT NULL,
  CONSTRAINT t_ncd_foot_artery_pkey PRIMARY KEY (t_ncd_foot_artery_id)
);

CREATE TABLE t_ncd_foot_assess
(
  t_ncd_foot_assess_id character varying(255) NOT NULL,
  t_ncd_foot_complications_id character varying(255) NOT NULL,
  prodoscope_result character varying(255),
  nail_problem character varying(255),
  nail_problem_detail character varying(255),
  wart_problem character varying(255),
  wart_problem_detail character varying(255),
  foot_deformities character varying(255),
  skin_color character varying(255),
  hair_loss character varying(255),
  skin_temperature character varying(255),
  fungal_infections character varying(255),
  record_date_time character varying(19) NOT NULL,
  update_date_time character varying(19) NOT NULL,
  user_record_id character varying(255) NOT NULL,
  user_update_id character varying(255) NOT NULL,
  CONSTRAINT t_ncd_foot_assess_pkey PRIMARY KEY (t_ncd_foot_assess_id)
);

CREATE TABLE t_ncd_foot_assess_detail
(
  t_ncd_foot_assess_detail_id character varying(255) NOT NULL,
  t_ncd_foot_assess_id character varying(255) NOT NULL,
  f_ncd_foot_assess_type_id character varying(1) NOT NULL,
  other_detail character varying(255),
  record_date_time character varying(19) NOT NULL,
  update_date_time character varying(19) NOT NULL,
  user_record_id character varying(255) NOT NULL,
  user_update_id character varying(255) NOT NULL,
  CONSTRAINT t_ncd_foot_assess_detail_pkey PRIMARY KEY (t_ncd_foot_assess_detail_id)
);

CREATE TABLE t_ncd_foot_result
(
  t_ncd_foot_result_id character varying(255) NOT NULL,
  t_ncd_foot_complications_id character varying(255) NOT NULL,
  f_ncd_foot_screen_result_id character varying(1) NOT NULL,
  appoint_date character varying(10),
  assessor character varying(255),
  assessor_date character varying(10),
  record_date_time character varying(19) NOT NULL,
  update_date_time character varying(19) NOT NULL,
  user_record_id character varying(255) NOT NULL,
  user_update_id character varying(255) NOT NULL,
  CONSTRAINT t_ncd_foot_result_pkey PRIMARY KEY (t_ncd_foot_result_id)
);

CREATE TABLE t_ncd_foot_protect
(
  t_ncd_foot_protect_id character varying(255) NOT NULL,
  t_ncd_foot_result_id character varying(255) NOT NULL,
  f_ncd_foot_protect_type_id character varying(1) NOT NULL,
  record_date_time character varying(19) NOT NULL,
  update_date_time character varying(19) NOT NULL,
  user_record_id character varying(255) NOT NULL,
  user_update_id character varying(255) NOT NULL,
  CONSTRAINT t_ncd_foot_protect_pkey PRIMARY KEY (t_ncd_foot_protect_id)
);


insert into f_person_ncd_screen_type values ('0','ไม่มีทะเบียน/ข้อมูลโรค');
insert into f_person_ncd_screen_type values ('1','มีทะเบียน/ข้อมูลโรค');
insert into f_person_ncd_screen_type values ('9','ไม่มีข้อมูล');

insert into f_person_ncd_group values ('0','ไม่มี');
insert into f_person_ncd_group values ('1','เบาหวาน');
insert into f_person_ncd_group values ('2','ความดัน');
insert into f_person_ncd_group values ('3','เบาหวานและความดัน');

insert into f_ncd_complications values ('0','ไม่มี');
insert into f_ncd_complications values ('1','ตา');
insert into f_ncd_complications values ('2','ไต');
insert into f_ncd_complications values ('3','เท้า');
insert into f_ncd_complications values ('4','หัวใจ');
insert into f_ncd_complications values ('5','สมอง');
insert into f_ncd_complications values ('6','อื่นๆ');

insert into f_ncd_verbal_screening values ('0','ไม่มี');
insert into f_ncd_verbal_screening values ('1','มีประวัติ พ่อ แม่ ญาติพี่น้องสายตรงเป็นเบาหวาน');
insert into f_ncd_verbal_screening values ('2','รอบเอวเกิน ในชาย ≥ 90 ซม. หญิง   ≥ 80 ซม. หรือ มีภาวะอ้วนดัชนีมวลกาย ≥ 25 กก./ม2');
insert into f_ncd_verbal_screening values ('3','ความดันโลหิต ≥ 140/90  มม.ปรอท หรือเคยมีประวัติเป็นโรคความดันโลหิตสูง ');
insert into f_ncd_verbal_screening values ('4','มีประวัติน้ำตาลในเลือดสูง    (FPG = 100-125 mg%, OGTT > 140mg%)');
insert into f_ncd_verbal_screening values ('5','มีประวัติ มีภาวะไขมันในเลือดสูง (Triglyceride > 250 mg %,   HDL < 35 mg %)');
insert into f_ncd_verbal_screening values ('6','ผู้หญิงที่เคยมีประวัติเป็นเบาหวานขณะตั้งครรภ์ หรือ มีบุตรที่มีน้ำหนักแรกเกิดเกิน 4 กิโลกรัม');
insert into f_ncd_verbal_screening values ('7','มีประวัติ ความดันสูงในญาติสายตรง');


insert into f_ncd_family_relation values ('1','พ่อแม่');
insert into f_ncd_family_relation values ('2','พี่ น้อง(ญาติสายตรง)');

insert into f_ncd_history_conduct values ('1','รับการรักษาอยู่/ปฏิบัติตามแพทย์แนะนำ');
insert into f_ncd_history_conduct values ('2','รับการรักษาแต่ไม่สม่ำเสมอ ');
insert into f_ncd_history_conduct values ('3','เคยรักษา ขณะนี้ไม่รักษา/หายาทานเอง');
insert into f_ncd_history_conduct values ('9','ไม่ได้ถาม');

insert into f_ncd_cigarette_feq values ('0','ไม่สูบ');
insert into f_ncd_cigarette_feq values ('1','สูบนานๆครั้ง');
insert into f_ncd_cigarette_feq values ('2','สูบเป็นครั้งคราว');
insert into f_ncd_cigarette_feq values ('3','สูบเป็นประจำ');
insert into f_ncd_cigarette_feq values ('4','เคยสูบแต่เลิกแล้ว');
insert into f_ncd_cigarette_feq values ('9','ไม่ได้ถาม');

insert into f_ncd_alcohol_feq values ('0','ไม่ดื่ม');
insert into f_ncd_alcohol_feq values ('1','ดื่มนานๆครั้ง');
insert into f_ncd_alcohol_feq values ('2','ดื่มเป็นครั้งคราว');
insert into f_ncd_alcohol_feq values ('3','ดื่มเป็นประจำ');
insert into f_ncd_alcohol_feq values ('4','เคยดื่มแต่เลิกแล้ว');
insert into f_ncd_alcohol_feq values ('9','ไม่ได้ถาม');


insert into f_ncd_exercise_feq values ('0','ไม่ออกกำลังกายเลย');
insert into f_ncd_exercise_feq values ('1','ออกกำลังกาย ทุกวันครั้งละ 30 นาที');
insert into f_ncd_exercise_feq values ('2','ออกกำลังกาย สัปดาห์ละมากกว่า 3 ครั้งครั้งละ 30 นาทีสม่ำเสมอ  ');
insert into f_ncd_exercise_feq values ('3','ออกกำลังกาย สัปดาห์ละ 3 ครั้ง  ครั้งละ 30 นาทีสม่ำเสมอ');
insert into f_ncd_exercise_feq values ('4','ออกกำลังกาย น้อยกว่าสัปดาห์ละ 3 ครั้ง  ');
insert into f_ncd_exercise_feq values ('9','ไม่ได้ถาม');


insert into f_ncd_fruits_vegetable values ('0','ไม่ทาน');
insert into f_ncd_fruits_vegetable values ('1','ทานน้อย');
insert into f_ncd_fruits_vegetable values ('2','ทานเป็นประจำ');
insert into f_ncd_fruits_vegetable values ('9','ไม่ได้ถาม');


insert into f_ncd_food_taste values ('0','ไม่ชอบ');
insert into f_ncd_food_taste values ('1','หวาน');
insert into f_ncd_food_taste values ('2','เค็ม');
insert into f_ncd_food_taste values ('3','มัน');

insert into f_ncd_drive_protect values ('0','ไม่ขับขี่/โดยสาร');
insert into f_ncd_drive_protect values ('1','ขับขี่/โดยสาร และใส่หมวกกันน็อก/คาดเข็มขัดนิรภัยทุกครั้ง');
insert into f_ncd_drive_protect values ('2','ขับขี่/โดยสาร และใส่หมวกกันน็อก/คาดเข็มขัดนิรภัยบางครั้ง');
insert into f_ncd_drive_protect values ('3','ขับขี่/โดยสาร และใส่หมวกกันน็อก/คาดเข็มขัดนิรภัยนาน ๆ ครั้ง (ใส่เฉพาะเมื่อมีด่านตรวจ)');
insert into f_ncd_drive_protect values ('9','ไมได้ถาม');

insert into f_ncd_sex_protect values ('0','ไม่ใช้');
insert into f_ncd_sex_protect values ('1','ใช้เมื่อถูกขอร้อง ');
insert into f_ncd_sex_protect values ('2','ใช้ทุกครั้ง ');
insert into f_ncd_sex_protect values ('3','ไม่เคยมีเพศสัมพันธุ์กับผู้ที่ไม่ใช่สามีหรือภรรยา');
insert into f_ncd_sex_protect values ('4','ไม่ตอบ');
insert into f_ncd_sex_protect values ('9','ไม่ได้ถาม');

insert into f_ncd_risk_group values ('1','กลุ่มปกติ');
insert into f_ncd_risk_group values ('2','กลุ่มเสี่ยงสูง');
insert into f_ncd_risk_group values ('3','กลุ่มสงสัยรายใหม่');
insert into f_ncd_risk_group values ('4','กลุ่มป่วยที่ ไม่มีภาวะแทรกซ้อน');
insert into f_ncd_risk_group values ('5','กลุ่มป่วยที่ มีภาวะแทรกซ้อน');

insert into f_ncd_risk_type values ('0','ไม่เสี่ยง');
insert into f_ncd_risk_type values ('1','เสี่ยง');
insert into f_ncd_risk_type values ('2','ป่วย');

insert into f_ncd_operations_type values ('1','ให้คำแนะนำการดูแลตนเอง  และตรวจคัดกรองซ้ำทุก 1 ปี');
insert into f_ncd_operations_type values ('2','ลงทะเบียนกลุ่มเสี่ยงต่อโรค Metabolic และแนะนำเข้าโครงการปรับเปลี่ยนพฤติกรรม');
insert into f_ncd_operations_type values ('3','ส่งต่อเพื่อรักษา');


insert into b_item_lab_ncd_std values ('ncd201000000000002','32203','Glucose(DTX Fasting)','Glucose(DTX Fasting)เจาะปลายนิ้วแบบอดอาหาร','1');
insert into b_item_lab_ncd_std values ('ncd201000000000003','32203','Glucose(DTX NonFasting)','Glucose(DTX NonFasting)เจาะปลายนิ้วแบบไม่อดอาหาร','1');
insert into b_item_lab_ncd_std values ('ncd201000000000004','32203','Glucose(FBS)','Glucose(FBS)เจาะเส้นเลือดดำแบบอดอาหาร','1');
insert into b_item_lab_ncd_std values ('ncd201000000000005','32203','Glucose(BS)','Glucose(BS)เจาะเส้นเลือดดำแบบไม่อดอาหาร','1');
insert into b_item_lab_ncd_std values ('ncd201000000000006','32401','Hb A1C','ฮีโมโกบิน A1C','1');
insert into b_item_lab_ncd_std values ('ncd201000000000007','32501','Lipid  - Cholesterol','','1');
insert into b_item_lab_ncd_std values ('ncd201000000000008','32502','Lipid  - TG (Triglyceride)','','1');
insert into b_item_lab_ncd_std values ('ncd201000000000009','32503','Lipid  - HDL-chol','','1');
insert into b_item_lab_ncd_std values ('ncd201000000000010','32504','Lipid  - LDL-chol (direct) สั่งรายการเดียว ','','1');
insert into b_item_lab_ncd_std values ('ncd201000000000011','32201','BUN (Blood Urea Nitrogen)','','1');
insert into b_item_lab_ncd_std values ('ncd201000000000012','32202','Creatinine','','1');
insert into b_item_lab_ncd_std values ('ncd201000000000013','32102','Sodium (Na)','','1');
insert into b_item_lab_ncd_std values ('ncd201000000000014','32104','Chloride (CI)','','1');
insert into b_item_lab_ncd_std values ('ncd201000000000015','34303','TCO2','','1');
insert into b_item_lab_ncd_std values ('ncd201000000000016','32103','Potassium (K)','','1');
insert into b_item_lab_ncd_std values ('ncd201000000000017','34002','Urine Na','','1');
insert into b_item_lab_ncd_std values ('ncd201000000000018','34004','Urine Potassium','','1');
insert into b_item_lab_ncd_std values ('ncd201000000000019','34103','Urine Protein (Random)','','1');
insert into b_item_lab_ncd_std values ('ncd201000000000020','34104','Urine Protein (24 Hours) ','','1');
insert into b_item_lab_ncd_std values ('ncd201000000000021','31004','Urine albumin','','1');
insert into b_item_lab_ncd_std values ('ncd201000000000022','34116','Urine Microalbumin','ตรวจ strip','1');
insert into b_item_lab_ncd_std values ('ncd201000000000023','34116','Urine Microalbumin','ตรวจเครื่อง','1');
insert into b_item_lab_ncd_std values ('ncd201000000000024','32205','Uric acid','','1');
insert into b_item_lab_ncd_std values ('ncd201000000000025','30104','CBC (Hematocrit)','','1');
insert into b_item_lab_ncd_std values ('ncd201000000000026','30104','CBC (Hemoglobin)','','1');
insert into b_item_lab_ncd_std values ('ncd201000000000027','30101','CBC (+ diff. + RBC morphology + plt count) by automation','','1');
insert into b_item_lab_ncd_std values ('ncd201000000000028','31001','Urine Analysis','','1');

insert into f_ncd_group_code values ('ncd202000000000001','01','โรคเบาหวาน(Diabetes mellitus)','E10','E15.9','1','2897389127774');
insert into f_ncd_group_code values ('ncd202000000000002','02','โรคความดัน(Hypertension)','I10','I15.9','2','2892021678342');
insert into f_ncd_group_code values ('ncd202000000000003','26','โรคหัวใจขาดเลือด (MI)','I20','I25.9','3','2891497002173');
insert into f_ncd_group_code values ('ncd202000000000004','27','โรคหลอดเลือดสมอง (Strokes)','I60','I69.9','4','');
insert into f_ncd_group_code values ('ncd202000000000005','28','โรคปอดอุดกลั้นเรื้อรัง (COPD)','J44','J44.9','5','2896961106128');
insert into f_ncd_group_code values ('ncd202000000000006','29','วัณโรค (TB)','A15','A19.9','6','2899957321157');
insert into f_ncd_group_code values ('ncd202000000000007','30','บำบัดยาเสพติด','F11','F19.9','7','');
insert into f_ncd_group_code values ('ncd202000000000008','31','โรคจิตเภทและหลงผิด(Schizophrenia)','F20','F29.9','8','2892568641652');
insert into f_ncd_group_code values ('ncd202000000000009','32','โรคธัยรอยด์(Thyroid)','E05','E05','9','2899167805640');
insert into f_ncd_group_code values ('ncd202000000000010','33','โรคหอบหืด(Asthma)','J45','J45.9','10','2892486968508');
insert into f_ncd_group_code values ('ncd202000000000011','34','โรคซึมเศร้า(Depression)','F30','F39.9','11','');
insert into f_ncd_group_code values ('ncd202000000000012','35','โรคอัมพฤกษ์ อัมพาต (Palsy)','G80','G83.9','12','');
insert into f_ncd_group_code values ('ncd202000000000013','36','โรคหลอดลมอักเสบเรื้อรัง(Bronchitis)','J42','J42','13','');
insert into f_ncd_group_code values ('ncd202000000000014','37','โรคพิษสุราเรื้อรัง (Alcoholism)','F10.0','F10.2','14','');
insert into f_ncd_group_code values ('ncd202000000000015','03A','โรคตับอักเสบเรื้อรัง (Chronic hepatitis)','K73','K73.9','15','');
insert into f_ncd_group_code values ('ncd202000000000016','03B','โรคตับแข็ง (Cirrhosis of liver)','B18','B18','16','');
insert into f_ncd_group_code values ('ncd202000000000017','03B','โรคตับแข็ง (Cirrhosis of liver)','K74','K74','17','');
insert into f_ncd_group_code values ('ncd202000000000018','04','โรคหัวใจล้มเหลว(Congestive heart failure)','I50','I50.9','18','2891497002173');
insert into f_ncd_group_code values ('ncd202000000000019','05','โรคเส้นเลือดสมองแตก/อุดตัน (Carebrovascular accident)','I69','I69','19','');
insert into f_ncd_group_code values ('ncd202000000000020','06','โรคมะเร็ง (Malignancy)','C00','C97.9','20','');
insert into f_ncd_group_code values ('ncd202000000000021','07','โรคภูมิคุ้มกันบกพร่อง(AIDS)','B20','B24.9','21','');
insert into f_ncd_group_code values ('ncd202000000000022','08','โรคถุงลมโป่งพอง(Emphysema)','J43','J43','22','');
insert into f_ncd_group_code values ('ncd202000000000023','09','โรคไตวายเรื้อรัง(Chronic renal Failure)','N18','N18','23','2895478536034');
insert into f_ncd_group_code values ('ncd202000000000024','10','โรคพาร์กินสัน (Parkinson)','G20','G22.9','24','');
insert into f_ncd_group_code values ('ncd202000000000025','11','โรคมายแอสทีเนีย เกรวิส(Myasthenia gravis)','G70','G70','25','');
insert into f_ncd_group_code values ('ncd202000000000026','12','โรคเบาจืด(Diabetes insipidus)','E23.2','E23.2','26','');
insert into f_ncd_group_code values ('ncd202000000000027','12','โรคเบาจืด(Diabetes insipidus)','N25.1','N25.1','27','');
insert into f_ncd_group_code values ('ncd202000000000028','13','โรคมัลติเพิล สเคลอโรสิส (Multiple sclerosis)','G35','G35','28','');
insert into f_ncd_group_code values ('ncd202000000000029','14','โรคไขมันในเลือดสูง(Dyslipidemia)','E78','E78','29','');
insert into f_ncd_group_code values ('ncd202000000000030','15','โรคข้ออักเสบรูมาตอยด์ (Rheumatoid arthritis)','M05','M06.9','30','');
insert into f_ncd_group_code values ('ncd202000000000031','16','โรคต้อหิน(Glaucoma)','H40','H40.9','31','');
insert into f_ncd_group_code values ('ncd202000000000032','16','โรคต้อหิน(Glaucoma)','H42','H42.8','32','');
insert into f_ncd_group_code values ('ncd202000000000033','17','โรคไต เนฟโฟรติค (Nephrotic syndrome)','N04','N04','33','2895478536034');
insert into f_ncd_group_code values ('ncd202000000000034','18','โรคลูปัส(SLE)','M32','M32.9','34','');
insert into f_ncd_group_code values ('ncd202000000000035','19','โรคเลือดอะพลาสติค(Aplastic anemia)','D60','D61','35','');
insert into f_ncd_group_code values ('ncd202000000000036','20','โรคธาลัสซีเมีย(Thalassemia)','D56','D56.9','36','2890299668530');
insert into f_ncd_group_code values ('ncd202000000000037','21','โรคฮีโมฟิลเลีย (Hemophilia)','D66','D66','37','');
insert into f_ncd_group_code values ('ncd202000000000038','22','โรคเรื้อนกวาง(Psoriasis)','L40','L40.9','38','');
insert into f_ncd_group_code values ('ncd202000000000039','23','โรคผิวหนังพุพองเรื้อรัง(Chronic vesiculobullous disease)','L10','L13.9','39','');
insert into f_ncd_group_code values ('ncd202000000000040','24','โรคเลือด ไอทีพี (ITP)','D69.3','D69.3','40','');
insert into f_ncd_group_code values ('ncd202000000000041','25','โรคต่อมไทรอยด์เป็นพิษ (Thyrotoxicosis)','E05','E05.9','41','2899167805640');

insert into f_ncd_ans_result values('0','ไม่ใช่ ไม่เป็น ไม่มี ไม่ชอบ ไม่ให้');
insert into f_ncd_ans_result values('1','ใช่ เป็น มี ชอบ ให้');
insert into f_ncd_ans_result values('9','ไม่ตรวจ ไม่เคยตรวจ ไม่ได้ถาม');

insert into f_ncd_disease_complications values ('1','ตา');
insert into f_ncd_disease_complications values ('2','ไต');
insert into f_ncd_disease_complications values ('3','เท้า');
insert into f_ncd_disease_complications values ('4','หัวใจ');
insert into f_ncd_disease_complications values ('5','สมอง');
insert into f_ncd_disease_complications values ('6','ปริทัณฑ์');

insert into f_ncd_dm_type values ('1','DM Type1 (IDDM)');
insert into f_ncd_dm_type values ('2','DM Type2 (NIDDM)');
insert into f_ncd_dm_type values ('3','GDM');
insert into f_ncd_dm_type values ('4','DM Other type');
insert into f_ncd_dm_type values ('6','Uncertain');
insert into f_ncd_dm_type values ('9','ไม่ได้ถาม');

insert into f_ncd_ans_normal_result values ('0','ไม่ปกติ');
insert into f_ncd_ans_normal_result values ('1','ปกติ');
insert into f_ncd_ans_normal_result values ('9','ไม่ตรวจ');

insert into f_ncd_eyes_result_type values ('0','ไม่ระบุ');
insert into f_ncd_eyes_result_type values ('1','No PL');
insert into f_ncd_eyes_result_type values ('2','PL');
insert into f_ncd_eyes_result_type values ('3','PJ');
insert into f_ncd_eyes_result_type values ('4','HM');
insert into f_ncd_eyes_result_type values ('5','FC');
insert into f_ncd_eyes_result_type values ('6','20/200(6/60)');
insert into f_ncd_eyes_result_type values ('7','20/100(6/36)');
insert into f_ncd_eyes_result_type values ('8','20/100(6/36)');
insert into f_ncd_eyes_result_type values ('9','20/70(6/24)');
insert into f_ncd_eyes_result_type values ('10','20/30(6/9)');
insert into f_ncd_eyes_result_type values ('11','20/20(6/6)');
insert into f_ncd_eyes_result_type values ('12','5/60');
insert into f_ncd_eyes_result_type values ('13','4/60');
insert into f_ncd_eyes_result_type values ('14','3/60');
insert into f_ncd_eyes_result_type values ('15','2/60');
insert into f_ncd_eyes_result_type values ('16','1/60');

insert into f_ncd_eyes_diagnosis_type values ('0','ไม่ระบุ');
insert into f_ncd_eyes_diagnosis_type values ('1','No DR');
insert into f_ncd_eyes_diagnosis_type values ('2','Mild NPDR');
insert into f_ncd_eyes_diagnosis_type values ('3','Moderate NPDR');
insert into f_ncd_eyes_diagnosis_type values ('4','Severe NPDR');
insert into f_ncd_eyes_diagnosis_type values ('5','CSME');
insert into f_ncd_eyes_diagnosis_type values ('6','PDR');

insert into f_ncd_eyes_ma_type values ('0','ไม่ส่ง');
insert into f_ncd_eyes_ma_type values ('1','ส่งพบจักษุแพทย์เพื่อยิง laser');
insert into f_ncd_eyes_ma_type values ('2','ส่งพบจักษุแพทย์เพื่อผ่าตัด');

insert into f_ncd_dm_treat_type values ('0','ไม่ได้กินยา');
insert into f_ncd_dm_treat_type values ('1','ฉีด insulin ');
insert into f_ncd_dm_treat_type values ('2','กินยา ');
insert into f_ncd_dm_treat_type values ('3','อื่นๆ');
insert into f_ncd_dm_treat_type values ('9','ไม่ได้ถาม');

insert into f_ncd_lose_sensation_type values ('0','ไม่มี');
insert into f_ncd_lose_sensation_type values ('1','ชา');
insert into f_ncd_lose_sensation_type values ('2','ปวด');
insert into f_ncd_lose_sensation_type values ('9','ไม่ได้ถาม');

insert into f_ncd_shoes_type values ('0','ไม่ใส่');
insert into f_ncd_shoes_type values ('1','รองเท้าแตะแบบหูหนีบ');
insert into f_ncd_shoes_type values ('2','รองเท้าแตะแบบสวม');
insert into f_ncd_shoes_type values ('3','รองเท้าผ้าใบ');
insert into f_ncd_shoes_type values ('4','รองเท้าหุ้มส้น/คัตชู');
insert into f_ncd_shoes_type values ('5','อื่นๆ');
insert into f_ncd_shoes_type values ('9','ไม่ได้ถาม');

insert into f_ncd_foot_screen_result values ('1','low risk');
insert into f_ncd_foot_screen_result values ('2','Moderate risk');
insert into f_ncd_foot_screen_result values ('3','High risk');
insert into f_ncd_foot_screen_result values ('4','Very High risk (FOOT ULCER)');
insert into f_ncd_foot_screen_result values ('5','Very High risk (AMPUTATION)');
insert into f_ncd_foot_screen_result values ('9','ไม่ได้ตรวจ');

insert into f_ncd_foot_protect_type values ('1','ดูแลเท้าประจำวันและรองเท้าที่เหมาะสม');
insert into f_ncd_foot_protect_type values ('2','รองเท้าพิเศษ');
insert into f_ncd_foot_protect_type values ('3','ตัดเล็บ');
insert into f_ncd_foot_protect_type values ('4','ควบคุมน้ำตาล');
insert into f_ncd_foot_protect_type values ('5','ขูด callus');
insert into f_ncd_foot_protect_type values ('6','ทาโลชั่น');
insert into f_ncd_foot_protect_type values ('7','ส่งต่อคลินิกเท้า');

insert into f_ncd_prodoscope_type values ('1','Normal');
insert into f_ncd_prodoscope_type values ('2','Flat Foot ');
insert into f_ncd_prodoscope_type values ('3','High Arch Foot');
insert into f_ncd_prodoscope_type values ('9','ไม่ระบุ');


insert into f_ncd_foot_assess_type values ('1','hammer toes ');
insert into f_ncd_foot_assess_type values ('2','claw toes');
insert into f_ncd_foot_assess_type values ('3','bunions');
insert into f_ncd_foot_assess_type values ('9','nony_prominence ');
insert into f_ncd_foot_assess_type values ('5','char_cot_foot  ');
insert into f_ncd_foot_assess_type values ('6','other');

insert into f_ncd_foot_screen_skin_color values ('1','ปกติ');
insert into f_ncd_foot_screen_skin_color values ('2','แดง');
insert into f_ncd_foot_screen_skin_color values ('3','ซีด');
insert into f_ncd_foot_screen_skin_color values ('4','คล้ำ');
insert into f_ncd_foot_screen_skin_color values ('9','ไม่ได้ตรวจ');

insert into f_ncd_foot_skin_temperature values ('0','เย็น');
insert into f_ncd_foot_skin_temperature values ('1','ปกติ');
insert into f_ncd_foot_skin_temperature values ('9','ไม่ได้ตรวจ');

insert into f_ncd_wound_position values ('1','เท้าซ้าย');
insert into f_ncd_wound_position values ('2','เท้าขวา');
insert into f_ncd_wound_position values ('3','ทั้งเท้าซ้ายและขวา');
insert into f_ncd_wound_position values ('9','ไม่ได้ตรวจ');

insert into f_ncd_cigarette_type values ('1','บุหรี่ปกติ');
insert into f_ncd_cigarette_type values ('2','บุหรี่พื้นเมือง(ขี้โย)มวนเล็ก');
insert into f_ncd_cigarette_type values ('3','บุหรี่พื้นเมือง(ขี้โย)มวนกลาง');
insert into f_ncd_cigarette_type values ('4','บุหรี่พื้นเมือง(ขี้โย)มวนใหญ่ 1 คืบ)');
insert into f_ncd_cigarette_type values ('9','ไม่ทราบชนิด');

CREATE TABLE s_ncd_version (
    s_ncd_version_id                    varchar(255) NOT NULL,
    version_ncd_number            	varchar(255) NULL,
    version_ncd_description       	varchar(255) NULL,
    version_ncd_application_number	varchar(255) NULL,
    version_ncd_database_number   	varchar(255) NULL,
    version_ncd_update_time       	varchar(255) NULL
    );

ALTER TABLE "s_ncd_version"
	ADD CONSTRAINT "s_ncd_version_pkey"
	PRIMARY KEY ("s_ncd_version_id");


INSERT INTO s_ncd_version VALUES ('9760000000001', '1', 'NCD Module', '1.0.20110907', '1.0.20110907', (select current_date) || ','|| (select current_time));

INSERT INTO s_script_update_log values ('NCD_Module','update_ncd_001.sql',(select current_date) || ','|| (select current_time),'Initialize NCD Module');
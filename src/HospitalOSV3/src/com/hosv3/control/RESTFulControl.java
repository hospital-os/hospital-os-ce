/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.control;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.hospital_os.object.RestfulUrl;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hosv3.object.ServiceResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.FieldPosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import sd.util.datetime.DateTimeUtil;

/**
 *
 * @author Somprasong Damyos <somprasong@hospital-os.com>
 */
public class RESTFulControl {

    private static final Logger LOG = Logger.getLogger(RESTFulControl.class.getName());
    private UpdateStatus theUS;
    private ConnectionInf theConnectionInf;
    private HosDB theHosDB;

    public void setHosControl(HosControl hosControl) {
//        this.theHC = hosControl;
        this.theConnectionInf = hosControl.theConnectionInf;
        this.theHosDB = hosControl.theHosDB;
    }

    public void setUpdateStatus(UpdateStatus updateStatus) {
        this.theUS = updateStatus;
    }

    public RestfulUrl getRestfulUrl(String code) {
        RestfulUrl restfulUrl = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            restfulUrl = theHosDB.theRestfulUrlDB.selectByCode(code);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return restfulUrl;
    }

    public ServiceResponse getJSONFromRESTful(String path) {
        return getJSONFromRESTful(path, "");
    }

    public ServiceResponse getJSONFromRESTful(String path, LinkedHashMap<String, Object> params) {
        return getJSONFromRESTful(path, getQueryParameter(params));
    }

    public ServiceResponse getJSONFromRESTful(String path, String queryParam) {
        return getJSONFromRESTful(path, queryParam, null);
    }

    public ServiceResponse getJSONFromRESTful(String path, String queryParam, Map<String, String> headers) {
        ServiceResponse serviceResponse = new ServiceResponse();
        Object responses = new ArrayList<LinkedHashMap<String, Object>>();
        serviceResponse.setResponseData(responses);
        java.net.URL url;
        HttpURLConnection httpURLConnection = null;
        BufferedReader reader;
        try {
            // request data
            LOG.info("Creating connection to RESTful.");
            url = new URL(path + queryParam);
//                    LOG.info(url.toString());
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setConnectTimeout(60000);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setRequestMethod("GET");
            if (headers != null && !headers.isEmpty()) {
                for (String key : headers.keySet()) {
                    httpURLConnection.setRequestProperty(key, headers.get(key));
                }
            }
            LOG.info("Sending request to RESTful.");
            reader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream(), Charset.forName("utf-8")));
            String response = "";
            String readLine;
            while ((readLine = reader.readLine()) != null) {
                response += readLine;
            }
            reader.close();
            LOG.info("Retrieved response from RESTful.");
//            LOG.log(Level.INFO, "response\n{0}", response);
            responses = getJSONArrayFromString(response);

            if (responses == null) {
                responses = getJSONObjectFromString(response);
            }
//            LOG.log(Level.INFO, "json\n{0}", responses.toString());
            serviceResponse.setResponseCode(ServiceResponse.SUCCESS);
            serviceResponse.setResponseMessage("Retrieved response from RESTful.");
            serviceResponse.setResponseData(responses);
        } catch (IOException ex) {
            String responseErr = "";
            try {
                if (httpURLConnection != null && 4 == httpURLConnection.getResponseCode() / 100) {
                    try (BufferedReader readerErr = new BufferedReader(
                            new InputStreamReader(
                                    httpURLConnection.getErrorStream(),
                                    Charset.forName("utf-8")))) {
                        String readLine;
                        while ((readLine = readerErr.readLine()) != null) {
                            responseErr += readLine;
                        }
                        LinkedHashMap<String, Object> mapResponse = getJSONObjectFromString(responseErr);
                        if (mapResponse != null) {
                            responseErr = (String) (mapResponse.get("label") != null
                                    ? mapResponse.get("label")
                                    : "");
                        }
                    }
                }
            } catch (IOException ex2) {
                LOG.log(Level.SEVERE, ex2.getMessage(), ex2);
            }
            try {
                responseErr = !responseErr.isEmpty()
                        ? responseErr
                        : httpURLConnection == null
                                ? ""
                                : String.format("(HTTP Response %s : %s)",
                                        new Object[]{
                                            httpURLConnection.getResponseCode(),
                                            httpURLConnection.getResponseMessage()
                                        });
            } catch (IOException ex1) {
                LOG.log(Level.SEVERE, null, ex1);
            }
            String msgErr = String.format("�������ö�Ѻ���Ѿ��ҡ RESTful �� ���ͧ�ҡ\n%s", responseErr.isEmpty() ? ex.getMessage() : responseErr);
            LOG.log(Level.SEVERE, msgErr);
            serviceResponse.setResponseCode(ServiceResponse.FAIL);
            serviceResponse.setResponseMessage(msgErr);
            theUS.setStatus(msgErr, UpdateStatus.ERROR);
        } finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        }
        return serviceResponse;
    }

    public static String getQueryParameter(LinkedHashMap<String, Object> params) {
        if (params.size() == 0) {
            return "";
        }
        return "?" + getParameter(params);
    }

    public static String getParameter(LinkedHashMap<String, Object> params) {
        String queryParam = "";
        for (String key : params.keySet()) {
            Object value = params.get(key);
            if (value instanceof Collection) {
                Collection collection = (Collection) value;
                for (Object object : collection) {
                    queryParam += ((queryParam.isEmpty() ? "" : "&") + key + "=" + encodeParameter(String.valueOf(object)));
                }
            } else {
                queryParam += ((queryParam.isEmpty() ? "" : "&") + key + "=" + encodeParameter(String.valueOf(value)));
            }
        }
        return queryParam;
    }

    public static String encodeParameter(String param) {
        try {
            return URLEncoder.encode(String.valueOf(param), "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            return "";
        }
    }

    public ServiceResponse sendMethodPOST(String path, LinkedHashMap<String, Object> params) {
        return sendMethodPOST(path, params, null);
    }

    public ServiceResponse sendMethodPOST(String path, LinkedHashMap<String, Object> params, Map<String, String> headers) {
        ServiceResponse serviceResponse = new ServiceResponse();
        java.net.URL url;
        HttpURLConnection httpURLConnection = null;
        OutputStreamWriter writer;
        try {
            LOG.info("Creating connection to RESTful.");
            url = new URL(path);
//            LOG.info(url.toString());
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setRequestProperty("Content-type", "application/x-www-form-urlencoded");
            if (headers != null && !headers.isEmpty()) {
                for (String key : headers.keySet()) {
                    httpURLConnection.setRequestProperty(key, headers.get(key));
                }
            }
            LOG.info("Sending POST request to RESTful.");
            writer = new OutputStreamWriter(httpURLConnection.getOutputStream(), "UTF-8");
            String msg = getParameter(params);
            writer.write(msg);
            writer.flush();
            writer.close();
            LOG.info("Retrieved response from RESTful.");
            if (2 != httpURLConnection.getResponseCode() / 100) {
                String responseErr = String.format("(HTTP Response %s : %s)", new Object[]{httpURLConnection.getResponseCode(), httpURLConnection.getResponseMessage()});
                String msgErr = String.format("����觢�����Ẻ POST ��ѧ Server �����ͧ�ҡ\n(%s)", responseErr);
                LOG.log(Level.SEVERE, msgErr);
                serviceResponse.setResponseCode(ServiceResponse.FAIL);
                serviceResponse.setResponseMessage(msgErr);
            } else {
                LOG.log(Level.INFO, "Send POST request to RESTful succeeded.");
                serviceResponse.setResponseCode(ServiceResponse.SUCCESS);
                serviceResponse.setResponseMessage("Send POST request to RESTful succeeded.");
            }

        } catch (IOException ex) {
            try {
                LOG.log(Level.SEVERE, ex.getMessage()
                        + (httpURLConnection != null ? "\n" + httpURLConnection.getResponseCode() + " : " + httpURLConnection.getResponseMessage() : ""), ex);
            } catch (IOException ex1) {
                LOG.log(Level.SEVERE, null, ex1);
            }
            serviceResponse.setResponseCode(ServiceResponse.FAIL);
            serviceResponse.setResponseMessage(ex.getMessage());
        } finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        }
        return serviceResponse;
    }

    public static <T extends Object> T toJSON(String string, TypeReference typeRef) throws IOException {
        JsonFactory factory = new JsonFactory();
        ObjectMapper mapper = new ObjectMapper(factory);
        return (T) mapper.readValue(string, typeRef);
    }

    public static List<LinkedHashMap<String, Object>> getJSONArrayFromString(String json) {
        try {
            TypeReference<List<LinkedHashMap<String, Object>>> typeRef = new TypeReference<List<LinkedHashMap<String, Object>>>() {
            };
            return toJSON(json, typeRef);
        } catch (IOException ex) {
            return null;
        }
    }

    public static LinkedHashMap<String, Object> getJSONObjectFromString(String json) {
        TypeReference<LinkedHashMap<String, Object>> typeRef = new TypeReference<LinkedHashMap<String, Object>>() {
        };
        try {
            return toJSON(json, typeRef);
        } catch (IOException ex) {
            System.err.println("Can not convert to JSONObject: " + ex.getMessage());
            return null;
        }
    }

    public static String convertJSONObjectToString(LinkedHashMap<String, Object> json) {
        String json_value;
        ObjectMapper mapper = new ObjectMapper();
        mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.US) {
            @Override
            public StringBuffer format(Date date, StringBuffer toAppendTo, FieldPosition fieldPosition) {
                return super.format(date, toAppendTo, fieldPosition).insert(26, ':');
            }
        });
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        try {
            //convert map to JSON string
            json_value = mapper.writeValueAsString(json);
        } catch (JsonProcessingException ex) {
            json_value = null;
        }
        return json_value;
    }

    public ServiceResponse sendDataFromSql(String path, String sql) {
        ServiceResponse serviceResponse = new ServiceResponse();
        LOG.info("Preparing data from database.");
        List<LinkedHashMap<String, Object>> patients = getDataFromSql(sql);
        if (patients.isEmpty()) {
            LOG.info("Cancel : No data from sql.");
            serviceResponse.setResponseCode(ServiceResponse.FAIL);
            serviceResponse.setResponseMessage("No data from sql.");
            return serviceResponse;
        }
        LinkedHashMap<String, Object> jsonObject = patients.get(0);
        LOG.info("Prepare data completed.");
        // send to server
        java.net.URL url;
        HttpURLConnection httpURLConnection = null;
        OutputStreamWriter writer;
        try {
            // Make json string
            LOG.info("Converting data to JSON.");

            //convert map to JSON string
            String json = convertJSONObjectToString(jsonObject);// new JSON(RF1).toString();//
//                        LOG.log(Level.INFO, "Convert data to JSON completed. {0}", json);
            url = new URL(path);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setRequestMethod("PUT");
            httpURLConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            LOG.info("Sending JSON via RESTful.");
            writer = new OutputStreamWriter(httpURLConnection.getOutputStream(), "UTF-8");
            String result = new String(json.getBytes("UTF-8"), "UTF-8");
//            LOG.log(Level.INFO, result);
            writer.write(result);
            writer.flush();
            writer.close();
            if (2 == httpURLConnection.getResponseCode() / 100) {
                String msg = String.format("Send JSON via RESTful comleted. (HTTP Response %s : %s)", new Object[]{httpURLConnection.getResponseCode(), httpURLConnection.getResponseMessage()});
                LOG.log(Level.INFO, msg);
                serviceResponse.setResponseCode(ServiceResponse.SUCCESS);
                serviceResponse.setResponseMessage(msg);
            } else {
                String responseErr = "";
                try {
                    responseErr = !responseErr.isEmpty() ? responseErr : String.format("(HTTP Response %s : %s)", new Object[]{httpURLConnection.getResponseCode(), httpURLConnection.getResponseMessage()});
                } catch (IOException ex1) {
                    LOG.log(Level.SEVERE, null, ex1);
                }
                String msgErr = String.format("�������ö�ѹ�֡�����ż����¡�Ѻ��ѧ Server �� ���ͧ�ҡ\n(%s)", responseErr);
                LOG.log(Level.INFO, msgErr);
                serviceResponse.setResponseCode(ServiceResponse.FAIL);
                serviceResponse.setResponseMessage(msgErr);
            }
        } catch (IOException ex) {
            try {
                LOG.log(Level.SEVERE, ex.getMessage()
                        + (httpURLConnection != null ? "\n" + httpURLConnection.getResponseCode() + " : " + httpURLConnection.getResponseMessage() : ""), ex);
            } catch (IOException ex1) {
                LOG.log(Level.SEVERE, null, ex1);
            }
            serviceResponse.setResponseCode(ServiceResponse.FAIL);
            serviceResponse.setResponseMessage(ex.getMessage());
        } finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        }
        return serviceResponse;
    }

    public List<LinkedHashMap<String, Object>> getDataFromSql(String sql) {
        List<LinkedHashMap<String, Object>> resultList = new ArrayList<>();
        Connection connection = theConnectionInf.getConnection();
        Statement stmt = null;
        ResultSet rs = null;
        try {
            // ��ҹ sql �ҡ File
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);
            // Get result set meta data
            ResultSetMetaData rsmd = rs.getMetaData();
            int numColumns = rsmd.getColumnCount();
            String[] columnNames = new String[numColumns];
            // Get the column names; column indices start from 1
            for (int i = 1; i < numColumns + 1; i++) {
                columnNames[i - 1] = rsmd.getColumnName(i);
            }
            while (rs.next()) {
                LinkedHashMap<String, Object> record = new LinkedHashMap<>();
                for (int i = 0; i < numColumns; i++) {
                    Object obj = rs.getObject(columnNames[i]);
                    if (obj != null) {
                        record.put(columnNames[i], rs.getObject(columnNames[i]));
                    }
                }
                resultList.add(record);
            }
        } catch (SQLException ex) {
            try {
                LOG.log(Level.SEVERE, ex.getMessage(), ex);
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(Level.SEVERE, null, ex1);
            }
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOG.log(Level.SEVERE, null, ex);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    LOG.log(Level.SEVERE, null, ex);
                }
            }
        }
        return resultList;
    }

    public List<LinkedHashMap<String, Object>> getDataFromSqlWithNewConnection(String sql) {
        List<LinkedHashMap<String, Object>> resultList = new ArrayList<>();
        Connection connection = theConnectionInf.getClone().getConnection();
        Statement stmt = null;
        ResultSet rs = null;
        try {
            connection.setAutoCommit(false);
            // ��ҹ sql �ҡ File
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);
            // Get result set meta data
            ResultSetMetaData rsmd = rs.getMetaData();
            int numColumns = rsmd.getColumnCount();
            String[] columnNames = new String[numColumns];
            // Get the column names; column indices start from 1
            for (int i = 1; i < numColumns + 1; i++) {
                columnNames[i - 1] = rsmd.getColumnName(i);
            }
            while (rs.next()) {
                LinkedHashMap<String, Object> record = new LinkedHashMap<>();
                for (int i = 0; i < numColumns; i++) {
                    Object obj = rs.getObject(columnNames[i]);
                    if (obj != null) {
                        record.put(columnNames[i], rs.getObject(columnNames[i]));
                    }
                }
                resultList.add(record);
            }
            connection.commit();
        } catch (SQLException ex) {
            try {
                LOG.log(Level.SEVERE, ex.getMessage(), ex);
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(Level.SEVERE, null, ex1);
            }
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOG.log(Level.SEVERE, null, ex);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    LOG.log(Level.SEVERE, null, ex);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOG.log(Level.SEVERE, null, ex);
                }
            }
        }
        return resultList;
    }

    private String setParameters(String sql, Map<String, String> params) {
        if (params == null) {
            return sql;
        }
        for (String key : params.keySet()) {
            String value = params.get(key);
            sql = sql.replaceAll(":" + key, value);
        }
        return sql;
    }

    public static String getStringValue(LinkedHashMap<String, Object> map, String key) {
        return getStringValue(map.get(key));
    }

    public static String getStringValue(Object object) {
        String str = String.valueOf(object);
        return str == null || str.equalsIgnoreCase("null") ? "" : str;
    }

    public static String getStringDateValue(LinkedHashMap<String, Object> map, String key, String format) {
        return getStringDateValue(map.get(key), format);
    }

    public static String getStringDateValue(LinkedHashMap<String, Object> map, String key, String format, Locale locale) {
        return getStringDateValue(map.get(key), format, locale);
    }

    public static String getStringDateValue(Object object, String format) {
        return getStringDateValue(object, format, null);
    }

    public static String getStringDateValue(Object object, String format, Locale locale) {
        Date date = getDateValue(object);
        SimpleDateFormat sdf = new SimpleDateFormat(format, locale == null ? Locale.getDefault() : locale);
        return date == null ? "" : sdf.format(date);
    }

    public static Date getDateValue(LinkedHashMap<String, Object> map, String key) {
        return getDateValue(map.get(key));
    }

    public static Date getDateValue(Object object) {
        String str = getStringValue(object);
        return str == null || str.equalsIgnoreCase("null")
                ? null
                : DateTimeUtil.stringToDate(str,
                        "yyyy-MM-dd'T'HH:mm:ss",
                        DateTimeUtil.LOCALE_EN);
    }

    public ServiceResponse updateImmunization(String path, String jsonData) {
        ServiceResponse serviceResponse = new ServiceResponse();
        java.net.URL url;
        HttpURLConnection httpURLConnection = null;
        OutputStreamWriter writer;
        try {
            LOG.info("Creating connection to RESTful.");
            url = new URL(path);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setRequestProperty("Content-type", "application/json");
            httpURLConnection.setRequestMethod("POST");
            LOG.info("Sending POST request to RESTful.");
            writer = new OutputStreamWriter(httpURLConnection.getOutputStream(), "UTF-8");
            writer.write(jsonData);
            writer.flush();
            writer.close();
            LOG.info("Retrieved response from RESTful.");
            if (2 != httpURLConnection.getResponseCode() / 100) {
                StringBuilder response = new StringBuilder();
                try (BufferedReader in = new BufferedReader(new InputStreamReader(httpURLConnection.getErrorStream(), Charset.forName("utf-8")))) {
                    String inputLine;
                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
                }
                String responseErr = String.format("(HTTP Response %s : %s)", new Object[]{httpURLConnection.getResponseCode(), httpURLConnection.getResponseMessage()});
                serviceResponse.setResponseCode(ServiceResponse.FAIL);
                if (response.length() > 0) {
                    responseErr = response.toString();
                }
                String msgErr = String.format("����觢�����Ẻ POST ��ѧ Server �����ͧ�ҡ\n(%s)", responseErr);
                LOG.log(Level.SEVERE, msgErr);
                theUS.setStatus("�觢����� Online ��������", UpdateStatus.WARNING);
                serviceResponse.setResponseMessage(msgErr);
                serviceResponse.setResponseData(responseErr);
            } else {
                StringBuilder response = new StringBuilder();
                try (BufferedReader in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream(), Charset.forName("utf-8")))) {
                    String inputLine;
                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
                }
                LOG.log(Level.INFO, "Send POST request to RESTful succeeded.");
                theUS.setStatus("�觢����� Online �����", UpdateStatus.COMPLETE);
                serviceResponse.setResponseCode(ServiceResponse.SUCCESS);
                serviceResponse.setResponseMessage("Send POST request to RESTful succeeded.");
                serviceResponse.setResponseData(response);
            }
        } catch (IOException ex) {
            try {
                LOG.log(Level.SEVERE, ex.getMessage()
                        + (httpURLConnection != null ? "\n" + httpURLConnection.getResponseCode() + " : " + httpURLConnection.getResponseMessage() : ""), ex);
            } catch (IOException ex1) {
                LOG.log(Level.SEVERE, null, ex1);
            }
            theUS.setStatus("�觢����� Online �Դ��Ҵ", UpdateStatus.ERROR);
            serviceResponse.setResponseCode(ServiceResponse.FAIL);
            serviceResponse.setResponseMessage(ex.getMessage());
        } finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        }
        return serviceResponse;
    }

    public ServiceResponse getJSONByMethodGET(String path, LinkedHashMap<String, Object> params) {
        return getJSONByMethodGET(path, params, null);
    }

    public ServiceResponse getJSONByMethodGET(String path, LinkedHashMap<String, Object> params, Map<String, String> headers) {
        ServiceResponse serviceResponse = new ServiceResponse();
        java.net.URL url;
        HttpURLConnection httpURLConnection = null;
        try {
            String data = getQueryParameter(params);
            LOG.info("Creating connection to RESTful.");
            if (path.indexOf("?") > 0) {
                // remove ? from data
                data = "&" + data.substring(1);
            }
            url = new URL(path + data);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setConnectTimeout(15000); //set timeout to 15 seconds
            if (headers != null && !headers.isEmpty()) {
                for (String key : headers.keySet()) {
                    httpURLConnection.setRequestProperty(key, headers.get(key));
                }
            }
            LOG.info("Sending GET request to RESTful.");
//            LOG.info("URL:" + url.toString());
            LOG.info("Retrieved response from RESTful.");

            serviceResponse = getServiceResponse(httpURLConnection);
        } catch (java.net.SocketTimeoutException ex) {
            LOG.log(Level.SEVERE, null, ex);
            serviceResponse.setResponseCode(ServiceResponse.FAIL);
            serviceResponse.setResponseMessage(ex.getMessage());
        } catch (IOException ex) {
            try {
                LOG.log(Level.SEVERE, ex.getMessage()
                        + (httpURLConnection != null ? "\n" + httpURLConnection.getResponseCode() + " : " + httpURLConnection.getResponseMessage() : ""), ex);
            } catch (IOException ex1) {
                LOG.log(Level.SEVERE, null, ex1);
            }
            serviceResponse.setResponseCode(ServiceResponse.FAIL);
            serviceResponse.setResponseMessage(ex.getMessage());
        } finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        }
        return serviceResponse;
    }

    public ServiceResponse postJson(String path, LinkedHashMap<String, Object> data) {
        return postJson(path, data, "");
    }

    public ServiceResponse postJson(String path, LinkedHashMap<String, Object> data, String authToken) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", authToken);
        return postJson(path, data, headers);
    }

    public ServiceResponse postJson(String path, LinkedHashMap<String, Object> data, Map<String, String> headers) {
        ServiceResponse serviceResponse = new ServiceResponse();
        // send to server
        java.net.URL url;
        HttpURLConnection httpURLConnection = null;
        OutputStreamWriter writer;
        try {
            // Make json string
            LOG.info("Converting data to JSON.");
            ObjectMapper mapper = new ObjectMapper();
            mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.US) {
                @Override
                public StringBuffer format(Date date, StringBuffer toAppendTo, FieldPosition fieldPosition) {
                    return super.format(date, toAppendTo, fieldPosition).insert(26, ':');
                }
            });
            mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
            //convert map to JSON string
            String json = mapper.writeValueAsString(data);
            LOG.log(Level.INFO, "Convert data to JSON completed. {0}", json);
            url = new URL(path);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            httpURLConnection.setConnectTimeout(15000); //set timeout to 15 seconds
            if (headers != null && !headers.isEmpty()) {
                for (String key : headers.keySet()) {
                    httpURLConnection.setRequestProperty(key, headers.get(key));
                }
            }
            LOG.info("Sending JSON via RESTful.");
            writer = new OutputStreamWriter(httpURLConnection.getOutputStream(), "UTF-8");
            String result = new String(json.getBytes("UTF-8"), "UTF-8");
//            LOG.log(Level.INFO, result);
            writer.write(result);
            writer.flush();
            writer.close();

            LOG.info("Retrieved response from RESTful.");
            serviceResponse = getServiceResponse(httpURLConnection);
        } catch (java.net.SocketTimeoutException ex) {
            LOG.log(Level.SEVERE, null, ex);
            serviceResponse.setResponseCode(ServiceResponse.FAIL);
            serviceResponse.setResponseMessage(ex.getMessage());
        } catch (IOException ex) {
            try {
                LOG.log(Level.SEVERE, ex.getMessage()
                        + (httpURLConnection != null ? "\n" + httpURLConnection.getResponseCode() + " : " + httpURLConnection.getResponseMessage() : ""), ex);
            } catch (IOException ex1) {
                LOG.log(Level.SEVERE, null, ex1);
            }
            serviceResponse.setResponseCode(ServiceResponse.FAIL);
            serviceResponse.setResponseMessage(ex.getMessage());
        } finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        }
        return serviceResponse;
    }

    private ServiceResponse getServiceResponse(HttpURLConnection httpURLConnection) throws IOException {
        ServiceResponse serviceResponse = new ServiceResponse();
        if (2 != httpURLConnection.getResponseCode() / 100) {
            StringBuilder response = new StringBuilder();
            try (BufferedReader in = new BufferedReader(new InputStreamReader(httpURLConnection.getErrorStream(), Charset.forName("utf-8")))) {
                String inputLine;
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
            }
            String responseErr = String.format("(HTTP Response %s : %s)", new Object[]{httpURLConnection.getResponseCode(), httpURLConnection.getResponseMessage()});
            serviceResponse.setResponseCode(ServiceResponse.FAIL);
            if (response.length() > 0) {
                responseErr = response.toString();
            }
            LOG.log(Level.SEVERE, "Retrived response: {0}", responseErr);
            serviceResponse.setResponseMessage(responseErr);
            Object responses = getJSONArrayFromString(response.toString());
            if (responses == null) {
                responses = getJSONObjectFromString(response.toString());
            }
            serviceResponse.setResponseData(responses);
        } else {

            StringBuilder response = new StringBuilder();
            try (BufferedReader in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream(), Charset.forName("utf-8")))) {
                String inputLine;
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
            }
            String msg = String.format("(HTTP Response %s : %s)", new Object[]{httpURLConnection.getResponseCode(), httpURLConnection.getResponseMessage()});
            LOG.log(Level.INFO, "Retrived response: {0}", msg);
            serviceResponse.setResponseCode(ServiceResponse.SUCCESS);
            serviceResponse.setResponseMessage(msg);
            Object responses = getJSONArrayFromString(response.toString());
            if (responses == null) {
                responses = getJSONObjectFromString(response.toString());
            }
            serviceResponse.setResponseData(responses);
        }
        return serviceResponse;
    }
}

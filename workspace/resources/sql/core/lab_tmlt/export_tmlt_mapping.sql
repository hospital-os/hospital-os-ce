select trim(b_item.item_number) as lab_code
    ,replace(replace(replace(regexp_replace(b_item.item_common_name, E'[\r\n\t]', '', 'g'),'<','('),'>',')'),'&','and') as lab_name
    ,case when b_item_lab_set.b_item_lab_set_id is null
          then '1'
          else '2' end as lab_type
    ,case when b_item.f_item_lab_location_id is not null
          then b_item.f_item_lab_location_id
          else '1' end as location
    ,case when b_item_price.item_price is not null
          then cast(b_item_price.item_price as decimal(12,2))::text
          else '0.00' end as lab_price
    ,case when b_lab_tmlt.component is not null
          then b_lab_tmlt.component
          else '' end as component
    ,case when b_lab_tmlt.scale is not null
          then b_lab_tmlt.scale
          else '' end as scale
    ,case when b_lab_tmlt.specimen is not null
          then b_lab_tmlt.specimen
          else '' end as specimen
    ,case when b_lab_tmlt.unit is not null
          then b_lab_tmlt.unit
          else '' end as unit
    ,case when b_lab_tmlt.method is not null
          then b_lab_tmlt.method
          else '' end as method
    ,case when b_lab_tmlt.cscode is not null
          then b_lab_tmlt.cscode
          else '' end as cscode
    ,case when b_lab_tmlt.tmltcode is not null
          then b_lab_tmlt.tmltcode
          else '' end as tmlt
    ,case when b_lab_tmlt.loinc is not null
          then b_lab_tmlt.loinc
          else '' end as loinc
from b_item
    inner join b_map_lab_tmlt on b_item.b_item_id = b_map_lab_tmlt.b_item_id
    inner join b_lab_tmlt on b_map_lab_tmlt.b_lab_tmlt_tmltcode = b_lab_tmlt.tmltcode
    inner join b_item_subgroup on b_item.b_item_subgroup_id = b_item_subgroup.b_item_subgroup_id
    inner join (select b_item.b_item_id
                    ,b_item_price.item_price
                from b_item_price
                    inner join (select b_item.b_item_id
                                    ,max(b_item_price.record_datetime) as record_datetime
                                from b_item
                                    inner join b_map_lab_tmlt on b_item.b_item_id = b_map_lab_tmlt.b_item_id
                                    inner join b_item_subgroup on b_item.b_item_subgroup_id = b_item_subgroup.b_item_subgroup_id
                                    inner join b_item_price on b_item.b_item_id = b_item_price.b_item_id
                                where b_item_subgroup.f_item_group_id = '2'
                                and b_map_lab_tmlt.record_datetime::date between ':start_date'::date and ':end_date'::date
                                group by b_item.b_item_id
                                ) as max_record on b_item_price.b_item_id = max_record.b_item_id
                                               and b_item_price.record_datetime = max_record.record_datetime
                    inner join b_item on b_item.b_item_id = b_item_price.b_item_id
                    inner join b_map_lab_tmlt on b_item.b_item_id = b_map_lab_tmlt.b_item_id
                    inner join b_item_subgroup on b_item.b_item_subgroup_id = b_item_subgroup.b_item_subgroup_id
                where b_item_subgroup.f_item_group_id = '2'
                and b_map_lab_tmlt.record_datetime::date between ':start_date'::date and ':end_date'::date
            ) as b_item_price on b_item.b_item_id = b_item_price.b_item_id
    left join b_item_lab_set on b_item.b_item_id = b_item_lab_set.b_item_id
    cross join b_site
where b_item_subgroup.f_item_group_id = '2'
and b_map_lab_tmlt.record_datetime::date between ':start_date'::date and ':end_date'::date
order by b_item.item_common_name
package com.hospital_os.object;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.Persistent;

/*
 *author by pee
 *
 */
public class ICD10 extends Persistent implements CommonInf {

    public String icd10_id;
    public String description;
    public String icd10_description_th;
    public String other_description;
    public String generate_code;
    public String icd10_accident = "0";
    public String active = "1";

    @Override
    public String getCode() {
        return getObjectId();
    }

    @Override
    public String getName() {
        return icd10_id + ":" + description;
    }

    @Override
    public String toString() {
        return getName();
    }
}

package com.hosv3.control.lookup;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.LookupControlInf;
import com.hosv3.control.LookupControl;
import com.hosv3.control.SetupControl;
import com.hosv3.utility.connection.ExecuteControlInf;

/**
 *
 * @not deprecated because use henbe package and bad read data
 *
 * @author sumo
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class Icd10Lookup implements LookupControlInf, ExecuteControlInf {

    boolean is_lookup = true;
    private LookupControl theLookup;
    private SetupControl theEC;

    /**
     * Creates a new instance of VitalTemplateLookup
     */
    public Icd10Lookup(LookupControl lookup) {
        theLookup = lookup;
    }

    @Override
    public java.util.Vector listData(String str) {
        if (str == null || str.equals("") || str.equals("%")) {
            return null;
        } else {
            return theLookup.listIcd10ByIdNameGroup(str, "", false, 1);
        }
    }

    @Override
    public CommonInf readHosData(String str) {
        return theLookup.readICD10ById(str);
    }

    @Override
    public boolean execute(Object str) {
        return false;
    }
}

package com.hosv3.objdb;

import com.hospital_os.objdb.GroupIcd10DB;
import com.hospital_os.usecase.connection.ConnectionInf;

/**
 * @deprecated henbe unused used hospital_os instead
 *
 */
public class GroupIcd102DB extends GroupIcd10DB {

    public GroupIcd102DB(ConnectionInf db) {
        super(db);
    }
}

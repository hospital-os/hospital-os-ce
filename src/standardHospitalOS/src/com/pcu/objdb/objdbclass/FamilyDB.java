/*
 * FamilyDB.java
 *
 * Created on 3 �ѹ��¹ 2548, 10:59 �.
 *
 */
package com.pcu.objdb.objdbclass;

import com.hospital_os.objdb.PersonForeignerDB;
import com.hospital_os.objdb.PictureProfileDB;
import com.hospital_os.object.PictureProfile;
import com.hospital_os.usecase.connection.*;
import com.hospital_os.utility.Gutil;
import com.pcu.object.*;
import java.sql.*;
import java.util.*;

/**
 *
 * @author Jao
 */
@SuppressWarnings({"ClassWithoutLogger", "UseOfObsoleteCollectionType"})
public class FamilyDB {

    public ConnectionInf theConnectionInf;
    public Family dbObj;
    final private String idtable = "705";
    private final PersonForeignerDB personForeignerDB;
    private final PictureProfileDB pictureProfileDB;

    public FamilyDB(ConnectionInf db) {
        theConnectionInf = db;
        personForeignerDB = new PersonForeignerDB(db);
        pictureProfileDB = new PictureProfileDB(db);
        dbObj = new Family();
        initConfig();
    }

    public boolean initConfig() {
        dbObj.table = "t_health_family";
        dbObj.pk_field = "t_health_family_id";
        dbObj.home_id = "t_health_home_id";
        dbObj.family_number = "t_health_family_number";
        dbObj.pid = "patient_pid";
        dbObj.passport_no = "passport_no";
        dbObj.f_prefix_id = "f_prefix_id";
        dbObj.patient_name = "patient_name";
        dbObj.patient_last_name = "patient_last_name";
        dbObj.patient_firstname_eng = "patient_firstname_eng";
        dbObj.patient_lastname_eng = "patient_lastname_eng";
        dbObj.patient_birthday = "patient_birthday";
        dbObj.patient_birthday_true = "patient_birthday_true";
        dbObj.f_sex_id = "f_sex_id";
        dbObj.marriage_status_id = "f_patient_marriage_status_id";
        dbObj.education_type_id = "f_patient_education_type_id";
        dbObj.occupation_id = "f_patient_occupation_id";
        dbObj.nation_id = "f_patient_nation_id";
        dbObj.race_id = "f_patient_race_id";
        dbObj.religion_id = "f_patient_religion_id";
        dbObj.status_id = "f_patient_family_status_id";
        dbObj.father_firstname = "patient_father_firstname";
        dbObj.father_lastname = "patient_father_lastname";
        dbObj.father_pid = "patient_father_pid";
        dbObj.mother_firstname = "patient_mother_firstname";
        dbObj.mother_lastname = "patient_mother_lastname";
        dbObj.mother_pid = "patient_mother_pid";
        dbObj.couple_firstname = "patient_couple_firstname";
        dbObj.couple_lastname = "patient_couple_lastname";
        dbObj.couple_id = "patient_couple_id";
        dbObj.work_office = "patient_work_office";
        dbObj.blood_group_id = "f_patient_blood_group_id";
        dbObj.area_status_id = "f_patient_area_status_id";
        dbObj.record_date_time = "record_date_time";
        dbObj.modify_date_time = "modify_date_time";
        dbObj.cancel_date_time = "cancel_date_time";
        dbObj.staff_record = "health_family_staff_record";
        dbObj.staff_modify = "health_family_staff_modify";
        dbObj.staff_cancel = "health_family_staff_cancel";
        dbObj.active = "health_family_active";
        dbObj.hn_hcis = "health_family_hn_hcis";
        dbObj.discharge_status_id = "f_patient_discharge_status_id";
        dbObj.discharge_date_time = "patient_discharge_date_time";
        dbObj.move_in_date_time = "patient_move_in_date_time";
        dbObj.father_fid = "father_family_id";
        dbObj.mother_fid = "mother_family_id";
        dbObj.couple_fid = "couple_family_id";
        dbObj.rh = "health_family_rh";
        dbObj.revenue = "health_family_revenue";
        dbObj.f_person_village_status_id = "f_person_village_status_id";
        dbObj.f_person_affiliated_id = "f_person_affiliated_id";
        dbObj.f_person_rank_id = "f_person_rank_id";
        dbObj.f_person_jobtype_id = "f_person_jobtype_id";
        dbObj.skin_color = "skin_color";
        return true;
    }

    public int insert(Family p) throws Exception {
        p.generateOID(idtable);
        String sql = "insert into " + dbObj.table + " ("
                + dbObj.pk_field
                + " ," + dbObj.home_id
                + " ," + dbObj.family_number
                + " ," + dbObj.pid
                + " ," + dbObj.passport_no
                + " ," + dbObj.f_prefix_id
                + " ," + dbObj.patient_name
                + " ," + dbObj.patient_last_name
                + " ," + dbObj.patient_firstname_eng
                + " ," + dbObj.patient_lastname_eng
                + " ," + dbObj.patient_birthday
                + " ," + dbObj.patient_birthday_true
                + " ," + dbObj.f_sex_id
                + " ," + dbObj.marriage_status_id
                + " ," + dbObj.education_type_id
                + " ," + dbObj.occupation_id
                + " ," + dbObj.nation_id
                + " ," + dbObj.race_id
                + " ," + dbObj.religion_id
                + " ," + dbObj.status_id
                + " ," + dbObj.father_firstname
                + " ," + dbObj.father_lastname
                + " ," + dbObj.father_pid
                + " ," + dbObj.mother_firstname
                + " ," + dbObj.mother_lastname
                + " ," + dbObj.mother_pid
                + " ," + dbObj.couple_firstname
                + " ," + dbObj.couple_lastname
                + " ," + dbObj.couple_id
                + " ," + dbObj.work_office
                + " ," + dbObj.blood_group_id
                + " ," + dbObj.area_status_id
                + " ," + dbObj.record_date_time
                + " ," + dbObj.modify_date_time
                + " ," + dbObj.cancel_date_time
                + " ," + dbObj.staff_record
                + " ," + dbObj.staff_modify
                + " ," + dbObj.staff_cancel
                + " ," + dbObj.active
                + " ," + dbObj.hn_hcis
                + " ," + dbObj.discharge_status_id
                + " ," + dbObj.discharge_date_time
                + " ," + dbObj.move_in_date_time
                + " ," + dbObj.father_fid
                + " ," + dbObj.mother_fid
                + " ," + dbObj.couple_fid
                + " ," + dbObj.rh
                + " ," + dbObj.revenue
                + " ," + dbObj.f_person_village_status_id
                + " ," + dbObj.f_person_affiliated_id
                + " ," + dbObj.f_person_rank_id
                + " ," + dbObj.f_person_jobtype_id
                + " ," + dbObj.skin_color
                + " ) values ('"
                + p.getObjectId()
                + "','" + p.home_id
                + "','" + Gutil.CheckReservedWords(p.family_number)
                + "','" + Gutil.CheckReservedWords(p.pid)
                + "','" + Gutil.CheckReservedWords(p.passport_no)
                + "','" + p.f_prefix_id
                + "','" + Gutil.CheckReservedWords(p.patient_name)
                + "','" + Gutil.CheckReservedWords(p.patient_last_name)
                + "','" + Gutil.CheckReservedWords(p.patient_firstname_eng)
                + "','" + Gutil.CheckReservedWords(p.patient_lastname_eng)
                + "','" + p.patient_birthday
                + "','" + p.patient_birthday_true
                + "','" + p.f_sex_id
                + "','" + p.marriage_status_id
                + "','" + p.education_type_id
                + "','" + p.occupation_id
                + "','" + p.nation_id
                + "','" + p.race_id
                + "','" + p.religion_id
                + "','" + p.status_id
                + "','" + Gutil.CheckReservedWords(p.father_firstname)
                + "','" + Gutil.CheckReservedWords(p.father_lastname)
                + "','" + Gutil.CheckReservedWords(p.father_pid)
                + "','" + Gutil.CheckReservedWords(p.mother_firstname)
                + "','" + Gutil.CheckReservedWords(p.mother_lastname)
                + "','" + Gutil.CheckReservedWords(p.mother_pid)
                + "','" + Gutil.CheckReservedWords(p.couple_firstname)
                + "','" + Gutil.CheckReservedWords(p.couple_lastname)
                + "','" + Gutil.CheckReservedWords(p.couple_id)
                + "','" + p.work_office
                + "','" + p.blood_group_id
                + "','" + p.area_status_id
                + "','" + p.record_date_time
                + "','" + p.modify_date_time
                + "','" + p.cancel_date_time
                + "','" + p.staff_record
                + "','" + p.staff_modify
                + "','" + p.staff_cancel
                + "','" + p.active
                + "','" + p.hn_hcis
                + "','" + p.discharge_status_id
                + "','" + p.discharge_date_time
                + "','" + p.move_in_date_time
                + "','" + p.father_fid
                + "','" + p.mother_fid
                + "','" + p.couple_fid
                + "','" + p.rh
                + "','" + p.revenue
                + "','" + p.f_person_village_status_id
                + "','" + p.f_person_affiliated_id
                + "','" + p.f_person_rank_id
                + "','" + p.f_person_jobtype_id
                + "','" + p.skin_color
                + "')";
        int ret = theConnectionInf.eUpdate(sql);
        PictureProfile pp = new PictureProfile();
        pp.t_person_id = p.getObjectId();
        pictureProfileDB.upsert(pp);
        return ret;
    }

    public int update(Family o) throws Exception {
        String sql = "update " + dbObj.table + " set ";
        Family p = o;
        String field = ""
                + "', " + dbObj.home_id + "='" + p.home_id
                + "', " + dbObj.family_number + "='" + Gutil.CheckReservedWords(p.family_number)
                + "', " + dbObj.pid + "='" + Gutil.CheckReservedWords(p.pid)
                + "', " + dbObj.passport_no + "='" + Gutil.CheckReservedWords(p.passport_no)
                + "', " + dbObj.f_prefix_id + "='" + p.f_prefix_id
                + "', " + dbObj.patient_name + "='" + Gutil.CheckReservedWords(p.patient_name)
                + "', " + dbObj.patient_last_name + "='" + Gutil.CheckReservedWords(p.patient_last_name)
                + "', " + dbObj.patient_firstname_eng + "='" + Gutil.CheckReservedWords(p.patient_firstname_eng)
                + "', " + dbObj.patient_lastname_eng + "='" + Gutil.CheckReservedWords(p.patient_lastname_eng)
                + "', " + dbObj.patient_birthday + "='" + p.patient_birthday
                + "', " + dbObj.patient_birthday_true + "='" + p.patient_birthday_true
                + "', " + dbObj.f_sex_id + "='" + p.f_sex_id
                + "', " + dbObj.marriage_status_id + "='" + p.marriage_status_id
                + "', " + dbObj.education_type_id + "='" + p.education_type_id
                + "', " + dbObj.occupation_id + "='" + p.occupation_id
                + "', " + dbObj.nation_id + "='" + p.nation_id
                + "', " + dbObj.race_id + "='" + p.race_id
                + "', " + dbObj.religion_id + "='" + p.religion_id
                + "', " + dbObj.status_id + "='" + p.status_id
                + "', " + dbObj.father_firstname + "='" + Gutil.CheckReservedWords(p.father_firstname)
                + "', " + dbObj.father_lastname + "='" + Gutil.CheckReservedWords(p.father_lastname)
                + "', " + dbObj.father_pid + "='" + Gutil.CheckReservedWords(p.father_pid)
                + "', " + dbObj.mother_firstname + "='" + Gutil.CheckReservedWords(p.mother_firstname)
                + "', " + dbObj.mother_lastname + "='" + Gutil.CheckReservedWords(p.mother_lastname)
                + "', " + dbObj.mother_pid + "='" + Gutil.CheckReservedWords(p.mother_pid)
                + "', " + dbObj.couple_firstname + "='" + Gutil.CheckReservedWords(p.couple_firstname)
                + "', " + dbObj.couple_lastname + "='" + Gutil.CheckReservedWords(p.couple_lastname)
                + "', " + dbObj.couple_id + "='" + Gutil.CheckReservedWords(p.couple_id)
                + "', " + dbObj.work_office + "='" + p.work_office
                + "', " + dbObj.blood_group_id + "='" + p.blood_group_id
                + "', " + dbObj.area_status_id + "='" + p.area_status_id
                + "', " + dbObj.record_date_time + "='" + p.record_date_time
                + "', " + dbObj.modify_date_time + "='" + p.modify_date_time
                + "', " + dbObj.cancel_date_time + "='" + p.cancel_date_time
                + "', " + dbObj.staff_record + "='" + p.staff_record
                + "', " + dbObj.staff_modify + "='" + p.staff_modify
                + "', " + dbObj.staff_cancel + "='" + p.staff_cancel
                + "', " + dbObj.active + "='" + p.active
                + "', " + dbObj.hn_hcis + "='" + p.hn_hcis
                + "', " + dbObj.discharge_status_id + "='" + p.discharge_status_id
                + "', " + dbObj.discharge_date_time + "='" + p.discharge_date_time
                + "', " + dbObj.move_in_date_time + "='" + p.move_in_date_time
                + "', " + dbObj.father_fid + "='" + p.father_fid
                + "', " + dbObj.mother_fid + "='" + p.mother_fid
                + "', " + dbObj.couple_fid + "='" + p.couple_fid
                + "', " + dbObj.rh + "='" + p.rh
                + "', " + dbObj.revenue + "='" + p.revenue
                + "', " + dbObj.f_person_village_status_id + "='" + p.f_person_village_status_id
                + "', " + dbObj.f_person_affiliated_id + "='" + p.f_person_affiliated_id
                + "', " + dbObj.f_person_rank_id + "='" + p.f_person_rank_id
                + "', " + dbObj.f_person_jobtype_id + "='" + p.f_person_jobtype_id
                + "', " + dbObj.skin_color + "='" + p.skin_color
                + "' where t_health_family_id='" + p.getObjectId() + "'";
        sql += field.substring(2);
        return theConnectionInf.eUpdate(sql);
    }

    public int updatePID(Family o) throws Exception {
        String sql = "UPDATE t_health_family\n"
                + "   SET patient_pid=?\n"
                + " WHERE t_health_family_id=?";
        PreparedStatement ePQuery = null;
        try {
            ePQuery = theConnectionInf.ePQuery(sql);
            int index = 1;
            ePQuery.setString(index++, o.pid.trim());
            ePQuery.setString(index++, o.getObjectId());
            return ePQuery.executeUpdate();
        } finally {
            if (ePQuery != null) {
                ePQuery.close();
            }
        }
    }

    public int delete(Family o) throws Exception {
        String sql = "delete from " + dbObj.table
                + " where t_health_family_id='" + o.getObjectId() + "'";
        return theConnectionInf.eUpdate(sql);
    }

    public Vector selectAllFamily() throws Exception {
        String sql = "select t_health_family.*, t_patient.patient_hn from t_health_family "
                + " left join t_patient on t_patient.t_health_family_id = t_health_family.t_health_family_id "
                + " where t_health_family.patient_pid <> '" + "'" + "  and t_health_family.health_family_active = '1'";

        Vector v = eQuery(sql);
        if (v.isEmpty()) {
            return null;
        } else {
            return v;
        }
    }

    /*
     *@author henbe pongtorn
     *@name �鹼����µ������ ʡ����к�ҹ
     */
    public Vector selectByFnameLnameHome(String fname, String lname, String home_id) throws Exception {
        String sql = "select t_health_family.*, t_patient.patient_hn from t_health_family "
                + " left join t_patient on t_patient.t_health_family_id = t_health_family.t_health_family_id "
                + " where " + "t_health_family.t_health_home_id = '" + home_id + "' "
                + " and t_health_family.patient_name = '" + Gutil.CheckReservedWords(fname) + "' "
                + " and t_health_family.patient_last_name = '" + Gutil.CheckReservedWords(lname) + "' "
                + " and t_health_family.health_family_active ='1' ";
        return eQuery(sql);
    }

    public Family selectOwnerByHomeId(String home_id) throws Exception {
        String sql = "select t_health_family.*, t_patient.patient_hn from t_health_family "
                + " left join t_patient on t_patient.t_health_family_id = t_health_family.t_health_family_id "
                + " where t_health_family.t_health_home_id"
                + " = '" + home_id + "' "
                + "and t_health_family.health_family_active ='1'"
                + "and t_health_family.f_patient_family_status_id ='1'"
                + "  order by t_health_family.patient_name";
        Vector eQuery = eQuery(sql);
        return (Family) (eQuery.isEmpty() ? null : eQuery.get(0));
    }

    public Vector selectFamilyByHomeId(String home_id, int limit) throws Exception {
        String sql = "select t_health_family.*, t_patient.patient_hn from t_health_family "
                + " left join t_patient on t_patient.t_health_family_id = t_health_family.t_health_family_id "
                + " where t_health_family.t_health_home_id"
                + " = '" + home_id + "' and t_health_family.health_family_active ='1'"
                + "  order by t_health_family.patient_name"
                + " limit " + limit;
        return eQuery(sql);
    }

    public Vector selectFamilyByHomeId(String home_id) throws Exception {
        return selectFamilyByHomeId(home_id, 100);
    }

    public Vector selectByOtherHn(String number) throws Exception {
        String sql = "select t_health_family.*, t_patient.patient_hn from t_health_family "
                + " left join t_patient on t_patient.t_health_family_id = t_health_family.t_health_family_id "
                + " where t_health_family.health_family_hn_hcis"
                + " like '%" + number + "'"
                + " and t_health_family.health_family_active ='1' ";

        Vector v = eQuery(sql);
        if (v.isEmpty()) {
            return null;
        } else {
            return v;
        }
    }

    public Vector setlectFamilyByName(String name, String lastname) throws Exception {
        String sql = "select t_health_family.*, t_patient.patient_hn from t_health_family "
                + " left join t_patient on t_patient.t_health_family_id = t_health_family.t_health_family_id "
                + " where t_health_family.patient_name like '%" + Gutil.CheckReservedWords(name) + "%'"
                + " and t_health_family.patient_last_name like '%" + Gutil.CheckReservedWords(lastname) + "%'";
        Vector v = this.eQuery(sql);
        return v;
    }

    public Vector selectFamilyByPId(String pid) throws Exception {
        String sql = "select t_health_family.*, t_patient.patient_hn from t_health_family "
                + " left join t_patient on t_patient.t_health_family_id = t_health_family.t_health_family_id "
                + " where t_health_family.patient_pid = '" + pid + "' and t_health_family.health_family_active ='1' Order by t_health_family.record_date_time";
        return eQuery(sql);
    }

    public Family selectFamilyByHcis(String pid) throws Exception {
        String sql = "select t_health_family.*, t_patient.patient_hn from t_health_family "
                + " left join t_patient on t_patient.t_health_family_id = t_health_family.t_health_family_id "
                + " where t_health_family.health_family_hn_hcis"
                + " = '" + pid + "' and t_health_family.health_family_active ='1' Order by t_health_family.record_date_time";
        Vector eQuery = eQuery(sql);
        return (Family) (eQuery.isEmpty() ? null : eQuery.get(0));
    }

    public Vector selectLikeHcis(String pid) throws Exception {
        String sql = "select t_health_family.*, t_patient.patient_hn from t_health_family "
                + " left join t_patient on t_patient.t_health_family_id = t_health_family.t_health_family_id "
                + " where t_health_family.health_family_hn_hcis"
                + " like '" + pid + "' and t_health_family.health_family_active ='1' Order by t_health_family.record_date_time";
        return eQuery(sql);
    }

    public Vector queryByFLName(String fname, String lname) throws Exception {
        String sql = "select t_health_family.*, t_patient.patient_hn from t_health_family "
                + " left join t_patient on t_patient.t_health_family_id = t_health_family.t_health_family_id "
                + " where "
                + "t_health_family.patient_name like '" + Gutil.CheckReservedWords(fname) + "' and "
                + "t_health_family.patient_last_name like '" + Gutil.CheckReservedWords(lname) + "' and "
                + "t_health_family.health_family_active ='1'";
        return eQuery(sql);
    }

    public Vector queryByFLName(String fname, String lname, String sexId) throws Exception {
        String sql = "select t_health_family.*, t_patient.patient_hn from t_health_family"
                + " left join t_patient on t_patient.t_health_family_id = t_health_family.t_health_family_id "
                + " where t_health_family.health_family_active ='1' \n";
        if (fname != null && !fname.isEmpty()) {
            sql += "and t_health_family.patient_name like '" + Gutil.CheckReservedWords(fname) + "' \n ";
        }
        if (lname != null && !lname.isEmpty()) {
            sql += "and t_health_family.patient_last_name like '" + Gutil.CheckReservedWords(lname) + "' \n ";
        }
        if (sexId != null && !sexId.isEmpty()) {
            sql += "t_health_family.f_sex_id = '" + sexId + "'";
        }
        return eQuery(sql);
    }

    public Vector queryByFName(String fname) throws Exception {
        String sql = "select t_health_family.*, t_patient.patient_hn from t_health_family "
                + " left join t_patient on t_patient.t_health_family_id = t_health_family.t_health_family_id "
                + " where t_health_family.patient_name like '" + Gutil.CheckReservedWords(fname) + "' and "
                + "t_health_family.health_family_active = '1' "
                + "order by t_health_family.patient_name";

        return eQuery(sql);
    }

    public Vector queryBySName(String lname) throws Exception {
        String sql = "select t_health_family.*, t_patient.patient_hn from t_health_family "
                + " left join t_patient on t_patient.t_health_family_id = t_health_family.t_health_family_id "
                + " where "
                + " t_health_family.patient_last_name like '" + Gutil.CheckReservedWords(lname) + "' and "
                + " t_health_family.health_family_active = '1' ";

        return eQuery(sql);
    }

    /*
     *@Author : henbe pongtorn
     *@date : 31/03/2549
     *@see : �鹢����Ũҡ PrimaryKey �ͧ�ѹ
     */
    public Family selectByPK(String family_id) throws Exception {
        if (family_id == null || family_id.length() == 0 || family_id.equals("null")) {
            return null;
        }
        String sql = "select t_health_family.*, t_patient.patient_hn from t_health_family "
                + " left join t_patient on t_patient.t_health_family_id = t_health_family.t_health_family_id "
                + " where t_health_family.t_health_family_id = '" + family_id + "'";
        Vector v = eQuery(sql);
        if (v.isEmpty()) {
            return null;
        } else {
            return (Family) v.get(0);
        }
    }

    /*
     *@Author : henbe pongtorn
     *@date : 18/03/2549
     *@see : �鹻�Ъҡèҡ�Ţ�ѵû�ЪҪ�
     *@deprecated �ջѭ����Ҷ�Ҥ��������¤��з����Դ��Ҵ��
     */
    public Family selectByPid(String pid) throws Exception {
        String sql = "select t_health_family.*, t_patient.patient_hn from t_health_family "
                + " left join t_patient on t_patient.t_health_family_id = t_health_family.t_health_family_id "
                + " where t_health_family.patient_pid = '" + Gutil.CheckReservedWords(pid)
                + "' and t_health_family.health_family_active ='1'";
        Vector v = eQuery(sql);
        if (v.isEmpty()) {
            return null;
        } else {
            return (Family) v.get(0);
        }
    }

    public Vector selectByPid(String pid, String active) throws Exception {
        String sql = "select t_health_family.*, t_patient.patient_hn from t_health_family "
                + " left join t_patient on t_patient.t_health_family_id = t_health_family.t_health_family_id "
                + " where t_health_family.patient_pid = '" + Gutil.CheckReservedWords(pid)
                + "' and t_health_family.health_family_active ='" + active + "'";
        return eQuery(sql);
    }

    /**
     * ���Ҽ����·���ѧ�����ŧ�Է����Шӵ��
     *
     * @param offset = ��������鹷���ͧ�������ʴ� limit =
     * �ӹǹ�Ƿ���ͧ����ʴ�
     * @param limit
     * @param name
     * @param lname
     * @return Vector �ͧ Family
     * @throws Exception
     * @author kingland
     * @date 29/05/2549
     */
    public Vector selectFamilyNotHaveFamilyPayment(String offset, String limit, String name, String lname) throws Exception {
        String sql = "select t_health_family.*, t_patient.patient_hn from t_health_family "
                + " left join t_patient on t_patient.t_health_family_id = t_health_family.t_health_family_id "
                + " where t_health_family.t_health_family_id not in "
                + "( select t_health_family_id from t_patient_payment "
                + "where t_health_family_id <> '' "
                + "and t_health_family_id is not null "
                + "group by t_health_family_id) ";

        if (name != null && !"".equals(name)) {
            sql = sql + " and t_health_family.patient_name like '" + Gutil.CheckReservedWords(name) + "%'";
        }
        if (lname != null && !"".equals(lname)) {
            sql = sql + " and t_health_family.patient_last_name like '" + Gutil.CheckReservedWords(lname) + "%'";
        }
        if (!"".equals(offset) && !"".equals(limit)) {
            sql = sql + " offset " + offset + " limit " + limit;
        }
        Vector v = eQuery(sql);
        return v;
    }

    public int updatePidByFid(String patient_pid, String family_id) throws Exception {
        String sql = "update t_health_family"
                + " set patient_pid='" + patient_pid + "'"
                + " where t_health_family_id='" + family_id + "'";
        return theConnectionInf.eUpdate(sql);
    }

    public int updateDischarge(Family family) throws Exception {
        String sql = "update t_health_family set "
                + dbObj.discharge_status_id + " = '" + family.discharge_status_id + "'"
                + "," + dbObj.discharge_date_time + " = '" + family.discharge_date_time + "'"
                + " where t_health_family_id='" + family.getObjectId() + "'";
        return theConnectionInf.eUpdate(sql);
    }

    public int updateChildToNewFatherIdByFatherId(Family des, Family src) throws Exception {
        String sql = "UPDATE t_health_family SET father_family_id = ?\n"
                + ", patient_father_firstname = ?\n"
                + ", patient_father_lastname = ?\n"
                + ", patient_father_pid = ?\n"
                + "WHERE father_family_id = ?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, des.getObjectId());
            ePQuery.setString(index++, des.patient_name);
            ePQuery.setString(index++, des.patient_last_name);
            ePQuery.setString(index++, des.pid);
            ePQuery.setString(index++, src.getObjectId());
            return ePQuery.executeUpdate();
        }
    }

    public int updateChildToNewMotherIdByMotherId(Family des, Family src) throws Exception {
        String sql = "UPDATE t_health_family SET mother_family_id = ?\n"
                + ", patient_mother_firstname = ?\n"
                + ", patient_mother_lastname = ?\n"
                + ", patient_mother_pid = ?\n"
                + "WHERE mother_family_id = ?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, des.getObjectId());
            ePQuery.setString(index++, des.patient_name);
            ePQuery.setString(index++, des.patient_last_name);
            ePQuery.setString(index++, des.pid);
            ePQuery.setString(index++, src.getObjectId());
            return ePQuery.executeUpdate();
        }
    }

    public int updateCoupleToNewCoupleIdByCoupleId(Family des, Family src) throws Exception {
        String sql = "UPDATE t_health_family SET couple_family_id = ?\n"
                + ", patient_couple_firstname = ?\n"
                + ", patient_couple_lastname = ?\n"
                + ", patient_couple_id = ?\n"
                + "WHERE couple_family_id = ?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, des.getObjectId());
            ePQuery.setString(index++, des.patient_name);
            ePQuery.setString(index++, des.patient_last_name);
            ePQuery.setString(index++, des.pid);
            ePQuery.setString(index++, src.getObjectId());
            return ePQuery.executeUpdate();
        }
    }

    /**
     *
     * family.work_office = " ���»���ѵ���Ţ�ѵ� " + des_family_id; }
     * family.staff_cancel = theHO.theEmployee.getObjectId();
     * family.cancel_date_time = theHO.date_time; family.active = "0";
     *
     * @param family
     * @return
     * @throws Exception
     */
    public int updateActive(Family family) throws Exception {
        String sql = "update " + dbObj.table + " set "
                + dbObj.staff_cancel + " = '" + family.staff_cancel + "'"
                + "," + dbObj.cancel_date_time + " = '" + family.cancel_date_time + "'"
                + ",health_family_active = '" + family.active + "'"
                + " where t_health_family_id='" + family.getObjectId() + "'";
        return theConnectionInf.eUpdate(sql);
    }

    public Vector selectByHomeId(String home_id, String status_id) throws Exception {
        String sql = "select t_health_family.*, t_patient.patient_hn from t_health_family "
                + " left join t_patient on t_patient.t_health_family_id = t_health_family.t_health_family_id "
                + " where "
                + "t_health_family.t_health_home_id = '" + home_id + "'" + "  and "
                + "t_health_family.f_patient_family_status_id = '" + status_id + "'" + "  and "
                + "t_health_family.health_family_active = '1'";
        return eQuery(sql);
    }

    public String selectMaxHCIS() throws Exception {
        ResultSet rs = theConnectionInf.eQuery("select max(health_family_hn_hcis) as max_hcis from t_health_family");
        String max = "0";
        if (rs.next()) {
            max = rs.getString("max_hcis");
        }
        rs.close();
        return max;
    }

    public Vector queryByCID(String cid) throws Exception {
        String sql = "select t_health_family.*, t_patient.patient_hn from t_health_family"
                + " left join t_patient on t_patient.t_health_family_id = t_health_family.t_health_family_id "
                + " where "
                + "t_health_family.patient_pid = '" + cid + "' and "
                + "t_health_family.health_family_active ='1'";
        return eQuery(sql);
    }

    public Vector queryBySexId(String sexId) throws Exception {
        String sql = "select t_health_family.*, t_patient.patient_hn from t_health_family"
                + " left join t_patient on t_patient.t_health_family_id = t_health_family.t_health_family_id "
                + " where "
                + "t_health_family.f_sex_id = '" + sexId + "' and "
                + "t_health_family.health_family_active ='1'";
        return eQuery(sql);
    }

    public Vector queryByIntervalDOB(String dob1, String dob2) throws Exception {
        String sql = "select t_health_family.*, t_patient.patient_hn from t_health_family"
                + " left join t_patient on t_patient.t_health_family_id = t_health_family.t_health_family_id "
                + " where "
                + " t_health_family.patient_birthday_true = '1' and t_health_family.patient_birthday >= '" + dob1 + "' and t_health_family.patient_birthday <= '" + dob2 + "' and ";
        sql += "t_health_family.health_family_active ='1'";
        return eQuery(sql);
    }

    public Vector queryBySexIdAndIntervalDOB(String sexId, String dob1, String dob2) throws Exception {
        String sql = "select t_health_family.*, t_patient.patient_hn from t_health_family"
                + " left join t_patient on t_patient.t_health_family_id = t_health_family.t_health_family_id "
                + " where "
                + " t_health_family.f_sex_id = '" + sexId + "' and "
                + " t_health_family.patient_birthday_true = '1' and t_health_family.patient_birthday >= '" + dob1 + "' and t_health_family.patient_birthday <= '" + dob2 + "' and ";
        sql += "t_health_family.health_family_active ='1'";
        return eQuery(sql);
    }

    public List<Family> listByKeyword(String keyword) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select t_health_family.*, t_patient.patient_hn from t_health_family "
                    + " left join t_patient on t_patient.t_health_family_id = t_health_family.t_health_family_id "
                    + " where (upper(patient_name) like upper(?) "
                    + "or upper(patient_last_name) like upper(?) "
                    + "or upper(health_family_hn_hcis) like upper(?) "
                    + "or upper(t_health_family.patient_pid) like upper(?) ) and health_family_active = '1'";
            preparedStatement = theConnectionInf.ePQuery(sql);
            preparedStatement.setString(1, "%" + keyword + "%");
            preparedStatement.setString(2, "%" + keyword + "%");
            preparedStatement.setString(3, "%" + keyword + "%");
            preparedStatement.setString(4, "%" + keyword + "%");
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public Vector eQuery(String sql) throws Exception {
        Family p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            p = new Family();
            p.setObjectId(rs.getString(dbObj.pk_field));
            p.home_id = rs.getString(dbObj.home_id);
            p.family_number = rs.getString(dbObj.family_number);
            p.pid = rs.getString(dbObj.pid);
            p.passport_no = rs.getString(dbObj.passport_no);
            p.f_prefix_id = rs.getString(dbObj.f_prefix_id);
            p.patient_name = rs.getString(dbObj.patient_name);
            p.patient_last_name = rs.getString(dbObj.patient_last_name);
            p.patient_firstname_eng = rs.getString(dbObj.patient_firstname_eng);
            p.patient_lastname_eng = rs.getString(dbObj.patient_lastname_eng);
            p.patient_birthday = rs.getString(dbObj.patient_birthday);
            p.patient_birthday_true = rs.getString(dbObj.patient_birthday_true);
            p.f_sex_id = rs.getString(dbObj.f_sex_id);
            p.marriage_status_id = rs.getString(dbObj.marriage_status_id);
            p.education_type_id = rs.getString(dbObj.education_type_id);
            p.occupation_id = rs.getString(dbObj.occupation_id);
            p.nation_id = rs.getString(dbObj.nation_id);
            p.race_id = rs.getString(dbObj.race_id);
            p.religion_id = rs.getString(dbObj.religion_id);
            p.status_id = rs.getString(dbObj.status_id);
            p.father_firstname = rs.getString(dbObj.father_firstname);
            p.father_lastname = rs.getString(dbObj.father_lastname);
            p.father_pid = rs.getString(dbObj.father_pid);
            p.mother_firstname = rs.getString(dbObj.mother_firstname);
            p.mother_lastname = rs.getString(dbObj.mother_lastname);
            p.mother_pid = rs.getString(dbObj.mother_pid);
            p.couple_firstname = rs.getString(dbObj.couple_firstname);
            p.couple_lastname = rs.getString(dbObj.couple_lastname);
            p.couple_id = rs.getString(dbObj.couple_id);
            p.work_office = rs.getString(dbObj.work_office);
            p.blood_group_id = rs.getString(dbObj.blood_group_id);
            p.area_status_id = rs.getString(dbObj.area_status_id);
            p.record_date_time = rs.getString(dbObj.record_date_time);
            p.modify_date_time = rs.getString(dbObj.modify_date_time);
            p.cancel_date_time = rs.getString(dbObj.cancel_date_time);
            p.staff_record = rs.getString(dbObj.staff_record);
            p.staff_modify = rs.getString(dbObj.staff_modify);
            p.staff_cancel = rs.getString(dbObj.staff_cancel);
            p.active = rs.getString(dbObj.active);
            p.hn_hcis = rs.getString(dbObj.hn_hcis);
            p.discharge_status_id = rs.getString(dbObj.discharge_status_id);
            p.discharge_date_time = rs.getString(dbObj.discharge_date_time);
            p.move_in_date_time = rs.getString(dbObj.move_in_date_time);
            p.father_fid = rs.getString(dbObj.father_fid);
            p.mother_fid = rs.getString(dbObj.mother_fid);
            p.couple_fid = rs.getString(dbObj.couple_fid);
            p.rh = rs.getString(dbObj.rh);
            p.revenue = rs.getString(dbObj.revenue);
            p.f_person_village_status_id = rs.getString(dbObj.f_person_village_status_id);
            p.f_person_affiliated_id = rs.getString(dbObj.f_person_affiliated_id);
            p.f_person_rank_id = rs.getString(dbObj.f_person_rank_id);
            p.f_person_jobtype_id = rs.getString(dbObj.f_person_jobtype_id);
            p.skin_color = rs.getString(dbObj.skin_color);
            try {
                p.hn = rs.getString("patient_hn");
            } catch (Exception ex) {
                p.hn = "";
            }
            if (!p.nation_id.equals("99")) {
                p.personForeigner = personForeignerDB.selectByPersonId(p.getObjectId());
            }
            try {
                p.pictureProfile = pictureProfileDB.selectByPersonId(p.getObjectId());
            } catch (Exception e) {
            }
            list.add(p);
        }
        rs.close();
        return list;
    }

    public List<Family> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<Family> list = new ArrayList<Family>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Family p = new Family();
                p.setObjectId(rs.getString(dbObj.pk_field));
                p.home_id = rs.getString(dbObj.home_id);
                p.family_number = rs.getString(dbObj.family_number);
                p.pid = rs.getString(dbObj.pid);
                p.passport_no = rs.getString(dbObj.passport_no);
                p.f_prefix_id = rs.getString(dbObj.f_prefix_id);
                p.patient_name = rs.getString(dbObj.patient_name);
                p.patient_last_name = rs.getString(dbObj.patient_last_name);
                p.patient_firstname_eng = rs.getString(dbObj.patient_firstname_eng);
                p.patient_lastname_eng = rs.getString(dbObj.patient_lastname_eng);
                p.patient_birthday = rs.getString(dbObj.patient_birthday);
                p.patient_birthday_true = rs.getString(dbObj.patient_birthday_true);
                p.f_sex_id = rs.getString(dbObj.f_sex_id);
                p.marriage_status_id = rs.getString(dbObj.marriage_status_id);
                p.education_type_id = rs.getString(dbObj.education_type_id);
                p.occupation_id = rs.getString(dbObj.occupation_id);
                p.nation_id = rs.getString(dbObj.nation_id);
                p.race_id = rs.getString(dbObj.race_id);
                p.religion_id = rs.getString(dbObj.religion_id);
                p.status_id = rs.getString(dbObj.status_id);
                p.father_firstname = rs.getString(dbObj.father_firstname);
                p.father_lastname = rs.getString(dbObj.father_lastname);
                p.father_pid = rs.getString(dbObj.father_pid);
                p.mother_firstname = rs.getString(dbObj.mother_firstname);
                p.mother_lastname = rs.getString(dbObj.mother_lastname);
                p.mother_pid = rs.getString(dbObj.mother_pid);
                p.couple_firstname = rs.getString(dbObj.couple_firstname);
                p.couple_lastname = rs.getString(dbObj.couple_lastname);
                p.couple_id = rs.getString(dbObj.couple_id);
                p.work_office = rs.getString(dbObj.work_office);
                p.blood_group_id = rs.getString(dbObj.blood_group_id);
                p.area_status_id = rs.getString(dbObj.area_status_id);
                p.record_date_time = rs.getString(dbObj.record_date_time);
                p.modify_date_time = rs.getString(dbObj.modify_date_time);
                p.cancel_date_time = rs.getString(dbObj.cancel_date_time);
                p.staff_record = rs.getString(dbObj.staff_record);
                p.staff_modify = rs.getString(dbObj.staff_modify);
                p.staff_cancel = rs.getString(dbObj.staff_cancel);
                p.active = rs.getString(dbObj.active);
                p.hn_hcis = rs.getString(dbObj.hn_hcis);
                p.discharge_status_id = rs.getString(dbObj.discharge_status_id);
                p.discharge_date_time = rs.getString(dbObj.discharge_date_time);
                p.move_in_date_time = rs.getString(dbObj.move_in_date_time);
                p.father_fid = rs.getString(dbObj.father_fid);
                p.mother_fid = rs.getString(dbObj.mother_fid);
                p.couple_fid = rs.getString(dbObj.couple_fid);
                p.rh = rs.getString(dbObj.rh);
                p.revenue = rs.getString(dbObj.revenue);
                p.f_person_village_status_id = rs.getString(dbObj.f_person_village_status_id);
                p.f_person_affiliated_id = rs.getString(dbObj.f_person_affiliated_id);
                p.f_person_rank_id = rs.getString(dbObj.f_person_rank_id);
                p.f_person_jobtype_id = rs.getString(dbObj.f_person_jobtype_id);
                p.skin_color = rs.getString(dbObj.skin_color);
                try {
                    p.hn = rs.getString("patient_hn");
                } catch (Exception ex) {
                    p.hn = "";
                }
                if (!p.nation_id.equals("99")) {
                    p.personForeigner = personForeignerDB.selectByPersonId(p.getObjectId());
                }
                try {
                    p.pictureProfile = pictureProfileDB.selectByPersonId(p.getObjectId());
                } catch (Exception e) {
                }
                list.add(p);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }
}

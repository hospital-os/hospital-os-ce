/*
 * PanelSetupOffice.java
 *
 * Created on 4 ���Ҥ� 2546, 11:07 �.
 */
package com.hosv3.gui.panel.setup;

import com.hospital_os.usecase.connection.UpdateStatus;
import com.hospital_os.utility.IPAddressValidator;
import com.hospital_os.utility.TaBleModel;
import com.hosv3.control.*;
import com.hosv3.gui.dialog.HosDialog;
import com.hosv3.object.ReceiptSequance;
import java.awt.Color;
import java.util.Vector;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 *
 * @author LionHeart
 */
@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public class PanelSetupRecieptSequance extends javax.swing.JPanel implements DocumentListener {

    private static final long serialVersionUID = 1L;
    UpdateStatus theUS;
    HosDialog theHD;
    HosControl theHC;
    ReceiptSequance theReceiptSequance;
    Vector theReceiptSequanceV;
    String cols[] = {"Name"};
    SetupControl theSetupControl;

    @SuppressWarnings("LeakingThisInConstructor")
    public PanelSetupRecieptSequance() {
        initComponents();
        jLabel4.setText("Receipt Sequence");
        jTextFieldIP.getDocument().addDocumentListener(this);
        jTextFieldPattern.getDocument().addDocumentListener(this);
        validateUI();
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        tableResultsModel1 = new com.hospital_os.utility.TableResultsModel();
        jPanel2 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jTextFieldPattern = new javax.swing.JTextField();
        jTextFieldIP = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jTextFieldNextValue = new sd.comp.jcalendar.JSpinField();
        jTextFieldBookNo = new sd.comp.jcalendar.JSpinField();
        jTextFieldBeginNo = new sd.comp.jcalendar.JSpinField();
        jTextFieldEndNo = new sd.comp.jcalendar.JSpinField();
        jTextFieldReceiptQty = new sd.comp.jcalendar.JSpinField();
        jPanel3 = new javax.swing.JPanel();
        jLabelICD9code = new javax.swing.JLabel();
        jTextFieldSCode = new javax.swing.JTextField();
        btnSearch = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new com.hosv3.gui.component.HJTableSort();
        jPanel4 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jButtonAdd = new javax.swing.JButton();
        btnDel = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();

        setLayout(new java.awt.GridBagLayout());

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel2.setMaximumSize(new java.awt.Dimension(350, 330));
        jPanel2.setMinimumSize(new java.awt.Dimension(350, 330));
        jPanel2.setPreferredSize(new java.awt.Dimension(350, 330));
        jPanel2.setLayout(new java.awt.GridBagLayout());

        jLabel8.setFont(jLabel8.getFont());
        jLabel8.setText("�ٻẺ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel2.add(jLabel8, gridBagConstraints);

        jLabel9.setFont(jLabel9.getFont());
        jLabel9.setText("IP");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel2.add(jLabel9, gridBagConstraints);

        jLabel10.setFont(jLabel10.getFont());
        jLabel10.setText("�Ţ��������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(8, 5, 5, 5);
        jPanel2.add(jLabel10, gridBagConstraints);

        jTextFieldPattern.setFont(jTextFieldPattern.getFont());
        jTextFieldPattern.setDoubleBuffered(true);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel2.add(jTextFieldPattern, gridBagConstraints);

        jTextFieldIP.setFont(jTextFieldIP.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel2.add(jTextFieldIP, gridBagConstraints);

        jLabel7.setFont(jLabel7.getFont());
        jLabel7.setText("XX.0000=XX0001, 0yy0000 = 0490001");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 2, 5);
        jPanel2.add(jLabel7, gridBagConstraints);

        jLabel12.setFont(jLabel12.getFont());
        jLabel12.setText("�ӹǹ�����(�)");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(8, 5, 0, 5);
        jPanel2.add(jLabel12, gridBagConstraints);

        jLabel11.setFont(jLabel11.getFont());
        jLabel11.setText("�Ţ�������ش");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 0, 0);
        jPanel2.add(jLabel11, gridBagConstraints);

        jLabel6.setFont(jLabel6.getFont());
        jLabel6.setText("�Ţ����������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 0, 0);
        jPanel2.add(jLabel6, gridBagConstraints);

        jLabel5.setFont(jLabel5.getFont());
        jLabel5.setText("�������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 0, 0);
        jPanel2.add(jLabel5, gridBagConstraints);

        jLabel3.setFont(jLabel3.getFont());
        jLabel3.setText("�������Ţ�������稨ҡ�ç�����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel2.add(jLabel3, gridBagConstraints);

        jTextFieldNextValue.setMinimum(1);
        jTextFieldNextValue.setValue(1);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel2.add(jTextFieldNextValue, gridBagConstraints);

        jTextFieldBookNo.setMinimum(1);
        jTextFieldBookNo.setValue(1);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel2.add(jTextFieldBookNo, gridBagConstraints);

        jTextFieldBeginNo.setMinimum(1);
        jTextFieldBeginNo.setValue(1);
        jTextFieldBeginNo.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jTextFieldBeginNoPropertyChange(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel2.add(jTextFieldBeginNo, gridBagConstraints);

        jTextFieldEndNo.setMinimum(1);
        jTextFieldEndNo.setValue(1);
        jTextFieldEndNo.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jTextFieldEndNoPropertyChange(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel2.add(jTextFieldEndNo, gridBagConstraints);

        jTextFieldReceiptQty.setMinimum(1);
        jTextFieldReceiptQty.setValue(1);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel2.add(jTextFieldReceiptQty, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 5, 5);
        add(jPanel2, gridBagConstraints);

        jPanel3.setMinimumSize(new java.awt.Dimension(300, 96));
        jPanel3.setPreferredSize(new java.awt.Dimension(300, 154));
        jPanel3.setLayout(new java.awt.GridBagLayout());

        jLabelICD9code.setFont(jLabelICD9code.getFont());
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("com/hosv3/property/thai"); // NOI18N
        jLabelICD9code.setText(bundle.getString("SEARCH")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 11);
        jPanel3.add(jLabelICD9code, gridBagConstraints);

        jTextFieldSCode.setFont(jTextFieldSCode.getFont());
        jTextFieldSCode.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextFieldSCodeKeyPressed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 5, 5);
        jPanel3.add(jTextFieldSCode, gridBagConstraints);

        btnSearch.setFont(btnSearch.getFont());
        btnSearch.setText(bundle.getString("SEARCH")); // NOI18N
        btnSearch.setMaximumSize(new java.awt.Dimension(67, 25));
        btnSearch.setMinimumSize(new java.awt.Dimension(67, 25));
        btnSearch.setPreferredSize(new java.awt.Dimension(67, 25));
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 5, 5);
        jPanel3.add(btnSearch, gridBagConstraints);

        jScrollPane1.setMinimumSize(new java.awt.Dimension(100, 22));
        jScrollPane1.setPreferredSize(new java.awt.Dimension(100, 80));

        jTable1.setModel(tableResultsModel1);
        jTable1.setFillsViewportHeight(true);
        jTable1.setFont(jTable1.getFont());
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTable1MouseReleased(evt);
            }
        });
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTable1KeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 5, 5);
        jPanel3.add(jScrollPane1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 5, 5);
        add(jPanel3, gridBagConstraints);

        jPanel4.setLayout(new java.awt.GridBagLayout());

        jLabel4.setFont(jLabel4.getFont().deriveFont(jLabel4.getFont().getSize()+7f));
        jLabel4.setText(bundle.getString("PANEL_SETUP_OFFICE_INCUP")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        jPanel4.add(jLabel4, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(jPanel4, gridBagConstraints);

        jPanel1.setLayout(new java.awt.GridBagLayout());

        jButtonAdd.setFont(jButtonAdd.getFont());
        jButtonAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/plus16.gif"))); // NOI18N
        jButtonAdd.setMaximumSize(new java.awt.Dimension(24, 24));
        jButtonAdd.setMinimumSize(new java.awt.Dimension(24, 24));
        jButtonAdd.setPreferredSize(new java.awt.Dimension(24, 24));
        jButtonAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAddActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 2);
        jPanel1.add(jButtonAdd, gridBagConstraints);

        btnDel.setFont(btnDel.getFont());
        btnDel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/minus16.gif"))); // NOI18N
        btnDel.setEnabled(false);
        btnDel.setMaximumSize(new java.awt.Dimension(24, 24));
        btnDel.setMinimumSize(new java.awt.Dimension(24, 24));
        btnDel.setPreferredSize(new java.awt.Dimension(24, 24));
        btnDel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDelActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 5, 5);
        jPanel1.add(btnDel, gridBagConstraints);

        btnSave.setFont(btnSave.getFont());
        btnSave.setText(bundle.getString("SAVE")); // NOI18N
        btnSave.setEnabled(false);
        btnSave.setMargin(new java.awt.Insets(2, 7, 2, 7));
        btnSave.setMinimumSize(new java.awt.Dimension(24, 24));
        btnSave.setPreferredSize(new java.awt.Dimension(60, 24));
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 5, 5);
        jPanel1.add(btnSave, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 5, 5);
        add(jPanel1, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void btnDelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDelActionPerformed
        this.doDelete();
    }//GEN-LAST:event_btnDelActionPerformed
    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        this.doSaveOrUpdate();
    }//GEN-LAST:event_btnSaveActionPerformed
    private void jButtonAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAddActionPerformed
        this.setReceiptSequance(null);
    }//GEN-LAST:event_jButtonAddActionPerformed
    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        this.doSearch();
    }//GEN-LAST:event_btnSearchActionPerformed
    private void jTable1MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseReleased
        this.doSelectedItem();
    }//GEN-LAST:event_jTable1MouseReleased
    private void jTable1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyReleased
        this.doSelectedItem();
	}//GEN-LAST:event_jTable1KeyReleased

    private void jTextFieldSCodeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldSCodeKeyPressed
        if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
            this.doSearch();
        }
    }//GEN-LAST:event_jTextFieldSCodeKeyPressed

    private void jTextFieldBeginNoPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jTextFieldBeginNoPropertyChange
        this.validateUI();
    }//GEN-LAST:event_jTextFieldBeginNoPropertyChange

    private void jTextFieldEndNoPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jTextFieldEndNoPropertyChange
        this.validateUI();
    }//GEN-LAST:event_jTextFieldEndNoPropertyChange
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDel;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnSearch;
    private javax.swing.JButton jButtonAdd;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelICD9code;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private com.hosv3.gui.component.HJTableSort jTable1;
    private sd.comp.jcalendar.JSpinField jTextFieldBeginNo;
    private sd.comp.jcalendar.JSpinField jTextFieldBookNo;
    private sd.comp.jcalendar.JSpinField jTextFieldEndNo;
    private javax.swing.JTextField jTextFieldIP;
    private sd.comp.jcalendar.JSpinField jTextFieldNextValue;
    private javax.swing.JTextField jTextFieldPattern;
    private sd.comp.jcalendar.JSpinField jTextFieldReceiptQty;
    private javax.swing.JTextField jTextFieldSCode;
    private com.hospital_os.utility.TableResultsModel tableResultsModel1;
    // End of variables declaration//GEN-END:variables

    public void setControl(HosControl hc, UpdateStatus us) {
        theUS = us;
        theHC = hc;
        theSetupControl = hc.theSetupControl;
        btnSearch.doClick();
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        validateUI();
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        validateUI();
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        validateUI();
    }

    private void validateUI() {
        boolean validateIP = IPAddressValidator.validate(jTextFieldIP.getText());
        jTextFieldIP.setBackground(validateIP ? Color.white : Color.red);
        btnSave.setEnabled(validateIP && jTextFieldBeginNo.getValue() <= jTextFieldEndNo.getValue() && !jTextFieldPattern.getText().isEmpty());
        btnDel.setEnabled(theReceiptSequance != null);
    }

    private void doSearch() {
        setReceiptSequance(null);
        Vector v = this.theHC.theSetupControl.listReceiptSequance(jTextFieldSCode.getText());
        this.setReceiptSequanceV(v);
        validateUI();
    }

    private void doSelectedItem() {
        int select = jTable1.getSelectedRow();
        if (select < 0) {
            return;
        }
        ReceiptSequance tmp = this.theHC.theSetupControl.getReceiptSequance(((ReceiptSequance) theReceiptSequanceV.get(select)).getObjectId());
        this.setReceiptSequance(tmp);
        validateUI();
    }

    private void doSaveOrUpdate() {
        this.getReceiptSequance();
        int res = this.theHC.theSetupControl.saveReceiptSequance(theReceiptSequance);
        switch (res) {
            case 4:
                theUS.setStatus("��سҡ�͡ IP", UpdateStatus.WARNING);
                break;
            case 5:
                theUS.setStatus("��سҡ�͡ �ٻẺ", UpdateStatus.WARNING);
                break;
            case 6:
                theUS.setStatus("��سҡ�͡ ��ҶѴ�", UpdateStatus.WARNING);
                break;
            case 7:
                theUS.setStatus("�������ö�ѹ�֡ IP Address �����", UpdateStatus.WARNING);
                break;
            case 8:
                theUS.setStatus("�������ö�ѹ�֡������� ��ӡѺ IP Address �����", UpdateStatus.WARNING);
                break;
            default:
                break;
        }
        this.doSearch();
    }

    private void doDelete() {
        theHC.theSetupControl.deleteReceiptSequance(theReceiptSequance);
        this.doSearch();
    }

    private void getReceiptSequance() {
        if (theReceiptSequance == null) {
            theReceiptSequance = new ReceiptSequance();
        }
        theReceiptSequance.receipt_sequence_active = "1";
        theReceiptSequance.receipt_sequence_ip = this.jTextFieldIP.getText();
        theReceiptSequance.receipt_sequence_pattern = this.jTextFieldPattern.getText();
        theReceiptSequance.receipt_sequence_value = String.valueOf(this.jTextFieldNextValue.getValue());
        theReceiptSequance.book_no = String.valueOf(jTextFieldBookNo.getValue());
        theReceiptSequance.begin_no = String.valueOf(jTextFieldBeginNo.getValue());
        theReceiptSequance.end_no = String.valueOf(jTextFieldEndNo.getValue());
        theReceiptSequance.receipt_qty = String.valueOf(jTextFieldReceiptQty.getValue());
    }

    private void setReceiptSequance(ReceiptSequance receiptSequance) {
        theReceiptSequance = receiptSequance;
        if (theReceiptSequance == null) {
            theReceiptSequance = new ReceiptSequance();
        }
        jTextFieldIP.setText(theReceiptSequance.receipt_sequence_ip);
        jTextFieldPattern.setText(theReceiptSequance.receipt_sequence_pattern);
        jTextFieldNextValue.setValue(Integer.parseInt(
                theReceiptSequance.receipt_sequence_value == null
                || theReceiptSequance.receipt_sequence_value.isEmpty()
                ? "1" : theReceiptSequance.receipt_sequence_value));
        if (theReceiptSequance.getObjectId() == null) {
            jTextFieldIP.setEnabled(true);
        } else {
            jTextFieldIP.setEnabled(false);
        }
        jTextFieldBookNo.setValue(Integer.parseInt(
                theReceiptSequance.book_no == null
                || theReceiptSequance.book_no.isEmpty()
                ? "1" : theReceiptSequance.book_no));
        jTextFieldBeginNo.setValue(Integer.parseInt(
                theReceiptSequance.begin_no == null
                || theReceiptSequance.begin_no.isEmpty()
                ? "1" : theReceiptSequance.begin_no));
        jTextFieldEndNo.setValue(Integer.parseInt(
                theReceiptSequance.end_no == null
                || theReceiptSequance.end_no.isEmpty()
                ? "1" : theReceiptSequance.end_no));
        jTextFieldReceiptQty.setValue(Integer.parseInt(
                theReceiptSequance.receipt_qty == null
                || theReceiptSequance.receipt_qty.isEmpty()
                ? "1" : theReceiptSequance.receipt_qty));
    }

    private void setReceiptSequanceV(Vector v) {
        theReceiptSequanceV = v;
        if (theReceiptSequanceV == null) {
            theReceiptSequanceV = new Vector();
        }
        TaBleModel tm = new TaBleModel(cols, theReceiptSequanceV.size());
        for (int i = 0; i < theReceiptSequanceV.size(); i += 1) {
            ReceiptSequance tmp = (ReceiptSequance) theReceiptSequanceV.get(i);
            tm.setValueAt(tmp.receipt_sequence_ip, i, 0);
        }
        jTable1.setModel(tm);
    }
}

/*
 * HosControl.java
 *
 * Created on 27 ���Ҥ� 2546, 19:26 �.
 */
package com.hosv3.control;

import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.usecase.connection.ModuleInfTool;
import com.hospital_os.usecase.connection.UpdateStatus;
import static com.hosv3.control.PrintControl.MODE_PRINT;
import com.hosv3.control.thread.ImportFileToDBControlThread;
import com.hosv3.gui.panel.transaction.HosPanel;
import com.hosv3.object.HosObject;
import com.hosv3.object.LookupObject;
import com.hosv3.subject.HosSubject;
import com.hosv3.utility.ConnectionDBMgr;
import com.hosv3.utility.DialogConfig;
import com.hosv3.utility.ResourceBundle;
import java.util.Vector;

/**
 *
 * @author henbe
 */
@SuppressWarnings("ClassWithoutLogger")
public class HosControl {

    public ConnectionInf theConnectionInf;
    public HosObject theHO;
    public LookupObject theLO;
    public HosSubject theHS;
    public UpdateStatus theUS;
    public HosDB theHosDB;
    public PatientControl thePatientControl;
    public LookupControl theLookupControl;
    public SystemControl theSystemControl;
    public VisitControl theVisitControl;
    public VitalControl theVitalControl;
    public DiagnosisControl theDiagnosisControl;
    public OrderControl theOrderControl;
    public SetupControl theSetupControl;
    public BillingControl theBillingControl;
    public PrintControl thePrintControl;
    public ResultControl theResultControl;
    public LabReferControl theLabReferControl;
    public GPatientSuit theGPS;
    public HosPanel theHP;
    public LabControl theLabControl;
    public NotifyNoteControl theNotifyNoteControl;
    public AllDialogDatasourceControl theAllDialogDatasourceControl;
    public ImportFileToDBControlThread theImportFileToDBControlThread;
    public RESTFulControl theRESTFulControl;
    private Vector theModuleV;

    /**
     * Creates a new instance of HosControl
     */
    public HosControl() {
        String url = "jdbc:postgresql://localhost:5432/hos";
        String user = "postgres";
        String pass = "postgres";
        String dri = "org.postgresql.Driver";
        String type = "0"; //0 postgres 1 mysql 2 sqlserverT
        theConnectionInf = new ConnectionDBMgr(dri, url, user, pass, type);
        initControl(theConnectionInf);
    }

    public void setHosPanel(HosPanel hp) {
        theHP = hp;
    }

    public HosControl(String url, String uname, String passwd, int statusApp, String di, String typeDatabase) {
        theConnectionInf = new ConnectionDBMgr(di, url, uname, passwd, typeDatabase);
        initControl(theConnectionInf);
    }

    public HosControl(ConnectionInf c) {
        initControl(c);
    }

    public HosDB getHosDB() {
        return theHosDB;
    }

    private void initControl(ConnectionInf c) {
        theConnectionInf = c;
        LookupObject lo = new LookupObject();
        HosObject ho = new HosObject(lo);
        HosSubject hs = new HosSubject();
        theHosDB = new HosDB(theConnectionInf);
        theHO = ho;
        theHS = hs;
        theLO = lo;
        theHO.local_db = DialogConfig.readDbFile();
        theLookupControl = new LookupControl(theConnectionInf, ho, theHosDB, hs, lo);
        theLookupControl.setHosControl(this);
        theSystemControl = new SystemControl(theConnectionInf, ho, theHosDB, hs);
        theSystemControl.setHosControl(this);
        theSystemControl.setDepControl(theLookupControl);
        thePatientControl = new PatientControl(theConnectionInf, ho, theHosDB, hs, lo);
        thePatientControl.setHosControl(this);
        thePatientControl.setSystemControl(theSystemControl);
        theSetupControl = new SetupControl(theConnectionInf, ho, theHosDB, hs, lo);
        theSetupControl.setSystemControl(theSystemControl);
        theSetupControl.setHosControl(this);
        theSetupControl.setDepControl(theLookupControl, thePatientControl);
        theOrderControl = new OrderControl(theConnectionInf, ho, theHosDB, hs);
        theOrderControl.setSystemControl(theSystemControl);
        theOrderControl.setHosControl(this);
        theVisitControl = new VisitControl(theConnectionInf, ho, theHosDB, hs);
        theVisitControl.setHosControl(this);
        theVisitControl.setDepControl(theLookupControl, theOrderControl, thePatientControl, theSystemControl);
        theDiagnosisControl = new DiagnosisControl(theConnectionInf, ho, theHosDB, hs);
        theDiagnosisControl.setSystemControl(theSystemControl);
        theDiagnosisControl.setHosControl(this);
        theDiagnosisControl.setDepControl(theLookupControl, theVisitControl);
        theOrderControl.setDepControl(theDiagnosisControl, theVisitControl, theLookupControl, theSetupControl);
        thePatientControl.setDepControl(theLookupControl, theVisitControl);
        theVitalControl = new VitalControl(theConnectionInf, ho, theHosDB, hs);
        theVitalControl.setSystemControl(theSystemControl);
        theVitalControl.setDepControl(theLookupControl, theVisitControl, theDiagnosisControl, theOrderControl);
        theVitalControl.setHosControl(this);
        theLabReferControl = new LabReferControl(theConnectionInf, ho, theHosDB, hs, lo);
        theLabReferControl.setDepControl(theLookupControl);
        theLabReferControl.setSystemControl(theSystemControl);
        theLabReferControl.setHosControl(this);
        theResultControl = new ResultControl(theConnectionInf, ho, theHosDB, hs);
        theResultControl.setDepControl(theLookupControl, theVisitControl, theOrderControl);
        theResultControl.setSystemControl(theSystemControl);
        theResultControl.setHosControl(this);
        theBillingControl = new BillingControl(theConnectionInf, ho, theHosDB, hs, lo);
        theBillingControl.setSystemControl(theSystemControl);
        theBillingControl.setDepControl(theLookupControl, theOrderControl, theVisitControl);
        theBillingControl.setHosControl(this);
        thePrintControl = new PrintControl(theConnectionInf, ho, theHosDB, hs, lo);
        thePrintControl.setSystemControl(theSystemControl);
        thePrintControl.setDepControl(theLookupControl, theSetupControl, theVisitControl, theOrderControl, theVitalControl, theDiagnosisControl, theResultControl, theBillingControl, thePatientControl);
        thePrintControl.setHosControl(this);
        theLabControl = new LabControl(this);
        theNotifyNoteControl = new NotifyNoteControl(theConnectionInf, ho, theHosDB, hs, theLookupControl);
        theNotifyNoteControl.setHosControl(this);
        theAllDialogDatasourceControl = new AllDialogDatasourceControl(this);
        theImportFileToDBControlThread = new ImportFileToDBControlThread(theConnectionInf);
        theRESTFulControl = new RESTFulControl();
        theRESTFulControl.setHosControl(this);
    }

    public ConnectionInf getConnecton() {
        return theConnectionInf;
    }

    public void setUpdateStatus(UpdateStatus us) {
        theUS = us;
        theGPS = new GPatientSuit(theHO, theHosDB, theUS);
        theLookupControl.setUpdateStatus(theUS);
        theSystemControl.setUpdateStatus(theUS);
        thePatientControl.setUpdateStatus(theUS);
        theOrderControl.setUpdateStatus(theUS);
        theVisitControl.setUpdateStatus(theUS);
        theVitalControl.setUpdateStatus(theUS);
        thePrintControl.setUpdateStatus(theUS);
        theDiagnosisControl.setUpdateStatus(theUS);
        theBillingControl.setUpdateStatus(theUS);
        theResultControl.setUpdateStatus(theUS);
        theSetupControl.setUpdateStatus(theUS);
        theLabReferControl.setUpdateStatus(theUS);
        theNotifyNoteControl.setUpdateStatus(theUS);
        theLabControl.setUpdateStatus(theUS);
        theImportFileToDBControlThread.setUpdateStatus(us);
        theRESTFulControl.setUpdateStatus(us);
    }
    public final static int TRANSACTION_INSERT = 0;
    public final static int TRANSACTION_UPDATE = 1;
    public final static int TRANSACTION_INACTIVE = 2;
    public final static int TRANSACTION_QUERY = 3;
    public final static int TRANSACTION_IMPORT = 4;
    public final static int TRANSACTION_DELETE = 5;
    public final static int UNKONW = 6;
    public final static int PRINT = 7;

    public void updateTransactionSuccess(int mode) {
        updateTransactionSuccess(mode, null);
    }

    public void updateTransactionSuccess(int mode, String description) {
        updateTransactionStatus(mode, description, UpdateStatus.COMPLETE, null);
    }

    public void updateTransactionFail(int mode) {
        updateTransactionFail(mode, (String) null);
    }

    public void updateTransactionFail(int mode, String description) {
        updateTransactionFail(mode, description, null);
    }

    public void updateTransactionFail(int mode, Exception ex) {
        updateTransactionFail(mode, null, ex);
    }

    public void updateTransactionFail(int mode, String description, Exception ex) {
        updateTransactionStatus(mode, description, UpdateStatus.ERROR, ex);
    }

    public void updateTransactionStatus(int mode, int status) {
        updateTransactionStatus(mode, status, "");
    }

    public void updateTransactionStatus(int mode, int status, String desc) {
        updateTransactionStatus(mode, desc, status, null);
    }

    public void updateTransactionStatus(int mode, int status, Exception ex) {
        updateTransactionStatus(mode, null, status, null);
    }

    public void updateTransactionStatus(int mode, String description, int status, Exception ex) {
        StringBuilder text = new StringBuilder();
        switch (mode) {
            case TRANSACTION_INSERT:
                text.append(ResourceBundle.getBundleGlobal("TEXT.TRANSACTION.INSERT"));
                break;
            case TRANSACTION_UPDATE:
                text.append(ResourceBundle.getBundleGlobal("TEXT.TRANSACTION.UPDATE"));
                break;
            case TRANSACTION_INACTIVE:
                text.append(ResourceBundle.getBundleGlobal("TEXT.TRANSACTION.DELETE"));
                break;
            case TRANSACTION_QUERY:
                text.append(ResourceBundle.getBundleGlobal("TEXT.TRANSACTION.QUERY"));
                break;
            case TRANSACTION_IMPORT:
                text.append(ResourceBundle.getBundleGlobal("TEXT.TRANSACTION.IMPORT"));
                break;
            case TRANSACTION_DELETE:
                text.append(ResourceBundle.getBundleGlobal("TEXT.TRANSACTION.DELETE"));
                break;
            case PRINT:
                text.append(ResourceBundle.getBundleGlobal("TEXT.PRINTING"));
                break;
            default:
                break;
        }
        if (description != null && !description.isEmpty()) {
            text.append(description).append(" ");
        }
        switch (status) {
            case UpdateStatus.COMPLETE:
                text.append(ResourceBundle.getBundleGlobal("TEXT.SUCCESS"));
                break;
            case UpdateStatus.ERROR:
                text.append(ResourceBundle.getBundleGlobal("TEXT.FAIL"));
                break;
            default:
                break;
        }
        theUS.setStatus(text.toString(), status);
        // in the feature
        // send email with Exception
    }

    public void updatePrintSuccess(String message) {
        updateTransactionSuccess(PRINT, message);
    }

    public void updatePrintFail(String message, Exception ex) {
        updateTransactionFail(PRINT, message, ex);
    }

    public void updatePrintSuccess(int mode) {
        String msg = ResourceBundle.getBundleText("com.hosv3.control.PrintControl."
                + (mode == MODE_PRINT ? "PRINT" : "PREVIEW"));
        updatePrintSuccess(msg);
    }

    public void updatePrintFail(int mode, Exception ex) {
        String msg = ResourceBundle.getBundleText("com.hosv3.control.PrintControl."
                + (mode == MODE_PRINT ? "PRINT" : "PREVIEW"));
        updatePrintFail(msg, ex);
    }

    public void setModules(Vector<ModuleInfTool> theModuleV) {
        this.theModuleV = theModuleV;
    }

    public Vector<ModuleInfTool> getModules() {
        return this.theModuleV;
    }
}

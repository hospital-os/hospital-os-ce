/*
 * secure.java
 *
 * Created on 23 �չҤ� 2547, 17:27 �.
 */
package com.hospital_os.utility;

/**
 *
 * @author Administrator
 */
@SuppressWarnings("ClassWithoutLogger")
public class SecureAdmin {

    public static boolean checkUserPassword(String user, String password) {
        boolean result = false;
        char[] as = user.toCharArray();
        int i;
        for (i = 0; i < as.length; i++) {
            int l = as[i];
            if (48 <= l && l <= 57) {
                break;
            }
        }
        if (i > as.length) {
            result = false;
        } else {
            String userchar = user.substring(0, i);
            String userint = user.substring(i);

            int mid = userint.length() / 2;
            String userhead = userint.substring(mid);
            String userletter = userint.substring(0, mid);
            userchar = encode(userchar);
            userhead = encodei(userhead);
            userletter = encodei(userletter);
            String passwordend = userhead + userchar + userletter;
            if (password.equals(passwordend)) {
                result = true;
            }
        }
        return result;
    }

    private static String encode(String dat) {
        char[] text = dat.toCharArray();

        for (int i = 0; i < text.length; i++) {
            int letter = text[i];
            letter += 4;
            text[i] = (char) letter;
        }
        return new String(text);
    }

    private static String encodei(String dat) {
        char[] text = dat.toCharArray();

        for (int i = 0; i < text.length; i++) {
            int letter = text[i];
            letter += 7;
            text[i] = (char) letter;
        }

        return new String(text);
    }
}
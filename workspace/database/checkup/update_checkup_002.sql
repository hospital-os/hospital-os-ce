CREATE OR REPLACE FUNCTION "public"."isdigits" (in text) RETURNS bool AS
$BODY$
select $1 ~ '^(-)?[0.0-9.0]+$' as result
$BODY$
LANGUAGE 'sql';


INSERT INTO s_checkup_version VALUES ('2', '2', 'Checkup Module', '1.1.0', '1.1.0', (select current_date) || ','|| (select current_time));

INSERT INTO s_script_update_log values ('checkup_Module','update_checkup_002.sql',(select current_date) || ','|| (select current_time),'Update Checkup Module');

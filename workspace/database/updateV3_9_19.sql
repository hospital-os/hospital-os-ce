CREATE TABLE t_visit_anemia
(
  t_visit_anemia_id character varying(255) NOT NULL,
  t_visit_id character varying(255) NOT NULL,
  anemia_status character varying(1) NOT NULL DEFAULT '9',
  user_record character varying(255) NOT NULL,
  record_date_time character varying(19) NOT NULL,
  user_modify character varying(255) NOT NULL,
  modify_date_time character varying(19) NOT NULL,
  active character varying(1) NOT NULL default '1',
  CONSTRAINT t_visit_anemia_pkey PRIMARY KEY (t_visit_anemia_id)
);
CREATE INDEX t_visit_id_visit_anemia
  ON t_visit_anemia
  USING btree
  (t_visit_id);


CREATE TABLE t_patient_past_vaccine
(
  t_patient_past_vaccine_id character varying(255) NOT NULL,
  t_patient_id character varying(25) NOT NULL,
  patient_past_vaccine_complete character varying(255) NOT NULL default '9',
  patient_past_vaccine_no_comlete_name text,
  user_record character varying(255) NOT NULL,
  record_date_time character varying(19) NOT NULL,
  user_modify character varying(255) NOT NULL,
  modify_date_time character varying(19) NOT NULL,
  active character varying(1) NOT NULL default '1',
  CONSTRAINT t_patient_past_vaccine_pkey PRIMARY KEY (t_patient_past_vaccine_id)
);
CREATE INDEX t_patient_id_patient_past_vaccine
  ON t_patient_past_vaccine
  USING btree
  (t_patient_id);

CREATE TABLE t_allergic_reactions
(
  t_allergic_reactions_id character varying(255) NOT NULL,
  t_patient_id character varying(25) NOT NULL,
  f_allergic_reactions_type_id character varying(1) NOT NULL default '1',
  allergic_reactions_list text NOT NULL,
  allergic_reactions_check character varying(1) NOT NULL default '0',
  allergic_reactions_checker character varying(255) NULL,
  allergic_reactions_check_detail text default '',
  user_record character varying(255) NOT NULL,
  record_date_time character varying(19) NOT NULL,
  user_modify character varying(255) NOT NULL,
  modify_date_time character varying(19) NOT NULL,
  active character varying(1) NOT NULL default '1',
  CONSTRAINT t_allergic_reactions_pkey PRIMARY KEY (t_allergic_reactions_id)
);
CREATE INDEX t_patient_id_allergic_reactions
  ON t_allergic_reactions
  USING btree
  (t_patient_id);

CREATE TABLE t_allergy_drug_order
(
  t_allergy_drug_order_id character varying(255) NOT NULL,
  t_patient_drug_allergy_id character varying(255) NOT NULL,
  t_order_id character varying(255) NOT NULL,
  allergy_drug_order_cause text NOT NULL,
  user_record character varying(255) NOT NULL,
  record_date_time character varying(19) NOT NULL,
  user_modify character varying(255) NOT NULL,
  modify_date_time character varying(19) NOT NULL,
  active character varying(1) NOT NULL default '1',
  CONSTRAINT t_allergy_drug_order_pkey PRIMARY KEY (t_allergy_drug_order_id)
);
CREATE INDEX t_patient_id_allergy_drug_order
  ON t_allergy_drug_order
  USING btree
  (t_patient_drug_allergy_id);
CREATE INDEX t_order_id_allergy_drug_order
  ON t_allergy_drug_order
  USING btree
  (t_order_id);

CREATE TABLE t_g_6_pd
(
  t_g_6_pd_id character varying(255) NOT NULL,
  t_patient_id character varying(25) NOT NULL,
  g_6_pd character varying(1) NOT NULL default '0',
  user_record character varying(255) NOT NULL,
  record_date_time character varying(19) NOT NULL,
  user_modify character varying(255) NOT NULL,
  modify_date_time character varying(19) NOT NULL,
  active character varying(1) NOT NULL default '1',
  CONSTRAINT t_g_6_pd_pkey PRIMARY KEY (t_g_6_pd_id)
);
CREATE INDEX t_patient_id_g_6_pd
  ON t_g_6_pd
  USING btree
  (t_patient_id);

ALTER TABLE t_patient_drug_allergy RENAME TO t_patient_drug_allergy_backup;
ALTER INDEX t_patient_drug_allergy_pkey RENAME TO t_patient_drug_allergy_backup_pkey;

CREATE TABLE t_patient_drug_allergy
(
  t_patient_drug_allergy_id character varying(255) NOT NULL,
  t_patient_id character varying(25) NOT NULL,
  b_item_drug_standard_id character varying(255) NOT NULL,
  f_allergy_type_id character varying(1) NOT NULL,
  drug_allergy_symtom_date character varying(19) NOT NULL,
  drug_allergy_symtom text NOT NULL,
  f_naranjo_interpretation_id character varying(1) NOT NULL,
  drug_allergy_note text NULL,
  f_allergy_warning_type_id character varying(1) NOT NULL,
  drug_allergy_report_date character varying(19) NOT NULL,
  pharma_assess_id character varying(255) NOT NULL,
  doctor_diag_id character varying(255) NOT NULL,
  user_record character varying(255) NOT NULL,
  record_date_time character varying(19) NOT NULL,
  user_modify character varying(255) NOT NULL,
  modify_date_time character varying(19) NOT NULL,
  active character varying(1) NOT NULL default '1',
  CONSTRAINT t_patient_drug_allergy_pkey PRIMARY KEY (t_patient_drug_allergy_id)
);
CREATE INDEX t_patient_id_patient_drug_allergy
  ON t_patient_drug_allergy
  USING btree
  (t_patient_id);
CREATE INDEX b_item_drug_standard_id_patient_drug_allergy
  ON t_patient_drug_allergy
  USING btree
  (b_item_drug_standard_id);

CREATE TABLE t_naranjo
(
  t_naranjo_id character varying(255) NOT NULL,
  t_patient_drug_allergy_id character varying(255) NOT NULL,
  naranjo_result1 integer NOT NULL default 9,
  naranjo_result2 integer NOT NULL default 9,
  naranjo_result3 integer NOT NULL default 9,
  naranjo_result4 integer NOT NULL default 9,
  naranjo_result5 integer NOT NULL default 9,
  naranjo_result6 integer NOT NULL default 9,
  naranjo_result7 integer NOT NULL default 9,
  naranjo_result8 integer NOT NULL default 9,
  naranjo_result9 integer NOT NULL default 9,
  naranjo_result10 integer NOT NULL default 9,
  naranjo_total integer NOT NULL default 0,
  user_record character varying(255) NOT NULL,
  record_date_time character varying(19) NOT NULL,
  user_modify character varying(255) NOT NULL,
  modify_date_time character varying(19) NOT NULL,
  active character varying(1) NOT NULL DEFAULT '1',
  CONSTRAINT t_naranjo_pkey PRIMARY KEY (t_naranjo_id)
);
CREATE INDEX t_patient_drug_allergy_id_naranjo
  ON t_naranjo
  USING btree
  (t_patient_drug_allergy_id);

CREATE TABLE b_item_drug_map_g6pd
(
  b_item_drug_map_g6pd_id character varying(255) NOT NULL,
  b_item_id character varying(255) NOT NULL,
  CONSTRAINT b_item_drug_map_g6pd_pkey PRIMARY KEY (b_item_drug_map_g6pd_id)
);

ALTER TABLE b_item_drug_map_g6pd ADD UNIQUE (b_item_id);

CREATE TABLE f_allergic_reactions_type
(
  f_allergic_reactions_type_id character varying(1) NOT NULL,
  allergic_reactions_type_description character varying(255) NOT NULL,
  CONSTRAINT f_allergic_reactions_type_pkey PRIMARY KEY (f_allergic_reactions_type_id)
);

CREATE TABLE f_allergy_warning_type
(
  f_allergy_warning_type_id character varying(1) NOT NULL,
  warning_type_description character varying(255) NOT NULL,
  CONSTRAINT f_allergy_warning_type_pkey PRIMARY KEY (f_allergy_warning_type_id)
);

CREATE TABLE f_allergy_type
(
  f_allergy_type_id character varying(1) NOT NULL,
  allergy_type_description character varying(255) NOT NULL,
  CONSTRAINT f_allergy_type_pkey PRIMARY KEY (f_allergy_type_id)
);

CREATE TABLE f_naranjo_interpretation
(
  f_naranjo_interpretation_id character varying(1) NOT NULL,
  naranjo_interpretation_detail character varying(255) NOT NULL,
  max_score integer NOT NULL,
  min_score integer NOT NULL,
  CONSTRAINT f_naranjo_interpretation_pkey PRIMARY KEY (f_naranjo_interpretation_id)
);

delete  from b_item_drug_standard_map_item where b_item_drug_standard_id = '';

ALTER TABLE t_patient
	ADD COLUMN deny_allergy character varying(1) NOT NULL DEFAULT '0';

insert into  f_allergy_warning_type values ('1','แพ้ยา');
insert into  f_allergy_warning_type values ('2','เฝ้าระวังการใช้ยา');
insert into  f_allergy_warning_type values ('3','สงสัยแพ้ยา');

insert into  f_allergy_type values ('1','A');
insert into  f_allergy_type values ('2','B');
insert into  f_allergy_type values ('9','ไม่ระบุ');

insert into  f_naranjo_interpretation values ('1','น่าสงสัย (Doubtful)',-999,0);
insert into  f_naranjo_interpretation values ('2','อาจจะใช่ (Possible)',1,4);
insert into  f_naranjo_interpretation values ('3','น่าจะใช่ (Probable)',5,8);
insert into  f_naranjo_interpretation values ('4','ใช่แน่ (Definite)',9,999);

insert into  f_allergic_reactions_type values ('1','ยา');
insert into  f_allergic_reactions_type values ('2','สารเคมีและอื่นๆ');

INSERT INTO f_gui_action (f_gui_action_id, gui_action_name, gui_action_note) 
	VALUES ('0414', 'แจ้งข้อมูลแพ้ยา/สารเคมีและอื่นๆ', NULL);
INSERT INTO f_gui_action (f_gui_action_id, gui_action_name, gui_action_note) 
	VALUES ('5508', 'จับคู่รายการยา ชื่อสามัญทางยา และกลุ่มยามาตรฐาน', NULL);
INSERT INTO f_gui_action (f_gui_action_id, gui_action_name, gui_action_note) 
	VALUES ('5509', 'จับคู่รายการยา กับโรค G-6-PD', NULL);


insert into b_icd9 values ('0555000000001','0010000','การตรวจรักษากรณีผู้ป่วยนอก ครั้งแรก','First Outpatient Care','','');
insert into b_icd9 values ('0555000000002','0020000','การตรวจรักษากรณีผู้ป่วยนอก ครั้งต่อไป สำหรับปัญหาเดียวกัน','Follow up Outpatient Care for the Same Illness','','');
insert into b_icd9 values ('0555000000003','0030000','การตรวจรักษากรณีผู้ป่วยนอก ครั้งต่อไป สำหรับปัญหาเรื้อรัง','Regular Follow Up Outpatient Care for a Chronic Condition','','');
insert into b_icd9 values ('0555000000004','0040000','การตรวจรักษาผู้ป่วยนอก กรณีฉุกเฉินทั่วไป','Emergency Outpatient Care','','');
insert into b_icd9 values ('0555000000005','0120000','การตรวจสุขภาพต่าง ๆ อาทิ เพื่อประเมินทั่วไป เพื่อการสมัครงานและเพื่อการประกันชีวิต','Checkups','','');
insert into b_icd9 values ('0555000000006','0130000','การตรวจประเมินผู้ป่วยที่บ้าน','Home visit','','');
insert into b_icd9 values ('0555000000007','1011170','ตัดไหมที่หนังศีรษะ','Removal of sutures from scalp','','');
insert into b_icd9 values ('0555000000008','1011171','ดึงวัตถุแปลกปลอมออกจากหนังศีรษะ','Removal of foreign body from scalp','','');
insert into b_icd9 values ('0555000000009','1011770','พันแผลโดยใช้แรงกดเพื่อห้ามเลือดบริเวณหนังศีรษะ','Application of pressure dressing on scalp','','');
insert into b_icd9 values ('0555000000010','1012270','เจาะดูดของเหลวจากบริเวณหนังศีรษะ','Aspiration of scalp','','');
insert into b_icd9 values ('0555000000011','1012271','ผ่าระบายของเหลวจากบริเวณหนังศีรษะ','Drainage of scalp','','');
insert into b_icd9 values ('0555000000012','1012670','ตัดเนื้อตายออกจากบริเวณหนังศีรษะ','Debridement of scalp','','');
insert into b_icd9 values ('0555000000013','1012671','ผ่าตัดรอยโรคออกจากหนังศีรษะ','Excision of lesion of scalp','','');
insert into b_icd9 values ('0555000000014','1012870','จี้ทำลายรอยโรคบริเวณหนังศีรษะ','Destruction of lesion of scalp','','');
insert into b_icd9 values ('0555000000015','1013970','เย็บแผลบริเวณหนังศีรษะ','Repair of laceration or wound of scalp','','');
insert into b_icd9 values ('0555000000016','1018070','ทำแผลที่หนังศีรษะ','Wound dressing on scalp','','');
insert into b_icd9 values ('0555000000017','1541170','ตัดไหมที่หน้า','Removal of sutures from facial skin','','');
insert into b_icd9 values ('0555000000018','1541171','ดึงวัตถุแปลกปลอมออกจากผิวหนังบริเวณหน้า','Removal of foreign body from facial skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000019','1542270','เจาะดูดของเหลวจากผิวหนังบริเวณหน้า','Aspiration of facial skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000020','1542271','ผ่าระบายของเหลวจากผิวหนังบริเวณหน้า','Drainage of facial skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000021','1542670','ผ่าตัดเนื้อตายออกจากบริเวณ แผล บริเวณติดเชื้อ แผลไหม้ ที่หน้า','Excisional debridement of wound, infection or burn of facial skin','','');
insert into b_icd9 values ('0555000000022','1542671','ผ่าตัดรอยโรคออกจากผิวหนังบริเวณหน้า','Excision of lesion of facial skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000023','1542870','จี้ทำลายรอยโรคผิวหนังบริเวณหน้า','Destruction of lesion of facial skin','','');
insert into b_icd9 values ('0555000000024','1543970','เย็บแผลที่หน้า','Suture of laceration of facial skin','','');
insert into b_icd9 values ('0555000000025','1548070','ทำแผลที่หน้า','Wound dressing on face','','');
insert into b_icd9 values ('0555000000026','1632671','ผ่าตัดกุ้งยิงที่ตาข้างเดียว','Excision of chalazion; single or multiple, one eye','','');
insert into b_icd9 values ('0555000000027','1632672','ผ่าตัดกุ้งยิงที่ตาสองข้าง','Excision of chalazion; single or multiple, both eye','','');
insert into b_icd9 values ('0555000000028','1632678','การตัดรอยโรคเล็กๆที่เปลือกตา','Excision of other minor lesion of eyelid','','');
insert into b_icd9 values ('0555000000029','1671171','ดึงวัตถุแปลกปลอมออกจากตาดำ','Removal of foreign body from cornea','','');
insert into b_icd9 values ('0555000000030','1672671','การผ่าลอกต้อเนื้อ','Simple pterygium excision','','');
insert into b_icd9 values ('0555000000031','1679999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณตาดำ','Other procedures and operations on cornea','','');
insert into b_icd9 values ('0555000000032','1761171','ดึงวัตถุแปลกปลอมที่อยู่ชั้นผิวในตา โดยไม่ต้องผ่า','Removal of superficial  foreign body from eye without incision','','');
insert into b_icd9 values ('0555000000033','1768370','ล้างตา','Irrigation of eye','','');
insert into b_icd9 values ('0555000000034','1781170','ตัดไหมที่ใบหู','Removal of sutures from external ear','','');
insert into b_icd9 values ('0555000000035','1781171','ดึงวัตถุแปลกปลอมออกจากรูหู','Removal of intraluminal foreign body from ear','','');
insert into b_icd9 values ('0555000000036','1781172','ดึงผ้าที่อุดห้ามเลือดออกจากรูหู','Removal of packing of external auditory canal','','');
insert into b_icd9 values ('0555000000037','1781173','ดึงขี้หูออกจากรูหู','Removal of cerumen from ear','','');
insert into b_icd9 values ('0555000000038','1781770','ใช้ผ้าอุดห้ามเลือดในรูหู','Packing of external auditory canal','','');
insert into b_icd9 values ('0555000000039','1782270','เจาะดูดของเหลวจากหู','Aspiration of external ear','','');
insert into b_icd9 values ('0555000000040','1782271','ผ่าระบายของเหลวจากบริเวณ ใบหู','Drainage of external ear','','');
insert into b_icd9 values ('0555000000041','1782272','ผ่าระบายของเหลวประเภทน้ำเหลืองหรือก้อนเลือดออกจากใบหู','Drainage of seroma or hematoma of pinna','','');
insert into b_icd9 values ('0555000000042','1782278','ผ่าระบายของเหลวประเภทอื่นออกจากใบหู','Drainage of other lesions of pinna','','');
insert into b_icd9 values ('0555000000043','1782670','ตัดเนื้อตายออกจากบริเวณใบหู','Debridement of external ear','','');
insert into b_icd9 values ('0555000000044','1782671','ผ่าตัดรอยโรคออกจาก ใบหู','Excision of lesion of external ear','','');
insert into b_icd9 values ('0555000000045','1782870','จี้ทำลายรอยโรคบริเวณใบหู','Destruction of lesion of external ear','','');
insert into b_icd9 values ('0555000000046','1783970','เย็บแผลที่ใบหู','Suture of laceration of external ear or pinna','','');
insert into b_icd9 values ('0555000000047','1788070','ทำแผลที่ใบหู','Dressing wound of external ear','','');
insert into b_icd9 values ('0555000000048','1788370','ล้างรูหู','Irrigation of external auditory canal','','');
insert into b_icd9 values ('0555000000049','1851170','ตัดไหมที่จมูก','Removal of sutures from nose','','');
insert into b_icd9 values ('0555000000050','1851171','ดึงวัตถุแปลกปลอมออกจากผิวหนังบริเวณจมูก','Removal of foreign body from skin of nose','','');
insert into b_icd9 values ('0555000000051','1852270','เจาะดูดของเหลวจากผิวหนังบริเวณจมูก','Aspiration of nose','','');
insert into b_icd9 values ('0555000000052','1852271','ผ่าระบายของเหลวจากผิวหนังบริเวณ จมูก','Drainage of nose','','');
insert into b_icd9 values ('0555000000053','1852670','ตัดเนื้อตายออกจากผิวหนังบริเวณจมูก','Debridement of nose','','');
insert into b_icd9 values ('0555000000054','1852671','ผ่าตัดรอยโรคออกจากผิวหนังบริเวณจมูก','Excision of lesion of nose','','');
insert into b_icd9 values ('0555000000055','1852870','จี้ทำลายรอยโรคผิวหนังบริเวณจมูก','Destruction of lesion of nose','','');
insert into b_icd9 values ('0555000000056','1853970','เย็บแผลที่จมูก','Suture of laceration of nose','','');
insert into b_icd9 values ('0555000000057','1858070','ทำแผลที่จมูก','Dressing wound on nose','','');
insert into b_icd9 values ('0555000000058','1861171','ดึงวัตถุแปลกปลอมออกจากช่องโพรงจมูก','Removal of intraluminal foreign body from nasal cavity','','');
insert into b_icd9 values ('0555000000059','1861172','ดึงผ้าอุดห้ามเลือดในโพรงจมูกออก','Removal of nasal packing','','');
insert into b_icd9 values ('0555000000060','1861270','เปลี่ยนผ้าที่ใช้อุดห้ามเลือดในโพรงจมูก','Replacement of nasal packing','','');
insert into b_icd9 values ('0555000000061','1862570','ห้ามเลือดกำเดาโดยใช้ผ้าอุดห้ามเลือดในจมูกส่วนหน้า','Control of epistaxis by anterior nasal packing','','');
insert into b_icd9 values ('0555000000062','1862571','ห้ามเลือดกำเดาโดยใช้ผ้าอุดห้ามเลือดในจมูกส่วนหลัง(และส่วนหน้า)','Control of epistaxis by posterior (and anterior) nasal packing','','');
insert into b_icd9 values ('0555000000063','1868370','ล้างโพรงจมูก','Irrigation of nasal passages','','');
insert into b_icd9 values ('0555000000064','2001370','การใส่ปลอกป้องกันคอ','Application of neck support','','');
insert into b_icd9 values ('0555000000065','2011170','ตัดไหมจากผิวหนังบริเวณคอ','Removal of sutures from neck skin','','');
insert into b_icd9 values ('0555000000066','2011171','ดึงวัตถุแปลกปลอมออกจากผิวหนัง และชั้นใต้ผิวหนังของคอ','Removal of foreign body from neck skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000067','2012270','เจาะดูดของเหลวจากบริเวณผิวหนัง และชั้นใต้ผิวหนังของคอ','Aspiration of neck skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000068','2012271','ผ่าระบายของเหลวจากบริเวณผิวหนัง และชั้นใต้ผิวหนังของคอ','Drainage of neck skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000069','2012670','ผ่าตัดเนื้อตายออกจากบริเวณ แผล บริเวณติดเชื้อ แผลไหม้ ที่คอ','Excisional debridement of wound, infection or burn of neck skin','','');
insert into b_icd9 values ('0555000000070','2012671','ผ่าตัดรอยโรคออกจาก ผิวหนัง และชั้นใต้ผิวหนังของคอ','Excision of lesion of neck skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000071','2012870','จี้ทำลายรอยโรคบริเวณผิวหนังคอ','Destruction of lesion of neck skin','','');
insert into b_icd9 values ('0555000000072','2013970','เย็บแผลที่คอ','Suture of laceration of neck skin','','');
insert into b_icd9 values ('0555000000073','2018070','ทำแผลที่ผิวหนังคอ','Wound dressing at neck','','');
insert into b_icd9 values ('0555000000074','2251170','ตัดไหมที่ริมฝีปาก','Removal of sutures from lip','','');
insert into b_icd9 values ('0555000000075','2252670','ตัดเนื้อตายออกจากบริเวณริมฝีปาก','Debridement of lip','','');
insert into b_icd9 values ('0555000000076','2252671','ตัดรอยโรคออกจากริมฝีปาก','Excision of lesion of lip','','');
insert into b_icd9 values ('0555000000077','2253970','เย็บแผลที่ริมฝีปาก','Suture of laceration of lip','','');
insert into b_icd9 values ('0555000000078','2287430','การปรับแต่งฟันปลอมเต็มส่วน ขากรรไกรล่าง','Adjust complete denture, -lower','','');
insert into b_icd9 values ('0555000000079','2331170','ตัดไหมที่ปาก','Removal of sutures from mouth','','');
insert into b_icd9 values ('0555000000080','2331171','ดึงวัตถุแปลกปลอมออกจากปากโดยไม่ต้องผ่า','Removal of foreign body from mouth without incision','','');
insert into b_icd9 values ('0555000000081','2332670','ตัดเนื้อตายออกจากบริเวณปาก','Debridement of mouth','','');
insert into b_icd9 values ('0555000000082','2332671','ผ่าตัดรอยโรคออกจากปาก','Excision of lesion of mouth','','');
insert into b_icd9 values ('0555000000083','2332870','จี้ทำลายรอยโรคบริเวณปาก','Destruction of lesion of mouth','','');
insert into b_icd9 values ('0555000000084','2333970','เย็บแผลในปาก','Suture of laceration of mouth','','');
insert into b_icd9 values ('0555000000085','2338070','ทำแผลในปาก','Wound dressing at mouth','','');
insert into b_icd9 values ('0555000000086','2338370','ล้างช่องปาก','Irrigation of mouth','','');
insert into b_icd9 values ('0555000000087','2381100','ถอดผ้าอุดห้ามเลือดที่ฟัน','Removal of dental packing','','');
insert into b_icd9 values ('0555000000088','2381101','ดึงลวดมัดฟันออก','Removal of dental wiring','','');
insert into b_icd9 values ('0555000000089','2381200','เปลี่ยนผ้าอุดห้ามเลือดที่ฟัน','Replacement of dental packing','','');
insert into b_icd9 values ('0555000000090','2381201','เปลี่ยนลวดมัดฟัน','Replacement of dental wiring','','');
insert into b_icd9 values ('0555000000091','2382770','ถอนฟันแท้','Extraction of permanent tooth','','');
insert into b_icd9 values ('0555000000092','2387179','การอุดฟันที่ไม่ระบุชนิดวัสดุอุด','Tooth filling, unspecified','','');
insert into b_icd9 values ('0555000000093','2387279','การรักษารากฟัน ไม่ระบุชนิด','Root canal therapy, unspecified','','');
insert into b_icd9 values ('0555000000094','2389999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณฟัน','Other procedures and operations on teeth','','');
insert into b_icd9 values ('0555000000095','2441171','ดึงวัตถุแปลกปลอมออกจากช่องคอโดยไม่ต้องผ่า','Removal of foreign body from pharynx without incision','','');
insert into b_icd9 values ('0555000000096','2449999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณช่องคอ','Other operation on pharynx','','');
insert into b_icd9 values ('0555000000097','2451171','ดึงวัตถุแปลกปลอมออกจากทอนซิลโดยไม่ต้องผ่า','Removal of foreign body from tonsil and adenoid without incision','','');
insert into b_icd9 values ('0555000000098','2459999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณทอนซิล','Other procedures and operations on tonsils and adenoid','','');
insert into b_icd9 values ('0555000000099','2501070','การใส่ท่อช่วยหายใจเข้าหลอดคอ','Endotracheal intubation','','');
insert into b_icd9 values ('0555000000100','2501170','การถอดท่อหลอดคอ','Removal of endotracheal tube','','');
insert into b_icd9 values ('0555000000101','2501270','การเปลี่ยนท่อหลอดคอ','Replacement of endotracheal tube','','');
insert into b_icd9 values ('0555000000102','2509999','หัตถการและการผ่าตัดอื่นๆที่หลอดคอ','Other procedures and operation on trachea','','');
insert into b_icd9 values ('0555000000103','2829999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณกระดูกนิ้วเท้า','Other procedures and operations on phalanges of foot','','');
insert into b_icd9 values ('0555000000104','2899999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณข้อสะโพก','Other procedures and operations on hip joint','','');
insert into b_icd9 values ('0555000000105','3010400','การตัดชิ้นเนื้อไปชันสูตรจากบริเวณอก','Biopsy of chest skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000106','3011170','ตัดไหมที่หน้าอก','Removal of sutures from chest skin','','');
insert into b_icd9 values ('0555000000107','3011171','ดึงวัตถุแปลกปลอมออกจากผิวหนัง และชั้นใต้ผิวหนังหน้าอก','Removal of foreign body from chest skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000108','3012270','เจาะดูดของเหลวจากบริเวณ ผิวหนัง และชั้นใต้ผิวหนังหน้าอก','Aspiration of chest skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000109','3012271','ผ่าระบายของเหลวจากบริเวณ ผิวหนัง และชั้นใต้ผิวหนังหน้าอก','Drainage of chest skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000110','3012670','ผ่าตัดเนื้อตายออกจากบริเวณ แผล บริเวณติดเชื้อ แผลไหม้ที่หน้าอก','Excisional debridement of wound, infection or burn of chest skin','','');
insert into b_icd9 values ('0555000000111','3012671','ผ่าตัดรอยโรคออกจาก ผิวหนัง และชั้นใต้ผิวหนังหน้าอก','Excision of lesion of chest skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000112','3012870','จี้ทำลายรอยโรคบริเวณ ผิวหนัง และชั้นใต้ผิวหนังหน้าอก','Destruction of lesion of chest skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000113','3013970','เย็บแผลบริเวณอก','Suture of laceration of chest skin','','');
insert into b_icd9 values ('0555000000114','3018070','ทำแผลที่อก','Wound dressing at chest','','');
insert into b_icd9 values ('0555000000115','3019999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณ ผิวหนัง และชั้นใต้ผิวหนังหน้าอก','Other procedures and operations on chest skin','','');
insert into b_icd9 values ('0555000000116','3020400','การตัดชิ้นเนื้อที่เต้านมไปชันสูตรโดยไม่ผ่าเปิด','Closed biopsy of breast','','');
insert into b_icd9 values ('0555000000117','3020401','การผ่าตัดนำชิ้นเนื้อที่เต้านมออกไปชันสูตร','Open biopsy of breast','','');
insert into b_icd9 values ('0555000000118','3021170','ตัดไหมที่เต้านม','Removal of sutures from breast','','');
insert into b_icd9 values ('0555000000119','3021171','ดึงวัตถุแปลกปลอมออกจากเต้านม','Removal of foreign body from breast','','');
insert into b_icd9 values ('0555000000120','3021500','รีดนมจากเต้านมมารดาที่อยู่ในช่วงให้นมบุตร','Extraction of milk from lactating breast','','');
insert into b_icd9 values ('0555000000121','3022270','เจาะดูดของเหลวจากบริเวณเต้านม','Aspiration of breast','','');
insert into b_icd9 values ('0555000000122','3022271','ผ่าระบายของเหลวจากบริเวณเต้านม','Drainage of breast','','');
insert into b_icd9 values ('0555000000123','3022670','ตัดเนื้อตายออกจากบริเวณเต้านม','Debridement of breast','','');
insert into b_icd9 values ('0555000000124','3022671','ผ่าตัดรอยโรคเฉพาะที่ออกจากเต้านม','Local excision of lesion of breast','','');
insert into b_icd9 values ('0555000000125','3022870','จี้ทำลายรอยโรคบริเวณเต้านม','Destruction of lesion of breast','','');
insert into b_icd9 values ('0555000000126','3023970','เย็บแผลที่เต้านม','Suture of laceration of breast','','');
insert into b_icd9 values ('0555000000127','3028070','ทำแผลที่เต้านม','Wound dressing on breast','','');
insert into b_icd9 values ('0555000000128','3029999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณเต้านม','Other procedures and operations on the breast','','');
insert into b_icd9 values ('0555000000129','3781170','การถอดท่อระบายออกจากช่องเยื่อหุ้มปอด','Removal of thoracotomy tube or pleural cavity drain','','');
insert into b_icd9 values ('0555000000130','3781270','การเปลี่ยนท่อระบายช่องเยื่อหุ้มปอด','Replacement of drainage tube of pleural cavity','','');
insert into b_icd9 values ('0555000000131','3782270','การใส่ท่อระบายช่องเยื่อหุ้มปอด','Insertion of intercostal catheter for drainage','','');
insert into b_icd9 values ('0555000000132','4010400','การตัดชิ้นเนื้อไปชันสูตรจากผิวหนังบริเวณหน้าท้อง','Biopsy of abdominal skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000133','4011170','ตัดไหมที่หน้าท้อง','Removal of sutures from abdominal skin','','');
insert into b_icd9 values ('0555000000134','4011171','ดึงวัตถุแปลกปลอมออกจากผิวหนัง และชั้นใต้ผิวหนังหน้าท้อง','Removal of foreign body from abdominal skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000135','4012270','เจาะดูดของเหลวจากบริเวณ ผิวหนัง และชั้นใต้ผิวหนังหน้าท้อง','Aspiration of abdominal skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000136','4012271','ผ่าระบายของเหลวจากบริเวณผิวหนัง และชั้นใต้ผิวหนังหน้าท้อง','Drainage of abdominal skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000137','4012670','ผ่าตัดเนื้อตายออกจากบริเวณ แผล บริเวณติดเชื้อ แผลไหม้ที่หน้าท้อง','Excisional debridement of wound, infection or burn of abdominal  skin','','');
insert into b_icd9 values ('0555000000138','4012671','ผ่าตัดรอยโรคออกจาก ผิวหนัง และชั้นใต้ผิวหนังหน้าท้อง','Excision of lesion of abdominal skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000139','4012870','จี้ทำลายรอยโรคบริเวณ ผิวหนัง และชั้นใต้ผิวหนังหน้าท้อง','Destruction of lesion of abdominal skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000140','4013970','เย็บแผลที่ท้อง','Suture of laceration of abdominal skin','','');
insert into b_icd9 values ('0555000000141','4018070','ทำแผลที่ท้อง','Wound dressing at abdomen','','');
insert into b_icd9 values ('0555000000142','4019999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณผิวหนัง และชั้นใต้ผิวหนังหน้าท้อง','Other procedures and operations of abdominal skin','','');
insert into b_icd9 values ('0555000000143','4411070','การใส่ท่อ nasogastric tube','Insertion of nasogastric tube','','');
insert into b_icd9 values ('0555000000144','4411170','การถอดท่อ nasogastric tube','Removal of nasogastric tube','','');
insert into b_icd9 values ('0555000000145','4411270','การเปลี่ยนท่อ nasogastric tube','Replacement of nasogastric tube','','');
insert into b_icd9 values ('0555000000146','4418370','การล้างท้อง','Gastric lavage','','');
insert into b_icd9 values ('0555000000147','4418371','การสวนล้างท่อ nasogastric tube','Other irrigation of nasogastric tube','','');
insert into b_icd9 values ('0555000000148','4419999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณกระเพาะ','Other procedures and operations on stomach','','');
insert into b_icd9 values ('0555000000149','4550000','การส่องกล้องตรวจทวารหนัก','Anoscopy','','');
insert into b_icd9 values ('0555000000150','4550401','การตัดชิ้นเนื้อบริเวณรอบก้นไปชันสูตร','Biopsy of perianal tissue','','');
insert into b_icd9 values ('0555000000151','4551170','การควักอุจจาระออกจากทวารหนัก','Removal of impacted feces','','');
insert into b_icd9 values ('0555000000152','4551171','ดึงวัตถุแปลกปลอมออกจากทวารหนักโดยไม่ต้องผ่า','Removal of foreign body from anus without incision','','');
insert into b_icd9 values ('0555000000153','4552271','ผ่าระบายฝีบริเวณรอบก้น','Drainage of perianal abscess','','');
insert into b_icd9 values ('0555000000154','4558370','การสวนอุจจาระ','Transanal enema','','');
insert into b_icd9 values ('0555000000155','4559999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณทวารหนัก','Other procedures and operations on anus','','');
insert into b_icd9 values ('0555000000156','4811070','การใส่ท่อระบายปัสสาวะแบบทิ้งคาไว้','Insertion of indwelling urinary catheter','','');
insert into b_icd9 values ('0555000000157','4811170','การถอดท่อระบายปัสสาวะ','Removal of indwelling urinary catheter','','');
insert into b_icd9 values ('0555000000158','4811270','การเปลี่ยนท่อระบายปัสสาวะแบบทิ้งคาไว้','Replacement of indwelling urinary catheter','','');
insert into b_icd9 values ('0555000000159','4818370','การสวนปัสสาวะ การล้างท่อระบายปัสสาวะ','Transurethral clearance of bladder','','');
insert into b_icd9 values ('0555000000160','4819999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณองคชาติ','Other procedures and operations on bladder','','');
insert into b_icd9 values ('0555000000161','5011170','ตัดไหมที่องคชาติ','Removal of sutures from penis','','');
insert into b_icd9 values ('0555000000162','5018070','ทำแผลที่องคชาติ','Wound dressing on penis','','');
insert into b_icd9 values ('0555000000163','5019999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณองคชาติ','Other procedures and operations on penis','','');
insert into b_icd9 values ('0555000000164','5031170','ตัดไหมที่ถุงอัณฑะ','Removal of sutures from scrotum','','');
insert into b_icd9 values ('0555000000165','5038070','ทำแผลบริเวณถุงอัณฑะ','Wound dressing on scrotum','','');
insert into b_icd9 values ('0555000000166','5039999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณถุงอัณฑะ','Other procedures and operations on scrotum and tunica vaginalis','','');
insert into b_icd9 values ('0555000000167','5161170','ตัดไหมที่อวัยวะเพศหญิง','Removal of suture from vulva','','');
insert into b_icd9 values ('0555000000168','5161171','ดึงวัตถุแปลกปลอมออกจากอวัยวะเพศหญิงโดยไม่ต้องผ่า','Non-operative removal of foreign body from vulva','','');
insert into b_icd9 values ('0555000000169','5162270','เจาะดูดของเหลวจากบริเวณอวัยวะเพศหญิง','Aspiration of vulva','','');
insert into b_icd9 values ('0555000000170','5162271','ผ่าระบายของเหลวจากบริเวณอวัยวะเพศหญิง','Drainage of vulva','','');
insert into b_icd9 values ('0555000000171','5162272','ผ่าระบายเลือดคั่งหลังคลอดจากบริเวณอวัยวะเพศหญิง','Drainage of obstetric hematoma of vulva','','');
insert into b_icd9 values ('0555000000172','5162273','เจาะดูดของเหลวจากบริเวณถุงน้ำหรือฝีบาร์โธลิน','Aspiration of Bartholin s cyst or abscess','','');
insert into b_icd9 values ('0555000000173','5162274','ผ่าระบายของเหลวจากบริเวณถุงน้ำหรือฝีบาร์โธลิน','Incision and drainage of Bartholin''s cyst or abscess','','');
insert into b_icd9 values ('0555000000174','5162275','การผ่าเปิดถุงน้ำหรือฝีบาร์โธลิน','Marsupialization of Bartholin''s cyst or abscess','','');
insert into b_icd9 values ('0555000000175','5162870','จี้ทำลายรอยโรคบริเวณอวัยวะเพศหญิง','Destruction of lesion of vulva','','');
insert into b_icd9 values ('0555000000176','5163970','เย็บแผลที่อวัยวะเพศหญิง','Suture of laceration of vulva','','');
insert into b_icd9 values ('0555000000177','5169999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณอวัยวะเพศหญิง','Other procedures and operations on vulva','','');
insert into b_icd9 values ('0555000000178','5170010','การส่องกล้องตรวจภายในช่องคลอด','Vaginoscopy','','');
insert into b_icd9 values ('0555000000179','5171170','ตัดไหมที่ช่องคลอด','Removal of suture from vagina','','');
insert into b_icd9 values ('0555000000180','5171172','การดึงวัตถุแปลกปลอมออกจากช่องคลอดโดยไม่ผ่า','Non-operative removal of foreign body from vagina','','');
insert into b_icd9 values ('0555000000181','5171270','การเปลี่ยนผ้าอุดห้ามเลือดในช่องคลอด','Replacement of vaginal packing','','');
insert into b_icd9 values ('0555000000182','5171770','การใส่ผ้าอุดห้ามเลือดในช่องคลอด','Vaginal packing','','');
insert into b_icd9 values ('0555000000183','5179999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณช่องคลอด','Other procedures and operations on vagina','','');
insert into b_icd9 values ('0555000000184','5191070','การใส่ห่วงอนามัยคุมกำเนิด','Insertion of intrauterine contraceptive device','','');
insert into b_icd9 values ('0555000000185','5191170','การถอดห่วงอนามัยคุมกำเนิด','Removal of intrauterine contraceptive device','','');
insert into b_icd9 values ('0555000000186','5199999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณมดลูก','Other procedures and operations on uterus','','');
insert into b_icd9 values ('0555000000187','5265410','การทำคลอดโดยไม่ต้องตัดฝีเย็บ','Normal delivery without episiotomy','','');
insert into b_icd9 values ('0555000000188','5265411','การทำคลอดร่วมกับการตัดฝีเย็บ','Normal delivery with episiotomy','','');
insert into b_icd9 values ('0555000000189','5300400','การตัดชิ้นเนื้อไปชันสูตรจากผิวหนังของหนังส่วนบน','Biopsy of upper back skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000190','5301170','ตัดไหมที่หลังส่วนบน','Removal of sutures on upper back skin','','');
insert into b_icd9 values ('0555000000191','5301171','ดึงวัตถุแปลกปลอมออกจากผิวหนังของหลังส่วนบน','Removal of foreign body from upper back skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000192','5301770','พันแผลโดยใช้แรงกดเพื่อห้ามเลือดบริเวณผิวหนังของหลังส่วนบน','Application of pressure dressing on upper back skin','','');
insert into b_icd9 values ('0555000000193','5302270','เจาะดูดของเหลวจากบริเวณผิวหนังของหลังส่วนบน','Aspiration of upper back skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000194','5302271','ผ่าระบายของเหลวจากบริเวณผิวหนังของหลังส่วนบน','Drainage of upper back skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000195','5302670','ผ่าตัดเนื้อตายออกจากบริเวณ แผล บริเวณติดเชื้อ แผลไหม้ ที่ผิวหนังบริเวณหลังส่วนบน','Excisional debridement of wound, infection or burn of upper back skin','','');
insert into b_icd9 values ('0555000000196','5302671','การตัดรอยโรคออกจากผิวหนังของหลังส่วนบน','Excision of lesion of upper back skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000197','5302870','จี้ทำลายรอยโรคบริเวณผิวหนังของหลังส่วนบน และชั้นใต้ผิวหนัง','Destruction of lesion of upper back skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000198','5303970','เย็บแผลที่หลังส่วนบน','Suture of laceration of upper back skin','','');
insert into b_icd9 values ('0555000000199','5308070','ทำแผลที่หลังส่วนบน','Wound dressing on upper back skin','','');
insert into b_icd9 values ('0555000000200','5309999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณผิวหนังของหลังส่วนบน','Other procedures and operations on upper back skin','','');
insert into b_icd9 values ('0555000000201','5310400','การตัดชิ้นเนื้อไปชันสูตรจากผิวหนังของหนังส่วนล่าง','Biopsy of lower back skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000202','5311170','ตัดไหมที่หลังส่วนล่าง','Removal of sutures on lower back skin','','');
insert into b_icd9 values ('0555000000203','5311171','ดึงวัตถุแปลกปลอมออกจากผิวหนังของหลังส่วนล่าง','Removal of foreign body from lower back skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000204','5311770','พันแผลโดยใช้แรงกดเพื่อห้ามเลือดบริเวณผิวหนังของหลังส่วนล่าง','Application of pressure dressing on lower back skin','','');
insert into b_icd9 values ('0555000000205','5312270','เจาะดูดของเหลวจากบริเวณผิวหนังของหลังส่วนล่าง','Aspiration of lower back skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000206','5312271','ผ่าระบายของเหลวจากบริเวณผิวหนังของหลังส่วนล่าง','Drainage of lower back skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000207','5312670','ผ่าตัดเนื้อตายออกจากบริเวณ แผล บริเวณติดเชื้อ แผลไหม้ที่ผิวหนังของหลังส่วนล่าง','Excisional debridement of wound, infection or burn of lower back skin','','');
insert into b_icd9 values ('0555000000208','5312671','การตัดรอยโรคออกจากผิวหนังของหลังส่วนล่าง','Excision of lesion of lower back skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000209','5312870','จี้ทำลายรอยโรคบริเวณ ผิวหนังของหลังส่วนล่างและชั้นใต้ผิวหนัง','Destruction of lesion of lower back skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000210','5313970','เย็บแผลที่หลังส่วนล่าง','Suture of laceration of lower back skin','','');
insert into b_icd9 values ('0555000000211','5318070','ทำแผลที่หลังส่วนล่าง','Wound dressing on lower back skin','','');
insert into b_icd9 values ('0555000000212','5319999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณผิวหนังของหลังส่วนล่าง','Other procedures and operations on lower back skin','','');
insert into b_icd9 values ('0555000000213','5320400','การตัดชิ้นเนื้อไปชันสูตรจากผิวหนังของแก้มก้น','Biopsy of buttock skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000214','5321170','ตัดไหมที่แก้มก้น','Removal of sutures on buttock skin','','');
insert into b_icd9 values ('0555000000215','5321171','ดึงวัตถุแปลกปลอมออกจากผิวหนังแก้มก้น','Removal of foreign body from buttock skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000216','5321770','พันแผลโดยใช้แรงกดเพื่อห้ามเลือดบริเวณผิวหนังแก้มก้น','Application of pressure dressing on buttock skin','','');
insert into b_icd9 values ('0555000000217','5322270','เจาะดูดของเหลวจากบริเวณผิวหนังแก้มก้น','Aspiration of buttock skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000218','5322271','ผ่าระบายของเหลวจากบริเวณผิวหนังแก้มก้น','Drainage of buttock skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000219','5322670','ผ่าตัดเนื้อตายออกจากบริเวณแผล บริเวณติดเชื้อ แผลไหม้ที่ผิวหนังแก้มก้น','Excisional debridement of wound, infection or burn of buttock skin','','');
insert into b_icd9 values ('0555000000220','5322671','การตัดรอยโรคออกจากผิวหนังแก้มก้น','Excision of lesion of buttock skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000221','5322870','จี้ทำลายรอยโรคบริเวณ ผิวหนังแก้มก้น และชั้นใต้ผิวหนัง','Destruction of lesion of buttock skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000222','5323970','เย็บแผลที่แก้มก้น','Suture of laceration of buttock skin','','');
insert into b_icd9 values ('0555000000223','5328070','ทำแผลที่แก้มก้น','Wound dressing on buttock skin','','');
insert into b_icd9 values ('0555000000224','5329999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณผิวหนังแก้มก้น','Other procedures and operations on buttock skin','','');
insert into b_icd9 values ('0555000000225','5581370','การดามกระดูกสันหลัง','Nonoperative immobilization of vertebra','','');
insert into b_icd9 values ('0555000000226','6010400','การตัดชิ้นเนื้อไปชันสูตรจากผิวหนังบริเวณไหล่','Biopsy of shoulder skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000227','6011170','ตัดไหมที่ไหล่','Removal of sutures on shoulder skin','','');
insert into b_icd9 values ('0555000000228','6011171','ดึงวัตถุแปลกปลอมออกจาก ผิวหนัง และชั้นใต้ผิวหนังไหล่','Removal of foreign body from shoulder skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000229','6011770','พันแผลโดยใช้แรงกดเพื่อห้ามเลือดบริเวณไหล่','Application of pressure dressing on shoulder skin','','');
insert into b_icd9 values ('0555000000230','6012270','เจาะดูดของเหลวจากรอยโรคบริเวณ ผิวหนัง และชั้นใต้ผิวหนังไหล่','Aspiration of lesions in shoulder skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000231','6012271','ผ่าระบายของเหลวจากบริเวณ ผิวหนัง และชั้นใต้ผิวหนังไหล่','Drainage of shoulder skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000232','6012670','ผ่าตัดเนื้อตายออกจากบริเวณ แผล บริเวณติดเชื้อ แผลไหม้ ที่ไหล่','Excisional debridement of wound, infection or burn of shoulder skin','','');
insert into b_icd9 values ('0555000000233','6012671','ผ่าตัดรอยโรคออกจากผิวหนัง และชั้นใต้ผิวหนังไหล่','Excision of lesion of shoulder skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000234','6012870','จี้ทำลายรอยโรคบริเวณผิวหนัง และชั้นใต้ผิวหนังไหล่','Destruction of lesion of shoulder skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000235','6013970','เย็บแผลที่ไหล่','Suture of laceration of shoulder skin','','');
insert into b_icd9 values ('0555000000236','6018070','ทำแผลที่ไหล่','Wound dressing on shoulder skin','','');
insert into b_icd9 values ('0555000000237','6019999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณผิวหนังไหล่','Other procedures and operations on shoulder skin','','');
insert into b_icd9 values ('0555000000238','6020400','การตัดชิ้นเนื้อไปชันสูตรบริเวณผิวหนังแขนส่วนบน','Biopsy of upper arm skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000239','6021170','ตัดไหมที่แขนส่วนบน','Removal of sutures on upper arm skin','','');
insert into b_icd9 values ('0555000000240','6021171','ดึงวัตถุแปลกปลอมออกจากผิวหนัง และชั้นใต้ผิวหนังแขนส่วนบน','Removal of foreign body from upper arm skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000241','6021770','พันแผลโดยใช้แรงกดเพื่อห้ามเลือดบริเวณผิวหนังแขนส่วนบน','Application of pressure dressing on upper arm skin','','');
insert into b_icd9 values ('0555000000242','6021771','การขันชะเนาะที่แขนส่วนบน','Application of tourniquet at upper arm','','');
insert into b_icd9 values ('0555000000243','6022270','เจาะดูดของเหลวจากชรอยโรคบริเวณชผิวหนัง และชั้นใต้ผิวหนังแขนส่วนบน','Aspiration of lesions in upper arm skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000244','6022271','ผ่าระบายของเหลวจากบริเวณผิวหนัง และชั้นใต้ผิวหนังแขนส่วนบน','Drainage of upper arm skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000245','6022670','ผ่าตัดเนื้อตายออกจากบริเวณ แผล บริเวณติดเชื้อ แผลไหม้ ที่แขนส่วนบน','Excisional debridement of wound, infection or burn of upper arm skin','','');
insert into b_icd9 values ('0555000000246','6022671','ผ่าตัดรอยโรคออกจาก ผิวหนัง และชั้นใต้ผิวหนังแขนส่วนบน','Excision of lesion of upper arm skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000247','6022870','จี้ทำลายรอยโรคบริเวณผิวหนัง และชั้นใต้ผิวหนังแขนส่วนบน','Destruction of lesion of upper arm skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000248','6023970','เย็บแผลที่แขนส่วนบน','Suture of laceration of upper arm skin','','');
insert into b_icd9 values ('0555000000249','6028070','ทำแผลที่แขนส่วนบน','Wound dressing on upper arm skin','','');
insert into b_icd9 values ('0555000000250','6029999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณผิวหนังแขนส่วนบน','Other procedures and operations on upper arm skin','','');
insert into b_icd9 values ('0555000000251','6030400','การตัดชิ้นเนื้อไปชันสูตรบริเวณข้อศอก','Biopsy of elbow skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000252','6031170','ตัดไหมที่ข้อศอก','Removal of sutures on elbow skin','','');
insert into b_icd9 values ('0555000000253','6031171','ดึงวัตถุแปลกปลอมออกจากชผิวหนัง และชั้นใต้ผิวหนังข้อศอก','Removal of foreign body from elbow skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000254','6031770','พันแผลโดยใช้แรงกดเพื่อห้ามเลือดบริเวณข้อศอก','Application of pressure dressing on elbow skin','','');
insert into b_icd9 values ('0555000000255','6032270','เจาะดูดของเหลวจากรอยโรคบริเวณผิวหนัง และชั้นใต้ผิวหนังข้อศอก','Aspiration of lesions in elbow skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000256','6032271','ผ่าระบายของเหลวจากบริเวณผิวหนัง และชั้นใต้ผิวหนังข้อศอก','Drainage of elbow skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000257','6032670','ผ่าตัดเนื้อตายออกจากบริเวณ แผล บริเวณติดเชื้อ แผลไหม้ที่ข้อศอก','Excisional debridement of wound, infection or burn of elbow skin','','');
insert into b_icd9 values ('0555000000258','6032671','ผ่าตัดรอยโรคออกจากผิวหนัง และชั้นใต้ผิวหนัง ข้อศอก','Excision of lesion of elbow skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000259','6032870','จี้ทำลายรอยโรคบริเวณผิวหนัง และชั้นใต้ผิวหนังข้อศอก','Destruction of lesion of elbow skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000260','6033970','เย็บแผลที่ข้อศอก','Suture of laceration of elbow skin','','');
insert into b_icd9 values ('0555000000261','6038070','ทำแผลที่ข้อศอก','Wound dressing on elbow skin','','');
insert into b_icd9 values ('0555000000262','6039999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณผิวหนังข้อศอก','Other procedures and operations on elbow skin','','');
insert into b_icd9 values ('0555000000263','6040400','การตัดชิ้นเนื้อไปชันสูตรจากแขนท่อนล่าง','Biopsy of forearm skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000264','6041170','ตัดไหมที่แขนท่อนล่าง','Removal of sutures on forearm skin','','');
insert into b_icd9 values ('0555000000265','6041171','ขันชะเนาะที่แขนท่อนล่าง','Application of tourniquet at forearm','','');
insert into b_icd9 values ('0555000000266','6041770','พันแผลโดยใช้แรงกดเพื่อห้ามเลือดบริเวณแขนท่อนล่าง','Application of pressure dressing on forearm skin','','');
insert into b_icd9 values ('0555000000267','6042270','เจาะดูดของเหลวจากรอยโรคบริเวณผิวหนัง และชั้นใต้ผิวหนังแขนท่อนล่าง','Aspiration of lesions in forearm skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000268','6042271','ผ่าระบายของเหลวจากบริเวณ ผิวหนัง และชั้นใต้ผิวหนังแขนท่อนล่าง','Drainage of forearm skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000269','6042670','ผ่าตัดเนื้อตายออกจากบริเวณ แผล บริเวณติดเชื้อ แผลไหม้ ที่แขนท่อนล่าง','Excisional debridement of wound, infection or burn of forearm skin','','');
insert into b_icd9 values ('0555000000270','6042671','ผ่าตัดรอยโรคออกจากผิวหนัง และชั้นใต้ผิวหนังแขนท่อนล่าง','Excision of lesion of forearm skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000271','6042870','จี้ทำลายรอยโรคบริเวณผิวหนัง และชั้นใต้ผิวหนังแขนท่อนล่าง','Destruction of lesion of forearm skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000272','6043970','เย็บแผลที่แขนท่อนล่าง','Suture of laceration of forearm skin','','');
insert into b_icd9 values ('0555000000273','6048070','ทำแผลที่แขนท่อนล่าง','Wound dressing on forearm skin','','');
insert into b_icd9 values ('0555000000274','6049999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณผิวหนังแขนท่อนล่าง','Other procedures and operations on forearm skin','','');
insert into b_icd9 values ('0555000000275','6050400','การตัดชิ้นเนื้อไปชันสูตรจากผิวหนังข้อมือ','Biopsy of wrist skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000276','6051170','ตัดไหมที่ข้อมือ','Removal of sutures on wrist skin','','');
insert into b_icd9 values ('0555000000277','6051171','ดึงวัตถุแปลกปลอมออกจากผิวหนัง และชั้นใต้ผิวหนังข้อมือ','Removal of foreign body from wrist skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000278','6051770','พันแผลโดยใช้แรงกดเพื่อห้ามเลือดบริเวณผิวหนังข้อมือ','Application of pressure dressing on wrist skin','','');
insert into b_icd9 values ('0555000000279','6052270','เจาะดูดของเหลวจากบริเวณ รอยโรคบริเวณผิวหนัง และชั้นใต้ผิวหนังข้อมือ','Aspiration of lesions in wrist skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000280','6052271','ผ่าระบายของเหลวจากบริเวณ ผิวหนัง และชั้นใต้ผิวหนังข้อมือ','Drainage of wrist skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000281','6052670','ผ่าตัดเนื้อตายออกจากบริเวณ แผล บริเวณติดเชื้อ แผลไหม้ ที่ข้อมือ','Excisional debridement of wound, infection or burn of wrist skin','','');
insert into b_icd9 values ('0555000000282','6052671','ผ่าตัดรอยโรคออกจาก ผิวหนัง และชั้นใต้ผิวหนังข้อมือ','Excision of lesion of wrist skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000283','6052870','จี้ทำลายรอยโรคบริเวณผิวหนัง และชั้นใต้ผิวหนังข้อมือ','Destruction of lesion of wrist skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000284','6053970','เย็บแผลที่ข้อมือ','Suture of laceration of wrist skin','','');
insert into b_icd9 values ('0555000000285','6058070','ทำแผลที่ข้อมือ','Wound dressing on wrist skin','','');
insert into b_icd9 values ('0555000000286','6059999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณผิวหนังข้อมือ','Other procedures and operations on wrist skin','','');
insert into b_icd9 values ('0555000000287','6060400','การตัดชิ้นเนื้อไปชันสูตรจากผิวหนังบริเวณมือ','Biopsy of hand skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000288','6061170','ตัดไหมที่มือ','Removal of sutures on hand skin','','');
insert into b_icd9 values ('0555000000289','6061171','ดึงวัตถุแปลกปลอมออกจากผิวหนัง และชั้นใต้ผิวหนังมือ','Removal of foreign body from hand skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000290','6061770','พันแผลโดยใช้แรงกดเพื่อห้ามเลือดบริเวณมือ','Application of pressure dressing on hand skin','','');
insert into b_icd9 values ('0555000000291','6062270','เจาะดูดของเหลวจากรอยโรคบริเวณผิวหนัง และชั้นใต้ผิวหนังมือ','Aspiration of lesions in hand skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000292','6062271','ผ่าระบายของเหลวจากบริเวณ ผิวหนัง และชั้นใต้ผิวหนังมือ','Drainage of hand skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000293','6062670','ผ่าตัดเนื้อตายออกจากบริเวณ แผล บริเวณติดเชื้อ แผลไหม้ ที่มือ','Excisional debridement of wound, infection or burn of hand skin','','');
insert into b_icd9 values ('0555000000294','6062671','ผ่าตัดรอยโรคออกจาก ผิวหนัง และชั้นใต้ผิวหนังมือ','Excision of lesion of hand skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000295','6062870','จี้ทำลายรอยโรคบริเวณ ผิวหนัง และชั้นใต้ผิวหนังมือ','Destruction of lesion of hand skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000296','6063970','เย็บแผลที่มือ','Suture of laceration of hand skin','','');
insert into b_icd9 values ('0555000000297','6068070','ทำแผลที่มือ','Wound dressing on hand skin','','');
insert into b_icd9 values ('0555000000298','6069999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณผิวหนังมือ','Other procedures and operations on hand skin','','');
insert into b_icd9 values ('0555000000299','6491370','การดามกระดูกไหปลาร้า','Nonoperative immobilization of clavicle','','');
insert into b_icd9 values ('0555000000300','6491371','การใส่เผือกอ่อนที่กระดูกไหปลาร้า','Application of slab for clavicle injury','','');
insert into b_icd9 values ('0555000000301','6491372','การใส่เผือกที่กระดูกไหปลาร้า','Application of cast for clavicle injury','','');
insert into b_icd9 values ('0555000000302','6499999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณกระดูกไหปลาร้า','Other procedures and operations on clavicle','','');
insert into b_icd9 values ('0555000000303','6541370','การดามกระดูกต้นแขน','Nonoperative immobilization of humerus','','');
insert into b_icd9 values ('0555000000304','6541371','การใส่เฝือกอ่อนกระดูกต้นแขน','Application of slab for humerus injury','','');
insert into b_icd9 values ('0555000000305','6541372','การใส่เฝือกที่กระดูกต้นแขน','Application of cast for humerus  injury','','');
insert into b_icd9 values ('0555000000306','6549999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณกระดูกต้นแขน','Other procedures and operations on humerus','','');
insert into b_icd9 values ('0555000000307','6811370','การดามกระดูกแขนท่อนล่าง','Nonoperative immobilization of forearm bone','','');
insert into b_icd9 values ('0555000000308','6811371','การใส่เฝือกอ่อนที่กระดูกแขนท่อนล่าง','Application of slab for forearm bone injury','','');
insert into b_icd9 values ('0555000000309','6811372','การใส่เฝือกที่กระดูกแขนท่อนล่าง','Application of cast for forearm bone injury','','');
insert into b_icd9 values ('0555000000310','6819999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณกระดูกแขนท่อนล่าง','Other procedures and operations on forearm bone','','');
insert into b_icd9 values ('0555000000311','6901370','การดามกระดูกมือ','Nonoperative immobilization of metacarpal bones','','');
insert into b_icd9 values ('0555000000312','6901371','การใส่เฝือกอ่อนที่กระดูกมือ','Application of slab for metacarpal bones injury','','');
insert into b_icd9 values ('0555000000313','6901372','การใส่เฝือกที่กระดูกมือ','Application of cast for metacarpal bones injury','','');
insert into b_icd9 values ('0555000000314','6909999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณกระดูกนิ้วมือ','Other procedures and operations on metacarpal bones','','');
insert into b_icd9 values ('0555000000315','6941370','การดามกระดูกนิ้วมือ','Nonoperative immobilization of finger bones','','');
insert into b_icd9 values ('0555000000316','6941371','การใส่เฝือกอ่อนที่กระดูกนิ้วมือ','Application of slab for finger bones injury','','');
insert into b_icd9 values ('0555000000317','6941372','การใส่เฝือกที่กระดูกนิ้วมือ','Application of cast for finger bones injury','','');
insert into b_icd9 values ('0555000000318','6949999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณกระดูกนิ้วมือ','Other procedures and operations on finger bones','','');
insert into b_icd9 values ('0555000000319','6971370','การดามข้อไหล่','Nonoperative immobilization of shoulder joint','','');
insert into b_icd9 values ('0555000000320','6971371','การใส่เฝือกอ่อนที่ข้อไหล่','Application of slab for shoulder joint injury','','');
insert into b_icd9 values ('0555000000321','6971372','การใส่เฝือกที่ข้อไหล่','Application of cast for shoulder joint injury','','');
insert into b_icd9 values ('0555000000322','6979999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณข้อไหล่','Other procedures and operations on shoulder joint','','');
insert into b_icd9 values ('0555000000323','6981370','การดามข้อศอก','Nonoperative immobilization of elbow joint','','');
insert into b_icd9 values ('0555000000324','6981371','การใส่เฝือกอ่อนที่ข้อศอก','Application of slab for elbow joint injury','','');
insert into b_icd9 values ('0555000000325','6981372','การใส่เฝือกที่ข้อศอก','Application of cast for elbow joint injury','','');
insert into b_icd9 values ('0555000000326','6989999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณข้อศอก','Other procedures and operations on elbow joint','','');
insert into b_icd9 values ('0555000000327','7021370','การดามข้อมือ','Nonoperative immobilization of wrist joint','','');
insert into b_icd9 values ('0555000000328','7021371','การใส่เฝือกอ่อนที่ข้อมือ','Application of slab for wrist joint injury','','');
insert into b_icd9 values ('0555000000329','7021372','การใส่เฝือกที่ข้อมือ','Application of cast for wrist joint injury','','');
insert into b_icd9 values ('0555000000330','7029999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณข้อมือ','Other procedures and operations on wrist joint','','');
insert into b_icd9 values ('0555000000331','7041370','การดามข้อบริเวณโคนนิ้วมือ','Nonoperative immobilization of metacarpophalangeal joint','','');
insert into b_icd9 values ('0555000000332','7041371','การใส่เฝือกอ่อนที่ข้อบริเวณโคนนิ้วมือ','Application of slab for metacarpophalangeal joint injury','','');
insert into b_icd9 values ('0555000000333','7041372','การใส่เฝือกที่ข้อบริเวณโคนนิ้วมือ','Application of cast for metacarpophalangeal joint injury','','');
insert into b_icd9 values ('0555000000334','7049999','การทำหัตถการและการผ่าตัดอื่นๆที่ข้อบริเวณโคนนิ้วมือ','Other procedures and operations on metacarpophalangeal joint','','');
insert into b_icd9 values ('0555000000335','7270400','การตัดชิ้นเนื้อไปชันสูตรจากนิ้วหัวแม่มือ','Biopsy of thumb','','');
insert into b_icd9 values ('0555000000336','7271170','ตัดไหมที่นิ้วหัวแม่มือ','Removal of sutures on thumb','','');
insert into b_icd9 values ('0555000000337','7271171','ดึงวัตถุแปลกปลอมออกจากนิ้วหัวแม่มือ','Removal of foreign body from thumb','','');
insert into b_icd9 values ('0555000000338','7271770','พันแผลโดยใช้แรงกดเพื่อห้ามเลือดบริเวณนิ้วหัวแม่มือ','Application of pressure dressing on thumb','','');
insert into b_icd9 values ('0555000000339','7272270','เจาะดูดของเหลวจากรอยโรคบริเวณนิ้วหัวแม่มือ','Aspiration of lesions in thumb','','');
insert into b_icd9 values ('0555000000340','7272271','ผ่าระบายของเหลวจากบริเวณนิ้วหัวแม่มือ','Drainage of thumb','','');
insert into b_icd9 values ('0555000000341','7272670','ผ่าตัดเนื้อตายออกจากบริเวณ แผล บริเวณติดเชื้อ แผลไหม้ ที่นิ้วหัวแม่มือ','Excisional debridement of wound, infection or burn of thumb','','');
insert into b_icd9 values ('0555000000342','7272671','ผ่าตัดรอยโรคออกจากนิ้วหัวแม่มือ','Excision of lesion of thumb','','');
insert into b_icd9 values ('0555000000343','7272770','ถอดเล็บนิ้วหัวแม่มือ','Removal of nail, nailbed or nailfold of thumb','','');
insert into b_icd9 values ('0555000000344','7272870','จี้ทำลายรอยโรคบริเวณนิ้วหัวแม่มือ','Destruction of lesion of thumb','','');
insert into b_icd9 values ('0555000000345','7273970','เย็บแผลที่นิ้วหัวแม่มือ','Suture of laceration of thumb','','');
insert into b_icd9 values ('0555000000346','7278070','ทำแผลที่นิ้วหัวแม่มือ','Wound dressing on thumb','','');
insert into b_icd9 values ('0555000000347','7279999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณนิ้วหัวแม่มือ','Other procedures and operations on thumb','','');
insert into b_icd9 values ('0555000000348','7280400','การตัดชิ้นเนื้อไปชันสูตรจากนิ้วมือ ไม่รวมนิ้วหัวแม่มือ','Biopsy of finger(s)','','');
insert into b_icd9 values ('0555000000349','7281170','ตัดไหมที่นิ้วมือ ไม่รวมนิ้วหัวแม่มือ','Removal of sutures on finger(s)','','');
insert into b_icd9 values ('0555000000350','7281171','ดึงวัตถุแปลกปลอมออกจากนิ้วมือ ไม่รวมนิ้วหัวแม่มือ','Removal of foreign body from finger(s)','','');
insert into b_icd9 values ('0555000000351','7281770','พันแผลโดยใช้แรงกดเพื่อห้ามเลือดบริเวณนิ้วมือ ไม่รวมนิ้วหัวแม่มือ','Application of pressure dressing on finger(s)','','');
insert into b_icd9 values ('0555000000352','7282170','การเจาะหรือกรีดบริเวณนิ้วมือ ไม่รวมนิ้วหัวแม่มือ','Incision of finger(s) except thumb','','');
insert into b_icd9 values ('0555000000353','7282270','เจาะดูดของเหลวจากรอยโรคบริเวณนิ้วมือไม่รวมนิ้วหัวแม่มือ','Aspiration of lesions in finger(s)','','');
insert into b_icd9 values ('0555000000354','7282271','ผ่าระบายของเหลวจากบริเวณนิ้วมือ ไม่รวมนิ้วหัวแม่มือ','Drainage of finger(s)','','');
insert into b_icd9 values ('0555000000355','7282670','ผ่าตัดเนื้อตายออกจากบริเวณ แผล บริเวณติดเชื้อ แผลไหม้ ที่นิ้วมือ','Excisional debridement of wound, infection or burn of finger(s)','','');
insert into b_icd9 values ('0555000000356','7282671','ผ่าตัดรอยโรคออกจากนิ้วมือ ไม่รวมนิ้วหัวแม่มือ','Excision of lesion of finger(s)','','');
insert into b_icd9 values ('0555000000357','7282770','ถอดเล็บนิ้วมือ ไม่รวมนิ้วหัวแม่มือ','Removal of nail, nailbed or nailfold of finger(s)','','');
insert into b_icd9 values ('0555000000358','7282870','จี้ทำลายรอยโรคบริเวณนิ้วมือ ไม่รวมนิ้วหัวแม่มือ','Destruction of lesion of finger(s)','','');
insert into b_icd9 values ('0555000000359','7283970','เย็บแผลที่นิ้วมือ ไม่รวมนิ้วหัวแม่มือ','Suture of laceration of finger(s)','','');
insert into b_icd9 values ('0555000000360','7288070','ทำแผลที่นิ้วมือ ไม่รวมนิ้วหัวแม่มือ','Wound dressing on finger(s)','','');
insert into b_icd9 values ('0555000000361','7289999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณ นิ้วมือ ไม่รวมนิ้วหัวแม่มือ','Other procedures and operations on finger(s)','','');
insert into b_icd9 values ('0555000000362','7320400','การตัดชิ้นเนื้อไปชันสูตรจากบริเวณสะโพกและต้นขาส่วนบน','Biopsy of hip and upper thigh skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000363','7321170','ตัดไหมที่สะโพกและต้นขาส่วนบน','Removal of sutures on hip and upper thigh skin','','');
insert into b_icd9 values ('0555000000364','7321171','ดึงวัตถุแปลกปลอมออกจาก ผิวหนัง และชั้นใต้ผิวหนัง สะโพก และ ต้นขาส่วนบน','Removal of foreign body from hip and upper thigh skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000365','7321770','พันแผลโดยใช้แรงกดเพื่อห้ามเลือดบริเวณ สะโพก และ ต้นขาส่วนบน','Application of pressure dressing on hip and upper thigh skin','','');
insert into b_icd9 values ('0555000000366','7321771','ขันชะเนาะที่ต้นขา','Application of tourniquet at upper thigh','','');
insert into b_icd9 values ('0555000000367','7322270','เจาะดูดของเหลวจากบริเวณ ผิวหนัง และชั้นใต้ผิวหนัง สะโพก และ ต้นขาส่วนบน','Aspiration of lesions in hip and upper thigh skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000368','7322271','ผ่าระบายของเหลวจากบริเวณ ผิวหนัง และชั้นใต้ผิวหนังสะโพกและต้นขาส่วนบน','Drainage of hip and upper thigh skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000369','7322670','ผ่าตัดเนื้อตายออกจากบริเวณ แผล บริเวณติดเชื้อ แผลไหม้ ที่สะโพกและต้นขาส่วนบน','Excisional debridement of wound, infection or burn of hip and upper thigh skin','','');
insert into b_icd9 values ('0555000000370','7322671','ผ่าตัดรอยโรคออกจากผิวหนัง และชั้นใต้ผิวหนัง สะโพกและต้นขาส่วนบน','Excision of lesion of hip and upper thigh skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000371','7322870','จี้ทำลายรอยโรคผิวหนัง และชั้นใต้ผิวหนัง สะโพกและต้นขาส่วนบน','Destruction of lesion of hip and upper thigh skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000372','7323970','เย็บแผลที่สะโพกและต้นขาส่วนบน','Suture of laceration of hip and upper thigh skin','','');
insert into b_icd9 values ('0555000000373','7328070','ทำแผลที่ สะโพกและต้นขาส่วนบน','Wound dressing on hip and upper thigh skin','','');
insert into b_icd9 values ('0555000000374','7329999','หัตถการและการทำผ่าตัดอื่นๆที่ผิวหนัง สะโพกและต้นขาส่วนบน','Other procedures and operations on hip and upper thigh skin','','');
insert into b_icd9 values ('0555000000375','7330400','การตัดชิ้นเนื้อไปชันสูตรจากต้นขาส่วนเหนือเข่า','Biopsy of lower thigh skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000376','7331170','ตัดไหมที่ต้นขาส่วนเหนือเข่า','Removal of sutures on lower thigh skin','','');
insert into b_icd9 values ('0555000000377','7331171','ดึงวัตถุแปลกปลอมออกจากผิวหนัง และชั้นใต้ผิวหนังต้นขาส่วนเหนือเข่า','Removal of foreign body from lower thigh skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000378','7331770','พันแผลโดยใช้แรงกดเพื่อห้ามเลือดบริเวณผิวหนังต้นขาส่วนเหนือเข่า','Application of pressure dressing on lower thigh skin','','');
insert into b_icd9 values ('0555000000379','7332270','เจาะดูดของเหลวจากบริเวณ ผิวหนัง และชั้นใต้ผิวหนังต้นขาส่วนเหนือเข่า','Aspiration of lesions in lower thigh skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000380','7332271','ผ่าระบายของเหลวจากบริเวณ ผิวหนัง และชั้นใต้ผิวหนังต้นขาส่วนเหนือเข่า','Drainage of lower thigh skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000381','7332670','ผ่าตัดเนื้อตายออกจากบริเวณ แผล บริเวณติดเชื้อ แผลไหม้ ที่ต้นขาส่วนเหนือเข่า','Excisional debridement of wound, infection or burn of lower thigh skin','','');
insert into b_icd9 values ('0555000000382','7332671','ผ่าตัดรอยโรคออกจาก ผิวหนัง และชั้นใต้ผิวหนังต้นขาส่วนเหนือเข่า','Excision of lesion of lower thigh skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000383','7332870','จี้ทำลายรอยโรคบริเวณ ผิวหนัง และชั้นใต้ผิวหนังต้นขาส่วนเหนือเข่า','Destruction of lesion of lower thigh skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000384','7333970','เย็บแผลที่ต้นขาส่วนเหนือเข่า','Suture of laceration of lower thigh skin','','');
insert into b_icd9 values ('0555000000385','7338070','ทำแผลที่ต้นขาส่วนเหนือเข่า','Wound dressing on lower thigh skin','','');
insert into b_icd9 values ('0555000000386','7339999','หัตถการและการทำผ่าตัดอื่นๆที่ผิวหนังต้นขาส่วนเหนือเข่า','Other procedures and operations on lower thigh skin','','');
insert into b_icd9 values ('0555000000387','7340400','การตัดชิ้นเนื้อไปชันสูตรจากเข่า','Biopsy of knee skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000388','7341170','ตัดไหมที่เข่า','Removal of sutures on knee skin','','');
insert into b_icd9 values ('0555000000389','7341171','ดึงวัตถุแปลกปลอมออกจากผิวหนัง และชั้นใต้ผิวหนังเข่า','Removal of foreign body from knee skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000390','7341770','พันแผลโดยใช้แรงกดเพื่อห้ามเลือดบริเวณเข่า','Application of pressure dressing on knee skin','','');
insert into b_icd9 values ('0555000000391','7342270','เจาะดูดของเหลวจากบริเวณผิวหนัง และชั้นใต้ผิวหนังเข่า','Aspiration of lesions in knee skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000392','7342271','ผ่าระบายของเหลวจากบริเวณผิวหนัง และชั้นใต้ผิวหนังเข่า','Drainage of knee skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000393','7342670','ผ่าตัดเนื้อตายออกจากบริเวณ แผล บริเวณติดเชื้อ แผลไหม้ ที่เข่า','Excisional debridement of wound, infection or burn of knee skin','','');
insert into b_icd9 values ('0555000000394','7342671','ผ่าตัดรอยโรคออกจากผิวหนัง และชั้นใต้ผิวหนังเข่า','Excision of lesion of knee skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000395','7342870','จี้ทำลายรอยโรคบริเวณผิวหนัง และชั้นใต้ผิวหนังเข่า','Destruction of lesion of knee skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000396','7343970','เย็บแผลที่เข่า','Suture of laceration of knee skin','','');
insert into b_icd9 values ('0555000000397','7348070','ทำแผลที่เข่า','Wound dressing on knee skin','','');
insert into b_icd9 values ('0555000000398','7349999','หัตถการและการทำผ่าตัดอื่นๆที่ผิวหนังเข่า','Other procedures and operations on knee skin','','');
insert into b_icd9 values ('0555000000399','7350400','การตัดชิ้นเนื้อไปชันสูตรจากผิวหนังขาท่อนล่าง','Biopsy of lower leg skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000400','7351170','ตัดไหมที่ขาท่อนล่าง','Removal of sutures on lower leg skin','','');
insert into b_icd9 values ('0555000000401','7351171','ดึงวัตถุแปลกปลอมออกจากผิวหนัง และชั้นใต้ผิวหนังขาท่อนล่าง','Removal of foreign body from lower leg skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000402','7351770','พันแผลโดยใช้แรงกดเพื่อห้ามเลือดบริเวณผิวหนังขาท่อนล่าง','Application of pressure dressing on lower leg skin','','');
insert into b_icd9 values ('0555000000403','7351771','ขันชะเนาะที่ขาท่อนล่าง','Application of tourniquet at lower leg','','');
insert into b_icd9 values ('0555000000404','7352270','เจาะดูดของเหลวจากบริเวณผิวหนัง และชั้นใต้ผิวหนังขาท่อนล่าง','Aspiration of lesions in lower leg skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000405','7352271','ผ่าระบายของเหลวจากบริเวณผิวหนัง และชั้นใต้ผิวหนังขาท่อนล่าง','Drainage of lower leg skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000406','7352670','ผ่าตัดเนื้อตายออกจากบริเวณ แผล บริเวณติดเชื้อ แผลไหม้ ที่ขาท่อนล่าง','Excisional debridement of wound, infection or burn of lower leg skin','','');
insert into b_icd9 values ('0555000000407','7352671','ผ่าตัดรอยโรคออกจาก ผิวหนัง และชั้นใต้ผิวหนังขาท่อนล่าง','Excision of lesion of lower leg skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000408','7352870','จี้ทำลายรอยโรคบริเวณ ผิวหนัง และชั้นใต้ผิวหนังขาท่อนล่าง','Destruction of lesion of lower leg skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000409','7353970','เย็บแผลที่ขาท่อนล่าง','Suture of laceration of lower leg skin','','');
insert into b_icd9 values ('0555000000410','7358070','ทำแผลที่ขาท่อนล่าง','Wound dressing on lower leg skin','','');
insert into b_icd9 values ('0555000000411','7359999','หัตถการและการทำผ่าตัดอื่นๆที่ผิวหนังขาท่อนล่าง','Other procedures and operations on lower leg skin','','');
insert into b_icd9 values ('0555000000412','7360400','การตัดชิ้นเนื้อไปชันสูตรจากบริเวณผิวหนังข้อเท้า','Biopsy of ankle skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000413','7361170','ตัดไหมที่ข้อเท้า','Removal of sutures on ankle skin','','');
insert into b_icd9 values ('0555000000414','7361171','ดึงวัตถุแปลกปลอมออกจากผิวหนัง และชั้นใต้ผิวหนังข้อเท้า','Removal of foreign body from ankle skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000415','7361770','พันแผลโดยใช้แรงกดเพื่อห้ามเลือดบริเวณผิวหนังข้อเท้า','Application of pressure dressing on ankle skin','','');
insert into b_icd9 values ('0555000000416','7362270','เจาะดูดของเหลวจากบริเวณผิวหนัง และชั้นใต้ผิวหนังข้อเท้า','Aspiration of lesions in ankle skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000417','7362271','ผ่าระบายของเหลวจากบริเวณผิวหนัง และชั้นใต้ผิวหนังข้อเท้า','Drainage of ankle skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000418','7362670','ผ่าตัดเนื้อตายออกจากบริเวณ แผล บริเวณติดเชื้อ แผลไหม้ ที่ข้อเท้า','Excisional debridement of wound, infection or burn of ankle skin','','');
insert into b_icd9 values ('0555000000419','7362671','ผ่าตัดรอยโรคออกจาก ผิวหนัง และชั้นใต้ผิวหนังข้อเท้า','Excision of lesion of ankle skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000420','7362870','จี้ทำลายรอยโรคบริเวณผิวหนัง และชั้นใต้ผิวหนังข้อเท้า','Destruction of lesion of ankle skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000421','7363970','เย็บแผลที่ข้อเท้า','Suture of laceration of ankle skin','','');
insert into b_icd9 values ('0555000000422','7368070','ทำแผล ที่ข้อเท้า','Wound dressing on ankle skin','','');
insert into b_icd9 values ('0555000000423','7369999','หัตถการและการทำผ่าตัดอื่นๆที่ผิวหนังข้อเท้า','Other procedures and operations on ankle skin','','');
insert into b_icd9 values ('0555000000424','7370400','การตัดชิ้นเนื้อไปชันสูตรจากผิวหนังส่วนเท้า','Biopsy of foot skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000425','7371170','ตัดไหมที่เท้า','Removal of sutures on foot skin','','');
insert into b_icd9 values ('0555000000426','7371171','ดึงวัตถุแปลกปลอมออกจากผิวหนัง และชั้นใต้ผิวหนังเท้า','Removal of foreign body from foot skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000427','7371770','พันแผลโดยใช้แรงกดเพื่อห้ามเลือดบริเวณผิวหนังเท้า','Application of pressure dressing on foot skin','','');
insert into b_icd9 values ('0555000000428','7372270','เจาะดูดของเหลวจากบริเวณผิวหนัง และชั้นใต้ผิวหนังเท้า','Aspiration of lesions in foot skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000429','7372271','ผ่าระบายของเหลวจากบริเวณผิวหนัง และชั้นใต้ผิวหนังเท้า','Drainage of foot skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000430','7372670','ผ่าตัดเนื้อตายออกจากบริเวณ แผล บริเวณติดเชื้อ แผลไหม้ ที่เท้า','Excisional debridement of wound, infection or burn of foot skin','','');
insert into b_icd9 values ('0555000000431','7372671','ผ่าตัดรอยโรคออกจากผิวหนัง และชั้นใต้ผิวหนังเท้า','Excision of lesion of foot skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000432','7372870','จี้ทำลายรอยโรคบริเวณ ผิวหนัง และชั้นใต้ผิวหนังเท้า','Destruction of lesion of foot skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000433','7373970','เย็บแผลที่เท้า','Suture of laceration of foot skin','','');
insert into b_icd9 values ('0555000000434','7378070','ทำแผลที่เท้า','Wound dressing on foot skin','','');
insert into b_icd9 values ('0555000000435','7379999','หัตถการและการทำผ่าตัดอื่นๆบริเวณผิวหนังเท้า','Other procedures and operations on foot skin','','');
insert into b_icd9 values ('0555000000436','7981370','การดามกระดูกต้นขา','Nonoperative immobilization of femur','','');
insert into b_icd9 values ('0555000000437','7981371','การใส่เฝือกอ่อนที่กระดูกต้นขา','Application of slab for femur injury','','');
insert into b_icd9 values ('0555000000438','7981372','การใส่เฝือกที่กระดูกต้นขา','Application of cast for femur injury','','');
insert into b_icd9 values ('0555000000439','7989999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณกระดูกต้นขา','Other procedures and operations on femur','','');
insert into b_icd9 values ('0555000000440','7991370','การดามกระดูกสะบ้า','Nonoperative immobilization of patella','','');
insert into b_icd9 values ('0555000000441','7991371','การใส่เฝือกอ่อนที่กระดูกสะบ้า','Application of slab for patella injury','','');
insert into b_icd9 values ('0555000000442','7991372','การใส่เฝือกที่กระดูกสะบ้า','Application of cast for patella injury','','');
insert into b_icd9 values ('0555000000443','7999999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณ กระดูกสะบ้า','Other procedures and operations on patella','','');
insert into b_icd9 values ('0555000000444','8141370','การดามกระดูกแข้ง','Nonoperative immobilization of tibia','','');
insert into b_icd9 values ('0555000000445','8141371','การใส่เฝือกอ่อนที่กระดูกแข้ง','Application of slab for tibia injury','','');
insert into b_icd9 values ('0555000000446','8147372','การใส่เฝือกที่กระดูกแข้ง','Application of cast for tibia injury','','');
insert into b_icd9 values ('0555000000447','8149999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณ กระดูกแข้ง','Other procedures and operations on tibia','','');
insert into b_icd9 values ('0555000000448','8241370','การดามกระดูกเท้า','Nonoperative immobilization of metatarsal bones','','');
insert into b_icd9 values ('0555000000449','8241371','การใส่เฝือกอ่อนที่เท้า','Application of slab for metatarsal bones injury','','');
insert into b_icd9 values ('0555000000450','8241372','การใส่เฝือกที่เท้า','Application of cast for metatarsal bones injury','','');
insert into b_icd9 values ('0555000000451','8249999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณกระดูกเท้า','Other procedures and operations on metatarsal bones','','');
insert into b_icd9 values ('0555000000452','8281370','การดามนิ้วเท้า','Nonoperative immobilization of phalanges of foot','','');
insert into b_icd9 values ('0555000000453','8281371','การใส่เฝือกอ่อนที่นิ้วเท้า','Application of slab for phalanges of foot injury','','');
insert into b_icd9 values ('0555000000454','8281372','การใส่เฝือกที่นิ้วเท้า','Application of cast for phalanges of foot injury','','');
insert into b_icd9 values ('0555000000455','8291370','การดามข้อสะโพก','Nonoperative immobilization of hip joint','','');
insert into b_icd9 values ('0555000000456','8291371','การใส่เฝือกอ่อนที่สะโพก','Application of slab for hip joint injury','','');
insert into b_icd9 values ('0555000000457','8291372','การใส่เฝือกที่สะโพก','Application of cast for hip joint injury','','');
insert into b_icd9 values ('0555000000458','8331370','การดามเข่า','Nonoperative immobilization of knee joint','','');
insert into b_icd9 values ('0555000000459','8331371','การใส่เฝือกอ่อนที่เข่า','Application of slab for knee joint injury','','');
insert into b_icd9 values ('0555000000460','8331372','การใส่เฝือกที่เข่า','Application of cast for knee joint injury','','');
insert into b_icd9 values ('0555000000461','8339999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณเข่า','Other procedures and operations on knee joint','','');
insert into b_icd9 values ('0555000000462','8371370','การดามข้อเท้า','Nonoperative immobilization of ankle joint','','');
insert into b_icd9 values ('0555000000463','8371371','การใส่เฝือกอ่อนที่ข้อเท้า','Application of slab for ankle joint injury','','');
insert into b_icd9 values ('0555000000464','8371372','การใส่เฝือกที่ข้อเท้า','Application of cast for ankle joint injury','','');
insert into b_icd9 values ('0555000000465','8379999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณข้อเท้า','Other procedures and operations on ankle joint','','');
insert into b_icd9 values ('0555000000466','8411340','การดามข้อโคนนิ้วเท้า','Nonoperative immobilization of metatarsophalangeal joint','','');
insert into b_icd9 values ('0555000000467','8411371','การใส่เฝือกอ่อนที่โคนนิ้วเท้า','Application of slab for metatarsophalangeal joint injury','','');
insert into b_icd9 values ('0555000000468','8411372','การใส่เฝือกที่โคนนิ้วเท้า','Application of cast for metatarsophalangeal joint injury','','');
insert into b_icd9 values ('0555000000469','8419999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณข้อโคนนิ้วเท้า','Other procedures and operations on metatarsophalangeal joint','','');
insert into b_icd9 values ('0555000000470','8770400','การตัดชิ้นเนื้อไปชันสูตรจากนิ้วหัวแม่เท้า','Biopsy of great toe','','');
insert into b_icd9 values ('0555000000471','8771170','ตัดไหมที่นิ้วหัวแม่เท้า','Removal of sutures on great toe','','');
insert into b_icd9 values ('0555000000472','8771171','ดึงวัตถุแปลกปลอมออกจากนิ้วหัวแม่เท้า','Removal of foreign body from great toe','','');
insert into b_icd9 values ('0555000000473','8771770','พันแผลโดยใช้แรงกดเพื่อห้ามเลือดบริเวณนิ้วหัวแม่เท้า','Application of pressure dressing on great toe','','');
insert into b_icd9 values ('0555000000474','8772270','เจาะดูดของเหลวจากรอยโรคบริเวณนิ้วหัวแม่เท้า','Aspiration of lesions in great toe','','');
insert into b_icd9 values ('0555000000475','8772271','ผ่าระบายของเหลวจากบริเวณนิ้วหัวแม่เท้า','Drainage of great toe','','');
insert into b_icd9 values ('0555000000476','8772670','ผ่าตัดเนื้อตายออกจากบริเวณ แผล บริเวณติดเชื้อ แผลไหม้ ที่นิ้วหัวแม่เท้า','Excisional debridement of wound, infection or burn of great toe','','');
insert into b_icd9 values ('0555000000477','8772671','ผ่าตัดรอยโรคออกจากนิ้วหัวแม่เท้า','Excision of lesion of great toe','','');
insert into b_icd9 values ('0555000000478','8772770','ถอดเล็บนิ้วหัวแม่เท้า','Removal of nail, nailbed or nailfold of great toe','','');
insert into b_icd9 values ('0555000000479','8772870','การจี้ทำลายรอยโรคบริเวณนิ้วหัวแม่เท้า','Destruction of lesion of great toe','','');
insert into b_icd9 values ('0555000000480','8773970','เย็บแผลที่นิ้วหัวแม่เท้า','Suture of laceration of great toe','','');
insert into b_icd9 values ('0555000000481','8778070','ทำแผลที่นิ้วหัวแม่เท้า','Wound dressing on great toe','','');
insert into b_icd9 values ('0555000000482','8779999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณนิ้วหัวแม่เท้า','Other procedures and operations on great toe','','');
insert into b_icd9 values ('0555000000483','8780400','การตัดชิ้นเนื้อไปชันสูตรจากนิ้วเท้า','Biopsy of toe(s) except great toe','','');
insert into b_icd9 values ('0555000000484','8781170','ตัดไหมที่นิ้วเท้า ไม่รวมนิ้วหัวแม่เท้า','Removal of sutures on toe(s) except great toe','','');
insert into b_icd9 values ('0555000000485','8781171','ดึงวัตถุแปลกปลอมออกจากนิ้วเท้า ไม่รวมนิ้วหัวแม่เท้า','Removal of foreign body from toe(s) except great toe','','');
insert into b_icd9 values ('0555000000486','8781700','พันแผลโดยใช้แรงกดเพื่อห้ามเลือดบริเวณนิ้วเท้า ไม่รวมนิ้วหัวแม่เท้า','Application of pressure dressing on toe(s) except great toe','','');
insert into b_icd9 values ('0555000000487','8782270','เจาะดูดของเหลวจากรอยโรคบริเวณนิ้วเท้า ไม่รวมนิ้วหัวแม่เท้า','Aspiration of lesions in toe(s) except great toe','','');
insert into b_icd9 values ('0555000000488','8782271','ผ่าระบายของเหลวจากบริเวณนิ้วเท้า ไม่รวมนิ้วหัวแม่เท้า','Drainage of toe(s) except great toe','','');
insert into b_icd9 values ('0555000000489','8782670','ผ่าตัดเนื้อตายออกจากบริเวณ แผล บริเวณติดเชื้อ แผลไหม้ที่นิ้วเท้า ไม่รวมนิ้วหัวแม่เท้า','Excisional debridement of wound, infection or burn of toe(s) except great toe','','');
insert into b_icd9 values ('0555000000490','8782671','ผ่าตัดรอยโรคออกจากนิ้วเท้า ไม่รวมนิ้วหัวแม่เท้า','Excision of lesion of toe(s) except great toe','','');
insert into b_icd9 values ('0555000000491','8782770','ถอดเล็บนิ้วเท้า ไม่รวมนิ้วหัวแม่เท้า','Removal of nail, nailbed or nailfold of toe(s) except great toe','','');
insert into b_icd9 values ('0555000000492','8782870','การจี้ทำลายรอยโรคบริเวณนิ้วเท้าไม่รวมนิ้วหัวแม่เท้า','Destruction of lesion of toe(s) except great toe','','');
insert into b_icd9 values ('0555000000493','8783970','เย็บแผลที่นิ้วเท้าไม่รวมนิ้วหัวแม่เท้า','Suture of laceration of toe(s) except great toe','','');
insert into b_icd9 values ('0555000000494','8788070','ทำแผลที่นิ้วเท้าไม่รวมนิ้วหัวแม่เท้า','Wound dressing on toe(s) except great toe','','');
insert into b_icd9 values ('0555000000495','8789999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณนิ้วเท้า ไม่รวมนิ้วหัวแม่เท้า','Other procedures and operations on toe(s) except great toe','','');
insert into b_icd9 values ('0555000000496','9008203','การให้เลือด','Transfusion of whole blood','','');
insert into b_icd9 values ('0555000000497','9008204','การให้ packed red cells','Transfusion of packed red cells','','');
insert into b_icd9 values ('0555000000498','9008205','การให้เกร็ดเลือด','Transfusion of platelets','','');
insert into b_icd9 values ('0555000000499','9008206','การให้ coagulation factors','Transfusion of coagulation factors','','');
insert into b_icd9 values ('0555000000500','9008207','การให้ซีรั่ม','Transfusion of other serum','','');
insert into b_icd9 values ('0555000000501','9008208','การให้สารอื่นๆ','Transfusion of other substance','','');
insert into b_icd9 values ('0555000000502','9008209','การให้น้ำเกลือ','Transfusion of intravenous fluid','','');
insert into b_icd9 values ('0555000000503','9010200','การวัดความหนาชั้นผิวหนัง','Skin fold thickness measurement','','');
insert into b_icd9 values ('0555000000504','9011010','การฝังยาคุมกำเนิดใต้ผิวหนัง','Implantation of contraceptive device into skin','','');
insert into b_icd9 values ('0555000000505','9011110','การถอดยาคุมกำเนิดออกจากใต้ผิวหนัง','Removal of contraceptive device from skin','','');
insert into b_icd9 values ('0555000000506','9018170','การฉีดยาเข้าผิวหนังหรือชั้นใต้ผิวหนัง','Injection into skin or subcutaneous tissue','','');
insert into b_icd9 values ('0555000000507','9038170','การฉีดยาเข้ากล้าม','Injection into muscle','','');
insert into b_icd9 values ('0555000000508','9082100','การเจาะเลือดจากหลอดเลือดดำ','Vein puncture','','');
insert into b_icd9 values ('0555000000509','9088170','การฉีดยาเข้าหลอดเลือดดำ','Injection into vein','','');
insert into b_icd9 values ('0555000000510','9390140','การส่องกล้องตรวจตา','Ophthalmoscopy','','');
insert into b_icd9 values ('0555000000511','9390200','การตรวจวัดสายตา','Visual acuity test','','');
insert into b_icd9 values ('0555000000512','9999999','การทำหัตถการและการผ่าตัดอื่นๆที่มิได้ระบุไว้ หรือระบุไว้เป็นอื่น','Other and unspecified procedures or operations','','');
insert into b_icd9 values ('0555000000513','1010000','Physical examination on scalp','Physical examination on scalp','','');
insert into b_icd9 values ('0555000000514','1010400','Biopsy of scalp','การตัดชิ้นเนื้อบริเวณหนังศีรษะไปชันสูตร','','');
insert into b_icd9 values ('0555000000515','1011100','Removal of sutures on scalp','Removal of sutures on scalp','','');
insert into b_icd9 values ('0555000000516','1011101','Removal of foreign body from scalp','Removal of foreign body from scalp','','');
insert into b_icd9 values ('0555000000517','1011700','Application of pressure dressing on scalp','Application of pressure dressing on scalp','','');
insert into b_icd9 values ('0555000000518','1012100','Incision on scalp','Incision on scalp','','');
insert into b_icd9 values ('0555000000519','1012200','Aspiration of scalp','Aspiration of scalp','','');
insert into b_icd9 values ('0555000000520','1012201','Drainage of scalp Include : Incision and drainage','Drainage of scalp Include : Incision and drainage','','');
insert into b_icd9 values ('0555000000521','1012400','Release of scar on scalp','Release of scar on scalp','','');
insert into b_icd9 values ('0555000000522','1012600','Debridement of scalp','Debridement of scalp','','');
insert into b_icd9 values ('0555000000523','1012601','Excision of lesion of scalp','Excision of lesion of scalp','','');
insert into b_icd9 values ('0555000000524','1012602','Wide excision of scalp','Wide excision of scalp','','');
insert into b_icd9 values ('0555000000525','1012800','Destruction of lesion of scalp','Destruction of lesion of scalp','','');
insert into b_icd9 values ('0555000000526','1012801','Epilation of hair','Epilation of hair','','');
insert into b_icd9 values ('0555000000527','1013900','Repair of laceration or wound of scalp','Repair of laceration or wound of scalp','','');
insert into b_icd9 values ('0555000000528','1014300','Insertion  of tissue expander into scalp','Insertion  of tissue expander into scalp','','');
insert into b_icd9 values ('0555000000529','1014800','Closure of wound of scalp by skin graft','Closure of wound of scalp by skin graft','','');
insert into b_icd9 values ('0555000000530','1014801','Closure of wound of scalp by flap','Closure of wound of scalp by flap','','');
insert into b_icd9 values ('0555000000531','1014802','Reconstruction of scalp','Reconstruction of scalp','','');
insert into b_icd9 values ('0555000000532','1018000','Wound dressing on scalp','Wound dressing on scalp','','');
insert into b_icd9 values ('0555000000533','1019999','Other procedures and operations on scalp','การทำหัตถการและการผ่าตัดอื่นๆบริเวณหนังศีรษะ','','');
insert into b_icd9 values ('0555000000534','1050500','Plain radiography of skull, postero - anterior(PA) and lateral view','Plain radiography of skull, postero - anterior(PA) and lateral view','','');
insert into b_icd9 values ('0555000000535','1050501','Plain radiography of skull, antero - posterior(AP) and lateral view and lateral and towne''s view','Plain radiography of skull, antero - posterior(AP) and lateral view and lateral and towne''s view','','');
insert into b_icd9 values ('0555000000536','1050592','Cephalogram','Cephalogram','','');
insert into b_icd9 values ('0555000000537','1053908','Other repair of skull','Other repair of skull','','');
insert into b_icd9 values ('0555000000538','1054400','Insertion of skull plate','Insertion of skull plate','','');
insert into b_icd9 values ('0555000000539','1054500','Operative removal of skull plate Exclude : remaval with synchronous replacement(105-46-00)','Operative removal of skull plate Exclude : remaval with synchronous replacement(105-46-00)','','');
insert into b_icd9 values ('0555000000540','1054600','Replacement of skull plate','Replacement of skull plate','','');
insert into b_icd9 values ('0555000000541','1054700','Revision of bone flap of skull','Revision of bone flap of skull','','');
insert into b_icd9 values ('0555000000542','1059999','Other procedures and operations on skull','Other procedures and operations on skull','','');
insert into b_icd9 values ('0555000000543','1452800','Destruction of trigeminal nerve e.g cryoanalgesia, injection, radiofrequency','Destruction of trigeminal nerve e.g cryoanalgesia, injection, radiofrequency','','');
insert into b_icd9 values ('0555000000544','1453800','Transposition of trigeminalnerves','Transposition of trigeminalnerves','','');
insert into b_icd9 values ('0555000000545','1453900','Repair of trigeminal nerves Exclude : repair of old traumatic injury of trigeminal nerves(145-29-02)','Repair of trigeminal nerves Exclude : repair of old traumatic injury of trigeminal nerves(145-29-02)','','');
insert into b_icd9 values ('0555000000546','1453901','Trigeminal nerves graft','Trigeminal nerves graft','','');
insert into b_icd9 values ('0555000000547','1453902','Repair of traumatic injury of trigeminal nerves Exclude : repair of old traumatic injury of trigeminal nerves(145-29-00)','Repair of traumatic injury of trigeminal nerves Exclude : repair of old traumatic injury of trigeminal nerves(145-29-00)','','');
insert into b_icd9 values ('0555000000548','1454700','Revision of previous repair of trigeminal nerve','Revision of previous repair of trigeminal nerve','','');
insert into b_icd9 values ('0555000000549','1419999','Other procedures and operations on trigeminal nerve','Other procedures and operations on trigeminal nerve','','');
insert into b_icd9 values ('0555000000550','1513800','Transposition of accessory nerve','Transposition of accessory nerve','','');
insert into b_icd9 values ('0555000000551','1513801','Accessory-facial anastomosis','Accessory-facial anastomosis','','');
insert into b_icd9 values ('0555000000552','1513802','Acessory-hypoglossal anastomosis','Acessory-hypoglossal anastomosis','','');
insert into b_icd9 values ('0555000000553','1519999','Other procedures and operations on accessory nerve','Other procedures and operations on accessory nerve','','');
insert into b_icd9 values ('0555000000554','1523800','Transposition of hypoglossal nerve','Transposition of hypoglossal nerve','','');
insert into b_icd9 values ('0555000000555','1523801','Hypoglossal-facial nerve anastomosis','Hypoglossal-facial nerve anastomosis','','');
insert into b_icd9 values ('0555000000556','1529999','Other procedures and operations on hypoglossal nerve','Other procedures and operations on hypoglossal nerve','','');
insert into b_icd9 values ('0555000000557','1530400','Closed biopsy of cranial nerve','Closed biopsy of cranial nerve','','');
insert into b_icd9 values ('0555000000558','1530401','Open biopsy of cranial nerve','Open biopsy of cranial nerve','','');
insert into b_icd9 values ('0555000000559','1532100','Incision of cranial nerve','Incision of cranial nerve','','');
insert into b_icd9 values ('0555000000560','1532300','Decompression of cranial nerves','Decompression of cranial nerves','','');
insert into b_icd9 values ('0555000000561','1532400','Division of cranial nerves','Division of cranial nerves','','');
insert into b_icd9 values ('0555000000562','1532409','Lysis of adhesions of cranial nerves','Lysis of adhesions of cranial nerves','','');
insert into b_icd9 values ('0555000000563','1532600','Excision of cranial nerve ganglion','Excision of cranial nerve ganglion','','');
insert into b_icd9 values ('0555000000564','1532601','Excision of lesion of cranial nerves','Excision of lesion of cranial nerves','','');
insert into b_icd9 values ('0555000000565','1532800','Destruction of cranial nerves by cryoanalgesia,injection of neurolysis agent,radiofrequency','Destruction of cranial nerves by cryoanalgesia,injection of neurolysis agent,radiofrequency','','');
insert into b_icd9 values ('0555000000566','1533800','Transposition of cranial nerves','Transposition of cranial nerves','','');
insert into b_icd9 values ('0555000000567','1533900','Repair of cranial nerves Excludes : Repair of old traumatic injury of cranial nerves(153-29-00)','Repair of cranial nerves Excludes : Repair of old traumatic injury of cranial nerves(153-29-00)','','');
insert into b_icd9 values ('0555000000568','1533901','Cranial nerves graft','Cranial nerves graft','','');
insert into b_icd9 values ('0555000000569','1533902','Repair of old traumatic injury of cranial nerves Excludes : Repair  of current injury of cranial nerves(153-29-00)','Repair of old traumatic injury of cranial nerves Excludes : Repair  of current injury of cranial nerves(153-29-00)','','');
insert into b_icd9 values ('0555000000570','1534700','Revision of previous repair of cranial nerves','Revision of previous repair of cranial nerves','','');
insert into b_icd9 values ('0555000000571','1535000','Transplantation of cranial nerves','Transplantation of cranial nerves','','');
insert into b_icd9 values ('0555000000572','1539999','Other procedures and operations on cranial nervesFace','Other procedures and operations on cranial nervesFace','','');
insert into b_icd9 values ('0555000000573','1540400','Biopsy of facial skin and subcutaneous tissue','การตัดชิ้นเนื้อบริเวณผิวหนังใบหน้าไปชันสูตร','','');
insert into b_icd9 values ('0555000000574','1541500','Massage of face','Massage of face','','');
insert into b_icd9 values ('0555000000575','1541501','Acupressure at face','Acupressure at face','','');
insert into b_icd9 values ('0555000000576','1541503','Soft tissue stretching at face','Soft tissue stretching at face','','');
insert into b_icd9 values ('0555000000577','1541504','Muscle stretching at face','Muscle stretching at face','','');
insert into b_icd9 values ('0555000000578','1541505','Muscle stretching with coalant spray at face','Muscle stretching with coalant spray at face','','');
insert into b_icd9 values ('0555000000579','1541506','Tendon stretching at face','Tendon stretching at face','','');
insert into b_icd9 values ('0555000000580','1541507','Joint manipulation/play at face','Joint manipulation/play at face','','');
insert into b_icd9 values ('0555000000581','1541000','Insertion of tissue expander into facial skin','Insertion of tissue expander into facial skin','','');
insert into b_icd9 values ('0555000000582','1541100','Removalof foreign body from facial skin and subcutaneous tissue','Removalof foreign body from facial skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000583','1542100','Incision of pilonidal cyst or sinus of facial skin','Incision of pilonidal cyst or sinus of facial skin','','');
insert into b_icd9 values ('0555000000584','1542198','Other incision of facial skin and subcutaneous tissue','Other incision of facial skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000585','1542200','Aspiration of facial skin and subcutaneous tissue','Aspiration of facial skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000586','1542201','Drainage of facial skin and subcutaneous tissue, Include : Incision and drainage','Drainage of facial skin and subcutaneous tissue, Include : Incision and drainage','','');
insert into b_icd9 values ('0555000000587','1542202','Marsupialization of facial skin and subcutaneous tissue','Marsupialization of facial skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000588','1542299','Drainage of facial skin and subcutaneous tissue, unspecified','Drainage of facial skin and subcutaneous tissue, unspecified','','');
insert into b_icd9 values ('0555000000589','1542600','Excisional debridement of wound,infection or burn of facial skin','Excisional debridement of wound,infection or burn of facial skin','','');
insert into b_icd9 values ('0555000000590','1542601','Excision  of pilonidal cyst or sinus of facial skin','Excision  of pilonidal cyst or sinus of facial skin','','');
insert into b_icd9 values ('0555000000591','1542602','Radical excision of facial skin lesion, Wide excision of facial skin lesion','Radical excision of facial skin lesion, Wide excision of facial skin lesion','','');
insert into b_icd9 values ('0555000000592','1542603','Excision of facial skin for graft to other site','Excision of facial skin for graft to other site','','');
insert into b_icd9 values ('0555000000593','1542610','Cutting and preparation of pedical grafts of flaps of face','Cutting and preparation of pedical grafts of flaps of face','','');
insert into b_icd9 values ('0555000000594','1542698','Other local incision of lesion of facial skin and subcutaneous tissue','Other local incision of lesion of facial skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000595','1542800','Nonexcisional debridement of wound ,infection or burn of facial skin','Nonexcisional debridement of wound ,infection or burn of facial skin','','');
insert into b_icd9 values ('0555000000596','1542801','Chemosurgery of facial skin,Chemical peel of skin','Chemosurgery of facial skin,Chemical peel of skin','','');
insert into b_icd9 values ('0555000000597','1542898','Other destruction of lesion of facial skin','Other destruction of lesion of facial skin','','');
insert into b_icd9 values ('0555000000598','1543900','Suture of laceration of facial skin','Suture of laceration of facial skin','','');
insert into b_icd9 values ('0555000000599','1543901','Repair for facial weakness,Repair of facial paralysis','Repair for facial weakness,Repair of facial paralysis','','');
insert into b_icd9 values ('0555000000600','1543902','Facial rhytidectomy,Face lift','Facial rhytidectomy,Face lift','','');
insert into b_icd9 values ('0555000000601','1543998','Other repair of facial skin','Other repair of facial skin','','');
insert into b_icd9 values ('0555000000602','1544700','Revision of pedicle or flap graft of facial skin','Revision of pedicle or flap graft of facial skin','','');
insert into b_icd9 values ('0555000000603','1544800','Advancement of pedicle graft of facial skin','Advancement of pedicle graft of facial skin','','');
insert into b_icd9 values ('0555000000604','1544801','Attachment of pedicle or flap graft of facial skin','Attachment of pedicle or flap graft of facial skin','','');
insert into b_icd9 values ('0555000000605','1544802','Facial skin reduction plastic operation','Facial skin reduction plastic operation','','');
insert into b_icd9 values ('0555000000606','1544803','Relaxation of scar or web contracture of facial skin','Relaxation of scar or web contracture of facial skin','','');
insert into b_icd9 values ('0555000000607','1544898','Other reconstruction of gfacial skin','Other reconstruction of gfacial skin','','');
insert into b_icd9 values ('0555000000608','1545000','Split-thickness facial skin graft,Skin graft NOS','Split-thickness facial skin graft,Skin graft NOS','','');
insert into b_icd9 values ('0555000000609','1545001','Full-Thickness facial skin graft','Full-Thickness facial skin graft','','');
insert into b_icd9 values ('0555000000610','1545002','Heterograft to facial skin','Heterograft to facial skin','','');
insert into b_icd9 values ('0555000000611','1545003','Homograft to facial skin','Homograft to facial skin','','');
insert into b_icd9 values ('0555000000612','1545004','Dermal regenerative graft of facial skin, Artificial skin, Creation of neodermis, Decellularlized allodermis, Integumentary matrix implants, Prosthetic implant of dermal layer of skin, Regenative dermal layer of skin','Dermal regenerative graft of facial skin, Artificial skin, Creation of neodermis, Decellularlized allodermis, Integumentary matrix implants, Prosthetic implant of dermal layer of skin, Regenative dermal layer of skin','','');
insert into b_icd9 values ('0555000000613','1548100','Injection or tattooing of facial skin lesion and defect','Injection or tattooing of facial skin lesion and defect','','');
insert into b_icd9 values ('0555000000615','1590400','Biopsy of maxilla','Biopsy of maxilla','','');
insert into b_icd9 values ('0555000000616','1591100','Removal of external maxillary fixation devices','Removal of external maxillary fixation devices','','');
insert into b_icd9 values ('0555000000617','1592200','Marsupialization of lesion of maxilla','Marsupialization of lesion of maxilla','','');
insert into b_icd9 values ('0555000000618','1592600','Excision of dental (odontogenic) lesion of maxilla','Excision of dental (odontogenic) lesion of maxilla','','');
insert into b_icd9 values ('0555000000619','1592601','Excision of non - odontogenic lesion of maxilla','Excision of non - odontogenic lesion of maxilla','','');
insert into b_icd9 values ('0555000000620','1592602','Sequestrectomy of maxilla','Sequestrectomy of maxilla','','');
insert into b_icd9 values ('0555000000621','1592603','Alveolectomy of maxilla,Alveoloplasty of maxilla','Alveolectomy of maxilla,Alveoloplasty of maxilla','','');
insert into b_icd9 values ('0555000000622','1592604','Torectomy of maxilla,Removed torus palatinus','Torectomy of maxilla,Removed torus palatinus','','');
insert into b_icd9 values ('0555000000623','1592610','Partial maxillectomy,Medial maxillectomy','Partial maxillectomy,Medial maxillectomy','','');
insert into b_icd9 values ('0555000000624','1592611','Total maxillectomy,unilateral Excludes : Total maxillectomy with orbital exenteration(Radical orbitomaxillectomy 177-26-12)','Total maxillectomy,unilateral Excludes : Total maxillectomy with orbital exenteration(Radical orbitomaxillectomy 177-26-12)','','');
insert into b_icd9 values ('0555000000625','1592612','Total maxillectomy,bilateral','Total maxillectomy,bilateral','','');
insert into b_icd9 values ('0555000000626','1592698','Other excision operation of maxilla','Other excision operation of maxilla','','');
insert into b_icd9 values ('0555000000627','1593600','Closed reduction of maxillary alveolar fracture','Closed reduction of maxillary alveolar fracture','','');
insert into b_icd9 values ('0555000000628','1593601','Open reduction of maxillary alveolar fracture','Open reduction of maxillary alveolar fracture','','');
insert into b_icd9 values ('0555000000629','1593602','Closed reduction of maxillary  fracture','Closed reduction of maxillary  fracture','','');
insert into b_icd9 values ('0555000000630','1593603','Open reduction of maxillary  fracture','Open reduction of maxillary  fracture','','');
insert into b_icd9 values ('0555000000631','1593700','External fixation of maxillary fracture','External fixation of maxillary fracture','','');
insert into b_icd9 values ('0555000000632','1593701','Internal fixation of maxillary fracture','Internal fixation of maxillary fracture','','');
insert into b_icd9 values ('0555000000633','1593900','Segmental osteoplasty of maxilla, Maxillary osteoplasty, Subapical osteotomy of maxilla','Segmental osteoplasty of maxilla, Maxillary osteoplasty, Subapical osteotomy of maxilla','','');
insert into b_icd9 values ('0555000000634','1593901','Total osteoplasty of maxilla, Le Fort 1 osteotomy, Le Fort 2 osteotomy, Multiple pieces Le Fort 1 osteotomy','Total osteoplasty of maxilla, Le Fort 1 osteotomy, Le Fort 2 osteotomy, Multiple pieces Le Fort 1 osteotomy','','');
insert into b_icd9 values ('0555000000635','1593908','Other repair operation of maxilla','Other repair operation of maxilla','','');
insert into b_icd9 values ('0555000000636','1594400','Insertion of synthetic implant in maxilla bone','Insertion of synthetic implant in maxilla bone','','');
insert into b_icd9 values ('0555000000637','1594500','Removal of internal fixation device from maxilla','Removal of internal fixation device from maxilla','','');
insert into b_icd9 values ('0555000000638','1594800','Ridge augmentation of maxilla with alloplastic material','Ridge augmentation of maxilla with alloplastic material','','');
insert into b_icd9 values ('0555000000639','1594801','Bone graft to maxilla, Repair alveolar cleft with bone graft, Sinus lift procedure','Bone graft to maxilla, Repair alveolar cleft with bone graft, Sinus lift procedure','','');
insert into b_icd9 values ('0555000000640','1594802','Bone and soft tissue reconstruction of maxilla','Bone and soft tissue reconstruction of maxilla','','');
insert into b_icd9 values ('0555000000641','1594898','Other reconstructive surgery of maxilla','Other reconstructive surgery of maxilla','','');
insert into b_icd9 values ('0555000000642','1599999','Other procedures and operations on maxilla','Other procedures and operations on maxilla','','');
insert into b_icd9 values ('0555000000643','1600400','Biopsy of mandible','Biopsy of mandible','','');
insert into b_icd9 values ('0555000000644','1601100','Removal of external mandibular fixation devices','Removal of external mandibular fixation devices','','');
insert into b_icd9 values ('0555000000645','1602200','Marsupialization of mandible','Marsupialization of mandible','','');
insert into b_icd9 values ('0555000000646','1602600','Excision of dental (odontogenic) lesion of mandible','Excision of dental (odontogenic) lesion of mandible','','');
insert into b_icd9 values ('0555000000647','1602601','Excision of non - odontogenic lesion of mandible','Excision of non - odontogenic lesion of mandible','','');
insert into b_icd9 values ('0555000000648','1602602','Coronoidectomy','Coronoidectomy','','');
insert into b_icd9 values ('0555000000649','1602603','Sequestrectomy of mandible','Sequestrectomy of mandible','','');
insert into b_icd9 values ('0555000000650','1602604','Saucerization of mandible','Saucerization of mandible','','');
insert into b_icd9 values ('0555000000651','1602605','Decortication of mandible','Decortication of mandible','','');
insert into b_icd9 values ('0555000000652','1602606','Alveolectomy of mandible,Alveoloplasty of mandible','Alveolectomy of mandible,Alveoloplasty of mandible','','');
insert into b_icd9 values ('0555000000653','1602607','Torectomy of mandible,Removed of torus mandibularis','Torectomy of mandible,Removed of torus mandibularis','','');
insert into b_icd9 values ('0555000000654','1602610','Partial mandibulectomy,Hemimandibulectomy','Partial mandibulectomy,Hemimandibulectomy','','');
insert into b_icd9 values ('0555000000655','1602611','Total mandibulectomy','Total mandibulectomy','','');
insert into b_icd9 values ('0555000000656','1602698','Other excision operation of mandible','Other excision operation of mandible','','');
insert into b_icd9 values ('0555000000657','1603600','Closed reduction of mandibular alveolar fracture','Closed reduction of mandibular alveolar fracture','','');
insert into b_icd9 values ('0555000000658','1603601','Open reduction of mandibular alveolar fracture','Open reduction of mandibular alveolar fracture','','');
insert into b_icd9 values ('0555000000659','1603602','Closed reduction of mandibular  fracture','Closed reduction of mandibular  fracture','','');
insert into b_icd9 values ('0555000000660','1603603','Open reduction of mandibular  fracture','Open reduction of mandibular  fracture','','');
insert into b_icd9 values ('0555000000661','1603700','External fixation of mandibular fracture','External fixation of mandibular fracture','','');
insert into b_icd9 values ('0555000000662','1603701','Internal fixation of mandibular fracture','Internal fixation of mandibular fracture','','');
insert into b_icd9 values ('0555000000663','1603900','Closed osteoplasty of mandibular ramus, Gigli saw osteotomy','Closed osteoplasty of mandibular ramus, Gigli saw osteotomy','','');
insert into b_icd9 values ('0555000000664','1603901','Open osteoplasty of mandibular ramus, Sagital split(ramus) osteotomy, Bilateral sagital split osteotomy, (Intra - oral) vertical ramus osteotomy','Open osteoplasty of mandibular ramus, Sagital split(ramus) osteotomy, Bilateral sagital split osteotomy, (Intra - oral) vertical ramus osteotomy','','');
insert into b_icd9 values ('0555000000665','1603902','Osteoplasty of body of mandible','Osteoplasty of body of mandible','','');
insert into b_icd9 values ('0555000000666','1603903','Other orthognathic surgery on mandible, Mandibular osteoplasty, Segmental or subapical osteotomy','Other orthognathic surgery on mandible, Mandibular osteoplasty, Segmental or subapical osteotomy','','');
insert into b_icd9 values ('0555000000667','1603904','Reduction genioplasty, Reduction mentoplasty','Reduction genioplasty, Reduction mentoplasty','','');
insert into b_icd9 values ('0555000000668','1603998','Other repair operation of mandible','Other repair operation of mandible','','');
insert into b_icd9 values ('0555000000669','1604400','Insertion of synthetic implant in mandible','Insertion of synthetic implant in mandible','','');
insert into b_icd9 values ('0555000000670','1604500','Removal of internal fixation device from mandible','Removal of internal fixation device from mandible','','');
insert into b_icd9 values ('0555000000671','1604800','Augmentation genioplasty, Mentoplasty','Augmentation genioplasty, Mentoplasty','','');
insert into b_icd9 values ('0555000000672','1604801','Ridge augmentation of mandible with alloplastic material','Ridge augmentation of mandible with alloplastic material','','');
insert into b_icd9 values ('0555000000673','1604802','Bone graft to mandible','Bone graft to mandible','','');
insert into b_icd9 values ('0555000000674','1604803','Bone and soft tissue reconstruction of mandible','Bone and soft tissue reconstruction of mandible','','');
insert into b_icd9 values ('0555000000675','1604898','Other reconstruction of mandible','Other reconstruction of mandible','','');
insert into b_icd9 values ('0555000000676','1609999','Other procedures and operations on mandible','Other procedures and operations on mandible','','');
insert into b_icd9 values ('0555000000677','1610400','Biopsy of facial bone','Biopsy of facial bone','','');
insert into b_icd9 values ('0555000000678','1611000','Insertion of electrodes into facial bones, Insertion of sphenoidal electrodes','Insertion of electrodes into facial bones, Insertion of sphenoidal electrodes','','');
insert into b_icd9 values ('0555000000679','1612100','Incision of facial bone, Reopening of osteotomy site of facial bone','Incision of facial bone, Reopening of osteotomy site of facial bone','','');
insert into b_icd9 values ('0555000000680','1612600','Debridement of lesion of facial bone','Debridement of lesion of facial bone','','');
insert into b_icd9 values ('0555000000681','1612601','Local excision of lesion of facial bone','Local excision of lesion of facial bone','','');
insert into b_icd9 values ('0555000000682','1612603','Sequestrectomy of facial bone, Removal of necritic bone chip from facial bone','Sequestrectomy of facial bone, Removal of necritic bone chip from facial bone','','');
insert into b_icd9 values ('0555000000683','1612610','Partial ostectomy of facial bone','Partial ostectomy of facial bone','','');
insert into b_icd9 values ('0555000000684','1612611','Total ostectomy of facial bone','Total ostectomy of facial bone','','');
insert into b_icd9 values ('0555000000685','1612698','Other resection of facial bone','Other resection of facial bone','','');
insert into b_icd9 values ('0555000000686','1612800','Destruction of lesion of facial bone','Destruction of lesion of facial bone','','');
insert into b_icd9 values ('0555000000687','1613600','Closed reduction of facial fracture','Closed reduction of facial fracture','','');
insert into b_icd9 values ('0555000000688','1613601','Open reduction of facial fracture','Open reduction of facial fracture','','');
insert into b_icd9 values ('0555000000689','1613699','Reduction of facial fracture, not otherwise specified','Reduction of facial fracture, not otherwise specified','','');
insert into b_icd9 values ('0555000000690','1613700','External fixation of facial fracture','External fixation of facial fracture','','');
insert into b_icd9 values ('0555000000691','1613701','Internal fixation of facial fracture','Internal fixation of facial fracture','','');
insert into b_icd9 values ('0555000000692','1613900','Repair of facial bone, Osteoplasty of facial bone','Repair of facial bone, Osteoplasty of facial bone','','');
insert into b_icd9 values ('0555000000693','1614400','Insertion of synthetic implant in facial bone','Insertion of synthetic implant in facial bone','','');
insert into b_icd9 values ('0555000000694','1614500','Removal of internal fixation device from facial bone','Removal of internal fixation device from facial bone','','');
insert into b_icd9 values ('0555000000695','1614800','Bone graft to facial bone','Bone graft to facial bone','','');
insert into b_icd9 values ('0555000000696','1614801','Bone and soft tissue reconstruction of facial bone','Bone and soft tissue reconstruction of facial bone','','');
insert into b_icd9 values ('0555000000697','1614802','Augmentation of facial bone with alloplastic material','Augmentation of facial bone with alloplastic material','','');
insert into b_icd9 values ('0555000000698','1614898','Other reconstructive operation of facial bone','Other reconstructive operation of facial bone','','');
insert into b_icd9 values ('0555000000699','1619999','Other procedures and operations on facial bone','Other procedures and operations on facial bone','','');
insert into b_icd9 values ('0555000000700','1620000','Temporomandibular joint arthoscopy','Temporomandibular joint arthoscopy','','');
insert into b_icd9 values ('0555000000701','1621100','Removal of internal fixation device from temporomandibular joint','Removal of internal fixation device from temporomandibular joint','','');
insert into b_icd9 values ('0555000000702','1621500','Manipulation of temporomandibular joint','Manipulation of temporomandibular joint','','');
insert into b_icd9 values ('0555000000703','1622400','Arthroscopic lysis adhesion of temporomandibular joint','Arthroscopic lysis adhesion of temporomandibular joint','','');
insert into b_icd9 values ('0555000000704','1622600','Debridement of temporomandibular joint','Debridement of temporomandibular joint','','');
insert into b_icd9 values ('0555000000705','1622601','Condylar shave','Condylar shave','','');
insert into b_icd9 values ('0555000000706','1622602','High condylectomy','High condylectomy','','');
insert into b_icd9 values ('0555000000707','1622603','Temporomandibular joint eminectomy','Temporomandibular joint eminectomy','','');
insert into b_icd9 values ('0555000000708','1622604','Temporomandibular joint menisectomy','Temporomandibular joint menisectomy','','');
insert into b_icd9 values ('0555000000709','1622605','Temporomandibular joint synovectomy','Temporomandibular joint synovectomy','','');
insert into b_icd9 values ('0555000000710','1622606','Temporomandibular joint myotomy','Temporomandibular joint myotomy','','');
insert into b_icd9 values ('0555000000711','1622607','Temporomandibular joint discectomy','Temporomandibular joint discectomy','','');
insert into b_icd9 values ('0555000000712','1622610','Arthroscopic debridement of temporomandibular joint','Arthroscopic debridement of temporomandibular joint','','');
insert into b_icd9 values ('0555000000713','1622615','Arthroscopic synovectomy of temporomandibular joint','Arthroscopic synovectomy of temporomandibular joint','','');
insert into b_icd9 values ('0555000000714','1622617','Arthroscopic discectomy of temporomandibular joint','Arthroscopic discectomy of temporomandibular joint','','');
insert into b_icd9 values ('0555000000715','1622620','Gap arthroplasty of temporomandibular joint','Gap arthroplasty of temporomandibular joint','','');
insert into b_icd9 values ('0555000000716','1622621','Gap arthroplasty of temporomandibular joint with interposition material','Gap arthroplasty of temporomandibular joint with interposition material','','');
insert into b_icd9 values ('0555000000717','1622698','Other Excisional operation of temporomandibular joint','Other Excisional operation of temporomandibular joint','','');
insert into b_icd9 values ('0555000000718','1623600','Closed reduction of temporomandibular dislocation','Closed reduction of temporomandibular dislocation','','');
insert into b_icd9 values ('0555000000719','1623601','Open reduction of temporomandibular dislocation','Open reduction of temporomandibular dislocation','','');
insert into b_icd9 values ('0555000000720','1623810','Arthroscopic temporomandibular articular disc repositioning and stabilization','Arthroscopic temporomandibular articular disc repositioning and stabilization','','');
insert into b_icd9 values ('0555000000721','1623900','Repair of temporomandibular joint capsule','Repair of temporomandibular joint capsule','','');
insert into b_icd9 values ('0555000000722','1623901','Repair of temporomandibular joint meniscus, Repair of temporomandibular joint disc','Repair of temporomandibular joint meniscus, Repair of temporomandibular joint disc','','');
insert into b_icd9 values ('0555000000723','1623998','Other repair of temporomandibular joint','Other repair of temporomandibular joint','','');
insert into b_icd9 values ('0555000000724','1624400','Operative insertion of articular disc of temporomandibular joint','Operative insertion of articular disc of temporomandibular joint','','');
insert into b_icd9 values ('0555000000725','1624800','Arthroplasty of temporomandibular joint','Arthroplasty of temporomandibular joint','','');
insert into b_icd9 values ('0555000000726','1624801','Reconstruction of temporomandibular joint with autogeneous material','Reconstruction of temporomandibular joint with autogeneous material','','');
insert into b_icd9 values ('0555000000727','1624802','Reconstruction of temporomandibular joint with alloplastic material','Reconstruction of temporomandibular joint with alloplastic material','','');
insert into b_icd9 values ('0555000000728','1624898','Other reconstruction of temporomandibular joint','Other reconstruction of temporomandibular joint','','');
insert into b_icd9 values ('0555000000729','1628100','Injection of therapeutic substance into temporomandibular joint','Injection of therapeutic substance into temporomandibular joint','','');
insert into b_icd9 values ('0555000000730','1629999','Other procedures and operations on temporomandibular  joint','Other procedures and operations on temporomandibular  joint','','');
insert into b_icd9 values ('0555000000731','1630400','Biopsy of eyelids','ตัดชิ้นเนื้อเพื่อชันสูตรบริเวณเปลือกตา','','');
insert into b_icd9 values ('0555000000732','1630499','Other diagnostic procedure on eyelid','Other diagnostic procedure on eyelid','','');
insert into b_icd9 values ('0555000000733','1632100','Incision of lid margin','Incision of lid margin','','');
insert into b_icd9 values ('0555000000734','1632199','Other incision of eyelid','Other incision of eyelid','','');
insert into b_icd9 values ('0555000000735','1632200','Blepharotomy, drainage of abscess, eyelid','Blepharotomy, drainage of abscess, eyelid','','');
insert into b_icd9 values ('0555000000736','1632400','Canthotomy(separate procedure), Enlargement of palpebral fissure','Canthotomy(separate procedure), Enlargement of palpebral fissure','','');
insert into b_icd9 values ('0555000000737','1632401','Severing of tarsorrhaphy, Split of tarsorrhaphy','Severing of tarsorrhaphy, Split of tarsorrhaphy','','');
insert into b_icd9 values ('0555000000738','1633900','Linear repair of laceration of eyelid','Linear repair of laceration of eyelid','','');
insert into b_icd9 values ('0555000000739','1633901','Linear repair of laceration of eyebrow','Linear repair of laceration of eyebrow','','');
insert into b_icd9 values ('0555000000740','1633910','Repair of laceration involving lid margin, partial-thickness','Repair of laceration involving lid margin, partial-thickness','','');
insert into b_icd9 values ('0555000000741','1633918','Other repair of laceration of eyelid, partial-thickness','Other repair of laceration of eyelid, partial-thickness','','');
insert into b_icd9 values ('0555000000742','1633920','Repair of laceration involving lid margin, full-thickness','Repair of laceration involving lid margin, full-thickness','','');
insert into b_icd9 values ('0555000000743','1633928','Other repair of laceration of eyelid, full-thickness','Other repair of laceration of eyelid, full-thickness','','');
insert into b_icd9 values ('0555000000744','1633999','Repair of eyelid, unspecified, Repair of eyelid NOS','Repair of eyelid, unspecified, Repair of eyelid NOS','','');
insert into b_icd9 values ('0555000000745','1639999','Other procedures and operations on eyelids','การทำหัตถการและการผ่าตัดอื่นๆบริเวณเปลือกตา','','');
insert into b_icd9 values ('0555000000746','1640400','Biopsy of lacrimal gland','Biopsy of lacrimal gland','','');
insert into b_icd9 values ('0555000000747','1640498','Other diagnostics procedures on lacrimal gland','Other diagnostics procedures on lacrimal gland','','');
insert into b_icd9 values ('0555000000748','1640499','Unspecified diagnostics procedurres on lacrimal gland','Unspecified diagnostics procedurres on lacrimal gland','','');
insert into b_icd9 values ('0555000000749','1642100','Incision of lacrimal gland , Incision on lacrimal cyst /abscess(with drainage)','Incision of lacrimal gland , Incision on lacrimal cyst /abscess(with drainage)','','');
insert into b_icd9 values ('0555000000750','1642600','Excision or lacrimal gland ; partial, except tumor Exclude : Excision for tumor (164-26-01)','Excision or lacrimal gland ; partial, except tumor Exclude : Excision for tumor (164-26-01)','','');
insert into b_icd9 values ('0555000000751','1642601','Excision or lacrimal gland ; partial, for tumor Exclude : Biopsy of Laacrimal gland (164 -04-00)','Excision or lacrimal gland ; partial, for tumor Exclude : Biopsy of Laacrimal gland (164 -04-00)','','');
insert into b_icd9 values ('0555000000752','1649999','Other procedures and operations on lacrimal grand','Other procedures and operations on lacrimal grand','','');
insert into b_icd9 values ('0555000000753','1651200','Replacement of stent in nasolacrimal duct','Replacement of stent in nasolacrimal duct','','');
insert into b_icd9 values ('0555000000754','1651500','Probing of lacrimal punctum, unilateral or bilateral','Probing of lacrimal punctum, unilateral or bilateral','','');
insert into b_icd9 values ('0555000000755','1651501','Probing of lacrimal canaliculi, unilateral or bilateral','Probing of lacrimal canaliculi, unilateral or bilateral','','');
insert into b_icd9 values ('0555000000756','1651502','Probing of nasolacrimal duct, unilateral or bilateral Exclude : that with insertion of tube or stent(165-15-03)','Probing of nasolacrimal duct, unilateral or bilateral Exclude : that with insertion of tube or stent(165-15-03)','','');
insert into b_icd9 values ('0555000000757','1651503','Intubation of nasolacrimal duct, unilateral or bilateral','Intubation of nasolacrimal duct, unilateral or bilateral','','');
insert into b_icd9 values ('0555000000758','1651504','Probing of nasolacrimal duct, unilateral or bilateral, under general anesthesia','Probing of nasolacrimal duct, unilateral or bilateral, under general anesthesia','','');
insert into b_icd9 values ('0555000000759','1652101','Incision of lacrimal canaliculi','Incision of lacrimal canaliculi','','');
insert into b_icd9 values ('0555000000760','1652102','Incision of lacrimal sac, Incision of lacrimal sac(with drainage), Dacryocystotomy, Dacryocystostomy','Incision of lacrimal sac, Incision of lacrimal sac(with drainage), Dacryocystotomy, Dacryocystostomy','','');
insert into b_icd9 values ('0555000000761','1653301','Dacryocystorhinostomy(DCR)','Dacryocystorhinostomy(DCR)','','');
insert into b_icd9 values ('0555000000762','1658300','Irrigation of lacrimal passage','Irrigation of lacrimal passage','','');
insert into b_icd9 values ('0555000000763','1659999','Other procedures and operations on lacrimal system','Other procedures and operations on lacrimal system','','');
insert into b_icd9 values ('0555000000764','1662108','Other incision of conjunctiva for surgical approach','Other incision of conjunctiva for surgical approach','','');
insert into b_icd9 values ('0555000000765','1668100','Subconjunctival injection','Subconjunctival injection','','');
insert into b_icd9 values ('0555000000766','1669999','Other procedures and operations on conjunctiva','Other procedures and operations on conjunctiva','','');
insert into b_icd9 values ('0555000000767','1769999','Other procedures and operations on conjunctiva','การทำหัตถการและการผ่าตัดอื่นๆบริเวณตา','','');
insert into b_icd9 values ('0555000000768','1772100','Orbitotomy ; transconjunctival approach for exploration Include : for biopsy, drainageof retrobulbar abscess, Optic nerve sheath decompression','Orbitotomy ; transconjunctival approach for exploration Include : for biopsy, drainageof retrobulbar abscess, Optic nerve sheath decompression','','');
insert into b_icd9 values ('0555000000769','1772110','Orbitotomy with bone flap (lateral approach) for exploration, Orbitotomy with lateral approach (Kroenlein operation) Include : for biopsy, drainage of retrobulbar abscess','Orbitotomy with bone flap (lateral approach) for exploration, Orbitotomy with lateral approach (Kroenlein operation) Include : for biopsy, drainage of retrobulbar abscess','','');
insert into b_icd9 values ('0555000000770','1772111','Orbitotomy with bone flap (lateral approach) for removal of lesion, Orbitotomy with lateral approach (Kroenlein operation) Include : for removal of foreign body for decompression of orbit/optic nerve','Orbitotomy with bone flap (lateral approach) for removal of lesion, Orbitotomy with lateral approach (Kroenlein operation) Include : for removal of foreign body for decompression of orbit/optic nerve','','');
insert into b_icd9 values ('0555000000771','1712112','Orbitotomy with bone flap ( medial approach) for exploration Include : for biopsy, drainageof retrobullbar abscess','Orbitotomy with bone flap ( medial approach) for exploration Include : for biopsy, drainageof retrobullbar abscess','','');
insert into b_icd9 values ('0555000000772','1772113','Orbitotomy with bone flap (medial approach) for removal of lesion Include : for removal of foreign body for decompression of orbit/optic nerve','Orbitotomy with bone flap (medial approach) for removal of lesion Include : for removal of foreign body for decompression of orbit/optic nerve','','');
insert into b_icd9 values ('0555000000773','1772600','Debridement of fracture of orbit','Debridement of fracture of orbit','','');
insert into b_icd9 values ('0555000000774','1772601','Excision of lesion of orbit','Excision of lesion of orbit','','');
insert into b_icd9 values ('0555000000775','1772610','Exenteration of orbit with removal of orbital content only','Exenteration of orbit with removal of orbital content only','','');
insert into b_icd9 values ('0555000000776','1772611','Exenteration of orbit with removal of orbital bone','Exenteration of orbit with removal of orbital bone','','');
insert into b_icd9 values ('0555000000777','1772612','Exenteration of orbit with removal of orbital adjacent structures, Radical orbitomaxillectomy','Exenteration of orbit with removal of orbital adjacent structures, Radical orbitomaxillectomy','','');
insert into b_icd9 values ('0555000000778','1772613','Exenteration of orbit with removal of orbital content with muscle or myocutaneous flap/temporalis muscle transplant','Exenteration of orbit with removal of orbital content with muscle or myocutaneous flap/temporalis muscle transplant','','');
insert into b_icd9 values ('0555000000779','1772619','Other exenteration of orbit, exenteration of orbit NOS','Other exenteration of orbit, exenteration of orbit NOS','','');
insert into b_icd9 values ('0555000000780','1773600','Reduction of fracture of orbit','Reduction of fracture of orbit','','');
insert into b_icd9 values ('0555000000781','1773700','External fixation of fracture of orbit','External fixation of fracture of orbit','','');
insert into b_icd9 values ('0555000000782','1773701','Internal fixation of fracture of orbit','Internal fixation of fracture of orbit','','');
insert into b_icd9 values ('0555000000783','1773900','Repair of wound of orbit Excludes : Reduction of fracture of orbit(see chapter 1 for more codes on reconstruction of facial bone), Repair of extraocular muscles(175-39-00)','Repair of wound of orbit Excludes : Reduction of fracture of orbit(see chapter 1 for more codes on reconstruction of facial bone), Repair of extraocular muscles(175-39-00)','','');
insert into b_icd9 values ('0555000000784','1773999','Other repair of injury of orbit','Other repair of injury of orbit','','');
insert into b_icd9 values ('0555000000785','1774500','Removal of penetrating foreign body from orbit','Removal of penetrating foreign body from orbit','','');
insert into b_icd9 values ('0555000000786','1774501','Removal of ocular implant (Implant inside muscle cone) Include : removal of glllllllass ball, Hydroxyapatite','Removal of ocular implant (Implant inside muscle cone) Include : removal of glllllllass ball, Hydroxyapatite','','');
insert into b_icd9 values ('0555000000787','1774502','Removal of orbital implant (Implant outside muscle cone), off plate screw','Removal of orbital implant (Implant outside muscle cone), off plate screw','','');
insert into b_icd9 values ('0555000000788','1774700','Revision of anopthalmic socket with graft(any type) Include : procedures on anophthalmic orbit or socket','Revision of anopthalmic socket with graft(any type) Include : procedures on anophthalmic orbit or socket','','');
insert into b_icd9 values ('0555000000789','1774701','Revision of anopthalmic socket and insertion of ocular implant ; without any graft','Revision of anopthalmic socket and insertion of ocular implant ; without any graft','','');
insert into b_icd9 values ('0555000000790','1774702','Revision of anopthalmic socket and insertion of ocular implant ; with any graft','Revision of anopthalmic socket and insertion of ocular implant ; with any graft','','');
insert into b_icd9 values ('0555000000791','1774703','Revision of anopthalmic socket and insertion of ocular implant ; with use of any material for reinforcement and/or attachment of muscle of implant','Revision of anopthalmic socket and insertion of ocular implant ; with use of any material for reinforcement and/or attachment of muscle of implant','','');
insert into b_icd9 values ('0555000000792','1774708','Other revision of anopthalmic socket','Other revision of anopthalmic socket','','');
insert into b_icd9 values ('0555000000793','1774710','Modification of ocular implant Include: with placement or replacement of pegs, Drilling receptacle for prosthesis appendage','Modification of ocular implant Include: with placement or replacement of pegs, Drilling receptacle for prosthesis appendage','','');
insert into b_icd9 values ('0555000000794','1774711','Secondary graft to exenteration cavity','Secondary graft to exenteration cavity','','');
insert into b_icd9 values ('0555000000795','1774718','Other revision of exenteration cavity','Other revision of exenteration cavity','','');
insert into b_icd9 values ('0555000000796','1774719','Other secondary procedures after removal of eyeball','Other secondary procedures after removal of eyeball','','');
insert into b_icd9 values ('0555000000797','1778100','Retrobulbar injection of therapeutic agent','Retrobulbar injection of therapeutic agent','','');
insert into b_icd9 values ('0555000000798','1779999','Other procedures and operations on orbit','Other procedures and operations on orbit','','');
insert into b_icd9 values ('0555000000799','1780000','Otoscopy','การส่องกล้องตรวจรูหู','','');
insert into b_icd9 values ('0555000000800','1780400','Biopsy of external ear','การตัดชิ้นเนื้อไปชันสูตรบริเวณหู','','');
insert into b_icd9 values ('0555000000801','1780401','Biopsy of lesion of pinna','Biopsy of lesion of pinna','','');
insert into b_icd9 values ('0555000000802','1780402','Biopsy of external auditory canal lesion of tissue','Biopsy of external auditory canal lesion of tissue','','');
insert into b_icd9 values ('0555000000803','1780498','Other diagnostic procedures on external ear','Other diagnostic procedures on external ear','','');
insert into b_icd9 values ('0555000000804','1780499','Unspecified diagnostic procedures on external ear','Unspecified diagnostic procedures on external ear','','');
insert into b_icd9 values ('0555000000805','1781700','Packing of external auditory canal','Packing of external auditory canal','','');
insert into b_icd9 values ('0555000000806','1782201','Drainage of seroma or hematoma of pinna','Drainage of seroma or hematoma of pinna','','');
insert into b_icd9 values ('0555000000807','1783900','Suture of laceration of external ear or pinna','Suture of laceration of external ear or pinna','','');
insert into b_icd9 values ('0555000000808','1783901','Suture of laceration of external auditory canal','Suture of laceration of external auditory canal','','');
insert into b_icd9 values ('0555000000809','1784800','Surgical correction of prominent ear, Ear pining, Ear setback','Surgical correction of prominent ear, Ear pining, Ear setback','','');
insert into b_icd9 values ('0555000000810','1784801','Construction of auricle of ear, Prosthetic appliance for absent ear, Prosthetic appliance for absent ear, Reconstruction of auricle','Construction of auricle of ear, Prosthetic appliance for absent ear, Prosthetic appliance for absent ear, Reconstruction of auricle','','');
insert into b_icd9 values ('0555000000811','1788000','Dressing ound of external ear','Dressing ound of external ear','','');
insert into b_icd9 values ('0555000000812','1789999','Other operation on external ear','การทำหัตถการและการผ่าตัดอื่นๆบริเวณ ใบหู','','');
insert into b_icd9 values ('0555000000813','1850400','Biopsy of nose','การตัดชิ้นเนื้อไปชันสูตรจากบริเวณจมูก','','');
insert into b_icd9 values ('0555000000814','1852101','Incision of skin of nose','Incision of skin of nose','','');
insert into b_icd9 values ('0555000000815','1852600','Excision of naso - alveolar cyst','Excision of naso - alveolar cyst','','');
insert into b_icd9 values ('0555000000816','1852601','Resection of nose','Resection of nose','','');
insert into b_icd9 values ('0555000000817','1852698','Local excision of other lesion of nose','Local excision of other lesion of nose','','');
insert into b_icd9 values ('0555000000818','1852699','Local excision of  lesion of nose, not otherwise specified','Local excision of  lesion of nose, not otherwise specified','','');
insert into b_icd9 values ('0555000000819','1852799','Local destruction of other lesion of nose, not otherwise specified','Local destruction of other lesion of nose, not otherwise specified','','');
insert into b_icd9 values ('0555000000820','1852800','Destruction of lesion of nose','Destruction of lesion of nose','','');
insert into b_icd9 values ('0555000000821','1852808','Local destruction of other lesion of nose','Local destruction of other lesion of nose','','');
insert into b_icd9 values ('0555000000822','1852900','auputation of nose','auputation of nose','','');
insert into b_icd9 values ('0555000000823','1853900','Suture of laceration of nose','Suture of laceration of nose','','');
insert into b_icd9 values ('0555000000824','1853999','Other repair and plastic operation on nose','Other repair and plastic operation on nose','','');
insert into b_icd9 values ('0555000000825','1854000','Closure of nosal fistular, Nasolabial fistulectomy, Nasopharyngeal fistulectomy, Oronasal fistulectomy','Closure of nosal fistular, Nasolabial fistulectomy, Nasopharyngeal fistulectomy, Oronasal fistulectomy','','');
insert into b_icd9 values ('0555000000826','1858000','Dressing wound on nose','Dressing wound on nose','','');
insert into b_icd9 values ('0555000000827','1859999','Other operation on nose','การทำหัตถการและการผ่าตัดอื่นๆบริเวณจมูก','','');
insert into b_icd9 values ('0555000000828','1860000','Rhinoscopy','การส่องกล้องตรวจโพรงจมูก','','');
insert into b_icd9 values ('0555000000829','1860001','Rhinoscopy using endoscope','Rhinoscopy using endoscope','','');
insert into b_icd9 values ('0555000000830','1861100','Removal of nasal packing','Removal of nasal packing','','');
insert into b_icd9 values ('0555000000831','1861200','Replacement of nasal packing','Replacement of nasal packing','','');
insert into b_icd9 values ('0555000000832','1862500','Control of epistaxis by anterior nasal packing','Control of epistaxis by anterior nasal packing','','');
insert into b_icd9 values ('0555000000833','1862501','Control of epistaxis by posterior (and anterior)nasal packing','Control of epistaxis by posterior (and anterior)nasal packing','','');
insert into b_icd9 values ('0555000000834','1862502','Control of epistaxis by cauterization( and packing )','Control of epistaxis by cauterization( and packing )','','');
insert into b_icd9 values ('0555000000835','1862503','Control of epistaxis by ligation of ethmoid arteries','Control of epistaxis by ligation of ethmoid arteries','','');
insert into b_icd9 values ('0555000000836','1862504','Control of epistaxis by endoscopic ligation of the sphenopalatine artery','Control of epistaxis by endoscopic ligation of the sphenopalatine artery','','');
insert into b_icd9 values ('0555000000837','1862505','Control of epistaxis by ( transantral) ligation of the maxillary artery','Control of epistaxis by ( transantral) ligation of the maxillary artery','','');
insert into b_icd9 values ('0555000000838','1862506','Control of epistaxis by ligation of the external carotid artery','Control of epistaxis by ligation of the external carotid artery','','');
insert into b_icd9 values ('0555000000839','1862507','Control of epistaxis by excision of nasal mucosa and skin grafting of septum and lateral nasal wall','Control of epistaxis by excision of nasal mucosa and skin grafting of septum and lateral nasal wall','','');
insert into b_icd9 values ('0555000000840','1862508','Control of epitaxis by others means','Control of epitaxis by others means','','');
insert into b_icd9 values ('0555000000841','1862599','Control of epitaxis, not otherwise specified','Control of epitaxis, not otherwise specified','','');
insert into b_icd9 values ('0555000000842','1868300','Irrigation of nasal passages','Irrigation of nasal passages','','');
insert into b_icd9 values ('0555000000843','1869999','Other operation on nasal cavity','การทำหัตถการและการผ่าตัดอื่นๆบริเวณโพรงจมูก','','');
insert into b_icd9 values ('0555000000844','1872400','Fracture of the turbinates','Fracture of the turbinates','','');
insert into b_icd9 values ('0555000000845','1872600','Turbinectomy by diathermy or cryosurgery','Turbinectomy by diathermy or cryosurgery','','');
insert into b_icd9 values ('0555000000846','1872601','Laser turbinectomy','Laser turbinectomy','','');
insert into b_icd9 values ('0555000000847','1872699','Other turbinectomy','Other turbinectomy','','');
insert into b_icd9 values ('0555000000848','1882100','Nasal septotomy diathermy','Nasal septotomy diathermy','','');
insert into b_icd9 values ('0555000000849','1882200','Drainage of septal abscess','Drainage of septal abscess','','');
insert into b_icd9 values ('0555000000850','1882600','Submucous resection of septum of nose','Submucous resection of septum of nose','','');
insert into b_icd9 values ('0555000000851','1884700','Revision rhinoplasty, Rhinoseptoplasty, Twisted nose rhinoplasty','Revision rhinoplasty, Rhinoseptoplasty, Twisted nose rhinoplasty','','');
insert into b_icd9 values ('0555000000852','1884800','Limited rhinoplasty, Plastic repair of nasolabial flaps, Tip rhinoplasty','Limited rhinoplasty, Plastic repair of nasolabial flaps, Tip rhinoplasty','','');
insert into b_icd9 values ('0555000000853','1884801','Augmentation rhinoplasty, Augmentation rhinoplasty with : graft, synthetic implant','Augmentation rhinoplasty, Augmentation rhinoplasty with : graft, synthetic implant','','');
insert into b_icd9 values ('0555000000854','1884808','Other rhinoplasty','Other rhinoplasty','','');
insert into b_icd9 values ('0555000000855','1884819','Other septoplasty, Crushing of nasal septum, Repair of septal perforation Excludes : septoplasty with synchonous submucous resection of septum ( 188-26-00)','Other septoplasty, Crushing of nasal septum, Repair of septal perforation Excludes : septoplasty with synchonous submucous resection of septum ( 188-26-00)','','');
insert into b_icd9 values ('0555000000856','1890500','Plain radiography of frontal sinus','Plain radiography of frontal sinus','','');
insert into b_icd9 values ('0555000000857','1890800','Computed tomographyof frontal sinus','Computed tomographyof frontal sinus','','');
insert into b_icd9 values ('0555000000858','1892210','Frontal sinusotomy','Frontal sinusotomy','','');
insert into b_icd9 values ('0555000000859','1892211','Frontal sinusotomy using endoscope','Frontal sinusotomy using endoscope','','');
insert into b_icd9 values ('0555000000860','1892212','Frontal sinusotomy, external approach','Frontal sinusotomy, external approach','','');
insert into b_icd9 values ('0555000000861','1892600','Excision of lesion of frontal sinus','Excision of lesion of frontal sinus','','');
insert into b_icd9 values ('0555000000862','1892601','Excision of lesion of frontal sinus using endoscope','Excision of lesion of frontal sinus using endoscope','','');
insert into b_icd9 values ('0555000000863','1892602','Excision of lesion of frontal sinus, external approach','Excision of lesion of frontal sinus, external approach','','');
insert into b_icd9 values ('0555000000864','1892603','Frontal sinusectomy','Frontal sinusectomy','','');
insert into b_icd9 values ('0555000000865','1892699','Sinusectomy: not otherwise specifieds; frontal sinus','Sinusectomy: not otherwise specifieds; frontal sinus','','');
insert into b_icd9 values ('0555000000866','1892800','Obliteration of frontal sinus(with fat), Osteoplastic flap','Obliteration of frontal sinus(with fat), Osteoplastic flap','','');
insert into b_icd9 values ('0555000000867','1893100','Dilation of frontonasal duct','Dilation of frontonasal duct','','');
insert into b_icd9 values ('0555000000868','1894800','Reconstruction of frontal nasal','Reconstruction of frontal nasal','','');
insert into b_icd9 values ('0555000000869','1899999','Other procedures and operations on frontal nasal sinus','Other procedures and operations on frontal nasal sinus','','');
insert into b_icd9 values ('0555000000870','1920000','Transillumination of maxillary nasal sinus','Transillumination of maxillary nasal sinus','','');
insert into b_icd9 values ('0555000000871','1920401','Open biopsy of maxillary nasal sinus for aspiration or lavage','Open biopsy of maxillary nasal sinus for aspiration or lavage','','');
insert into b_icd9 values ('0555000000872','1920500','Plain radiography for maxillary sinus','Plain radiography for maxillary sinus','','');
insert into b_icd9 values ('0555000000873','1920800','Computed tomography of maxillary sinus','Computed tomography of maxillary sinus','','');
insert into b_icd9 values ('0555000000874','1920920','Magnetic resonance imaging (MRI) of maxillary sinus','Magnetic resonance imaging (MRI) of maxillary sinus','','');
insert into b_icd9 values ('0555000000875','1922100','Incision of maxillary nasal sinus','Incision of maxillary nasal sinus','','');
insert into b_icd9 values ('0555000000876','1922201','Puncture of maxillary nasal sinus for aspiration of lavage','Puncture of maxillary nasal sinus for aspiration of lavage','','');
insert into b_icd9 values ('0555000000877','1922210','Intranasal antrotomy, Inferior antrostomy, Middle antrosttomy','Intranasal antrotomy, Inferior antrostomy, Middle antrosttomy','','');
insert into b_icd9 values ('0555000000878','1922211','Nasoantral window','Nasoantral window','','');
insert into b_icd9 values ('0555000000879','1922212','Radical maxillary antrotomy, Removal of lining membrane of maxillary sinus using Caldwell-Luc approach, Antrectomy or classical Caldwell-Luc operation','Radical maxillary antrotomy, Removal of lining membrane of maxillary sinus using Caldwell-Luc approach, Antrectomy or classical Caldwell-Luc operation','','');
insert into b_icd9 values ('0555000000880','1922219','Other external maxillary antrotomy, Exploration of maxillary antrum with Caldwell-luc approach','Other external maxillary antrotomy, Exploration of maxillary antrum with Caldwell-luc approach','','');
insert into b_icd9 values ('0555000000881','1924000','Closure of maxillary nasal sinus fistula','Closure of maxillary nasal sinus fistula','','');
insert into b_icd9 values ('0555000000882','1929999','Other procedures and operations on maxillary nasal sinus','Other procedures and operations on maxillary nasal sinus','','');
insert into b_icd9 values ('0555000000883','1930000','Transillumination of nasal','Transillumination of nasal','','');
insert into b_icd9 values ('0555000000884','1930001','Endoscopy of nasal sinuses (without biopsy)','Endoscopy of nasal sinuses (without biopsy)','','');
insert into b_icd9 values ('0555000000885','1930400','Close(endoscopic)(needle) biopsy of nasal sinus','Close(endoscopic)(needle) biopsy of nasal sinus','','');
insert into b_icd9 values ('0555000000886','1930401','Open biopsy of nasal sinus','Open biopsy of nasal sinus','','');
insert into b_icd9 values ('0555000000887','1930499','Other diagnostic procedures on nasal sinuses','Other diagnostic procedures on nasal sinuses','','');
insert into b_icd9 values ('0555000000888','1930500','Plain radiography of paranasal sinus','Plain radiography of paranasal sinus','','');
insert into b_icd9 values ('0555000000889','1932100','Incision of multiple nasal sinuses','Incision of multiple nasal sinuses','','');
insert into b_icd9 values ('0555000000890','1932200','Aspiration and lavage of nasal sinus through natural ostium','Aspiration and lavage of nasal sinus through natural ostium','','');
insert into b_icd9 values ('0555000000891','1932201','Puncture of nasal sinus for aspiration or lavage','Puncture of nasal sinus for aspiration or lavage','','');
insert into b_icd9 values ('0555000000892','1932209','Aspiration and lavage of nasal sinus, not otherwise specified','Aspiration and lavage of nasal sinus, not otherwise specified','','');
insert into b_icd9 values ('0555000000893','1932219','Sinussotomy, not otherwise specified','Sinussotomy, not otherwise specified','','');
insert into b_icd9 values ('0555000000894','1932600','Sinusectomy','Sinusectomy','','');
insert into b_icd9 values ('0555000000895','1932699','Sinusectomy, not otherwise specified','Sinusectomy, not otherwise specified','','');
insert into b_icd9 values ('0555000000896','1933999','Other repair of nasal sinus, Reconstruction of frontonasal duct, Repair of bone of accessory sinus','Other repair of nasal sinus, Reconstruction of frontonasal duct, Repair of bone of accessory sinus','','');
insert into b_icd9 values ('0555000000897','1934000','Closure of nasal sinus fistula, Repair of oro-antral fistula','Closure of nasal sinus fistula, Repair of oro-antral fistula','','');
insert into b_icd9 values ('0555000000898','1939999','Oter operation on nasal sinuses','Oter operation on nasal sinuses','','');
insert into b_icd9 values ('0555000000899','1950400','Closed(needle) biopsy of parotid gland or duct','Closed(needle) biopsy of parotid gland or duct','','');
insert into b_icd9 values ('0555000000900','1950401','Open biopsy of parotid gland or duct','Open biopsy of parotid gland or duct','','');
insert into b_icd9 values ('0555000000901','1950499','Other diagnostic procedures on parotid gland or ducts','Other diagnostic procedures on parotid gland or ducts','','');
insert into b_icd9 values ('0555000000902','1951100','Removal of calculus from parotid salivary duct','Removal of calculus from parotid salivary duct','','');
insert into b_icd9 values ('0555000000903','1952200','Drainage of abscess of parotid area','Drainage of abscess of parotid area','','');
insert into b_icd9 values ('0555000000904','1952600','Superficial parotidectomy, Partial parotidectomy','Superficial parotidectomy, Partial parotidectomy','','');
insert into b_icd9 values ('0555000000905','1952601','Total parotidectomy','Total parotidectomy','','');
insert into b_icd9 values ('0555000000906','1952698','Other excision of parotid gland lesion','Other excision of parotid gland lesion','','');
insert into b_icd9 values ('0555000000907','1952699','Parotidectomy, not otherwise specified','Parotidectomy, not otherwise specified','','');
insert into b_icd9 values ('0555000000908','1953100','Dilatation of parotid salivary duct','Dilatation of parotid salivary duct','','');
insert into b_icd9 values ('0555000000909','1953900','Repair of parotid salivary duct','Repair of parotid salivary duct','','');
insert into b_icd9 values ('0555000000910','1953901','Suture of laceration of submandibular  salivary gland','Suture of laceration of submandibular  salivary gland','','');
insert into b_icd9 values ('0555000000911','1953999','Other repair on parotid gland or duct, Fistulization of parotid gland','Other repair on parotid gland or duct, Fistulization of parotid gland','','');
insert into b_icd9 values ('0555000000912','1954000','Closure of parotid gland fistula','Closure of parotid gland fistula','','');
insert into b_icd9 values ('0555000000913','1954500','Open removal of calculus from parotid salivary duct','Open removal of calculus from parotid salivary duct','','');
insert into b_icd9 values ('0555000000914','1954899','Other plastic operation on parotid gland or duct, Plastic repair of parotid gland, Transplantation of parotid gland','Other plastic operation on parotid gland or duct, Plastic repair of parotid gland, Transplantation of parotid gland','','');
insert into b_icd9 values ('0555000000915','1959999','Other operation of parotoid salivary gland or duct','Other operation of parotoid salivary gland or duct','','');
insert into b_icd9 values ('0555000000916','1960400','Closed(needle) biopsy of submandibular gland or duct','Closed(needle) biopsy of submandibular gland or duct','','');
insert into b_icd9 values ('0555000000917','1960401','Open biopsy of submandibular gland or duct','Open biopsy of submandibular gland or duct','','');
insert into b_icd9 values ('0555000000918','1960499','Other diagnostic procedures on submandibular gland or ducts','Other diagnostic procedures on submandibular gland or ducts','','');
insert into b_icd9 values ('0555000000919','1961100','Removal of calculus from submandibular gland','Removal of calculus from submandibular gland','','');
insert into b_icd9 values ('0555000000920','1962200','Drainage of abscess of submandibular gland','Drainage of abscess of submandibular gland','','');
insert into b_icd9 values ('0555000000921','1962600','Partial excision of submandibular gland','Partial excision of submandibular gland','','');
insert into b_icd9 values ('0555000000922','1962601','Totaal excision of submandibular gland','Totaal excision of submandibular gland','','');
insert into b_icd9 values ('0555000000923','1962608','Other excision of submandibular gland','Other excision of submandibular gland','','');
insert into b_icd9 values ('0555000000924','1962699','Excision of submandibular gland, not otherwise specified','Excision of submandibular gland, not otherwise specified','','');
insert into b_icd9 values ('0555000000925','1963100','Dilatation of submandibular duct','Dilatation of submandibular duct','','');
insert into b_icd9 values ('0555000000926','1963900','Repair of submandibular duct','Repair of submandibular duct','','');
insert into b_icd9 values ('0555000000927','1963901','Suture of laceration of submandibular salivary gland','Suture of laceration of submandibular salivary gland','','');
insert into b_icd9 values ('0555000000928','1963999','Other repair on submandibular gland or duct, Fistulization of submandibular gland','Other repair on submandibular gland or duct, Fistulization of submandibular gland','','');
insert into b_icd9 values ('0555000000929','1964000','Closure of submandibular gland fistular','Closure of submandibular gland fistular','','');
insert into b_icd9 values ('0555000000930','1964500','Open removal of calculus from submandibular duct','Open removal of calculus from submandibular duct','','');
insert into b_icd9 values ('0555000000931','1964899','Other plastic operation on submandibular gland or duct','Other plastic operation on submandibular gland or duct','','');
insert into b_icd9 values ('0555000000932','1969999','Other  operation on submandibular gland or duct','Other  operation on submandibular gland or duct','','');
insert into b_icd9 values ('0555000000933','1980400','Closed(needle) biopsy of salivary gland','Closed(needle) biopsy of salivary gland','','');
insert into b_icd9 values ('0555000000934','1980401','Open biopsy of salivary gland','Open biopsy of salivary gland','','');
insert into b_icd9 values ('0555000000935','1980409','Other diagnostic procedures on salivary gland or duct','Other diagnostic procedures on salivary gland or duct','','');
insert into b_icd9 values ('0555000000936','1981100','Removal of calculus from salivary gland','Removal of calculus from salivary gland','','');
insert into b_icd9 values ('0555000000937','1982100','Incision of salivary gland or duct','Incision of salivary gland or duct','','');
insert into b_icd9 values ('0555000000938','1982200','Drainage of abscess of salivary gland','Drainage of abscess of salivary gland','','');
insert into b_icd9 values ('0555000000939','1982201','Marsupialization of salivary gland or duct','Marsupialization of salivary gland or duct','','');
insert into b_icd9 values ('0555000000940','1982600','Partial sialoadenectomy','Partial sialoadenectomy','','');
insert into b_icd9 values ('0555000000941','1982601','Complete sialoadenectomy','Complete sialoadenectomy','','');
insert into b_icd9 values ('0555000000942','1982608','Other excision of salivary gland lesion','Other excision of salivary gland lesion','','');
insert into b_icd9 values ('0555000000943','1982699','Sialoadectomy, not otherwise specified','Sialoadectomy, not otherwise specified','','');
insert into b_icd9 values ('0555000000944','1983100','Dilatation of salivary duct','Dilatation of salivary duct','','');
insert into b_icd9 values ('0555000000945','1983900','Repair of salivary duct','Repair of salivary duct','','');
insert into b_icd9 values ('0555000000946','1983901','Suture of laceration of salivary gland','Suture of laceration of salivary gland','','');
insert into b_icd9 values ('0555000000947','1983999','Other repair on salivary gland or duct, Fistulization of salivary gland','Other repair on salivary gland or duct, Fistulization of salivary gland','','');
insert into b_icd9 values ('0555000000948','1984000','Closure of salivary gland fistula','Closure of salivary gland fistula','','');
insert into b_icd9 values ('0555000000949','1984400','Probing of salivary duct','Probing of salivary duct','','');
insert into b_icd9 values ('0555000000950','1984500','Open removal of calculus from salivary duct','Open removal of calculus from salivary duct','','');
insert into b_icd9 values ('0555000000951','1984808','Other plastic operation on salivary gland or duct, Plastic repair of salivary gland, Transplantation of salivary gland','Other plastic operation on salivary gland or duct, Plastic repair of salivary gland, Transplantation of salivary gland','','');
insert into b_icd9 values ('0555000000952','1989999','Other operation of salivary gland or duct','Other operation of salivary gland or duct','','');
insert into b_icd9 values ('0555000000953','2000500','Plain radiography of neck, antero - posterior (AP) and lateral','Plain radiography of neck, antero - posterior (AP) and lateral','','');
insert into b_icd9 values ('0555000000954','2000502','Plain radiography of neck, lateral view','Plain radiography of neck, lateral view','','');
insert into b_icd9 values ('0555000000955','2001301','Appplication of neck support','Appplication of neck support','','');
insert into b_icd9 values ('0555000000956','2010400','Biopsy of neck skin and subcutaneous tissue','การตัดชิ้นเนื้อไปชันสูตรจากผิวหนังบริเวณคอ','','');
insert into b_icd9 values ('0555000000957','2011000','Insertion of totally implantable infusion pump into neck skin','Insertion of totally implantable infusion pump into neck skin','','');
insert into b_icd9 values ('0555000000958','2011001','Insertion of totally implantable vascular access device into neck skin','Insertion of totally implantable vascular access device into neck skin','','');
insert into b_icd9 values ('0555000000959','2011002','Insertion of tissue expander into neck skin','Insertion of tissue expander into neck skin','','');
insert into b_icd9 values ('0555000000960','2011100','Removal of foreign body from neck skin and subcutaneous tissue','Removal of foreign body from neck skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000961','2012100','Incision of pilonidal cyst or sinus of neck skin','Incision of pilonidal cyst or sinus of neck skin','','');
insert into b_icd9 values ('0555000000962','2012108','Other incision of neck skin and subcutaneous tissue, Exploration of sinus track, skin, Exploration of superficial fossa','Other incision of neck skin and subcutaneous tissue, Exploration of sinus track, skin, Exploration of superficial fossa','','');
insert into b_icd9 values ('0555000000963','2012200','Aspiration of neck skin and subcutaneous tissue, Aspiration of : abscess, hematoma, seroma','Aspiration of neck skin and subcutaneous tissue, Aspiration of : abscess, hematoma, seroma','','');
insert into b_icd9 values ('0555000000964','2012201','Drainage of neck skin and subcutaneous tissue, Include : abscess hematoma seroma','Drainage of neck skin and subcutaneous tissue, Include : abscess hematoma seroma','','');
insert into b_icd9 values ('0555000000965','2012600','Excisional debridement of wound, Infection or burn of neck skin, Removal by excision of: devitalized tissue ,necrosis,slough','Excisional debridement of wound, Infection or burn of neck skin, Removal by excision of: devitalized tissue ,necrosis,slough','','');
insert into b_icd9 values ('0555000000966','2012601','Excision of pilonidal cyst or sinus of neck skin, Marsupialization of cyst','Excision of pilonidal cyst or sinus of neck skin, Marsupialization of cyst','','');
insert into b_icd9 values ('0555000000967','2012602','Excision of neck skin for graft to other site','Excision of neck skin for graft to other site','','');
insert into b_icd9 values ('0555000000968','2012608','Other local excision of lesion of neck skin and subcutaneous tissue','Other local excision of lesion of neck skin and subcutaneous tissue','','');
insert into b_icd9 values ('0555000000969','2012610','Cutting and preparation of pedical grafts of flaps','Cutting and preparation of pedical grafts of flaps','','');
insert into b_icd9 values ('0555000000970','2012615','Radical excision of neck skin lesion, Wide excision of skin lesion involving underlying or adjacent structure','Radical excision of neck skin lesion, Wide excision of skin lesion involving underlying or adjacent structure','','');
insert into b_icd9 values ('0555000000971','2012805','Nonexcisional debridement of wound, infection or burn of neck skin, Removal of devitalized tissue, necrosis and slough by: brushing, irrigation(under pressure),scrubbing, washing','Nonexcisional debridement of wound, infection or burn of neck skin, Removal of devitalized tissue, necrosis and slough by: brushing, irrigation(under pressure),scrubbing, washing','','');
insert into b_icd9 values ('0555000000972','2012808','Other local destruction of lesion of neck skin and subcutaneous tissue, Destruction of neck skin by: cauterization, cryosurgery, fulgulation, laser beam, that with Z-plasty','Other local destruction of lesion of neck skin and subcutaneous tissue, Destruction of neck skin by: cauterization, cryosurgery, fulgulation, laser beam, that with Z-plasty','','');
insert into b_icd9 values ('0555000000973','2013900','Suture of laceration of neck skin','Suture of laceration of neck skin','','');
insert into b_icd9 values ('0555000000974','2013908','Other repair of neck skin','Other repair of neck skin','','');
insert into b_icd9 values ('0555000000975','2014700','Revision of pedicle or flap graft to neck skin','Revision of pedicle or flap graft to neck skin','','');
insert into b_icd9 values ('0555000000976','2014800','Advancement of pedicle graft to neck skin','Advancement of pedicle graft to neck skin','','');
insert into b_icd9 values ('0555000000977','2014801','Attachment  of pedicle or flap graft of neck skin','Attachment  of pedicle or flap graft of neck skin','','');
insert into b_icd9 values ('0555000000978','2014802','Neck skin reduction plastic operation','Neck skin reduction plastic operation','','');
insert into b_icd9 values ('0555000000979','2014803','Relaxation of scar or web contracture of neck skin','Relaxation of scar or web contracture of neck skin','','');
insert into b_icd9 values ('0555000000980','2014809','Other reconstruction of neck skin','Other reconstruction of neck skin','','');
insert into b_icd9 values ('0555000000981','2015000','Suture of laceration of neck skin','Suture of laceration of neck skin','','');
insert into b_icd9 values ('0555000000982','2015001','Split- thickness neck skin graft','Split- thickness neck skin graft','','');
insert into b_icd9 values ('0555000000983','2015002','Full - thickness neck skin graft','Full - thickness neck skin graft','','');
insert into b_icd9 values ('0555000000984','2015004','Homograft to neck skin, Graft to skin of: amnionic membrane, skin from donor','Homograft to neck skin, Graft to skin of: amnionic membrane, skin from donor','','');
insert into b_icd9 values ('0555000000985','2018000','Wound dressing at neck','Wound dressing at neck','','');
insert into b_icd9 values ('0555000000986','2019999','Other procedures and operations on neck skin','การทำหัตถการและการผ่าตัดอื่นๆบริเวณผิวหนังของคอ','','');
insert into b_icd9 values ('0555000000987','2042601','Excision of branchial cleft cyst or vestige','Excision of branchial cleft cyst or vestige','','');
insert into b_icd9 values ('0555000000988','2043900','Closure of branchial cleft fistula','Closure of branchial cleft fistula','','');
insert into b_icd9 values ('0555000000989','2049999','Other procedures and operations on branchial cleft cyst or fistula','Other procedures and operations on branchial cleft cyst or fistula','','');
insert into b_icd9 values ('0555000000990','2053000','Ligation of carotid artery','Ligation of carotid artery','','');
insert into b_icd9 values ('0555000000991','2053098','Other surgical occlusion of carotid artery, Clamping of carotid artery, Occlusion of carotid artery','Other surgical occlusion of carotid artery, Clamping of carotid artery, Occlusion of carotid artery','','');
insert into b_icd9 values ('0555000000992','2053900','Suture of carotid artery','Suture of carotid artery','','');
insert into b_icd9 values ('0555000000993','2059999','Other procedures and operations on carotid artery','Other procedures and operations on carotid artery','','');
insert into b_icd9 values ('0555000000994','2140400','Biopsy of lymphatic structure','Biopsy of lymphatic structure','','');
insert into b_icd9 values ('0555000000995','2140401','Biopsy of cervical lymph node','Biopsy of cervical lymph node','','');
insert into b_icd9 values ('0555000000996','2140408','Other diagnostic procedures on lymphatic structure','Other diagnostic procedures on lymphatic structure','','');
insert into b_icd9 values ('0555000000997','2142100','Incision of lymphatic structure','Incision of lymphatic structure','','');
insert into b_icd9 values ('0555000000998','2142600','Excision of cervical lymph node','Excision of cervical lymph node','','');
insert into b_icd9 values ('0555000000999','2142601','Simple excision of other neck lymphatic structures, Excision of: cystic hygroma, lymphangioma','Simple excision of other neck lymphatic structures, Excision of: cystic hygroma, lymphangioma','','');
insert into b_icd9 values ('0555000001000','2142602','Regional cervical lymph node excision, Extended regional lymph node excision','Regional cervical lymph node excision, Extended regional lymph node excision','','');
insert into b_icd9 values ('0555000001001','2142603','Radical neck dissection, unilateral','Radical neck dissection, unilateral','','');
insert into b_icd9 values ('0555000001002','2142604','Modified radical neck dissection, unilateral','Modified radical neck dissection, unilateral','','');
insert into b_icd9 values ('0555000001003','2142605','Radical neck dissection, bilateral','Radical neck dissection, bilateral','','');
insert into b_icd9 values ('0555000001004','2142606','Modified radical neck dissection, bilateral','Modified radical neck dissection, bilateral','','');
insert into b_icd9 values ('0555000001005','2142609','Radical neck dissection, not otherwise specified','Radical neck dissection, not otherwise specified','','');
insert into b_icd9 values ('0555000001006','2149220','Teleradiotherapy of the cervical nodes tumor','Teleradiotherapy of the cervical nodes tumor','','');
insert into b_icd9 values ('0555000001007','2149999','Other procedures and operations on cervical lymph nodes','Other procedures and operations on cervical lymph nodes','','');
insert into b_icd9 values ('0555000001008','2162210','Incision and drainage of submandibular space','Incision and drainage of submandibular space','','');
insert into b_icd9 values ('0555000001009','2172210','Incision and drainage of other fascial space of head and neck','Incision and drainage of other fascial space of head and neck','','');
insert into b_icd9 values ('0555000001010','2212200','Drainage of thyroglossal tract by incision','Drainage of thyroglossal tract by incision','','');
insert into b_icd9 values ('0555000001011','2212600','Excision of thyroglossal duct or tract','Excision of thyroglossal duct or tract','','');
insert into b_icd9 values ('0555000001012','2219999','Other procedures and operations on thyroglossal remnant','Other procedures and operations on thyroglossal remnant','','');
insert into b_icd9 values ('0555000001013','2250400','Biopsy of lip','การตัดชิ้นเนื้อไปชันสูตรจากริมฝีปาก','','');
insert into b_icd9 values ('0555000001014','2252100','Incision of lip','Incision of lip','','');
insert into b_icd9 values ('0555000001015','2252600','Wide excision of lesion of lip','Wide excision of lesion of lip','','');
insert into b_icd9 values ('0555000001016','2252699','Other excision of lesion or tissue of lip','Other excision of lesion or tissue of lip','','');
insert into b_icd9 values ('0555000001017','2253900','Suture of laceration of lip','Suture of laceration of lip','','');
insert into b_icd9 values ('0555000001018','2253901','Repair of cleft lip, cheiloplasty','Repair of cleft lip, cheiloplasty','','');
insert into b_icd9 values ('0555000001019','2254800','Full-thickness skin graft to lip','Full-thickness skin graft to lip','','');
insert into b_icd9 values ('0555000001020','2254808','Other skin graft to lip','Other skin graft to lip','','');
insert into b_icd9 values ('0555000001021','2254810','Attachment of pedicle or flap graft to lip','Attachment of pedicle or flap graft to lip','','');
insert into b_icd9 values ('0555000001022','2254899','Other plastic repair of lip','Other plastic repair of lip','','');
insert into b_icd9 values ('0555000001023','2259999','Other procedures and operations on lip','การทำหัตถการและการผ่าตัดอื่นๆบริเวณริมฝีปาก','','');
insert into b_icd9 values ('0555000001024','2262100','Labial frenotomy','Labial frenotomy','','');
insert into b_icd9 values ('0555000001025','2262600','Labial frenectomy','Labial frenectomy','','');
insert into b_icd9 values ('0555000001026','2269999','Other procedures and operations onlabial mucosa','Other procedures and operations onlabial mucosa','','');
insert into b_icd9 values ('0555000001027','2292200','Drainage of floor of mouth Include : Incision and drainge, Drainage of Ludwig''s angina','Drainage of floor of mouth Include : Incision and drainge, Drainage of Ludwig''s angina','','');
insert into b_icd9 values ('0555000001028','2292201','Marsupialization of floor of mouth lesion','Marsupialization of floor of mouth lesion','','');
insert into b_icd9 values ('0555000001029','2299999','Other procedures and operations on floor of mouth','Other procedures and operations on floor of mouth','','');
insert into b_icd9 values ('0555000001030','2330400','Biopsy of mouth','การตัดชิ้นเนื้อไปชันสูตรจากบริเวณปาก','','');
insert into b_icd9 values ('0555000001031','2330499','Other diagnostic procedures on oral cavity','Other diagnostic procedures on oral cavity','','');
insert into b_icd9 values ('0555000001032','2331100','Removal of foreign body from mouth without incision','Removal of foreign body from mouth without incision','','');
insert into b_icd9 values ('0555000001033','2332100','Incision of mouth, unspecified structure','Incision of mouth, unspecified structure','','');
insert into b_icd9 values ('0555000001034','2332200','Drainageof mouth ; Include : Incision and drainage','ผ่าระบายของเหลวจากบริเวณปาก','','');
insert into b_icd9 values ('0555000001035','2332600','Debridement of mouth','Debridement of mouth','','');
insert into b_icd9 values ('0555000001036','2332601','Wide excision of lesion of mouth','Wide excision of lesion of mouth','','');
insert into b_icd9 values ('0555000001037','2332699','Other excision of mouth','Other excision of mouth','','');
insert into b_icd9 values ('0555000001038','2333900','Suture of laceration of mouth','Suture of laceration of mouth','','');
insert into b_icd9 values ('0555000001039','2334000','Closure of fistular of mouth, Oro - cutaneous fistula, Oro - nasal fistula','Closure of fistular of mouth, Oro - cutaneous fistula, Oro - nasal fistula','','');
insert into b_icd9 values ('0555000001040','2334800','Full - thickness skin graft to mouth','Full - thickness skin graft to mouth','','');
insert into b_icd9 values ('0555000001041','2334801','Mucosal graft to the mouth','Mucosal graft to the mouth','','');
insert into b_icd9 values ('0555000001042','2334808','Other skin graft to mouth','Other skin graft to mouth','','');
insert into b_icd9 values ('0555000001043','2334810','Attachment to pedical of flap to  mouth','Attachment to pedical of flap to  mouth','','');
insert into b_icd9 values ('0555000001044','2334820','Extension of deepening of buccolabial or lingual sulcus, Vestibuloplasty of mouth','Extension of deepening of buccolabial or lingual sulcus, Vestibuloplasty of mouth','','');
insert into b_icd9 values ('0555000001045','2334899','Other plastic repair of mouth','Other plastic repair of mouth','','');
insert into b_icd9 values ('0555000001046','2338000','Wound dressing at mouth','Wound dressing at mouth','','');
insert into b_icd9 values ('0555000001047','2338300','Irrigation of mouth','Irrigation of mouth','','');
insert into b_icd9 values ('0555000001049','2350400','Closed (Needle)biopsy of tongue','Closed (Needle)biopsy of tongue','','');
insert into b_icd9 values ('0555000001050','2350401','Open biopsy of tongue, Wedge biopsy of tongue','Open biopsy of tongue, Wedge biopsy of tongue','','');
insert into b_icd9 values ('0555000001051','2350499','Other diagnostic procedures on tongue','Other diagnostic procedures on tongue','','');
insert into b_icd9 values ('0555000001052','2352100','Lingual frenulotomy','Lingual frenulotomy','','');
insert into b_icd9 values ('0555000001053','2352199','Other glossotomy','Other glossotomy','','');
insert into b_icd9 values ('0555000001054','2352400','Lysis of adhesions of tongue','Lysis of adhesions of tongue','','');
insert into b_icd9 values ('0555000001055','2352600','Lingual frenectomy, Excision of tongue tie, Excision of ankyloglossia','Lingual frenectomy, Excision of tongue tie, Excision of ankyloglossia','','');
insert into b_icd9 values ('0555000001056','2352601','Excision of lesion or tissue of tongue','Excision of lesion or tissue of tongue','','');
insert into b_icd9 values ('0555000001057','2352602','Partial glossectomy','Partial glossectomy','','');
insert into b_icd9 values ('0555000001058','2352603','Complete glossectomy, Glosstectomy','Complete glossectomy, Glosstectomy','','');
insert into b_icd9 values ('0555000001059','2352604','Radical glossectomy','Radical glossectomy','','');
insert into b_icd9 values ('0555000001060','2352800','Destruction of lesion or tissue of tongue','Destruction of lesion or tissue of tongue','','');
insert into b_icd9 values ('0555000001061','2352801','Radiofrequency surgery of tongue base','Radiofrequency surgery of tongue base','','');
insert into b_icd9 values ('0555000001062','2353900','Suture of laceration of tongue','Suture of laceration of tongue','','');
insert into b_icd9 values ('0555000001063','2354899','Other repair and plastic operations on tongue, Fascial sling of tongue, Fusion of tongue(to lip), Graft of mucosa or skin to tongue','Other repair and plastic operations on tongue, Fascial sling of tongue, Fusion of tongue(to lip), Graft of mucosa or skin to tongue','','');
insert into b_icd9 values ('0555000001064','2359999','Other procedures and operation on tongue','Other procedures and operation on tongue','','');
insert into b_icd9 values ('0555000001065','2360400','Biopsy of gum','Biopsy of gum','','');
insert into b_icd9 values ('0555000001066','2360401','Biopsy of alveoli','Biopsy of alveoli','','');
insert into b_icd9 values ('0555000001067','2360499','Other diagnostic procedures on gums and alveoli','Other diagnostic procedures on gums and alveoli','','');
insert into b_icd9 values ('0555000001068','2362100','Incision of gum or alveolar bone, Apical alveolotomy','Incision of gum or alveolar bone, Apical alveolotomy','','');
insert into b_icd9 values ('0555000001069','2362600','Excision of lesion or tissue of gum','Excision of lesion or tissue of gum','','');
insert into b_icd9 values ('0555000001070','2363900','Suture of laceration of gum','Suture of laceration of gum','','');
insert into b_icd9 values ('0555000001071','2364800','Ginngivoplasty, Gingivoplasty with bone or soft tissue graft','Ginngivoplasty, Gingivoplasty with bone or soft tissue graft','','');
insert into b_icd9 values ('0555000001072','2369999','Other procedures and operation on gum','Other procedures and operation on gum','','');
insert into b_icd9 values ('0555000001073','2372600','Surgical removal of deciduous teeth','Surgical removal of deciduous teeth','','');
insert into b_icd9 values ('0555000001074','2372700','Extraction of deciduous teeth','Extraction of deciduous teeth','','');
insert into b_icd9 values ('0555000001075','2381400','Removal of dental packing','Removal of dental packing','','');
insert into b_icd9 values ('0555000001076','2381401','Removal of dental wiring','Removal of dental wiring','','');
insert into b_icd9 values ('0555000001077','2381402','Extraction of permanent teeth','Extraction of permanent teeth','','');
insert into b_icd9 values ('0555000001078','2381500','Replacement of dental packing','Replacement of dental packing','','');
insert into b_icd9 values ('0555000001079','2381501','Replacement of dental wiring','Replacement of dental wiring','','');
insert into b_icd9 values ('0555000001080','2382300','Exposure of tooth','Exposure of tooth','','');
insert into b_icd9 values ('0555000001081','2382600','Surgical removal of permanent teeth (non - impacted tooth)','Surgical removal of permanent teeth (non - impacted tooth)','','');
insert into b_icd9 values ('0555000001082','2382601','Surgical removal of permanent teeth (soft tissue impaction)','Surgical removal of permanent teeth (soft tissue impaction)','','');
insert into b_icd9 values ('0555000001083','2382602','Surgical removal of permanent teeth (partial bony impaction)','Surgical removal of permanent teeth (partial bony impaction)','','');
insert into b_icd9 values ('0555000001084','2382603','Surgical removal of permanent teeth (complete bony impaction)','Surgical removal of permanent teeth (complete bony impaction)','','');
insert into b_icd9 values ('0555000001085','2382604','Surgical removal of permanent teeth (variation of impaction, embedded tooth)','Surgical removal of permanent teeth (variation of impaction, embedded tooth)','','');
insert into b_icd9 values ('0555000001086','2383700','Dental wiring','Dental wiring','','');
insert into b_icd9 values ('0555000001087','2384900','Reimplantation teeth','Reimplantation teeth','','');
insert into b_icd9 values ('0555000001088','2385000','Tooth transplantation','Tooth transplantation','','');
insert into b_icd9 values ('0555000001089','2392600','Surgical removal of abnormal teeth, Natal and neonatal tooth, Supernumerary tooth, mesiodens, Distomolar, Fourth molar, Paramolar','Surgical removal of abnormal teeth, Natal and neonatal tooth, Supernumerary tooth, mesiodens, Distomolar, Fourth molar, Paramolar','','');
insert into b_icd9 values ('0555000001090','2392700','Extraction of abnormal teeth, Natal and neonatal tooth, Supernumerary tooth, mesiodens, Distomolar, Fourth molar, Paramolar','Extraction of abnormal teeth, Natal and neonatal tooth, Supernumerary tooth, mesiodens, Distomolar, Fourth molar, Paramolar','','');
insert into b_icd9 values ('0555000001091','2400400','Biopsy of bony palate','Biopsy of bony palate','','');
insert into b_icd9 values ('0555000001092','2402100','Incision of palate','Incision of palate','','');
insert into b_icd9 values ('0555000001093','2402600','Local excision of lesion or tissue or bony palate, Local excision or destruction of palate by:cautery, chemotherapy, cryotherapy','Local excision of lesion or tissue or bony palate, Local excision or destruction of palate by:cautery, chemotherapy, cryotherapy','','');
insert into b_icd9 values ('0555000001094','2402601','Wide excision of lesion or tissue of bony palate, En bloc resection of alveolar process and palate','Wide excision of lesion or tissue of bony palate, En bloc resection of alveolar process and palate','','');
insert into b_icd9 values ('0555000001095','2409999','Other procedures and operation on hard palate','Other procedures and operation on hard palate','','');
insert into b_icd9 values ('0555000001096','2410400','Biopsy of soft palate','Biopsy of soft palate','','');
insert into b_icd9 values ('0555000001097','2412100','Incision of soft palate','Incision of soft palate','','');
insert into b_icd9 values ('0555000001098','2412600','Excision of lesion of soft palate','Excision of lesion of soft palate','','');
insert into b_icd9 values ('0555000001099','2412601','Wide excision of lesion of soft palate','Wide excision of lesion of soft palate','','');
insert into b_icd9 values ('0555000001100','2413900','Suture of laceration of palate','Suture of laceration of palate','','');
insert into b_icd9 values ('0555000001101','2414700','Revise of cleft palate repair, Secondary: attachment of pharyngeal flap, lengthening of palate','Revise of cleft palate repair, Secondary: attachment of pharyngeal flap, lengthening of palate','','');
insert into b_icd9 values ('0555000001102','2414800','Correction of cleft palate, Correction of cleft palate by push - back operation','Correction of cleft palate, Correction of cleft palate by push - back operation','','');
insert into b_icd9 values ('0555000001103','2414801','Palatoplasty reconstruction of palate,','Palatoplasty reconstruction of palate,','','');
insert into b_icd9 values ('0555000001104','2414802','Uvulopalatoplasty(UPP)','Uvulopalatoplasty(UPP)','','');
insert into b_icd9 values ('0555000001105','2414803','Uvulopalatopharyngoplasty(UPPP)','Uvulopalatopharyngoplasty(UPPP)','','');
insert into b_icd9 values ('0555000001106','2414804','Laser - assisted uvulopalatoplasty(LAUP)','Laser - assisted uvulopalatoplasty(LAUP)','','');
insert into b_icd9 values ('0555000001107','2414805','Cautery assisteddddd palatat stiffrn operation(CAPSO)','Cautery assisteddddd palatat stiffrn operation(CAPSO)','','');
insert into b_icd9 values ('0555000001108','2414806','Radiofrequency surgery of soft palate','Radiofrequency surgery of soft palate','','');
insert into b_icd9 values ('0555000001109','2414899','Other plastic repair of palate','Other plastic repair of palate','','');
insert into b_icd9 values ('0555000001110','2419999','Other procedures and operations on soft palate','Other procedures and operations on soft palate','','');
insert into b_icd9 values ('0555000001111','2420400','Biopsy of uvula','Biopsy of uvula','','');
insert into b_icd9 values ('0555000001112','2422100','Incision of uvula','Incision of uvula','','');
insert into b_icd9 values ('0555000001113','2422600','Excision of uvula','Excision of uvula','','');
insert into b_icd9 values ('0555000001114','2422601','Excision of uvula using laser','Excision of uvula using laser','','');
insert into b_icd9 values ('0555000001115','2423900','Repair of uvula','Repair of uvula','','');
insert into b_icd9 values ('0555000001116','2429999','Other procedures and operations on uvala','Other procedures and operations on uvala','','');
insert into b_icd9 values ('0555000001117','2330010','Limited oral evaluation - problem focused','การตรวจและการประเมินสภาพช่องปากเฉพาะตำแหน่งหรือที่มีอาการฉุกเฉินรวมถึงการตรวจเพื่อติดตามผล','','');
insert into b_icd9 values ('0555000001118','2330011','Comprehensive oral evaluation','การตรวจและการประเมินสภาพช่องปากทั้งปาก และวางแผนการรักษา','','');
insert into b_icd9 values ('0555000001119','2330012','Detailed and extensive oral evaluation-problem-focused, by report','Detailed and extensive oral evaluation-problem-focused, by report','','');
insert into b_icd9 values ('0555000001120','2330013','Periodic oral evaluation','Periodic oral evaluation','','');
insert into b_icd9 values ('0555000001121','2330014','Re-evaluation-limited, problem focused (Established patient;not post-operative visit)','Re-evaluation-limited, problem focused (Established patient;not post-operative visit)','','');
insert into b_icd9 values ('0555000001122','2330020','Caries susceptibility tests','Caries susceptibility tests','','');
insert into b_icd9 values ('0555000001123','2330030','Dental diagnostic casts','Dental diagnostic casts','','');
insert into b_icd9 values ('0555000001124','2330031','Diagnostic intraoral and extraoral images','Diagnostic intraoral and extraoral images','','');
insert into b_icd9 values ('0555000001125','2330040','Bacteriologic studies for determination of pathologic agents','Bacteriologic studies for determination of pathologic agents','','');
insert into b_icd9 values ('0555000001126','2380010','Dental pulp vitality tests','Dental pulp vitality tests','','');
insert into b_icd9 values ('0555000001127','2330099','Unspecified oral evaluation, by report','Unspecified oral evaluation, by report','','');
insert into b_icd9 values ('0555000001128','2330500','Intraoral occlusal film','Intraoral occlusal film','','');
insert into b_icd9 values ('0555000001129','2370500','Intraoral periapical film for deciduous tooth including periapical and bitewing view','Intraoral periapical film for deciduous tooth including periapical and bitewing view','','');
insert into b_icd9 values ('0555000001130','2380500','Intraoral periapical film for permanent tooth including periapical and bitewing view','Intraoral periapical film for permanent tooth including periapical and bitewing view','','');
insert into b_icd9 values ('0555000001131','2330502','Other intraoral films, by report','Other intraoral films, by report','','');
insert into b_icd9 values ('0555000001132','1050502','Postero-anterior or lateral skull and facial bone survey film','Postero-anterior or lateral skull and facial bone survey film','','');
insert into b_icd9 values ('0555000001133','1610502','Cephalometric film','Cephalometric film','','');
insert into b_icd9 values ('0555000001134','1620692','Temporomandibular joint arthrogram, including injection','Temporomandibular joint arthrogram, including injection','','');
insert into b_icd9 values ('0555000001135','1980502','Panoramic film','Panoramic film','','');
insert into b_icd9 values ('0555000001136','1980506','Tomographic survey','Tomographic survey','','');
insert into b_icd9 values ('0555000001137','1970560','Sialography','Sialography','','');
insert into b_icd9 values ('0555000001138','2337010','การจ่ายยาฟลูออไรด์ชนิดกิน','Oral administration of fluoride tablet for preventive purpose.','','');
insert into b_icd9 values ('0555000001139','2377010','การขูดและขัดฟันในเด็ก','Prophylaxis-child','','');
insert into b_icd9 values ('0555000001140','2377020','การเคลือบฟันด้วยฟลูออไรด์ในเด็ก','Topical application of fluoride-child','','');
insert into b_icd9 values ('0555000001141','2377021','การเคลือบฟันด้วยฟลูออไรด์ในเด็ก(รวมขัดฟัน)','Topical application of fluoride (including prophylaxis)-child','','');
insert into b_icd9 values ('0555000001142','2377030','การเคลือบหลุมร่องฟันน้ำนม','Sealant per tooth, -primary','','');
insert into b_icd9 values ('0555000001143','2377040','การบูรณะฟันน้ำนมด้วยเรซินเพื่อป้องกัน','Preventive resin restoration, -primary','','');
insert into b_icd9 values ('0555000001144','2387010','การขูดและขัดฟันในผู้ใหญ่','Prophylaxis-adult','','');
insert into b_icd9 values ('0555000001145','2387020','การเคลือบฟันด้วยฟลูออไรด์ในผู้ใหญ่','Topical application of fluoride-adult','','');
insert into b_icd9 values ('0555000001146','2387021','การเคลือบฟันด้วยฟลูออไรด์ในผู้ใหญ่(รวมขัดฟัน)','Topical application of fluoride (including prophylaxis)-adult','','');
insert into b_icd9 values ('0555000001147','2387030','การเคลือบหลุมร่องฟันแท้','Sealant per tooth, -permanent','','');
insert into b_icd9 values ('0555000001148','2387040','การบูรณะฟันแท้ด้วยเรซินเพื่อป้องกัน','Preventive resin restoration, -permanent','','');
insert into b_icd9 values ('0555000001149','2337099','Unspecified prophylaxis procedure, by report','Unspecified prophylaxis procedure, by report','','');
insert into b_icd9 values ('0555000001150','2338610','Oral hygiene instructions','Oral hygiene instructions','','');
insert into b_icd9 values ('0555000001151','2338611','Oral hygiene instruction in conjuction with periodontic treatment','Oral hygiene instruction in conjuction with periodontic treatment','','');
insert into b_icd9 values ('0555000001152','2338620','Nutritional counseling for control of dental disease','Nutritional counseling for control of dental disease','','');
insert into b_icd9 values ('0555000001153','2338621','Tobacco counseling for control and prevention of oral disease','Tobacco counseling for control and prevention of oral disease','','');
insert into b_icd9 values ('0555000001154','23771A1','การอุดฟันด้วยอมัลกัม 1 ด้าน (ฟันน้ำนม)','Amalgam  one surface, -primary','','');
insert into b_icd9 values ('0555000001155','23771A2','การอุดฟันด้วยอมัลกัม 2 ด้าน (ฟันน้ำนม)','Amalgam two surfaces, -primary','','');
insert into b_icd9 values ('0555000001156','23771A3','การอุดฟันด้วยอมัลกัม 3 ด้าน (ฟันน้ำนม)','Amalgam three surfaces, -primary','','');
insert into b_icd9 values ('0555000001157','23771A4','การอุดฟันด้วยอมัลกัม 4 ด้านหรือมากกว่า (ฟันน้ำนม)','Amalgam four or more surfaces, -primary','','');
insert into b_icd9 values ('0555000001158','23871A1','การอุดฟันด้วยอมัลกัม 1 ด้าน (ฟันแท้)','Amalgam one surface, -permanent','','');
insert into b_icd9 values ('0555000001159','23871A2','การอุดฟันด้วยอมัลกัม 2 ด้าน (ฟันแท้)','Amalgam two surfaces, -permanent','','');
insert into b_icd9 values ('0555000001160','23871A3','การอุดฟันด้วยอมัลกัม 3 ด้าน (ฟันแท้)','Amalgam three surfaces, -permanent','','');
insert into b_icd9 values ('0555000001161','23871A4','การอุดฟันด้วยอมัลกัม 4 ด้านหรือมากกว่า (ฟันแท้)','Amalgam four or more surfaces, -permanent','','');
insert into b_icd9 values ('0555000001162','23771B1','การอุดฟันด้วยเรซินคอมโพสิต/กลาสไอโอโนเมอร์  1 ด้าน (ฟันน้ำนม-ฟันหน้า)','Resin-based composite one surface, anterior-primary','','');
insert into b_icd9 values ('0555000001163','23771B2','การอุดฟันด้วยเรซินคอมโพสิต/กลาสไอโอโนเมอร์  2 ด้าน (ฟันน้ำนม-ฟันหน้า)','Resin-based composite two surfaces, anterior-primary','','');
insert into b_icd9 values ('0555000001164','23771B3','การอุดฟันด้วยเรซินคอมโพสิต/กลาสไอโอโนเมอร์  3 ด้าน (ฟันน้ำนม-ฟันหน้า)','Resin-based composite three surfaces, anterior-primary','','');
insert into b_icd9 values ('0555000001165','23771B4','การอุดฟันด้วยเรซินคอมโพสิต/กลาสไอโอโนเมอร์  4 ด้านหรือมากกว่า หรือถึงมุมปลายฟันหน้า (ฟันน้ำนม-ฟันหน้า)','Resin-based composite four or more surfaces or involving incisal angle, anterior-primany','','');
insert into b_icd9 values ('0555000001166','23771B5','การบูรณะฟันด้วยครอบฟันชนิดเรซินคอมโพสิต (ฟันน้ำนม-ฟันหน้า)','Resin-based composite crown, anterior-primary','','');
insert into b_icd9 values ('0555000001167','23771C1','การอุดฟันด้วยเรซินคอมโพสิต/กลาสไอโอโนเมอร์  1 ด้าน (ฟันน้ำนม-ฟันหลัง)','Resin-based composite - one surfaces, posterior-primary','','');
insert into b_icd9 values ('0555000001168','23771C2','การอุดฟันด้วยเรซินคอมโพสิต/กลาสไอโอโนเมอร์  2 ด้าน (ฟันน้ำนม-ฟันหลัง)','Resin-based composite -two surfaces, posterior-primary','','');
insert into b_icd9 values ('0555000001169','23771C3','การอุดฟันด้วยเรซินคอมโพสิต/กลาสไอโอโนเมอร์  3 ด้านหรือมากกว่า (ฟันน้ำนม-ฟันหลัง)','Resin-based composite -  three or more surface, posterior-primary','','');
insert into b_icd9 values ('0555000001170','23771C4','Resin-based composite four or more surfaces or involving incisal angle, posterior-primany','Resin-based composite four or more surfaces or involving incisal angle, posterior-primany','','');
insert into b_icd9 values ('0555000001171','23871B1','การอุดฟันด้วยเรซินคอมโพสิต/กลาสไอโอโนเมอร์  1 ด้าน (ฟันแท้-ฟันหน้า)','Resin-based composite one surface, anterior-permanent','','');
insert into b_icd9 values ('0555000001172','23871B2','การอุดฟันด้วยเรซินคอมโพสิต/กลาสไอโอโนเมอร์  2 ด้าน (ฟันแท้-ฟันหน้า)','Resin-based composite two surfaces, anterior-permanent','','');
insert into b_icd9 values ('0555000001173','23871B3','การอุดฟันด้วยเรซินคอมโพสิต/กลาสไอโอโนเมอร์  3 ด้าน (ฟันแท้-ฟันหน้า)','Resin-based composite  three surfaces, anterior-permanent','','');
insert into b_icd9 values ('0555000001174','23871B4','การอุดฟันด้วยเรซินคอมโพสิต/กลาสไอโอโนเมอร์  4 ด้านหรือมากกว่า หรือถึงมุมปลายฟันหน้า (ฟันแท้-ฟันหน้า)','Resin-based composite  four or more surfaces or involving incisal angle, anterior-permanent','','');
insert into b_icd9 values ('0555000001175','23871B5','การบูรณะฟันด้วยครอบฟันชนิดเรซินคอมโพสิต (ฟันแท้-ฟันหน้า)','Resin-based composite crown, anterior-permanent','','');
insert into b_icd9 values ('0555000001176','23871C1','การอุดฟันด้วยเรซินคอมโพสิต/กลาสไอโอโนเมอร์  1 ด้าน (ฟันแท้-ฟันหลัง)','Resin-based composite - one surfaces, posterior-permanent','','');
insert into b_icd9 values ('0555000001177','23871C2','การอุดฟันด้วยเรซินคอมโพสิต/กลาสไอโอโนเมอร์  2 ด้าน (ฟันแท้-ฟันหลัง)','Resin-based composite - two surfaces, posterior-permanent','','');
insert into b_icd9 values ('0555000001178','23871C3','การอุดฟันด้วยเรซินคอมโพสิต/กลาสไอโอโนเมอร์  3 ด้าน (ฟันแท้-ฟันหลัง)','Resin-based composite - three bsurfaces, posterior-permanent','','');
insert into b_icd9 values ('0555000001179','23871C4','การอุดฟันด้วยเรซินคอมโพสิต/กลาสไอโอโนเมอร์  4 ด้านหรือมากกว่า (ฟันแท้-ฟันหลัง)','Resin-based composite four or more surfaces , posterior-permanent','','');
insert into b_icd9 values ('0555000001180','2387188','การปักหมุดเพื่อเพิ่มการยึดอยู่ของวัสดุอุดฟัน (ต่อซี่)','Pin retention - per tooth, in addition to restoration','','');
insert into b_icd9 values ('0555000001181','23871D1','Gold foil - one surface','Gold foil - one surface','','');
insert into b_icd9 values ('0555000001182','23871D2','Gold foil - two surfaces','Gold foil - two surfaces','','');
insert into b_icd9 values ('0555000001183','23871D3','Gold foil - three or more surfaces','Gold foil - three or more surfaces','','');
insert into b_icd9 values ('0555000001184','23871E1','Inlay metallic one surface','Inlay metallic one surface','','');
insert into b_icd9 values ('0555000001185','23871E2','Inlay metallic two surfaces','Inlay metallic two surfaces','','');
insert into b_icd9 values ('0555000001186','23871E3','Inlay metallic three or more surfaces','Inlay metallic three or more surfaces','','');
insert into b_icd9 values ('0555000001187','23871F1','Inlay porcelain/ceramic one surface','Inlay porcelain/ceramic one surface','','');
insert into b_icd9 values ('0555000001188','23871F2','Inlay porcelain/ceramic two surfaces','Inlay porcelain/ceramic two surfaces','','');
insert into b_icd9 values ('0555000001189','23871F3','Inlay porcelain/ceramic three or more surfaces','Inlay porcelain/ceramic three or more surfaces','','');
insert into b_icd9 values ('0555000001190','23871G1','Inlay resin based composite one surface','Inlay resin based composite one surface','','');
insert into b_icd9 values ('0555000001191','23871G2','Inlay resin based composite two surface','Inlay resin based composite two surface','','');
insert into b_icd9 values ('0555000001192','23871G3','Inlay resin based composite three or more surfaces','Inlay resin based composite three or more surfaces','','');
insert into b_icd9 values ('0555000001193','23871H2','Onlay metallic two surfaces','Onlay metallic two surfaces','','');
insert into b_icd9 values ('0555000001194','23871H3','Onlay metallic three surfaces','Onlay metallic three surfaces','','');
insert into b_icd9 values ('0555000001195','23871H4','Onlay metallic four or more surfaces','Onlay metallic four or more surfaces','','');
insert into b_icd9 values ('0555000001196','23871J2','Onlay porcelain/ceramic two surfaces','Onlay porcelain/ceramic two surfaces','','');
insert into b_icd9 values ('0555000001197','23871J3','Onlay porcelain/ceramic three surfaces','Onlay porcelain/ceramic three surfaces','','');
insert into b_icd9 values ('0555000001198','23871J4','Onlay porcelain/ceramic four or more surfaces','Onlay porcelain/ceramic four or more surfaces','','');
insert into b_icd9 values ('0555000001199','23871K2','Onlay resin based composite two surfaces','Onlay resin based composite two surfaces','','');
insert into b_icd9 values ('0555000001200','23871K3','Onlay resin based composite three surfaces','Onlay resin based composite three surfaces','','');
insert into b_icd9 values ('0555000001201','23871K4','Onlay resin based composite four or more surfaces','Onlay resin based composite four or more surfaces','','');
insert into b_icd9 values ('0555000001202','23771L1','การบูรณะฟันด้วยครอบฟันสำเร็จรูปเหล็กไร้สนิม (ฟันน้ำนม)','Prefabricated stainless steel crown, -primary','','');
insert into b_icd9 values ('0555000001203','23871L1','การบูรณะฟันด้วยครอบฟันสำเร็จรูปเหล็กไร้สนิม (ฟันแท้)','Prefabricated stainless steel crown, -primanent','','');
insert into b_icd9 values ('0555000001204','23871L2','การบูรณะฟันด้วยครอบฟันสำเร็จรูปเรซิน','Prefabricated resin crown','','');
insert into b_icd9 values ('0555000001205','23871L3','การบูรณะฟันด้วยครอบฟันสำเร็จรูปเหล็กไร้สนิม ที่มีช่องบรรจุเรซิน','Prefabricated stainless steel crown with resin window','','');
insert into b_icd9 values ('0555000001206','23871M1','การบูรณะฟันด้วยครอบฟันเรซินแบบโลหะมีสกุลอัตราส่วนสูง','Crown resin, laboratory','','');
insert into b_icd9 values ('0555000001207','23871M2','การบูรณะฟันด้วยครอบฟันเรซินแบบโลหะไร้สกุล','Crown resin with high noble metal','','');
insert into b_icd9 values ('0555000001208','23871M3','การบูรณะฟันด้วยครอบฟันเรซินแบบโลหะมีสกุล','Crown resin with predominantly base metal','','');
insert into b_icd9 values ('0555000001209','23871M4','การบูรณะฟันด้วยครอบฟันเรซิน ที่ทำเฉพาะผู้ป่วยแต่ละราย','Crown resin with noble metal','','');
insert into b_icd9 values ('0555000001210','23871N1','การบูรณะฟันด้วยครอบฟันโลหะเคลือบกระเบื้องชนิดโลหะมีสกุลอัตราส่วนสูง','Crown porcelain/ceramic substrate','','');
insert into b_icd9 values ('0555000001211','23871N2','การบูรณะฟันด้วยครอบฟันโลหะเคลือบกระเบื้องชนิดโลหะไร้สกุล','Crown porcelain fused to high noble metal','','');
insert into b_icd9 values ('0555000001212','23871N3','การบูรณะฟันด้วยครอบฟันโลหะเคลือบกระเบื้องชนิดโลหะมีสกุล','Crown porcelain fused to predominantly base metal','','');
insert into b_icd9 values ('0555000001213','23871N4','การบูรณะฟันด้วยครอบฟันกระเบื้อง','Crown porcelain fused to noble metal','','');
insert into b_icd9 values ('0555000001214','23871P1','การบูรณะฟันด้วยครอบฟันโลหะ ? ชนิดโลหะมีสกุลอัตราส่วนสูง','Crown 3/4 cast high noble metal','','');
insert into b_icd9 values ('0555000001215','23871P2','การบูรณะฟันด้วยครอบฟันโลหะ ? ชนิดโลหะไร้สกุล','Crown 3/4 cast predominantly base metal','','');
insert into b_icd9 values ('0555000001216','23871P3','การบูรณะฟันด้วยครอบฟันโลหะ ? ชนิดโลหะมีสกุล','Crown 3/4 cast noble metal','','');
insert into b_icd9 values ('0555000001217','23871P4','การบูรณะฟันด้วยครอบฟันกระเบื้อง ?','Crown 3/4 porcelain/ceramic','','');
insert into b_icd9 values ('0555000001218','23871P5','การบูรณะฟันด้วยครอบฟันโลหะมีสกุลอัตราส่วนสูง','Crown full cast high noble metal','','');
insert into b_icd9 values ('0555000001219','23871P6','การบูรณะฟันด้วยครอบฟันโลหะไร้สกุล','Crown full cast predominantly base metal','','');
insert into b_icd9 values ('0555000001220','23871P7','การบูรณะฟันด้วยครอบฟันโลหะมีสกุล','Crown full cast noble metal','','');
insert into b_icd9 values ('0555000001221','23871Q1','การบูรณะฟันด้วยวีเนียร์ชนิดเรซิน ลามิเนต (ทำในคลินิก)','Labial veneer (resin laminate) - chairside','','');
insert into b_icd9 values ('0555000001222','23871Q2','การบูรณะฟันด้วยวีเนียร์ชนิดเรซิน ลามิเนต (ทำจากห้องปฏิบัติการ)','Labial veneer (resin laminate) - laboratory','','');
insert into b_icd9 values ('0555000001223','23871Q3','การบูรณะฟันด้วยวีเนียร์ชนิดกระเบื้อง ลามิเนต (ทำจากห้องปฏิบัติการ)','Labial veneer (porcelain laminate)-laboratory','','');
insert into b_icd9 values ('0555000001224','23871Q4','การบูรณะฟันด้วยครอบฟันชั่วคราว','Provisional crown','','');
insert into b_icd9 values ('0555000001225','23871R1','การรื้อครอบฟัน','Crown removal','','');
insert into b_icd9 values ('0555000001226','23871R2','การรื้อเดือยฟัน','Post removal (not in conjunction with endodontic therapy)','','');
insert into b_icd9 values ('0555000001227','23871R3','การยึดชิ้นอุดฝังที่หลุดกลับเข้าที่เดิม','Recement inlay','','');
insert into b_icd9 values ('0555000001228','23871R4','การยึดครอบฟันที่หลุดกลับเข้าที่เดิม','Recement crown','','');
insert into b_icd9 values ('0555000001229','23871R5','การซ่อมครอบฟัน (อธิบายวิธีการด้วย)','Crown repair, by report','','');
insert into b_icd9 values ('0555000001230','23871S1','การเตรียมคลองรากฟันสำหรับเดือยฟันสำเร็จรูป ไม่ควรลงรหัสนี้พร้อมกับรหัส 238-71-S3 และ 238-71-S4','Canal preparation and fitting of preformed dowel or post','','');
insert into b_icd9 values ('0555000001231','23871S2','การก่อแกน(core build up) รวมถึงการปักหมุด','Core buildup, including any pins','','');
insert into b_icd9 values ('0555000001232','23871S3','การบูรณะฟันด้วยเดือยฟันชนิดเหวี่ยง','Cast post and core in addition to crown.','','');
insert into b_icd9 values ('0555000001233','23871S4','การบูรณะฟันด้วยเดือยฟันสำเร็จรูป รวมกับการก่อแกนฟัน','Prefabricated post and core in addition to crown','','');
insert into b_icd9 values ('0555000001234','23871S5','การกรอแต่งผิวเคลือบฟัน 1-2 ซี่รวมถึงการกำจัดส่วนฟันที่ยื่น','Odontoplasty 1-2 teeth; includes removal of enamel projections','','');
insert into b_icd9 values ('0555000001235','23871S6','การขัดผิวเคลือบฟัน(enamel abrasion)','Enamel microabrasion','','');
insert into b_icd9 values ('0555000001236','23871S8','การกรอแต่งฟันและขั้นตอนอื่นๆ ในการบูรณะฟันด้วยชิ้นอุดฝัง/ชิ้นอุดครอบ เดือยฟัน ครอบฟัน และ วีเนียร์','Tooth preparation and other steps in the process of inlay/onlay, post and core, crown,and labial veneer','','');
insert into b_icd9 values ('0555000001237','23871T1','การฟอกสีฟันที่มีชีวิต (ทำในคลินิก)','Bleaching of discolored tooth, vital, chair side','','');
insert into b_icd9 values ('0555000001238','23871T2','การฟอกสีฟันที่มีชีวิต (ทำที่บ้าน)','Bleaching of discolored tooth, vital, home','','');
insert into b_icd9 values ('0555000001239','23871T3','การฟอกสีฟันที่ไม่มีชีวิต','Bleaching of discolored tooth, non vital','','');
insert into b_icd9 values ('0555000001240','23871U1','การอุดฟันด้วยวัสดุระงับปวด','Sedative filling','','');
insert into b_icd9 values ('0555000001241','23871U2','การใส่ครอบฟัน/แถบรัดชั่วคราว (กรณีฟันหัก)','Temporary crown/band (fractured tooth)','','');
insert into b_icd9 values ('0555000001242','2387199','การบูรณะฟันด้วยวิธีการอื่นๆ (อธิบายวิธีการด้วย)','Unspecified restorative procedure, by report','','');
insert into b_icd9 values ('0555000001243','2377210','การตัดโพรงประสาทฟันออกทั้งหมด (ฟันน้ำนม) (ไม่รวมการบูรณะฟัน)','Therapeutic pulpotomy, -primary','','');
insert into b_icd9 values ('0555000001244','2377211','การกำจัดโพรงประสาทฟันออกอย่างคร่าวๆ (ฟันน้ำนม)','Gross pulpal debridement, -primary','','');
insert into b_icd9 values ('0555000001245','2387210','การทำ direct pulp capping (ฟันแท้)','Pulp cap, direct, -permanent','','');
insert into b_icd9 values ('0555000001246','2387211','การทำ indirect pulp capping (ฟันแท้)','Pulp cap, indirect, -permanent','','');
insert into b_icd9 values ('0555000001247','2387212','การตัดโพรงประสาทฟันออกบางส่วน (ฟันแท้) (ไม่รวมการบูรณะฟัน)','Therapeutic partial pulpotomy, -permanent','','');
insert into b_icd9 values ('0555000001248','2387213','การตัดโพรงประสาทฟันออกทั้งหมด (ฟันแท้) (ไม่รวมการบูรณะฟัน)','Therapeutic pulpotomy, -permanent','','');
insert into b_icd9 values ('0555000001249','2387214','การกำจัดโพรงประสาทฟันออกอย่างคร่าวๆ (ฟันแท้)','Gross pulpal debridement, -permanent','','');
insert into b_icd9 values ('0555000001250','2377220',' (ไม่รวมการบูรณะฟัน)','Pulpal therapy (resorbable filling) , anterior-primary, first and other visits, primary incisors and cuspids','','');
insert into b_icd9 values ('0555000001251','2377221',' (ไม่รวมการบูรณะฟัน)','Pulpal therapy (resorbable filling) , anterior-primary, root filled visits,','','');
insert into b_icd9 values ('0555000001252','2377222','(ไม่รวมการบูรณะฟัน)','Pulpal therapy (resorbable filling), posterior-primary, first and other visits restoration primary first and second molars.','','');
insert into b_icd9 values ('0555000001253','2377223',' (ไม่รวมการบูรณะฟัน)','Pulpal therapy (resorbable filling) , posterior-primary, root filled visits,','','');
insert into b_icd9 values ('0555000001254','2387220','การเปิด/ขยาย/ล้าง/ใส่ยาในคลองรากฟันหน้า (ไม่รวมการบูรณะฟัน)','Endodontic therapy on permanent teeth, anterior','','');
insert into b_icd9 values ('0555000001255','2387221','การเปิด/ขยาย/ล้าง/ใส่ยาในคลองรากฟันกรามน้อย(ไม่รวมการบูรณะฟัน)','Endodontic therapy on permanent teeth, bicuspid','','');
insert into b_icd9 values ('0555000001256','2387222','การเปิด/ขยาย/ล้าง/ใส่ยาในคลองรากฟันกราม(ไม่รวมการบูรณะฟัน)','Endodontic therapy on permanent teeth, molar','','');
insert into b_icd9 values ('0555000001257','2387223','การอุดคลองรากฟันหน้า (ไม่รวมการบูรณะฟัน)','Endodontic therapy on permanent teeth, root filled visit,- anterior','','');
insert into b_icd9 values ('0555000001258','2387224','การอุดคลองรากฟันกรามน้อย(ไม่รวมการบูรณะฟัน)','Endodontic therapy on permanent teeth, root filled visit,- bicuspid','','');
insert into b_icd9 values ('0555000001259','2387225','การอุดฟันคลองรากฟันกราม(ไม่รวมการบูรณะฟัน)','Endodontic therapy on permanent teeth, root filled visit,- molar','','');
insert into b_icd9 values ('0555000001260','2387226','การเปิด ขยาย ล้าง และอุดคลองรากฟันหน้า(ไม่รวมการบูรณะฟัน)','Endodontic therapy on  permanent teeth, anterior, one visit','','');
insert into b_icd9 values ('0555000001261','2387227','การเปิด ขยาย ล้าง และอุดคลองรากฟันกรามน้อย(ไม่รวมการบูรณะฟัน)','Endodontic therapy on  permanent teeth, bicuspid, one visit','','');
insert into b_icd9 values ('0555000001262','2387228','การเปิด ขยาย ล้าง และอุดคลองรากฟันกราม(ไม่รวมการบูรณะฟัน)','Endodontic therapy on  permanenty teeth, molar, one visit','','');
insert into b_icd9 values ('0555000001263','2387230','การรักษากรณีคลองรากฟันอุดตันโดยวิธีไม่ผ่าตัด','Treatment of root canal obstruction;non - surgical access','','');
insert into b_icd9 values ('0555000001264','2387231','การรักษาคลองรากฟันไม่สมบูรณ์ กรณีฟันที่ไม่สามารถบูรณะได้ หรือฟันที่แตกหัก','Incomplete endodontic therapy;inoperable or fractured tooth','','');
insert into b_icd9 values ('0555000001265','2387232','การซ่อมแซมรูทะลุจากการรักษาคลองรากฟันผิดวิธี','Internal root repair of perforation defects','','');
insert into b_icd9 values ('0555000001266','2387240','การเปิด/ขยาย/ล้าง/ใส่ยาในคลองรากฟันหน้า retreatment ฟันหน้า','Endodontic retreatment of previous root canal therapy, -anterior','','');
insert into b_icd9 values ('0555000001267','2387241','การเปิด/ขยาย/ล้าง/ใส่ยาในคลองรากฟัน retreatment ฟันกรามน้อย','Endodontic retreatment of previous root canal therapy, -bicuspid','','');
insert into b_icd9 values ('0555000001268','2387242','การเปิด/ขยาย/ล้างใส่ยาในคลองรากฟัน retreatment ฟันกราม','Endodontic retreatment of previous root canal therapy, -molar','','');
insert into b_icd9 values ('0555000001269','2387243','การอุดคลองรากฟัน retreatment ในฟันหน้า','Endodontic retreatment, root filled visit,- anterior','','');
insert into b_icd9 values ('0555000001270','2387244','การอุดคลองรากฟัน retreatment ในฟันกรามน้อย','Endodontic retreatment, root filled visit,- bicuspid','','');
insert into b_icd9 values ('0555000001271','2387245','การอุดคลองรากฟัน retreatment ในฟันกราม','Endodontic retreatment, root filled visit,- molar','','');
insert into b_icd9 values ('0555000001272','2387250','กระบวนการเหนี่ยวนำให้ปลายรากปิด ครั้งแรก','Apexification/recalcification-initial visit','','');
insert into b_icd9 values ('0555000001273','2387251','กระบวนการเหนี่ยวนำให้ปลายรากปิด ระหว่างการรักษา','Apexification/recalcification-interim medication replacement','','');
insert into b_icd9 values ('0555000001274','2387252','กระบวนการเหนี่ยวนำให้ปลายรากปิด ครั้งสุดท้าย','Apexification/recalcification-final visit','','');
insert into b_icd9 values ('0555000001275','2387260','การตัดปลายรากฟัน ในฟันหน้า 1 ซี่','Apicoectomy/periradicular surgery, 1 tooth, -anterior','','');
insert into b_icd9 values ('0555000001276','2387261','การตัดปลายรากฟันในฟันหน้า 2 ซี่','Apicoectomy/periradicular surgery, 2 teeth, -anterior','','');
insert into b_icd9 values ('0555000001277','2387262','การตัดปลายรากฟันในฟันหน้า 3 ซี่','Apicoectomy/periradicular surgery, 3 teeth, -anterior','','');
insert into b_icd9 values ('0555000001278','2387263','การตัดปลายรากฟันในฟันหน้า 4 ซี่ หรือมากกว่า','Apicoectomy/periradicular surgery, 4 or more teeth, -anterior','','');
insert into b_icd9 values ('0555000001279','2387264','การตัดปลายรากฟัน ในฟันกรามน้อย 1 ราก','Apicoectomy/periradicular surgery, 1 root, -bicuspid','','');
insert into b_icd9 values ('0555000001280','2387265','การตัดปลายรากฟัน ในฟันกรามน้อย 2 ราก','Apicoectomy/periradicular surgery, 2 root, -bicuspid','','');
insert into b_icd9 values ('0555000001281','2387266','การตัดปลายรากฟัน ในฟันกราม 1 ราก','Apicoectomy/periradicular surgery, 1 root , -molar','','');
insert into b_icd9 values ('0555000001282','2387267','การตัดปลายรากฟัน ในฟันกราม 2 ราก','Apicoectomy/periradicular surgery, 2 root , -molar','','');
insert into b_icd9 values ('0555000001283','2387268','การตัดปลายรากฟัน ในฟันกราม 3 ราก','Apicoectomy/periradicular surgery, 3 root , -molar','','');
insert into b_icd9 values ('0555000001284','2387270','การอุดย้อนปลายรากฟัน  1 ราก','Retrograde filling - 1 roots','','');
insert into b_icd9 values ('0555000001285','2387271','การอุดย้อนปลายรากฟัน 2 ราก','Retrograde filling - 2 roots','','');
insert into b_icd9 values ('0555000001286','2387272','การอุดย้อนปลายรากฟัน  3 ราก','Retrograde filling - 3 roots','','');
insert into b_icd9 values ('0555000001287','2387273','การอุดย้อนปลายรากฟัน  4 ราก','Retrograde filling - 4 roots','','');
insert into b_icd9 values ('0555000001288','2387280','การตัดรากฟันออก  1  ราก','Root amputation, 1 root','','');
insert into b_icd9 values ('0555000001289','2387281','การตัดรากฟันออก  1  ราก โดยไม่เปิดแผ่นเหงือก','Root amputation, 1 root, without mucoperiosteal flap','','');
insert into b_icd9 values ('0555000001290','2387282','การตัดรากฟันออก  2  ราก','Root amputation, 2 root','','');
insert into b_icd9 values ('0555000001291','2387283','การตัดรากฟันออก  2  ราก โดยไม่เปิดแผ่นเหงือก','Root amputation, 2 root, without mucoperiosteal flap','','');
insert into b_icd9 values ('0555000001292','2387284','การตัดแบ่งรากฟัน','Hemisection (including any root removal), not including root canal therapy','','');
insert into b_icd9 values ('0555000001293','2387285','การปลูกฟันซ้ำ (ร่วมกับการใส่เฝือกฟัน)','Intentional reimplantation (including necessary splinting)','','');
insert into b_icd9 values ('0555000001294','2387290','การแยกฟันโดยใส่แผ่นยางกันน้ำลาย','Surgical procedure for isolation of tooth with rubber dam','','');
insert into b_icd9 values ('0555000001295','2387299','การรักษาอื่นๆทางวิทยาเอ็นโดดอนท์ (อธิบายวิธีการด้วย)','Unspecified endodontic precedure, by report','','');
insert into b_icd9 values ('0555000001296','2277310','การขูดหินน้ำลาย(เฉพาะฟันบน)','Periodontal  debridement, -upper arch','','');
insert into b_icd9 values ('0555000001297','2277320','การขูดหินน้ำลายและประเมินสภาพปริทันต์เป็นระยะ ภายหลังการรักษาโรคปริทันต์อักเสบ(โดยทั่วไป จะรักษาทุก 3 เดือน) (เฉพาะฟันบน)','Periodontal maintenance procedures, following active therapy, -upper arch','','');
insert into b_icd9 values ('0555000001298','2287310','การขูดหินน้ำลาย(เฉพาะฟันล่าง)','Periodontal  debridement, -lower  arch','','');
insert into b_icd9 values ('0555000001299','2287320','การขูดหินน้ำลายและประเมินสภาพปริทันต์เป็นระยะ ภายหลังการรักษาโรคปริทันต์อักเสบ(โดยทั่วไป จะรักษาทุก 3 เดือน) (เฉพาะฟันล่าง)','Periodontal maintenance procedures, following active therapy, -lower arch','','');
insert into b_icd9 values ('0555000001300','2367310','Debridement to enable comprehensive periodontal evaluation and diagnosis, -full arch','Debridement to enable comprehensive periodontal evaluation and diagnosis, -full arch','','');
insert into b_icd9 values ('0555000001301','2367320','Periodontal scaling and root planing, per quadrant','Periodontal scaling and root planing, per quadrant','','');
insert into b_icd9 values ('0555000001302','2367330','Periodontal maintenance precedures, following active therapy, -full  mouth','Periodontal maintenance precedures, following active therapy, -full  mouth','','');
insert into b_icd9 values ('0555000001303','2377340','Localized delivery of chemotherapeutic agents via a controlled release  vehicle into diseased crevicular tissue, per tooth, by report','Localized delivery of chemotherapeutic agents via a controlled release  vehicle into diseased crevicular tissue, per tooth, by report','','');
insert into b_icd9 values ('0555000001304','2387310','Provisional splinting - intracoronal','Provisional splinting - intracoronal','','');
insert into b_icd9 values ('0555000001305','2387311','Provisional splinting - extracoronal','Provisional splinting - extracoronal','','');
insert into b_icd9 values ('0555000001306','2337310','Clinical crown lengthening - hard tissue','Clinical crown lengthening - hard tissue','','');
insert into b_icd9 values ('0555000001307','2337311','Osseous surgery (including flap entry and closure) - per quadrant','Osseous surgery (including flap entry and closure) - per quadrant','','');
insert into b_icd9 values ('0555000001308','2337312','Bone replacement graft - first site in quadrant','Bone replacement graft - first site in quadrant','','');
insert into b_icd9 values ('0555000001309','2337313','Bone replacement graft - each additional site in quadrant','Bone replacement graft - each additional site in quadrant','','');
insert into b_icd9 values ('0555000001310','2367350','Gingivectomy or gingivoplasty-per tooth','Gingivectomy or gingivoplasty-per tooth','','');
insert into b_icd9 values ('0555000001311','2367351','Gingivectomy or gingivoplasty-per quadrant','Gingivectomy or gingivoplasty-per quadrant','','');
insert into b_icd9 values ('0555000001312','2367352','Gingival curettage, surgical - per quadrant, by report','Gingival curettage, surgical - per quadrant, by report','','');
insert into b_icd9 values ('0555000001313','2367353','Gingival flap procedure, including root planing - per quadrant','Gingival flap procedure, including root planing - per quadrant','','');
insert into b_icd9 values ('0555000001314','2367354','Surgical revision procedure, per tooth','Surgical revision procedure, per tooth','','');
insert into b_icd9 values ('0555000001315','2367355','Distal or proximal wedge precedure (when not performed in conjunction with surgical  precedres in the same anatomical area)','Distal or proximal wedge precedure (when not performed in conjunction with surgical  precedres in the same anatomical area)','','');
insert into b_icd9 values ('0555000001316','2367360','Apically positioned flap','Apically positioned flap','','');
insert into b_icd9 values ('0555000001317','2367361','Coronal positioned flap','Coronal positioned flap','','');
insert into b_icd9 values ('0555000001318','2367362','Semilunar flap','Semilunar flap','','');
insert into b_icd9 values ('0555000001319','2367370','Pedicle soft tissue graft procedure','Pedicle soft tissue graft procedure','','');
insert into b_icd9 values ('0555000001320','2367371','Free  soft tissue graft procedure (including donor site surgery)','Free  soft tissue graft procedure (including donor site surgery)','','');
insert into b_icd9 values ('0555000001321','2367372','Subepithelial connective tissue graft procedure (including donor site surgery)','Subepithelial connective tissue graft procedure (including donor site surgery)','','');
insert into b_icd9 values ('0555000001322','2367380','Guided tissue regeneration-resorbable barrier, per site','Guided tissue regeneration-resorbable barrier, per site','','');
insert into b_icd9 values ('0555000001323','2367381','Guided tissue regeneration-nonresorbable barrier, per site (includes membrane removal)','Guided tissue regeneration-nonresorbable barrier, per site (includes membrane removal)','','');
insert into b_icd9 values ('0555000001324','2367388','Periodontal surgery : post - operative procedures','Periodontal surgery : post - operative procedures','','');
insert into b_icd9 values ('0555000001325','2367399','Unspecified periodontal procedure, by report','Unspecified periodontal procedure, by report','','');
insert into b_icd9 values ('0555000001326','2277410','การใส่ฟันปลอมเต็มส่วน –ขากรรไกรบน','Complete denture -upper','','');
insert into b_icd9 values ('0555000001327','2277411','การใส่ฟันปลอมเต็มส่วนชั่วคราวใส่ทันที –ขากรรไกรบน','Immediate complete denture -upper','','');
insert into b_icd9 values ('0555000001328','2277412','การใส่ฟันปลอมเฉพาะกาลเต็มส่วน –ขากรรไกรบน','Interim complete denture -upper','','');
insert into b_icd9 values ('0555000001329','2277413','การใส่ฟันปลอมเต็มส่วนทับราก –ขากรรไกรบน','Complete overdenture -upper','','');
insert into b_icd9 values ('0555000001330','2277420','การใส่ฟันปลอมบางส่วนถอดได้ฐานอคริลิก –ขากรรไกรบน','Acrylic partial denture, including any conventional clasps, rests and teeth -upper','','');
insert into b_icd9 values ('0555000001331','2277421','การใส่ฟันปลอมบางส่วนถอดได้ชนิดโครงโลหะ –ขากรรไกรบน','Metallic partial denture, including any conventional clasps, rests and teeth -upper','','');
insert into b_icd9 values ('0555000001332','2277422','การใส่ฟันปลอมบางส่วนถอดได้ชนิดมีสลัก –ขากรรไกรบน','Metallic partial denture with precision attachment -upper','','');
insert into b_icd9 values ('0555000001333','2277430','การปรับแต่งฟันปลอมเต็มส่วน –ขากรรไกรบน','Adjust complete denture -upper','','');
insert into b_icd9 values ('0555000001334','2277431','การปรับแต่งฟันปลอมบางส่วนถอดได้ –ขากรรไกรบน','Adjust partial denture-acrylic and metalic -upper','','');
insert into b_icd9 values ('0555000001335','2277432','การปรับฟันปลอมถอดได้หลังการทำศัลยกรรมรากเทียม –ขากรรไกรบน','Modification of removable prosthesis following implant surgery -upper','','');
insert into b_icd9 values ('0555000001336','2277440','การซ่อมฐานฟันปลอมทั้งปากบานที่แตกหัก','Repair broken complete denture base','','');
insert into b_icd9 values ('0555000001337','2277441','การซ่อมฐานฟันปลอมอคริลิก ของฟันปลอมบางส่วนถอดได้ชนิดโครงโลหะ –ขากรรไกรบน','Repair resin denture base -upper','','');
insert into b_icd9 values ('0555000001338','2277442','การซ่อมส่วนโครงโลหะของฟันปลอมบางส่วนถอดได้ชนิดโครงโลหะ  -ขากรรไกรบน','Repair cast framework -upper','','');
insert into b_icd9 values ('0555000001339','2277443','การซ่อมหรือเติมตะขอที่หักสำหรับฟันปลอมบางส่วนถอดได้ชนิดโครงโลหะ –ขากรรไกรบน','Repair or replace broken clasp -upper','','');
insert into b_icd9 values ('0555000001340','2277444','การเติมฟันที่แตกหักหรือลึกสำหรับฟันปลอมบางส่วนถอดได้ชนิดโครงโลหะ –ขากรรไกรบน','Replace broken or worn teeth -upper','','');
insert into b_icd9 values ('0555000001341','2277445','การเติมฟันบนฟันปลอมบางส่วนถอดได้ชนิดโครงโลหะ /ฐานอคริลิกอันเดิม  -ขากรรไกรบน','Add tooth to existing partial denture -upper','','');
insert into b_icd9 values ('0555000001342','2277446','การเติมตะขอบนฟันปลอมบางส่วนถอดได้ชนิดโครงโลหะ/ฐานอคริลิกอันเดิม  -ขากรรไกรบน','Add clasp to existing partial denture -upper','','');
insert into b_icd9 values ('0555000001343','2277447','การทดแทนส่วนของ semi-precision หรือ precision attachment ในฟันปลอมบางส่วนถอดได้บน ชนิดโครงโลหะ  -ขากรรไกรบน','Replacement of replacable part of semi-precision or precision attachment, male or female component - upper','','');
insert into b_icd9 values ('0555000001344','2277450','การเสริมฐานฟันปลอมทั้งปากโดยทำในปาก  -ขากรรไกรบน','Reline complete denture, chairside -upper','','');
insert into b_icd9 values ('0555000001345','2277451','การเสริมฐานอคริลิกสำหรับฟันปลอมบางส่วนถอดได้ชนิดโครงโลหะบนโดยทำในปากผู้ป่วย  -ขากรรไกรบน','Reline partial denture, chairside -upper','','');
insert into b_icd9 values ('0555000001346','2277460','การเสริมฐานฟันปลอมทั้งปากโดยพิมพ์ปากส่งห้องปฏิบัติการทันตกรรม  -ขากรรไกรบน','Reline complete denture, laboratory -upper','','');
insert into b_icd9 values ('0555000001347','2277461','การเสริมฐานอคริลิกสำหรับฟันปลอมบางส่วน  -ขากรรไกรบน','Reline partial denture, laboratory -upper','','');
insert into b_icd9 values ('0555000001348','2277470','การเปลี่ยนฐานอคริลิกสำหรับฟันปลอมทั้งปาก (ฟันบน)','Rebase complete denture, upper','','');
insert into b_icd9 values ('0555000001349','2277471','การเปลี่ยนฐานอคริลิกสำหรับฟันปลอมบางส่วน (ฟันบน)','Rebase partial denture, upper','','');
insert into b_icd9 values ('0555000001350','2277472','การเสริมฐานฟันปลอมทั้งปาก โดยใช้สารปรับภาวะ (tissue conditioning) (ฟันบน)','Tissue conditioning-complete denture -upper','','');
insert into b_icd9 values ('0555000001351','2277473','การเสริมฐานฟันปลอมบางส่วน โดยใช้สารปรับภาวะ (tissue conditioning) (ฟันบน)','Tissue conditioning-partial denture -upper','','');
insert into b_icd9 values ('0555000001352','2287410','การใส่ฟันปลอมเต็มส่วน –ขากรรไกรล่าง','Complete denture -lower','','');
insert into b_icd9 values ('0555000001353','2287411','การใส่ฟันปลอมเต็มส่วนชั่วคราวใส่ทันที –ขากรรไกรล่าง','Immediate complete denture -lower','','');
insert into b_icd9 values ('0555000001354','2287412','การใส่ฟันปลอมเฉพาะกาลเต็มส่วน –ขากรรไกรล่าง','Interim complete denture -lower','','');
insert into b_icd9 values ('0555000001355','2287413','การใส่ฟันปลอมเต็มส่วนทับราก –ขากรรไกรล่าง','Complete overdenture -lower','','');
insert into b_icd9 values ('0555000001356','2287420','การใส่ฟันปลอมบางส่วนถอดได้ฐานอคริลิก –ขากรรไกรล่าง','Acrylic partial denture, including any conventional clasps, rests and teeth -lower','','');
insert into b_icd9 values ('0555000001357','2287421','การใส่ฟันปลอมบางส่วนถอดได้ชนิดโครงโลหะ –ขากรรไกรล่าง','Metallic partial denture, including any conventional clasps, rests and teeth -lower','','');
insert into b_icd9 values ('0555000001358','2287422','การใส่ฟันปลอมบางส่วนถอดได้ชนิดมีสลัก –ขากรรไกรล่าง','Metallic partial denture with precision attachment -lower','','');
insert into b_icd9 values ('0555000001359','2287423','การใส่ฟันปลอมบางส่วนถอดได้ชนิดมีบานพับ','Swinglock partial denture','','');
insert into b_icd9 values ('0555000001360','2287424','Adjust complete denture -lower','Adjust complete denture -lower','','');
insert into b_icd9 values ('0555000001361','2287431','การปรับแต่งฟันปลอมบางส่วนถอดได้ –ขากรรไกรล่าง','Adjust partial denture-acrylic and metalic -lower','','');
insert into b_icd9 values ('0555000001362','2287432','การปรับฟันปลอมถอดได้หลังการทำศัลยกรรมรากเทียม –ขากรรไกรล่าง','Modification of removable prosthesis following implant surgery -lower','','');
insert into b_icd9 values ('0555000001363','2287441','การซ่อมฐานฟันปลอมอคริลิก ของฟันปลอมบางส่วนถอดได้ชนิดโครงโลหะ –ขากรรไกรล่าง','Repair resin denture base -lower','','');
insert into b_icd9 values ('0555000001364','2287442','การซ่อมส่วนโครงโลหะของฟันปลอมบางส่วนถอดได้ชนิดโครงโลหะ –ขากรรไกรบน','Repair cast framework -lower','','');
insert into b_icd9 values ('0555000001365','2287443','การซ่อมหรือเติมตะขอที่หักสำหรับฟันปลอมบางส่วนถอดได้ชนิดโครงโลหะ –ขากรรไกรล่าง','Repair or replace broken clasp -lower','','');
insert into b_icd9 values ('0555000001366','2287444','การเติมฟันที่แตกหักหรือลึกสำหรับฟันปลอมบางส่วนถอดได้ชนิดโครงโลหะ –ขากรรไกรล่าง','Replace broken or worn teeth -lower','','');
insert into b_icd9 values ('0555000001367','2287445','การเติมฟันล่างฟันปลอมบางส่วนถอดได้ชนิดโครงโลหะ /ฐานอคริลิกอันเดิม  -ขากรรไกรล่าง','Add tooth to existing partial denture -lower','','');
insert into b_icd9 values ('0555000001368','2287446','การเติมตะขอบนฟันปลอมบางส่วนถอดได้ชนิดโครงโลหะ/ฐานอคริลิกอันเดิม –ขากรรไกรล่าง','Add clasp to existing partial denture -lower','','');
insert into b_icd9 values ('0555000001369','2287447','การทดแทนส่วนของ semi-precision หรือ precision attachment ในฟันปลอมบางส่วนถอดได้ล่างชนิดโครงโลหะ  -ขากรรไกรล่าง','Replacement of replacable part of semi-precision or precision attachment, male or female component - lower','','');
insert into b_icd9 values ('0555000001370','2287450','การเสริมฐานฟันปลอมทั้งปากโดยทำในปาก  -ขากรรไกรล่าง','Reline complete denture, chairside -lower','','');
insert into b_icd9 values ('0555000001371','2287451','การเสริมฐานอคริลิกสำหรับฟันปลอมบางส่วนถอดได้ชนิดโครงโลหะล่างโดยทำในปากผู้ป่วย  -ขากรรไกรล่าง','Reline partial denture, chairside -lower','','');
insert into b_icd9 values ('0555000001372','2287460','การเสริมฐานฟันปลอมทั้งปากโดยพิมพ์ปากส่งห้องปฏิบัติการทันตกรรม  -ขากรรไกรล่าง','Reline complete denture, laboratory -lower','','');
insert into b_icd9 values ('0555000001373','2287461','การเสริมฐานอคริลิกสำหรับฟันปลอมบางส่วน  -ขากรรไกรล่าง','Reline partial denture, laboratory -lower','','');
insert into b_icd9 values ('0555000001374','2287470','การเปลี่ยนฐานอคริลิกสำหรับฟันปลอมทั้งปาก (ฟันล่าง)','Rebase complete denture, lower','','');
insert into b_icd9 values ('0555000001375','2287471','การเปลี่ยนฐานอคริลิกสำหรับฟันปลอมบางส่วน (ฟันล่าง)','Rebase partial denture, lower','','');
insert into b_icd9 values ('0555000001376','2287472','การเสริมฐานฟันปลอมทั้งปาก โดยใช้สารปรับภาวะ (tissue conditioning) (ฟันล่าง)','Tissue conditioning-complete denture -lower','','');
insert into b_icd9 values ('0555000001377','2287473','การเสริมฐานฟันปลอมบางส่วน โดยใช้สารปรับภาวะ (tissue conditioning) (ฟันล่าง)','Tissue conditioning-partial denture -lower','','');
insert into b_icd9 values ('0555000001378','2387410','การใส่สะพานฟันถอดได้','Removable bridge, including clasps and teeth','','');
insert into b_icd9 values ('0555000001379','2387420','การเติมฟันที่หายหรือแตกหักสำหรับฟันปลอมทั้งปาก','Replace missing or broken teeth-complete denture, each tooth','','');
insert into b_icd9 values ('0555000001380','2337599','การรักษาทางทันตกรรมประดิษฐ์ด้วยวิธีการอื่น','Unspecified prosthesis procedure, by report','','');
insert into b_icd9 values ('0555000001381','23874A1','Crown - porcelain fused to high noble metal','Crown - porcelain fused to high noble metal','','');
insert into b_icd9 values ('0555000001382','23874A2','Crown - porcelain fused to predominantly base metal','Crown - porcelain fused to predominantly base metal','','');
insert into b_icd9 values ('0555000001383','23874A3','Crown - porcelain fused to noble metal','Crown - porcelain fused to noble metal','','');
insert into b_icd9 values ('0555000001384','23874B1','Crown - full cast high noble metal','Crown - full cast high noble metal','','');
insert into b_icd9 values ('0555000001385','23874B2','Crown - full cast predominantly base metal','Crown - full cast predominantly base metal','','');
insert into b_icd9 values ('0555000001386','23874B3','Crown - full cast noble metal','Crown - full cast noble metal','','');
insert into b_icd9 values ('0555000001387','23874C1','Crown - 3/4 cast high noble metal','Crown - 3/4 cast high noble metal','','');
insert into b_icd9 values ('0555000001388','23874C2','Crown - 3/4 cast predominantly base metal','Crown - 3/4 cast predominantly base metal','','');
insert into b_icd9 values ('0555000001389','23874C3','Crown - 3/4 cast noble metal','Crown - 3/4 cast noble metal','','');
insert into b_icd9 values ('0555000001390','23874C4','Crown - 3/4 porcelain/ceramic','Crown - 3/4 porcelain/ceramic','','');
insert into b_icd9 values ('0555000001391','23874D1','Crown - resin with high noble metal','Crown - resin with high noble metal','','');
insert into b_icd9 values ('0555000001392','23874D2','Crown - resin with predominantly base metal','Crown - resin with predominantly base metal','','');
insert into b_icd9 values ('0555000001393','23874D3','Crown - resin with noble metal','Crown - resin with noble metal','','');
insert into b_icd9 values ('0555000001394','2.39E+05','Crown - porcelain/ceramic','Crown - porcelain/ceramic','','');
insert into b_icd9 values ('0555000001395','23874F1','Retainer - cast metal for resin bonded fixed prosthesis','Retainer - cast metal for resin bonded fixed prosthesis','','');
insert into b_icd9 values ('0555000001396','23874F2','Retainer - porcelain/ceramic for resin bonded fixed prosthesis','Retainer - porcelain/ceramic for resin bonded fixed prosthesis','','');
insert into b_icd9 values ('0555000001397','23874G1','Inlay - metallic - two surfaces','Inlay - metallic - two surfaces','','');
insert into b_icd9 values ('0555000001398','23874G2','Inlay - metallic - three or more surfaces','Inlay - metallic - three or more surfaces','','');
insert into b_icd9 values ('0555000001399','23874G3','Onlay - metallic - three surfaces','Onlay - metallic - three surfaces','','');
insert into b_icd9 values ('0555000001400','23874G4','Onlay - metallic - four or more surfaces','Onlay - metallic - four or more surfaces','','');
insert into b_icd9 values ('0555000001401','23874H5','Inlay/onlay - porcelain/ceramic','Inlay/onlay - porcelain/ceramic','','');
insert into b_icd9 values ('0555000001402','23874J1','Pontic - porcelain fused to high noble metal','Pontic - porcelain fused to high noble metal','','');
insert into b_icd9 values ('0555000001403','23874J2','Pontic - porcelain fused to predominantly base metal','Pontic - porcelain fused to predominantly base metal','','');
insert into b_icd9 values ('0555000001404','23874J3','Pontic - porcelain fused to  noble metal','Pontic - porcelain fused to  noble metal','','');
insert into b_icd9 values ('0555000001405','23874K1','Pontic - cast high noble metal','Pontic - cast high noble metal','','');
insert into b_icd9 values ('0555000001406','23874K2','Pontic - cast predominantly base metal','Pontic - cast predominantly base metal','','');
insert into b_icd9 values ('0555000001407','23874K3','Pontic - cast noble metal','Pontic - cast noble metal','','');
insert into b_icd9 values ('0555000001408','23874L1','Pontic - resin with high noble metal','Pontic - resin with high noble metal','','');
insert into b_icd9 values ('0555000001409','23874L2','Pontic - resin with predominantly base metal','Pontic - resin with predominantly base metal','','');
insert into b_icd9 values ('0555000001410','23874L3','Pontic - resin with  noble metal','Pontic - resin with  noble metal','','');
insert into b_icd9 values ('0555000001411','23874M1','Pontic - porcelain / ceramic','Pontic - porcelain / ceramic','','');
insert into b_icd9 values ('0555000001412','23874N1','Core build up for retainer, including any pins','Core build up for retainer, including any pins','','');
insert into b_icd9 values ('0555000001413','23874P1','Prefabricated post and core in addition to fixed partial denture retainer','Prefabricated post and core in addition to fixed partial denture retainer','','');
insert into b_icd9 values ('0555000001414','23874P2','Each additional prefabricated post - same tooth','Each additional prefabricated post - same tooth','','');
insert into b_icd9 values ('0555000001415','23874Q1','Cast post as part of fixed partial denture retainer','Cast post as part of fixed partial denture retainer','','');
insert into b_icd9 values ('0555000001416','23874Q2','Cast post and core in addition to fixed partial denture retainer','Cast post and core in addition to fixed partial denture retainer','','');
insert into b_icd9 values ('0555000001417','23874Q3','Each additional cast post - same tooth','Each additional cast post - same tooth','','');
insert into b_icd9 values ('0555000001418','23874R1','Coping - metal','Coping - metal','','');
insert into b_icd9 values ('0555000001419','23874R2','การทำ  coping abutment ด้วยวัสดุบรูณะฟัน','การทำ  coping abutment ด้วยวัสดุบรูณะฟัน','','');
insert into b_icd9 values ('0555000001420','23874S1','Stress breaker','Stress breaker','','');
insert into b_icd9 values ('0555000001421','23874T1','Precision attachment','Precision attachment','','');
insert into b_icd9 values ('0555000001422','23874U1','Connector bar','Connector bar','','');
insert into b_icd9 values ('0555000001423','23874V1','Recement fixed partial denture','Recement fixed partial denture','','');
insert into b_icd9 values ('0555000001424','23874V2','Fixed partial denture repair, by report','Fixed partial denture repair, by report','','');
insert into b_icd9 values ('0555000001425','23874V3','Removal of fixed partial denture','Removal of fixed partial denture','','');
insert into b_icd9 values ('0555000001426','2337499','Unspecified prosthesis procedure, by report','Unspecified prosthesis procedure, by report','','');
insert into b_icd9 values ('0555000001427','1057410','Cranial prosthesis','Cranial prosthesis','','');
insert into b_icd9 values ('0555000001428','1547410','Facial prosthesis','Facial prosthesis','','');
insert into b_icd9 values ('0555000001429','1547411','Facial prosthesis, replacement','Facial prosthesis, replacement','','');
insert into b_icd9 values ('0555000001430','1547412','Facial augmentation implant prosthesis','Facial augmentation implant prosthesis','','');
insert into b_icd9 values ('0555000001431','1547413','Facial moulage(sectional)','Facial moulage(sectional)','','');
insert into b_icd9 values ('0555000001432','1547414','Facial moulage(complete)','Facial moulage(complete)','','');
insert into b_icd9 values ('0555000001433','1597430','Surgical stent - upper','Surgical stent - upper','','');
insert into b_icd9 values ('0555000001434','1597431','Surgical splint- upper','Surgical splint- upper','','');
insert into b_icd9 values ('0555000001435','1607410','Mandibular resection prosthesis with guide flange','Mandibular resection prosthesis with guide flange','','');
insert into b_icd9 values ('0555000001436','1607411','Mandibular resection prosthesis without guide flange','Mandibular resection prosthesis without guide flange','','');
insert into b_icd9 values ('0555000001437','1607430','Surgical stent - lower','Surgical stent - lower','','');
insert into b_icd9 values ('0555000001438','1607431','Surgical splint- lower','Surgical splint- lower','','');
insert into b_icd9 values ('0555000001439','1627410','Trismus appliance (not for TMD treatment)','Trismus appliance (not for TMD treatment)','','');
insert into b_icd9 values ('0555000001440','1767410','Ocular prosthesis','Ocular prosthesis','','');
insert into b_icd9 values ('0555000001441','1767415','Ocular prosthesis, interim','Ocular prosthesis, interim','','');
insert into b_icd9 values ('0555000001442','1777410','Orbital prosthesis','Orbital prosthesis','','');
insert into b_icd9 values ('0555000001443','1777411','Orbital prosthesis, replacement','Orbital prosthesis, replacement','','');
insert into b_icd9 values ('0555000001444','1787410','Auricular prosthesis','Auricular prosthesis','','');
insert into b_icd9 values ('0555000001445','1787411','Auricular prosthesis, replacement','Auricular prosthesis, replacement','','');
insert into b_icd9 values ('0555000001446','1857410','Nasal prosthesis','Nasal prosthesis','','');
insert into b_icd9 values ('0555000001447','1857411','Nasal prosthesis, replacement','Nasal prosthesis, replacement','','');
insert into b_icd9 values ('0555000001448','1887410','Nasal septal prosthesis','Nasal septal prosthesis','','');
insert into b_icd9 values ('0555000001449','2407410','Obturator prosthesis, surgical','Obturator prosthesis, surgical','','');
insert into b_icd9 values ('0555000001450','2407411','Obturator prosthesis, definitive','Obturator prosthesis, definitive','','');
insert into b_icd9 values ('0555000001451','2407412','Obturator prosthesis, modification','Obturator prosthesis, modification','','');
insert into b_icd9 values ('0555000001452','2407415','Obturator prosthesis, interim','Obturator prosthesis, interim','','');
insert into b_icd9 values ('0555000001453','2407420','Feeding aid','Feeding aid','','');
insert into b_icd9 values ('0555000001454','2417410','Speech aid prosthesis, pediatric','Speech aid prosthesis, pediatric','','');
insert into b_icd9 values ('0555000001455','2417411','Speech aid prosthesis, adult','Speech aid prosthesis, adult','','');
insert into b_icd9 values ('0555000001456','2417412','Palate lift prosthesis, definitive','Palate lift prosthesis, definitive','','');
insert into b_icd9 values ('0555000001457','1547499','Unspecified maxillofacial prosthesis , by report','Unspecified maxillofacial prosthesis , by report','','');
insert into b_icd9 values ('0555000001458','1547510','Surgical placement of craniofaial (osseointegrated) implant for anchoring of facial prosthesis','Surgical placement of craniofaial (osseointegrated) implant for anchoring of facial prosthesis','','');
insert into b_icd9 values ('0555000001459','1547520','Prosthetic stage of  craniofaial (osseointegrated) implant for facial prosthesis','Prosthetic stage of  craniofaial (osseointegrated) implant for facial prosthesis','','');
insert into b_icd9 values ('0555000001460','1777510','Prosthetic stage of  craniofaial (osseointegrated) implant for anchoring of orbital(eye) prosthesis','Prosthetic stage of  craniofaial (osseointegrated) implant for anchoring of orbital(eye) prosthesis','','');
insert into b_icd9 values ('0555000001461','1777520','Prosthetic stage of  craniofaial (osseointegrated) implant for  orbit(eye) prosthesis','Prosthetic stage of  craniofaial (osseointegrated) implant for  orbit(eye) prosthesis','','');
insert into b_icd9 values ('0555000001462','1787510','Surgical placement of craniofaial (osseointegrated) implant for anchoring of ear prosthesis','Surgical placement of craniofaial (osseointegrated) implant for anchoring of ear prosthesis','','');
insert into b_icd9 values ('0555000001463','1787520','Prosthetic stage of  craniofaial (osseointegrated) implant for ear prosthesis','Prosthetic stage of  craniofaial (osseointegrated) implant for ear prosthesis','','');
insert into b_icd9 values ('0555000001464','1857510','Surgical placement of craniofaial (osseointegrated) implant for anchoring of ear prosthesis','Surgical placement of craniofaial (osseointegrated) implant for anchoring of ear prosthesis','','');
insert into b_icd9 values ('0555000001465','1857520','Prosthetic stage of  craniofaial (osseointegrated) implant for nose prosthesis','Prosthetic stage of  craniofaial (osseointegrated) implant for nose prosthesis','','');
insert into b_icd9 values ('0555000001466','2337510','Surgical placement of implant body (fixture) : endosteal implant','Surgical placement of implant body (fixture) : endosteal implant','','');
insert into b_icd9 values ('0555000001467','2337511','Surgical placement of healing cap : endosteal implant','Surgical placement of healing cap : endosteal implant','','');
insert into b_icd9 values ('0555000001468','2337512','Surgical placement of implant body and healing cap : endosteal implant','Surgical placement of implant body and healing cap : endosteal implant','','');
insert into b_icd9 values ('0555000001469','2337513','Abutment placement or substitution : endosteal implant','Abutment placement or substitution : endosteal implant','','');
insert into b_icd9 values ('0555000001470','2337514','Surgical placement : eposteal implant','Surgical placement : eposteal implant','','');
insert into b_icd9 values ('0555000001471','2337515','Surgical placement : transosteal implant','Surgical placement : transosteal implant','','');
insert into b_icd9 values ('0555000001472','2337516','Implant removal, by report','Implant removal, by report','','');
insert into b_icd9 values ('0555000001473','2337519','Other surgical technique for implant, by report','Other surgical technique for implant, by report','','');
insert into b_icd9 values ('0555000001474','2337520','Prefabricated abutment','Prefabricated abutment','','');
insert into b_icd9 values ('0555000001475','2337521','Custom abutment','Custom abutment','','');
insert into b_icd9 values ('0555000001476','2337522','Dental implant supported connecting bar','Dental implant supported connecting bar','','');
insert into b_icd9 values ('0555000001477','2337523','Abutment supported porcelain/ceramic crown','Abutment supported porcelain/ceramic crown','','');
insert into b_icd9 values ('0555000001478','2337524','Abutment supported porcelain fused to metal crown (high noble metal)','Abutment supported porcelain fused to metal crown (high noble metal)','','');
insert into b_icd9 values ('0555000001479','2337525','Abutment supported porcelain fused to metal crown (predominantly base metal)','Abutment supported porcelain fused to metal crown (predominantly base metal)','','');
insert into b_icd9 values ('0555000001480','2337526','Abutment supported porcelain fused to metal crown , noble metal','Abutment supported porcelain fused to metal crown , noble metal','','');
insert into b_icd9 values ('0555000001481','2337527','Abutment supported cast metal crown, high noble metal','Abutment supported cast metal crown, high noble metal','','');
insert into b_icd9 values ('0555000001482','2337528','Abutment supported cast metal crown, predominantly base metal','Abutment supported cast metal crown, predominantly base metal','','');
insert into b_icd9 values ('0555000001483','2337529','Abutment supported cast metal crown, noble metal','Abutment supported cast metal crown, noble metal','','');
insert into b_icd9 values ('0555000001484','2337530','Abutment supported retainer for porcelain/ceramic FPD','Abutment supported retainer for porcelain/ceramic FPD','','');
insert into b_icd9 values ('0555000001485','2337531','Abutment supported retainer for porcelain fused to metal FPD (high noble metal)','Abutment supported retainer for porcelain fused to metal FPD (high noble metal)','','');
insert into b_icd9 values ('0555000001486','2337532','Abutment supported retainer for porcelain fused to metal FPD, predominantly base metal','Abutment supported retainer for porcelain fused to metal FPD, predominantly base metal','','');
insert into b_icd9 values ('0555000001487','2337533','Abutment supported retainer for porcelain fused to metal FPD, noble metal','Abutment supported retainer for porcelain fused to metal FPD, noble metal','','');
insert into b_icd9 values ('0555000001488','2337534','Abutment supported retainer for cast metal metal FPD, high noble metal','Abutment supported retainer for cast metal metal FPD, high noble metal','','');
insert into b_icd9 values ('0555000001489','2337535','Abutment supported retainer for  cast metal  FPD, predominantly base metal','Abutment supported retainer for  cast metal  FPD, predominantly base metal','','');
insert into b_icd9 values ('0555000001490','2337536','Abutment supported retainer for  cast metal  FPD, noble metal','Abutment supported retainer for  cast metal  FPD, noble metal','','');
insert into b_icd9 values ('0555000001491','2337540','Implant supported porcelain/ceramic crown','Implant supported porcelain/ceramic crown','','');
insert into b_icd9 values ('0555000001492','2337541','Implant supported retainer for porcelain fused ot metal FPD (titanium, titanium alloy, or high noble metal)','Implant supported retainer for porcelain fused ot metal FPD (titanium, titanium alloy, or high noble metal)','','');
insert into b_icd9 values ('0555000001493','2337542','Implant supported retainer for cast matal FPD  (titanium, titanium alloy, or high moble metal)','Implant supported retainer for cast matal FPD  (titanium, titanium alloy, or high moble metal)','','');
insert into b_icd9 values ('0555000001494','2337543','Implant supported retainer for ceramic FPD','Implant supported retainer for ceramic FPD','','');
insert into b_icd9 values ('0555000001495','2337544','Implant supported retainer for porcelain fused ot metal FPD (titanium, titanium alloy, or high noble metal)','Implant supported retainer for porcelain fused ot metal FPD (titanium, titanium alloy, or high noble metal)','','');
insert into b_icd9 values ('0555000001496','2337545','Implant supported retainer for cast matal FPD  (titanium, titanium alloy, or high moble metal )','Implant supported retainer for cast matal FPD  (titanium, titanium alloy, or high moble metal )','','');
insert into b_icd9 values ('0555000001497','2337550','Implant/abutment supported fixed denture for completely edentulous arch','Implant/abutment supported fixed denture for completely edentulous arch','','');
insert into b_icd9 values ('0555000001498','2337551','Implant/abutment supported fixed denture for partially edentulous arch','Implant/abutment supported fixed denture for partially edentulous arch','','');
insert into b_icd9 values ('0555000001499','2337560','Implant maintenance procedures, including removal of prosthesis, cleansing of prosthesis and abutments and reinsertion of prosthesis','Implant maintenance procedures, including removal of prosthesis, cleansing of prosthesis and abutments and reinsertion of prosthesis','','');
insert into b_icd9 values ('0555000001500','2337570','Repair implant abutment, by report','Repair implant abutment, by report','','');
insert into b_icd9 values ('0555000001501','2337580','Repair implant supported prosthesis, by report','Repair implant supported prosthesis, by report','','');
insert into b_icd9 values ('0555000001502','2337598','Unspecified prosthesis procedure, by report','Unspecified prosthesis procedure, by report','','');
insert into b_icd9 values ('0555000001504','2337610','Removable appliance therapy','Removable appliance therapy','','');
insert into b_icd9 values ('0555000001505','2337611','Partial  fixed appliance therapy','Partial  fixed appliance therapy','','');
insert into b_icd9 values ('0555000001506','2337620','Removable appliance therapy','Removable appliance therapy','','');
insert into b_icd9 values ('0555000001507','2337621','Partial  fixed appliance therapy','Partial  fixed appliance therapy','','');
insert into b_icd9 values ('0555000001508','2337630','Removeable functional  appliance therapy','Removeable functional  appliance therapy','','');
insert into b_icd9 values ('0555000001509','2337631','Partial  fixed appliance therapy','Partial  fixed appliance therapy','','');
insert into b_icd9 values ('0555000001510','2337632','Simple full fixed appliance therapy','Simple full fixed appliance therapy','','');
insert into b_icd9 values ('0555000001511','2337633','Complex full fixed appliance therapy','Complex full fixed appliance therapy','','');
insert into b_icd9 values ('0555000001512','2337634','Orthognathic surgery therapy','Orthognathic surgery therapy','','');
insert into b_icd9 values ('0555000001513','2337640','Removable appliance','Removable appliance','','');
insert into b_icd9 values ('0555000001514','2337641','Fixed functional  appliance','Fixed functional  appliance','','');
insert into b_icd9 values ('0555000001515','2337642','Passive intraoral appliance  เช่น  transpalatal arch, nance','Passive intraoral appliance  เช่น  transpalatal arch, nance','','');
insert into b_icd9 values ('0555000001516','2337643','Active intraoral appliance, orthodontic force เช่น Quad helix','Active intraoral appliance, orthodontic force เช่น Quad helix','','');
insert into b_icd9 values ('0555000001517','2337644','Active intraoral appliance, orthopaedic force เช่น RME','Active intraoral appliance, orthopaedic force เช่น RME','','');
insert into b_icd9 values ('0555000001518','2337645','Head gear','Head gear','','');
insert into b_icd9 values ('0555000001519','2337646','Fask mask','Fask mask','','');
insert into b_icd9 values ('0555000001520','2337650','Removable appliance therapy','Removable appliance therapy','','');
insert into b_icd9 values ('0555000001521','2337651','Fixed appliance therapy','Fixed appliance therapy','','');
insert into b_icd9 values ('0555000001522','2337660','Analysis  of model,  cephalogram or growth forecast and treatment  prediction','Analysis  of model,  cephalogram or growth forecast and treatment  prediction','','');
insert into b_icd9 values ('0555000001523','2337661','Pre-orthodontic treatment visit','Pre-orthodontic treatment visit','','');
insert into b_icd9 values ('0555000001524','2337662','Periodic orthodontic treatment visit','Periodic orthodontic treatment visit','','');
insert into b_icd9 values ('0555000001525','2337663','Orthodontic retention (removal of appliances, construction and  plaecment of retainer(s)','Orthodontic retention (removal of appliances, construction and  plaecment of retainer(s)','','');
insert into b_icd9 values ('0555000001526','2337664','Orthodontic treatment','Orthodontic treatment','','');
insert into b_icd9 values ('0555000001527','2337665','Repair of orthodontic appliance','Repair of orthodontic appliance','','');
insert into b_icd9 values ('0555000001528','2337666','Replacement of lost or broken retainer','Replacement of lost or broken retainer','','');
insert into b_icd9 values ('0555000001529','2337667','Setup model','Setup model','','');
insert into b_icd9 values ('0555000001530','2337699','Unspecified orthodontic procedure, by report','Unspecified orthodontic procedure, by report','','');
insert into b_icd9 values ('0555000001531','1549910','Non-surgical treatments eg.; physiotherapy, jaw exercise, occlusal splint, acupuncture medication','Non-surgical treatments eg.; physiotherapy, jaw exercise, occlusal splint, acupuncture medication','','');
insert into b_icd9 values ('0555000001532','1549911','Surgical treatments eg; alcohol injection to the nerves involved, cryosurgery, laser surgery','Surgical treatments eg; alcohol injection to the nerves involved, cryosurgery, laser surgery','','');
insert into b_icd9 values ('0555000001533','1549998','Other treatments, nonspecified','Other treatments, nonspecified','','');
insert into b_icd9 values ('0555000001534','1549999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณหน้า','Other procedures and operations on facaial skin','','');
insert into b_icd9 values ('0555000001535','1629910','Non-surgical treatments eg; physiotherapy, jaw exercise, occlusal splint, acupuncture thermal, ultrasound and medication','Non-surgical treatments eg; physiotherapy, jaw exercise, occlusal splint, acupuncture thermal, ultrasound and medication','','');
insert into b_icd9 values ('0555000001536','2339910','Occlusion analysis- mounted case','Occlusion analysis- mounted case','','');
insert into b_icd9 values ('0555000001537','2339911','Occlusal guard, by report','Occlusal guard, by report','','');
insert into b_icd9 values ('0555000001538','2339912','Fabrication of athletic mouthguard','Fabrication of athletic mouthguard','','');
insert into b_icd9 values ('0555000001539','2339913','Occlusal adjustment, limited','Occlusal adjustment, limited','','');
insert into b_icd9 values ('0555000001540','2339914','Occlusal adjustment, complete','Occlusal adjustment, complete','','');
insert into b_icd9 values ('0555000001541','2339915','Palliative/emergency treatment of dental pain -minor procedure','Palliative/emergency treatment of dental pain -minor procedure','','');
insert into b_icd9 values ('0555000001542','2339920','Consultation','Consultation','','');
insert into b_icd9 values ('0555000001543','2339999','การทำหัตถการและการผ่าตัดอื่นๆบริเวณในปาก','Unspecified dental procedure, by report','','');

INSERT INTO s_version VALUES ('9701000000052', '52', 'Hospital OS, Community Edition', '3.9.19', '3.18.010112', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_19.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.19');
package com.hospital_os.objdb;

import com.hospital_os.object.*;
import com.hospital_os.usecase.connection.*;
import com.hospital_os.utility.*;
import java.sql.*;
import java.util.*;

@SuppressWarnings("UseOfObsoleteCollectionType")
public class RelationDB {

    public ConnectionInf theConnectionInf;
    public Relation dbObj;
    final public String idtable = "227";/*"199";
     */


    /**
     * @param ConnectionInf db
     * @roseuid 3F65897F0326
     */
    public RelationDB(ConnectionInf db) {
        theConnectionInf = db;
        dbObj = new Relation();
        initConfig();
    }

    private boolean initConfig() {
        dbObj.table = "f_patient_relation";
        dbObj.pk_field = "f_patient_relation_id";
        dbObj.description = "patient_relation_description";
        dbObj.max_relation_id = "max_relation_id";
        return true;
    }

    /**
     * @param cmd
     * @param o
     * @return int
     * @roseuid 3F6574DE0394
     */
    public int insert(Relation o) throws Exception {
        String sql = "";
        Relation p = o;

        sql = "insert into " + dbObj.table + " ("
                + dbObj.pk_field
                + " ," + dbObj.description
                + " ) values ('"
                + p.getObjectId()
                + "','" + Gutil.CheckReservedWords(p.description)
                + "')";
        sql = Gutil.convertSQLToMySQL(sql, theConnectionInf.gettypeDatabase());

        return theConnectionInf.eUpdate(sql);
    }

    public int update(Relation o) throws Exception {
        String sql = "update " + dbObj.table + " set ";
        Relation p = o;
        String field = ""
                + "', " + dbObj.description + "='" + Gutil.CheckReservedWords(p.description)
                + "' where " + dbObj.pk_field + "='" + p.getObjectId() + "'";
        sql = Gutil.convertSQLToMySQL(sql + field.substring(2), theConnectionInf.gettypeDatabase());

        return theConnectionInf.eUpdate(sql);
    }

    public int delete(Relation o) throws Exception {
        String sql = "delete from " + dbObj.table
                + " where " + dbObj.pk_field + "='" + o.getObjectId() + "'";
        return theConnectionInf.eUpdate(sql);
    }

    public Relation selectByPK(String pk) throws Exception {
        String sql = "select * from " + dbObj.table
                + " where " + dbObj.pk_field
                + " = '" + pk + "'";

        Vector v = eQuery(sql);
        if (v.isEmpty()) {
            return null;
        } else {
            return (Relation) v.get(0);
        }
    }

    public Relation selectByName(String name) throws Exception {
        String sql = "select * from " + dbObj.table
                + " where " + dbObj.description
                + " like '" + Gutil.CheckReservedWords(name.trim()) + "'";

        Vector v = eQuery(sql);

        if (v.isEmpty()) {
            return null;
        } else {
            return (Relation) v.get(0);
        }
    }

    public Vector selectAll() throws Exception {
        String sql = "select * from " + dbObj.table + " order by "
                + dbObj.description;
        Vector v = veQuery(sql);
        if (v.isEmpty()) {
            return null;
        } else {
            return v;
        }

    }

    /**
     * @deprecated henbe unused
     *
     */
    public Vector veQuery(String sql) throws Exception {
        ComboFix p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            p = new ComboFix();
            p.code = rs.getString(dbObj.pk_field);
            p.name = rs.getString(dbObj.description);
            list.add(p);
        }
        rs.close();
        return list;
    }

    public Vector eQuery(String sql) throws Exception {
        Relation p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            p = new Relation();
            p.setObjectId(rs.getString(dbObj.pk_field));
            p.description = rs.getString(dbObj.description);
            list.add(p);
        }
        rs.close();
        return list;
    }

    public String selectMaxCode() throws Exception {
        String sql = "select max(" + dbObj.pk_field + ") as max_relation_id from " + dbObj.table;

        Vector v = relationQuery(sql);
        if (v.isEmpty()) {
            return null;
        } else {
            return ((Relation) v.get(0)).getObjectId();
        }
    }

    public Vector relationQuery(String sql) throws Exception {
        Relation p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            p = new Relation();
            p.setObjectId(rs.getString(dbObj.max_relation_id));
            list.add(p);
        }
        rs.close();
        return list;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.objdb.objdbclass;

import com.hospital_os.usecase.connection.ConnectionInf;
import com.pcu.object.THealthRehabilitation;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sompr
 */
public class THealthRehabilitationDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "PCU99";

    public THealthRehabilitationDB(ConnectionInf c) {
        this.connectionInf = c;
    }

    public int insert(THealthRehabilitation obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO t_health_rehabilitation(\n"
                    + "            t_health_rehabilitation_id, t_person_id, t_visit_id, survey_date, start_date, finish_date, f_rehabilitation_code_id, f_rehabilitation_device_id, device_qty, hospital, active, user_record_id, user_update_id)\n"
                    + "    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.getGenID(tableId));
            preparedStatement.setString(index++, obj.t_person_id);
            preparedStatement.setString(index++, obj.t_visit_id);
            preparedStatement.setDate(index++, new Date(obj.survey_date.getTime()));
            preparedStatement.setDate(index++, obj.start_date == null ? null : new Date(obj.start_date.getTime()));
            preparedStatement.setDate(index++, obj.finish_date == null ? null : new Date(obj.finish_date.getTime()));
            preparedStatement.setString(index++, obj.f_rehabilitation_code_id);
            preparedStatement.setString(index++, obj.f_rehabilitation_device_id);
            if (obj.f_rehabilitation_device_id == null) {
                preparedStatement.setNull(index++, Types.INTEGER);
            } else {
                preparedStatement.setInt(index++, obj.device_qty);
            }
            preparedStatement.setString(index++, obj.hospital);
            preparedStatement.setString(index++, obj.active);
            preparedStatement.setString(index++, obj.user_record_id);
            preparedStatement.setString(index++, obj.user_update_id);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(THealthRehabilitation obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_health_rehabilitation\n");
            sql.append("   SET survey_date=?, start_date=?, finish_date=?, f_rehabilitation_code_id=?, f_rehabilitation_device_id=?, device_qty=?, hospital=?, \n");
            sql.append("       active=?, user_update_id=?, update_date_time = current_timestamp\n");
            sql.append(" WHERE t_health_rehabilitation_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setDate(index++, new Date(obj.survey_date.getTime()));
            preparedStatement.setDate(index++, obj.start_date == null ? null : new Date(obj.start_date.getTime()));
            preparedStatement.setDate(index++, obj.finish_date == null ? null : new Date(obj.finish_date.getTime()));
            preparedStatement.setString(index++, obj.f_rehabilitation_code_id);
            preparedStatement.setString(index++, obj.f_rehabilitation_device_id);
            if (obj.f_rehabilitation_device_id == null) {
                preparedStatement.setNull(index++, Types.INTEGER);
            } else {
                preparedStatement.setInt(index++, obj.device_qty);
            }
            preparedStatement.setString(index++, obj.hospital);
            preparedStatement.setString(index++, obj.active);
            preparedStatement.setString(index++, obj.user_update_id);
            preparedStatement.setString(index++, obj.getObjectId());
            // execute update SQL stetement

            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int inactive(THealthRehabilitation obj) throws Exception {
        int ret = 0;
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_health_rehabilitation\n");
            sql.append("  SET active=?, user_cancel_id=?, cancel_date_time=current_timestamp\n");
            sql.append(" WHERE t_health_rehabilitation_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, "0");
            preparedStatement.setString(2, obj.user_cancel_id);
            preparedStatement.setString(3, obj.getObjectId());
            // execute update SQL stetement
            ret = preparedStatement.executeUpdate();
        } catch (Exception ex) {
            ret = 0;
            throw ex;
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
        return ret;
    }

    public THealthRehabilitation select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select t_health_rehabilitation.* from t_health_rehabilitation\n"
                    + "where t_health_rehabilitation.t_health_rehabilitation_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<THealthRehabilitation> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<THealthRehabilitation> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<THealthRehabilitation> list = new ArrayList<THealthRehabilitation>();
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()) {
            THealthRehabilitation obj = new THealthRehabilitation();
            obj.setObjectId(rs.getString("t_health_rehabilitation_id"));
            obj.t_person_id = rs.getString("t_person_id");
            obj.t_visit_id = rs.getString("t_visit_id");
            obj.survey_date = rs.getDate("survey_date");
            obj.start_date = rs.getDate("start_date");
            obj.finish_date = rs.getDate("finish_date");
            obj.f_rehabilitation_code_id = rs.getString("f_rehabilitation_code_id");
            obj.f_rehabilitation_device_id = rs.getString("f_rehabilitation_device_id");
            obj.device_qty = rs.getInt("device_qty");
            obj.hospital = rs.getString("hospital");
            obj.active = rs.getString("active");
            obj.user_record_id = rs.getString("user_record_id");
            obj.record_date_time = rs.getTimestamp("record_date_time");
            obj.user_update_id = rs.getString("user_update_id");
            obj.update_date_time = rs.getTimestamp("update_date_time");
            obj.user_cancel_id = rs.getString("user_cancel_id");
            obj.cancel_date_time = rs.getTimestamp("cancel_date_time");
            list.add(obj);
        }
        rs.close();
        return list;
    }

    public int updateFidByFid(String family_id, String family_from) throws Exception {
        String sql = "UPDATE t_health_rehabilitation SET t_person_id = '" + family_id + "' WHERE t_person_id = '" + family_from + "'";
        return connectionInf.eUpdate(sql);
    }
}

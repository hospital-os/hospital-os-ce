/*
 * AccidentTypeLookup.java
 *
 * Created on 2 �Զع�¹ 2549, 13:28 �.
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
package com.hosv3.control.lookup;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.usecase.connection.LookupControlInf;
import com.hospital_os.utility.ComboFix;
import java.sql.ResultSet;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @not deprecated because use henbe package
 *
 * @author Padungrat(tong)
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class R53SurveilOrgaLookup implements LookupControlInf {

    private static final Logger LOG = Logger.getLogger(R53SurveilOrgaLookup.class.getName());
    private final ConnectionInf theConnectionInf;
    private Vector vret;

    /**
     * Creates a new instance of AccidentTypeLookup
     */
    public R53SurveilOrgaLookup(ConnectionInf con) {
        theConnectionInf = con;
    }

    @Override
    public java.util.Vector listData(String str) {
        try {
            theConnectionInf.open();
            ResultSet rs = theConnectionInf.eQuery(
                    "select * from (select min(id) as id,name||' ('||min(id)||')' as des "
                    + "from r_rp1853_surveiloganism group by name order by name) instype "
                    + "where id ilike '" + str + "' or des ilike '%" + str + "%' order by des");
            vret = new Vector();
            while (rs.next()) {
                ComboFix cf = new ComboFix();
                cf.code = rs.getString(1);
                cf.name = rs.getString(2);
                vret.add(cf);
            }
            return vret;
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
            return null;
        } finally {
            theConnectionInf.close();
        }
    }

    @Override
    public CommonInf readHosData(String str) {
        Vector v = listData(str);
        if (v.isEmpty()) {
            return null;
        }
        return (ComboFix) v.get(0);
    }
}

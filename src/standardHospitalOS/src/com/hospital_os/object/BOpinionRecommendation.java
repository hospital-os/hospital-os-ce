/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.Persistent;
import java.util.Date;

/**
 *
 * @author Somprasong
 */
public class BOpinionRecommendation extends Persistent implements CommonInf {

    public String code = "";
    public String description = "";
    public String active = "1";
    public Date record_datetime;
    public String user_record_id = "";
    public Date update_datetime;
    public String user_update_id = "";

    @Override
    public String getCode() {
        return getObjectId();
    }

    @Override
    public String getName() {
        return description;
    }

    @Override
    public String toString() {
        return getName();
    }
}

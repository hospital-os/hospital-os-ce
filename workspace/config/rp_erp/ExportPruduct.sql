select Distinct
--Search Key
i.item_number as search_key
--name
,case when (i.item_trade_name is null or i.item_trade_name='') and (i.item_common_name is null or i.item_common_name='')
    then i.item_nick_name
when (i.item_trade_name is null or i.item_trade_name='') and i.item_common_name is not null
    then i.item_common_name
when i.item_trade_name='-' and i.item_common_name is not null
    then i.item_common_name
else i.item_trade_name
end as item_name
--x12de355 or UOM
,case when idu.item_drug_uom_number is null
then ''
else idu.item_drug_uom_number
end as uom
--productcategory
--,case when bis.f_item_group_id = '1'
--    then 'DRUG'
--2=Lab
--when bis.f_item_group_id = '2'
--    then 'LAB'
--3=XRay
--when bis.f_item_group_id = '3' 
--    then 'XRAY'
--when bis.f_item_group_id = '4'
--    then 'MEDS'
--5Service
--when bis.f_item_group_id = '5'
--    then 'SERV'
--end 
,'Standard' as productcategory
--producttype I=Item S=Service
,case when bis.f_item_group_id='1' or bis.f_item_group_id='4'
    then 'I'
else 'S'
end as producttype
--bpartner_value
,case when nd.company is null
then ''
else nd.company
end as bpartner_value
--iso_code
,'THB' as iso_code
--pricelist
,ip.item_price as pricelist
--pricepo
,0 as pricepo
--pricestd
,ip.item_price as pricestd
--pricelimit
,ip.item_price as pricelimit
from
b_item i 
left join b_nhso_map_drug nmd on i.b_item_id = nmd.b_item_id
left join b_nhso_drugcode24 nd on nmd.b_nhso_drugcode24_id = nd.b_nhso_drugcode24_id
left join b_item_drug id on i.b_item_id = id.b_item_id
left join b_item_drug_uom idu on id.item_drug_use_uom = idu.b_item_drug_uom_id
left join b_item_subgroup bis on (i.b_item_subgroup_id = bis.b_item_subgroup_id and bis.item_subgroup_active='1'
                                    )
left join b_item_price ip on i.b_item_id = ip.b_item_id

where
i.item_active='1'
and bis.f_item_group_id <> '2' and bis.f_item_group_id <> '3'

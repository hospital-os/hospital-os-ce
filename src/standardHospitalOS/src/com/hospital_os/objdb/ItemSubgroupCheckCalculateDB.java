/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.ItemSubgroupCheckCalculate;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.Gutil;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class ItemSubgroupCheckCalculateDB {

    private final ConnectionInf theConnectionInf;
    private final String tableId = "824";

    public ItemSubgroupCheckCalculateDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public int insert(ItemSubgroupCheckCalculate obj) throws Exception {
        String sql = "INSERT INTO b_map_item_subgroup_check_calculate( "
                + "b_map_item_subgroup_check_calculate_id, b_item_subgroup_id) "
                + "VALUES ('%s', '%s')";
        sql = String.format(sql, obj.getGenID(tableId), obj.b_item_subgroup_id);
        return theConnectionInf.eUpdate(sql);
    }

    public int delete(ItemSubgroupCheckCalculate itemDrugMapG6PD) throws Exception {
        String sql = "delete from b_map_item_subgroup_check_calculate where b_map_item_subgroup_check_calculate_id = '%s'";
        return theConnectionInf.eUpdate(String.format(sql, itemDrugMapG6PD.getObjectId()));
    }

    public ItemSubgroupCheckCalculate selectById(String id) throws Exception {
        String sql = "select *, b_item_subgroup.item_subgroup_description from b_map_item_subgroup_check_calculate "
                + "inner join b_item_subgroup on b_map_item_subgroup_check_calculate.b_item_subgroup_id = b_item_subgroup.b_item_subgroup_id "
                + "where b_map_item_subgroup_check_calculate_id = '%s'";
        List<ItemSubgroupCheckCalculate> eQuery = eQuery(String.format(sql, id));
        return (ItemSubgroupCheckCalculate) (eQuery.isEmpty() ? null : eQuery.get(0));
    }

    public ItemSubgroupCheckCalculate selectByItemId(String id) throws Exception {
        String sql = "select *, b_item_subgroup.item_subgroup_description from b_map_item_subgroup_check_calculate "
                + "inner join b_item_subgroup on b_map_item_subgroup_check_calculate.b_item_subgroup_id = b_item_subgroup.b_item_subgroup_id "
                + "where b_item_subgroup_id = '%s'";
        List<ItemSubgroupCheckCalculate> eQuery = eQuery(String.format(sql, id));
        return (ItemSubgroupCheckCalculate) (eQuery.isEmpty() ? null : eQuery.get(0));
    }

    public List<ItemSubgroupCheckCalculate> listAll() throws Exception {
        String sql = "select *, b_item_subgroup.item_subgroup_description from b_map_item_subgroup_check_calculate "
                + "inner join b_item_subgroup on b_map_item_subgroup_check_calculate.b_item_subgroup_id = b_item_subgroup.b_item_subgroup_id ";
        return eQuery(sql);
    }

    public List<ItemSubgroupCheckCalculate> eQuery(String sql) throws Exception {
        List<ItemSubgroupCheckCalculate> list = new ArrayList<ItemSubgroupCheckCalculate>();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        while (rs.next()) {
            ItemSubgroupCheckCalculate p = new ItemSubgroupCheckCalculate();
            p.setObjectId(rs.getString("b_map_item_subgroup_check_calculate_id"));
            p.b_item_subgroup_id = rs.getString("b_item_subgroup_id");
            try{
                p.item_subgroup_name = rs.getString("item_subgroup_description");
            }catch(Exception ex){
                
            }
            list.add(p);
        }
        rs.close();
        return list;
    }

    public List<Object[]> listByKeyword(String keyword) throws Exception {
        String sql = "select b_map_item_subgroup_check_calculate.b_map_item_subgroup_check_calculate_id"
                + ", b_item_subgroup.item_subgroup_description "
                + "from b_map_item_subgroup_check_calculate "
                + "inner join b_item_subgroup on b_map_item_subgroup_check_calculate.b_item_subgroup_id = b_item_subgroup.b_item_subgroup_id "
                + "where UPPER(b_item_subgroup.item_subgroup_description) like UPPER('%s') order by b_item_subgroup.item_subgroup_description";
        return theConnectionInf.eComplexQuery(String.format(sql, keyword == null || keyword.isEmpty() ? "%" : ("%" + Gutil.CheckReservedWords(keyword) + "%")));
    }

    public List<Object[]> listNotMapByKeyword(String keyword) throws Exception {
        String sql = "select b_item_subgroup.b_item_subgroup_id"
                + ", b_item_subgroup.item_subgroup_description "
                + "from b_item_subgroup "
                + "where b_item_subgroup.item_subgroup_active = '1' "
                + "and b_item_subgroup.b_item_subgroup_id not in (select b_item_subgroup_id from b_map_item_subgroup_check_calculate) "
                + "and UPPER(b_item_subgroup.item_subgroup_description) like UPPER('%s') order by b_item_subgroup.item_subgroup_description";
        return theConnectionInf.eComplexQuery(String.format(sql, keyword == null || keyword.isEmpty() ? "%" : ("%" + Gutil.CheckReservedWords(keyword) + "%")));
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.utility;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class StringUtil {

    public static String getStringFromDouble(Double d) {
        return (d.toString().lastIndexOf(".0") == -1
                || Integer.parseInt(d.toString().substring(d.toString().indexOf('.') + 1)) > 0
                ? d.toString() : d.toString().substring(0, d.toString().indexOf('.')));

    }

    public static Map<String, String> toMap(String mapAsString) {
        if (mapAsString == null || mapAsString.isEmpty() || mapAsString.equalsIgnoreCase("{}")) {
            return new HashMap<>();
        }
        Map<String, String> map = Arrays.stream(mapAsString.substring(1, mapAsString.length() - 1).split(","))
                .map(entry -> entry.trim().split("="))
                .collect(Collectors.toMap(entry -> entry[0], entry -> entry[1]));
        return map;
    }

    public static String fromMap(Map<?, ?> map) {
        String mapAsString = map.keySet().stream()
                .map(key -> key + "=" + map.get(key))
                .collect(Collectors.joining(", ", "{", "}"));
        return mapAsString;
    }
}

CREATE TABLE IF NOT EXISTS f_patient_family_history
(
	f_patient_family_history_id		integer ,
	description                     text ,
	CONSTRAINT f_patient_family_history_pk PRIMARY KEY (f_patient_family_history_id)
);
INSERT INTO f_patient_family_history VALUES (1 ,'เบาหวาน')
,(2,'ความดัน')
,(3,'โรคติดต่อ')
,(4,'มีประวัติไขมันสูงในครอบครัวสายตรง (Familial hyperlipidemia)')
,(5,'ญาติสายตรง(พ่อ แม่ พี่น้อง)เป็นโรคหลอดเลือดหัวใจตีบก่อนอายุ 55 ปี');

ALTER TABLE t_patient_family_history RENAME TO t_patient_family_history_backup;

CREATE TABLE IF NOT EXISTS t_patient_family_history 
(
	t_patient_id                character varying(50) NOT NULL,
        f_patient_family_historys           jsonb,
        patient_family_history_other       text,
	record_datetime 		timestamp without time zone NOT NULL default current_timestamp,
	user_record_id            character varying(50) NOT NULL,
	update_datetime 	   timestamp without time zone NOT NULL default current_timestamp,
	user_update_id 			 character varying(50) NOT NULL,
	
	CONSTRAINT t_patient_family_history_pk PRIMARY KEY (t_patient_id),  
	CONSTRAINT t_patient_family_history_patient_fk FOREIGN KEY (t_patient_id) 
		REFERENCES t_patient (t_patient_id) ON UPDATE NO ACTION ON DELETE NO ACTION
);

INSERT INTO t_patient_family_history 
(select 
        t_patient_family_history_backup.t_patient_id
        ,min(case when patient_family_history_topic = 'เบาหวาน' then '{"1": "'||patient_family_history_description||'"}' else '{}' end)::jsonb
        ||min(case when patient_family_history_topic = 'ความดัน' then '{"2": "'||patient_family_history_description||'"}' else '{}' end)::jsonb
        ||min(case when patient_family_history_topic = 'โรคติดต่อ' then '{"3": "'||patient_family_history_description||'"}' else '{}' end)::jsonb
        as f_patient_family_historys
        ,min(case when patient_family_history_topic = 'อื่นๆ' then patient_family_history_description end) as patient_family_history_other
        ,text_to_timestamp(patient_family_history_record_date_time) as record_datetime
        ,patient_family_history_staff_record as user_record_id
        ,text_to_timestamp(patient_family_history_record_date_time) as update_datetime
        ,patient_family_history_staff_record as user_update_id
from t_patient_family_history_backup inner join t_patient on t_patient_family_history_backup.t_patient_id = t_patient.t_patient_id 
group by
        t_patient_family_history_backup.t_patient_id
        ,record_datetime
        ,user_record_id
        ,update_datetime
        ,user_update_id);

DROP TABLE t_patient_family_history_backup;

-- update db version
INSERT INTO s_version VALUES ('9701000000081', '81', 'Hospital OS, Community Edition', '3.9.48', '3.30.0', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_48.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.48');
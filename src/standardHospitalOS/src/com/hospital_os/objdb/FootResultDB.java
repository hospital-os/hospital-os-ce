/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.FootResult;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class FootResultDB {

    public ConnectionInf theConnectionInf;
    final public String idtable = "993";

    public FootResultDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public int insert(FootResult o) throws Exception {
        o.generateOID(idtable);
        String sql = "INSERT INTO t_foot_result( "
                + "t_foot_result_id, t_visit_id, f_foot_screen_result_id, appoint_date, "
                + "assessor, assessor_date, active, record_date_time, user_record_id,  "
                + "update_date_time, user_update_id) "
                + "VALUES ('%s', '%s', '%s', '%s',  "
                + "'%s', '%s', '%s', '%s', '%s',  "
                + "'%s', '%s')";
        Object[] values = new Object[]{
            o.getObjectId(), o.t_visit_id, o.f_foot_screen_result_id, o.appoint_date,
            o.assessor, o.assessor_date, o.active, o.record_date_time, o.user_record_id,
            o.update_date_time, o.user_update_id};
        return theConnectionInf.eUpdate(String.format(sql, values));
    }

    public int update(FootResult o) throws Exception {
        String sql = "UPDATE t_foot_result "
                + "SET f_foot_screen_result_id='%s',  "
                + "appoint_date='%s', assessor='%s', assessor_date='%s', "
                + "update_date_time='%s',user_update_id='%s' "
                + "WHERE t_foot_result_id='%s'";
        Object[] values = new Object[]{
            o.f_foot_screen_result_id, o.appoint_date,
            o.assessor, o.assessor_date,
            o.update_date_time, o.user_update_id,
            o.getObjectId()};
        return theConnectionInf.eUpdate(String.format(sql, values));

    }

    public int delete(FootResult o) throws Exception {
        String sql = "UPDATE t_foot_result "
                + "SET active='0',update_date_time='%s',user_update_id='%s' "
                + "WHERE t_foot_result_id='%s'";
        Object[] values = new Object[]{o.update_date_time,
            o.user_update_id, o.getObjectId()};
        return theConnectionInf.eUpdate(String.format(sql, values));
    }

    public FootResult findByVisitId(String visitId) throws Exception {
        String sql = "select * from t_foot_result where t_visit_id = '%s' and active = '1'";
        List<FootResult> list = eQuery(String.format(sql, visitId));
        return list.isEmpty() ? null : list.get(0);
    }

    private List<FootResult> eQuery(String sql) throws Exception {
        List<FootResult> list = new ArrayList<FootResult>();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        while (rs.next()) {
            FootResult o = new FootResult();
            o.setObjectId(rs.getString("t_foot_result_id"));
            o.t_visit_id = rs.getString("t_visit_id");
            o.f_foot_screen_result_id = rs.getString("f_foot_screen_result_id");
            o.appoint_date = rs.getString("appoint_date");
            o.assessor = rs.getString("assessor");
            o.assessor_date = rs.getString("assessor_date");
            o.active = rs.getString("active");
            o.record_date_time = rs.getString("record_date_time");
            o.user_record_id = rs.getString("user_record_id");
            o.update_date_time = rs.getString("update_date_time");
            o.user_update_id = rs.getString("user_update_id");
            list.add(o);
        }
        rs.close();
        return list;
    }
}

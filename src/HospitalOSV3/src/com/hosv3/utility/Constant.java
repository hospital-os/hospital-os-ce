package com.hosv3.utility;

import com.hospital_os.utility.IOStream;
import com.language.utility.language.Language;
import java.awt.Component;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <b>Title:</b>
 * Constant<br> <b>Description:</b><blockquote> Contain constant in program
 * </blockquote> <b>Copyright:</b> Copyright (c) 2002<br> <b>Company:</b> 4th
 * Tier<br>
 *
 * @author Surachai Thowong
 * @not deprecated ����ͧ��ѧ�ѹ������ǹ���������
 * @version 1.0 2002-01-17
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class Constant extends com.hospital_os.utility.Constant {

    private static Properties data = Language.getProperties("hospital_os", Config.LANGUAGE_FILE, Config.LANGUAGE_PATH);
    private static String old_str;
    public static String STR_COMPLETE = Constant.getTextBundle("�������");
    public static String STR_ERROR = Constant.getTextBundle("�Դ��Ҵ");
    private static DecimalFormat formatter = new DecimalFormat("###0.00");
    private static DecimalFormat formatter2 = new DecimalFormat("#,##0.00");

    /*
     * -1 fail format 0 fail pattern 1 right pattern
     */
    public static int isCorrectPID(String pid) {
        int a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13;
        if (pid.length() != 13) {
            return -1;
        }
        a13 = Integer.parseInt(pid.substring(0, 1));
        a12 = Integer.parseInt(pid.substring(1, 2));
        a11 = Integer.parseInt(pid.substring(2, 3));
        a10 = Integer.parseInt(pid.substring(3, 4));
        a9 = Integer.parseInt(pid.substring(4, 5));
        a8 = Integer.parseInt(pid.substring(5, 6));
        a7 = Integer.parseInt(pid.substring(6, 7));
        a6 = Integer.parseInt(pid.substring(7, 8));
        a5 = Integer.parseInt(pid.substring(8, 9));
        a4 = Integer.parseInt(pid.substring(9, 10));
        a3 = Integer.parseInt(pid.substring(10, 11));
        a2 = Integer.parseInt(pid.substring(11, 12));
        a1 = Integer.parseInt(pid.substring(12, 13));

        int sum = (13 * a13) + (12 * a12) + (11 * a11) + (10 * a10) + (9 * a9) + (8 * a8) + (7 * a7) + (6 * a6) + (5 * a5) + (4 * a4) + (3 * a3) + (2 * a2);
        int values = ((11 - (sum % 11)) % 10);
        if (values == a1) {
            return 1;
        } else {
            return 0;
        }
    }

    public static String getTextBundleConfig(String str) {
        if (str.trim().isEmpty()) {
            return "";
        }
        try {
            return java.util.ResourceBundle.getBundle("com/hosv3/property/Config").getString(str);
        } catch (Exception e) {
            // com.hospital_os.utility.Constant.println(str + ":Not Found ");
            return str;
        }
    }

    public static String getStringSplitPipeAnd(String data, int datareturn) {
        String spiltPipe[] = data.split("&", 2);
        return spiltPipe[datareturn];
    }

    public static String getTextBundle(String str) {
        if (str == null || str.trim().isEmpty()) {
            return null;
        }
        try {
            String ret = data.getProperty(str.trim());
            if (ret != null) {
                return ret;
            }

//            printlnFile(str);
//            str = "NotFnd";
            return str;

        } catch (Exception e) {
//            printlnFile(str);
//            str = "NotFnd";
            return str;
        }
    }

    public static String calculateBMI(String weight, String hight) {
        float bmi = 0;
        if (!weight.isEmpty() && !hight.isEmpty()) {
            float w = Float.parseFloat(weight);
            float h = Float.parseFloat(hight);
            bmi = (w * 10000) / (h * h);
        }
        return doubleToDBString(bmi);
    }

    public static int calculateAgeGroup(Map<String, Integer> age) {
        int year = age.get("year");
        int month = age.get("month");
        int day = age.get("day");
        if (year == 0) {
            if (month == 0) {
                if (day <= 3) {
                    return 1;
                } else {
                    return 2;
                }
            } else {
                if (month == 1) {
                    return 3;
                } else {
                    return 4;
                }
            }
        } else {
            if (year <= 2) {
                return 5;
            } else if (year <= 5) {
                return 6;
            } else if (year <= 7) {
                return 7;
            } else if (year <= 9) {
                return 8;
            } else if (year <= 15) {
                return 9;
            } else {
                return 10;
            }
        }
    }

    public static int calRRscore(String RRString, int ageGroup) {
        int[] RRmaxArray = new int[]{80, 80, 75, 65, 65, 48, 45, 45, 30, 24};
        int[] RRmiduArray = new int[]{70, 70, 65, 55, 55, 38, 35, 35, 20, 20};
        int[] RRmidlArray = new int[]{34, 34, 29, 24, 24, 14, 12, 12, 10, 11};

        int RRmax = RRmaxArray[(ageGroup < RRmaxArray.length ? ageGroup : RRmaxArray.length) - 1];
        int RRmidu = RRmiduArray[(ageGroup < RRmiduArray.length ? ageGroup : RRmiduArray.length) - 1];
        int RRmidl = RRmidlArray[(ageGroup < RRmidlArray.length ? ageGroup : RRmidlArray.length) - 1];
        String RRmin = (ageGroup == 10 ? "8" : "FALSE");

        if (RRString == null || RRString.isEmpty()) {
            return 0;
        } else {
            Double RR = Double.parseDouble(RRString);
            if (ageGroup == 10) {
                if (RRmin.equals("FALSE") || RR <= Integer.parseInt(RRmin)) {
                    return 3;
                } else if (RR <= RRmidl) {
                    return 1;
                } else if (RR <= RRmidu) {
                    return 0;
                } else if (RR <= RRmax) {
                    return 2;
                } else {
                    return 3;
                }
            } else {
                if (RR < RRmidl) {
                    return 3;
                } else if (RR < RRmidu) {
                    return 0;
                } else if (RR <= RRmax) {
                    return 2;
                } else {
                    return 3;
                }
            }
        }
    }

    public static int calHRscore(String HRString, int ageGroup) {
        int[] HRmaxArray = new int[]{190, 180, 150, 150, 140, 140, 125, 125, 115, 130};
        int[] HRmiduArray = new int[]{180, 170, 140, 140, 130, 130, 115, 115, 105, 110};
        int[] HRmidlArray = new int[]{100, 100, 80, 80, 70, 65, 60, 60, 55, 50};

        int HRmax = HRmaxArray[(ageGroup < HRmaxArray.length ? ageGroup : HRmaxArray.length) - 1];
        int HRmidu = HRmiduArray[(ageGroup < HRmiduArray.length ? ageGroup : HRmiduArray.length) - 1];
        String HRmid = (ageGroup == 10 ? "90" : "FALSE");
        int HRmidl = HRmidlArray[(ageGroup < HRmidlArray.length ? ageGroup : HRmidlArray.length) - 1];
        String HRmin = (ageGroup == 10 ? "40" : "FALSE");

        if (HRString == null || HRString.isEmpty()) {
            return 0;
        } else {
            Double HR = Double.parseDouble(HRString);
            if (ageGroup == 10) {
                if (HRmin.equals("FALSE") || HR <= Integer.parseInt(HRmin)) {
                    return 3;
                } else if (HR <= HRmidl) {
                    return 1;
                } else if (HRmid.equals("FALSE") || HR <= Integer.parseInt(HRmid)) {
                    return 0;
                } else if (HR <= HRmidu) {
                    return 1;
                } else if (HR <= HRmax) {
                    return 2;
                } else {
                    return 3;
                }
            } else {
                if (HR < HRmidl) {
                    return 3;
                } else if (HR < HRmidu) {
                    return 0;
                } else if (HR <= HRmax) {
                    return 2;
                } else {
                    return 3;
                }
            }
        }
    }

    public static int calO2supScore(String oxygenString, int ageGroup) {
        Double oxygen = Double.parseDouble(oxygenString);
        if (ageGroup == 10) {
            if (oxygen == 0) {
                return 0;
            } else {
                return 2;
            }
        } else {
            if (oxygen <= 2) {
                return 0;
            } else if (oxygen <= 5) {
                return 1;
            } else if (oxygen <= 7) {
                return 2;
            } else {
                return 3;
            }
        }
    }

    public static int calBTscore(String BTString, int ageGroup) {
        if (BTString == null || BTString.isEmpty()) {
            return 0;
        } else {
            Double BT = Double.parseDouble(BTString);
            if (ageGroup == 10) {
                if (BT <= 35) {
                    return 3;
                } else if (BT <= 36) {
                    return 1;
                } else if (BT <= 38) {
                    return 0;
                } else if (BT <= 39) {
                    return 1;
                } else {
                    return 2;
                }
            } else {
                return 0;
            }
        }
    }

    public static String calBPscore(String BPString, int ageGroup) {
        int[] BPminArray = new int[]{60, 70, 70, 70, 72, 76, 82, 86, 90, 90};
        int BPmin = BPminArray[(ageGroup < BPminArray.length ? ageGroup : BPminArray.length) - 1];

        if (BPString == null || BPString.isEmpty()) {
            return "0";
        } else {
            Double BP = Double.parseDouble(BPString);
            if (ageGroup == 10) {
                if (BP < 90) {
                    return "3";
                } else if (BP <= 100) {
                    return "2";
                } else if (BP <= 110) {
                    return "1";
                } else if (BP <= 219) {
                    return "0";
                } else {
                    return "3";
                }
            } else {
                if (BP <= BPmin) {
                    return "ABNORMAL";
                } else {
                    return "NORMAL";
                }
            }
        }
    }

    public static int calO2satScore(String o2satString, int ageGroup) {
        if (o2satString == null || o2satString.isEmpty()) {
            return 0;
        } else {
            Double o2sat = Double.parseDouble(o2satString);
            if (ageGroup == 10) {
                if (o2sat <= 91) {
                    return 3;
                } else if (o2sat <= 93) {
                    return 2;
                } else if (o2sat <= 95) {
                    return 1;
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        }
    }

    public static void setEnabled(Component c, boolean b) {
        if (c.isEnabled()) {
            c.setEnabled(b);
        }
    }

    public static boolean checkModulePrinting() {
        try {
            Class.forName("com.printing.gui.PrintingFrm");
            return true;
        } catch (ClassNotFoundException ex) {
            return false;
        }
    }

    public static String convertSQLToMySQL(String sql, String typeDatabase) {
        if (typeDatabase.equalsIgnoreCase("2")) {
            return IOStream.Unicode2ASCII(sql);

        } else {
            return sql;
        }
    }

    public static String getTextBundleImage(String str) {
        if (str.trim().isEmpty()) {
            return "";
        }
        try {
            return java.util.ResourceBundle.getBundle("com/hospital_os/property/image").getString(str);
        } catch (Exception e) {
            // com.hospital_os.utility.Constant.println(str + ":Not Found ");
            return str;
        }
    }

    /*
     * ��ͧ��þ�������ѡ��㹪�ͧ��ҧ�������Թ�ӹǹ����˹�
     */
    public static void filterTextKey(javax.swing.JTextField jtf, int num) {
        if (jtf.getText().length() > num) {
            jtf.setText(jtf.getText().substring(1, num + 1));
        }
        //if(jtf.getText().length()==num)
        //  jtf.transferFocus();
    }

    /**
     * ��ӹǳ�ҷ�ȹ��� 2 ���˹� ���ա�ûѴ�� 1 �ҷ �����ȹ��������ҧ
     * 0.01-0.99
     *
     * @param num
     * @return
     */
    public static String dicimalCalMoney(String num) {
        int dic = 2;
        if (num == null) {
            return "0";
        }
        try {
            Double.parseDouble(num);
        } catch (Exception ex) {
            return "0";
        }
        //��ȹ�����ͧ�ҡ���� 2 ���˹�
        if (dic < 1) {
            dic = 2;
        }

        java.text.DecimalFormat d = new java.text.DecimalFormat();
        String dicimals = "";
        int mul = 1;

        for (int i = 0; i < dic; i++) {
            dicimals += "0";
            mul *= 10;
        }
        d.applyPattern(dicimals);

        String re = String.valueOf(num);
        String fon;
        //��Ǩ�ͺ����鹨ӹǹ������ͷ�ʹ���
        try {
            fon = re.substring(0, re.indexOf('.'));
        } catch (Exception ex) {
            re += ".0000";
            fon = re.substring(0, re.indexOf('.'));
        }
        //   com.hospital_os.utility.Constant.println(re);
        //�Ӣ�������ѧ�ش��ʹ���
        re = re.substring(re.indexOf('.') + 1) + "0000";

        int nu = 0;
        //��Ǩ�ͺ��Ҥ�����ǹ���ҡ���� 2 �������
        if (re.length() > 2) {
            //�ӷ�ʹ������˹觷�� 3 ����
            String sec = re.substring(dic, dic + 1);
            nu = Integer.parseInt(sec);
        }
        //�ӷ�ʹ������˹觷�� 1��� 2 ����������;Ԩ�ó�
        int un = Integer.parseInt(re.substring(0, dic));
        //��ҵ��˹觷�� 3 �դ���ҡ���� 5 ���Ѵ���
        //��ҹ��¡��ҡ���餧���

        if (nu >= 5) {
            un += 1;
        }
        String ddd;

        if (un < 0.1 * mul) {
            ddd = String.valueOf(0.0 * mul);
        } else {
            ddd = String.valueOf(1.00 * mul);
        }
        un = Integer.parseInt(ddd.substring(0, ddd.indexOf('.')));

        if (un >= 1.0 * mul) {
            int f = Integer.parseInt(fon);
            f += 1;
            fon = String.valueOf(f);
            un = 0;
        }
        return fon + "." + d.format(un);
    }

    /**
     * �ӹǳ੾���Ҥ��ѧ��Ѻ��äԴ�Թ�����
     *
     * @param num
     * @return
     */
    public static String dicimalMoney(String num) {
        if (num == null) {
            return "0";
        }

        try {
            Double.parseDouble(num);
        } catch (Exception ex) {
            return "0";
        }

        java.text.DecimalFormat d = new java.text.DecimalFormat();
        d.applyPattern("00");

        String re = String.valueOf(num);
        String fon;
        //��Ǩ�ͺ����鹨ӹǹ������ͷ�ʹ���
        try {
            fon = re.substring(0, re.indexOf('.'));
        } catch (Exception ex) {
            re += ".0000";
            fon = re.substring(0, re.indexOf('.'));
        }
        //�Ӣ�������ѧ�ش��ʹ���
        re = re.substring(re.indexOf('.') + 1) + "0000";

        int nu = 0;
        //��Ǩ�ͺ��Ҥ�����ǹ���ҡ���� 2 �������
        if (re.length() > 2) {
            //�ӷ�ʹ������˹觷�� 3 ����
            String sec = re.substring(2, 3);
            nu = Integer.parseInt(sec);
        }
        //com.hospital_os.utility.Constant.println(re);
        //�ӷ�ʹ������˹觷�� 1��� 2 ����������;Ԩ�ó�
        int un = Integer.parseInt(re.substring(0, 2));
        //  com.hospital_os.utility.Constant.println(nu);
        //   com.hospital_os.utility.Constant.println(un);
        //��ҵ��˹觷�� 3 �դ���ҡ���� 5 ���Ѵ���
        //��ҹ��¡��ҡ���餧���
        if (nu >= 5) {
            un += 1;
        }

        //��Ǩ�ͺ����ҡ���� 100 �������Ҷ���ҡ���� ���Ѵ���
        //�¡�úǡ�ҡ���˹�Ҩش
        if (un > 100) {
            int f = Integer.parseInt(fon);
            f += 1;
            fon = String.valueOf(f);
            un = 0;
        }
        return fon + "." + d.format(un);
    }

    public static String dicimalNotrim(String num) {
        try {
            Double.parseDouble(num);
        } catch (Exception ex) {
            return "0";
        }

        String decimal;
        decimal = num.substring(num.indexOf('.') + 1);
        int deci = decimal.length();
        for (int i = deci - 1; i >= 0; i--) {
        }
        return "";
    }

    public static String generateOid(String table_id, String hos_id) throws Exception {
        if (table_id.length() != 3) {
            throw new Exception("table_id must be 3 digits:" + table_id);
        }
        if (hos_id.length() != 5) {
            throw new Exception("hospital_id must be 5 digits:" + table_id);
        }
        Integer.parseInt(table_id);
        Integer.parseInt(hos_id);
        String id = table_id + hos_id + System.currentTimeMillis();
        return id;
    }

    /**
     * @deprecate henbe unused
     *
     * @param str
     */
    public static void printlnFile(String str) {
        FileOutputStream fos = null;
        try {
            if (str.equals(old_str)) {
                return;
            }
            str += "\n";
            fos = new FileOutputStream("bundle.txt", true);
            fos.write(str.getBytes());
            old_str = str;
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                LOG.log(Level.SEVERE, ex.getMessage(), ex);
            }
        }
    }
//    public static void main(String[] args){
//        printlnFile("test");
//        printlnFile("henbe test");
//    }

    /*
     * @author Pongtorn (Henbe) �ŧ string �� double ����Ҩҡ˹�� xray
     * �������˹�� gui ����ͧ�� try catch
     */
    public static double toDouble(String value) {
        double dvalue = 0.0;
        try {
            dvalue = Double.parseDouble(value.trim());
        } catch (Exception ex) {
        }
        return dvalue;
    }

    public static void catchException(Exception ex) {
        LOG.log(Level.SEVERE, ex.getMessage(), ex);
    }
    private static final Logger LOG = Logger.getLogger(Constant.class.getName());
}

package com.hospital_os.object;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.Persistent;

public class Uom extends Persistent implements CommonInf {

    public String uom_id;
    public String description;
    public String active;
    public String ucum_unit_code;

    @Override
    public String getCode() {
        return this.getObjectId();
    }

    @Override
    public String getName() {
        return this.description;
    }

    @Override
    public String toString() {
        return getName();
    }
}

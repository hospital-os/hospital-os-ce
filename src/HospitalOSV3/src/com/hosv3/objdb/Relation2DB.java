/*
 * Relation2DB.java
 *
 * Created on 18 ���Ҥ� 2548, 11:19 �.
 */
package com.hosv3.objdb;

/**
 *
 * @author sumo1
 */
import com.hospital_os.objdb.RelationDB;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.Gutil;
import com.hosv3.object.Relation2;
import java.sql.ResultSet;
import java.util.Vector;

@SuppressWarnings("UseOfObsoleteCollectionType")
public class Relation2DB extends RelationDB {

    /**
     * Creates a new instance of Relation2DB
     */
    public Relation2DB(ConnectionInf db) {
        super(db);
    }
    /////////////////////////////////////////////////////////////////

    @Override
    public Vector eQuery(String sql) throws Exception {
        Relation2 p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);

        while (rs.next()) {
            p = new Relation2();
            p.setObjectId(rs.getString(dbObj.pk_field));
            p.description = rs.getString(dbObj.description);
            list.add(p);
        }
        rs.close();
        return list;
    }

    ////////////////////////////////////////////////////////////////////////////     
    public Vector selectByName1(String name) throws Exception {
        String sql = "select * from " + dbObj.table
                + " where UPPER(" + dbObj.description
                + ") like UPPER('%" + Gutil.CheckReservedWords(name) + "%') order by " + dbObj.description;
        return eQuery(sql);
    }

    @Override
    public Vector selectAll() throws Exception {
        String sql = "select * from " + dbObj.table + " order by " + dbObj.description;
        return eQuery(sql);
    }
}

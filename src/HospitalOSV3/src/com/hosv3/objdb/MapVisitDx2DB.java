package com.hosv3.objdb;

import com.hospital_os.objdb.MapVisitDxDB;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.util.List;
import java.util.Vector;

@SuppressWarnings("UseOfObsoleteCollectionType")
public class MapVisitDx2DB extends MapVisitDxDB {

    public MapVisitDx2DB(ConnectionInf db) {
        super(db);
    }

    public Vector selectByVid(String vid) throws Exception {
        String sql = " select * from " + dbObj.table
                + " where " + dbObj.pk_field
                + " like " + vid + "%"
                + " AND " + dbObj.visit_diag_map_active + "='1'";
        return eQuery(sql);
    }

    public int deleteByVid(String vID) throws Exception {
        String sql = "delete from " + dbObj.table
                + " where " + dbObj.visit_id + " like '" + vID + "%'";

        return theConnectionInf.eUpdate(sql);
    }

    public int updateInActiveByVisitMap(String id, String date_time, String eid) throws Exception {
        String sql = "update " + dbObj.table
                + " set " + dbObj.visit_diag_map_active + " = '0'"
                + " , " + dbObj.visit_diag_map_cancel_date_time + " = '" + date_time + "'"
                + " , " + dbObj.visit_diag_map_staff_cancel + " = '" + eid + "'"
                + " where t_visit_diag_map_id = '" + id + "'"
                + " and " + dbObj.visit_diag_map_active + " = '1'";
        return theConnectionInf.eUpdate(sql);
    }

    public String getThaiDxByVisitId(String visitId) throws Exception {
        String sql = "select b_template_dx.template_dx_thaidescription \n"
                + "from t_visit_diag_map\n"
                + "inner join b_template_dx on b_template_dx.b_template_dx_id = t_visit_diag_map.b_template_dx_id\n"
                + "where t_visit_diag_map.t_visit_id = '" + visitId + "' and visit_diag_map_active = '1'";
        List<Object[]> eComplexQuery = theConnectionInf.eComplexQuery(sql);
        return eComplexQuery.isEmpty() ? "" : String.valueOf(eComplexQuery.get(0)[0]);
    }
}

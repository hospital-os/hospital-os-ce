/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.utility;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class CellRendererTextArea implements TableCellRenderer {

    JTextArea textArea;

    public CellRendererTextArea() {
        textArea = new JTextArea();
        textArea.setLineWrap(true);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table,
            Object value,
            boolean isSelected,
            boolean hasFocus,
            int row, int column) {
        String text = (String) value;
        textArea.setText(text);
        String tooltip = "<html><b>" + text.replaceAll("\n", "<br>") + "</b></html>";
        textArea.setToolTipText(tooltip);
        int height = textArea.getLineCount() * 22;
        if (height > table.getRowHeight(row)) {
            table.setRowHeight(row, height);
        }
        if (table != null) {
            if (table.isCellEditable(row, column)) {
                textArea.setForeground(Color.blue);
            } else {
                if (isSelected) {
                    textArea.setBackground(table.getSelectionBackground());
                    textArea.setForeground(table.getSelectionForeground());
                } else {
                    textArea.setBackground(table.getBackground());
                    textArea.setForeground(table.getForeground());;
                }
            }
        }
        return textArea;
    }
}

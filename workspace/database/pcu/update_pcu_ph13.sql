--issues#669
CREATE TABLE IF NOT EXISTS f_health_anc_period (
	f_health_anc_period_id	VARCHAR(1) NOT NULL,
	description				TEXT NOT NULL,
	CONSTRAINT f_health_anc_period_pk PRIMARY KEY (f_health_anc_period_id)
);

INSERT INTO f_health_anc_period (f_health_anc_period_id,description)
VALUES ('0','ไม่อยู่ในช่วงของการฝากครรภ์')
,('1','ครั้งที่ 1 อายุครรภ์ <= 12 สัปดาห์')
,('2','ครั้งที่ 2 อายุครรภ์ 13 - 20 สัปดาห์')
,('3','ครั้งที่ 3 อายุครรภ์ 21 - 26 สัปดาห์')
,('4','ครั้งที่ 4 อายุครรภ์ 27 - 30 สัปดาห์')
,('5','ครั้งที่ 5 อายุครรภ์ 31 - 34 สัปดาห์')
,('6','ครั้งที่ 6 อายุครรภ์ 35 - 36 สัปดาห์')
,('7','ครั้งที่ 7 อายุครรภ์ 37 - 38 สัปดาห์')
,('8','ครั้งที่ 8 อายุครรภ์ 39 - 40 สัปดาห์')
ON CONFLICT (f_health_anc_period_id) 
DO NOTHING;

ALTER TABLE t_health_anc ADD IF NOT EXISTS f_health_anc_period_id VARCHAR(1) DEFAULT NULL;

-- issues#678
-- เก็บข้อมูลบริษัทผู้ผลิต (b_item_manufacture_id) 
ALTER TABLE b_health_epi_group ADD IF NOT EXISTS b_item_manufacturer_id VARCHAR(255) DEFAULT NULL;
-- เก็บข้อมูลตำแหน่งที่ฉีด (code) 
ALTER TABLE b_health_epi_group ADD IF NOT EXISTS act_site_code VARCHAR(50) DEFAULT '0';
-- เก็บข้อมูลวิธีการฉีด (code) 
ALTER TABLE b_health_epi_group ADD IF NOT EXISTS immunization_route_code VARCHAR(50) DEFAULT '0';

-- update db version
INSERT INTO s_health_version VALUES ('9710000000013','13','PCU, Community Edition','1.37.0','1.7.0', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

insert into s_script_update_log values ('pcu','update_pcu_ph13.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'update PCU ph12 -> ph13');
/*
 * OrderSubject.java
 *
 * Created on 26 ���Ҥ� 2546, 22:52 �.
 */
package com.hosv3.subject;

import com.hosv3.usecase.transaction.ManageOrderResp;
import java.util.Vector;

/**
 *
 * @author henbe
 */
@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public class OrderSubject implements ManageOrderResp {

    private Vector vManageOrder;

    public OrderSubject() {
        vManageOrder = new Vector();
    }

    public void removeAttach() {
        vManageOrder.removeAllElements();

    }
    /*
     * attach
     */

    public void attachManageOrder(ManageOrderResp o) {
        vManageOrder.add(o);
    }

    public void attachManageOrder(ManageOrderResp o, int index) {
        vManageOrder.add(index, o);
    }

    public void detach(ManageOrderResp o) {
        vManageOrder.remove(o);
    }
    /*
     * method
     */

    @Override
    public void notifyReceiveReturnDrug(String msg, int status) {
        for (int i = 0, size = vManageOrder.size(); i < size; i++) {
            ((ManageOrderResp) vManageOrder.get(i)).notifyReceiveReturnDrug(msg, status);
        }
    }

    @Override
    public void notifySaveOrderItemInLab(String msg, int status) {
        for (int i = 0, size = vManageOrder.size(); i < size; i++) {
            ((ManageOrderResp) vManageOrder.get(i)).notifySaveOrderItemInLab(msg, status);
        }
    }

    /**
     * @param msg
     * @param status
     * @author padungrat(tong)
     * @date 13/03/49,10:45
     *
     * public void notifySaveOrderItemInXRay(String msg,int status) { for(int
     * i=0,size=vManageOrder.size();i<size;i++)
     * ((ManageOrderResp)vManageOrder.get(i)).notifySaveOrderItemInXRay(msg,status);
     * }
     *
     */
    @Override
    public void notifyCheckAutoOrder(String msg, int status) {
        for (int i = 0, size = vManageOrder.size(); i < size; i++) {
            ((ManageOrderResp) vManageOrder.get(i)).notifyCheckAutoOrder(msg, status);
        }
    }

    @Override
    public void notifyDoctorOffDrug(String msg, int status) {
        for (int i = 0, size = vManageOrder.size(); i < size; i++) {
            ((ManageOrderResp) vManageOrder.get(i)).notifyDoctorOffDrug(msg, status);
        }
    }

    @Override
    public void notifySaveOrderItem(String msg, int status) {
        for (int i = 0, size = vManageOrder.size(); i < size; i++) {
            ((ManageOrderResp) vManageOrder.get(i)).notifySaveOrderItem(msg, status);
        }
    }

    @Override
    public void notifyVerifyOrderItem(String msg, int status) {
        for (int i = 0, size = vManageOrder.size(); i < size; i++) {
            ((ManageOrderResp) vManageOrder.get(i)).notifyVerifyOrderItem(msg, status);
        }
    }

    @Override
    public void notifyCancelOrderItem(String str, int status) {
        for (int i = 0, size = vManageOrder.size(); i < size; i++) {
            ((ManageOrderResp) vManageOrder.get(i)).notifyCancelOrderItem(str, status);
        }
    }

    @Override
    public void notifyDispenseOrderItem(String str, int status) {
        for (int i = 0, size = vManageOrder.size(); i < size; i++) {
            ((ManageOrderResp) vManageOrder.get(i)).notifyDispenseOrderItem(str, status);
        }
    }

    @Override
    public void notifyExecuteOrderItem(String str, int status) {
        for (int i = 0, size = vManageOrder.size(); i < size; i++) {
            ((ManageOrderResp) vManageOrder.get(i)).notifyExecuteOrderItem(str, status);
        }
    }

    @Override
    public void notifyContinueOrderItem(String str, int status) {
        for (int i = 0, size = vManageOrder.size(); i < size; i++) {
            ((ManageOrderResp) vManageOrder.get(i)).notifyContinueOrderItem(str, status);
        }
    }

    @Override
    public void notifyReferOutLab(String msg, int status) {
        for (int i = 0, size = vManageOrder.size(); i < size; i++) {
            ((ManageOrderResp) vManageOrder.get(i)).notifyReferOutLab(msg, status);
        }
    }

    @Override
    public void notifySaveReturnDrug(String str, int status) {
        for (int i = 0, size = vManageOrder.size(); i < size; i++) {
            ((ManageOrderResp) vManageOrder.get(i)).notifySaveReturnDrug(str, status);
        }
    }

    @Override
    public void notifySaveOrderRequest(String str, int status) {
        for (int i = 0, size = vManageOrder.size(); i < size; i++) {
            ((ManageOrderResp) vManageOrder.get(i)).notifySaveOrderRequest(str, status);
        }
    }
}

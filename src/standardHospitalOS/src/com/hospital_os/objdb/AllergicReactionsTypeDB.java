/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.AllergicReactionsType;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.ComboFix;
import java.sql.ResultSet;
import java.util.Vector;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class AllergicReactionsTypeDB {

    private final ConnectionInf theConnectionInf;

    public AllergicReactionsTypeDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public AllergicReactionsType selectById(String id) throws Exception {
        String sql = "select * from f_allergic_reactions_type where f_allergic_reactions_type_id = '%s'";
        Vector eQuery = eQuery(String.format(sql, id));
        return (AllergicReactionsType) (eQuery.isEmpty() ? null : eQuery.get(0));
    }

    public Vector<AllergicReactionsType> listAll() throws Exception {
        String sql = "select * from f_allergic_reactions_type order by f_allergic_reactions_type_id";
        Vector eQuery = eQuery(sql);
        return eQuery;
    }

    public Vector eQuery(String sql) throws Exception {
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        while (rs.next()) {
            AllergicReactionsType p = new AllergicReactionsType();
            p.setObjectId(rs.getString("f_allergic_reactions_type_id"));
            p.description = rs.getString("allergic_reactions_type_description");
            list.add(p);
        }
        rs.close();
        return list;
    }

    public Vector<ComboFix> getComboboxDatasources() throws Exception {
        Vector<ComboFix> list = new Vector<ComboFix>();
        Vector<AllergicReactionsType> listAll = listAll();
        for (AllergicReactionsType obj : listAll) {
            list.add(new ComboFix(obj.getObjectId(), obj.description));
        }
        return list;
    }
}

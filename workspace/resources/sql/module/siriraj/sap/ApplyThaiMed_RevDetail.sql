SELECT  
         t_patient.patient_pid as person_id
        ,t_visit.visit_hn as hn
        ,t_visit.visit_vn as vn
        ,f_patient_prefix.patient_prefix_description ||' '|| t_patient.patient_firstname ||' '|| t_patient.patient_lastname as fname
        ,case when f_item_group.f_item_group_id ='1'
                    then 'M'
                when f_item_group.f_item_group_id ='4'
                    then 'S'
                else 'O' end as drug_type
        ,t_order.order_common_name as drug_name
        ,'' as drug_code
        ,t_order.order_price  AS drug_price
        ,cast(t_order.order_qty as decimal) AS amount
        ,(t_order.order_price * t_order.order_qty) as total_price
        ,t_billing_invoice_item.billing_invoice_item_payer_share as payer_share
        ,t_billing_invoice_item.billing_invoice_item_patient_share as patient_share
        ,b_contract_plans.contract_plans_description as plan
        ,substr(t_billing.billing_billing_date_time,9,2)||substr(t_billing.billing_billing_date_time,6,2)||substr(t_billing.billing_billing_date_time,1,4) as billing_date
        ,case when t_siriraj_medical_bill.t_siriraj_medical_bill_id is not null
                    then t_siriraj_medical_bill.medical_bill_no
                when t_billing_receipt.t_billing_receipt_id is not null
                    then t_billing_receipt.billing_receipt_number
                else '' end as receipt_no 
        , employee_prefix.patient_prefix_description||employee.person_firstname||'  '||employee.person_lastname  as doctor_code

        ,substr(t_order.order_date_time,9,2)||substr(t_order.order_date_time,6,2)||substr(t_order.order_date_time,1,4)||replace(substr(t_order.order_date_time,12),':','')as prescribtion_date

        ,t_order.order_qty  as pack_size
        ,uom_purch.b_item_drug_uom_id as unit_code
        ,uom_purch.item_drug_uom_description as unit
        ,b_map_group_code.f_group_code_id as code
        ,substr(b_nhso_map_drug.f_nhso_drug_id,1,24)  AS drug_std_code
        ,b_item_drug_instruction.b_item_drug_instruction_id as sig_code
        ,case when t_order_drug.order_drug_dose  > 0 
                    then b_item_drug_instruction.item_drug_instruction_description 
                            ||' '||t_order_drug.order_drug_dose
                            ||' '||uom_use.item_drug_uom_description
                     else '' end   as sig_name 

from
        t_billing_receipt inner join t_visit on t_billing_receipt.t_visit_id = t_visit.t_visit_id
        inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
        left join f_patient_prefix on t_patient.f_patient_prefix_id = f_patient_prefix.f_patient_prefix_id
        inner join t_billing on t_billing_receipt.t_visit_id = t_billing.t_visit_id
        inner join t_billing_invoice on t_billing.t_billing_id = t_billing_invoice.t_billing_id
        left join t_siriraj_medical_bill on t_billing_invoice.t_billing_invoice_id = t_siriraj_medical_bill.t_billing_invoice_id

        inner join t_visit_payment on t_billing_invoice.t_payment_id = t_visit_payment.t_visit_payment_id
                        and t_visit_payment.visit_payment_active = '1'
        inner join b_contract_plans on t_visit_payment.b_contract_plans_id =  b_contract_plans.b_contract_plans_id
        inner join b_siriraj_direct_draw_map_plan on b_contract_plans.b_contract_plans_id = b_siriraj_direct_draw_map_plan.b_contract_plans_id


        inner join t_billing_invoice_item on t_billing_invoice_item.t_billing_invoice_id = t_billing_invoice.t_billing_invoice_id
                                    and t_billing_invoice_item.billing_invoice_item_active ='1'
        inner join  t_order on t_order.f_order_status_id not in ('0','3')
                                and t_billing_invoice_item.t_order_item_id = t_order.t_order_id

        inner join f_item_group on t_order.f_item_group_id = f_item_group.f_item_group_id
        left join b_nhso_map_drug on t_order.b_item_id = b_nhso_map_drug.b_item_id
        left join t_order_drug on t_order.t_order_id=t_order_drug.t_order_id
                                        and t_order_drug.order_drug_active = '1'
        left join  b_item_drug_uom as uom_purch on t_order_drug.b_item_drug_uom_id_purch = uom_purch.b_item_drug_uom_id 
        left join  b_item_drug_uom as uom_use on t_order_drug.b_item_drug_uom_id_use = uom_use.b_item_drug_uom_id 

        left join b_item_drug_instruction on t_order_drug.b_item_drug_instruction_id = b_item_drug_instruction.b_item_drug_instruction_id 
                                                and b_item_drug_instruction.item_drug_instruction_active='1'
        left join b_map_group_code on t_billing_invoice_item.b_item_id = b_map_group_code.b_item_id

        left join b_employee on t_order.order_staff_order = b_employee.b_employee_id
        left join t_person  as employee on b_employee.t_person_id = employee.t_person_id
        left join f_patient_prefix as employee_prefix on employee.f_prefix_id = employee_prefix.f_patient_prefix_id


WHERE          
        substr(t_billing_receipt.billing_receipt_date_time,1,10) = ':exportdate'
        and t_billing_receipt.billing_receipt_active='1'
order by 
        t_patient.patient_pid 
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.BDayOff;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class BDayOffDB {

    public ConnectionInf connectionInf;
    final public String tableId = "862";

    public BDayOffDB(ConnectionInf db) {
        connectionInf = db;
    }

    public int insert(BDayOff obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO b_dayoff(\n"
                    + "            b_dayoff_id, dayoff_name, dayoff_date, dayoff_description,\n"
                    + "            user_record,user_modify)\n"
                    + "    VALUES (?, ?, ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.getGenID(tableId));
            preparedStatement.setString(index++, obj.dayoff_name);
            preparedStatement.setDate(index++, new Date(obj.dayoff_date.getTime()));
            preparedStatement.setString(index++, obj.dayoff_description);
            preparedStatement.setString(index++, obj.user_record);
            preparedStatement.setString(index++, obj.user_modify);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(BDayOff obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE b_dayoff\n"
                    + "   SET dayoff_name=?, dayoff_date=?, \n"
                    + "       dayoff_description=?,modify_date_time=current_timestamp, user_modify=?\n"
                    + " WHERE b_dayoff_id=?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.dayoff_name);
            preparedStatement.setDate(index++, new Date(obj.dayoff_date.getTime()));
            preparedStatement.setString(index++, obj.dayoff_description);
            preparedStatement.setString(index++, obj.user_modify);
            preparedStatement.setString(index++, obj.getObjectId());
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(BDayOff obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM b_dayoff\n");
            sql.append(" WHERE b_dayoff_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public BDayOff selectById(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_dayoff\n"
                    + "where b_dayoff_id = ?\n";
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, id);
            List<BDayOff> executeQuery = executeQuery(preparedStatement);
            return executeQuery.isEmpty() ? null : executeQuery.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<BDayOff> listDayOff(boolean isSelectAll, java.util.Date dateStart) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * \n"
                    + "from b_dayoff\n";
            if (!isSelectAll) {
                sql += "where dayoff_date >= ? \n";
            }
            sql += "order by b_dayoff.dayoff_date asc";
            preparedStatement = connectionInf.ePQuery(sql);
            if (!isSelectAll) {
                preparedStatement.setDate(1, new Date(dateStart.getTime()));
            }
            List<BDayOff> list = executeQuery(preparedStatement);
            return list;
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public BDayOff selectByDate(java.util.Date date) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_dayoff where dayoff_date = ? ";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setDate(1, date == null ? null : new Date(date.getTime()));
            List<BDayOff> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<BDayOff> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<BDayOff> list = new ArrayList<BDayOff>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                BDayOff obj = new BDayOff();
                obj.setObjectId(rs.getString("b_dayoff_id"));
                obj.dayoff_date = rs.getDate("dayoff_date");
                obj.dayoff_description = rs.getString("dayoff_description");
                obj.dayoff_name = rs.getString("dayoff_name");
                obj.record_date_time = rs.getTimestamp("record_date_time");
                obj.user_record = rs.getString("user_record");
                obj.modify_date_time = rs.getTimestamp("modify_date_time");
                obj.user_modify = rs.getString("user_modify");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }
}

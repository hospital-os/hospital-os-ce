/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
public class DrugTMT extends Persistent implements CommonInf {

    public String fsn = "";
    public String manufacturer = "";
    public String changedate = "";

    @Override
    public String getCode() {
        return getObjectId();
    }

    @Override
    public String getName() {
        return fsn;
    }

    @Override
    public String toString() {
        return getName();
    }
}

--#79
INSERT INTO public.f_gui_action (f_gui_action_id, gui_action_name, gui_action_note) VALUES('2710', 'ยกเลิกรายการที่ไม่ได้สั่งเอง', NULL);

--#80
ALTER TABLE B_ITEM_DRUG_OTHER_LANGUAGE ADD COLUMN SPECIAL_DOSE text default '';
ALTER TABLE B_ITEM_SUPPLY ADD COLUMN SUPPLEMENT_LABEL text default '';
ALTER TABLE t_order ADD supplement_label text default '';

--API for doctor schedule via google sheet
INSERT INTO b_restful_url (b_restful_url_id, restful_code, restful_url, user_update_id, is_system)
VALUES  ('999' || (select b_visit_office_id from b_site) || 'doctor.schedule.ggsheet','doctor.schedule.ggsheet','https://script.google.com/macros/s/[YOUR_APP_ID]/exec?path=/[YOUR_API_PATH]',(select rpad('157'||b_visit_office_id,18,'0')  from b_site), true);

update t_patient_appointment set patient_appointment_doctor = null where patient_appointment_doctor = '' or lower(patient_appointment_doctor) = 'null';
-- update db version
INSERT INTO s_version VALUES ('9701000000096', '96', 'Hospital OS, Community Edition', '3.9.62', '3.42.0', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_62.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.62');
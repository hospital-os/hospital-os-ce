/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.PaymentType;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class PaymentTypeDB {

    private final ConnectionInf connectionInf;

    public PaymentTypeDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public PaymentType select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_payment_type where f_payment_type_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<PaymentType> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<PaymentType> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_payment_type order by f_payment_type_id";
            preparedStatement = connectionInf.ePQuery(sql);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<PaymentType> list(String... ids) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_payment_type where f_payment_type_id in (%s) order by f_payment_type_id";
            StringBuilder sb = new StringBuilder();
            for (String id : ids) {
                sb.append("'").append(id).append("',");
            }
            preparedStatement = connectionInf.ePQuery(String.format(sql,
                    sb.toString().substring(0, sb.toString().length() - 1)));
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<PaymentType> listByKeyword(String keyword) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_payment_type where upper(description) like upper(?) order by description";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, "%" + (keyword == null || keyword.isEmpty() ? "" : keyword) + "%");
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<PaymentType> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<PaymentType> list = new ArrayList<PaymentType>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                PaymentType obj = new PaymentType();
                obj.setObjectId(rs.getString("f_payment_type_id"));
                obj.description = rs.getString("description");
                obj.require_bank_info = rs.getString("require_bank_info");
                obj.require_account_name = rs.getString("require_account_name");
                obj.require_account_no = rs.getString("require_account_no");
                obj.require_card_type = rs.getString("require_card_type");
                obj.require_payment_date = rs.getString("require_payment_date");
                obj.lbl_bank_info = rs.getString("lbl_bank_info");
                obj.lbl_account_name = rs.getString("lbl_account_name");
                obj.lbl_account_no = rs.getString("lbl_account_no");
                obj.lbl_card_type = rs.getString("lbl_card_type");
                obj.lbl_payment_date = rs.getString("lbl_payment_date");
                obj.limit_account_number = rs.getInt("limit_account_number");
                obj.is_auto_check_bank = rs.getString("is_auto_check_bank");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (PaymentType obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }
    
    public List<CommonInf> getComboboxDatasource(String... ids) throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (PaymentType obj : list(ids)) {
            list.add((CommonInf) obj);
        }
        return list;
    }
}

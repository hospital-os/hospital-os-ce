package com.hospital_os.objdb;
import com.hospital_os.usecase.connection.*;
import com.hospital_os.utility.*;
import java.util.*;
import java.sql.*;
import com.hospital_os.object.HALavel;

@SuppressWarnings("UseOfObsoleteCollectionType")
public class HALavelDB
{
    public ConnectionInf theConnectionInf;
    public HALavel dbObj;
    final public String idtable = "255";
    /**
     * @param ConnectionInf db
     * @roseuid 3F65897F0326
     */
    public HALavelDB(ConnectionInf db)
    {
        theConnectionInf=db;
        dbObj = new HALavel();
        initConfig();
    }

    private boolean initConfig()
    {
        dbObj.table="f_ha_level";
        dbObj.pk_field="f_ha_level_id";
        dbObj.description="ha_level_description";
        return true;
    }

    /**
     * @param cmd
     * @param o
     * @return int
     * @roseuid 3F6574DE0394
     */
    public int insert(HALavel o) throws Exception
    {
        String sql="";
        HALavel p = o;
        p.generateOID(idtable);
        sql="insert into " + dbObj.table + " ("
        + dbObj.pk_field
        + " ,"	+ dbObj.description
        + " ) values ('"
        + p.getObjectId()
        + "','" + p.description
        + "')";
        sql = Gutil.convertSQLToMySQL(sql,theConnectionInf.gettypeDatabase());
        return theConnectionInf.eUpdate(sql);
    }

    public int update(HALavel o) throws Exception
    {
        String sql="update " + dbObj.table + " set ";
        HALavel p=o;
        String field =""
        + "', " + dbObj.description + "='" + p.description
        + "' where " + dbObj.pk_field + "='" + p.getObjectId() +"'";
        sql = Gutil.convertSQLToMySQL(sql+field.substring(2),theConnectionInf.gettypeDatabase());
        return theConnectionInf.eUpdate(sql);
    }

    public int delete(HALavel o) throws Exception
    {
        String sql="delete from " + dbObj.table
        + " where " + dbObj.pk_field + "='" + o.getObjectId() +"'";
        return theConnectionInf.eUpdate(sql);
    }

    public HALavel selectByPK(String pk) throws Exception
    {
        String sql="select * from " + dbObj.table
        + " where " + dbObj.pk_field
        + " = '" + pk + "'";

        Vector v=eQuery(sql);
        if(v.isEmpty())
            return null;
        else
            return (HALavel)v.get(0);
    }

    public Vector selectAllCombofix() throws Exception
    {
        Vector v = new Vector();
        String sql ="select * from " + dbObj.table
        + " order by " + dbObj.description;
        v = eQuery(sql);
        if(v.isEmpty())
            return null;
        else
            return v;
    }

    public Vector selectAll() throws Exception
    {
        Vector v = new Vector();
        String sql ="select * from " + dbObj.table
        + " order by " + dbObj.pk_field;
        v = eQuery(sql);
        if(v.isEmpty())
            return null;
        else
            return v;
    }

    public Vector selectOld() throws Exception
    {
        Vector v = new Vector();
        String sql ="select * from " + dbObj.table
        + " order by " + dbObj.pk_field;
        v = eQuery(sql);
        if(v.isEmpty())
            return null;
        else
            return v;
    }

    public Vector eQuery(String sql) throws Exception
    {
        HALavel p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while(rs.next())
        {
            p = new HALavel();
            p.setObjectId(rs.getString(dbObj.pk_field));
            p.description = rs.getString(dbObj.description);
            list.add(p);
        }
        rs.close();
        return list;
    }

}

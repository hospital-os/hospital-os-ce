/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
public class FootArtery extends Persistent {

    public String t_visit_id = "";
    public String left_dorsalis_pedis = "";
    public String right_dorsalis_pedis = "";
    public String left_posterior_tibial = "";
    public String right_posterior_tibial = "";
    public String left_gangrene = "";
    public String right_gangrene = "";
    public String left_sensory = "";
    public String left_sensory_count = "";
    public String right_sensory = "";
    public String right_sensory_count = "";
    public String wound = "";
    public String wound_position = "";
    public String wound_area = "";
    public String wound_size = "";
    public String active = "1";
    public String record_date_time = "";
    public String user_record_id = "";
    public String update_date_time = "";
    public String user_update_id = "";
}

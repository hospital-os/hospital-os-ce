package com.hospital_os.objdb;

import com.hospital_os.object.PatientDrugAllergy;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.Gutil;
import java.sql.ResultSet;
import java.util.Vector;

@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public class PatientDrugAllergyDB {

    protected final ConnectionInf theConnectionInf;
    private final String idtable = "148";

    public PatientDrugAllergyDB(ConnectionInf db) {
        theConnectionInf = db;
    }
    /*
     * ////////////////////////////////////////////////////////////////////////////////////
     */

    public int updatePatientByPatient(String old_id, String new_id) throws Exception {
        String sql = "Update t_patient_drug_allergy set t_patient_id = '" + new_id + "'"
                + ",drug_allergy_symtom = drug_allergy_symtom ||'-'||'" + old_id + "'"
                + " where t_patient_id = '" + old_id + "'";
        return theConnectionInf.eUpdate(sql);
    }

    public int insert(PatientDrugAllergy o) throws Exception {
        String sql = "INSERT INTO t_patient_drug_allergy( "
                + "t_patient_drug_allergy_id, t_patient_id, b_item_drug_standard_id, "
                + " f_allergy_type_id, drug_allergy_symtom_date, drug_allergy_symtom,  "
                + "f_naranjo_interpretation_id, drug_allergy_note, f_allergy_warning_type_id, "
                + "drug_allergy_report_date, pharma_assess_id, doctor_diag_id,  "
                + "f_allergy_level_id, f_allergy_informant_id, allergy_informant_hospital_id, f_allergy_icd10_id, user_record,  "
                + "record_date_time, user_modify, modify_date_time, active) "
                + "VALUES ('%s', '%s', '%s', "
                + "'%s', '%s', '%s', "
                + "'%s', '%s', '%s', "
                + "'%s', '%s', '%s', "
                + "'%s', '%s', '%s', '%s', '%s', "
                + "'%s', '%s', '%s', '%s')";
        sql = String.format(sql,
                o.getObjectId() == null ? o.getGenID(idtable) : o.getObjectId(), o.t_patient_id, o.b_item_drug_standard_id,
                o.f_allergy_type_id, o.drug_allergy_symtom_date, Gutil.CheckReservedWords(o.drug_allergy_symtom),
                o.f_naranjo_interpretation_id, Gutil.CheckReservedWords(o.drug_allergy_note), o.f_allergy_warning_type_id,
                o.drug_allergy_report_date, o.pharma_assess_id, o.doctor_diag_id,
                o.f_allergy_level_id, o.f_allergy_informant_id, o.allergy_informant_hospital_id, o.f_allergy_icd10_id, o.user_record,
                o.record_date_time, o.user_modify, o.modify_date_time, "1");
        return theConnectionInf.eUpdate(sql);
    }

    public int update(PatientDrugAllergy o) throws Exception {
        String sql = "UPDATE t_patient_drug_allergy "
                + "SET b_item_drug_standard_id='%s',  "
                + "f_allergy_type_id='%s', drug_allergy_symtom_date='%s', drug_allergy_symtom='%s',  "
                + "f_naranjo_interpretation_id='%s', drug_allergy_note='%s', f_allergy_warning_type_id='%s',  "
                + "drug_allergy_report_date='%s', pharma_assess_id='%s', doctor_diag_id='%s',  "
                + "f_allergy_level_id='%s', f_allergy_informant_id='%s', allergy_informant_hospital_id='%s',  "
                + "f_allergy_icd10_id='%s', user_modify='%s', modify_date_time='%s' "
                + "WHERE t_patient_drug_allergy_id ='%s'";
        sql = String.format(sql,
                o.b_item_drug_standard_id,
                o.f_allergy_type_id, o.drug_allergy_symtom_date, Gutil.CheckReservedWords(o.drug_allergy_symtom),
                o.f_naranjo_interpretation_id, Gutil.CheckReservedWords(o.drug_allergy_note), o.f_allergy_warning_type_id,
                o.drug_allergy_report_date, o.pharma_assess_id, o.doctor_diag_id,
                o.f_allergy_level_id, o.f_allergy_informant_id, o.allergy_informant_hospital_id,
                o.f_allergy_icd10_id, o.user_modify, o.modify_date_time, o.getObjectId());
        return theConnectionInf.eUpdate(sql);
    }

    public int delete(PatientDrugAllergy o) throws Exception {
        String sql = "UPDATE t_patient_drug_allergy "
                + "SET user_modify='%s', modify_date_time='%s', active ='%s' "
                + "WHERE t_patient_drug_allergy_id ='%s'";
        sql = String.format(sql,
                o.user_modify, o.modify_date_time, "0", o.getObjectId());
        return theConnectionInf.eUpdate(sql);
    }

    public Vector selectByPatientId(String patient_id) throws Exception {
        String sql = "select t_patient_drug_allergy.*"
                + ", b_item_drug_standard.item_drug_standard_description "
                + ", f_naranjo_interpretation.naranjo_interpretation_detail "
                + ", f_allergy_warning_type.warning_type_description "
                + "from t_patient_drug_allergy "
                + "inner join b_item_drug_standard on b_item_drug_standard.b_item_drug_standard_id = t_patient_drug_allergy.b_item_drug_standard_id "
                + "inner join f_naranjo_interpretation on f_naranjo_interpretation.f_naranjo_interpretation_id = t_patient_drug_allergy.f_naranjo_interpretation_id "
                + "inner join f_allergy_warning_type on f_allergy_warning_type.f_allergy_warning_type_id = t_patient_drug_allergy.f_allergy_warning_type_id "
                + "where t_patient_id = '" + patient_id + "' and t_patient_drug_allergy.active = '1' "
                + "order by b_item_drug_standard.item_drug_standard_description ";
        return eQuery(sql);
    }

    public Vector seclectUnImported(String pharmaAssessId) throws Exception {
        String sql = "SELECT "
                + "max(t_patient_drug_allergy_id) as t_patient_drug_allergy_id  "
                + ",t_patient_id as t_patient_id  "
                + ",b_item_drug_standard_map_item.b_item_drug_standard_id as b_item_drug_standard_id  "
                + ",'9' as f_allergy_type_id  "
                + ",case when patient_drug_allergy_record_date_time <> '' then substring(patient_drug_allergy_record_date_time,1,10)  "
                + "else (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_date, '-MM-DD')  end as drug_allergy_symtom_date  "
                + ",patient_drug_allergy_symptom as drug_allergy_symtom "
                + ",'3' as f_naranjo_interpretation_id  "
                + ",'' as drug_allergy_note "
                + ",'1' as f_allergy_warning_type_id "
                + ",case when patient_drug_allergy_record_date_time <> '' then substring(patient_drug_allergy_record_date_time,1,10)  "
                + "else (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_date, '-MM-DD')  end  as drug_allergy_report_date  "
                + ",'" + pharmaAssessId + "' as pharma_assess_id  "
                + ",'' as doctor_diag_id  "
                + ",'" + pharmaAssessId + "' as user_record  "
                + ",case when patient_drug_allergy_record_date_time <> '' then patient_drug_allergy_record_date_time "
                + "else (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS') end  as record_date_time  "
                + ",'" + pharmaAssessId + "' as user_modify  "
                + ",case when patient_drug_allergy_record_date_time <> '' then patient_drug_allergy_record_date_time "
                + "else (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS') end  as modify_date_time  "
                + ",'1'as active  "
                + "FROM "
                + "t_patient_drug_allergy_backup "
                + "inner join b_item on b_item.b_item_id = t_patient_drug_allergy_backup.b_item_id "
                + "inner join b_item_drug_standard_map_item on b_item_drug_standard_map_item.b_item_id = b_item.b_item_id "
                + "where t_patient_drug_allergy_backup.flag = '0' "
                + "GROUP BY "
                + "t_patient_id "
                + ", b_item_drug_standard_map_item.b_item_drug_standard_id  "
                + ",f_allergy_type_id  "
                + ",drug_allergy_symtom_date  "
                + ",drug_allergy_symtom "
                + ",f_naranjo_interpretation_id  "
                + ",drug_allergy_note "
                + ",f_allergy_warning_type_id "
                + ",drug_allergy_report_date  "
                + ",pharma_assess_id  "
                + ",doctor_diag_id  "
                + ",user_record  "
                + ",record_date_time  "
                + ",user_modify  "
                + ",modify_date_time "
                + ",active ";
        return eQuery2(sql);
    }

    public int updateImported(PatientDrugAllergy o) throws Exception {
        String sql = "UPDATE t_patient_drug_allergy_backup "
                + "SET flag ='1' "
                + "WHERE t_patient_drug_allergy_id ='%s'";
        sql = String.format(sql, o.getObjectId());
        return theConnectionInf.eUpdate(sql);
    }

    public Vector eQuery(String sql) throws Exception {
        PatientDrugAllergy p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            p = new PatientDrugAllergy();
            p.setObjectId(rs.getString("t_patient_drug_allergy_id"));
            p.t_patient_id = rs.getString("t_patient_id");
            p.b_item_drug_standard_id = rs.getString("b_item_drug_standard_id");
            p.f_allergy_type_id = rs.getString("f_allergy_type_id");
            p.drug_allergy_symtom_date = rs.getString("drug_allergy_symtom_date");
            p.drug_allergy_symtom = rs.getString("drug_allergy_symtom");
            p.f_naranjo_interpretation_id = rs.getString("f_naranjo_interpretation_id");
            p.drug_allergy_note = rs.getString("drug_allergy_note");
            p.f_allergy_warning_type_id = rs.getString("f_allergy_warning_type_id");
            p.drug_allergy_report_date = rs.getString("drug_allergy_report_date");
            p.pharma_assess_id = rs.getString("pharma_assess_id");
            p.doctor_diag_id = rs.getString("doctor_diag_id");
            p.user_record = rs.getString("user_record");
            p.record_date_time = rs.getString("record_date_time");
            p.user_modify = rs.getString("user_modify");
            p.modify_date_time = rs.getString("modify_date_time");
            p.active = rs.getString("active");
            p.generic_name = rs.getString("item_drug_standard_description");
            p.naranjo_interpretation = rs.getString("naranjo_interpretation_detail");
            p.allergy_warning = rs.getString("warning_type_description");
            p.f_allergy_level_id = rs.getString("f_allergy_level_id");
            p.f_allergy_informant_id = rs.getString("f_allergy_informant_id");
            p.allergy_informant_hospital_id = rs.getString("allergy_informant_hospital_id");
            p.f_allergy_icd10_id = rs.getString("f_allergy_icd10_id");
            list.add(p);
        }
        rs.close();
        return list;
    }

    public Vector eQuery2(String sql) throws Exception {
        PatientDrugAllergy p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            p = new PatientDrugAllergy();
            p.setObjectId(rs.getString("t_patient_drug_allergy_id"));
            p.t_patient_id = rs.getString("t_patient_id");
            p.b_item_drug_standard_id = rs.getString("b_item_drug_standard_id");
            p.f_allergy_type_id = rs.getString("f_allergy_type_id");
            p.drug_allergy_symtom_date = rs.getString("drug_allergy_symtom_date");
            p.drug_allergy_symtom = rs.getString("drug_allergy_symtom");
            p.f_naranjo_interpretation_id = rs.getString("f_naranjo_interpretation_id");
            p.drug_allergy_note = rs.getString("drug_allergy_note");
            p.f_allergy_warning_type_id = rs.getString("f_allergy_warning_type_id");
            p.drug_allergy_report_date = rs.getString("drug_allergy_report_date");
            p.pharma_assess_id = rs.getString("pharma_assess_id");
            p.doctor_diag_id = rs.getString("doctor_diag_id");
            p.user_record = rs.getString("user_record");
            p.record_date_time = rs.getString("record_date_time");
            p.user_modify = rs.getString("user_modify");
            p.modify_date_time = rs.getString("modify_date_time");
            p.active = rs.getString("active");
            list.add(p);
        }
        rs.close();
        return list;
    }
}

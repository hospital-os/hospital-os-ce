/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.LisLn;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.Gutil;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.ResultSet;
import java.util.Vector;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class LisLnDB {

    private ConnectionInf theConnectionInf;
    private LisLn dbObj;
    final public String idtable = "lis1";/*"178";*/

    OrderItemDrugDB theOrderItemDrugDB;

    /**
     * @param ConnectionInf db
     * @roseuid 3F65897F0326
     */
    public LisLnDB(ConnectionInf db) {
        theConnectionInf = db;
        dbObj = new LisLn();
        this.initConfig();
    }

    private LisLn initConfig() {
        dbObj.table = "t_lis_ln";
        dbObj.pk_field = "t_lis_ln_id";
        dbObj.ln = "lab_number";
        dbObj.visit_id = "t_visit_id";
        dbObj.exec_datetime = "exec_datetime";
        dbObj.exec_computer_name = "exec_computer_name";
        dbObj.exec_computer_ip = "exec_computer_ip";
        dbObj.exec_user_id = "exec_user_id";
        dbObj.exec_location_id = "exec_location_id";
        return dbObj;
    }

    public int insert(LisLn p) throws Exception {
        InetAddress addr = null;
        try {
            addr = InetAddress.getLocalHost();
        } catch (UnknownHostException ex) {
        }
        p.generateOID(idtable);
        StringBuilder sql = new StringBuilder("insert into ");
        sql.append(dbObj.table);
        sql.append(" (");
        sql.append(dbObj.pk_field);
        sql.append(" ,");
        sql.append(dbObj.ln);
        sql.append(" ,");
        sql.append(dbObj.visit_id);
        sql.append(" ,");
        sql.append(dbObj.exec_user_id);
        sql.append(" ,");
        sql.append(dbObj.exec_location_id);
        sql.append(" ,");
        sql.append(dbObj.exec_datetime);
        sql.append(" ,");
        sql.append(dbObj.exec_computer_name);
        sql.append(" ,");
        sql.append(dbObj.exec_computer_ip);
        sql.append(" ) values ('");
        sql.append(p.getObjectId());
        sql.append("','");
        sql.append(p.ln);
        sql.append("','");
        sql.append(p.visit_id);
        sql.append("','");
        sql.append(p.exec_user_id);
        sql.append("','");
        sql.append(p.exec_location_id);
        sql.append("','");
        sql.append(p.exec_datetime);
        sql.append("','");
        sql.append(Gutil.CheckReservedWords(addr != null ? addr.getHostName() : p.exec_computer_name));
        sql.append("','");
        sql.append(addr != null ? addr.getHostAddress() : p.exec_computer_ip);
        sql.append("')");
        return theConnectionInf.eUpdate(sql.toString());
    }

    /**
     *
     * @author Somprasong ���Ҥ�� t_lab_order_number_map_visit_id ����ش����
     * �������������ա 1 ���ǨѴ���������ٻ yyMMddxxxx
     * @param c2
     * @param visitId
     * @param str_verify
     * @param executeDate
     * @return
     * @throws SQLException
     * @throws Exception
     */
    public String getLabNumber(String visitId, String executeDate,
            String execStaffId, String servicePointId) throws Exception {
        String strDate = executeDate.substring(2, 10).replaceAll("-", "");
        String sql = "SELECT lab_number "
                + "FROM t_lis_ln "
                + "Where t_lis_ln.t_visit_id = '" + visitId + "' "
                + "and t_lis_ln.exec_datetime = '" + executeDate + "'";
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        String labnumber = "";
        if (rs.next()) {
            labnumber = rs.getString("lab_number");
        }
        rs.close();
        if (!labnumber.isEmpty()) {
            return labnumber;
        }
        // ��ҵ�Ǩ�ͺ�������������� ��� running no. ��������
        sql = "select max(t_lis_ln.lab_number) as lab_number from t_lis_ln where lab_number like (select substring((to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_date, 'MMDD')  || '%', 3))";

        rs = theConnectionInf.eQuery(sql.toString());
        if (rs.next()) {
            labnumber = rs.getString("lab_number");
        }
        rs.close();
        // �ѹ���� ����á
        if (labnumber == null || labnumber.isEmpty()) {
            labnumber = strDate + "0001";
            LisLn lisLn = new LisLn();
            lisLn.ln = labnumber;
            lisLn.visit_id = visitId;
            lisLn.exec_datetime = executeDate;
            lisLn.exec_user_id = execStaffId;
            lisLn.exec_location_id = servicePointId;
            this.insert(lisLn);
            return labnumber;
        }
        // �ѹ���ǡѹ ������ա1
        int latestNo = Integer.parseInt(labnumber.substring(6));
        String nextNo = String.valueOf(latestNo + 1);
        switch (nextNo.length()) {
            case 1:
                labnumber = strDate + "000" + nextNo;
                break;
            case 2:
                labnumber = strDate + "00" + nextNo;
                break;
            case 3:
                labnumber = strDate + "0" + nextNo;
                break;
            default:
                labnumber = strDate + nextNo;
                break;
        }
        LisLn lisLn = new LisLn();
        lisLn.ln = labnumber;
        lisLn.visit_id = visitId;
        lisLn.exec_datetime = executeDate;
        lisLn.exec_user_id = execStaffId;
        lisLn.exec_location_id = servicePointId;
        this.insert(lisLn);
        return labnumber;
    }

    @SuppressWarnings("UseOfObsoleteCollectionType")
    public Vector eQuery(String sql) throws Exception {
        LisLn p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        while (rs.next()) {
            p = new LisLn();
            p.setObjectId(rs.getString(dbObj.pk_field));
            p.visit_id = rs.getString(dbObj.visit_id);
            p.ln = rs.getString(dbObj.ln);
            p.exec_datetime = rs.getString(dbObj.exec_datetime);
            p.rec_datetime = rs.getTimestamp("rec_datetime");
        }
        rs.close();
        return list;
    }
}

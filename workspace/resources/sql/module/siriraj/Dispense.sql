select
             b_site.b_visit_office_id as provider_id     
             ,lpad(t_patient.patient_hn,10,'0')||lpad(t_visit.visit_vn,10,'0') as dispense_id     
             ,lpad(t_visit.visit_vn,9,'0') ||0|| replace(substr(t_billing_invoice.t_billing_invoice_date_time,12,19),':','') as invoice_no      
             ,t_patient.patient_hn as hn     
             ,t_patient.patient_pid as pid     
             ,cast(substr(order_date.order_date_time,1,4) as numeric) -543 ||substr(order_date.order_date_time,5,6)||'T'||substr(order_date.order_date_time,12,8) as prescription_date     
             ,case when order_dispense.order_dispense_date_time = '' then ''     
             else cast(substr(order_dispense.order_dispense_date_time,1,4) as numeric) -543      
             ||substr(order_dispense.order_dispense_date_time,5,6)||'T'||substr(order_dispense.order_dispense_date_time,12,8) end as dispensed_date      

             ,case when b_employee.f_employee_authentication_id = '3' and b_employee.employee_number  <> ''     
             and (f_patient_prefix.patient_prefix_description not ilike 'ทพ.%' or f_patient_prefix.patient_prefix_description not ilike 'ทพญ.%')      
             then  'ว'||b_employee.employee_number      
             when b_employee.f_employee_authentication_id = '3' and b_employee.employee_number  <> ''      
             and (f_patient_prefix.patient_prefix_description ilike 'ทพ.%' or f_patient_prefix.patient_prefix_description ilike 'ทพญ.%')     
             then  'ท'||b_employee.employee_number      
             when b_employee.f_employee_authentication_id = '2' and b_employee.employee_number  <> ''  then  'พ'||b_employee.employee_number     
              when b_employee.f_employee_authentication_id = '6' and b_employee.employee_number  <> ''  then  'ภ'||b_employee.employee_number      
             else '-' end as prescriber     

             ,count(t_billing_invoice_item.t_billing_invoice_item_id) as item_count     
             ,sum(t_billing_invoice_item.billing_invoice_item_total)||'.00'  as charge_amount     
             ,sum(t_billing_invoice_item.billing_invoice_item_payer_share)||'.00'   as claim_amount      
             ,sum(t_billing_invoice_item.billing_invoice_item_patient_share)||'.00'   as paid_amount     
             ,'0.00' as other_amount     
             ,'' as reimburser     
             ,'' as benefit_plan     
             ,'1' as dispense_status     


from     
        t_billing_invoice inner join t_visit on t_billing_invoice.t_visit_id = t_visit.t_visit_id        
                            and t_billing_invoice.billing_invoice_active = '1'     
                             and t_visit.f_visit_type_id = '0'     
                             and t_visit.f_visit_status_id in ('2','3')     
                            and cast(t_billing_invoice.billing_invoice_payer_share as float) > 0.0
     inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id      
     inner join t_billing_invoice_item on t_billing_invoice.t_billing_invoice_id = t_billing_invoice_item.t_billing_invoice_id      
                        and t_billing_invoice_item.billing_invoice_item_active = '1'      
     inner join t_order on  t_order.t_order_id = t_billing_invoice_item.t_order_item_id       
                     and t_order.f_order_status_id not in ('0','3')     
                     and t_order.f_item_group_id = '1'   
  
     inner join      
     (select     
            t_order.t_patient_id as t_patient_id     
            ,t_order.t_visit_id as t_visit_id     
            ,min(t_order.order_date_time) as order_date_time     
     from t_order inner join t_visit on t_order.t_visit_id = t_visit.t_visit_id     
                inner join t_billing_invoice on t_visit.t_visit_id = t_billing_invoice.t_visit_id
     where     
            t_order.f_order_status_id not in ('0','3')     
            and t_order.f_item_group_id = '1'     
            and t_visit.f_visit_type_id = '0'     
            and t_visit.f_visit_status_id in ('2','3')    
            and lpad(t_visit.visit_vn,9,'0') ||0|| replace(substr(t_billing_invoice.t_billing_invoice_date_time,12,19),':','')  = ?
    
     group by     
            t_order.t_patient_id     
            ,t_order.t_visit_id     
     order by     
            order_date_time asc     
     ) as order_date on t_visit.t_visit_id = order_date.t_visit_id       

     inner join      
     (select     
             t_order.t_patient_id as t_patient_id     
             ,t_order.t_visit_id as t_visit_id     
             ,max(t_order.order_dispense_date_time) as order_dispense_date_time        
     from t_order inner join t_visit on t_order.t_visit_id = t_visit.t_visit_id     
                inner join t_billing_invoice on t_visit.t_visit_id = t_billing_invoice.t_visit_id 
     where     
             t_order.f_order_status_id not in ('0','3')     
             and t_order.f_item_group_id = '1'     
            and t_visit.f_visit_type_id = '0'     
            and t_visit.f_visit_status_id in ('2','3')    
             and lpad(t_visit.visit_vn,9,'0') ||0|| replace(substr(t_billing_invoice.t_billing_invoice_date_time,12,19),':','')  = ?                            
     group by     
             t_order.t_patient_id     
             ,t_order.t_visit_id) as order_dispense 
                                    on t_visit.t_visit_id = order_dispense.t_visit_id       


     inner join      
     (select     
             t_order.t_patient_id as t_patient_id     
             ,t_order.t_visit_id as t_visit_id     
             ,t_order.order_staff_order as order_staff_order     
             ,min(t_order.order_date_time) as order_date_time     
     from t_order inner join t_visit on t_order.t_visit_id = t_visit.t_visit_id     
                inner join t_billing_invoice on t_visit.t_visit_id = t_billing_invoice.t_visit_id 
     where     
             f_order_status_id not in ('0','3')     
             and t_order.f_item_group_id = '1'     
             and t_visit.f_visit_type_id = '0'     
             and t_visit.f_visit_status_id in ('2','3')  
             and lpad(t_visit.visit_vn,9,'0') ||0|| replace(substr(t_billing_invoice.t_billing_invoice_date_time,12,19),':','')  = ?         

     group by     
         t_order.t_patient_id     
         ,t_order.t_visit_id     
         ,t_order.order_staff_order     
     order by     
     order_date_time asc     
     ) as order_staff on t_visit.t_visit_id = order_staff.t_visit_id       
                         and order_date.order_date_time = order_staff.order_date_time     

     left join b_employee on order_staff.order_staff_order = b_employee.b_employee_id      
     inner join t_visit_payment on  t_billing_invoice.t_payment_id = t_visit_payment.t_visit_payment_id     
                                            and t_visit_payment.visit_payment_active = '1'     

     and t_visit_payment.b_contract_plans_id in (select  b_welfare_direct_draw_map_plan.b_contract_plans_id   from    b_welfare_direct_draw_map_plan ) 

     left join t_person on b_employee.t_person_id = t_person.t_person_id     
     left join f_patient_prefix on t_person.f_prefix_id = f_patient_prefix.f_patient_prefix_id     
     cross join b_site     

where
        lpad(t_visit.visit_vn,9,'0') ||0|| replace(substr(t_billing_invoice.t_billing_invoice_date_time,12,19),':','')  = ?

group by     
     provider_id     
     ,dispense_id     
     ,invoice_no     
     ,hn     
     ,pid     
     ,prescription_date     
     ,dispensed_date     
     ,prescriber     
     ,other_amount     
     ,reimburser     
     ,benefit_plan     
     ,dispense_status     
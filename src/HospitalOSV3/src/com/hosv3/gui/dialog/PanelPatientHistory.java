/*
 * PanelPatientHistory.java
 *
 * Created on 5 ����Ҥ� 2549, 10:50 �.
 */
package com.hosv3.gui.dialog;

import com.hospital_os.object.*;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hospital_os.utility.*;
import com.hosv3.control.HosControl;
import com.hosv3.object.HosObject;
import com.hosv3.subject.HosSubject;
import com.hosv3.utility.Constant;
import com.hosv3.utility.DateUtil;
import com.hosv3.utility.GuiLang;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Vector;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.text.BadLocationException;

/**
 *
 * @author Administrator
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class PanelPatientHistory extends javax.swing.JPanel {

    private static final long serialVersionUID = 1L;
    HosObject theHO;
    HosControl theHC;
    HosSubject theHS;
    UpdateStatus theUS;
    //HosDialog theHD;
    JDialog theJD;
    PanelSetupSearchSub theDAllergy;
    JFrame theJFrame;
    Vector vPastHistory = new Vector();
    Vector vPersonalDisease = new Vector();
    Vector vRiskFactor = new Vector();
    private G6PD theG6pd;
    private PatientPastVaccine thePatientPastVaccine;
    private final String[] columns = new String[]{"������", "��¡�÷����", "�ѹ���ѹ�֡", "ʶҹ�"};
    private final TableModel tableModel = new TableModel(columns);
    private final String[] columnsDrugAllergy = new String[]{"�������ѭ�ҧ��", "�ҡ��", "�ѹ������ҡ��", "�ѹ�����§ҹ", "�š�û����Թ", "�����͹"};
    private final TableModelDrugAllergy tableModelDrugAllergy = new TableModelDrugAllergy(columnsDrugAllergy);
    private DialogInformIntolerance theDialogInformIntolerance;
    private DialogPatientDrugAllergyAssess theDialogPatientDrugAllergyAssess;
    private PatientFamilyHistory patientFamilyHistory;
    private final TableModelComplexDataSource modelAdl = new TableModelComplexDataSource(new String[]{
        "�ѹ�������Թ", "VN", "��ṹ", "�š�û����Թ"
    }, false, false);
    private DialogPatientAdl theDialogPatientAdl;
    private final TableModelComplexDataSource model2QPlus = new TableModelComplexDataSource(new String[]{
        "�ѹ�������Թ", "VN", "�š�û����Թ"
    }, false, false);
    private DialogPatient2QPlus theDialogPatient2QPlus;

    /**
     * Creates new form PanelPatientHistory
     */
    public PanelPatientHistory() {
        initComponents();
        setLanguage(null);
        initTable();
    }

    public PanelPatientHistory(HosControl hc, UpdateStatus us) {
        initComponents();
        setLanguage(null);
        setControl(hc, us);
        initDialog();
        initTable();
    }

    private void setLanguage(String msg) {
        GuiLang.setTextBundle(jPanel8);
        GuiLang.setTextBundle(jPanel10);
        GuiLang.setTextBundle(jPanel1);
        GuiLang.setTextBundle(jPanel3);
        GuiLang.setLanguage(jLabel1);
        GuiLang.setLanguage(jLabel2);
        GuiLang.setLanguage(jLabel3);
        GuiLang.setLanguage(jLabel4);
        GuiLang.setLanguage(jButtonAddAllergy);
        GuiLang.setLanguage(jButtonDelAllergy);
        GuiLang.setLanguage(jButtonSavePatientHistory);
        GuiLang.setLanguage(jCheckBox_fmh_com);
        GuiLang.setLanguage(jCheckBox_fmh_dm);
        GuiLang.setLanguage(jCheckBox_fmh_ht);
        GuiLang.setLanguage(jCheckBox_fat);
        GuiLang.setLanguage(jRadioButton_fat1);
        GuiLang.setLanguage(jRadioButton_fat2);
        GuiLang.setLanguage(jRadioButton_fat3);
    }

    public void setControl(HosControl hc, UpdateStatus us) {
        theHC = hc;
        theHO = hc.theHO;
        theHS = hc.theHS;
        theUS = us;
        java.util.Vector<ComboFix> listAllergicReactionsType = new java.util.Vector<ComboFix>();
        listAllergicReactionsType.addAll(theHC.theLookupControl.listAllergicReactionsType());
        listAllergicReactionsType.add(0, new ComboFix("%", "������"));
        ComboboxModel.initComboBox(cbShowIntoleranceType, listAllergicReactionsType);
    }

    public void initDialog() {
        theJD = new JDialog(theUS.getJFrame(), true);
        theJD.setTitle(Constant.getTextBundle("����ѵԼ�����"));
        theJD.getContentPane().add(this);
        theJD.setSize(800, 600);
        theJD.setLocationRelativeTo(null);
    }

    public Vector getPastHistoryV() {
        vPastHistory.removeAllElements();
        if (!jTextField_has_sick.getText().isEmpty()) {
            PastHistory ph = new PastHistory();
            ph.topic = "�»�����";
            ph.description = jTextField_has_sick.getText();
            ph.date_desc = this.dateComboBox_sick_date.getText();
            vPastHistory.add(ph);
        }
        if (!jTextArea_past_hx.getText().isEmpty()) {
            PastHistory ph = new PastHistory();
            ph.topic = "����";
            ph.description = this.jTextArea_past_hx.getText();
            vPastHistory.add(ph);
        }
        return vPastHistory;
    }

    public void setPastHistoryV(Vector v) {
        jTextField_has_sick.setText("");
        dateComboBox_sick_date.setText("");
        jTextArea_past_hx.setText("");
        //��Ǩ�ͺ��͹����繤����ҧ������Ҩ�������ͧ�ӵ�������������
        if (v == null) {
            return;
        }
        for (int i = 0, size = v.size(); i < size; i++) {
            PastHistory ph = (PastHistory) v.get(i);
            if (ph.topic.equals("�»�����")) {
                jTextField_has_sick.setText(ph.description);
                dateComboBox_sick_date.setText(DateUtil.convertFieldDate(ph.date_desc));
            } else {
                jTextArea_past_hx.setText(ph.description);
            }
        }
    }

    public PatientFamilyHistory getFamilyHistory() {
        this.patientFamilyHistory.family_history1_select = this.jCheckBox_fmh_dm.isSelected();
        this.patientFamilyHistory.family_history1_detail = this.jCheckBox_fmh_dm.isSelected()
                ? this.jTextField_fhx_dm.getText()
                : "";
        this.patientFamilyHistory.family_history2_select = this.jCheckBox_fmh_ht.isSelected();
        this.patientFamilyHistory.family_history2_detail = this.jCheckBox_fmh_ht.isSelected()
                ? this.jTextField_fhx_ht.getText()
                : "";
        this.patientFamilyHistory.family_history3_select = this.jCheckBox_fmh_com.isSelected();
        this.patientFamilyHistory.family_history3_detail = this.jCheckBox_fmh_com.isSelected()
                ? this.jTextField_fhx_com.getText()
                : "";
        this.patientFamilyHistory.family_history4_select = this.jCheckBox_fmh_FH.isSelected();
        this.patientFamilyHistory.family_history4_detail = this.jCheckBox_fmh_FH.isSelected()
                ? this.jTextField_fhx_FH.getText()
                : "";
        this.patientFamilyHistory.family_history5_select = this.jCheckBox_fmh_coronary.isSelected();
        this.patientFamilyHistory.family_history5_detail = this.jCheckBox_fmh_coronary.isSelected()
                ? this.jTextField_fhx_coronary.getText()
                : "";
        this.patientFamilyHistory.family_history1_select = this.jCheckBox_fmh_dm.isSelected();
        this.patientFamilyHistory.patient_family_history_other = !jTextArea_fhx_other.getText().isEmpty()
                ? this.jTextArea_fhx_other.getText()
                : "";
        return this.patientFamilyHistory;
    }

    public void setFamilyHistoryV(PatientFamilyHistory obj) {
        this.patientFamilyHistory = obj;
        jCheckBox_fmh_com.setSelected(false);
        jCheckBox_fmh_dm.setSelected(false);
        jCheckBox_fmh_ht.setSelected(false);
        jCheckBox_fmh_FH.setSelected(false);
        jCheckBox_fmh_coronary.setSelected(false);
        this.jTextField_fhx_dm.setText("");
        this.jTextField_fhx_ht.setText("");
        this.jTextField_fhx_com.setText("");
        this.jTextField_fhx_FH.setText("");
        this.jTextField_fhx_coronary.setText("");
        this.jTextArea_fhx_other.setText("");
        //��Ǩ�ͺ��͹����繤����ҧ������Ҩ�������ͧ�ӵ�������������
        if (obj == null) {
            return;
        }
        if (obj.family_history1_select) {
            jCheckBox_fmh_dm.setSelected(true);
            this.jTextField_fhx_dm.setText(obj.family_history1_detail);
        }
        if (obj.family_history2_select) {
            jCheckBox_fmh_ht.setSelected(true);
            this.jTextField_fhx_ht.setText(obj.family_history2_detail);
        }
        if (obj.family_history3_select) {
            jCheckBox_fmh_com.setSelected(true);
            this.jTextField_fhx_com.setText(obj.family_history3_detail);
        }
        if (obj.family_history4_select) {
            jCheckBox_fmh_FH.setSelected(true);
            this.jTextField_fhx_FH.setText(obj.family_history4_detail);
        }
        if (obj.family_history5_select) {
            jCheckBox_fmh_coronary.setSelected(true);
            this.jTextField_fhx_coronary.setText(obj.family_history5_detail);
        }
        this.jTextArea_fhx_other.setText(obj.patient_family_history_other);
    }

    public Vector getPersonalDiseaseV() {
        vPersonalDisease.removeAllElements();
        int line_count = this.jTextAreaPersonalDisease.getLineCount();

        for (int i = 0; i < line_count; i++) {
            try {
                int start = jTextAreaPersonalDisease.getLineStartOffset(i);
                int end = jTextAreaPersonalDisease.getLineEndOffset(i);
                String line = jTextAreaPersonalDisease.getText(start, end - start);
                if (line.endsWith("\n")) {
                    line = line.substring(0, line.length() - 1);
                }
                PersonalDisease pe = new PersonalDisease();
                pe.description = line;
                vPersonalDisease.add(pe);
            } catch (BadLocationException ex) {
                LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            }
        }
        return vPersonalDisease;
    }

    public void setPersonalDiseaseV(Vector v) {
        this.jTextAreaPersonalDisease.setText("");
        String physical_ex = "";
        //��Ǩ�ͺ��͹����繤����ҧ������Ҩ�������ͧ�ӵ�������������
        if (v == null) {
            return;
        }

        for (int i = 0, size = v.size(); i < size; i++) {
            PersonalDisease pe = (PersonalDisease) v.get(i);
            if (physical_ex.isEmpty()) {
                physical_ex = pe.description;
            } else {
                physical_ex = physical_ex + "\n" + pe.description;
            }
        }
        this.jTextAreaPersonalDisease.setText(physical_ex);
    }

    private String getAlcoholResult() {
        if (jrBtnNoDrink.isSelected()) {
            return "0";
        } else if (jrBtnInfrequentDrink.isSelected()) {
            return "1";
        } else if (jrBtnOccasionallyDrink.isSelected()) {
            return "2";
        } else if (jrBtnRegularlyDrink.isSelected()) {
            return "3";
        } else if (jrBtnQuitDrink.isSelected()) {
            return "4";
        } else {
            return "9";
        }
    }

    private void setAlcoholResult(String value) {
        if ("0".equals(value)) {
            jrBtnNoDrink.setSelected(true);
        } else if ("1".equals(value)) {
            jrBtnInfrequentDrink.setSelected(true);
        } else if ("2".equals(value)) {
            jrBtnOccasionallyDrink.setSelected(true);
        } else if ("3".equals(value)) {
            jrBtnRegularlyDrink.setSelected(true);
        } else if ("4".equals(value)) {
            jrBtnQuitDrink.setSelected(true);
        } else {
            jrBtnNotAskDrink.setSelected(true);
        }
    }

    private String getCigareteResult() {
        if (jrBtnNoSmoke.isSelected()) {
            return "0";
        } else if (jrBtnInfrequentSmoke.isSelected()) {
            return "1";
        } else if (jrBtnOccasionallySmoke.isSelected()) {
            return "2";
        } else if (jrBtnRegularlySmoke.isSelected()) {
            return "3";
        } else if (jrBtnQuitSmoke.isSelected()) {
            return "4";
        } else {
            return "9";
        }
    }

    private void setCigareteResult(String value) {
        if ("0".equals(value)) {
            jrBtnNoSmoke.setSelected(true);
        } else if ("1".equals(value)) {
            jrBtnInfrequentSmoke.setSelected(true);
        } else if ("2".equals(value)) {
            jrBtnOccasionallySmoke.setSelected(true);
        } else if ("3".equals(value)) {
            jrBtnRegularlySmoke.setSelected(true);
        } else if ("4".equals(value)) {
            jrBtnQuitSmoke.setSelected(true);
        } else {
            jrBtnNotAskSmoke.setSelected(true);
        }
    }

    private String getExerciseResult() {
        if (rbtn0.isSelected()) {
            return "0";
        } else if (rbtn1.isSelected()) {
            return "1";
        } else if (rbtn2.isSelected()) {
            return "2";
        } else if (rbtn3.isSelected()) {
            return "3";
        } else if (rbtn4.isSelected()) {
            return "4";
        } else {
            return "9";
        }
    }

    private void setExerciseResult(String value) {
        if ("0".equals(value)) {
            rbtn0.setSelected(true);
        } else if ("1".equals(value)) {
            rbtn1.setSelected(true);
        } else if ("2".equals(value)) {
            rbtn2.setSelected(true);
        } else if ("3".equals(value)) {
            rbtn3.setSelected(true);
        } else if ("4".equals(value)) {
            rbtn4.setSelected(true);
        } else {
            rbtn9.setSelected(true);
        }
    }

    public Vector getRiskFactorV() {
        vRiskFactor.removeAllElements();
        RiskFactor ph;
        // 3.9.16b01
        if (!jrBtnNotAskDrink.isSelected() || !txtDrinkRemark.getText().isEmpty()) {
            ph = new RiskFactor();
            ph.topic = "������š�����";
            ph.description = this.txtDrinkRemark.getText();
            ph.risk_alcoho_result = getAlcoholResult();
            ph.risk_alcoho_about = txtDrink.isEditable() ? txtDrink.getText() : "";
            ph.risk_alcoho_about_week = txtDrinkWeek.isEditable() ? txtDrinkWeek.getText() : "";
            ph.risk_alcoho_advise = jcboxRecommendQuitDrink.isEnabled() ? (jcboxRecommendQuitDrink.isSelected() ? "1" : "0") : "0";
            vRiskFactor.add(ph);
        }

        if (!jrBtnNotAskSmoke.isSelected() || !txtCigarRemark.getText().isEmpty()) {
            ph = new RiskFactor();
            ph.topic = "�ٺ������";
            ph.description = this.txtCigarRemark.getText();
            ph.risk_cigarette_result = getCigareteResult();
            ph.risk_cigarette_about = txtSmoke.isEditable() ? txtSmoke.getText() : "";
            ph.risk_cigarette_about_week = txtSmokeWeek.isEditable() ? txtSmokeWeek.getText() : "";
            ph.risk_cigarette_advise = jcboxRecommendQuitSmoke.isEnabled() ? (jcboxRecommendQuitSmoke.isSelected() ? "1" : "0") : "0";
            vRiskFactor.add(ph);
        }

        if (!rbtn9.isSelected() || !txtExerciseRemark.getText().isEmpty()) {
            ph = new RiskFactor();
            ph.topic = "�͡���ѧ���";
            ph.description = this.txtExerciseRemark.getText();
            ph.risk_exercise_result = getExerciseResult();
            vRiskFactor.add(ph);
        }

        if (this.jCheckBox_fat.isSelected()) {
            ph = new RiskFactor();
            ph.topic = "��ǹ";
            if (jRadioButton_fat1.isSelected()) {
                ph.description = "�дѺ 1";
            } else if (jRadioButton_fat2.isSelected()) {
                ph.description = "�дѺ 2";
            } else if (jRadioButton_fat3.isSelected()) {
                ph.description = "�дѺ 3";
            }
            vRiskFactor.add(ph);
        }
        if (!jTextArea_risk.getText().isEmpty()) {
            ph = new RiskFactor();
            ph.topic = "����";
            ph.description = this.jTextArea_risk.getText();
            vRiskFactor.add(ph);
        }
        return vRiskFactor;
    }

    public void setRiskFactorV(Vector v) {
        this.txtCigarRemark.setText("");
        this.txtDrinkRemark.setText("");
        this.txtExerciseRemark.setText("");
        this.jTextArea_risk.setText("");
        this.jCheckBox_fat.setSelected(false);
        buttonGroup3.clearSelection();
        jcboxRecommendQuitDrink.setSelected(false);
        jcboxRecommendQuitSmoke.setSelected(false);
        txtDrink.setText("");
        txtDrinkWeek.setText("");
        txtSmoke.setText("");
        txtSmokeWeek.setText("");
        setAlcoholResult("9");
        setCigareteResult("9");
        setExerciseResult("9");
        //��Ǩ�ͺ��͹����繤����ҧ������Ҩ�������ͧ�ӵ�������������
        if (v == null) {
            return;
        }

        for (int i = 0, size = v.size(); i < size; i++) {
            RiskFactor ph = (RiskFactor) v.get(i);
            // 3.9.16b01
            if (ph.topic.equals("������š�����")) {
                this.txtDrinkRemark.setText(ph.description == null ? "" : ph.description);
                setAlcoholResult(ph.risk_alcoho_result == null ? "" : ph.risk_alcoho_result);
                txtDrink.setText(ph.risk_alcoho_about == null ? "" : ph.risk_alcoho_about);
                txtDrinkWeek.setText(ph.risk_alcoho_about_week == null ? "" : ph.risk_alcoho_about_week);
                jcboxRecommendQuitDrink.setSelected(ph.risk_alcoho_advise != null && ph.risk_alcoho_advise.equals("1"));
            } else if (ph.topic.equals("�ٺ������")) {
                this.txtCigarRemark.setText(ph.description == null ? "" : ph.description);
                setCigareteResult(ph.risk_cigarette_result == null ? "" : ph.risk_cigarette_result);
                txtSmoke.setText(ph.risk_cigarette_about == null ? "" : ph.risk_cigarette_about);
                txtSmokeWeek.setText(ph.risk_cigarette_about_week == null ? "" : ph.risk_cigarette_about_week);
                jcboxRecommendQuitSmoke.setSelected(ph.risk_cigarette_advise != null && ph.risk_cigarette_advise.equals("1"));
            } else if (ph.topic.equals("�͡���ѧ���")) {
                this.txtExerciseRemark.setText(ph.description == null ? "" : ph.description);
                setExerciseResult(ph.risk_exercise_result == null ? "" : ph.risk_exercise_result);
            } else if (ph.topic.equals("��ǹ")) {
                this.jCheckBox_fat.setSelected(true);
                if (ph.description.equals("�дѺ 1")) {
                    jRadioButton_fat1.setSelected(true);
                } else if (ph.description.equals("�дѺ 2")) {
                    jRadioButton_fat2.setSelected(true);
                } else if (ph.description.equals("�дѺ 3")) {
                    jRadioButton_fat3.setSelected(true);
                }
            } else {
                this.jTextArea_risk.setText(ph.description);
            }
        }
        this.validateRiskUI();
    }

    private void validateRiskUI() {
        this.jRadioButton_fat1.setEnabled(jCheckBox_fat.isSelected());
        this.jRadioButton_fat2.setEnabled(jCheckBox_fat.isSelected());
        this.jRadioButton_fat3.setEnabled(jCheckBox_fat.isSelected());
        txtDrink.setEditable(!(jrBtnNoDrink.isSelected() || jrBtnNotAskDrink.isSelected()));
        txtDrinkWeek.setEditable(!(jrBtnNoDrink.isSelected() || jrBtnNotAskDrink.isSelected()));
        txtSmoke.setEditable(!(jrBtnNoSmoke.isSelected() || jrBtnNotAskSmoke.isSelected()));
        txtSmokeWeek.setEditable(!(jrBtnNoSmoke.isSelected() || jrBtnNotAskSmoke.isSelected()));
        jcboxRecommendQuitDrink.setEnabled(!(jrBtnNoDrink.isSelected() || jrBtnNotAskDrink.isSelected()));
        jcboxRecommendQuitSmoke.setEnabled(!(jrBtnNoSmoke.isSelected() || jrBtnNotAskSmoke.isSelected()));
    }

    public void showDialog(int index) {
        setPatientHistory();
        jTabbedPane1.setSelectedIndex(index);
        theJD.setVisible(true);
    }

    public void showDialog(int index, int indexTab3) {
        setPatientHistory();
        jTabbedPane1.setSelectedIndex(index);
        if (indexTab3 == 0 || indexTab3 == 1) {
            jTabbedPane2.setSelectedIndex(indexTab3);
        }
        theJD.setVisible(true);
    }

    /**
     * @Author : amp henbe
     * @date : 11/02/2549 21/05/06
     * @see : �Ѵ�������ǡѺ Gui �ͧ����ѵ��ʹյ
     * @param : boolean -> true = ����ö�ӧҹ�� ,false = �ӧҹ�����
     */
    private void setPatientHistory() {
        this.setPastHistoryV(theHO.vPastHistory);
        this.setFamilyHistoryV(theHO.thePatientFamilyHistory);
        this.setPersonalDiseaseV(theHO.vPersonalDisease);
        this.setRiskFactorV(theHO.vRiskFactor);
//        this.setDrugAllergyV(theHO.vDrugAllergy);
        this.setG6PD(theHO.theG6pd);
        this.setPatentPastVaccine(theHO.thePatientPastVaccine);
        this.listTableData();
        this.listTableDrugAllergy();
        this.listTableAdl();
        this.listTable2qPlus();
        // pharma only
        jButtonAddAllergy.setEnabled(theHO.theEmployee.authentication_id.equals(Authentication.PHARM));
        jButtonDelAllergy.setEnabled(tableDrugAllergy.getSelectedRowCount() > 0 && theHO.theEmployee.authentication_id.equals(Authentication.PHARM));
        btnEditDrugAllergy.setEnabled(tableDrugAllergy.getSelectedRowCount() > 0 && theHO.theEmployee.authentication_id.equals(Authentication.PHARM));

        updateUIAdl();
        updateUI2qPlus();
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroup4 = new javax.swing.ButtonGroup();
        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        buttonGroup3 = new javax.swing.ButtonGroup();
        buttonGroup5 = new javax.swing.ButtonGroup();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        panelHistory = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        jTextAreaPersonalDisease = new com.hosv3.gui.component.BalloonTextArea();
        cbG6PD = new javax.swing.JCheckBox();
        jPanel8 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTextArea_past_hx = new com.hosv3.gui.component.BalloonTextArea();
        jTextField_has_sick = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        dateComboBox_sick_date = new com.hospital_os.utility.DateComboBox();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        rdVacNA = new javax.swing.JRadioButton();
        rdVacComplete = new javax.swing.JRadioButton();
        rdVacUnComplete = new javax.swing.JRadioButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtVacUnCompleteDetail = new javax.swing.JTextArea();
        jPanel10 = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTextArea_fhx_other = new com.hosv3.gui.component.BalloonTextArea();
        jCheckBox_fmh_dm = new javax.swing.JCheckBox();
        jCheckBox_fmh_com = new javax.swing.JCheckBox();
        jCheckBox_fmh_ht = new javax.swing.JCheckBox();
        jTextField_fhx_dm = new javax.swing.JTextField();
        jTextField_fhx_ht = new javax.swing.JTextField();
        jTextField_fhx_com = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jCheckBox_fmh_FH = new javax.swing.JCheckBox();
        jTextField_fhx_FH = new javax.swing.JTextField();
        jCheckBox_fmh_coronary = new javax.swing.JCheckBox();
        jTextField_fhx_coronary = new javax.swing.JTextField();
        jToolBar1 = new javax.swing.JToolBar();
        jButtonSavePatientHistory = new javax.swing.JButton();
        panelRisk = new javax.swing.JPanel();
        panelRiskAlcohol = new javax.swing.JPanel();
        jrBtnNoDrink = new javax.swing.JRadioButton();
        jrBtnInfrequentDrink = new javax.swing.JRadioButton();
        jrBtnOccasionallyDrink = new javax.swing.JRadioButton();
        jrBtnRegularlyDrink = new javax.swing.JRadioButton();
        jrBtnQuitDrink = new javax.swing.JRadioButton();
        jrBtnNotAskDrink = new javax.swing.JRadioButton();
        jcboxRecommendQuitDrink = new javax.swing.JCheckBox();
        panelAlcoholFeq = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        txtDrink = new com.hospital_os.utility.IntegerTextField(6);
        jLabel9 = new javax.swing.JLabel();
        jScrollPane8 = new javax.swing.JScrollPane();
        txtDrinkRemark = new javax.swing.JTextArea();
        panelAlcoholFeq1 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        txtDrinkWeek = new com.hospital_os.utility.IntegerTextField(6);
        jLabel14 = new javax.swing.JLabel();
        panelRiskCigarete = new javax.swing.JPanel();
        jrBtnNoSmoke = new javax.swing.JRadioButton();
        jrBtnInfrequentSmoke = new javax.swing.JRadioButton();
        jrBtnOccasionallySmoke = new javax.swing.JRadioButton();
        jrBtnRegularlySmoke = new javax.swing.JRadioButton();
        jrBtnQuitSmoke = new javax.swing.JRadioButton();
        jrBtnNotAskSmoke = new javax.swing.JRadioButton();
        jcboxRecommendQuitSmoke = new javax.swing.JCheckBox();
        panelSmokeFeq = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        txtSmoke = new com.hospital_os.utility.IntegerTextField(6);
        jLabel12 = new javax.swing.JLabel();
        jScrollPane9 = new javax.swing.JScrollPane();
        txtCigarRemark = new javax.swing.JTextArea();
        panelSmokeFeq1 = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        txtSmokeWeek = new com.hospital_os.utility.IntegerTextField(6);
        jLabel16 = new javax.swing.JLabel();
        panelExercise = new javax.swing.JPanel();
        rbtn0 = new javax.swing.JRadioButton();
        rbtn1 = new javax.swing.JRadioButton();
        rbtn2 = new javax.swing.JRadioButton();
        rbtn3 = new javax.swing.JRadioButton();
        rbtn4 = new javax.swing.JRadioButton();
        rbtn9 = new javax.swing.JRadioButton();
        jScrollPane10 = new javax.swing.JScrollPane();
        txtExerciseRemark = new javax.swing.JTextArea();
        jPanel5 = new javax.swing.JPanel();
        jCheckBox_fat = new javax.swing.JCheckBox();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTextArea_risk = new com.hosv3.gui.component.BalloonTextArea();
        jRadioButton_fat1 = new javax.swing.JRadioButton();
        jRadioButton_fat2 = new javax.swing.JRadioButton();
        jLabel6 = new javax.swing.JLabel();
        jRadioButton_fat3 = new javax.swing.JRadioButton();
        jToolBar6 = new javax.swing.JToolBar();
        jButtonSavePatientHistory2 = new javax.swing.JButton();
        panelDrugAllergy = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();
        jLabel5 = new javax.swing.JLabel();
        cbShowIntoleranceType = new javax.swing.JComboBox();
        jToolBar2 = new javax.swing.JToolBar();
        btnEditInformIntolerance = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jPanel11 = new javax.swing.JPanel();
        jToolBar3 = new javax.swing.JToolBar();
        jButtonAddAllergy = new javax.swing.JButton();
        jToolBar4 = new javax.swing.JToolBar();
        jButtonDelAllergy = new javax.swing.JButton();
        jToolBar5 = new javax.swing.JToolBar();
        btnEditDrugAllergy = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        tableDrugAllergy = new javax.swing.JTable();
        panelPatientADL = new javax.swing.JPanel();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        panelADL = new javax.swing.JPanel();
        jPanel13 = new javax.swing.JPanel();
        btnAddAdl = new javax.swing.JButton();
        btnDelAdl = new javax.swing.JButton();
        bynEditDel = new javax.swing.JButton();
        jScrollPane11 = new javax.swing.JScrollPane();
        tableAdl = new javax.swing.JTable();
        panel2QPlus = new javax.swing.JPanel();
        jScrollPane12 = new javax.swing.JScrollPane();
        table2qPlus = new javax.swing.JTable();
        jPanel14 = new javax.swing.JPanel();
        btnAdd2qPlus = new javax.swing.JButton();
        btnDel2qPlus = new javax.swing.JButton();
        bynEdit2qPlus = new javax.swing.JButton();

        setLayout(new java.awt.GridBagLayout());

        jTabbedPane1.setFont(jTabbedPane1.getFont().deriveFont(jTabbedPane1.getFont().getSize()+2f));

        panelHistory.setLayout(new java.awt.GridBagLayout());

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("�ä��Шӵ�� (�к�)"));
        jPanel1.setMinimumSize(new java.awt.Dimension(156, 150));
        jPanel1.setPreferredSize(new java.awt.Dimension(156, 150));
        jPanel1.setLayout(new java.awt.GridBagLayout());

        jScrollPane7.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        jTextAreaPersonalDisease.setRows(4);
        jTextAreaPersonalDisease.setFont(jTextAreaPersonalDisease.getFont());
        jScrollPane7.setViewportView(jTextAreaPersonalDisease);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel1.add(jScrollPane7, gridBagConstraints);

        cbG6PD.setFont(cbG6PD.getFont());
        cbG6PD.setText("���ä���ͧ�͹��� G-6-PD");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel1.add(cbG6PD, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 0.25;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelHistory.add(jPanel1, gridBagConstraints);

        jPanel8.setBorder(javax.swing.BorderFactory.createTitledBorder("����ѵ��ʹյ"));
        jPanel8.setMinimumSize(new java.awt.Dimension(250, 200));
        jPanel8.setPreferredSize(new java.awt.Dimension(250, 200));
        jPanel8.setLayout(new java.awt.GridBagLayout());

        jScrollPane4.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        jTextArea_past_hx.setFont(jTextArea_past_hx.getFont());
        jTextArea_past_hx.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextArea_past_hxKeyReleased(evt);
            }
        });
        jScrollPane4.setViewportView(jTextArea_past_hx);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 0.5;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel8.add(jScrollPane4, gridBagConstraints);

        jTextField_has_sick.setFont(jTextField_has_sick.getFont());
        jTextField_has_sick.setMinimumSize(new java.awt.Dimension(150, 24));
        jTextField_has_sick.setPreferredSize(new java.awt.Dimension(150, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel8.add(jTextField_has_sick, gridBagConstraints);

        jLabel1.setFont(jLabel1.getFont());
        jLabel1.setText("�����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        jPanel8.add(jLabel1, gridBagConstraints);

        dateComboBox_sick_date.setFont(dateComboBox_sick_date.getFont());
        dateComboBox_sick_date.setMinimumSize(new java.awt.Dimension(93, 24));
        dateComboBox_sick_date.setPreferredSize(new java.awt.Dimension(93, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel8.add(dateComboBox_sick_date, gridBagConstraints);

        jLabel2.setFont(jLabel2.getFont());
        jLabel2.setText("��� (�к�)");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel8.add(jLabel2, gridBagConstraints);

        jLabel3.setFont(jLabel3.getFont());
        jLabel3.setText("�»����� (�к�)");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel8.add(jLabel3, gridBagConstraints);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("����Ѻ�Ѥ�չ"));
        jPanel2.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
        jPanel2.setLayout(new java.awt.GridBagLayout());

        buttonGroup4.add(rdVacNA);
        rdVacNA.setFont(rdVacNA.getFont());
        rdVacNA.setSelected(true);
        rdVacNA.setText("����Һ");
        rdVacNA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdVacNAActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(rdVacNA, gridBagConstraints);

        buttonGroup4.add(rdVacComplete);
        rdVacComplete.setFont(rdVacComplete.getFont());
        rdVacComplete.setText("�ú");
        rdVacComplete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdVacCompleteActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(rdVacComplete, gridBagConstraints);

        buttonGroup4.add(rdVacUnComplete);
        rdVacUnComplete.setFont(rdVacUnComplete.getFont());
        rdVacUnComplete.setText("���ú");
        rdVacUnComplete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdVacUnCompleteActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(rdVacUnComplete, gridBagConstraints);

        txtVacUnCompleteDetail.setColumns(20);
        txtVacUnCompleteDetail.setFont(txtVacUnCompleteDetail.getFont());
        txtVacUnCompleteDetail.setLineWrap(true);
        txtVacUnCompleteDetail.setRows(3);
        txtVacUnCompleteDetail.setEnabled(false);
        jScrollPane1.setViewportView(txtVacUnCompleteDetail);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(jScrollPane1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel8.add(jPanel2, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 0.25;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelHistory.add(jPanel8, gridBagConstraints);

        jPanel10.setBorder(javax.swing.BorderFactory.createTitledBorder("����ѵԤ�ͺ����"));
        jPanel10.setMinimumSize(new java.awt.Dimension(10, 200));
        jPanel10.setPreferredSize(new java.awt.Dimension(10, 200));
        jPanel10.setLayout(new java.awt.GridBagLayout());

        jScrollPane6.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        jTextArea_fhx_other.setFont(jTextArea_fhx_other.getFont());
        jScrollPane6.setViewportView(jTextArea_fhx_other);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel10.add(jScrollPane6, gridBagConstraints);

        jCheckBox_fmh_dm.setFont(jCheckBox_fmh_dm.getFont());
        jCheckBox_fmh_dm.setText("����ҹ (�кؼ����)");
        jCheckBox_fmh_dm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox_fmh_dmActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel10.add(jCheckBox_fmh_dm, gridBagConstraints);

        jCheckBox_fmh_com.setFont(jCheckBox_fmh_com.getFont());
        jCheckBox_fmh_com.setText("�ä�Դ��� (�кؼ����)");
        jCheckBox_fmh_com.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox_fmh_comActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel10.add(jCheckBox_fmh_com, gridBagConstraints);

        jCheckBox_fmh_ht.setFont(jCheckBox_fmh_ht.getFont());
        jCheckBox_fmh_ht.setText("�����ѹ (�кؼ����)");
        jCheckBox_fmh_ht.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox_fmh_htActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel10.add(jCheckBox_fmh_ht, gridBagConstraints);

        jTextField_fhx_dm.setColumns(25);
        jTextField_fhx_dm.setFont(jTextField_fhx_dm.getFont());
        jTextField_fhx_dm.setEnabled(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel10.add(jTextField_fhx_dm, gridBagConstraints);

        jTextField_fhx_ht.setColumns(25);
        jTextField_fhx_ht.setFont(jTextField_fhx_ht.getFont());
        jTextField_fhx_ht.setEnabled(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel10.add(jTextField_fhx_ht, gridBagConstraints);

        jTextField_fhx_com.setColumns(25);
        jTextField_fhx_com.setFont(jTextField_fhx_com.getFont());
        jTextField_fhx_com.setEnabled(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel10.add(jTextField_fhx_com, gridBagConstraints);

        jLabel4.setFont(jLabel4.getFont());
        jLabel4.setText("��� (�к�)");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel10.add(jLabel4, gridBagConstraints);

        jCheckBox_fmh_FH.setFont(jCheckBox_fmh_FH.getFont());
        jCheckBox_fmh_FH.setText("�ջ���ѵ���ѹ�٧㹤�ͺ������µç (Familial hyperlipidemia)  (�кؼ����)");
        jCheckBox_fmh_FH.setToolTipText(jCheckBox_fmh_FH.getText());
        jCheckBox_fmh_FH.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox_fmh_FHActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel10.add(jCheckBox_fmh_FH, gridBagConstraints);

        jTextField_fhx_FH.setColumns(25);
        jTextField_fhx_FH.setFont(jTextField_fhx_FH.getFont());
        jTextField_fhx_FH.setEnabled(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel10.add(jTextField_fhx_FH, gridBagConstraints);

        jCheckBox_fmh_coronary.setFont(jCheckBox_fmh_coronary.getFont());
        jCheckBox_fmh_coronary.setText("�ҵ���µç(��� ��� ����ͧ)���ä��ʹ���ʹ���㨵պ��͹���� 55 �� (�кؼ����)");
        jCheckBox_fmh_coronary.setToolTipText(jCheckBox_fmh_coronary.getText());
        jCheckBox_fmh_coronary.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox_fmh_coronaryActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel10.add(jCheckBox_fmh_coronary, gridBagConstraints);

        jTextField_fhx_coronary.setColumns(25);
        jTextField_fhx_coronary.setFont(jTextField_fhx_coronary.getFont());
        jTextField_fhx_coronary.setEnabled(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel10.add(jTextField_fhx_coronary, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelHistory.add(jPanel10, gridBagConstraints);

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        jButtonSavePatientHistory.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
        jButtonSavePatientHistory.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/btn_save.png"))); // NOI18N
        jButtonSavePatientHistory.setFocusable(false);
        jButtonSavePatientHistory.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonSavePatientHistory.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonSavePatientHistory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSavePatientHistoryActionPerformed(evt);
            }
        });
        jToolBar1.add(jButtonSavePatientHistory);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 2);
        panelHistory.add(jToolBar1, gridBagConstraints);

        jTabbedPane1.addTab("����ѵ�", panelHistory);

        panelRisk.setLayout(new java.awt.GridBagLayout());

        panelRiskAlcohol.setBorder(javax.swing.BorderFactory.createTitledBorder("��ô�������"));
        panelRiskAlcohol.setLayout(new java.awt.GridBagLayout());

        buttonGroup1.add(jrBtnNoDrink);
        jrBtnNoDrink.setFont(jrBtnNoDrink.getFont());
        jrBtnNoDrink.setText("������");
        jrBtnNoDrink.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jrBtnNoDrinkActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelRiskAlcohol.add(jrBtnNoDrink, gridBagConstraints);

        buttonGroup1.add(jrBtnInfrequentDrink);
        jrBtnInfrequentDrink.setFont(jrBtnInfrequentDrink.getFont());
        jrBtnInfrequentDrink.setText("�����ҹ� ����");
        jrBtnInfrequentDrink.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jrBtnInfrequentDrinkActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelRiskAlcohol.add(jrBtnInfrequentDrink, gridBagConstraints);

        buttonGroup1.add(jrBtnOccasionallyDrink);
        jrBtnOccasionallyDrink.setFont(jrBtnOccasionallyDrink.getFont());
        jrBtnOccasionallyDrink.setText("�����繤��駤���");
        jrBtnOccasionallyDrink.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jrBtnOccasionallyDrinkActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelRiskAlcohol.add(jrBtnOccasionallyDrink, gridBagConstraints);

        buttonGroup1.add(jrBtnRegularlyDrink);
        jrBtnRegularlyDrink.setFont(jrBtnRegularlyDrink.getFont());
        jrBtnRegularlyDrink.setText("�����繻�Ш�");
        jrBtnRegularlyDrink.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jrBtnRegularlyDrinkActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelRiskAlcohol.add(jrBtnRegularlyDrink, gridBagConstraints);

        buttonGroup1.add(jrBtnQuitDrink);
        jrBtnQuitDrink.setFont(jrBtnQuitDrink.getFont());
        jrBtnQuitDrink.setText("�´�������ԡ����");
        jrBtnQuitDrink.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jrBtnQuitDrinkActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelRiskAlcohol.add(jrBtnQuitDrink, gridBagConstraints);

        buttonGroup1.add(jrBtnNotAskDrink);
        jrBtnNotAskDrink.setFont(jrBtnNotAskDrink.getFont());
        jrBtnNotAskDrink.setSelected(true);
        jrBtnNotAskDrink.setText("�������");
        jrBtnNotAskDrink.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jrBtnNotAskDrinkActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelRiskAlcohol.add(jrBtnNotAskDrink, gridBagConstraints);

        jcboxRecommendQuitDrink.setFont(jcboxRecommendQuitDrink.getFont());
        jcboxRecommendQuitDrink.setText("���й������ԡ��������");
        jcboxRecommendQuitDrink.setEnabled(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelRiskAlcohol.add(jcboxRecommendQuitDrink, gridBagConstraints);

        panelAlcoholFeq.setLayout(new java.awt.GridBagLayout());

        jLabel8.setFont(jLabel8.getFont());
        jLabel8.setText("�ӹǹ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        panelAlcoholFeq.add(jLabel8, gridBagConstraints);

        txtDrink.setEditable(false);
        txtDrink.setColumns(6);
        txtDrink.setFont(txtDrink.getFont());
        txtDrink.setMinimumSize(new java.awt.Dimension(54, 22));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        panelAlcoholFeq.add(txtDrink, gridBagConstraints);

        jLabel9.setFont(jLabel9.getFont());
        jLabel9.setText("����/��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        panelAlcoholFeq.add(jLabel9, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelRiskAlcohol.add(panelAlcoholFeq, gridBagConstraints);

        jScrollPane8.setBorder(javax.swing.BorderFactory.createTitledBorder("�����˵�"));

        txtDrinkRemark.setColumns(15);
        txtDrinkRemark.setFont(txtDrinkRemark.getFont());
        txtDrinkRemark.setLineWrap(true);
        txtDrinkRemark.setRows(3);
        jScrollPane8.setViewportView(txtDrinkRemark);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridheight = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelRiskAlcohol.add(jScrollPane8, gridBagConstraints);

        panelAlcoholFeq1.setLayout(new java.awt.GridBagLayout());

        jLabel13.setFont(jLabel13.getFont());
        jLabel13.setText("�ӹǹ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        panelAlcoholFeq1.add(jLabel13, gridBagConstraints);

        txtDrinkWeek.setEditable(false);
        txtDrinkWeek.setColumns(6);
        txtDrinkWeek.setFont(txtDrinkWeek.getFont());
        txtDrinkWeek.setMinimumSize(new java.awt.Dimension(54, 22));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        panelAlcoholFeq1.add(txtDrinkWeek, gridBagConstraints);

        jLabel14.setFont(jLabel14.getFont());
        jLabel14.setText("����/�ѻ����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        panelAlcoholFeq1.add(jLabel14, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelRiskAlcohol.add(panelAlcoholFeq1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        panelRisk.add(panelRiskAlcohol, gridBagConstraints);

        panelRiskCigarete.setBorder(javax.swing.BorderFactory.createTitledBorder("����ٺ������"));
        panelRiskCigarete.setLayout(new java.awt.GridBagLayout());

        buttonGroup2.add(jrBtnNoSmoke);
        jrBtnNoSmoke.setFont(jrBtnNoSmoke.getFont());
        jrBtnNoSmoke.setText("����ٺ");
        jrBtnNoSmoke.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jrBtnNoSmokeActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelRiskCigarete.add(jrBtnNoSmoke, gridBagConstraints);

        buttonGroup2.add(jrBtnInfrequentSmoke);
        jrBtnInfrequentSmoke.setFont(jrBtnInfrequentSmoke.getFont());
        jrBtnInfrequentSmoke.setText("�ٺ�ҹ� ���� ");
        jrBtnInfrequentSmoke.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jrBtnInfrequentSmokeActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelRiskCigarete.add(jrBtnInfrequentSmoke, gridBagConstraints);

        buttonGroup2.add(jrBtnOccasionallySmoke);
        jrBtnOccasionallySmoke.setFont(jrBtnOccasionallySmoke.getFont());
        jrBtnOccasionallySmoke.setText("�ٺ�繤��駤���");
        jrBtnOccasionallySmoke.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jrBtnOccasionallySmokeActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelRiskCigarete.add(jrBtnOccasionallySmoke, gridBagConstraints);

        buttonGroup2.add(jrBtnRegularlySmoke);
        jrBtnRegularlySmoke.setFont(jrBtnRegularlySmoke.getFont());
        jrBtnRegularlySmoke.setText("�ٺ�繻�Ш�");
        jrBtnRegularlySmoke.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jrBtnRegularlySmokeActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelRiskCigarete.add(jrBtnRegularlySmoke, gridBagConstraints);

        buttonGroup2.add(jrBtnQuitSmoke);
        jrBtnQuitSmoke.setFont(jrBtnQuitSmoke.getFont());
        jrBtnQuitSmoke.setText("���ٺ����ԡ����");
        jrBtnQuitSmoke.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jrBtnQuitSmokeActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelRiskCigarete.add(jrBtnQuitSmoke, gridBagConstraints);

        buttonGroup2.add(jrBtnNotAskSmoke);
        jrBtnNotAskSmoke.setFont(jrBtnNotAskSmoke.getFont());
        jrBtnNotAskSmoke.setSelected(true);
        jrBtnNotAskSmoke.setText("�������");
        jrBtnNotAskSmoke.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jrBtnNotAskSmokeActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelRiskCigarete.add(jrBtnNotAskSmoke, gridBagConstraints);

        jcboxRecommendQuitSmoke.setFont(jcboxRecommendQuitSmoke.getFont());
        jcboxRecommendQuitSmoke.setText("���й������ԡ�ٺ������");
        jcboxRecommendQuitSmoke.setEnabled(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelRiskCigarete.add(jcboxRecommendQuitSmoke, gridBagConstraints);

        panelSmokeFeq.setLayout(new java.awt.GridBagLayout());

        jLabel11.setFont(jLabel11.getFont());
        jLabel11.setText("�ӹǹ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        panelSmokeFeq.add(jLabel11, gridBagConstraints);

        txtSmoke.setEditable(false);
        txtSmoke.setColumns(6);
        txtSmoke.setFont(txtSmoke.getFont());
        txtSmoke.setMinimumSize(new java.awt.Dimension(54, 22));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        panelSmokeFeq.add(txtSmoke, gridBagConstraints);

        jLabel12.setFont(jLabel12.getFont());
        jLabel12.setText("����/��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        panelSmokeFeq.add(jLabel12, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelRiskCigarete.add(panelSmokeFeq, gridBagConstraints);

        jScrollPane9.setBorder(javax.swing.BorderFactory.createTitledBorder("�����˵�"));

        txtCigarRemark.setColumns(15);
        txtCigarRemark.setFont(txtCigarRemark.getFont());
        txtCigarRemark.setLineWrap(true);
        txtCigarRemark.setRows(3);
        jScrollPane9.setViewportView(txtCigarRemark);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridheight = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelRiskCigarete.add(jScrollPane9, gridBagConstraints);

        panelSmokeFeq1.setLayout(new java.awt.GridBagLayout());

        jLabel15.setFont(jLabel15.getFont());
        jLabel15.setText("�ӹǹ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        panelSmokeFeq1.add(jLabel15, gridBagConstraints);

        txtSmokeWeek.setEditable(false);
        txtSmokeWeek.setColumns(6);
        txtSmokeWeek.setFont(txtSmokeWeek.getFont());
        txtSmokeWeek.setMinimumSize(new java.awt.Dimension(54, 22));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        panelSmokeFeq1.add(txtSmokeWeek, gridBagConstraints);

        jLabel16.setFont(jLabel16.getFont());
        jLabel16.setText("�ǹ/�ѹ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        panelSmokeFeq1.add(jLabel16, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelRiskCigarete.add(panelSmokeFeq1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 5, 5);
        panelRisk.add(panelRiskCigarete, gridBagConstraints);

        panelExercise.setBorder(javax.swing.BorderFactory.createTitledBorder("����͡���ѧ���"));
        panelExercise.setLayout(new java.awt.GridBagLayout());

        buttonGroup5.add(rbtn0);
        rbtn0.setFont(rbtn0.getFont());
        rbtn0.setText("����͡���ѧ���");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelExercise.add(rbtn0, gridBagConstraints);

        buttonGroup5.add(rbtn1);
        rbtn1.setFont(rbtn1.getFont());
        rbtn1.setText("�͡���ѧ��� �ء�ѹ������ 30 �ҷ�");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelExercise.add(rbtn1, gridBagConstraints);

        buttonGroup5.add(rbtn2);
        rbtn2.setFont(rbtn2.getFont());
        rbtn2.setText("�͡���ѧ��� �ѻ�������ҡ���� 3 ���� ������ 30 �ҷ���������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelExercise.add(rbtn2, gridBagConstraints);

        buttonGroup5.add(rbtn3);
        rbtn3.setFont(rbtn3.getFont());
        rbtn3.setText("�͡���ѧ���  �ѻ������ 3 ���� ������ 30 �ҷ���������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelExercise.add(rbtn3, gridBagConstraints);

        buttonGroup5.add(rbtn4);
        rbtn4.setFont(rbtn4.getFont());
        rbtn4.setText("�͡���ѧ��� ���¡����ѻ������ 3 ����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelExercise.add(rbtn4, gridBagConstraints);

        buttonGroup5.add(rbtn9);
        rbtn9.setFont(rbtn9.getFont());
        rbtn9.setSelected(true);
        rbtn9.setText("�������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelExercise.add(rbtn9, gridBagConstraints);

        jScrollPane10.setBorder(javax.swing.BorderFactory.createTitledBorder("�����˵�"));

        txtExerciseRemark.setColumns(15);
        txtExerciseRemark.setFont(txtExerciseRemark.getFont());
        txtExerciseRemark.setLineWrap(true);
        txtExerciseRemark.setRows(3);
        jScrollPane10.setViewportView(txtExerciseRemark);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridheight = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelExercise.add(jScrollPane10, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 5, 5);
        panelRisk.add(panelExercise, gridBagConstraints);

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("������ǹ"));
        jPanel5.setLayout(new java.awt.GridBagLayout());

        jCheckBox_fat.setFont(jCheckBox_fat.getFont());
        jCheckBox_fat.setText("��ǹ�дѺ (�к�)");
        jCheckBox_fat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox_fatActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel5.add(jCheckBox_fat, gridBagConstraints);

        jScrollPane5.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        jTextArea_risk.setRows(3);
        jTextArea_risk.setFont(jTextArea_risk.getFont());
        jScrollPane5.setViewportView(jTextArea_risk);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel5.add(jScrollPane5, gridBagConstraints);

        buttonGroup3.add(jRadioButton_fat1);
        jRadioButton_fat1.setFont(jRadioButton_fat1.getFont());
        jRadioButton_fat1.setText("1");
        jRadioButton_fat1.setEnabled(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel5.add(jRadioButton_fat1, gridBagConstraints);

        buttonGroup3.add(jRadioButton_fat2);
        jRadioButton_fat2.setFont(jRadioButton_fat2.getFont());
        jRadioButton_fat2.setText("2");
        jRadioButton_fat2.setEnabled(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel5.add(jRadioButton_fat2, gridBagConstraints);

        jLabel6.setFont(jLabel6.getFont());
        jLabel6.setText("���� (�к�)");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel5.add(jLabel6, gridBagConstraints);

        buttonGroup3.add(jRadioButton_fat3);
        jRadioButton_fat3.setFont(jRadioButton_fat3.getFont());
        jRadioButton_fat3.setText("3");
        jRadioButton_fat3.setEnabled(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel5.add(jRadioButton_fat3, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridheight = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        panelRisk.add(jPanel5, gridBagConstraints);

        jToolBar6.setFloatable(false);
        jToolBar6.setRollover(true);

        jButtonSavePatientHistory2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/btn_save.png"))); // NOI18N
        jButtonSavePatientHistory2.setFocusable(false);
        jButtonSavePatientHistory2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonSavePatientHistory2.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonSavePatientHistory2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSavePatientHistory2ActionPerformed(evt);
            }
        });
        jToolBar6.add(jButtonSavePatientHistory2);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 2);
        panelRisk.add(jToolBar6, gridBagConstraints);

        jTabbedPane1.addTab("��������§", panelRisk);

        panelDrugAllergy.setLayout(new java.awt.GridBagLayout());

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("��¡���駢���������/�������������"));
        jPanel4.setLayout(new java.awt.GridBagLayout());

        table.setFont(table.getFont());
        table.setFillsViewportHeight(true);
        table.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane2.setViewportView(table);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel4.add(jScrollPane2, gridBagConstraints);

        jLabel5.setFont(jLabel5.getFont());
        jLabel5.setText("�����������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel4.add(jLabel5, gridBagConstraints);

        cbShowIntoleranceType.setFont(cbShowIntoleranceType.getFont());
        cbShowIntoleranceType.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbShowIntoleranceTypeActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel4.add(cbShowIntoleranceType, gridBagConstraints);

        jToolBar2.setFloatable(false);
        jToolBar2.setRollover(true);

        btnEditInformIntolerance.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/btn_edit.png"))); // NOI18N
        btnEditInformIntolerance.setFocusable(false);
        btnEditInformIntolerance.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnEditInformIntolerance.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnEditInformIntolerance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditInformIntoleranceActionPerformed(evt);
            }
        });
        jToolBar2.add(btnEditInformIntolerance);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_END;
        jPanel4.add(jToolBar2, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 0.5;
        panelDrugAllergy.add(jPanel4, gridBagConstraints);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("��¡���ҷ����"));
        jPanel3.setLayout(new java.awt.GridBagLayout());

        jPanel11.setLayout(new java.awt.GridBagLayout());

        jToolBar3.setFloatable(false);
        jToolBar3.setRollover(true);

        jButtonAddAllergy.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/btn_add.png"))); // NOI18N
        jButtonAddAllergy.setFocusable(false);
        jButtonAddAllergy.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonAddAllergy.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonAddAllergy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAddAllergyActionPerformed(evt);
            }
        });
        jToolBar3.add(jButtonAddAllergy);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_END;
        jPanel11.add(jToolBar3, gridBagConstraints);

        jToolBar4.setFloatable(false);
        jToolBar4.setRollover(true);

        jButtonDelAllergy.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/btn_delete.png"))); // NOI18N
        jButtonDelAllergy.setEnabled(false);
        jButtonDelAllergy.setFocusable(false);
        jButtonDelAllergy.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonDelAllergy.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonDelAllergy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDelAllergyActionPerformed(evt);
            }
        });
        jToolBar4.add(jButtonDelAllergy);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_END;
        jPanel11.add(jToolBar4, gridBagConstraints);

        jToolBar5.setFloatable(false);
        jToolBar5.setRollover(true);

        btnEditDrugAllergy.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/btn_edit.png"))); // NOI18N
        btnEditDrugAllergy.setEnabled(false);
        btnEditDrugAllergy.setFocusable(false);
        btnEditDrugAllergy.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnEditDrugAllergy.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnEditDrugAllergy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditDrugAllergyActionPerformed(evt);
            }
        });
        jToolBar5.add(btnEditDrugAllergy);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_END;
        jPanel11.add(jToolBar5, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        jPanel3.add(jPanel11, gridBagConstraints);

        tableDrugAllergy.setFont(tableDrugAllergy.getFont());
        tableDrugAllergy.setFillsViewportHeight(true);
        tableDrugAllergy.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tableDrugAllergy.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableDrugAllergyMouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tableDrugAllergyMouseReleased(evt);
            }
        });
        tableDrugAllergy.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tableDrugAllergyKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tableDrugAllergyKeyReleased(evt);
            }
        });
        jScrollPane3.setViewportView(tableDrugAllergy);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jScrollPane3, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 0.5;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 3, 0);
        panelDrugAllergy.add(jPanel3, gridBagConstraints);

        jTabbedPane1.addTab("����", panelDrugAllergy);

        panelPatientADL.setLayout(new java.awt.GridBagLayout());

        panelADL.setLayout(new java.awt.GridBagLayout());

        jPanel13.setLayout(new java.awt.GridBagLayout());

        btnAddAdl.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/btn_add16.png"))); // NOI18N
        btnAddAdl.setFocusable(false);
        btnAddAdl.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnAddAdl.setMaximumSize(new java.awt.Dimension(26, 26));
        btnAddAdl.setMinimumSize(new java.awt.Dimension(26, 26));
        btnAddAdl.setPreferredSize(new java.awt.Dimension(26, 26));
        btnAddAdl.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnAddAdl.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddAdlActionPerformed(evt);
            }
        });
        jPanel13.add(btnAddAdl, new java.awt.GridBagConstraints());

        btnDelAdl.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/btn_delete16.png"))); // NOI18N
        btnDelAdl.setEnabled(false);
        btnDelAdl.setFocusable(false);
        btnDelAdl.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnDelAdl.setMaximumSize(new java.awt.Dimension(26, 26));
        btnDelAdl.setMinimumSize(new java.awt.Dimension(26, 26));
        btnDelAdl.setPreferredSize(new java.awt.Dimension(26, 26));
        btnDelAdl.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnDelAdl.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDelAdlActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        jPanel13.add(btnDelAdl, gridBagConstraints);

        bynEditDel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/btn_edit16.png"))); // NOI18N
        bynEditDel.setEnabled(false);
        bynEditDel.setFocusable(false);
        bynEditDel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        bynEditDel.setMaximumSize(new java.awt.Dimension(26, 26));
        bynEditDel.setMinimumSize(new java.awt.Dimension(26, 26));
        bynEditDel.setPreferredSize(new java.awt.Dimension(26, 26));
        bynEditDel.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        bynEditDel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bynEditDelActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel13.add(bynEditDel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        panelADL.add(jPanel13, gridBagConstraints);

        tableAdl.setFont(tableAdl.getFont());
        tableAdl.setModel(modelAdl);
        tableAdl.setFillsViewportHeight(true);
        tableAdl.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tableAdl.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableAdlMouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tableAdlMouseReleased(evt);
            }
        });
        tableAdl.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tableAdlKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tableAdlKeyReleased(evt);
            }
        });
        jScrollPane11.setViewportView(tableAdl);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelADL.add(jScrollPane11, gridBagConstraints);

        jTabbedPane2.addTab("ADL", panelADL);

        panel2QPlus.setLayout(new java.awt.GridBagLayout());

        table2qPlus.setFont(table2qPlus.getFont());
        table2qPlus.setModel(model2QPlus);
        table2qPlus.setFillsViewportHeight(true);
        table2qPlus.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        table2qPlus.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                table2qPlusMouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                table2qPlusMouseReleased(evt);
            }
        });
        table2qPlus.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                table2qPlusKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                table2qPlusKeyReleased(evt);
            }
        });
        jScrollPane12.setViewportView(table2qPlus);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panel2QPlus.add(jScrollPane12, gridBagConstraints);

        jPanel14.setLayout(new java.awt.GridBagLayout());

        btnAdd2qPlus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/btn_add16.png"))); // NOI18N
        btnAdd2qPlus.setFocusable(false);
        btnAdd2qPlus.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnAdd2qPlus.setMaximumSize(new java.awt.Dimension(26, 26));
        btnAdd2qPlus.setMinimumSize(new java.awt.Dimension(26, 26));
        btnAdd2qPlus.setPreferredSize(new java.awt.Dimension(26, 26));
        btnAdd2qPlus.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnAdd2qPlus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAdd2qPlusActionPerformed(evt);
            }
        });
        jPanel14.add(btnAdd2qPlus, new java.awt.GridBagConstraints());

        btnDel2qPlus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/btn_delete16.png"))); // NOI18N
        btnDel2qPlus.setEnabled(false);
        btnDel2qPlus.setFocusable(false);
        btnDel2qPlus.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnDel2qPlus.setMaximumSize(new java.awt.Dimension(26, 26));
        btnDel2qPlus.setMinimumSize(new java.awt.Dimension(26, 26));
        btnDel2qPlus.setPreferredSize(new java.awt.Dimension(26, 26));
        btnDel2qPlus.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnDel2qPlus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDel2qPlusActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        jPanel14.add(btnDel2qPlus, gridBagConstraints);

        bynEdit2qPlus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/btn_edit16.png"))); // NOI18N
        bynEdit2qPlus.setEnabled(false);
        bynEdit2qPlus.setFocusable(false);
        bynEdit2qPlus.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        bynEdit2qPlus.setMaximumSize(new java.awt.Dimension(26, 26));
        bynEdit2qPlus.setMinimumSize(new java.awt.Dimension(26, 26));
        bynEdit2qPlus.setPreferredSize(new java.awt.Dimension(26, 26));
        bynEdit2qPlus.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        bynEdit2qPlus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bynEdit2qPlusActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel14.add(bynEdit2qPlus, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        panel2QPlus.add(jPanel14, gridBagConstraints);

        jTabbedPane2.addTab("2QPlus", panel2QPlus);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        panelPatientADL.add(jTabbedPane2, gridBagConstraints);

        jTabbedPane1.addTab("����٧����", panelPatientADL);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        add(jTabbedPane1, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jCheckBox_fmh_comActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox_fmh_comActionPerformed
        jTextField_fhx_com.setEnabled(jCheckBox_fmh_com.isSelected());
    }//GEN-LAST:event_jCheckBox_fmh_comActionPerformed

    private void jCheckBox_fmh_htActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox_fmh_htActionPerformed
        jTextField_fhx_ht.setEnabled(jCheckBox_fmh_ht.isSelected());
    }//GEN-LAST:event_jCheckBox_fmh_htActionPerformed

    private void jCheckBox_fmh_dmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox_fmh_dmActionPerformed
        jTextField_fhx_dm.setEnabled(jCheckBox_fmh_dm.isSelected());
    }//GEN-LAST:event_jCheckBox_fmh_dmActionPerformed

    private void jButtonDelAllergyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDelAllergyActionPerformed
        this.doDeleteDrugAllergy();
    }//GEN-LAST:event_jButtonDelAllergyActionPerformed

    private void jButtonAddAllergyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAddAllergyActionPerformed
        this.doOpenNewDrugAllergyAssessDialog();
    }//GEN-LAST:event_jButtonAddAllergyActionPerformed

    private void jTextArea_past_hxKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextArea_past_hxKeyReleased
    }//GEN-LAST:event_jTextArea_past_hxKeyReleased

    private void jButtonSavePatientHistoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSavePatientHistoryActionPerformed
        this.doSaveHistory();
    }//GEN-LAST:event_jButtonSavePatientHistoryActionPerformed

    private void rdVacNAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdVacNAActionPerformed
        txtVacUnCompleteDetail.setEnabled(rdVacUnComplete.isSelected());
    }//GEN-LAST:event_rdVacNAActionPerformed

    private void rdVacCompleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdVacCompleteActionPerformed
        txtVacUnCompleteDetail.setEnabled(rdVacUnComplete.isSelected());
    }//GEN-LAST:event_rdVacCompleteActionPerformed

    private void rdVacUnCompleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdVacUnCompleteActionPerformed
        txtVacUnCompleteDetail.setEnabled(rdVacUnComplete.isSelected());
    }//GEN-LAST:event_rdVacUnCompleteActionPerformed

    private void cbShowIntoleranceTypeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbShowIntoleranceTypeActionPerformed

        listTableData();     }//GEN-LAST:event_cbShowIntoleranceTypeActionPerformed

    private void btnEditInformIntoleranceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditInformIntoleranceActionPerformed
        this.doOpenInformIntoleranceDialog();
    }//GEN-LAST:event_btnEditInformIntoleranceActionPerformed

    private void tableDrugAllergyMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableDrugAllergyMouseClicked
        if (evt.getClickCount() == 2) {
            doOpenEditDrugAllergyAssessDialog();
        }
    }//GEN-LAST:event_tableDrugAllergyMouseClicked

    private void tableDrugAllergyKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tableDrugAllergyKeyPressed
        if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
            doOpenEditDrugAllergyAssessDialog();
        }
    }//GEN-LAST:event_tableDrugAllergyKeyPressed

    private void btnEditDrugAllergyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditDrugAllergyActionPerformed
        doOpenEditDrugAllergyAssessDialog();
    }//GEN-LAST:event_btnEditDrugAllergyActionPerformed

    private void tableDrugAllergyMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableDrugAllergyMouseReleased
        btnEditDrugAllergy.setEnabled(tableDrugAllergy.getSelectedRowCount() > 0 && theHO.theEmployee.authentication_id.equals(Authentication.PHARM));
        jButtonDelAllergy.setEnabled(tableDrugAllergy.getSelectedRowCount() > 0 && theHO.theEmployee.authentication_id.equals(Authentication.PHARM));
    }//GEN-LAST:event_tableDrugAllergyMouseReleased

    private void tableDrugAllergyKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tableDrugAllergyKeyReleased
        btnEditDrugAllergy.setEnabled(tableDrugAllergy.getSelectedRowCount() > 0 && theHO.theEmployee.authentication_id.equals(Authentication.PHARM));
        jButtonDelAllergy.setEnabled(tableDrugAllergy.getSelectedRowCount() > 0 && theHO.theEmployee.authentication_id.equals(Authentication.PHARM));
    }//GEN-LAST:event_tableDrugAllergyKeyReleased

    private void jCheckBox_fatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox_fatActionPerformed
        this.validateRiskUI();
        calculateObesityClass();
    }//GEN-LAST:event_jCheckBox_fatActionPerformed

    private void jrBtnNoDrinkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jrBtnNoDrinkActionPerformed
        this.validateRiskUI();
    }//GEN-LAST:event_jrBtnNoDrinkActionPerformed

    private void jrBtnInfrequentDrinkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jrBtnInfrequentDrinkActionPerformed
        this.validateRiskUI();
    }//GEN-LAST:event_jrBtnInfrequentDrinkActionPerformed

    private void jrBtnOccasionallyDrinkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jrBtnOccasionallyDrinkActionPerformed
        this.validateRiskUI();
    }//GEN-LAST:event_jrBtnOccasionallyDrinkActionPerformed

    private void jrBtnRegularlyDrinkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jrBtnRegularlyDrinkActionPerformed
        this.validateRiskUI();
    }//GEN-LAST:event_jrBtnRegularlyDrinkActionPerformed

    private void jrBtnQuitDrinkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jrBtnQuitDrinkActionPerformed
        this.validateRiskUI();
    }//GEN-LAST:event_jrBtnQuitDrinkActionPerformed

    private void jrBtnNotAskDrinkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jrBtnNotAskDrinkActionPerformed
        this.validateRiskUI();
    }//GEN-LAST:event_jrBtnNotAskDrinkActionPerformed

    private void jrBtnNoSmokeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jrBtnNoSmokeActionPerformed
        this.validateRiskUI();
    }//GEN-LAST:event_jrBtnNoSmokeActionPerformed

    private void jrBtnInfrequentSmokeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jrBtnInfrequentSmokeActionPerformed
        this.validateRiskUI();
    }//GEN-LAST:event_jrBtnInfrequentSmokeActionPerformed

    private void jrBtnOccasionallySmokeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jrBtnOccasionallySmokeActionPerformed
        this.validateRiskUI();
    }//GEN-LAST:event_jrBtnOccasionallySmokeActionPerformed

    private void jrBtnRegularlySmokeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jrBtnRegularlySmokeActionPerformed
        this.validateRiskUI();
    }//GEN-LAST:event_jrBtnRegularlySmokeActionPerformed

    private void jrBtnQuitSmokeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jrBtnQuitSmokeActionPerformed
        this.validateRiskUI();
    }//GEN-LAST:event_jrBtnQuitSmokeActionPerformed

    private void jrBtnNotAskSmokeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jrBtnNotAskSmokeActionPerformed
        this.validateRiskUI();
    }//GEN-LAST:event_jrBtnNotAskSmokeActionPerformed

    private void jButtonSavePatientHistory2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSavePatientHistory2ActionPerformed
        this.doSaveRisk();
    }//GEN-LAST:event_jButtonSavePatientHistory2ActionPerformed

    private void jCheckBox_fmh_FHActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox_fmh_FHActionPerformed
        jTextField_fhx_FH.setEnabled(jCheckBox_fmh_FH.isSelected());
    }//GEN-LAST:event_jCheckBox_fmh_FHActionPerformed

    private void jCheckBox_fmh_coronaryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox_fmh_coronaryActionPerformed
        jTextField_fhx_coronary.setEnabled(jCheckBox_fmh_coronary.isSelected());
    }//GEN-LAST:event_jCheckBox_fmh_coronaryActionPerformed

    private void btnAddAdlActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddAdlActionPerformed
        doOpenDialogAdl();
    }//GEN-LAST:event_btnAddAdlActionPerformed

    private void btnDelAdlActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDelAdlActionPerformed
        doDeleteAdl();
    }//GEN-LAST:event_btnDelAdlActionPerformed

    private void bynEditDelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bynEditDelActionPerformed
        doOpenEditDialogAdl();
    }//GEN-LAST:event_bynEditDelActionPerformed

    private void tableAdlMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableAdlMouseClicked
        updateUIAdl();
    }//GEN-LAST:event_tableAdlMouseClicked

    private void tableAdlMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableAdlMouseReleased
        updateUIAdl();
    }//GEN-LAST:event_tableAdlMouseReleased

    private void tableAdlKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tableAdlKeyPressed
        updateUIAdl();
    }//GEN-LAST:event_tableAdlKeyPressed

    private void tableAdlKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tableAdlKeyReleased
        updateUIAdl();
    }//GEN-LAST:event_tableAdlKeyReleased

    private void table2qPlusMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_table2qPlusMouseClicked
        updateUI2qPlus();
    }//GEN-LAST:event_table2qPlusMouseClicked

    private void table2qPlusMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_table2qPlusMouseReleased
        updateUI2qPlus();
    }//GEN-LAST:event_table2qPlusMouseReleased

    private void table2qPlusKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_table2qPlusKeyPressed
        updateUI2qPlus();
    }//GEN-LAST:event_table2qPlusKeyPressed

    private void table2qPlusKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_table2qPlusKeyReleased
        updateUI2qPlus();
    }//GEN-LAST:event_table2qPlusKeyReleased

    private void btnAdd2qPlusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAdd2qPlusActionPerformed
        doOpenDialog2qPlus();
    }//GEN-LAST:event_btnAdd2qPlusActionPerformed

    private void btnDel2qPlusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDel2qPlusActionPerformed
        doDelete2qPlus();
    }//GEN-LAST:event_btnDel2qPlusActionPerformed

    private void bynEdit2qPlusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bynEdit2qPlusActionPerformed
        doOpenEditDialog2qPlus();
    }//GEN-LAST:event_bynEdit2qPlusActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd2qPlus;
    private javax.swing.JButton btnAddAdl;
    private javax.swing.JButton btnDel2qPlus;
    private javax.swing.JButton btnDelAdl;
    private javax.swing.JButton btnEditDrugAllergy;
    private javax.swing.JButton btnEditInformIntolerance;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.ButtonGroup buttonGroup3;
    private javax.swing.ButtonGroup buttonGroup4;
    private javax.swing.ButtonGroup buttonGroup5;
    private javax.swing.JButton bynEdit2qPlus;
    private javax.swing.JButton bynEditDel;
    private javax.swing.JCheckBox cbG6PD;
    private javax.swing.JComboBox cbShowIntoleranceType;
    private com.hospital_os.utility.DateComboBox dateComboBox_sick_date;
    private javax.swing.JButton jButtonAddAllergy;
    private javax.swing.JButton jButtonDelAllergy;
    private javax.swing.JButton jButtonSavePatientHistory;
    private javax.swing.JButton jButtonSavePatientHistory2;
    private javax.swing.JCheckBox jCheckBox_fat;
    private javax.swing.JCheckBox jCheckBox_fmh_FH;
    private javax.swing.JCheckBox jCheckBox_fmh_com;
    private javax.swing.JCheckBox jCheckBox_fmh_coronary;
    private javax.swing.JCheckBox jCheckBox_fmh_dm;
    private javax.swing.JCheckBox jCheckBox_fmh_ht;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JRadioButton jRadioButton_fat1;
    private javax.swing.JRadioButton jRadioButton_fat2;
    private javax.swing.JRadioButton jRadioButton_fat3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane12;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTabbedPane jTabbedPane2;
    private com.hosv3.gui.component.BalloonTextArea jTextAreaPersonalDisease;
    private com.hosv3.gui.component.BalloonTextArea jTextArea_fhx_other;
    private com.hosv3.gui.component.BalloonTextArea jTextArea_past_hx;
    private com.hosv3.gui.component.BalloonTextArea jTextArea_risk;
    private javax.swing.JTextField jTextField_fhx_FH;
    private javax.swing.JTextField jTextField_fhx_com;
    private javax.swing.JTextField jTextField_fhx_coronary;
    private javax.swing.JTextField jTextField_fhx_dm;
    private javax.swing.JTextField jTextField_fhx_ht;
    private javax.swing.JTextField jTextField_has_sick;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JToolBar jToolBar2;
    private javax.swing.JToolBar jToolBar3;
    private javax.swing.JToolBar jToolBar4;
    private javax.swing.JToolBar jToolBar5;
    private javax.swing.JToolBar jToolBar6;
    private javax.swing.JCheckBox jcboxRecommendQuitDrink;
    private javax.swing.JCheckBox jcboxRecommendQuitSmoke;
    private javax.swing.JRadioButton jrBtnInfrequentDrink;
    private javax.swing.JRadioButton jrBtnInfrequentSmoke;
    private javax.swing.JRadioButton jrBtnNoDrink;
    private javax.swing.JRadioButton jrBtnNoSmoke;
    private javax.swing.JRadioButton jrBtnNotAskDrink;
    private javax.swing.JRadioButton jrBtnNotAskSmoke;
    private javax.swing.JRadioButton jrBtnOccasionallyDrink;
    private javax.swing.JRadioButton jrBtnOccasionallySmoke;
    private javax.swing.JRadioButton jrBtnQuitDrink;
    private javax.swing.JRadioButton jrBtnQuitSmoke;
    private javax.swing.JRadioButton jrBtnRegularlyDrink;
    private javax.swing.JRadioButton jrBtnRegularlySmoke;
    private javax.swing.JPanel panel2QPlus;
    private javax.swing.JPanel panelADL;
    private javax.swing.JPanel panelAlcoholFeq;
    private javax.swing.JPanel panelAlcoholFeq1;
    private javax.swing.JPanel panelDrugAllergy;
    private javax.swing.JPanel panelExercise;
    private javax.swing.JPanel panelHistory;
    private javax.swing.JPanel panelPatientADL;
    private javax.swing.JPanel panelRisk;
    private javax.swing.JPanel panelRiskAlcohol;
    private javax.swing.JPanel panelRiskCigarete;
    private javax.swing.JPanel panelSmokeFeq;
    private javax.swing.JPanel panelSmokeFeq1;
    private javax.swing.JRadioButton rbtn0;
    private javax.swing.JRadioButton rbtn1;
    private javax.swing.JRadioButton rbtn2;
    private javax.swing.JRadioButton rbtn3;
    private javax.swing.JRadioButton rbtn4;
    private javax.swing.JRadioButton rbtn9;
    private javax.swing.JRadioButton rdVacComplete;
    private javax.swing.JRadioButton rdVacNA;
    private javax.swing.JRadioButton rdVacUnComplete;
    private javax.swing.JTable table;
    private javax.swing.JTable table2qPlus;
    private javax.swing.JTable tableAdl;
    private javax.swing.JTable tableDrugAllergy;
    private javax.swing.JTextArea txtCigarRemark;
    private com.hospital_os.utility.IntegerTextField txtDrink;
    private javax.swing.JTextArea txtDrinkRemark;
    private com.hospital_os.utility.IntegerTextField txtDrinkWeek;
    private javax.swing.JTextArea txtExerciseRemark;
    private com.hospital_os.utility.IntegerTextField txtSmoke;
    private com.hospital_os.utility.IntegerTextField txtSmokeWeek;
    private javax.swing.JTextArea txtVacUnCompleteDetail;
    // End of variables declaration//GEN-END:variables

    private void setG6PD(G6PD theG6pd) {
        this.theG6pd = theG6pd;
        if (this.theG6pd == null) {
            this.theG6pd = new G6PD();
            this.theG6pd.t_patient_id = theHO.thePatient.getObjectId();
        }
        cbG6PD.setSelected(this.theG6pd.g_6_pd.equals("1"));
    }

    private G6PD getG6PD() {
        this.theG6pd.g_6_pd = cbG6PD.isSelected() ? "1" : "0";
        return this.theG6pd;
    }

    private void setPatentPastVaccine(PatientPastVaccine thePatientPastVaccine) {
        this.thePatientPastVaccine = thePatientPastVaccine;
        if (this.thePatientPastVaccine == null) {
            this.thePatientPastVaccine = new PatientPastVaccine();
            this.thePatientPastVaccine.t_patient_id = theHO.thePatient.getObjectId();
        }
        if ("0".equals(this.thePatientPastVaccine.patient_past_vaccine_complete)) {
            rdVacUnComplete.setSelected(true);
        } else if ("1".equals(this.thePatientPastVaccine.patient_past_vaccine_complete)) {
            rdVacComplete.setSelected(true);
        } else {
            rdVacNA.setSelected(true);
        }
        txtVacUnCompleteDetail.setText(rdVacUnComplete.isSelected() ? this.thePatientPastVaccine.patient_past_vaccine_no_comlete_name : "");
        txtVacUnCompleteDetail.setEnabled(rdVacUnComplete.isSelected());
    }

    private PatientPastVaccine getPatientPastVaccine() {
        this.thePatientPastVaccine.patient_past_vaccine_complete
                = rdVacUnComplete.isSelected() ? "0" : (rdVacComplete.isSelected() ? "1" : "9");
        this.thePatientPastVaccine.patient_past_vaccine_no_comlete_name = rdVacUnComplete.isSelected() ? txtVacUnCompleteDetail.getText() : "";
        return this.thePatientPastVaccine;
    }

    private void initTable() {
        table.setModel(tableModel);
        TableColumnModel cm = table.getColumnModel();
        DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();
        dtcr.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);
        cm.getColumn(0).setPreferredWidth(70);
        cm.getColumn(0).setCellRenderer(dtcr);
        cm.getColumn(1).setPreferredWidth(150);
        cm.getColumn(1).setCellRenderer(new CellRendererTextArea());
        cm.getColumn(2).setCellRenderer(new CellRendererDate(
                "yyyy-MM-dd,HH:mm",
                new Locale("th", "TH"),
                "dd-MMM-yyyy, HH:mm",
                new Locale("th", "TH")));

        cm.getColumn(2).setMinWidth(150);
        cm.getColumn(2).setMaxWidth(150);
        cm.getColumn(2).setPreferredWidth(150);
        cm.getColumn(3).setPreferredWidth(30);
        cm.getColumn(3).setCellRenderer(new CellRendererStatus());

        jScrollPane2.setViewportView(table);
        jScrollPane2.revalidate();
        jScrollPane2.repaint();

        tableDrugAllergy.setModel(tableModelDrugAllergy);
        cm = tableDrugAllergy.getColumnModel();
        dtcr.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);
        cm.getColumn(0).setPreferredWidth(100);
        cm.getColumn(0).setCellRenderer(new CellRendererHos(CellRendererHos.TOOL_TIP_TEXT));
        cm.getColumn(1).setPreferredWidth(100);
        cm.getColumn(1).setCellRenderer(new CellRendererTextArea());
        cm.getColumn(2).setCellRenderer(new CellRendererDate(
                "yyyy-MM-dd",
                new Locale("th", "TH"),
                "dd-MMM-yyyy",
                new Locale("th", "TH")));

        cm.getColumn(2).setMinWidth(100);
        cm.getColumn(2).setMaxWidth(100);
        cm.getColumn(2).setPreferredWidth(100);
        cm.getColumn(3).setCellRenderer(new CellRendererDate(
                "yyyy-MM-dd",
                new Locale("th", "TH"),
                "dd-MMM-yyyy",
                new Locale("th", "TH")));

        cm.getColumn(3).setMinWidth(100);
        cm.getColumn(3).setMaxWidth(100);
        cm.getColumn(3).setPreferredWidth(100);
        cm.getColumn(4).setPreferredWidth(50);
        cm.getColumn(4).setCellRenderer(new CellRendererHos(CellRendererHos.TOOL_TIP_TEXT));
        cm.getColumn(5).setPreferredWidth(30);
        cm.getColumn(5).setCellRenderer(new CellRendererHos(CellRendererHos.TOOL_TIP_TEXT));

        jScrollPane3.setViewportView(tableDrugAllergy);
        jScrollPane3.revalidate();
        jScrollPane3.repaint();

        tableAdl.getColumnModel().getColumn(0).setMaxWidth(180);
        tableAdl.getColumnModel().getColumn(0).setMinWidth(180);
        tableAdl.getColumnModel().getColumn(0).setPreferredWidth(180);
        tableAdl.getColumnModel().getColumn(0).setCellRenderer(new CellRendererHos(CellRendererHos.ALIGNMENT_CENTER));
        tableAdl.getColumnModel().getColumn(1).setMaxWidth(180);
        tableAdl.getColumnModel().getColumn(1).setMinWidth(180);
        tableAdl.getColumnModel().getColumn(1).setPreferredWidth(180);
        tableAdl.getColumnModel().getColumn(1).setCellRenderer(new CellRendererHos(CellRendererHos.ALIGNMENT_CENTER));
        tableAdl.getColumnModel().getColumn(2).setMaxWidth(120);
        tableAdl.getColumnModel().getColumn(2).setMinWidth(120);
        tableAdl.getColumnModel().getColumn(2).setPreferredWidth(120);
        tableAdl.getColumnModel().getColumn(2).setCellRenderer(new CellRendererHos(CellRendererHos.ALIGNMENT_CENTER));
        tableAdl.getColumnModel().getColumn(3).setCellRenderer(new CellRendererHos(CellRendererHos.TOOL_TIP_TEXT));

        table2qPlus.getColumnModel().getColumn(0).setMaxWidth(180);
        table2qPlus.getColumnModel().getColumn(0).setMinWidth(180);
        table2qPlus.getColumnModel().getColumn(0).setPreferredWidth(180);
        table2qPlus.getColumnModel().getColumn(0).setCellRenderer(new CellRendererHos(CellRendererHos.ALIGNMENT_CENTER));
        table2qPlus.getColumnModel().getColumn(1).setMaxWidth(180);
        table2qPlus.getColumnModel().getColumn(1).setMinWidth(180);
        table2qPlus.getColumnModel().getColumn(1).setPreferredWidth(180);
        table2qPlus.getColumnModel().getColumn(1).setCellRenderer(new CellRendererHos(CellRendererHos.ALIGNMENT_CENTER));
        table2qPlus.getColumnModel().getColumn(2).setCellRenderer(new CellRendererHos(CellRendererHos.TOOL_TIP_TEXT));
    }

    private void listTableData() {
        tableModel.getData().clear();
        tableModel.fireTableDataChanged();
        List<Object[]> list = theHC.thePatientControl.listAllergicReactionsByPId(theHC.theHO.thePatient.getObjectId(), Gutil.getGuiData(cbShowIntoleranceType));
        tableModel.getData().addAll(list);
        tableModel.fireTableDataChanged();
    }

    private void doOpenInformIntoleranceDialog() {
        if (theDialogInformIntolerance == null) {
            theDialogInformIntolerance = new DialogInformIntolerance(theUS.getJFrame(), true);
            theDialogInformIntolerance.setControl(theHC);
        }
        theDialogInformIntolerance.openDialog(theHC.theHO.thePatient);
        listTableData();
    }

    private void listTableDrugAllergy() {
        tableModelDrugAllergy.getData().clear();
        tableModelDrugAllergy.fireTableDataChanged();
        Vector<PatientDrugAllergy> list = theHC.thePatientControl.listPatientDrugAllergyByPId(theHC.theHO.thePatient.getObjectId());
        tableModelDrugAllergy.getData().addAll(list);
        tableModelDrugAllergy.fireTableDataChanged();
        btnEditDrugAllergy.setEnabled(tableDrugAllergy.getSelectedRowCount() > 0);
        jButtonDelAllergy.setEnabled(tableDrugAllergy.getSelectedRowCount() > 0);
    }

    private void doDeleteDrugAllergy() {
        PatientDrugAllergy pda = tableModelDrugAllergy.getSelectedPatientDrugAllergy(tableDrugAllergy.getSelectedRow());
        int ret = theHC.thePatientControl.deletePatientDrugAllergy(pda);
        if (ret == 1) {
            listTableDrugAllergy();
        }
    }

    private void doOpenNewDrugAllergyAssessDialog() {
        if (theHO.theVisit != null && theHO.theVisit.deny_allergy.equals("1")) {
            theUS.setStatus("�����»���ʸ��������������ö������¡���ҷ������", UpdateStatus.WARNING);
            return;
        }
        if (theDialogPatientDrugAllergyAssess == null) {
            theDialogPatientDrugAllergyAssess = new DialogPatientDrugAllergyAssess(theUS.getJFrame(), true);
            theDialogPatientDrugAllergyAssess.setControl(theHC);
        }
        theDialogPatientDrugAllergyAssess.openNewDialog(theHC.theHO.thePatient);
        listTableDrugAllergy();
    }

    private void doOpenEditDrugAllergyAssessDialog() {
        if (theHO.theEmployee.authentication_id.equals(Authentication.PHARM)) {
            if (theHO.theVisit != null && theHO.theVisit.deny_allergy.equals("1")) {
                theUS.setStatus("�����»���ʸ��������������ö����¡���ҷ������", UpdateStatus.WARNING);
                return;
            }
            if (theDialogPatientDrugAllergyAssess == null) {
                theDialogPatientDrugAllergyAssess = new DialogPatientDrugAllergyAssess(theUS.getJFrame(), true);
                theDialogPatientDrugAllergyAssess.setControl(theHC);
            }
            PatientDrugAllergy pda = tableModelDrugAllergy.getSelectedPatientDrugAllergy(tableDrugAllergy.getSelectedRow());
            theDialogPatientDrugAllergyAssess.openEditDialog(pda);
            listTableDrugAllergy();
        }
    }

    private void doSaveHistory() {
        theHC.thePatientControl.savePatientHistory(getPastHistoryV(),
                getFamilyHistory(),
                getPersonalDiseaseV(),
                getG6PD(),
                getPatientPastVaccine());
    }

    private void doSaveRisk() {
        theHC.thePatientControl.savePatientRisk(getRiskFactorV());
    }

    private void listTableAdl() {
        tableAdl.clearSelection();
        modelAdl.clearTable();
        modelAdl.setComplexDataSources(theHC.thePatientControl.listPatientAdl(theHO.thePatient.getObjectId()));
        modelAdl.fireTableDataChanged();
        updateUIAdl();
    }

    private void doDeleteAdl() {
        if (tableAdl.getSelectedRow() < 0) {
            return;
        }
        ComplexDataSource tcds = modelAdl.getSelectedComplexDataSource(tableAdl.getSelectedRow());
        int ret = theHC.thePatientControl.deletePatientADl((String) tcds.getId());
        if (ret == 1) {
            listTableAdl();
        }
    }

    private void doOpenDialogAdl() {
        if (theDialogPatientAdl == null) {
            theDialogPatientAdl = new DialogPatientAdl(theUS.getJFrame(), true);
            theDialogPatientAdl.setControl(theHC);
        }
        theDialogPatientAdl.showDialog(theHC.theHO.thePatient);
        listTableAdl();
    }

    private void updateUIAdl() {
        btnDelAdl.setEnabled(tableAdl.getSelectedRowCount() > 0);
        bynEditDel.setEnabled(tableAdl.getSelectedRowCount() > 0);
    }

    private void doOpenEditDialogAdl() {
        if (tableAdl.getSelectedRow() < 0) {
            return;
        }
        if (theDialogPatientAdl == null) {
            theDialogPatientAdl = new DialogPatientAdl(theUS.getJFrame(), true);
            theDialogPatientAdl.setControl(theHC);
        }
        ComplexDataSource tcds = modelAdl.getSelectedComplexDataSource(tableAdl.getSelectedRow());
        theDialogPatientAdl.showEditDialog(theHC.theHO.thePatient, (String) tcds.getId());
        listTableAdl();
    }

    private void listTable2qPlus() {
        table2qPlus.clearSelection();
        model2QPlus.clearTable();
        model2QPlus.setComplexDataSources(theHC.thePatientControl.listPatient2qPlus(theHO.thePatient.getObjectId()));
        model2QPlus.fireTableDataChanged();
        updateUI2qPlus();
    }

    private void doDelete2qPlus() {
        if (table2qPlus.getSelectedRow() < 0) {
            return;
        }
        ComplexDataSource tcds = model2QPlus.getSelectedComplexDataSource(table2qPlus.getSelectedRow());
        int ret = theHC.thePatientControl.deletePatient2qPlus((Patient2qPlus) tcds.getId());
        if (ret == 1) {
            listTable2qPlus();
        }
    }

    private void doOpenDialog2qPlus() {
        if (theDialogPatient2QPlus == null) {
            theDialogPatient2QPlus = new DialogPatient2QPlus(theUS.getJFrame(), true);
            theDialogPatient2QPlus.setControl(theHC);
        }
        theDialogPatient2QPlus.showDialog(theHC.theHO.thePatient);
        listTable2qPlus();
    }

    private void updateUI2qPlus() {
        btnDel2qPlus.setEnabled(table2qPlus.getSelectedRowCount() > 0);
        bynEdit2qPlus.setEnabled(table2qPlus.getSelectedRowCount() > 0);
    }

    private void doOpenEditDialog2qPlus() {
        if (table2qPlus.getSelectedRow() < 0) {
            return;
        }
        if (theDialogPatient2QPlus == null) {
            theDialogPatient2QPlus = new DialogPatient2QPlus(theUS.getJFrame(), true);
            theDialogPatient2QPlus.setControl(theHC);
        }
        ComplexDataSource tcds = model2QPlus.getSelectedComplexDataSource(table2qPlus.getSelectedRow());
        theDialogPatient2QPlus.showEditDialog(theHC.theHO.thePatient, (Patient2qPlus) tcds.getId());
        listTable2qPlus();
    }

    /**
     * Feature #175
     */
    private void calculateObesityClass() {
        if (jCheckBox_fat.isSelected()) {
            for (Object object : theHO.vVitalSign) {
                VitalSign vs = (VitalSign) object;
                if (vs.bmi == null || vs.bmi.isEmpty()) {
                    continue;
                }
                float bmi = Float.parseFloat(vs.bmi);
                if (bmi == 0.0f) {
                    continue;
                }
                if (bmi >= 40.0f) {
                    jRadioButton_fat3.setSelected(true);
                    break;
                } else if (bmi < 40.0f && bmi >= 35.0f) {
                    jRadioButton_fat2.setSelected(true);
                    break;
                } else if (bmi < 35.0f && bmi >= 30.0f) {
                    jRadioButton_fat1.setSelected(true);
                    break;
                }
            }
        }
    }

    private class TableModel extends AbstractTableModel {

        private static final long serialVersionUID = 1L;
        private final String[] columns;
        private List<Object[]> data = new ArrayList<Object[]>();

        TableModel(String[] columns) {
            this.columns = columns;
        }

        public Object[] getRow(int row) {
            return data.get(row);
        }

        @Override
        public int getRowCount() {
            return data.size();
        }

        @Override
        public int getColumnCount() {
            return columns.length;
        }

        @Override
        public String getColumnName(int col) {
            return columns[col];
        }

        @Override
        public Object getValueAt(int row, int col) {
            Object[] object = data.get(row);
            switch (col) {
                case 0:
                    return object[1] == null ? "" : object[1].toString();
                case 1:
                    return object[2] == null ? "" : object[2].toString();
                case 2:
                    return object[4] == null ? "" : object[4].toString();
                case 3:
                    return object[5] == null ? "" : object[5].toString();
                default:
                    return object[0] == null ? "" : object[0].toString();
            }
        }

        public List<Object[]> getData() {
            return data;
        }
    }

    private class CellRendererStatus extends JLabel implements TableCellRenderer {

        private static final long serialVersionUID = 1L;

        CellRendererStatus() {
            setHorizontalAlignment(CENTER);
        }

        @Override
        public Component getTableCellRendererComponent(JTable table,
                Object value,
                boolean isSelected,
                boolean hasFocus,
                int row, int column) {
            String str = (String) value;
            if (str.equals("1")) {
                setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/checked_icon.png")));
                setToolTipText("��Ǩ�ͺ����");
            } else if (str.equals("2")) {
                setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/inactive-icon.png")));
                setToolTipText("���Ѫ�Ѻ��Һ �͡�û����Թ");
            } else {
                setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/clock-icon.png")));
                setToolTipText("�͡�õ�Ǩ�ͺ");
            }
            if (isSelected) {
                this.setBackground(table.getSelectionBackground());
                this.setForeground(table.getSelectionForeground());
            } else {
                this.setBackground(table.getBackground());
                this.setForeground(table.getForeground());
            }
            return this;
        }
    }

    private class TableModelDrugAllergy extends AbstractTableModel {

        private static final long serialVersionUID = 1L;
        private final String[] columns;
        private Vector<PatientDrugAllergy> data = new Vector<PatientDrugAllergy>();

        TableModelDrugAllergy(String[] columns) {
            this.columns = columns;
        }

        public PatientDrugAllergy getRow(int row) {
            return data.get(row);
        }

        @Override
        public int getRowCount() {
            return data.size();
        }

        @Override
        public int getColumnCount() {
            return columns.length;
        }

        @Override
        public String getColumnName(int col) {
            return columns[col];
        }

        @Override
        public Object getValueAt(int row, int col) {
            PatientDrugAllergy object = data.get(row);
            switch (col) {
                case 0:
                    return object.generic_name;
                case 1:
                    return object.drug_allergy_symtom;
                case 2:
                    return object.drug_allergy_symtom_date;
                case 3:
                    return object.drug_allergy_report_date;
                case 4:
                    return object.naranjo_interpretation;
                case 5:
                    return object.allergy_warning;
                default:
                    return object;
            }
        }

        public Vector<PatientDrugAllergy> getData() {
            return data;
        }

        public PatientDrugAllergy getSelectedPatientDrugAllergy(int row) {
            return data.get(row);
        }
    }
    private static final Logger LOG = Logger.getLogger(PanelPatientHistory.class.getName());
}

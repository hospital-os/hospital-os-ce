/*
 * BooleanImageTableCellRenderer.java
 *
 * Created on 9 �ѹ��¹ 2545, 9:12 �.
 */
package com.hospital_os.utility;

import com.hospital_os.object.AppointmentStatus;
import com.hospital_os.object.ListTransfer;
import com.hospital_os.object.QueueLabStatus;
import com.hospital_os.object.QueueXrayStatus;
import com.hospital_os.object.SequenceData;
import com.hospital_os.object.Transfer;
import java.awt.Color;
import java.awt.Component;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import static javax.swing.SwingConstants.CENTER;
import static javax.swing.SwingConstants.RIGHT;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author henbe
 */
@SuppressWarnings("ClassWithoutLogger")
public class CellRendererHos extends JLabel implements TableCellRenderer {

    private static final long serialVersionUID = 1L;
    public static int DATE = 10;
    public static int VN = 11;
    public static int HN = 12;
    public static int STRING = 13;
    public static int ALLERGY = 14;
    public static int LAB_STATUS = 15;
    public static int APPOINTMENT_STATUS = 16;//amp:9/8/2549
    public static int DATE_TIME = 17;//sumo:31/8/2549
    public static int DATE_TIME2 = 22;//sumo:31/8/2549
    public static int TOOL_TIP_TEXT = 18;
    public static int TIME = 19;//henbe
    public static int DATASOURCE = 20;
    public static int STATUS = 21;
    public static int CLAIM_STATUS = 23;
    public static int CURRENCY = 24;
    public static int PAYMENT_STATUS = 25;
    public static int ALIGNMENT_CENTER = 26;
    public static int ALIGNMENT_RIGHT = 27;
    public static int ACTIVE_STATUS = 28;
    public static int BORROW_STATUS = 29;
    public static int TRANSFER_STATUS = 30;
    public static int AGE = 31;
    public static int URGENY_STATUS = 32;
    public static int ESI_STATUS = 33;
    public static int XRAY_STATUS = 34;
    public static int RANGE_AGE = 35;
    public static int HIGHLIGHT_QUEUE = 36;
    public static int NEWS_PEWS_SCORE = 37;
    public static int AMOUNT = 38;
    public static int SENT_STATUS = 39;

    private final DecimalFormat formatter = new DecimalFormat("###,###,##0.00");
    private final DecimalFormat formatterNumber = new DecimalFormat("###0.##");

    public static String getNormalTextVN(String render_vn) {
        int index = render_vn.indexOf('/');
        if (index != -1) {
            try {
                String number = render_vn.substring(0, index);
                String year = render_vn.substring(index + 1);
                if (year.length() == 2) {
                    year = "0" + year;
                }
                DecimalFormat d = new DecimalFormat();
                d.applyPattern("000000");
                number = d.format(Integer.parseInt(number));
                return year + number;
            } catch (Exception e) {
                return "";
            }
        }
        return render_vn;
    }

    public static String getNormalTextHN(String render_hn) {
        int value = Integer.parseInt(render_hn);
        DecimalFormat d = new DecimalFormat();
        d.applyPattern("000000000");
        return d.format(value);
    }

    //sumo:31/8/2549
    public static String getRenderTextDateTime(String date_time) {
        String ret_date_time = "";
        try {
            if (date_time.length() == 10) {
                ret_date_time = DateUtil.getDateToStringShort(DateUtil.getDateFromText(date_time), false);
            }
            if (date_time.length() > 10) {
                ret_date_time = DateUtil.getDateToStringShort(DateUtil.getDateFromText(date_time), true);
            }
            return ret_date_time;
        } catch (Exception e) {
            return "";
        }
    }
    private final int mode;// = 13;
    private String thePattern;
    //////////////////////

    public CellRendererHos(int mode) {
        this(mode, null);
    }

    public CellRendererHos(int mode, String pattern) {
        this.mode = mode;
        setOpaque(true);
        thePattern = pattern;
        formatter.setMaximumFractionDigits(2);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        if (table != null) {
            if (table.isCellEditable(row, column)) {
                setForeground(Color.blue);
            }
            if (isSelected) {
                this.setBackground(table.getSelectionBackground());
                this.setForeground(table.getSelectionForeground());
            } else {
                this.setBackground(table.getBackground());
                this.setForeground(table.getForeground());
            }

            setFont(table.getFont());
        }

        try {
            initLabel(table, value, row, column);
            return this;
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE, null, ex);
            return this;
        }
    }

    public void initLabel(JTable table, Object value, int row, int column) throws Exception {
        setText("");
        if (mode == TOOL_TIP_TEXT) {
            setText(value == null ? "" : value.toString());
            setToolTipText(value == null ? "" : value.toString());
        } else if (mode == AGE) {
            if (value instanceof Date) {
                Date date = (Date) value;
                Calendar dob = Calendar.getInstance();
                dob.setTime(date);
                Calendar today = Calendar.getInstance();
                int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
                if (today.get(Calendar.MONTH) < dob.get(Calendar.MONTH)) {
                    age--;
                } else if (today.get(Calendar.MONTH) == dob.get(Calendar.MONTH)
                        && today.get(Calendar.DAY_OF_MONTH) < dob.get(Calendar.DAY_OF_MONTH)) {
                    age--;
                }
                setText(String.valueOf(age));
            } else if (value instanceof String) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd", new Locale("th", "TH"));
                Calendar dob = Calendar.getInstance();
                dob.setTime(sdf.parse(value.toString()));
                Calendar today = Calendar.getInstance();
                int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
                if (today.get(Calendar.MONTH) < dob.get(Calendar.MONTH)) {
                    age--;
                } else if (today.get(Calendar.MONTH) == dob.get(Calendar.MONTH)
                        && today.get(Calendar.DAY_OF_MONTH) < dob.get(Calendar.DAY_OF_MONTH)) {
                    age--;
                }
                setText(String.valueOf(age));
            }
            setToolTipText(getText());
            setHorizontalAlignment(CENTER);
        } else if (mode == DATE) {
            if (value instanceof Date) {
                Date date = (Date) value;
                setText(DateUtil.getDateShotToString(date, false));
            } else if (value instanceof String) {
                setText(getRenderTextDateTime(String.valueOf(value)));
            }
            setToolTipText(getText());
            setHorizontalAlignment(CENTER);
        } else if (mode == VN) {
            String vn = (String) value;
            if (vn.startsWith("1")) {
                this.setForeground(Color.RED);
            } else {
                this.setForeground(Color.BLACK);
            }
            if (thePattern != null) {
                setText(SequenceData.getGuiText(thePattern, vn) + " ");
                setHorizontalAlignment(RIGHT);
            } else {
                setText(vn);
                setHorizontalAlignment(CENTER);
            }

        } else if (mode == HN) {
            String hn = (String) value;
            if (thePattern != null) {
                setText(SequenceData.getGuiText(thePattern, hn) + " ");
                setHorizontalAlignment(RIGHT);
            } else {
                setText(hn);
                setHorizontalAlignment(CENTER);
            }

        } else if (mode == ALLERGY) {
            String allergy = (String) value;
            if (allergy != null && allergy.equals("1")) {
                setIcon(new ImageIcon(getClass().getResource(Gutil.getTextBundleImage("DRUGALLERGY"))));
            } else {
                setIcon(null);
            }
            setHorizontalAlignment(CENTER);
        } else if (mode == LAB_STATUS) {
            String status = (String) value;
            if (status.equals(QueueLabStatus.NOLAB)) {
                setToolTipText(QueueLabStatus.NOLAB_STR);
                setIcon(new ImageIcon(getClass().getResource(QueueLabStatus.NOLAB_FN)));
            } else if (status.equals(QueueLabStatus.REPORT)) {
                setToolTipText(QueueLabStatus.REPORT_STR);
                setIcon(new ImageIcon(getClass().getResource(QueueLabStatus.REPORT_FN)));
            } else if (status.equals(QueueLabStatus.SOMEREPORT)) {
                setToolTipText(QueueLabStatus.SOMEREPORT_STR);
                setIcon(new ImageIcon(getClass().getResource(QueueLabStatus.SOMEREPORT_FN)));
            } else if (status.equals(QueueLabStatus.WAIT)) {
                setToolTipText(QueueLabStatus.WAIT_STR);
                setIcon(new ImageIcon(getClass().getResource(QueueLabStatus.WAIT_FN)));
            } else if (status.equals(QueueLabStatus.REMAIN)) {
                setToolTipText(QueueLabStatus.REMAIN_STR);
                setIcon(new ImageIcon(getClass().getResource(QueueLabStatus.REMAIN_FN)));
            } else {
                setIcon(null);
                setToolTipText(null);
            }
            setHorizontalAlignment(CENTER);
        } else if (mode == APPOINTMENT_STATUS)//amp:9/8/2549
        {
            String status = (String) value;
            if (status.equals(AppointmentStatus.WAIT)) {
                setToolTipText(AppointmentStatus.WAIT_STR);
                setIcon(new ImageIcon(getClass().getResource(AppointmentStatus.WAIT_FN)));
            } else if (status.equals(AppointmentStatus.COMPLETE)) {
                setToolTipText(AppointmentStatus.COMPLETE_STR);
                setIcon(new ImageIcon(getClass().getResource(AppointmentStatus.COMPLETE_FN)));
            } else if (status.equals(AppointmentStatus.MISS)) {
                setToolTipText(AppointmentStatus.MISS_STR);
                setIcon(new ImageIcon(getClass().getResource(AppointmentStatus.MISS_FN)));
            } else if (status.equals(AppointmentStatus.CANCEL)) {
                setToolTipText(AppointmentStatus.CANCEL_STR);
                setIcon(new ImageIcon(getClass().getResource(AppointmentStatus.CANCEL_FN)));
            } else if (status.equals(AppointmentStatus.BEFORE)) {
                setToolTipText(AppointmentStatus.BEFORE_STR);
                setIcon(new ImageIcon(getClass().getResource(AppointmentStatus.BEFORE_FN)));
            } else if (status.equals(AppointmentStatus.AFTER)) {
                setToolTipText(AppointmentStatus.AFTER_STR);
                setIcon(new ImageIcon(getClass().getResource(AppointmentStatus.AFTER_FN)));
            } else if (status.equals(AppointmentStatus.CONFIRM)) {
                setToolTipText(AppointmentStatus.CONFIRM_STR);
                setIcon(new ImageIcon(getClass().getResource(AppointmentStatus.CONFIRM_FN)));
            } else {
                setIcon(null);
                setToolTipText(null);
            }
            setHorizontalAlignment(CENTER);
        } else if (mode == DATE_TIME) {
            if (value instanceof Date) {
                Date date = (Date) value;
                if (date != null) {
                    setText(DateUtil.getDateShotToString(date, true));
                }
            } else if (value instanceof String) {
                setText(getRenderTextDateTime(String.valueOf(value)));
            }
            setToolTipText(getText());
            setHorizontalAlignment(CENTER);
        } else if (mode == DATE_TIME2) {
            if (value instanceof Date) {
                Date date = (Date) value;
                if (date != null) {
                    setText(DateUtil.getDateToString(date, true));
                }
            } else if (value instanceof String) {
                setText(getRenderTextDateTime(String.valueOf(value)));
            }
            setToolTipText(getText());
            setHorizontalAlignment(CENTER);
        } else if (mode == TIME) {
            String time = (String) value;
            if (time.length() > 14) {
                time = time.substring(11);
            }
            if (time.length() == 10) {
                time = "";
            }
            setText(time);
            setHorizontalAlignment(CENTER);
        } else if (mode == DATASOURCE) {
            if (value instanceof DataSource) {
                DataSource dataSource = (DataSource) value;
                setText(dataSource.getValue());
            }
        } else if (mode == STATUS) {
            String txt = value + "";
            String arr[] = txt.split("\\^");
            if (arr.length >= 2) {
                setToolTipText("<html><body>" + arr[1] + "</body></html>");
            } else {
                setToolTipText("");
            }
            if (arr[0].equals("0")) {
                setIcon(new ImageIcon(getClass().getResource(QueueLabStatus.NOLAB_FN)));
                setToolTipText(getToolTipText().isEmpty() ? "<html><body><font color=red><b>�������¡���Ż</b></font></body></html>" : getToolTipText());
            } else if (arr[0].equals("1")) {
                setIcon(new ImageIcon(getClass().getResource(QueueLabStatus.WAIT_FN)));
                setToolTipText(getToolTipText().isEmpty() ? "<html><body><font color=red><b>�ͼ�</b></font></body></html>" : getToolTipText());
            } else if (arr[0].equals(QueueLabStatus.REPORT) || arr[0].equals("6")) {
                setToolTipText(getToolTipText().isEmpty() ? "<html><body><font color=green><b>��§ҹ�Ťú����</b></font></body></html>" : getToolTipText());
                setIcon(new ImageIcon(getClass().getResource(QueueLabStatus.REPORT_FN)));
            } else if (arr[0].equals(QueueLabStatus.SOMEREPORT) || arr[0].equals("7")) {
                setToolTipText(getToolTipText().isEmpty() ? "<html><body><font color=red><b>��§ҹ�źҧ��ǹ</b></font></body></html>" : getToolTipText());
                setIcon(new ImageIcon(getClass().getResource(QueueLabStatus.SOMEREPORT_FN)));
            } else if (arr[0].equals(QueueLabStatus.REMAIN)) {
                setToolTipText(getToolTipText().isEmpty() ? "<html><body><font color=red><b>��ҧ��</b></font></body></html>" : getToolTipText());
                setIcon(new ImageIcon(getClass().getResource(QueueLabStatus.REMAIN_FN)));
            } else if (arr[0].equals("8")) {
                setIcon(new ImageIcon(getClass().getResource("/com/hospital_os/images/ball_red.gif")));
            }
            setHorizontalAlignment(CENTER);
        } else if (mode == CLAIM_STATUS) {
            String status = (String) value;
            if (status.equals("0")) {
                setToolTipText("͹��ѵ�");
                setIcon(new ImageIcon(getClass().getResource("/com/hosv3/gui/images/active-icon.png")));
            } else if (status.equals("1")) {
                setToolTipText("��͹��ѵ�");
                setIcon(new ImageIcon(getClass().getResource("/com/hosv3/gui/images/clock-icon.png")));
            } else if (status.equals("2")) {
                setToolTipText("���͹��ѵ�");
                setIcon(new ImageIcon(getClass().getResource("/com/hosv3/gui/images/inactive-icon.png")));
            } else {
                setToolTipText(null);
                setIcon(null);
            }
            setHorizontalAlignment(CENTER);
        } else if (mode == PAYMENT_STATUS) {
            String status = (String) value;
            if (status.equals("0")) {
                setToolTipText("�ѧ������");
                setIcon(new ImageIcon(getClass().getResource("/com/hosv3/gui/images/inactive-icon.png")));
            } else if (status.equals("1")) {
                setToolTipText("��������");
                setIcon(new ImageIcon(getClass().getResource("/com/hosv3/gui/images/active-icon.png")));
            } else if (status.equals("2")) {
                setToolTipText("¡��ԡ");
                setIcon(new ImageIcon(getClass().getResource("/com/hosv3/gui/images/btn_delete16.png")));
            } else {
                setToolTipText(null);
                setIcon(null);
            }
            setHorizontalAlignment(CENTER);
        } else if (mode == ACTIVE_STATUS) {
            String status = (String) value;
            if (status.equals("0")) {
                setToolTipText("����������");
                setIcon(new ImageIcon(getClass().getResource("/com/hosv3/gui/images/inactive-icon.png")));
            } else if (status.equals("1")) {
                setToolTipText("�ѧ���������");
                setIcon(new ImageIcon(getClass().getResource("/com/hosv3/gui/images/active-icon.png")));
            } else {
                setToolTipText(null);
                setIcon(null);
            }
            setHorizontalAlignment(CENTER);
        } else if (mode == BORROW_STATUS) {
            String status = (String) value;
            if (status.equals("0")) {
                setToolTipText("�ѧ������Ѻ�׹");
                setIcon(new ImageIcon(getClass().getResource("/com/hosv3/gui/images/inactive-icon.png")));
            } else if (status.equals("1")) {
                setToolTipText("�׹�������");
                setIcon(new ImageIcon(getClass().getResource("/com/hosv3/gui/images/active-icon.png")));
            } else {
                setToolTipText(null);
                setIcon(null);
            }
            setHorizontalAlignment(CENTER);
        } else if (mode == TRANSFER_STATUS) {
            String status = (String) value;
            if (status.equals(Transfer.STATUS_WAIT)) {
                setToolTipText("�����¡��Ҩش��ԡ��");
                setIcon(new ImageIcon(getClass().getResource("/com/hosv3/gui/images/inactive-icon.png")));
            } else if (status.equals(Transfer.STATUS_PROCESS)) {
                setToolTipText("����㹨ش��ԡ��");
                setIcon(new ImageIcon(getClass().getResource("/com/hosv3/gui/images/wait_active-icon.png")));
            } else if (status.equals(Transfer.STATUS_COMPLETE)) {
                setToolTipText("�͡�ҡ�ش��ԡ������");
                setIcon(new ImageIcon(getClass().getResource("/com/hosv3/gui/images/active-icon.png")));
            } else {
                setToolTipText(null);
                setIcon(null);
            }
            setHorizontalAlignment(CENTER);
        } else if (mode == CURRENCY) {
            setHorizontalAlignment(SwingConstants.RIGHT);
            setText(formatter.format(value == null ? 0
                    : (value instanceof String ? Double.parseDouble(value.toString().isEmpty() ? "0" : value.toString()) : value)));
        } else if (mode == ALIGNMENT_CENTER) {
            setText(value == null ? "" : value.toString());
            setToolTipText(value == null ? "" : value.toString());
            setHorizontalAlignment(CENTER);
        } else if (mode == ALIGNMENT_RIGHT) {
            setText(value == null ? "" : value.toString());
            setToolTipText(value == null ? "" : value.toString());
            setHorizontalAlignment(RIGHT);
        } else if (mode == URGENY_STATUS) {
            String status = (String) value;
            if (ListTransfer.SHOW_ALL.equals(status)) {
                setText("<html><font color=red size=3><b>STAT</b></html>");
                setIcon(new ImageIcon(getClass().getResource("/com/hosv3/gui/images/urgeny.png")));
                setHorizontalTextPosition(SwingConstants.LEFT);
                setToolTipText("Stat & Urgeny");
            } else if (ListTransfer.SHOW_STAT.equals(status)) {
                setIcon(null);
                setText("<html><font color=red size=3><b>STAT</b></html>");
                setToolTipText("Stat");
            } else if (ListTransfer.SHOW_URGENT.equals(status)) {
                setIcon(new ImageIcon(getClass().getResource("/com/hosv3/gui/images/urgeny.png")));
                setToolTipText("Urgeny");
            } else {
                setIcon(null);
                setToolTipText(null);
                setText(null);
            }
            setHorizontalAlignment(CENTER);
        } else if (mode == ESI_STATUS) {
            String status = (String) value;
            if ("1".equals(status)) {
                setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/nu@24.png")));
                setBackground(Color.white);
                setToolTipText("Non - Urgency");
            } else if ("2".equals(status)) {
                setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/su@24.png")));
                setBackground(Color.green);
                setToolTipText("Semi - Urgency");
            } else if ("3".equals(status)) {
                setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/ur@24.png")));
                setBackground(Color.yellow);
                setToolTipText("Urgency");
            } else if ("4".equals(status)) {
                setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/em@24.png")));
                setBackground(Color.pink);
                setToolTipText("Emergency");
            } else if ("5".equals(status)) {
                setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/rs@24.png")));
                setBackground(Color.red);
                setToolTipText("Resuscitation");
            } else {
                setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/na@24.png")));
                setBackground(Color.lightGray);
                setToolTipText("Undefine");
            }
            setHorizontalAlignment(CENTER);
        } else if (mode == XRAY_STATUS) {
            String status = (String) value;
            if (status.equals(QueueXrayStatus.NOXRAY)) {
                setToolTipText(QueueXrayStatus.NOLAB_STR);
                setIcon(new ImageIcon(getClass().getResource(QueueXrayStatus.NOXRAY_FN)));
            } else if (status.equals(QueueXrayStatus.REPORT)) {
                setToolTipText(QueueXrayStatus.REPORT_STR);
                setIcon(new ImageIcon(getClass().getResource(QueueXrayStatus.REPORT_FN)));
            } else if (status.equals(QueueXrayStatus.WAIT)) {
                setToolTipText(QueueXrayStatus.WAIT_STR);
                setIcon(new ImageIcon(getClass().getResource(QueueXrayStatus.WAIT_FN)));
            } else {
                setIcon(null);
                setToolTipText(null);
            }
            setHorizontalAlignment(CENTER);
        } else if (mode == RANGE_AGE) {
            String status = (String) value;
            if (status != null && !status.isEmpty()) {
                setText("<html><font color=blue size=4><b>" + status + "</b></html>");
                setToolTipText(status);
            } else {
                setText(null);
                setToolTipText(null);
            }
            setHorizontalAlignment(CENTER);
        } else if (mode == HIGHLIGHT_QUEUE) {
            String[] temps = ((String) value).split("[|]");
            setText(temps[0] == null ? "" : temps[0]);
            if (temps.length >= 2 && temps[1].equals("2")) {
                setBackground(new Color(244, 132, 13));
                setToolTipText("���¡�����������ҵԴ���");
            } else if (temps.length >= 2 && temps[1].equals("1")) {
                setBackground(new Color(0, 204, 0));
                setToolTipText("���¡�������");
            } else {
                setBackground(getBackground());
                setToolTipText(getText());
            }
            setHorizontalAlignment(RIGHT);
        } else if (mode == NEWS_PEWS_SCORE) {
            if (value == null || value.toString().isEmpty()) {
                setText("");
                setToolTipText("");
                return;
            }
            String[] temps = ((String) value).split("[|]");
            String score = "<center>" + temps[0] + "</center>";
            String time = "<p style=\"color:orange\"><b>��Ǩ�ͺ��õ�駤�Ҥ�ṹ</b></p>";
            if (temps.length >= 2) {
                Date date = DateUtil.convertStringToDate(temps[1], "yyyy-MM-dd HH:mm:ss", DateUtil.LOCALE_EN);
                boolean isOvertime = DateUtil.beforeDate(date, DateUtil.getTimeStamp());
                String color = isOvertime ? "red" : "blue";
                String label = isOvertime ? "�Թ����" : "���駶Ѵ�";
                String dateThai = DateUtil.convertDateToString(date, "dd MMM yyyy HH:mm", DateUtil.LOCALE_TH);

                time = "<p style=\"color:" + color + "\"><b>" + label + " " + dateThai + "</b></p>";
            }
            setText("<html>" + score + time + "</html>");
            setToolTipText("<html>" + time + "</html>");
            setHorizontalAlignment(CENTER);
        } else if (mode == SENT_STATUS) {
            String status = String.valueOf(value);
            if (status.equals("0")) {
                setToolTipText("�ѧ���١�觢�����");
                setIcon(new ImageIcon(getClass().getResource("/com/hosv3/gui/images/inactive-icon.png")));
            } else if (status.equals("1")) {
                setToolTipText("�������");
                setIcon(new ImageIcon(getClass().getResource("/com/hosv3/gui/images/active-icon.png")));
            } else if (status.equals("2")) {
                setToolTipText("����������");
                setIcon(new ImageIcon(getClass().getResource("/com/hosv3/gui/images/btn_delete16.png")));
            } else {
                setToolTipText(null);
                setIcon(null);
            }
            setHorizontalAlignment(CENTER);
        } else if (mode == AMOUNT) {
            String amount;
            if (value instanceof Number) {
                amount = formatterNumber.format(value);
            } else {
                amount = String.valueOf(value);
            }
            setText(amount.lastIndexOf(".0") == -1
                    || Integer.parseInt(amount.substring(amount.indexOf('.') + 1)) > 0
                    ? amount : amount.substring(0, amount.indexOf('.')));
            setToolTipText(getText());
            setHorizontalAlignment(RIGHT);
        } else {
            String string = value == null ? "" : value.toString();
            setText(string);
            setToolTipText(string);
        }
    }
}

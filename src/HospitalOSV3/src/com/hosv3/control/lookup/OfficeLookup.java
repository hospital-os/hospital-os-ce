/*
 * DrugInstructionLookup.java
 *
 * Created on 21 �á�Ҥ� 2548, 10:55 �.
 */
package com.hosv3.control.lookup;

/*
 * PrescriberLookup.java
 *
 * Created on 9 ��Ȩԡ�¹ 2546, 9:23 �.
 */
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.LookupControlInf;
import com.hosv3.control.LookupControl;
//import com.hosv3.utility.*;

/**
 *
 * @author henbe
 */
public class OfficeLookup implements LookupControlInf {

    private LookupControl thePC;

    /**
     * Creates a new instance of PrescriberLookup
     */
    public OfficeLookup(LookupControl pc) {
        thePC = pc;
    }

    @Override
    @SuppressWarnings("UseOfObsoleteCollectionType")
    public java.util.Vector listData(String str) {
        return thePC.listOfficeByName(str);
    }

    public CommonInf readData(String pk) {
        return thePC.readHospitalByCode(pk);
    }

    @Override
    public CommonInf readHosData(String pk) {
        return thePC.readHospitalByCode(pk);
    }
}

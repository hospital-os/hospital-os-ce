/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author tanakrit
 */
public class ApiPaymentStatus extends Persistent {

    public static int PENDING = 1;
    public static int COMPLETED = 2;
    public static int DECLINED = 3;
    public static int FAILED = 4;

    public String description;
}

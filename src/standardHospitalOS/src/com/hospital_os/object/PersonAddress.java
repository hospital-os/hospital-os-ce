/*
 * To change this template;public String choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class PersonAddress extends Persistent {

    public String t_person_id;
    public String f_address_housetype_id;
    public String person_address_houseid;
    public String person_address_roomno;
    public String person_address_building;
    public String person_address_house;
    public String person_address_moo;
    public String person_address_villaname;
    public String person_address_soisub;
    public String person_address_soimain;
    public String person_address_road;
    public String person_address_tambon;
    public String person_address_amphur;
    public String person_address_changwat;
    public String person_address_telephone;
    public String person_address_mobile;
    public double person_address_latitude;
    public double person_address_longitude;
    public String person_address_other_country;
    public String person_address_other_country_active = "0";
    public String f_address_type_id;
    public String active = "1";
    public String user_record_id;
    public String record_date_time;
    public String user_modify_id;
    public String modify_date_time;
}

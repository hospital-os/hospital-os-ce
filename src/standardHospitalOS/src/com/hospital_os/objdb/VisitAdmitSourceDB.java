/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.ComboFix;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tanakrit
 */
public class VisitAdmitSourceDB {

    public ConnectionInf theConnectionInf;

    public VisitAdmitSourceDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public List<ComboFix> selectAll() throws Exception {
        String sql = "select * from f_visit_admit_source order by description";
        return ePQuery(sql);
    }

    public List<ComboFix> selectByKeyword(String keyword) throws Exception {
        String sql = "select * from f_visit_admit_source \n";
        if (keyword != null && !keyword.isEmpty()) {
            sql += " where (description ilike ? or code ilike ?) \n";
        }
        sql += "order by f_visit_admit_source_id";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            if (keyword != null && !keyword.isEmpty()) {
                ePQuery.setString(index++, "%" + keyword + "%");
                ePQuery.setString(index++, "%" + keyword + "%");
            }
            return ePQuery(ePQuery.toString());
        }
    }

    public List<CommonInf> getComboboxDatasource(String keyword) throws Exception {
        List<CommonInf> list = new ArrayList<>();
        for (ComboFix obj : selectByKeyword(keyword)) {
            list.add((CommonInf) obj);
        }
        return list;
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<>();
        for (ComboFix obj : selectByKeyword("")) {
            list.add((CommonInf) obj);
        }
        return list;
    }

    public List<ComboFix> ePQuery(String sql) throws Exception {
        ComboFix p;
        List<ComboFix> list = new ArrayList<>();
        try (ResultSet rs = theConnectionInf.eQuery(sql)) {
            while (rs.next()) {
                p = new ComboFix();
                p.code = rs.getString("f_visit_admit_source_id");
                p.name = (rs.getString("code") + " : " + rs.getString("description"));
                list.add(p);
            }
        }
        return list;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.objdb.objdbclass;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.pcu.object.FRehabilitationCode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sompr
 */
public class FRehabilitationCodeDB {

    private final ConnectionInf connectionInf;

    public FRehabilitationCodeDB(ConnectionInf db) {
        connectionInf = db;
    }

    public FRehabilitationCode select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_rehabilitation_code where f_rehabilitation_code_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<FRehabilitationCode> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<FRehabilitationCode> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_rehabilitation_code order by f_rehabilitation_code_id";
            preparedStatement = connectionInf.ePQuery(sql);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<FRehabilitationCode> list(String... ids) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_rehabilitation_code where f_rehabilitation_code_id in (?) order by f_rehabilitation_code_id";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setArray(1, connectionInf.getConnection().createArrayOf("varchar", ids));
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<FRehabilitationCode> listByKeyword(String keyword) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_rehabilitation_code where f_rehabilitation_code.f_rehabilitation_code_id||' : '|| \n"
                    + "(case when f_rehabilitation_code.description_th != '' \n"
                    + "then f_rehabilitation_code.description_th else f_rehabilitation_code.description_en end)  ilike ? order by f_rehabilitation_code_id";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, "%" + (keyword == null || keyword.isEmpty() ? "" : keyword) + "%");
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<FRehabilitationCode> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<FRehabilitationCode> list = new ArrayList<FRehabilitationCode>();
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()) {
            FRehabilitationCode obj = new FRehabilitationCode();
            obj.setObjectId(rs.getString("f_rehabilitation_code_id"));
            obj.description_th = rs.getString("description_th");
            obj.description_en = rs.getString("description_en");
            list.add(obj);
        }
        rs.close();
        return list;
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (FRehabilitationCode obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }

    public List<CommonInf> getComboboxDatasource(String... ids) throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (FRehabilitationCode obj : list(ids)) {
            list.add((CommonInf) obj);
        }
        return list;
    }
}

/*
 * result_lab.java
 *
 * Created on 18 ���Ҥ� 2546, 18:03 �.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author tong
 */
public class ResultLab extends Persistent {

    public String visit_id = "";
    public String order_item_id = "";
    public String result = "";
    public String unit = "";
    public String reporter = "";
    public String reported_time = "";
    public String name = "";
    public String active = "1";
    public String result_complete = "";
    public String item_id = "";
    public String result_type_id = "";
    public String min = "";
    public String critical_min = "";
    public String max = "";
    public String critical_max = "";
    public String result_group_id = "";
    public String index = "";
    public String lab_result_item_id = "";
    public String flag = ""; // receive data from LIS
    public String normal_range_type = "1";
    public String[] default_values = new String[]{};

}

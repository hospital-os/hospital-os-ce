/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.subject;

import com.hosv3.usecase.transaction.ManageServiceLoader;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class ServiceLoaderSubject implements ManageServiceLoader {

    private final List<ManageServiceLoader> list = new ArrayList<ManageServiceLoader>();

    public void removeAttach() {
        list.clear();

    }

    public void attachManage(ManageServiceLoader o) {
        list.add(o);
    }

    public boolean detach(ManageServiceLoader o) {
        return list.remove(o);
    }

    @Override
    public void notifyLoaderService() {
        for (int i = 0, size = list.size(); i < size; i++) {
            ((ManageServiceLoader) list.get(i)).notifyLoaderService();
        }
    }
}

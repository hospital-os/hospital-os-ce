/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.NewsPewsScore;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tanakrit
 */
public class NewsPewsScoreDB {

    public ConnectionInf theConnectionInf;
    final public String idtable = "981";

    public NewsPewsScoreDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public int insert(NewsPewsScore p) throws Exception {
        p.generateOID(idtable);
        String sql = "insert into b_news_pews_score ( \n"
                + "               b_news_pews_score_id , score, hours, mins,  \n"
                + "               user_record_id, user_update_id, newspews_type \n"
                + "     ) values (?, ?, ?, ?, \n"
                + "               ?, ?, ?)";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, p.getObjectId());
            ePQuery.setInt(index++, p.score);
            ePQuery.setInt(index++, p.hours);
            ePQuery.setInt(index++, p.mins);
            ePQuery.setString(index++, p.user_record_id);
            ePQuery.setString(index++, p.user_update_id);
            ePQuery.setObject(index++, p.newspews_type, java.sql.Types.OTHER);
            return ePQuery.executeUpdate();
        }
    }

    public int update(NewsPewsScore p) throws Exception {
        String sql = "update b_news_pews_score \n"
                + "      set score=?, hours=?, mins=?, user_update_id=?, update_date_time=current_timestamp, newspews_type=?"
                + "    where b_news_pews_score_id=?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setInt(index++, p.score);
            ePQuery.setInt(index++, p.hours);
            ePQuery.setInt(index++, p.mins);
            ePQuery.setString(index++, p.user_update_id);
            ePQuery.setObject(index++, p.newspews_type, java.sql.Types.OTHER);
            ePQuery.setString(index++, p.getObjectId());
            return ePQuery.executeUpdate();
        }
    }

    public int delete(NewsPewsScore p) throws Exception {
        String sql = "DELETE FROM b_news_pews_score where b_news_pews_score_id=? \n";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, p.getObjectId());
            return ePQuery.executeUpdate();
        }
    }

    public NewsPewsScore selectByScoreAndType(int score, String newsPewsType) throws Exception {
        String sql = "select * from b_news_pews_score where score = ? and newspews_type = ?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setInt(index++, score);
            ePQuery.setObject(index++, newsPewsType, java.sql.Types.OTHER);
            List<NewsPewsScore> list = executeQuery(ePQuery);
            return list.isEmpty() ? null : list.get(0);
        }
    }

    public NewsPewsScore selectMaxScore() throws Exception {
        String sql = "select * from b_news_pews_score "
                + " where score = (select max(score) from b_news_pews_score)";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            List<NewsPewsScore> list = executeQuery(ePQuery);
            return list.isEmpty() ? null : list.get(0);
        }
    }

    public List<NewsPewsScore> selectAll() throws Exception {
        String sql = "select * from b_news_pews_score order by score";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            return executeQuery(ePQuery);
        }
    }

    public List<NewsPewsScore> selectByType(String newsPewsType) throws Exception {
        String sql = "select * from b_news_pews_score where newspews_type = ? order by score";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setObject(index++, newsPewsType, java.sql.Types.OTHER);
            return executeQuery(ePQuery);
        }
    }

    public List<NewsPewsScore> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<NewsPewsScore> list = new ArrayList<>();
        try (ResultSet rs = preparedStatement.executeQuery()) {
            while (rs.next()) {
                NewsPewsScore p = new NewsPewsScore();
                p.setObjectId(rs.getString("b_news_pews_score_id"));
                p.score = rs.getInt("score");
                p.hours = rs.getInt("hours");
                p.mins = rs.getInt("mins");
                p.newspews_type = rs.getString("newspews_type");
                p.user_record_id = rs.getString("user_record_id");
                p.record_date_time = rs.getTimestamp("record_date_time");
                p.user_update_id = rs.getString("user_update_id");
                p.update_date_time = rs.getTimestamp("update_date_time");
                list.add(p);
            }
            return list;
        }
    }
}

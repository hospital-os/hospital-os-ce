/*
 * BooleanImageTableCellRenderer.java
 *
 * Created on 9 �ѹ��¹ 2545, 9:12 �.
 */
package com.hospital_os.utility;

import java.awt.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.table.*;

/**
 *
 * @author tong
 */
public class CelRendererTranfer extends JLabel implements TableCellRenderer {

    private static final long serialVersionUID = 1L;

    @Override
    public Component getTableCellRendererComponent(JTable table,
            Object value, boolean isSelected, boolean hasFocus,
            int row, int column) {
        setOpaque(true);
        setHorizontalAlignment(CENTER);
        if (isSelected) {
            this.setBackground(table.getSelectionBackground());
            this.setForeground(table.getSelectionForeground());
        } else {
            this.setBackground(table.getBackground());
            this.setForeground(table.getForeground());
        }
        try {
            if (value != null && ((String) value).equals("1")) {
                this.setIcon(new ImageIcon(getClass().getResource("/com/hospital_os/images/ball_red.gif")));
                return this;
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }
        this.setIcon(new ImageIcon(getClass().getResource("/com/hospital_os/images/ball_green.gif")));
        return this;
    }
    private static final Logger LOG = Logger.getLogger(CelRendererTranfer.class.getName());
}

/*
 * To change this template;public String choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class CommunityActivity extends Persistent {

    public String t_health_community_id;
    public String f_comactivity_id;
    public String start_date;
    public String finish_date;
    public String record_date_time;
    public String user_record_id;
    public String modify_date_time;
    public String user_modify_id;
    public String cancel_date_time;
    public String user_cancel_id;
    public String active = "1";
}

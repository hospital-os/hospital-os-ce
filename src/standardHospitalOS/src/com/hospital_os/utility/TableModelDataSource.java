/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.utility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class TableModelDataSource extends AbstractTableModel {

    private static final long serialVersionUID = 1L;
    private final String[] columns;
    private List<DataSource> data = new ArrayList<DataSource>();

    public TableModelDataSource(String column) {
        this.columns = new String[]{column};
    }

    public DataSource getRow(int row) {
        return data.get(row);
    }

    @Override
    public boolean isCellEditable(int row, int col) {
        return false;
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return columns.length;
    }

    @Override
    public String getColumnName(int col) {
        return columns[col];
    }

    @Override
    public Object getValueAt(int row, int col) {
        DataSource objects = data.get(row);
        return objects.getValue();
    }

    @SuppressWarnings("UseOfObsoleteCollectionType")
    public List<DataSource> getData() {
        return data;
    }

    public void setDataSources(List<DataSource> datas) {
        data.clear();
        if (datas != null && !datas.isEmpty()) {
            data.addAll(datas);
        }
    }

    public void addDataSource(DataSource objects) {
        data.add(objects);
    }

    public void addNotDuplicatIdDataSource(DataSource objects) {
        List<DataSource> dsClone = new ArrayList<DataSource>();
        dsClone.addAll(data);
        Collections.sort(dsClone, new DataSource());
        int binarySearch = Collections.binarySearch(dsClone, objects, new DataSource());
        if (binarySearch < 0) {
            data.add(objects);
        }
        dsClone.clear();
    }

    public void addNotDuplicatIdDataSources(List<DataSource> objects) {
        for (DataSource dataSource : objects) {
            addNotDuplicatIdDataSource(dataSource);
        }
    }

    @Override
    public void setValueAt(Object value, int row, int col) {
        DataSource dataSource = data.get(row);
        dataSource.setValue(String.valueOf(value));
        fireTableCellUpdated(row, col);
    }

    public void clearTable() {
        getData().clear();
        fireTableDataChanged();
    }

    public void removeData(int index) {
        data.remove(index);
        fireTableDataChanged();
    }

    public void removeDatas(int[] indexs) {
        for (int i = indexs.length - 1; i >= 0; i--) {
            data.remove(indexs[i]);
        }
        fireTableDataChanged();
    }

    public Object getSelectedId(int row) {
        DataSource objects = data.get(row);
        return objects.getId();
    }

    public String getSelectedValue(int row) {
        DataSource objects = data.get(row);
        return objects.getValue();
    }

    public DataSource getSelectedDataSource(int row) {
        return data.get(row);
    }

    public List<Object> getSelectedIds(int[] rows) {
        List<Object> list = new ArrayList<Object>();
        for (int row : rows) {
            DataSource objects = data.get(row);
            list.add(objects.getId());
        }
        return list;
    }

    public List<String> getSelectedValues(int[] rows) {
        List<String> list = new ArrayList<String>();
        for (int row : rows) {
            DataSource objects = data.get(row);
            list.add(objects.getValue());
        }
        return list;
    }

    public List<DataSource> getSelectedDataSources(int[] rows) {
        List<DataSource> list = new ArrayList<DataSource>();
        for (int row : rows) {
            DataSource objects = data.get(row);
            list.add(objects);
        }
        return list;
    }
}
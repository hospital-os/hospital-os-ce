package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

@SuppressWarnings("ClassWithoutLogger")
public class PatientDrugAllergy extends Persistent {

    public String t_patient_id = "";
    public String b_item_drug_standard_id = "";
    public String f_allergy_type_id = "9";
    public String drug_allergy_symtom_date = "";
    public String drug_allergy_symtom = "";
    public String f_naranjo_interpretation_id = "1";
    public String drug_allergy_note = "";
    public String f_allergy_warning_type_id = "1";
    public String drug_allergy_report_date = "";
    public String pharma_assess_id = "";
    public String doctor_diag_id = "";
    public String user_record = "";
    public String record_date_time = "";
    public String user_modify = "";
    public String modify_date_time = "";
    public String active = "1";
    public String generic_name = "";
    public String naranjo_interpretation = "";
    public String allergy_warning = "";
    public String f_allergy_level_id = "1";
    public String f_allergy_informant_id = "1";
    public String allergy_informant_hospital_id = "";
    public String f_allergy_icd10_id = "";
}
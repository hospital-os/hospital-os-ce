-- required t_address from v4
CREATE TABLE IF NOT EXISTS f_country (
  f_country_id character varying(255) NOT NULL,
  continent_en character varying(255) DEFAULT ''::character varying,
  continent_th character varying(255) DEFAULT ''::character varying,
  common_name_en character varying(255) DEFAULT ''::character varying,
  common_name_th character varying(255) DEFAULT ''::character varying,
  official_name_en character varying(255) DEFAULT ''::character varying,
  official_name_th character varying(255) DEFAULT ''::character varying,
  capital_name_en character varying(255) DEFAULT ''::character varying,
  capital_name_th character varying(255) DEFAULT ''::character varying,
  CONSTRAINT f_country_pkey PRIMARY KEY (f_country_id)
);
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('1', 'Asia', 'ทวีปเอเชีย', 'Cambodia', 'กัมพูชา', 'Kingdom of Cambodia', 'ราชอาณาจักรกัมพูชา', 'Phnom Penh', 'พนมเปญ');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('2', 'Asia', 'ทวีปเอเชีย', 'Qatar', 'กาตาร์', 'State of Qatar', 'รัฐกาตาร์', 'Doha', 'โดฮา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('3', 'Asia', 'ทวีปเอเชีย', 'South Korea', 'เกาหลีใต้', 'Republic of Korea', 'สาธารณรัฐเกาหลี', 'Seoul', 'โซล');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('4', 'Asia', 'ทวีปเอเชีย', 'North Korea', 'เกาหลีเหนือ', 'Democratic People’s Republic of Korea', 'สาธารณรัฐประชาธิปไตยประชาชนเกาหลี', 'Pyongyang', 'เปียงยาง');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('5', 'Asia', 'ทวีปเอเชีย', 'Kazakhstan', 'คาซัคสถาน', 'Republic of Kazakhstan', 'สาธารณรัฐคาซัคสถาน', 'Astana', 'อัสตานา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('6', 'Asia', 'ทวีปเอเชีย', 'Kyrgyzstan', 'คีร์กีซสถาน', 'Kyrgyz Republic', 'สาธารณรัฐคีร์กีซ', 'Bishkek', 'บิชเคก');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('7', 'Asia', 'ทวีปเอเชีย', 'Kuwait', 'คูเวต', 'State of Kuwait', 'รัฐคูเวต', 'Kuwait City', 'คูเวตซิตี');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('8', 'Asia', 'ทวีปเอเชีย', 'Georgia', 'จอร์เจีย', 'Georgia', 'จอร์เจีย', 'Tbilisi', 'ทบิลิซิ');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('9', 'Asia', 'ทวีปเอเชีย', 'Jordan', 'จอร์แดน', 'Hashemite Kingdom of Jordan', 'ราชอาณาจักรฮัชไมต์จอร์แดน', 'Amman', 'อัมมาน');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('10', 'Asia', 'ทวีปเอเชีย', 'China', 'จีน', 'People’s Republic of China', 'สาธารณรัฐประชาชนจีน', 'Beijing', 'ปักกิ่ง');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('11', 'Asia', 'ทวีปเอเชีย', 'Saudi Arabia', 'ซาอุดีอาระเบีย', 'Kingdom of Saudi Arabia', 'ราชอาณาจักรซาอุดีอาระเบีย', 'Riyadh', 'ริยาด');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('12', 'Asia', 'ทวีปเอเชีย', 'Syria', 'ซีเรีย', 'Syrian Arab Republic', 'สาธารณรัฐอาหรับซีเรีย', 'Damascus', 'ดามัสกัส');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('13', 'Asia', 'ทวีปเอเชีย', 'Cyprus', 'ไซปรัส', 'Republic of Cyprus', 'สาธารณรัฐไซปรัส', 'Nicosia', 'นิโคเซีย');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('14', 'Asia', 'ทวีปเอเชีย', 'Japan', 'ญี่ปุ่น', 'Japan', 'ญี่ปุ่น', 'Tokyo', 'โตเกียว');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('15', 'Asia', 'ทวีปเอเชีย', 'Timor-Leste', 'ติมอร์-เลสเต', 'Democratic Republic of Timor-Leste', 'สาธารณรัฐประชาธิปไตยติมอร์-เลสเต', 'Dili', 'ดิลี');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('16', 'Asia', 'ทวีปเอเชีย', 'Turkey', 'ตุรกี', 'Republic of Turkey', 'สาธารณรัฐตุรกี', 'Ankara', 'อังการา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('17', 'Asia', 'ทวีปเอเชีย', 'Turkmenistan', 'เติร์กเมนิสถาน', 'Turkmenistan', 'เติร์กเมนิสถาน', 'Ashgabat', 'อาชกาบัต');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('18', 'Asia', 'ทวีปเอเชีย', 'Tajikistan', 'ทาจิกิสถาน', 'Republic of Tajikistan', 'สาธารณรัฐทาจิกิสถาน', 'Dushanbe', 'ดูชานเบ');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('19', 'Asia', 'ทวีปเอเชีย', 'Thailand', 'ไทย', 'Kingdom of Thailand', 'ราชอาณาจักรไทย', 'Bangkok', 'กรุงเทพมหานคร');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('20', 'Asia', 'ทวีปเอเชีย', 'Nepal', 'เนปาล', 'Federal Democratic Republic of Nepal', 'สหพันธ์สาธารณรัฐประชาธิปไตยเนปาล', 'Kathmandu', 'กาฐมาณฑุ');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('21', 'Asia', 'ทวีปเอเชีย', 'Brunei Darussalam', 'บรูไนดารุสซาลาม', 'Negara Brunei Darussalam', 'เนการาบรูไนดารุสซาลาม', 'Bandar Seri Begawan', 'บันดาร์เสรีเบกาวัน');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('22', 'Asia', 'ทวีปเอเชีย', 'Bangladesh', 'บังกลาเทศ', 'People’s Republic of Bangladesh', 'สาธารณรัฐประชาชนบังกลาเทศ', 'Dhaka', 'ธากา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('23', 'Asia', 'ทวีปเอเชีย', 'Bahrain', 'บาห์เรน', 'Kingdom of Bahrain', 'ราชอาณาจักรบาห์เรน', 'Manama', 'มานามา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('24', 'Asia', 'ทวีปเอเชีย', 'Pakistan', 'ปากีสถาน', 'Islamic Republic of Pakistan', 'สาธารณรัฐอิสลามปากีสถาน', 'Islamabad', 'อิสลามาบัด');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('25', 'Asia', 'ทวีปเอเชีย', 'Palestine', 'ปาเลสไตน์', 'State of Palestine', 'รัฐปาเลสไตน์', 'Jerusalem', 'เยรูซาเลม');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('26', 'Asia', 'ทวีปเอเชีย', 'Myanmar', 'พม่า', 'Republic of the Union of Myanmar', 'สาธารณรัฐแห่งสหภาพเมียนมา', 'Nay Pyi Taw', 'เนปยีดอ');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('27', 'Asia', 'ทวีปเอเชีย', 'Philippines', 'ฟิลิปปินส์', 'Republic of the Philippines', 'สาธารณรัฐฟิลิปปินส์', 'Manila', 'มะนิลา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('28', 'Asia', 'ทวีปเอเชีย', 'Bhutan', 'ภูฏาน', 'Kingdom of Bhutan', 'ราชอาณาจักรภูฏาน', 'Thimphu', 'ทิมพู');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('29', 'Asia', 'ทวีปเอเชีย', 'Mongolia', 'มองโกเลีย', 'Mongolia', 'มองโกเลีย', 'Ulan Bator', 'อูลานบาตอร์');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('30', 'Asia', 'ทวีปเอเชีย', 'Maldives', 'มัลดีฟส์', 'Republic of Maldives', 'สาธารณรัฐมัลดีฟส์', 'Malé', 'มาเล');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('31', 'Asia', 'ทวีปเอเชีย', 'Malaysia', 'มาเลเซีย', 'Malaysia', 'มาเลเซีย', 'Kuala Lumpur', 'กัวลาลัมเปอร์');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('32', 'Asia', 'ทวีปเอเชีย', 'Yemen', 'เยเมน', 'Republic of Yemen', 'สาธารณรัฐเยเมน', 'Sana''a', 'ซานา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('33', 'Asia', 'ทวีปเอเชีย', 'Laos', 'ลาว', 'Lao People’s Democratic Republic', 'สาธารณรัฐประชาธิปไตยประชาชนลาว', 'Vientiane', 'เวียงจันทน์');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('34', 'Asia', 'ทวีปเอเชีย', 'Lebanon', 'เลบานอน', 'Republic of Lebanon', 'สาธารณรัฐเลบานอน', 'Beirut', 'เบรุต');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('35', 'Asia', 'ทวีปเอเชีย', 'Vietnam', 'เวียดนาม', 'Socialist Republic of Vietnam', 'สาธารณรัฐสังคมนิยมเวียดนาม', 'Hanoi', 'ฮานอย');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('36', 'Asia', 'ทวีปเอเชีย', 'Sri Lanka', 'ศรีลังกา', 'Democratic Socialist Republic of Sri Lanka', 'สาธารณรัฐสังคมนิยมประชาธิปไตยศรีลังกา', 'Kotte', 'โกตเต');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('37', 'Asia', 'ทวีปเอเชีย', 'United Arab Emirates', 'สหรัฐอาหรับเอมิเรตส์', 'United Arab Emirates', 'สหรัฐอาหรับเอมิเรตส์', 'Abu Dhabi', 'อาบูดาบี');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('38', 'Asia', 'ทวีปเอเชีย', 'Singapore', 'สิงคโปร์', 'Republic of Singapore', 'สาธารณรัฐสิงคโปร์', 'Singapore', 'สิงคโปร์');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('39', 'Asia', 'ทวีปเอเชีย', 'Afghanistan', 'อัฟกานิสถาน', 'Islamic Republic of Afghanistan', 'สาธารณรัฐอิสลามอัฟกานิสถาน', 'Kabul', 'คาบูล');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('40', 'Asia', 'ทวีปเอเชีย', 'Azerbaijan', 'อาเซอร์ไบจาน', 'Republic of Azerbaijan', 'สาธารณรัฐอาเซอร์ไบจาน', 'Baku', 'บากู');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('41', 'Asia', 'ทวีปเอเชีย', 'Armenia', 'อาร์มีเนีย', 'Republic of Armenia', 'สาธารณรัฐอาร์มีเนีย', 'Yerevan', 'เยเรวาน');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('42', 'Asia', 'ทวีปเอเชีย', 'India', 'อินเดีย', 'Republic of India', 'สาธารณรัฐอินเดีย', 'New Delhi', 'นิวเดลี');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('43', 'Asia', 'ทวีปเอเชีย', 'Indonesia', 'อินโดนีเซีย', 'Republic of Indonesia', 'สาธารณรัฐอินโดนีเซีย', 'Jakarta', 'จาการ์ตา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('44', 'Asia', 'ทวีปเอเชีย', 'Iraq', 'อิรัก', 'Republic of Iraq', 'สาธารณรัฐอิรัก', 'Baghdad', 'แบกแดด');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('45', 'Asia', 'ทวีปเอเชีย', 'Israel', 'อิสราเอล', 'State of Israel', 'รัฐอิสราเอล', 'Jerusalem', 'เยรูซาเลม');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('46', 'Asia', 'ทวีปเอเชีย', 'Iran', 'อิหร่าน', 'Islamic Republic of Iran', 'สาธารณรัฐอิสลามอิหร่าน', 'Tehran', 'เตหะราน');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('47', 'Asia', 'ทวีปเอเชีย', 'Uzbekistan', 'อุซเบกิสถาน', 'Republic of Uzbekistan', 'สาธารณรัฐอุซเบกิสถาน', 'Tashkent', 'ทาชเคนต์');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('48', 'Asia', 'ทวีปเอเชีย', 'Oman', 'โอมาน', 'Sultanate of Oman', 'รัฐสุลต่านโอมาน', 'Muscat', 'มัสกัต');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('49', 'Oceania', 'เขตโอเชียเนีย', 'Kiribati', 'คิริบาส', 'Republic of Kiribati', 'สาธารณรัฐคิริบาส', 'Tarawa', 'ตาระวา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('50', 'Oceania', 'เขตโอเชียเนีย', 'Samoa', 'ซามัว', 'Independent State of Samoa', 'รัฐเอกราชซามัว', 'Apia', 'อาปีอา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('51', 'Oceania', 'เขตโอเชียเนีย', 'Tonga', 'ตองกา', 'Kingdom of Tonga', 'ราชอาณาจักรตองกา', 'Nukuʻalofa', 'นูกูอะโลฟา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('52', 'Oceania', 'เขตโอเชียเนีย', 'Tuvalu', 'ตูวาลู', 'Tuvalu', 'ตูวาลู', 'Funafuti', 'ฟูนะฟูตี');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('53', 'Oceania', 'เขตโอเชียเนีย', 'Nauru', 'นาอูรู', 'Republic of Nauru', 'สาธารณรัฐนาอูรู', '–', '–');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('54', 'Oceania', 'เขตโอเชียเนีย', 'New Zealand', 'นิวซีแลนด์', 'New Zealand', 'นิวซีแลนด์', 'Wellington', 'เวลลิงตัน');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('55', 'Oceania', 'เขตโอเชียเนีย', 'Papua New Guinea', 'ปาปัวนิวกินี', 'Independent State of Papua New Guinea', 'รัฐเอกราชปาปัวนิวกินี', 'Port Moresby', 'พอร์ตมอร์สบี');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('56', 'Oceania', 'เขตโอเชียเนีย', 'Palau', 'ปาเลา', 'Republic of Palau', 'สาธารณรัฐปาเลา', 'Melekeok', 'เมเลเกโอก');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('57', 'Oceania', 'เขตโอเชียเนีย', 'Fiji', 'ฟิจิ', 'Republic of Fiji', 'สาธารณรัฐฟิจิ', 'Suva', 'ซูวา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('58', 'Oceania', 'เขตโอเชียเนีย', 'Micronesia', 'ไมโครนีเซีย', 'Federated States of Micronesia', 'สหพันธรัฐไมโครนีเซีย', 'Palikir', 'ปาลีกีร์');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('59', 'Oceania', 'เขตโอเชียเนีย', 'Vanuatu', 'วานูอาตู', 'Republic of Vanuatu', 'สาธารณรัฐวานูอาตู', 'Port Vila', 'พอร์ตวิลา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('60', 'Oceania', 'เขตโอเชียเนีย', 'Solomon Islands', 'หมู่เกาะโซโลมอน', 'Solomon Islands', 'หมู่เกาะโซโลมอน', 'Honiara', 'โฮนีอารา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('61', 'Oceania', 'เขตโอเชียเนีย', 'Marshall Islands', 'หมู่เกาะมาร์แชลล์', 'Republic of the Marshall Islands', 'สาธารณรัฐหมู่เกาะมาร์แชลล์', 'Majuro', 'มาจูโร');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('62', 'Oceania', 'เขตโอเชียเนีย', 'Australia', 'ออสเตรเลีย', 'Commonwealth of Australia', 'เครือรัฐออสเตรเลีย', 'Canberra', 'แคนเบอร์รา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('63', 'Europe', 'ทวีปยุโรป', 'Greece', 'กรีซ', 'Hellenic Republic', 'สาธารณรัฐเฮลเลนิก', 'Athens', 'เอเธนส์');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('64', 'Europe', 'ทวีปยุโรป', 'Kosovo', 'คอซอวอ  คอซอวอ', 'Republic of Kosovo', 'สาธารณรัฐคอซอวอ', 'Pristina', 'พริชตีนา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('65', 'Europe', 'ทวีปยุโรป', 'Croatia', 'โครเอเชีย', 'Republic of Croatia', 'สาธารณรัฐโครเอเชีย', 'Zagreb', 'ซาเกร็บ');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('66', 'Europe', 'ทวีปยุโรป', 'San Marino', 'ซานมารีโน', 'Republic of San Marino', 'สาธารณรัฐซานมารีโน', 'San Marino', 'ซานมารีโน');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('67', 'Europe', 'ทวีปยุโรป', 'Serbia', 'เซอร์เบีย', 'Republic of Serbia', 'สาธารณรัฐเซอร์เบีย', 'Belgrade', 'เบลเกรด');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('68', 'Europe', 'ทวีปยุโรป', 'Denmark', 'เดนมาร์ก', 'Kingdom of Denmark', 'ราชอาณาจักรเดนมาร์ก', 'Copenhagen', 'โคเปนเฮเกน');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('69', 'Europe', 'ทวีปยุโรป', 'Vatican City State', 'นครรัฐวาติกัน', 'Vatican City State', 'นครรัฐวาติกัน', 'Vatican', 'วาติกัน');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('70', 'Europe', 'ทวีปยุโรป', 'Norway', 'นอร์เวย์', 'Kingdom of Norway', 'ราชอาณาจักรนอร์เวย์', 'Oslo', 'ออสโล');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('71', 'Europe', 'ทวีปยุโรป', 'Netherlands', 'เนเธอร์แลนด์', 'Kingdom of the Netherlands', 'ราชอาณาจักรเนเธอร์แลนด์', 'Amsterdam', 'อัมสเตอร์ดัม');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('72', 'Europe', 'ทวีปยุโรป', 'Bosnia and Herzegovina', 'บอสเนียและเฮอร์เซโกวีนา', 'Bosnia and Herzegovina', 'บอสเนียและเฮอร์เซโกวีนา', 'Sarajevo', 'ซาราเยโว');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('73', 'Europe', 'ทวีปยุโรป', 'Bulgaria', 'บัลแกเรีย', 'Republic of Bulgaria', 'สาธารณรัฐบัลแกเรีย', 'Sofia', 'โซเฟีย');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('74', 'Europe', 'ทวีปยุโรป', 'Belgium', 'เบลเยียม', 'Kingdom of Belgium', 'ราชอาณาจักรเบลเยียม', 'Brussels', 'บรัสเซลส์');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('75', 'Europe', 'ทวีปยุโรป', 'Belarus', 'เบลารุส', 'Republic of Belarus', 'สาธารณรัฐเบลารุส', 'Minsk', 'มินสก์');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('76', 'Europe', 'ทวีปยุโรป', 'Portugal', 'โปรตุเกส', 'Portuguese Republic', 'สาธารณรัฐโปรตุเกส', 'Lisbon', 'ลิสบอน');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('77', 'Europe', 'ทวีปยุโรป', 'Poland', 'โปแลนด์', 'Republic of Poland', 'สาธารณรัฐโปแลนด์', 'Warsaw', 'วอร์ซอ');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('78', 'Europe', 'ทวีปยุโรป', 'France', 'ฝรั่งเศส', 'French Republic', 'สาธารณรัฐฝรั่งเศส', 'Paris', 'ปารีส');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('79', 'Europe', 'ทวีปยุโรป', 'Finland', 'ฟินแลนด์', 'Republic of Finland', 'สาธารณรัฐฟินแลนด์', 'Helsinki', 'เฮลซิงกิ');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('80', 'Europe', 'ทวีปยุโรป', 'Montenegro', 'มอนเตเนโกร', 'Montenegro', 'มอนเตเนโกร', 'Podgorica', 'พอดกอรีตซา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('81', 'Europe', 'ทวีปยุโรป', 'Moldova', 'มอลโดวา', 'Republic of Moldova', 'สาธารณรัฐมอลโดวา', 'Chișinău', 'คีชีเนา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('82', 'Europe', 'ทวีปยุโรป', 'Malta', 'มอลตา', 'Republic of Malta', 'สาธารณรัฐมอลตา', 'Valletta', 'วัลเลตตา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('83', 'Europe', 'ทวีปยุโรป', 'Monaco', 'โมนาโก', 'Principality of Monaco', 'ราชรัฐโมนาโก', 'Monaco', 'โมนาโก');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('84', 'Europe', 'ทวีปยุโรป', 'Ukraine', 'ยูเครน', 'Ukraine', 'ยูเครน', 'Kiev', 'เคียฟ');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('85', 'Europe', 'ทวีปยุโรป', 'Germany', 'เยอรมนี', 'Federal Republic of Germany', 'สหพันธ์สาธารณรัฐเยอรมนี', 'Berlin', 'เบอร์ลิน');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('86', 'Europe', 'ทวีปยุโรป', 'Russia', 'รัสเซีย', 'Russian Federation', 'สหพันธรัฐรัสเซีย', 'Moscow', 'มอสโก');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('87', 'Europe', 'ทวีปยุโรป', 'Romania', 'โรมาเนีย', 'Romania', 'โรมาเนีย', 'Bucharest', 'บูคาเรสต์');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('88', 'Europe', 'ทวีปยุโรป', 'Luxembourg', 'ลักเซมเบิร์ก', 'Grand Duchy of Luxembourg', 'ราชรัฐลักเซมเบิร์ก', 'Luxembourg', 'ลักเซมเบิร์ก');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('89', 'Europe', 'ทวีปยุโรป', 'Latvia', 'ลัตเวีย', 'Republic of Latvia', 'สาธารณรัฐลัตเวีย', 'Riga', 'รีกา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('90', 'Europe', 'ทวีปยุโรป', 'Liechtenstein', 'ลิกเตนสไตน์', 'Principality of Liechtenstein', 'ราชรัฐลิกเตนสไตน์', 'Vaduz', 'วาดุซ');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('91', 'Europe', 'ทวีปยุโรป', 'Lithuania', 'ลิทัวเนีย', 'Republic of Lithuania', 'สาธารณรัฐลิทัวเนีย', 'Vilnius', 'วิลนีอุส');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('92', 'Europe', 'ทวีปยุโรป', 'Spain', 'สเปน', 'Kingdom of Spain', 'ราชอาณาจักรสเปน', 'Madrid', 'มาดริด');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('93', 'Europe', 'ทวีปยุโรป', 'Slovakia', 'สโลวาเกีย', 'Slovak Republic', 'สาธารณรัฐสโลวัก', 'Bratislava', 'บราติสลาวา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('94', 'Europe', 'ทวีปยุโรป', 'Slovenia', 'สโลวีเนีย', 'Republic of Slovenia', 'สาธารณรัฐสโลวีเนีย', 'Ljubljana', 'ลูบลิยานา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('95', 'Europe', 'ทวีปยุโรป', 'Switzerland', 'สวิตเซอร์แลนด์', 'Swiss Confederation', 'สมาพันธรัฐสวิส', 'Bern', 'เบิร์น');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('96', 'Europe', 'ทวีปยุโรป', 'Sweden', 'สวีเดน', 'Kingdom of Sweden', 'ราชอาณาจักรสวีเดน', 'Stockholm', 'สตอกโฮล์ม');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('97', 'Europe', 'ทวีปยุโรป', 'United Kingdom', 'สหราชอาณาจักร', 'United Kingdom of Great Britain and Northern Ireland', 'สหราชอาณาจักรบริเตนใหญ่ และไอร์แลนด์เหนือ', 'London', 'ลอนดอน');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('98', 'Europe', 'ทวีปยุโรป', 'Czech Republic', 'สาธารณรัฐเช็ก', 'Czech Republic', 'สาธารณรัฐเช็ก', 'Prague', 'ปราก');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('99', 'Europe', 'ทวีปยุโรป', 'Republic of Macedonia', 'สาธารณรัฐมาซิโดเนีย', 'Republic of Macedonia', 'สาธารณรัฐมาซิโดเนีย', 'Skopje', 'สโกเปีย');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('100', 'Europe', 'ทวีปยุโรป', 'Austria', 'ออสเตรีย', 'Republic of Austria', 'สาธารณรัฐออสเตรีย', 'Vienna', 'เวียนนา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('101', 'Europe', 'ทวีปยุโรป', 'Andorra', 'อันดอร์รา', 'Principality of Andorra', 'ราชรัฐอันดอร์รา', 'Andorra la Vella', 'อันดอร์ราลาเวลลา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('102', 'Europe', 'ทวีปยุโรป', 'Italy', 'อิตาลี', 'Italian Republic', 'สาธารณรัฐอิตาลี', 'Rome', 'โรม');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('103', 'Europe', 'ทวีปยุโรป', 'Estonia', 'เอสโตเนีย', 'Republic of Estonia', 'สาธารณรัฐเอสโตเนีย', 'Tallinn', 'ทาลลินน์');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('104', 'Europe', 'ทวีปยุโรป', 'Albania', 'แอลเบเนีย', 'Republic of Albania', 'สาธารณรัฐแอลเบเนีย', 'Tirana', 'ติรานา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('105', 'Europe', 'ทวีปยุโรป', 'Iceland', 'ไอซ์แลนด์', 'Republic of Iceland', 'สาธารณรัฐไอซ์แลนด์', 'Reykjavík', 'เรคยาวิก');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('106', 'Europe', 'ทวีปยุโรป', 'Ireland', 'ไอร์แลนด์', 'Ireland', 'ไอร์แลนด์', 'Dublin', 'ดับลิน');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('107', 'Europe', 'ทวีปยุโรป', 'Hungary', 'ฮังการี', 'Hungary', 'ฮังการี', 'Budapest', 'บูดาเปสต์');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('108', 'Africa', 'ทวีปแอฟริกา', 'Ghana', 'กานา', 'Republic of Ghana', 'สาธารณรัฐกานา', 'Accra', 'อักกรา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('109', 'Africa', 'ทวีปแอฟริกา', 'Gabon', 'กาบอง', 'Gabonese Republic', 'สาธารณรัฐกาบอง', 'Libreville', 'ลีเบรอวิล');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('110', 'Africa', 'ทวีปแอฟริกา', 'Guinea', 'กินี', 'Republic of Guinea', 'สาธารณรัฐกินี', 'Conakry', 'โกนากรี');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('111', 'Africa', 'ทวีปแอฟริกา', 'Guinea-Bissau', 'กินี-บิสเซา', 'Republic of Guinea-Bissau', 'สาธารณรัฐกินี-บิสเซา', 'Bissau', 'บิสเซา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('112', 'Africa', 'ทวีปแอฟริกา', 'The Gambia', 'แกมเบีย', 'Republic of The Gambia', 'สาธารณรัฐแกมเบีย', 'Banjul', 'บันจูล');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('113', 'Africa', 'ทวีปแอฟริกา', 'Côte d’Ivoire', 'โกตดิวัวร์', 'Republic of Côte d’Ivoire', 'สาธารณรัฐโกตดิวัวร์', 'Yamoussoukro', 'ยามุสซุโกร');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('114', 'Africa', 'ทวีปแอฟริกา', 'Comoros', 'คอโมโรส', 'Union of the Comoros', 'สหภาพคอโมโรส', 'Moroni', 'โมโรนี');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('115', 'Africa', 'ทวีปแอฟริกา', 'Kenya', 'เคนยา', 'Republic of Kenya', 'สาธารณรัฐเคนยา', 'Nairobi', 'ไนโรบี');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('116', 'Africa', 'ทวีปแอฟริกา', 'Cape Verde', 'เคปเวิร์ด', 'Republic of Cape Verde', 'สาธารณรัฐเคปเวิร์ด', 'Praia', 'ไปรอา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('117', 'Africa', 'ทวีปแอฟริกา', 'Cameroon', 'แคเมอรูน', 'Republic of Cameroon', 'สาธารณรัฐแคเมอรูน', 'Yaoundé', 'ยาอุนเด');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('118', 'Africa', 'ทวีปแอฟริกา', 'Djibouti', 'จิบูตี', 'Republic of Djibouti', 'สาธารณรัฐจิบูตี', 'Djibouti', 'จิบูตี');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('119', 'Africa', 'ทวีปแอฟริกา', 'Chad', 'ชาด', 'Republic of Chad', 'สาธารณรัฐชาด', 'N’Djamena', 'เอ็นจาเมนา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('120', 'Africa', 'ทวีปแอฟริกา', 'Zimbabwe', 'ซิมบับเว', 'Republic of Zimbabwe', 'สาธารณรัฐซิมบับเว', 'Harare', 'ฮาราเร');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('121', 'Africa', 'ทวีปแอฟริกา', 'Sudan', 'ซูดาน', 'Republic of the Sudan', 'สาธารณรัฐซูดาน', 'Khartoum', 'คาร์ทูม');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('122', 'Africa', 'ทวีปแอฟริกา', 'Seychelles', 'เซเชลส์', 'Republic of Seychelles', 'สาธารณรัฐเซเชลส์', 'Victoria', 'วิกตอเรีย');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('123', 'Africa', 'ทวีปแอฟริกา', 'Senegal', 'เซเนกัล', 'Republic of Senegal', 'สาธารณรัฐเซเนกัล', 'Dakar', 'ดาการ์');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('124', 'Africa', 'ทวีปแอฟริกา', 'São Tomé and Príncipe', 'เซาตูเมและปรินซิปี', 'Democratic Republic of São Tomé and Príncipe', 'สาธารณรัฐประชาธิปไตยเซาตูเม และปรินซิปี', 'São Tomé', 'เซาตูเม');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('125', 'Africa', 'ทวีปแอฟริกา', 'South Sudan', 'เซาท์ซูดาน', 'Republic of South Sudan', 'สาธารณรัฐเซาท์ซูดาน', 'Juba', 'จูบา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('126', 'Africa', 'ทวีปแอฟริกา', 'Sierra Leone', 'เซียร์ราลีโอน', 'Republic of Sierra Leone', 'สาธารณรัฐเซียร์ราลีโอน', 'Freetown', 'ฟรีทาวน์');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('127', 'Africa', 'ทวีปแอฟริกา', 'Zambia', 'แซมเบีย', 'Republic of Zambia', 'สาธารณรัฐแซมเบีย', 'Lusaka', 'ลูซากา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('128', 'Africa', 'ทวีปแอฟริกา', 'Somalia', 'โซมาเลีย', 'Somali Republic', 'สาธารณรัฐโซมาลี', 'Mogadishu', 'โมกาดิชู');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('129', 'Africa', 'ทวีปแอฟริกา', 'Tunisia', 'ตูนิเซีย', 'Tunisian Republic', 'สาธารณรัฐตูนิเซีย', 'Tunis', 'ตูนิส');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('130', 'Africa', 'ทวีปแอฟริกา', 'Togo', 'โตโก', 'Togolese Republic', 'สาธารณรัฐโตโก', 'Lomé', 'โลเม');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('131', 'Africa', 'ทวีปแอฟริกา', 'Tanzania', 'แทนซาเนีย', 'United Republic of Tanzania', 'สหสาธารณรัฐแทนซาเนีย', 'Dodoma', 'โดโดมา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('132', 'Africa', 'ทวีปแอฟริกา', 'Namibia', 'นามิเบีย', 'Republic of Namibia', 'สาธารณรัฐนามิเบีย', 'Windhoek', 'วินด์ฮุก');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('133', 'Africa', 'ทวีปแอฟริกา', 'Nigeria', 'ไนจีเรีย', 'Federal Republic of Nigeria', 'สหพันธ์สาธารณรัฐไนจีเรีย', 'Abuja', 'อาบูจา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('134', 'Africa', 'ทวีปแอฟริกา', 'Niger', 'ไนเจอร์', 'Republic of Niger', 'สาธารณรัฐไนเจอร์', 'Niamey', 'นีอาเม');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('135', 'Africa', 'ทวีปแอฟริกา', 'Botswana', 'บอตสวานา', 'Republic of Botswana', 'สาธารณรัฐบอตสวานา', 'Gaborone', 'กาโบโรเน');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('136', 'Africa', 'ทวีปแอฟริกา', 'Burundi', 'บุรุนดี', 'Republic of Burundi', 'สาธารณรัฐบุรุนดี', 'Bujumbura', 'บูจุมบูรา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('137', 'Africa', 'ทวีปแอฟริกา', 'Burkina Faso', 'บูร์กินาฟาโซ', 'Burkina Faso', 'บูร์กินาฟาโซ', 'Ouagadougou', 'วากาดูกู');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('138', 'Africa', 'ทวีปแอฟริกา', 'Benin', 'เบนิน', 'Republic of Benin', 'สาธารณรัฐเบนิน', 'Porto-Novo', 'ปอร์โต-โนโว');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('139', 'Africa', 'ทวีปแอฟริกา', 'Mauritius', 'มอริเชียส', 'Republic of Mauritius', 'สาธารณรัฐมอริเชียส', 'Port Louis', 'พอร์ตหลุยส์');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('140', 'Africa', 'ทวีปแอฟริกา', 'Mauritania', 'มอริเตเนีย', 'Islamic Republic of Mauritania', 'สาธารณรัฐอิสลามมอริเตเนีย', 'Nouakchott', 'นูแอกชอต');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('141', 'Africa', 'ทวีปแอฟริกา', 'Madagascar', 'มาดากัสการ์', 'Republic of Madagascar', 'สาธารณรัฐมาดากัสการ์', 'Antananarivo', 'อันตานานาริโว');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('142', 'Africa', 'ทวีปแอฟริกา', 'Malawi', 'มาลาวี', 'Republic of Malawi', 'สาธารณรัฐมาลาวี', 'Lilongwe', 'ลิลองเว');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('143', 'Africa', 'ทวีปแอฟริกา', 'Mali', 'มาลี', 'Republic of Mali', 'สาธารณรัฐมาลี', 'Bamako', 'บามาโก');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('144', 'Africa', 'ทวีปแอฟริกา', 'Mozambique', 'โมซัมบิก', 'Republic of Mozambique', 'สาธารณรัฐโมซัมบิก', 'Maputo', 'มาปูโต');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('145', 'Africa', 'ทวีปแอฟริกา', 'Morocco', 'โมร็อกโก', 'Kingdom of Morocco', 'ราชอาณาจักรโมร็อกโก', 'Rabat', 'ราบัต');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('146', 'Africa', 'ทวีปแอฟริกา', 'Uganda', 'ยูกันดา', 'Republic of Uganda', 'สาธารณรัฐยูกันดา', 'Kampala', 'กัมปาลา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('147', 'Africa', 'ทวีปแอฟริกา', 'Rwanda', 'รวันดา', 'Republic of Rwanda', 'สาธารณรัฐรวันดา', 'Kigali', 'คิกาลี');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('148', 'Africa', 'ทวีปแอฟริกา', 'Libya', 'ลิเบีย', 'Libya', 'ลิเบีย', 'Tripoli', 'ตริโปลี');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('149', 'Africa', 'ทวีปแอฟริกา', 'Lesotho', 'เลโซโท', 'Kingdom of Lesotho', 'ราชอาณาจักรเลโซโท', 'Maseru', 'มาเซรู');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('150', 'Africa', 'ทวีปแอฟริกา', 'Liberia', 'ไลบีเรีย', 'Republic of Liberia', 'สาธารณรัฐไลบีเรีย', 'Monrovia', 'มันโรเวีย');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('151', 'Africa', 'ทวีปแอฟริกา', 'Swaziland', 'สวาซิแลนด์', 'Kingdom of Swaziland', 'ราชอาณาจักรสวาซิแลนด์', 'Mbabane', 'อัมบาบาเน');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('152', 'Africa', 'ทวีปแอฟริกา', 'Republic of the Congo', 'สาธารณรัฐคองโก', 'Republic of the Congo', 'สาธารณรัฐคองโก', 'Brazzaville', 'บราซซาวิล');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('153', 'Africa', 'ทวีปแอฟริกา', 'Democratic Republic of the Congo', 'สาธารณรัฐประชาธิปไตยคองโก', 'Democratic Republic of the Congo', 'สาธารณรัฐประชาธิปไตยคองโก', 'Kinshasa', 'กินชาซา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('154', 'Africa', 'ทวีปแอฟริกา', 'Central African Republic', 'สาธารณรัฐแอฟริกากลาง', 'Central African Republic', 'สาธารณรัฐแอฟริกากลาง', 'Bangui', 'บังกี');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('155', 'Africa', 'ทวีปแอฟริกา', 'Equatorial Guinea', 'อิเควทอเรียลกินี', 'Republic of Equatorial Guinea', 'สาธารณรัฐอิเควทอเรียลกินี', 'Malabo', 'มาลาโบ');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('156', 'Africa', 'ทวีปแอฟริกา', 'Egypt', 'อียิปต์', 'Arab Republic of Egypt', 'สาธารณรัฐอาหรับอียิปต์', 'Cairo', 'ไคโร');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('157', 'Africa', 'ทวีปแอฟริกา', 'Ethiopia', 'เอธิโอเปีย', 'Federal Democratic Republic of Ethiopia', 'สหพันธ์สาธารณรัฐประชาธิปไตยเอธิโอเปีย', 'Addis Ababa', 'แอดดิสอาบาบา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('158', 'Africa', 'ทวีปแอฟริกา', 'Eritrea', 'เอริเทรีย', 'State of Eritrea', 'รัฐเอริเทรีย', 'Asmara', 'แอสมารา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('159', 'Africa', 'ทวีปแอฟริกา', 'Angola', 'แองโกลา', 'Republic of Angola', 'สาธารณรัฐแองโกลา', 'Luanda', 'ลูอันดา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('160', 'Africa', 'ทวีปแอฟริกา', 'South Africa', 'แอฟริกาใต้', 'Republic of South Africa', 'สาธารณรัฐแอฟริกาใต้', 'Pretoria', 'พริทอเรีย');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('161', 'Africa', 'ทวีปแอฟริกา', 'Algeria', 'แอลจีเรีย', 'People’s Democratic Republic of Algeria', 'สาธารณรัฐประชาธิปไตยประชาชนแอลจีเรีย', 'Algiers', 'แอลเจียร์');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('162', 'North America', 'ทวีปอเมริกาเหนือ', 'Guatemala', 'กัวเตมาลา', 'Republic of Guatemala', 'สาธารณรัฐกัวเตมาลา', 'Guatemala City', 'กัวเตมาลาซิตี');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('163', 'North America', 'ทวีปอเมริกาเหนือ', 'Grenada', 'เกรเนดา', 'Grenada', 'เกรเนดา', 'Saint George’s', 'เซนต์จอร์เจส');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('164', 'North America', 'ทวีปอเมริกาเหนือ', 'Costa Rica', 'คอสตาริกา', 'Republic of Costa Rica', 'สาธารณรัฐคอสตาริกา', 'San José', 'ซันโฮเซ');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('165', 'North America', 'ทวีปอเมริกาเหนือ', 'Cuba', 'คิวบา', 'Republic of Cuba', 'สาธารณรัฐคิวบา', 'Havana', 'ฮาวานา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('166', 'North America', 'ทวีปอเมริกาเหนือ', 'Canada', 'แคนาดา', 'Canada', 'แคนาดา', 'Ottawa', 'ออตตาวา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('167', 'North America', 'ทวีปอเมริกาเหนือ', 'Jamaica', 'จาเมกา', 'Jamaica', 'จาเมกา', 'Kingston', 'คิงส์ตัน');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('168', 'North America', 'ทวีปอเมริกาเหนือ', 'Saint Kitts and Nevis', 'เซนต์คิตส์และเนวิส', 'Federation of Saint Kitts and Nevis', 'สหพันธรัฐเซนต์คิตส์และเนวิส', 'Basseterre', 'บาสแตร์');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('169', 'North America', 'ทวีปอเมริกาเหนือ', 'Saint Lucia', 'เซนต์ลูเซีย', 'Saint Lucia', 'เซนต์ลูเซีย', 'Castries', 'แคสตรีส์');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('170', 'North America', 'ทวีปอเมริกาเหนือ', 'Saint Vincent and the Grenadines', 'เซนต์วินเซนต์และเกรนาดีนส์', 'Saint Vincent and the Grenadines', 'เซนต์วินเซนต์และเกรนาดีนส์', 'Kingstown', 'คิงส์ทาวน์');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('171', 'North America', 'ทวีปอเมริกาเหนือ', 'Dominica', 'โดมินิกา', 'Commonwealth of Dominica', 'เครือรัฐโดมินิกา', 'Roseau', 'โรโซ');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('172', 'North America', 'ทวีปอเมริกาเหนือ', 'Nicaragua', 'นิการากัว', 'Republic of Nicaragua', 'สาธารณรัฐนิการากัว', 'Managua', 'มานากัว');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('173', 'North America', 'ทวีปอเมริกาเหนือ', 'Barbados', 'บาร์เบโดส', 'Barbados', 'บาร์เบโดส', 'Bridgetown', 'บริดจ์ทาวน์');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('174', 'North America', 'ทวีปอเมริกาเหนือ', 'Bahamas', 'บาฮามาส', 'Commonwealth of the Bahamas', 'เครือรัฐบาฮามาส', 'Nassau', 'แนสซอ');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('175', 'North America', 'ทวีปอเมริกาเหนือ', 'Belize', 'เบลีซ', 'Belize', 'เบลีซ', 'Belmopan', 'เบลโมแพน');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('176', 'North America', 'ทวีปอเมริกาเหนือ', 'Panama', 'ปานามา', 'Republic of Panama', 'สาธารณรัฐปานามา', 'Panama City', 'ปานามาซิตี');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('177', 'North America', 'ทวีปอเมริกาเหนือ', 'Mexico', 'เม็กซิโก', 'United Mexican States', 'สหรัฐเม็กซิโก', 'Mexico City', 'เม็กซิโกซิตี');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('178', 'North America', 'ทวีปอเมริกาเหนือ', 'United States of America', 'สหรัฐอเมริกา', 'United States of America', 'สหรัฐอเมริกา', 'Washington, D.C.', 'วอชิงตัน ดี.ซี.');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('179', 'North America', 'ทวีปอเมริกาเหนือ', 'Dominican Republic', 'สาธารณรัฐโดมินิกัน', 'Dominican Republic', 'สาธารณรัฐโดมินิกัน', 'Santo Domingo', 'ซันโตโดมิงโก');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('180', 'North America', 'ทวีปอเมริกาเหนือ', 'El Salvador', 'เอลซัลวาดอร์', 'Republic of El Salvador', 'สาธารณรัฐเอลซัลวาดอร์', 'San Salvador', 'ซันซัลวาดอร์');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('181', 'North America', 'ทวีปอเมริกาเหนือ', 'Antigua and Barbuda', 'แอนติกาและบาร์บูดา', 'Antigua and Barbuda', 'แอนติกาและบาร์บูดา', 'Saint John’s', 'เซนต์จอนส์');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('182', 'North America', 'ทวีปอเมริกาเหนือ', 'Honduras', 'ฮอนดูรัส', 'Republic of Honduras', 'สาธารณรัฐฮอนดูรัส', 'Tegucigalpa', 'เตกูซิกัลปา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('183', 'North America', 'ทวีปอเมริกาเหนือ', 'Haiti', 'เฮติ', 'Republic of Haiti', 'สาธารณรัฐเฮติ', 'Port-au-Prince', 'ปอร์โตแปรงซ์');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('184', 'South America', 'ทวีปอเมริกาใต้', 'Guyana', 'กายอานา', 'Cooperative Republic of Guyana', 'สาธารณรัฐสหกรณ์กายอานา', 'Georgetown', 'จอร์จทาวน์');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('185', 'South America', 'ทวีปอเมริกาใต้', 'Colombia', 'โคลอมเบีย', 'Republic of Colombia', 'สาธารณรัฐโคลอมเบีย', 'Bogotá', 'โบโกตา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('186', 'South America', 'ทวีปอเมริกาใต้', 'Chile', 'ชิลี', 'Republic of Chile', 'สาธารณรัฐชิลี', 'Santiago', 'ซานเตียโก');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('187', 'South America', 'ทวีปอเมริกาใต้', 'Suriname', 'ซูรินาเม', 'Republic of Suriname', 'สาธารณรัฐซูรินาเม', 'Paramaribo', 'ปารามาริโบ');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('188', 'South America', 'ทวีปอเมริกาใต้', 'Trinidad and Tobago', 'ตรินิแดดและโตเบโก', 'Republic of Trinidad and Tobago', 'สาธารณรัฐตรินิแดดและโตเบโก', 'Port of Spain', 'พอร์ตออฟสเปน');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('189', 'South America', 'ทวีปอเมริกาใต้', 'Brazil', 'บราซิล', 'Federative Republic of Brazil', 'สหพันธ์สาธารณรัฐบราซิล', 'Brasília', 'บราซิเลีย');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('190', 'South America', 'ทวีปอเมริกาใต้', 'Bolivia', 'โบลิเวีย', 'Plurinational State of Bolivia', 'รัฐพหุชนชาติแห่งโบลิเวีย', 'Sucre', 'ซูเกร');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('191', 'South America', 'ทวีปอเมริกาใต้', 'Paraguay', 'ปารากวัย', 'Republic of Paraguay', 'สาธารณรัฐปารากวัย', 'Asunción', 'อะซุนซิออง');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('192', 'South America', 'ทวีปอเมริกาใต้', 'Peru', 'เปรู', 'Republic of Peru', 'สาธารณรัฐเปรู', 'Lima', 'ลิมา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('193', 'South America', 'ทวีปอเมริกาใต้', 'Venezuela', 'เวเนซุเอลา', 'Bolivarian Republic of Venezuela', 'สาธารณรัฐโบลีวาร์แห่งเวเนซุเอลา', 'Caracas', 'การากัส');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('194', 'South America', 'ทวีปอเมริกาใต้', 'Argentina', 'อาร์เจนตินา', 'Argentine Republic', 'สาธารณรัฐอาร์เจนตินา', 'Buenos Aires', 'บัวโนสไอเรส');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('195', 'South America', 'ทวีปอเมริกาใต้', 'Uruguay', 'อุรุกวัย', 'Oriental Republic of Uruguay', 'สาธารณรัฐบูรพาอุรุกวัย', 'Montevideo', 'มอนเตวิเดโอ');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('196', 'South America', 'ทวีปอเมริกาใต้', 'Ecuador', 'เอกวาดอร์', 'Republic of Ecuador', 'สาธารณรัฐเอกวาดอร์', 'Quito', 'กีโต');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('197', 'Other', 'อื่นๆ', 'South Ossetia', 'เซาท์ออสซีเชีย', 'Republic of South Ossetia', 'สาธารณรัฐเซาท์ออสซีเชีย', 'Tskhinvali', 'สคินวาลี');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('198', 'Other', 'อื่นๆ', 'Somaliland', 'โซมาลีแลนด์', 'Republic of Somaliland', 'สาธารณรัฐโซมาลีแลนด์', 'Hargeisa', 'ฮาร์เกซา');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('199', 'Other', 'อื่นๆ', 'Northern Cyprus', 'ไซปรัสเหนือ', 'Turkish Republic of Northern Cyprus', 'สาธารณรัฐตุรกีแห่งไซปรัสเหนือ', 'Nicosia', 'นิโคเซีย');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('200', 'Other', 'อื่นๆ', 'Taiwan', 'ไต้หวัน', 'Republic of China', 'สาธารณรัฐจีน', 'Taipei', 'ไทเป');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('201', 'Other', 'อื่นๆ', 'Transnistria', 'ทรานส์นิสเตรีย', 'Pridnestrovian Moldavian Republic', 'สาธารณรัฐมอลดาเวียปรีดเนสโตรเวีย', 'Tiraspol', 'ตีรัสปอล');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('202', 'Other', 'อื่นๆ', 'Nagorno-Karabakh', 'นากอร์โน-คาราบัค', 'Nagorno-Karabakh Republic', 'สาธารณรัฐนากอร์โน-คาราบัค', 'Stepanakert', 'สเตพานาแกร์ต');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('203', 'Other', 'อื่นๆ', 'Sahrawi Arab Democratic Republic', 'สาธารณรัฐประชาธิปไตยอาหรับซาห์ราวี', 'Sahrawi Arab Democratic Republic', 'สาธารณรัฐประชาธิปไตยอาหรับซาห์ราวี', 'El Aaiún', 'เอลอายุน');
insert into f_country (f_country_id, continent_en, continent_th, common_name_en, common_name_th, official_name_en, official_name_th, capital_name_en, capital_name_th) values ('204', 'Other', 'อื่นๆ', 'Abkhazia', 'อับฮาเซีย', 'Republic of Abkhazia', 'สาธารณรัฐอับฮาเซีย', 'Sukhumi', 'ซูฮูมี');

CREATE TABLE IF NOT EXISTS t_address
(
  t_address_id character varying(30) NOT NULL,
  f_address_housetype_id character varying(2) DEFAULT '4'::character varying,
  organization_name text DEFAULT ''::character varying,
  house_no character varying(255) DEFAULT ''::character varying,
  building text DEFAULT ''::character varying,
  floor character varying(20) DEFAULT ''::character varying,
  room_no character varying(20) DEFAULT ''::character varying,    
  village_no character varying(20) DEFAULT ''::character varying,
  village text DEFAULT ''::character varying,
  soi text DEFAULT ''::character varying,
  sub_soi text DEFAULT ''::character varying,
  road text DEFAULT ''::character varying,
  sub_district_id character varying(6) DEFAULT ''::character varying,
  district_id character varying(6) DEFAULT ''::character varying,
  province_id character varying(6) DEFAULT ''::character varying,
  postal_code character varying(20) DEFAULT ''::character varying,
  country_id character varying(3) NOT NULL,
  is_use_location boolean NOT NULL DEFAULT false,
  latitude double precision,
  longitude double precision,
  address1 text DEFAULT ''::character varying,
  address2 text DEFAULT ''::character varying,
  city text DEFAULT ''::character varying,
  "state" text DEFAULT ''::character varying,  
  address_en text NULL DEFAULT ''::text,
  active character varying(1) NOT NULL DEFAULT '1'::character varying,
  record_date_time    TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
  user_record_id          CHARACTER VARYING(30) NULL,
  update_date_time    TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
  user_update_id        CHARACTER VARYING(30) NULL,
  cancel_date_time    timestamp WITH TIME ZONE NULL,
  user_cancel_id        CHARACTER VARYING(30) NULL,
  CONSTRAINT t_address_pkey PRIMARY KEY (t_address_id)
);

CREATE INDEX t_address_house_no_village_no_sub_district_id_idx
  ON t_address
  USING btree
  (house_no COLLATE pg_catalog."default", village_no COLLATE pg_catalog."default", sub_district_id COLLATE pg_catalog."default");


--t_visit_illness_address
INSERT INTO t_address(
            t_address_id, f_address_housetype_id, room_no, building, 
            house_no, village_no, village, 
            soi, sub_soi, road, 
            sub_district_id, district_id, province_id, country_id, 
            latitude, longitude, active, 
            record_date_time, user_record_id, update_date_time, user_update_id, 
             is_use_location )
SELECT t_visit_illness_address_id, f_address_housetype_id, 
       visit_illness_address_roomno, visit_illness_address_building, 
       visit_illness_address_house, visit_illness_address_moo, visit_illness_address_villaname, 
       visit_illness_address_soimain, visit_illness_address_soisub,
       visit_illness_address_road, visit_illness_address_tambon, visit_illness_address_amphur, 
       visit_illness_address_changwat, '19', visit_illness_address_latitude, 
       visit_illness_address_longitude, active, 
       case when record_date_time is null or record_date_time = '' then null else
((substring(record_date_time,0,5)::integer - 543)::varchar || substring(record_date_time,5,15))::timestamp end, 
       case when user_record_id in (select b_employee_id from b_employee) then user_record_id 
            else (select b_employee_id from b_employee where employee_login = 'admin') end as user_record_id, 
       case when modify_date_time is null or modify_date_time = '' then null else
((substring(modify_date_time,0,5)::integer - 543)::varchar || substring(modify_date_time,5,15))::timestamp end, 
       case when user_modify_id in (select b_employee_id from b_employee) then user_modify_id 
            else (select b_employee_id from b_employee where employee_login = 'admin') end user_modify_id, 
       case when visit_illness_address_latitude is not null and visit_illness_address_latitude <> 0
       and visit_illness_address_longitude is not null and visit_illness_address_longitude <> 0 then true else false end
  FROM t_visit_illness_address;

ALTER TABLE t_visit_illness_address RENAME TO t_visit_illness_address_temp;
ALTER TABLE t_visit_illness_address_temp
  RENAME CONSTRAINT t_visit_illness_address_pkey TO t_visit_illness_address_temp_pkey;

CREATE TABLE t_visit_illness_address
(
  t_visit_illness_address_id character varying(30) NOT NULL,
  t_visit_id character varying(30) NOT NULL,
  t_address_id character varying(30) NOT NULL,
  active character varying(1) NOT NULL DEFAULT '1'::character varying,
  record_date_time    TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
  user_record_id          CHARACTER VARYING(30) NOT NULL,
  update_date_time    TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
  user_update_id        CHARACTER VARYING(30) NOT NULL,
  CONSTRAINT t_visit_illness_address_pkey PRIMARY KEY (t_visit_illness_address_id),
  CONSTRAINT t_person_address_t_visit_id_t_address_id_active_key UNIQUE (t_visit_id, t_address_id, active)
);

INSERT INTO t_visit_illness_address(t_visit_illness_address_id, t_visit_id, t_address_id, active, record_date_time, user_record_id
    , update_date_time, user_update_id)
select t_visit_illness_address_id
    ,t_visit_id
    ,t_visit_illness_address_id as t_address_id
    ,active
    ,text_to_timestamp(record_date_time) as record_date_time
    ,user_record_id as user_record_id
    ,text_to_timestamp(modify_date_time) as update_date_time
    ,user_modify_id as user_update_id
from t_visit_illness_address_temp;

ALTER TABLE public.t_visit_illness_address ADD user_cancel_id varchar NULL;
ALTER TABLE public.t_visit_illness_address ADD cancel_date_time timestamp WITH TIME zone null;

--t_visit_illness_address_temp
DROP TABLE IF EXISTS t_visit_illness_address_temp;

-- แก้ไขการคำนวณ news/pews score
CREATE OR REPLACE FUNCTION public.calculate_rrscore(visit_vital_sign_respiratory_rate text, agegroup integer)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
    declare
         RRmaxArray int[];
         RRmiduArray int[];
         RRmidlArray int[];
         RRmax int;
         RRmidu int;
         RRmidl int;
         RRmin text;
         RR int;
    BEGIN
        RRmaxArray := '{80, 80, 75, 65, 65, 48, 45, 45, 30, 24}'::int[];
        RRmiduArray := '{70, 70, 65, 55, 55, 38, 35, 35, 20, 20}'::int[];
        RRmidlArray := '{34, 34, 29, 24, 24, 14, 12, 12, 10, 11}'::int[];
        RRmax := RRmaxArray[agegroup];
        RRmidu := RRmiduArray[agegroup];
        RRmidl := RRmidlArray[agegroup];
    
        if(agegroup = 10) then
            RRmin := '8';
        else
            RRmin := 'FALSE';
        end if;
        
        /*raise notice '% % % %',RRmax,RRmidu,RRmidl,RRmin;*/

        if(visit_vital_sign_respiratory_rate is null or visit_vital_sign_respiratory_rate ='') then 
            return null;
        else
            RR := (visit_vital_sign_respiratory_rate::int);
            if(agegroup = 10) then
                if(RRmin = 'FALSE' or RR <= (RRmin::int)) then 
                    return 3;
                elsif(RR <= RRmidl) then
                    return 1;
                elsif(RR <= RRmidu) then
                    return 0;
                elsif(RR <= RRmax) then
                    return 2;
                else
                    return 3;
                end if;
            else
                if(RR < RRmidl) then
                    return 3;
                elsif(RR < RRmidu) then
                    return 0;
                elsif(RR < RRmax) then
                    return 1;
                else
                    return 2;
                end if;
            end if;
        end if;
    END;
$function$
;

CREATE OR REPLACE FUNCTION public.calculate_hrscore(visit_vital_sign_heart_rate text, agegroup integer)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
    declare
         HRmaxAHRay int[];
         HRmiduAHRay int[];
         HRmidlAHRay int[];
         HRmax int;
         HRmidu int;
         HRmid text;
         HRmidl int;
         HRmin text;
         HR int;
    BEGIN
        HRmaxAHRay := '{190, 180, 150, 150, 140, 140, 125, 125, 115, 130}'::int[];
        HRmiduAHRay := '{180, 170, 140, 140, 130, 130, 115, 115, 105, 110}'::int[];
        HRmidlAHRay := '{100, 100, 80, 80, 70, 65, 60, 60, 55, 50}'::int[];
        HRmax := HRmaxAHRay[agegroup];
        HRmidu := HRmiduAHRay[agegroup];
        HRmidl := HRmidlAHRay[agegroup];
        if(agegroup = 10) then
            HRmid := '90';
            HRmin := '40';
        else
            HRmid := 'FALSE';
            HRmin := 'FALSE';
        end if;
    
        
        /*raise notice '% % % % %',HRmax,HRmidu,HRmid,HRmidl,HRmin;*/

        if(visit_vital_sign_heart_rate is null or visit_vital_sign_heart_rate = '') then 
            return null;
        else
            HR := (visit_vital_sign_heart_rate::int);
            if(agegroup = 10) then
                if(HRmin = 'FALSE' or HR <= (HRmin::int)) then 
                    return 3;
                elsif(HR <= HRmidl) then
                    return 1;
                elsif(HRmid = 'FALSE' or HR <= (HRmid::int)) then
                    return 0;
                elsif(HR <= HRmidu) then
                    return 1;
                elsif(HR <= HRmax) then
                    return 2;
                else
                    return 3;
                end if;
            else
                if(HR < HRmidl) then
                    return 3;
                elsif(HR > (HRmidu+30)) then
                    return 3;
                elsif(HR > (HRmidu+20)) then
                    return 2;
                else
                    return 0;
                end if;
            end if;
        end if;
    END;
$function$
;

CREATE OR REPLACE FUNCTION public.calculate_newspews_score(vital_sign_id character varying)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
    DECLARE
        agegroup int;
        rrscore int;
        hrscore int;
        O2supScore int;
        btscore int;
        bpscore int;
        o2satscore int;
        avpuscore int;
        behaviorscore int;
        nebulizescore int;
        vomitingscore int;
        capscore int;
        cardioscore int;
        respiscore int;
    BEGIN
        select calculate_agegroup(t_visit_vital_sign.t_patient_id)
        from t_visit_vital_sign
        into agegroup
        where t_visit_vital_sign.t_visit_vital_sign_id = vital_sign_id;
        
        select calculate_rrscore(t_visit_vital_sign.visit_vital_sign_respiratory_rate,agegroup) as rrscore
            ,calculate_hrscore(t_visit_vital_sign.visit_vital_sign_heart_rate,agegroup) as hrscore
            ,calculate_O2supScore(t_visit_vital_sign.visit_vital_sign_oxygen,agegroup) as O2supScore
            ,calculate_btscore(t_visit_vital_sign.visit_vital_sign_temperature,agegroup) as btscore
            ,calculate_bpscore(t_visit_vital_sign.visit_vital_sign_blood_presure,agegroup) as bpscore
            ,calculate_o2satscore(t_visit_vital_sign.visit_vital_sign_spo2,agegroup) as o2satscore
            ,case when t_visit_vital_sign.f_avpu_type_id is not null then f_avpu_type.avpu_score else null end as avpuscore
            ,case when t_visit_vital_sign.f_behavior_type_id is not null then f_behavior_type.behavior_score else null end as behaviorscore
            ,case when t_visit_vital_sign.f_received_nebulization_id = '0' then 0 
                  when t_visit_vital_sign.f_received_nebulization_id = '1' then 1 
                  else null end as nebulizescore
            ,case when t_visit_vital_sign.f_vomitting_id = '0' then 0 
                  when t_visit_vital_sign.f_vomitting_id = '1' then 1 
                  else null end as vomitingscore
            ,case when t_visit_vital_sign.f_cardiovascular_type_id is not null then f_cardiovascular_type.cap_score else null end as capscore
        from t_visit_vital_sign
            inner join t_visit on t_visit_vital_sign.t_visit_id = t_visit.t_visit_id
            inner join t_patient on t_visit.t_patient_id = t_patient.t_patient_id
            left join f_avpu_type on t_visit_vital_sign.f_avpu_type_id = f_avpu_type.f_avpu_type_id
            left join f_behavior_type on t_visit_vital_sign.f_behavior_type_id = f_behavior_type.f_behavior_type_id
            left join f_cardiovascular_type on t_visit_vital_sign.f_cardiovascular_type_id = f_cardiovascular_type.f_cardiovascular_type_id
        into rrscore,hrscore,O2supScore,btscore,bpscore,o2satscore,avpuscore,behaviorscore,nebulizescore,vomitingscore,capscore
        where t_visit_vital_sign.t_visit_vital_sign_id = vital_sign_id;

        if (capscore > hrscore) then 
            cardioscore := capscore;
        else
            cardioscore := hrscore;
        end if;

        if (rrscore > O2supScore) then 
            respiscore := rrscore;
        else
            respiscore := O2supScore;
        end if;

        if (agegroup = 10 and avpuscore is not null and rrscore is not null and hrscore is not null 
                and O2supScore is not null and btscore is not null and bpscore is not null and o2satscore is not null) then
            return (rrscore+hrscore+O2supScore+btscore+bpscore+o2satscore+avpuscore);
        elsif (agegroup < 10 and behaviorscore is not null and nebulizescore is not null 
                and vomitingscore is not null and cardioscore is not null and respiscore is not null and hrscore is not null) then
            return (behaviorscore+nebulizescore+vomitingscore+cardioscore+respiscore+hrscore);
        else
            return null;
        end if;
          
    END;
$function$
;

--# 470

CREATE TABLE IF NOT EXISTS f_lab_atk_product (
    f_lab_atk_product_id            INTEGER NOT NULL,
    lab_atk_device_name             CHARACTER VARYING(255) NOT NULL,
    lab_atk_fda_reg_no              CHARACTER VARYING(50) NOT NULL,
    lab_atk_manufacturer_code       CHARACTER VARYING(50) DEFAULT NULL,
    lab_atk_manufacturer_name       CHARACTER VARYING(255) DEFAULT NULL,
    lab_atk_manufacturer_country    CHARACTER VARYING(255) DEFAULT NULL,
    lab_atk_manufacturer_url        CHARACTER VARYING(255) DEFAULT NULL,
    lab_atk_product_code            CHARACTER VARYING(50) DEFAULT NULL,
    product_type                    CHARACTER VARYING(50) DEFAULT NULL,
CONSTRAINT f_lab_atk_product_pk PRIMARY KEY (f_lab_atk_product_id)
);

-- เพิ่มเก็บ id ข้อมูลผลิตภัณฑ์ ตาราง b_item
ALTER TABLE b_item ADD f_lab_atk_product_id INTEGER DEFAULT NULL;  

-- เพิ่มเก็บ id ข้อมูลผลิตภัณฑ์ ตาราง t_order
ALTER TABLE t_order ADD f_lab_atk_product_id INTEGER DEFAULT NULL;  

CREATE TABLE IF NOT EXISTS t_visit_covid_lab ( 
    t_visit_covid_lab_id    character varying(50) NOT NULL,
    t_visit_id              character varying(50) NOT NULL,
    sent_complete           INTEGER NOT NULL DEFAULT 0, --0 = not sent ,1 = sent completed, 2 = sent failed 
    record_date_time        TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    user_record_id          CHARACTER VARYING(30) NOT NULL,
    update_date_time        TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    user_update_id          CHARACTER VARYING(30) NOT NULL,
CONSTRAINT t_visit_covid_lab_pk PRIMARY KEY (t_visit_covid_lab_id)
);

DELETE FROM f_lab_atk_product;

INSERT INTO f_lab_atk_product
VALUES(1,'Panbio Covid-19 Ag Rapid Test (1)','T 6400127',null,'Abbott Rapid Diagnostics','Switzerlan',null,'1232','Pro'),
(2,'Panbio Covid-19 Ag Rapid Test (2)','T 6400119',null,'Abbott Rapid Diagnostics','Switzerlan',null,'1232','Pro'),
(3,'Flowflex SARS-CoV-2 Antigen Rapid Test (1)','T 6400165',null,'Acon Biotech (Hangzhou) Co., Ltd','China',null,'1457','Pro'),
(4,'Flowflex SARS-CoV-2 Antigen Rapid Test (2)','T 6400191',null,'Acon Biotech (Hangzhou) Co., Ltd','China',null,'1457','Pro'),
(9,'Flowflex SARS-CoV-2 Antigen rapid test (3)','T 6400231',null,'ACON Laboratories, Inc','United States',null,'1468',null),
(10,'Flowflex SARS-CoV-2 Antigen rapid test (4)','T 6400351',null,'ACON Laboratories, Inc','United States',null,'1468',null),
(11,'AESKU.RAPID SARS-CoV-2','T 6400528',null,'AESKU.DIAGNOSTICS GmbH & Co. KG','Germany',null,'2108',null),
(12,'Rapid COVID-19 Antigen Test (Colloidal Gold) (1)','T 6400153',null,'Anbio (Xiamen) Biotechnology Co., Ltd','China',null,'1822',null),
(13,'Rapid COVID-19 Antigen Test (Colloidal Gold) (2)','T 6400164',null,'Anbio (Xiamen) Biotechnology Co., Ltd','China',null,'1822',null),
(14,'COVID-19 (SARS-CoV-2) Antigen Test Kit (Colloidal Gold) - Nasal Swab','T 6400412',null,'Anhui Deep Blue Medical Technology Co., Ltd','China',null,'1815',null),
(15,'COVID-19 (SARS-CoV-2) Antigen Test Kit(Colloidal Gold) (1)','T 6400428',null,'Anhui Deep Blue Medical Technology Co., Ltd','China',null,'1736',null),
(16,'Asan Easy Test COVID-19 Ag','T 6400370',null,'Asan Pharmaceutical CO., LTD','South Korea',null,'1654',null),
(17,'BD VeritorTM System for Rapid Detection of SARS CoV 2','T 6400183',null,'Becton Dickinson','United States',null,'1065',null),
(18,'Novel Coronavirus (SARS-CoV-2) Antigen Rapid Test Kit (1)','T 6400228',null,'Beijing Jinwofu Bioengineering Technology Co.,Ltd.','China',null,'2072',null),
(19,'Novel Coronavirus (SARS-CoV-2) Antigen Rapid Test Kit (2)','T 6400501',null,'Beijing Jinwofu Bioengineering Technology Co.,Ltd.','China',null,'2072',null),
(20,'SARS-CoV-2 Antigen Rapid Test Kit (Colloidal Gold Immunochromatography)(1)','T 6400123',null,'Beijing Lepu Medical Technology Co., Ltd','China',null,'1331',null),
(21,'SARS-CoV-2 Antigen Rapid Test Kit (Colloidal Gold Immunochromatography)(2)','T 6400167',null,'Beijing Lepu Medical Technology Co., Ltd','China',null,'1331',null),
(22,'SARS-CoV-2 Antigen Rapid Test Kit (Colloidal Gold Immunochromatography) (3)','T 6400190',null,'Beijing Lepu Medical Technology Co., Ltd','China',null,'1331',null),
(23,'NowCheck COVID-19 Ag Test','T 6400179',null,'Bionote, Inc','South Korea',null,'1242',null),
(24,'BIOSYNEX COVID-19 Ag BSS','T 6400022',null,'BIOSYNEX S.A.','Switzerland',null,'1223',null),
(25,'BIOSYNEX COVID-19 Ag+ BSS','T 6400126',null,'BIOSYNEX S.A.','Switzerland',null,'1494',null),
(26,'SARS-CoV-2 Antigen Test Kit (colloidal gold method) (1)','T 6400551',null,'BIOTEKE CORPORATION (WUXI) CO., LTD','China',null,'2067',null),
(27,'SARS-CoV-2 Antigen Test Kit (colloidal gold method) (2)','T 6400560',null,'BIOTEKE CORPORATION (WUXI) CO., LTD','China',null,'2067',null),
(28,'CerTest SARS-CoV-2 Card test','T 6400058',null,'CerTest Biotec','Spain',null,'1173',null),
(29,'OnSite COVID-19 Ag Rapid Test','T 6400055',null,'CTK Biotech, Inc','United States',null,'1581',null),
(30,'DIAQUICK COVID-19 Ag Cassette','T 6400550',null,'DIALAB GmbH','Austria',null,'1375',null),
(31,'ActivXpress+ COVID-19 Antigen Complete Testing Kit','T 6400366',null,'Edinburgh Genetics Limited','United Kingdom',null,'1243',null),
(32,'SARS-CoV-2 Antigen Test Kit (Colloidal Gold) (1)','T 6400472',null,'Genrui Biotech Inc','China',null,'2012',null),
(33,'One Step Test for SARS-CoV-2 Antigen (Colloidal Gold) (1)','T 6400337',null,'Getein Biotech, Inc.','China',null,'2183',null),
(34,'One Step Test for SARS-CoV-2 Antigen (Colloidal Gold) (2)','T 6400441',null,'Getein Biotech, Inc.','China',null,'2183',null),
(35,'One Step Test for SARS-CoV-2 Antigen (Colloidal Gold) (3)','T 6400488',null,'Getein Biotech, Inc.','China',null,'2183',null),
(36,'One Step Test for SARS-CoV-2 Antigen (Colloidal Gold) (4)','T 6400540',null,'Getein Biotech, Inc.','China',null,'2183',null),
(37,'SARS-CoV-2 Antigen Kit (Colloidal Gold) - Nasal swab','T 6400516',null,'Goldsite Diagnostics Inc','China',null,'1197',null),
(38,'GENEDIA W COVID-19 Ag (1)','T 6400367',null,'Green Cross Medical Science Corp.','South Korea',null,'1144',null),
(39,'GENEDIA W COVID-19 Ag (2)','T 6400400',null,'Green Cross Medical Science Corp.','South Korea',null,'1144',null),
(40,'2019-nCoV Antigen Test Kit (colloidal gold method)','T 6400350',null,'Guangdong Hecin Scientific, Inc.','China',null,'1747',null),
(41,'COVID-19 Ag Rapid Test Kit (Immuno-Chromatography) - Saliva','T 6400545',null,'Guangdong Longsee Biomedical Co., Ltd','China',null,'1216',null),
(42,'COVID-19 Ag Test Kit (1)','T 6400450',null,'Guangdong Wesail Biotech Co., Ltd','China',null,'1360',null),
(43,'COVID-19 Ag Test Kit (2)','T 6400496',null,'Guangdong Wesail Biotech Co., Ltd','China',null,'1360',null),
(44,'Wondfo 2019-nCoV Antigen Test (Lateral Flow Method)','T 6400229',null,'Guangzhou Wondfo Biotech Co., Ltd','China',null,'1437',null),
(45,'COVID-19 Antigen Rapid Test','T 6400476',null,'Hangzhou AllTest Biotech Co., Ltd','China',null,'1257',null),
(46,'COVID-19 Antigen Rapid Test Cassette (1)','T 6400376',null,'Hangzhou Clongene Biotech Co., Ltd','China',null,'1610',null),
(47,'Covid-19 Antigen Rapid Test Kit','T 6400368',null,'Hangzhou Clongene Biotech Co., Ltd','China',null,'1363',null),
(48,'COVID-19 Antigen Rapid Test Device๏ (Colloidal Gold) (1)','T 6400383',null,'HANGZHOU LYSUN BIOTECHNOLOGY CO., LTD.','China',null,'2139',null),
(49,'COVID-19 Antigen Rapid Test Device๏ (Colloidal Gold) (2)','T 6400423',null,'HANGZHOU LYSUN BIOTECHNOLOGY CO., LTD.','China',null,'2139',null),
(50,'COVID-19 Antigen Test Cassette','T 6400514',null,'Hangzhou Testsea Biotechnology Co., Ltd','China',null,'1392',null),
(51,'Coronavirus Ag Rapid Test Cassette - Self test','T 6400541',null,'Healgen Scientific','United States',null,'1767',null),
(52,'Corona Virus (COVID-19) Antigen Rapid Test (Colloidal Gold)','T 6400118',null,'Hoyotek Biomedical Co.,Ltd','China',null,'1929',null),
(53,'Humasis COVID-19 Ag Test (1)','T 6400042',null,'Humasis','South Korea',null,'1263',null),
(54,'Humasis COVID-19 Ag Test (2)','T 6400104',null,'Humasis','South Korea',null,'1263',null),
(55,'Innova SARS CoV-2 Antigen Rapid Qualitative Test','T 6400213',null,'Innova Medical Group, Inc.','United States',null,'1801',null),
(56,'SARS-CoV-2 antigen Test Kit (LFIA) (1)','T 6400385',null,'Jiangsu Medomics medical technology Co.,Ltd.','China',null,'2006',null),
(57,'SARS-CoV-2 antigen Test Kit (LFIA) (2)','T 6400393',null,'Jiangsu Medomics medical technology Co.,Ltd.','China',null,'2006',null),
(58,'SARS-CoV-2 Antigen Rapid Test Kit (Colloidal Gold) (1)','T 6400422',null,'JOYSBIO (Tianjin) Biotechnology Co., Ltd','China',null,'1764',null),
(59,'SARS-CoV-2 Antigen Rapid Test Kit (Colloidal Gold) (2)','T 6400339',null,'JOYSBIO (Tianjin) Biotechnology Co., Ltd','China',null,'1764',null),
(60,'SARS-CoV-2 Antigen Rapid Test Kit (Colloidal Gold) (3)','T 6400479',null,'JOYSBIO (Tianjin) Biotechnology Co., Ltd','China',null,'1764',null),
(61,'SARS-CoV-2 Antigen Rapid Test Kit - Nasal swab','T 6400381',null,'Labnovation Technologies Inc','China',null,'1266',null),
(62,'PocRocSARS-CoV-2 Antigen Rapid Test Kit (Colloidal Gold) - Nasal swab','T 6400482',null,'Lumigenex (Suzhou) Co., Ltd','China',null,'2128',null),
(63,'NADAL COVID-19 Ag Test','T 6400214',null,'Nal von minden GmbH','Germany',null,'1162',null),
(64,'Novel Coronavirus (2019-nCoV) Antigen Testing Kit (Colloidal Gold) - Saliva','T 6400468',null,'Nanjing Norman Biological Technology Co.,Ltd','China',null,'2506',null),
(65,'NanoRepro SARS-CoV-2 Antigen Rapid Test','T 6400336',null,'NanoRepro AG','Germany',null,'2200',null),
(66,'COVID-19 Antigen Detection Kit (1)','T 6400416',null,'New Gene (Hangzhou) Bioengineering Co., Ltd','China',null,'1501',null),
(67,'COVID-19 Antigen Detection Kit (2)','T 6400533',null,'New Gene (Hangzhou) Bioengineering Co., Ltd','China',null,'1501',null),
(68,'PCL COVID19 Ag Rapid FIA','T 6400134',null,'PCL Inc','South Korea',null,'308',null),
(69,'PCL COVID19 Ag Gold','T 6400132',null,'PCL Inc.','South Korea',null,'2243',null),
(70,'Exdia COVID-19 Ag','T 6400054',null,'Precision Biosensor, Inc','South Korea',null,'1271',null),
(71,'SARS-CoV-2 Antigen Rapid Test - Saliva','T 6400544',null,'Qingdao Hightop Biotech Co., Ltd','China',null,'1341',null),
(72,'Sofia SARS Antigen FIA','T 6400047',null,'Quidel Corporation','United States',null,'1097',null),
(73,'SARS-CoV-2 Rapid Antigen Test','T 6400006',null,'Roche (SD BIOSENSOR)','Switzerland',null,'1604',null),
(74,'SARS-CoV-2 Rapid Antigen Test Nasal','T 6400170',null,'Roche (SD BIOSENSOR)','Switzerland',null,'2228',null),
(75,'Elecsys? SARS-CoV-2 Antigen','T 640008',null,'ROCHE Diagnostics','Germany',null,'2156',null),
(76,'STANDARD F COVID-19 Ag FIA','T 6400062',null,'SD BIOSENSOR Inc','South Korea',null,'344',null),
(77,'STANDARD Q COVID-19 Ag Test','T6300042',null,'SD BIOSENSOR Inc','South Korea',null,'345',null),
(78,'STANDARD Q COVID-19 Ag Test Nasal','T 6400120',null,'SD BIOSENSOR Inc','South Korea',null,'2052',null),
(79,'Sars-CoV-2 antigen test kit (colloidal gold) - Saliva','T 6400555',null,'Shenzhen Dymind Biotechnology Co., Ltd','China',null,'2415',null),
(80,'SARS-CoV-2 Antigen Test Kit (GICA) (1)','T 6400115',null,'Shenzhen Kisshealth Biotechnology Co., Ltd','China',null,'1813',null),
(81,'SARS-CoV-2 Antigen Test Kit (GICA) (2)','T 6400122',null,'Shenzhen Kisshealth Biotechnology Co., Ltd','China',null,'1813',null),
(82,'Green Spring SARS-CoV-2 Antigen-Rapid test-Set (1)','T 6400203',null,'Shenzhen Lvshiyuan Biotechnology Co., Ltd.','China',null,'2109',null),
(83,'Green Spring SARS-CoV-2 Antigen-Rapid test-Set (2)','T 6400520',null,'Shenzhen Lvshiyuan Biotechnology Co., Ltd.','China',null,'2109',null),
(84,'SARS-CoV-2 Antigen Test Kit (Colloidal Gold Chromatographic Immunoassay) - Saliva','T 6400413',null,'Shenzhen Microprofit Biotech Co., Ltd','China',null,'1967',null),
(85,'SARS-CoV-2 Ag Diagnostic Test Kit (Colloidal Gold) - Nasal swab','T 6400492',null,'Shenzhen Watmind Medical Co., Ltd','China',null,'1769',null),
(86,'CLINITEST Rapid Covid-19 Antigen Test (1)','T 6400129',null,'Siemens Healthineers','Germany',null,'1218',null),
(87,'CLINITEST Rapid Covid-19 Antigen Test (2)','T 6400140',null,'Siemens Healthineers','Germany',null,'1218',null),
(88,'SGTi-flex COVID-19 Ag - Nasal swab','T 6400414',null,'Sugentech, Inc','South Korea',null,'1114',null),
(89,'SARS-CoV-2 Rapid Antigen Test Cassette','T 6400424',null,'SureScreen Diagnostics','United Kingdom',null,'2297',null),
(90,'SARS-CoV-2 Antigen Rapid Test Kit (1)','T 6400371',null,'Triplex International Biosciences (China) Co., LTD.','China',null,'2074',null),
(91,'SARS-CoV-2 Antigen Rapid Test Kit (2)','T 6400372',null,'Triplex International Biosciences (China) Co., LTD.','China',null,'2074',null),
(92,'SARS-CoV-2 Antigen Rapid Test Kit (3)','T 6400371',null,'Triplex International Biosciences Co., Ltd','China',null,'1465',null),
(93,'SARS-CoV-2 Antigen Rapid Test Kit (4)','T 6400372',null,'Triplex International Biosciences Co., Ltd','China',null,'1465',null),
(94,'RapidFor SARS-CoV-2 Rapid Ag Test  - Nasal swab','T 6400473',null,'Vitrosens Biotechnology Co., Ltd','Turkey',null,'1443',null),
(95,'SARS-CoV-2 Antigen Rapid Test Kit','T 6400480',null,'Wuhan UNscience Biotechnology Co., Ltd.','China',null,'2090',null),
(96,'Rapid SARS-CoV-2 Antigen Test Card - Nasal swab (1)','T 6400395',null,'Xiamen Boson Biotech Co. Ltd','China',null,'1278',null),
(97,'Rapid SARS-CoV-2 Antigen Test Card - Nasal swab (2)','T 6400511',null,'Xiamen Boson Biotech Co. Ltd','China',null,'1278',null),
(98,'reOpenTest COVID-19 Antigen Rapid Test - Nasal swab','T 6400389',null,'Zhejiang Anji Saianfu Biotech Co., Ltd','China',null,'1295',null),
(99,'Coronavirus Ag Rapid Test Cassette (Swab)','T 6400204',null,'Zhejiang Orient Gene Biotech Co.,Ltd.','China',null,'1343',null),
(100,'COVID-19 Antigen Detection Kit (Colloidal Gold) (1)','T 6400474',null,'Zhuhai Lituo Biotechnology Co., Ltd','China',null,'1957',null),
(101,'COVID-19 Antigen Detection Kit (Colloidal Gold) (2)','T 640053',null,'Zhuhai Lituo Biotechnology Co., Ltd','China',null,'1957',null),
(102,'Panbio COVID-19 Antigen Self-Test','T 6400127',null,'Abbott Diagnostics Korea Inc.,','Korea',null,null,null),
(103,'COVID-19 Antigen Rapid Test ยี่ห้อ Seinofy','T 64000557',null,'Zhejiang Anji Saianfu Biotech Co., Ltd.','China',null,null,null),
(105,'SARS-CoV-2 Antigen Test Kit (Colloidal gold method) ยี่ห้อ Bio Teke','T 64000560',null,'BIOTEKE CORPORATION (WUXI) CO., LTD.','China',null,null,null),
(106,'Novel Coronavirus (SARS-CoV-2) Antigen Rapid Test Device (nasal swab) (1)','T 64000566',null,'Hangzhou Realy Co., Ltd.','China',null,null,null),
(107,'COVID-19 Antigen Sealing Tube Test Strip (Colloidal Gold)','T 64000567',null,'AMAZING BIOTECH (Shanghai) Co., Ltd.','null',null,null,null),
(108,'COVIFIND COVID-19 Antigen SELF TEST','T 64000568',null,'Meril Diagnostics Private Limited','India',null,null,null),
(109,'SARS-CoV-2 Antigen Rapid Test Kit ยี่ห้อ Diasia','T 64000569',null,'Diasia Biomedical Technology Co., Ltd.','China',null,null,null),
(110,'SARS-CoV-2 Antigen Test Kit (Colloidal Gold Method)  ยี่ห้อ BIO TEKE','T 64000570',null,'BIOTEKE CORPORATION (WUXI) CO., LTD','China',null,null,null),
(111,'RapidFor SARS-CoV-2 Rapid Antigen Test','T 64000572',null,'Vitrosens Biyoteknoloji Ltd. Sti. Serifali Mh. Sehit Sk.','Turkey',null,null,null),
(112,'BHM COVID-19 Antigen Rapid Test (1)','T 64000574',null,'BIRMINGHAM BIOTECH LTD.','United Kingdom',null,null,null),
(113,'Virusee SARS-CoV-2 Antigen Rapid Test (Colloidal Gold)','T 64000575',null,'Genobio Pharmaceutical Co., Ltd.','China',null,null,null),
(114,'Rapid SARS-CoV-2 Antigen Test Card (1)','T 64000578',null,'MP Biomedicals Germany GmbH','Germany',null,null,null),
(115,'COVID-19 Antigen Test Cassette (Saliva)','T 64000580',null,'Hangzhou Testsea Biotechnology Co., Ltd.','China',null,null,null),
(116,'COVID-19 Antigen Test Cassette (Nasal swab) (1)','T 64000581',null,'Hangzhou Testsea Biotechnology Co.,Ltd.','China',null,null,null),
(117,'SARS-CoV-2 Antigen Rapid Test Kit (Colloidal Gold) (4)','T 64000582',null,'JOYSBIO (TIANJIN) BIOTECHNOLOGY CO., LTD.','China',null,null,null),
(118,'One Step Test for SARS-CoV-2 Antigen (Colloidal Gold) (Saliva)','T 64000583',null,'Getein Biotech, Inc.','China',null,null,null),
(119,'COVICHEK COVID-19 Ag SALIVA HT','T 64000584',null,'Wizchem Co., Ltd.','Korea',null,null,null),
(120,'COVICHEK COVID-19 Ag Saliva Test At-Home','T 64000585',null,'Wizchem Co., Ltd.','Korea',null,null,null),
(122,'COVID-19 Antigen test Kit (Dry Color Latex Immunoassay)','T 64000587',null,'Lansion Biotechnology Co., Ltd.','China',null,null,null),
(123,'Gmate COVID-19 Ag Saliva','T 64000588',null,'PHILOSYS Co., Ltd.','Korea',null,null,null),
(124,'SARS-CoV-2 Antigen Rapid Test','T 64000591',null,'Hanzhou Genesea Biotechnology Co., Ltd.','China',null,null,null),
(125,'LONGSEE 2019-nCoV Ag Rapid Detection Kit (Immuno-Chromatography)','T 64000592',null,'Guangdong Longsee Biomedical Co. Ltd.','China',null,null,null),
(126,'Floxflex SARS-CoV-2 Antigen Rapid Test (Nasal/Saliva)','T 64000597',null,'ACON Biotech (Hangzhou) Co., Ltd.','China',null,null,null),
(127,'GENODE COVID-19 Antigen Rapid Self Test','T 64000600',null,'Bao Ruiyuan Biotech (Bejing) Co., Ltd.','China',null,null,null),
(128,'Livzon Rapid Test for SARS-CoV-2 Antigen (Lateral Flow)','T 64000603',null,'Zhuhai Livzon Diagnostics Inc.','China',null,null,null),
(130,'SGTi-flex COVID-19 Ag','T 64000607',null,'SUGENTECH, Inc.','Republic of Korea',null,null,null),
(131,'SARS-CoV-2 Antigen Rapid Test Kit (Colloidal Gold) ยี่ห้อ JOYSBIO','T 64000611',null,'JOYSBIO (TIANJIN) BIOTECHNOLOGY CO.,LTD.','China',null,null,null),
(133,'BHM COVID-19 Antigen Rapid Test (2)','T 64000614',null,'BIRMINGHAM BIOTECH LTD.','United Kingdom',null,null,null),
(134,'2019-nCoV Ag Test (Latex Chromatography Assay)','T 64000616',null,'Innovita (Tangshan) Biological Technology Co., Ltd.','China',null,null,null),
(135,'XABT 2019-nCoV Antigen Rapid Test Cassette Anterior Nasal Swab','T 64000618',null,'Beijing Applied Biological Technologies Co., Ltd.','China',null,null,null),
(136,'TESTSEALABS RAPID TEST KIT COVID-19 ANTIGEN TEST CASSETTE (SALIVA)','T 64000619',null,'HANGZHOU TESTSEA BIOTECHNOLOGY LTD.','China',null,null,null),
(137,'TESTSEALABS RAPID TEST KIT COVID-19 ANTIGEN TEST CASSETTE (1)','T 64000620',null,'HANGZHOU TESTSEA BIOTECHNOLOGY LTD.','China',null,null,null),
(138,'DOCTOR''S CHOICE NOVEL CORONA VIRUS (SARS-COV-2) SALIVARY ANTIGEN RAPID TEST KITS (DIPSTICK)','T 64000621',null,'SYMBION ASIASDN BHD.','MALAYSIA',null,null,null),
(139,'SARS-CoV-2 Antigen Self Test Nasal','T 6400121',null,'SD Biosensor Inc.,','Korea',null,null,null),
(140,'AnyLab COVID-19 Ag Test Kit','T 6400131',null,'Z Biotech Inc.,','Korea',null,null,null),
(143,'GSD NovaGen SARS-CoV-2 Ag Rapid Test','T 6400142',null,'Hangzhou AllTest Biotech Co Ltd,','China',null,null,null),
(144,'Vstrip COVID-19 Antigen Rapid Test Home Use RV2','T 6400143',null,'Panion & BF Biotech Inc. Xizhi Factory,','Taiwan',null,null,null),
(145,'FORA/ VTRUST COVID-19 ANTIGEN SELF TEST','T 6400144',null,'TaiDoc Technology Corp.','Taiwan',null,null,null),
(146,'HUMASIS COVID-19 Ag Home Test (1)','T 6400145',null,'Humasis Co., Ltd.','Republic of Korea',null,null,null),
(147,'CerTest SARS-CoV-2 (Ag-Nasal Sample-self Test)','T 6400146',null,'CERTEST BIOTECT','Spain',null,null,null),
(148,'Novel Coronavirus Ag Rapid Test Cassette','T 6400147',null,'Zhejiang Aiminde Biotechnology Co.,Ltd.','China',null,null,null),
(149,'Saliva SARS-CoV-2 (2019-CoV) Antigen Test Kit (Nanocarbon Assay)','T 6400148',null,'Jiaxing WiseTest Bio-Tech Co. Ltd.','China',null,null,null),
(151,'VTRUST COVID-19 Antigen Rapid Test','T 6400155',null,'Taidoc Technology Corporation Wugu Factory.','Taiwan',null,null,null),
(153,'SARS-CoV-2 Antigen Detection Kit  (Colloidal Gold Method) (1)','T 6400159',null,'Xiamen Jiqing Biomedical Technology Co, Ltd','China',null,null,null),
(154,'BIOCREDIT COVID-19 Ag Home Test Nasal (1)','T 6400162',null,'RapiGEN, Inc.','Korea',null,null,null),
(155,'INSTANT-VIEW PLUS COVID-19 ANTIGEN TEST SELF-TEST KIT','T 6400174',null,'ALFA SCIENTIFIC DESIGNS,','USA',null,null,null),
(157,'Novel Coronavirus (COVID-19) Antigen Test Kit','T 6400181',null,'Hangzhou Laihe Biotech Co.Ltd.','China',null,null,null),
(159,'2019-nCoV Antigen Test Kit','T 6400193',null,'Guangdong Hecin Scientific, Inc,','China',null,null,null),
(160,'COVID-19 Antigen Test Kit (Colloidal gold method)','T 6400196',null,'Nantong Diagnos Biotechnology Co., Ltd,','China',null,null,null),
(161,'SARS-CoV-2 Antigen Rapid Qualitative Test','T 6400197',null,'Xiamen Biotime Biotechnology Co., Ltd,','China',null,null,null),
(162,'SARS-CoV-2 Antigen (GICA)','T 6400199',null,'Shenzhen Lifotronic Technology Co., Ltd,','China',null,null,null),
(163,'Flowflex SARS-CoV-2 Antigen Rapid Test (5)','T 6400208',null,'ACON Biotech (Hangzhou) Co., Ltd.','China',null,null,null),
(164,'SARS-CoV-2 Antigen Assay Kit (Colloidal Gold Method) (1)','T 6400209',null,'Zybio Inc.,','China',null,null,null),
(165,'Novel Coronavirus (COVID-19) Antigen Test Kit (Colloidal Gold)','T 6400215',null,'Hangzhou Laihe Biotech Co., Ltd.','China',null,null,null),
(167,'COVID-19 Antigen Rapid Test Cassette (2)','T 6400217',null,'Hangzhou Testsea Biotechnology Co., Ltd.','China',null,null,null),
(168,'AFFINOME COVID-19 Ag SELF TEST','T 6400218',null,'บริษัท แอฟฟิโนม จำกัด','null',null,null,null),
(169,'Humasis COVID-19 Ag Home Test Self test','T 6400219',null,'Humasis Co., Ltd.','Republic of Korea',null,null,null),
(170,'COVID-19 Antigen Rapid Test Cassette (3)','T 6400225',null,'Hangzhou Biotest Biotech Co., Ltd.','China',null,null,null),
(171,'P4DETECT COVID-19 Ag','T 6400230',null,'PRIME4DIA Co.,LTD.','Republic of Korea',null,null,null),
(172,'COVID-19 (SARS-CoV-2) Antigen Test Kit (Colloidal Gold)','T 6400232',null,'ANHUI DEEPBLUE MEDICAL TECHNOLOGY CO., LTD.','China',null,null,null),
(173,'COVID-19 (SARS-CoV-2) Antigen Test Kit(Colloidal Gold) (2)','T 6400232',null,'ANHUI DEEPBLUE MEDICAL TECHNOLOGY CO., LTD.','China',null,null,null),
(174,'One Step Test for SARS-CoV-2 Antigen (Colloidal Gold) (5)','T 6400337',null,'Getein Biotech, Inc.','China',null,null,null),
(176,'Severe Acute Respiratory Syndrome Coronavirus 2 (SARS-CoV-2) Antigen Assay Kit by Colloidal Gold Method','T 6400345',null,'Maccura Biotechnology Co., Ltd','China',null,null,null),
(177,'VIRASPEC Covid-19 Tests (Sars-CoV-2) Saliva Test','T 6400346',null,'Thyrolytics AB,','Sweden',null,null,null),
(179,'BIOCREDIT COVID-19 Ag Home Test Nasal (2)','T 6400352',null,'RapiGEN, Inc.','Republic of Korea',null,null,null),
(180,'Rapid COVID-19 Antigen Test (Colloidal Gold) (3)','T 6400354',null,'Anbio (Xiamen) Biotechnology Co., Ltd.','China',null,null,null),
(181,'SARS-CoV-2 Antigen Rapid Test (Colloidal Gold)','T 6400355',null,'Baicare (Tianjin) Biotechnology','China',null,null,null),
(182,'Rapid COVID-19 Antigen Test (Colloidal Gold) (4)','T 6400357',null,'Ustar Biotechnologies (Hangzhou)  Ltd.','China',null,null,null),
(183,'Testsealabs COVID-19 antigen Test Cassette','T 6400358',null,'Hangzhou Testsea Biotechnology  Co., Ltd.','China',null,null,null),
(185,'Covid-19 Antigen Rapid Test Kit (Colloidal Gold)','T 6400359',null,'Jiangsu Konsung Bio-Medical Science And Technology Co., Ltd.','China',null,null,null),
(186,'SARS-CoV-2 Antigen Detection Kit  (Colloidal Gold Method) (2)','T 6400377',null,'Xiamen Jiqing Biomedical Technology Co., Ltd.','China',null,null,null),
(187,'COVID-19 Test Kit (Colloidal Gold Method)','T 6400382',null,'Hangzhou Singclean Medical Products Co., Ltd.','China',null,null,null),
(188,'NanoCOVID-19 Antigen SELF TEST','T 6400384',null,'ศูนย์นาโนเทคโนโลยีแห่งชาติ สวทช.','null',null,null,null),
(189,'Accufast SARS-CoV-2 Antigen Rapid Test Kit','T 6400387',null,'Ningbo Home Test Bio-technology Co., Ltd. (CHINA)','China',null,null,null),
(191,'SARS-CoV-2 Antigen Detection Kit  (Colloidal Gold Method) (3)','T 6400390',null,'Changsha Sinocare Inc.','China',null,null,null),
(192,'CLINITEST Rapid COVID-19 Antigen Self-Test','T 6400396',null,'Zhejiang Orient Gene Biotech Co., Ltd.','China',null,null,null),
(193,'TESTSEALABS RAPID TEST KIT COVID-19 ANTIGEN TEST CASSETTE (2)','T 6400397',null,'Hangzhou Testsea Biotechnology Co., Ltd.','China',null,null,null),
(194,'COVID-19 Test Kit (Colloidal Gold Method) ยี่ห้อ Singclean','T 6400401',null,'Hangzhou Singclean Medical Products Co., Ltd.','China',null,null,null),
(195,'VivaDiag SARS-CoV-2 Ag Saliva Rapid Test','T 6400405',null,'VivaCheck Biotech (Hangzhou) Co., Ltd.','China',null,null,null),
(196,'SARS-CoV-2 Antigen Test Kit (Colloidal Gold) Brand GENRUI','T 6400408',null,'GENRUI Biotech Inc.','China',null,null,null),
(197,'COVID-19 Antigen Test Kit','T 6400415',null,'Nantong Diagnos Biotechnology Co., Ltd.','China',null,null,null),
(200,'COVID-19 Antigen Test Cassette (Nasal swab) (2)','T 6400417',null,'SureScreen Diagnostics Ltd.','United Kingdom',null,null,null),
(202,'COVID-19 Antigen Rapid Test Device ยี่ห้อ Fastep','T 6400420',null,'Assure Tech. (Hangzhou) Co., Ltd.','China',null,null,null),
(203,'SARS-CoV-2 Antigen Detection Kit  (Colloidal Gold Method) (4)','T 6400421',null,'Xiamen Jiqing Biomedical Technology Co., Ltd.','China',null,null,null),
(204,'Covid-19 Salivary Antigen Rapid Test Kit (Colloidal Gold) ยี่ห้อ Konsung','T 6400425',null,'Jiangsu Konsung Bio Medical Science And Technology Co., Ltd.','China',null,null,null),
(205,'UGDx COVID-19 Ag Rapid Test','T 6400426',null,'UNIANCE GENE Co., LTD.','Republic of Korea',null,null,null),
(206,'StrongStep x Crazydoctor Premium SARS-CoV-2 Antigen Rapid Test','T 6400429',null,'Nanjing Limited Bio-Products Co., Ltd.','China',null,null,null),
(207,'StrongStep x Crazydoctor Premium SARS-CoV-2 Antigen Rapid Test (Saliva)','T 6400430',null,'Nanjing Limited Bio-Products Co., Ltd.','China',null,null,null),
(208,'StrongStep x Crazydoctor SARS-CoV-2 Antigen Rapid Test (Nasal Swab)','T 6400431',null,'Nanjing Limited Bio-Products Co., Ltd.','China',null,null,null),
(209,'StrongStep x Crazydoctor SARS-CoV-2 Antigen Rapid Test (Saliva)','T 6400432',null,'Nanjing Limited Bio-Products Co., Ltd.','China',null,null,null),
(210,'Coronavirus (2019-nCoV)-Antigentest- ยี่ห้อ Hotgen (4)','T 6400434',null,'Beijing Hotgen Biotech Co., Ltd.','China',null,null,null),
(212,'Alpine Biomedical COVID-19 Antigen Rapid Test Kit','T 6400437',null,'Alpine Biomedical Pvt.Ltd.','Replublic of India',null,null,null),
(213,'TESTSEALABS RAPID TEST KIT COVID-19 ANTIGEN TEST CASSETTE (3)','T 6400438',null,'HANGZHOU TESTSEA BIOTECHNOLOGY CO., LTD.','China',null,null,null),
(214,'Rapid SARS-CoV-2 Antigen Test Colloidal Gold (Nasal Specimen)','T 6400444',null,'Intec Products., Inc.','China',null,null,null),
(215,'SARS-CoV-2 Antigen Detection Kit (Colloidal Gold Method)','T 6400445',null,'Xiamen Jiqing Biomedical Technology Co., Ltd.','China',null,null,null),
(216,'Coronavirus (2019-nCoV)-Antigentest- ยี่ห้อ Hotgen (1)','T 6400447',null,'Beijing Hotgen Biotech Co., Ltd.','China',null,null,null),
(217,'Coronavirus (2019-nCoV)-Antigentest- ยี่ห้อ Hotgen (2)','T 6400451',null,'Beijing Hotgen Biotech Co., Ltd.','China',null,null,null),
(218,'SARS-CoV-2 Antigen Rapid Test Kit (Colloidal Gold) (5)','T 6400452',null,'JOYSBIO (Tianjin) Biotechnology Co., Ltd.','China',null,null,null),
(219,'COVID-19 Antigen Rapid Test Cassette (Saliva)','T 6400453',null,'SureScreen Diagnostics Ltd.','United Kingdom',null,null,null),
(220,'COVID-19 (SARS-CoV-2) Antigen Test Kit(Colloidal Gold) (3)','T 6400454',null,'ANHUI DEEPBLUE MEDICAL TECHNOLOGY CO., LTD.','China',null,null,null),
(221,'SARS-CoV-2 Antigen (GICA) ยี่ห้อ Lifotronic','T 6400458',null,'Shenzhen Lifotronic Technology Co., Ltd.','China',null,null,null),
(222,'Rapid SARS-CoV-2 Antigen Test Card (2)','T 6400460',null,'BioDetect (Xiamen) Biotechnology co., ltd.','China',null,null,null),
(223,'BIOCREDIT COVID-19 Ag Home Test Nasal','T 6400462',null,'Rapigen, Inc.','Republic of Korea',null,null,null),
(224,'SARS-CoV-2 Rapid Ag Test (Saliva) ยี่ห้อ Besthree','T 6400463',null,'Jiangsu Besthree Biotechnology Co., Ltd.','China',null,null,null),
(225,'SURE STATUS COVID-19 ANTIGEN CARD TEST','T 6400464',null,'Premier Medical Corporation Private Limited.','India',null,null,null),
(226,'COVID-19 (SARS-CoV-2) Antigen Test Kit (Colloidal Gold) ยี่ห้อ Deepblue','T 6400465',null,'ANHUI DEEPBLUE MEDICAL TECHNOLOGY CO., LTD.','China',null,null,null),
(227,'SARS-CoV-2 Antigen Rapid Test Cassette ชื่อทางการค้าHEALTH IMPACT','T 6400466',null,'Hangzhou Sejoy Electronics & Instruments Co., Ltd.','China',null,null,null),
(228,'COVID-19 Antigen Rapid Test Kit (Short nose)','T 6400467',null,'Beijing Beier Bioengineering Co., Ltd.','China',null,null,null),
(230,'SARS-CoV-2 Antigen Test Kit (Colloidal Gold) (2)','T 6400471',null,'Genrui Biotech Inc.','China',null,null,null),
(231,'COVID-19 Antigen Detection Kit (Colloidal Gold) ยี่ห้อ LITUO','T 6400481',null,'ZHUHAI LITUO BIOTECHNOLOGY CO., LTD.','China',null,null,null),
(234,'HUMASIS COVID-19 Ag Home Test (2)','T 6400484',null,'Humasis Co., Ltd.','China',null,null,null),
(235,'SARS-CoV-2 Antigen Rapid Detection Kit','T 6400485',null,'Shenzhen CASENVISION Medical Technology Co., ltd.','China',null,null,null),
(236,'COVID-19 Antigen Test Cassette Model : 2 in 1 For nasal swab and saliva','T 6400495',null,'Hangzhou Testsea Biotechnology Co., Ltd.','China',null,null,null),
(237,'COVID-19 Antigen Test Cassette Model : For nasal swab','T 6400495',null,'Hangzhou Testsea Biotechnology Co., Ltd.','China',null,null,null),
(238,'COVID-19 Antigen Test Cassette Model : For saliva','T 6400495',null,'Hangzhou Testsea Biotechnology Co., Ltd.','China',null,null,null),
(240,'COVID-19 Antigen Rapid Test Cassette (Colloidal Gold)','T 6400497',null,'Hangzhou HEO Technology Co., Ltd.','China',null,null,null),
(241,'COVID-19 Antigen Nasal Test Kit ยี่ห้อ Fastep','T 6400499',null,'Assure Tech. (Hangzhou) Co., Ltd.','China',null,null,null),
(242,'UNIBIOSCIENCE COVID-19 SALIVA ANTIGEN HOME TEST','T 6400505',null,'UNIBIOSCIENCE AS','Turkey',null,null,null),
(243,'Coronavirus (2019-nCoV)-Antigentest- ยี่ห้อ Hotgen (3)','T 6400506',null,'Beijing Hotgen Biotech Co., Ltd.','China',null,null,null),
(245,'Q-Sens COVID-19 Ag','T 6400507',null,'PRIME4DIA Co., Ltd','Republic of Korea',null,null,null),
(246,'COVID-19 Antigen Rapid Test Cassette (Saliva) ยี่ห้อ ANIMS','T 6400508',null,'Hangzhou Clongene Biotech Co., Ltd.','China',null,null,null),
(247,'COVID-19 Antigen Rapid Detection Kit (Colloidal Gold)','T 6400512',null,'Pro-med (Beijing) Technology Co., Ltd.','China',null,null,null),
(248,'COVID-19 Antigen Lateral Flow Test Device OTC Home Test ยี่ห้อ CoviSel','T 6400517',null,'Mylab Discovery Solutions Pvt.Ltd.','India',null,null,null),
(249,'SARS-CoV-2 Antigen Assay Kit (Colloidal Gold Method) (2)','T 6400524',null,'Zybio Inc.','China',null,null,null),
(250,'ADTech COVID-19 Ag RAPID KIT','T 6400525',null,'ADTech Co., LTD','Republic of Korea',null,null,null),
(252,'Novel Coronavirus (SARS-CoV-2) Antigen Rapid Test Device (nasal swab) (2)','T 6400526',null,'Hangzhou Realy Tech Co., Ltd.','China',null,null,null),
(254,'Tigsun COVID-19 Saliva Antigen Rapid Test','T 6400530',null,'Beijing Tigsun Diagnostics Co., Ltd.','China',null,null,null),
(255,'Healfo SARS-CoV-2 Ag Saliva Rapid Test','T 6400531',null,'Healvet Medtech GZ Ltd.','China',null,null,null),
(257,'SARS-CoV-2 Antigen Rapid Detection Kit ยี่ห้อ CAS-ENVISION','T 6400534',null,'Shenzhen CASEnvision Medical Technology Co., Ltd.','China',null,null,null),
(258,'COVID-19 Antigen Test Cassette (Saliva) ยี่ห้อ TESTSEALABS','T 6400536',null,'HANGZHOU TESTSEA BIOTECHNOLOGY CO., LTD.','China',null,null,null),
(259,'Novel Coronavirus (SARS-CoV-2) Antigen Rapid Test Kit (3)','T 6400537',null,'Beijing Jinwofu Bioengineering Technology Co., Ltd.','China',null,null,null),
(261,'COVID Test USSW SARS-CoV-2 Antigen Rapid Test Kit','T 6400539',null,'Wuhan Unscience Biotechnology Co., Ltd.','China',null,null,null),
(263,'COVID-19 (SARS-CoV-2) Antigen Test Kit (Colloidal Gold) For Self-testing','T 6400543',null,'ANHUI DEEPBLUE MEDICAL TECHNOLOGY CO.,LTD.','China',null,null,null),
(264,'COVID-19 (SARS-CoV-2) Antigen Test Kit (Colloidal Gold) ยี่ห้อ DEEPBLUE','T 6400547',null,'ANHUI DEEPBLUE MEDICAL TECHNOLOGY CO.,LTD.','China',null,null,null),
(265,'Novel Coronavirus (SARS-CoV-2) Antigen Rapid Test Device (saliva)','T 6400554',null,'Hangzhou Realy Co., Ltd.','China',null,null,null),
(266,'Novel Coronavirus 2019-nCoV Antigen Test (Colloidal Gold) ยี่ห้อ Hotgen','T 6500003',null,'Beijing HotgenBiotech Co., Ltd.','China',null,null,null);

CREATE TABLE public.f_covid_tmlt (
    f_covid_tmlt_id serial4 NOT NULL,
    tmlt_code varchar NOT NULL,
    tmlt_name varchar NOT NULL,
    component varchar NULL,
    specimen varchar NULL,
    "method" varchar NULL,
    CONSTRAINT f_covid_tmlt_pk PRIMARY KEY (f_covid_tmlt_id),
    CONSTRAINT f_covid_tmlt_un UNIQUE (tmlt_code)
);

INSERT INTO public.f_covid_tmlt
(f_covid_tmlt_id, tmlt_code, tmlt_name, component, specimen, "method")
VALUES(1, '350501', 'SARS coronavirus 2 RNA [+/-] in Respiratory specimen by NAA with probe detection', 'SARS coronavirus 2 RNA', 'Respiratory specimen', 'NAA with probe detection');
INSERT INTO public.f_covid_tmlt
(f_covid_tmlt_id, tmlt_code, tmlt_name, component, specimen, "method")
VALUES(2, '350502', 'SARS coronavirus 2 N gene [+/-] in Respiratory specimen by NAA with probe detection', 'SARS coronavirus 2 N gene', 'Respiratory specimen', 'NAA with probe detection');
INSERT INTO public.f_covid_tmlt
(f_covid_tmlt_id, tmlt_code, tmlt_name, component, specimen, "method")
VALUES(3, '350503', 'SARS coronavirus 2 RdRp gene [+/-] in Respiratory specimen by NAA with probe detection', 'SARS coronavirus 2 RdRp gene', 'Respiratory specimen', 'NAA with probe detection');
INSERT INTO public.f_covid_tmlt
(f_covid_tmlt_id, tmlt_code, tmlt_name, component, specimen, "method")
VALUES(4, '350506', 'SARS coronavirus 2 IgG Ab [+/-] in Serum or Plasma by Rapid immunoassay', 'SARS coronavirus 2 IgG Ab', 'Serum or Plasma', 'Rapid immunoassay');
INSERT INTO public.f_covid_tmlt
(f_covid_tmlt_id, tmlt_code, tmlt_name, component, specimen, "method")
VALUES(5, '350507', 'SARS coronavirus 2 IgM Ab [+/-] in Serum or Plasma by Rapid immunoassay', 'SARS coronavirus 2 IgM Ab', 'Serum or Plasma', 'Rapid immunoassay');
INSERT INTO public.f_covid_tmlt
(f_covid_tmlt_id, tmlt_code, tmlt_name, component, specimen, "method")
VALUES(6, '350508', 'SARS coronavirus 2 IgG+IgM Ab [+/-] in Serum or Plasma by Immunoassay', 'SARS coronavirus 2 IgG+IgM Ab', 'Serum or Plasma', 'Immunoassay');
INSERT INTO public.f_covid_tmlt
(f_covid_tmlt_id, tmlt_code, tmlt_name, component, specimen, "method")
VALUES(7, '350509', 'SARS coronavirus 2 Ag [+/-] in Respiratory specimen by Rapid immunoassay', 'SARS coronavirus 2 Ag', 'Respiratory specimen', 'Rapid immunoassay');
INSERT INTO public.f_covid_tmlt
(f_covid_tmlt_id, tmlt_code, tmlt_name, component, specimen, "method")
VALUES(8, '350926', 'SARS-CoV-2 (COVID-19) IgA Ab [+/-] in Serum or Plasma by Immunoassay', 'SARS-CoV-2 (COVID-19) IgA Ab', 'Serum or Plasma', 'Immunoassay');
INSERT INTO public.f_covid_tmlt
(f_covid_tmlt_id, tmlt_code, tmlt_name, component, specimen, "method")
VALUES(9, '350998', 'SARS-CoV-2 (COVID-19) RNA [+/-] in Saliva (oral fluid) by NAA with probe detection', 'SARS-CoV-2 (COVID-19) RNA', 'Saliva (oral fluid)', 'NAA with probe detection');
INSERT INTO public.f_covid_tmlt
(f_covid_tmlt_id, tmlt_code, tmlt_name, component, specimen, "method")
VALUES(10, '351015', 'SARS-CoV-2 (COVID-19) IgG Ab [+/-] in Serum or Plasma by Immunoassay', 'SARS-CoV-2 (COVID-19) IgG Ab', 'Serum or Plasma', 'Immunoassay');
INSERT INTO public.f_covid_tmlt
(f_covid_tmlt_id, tmlt_code, tmlt_name, component, specimen, "method")
VALUES(11, '351016', 'SARS-CoV-2 (COVID-19) IgG Ab [U/mL] in Serum or Plasma by Immunoassay"', 'SARS-CoV-2 (COVID-19) IgG Ab', 'Serum or Plasma', 'Immunoassay');
INSERT INTO public.f_covid_tmlt
(f_covid_tmlt_id, tmlt_code, tmlt_name, component, specimen, "method")
VALUES(12, '351017', 'SARS-CoV-2 (COVID-19) IgG+IgM Ab [+/-] in Serum, Plasma or Blood by Rapid immunoassay', 'SARS-CoV-2 (COVID-19) IgG+IgM Ab', 'Serum, Plasma or Blood', 'Rapid immunoassay');
INSERT INTO public.f_covid_tmlt
(f_covid_tmlt_id, tmlt_code, tmlt_name, component, specimen, "method")
VALUES(13, '351024', 'SARS-CoV-2 (COVID-19) IgM Ab [+/-] in Serum or Plasma by Immunoassay', 'SARS-CoV-2 (COVID-19) IgM Ab', 'Serum or Plasma', 'Immunoassay');
INSERT INTO public.f_covid_tmlt
(f_covid_tmlt_id, tmlt_code, tmlt_name, component, specimen, "method")
VALUES(14, '351087', 'SARS-CoV-2 (COVID-19) ORF1ab region [+/-] in Respiratory specimen by NAA with probe detection', 'SARS-CoV-2 (COVID-19) ORF1ab region', 'Respiratory specimen', 'NAA with probe detection');
INSERT INTO public.f_covid_tmlt
(f_covid_tmlt_id, tmlt_code, tmlt_name, component, specimen, "method")
VALUES(15, '351121', 'SARS coronavirus 2, 3 genes [+/-] in Respiratory specimen by NAA with probe detection', 'SARS coronavirus 2, 3 genes', 'Respiratory specimen', 'NAA with probe detection');
INSERT INTO public.f_covid_tmlt
(f_covid_tmlt_id, tmlt_code, tmlt_name, component, specimen, "method")
VALUES(16, '351122', 'SARS coronavirus 2, Ag [+/-] in Respiratory specimen by Chromatography', 'SARS coronavirus 2 Ag', 'Respiratory specimen', 'Chromatography');
INSERT INTO public.f_covid_tmlt
(f_covid_tmlt_id, tmlt_code, tmlt_name, component, specimen, "method")
VALUES(17, '351123', 'SARS coronavirus 2, 2 genes [+/-] in Respiratory specimen by NAA with probe detection', 'SARS coronavirus 2, 2 genes', 'Respiratory specimen', 'NAA with probe detection');
INSERT INTO public.f_covid_tmlt
(f_covid_tmlt_id, tmlt_code, tmlt_name, component, specimen, "method")
VALUES(18, '351134', 'SARS-CoV-2 (COVID-19) variant in XXX specimen by Sequencing', 'SARS-CoV-2 (COVID-19) variant', 'XXX specimen', 'Sequencing');
INSERT INTO public.f_covid_tmlt
(f_covid_tmlt_id, tmlt_code, tmlt_name, component, specimen, "method")
VALUES(19, '351135', 'SARS-CoV-2 (COVID-19) lineage in XXX specimen by Molecular genetics method', 'SARS-CoV-2 (COVID-19) lineage', 'XXX specimen', 'Molecular genetics method"');
INSERT INTO public.f_covid_tmlt
(f_covid_tmlt_id, tmlt_code, tmlt_name, component, specimen, "method")
VALUES(20, '351136', 'SARS-CoV-2 (COVID-19) S gene mutation [+/-] in XXX specimen by Molecular genetics method', 'SARS-CoV-2 (COVID-19) S gene mutation', 'XXX specimen', 'Molecular genetics method"');

-- update db version
INSERT INTO s_version VALUES ('9701000000105', '105', 'Hospital OS, Community Edition', '3.9.67', '3.47.0', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_67.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.67b01');

-- require when use other postgres user
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA public to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA thai_ascvd to hospitalos;
-- GRANT ALL ON all sequences in schema thai_ascvd to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA rp50 to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA rp50nhso to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA rp50data to hospitalos;
-- GRANT ALL privileges ON ALL TABLES IN SCHEMA health_insurance to hospitalos;
-- GRANT ALL ON all sequences in schema public to hospitalos; 
-- GRANT ALL ON all sequences in schema rp50 to hospitalos; 
-- GRANT ALL ON all sequences in schema rp50nhso to hospitalos; 
-- GRANT ALL ON all sequences in schema rp50data to hospitalos; 
-- GRANT ALL ON all sequences in schema health_insurance to hospitalos;
-- GRANT ALL ON all sequences in schema thai_ascvd to hospitalos;
-- GRANT USAGE ON SCHEMA public TO hospitalos;
-- GRANT USAGE ON SCHEMA rp50 TO hospitalos;
-- GRANT USAGE ON SCHEMA rp50nhso TO hospitalos;
-- GRANT USAGE ON SCHEMA rp50data TO hospitalos;
-- GRANT USAGE ON SCHEMA health_insurance TO hospitalos;
-- GRANT USAGE ON SCHEMA thai_ascvd TO hospitalos;
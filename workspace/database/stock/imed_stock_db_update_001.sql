CREATE TABLE b_option_stock (
    b_option_stock_id                varchar(255) NOT NULL,
    option_stock_value            	varchar(255) NOT NULL default '0',
    option_stock_description       	varchar(255) NULL
    );

ALTER TABLE b_option_stock
	ADD CONSTRAINT b_option_stock_pkey
	PRIMARY KEY (b_option_stock_id);

INSERT INTO b_option_stock VALUES ('ENABLE_ORDER_EMPTY', '0', 'สามารถสั่งรายการยาได้ แม้มีจำนวนเป็น 0');


INSERT INTO s_stock_version VALUES ('st00100000002', '02', 'Stock Module', '1.4.20120914', '1.1.20120914', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('Stock Module','imed_stock_db_update_001.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'Update iMed Stock');

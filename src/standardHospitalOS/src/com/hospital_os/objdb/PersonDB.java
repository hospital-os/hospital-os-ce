/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.Person;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class PersonDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "800";
    private final SequenceDataDB sequenceDataDB;
    private final PrefixDB prefixDB;

    public PersonDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
        sequenceDataDB = new SequenceDataDB(connectionInf);
        prefixDB = new PrefixDB(connectionInf);
    }

    public int insert(Person obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            obj.person_hcis = sequenceDataDB.updateSequence("hn_hcis", true);
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO t_person(\n");
            sql.append("            t_person_id, person_hcis, t_health_home_id, person_family_number, \n");
            sql.append("            f_prefix_id, person_firstname, person_lastname, \n");            
            sql.append("            person_prefix_eng, person_firstname_eng, person_lastname_eng, person_pid, person_passport, \n");
            sql.append("            person_birthday, person_birthday_true, f_sex_id, f_patient_blood_group_id, \n");
            sql.append("            f_rh_group_id, f_patient_nation_id, f_patient_race_id, f_patient_religion_id, \n");
            sql.append("            father_person_id, mother_person_id, couple_person_id, f_patient_marriage_status_id, \n");
            sql.append("            f_patient_education_type_id, f_patient_occupation_id, f_patient_family_status_id, \n");
            sql.append("            f_person_village_status_id, f_patient_area_status_id, f_person_foreigner_id, \n");
            sql.append("            person_foreigner_card_no, person_revenu, person_move_in_date_time, \n");
            sql.append("            active, user_record_id, record_date_time, user_modify_id, modify_date_time, f_person_affiliated_id, f_person_rank_id, f_person_jobtype_id)\n");
            sql.append("    VALUES (?, ?, ?, ?, \n");
            sql.append("            ?, ?, ?, \n");
            sql.append("            ?, ?, ?, ?, ?, \n");
            sql.append("            ?, ?, ?, ?, \n");
            sql.append("            ?, ?, ?, ?, \n");
            sql.append("            ?, ?, ?, ?, \n");
            sql.append("            ?, ?, ?, \n");
            sql.append("            ?, ?, ?, \n");
            sql.append("            ?, ?, ?, \n");
            sql.append("            ?, ?, ?, ?, ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.getGenID(tableId));
            preparedStatement.setString(index++, obj.person_hcis);
            preparedStatement.setString(index++, obj.t_health_home_id);
            preparedStatement.setString(index++, obj.person_family_number);
            preparedStatement.setString(index++, obj.f_prefix_id);
            preparedStatement.setString(index++, obj.person_firstname.trim());
            preparedStatement.setString(index++, obj.person_lastname.trim());
            preparedStatement.setString(index++, obj.person_prefix_eng.trim());
            preparedStatement.setString(index++, obj.person_firstname_eng.trim());
            preparedStatement.setString(index++, obj.person_lastname_eng.trim());
            preparedStatement.setString(index++, obj.person_pid);
            preparedStatement.setString(index++, obj.person_passport);
            preparedStatement.setString(index++, obj.person_birthday);
            preparedStatement.setString(index++, obj.person_birthday_true);
            preparedStatement.setString(index++, obj.f_sex_id);
            preparedStatement.setString(index++, obj.f_patient_blood_group_id);
            preparedStatement.setString(index++, obj.f_rh_group_id);
            preparedStatement.setString(index++, obj.f_patient_nation_id);
            preparedStatement.setString(index++, obj.f_patient_race_id);
            preparedStatement.setString(index++, obj.f_patient_religion_id);
            preparedStatement.setString(index++, obj.father_person_id);
            preparedStatement.setString(index++, obj.mother_person_id);
            preparedStatement.setString(index++, obj.couple_person_id);
            preparedStatement.setString(index++, obj.f_patient_marriage_status_id);
            preparedStatement.setString(index++, obj.f_patient_education_type_id);
            preparedStatement.setString(index++, obj.f_patient_occupation_id);
            preparedStatement.setString(index++, obj.f_patient_family_status_id);
            preparedStatement.setString(index++, obj.f_person_village_status_id);
            preparedStatement.setString(index++, obj.f_patient_area_status_id);
            preparedStatement.setString(index++, obj.f_person_foreigner_id);
            preparedStatement.setString(index++, obj.person_foreigner_card_no);
            preparedStatement.setString(index++, obj.person_revenu);
            preparedStatement.setString(index++, obj.person_move_in_date_time);
            preparedStatement.setString(index++, obj.active);
            preparedStatement.setString(index++, obj.user_record_id);
            preparedStatement.setString(index++, obj.record_date_time);
            preparedStatement.setString(index++, obj.user_modify_id);
            preparedStatement.setString(index++, obj.modify_date_time);
            preparedStatement.setString(index++, obj.f_person_affiliated_id);
            preparedStatement.setString(index++, obj.f_person_rank_id);
            preparedStatement.setString(index++, obj.f_person_jobtype_id);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(Person obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_person\n");
            sql.append("   SET t_health_home_id=?, person_family_number=?, \n");
            sql.append("       f_prefix_id=?, person_firstname=?, person_lastname=?, \n");
            sql.append("       person_prefix_eng=?, person_firstname_eng=?, person_lastname_eng=?, person_pid=?, \n");
            sql.append("       person_passport=?, person_birthday=?, person_birthday_true=?, \n");
            sql.append("       f_sex_id=?, f_patient_blood_group_id=?, f_rh_group_id=?, f_patient_nation_id=?, \n");
            sql.append("       f_patient_race_id=?, f_patient_religion_id=?, father_person_id=?, \n");
            sql.append("       mother_person_id=?, couple_person_id=?, f_patient_marriage_status_id=?, \n");
            sql.append("       f_patient_education_type_id=?, f_patient_occupation_id=?, f_patient_family_status_id=?, \n");
            sql.append("       f_person_village_status_id=?, f_patient_area_status_id=?, f_person_foreigner_id=?, \n");
            sql.append("       person_foreigner_card_no=?, person_revenu=?, person_move_in_date_time=?, \n");
            sql.append("       active=?, user_modify_id=?, \n");
            sql.append("       modify_date_time=?, f_person_affiliated_id=?, f_person_rank_id=?, f_person_jobtype_id = ?\n");
            sql.append(" WHERE t_person_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.t_health_home_id);
            preparedStatement.setString(index++, obj.person_family_number);
            preparedStatement.setString(index++, obj.f_prefix_id);
            preparedStatement.setString(index++, obj.person_firstname.trim());
            preparedStatement.setString(index++, obj.person_lastname.trim());
            preparedStatement.setString(index++, obj.person_prefix_eng.trim());
            preparedStatement.setString(index++, obj.person_firstname_eng.trim());
            preparedStatement.setString(index++, obj.person_lastname_eng.trim());
            preparedStatement.setString(index++, obj.person_pid);
            preparedStatement.setString(index++, obj.person_passport);
            preparedStatement.setString(index++, obj.person_birthday);
            preparedStatement.setString(index++, obj.person_birthday_true);
            preparedStatement.setString(index++, obj.f_sex_id);
            preparedStatement.setString(index++, obj.f_patient_blood_group_id);
            preparedStatement.setString(index++, obj.f_rh_group_id);
            preparedStatement.setString(index++, obj.f_patient_nation_id);
            preparedStatement.setString(index++, obj.f_patient_race_id);
            preparedStatement.setString(index++, obj.f_patient_religion_id);
            preparedStatement.setString(index++, obj.father_person_id);
            preparedStatement.setString(index++, obj.mother_person_id);
            preparedStatement.setString(index++, obj.couple_person_id);
            preparedStatement.setString(index++, obj.f_patient_marriage_status_id);
            preparedStatement.setString(index++, obj.f_patient_education_type_id);
            preparedStatement.setString(index++, obj.f_patient_occupation_id);
            preparedStatement.setString(index++, obj.f_patient_family_status_id);
            preparedStatement.setString(index++, obj.f_person_village_status_id);
            preparedStatement.setString(index++, obj.f_patient_area_status_id);
            preparedStatement.setString(index++, obj.f_person_foreigner_id);
            preparedStatement.setString(index++, obj.person_foreigner_card_no);
            preparedStatement.setString(index++, obj.person_revenu);
            preparedStatement.setString(index++, obj.person_move_in_date_time);
            preparedStatement.setString(index++, obj.active);
            preparedStatement.setString(index++, obj.user_modify_id);
            preparedStatement.setString(index++, obj.modify_date_time);
            preparedStatement.setString(index++, obj.f_person_affiliated_id);
            preparedStatement.setString(index++, obj.f_person_rank_id);
            preparedStatement.setString(index++, obj.f_person_jobtype_id);
            preparedStatement.setString(index++, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int inactive(Person obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_person\n");
            sql.append("   SET active=?, user_cancel_id=?, cancel_date_time=?\n");
            sql.append(" WHERE t_person_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, "0");
            preparedStatement.setString(2, obj.user_cancel_id);
            preparedStatement.setString(3, obj.cancel_date_time);
            preparedStatement.setString(4, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "delete from t_person where t_person_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            // execute delete SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public Person select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from t_person where t_person_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<Person> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public Person selectByPID(String pid) throws Exception {
        return selectByPID(pid, true);
    }

    public Person selectByPID(String pid, boolean isActive) throws Exception {
        List<Person> list = selectByPid(pid, isActive ? "1" : "0");
        return list.isEmpty() ? null : list.get(0);
    }

    public List<Person> selectByPid(String pid, String active) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from t_person where person_pid = ? and active = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, pid);
            preparedStatement.setString(2, active);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<Person> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<Person> list = new ArrayList<Person>();
        ResultSet rs = null;
        try {
            // execute insert SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Person obj = new Person();
                obj.setObjectId(rs.getString("t_person_id"));
                obj.person_hcis = rs.getString("person_hcis");
                obj.t_health_home_id = rs.getString("t_health_home_id");
                obj.person_family_number = rs.getString("person_family_number");
                obj.f_prefix_id = rs.getString("f_prefix_id");
                obj.person_firstname = rs.getString("person_firstname");
                obj.person_lastname = rs.getString("person_lastname");
                obj.person_prefix_eng = rs.getString("person_prefix_eng");
                obj.person_firstname_eng = rs.getString("person_firstname_eng");
                obj.person_lastname_eng = rs.getString("person_lastname_eng");
                obj.person_pid = rs.getString("person_pid");
                obj.person_passport = rs.getString("person_passport");
                obj.person_birthday = rs.getString("person_birthday");
                obj.person_birthday_true = rs.getString("person_birthday_true");
                obj.f_sex_id = rs.getString("f_sex_id");
                obj.f_patient_blood_group_id = rs.getString("f_patient_blood_group_id");
                obj.f_rh_group_id = rs.getString("f_rh_group_id");
                obj.f_patient_nation_id = rs.getString("f_patient_nation_id");
                obj.f_patient_race_id = rs.getString("f_patient_race_id");
                obj.f_patient_religion_id = rs.getString("f_patient_religion_id");
                obj.father_person_id = rs.getString("father_person_id");
                obj.mother_person_id = rs.getString("mother_person_id");
                obj.couple_person_id = rs.getString("couple_person_id");
                obj.f_patient_marriage_status_id = rs.getString("f_patient_marriage_status_id");
                obj.f_patient_education_type_id = rs.getString("f_patient_education_type_id");
                obj.f_patient_occupation_id = rs.getString("f_patient_occupation_id");
                obj.f_patient_family_status_id = rs.getString("f_patient_family_status_id");
                obj.f_person_village_status_id = rs.getString("f_person_village_status_id");
                obj.f_patient_area_status_id = rs.getString("f_patient_area_status_id");
                obj.f_person_foreigner_id = rs.getString("f_person_foreigner_id");
                obj.person_foreigner_card_no = rs.getString("person_foreigner_card_no");
                obj.person_revenu = rs.getString("person_revenu");
                obj.person_move_in_date_time = rs.getString("person_move_in_date_time");
                obj.active = rs.getString("active");
                obj.user_record_id = rs.getString("user_record_id");
                obj.record_date_time = rs.getString("record_date_time");
                obj.user_modify_id = rs.getString("user_modify_id");
                obj.modify_date_time = rs.getString("modify_date_time");
                obj.user_cancel_id = rs.getString("user_cancel_id");
                obj.cancel_date_time = rs.getString("cancel_date_time");
                obj.f_person_affiliated_id = rs.getString("f_person_affiliated_id");
                obj.f_person_rank_id = rs.getString("f_person_rank_id");
                obj.f_person_jobtype_id = rs.getString("f_person_jobtype_id");
                if (obj.f_prefix_id != null && !obj.f_prefix_id.isEmpty()) {
                    obj.prefix = prefixDB.selectByPK(obj.f_prefix_id);
                }
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<Person> listByKeyword(String keyword) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from t_person where (upper(person_firstname) like upper(?) "
                    + "or upper(person_lastname) like upper(?) "
                    + "or upper(person_pid) like upper(?) ) and active = '1'";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, "%" + keyword + "%");
            preparedStatement.setString(2, "%" + keyword + "%");
            preparedStatement.setString(3, "%" + keyword + "%");
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
}

package com.hospital_os.object.specialQuery;

/**
 *
 * @author Amp
 */
@SuppressWarnings("ClassWithoutLogger")
public class SpecialQueryResultLab {

    /**
     * Creates a new instance of SpecialQueryResultLab
     */
    public String item_id;
    public String verify_date_time;
    public String result_lab_id;
    public String time;
    public String visit_id;

    public SpecialQueryResultLab() {
    }
}

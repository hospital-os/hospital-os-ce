package com.hospital_os.objdb;

import com.hospital_os.object.DxTemplate;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.Vector;

@SuppressWarnings("UseOfObsoleteCollectionType")
public class DxTemplateDB {

    public ConnectionInf theConnectionInf;
    final public String idtable = "154";

    public DxTemplateDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public Vector selectByVid(String vid) throws Exception {
        String sql = "select * from t_visit_diag_map,b_template_dx  where t_visit_diag_map.b_template_dx_id= b_template_dx.b_template_dx_id and t_visit_diag_map.t_visit_id = ? and t_visit_diag_map.visit_diag_map_active = '1' limit 10";
        try (PreparedStatement ps = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ps.setString(index++, vid);
            return this.eQuery(ps.toString());
        }
    }

    public DxTemplate readByDes(String des) throws Exception {
        String sql = "select * from b_template_dx where template_dx_description ilike ? limit 1";
        try (PreparedStatement ps = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ps.setString(index++, "%" + des + "%");
            Vector<DxTemplate> eQuery = this.eQuery(ps.toString());
            return eQuery.isEmpty() ? null : eQuery.get(0);
        }
    }

    /**
     * @param cmd
     * @param o
     * @return int
     * @throws java.lang.Exception
     * @roseuid 3F6574DE0394
     */
    public int insert(DxTemplate o) throws Exception {
        String sql = "insert into b_template_dx (b_template_dx_id ,template_dx_description ,template_dx_icd_type ,b_icd10_number ,template_dx_guideafterdx ,template_dx_thaidescription ,b_visit_clinic_id ) values (?, ?, ?, ?, ?, ?, ?)";
        try (PreparedStatement ps = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ps.setString(index++, o.getGenID(idtable));
            ps.setString(index++, o.description);
            ps.setString(index++, o.icd_type);
            ps.setString(index++, o.icd_code.toUpperCase());
            ps.setString(index++, o.guide_after_dx);
            ps.setString(index++, o.thaidescription);
            ps.setString(index++, o.clinic_code);
            return ps.executeUpdate();
        }
    }

    public int update(DxTemplate o) throws Exception {
        String sql = "update b_template_dx set template_dx_description=?, template_dx_icd_type=?, b_icd10_number=?, template_dx_guideafterdx=?, template_dx_thaidescription=?, b_visit_clinic_id=? where b_template_dx_id=?";
        try (PreparedStatement ps = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ps.setString(index++, o.description);
            ps.setString(index++, o.icd_type);
            ps.setString(index++, o.icd_code.toUpperCase());
            ps.setString(index++, o.guide_after_dx);
            ps.setString(index++, o.thaidescription);
            ps.setString(index++, o.clinic_code);
            ps.setString(index++, o.getObjectId());
            return ps.executeUpdate();
        }
    }

    public int delete(DxTemplate o) throws Exception {
        String sql = "delete from b_template_dx where b_template_dx_id=?";
        try (PreparedStatement ps = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ps.setString(index++, o.getObjectId());
            return ps.executeUpdate();
        }
    }

    public DxTemplate selectByPK(String pk) throws Exception {
        String sql = "select * from b_template_dx where b_template_dx_id = ?";
        try (PreparedStatement ps = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ps.setString(index++, pk);
            Vector<DxTemplate> eQuery = this.eQuery(ps.toString());
            return eQuery.isEmpty() ? null : eQuery.get(0);
        }
    }

    public Vector selectAllByName(String keyword) throws Exception {
        String sql = "select * from b_template_dx\n";
        if (keyword != null && !keyword.isEmpty()) {
            sql += "where (b_template_dx_id ilike ? or template_dx_description ilike ?)\n";
        }
        sql += "order by  template_dx_description";
        try (PreparedStatement ps = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            if (keyword != null && !keyword.isEmpty()) {
                ps.setString(index++, keyword + "%");
                ps.setString(index++, keyword + "%");
            }
            return this.eQuery(ps.toString());
        }
    }

    public DxTemplate selectByName(String keyword) throws Exception {
        String sql = "select * from b_template_dx where template_dx_description = ?";
        try (PreparedStatement ps = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ps.setString(index++, keyword);
            Vector<DxTemplate> eQuery = this.eQuery(ps.toString());
            return eQuery.isEmpty() ? null : eQuery.get(0);
        }
    }

    /**
     * @author henbe
     */
    public Vector eQuery(String sql) throws Exception {
        DxTemplate p;
        Vector list = new Vector();
        try (ResultSet rs = theConnectionInf.eQuery(sql.toString())) {
            while (rs.next()) {
                p = new DxTemplate();
                p.setObjectId(rs.getString("b_template_dx_id"));
                p.description = rs.getString("template_dx_description");
                p.icd_type = rs.getString("template_dx_icd_type");
                p.icd_code = rs.getString("b_icd10_number");
                p.guide_after_dx = rs.getString("template_dx_guideafterdx");
                p.thaidescription = rs.getString("template_dx_thaidescription");
                p.clinic_code = rs.getString("b_visit_clinic_id");
                list.add(p);
            }
        }
        return list;
    }

    public DxTemplate getByUnuseICD10(String icd10, String visitId) throws Exception {
        String sql = "select * \n"
                + "from b_template_dx\n"
                + "where \n"
                + "b_template_dx.b_icd10_number = ?\n"
                + "and b_template_dx.b_template_dx_id not in (\n"
                + "    select t_visit_diag_map.b_template_dx_id from t_visit_diag_map where t_visit_id = ? and visit_diag_map_active = '1'\n"
                + ")";
        try (PreparedStatement ps = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ps.setString(index++, icd10);
            ps.setString(index++, visitId);
            Vector<DxTemplate> eQuery = this.eQuery(ps.toString());
            return eQuery.isEmpty() ? null : eQuery.get(0);
        }
    }

    public DxTemplate selectByItem(String ItemID, String VisitID) throws Exception {
        String sql = "select b_template_dx.*\n"
                + "from b_template_dx_map_item_risk  \n"
                + "inner join b_template_dx on b_template_dx_map_item_risk.b_template_dx_id  = b_template_dx.b_template_dx_id \n"
                + "where b_template_dx_map_item_risk.b_item_id = ? \n"
                + "and b_template_dx.b_template_dx_id in( \n"
                + "    select b_template_dx_id from t_visit_diag_map \n"
                + "    where t_visit_diag_map.t_visit_id = ? and t_visit_diag_map.visit_diag_map_active = '1')";
        try (PreparedStatement ps = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ps.setString(index++, ItemID);
            ps.setString(index++, VisitID);
            List<DxTemplate> list = eQuery(ps.toString());
            return list.isEmpty() ? null : list.get(0);
        }
    }

    public DxTemplate readDxTemplateByVisitID(String visit_id, String des) throws Exception {
        String sql = "select b_template_dx.* \n"
                + " from b_template_dx \n"
                + " where b_template_dx.b_template_dx_id in( \n"
                + "	select b_template_dx_id \n"
                + "	from t_visit_diag_map \n"
                + "	where t_visit_id = ? \n and visit_diag_map_dx ilike ?)";
        try (PreparedStatement ps = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ps.setString(index++, visit_id);
            ps.setString(index++, "%" + des + "%");
            List<DxTemplate> list = eQuery(ps.toString());
            return list.isEmpty() ? null : list.get(0);
        }
    }
}

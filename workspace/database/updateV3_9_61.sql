CREATE TABLE IF NOT EXISTS b_map_contract_plans_claim_price(
    b_map_contract_plans_claim_price_id   character varying(30)   NOT NULL,
    b_contract_plans_id                   character varying(30)   NOT NULL,
CONSTRAINT b_map_contract_plans_claim_price_pkey PRIMARY KEY (b_map_contract_plans_claim_price_id)
);


INSERT INTO f_refer_cause (f_refer_cause_id, refer_cause_description) VALUES  (10,'เพื่อหัตถการ หรือการตรวจวินิจฉัยทางห้องปฏิบัติการเฉพาะเรื่อง');

insert into f_gui_action (f_gui_action_id, gui_action_name) VALUES ('5707', 'ประเภทอัตราราคา');

--------------- เพิ่มตารางประเภทอัตราราคา
CREATE TABLE IF NOT EXISTS b_tariff (
  b_tariff_id         character varying(30) NOT NULL,
  code                text NOT NULL,
  description         text NOT NULL,
  active              character varying(1) NOT NULL DEFAULT '1'::character varying,
  record_date_time    TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
  user_record_id      CHARACTER VARYING(30) NOT NULL DEFAULT '1578299109445',
  update_date_time    TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
  user_update_id      CHARACTER VARYING(30) NOT NULL DEFAULT '1578299109445',
  CONSTRAINT b_tariff_pkey PRIMARY KEY (b_tariff_id)
);

BEGIN;
INSERT INTO b_tariff (b_tariff_id,code,description) SELECT '5707000000001','01','ราคาทั่วไป'
WHERE NOT EXISTS (SELECT 1 FROM b_tariff WHERE b_tariff_id = '5707000000001'); 
COMMIT;

--------------- เพิ่มข้อมูลอัตราราคา ในหน้าจอตั้งค่าสิทธิการรักษา
ALTER TABLE b_contract_plans ADD COLUMN IF NOT EXISTS b_tariff_id VARCHAR(255) NOT NULL DEFAULT '5707000000001';

--------------- เพิ่มข้อมูลอัตราราคา ในหน้าจอตั้งค่าราคารายการตรวจรักษา
ALTER TABLE b_item_price ADD COLUMN IF NOT EXISTS use_price_claim boolean NOT NULL DEFAULT false;
ALTER TABLE b_item_price ADD COLUMN IF NOT EXISTS item_price_claim FLOAT NOT NULL DEFAULT 0;
ALTER TABLE b_item_price ADD COLUMN IF NOT EXISTS b_tariff_id VARCHAR(255) NOT NULL DEFAULT '5707000000001';

--------------- เพิ่มข้อมูลอัตราราคา หน้าต่างสิทธิการรักษา แถบการรับบริการ
ALTER TABLE t_visit_payment ADD COLUMN IF NOT EXISTS b_tariff_id VARCHAR(255);
UPDATE t_visit_payment SET b_tariff_id = '5707000000001';

--------------- เพิ่มราคาเบิกได้ ในหน้าจอรายการตรวจรักษา
ALTER TABLE t_order ADD COLUMN IF NOT EXISTS use_price_claim boolean NOT NULL DEFAULT false;
ALTER TABLE t_order ADD COLUMN IF NOT EXISTS order_price_claim FLOAT NOT NULL DEFAULT 0;

-- update db version
INSERT INTO s_version VALUES ('9701000000095', '95', 'Hospital OS, Community Edition', '3.9.61', '3.41.0', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_61.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.61');
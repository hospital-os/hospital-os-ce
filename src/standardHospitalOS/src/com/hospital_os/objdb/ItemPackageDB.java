/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.ItemPackage;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tanakrit
 */
public class ItemPackageDB {

    public ConnectionInf theConnectionInf;
    final public String idtable = "990";

    public ItemPackageDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public int insert(ItemPackage p) throws Exception {
        p.generateOID(idtable);
        String sql = "INSERT INTO b_item_package(\n"
                + "               b_item_package_id, b_item_id, b_item_sub_id, item_seq, item_qty_purch)\n"
                + "        VALUES(?, ?, ?, ?, ?)";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, p.getObjectId());
            ePQuery.setString(index++, p.b_item_id);
            ePQuery.setString(index++, p.b_item_sub_id);
            ePQuery.setInt(index++, p.item_seq);
            ePQuery.setDouble(index++, p.item_qty_purch);
            // execute insert SQL stetement
            return ePQuery.executeUpdate();
        }
    }

    public int update(ItemPackage p) throws Exception {
        String sql = "UPDATE b_item_package\n"
                + "      SET b_item_id=?, b_item_sub_id=?, item_seq=?, item_qty_purch=?  \n"
                + "    WHERE b_item_package_id=? ";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, p.b_item_id);
            ePQuery.setString(index++, p.b_item_sub_id);
            ePQuery.setInt(index++, p.item_seq);
            ePQuery.setDouble(index++, p.item_qty_purch);
            ePQuery.setString(index++, p.getObjectId());
            // execute insert SQL stetement
            return ePQuery.executeUpdate();
        }
    }

    public int delete(ItemPackage o) throws Exception {
        String sql = "DELETE FROM b_item_package\n"
                + "WHERE b_item_package_id ='" + o.getObjectId() + "'";
        return theConnectionInf.eUpdate(sql);
    }

    public int delete(String itemId) throws Exception {
        String sql = "DELETE FROM b_item_package WHERE b_item_id='" + itemId + "'";
        return theConnectionInf.eUpdate(sql);
    }

    public List<ItemPackage> selectByItem(String itemID, String active) throws Exception {
        String sql = "select b_item.b_item_id\n"
                + "    ,b_item.item_number\n"
                + "    ,b_item.item_common_name\n"
                + "    ,b_item_sub.b_item_id as b_item_sub_id\n"
                + "    ,b_item_sub.item_common_name as item_sub_common_name\n"
                + "    ,b_item_package.item_seq\n"
                + "    ,b_item_package.item_qty_purch\n"
                + "    ,b_item_subgroup.f_item_group_id\n"
                + "    ,b_item_package.b_item_package_id\n"
                + "from b_item_package\n"
                + "    left join b_item on b_item_package.b_item_id = b_item.b_item_id\n"
                + "    left join b_item as b_item_sub on b_item_package.b_item_sub_id = b_item_sub.b_item_id\n"
                + "    left join b_item_subgroup on b_item_sub.b_item_subgroup_id = b_item_subgroup.b_item_subgroup_id\n"
                + "where b_item.b_item_id = '" + itemID + "'\n"
                + "    and b_item.item_active = '" + active + "'\n"
                + "    and b_item_sub.item_active = '1'\n"
                + "order by b_item_package.item_seq\n"
                + "    ,b_item_sub.item_common_name  ";
        return eQuery(sql);
    }

    public List<ItemPackage> eQuery(String sql) throws Exception {
        ItemPackage p;
        List<ItemPackage> list = new ArrayList<>();
        try (ResultSet rs = theConnectionInf.eQuery(sql)) {
            while (rs.next()) {
                p = new ItemPackage();
                p.setObjectId(rs.getString("b_item_package_id"));
                p.b_item_id = rs.getString("b_item_id");
                p.b_item_sub_id = rs.getString("b_item_sub_id");
                p.item_seq = rs.getInt("item_seq");
                p.item_qty_purch = rs.getDouble("item_qty_purch");
                try {
                    p.item_name = rs.getString("item_sub_common_name");
                } catch (Exception e) {
                }
                try {
                    p.f_item_group_id = rs.getString("f_item_group_id");
                } catch (Exception e) {
                }
                list.add(p);
            }
        }
        return list;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.subject;

import com.hospital_os.usecase.connection.UpdateStatus;
import com.hosv3.usecase.transaction.ManageRefer;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Khanate
 */
public class ReferSubject implements ManageRefer {

    private List<ManageRefer> list = new ArrayList<ManageRefer>();

    public void removeAttach() {
        list.clear();

    }

    public void attachManage(ManageRefer o) {
        list.add(o);
    }

    @Override
    public void notifyInactiveRefer(String referId, UpdateStatus us) throws Exception {
        for (ManageRefer manageRefer : list) {
            manageRefer.notifyInactiveRefer(referId, us);
        }
    }
}

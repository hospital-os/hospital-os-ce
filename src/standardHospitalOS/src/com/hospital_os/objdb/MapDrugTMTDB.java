/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.MapDrugTMT;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class MapDrugTMTDB {

    public ConnectionInf connectionInf;
    final public String tableId = "859";

    public MapDrugTMTDB(ConnectionInf db) {
        connectionInf = db;
    }

    public int insert(MapDrugTMT obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO b_map_drug_tmt(\n"
                    + "            b_map_drug_tmt_id, b_item_id, b_drug_tmt_tpucode)\n"
                    + "    VALUES (?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.getGenID(tableId));
            preparedStatement.setString(index++, obj.b_item_id);
            preparedStatement.setString(index++, obj.b_drug_tmt_tpucode);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
    
    public int updateChangeTPUCode(String id, String replaceId) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("Update b_map_drug_tmt set b_drug_tmt_tpucode = ?, record_datetime = current_timestamp\n");
            sql.append("WHERE b_drug_tmt_tpucode = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, replaceId);
            preparedStatement.setString(2, id);
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM b_map_drug_tmt\n");
            sql.append(" WHERE b_map_drug_tmt_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, id);
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(String... ids) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "DELETE FROM b_map_drug_tmt WHERE b_map_drug_tmt_id in (%s)";
            StringBuilder sb = new StringBuilder();
            for (String id : ids) {
                sb.append("'").append(id).append("',");
            }
            preparedStatement = connectionInf.ePQuery(String.format(sql,
                    sb.toString().substring(0, sb.toString().length() - 1)));
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<MapDrugTMT> list(String keyword, String grpId, String type) throws Exception {
        String sql = "select b_item.item_common_name\n"
                + ", b_drug_tmt.fsn\n"
                + ", b_item.b_item_id\n"
                + ", b_map_drug_tmt.b_drug_tmt_tpucode\n"
                + ", b_map_drug_tmt.b_map_drug_tmt_id\n"
                + "from\n"
                + "b_item\n"
                + "inner join b_item_subgroup on b_item.b_item_subgroup_id = b_item_subgroup.b_item_subgroup_id and b_item_subgroup.f_item_group_id in ('1','4')\n"
                + "left join b_map_drug_tmt on b_item.b_item_id = b_map_drug_tmt.b_item_id\n"
                + "left join b_drug_tmt on b_drug_tmt.tpucode = b_map_drug_tmt.b_drug_tmt_tpucode\n"
                + "\n"
                + "where b_item.item_active = '1'\n";
        if (keyword != null && !keyword.isEmpty()) {
            sql += "and (b_item.item_common_name ilike ? or b_item.item_trade_name ilike ? or b_item.item_nick_name ilike ?)\n";
        }
        if (grpId != null && !grpId.isEmpty()) {
            sql += "and b_item_subgroup.b_item_subgroup_id = ?\n";
        }
        if (type != null && !type.isEmpty()) {
            if (type.equals("1")) {
                sql += "and b_map_drug_tmt.b_map_drug_tmt_id is not null\n";
            } else if (type.equals("2")) {
                sql += "and b_map_drug_tmt.b_map_drug_tmt_id is null\n";
            }
        }
        sql += "order by b_item.item_common_name";
        PreparedStatement preparedStatement = connectionInf.ePQuery(sql.toString());
        int index = 1;
        if (keyword != null && !keyword.isEmpty()) {
            preparedStatement.setString(index++, "%" + keyword + "%");
            preparedStatement.setString(index++, "%" + keyword + "%");
            preparedStatement.setString(index++, "%" + keyword + "%");
        }
        if (grpId != null && !grpId.isEmpty()) {
            preparedStatement.setString(index++, grpId);
        }
        return executeQuery(preparedStatement);
    }

    public MapDrugTMT selectByItemId(String itemId) throws Exception {
        String sql = "select *\n"
                + "from b_map_drug_tmt\n"
                + "where b_map_drug_tmt.b_item_id = ?";
        PreparedStatement preparedStatement = connectionInf.ePQuery(sql.toString());
        int index = 1;
        preparedStatement.setString(index++, itemId);
        List<MapDrugTMT> executeQuery = executeQuery(preparedStatement);
        return executeQuery.isEmpty() ? null : executeQuery.get(0);
    }

    public List<MapDrugTMT> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<MapDrugTMT> list = new ArrayList<MapDrugTMT>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                MapDrugTMT obj = new MapDrugTMT();
                obj.setObjectId(rs.getString("b_map_drug_tmt_id"));
                obj.b_drug_tmt_tpucode = rs.getString("b_drug_tmt_tpucode");
                obj.b_item_id = rs.getString("b_item_id");
                try {
                    obj.item_name = rs.getString("item_common_name");
                } catch (Exception ex) {
                }
                try {
                    obj.fsn = rs.getString("fsn");
                } catch (Exception ex) {
                }
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }
}

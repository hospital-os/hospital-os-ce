/*
 * PanelOrder.java
 *
 * Created on 29 �ѹ��¹ 2546, 9:32 �.
 */
package com.hosv3.gui.panel.transaction;

import com.hospital_os.object.Active;
import com.hospital_os.object.Authentication;
import com.hospital_os.object.CategoryGroupItem;
import com.hospital_os.object.DiagDoctorClinic;
import com.hospital_os.object.DxTemplate;
import com.hospital_os.object.Employee;
import com.hospital_os.object.Item;
import com.hospital_os.object.ItemPrice;
import com.hospital_os.object.ItemSubgroupCheckCalculate;
import com.hospital_os.object.OrderItem;
import com.hospital_os.object.OrderItemDrug;
import com.hospital_os.object.OrderItemStatus;
import com.hospital_os.object.OrderLabSecret;
import com.hospital_os.object.OrderStatus;
import com.hospital_os.object.Patient;
import com.hospital_os.object.Payment;
import com.hospital_os.object.QueueDespense;
import com.hospital_os.object.QueueLab;
import com.hospital_os.object.QueueXray;
import com.hospital_os.object.Visit;
import com.hospital_os.object.VisitStatus;
import com.hospital_os.object.VisitType;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hospital_os.utility.CelRenderer;
import com.hospital_os.utility.CelRendererContinue;
import com.hospital_os.utility.CellRendererCheckBox;
import com.hospital_os.utility.CellRendererDayOrder;
import com.hospital_os.utility.CellRendererHos;
import com.hospital_os.utility.CellRendererItem;
import com.hospital_os.utility.ComboFix;
import com.hospital_os.utility.ComboboxModel;
import com.hospital_os.utility.Constant;
import com.hospital_os.utility.Gutil;
import com.hospital_os.utility.TaBleModel;
import com.hosv3.control.HosControl;
import com.hosv3.control.PrintControl;
import com.hosv3.gui.component.HCheckBoxEditor;
import com.hosv3.gui.dialog.DialogChooseOrderItemPrint;
import com.hosv3.gui.dialog.HosDialog;
import com.hosv3.gui.panel.transaction.inf.InfPanelOrder;
import com.hosv3.gui.panel.transaction.inf.PanelOrderInf;
import com.hosv3.object.GActionAuthV;
import com.hosv3.object.HosObject;
import com.hosv3.object.printobject.PrintSelectDrugList;
import com.hosv3.subject.HosSubject;
import com.hosv3.usecase.transaction.ManageBillingResp;
import com.hosv3.usecase.transaction.ManageLabXrayResp;
import com.hosv3.usecase.transaction.ManageOrderResp;
import com.hosv3.usecase.transaction.ManagePatientResp;
import com.hosv3.usecase.transaction.ManagePrintResp;
import com.hosv3.usecase.transaction.ManageVPaymentResp;
import com.hosv3.usecase.transaction.ManageVisitResp;
import com.hosv3.usecase.transaction.ManageVitalResp;
import com.hosv3.utility.GuiLang;
import com.hosv3.utility.TableRenderer;
import com.hosv3.utility.connection.ExecuteControlInf;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;
import java.util.logging.Logger;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import sd.jtable.celleditor.NumberCellEditor;

/**
 *
 * @author Surachai Thowong
 * @modify amp
 * @modify pu
 *
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class PanelOrder extends javax.swing.JPanel implements ManageVisitResp,
        ManagePatientResp,
        ManageOrderResp,
        ManageBillingResp,
        ManagePrintResp,
        ManageVitalResp,
        ManageLabXrayResp,
        ExecuteControlInf,
        PanelOrderInf,
        ManageVPaymentResp {

    protected HosObject theHO;
    protected HosControl theHC;
    protected HosSubject theHS;
    protected UpdateStatus theUS;
    protected HosDialog theHD;
    public final static String CARD_BLANK = "CARD_BLANK";
    public final static String CARD_DRUG = "CARD_DRUG";
    static final long serialVersionUID = 0;
    public final static String CARD_SERVICE = "CARD_SERVICE";
    public final static String CARD_SUPPLY = "CARD_SUPPLY";
    public final static String CARD_LAB = "CARD_LAB";
    public boolean show_cancel_order = true;
    public static String[] col_jTableItemList = {"NAME",
        "���ͷҧ��ä��"};
    protected CelRenderer cellRenderer = new CelRenderer();
    protected CelRendererContinue cellRendererContinue = new CelRendererContinue();
    protected CellRendererCheckBox cellRendererCheckBox = new CellRendererCheckBox();
    protected CellRendererDayOrder cellRendererDayOrder;
    protected CellRendererItem cellRendererItem;
    protected Patient thePatient = new Patient();
    /**
     * ��㹡�õ�Ǩ�ͺ��� Double-Click
     */
    protected long firstClickTime = 0;
    /*
     * �Ըա������
     */
    protected Vector theOrderItemV;
    protected Vector theItemV;
    protected QueueXray theQueueXray;
    protected QueueLab theQueueLab;
    protected QueueDespense theQueueDespense;
    protected OrderLabSecret theOrderLabSecret;
    protected DialogChooseOrderItemPrint theDialogChooseOrderItemPrint = null;
    protected ComboFix theComboFix = null;
    protected ComboFix theEmployeeComboFix = null;
    protected CardLayout layoutOrder;
    protected CardLayout layoutDrug;
    /*
     * ��㹡�þ����
     */
    private int preview = PrintControl.MODE_PREVIEW; //��㹡�þ�������������͵�ͧ����ʴ������š�͹�����
    private int nopreview = PrintControl.MODE_PRINT; //��㹡�þ������������������ͧ�������ʴ������š�͹�����
    private Visit theVisit;
    private DiagDoctorClinic ddc;
    private boolean isShowDoctor = false;
    private boolean isShowDose = false;
    private List<String> colOrderOPD;
    private List<String> colOrderIPD;
//    private CellEditorBalloonText cellEditorDoseShortcut;

    public PanelOrder() {
        initComponents();
        setLanguage(null);

        this.handleResized();

        this.tableItemList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        dateComboBoxStart.setEnabled(false);
        dateComboBoxEnd.setEnabled(false);
    }

    public void setControl(HosControl hc, UpdateStatus us) {
        theHC = hc;
        theHO = hc.theHO;
        theHS = hc.theHS;
        theUS = us;
        pDOrder1.setControl(hc, us);
        cellRendererDayOrder = new CellRendererDayOrder(true);
        cellRendererItem = new CellRendererItem(true);

        hc.theHS.theVisitSubject.attachManageVisit(this);
        hc.theHS.thePatientSubject.attachManagePatient(this);
        hc.theHS.theOrderSubject.attachManageOrder(this);
        hc.theHS.theBillingSubject.attachManageBilling(this);
        hc.theHS.thePrintSubject.attachManagePrint(this);
        hc.theHS.theResultSubject.attachManageLab(this);
        hc.theHS.theResultSubject.attachManageXray(this);
        hc.theHS.theVitalSubject.attachManageVital(this);
        hc.theHS.theVPaymentSubject.attach(this);
        isShowDoctor = "1".equals(theHC.theLookupControl.readOption(false).enable_order_doctor);
        isShowDose = theHC.theLookupControl.readOption().isShowDose();

        initTable();
        initComboBox();
        getInfPanelOrder().setOrderItem(null);
        getInfPanelOrder().setItem(null);
        setVisit(null);
        initAuthen(theHO.theEmployee);
        initPopup();
    }

    public void initTable() {
        colOrderOPD = new ArrayList() {
            {
                add("�");
                add("��¡��");
                if (isShowDose) {
                    add("Dose ���");
                }
                add("�ӹǹ");
                add("�Ҥ�");
                add("ʶҹ�");
                add("�͵�Ǩ");
                if (isShowDoctor) {
                    add("ᾷ�������");
                }
            }
        };

        colOrderIPD = new ArrayList() {
            {
                add("�");
                add("��¡��");
                if (isShowDose) {
                    add("Dose ���");
                }
                add("�ӹǹ");
                add("�Ҥ�");
                add("ʶҹ�");
                add("�͵�Ǩ");
                add("������ͧ");
                add("��ҹ");
                if (isShowDoctor) {
                    add("ᾷ�������");
                }
            }
        };
//        if (isShowDose) {
//            cellEditorDoseShortcut = new CellEditorBalloonText(theUS.getJFrame(),
//                    new DrugDoseShortcutLookup(theHC.theLookupControl));
//        }
    }

    @Override
    public void initComboBox() {
        ComboboxModel.initComboBox(jComboBoxGroup, theHC.theLookupControl.listCategoryGroupItem());
        Vector vc = theHC.theLookupControl.listCategoryGroup();
        ComboboxModel.initComboBox(jComboBoxGroup1, vc);
        ComboFix alltype = new ComboFix();
        alltype.code = "";
        alltype.name = "�ʴ�������";
        jComboBoxGroup1.insertItemAt((Object) alltype, 0);
        jComboBoxGroup1.setSelectedIndex(0);
        String command = theHC.theLookupControl.readOption().b3_command;
        String ttt = theHC.theLookupControl.readOption().b3_description;
        String icon = theHC.theLookupControl.readOption().b3_icon;
        this.jButtonB3.setVisible(!command.isEmpty());
        this.jButtonB3.setToolTipText(ttt);
        if (!icon.isEmpty()) {
            this.jButtonB3.setIcon(new javax.swing.ImageIcon("resources/image/" + icon));
            this.jButtonB3.setText("");
        }
    }

    public void initAuthen(Employee e) {
        GActionAuthV gaav = theHO.theGActionAuthV;
        jButtonCal.setVisible(gaav.isReadPOrderButtonCalBill());
        jButtonVerify.setVisible(gaav.isReadPOrderButtonVerify());
        jButtonExecute.setVisible(gaav.isReadPOrderButtonExecute());
        jButtonDispense.setVisible(gaav.isReadPOrderButtonDispense());
        jButtonCancel.setVisible(gaav.isReadPOrderButtonCancel());
        jButtonDrugRx.setVisible(gaav.isReadPOrderButtonPDrugRx());
        jButtonSticker.setVisible(gaav.isReadPOrderButtonPSticker());
        btnStickerTube.setVisible(gaav.isReadMenuPrinterSticker());
        // set visible button
        btnViewLISResult.setVisible(theHC.theLookupControl.readOption().enable_lis_result_viewer.equals("1")
                && theHC.theHO.theGActionAuthV.isReadPOrderViewLISResultFile());
    }

    public void setDialog(HosDialog hd) {
        theHD = hd;
    }

    private void initPopup() {
        GActionAuthV gaav = theHO.theGActionAuthV;
        jPopupMenu1 = new javax.swing.JPopupMenu();
        jPopupMenu1.setInvoker(this.tableOrderList);
        javax.swing.JSeparator jSeparator1 = new javax.swing.JSeparator();
        if (gaav.isReadPOrderButtonVerify()) {
            jMenuItemVerify = new javax.swing.JMenuItem();
            jMenuItemVerify.setAccelerator(javax.swing.KeyStroke.getKeyStroke('V'));
            jMenuItemVerify.setText(Constant.getTextBundle("�׹�ѹ"));
            jMenuItemVerify.addActionListener(new java.awt.event.ActionListener() {
                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jMenuItemVerifyActionPerformed(evt);
                }
            });
            jPopupMenu1.add(jMenuItemVerify);
        }
        if (gaav.isReadPOrderButtonCancel()) {
            jMenuItemCancel = new javax.swing.JMenuItem();
            jMenuItemCancel.setAccelerator(javax.swing.KeyStroke.getKeyStroke("DELETE"));
            jMenuItemCancel.setText(Constant.getTextBundle("¡��ԡ"));
            jMenuItemCancel.addActionListener(new java.awt.event.ActionListener() {
                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jMenuItemCancelActionPerformed(evt);
                }
            });
            jPopupMenu1.add(jMenuItemCancel);
        }
        if (gaav.isReadPOrderButtonExecute()) {
            jMenuItemExecute = new javax.swing.JMenuItem();
            jMenuItemExecute.setAccelerator(javax.swing.KeyStroke.getKeyStroke('E'));
            jMenuItemExecute.setText(Constant.getTextBundle("���Թ���"));
            jMenuItemExecute.addActionListener(new java.awt.event.ActionListener() {
                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jMenuItemExecuteActionPerformed(evt);
                }
            });
            jPopupMenu1.add(jMenuItemExecute);
        }
        if (gaav.isReadPOrderButtonDispense()) {
            jMenuItemDispense = new javax.swing.JMenuItem();
            jMenuItemDispense.setAccelerator(javax.swing.KeyStroke.getKeyStroke('D'));
            jMenuItemDispense.setText(Constant.getTextBundle("����"));
            jMenuItemDispense.addActionListener(new java.awt.event.ActionListener() {
                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jMenuItemDispenseActionPerformed(evt);
                }
            });
            jPopupMenu1.add(jMenuItemDispense);
        }
        jPopupMenu1.add(jSeparator1);
    }

    private void jMenuItemVerifyActionPerformed(java.awt.event.ActionEvent evt) {
        jButtonVerifyActionPerformed(null);
    }

    private void jMenuItemExecuteActionPerformed(java.awt.event.ActionEvent evt) {
        jButtonExecuteActionPerformed(null);
    }

    private void jMenuItemDispenseActionPerformed(java.awt.event.ActionEvent evt) {
        jButtonDispenseActionPerformed(null);
    }

    private void jMenuItemCancelActionPerformed(java.awt.event.ActionEvent evt) {
        jButtonCancelActionPerformed(null);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroupaKeySearch = new javax.swing.ButtonGroup();
        jSplitPane2 = new javax.swing.JSplitPane();
        jSplitPane1 = new javax.swing.JSplitPane();
        jPanelSearchItem = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableItemList = new com.hosv3.gui.component.HJTableSort();
        jPanel3 = new javax.swing.JPanel();
        jButtonDrugSet = new javax.swing.JButton();
        jButtonDrugOld = new javax.swing.JButton();
        jRadioButtonBegin = new javax.swing.JRadioButton();
        jRadioButtonConsist = new javax.swing.JRadioButton();
        btnDrugFavorite = new javax.swing.JButton();
        panelSearchOptions = new javax.swing.JPanel();
        panelOptionCategory = new javax.swing.JPanel();
        jComboBoxGroup = new javax.swing.JComboBox();
        jCheckBoxGroup = new javax.swing.JCheckBox();
        jPanel10 = new javax.swing.JPanel();
        jTextFieldKeyword = new javax.swing.JTextField();
        jButtonSearch = new javax.swing.JButton();
        jPanelOrderList = new javax.swing.JPanel();
        jPanelCondition = new javax.swing.JPanel();
        dateComboBoxEnd = new com.hospital_os.utility.DateComboBox();
        dateComboBoxStart = new com.hospital_os.utility.DateComboBox();
        jLabelTo = new javax.swing.JLabel();
        jCheckBoxPeriod = new javax.swing.JCheckBox();
        jLabel4 = new javax.swing.JLabel();
        jComboBoxGroup1 = new javax.swing.JComboBox();
        jButtonRefresh = new javax.swing.JButton();
        jScrollPaneOrderList = new javax.swing.JScrollPane();
        tableOrderList = new com.hosv3.gui.component.HJTableSort();
        jPanelRightSide = new javax.swing.JPanel();
        jButtonB3 = new javax.swing.JButton();
        jCheckBoxContinue = new javax.swing.JToggleButton();
        jCheckBoxShowCancel = new javax.swing.JToggleButton();
        btnStickerTube = new javax.swing.JButton();
        btnViewLISResult = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        btnPrintMarDrug = new javax.swing.JButton();
        btnPrintMarFluid = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        lblLimit = new javax.swing.JLabel();
        lblTotal = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jPanelAction = new javax.swing.JPanel();
        jButtonVerify = new javax.swing.JButton();
        jButtonExecute = new javax.swing.JButton();
        jButtonCancel = new javax.swing.JButton();
        jButtonDispense = new javax.swing.JButton();
        jToggleButtonSelectAll = new javax.swing.JToggleButton();
        jButtonCal = new javax.swing.JButton();
        jButtonDrugRx = new javax.swing.JButton();
        jButtonSticker = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextAreaNote = new javax.swing.JTextArea();
        jButtonBilling = new javax.swing.JButton();
        pDOrder1 = new com.hosv3.gui.panel.detail.PDOrder();

        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                formComponentResized(evt);
            }
            public void componentShown(java.awt.event.ComponentEvent evt) {
                formComponentShown(evt);
            }
        });
        setLayout(new java.awt.GridBagLayout());

        jSplitPane2.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        jSplitPane2.setOneTouchExpandable(true);

        jSplitPane1.setBorder(null);
        jSplitPane1.setOneTouchExpandable(true);

        jPanelSearchItem.setLayout(new java.awt.GridBagLayout());

        tableItemList.setFillsViewportHeight(true);
        tableItemList.setFont(tableItemList.getFont());
        tableItemList.setRowHeight(30);
        tableItemList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tableItemListMouseReleased(evt);
            }
        });
        tableItemList.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tableItemListKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tableItemListKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tableItemList);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelSearchItem.add(jScrollPane1, gridBagConstraints);

        jPanel3.setLayout(new java.awt.GridBagLayout());

        jButtonDrugSet.setFont(jButtonDrugSet.getFont());
        jButtonDrugSet.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/drug_set_24.png"))); // NOI18N
        jButtonDrugSet.setToolTipText("�ش��");
        jButtonDrugSet.setMargin(new java.awt.Insets(2, 4, 2, 4));
        jButtonDrugSet.setMaximumSize(new java.awt.Dimension(32, 32));
        jButtonDrugSet.setMinimumSize(new java.awt.Dimension(32, 32));
        jButtonDrugSet.setPreferredSize(new java.awt.Dimension(32, 32));
        jButtonDrugSet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDrugSetActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jButtonDrugSet, gridBagConstraints);

        jButtonDrugOld.setFont(jButtonDrugOld.getFont());
        jButtonDrugOld.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/drug_history_24.png"))); // NOI18N
        jButtonDrugOld.setToolTipText("�����");
        jButtonDrugOld.setMargin(new java.awt.Insets(2, 4, 2, 4));
        jButtonDrugOld.setMaximumSize(new java.awt.Dimension(32, 32));
        jButtonDrugOld.setMinimumSize(new java.awt.Dimension(32, 32));
        jButtonDrugOld.setPreferredSize(new java.awt.Dimension(32, 32));
        jButtonDrugOld.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDrugOldActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jButtonDrugOld, gridBagConstraints);

        buttonGroupaKeySearch.add(jRadioButtonBegin);
        jRadioButtonBegin.setFont(jRadioButtonBegin.getFont());
        jRadioButtonBegin.setSelected(true);
        jRadioButtonBegin.setText("��˹��");
        jRadioButtonBegin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonBeginActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jRadioButtonBegin, gridBagConstraints);

        buttonGroupaKeySearch.add(jRadioButtonConsist);
        jRadioButtonConsist.setFont(jRadioButtonConsist.getFont());
        jRadioButtonConsist.setText("��Сͺ����");
        jRadioButtonConsist.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonConsistActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jRadioButtonConsist, gridBagConstraints);

        btnDrugFavorite.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/favorite_24.png"))); // NOI18N
        btnDrugFavorite.setToolTipText("��¡���ҷ�������");
        btnDrugFavorite.setMaximumSize(new java.awt.Dimension(32, 32));
        btnDrugFavorite.setMinimumSize(new java.awt.Dimension(32, 32));
        btnDrugFavorite.setPreferredSize(new java.awt.Dimension(32, 32));
        btnDrugFavorite.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDrugFavoriteActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(btnDrugFavorite, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelSearchItem.add(jPanel3, gridBagConstraints);

        panelSearchOptions.setLayout(new javax.swing.BoxLayout(panelSearchOptions, javax.swing.BoxLayout.X_AXIS));

        panelOptionCategory.setLayout(new java.awt.GridBagLayout());

        jComboBoxGroup.setFont(jComboBoxGroup.getFont());
        jComboBoxGroup.setEnabled(false);
        jComboBoxGroup.setMinimumSize(new java.awt.Dimension(26, 24));
        jComboBoxGroup.setPreferredSize(new java.awt.Dimension(26, 24));
        jComboBoxGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxGroupActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelOptionCategory.add(jComboBoxGroup, gridBagConstraints);

        jCheckBoxGroup.setFont(jCheckBoxGroup.getFont());
        jCheckBoxGroup.setText("�����");
        jCheckBoxGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxGroupActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelOptionCategory.add(jCheckBoxGroup, gridBagConstraints);

        panelSearchOptions.add(panelOptionCategory);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanelSearchItem.add(panelSearchOptions, gridBagConstraints);

        jPanel10.setLayout(new java.awt.GridBagLayout());

        jTextFieldKeyword.setFont(jTextFieldKeyword.getFont());
        jTextFieldKeyword.setMaximumSize(new java.awt.Dimension(100, 21));
        jTextFieldKeyword.setMinimumSize(new java.awt.Dimension(100, 21));
        jTextFieldKeyword.setPreferredSize(new java.awt.Dimension(100, 21));
        jTextFieldKeyword.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTextFieldKeywordMouseReleased(evt);
            }
        });
        jTextFieldKeyword.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldKeywordActionPerformed(evt);
            }
        });
        jTextFieldKeyword.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldKeywordKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        jPanel10.add(jTextFieldKeyword, gridBagConstraints);

        jButtonSearch.setFont(jButtonSearch.getFont());
        jButtonSearch.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/find_24.png"))); // NOI18N
        jButtonSearch.setToolTipText("����");
        jButtonSearch.setMargin(new java.awt.Insets(2, 4, 2, 4));
        jButtonSearch.setMaximumSize(new java.awt.Dimension(32, 32));
        jButtonSearch.setMinimumSize(new java.awt.Dimension(32, 32));
        jButtonSearch.setPreferredSize(new java.awt.Dimension(32, 32));
        jButtonSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSearchActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 1, 0, 0);
        jPanel10.add(jButtonSearch, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelSearchItem.add(jPanel10, gridBagConstraints);

        jSplitPane1.setLeftComponent(jPanelSearchItem);

        jPanelOrderList.setMaximumSize(new java.awt.Dimension(500, 2147483647));
        jPanelOrderList.setLayout(new java.awt.GridBagLayout());

        jPanelCondition.setLayout(new java.awt.GridBagLayout());

        dateComboBoxEnd.setEnabled(false);
        dateComboBoxEnd.setFont(dateComboBoxEnd.getFont());
        dateComboBoxEnd.setMinimumSize(new java.awt.Dimension(110, 23));
        dateComboBoxEnd.setPreferredSize(new java.awt.Dimension(110, 23));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 6);
        jPanelCondition.add(dateComboBoxEnd, gridBagConstraints);

        dateComboBoxStart.setEnabled(false);
        dateComboBoxStart.setFont(dateComboBoxStart.getFont());
        dateComboBoxStart.setMinimumSize(new java.awt.Dimension(110, 23));
        dateComboBoxStart.setPreferredSize(new java.awt.Dimension(110, 23));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 6);
        jPanelCondition.add(dateComboBoxStart, gridBagConstraints);

        jLabelTo.setFont(jLabelTo.getFont());
        jLabelTo.setText("-");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 6);
        jPanelCondition.add(jLabelTo, gridBagConstraints);

        jCheckBoxPeriod.setFont(jCheckBoxPeriod.getFont());
        jCheckBoxPeriod.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxPeriodActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 6);
        jPanelCondition.add(jCheckBoxPeriod, gridBagConstraints);

        jLabel4.setFont(jLabel4.getFont());
        jLabel4.setText("�����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 6);
        jPanelCondition.add(jLabel4, gridBagConstraints);

        jComboBoxGroup1.setFont(jComboBoxGroup1.getFont());
        jComboBoxGroup1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxGroup1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 6);
        jPanelCondition.add(jComboBoxGroup1, gridBagConstraints);

        jButtonRefresh.setFont(jButtonRefresh.getFont());
        jButtonRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/refresh_24.png"))); // NOI18N
        jButtonRefresh.setToolTipText("���¡�٢�����");
        jButtonRefresh.setMaximumSize(new java.awt.Dimension(32, 32));
        jButtonRefresh.setMinimumSize(new java.awt.Dimension(32, 32));
        jButtonRefresh.setPreferredSize(new java.awt.Dimension(32, 32));
        jButtonRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonRefreshActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 6);
        jPanelCondition.add(jButtonRefresh, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelOrderList.add(jPanelCondition, gridBagConstraints);

        tableOrderList.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tableOrderList.setFillsViewportHeight(true);
        tableOrderList.setFont(tableOrderList.getFont());
        tableOrderList.setRowHeight(30);
        tableOrderList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tableOrderListMouseReleased(evt);
            }
        });
        tableOrderList.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tableOrderListKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tableOrderListKeyReleased(evt);
            }
        });
        jScrollPaneOrderList.setViewportView(tableOrderList);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelOrderList.add(jScrollPaneOrderList, gridBagConstraints);

        jPanelRightSide.setLayout(new java.awt.GridBagLayout());

        jButtonB3.setFont(jButtonB3.getFont());
        jButtonB3.setMaximumSize(new java.awt.Dimension(32, 32));
        jButtonB3.setMinimumSize(new java.awt.Dimension(32, 32));
        jButtonB3.setPreferredSize(new java.awt.Dimension(32, 32));
        jButtonB3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonB3ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanelRightSide.add(jButtonB3, gridBagConstraints);

        jCheckBoxContinue.setFont(jCheckBoxContinue.getFont());
        jCheckBoxContinue.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/drug_continue_24.png"))); // NOI18N
        jCheckBoxContinue.setToolTipText("����¡���ҵ�����ͧ");
        jCheckBoxContinue.setMaximumSize(new java.awt.Dimension(32, 32));
        jCheckBoxContinue.setMinimumSize(new java.awt.Dimension(32, 32));
        jCheckBoxContinue.setPreferredSize(new java.awt.Dimension(32, 32));
        jCheckBoxContinue.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxContinueActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        jPanelRightSide.add(jCheckBoxContinue, gridBagConstraints);

        jCheckBoxShowCancel.setFont(jCheckBoxShowCancel.getFont());
        jCheckBoxShowCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/cancel_24.png"))); // NOI18N
        jCheckBoxShowCancel.setToolTipText("����¡�÷��¡��ԡ");
        jCheckBoxShowCancel.setMaximumSize(new java.awt.Dimension(32, 32));
        jCheckBoxShowCancel.setMinimumSize(new java.awt.Dimension(32, 32));
        jCheckBoxShowCancel.setPreferredSize(new java.awt.Dimension(32, 32));
        jCheckBoxShowCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxShowCancelActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanelRightSide.add(jCheckBoxShowCancel, gridBagConstraints);

        btnStickerTube.setFont(btnStickerTube.getFont());
        btnStickerTube.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/barcode_24.png"))); // NOI18N
        btnStickerTube.setToolTipText("����� Sticker �Դ Tube");
        btnStickerTube.setMaximumSize(new java.awt.Dimension(32, 32));
        btnStickerTube.setMinimumSize(new java.awt.Dimension(32, 32));
        btnStickerTube.setPreferredSize(new java.awt.Dimension(32, 32));
        btnStickerTube.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStickerTubeActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        jPanelRightSide.add(btnStickerTube, gridBagConstraints);

        btnViewLISResult.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/folderlab@24.png"))); // NOI18N
        btnViewLISResult.setMaximumSize(new java.awt.Dimension(32, 32));
        btnViewLISResult.setMinimumSize(new java.awt.Dimension(32, 32));
        btnViewLISResult.setPreferredSize(new java.awt.Dimension(32, 32));
        btnViewLISResult.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewLISResultActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelRightSide.add(btnViewLISResult, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.weighty = 1.0;
        jPanelRightSide.add(jPanel1, gridBagConstraints);

        btnPrintMarDrug.setFont(btnPrintMarDrug.getFont().deriveFont((float)11));
        btnPrintMarDrug.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/mar_drug.png"))); // NOI18N
        btnPrintMarDrug.setToolTipText("������ Mar ����Ѻ��");
        btnPrintMarDrug.setMaximumSize(new java.awt.Dimension(32, 32));
        btnPrintMarDrug.setMinimumSize(new java.awt.Dimension(32, 32));
        btnPrintMarDrug.setPreferredSize(new java.awt.Dimension(32, 32));
        btnPrintMarDrug.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintMarDrugActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        jPanelRightSide.add(btnPrintMarDrug, gridBagConstraints);

        btnPrintMarFluid.setFont(btnPrintMarFluid.getFont());
        btnPrintMarFluid.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/mar_fluid.png"))); // NOI18N
        btnPrintMarFluid.setToolTipText("������ Mar ����Ѻ��ù��");
        btnPrintMarFluid.setMaximumSize(new java.awt.Dimension(32, 32));
        btnPrintMarFluid.setMinimumSize(new java.awt.Dimension(32, 32));
        btnPrintMarFluid.setPreferredSize(new java.awt.Dimension(32, 32));
        btnPrintMarFluid.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintMarFluidActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        jPanelRightSide.add(btnPrintMarFluid, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelOrderList.add(jPanelRightSide, gridBagConstraints);

        jPanel4.setLayout(new java.awt.GridBagLayout());

        lblLimit.setFont(lblLimit.getFont().deriveFont(lblLimit.getFont().getStyle() | java.awt.Font.BOLD, lblLimit.getFont().getSize()+1));
        lblLimit.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblLimit.setText("ǧ�Թ �ҷ");
        lblLimit.setOpaque(true);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel4.add(lblLimit, gridBagConstraints);

        lblTotal.setFont(lblTotal.getFont().deriveFont(lblTotal.getFont().getStyle() | java.awt.Font.BOLD, lblTotal.getFont().getSize()+1));
        lblTotal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTotal.setText("������Թ �ҷ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_END;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel4.add(lblTotal, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelOrderList.add(jPanel4, gridBagConstraints);

        jSplitPane1.setRightComponent(jPanelOrderList);

        jSplitPane2.setLeftComponent(jSplitPane1);

        jPanel2.setLayout(new java.awt.GridBagLayout());

        jPanelAction.setLayout(new java.awt.GridBagLayout());

        jButtonVerify.setFont(jButtonVerify.getFont());
        jButtonVerify.setText("�׹�ѹ");
        jButtonVerify.setToolTipText("���͡��¡�����ǡ����� v");
        jButtonVerify.setMargin(new java.awt.Insets(1, 3, 1, 3));
        jButtonVerify.setMinimumSize(new java.awt.Dimension(70, 32));
        jButtonVerify.setPreferredSize(new java.awt.Dimension(70, 32));
        jButtonVerify.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonVerifyActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 0.3;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelAction.add(jButtonVerify, gridBagConstraints);

        jButtonExecute.setFont(jButtonExecute.getFont());
        jButtonExecute.setText("���Թ���");
        jButtonExecute.setToolTipText("���͡��¡�����ǡ����� e");
        jButtonExecute.setMargin(new java.awt.Insets(1, 3, 1, 3));
        jButtonExecute.setMinimumSize(new java.awt.Dimension(70, 32));
        jButtonExecute.setPreferredSize(new java.awt.Dimension(70, 32));
        jButtonExecute.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonExecuteActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 0.3;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelAction.add(jButtonExecute, gridBagConstraints);

        jButtonCancel.setFont(jButtonCancel.getFont());
        jButtonCancel.setText("¡��ԡ");
        jButtonCancel.setToolTipText("���͡��¡�����ǡ����� Delete");
        jButtonCancel.setMargin(new java.awt.Insets(1, 3, 1, 3));
        jButtonCancel.setMinimumSize(new java.awt.Dimension(70, 32));
        jButtonCancel.setPreferredSize(new java.awt.Dimension(70, 32));
        jButtonCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 0.3;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelAction.add(jButtonCancel, gridBagConstraints);

        jButtonDispense.setFont(jButtonDispense.getFont());
        jButtonDispense.setText("����");
        jButtonDispense.setToolTipText("���͡��¡�����ǡ����� d");
        jButtonDispense.setMargin(new java.awt.Insets(1, 3, 1, 3));
        jButtonDispense.setMinimumSize(new java.awt.Dimension(70, 32));
        jButtonDispense.setPreferredSize(new java.awt.Dimension(70, 32));
        jButtonDispense.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDispenseActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 0.3;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelAction.add(jButtonDispense, gridBagConstraints);

        jToggleButtonSelectAll.setFont(jToggleButtonSelectAll.getFont());
        jToggleButtonSelectAll.setText("������");
        jToggleButtonSelectAll.setToolTipText("���͡��¡�����ǡ����� Ctrl+a");
        jToggleButtonSelectAll.setMargin(new java.awt.Insets(1, 3, 1, 3));
        jToggleButtonSelectAll.setMinimumSize(new java.awt.Dimension(70, 32));
        jToggleButtonSelectAll.setPreferredSize(new java.awt.Dimension(70, 32));
        jToggleButtonSelectAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButtonSelectAllActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 0.3;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelAction.add(jToggleButtonSelectAll, gridBagConstraints);

        jButtonCal.setFont(jButtonCal.getFont());
        jButtonCal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/calculate_24.png"))); // NOI18N
        jButtonCal.setToolTipText("�Դ�Թ ���͡��¡�����ǡ����� c");
        jButtonCal.setMinimumSize(new java.awt.Dimension(70, 32));
        jButtonCal.setPreferredSize(new java.awt.Dimension(70, 32));
        jButtonCal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCalActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 0.3;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelAction.add(jButtonCal, gridBagConstraints);

        jButtonDrugRx.setFont(jButtonDrugRx.getFont());
        jButtonDrugRx.setText("������");
        jButtonDrugRx.setToolTipText("���͡��¡�����ǡ����� d");
        jButtonDrugRx.setMargin(new java.awt.Insets(1, 3, 1, 3));
        jButtonDrugRx.setMinimumSize(new java.awt.Dimension(70, 32));
        jButtonDrugRx.setPreferredSize(new java.awt.Dimension(70, 32));
        jButtonDrugRx.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDrugRxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 0.3;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelAction.add(jButtonDrugRx, gridBagConstraints);

        jButtonSticker.setFont(jButtonSticker.getFont());
        jButtonSticker.setText("ʵ������");
        jButtonSticker.setToolTipText("���͡��¡�����ǡ����� d");
        jButtonSticker.setMargin(new java.awt.Insets(1, 3, 1, 3));
        jButtonSticker.setMinimumSize(new java.awt.Dimension(70, 32));
        jButtonSticker.setPreferredSize(new java.awt.Dimension(70, 32));
        jButtonSticker.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonStickerActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 0.3;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelAction.add(jButtonSticker, gridBagConstraints);

        jScrollPane2.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        jTextAreaNote.setFont(jTextAreaNote.getFont());
        jTextAreaNote.setLineWrap(true);
        jTextAreaNote.setWrapStyleWord(true);
        jTextAreaNote.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextAreaNoteFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextAreaNoteFocusLost(evt);
            }
        });
        jTextAreaNote.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextAreaNoteKeyReleased(evt);
            }
        });
        jScrollPane2.setViewportView(jTextAreaNote);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelAction.add(jScrollPane2, gridBagConstraints);

        jButtonBilling.setFont(jButtonBilling.getFont());
        jButtonBilling.setText("�����");
        jButtonBilling.setToolTipText("���͡��¡�����ǡ����� d");
        jButtonBilling.setMargin(new java.awt.Insets(1, 3, 1, 3));
        jButtonBilling.setMinimumSize(new java.awt.Dimension(70, 32));
        jButtonBilling.setPreferredSize(new java.awt.Dimension(70, 32));
        jButtonBilling.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonBillingActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 0.3;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelAction.add(jButtonBilling, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(5, 2, 5, 5);
        jPanel2.add(jPanelAction, gridBagConstraints);

        pDOrder1.setName("pdOrder"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel2.add(pDOrder1, gridBagConstraints);

        jSplitPane2.setRightComponent(jPanel2);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        add(jSplitPane2, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jCheckBoxContinueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxContinueActionPerformed
        continueDrug(false);
    }//GEN-LAST:event_jCheckBoxContinueActionPerformed

    private void jCheckBoxShowCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxShowCancelActionPerformed
        doRefreshOrderList();
    }//GEN-LAST:event_jCheckBoxShowCancelActionPerformed

    private void jTextFieldKeywordMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextFieldKeywordMouseReleased
        jTextFieldKeyword.setSelectionStart(0);
        jTextFieldKeyword.setSelectionEnd(jTextFieldKeyword.getText().length());

    }//GEN-LAST:event_jTextFieldKeywordMouseReleased

    private void jButtonBillingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonBillingActionPerformed
        if (!Gutil.checkModulePrinting()) {
            theUS.setStatus("����� Module Printing", UpdateStatus.WARNING);
            return;
        }
        Vector vOrder = getOrderItemV();
        theHC.thePrintControl.printSumByBillingGroupNew(preview, vOrder);
    }//GEN-LAST:event_jButtonBillingActionPerformed

    private void jTextAreaNoteKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextAreaNoteKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            jToggleButtonSelectAll.requestFocus();
        }
    }//GEN-LAST:event_jTextAreaNoteKeyReleased

    private void jButtonStickerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonStickerActionPerformed
        this.doPrintStickerAction(getOrderItemV());
    }//GEN-LAST:event_jButtonStickerActionPerformed

    private void jButtonDrugRxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDrugRxActionPerformed
        if (theHO.theVisit == null) {
            theUS.setStatus("��س����͡�����·������㹡�кǹ���", UpdateStatus.WARNING);
            return;
        }
        if (!Gutil.checkModulePrinting()) {
            theUS.setStatus("�������ö������� �ѧ�Ҵ Module Printing", UpdateStatus.WARNING);
            return;
        }

        // ��������红����š�þ������䢻ѭ�Ҿ���������Ҩҡ���������������������¡�÷�����͡�ռ���׹�ѹ�ҡ���� 1 �� ���仴֧�Ҿ�������
        // �¨о�������ᾷ�줹����ش��� sumo 25/7/2549
        PrintSelectDrugList pd = new PrintSelectDrugList();
        pd.typePrint = 1;
        pd.employeeid = "";
        pd.nameDoctor = "";
        pd.selectdrug = true;
        pd.selectlab = true;
        pd.selectservice = true;
        pd.selectsupply = true;
        pd.selectxray = true;
        pd.selectDental = true;

        List<OrderItem> ois = new ArrayList<OrderItem>();
        int[] rows = this.tableOrderList.getSelectedRows();
        for (int i = 0; i < rows.length; i++) {
            ois.add((OrderItem) theOrderItemV.get(rows[i]));
        }
        theHC.thePrintControl.printSelectDrugList(pd, nopreview, ois);
    }//GEN-LAST:event_jButtonDrugRxActionPerformed

    private void jButtonB3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonB3ActionPerformed
        String command = theHC.theLookupControl.readOption().b3_command;
        try {
            Runtime.getRuntime().exec(command);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
        }
    }//GEN-LAST:event_jButtonB3ActionPerformed

    private void jCheckBoxPeriodActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxPeriodActionPerformed
        if (jCheckBoxPeriod.isSelected()) {
            dateComboBoxEnd.setEnabled(true);
            dateComboBoxStart.setEnabled(true);
        } else {
            dateComboBoxEnd.setEnabled(false);
            dateComboBoxStart.setEnabled(false);
        }
        doRefreshOrderList();
    }//GEN-LAST:event_jCheckBoxPeriodActionPerformed

    private void tableOrderListKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tableOrderListKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            int row = tableOrderList.getSelectedRow();
            if (row == 0) {
                tableOrderList.setRowSelectionInterval(0, 0);
            } else {
                tableOrderList.setRowSelectionInterval(row - 1, row - 1);
            }
            this.getPanelOrder().requestFocus();
        }
    }//GEN-LAST:event_tableOrderListKeyPressed

    private void tableItemListKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tableItemListKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            int row = tableItemList.getSelectedRow();
            if (row == 0) {
                row = 1;
            }
            tableItemList.setRowSelectionInterval(row - 1, row - 1);
            getPanelOrder().requestFocus();
        }
    }//GEN-LAST:event_tableItemListKeyPressed

    private void jToggleButtonSelectAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButtonSelectAllActionPerformed
        //�������¡�õ�Ǩ�ѡ�����
        if (jToggleButtonSelectAll.isSelected()) {
            tableOrderList.selectAll();
            tableOrderList.requestFocus();
        } else {
            tableOrderList.clearSelection();
        }
        getInfPanelOrder().setOrderItems((OrderItem[]) theOrderItemV.toArray(new OrderItem[theOrderItemV.size()]));
        tableOrderListMouseReleased(null);
    }//GEN-LAST:event_jToggleButtonSelectAllActionPerformed

    private void jComboBoxGroup1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxGroup1ActionPerformed
        doRefreshOrderList();
    }//GEN-LAST:event_jComboBoxGroup1ActionPerformed

    private void jRadioButtonBeginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonBeginActionPerformed
        jButtonSearchActionPerformed(null);
    }//GEN-LAST:event_jRadioButtonBeginActionPerformed

    private void jRadioButtonConsistActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonConsistActionPerformed
        jButtonSearchActionPerformed(null);
    }//GEN-LAST:event_jRadioButtonConsistActionPerformed

    private void jButtonDrugOldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDrugOldActionPerformed
        if (theHO.theVisit.visit_type.equals(VisitType.IPD)) {
            theHD.showDialogOrderOldVisitByDate(theHO.thePatient, theHO.theVisit);
        } else {
            theHD.showDialogOrderOldVisitByVn(theHO.thePatient, theHO.theVisit);
        }
    }//GEN-LAST:event_jButtonDrugOldActionPerformed

    private void jButtonDrugSetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDrugSetActionPerformed
        this.showDialogOrderSet();
    }//GEN-LAST:event_jButtonDrugSetActionPerformed

    private void jCheckBoxGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxGroupActionPerformed
        jComboBoxGroup.setEnabled(jCheckBoxGroup.isSelected());
    }//GEN-LAST:event_jCheckBoxGroupActionPerformed

    private void tableOrderListKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tableOrderListKeyReleased
        this.doSelectedOrderList(evt);
    }//GEN-LAST:event_tableOrderListKeyReleased

    private void tableOrderListMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableOrderListMouseReleased
        tableItemList.clearSelection();
        if (evt == null) {
            return;
        }
        if (evt.isPopupTrigger()) {
            jPopupMenu1.setLocation((evt.getX() + this.tableOrderList.getLocationOnScreen().x), (evt.getY() + tableOrderList.getLocationOnScreen().y));
            jPopupMenu1.setVisible(true);
        } else {
            jPopupMenu1.setVisible(false);
        }
        theHO.is_order = true;
        int[] select = tableOrderList.getSelectedRows();
        if (select.length == 1) {
            OrderItem oi = (OrderItem) theOrderItemV.get(select[0]);
            getInfPanelOrder().setOrderItem(oi);
            if (evt.getClickCount() == 2) {
                if (oi.isLab()) {
                    this.doubleClickListLab(oi);
                } else if (oi.isXray()) {
                    this.doubleClickListXray(oi);
                }
            }
        } else if (select.length > 1) {
            OrderItem[] orderItems = new OrderItem[select.length];
            for (int i = 0; i < orderItems.length; i++) {
                orderItems[i] = (OrderItem) theOrderItemV.get(select[i]);
            }
            getInfPanelOrder().setOrderItems(orderItems);
        } else {
            getInfPanelOrder().setOrderItem(null);
        }
        jCheckBoxContinue.setSelected(isContinue(theOrderItemV, select));
        //�ӹǹ�������·�����������͡
        if (select.length > 1) {
            double price = 0;
            boolean isCeil = theHC.theLookupControl.readOption().use_money_ceil.equals("1");
            for (int i = 0; i < select.length; i++) {
                OrderItem oi = (OrderItem) theOrderItemV.get(select[i]);
                float ppu = Float.parseFloat(oi.price);
                float qty = Float.parseFloat(oi.qty);
                price += isCeil ? Math.ceil(ppu * qty) : (ppu * qty);
            }
            theUS.setStatus(Constant.getTextBundle("�ӹǹ��¡�÷�����͡") + " " + select.length + " "
                    + Constant.getTextBundle("��¡�� ���") + " " + Constant.doubleToDBString(price) + " "
                    + Constant.getTextBundle("�ҷ"), UpdateStatus.COMPLETE);
        }
        this.jToggleButtonSelectAll.setSelected(false);
    }//GEN-LAST:event_tableOrderListMouseReleased

    private void tableItemListKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tableItemListKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_UP || evt.getKeyCode() == KeyEvent.VK_DOWN) {
            tableOrderList.clearSelection();
            Item item_order = getItem();
            if (item_order == null) {
                return;
            }
            getInfPanelOrder().setItem(item_order);
        }
        if (evt.getKeyCode() == KeyEvent.VK_SPACE) {
            OrderItem oi = getInfPanelOrder().getOrderItem();
            if (oi.isLab() && "1".equals(oi.secret)) {
                return;
            }
            this.getInfPanelOrder().saveOrderItem();
        }
    }//GEN-LAST:event_tableItemListKeyReleased

    private void tableItemListMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableItemListMouseReleased
        this.doSelectedItem(evt);
    }//GEN-LAST:event_tableItemListMouseReleased

    private void jComboBoxGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxGroupActionPerformed
        if (jCheckBoxGroup.isSelected()) {
            jButtonSearchActionPerformed(null);
        }
    }//GEN-LAST:event_jComboBoxGroupActionPerformed

    private void jButtonVerifyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonVerifyActionPerformed
        this.doVerifySelectedItemAction();
    }//GEN-LAST:event_jButtonVerifyActionPerformed

    private void jButtonCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelActionPerformed
        this.doCancelAction();
    }//GEN-LAST:event_jButtonCancelActionPerformed

    private void jButtonDispenseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDispenseActionPerformed
        this.doDispenseAction();
    }//GEN-LAST:event_jButtonDispenseActionPerformed

    private void jButtonRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRefreshActionPerformed
        doRefreshOrderList();
    }//GEN-LAST:event_jButtonRefreshActionPerformed

    private void jButtonCalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCalActionPerformed
        this.doCalMoney();
    }//GEN-LAST:event_jButtonCalActionPerformed

    private void jButtonExecuteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonExecuteActionPerformed
        int[] row = tableOrderList.getSelectedRows();
        List<OrderItem> executeOrderItem = theHC.theOrderControl.executeOrderItem(getOrderItemV(), row);
        if (!executeOrderItem.isEmpty()) {
            this.doPrintStickerAction(executeOrderItem);
        }
    }//GEN-LAST:event_jButtonExecuteActionPerformed

    private void jTextFieldKeywordKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldKeywordKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_DOWN) {
            if (tableItemList.getRowCount() > 0) {
                tableItemList.requestFocus();
                tableItemList.setRowSelectionInterval(0, 0);
            }
        } else if (this.jTextFieldKeyword.getText().endsWith(" ")) {
            this.jButtonSearch.doClick();
        } else {
            this.jButtonSearch.doClick();
        }
    }//GEN-LAST:event_jTextFieldKeywordKeyReleased

    private void jButtonSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSearchActionPerformed
        doSearchAction();
    }//GEN-LAST:event_jButtonSearchActionPerformed

    private void jTextFieldKeywordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldKeywordActionPerformed
        doSearchAction();
    }//GEN-LAST:event_jTextFieldKeywordActionPerformed

    private void btnStickerTubeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStickerTubeActionPerformed
        this.doPrintStickerTube();

    }//GEN-LAST:event_btnStickerTubeActionPerformed

    private void jTextAreaNoteFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextAreaNoteFocusLost
        theHC.theVitalControl.saveDxNote(jTextAreaNote.getText());
    }//GEN-LAST:event_jTextAreaNoteFocusLost

    private void jTextAreaNoteFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextAreaNoteFocusGained
        jTextAreaNote.selectAll();
    }//GEN-LAST:event_jTextAreaNoteFocusGained

    private void btnDrugFavoriteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDrugFavoriteActionPerformed
        showDialogDrugFavorite();
    }//GEN-LAST:event_btnDrugFavoriteActionPerformed

    private void btnPrintMarDrugActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintMarDrugActionPerformed
        theHC.thePrintControl.printMarDrug(PrintControl.MODE_PRINT, getSelectedOrderIds());
    }//GEN-LAST:event_btnPrintMarDrugActionPerformed

    private void btnPrintMarFluidActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintMarFluidActionPerformed
        theHC.thePrintControl.printMarFluid(PrintControl.MODE_PRINT, getSelectedOrderIds());
    }//GEN-LAST:event_btnPrintMarFluidActionPerformed

    private void btnViewLISResultActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewLISResultActionPerformed
        theHD.showDialogLISResult();
    }//GEN-LAST:event_btnViewLISResultActionPerformed

    private void formComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentResized
        this.handleResized();
    }//GEN-LAST:event_formComponentResized

    private void formComponentShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentShown
        System.out.println("formComponentShown");
    }//GEN-LAST:event_formComponentShown

    /**
     * ��㹡������ҵ�����ͧ����¡��ԡ�ҵ�����ͧ�� �Ѻ������� boolean
     * ����� true �����͡���˹� ����� false ���͡������
     *
     * @param selectall ����� true ������͡������ ��зӡ�õ�Ǩ�ͺ �����
     * false ������͡੾�з��١��˹������Ǩҡ���ҧ
     *
     */
    public void continueDrug(boolean selectall) {
        if (selectall) {
            tableOrderList.selectAll();
            jCheckBoxContinue.setSelected(false);
        }
        int[] row = tableOrderList.getSelectedRows();
        /////////////////////////////////////////////////////////////////////////
        ddc = new DiagDoctorClinic();
        if (theHO.theEmployee.authentication_id.equals(Authentication.DOCTOR)) {
            ddc.doctor_id = theHO.theEmployee.getObjectId();
        }

        if (HosObject.isOrderChildPackage(getOrderItemV())) {
            theUS.setStatus("��¡�����㹪ش�������ö�������¡�õ�����ͧ��", UpdateStatus.WARNING);
            return;
        }

        if (jCheckBoxContinue.isSelected()) {
            if (!Authentication.DOCTOR.equals(theHO.theEmployee.authentication_id)) {
                if (!theHD.showDialogContinueDrug(ddc)) {
                    return;
                }
            }
            theHC.theOrderControl.continueOrderItem(theOrderItemV, row, new Employee(ddc.doctor_id));
        } ////////////////////////////////////////////////////////////////////////
        else {
            if (!isContinue(theOrderItemV, row)) {
                theUS.setStatus("�������¡���ҵ�����ͧ", UpdateStatus.WARNING);
                return;
            }
            if (!Authentication.DOCTOR.equals(theHO.theEmployee.authentication_id)) {
                if (!theHD.showDialogOffDrug(ddc)) {
                    return;
                }
            }
            theHC.theOrderControl.cancelOrderDrugContinue(theOrderItemV, row, new Employee(ddc.doctor_id));
        }
        jCheckBoxContinue.setSelected(isContinue(theOrderItemV, row));
        ///////////////////////////////////////////////////////////////////////////////
    }

    public boolean isContinue(Vector vorder, int[] row) {
        int haveContinue = 0;
        for (int i = 0; i < row.length; i++) {
            OrderItem oi = (OrderItem) vorder.get(row[i]);
            if (oi.continue_order.equals("1")) {
                haveContinue += 1;
            }
        }
        if (row.length > 0) {
            return (haveContinue == row.length);
        } else {
            return false;
        }
    }

    /*
     * �Ѻ�Ҩҡ�ӹǹ��
     */
    public static int countWaitDispense(Vector theOrderItemV) {
        int queueDespense = 0;
        for (int i = 0; i < theOrderItemV.size(); i++) {
            OrderItem theOrderItemVs = (OrderItem) theOrderItemV.get(i);
            if (theOrderItemVs.category_group.equals("1")
                    && theOrderItemVs.charge_complete.equals(Active.isEnable())
                    && theOrderItemVs.dispense != null
                    && !theOrderItemVs.dispense.isEmpty()) {
                queueDespense += 1;
            }
        }
        return queueDespense;
    }

    /*
     * �Ѻ�Ҩҡ�ӹǹ��
     */
    protected void setVisit(Visit v) {
        theVisit = v;
        if (v == null) {
            setEnabled(false);
            /*
             * �������ö�����ʵ���������� ������ͧ���͡������
             */
 /*
             * pu : 20/07/2549
             */
            setEnabledRead(true);
            setOrderV(null);
            this.jTextAreaNote.setText("");
            return;
        }
        this.jTextAreaNote.setText(v.diagnosis_note);
        jCheckBoxContinue.setVisible(v.visit_type.equals(VisitType.IPD));
        btnPrintMarDrug.setVisible(v.visit_type.equals(VisitType.IPD));
        btnPrintMarFluid.setVisible(v.visit_type.equals(VisitType.IPD));
        if (theHO.theVisit.locking.equals("1")
                && !theHO.theVisit.lock_user.equals(theHO.theEmployee.getObjectId())) {
            setEnabled(false);
            setEnabledRead(true);
            return;
        }
        if (theHO.theVisit.is_discharge_money.equals("1")) {
            setEnabled(false);
            //�����¨����Ѻ������ͨ����Թ��������
            boolean b = true;
            jButtonDispense.setEnabled(b);
            jButtonVerify.setEnabled(b);
            jButtonExecute.setEnabled(b);
            tableOrderList.setEnabled(b);
            jButtonRefresh.setEnabled(b);
            jToggleButtonSelectAll.setEnabled(b);
            jCheckBoxPeriod.setEnabled(b);
            dateComboBoxEnd.setEnabled(b);
            dateComboBoxStart.setEnabled(b);
            jComboBoxGroup1.setEnabled(b);
            return;
        }
        if (theVisit.isOutProcess()
                || theVisit.isDropVisit()) {
            setEnabled(false);
            return;
        }
        setEnabled(true);
        jCheckBoxContinue.setSelected(false);
    }

    /*
     *
     * //henbe_comment ��Ǩ�ͺ�����¡�è�������������ѧ
     * ��Ҩ���������ǡ����ź�͡�ҡ����ͨ����� public static int
     * checkDespense(Vector theOrderItemV) //henbe_error this function move to
     * control
     *
     */
    protected void setItemV(Vector vc) {
        theItemV = vc;
        TaBleModel tm;
        if (vc == null) {
            tm = new TaBleModel(col_jTableItemList, 0);
            tableItemList.setModel(tm);
            return;
        }
        tm = new TaBleModel(col_jTableItemList, vc.size());
        CategoryGroupItem cg = null;
        for (int i = 0; i < vc.size(); i++) {
            Item p = (Item) vc.get(i);
            if (cg == null || !cg.getObjectId().equals(p.item_group_code_category)) {
                cg = theHC.theLookupControl.readCategoryGroupItemById(p.item_group_code_category);
            }
            String group;
            if (cg != null) {
                group = cg.description;
            } else {
                group = "�������ջѭ��";
            }
            String[] common_name = {p.common_name, "����� " + group};
            String[] trade_name = {p.trade_name, "����� " + group};
            tm.setValueAt(common_name, i, 0);
            tm.setValueAt(trade_name, i, 1);
        }
        if (tableItemList.getColumnModel().getColumnCount() > 0) {
            int column1_wide = tableItemList.getColumnModel().getColumn(0).getWidth();
            int column2_wide = tableItemList.getColumnModel().getColumn(1).getWidth();
            tableItemList.setModel(tm);
            tableItemList.getColumnModel().getColumn(0).setPreferredWidth(column1_wide); // �ӹǹ
            tableItemList.getColumnModel().getColumn(1).setPreferredWidth(column2_wide); // �����
        } else {
            tableItemList.setModel(tm);
        }
        tableItemList.getColumnModel().getColumn(0).setCellRenderer(cellRendererItem);
        tableItemList.getColumnModel().getColumn(1).setCellRenderer(cellRendererItem);
    }

    private void setTableOrderList(boolean isIPD) {
        tableOrderList.getColumnModel().getColumn(0).setMaxWidth(60); // ��͡
        tableOrderList.getColumnModel().getColumn(0).setCellRenderer(cellRenderer);
        tableOrderList.getColumnModel().getColumn(1).setPreferredWidth(250); // �������ѭ����Ѻ þ.�����, ���͡�ä�� ����Ѻ�ٹ���ä��
        tableOrderList.getColumnModel().getColumn(1).setCellRenderer(cellRendererDayOrder);
        if (isShowDose) {
            tableOrderList.getColumnModel().getColumn(2).setPreferredWidth(180);
            tableOrderList.getColumnModel().getColumn(2).setMaxWidth(180); // dose ���
            tableOrderList.getColumnModel().getColumn(2).setCellRenderer(new CellRendererHos(CellRendererHos.ALIGNMENT_RIGHT));
//            tableOrderList.getColumnModel().getColumn(2).setCellEditor(cellEditorDoseShortcut);
        }
        tableOrderList.getColumnModel().getColumn(2 + (isShowDose ? 1 : 0)).setPreferredWidth(100); // �ӹǹ
        tableOrderList.getColumnModel().getColumn(2 + (isShowDose ? 1 : 0)).setMaxWidth(100); // �ӹǹ
        tableOrderList.getColumnModel().getColumn(2 + (isShowDose ? 1 : 0)).setCellEditor(new NumberCellEditor()); // �ӹǹ
        tableOrderList.getColumnModel().getColumn(2 + (isShowDose ? 1 : 0)).setCellRenderer(TableRenderer.getRendererRight());
        tableOrderList.getColumnModel().getColumn(3 + (isShowDose ? 1 : 0)).setPreferredWidth(100); // �Ҥ�
        tableOrderList.getColumnModel().getColumn(3 + (isShowDose ? 1 : 0)).setMaxWidth(100); // �Ҥ�

        tableOrderList.getColumnModel().getColumn(3 + (isShowDose ? 1 : 0)).setCellRenderer(TableRenderer.getRendererRight());
        tableOrderList.getColumnModel().getColumn(4 + (isShowDose ? 1 : 0)).setPreferredWidth(100); // ʶҹ�
        tableOrderList.getColumnModel().getColumn(4 + (isShowDose ? 1 : 0)).setMaxWidth(100); // ʶҹ�
        tableOrderList.getColumnModel().getColumn(4 + (isShowDose ? 1 : 0)).setCellRenderer(TableRenderer.getRendererCenter());
        tableOrderList.getColumnModel().getColumn(5 + (isShowDose ? 1 : 0)).setMaxWidth(60); // �͵�Ǩ
        HCheckBoxEditor ce = new HCheckBoxEditor(new JCheckBox());
        ce.setEControl(this);
        ce.setType("req");
        tableOrderList.getColumnModel().getColumn(5 + (isShowDose ? 1 : 0)).setCellEditor(ce);
        tableOrderList.getColumnModel().getColumn(5 + (isShowDose ? 1 : 0)).setCellRenderer(new CellRendererCheckBox());
        if (isIPD) {
            tableOrderList.getColumnModel().getColumn(6 + (isShowDose ? 1 : 0)).setMaxWidth(60); // ����ҵ�����ͧ
            tableOrderList.getColumnModel().getColumn(6 + (isShowDose ? 1 : 0)).setCellRenderer(cellRendererContinue);
            /**
             * ��¡�á�Ѻ��ҹ
             */
            HCheckBoxEditor order_home = new HCheckBoxEditor(new JCheckBox());
            order_home.setEControl(this);
            order_home.setType("home");
            tableOrderList.getColumnModel().getColumn(7 + (isShowDose ? 1 : 0)).setCellEditor(order_home);
            tableOrderList.getColumnModel().getColumn(7 + (isShowDose ? 1 : 0)).setMaxWidth(60); // ��Ѻ��ҹ
            tableOrderList.getColumnModel().getColumn(7 + (isShowDose ? 1 : 0)).setCellRenderer(new CellRendererCheckBox());
        }

        if (isShowDoctor) {
            tableOrderList.getColumnModel().getColumn((isIPD ? 8 : 6) + (isShowDose ? 1 : 0)).setPreferredWidth(180);
            tableOrderList.getColumnModel().getColumn((isIPD ? 8 : 6) + (isShowDose ? 1 : 0)).setMaxWidth(200); // ᾷ��
            tableOrderList.getColumnModel().getColumn((isIPD ? 8 : 6) + (isShowDose ? 1 : 0)).setCellRenderer(new CellRendererHos(CellRendererHos.TOOL_TIP_TEXT));
        }
    }

    public int[] getCurrentRowsOrder() {
        return tableOrderList.getSelectedRows();
    }

    public boolean setCurrentRowsOrder(int[] rows) {
        getInfPanelOrder().setOrderItem(null);
        this.jToggleButtonSelectAll.setSelected(false);
        if (rows.length == 0) {
            int rowCount = tableOrderList.getRowCount();
            if (rowCount > 0) {
                tableOrderList.selectAll();
                getInfPanelOrder().setOrderItems((OrderItem[]) theOrderItemV.toArray(new OrderItem[theOrderItemV.size()]));
            }
        } else {
            if (tableOrderList.getRowCount() > rows[rows.length - 1]) {
                tableOrderList.setRowSelectionInterval(rows[0], rows[rows.length - 1]);
            }
            if (rows.length == 1 && rows[0] < theOrderItemV.size()) {
                OrderItem oi = (OrderItem) this.theOrderItemV.get(rows[0]);
                getInfPanelOrder().setOrderItem(oi);
            } else {
                int[] selectedRows = tableOrderList.getSelectedRows();
                OrderItem[] orderItems = new OrderItem[selectedRows.length];
                for (int i = 0; i < orderItems.length; i++) {
                    orderItems[i] = (OrderItem) this.theOrderItemV.get(selectedRows[i]);
                }
                getInfPanelOrder().setOrderItems(orderItems);
            }
        }
        return true;
    }

    /*
     * ��੾�� order ����ѧ���١¡��ԡ ��ǰҹ������ objdb
     * �֧੾����ǹ������������
     */
    private void setOrderV(Vector vc) {
        theOrderItemV = vc;
        if (theOrderItemV == null) {
            TaBleModel tm = new TaBleModel(colOrderOPD.toArray(new String[0]), 0);
            tableOrderList.setModel(tm);
            getInfPanelOrder().setOrderItem(null);
            lblTotal.setText(String.format("������Թ %,.2f �ҷ", 0d));
            lblLimit.setBackground(lblTotal.getBackground());
            lblLimit.setText("ǧ�Թ�Է�����ѡ - �ҷ");
            return;
        }
        int[] rows = tableOrderList.getSelectedRows();
        /*
         * �����¹͡
         */
        if (theHO.theVisit.visit_type.equals(VisitType.OPD)) {
            /*
             * ���͡ѹ��Ƿ��¡��ԡ�����͡�
             */
            TaBleModel tm = new TaBleModel(colOrderOPD.toArray(new String[0]), theOrderItemV.size());
            boolean is_outpc = theHO.theVisit.visit_status.equals(VisitStatus.isOutProcess());
            boolean is_lock = theHO.theVisit.locking.equals("1")
                    && !theHO.theVisit.lock_user.equals(theHO.theEmployee.getObjectId());

            if (!is_outpc && !is_lock) {
//                if (isShowDose) {
//                    tm.addEditingCol(2);
//                }
                tm.addEditingCol(2 + (isShowDose ? 1 : 0));
                tm.addEditingCol(5 + (isShowDose ? 1 : 0));
            }

            boolean isCeil = theHC.theLookupControl.readOption().use_money_ceil.equals("1");
            for (int i = 0; i < theOrderItemV.size(); i++) {
                OrderItem order = (OrderItem) theOrderItemV.get(i);
                // Somprasong 20110823 ������� float �Фӹǳ�Դ�óշ�� 0.6*200 =121 ��觵�ͧ�� 120
                double ppu = Double.parseDouble(order.price);
                double qty = Double.parseDouble(order.qty);
                OrderItemStatus os = theHC.theLookupControl.readOrderItemStatus(order.status);
                String status = "";
                double sum = isCeil ? Math.ceil(ppu * qty) : (ppu * qty);
                if (os != null) {
                    status = os.description;
                }
                int index = 0;
                tm.setValueAt(order.isChildPackage() ? "hide" : order.charge_complete, i, index++);
                Hashtable ht = new Hashtable();
                Hashtable ht_status = new Hashtable();
                //������Ҥ��¤鹡�÷ӻ�ԡ���ҵ�͡ѹ��ҹ�鹨�������ͧ query �ҡ�Թ henbe
                ht.put("OrderItem", order);
                ht.put("ln", order.isLab() ? this.theHC.theOrderControl.getLabNumber(order.getObjectId()) : "-");
                ht.put("String", order.sDrugInteraction);
                String ttt = theHC.theOrderControl.getTTTRenderDayOrder(order, "");
                ht.put("display_string", ttt);

                if (order.isDrug()) {
                    if (order.order_dx_item_risk.equals("1")) {
                        DxTemplate Dx = theHC.theOrderControl.readDxTemplateByItemID(order, theHO.theVisit);
                        if (Dx != null) {
                            String text = "<br><br>�ҹ���ռŵ�ͼ����� " + (Dx.thaidescription.isEmpty() ? Dx.description : Dx.thaidescription);
                            ht.put("dx_itemrisk", text);
                        }
                    }
                }
                tm.setValueAt(ht, i, index++);
                if (isShowDose) {
                    if (!order.isDrug()) {
                        tm.setValueAt("", i, index++);
                    } else {
                        OrderItemDrug oid = order.theOrderItemDrug;
                        if (oid == null) {
                            oid = theHC.theOrderControl.readOrderItemDrugByOid(order.getObjectId());
                        }
                        tm.setValueAt(oid.usage_special.equals(Active.isEnable()) ? "" : oid.dose_short, i, index++);
                    }
                }
                int colQty = index++;
                tm.setValueAt(Constant.getShowDoubleString(order.qty), i, colQty);
                // if not drug/service/supply or dispensed or calculated can not change qty or use stock and !NotVertify
                boolean isNotVertify = order.status.equals(OrderStatus.NOT_VERTIFY);
                boolean isUseHStock = order.getProperty("suffix_name") != null;
                if (!order.isEditableQty()
                        || order.status.equals(OrderStatus.DISPENSE)
                        || order.charge_complete.equals(Active.isEnable())
                        || (isUseHStock && !isNotVertify)) {
                    tm.addDisableEditingCell(i, colQty);
                }
                tm.setValueAt(Constant.doubleToDBString(sum), i, index++);
                tm.setValueAt(status, i, index++);

                boolean status_enable = !OrderStatus.DIS_CONTINUE.equals(order.status) && !order.isChildPackage();

                ht_status.put("status", Boolean.valueOf(status_enable));
                ht_status.put("request", Boolean.valueOf(order.request.equals("1")));
                tm.setValueAt(ht_status, i, index++);
                if (isShowDoctor) {
                    tm.setValueAt(order.getProperty("doctorName"), i, index++);
                }
            }
            tableOrderList.setModel(tm);
            setTableOrderList(false);
        } else if (theHO.theVisit.visit_type.equals(VisitType.IPD)) { // �������
            /*
             * ���͡ѹ��Ƿ��¡��ԡ�����͡� ¡���ʶҹШ���
             */
            TaBleModel tm = new TaBleModel(colOrderIPD.toArray(new String[0]), theOrderItemV.size());
//            if (isShowDose) {
//                tm.addEditingCol(2);
//            }
            tm.addEditingCol(2 + (isShowDose ? 1 : 0));
            tm.addEditingCol(5 + (isShowDose ? 1 : 0));
            tm.addEditingCol(7 + (isShowDose ? 1 : 0));

            boolean isCeil = theHC.theLookupControl.readOption().use_money_ceil.equals("1");
            for (int i = 0; i < theOrderItemV.size(); i++) {
                OrderItem order = (OrderItem) theOrderItemV.get(i);
                OrderItemStatus os = theHC.theLookupControl.readOrderItemStatus(order.status);
                String status = "";
                if (os != null) {
                    status = os.description;
                }
                double ppu = Double.parseDouble(order.price);
                double qty = Double.parseDouble(order.qty);
                double sum = isCeil ? Math.ceil(ppu * qty) : (ppu * qty);
                int index = 0;
                tm.setValueAt(order.isChildPackage() ? "hide" : order.charge_complete, i, index++);
                //amp:07/04/2549
                Hashtable ht = new Hashtable();
                ht.put("OrderItem", order);
                ht.put("String", order.sDrugInteraction);
                String ttt = theHC.theOrderControl.getTTTRenderDayOrder(order, "");
                ht.put("display_string", ttt);
                ht.put("ln", order.isLab() ? this.theHC.theOrderControl.getLabNumber(order.getObjectId()) : "-");
                if (order.isDrug()) {
                    if (order.order_dx_item_risk.equals("1")) {
                        DxTemplate Dx = theHC.theOrderControl.readDxTemplateByItemID(order, theHO.theVisit);
                        if (Dx != null) {
                            String text = "<br><br>�ҹ���ռŵ�ͼ����� " + (Dx.thaidescription.isEmpty() ? Dx.description : Dx.thaidescription);
                            ht.put("dx_itemrisk", text);
                        }
                    }
                }
                tm.setValueAt(ht, i, index++);
                if (isShowDose) {
                    if (!order.isDrug()) {
                        tm.setValueAt("", i, index++);
                    } else {
                        OrderItemDrug oid = order.theOrderItemDrug;
                        if (oid == null) {
                            oid = theHC.theOrderControl.readOrderItemDrugByOid(order.getObjectId());
                        }
                        tm.setValueAt(oid.usage_special.equals(Active.isEnable()) ? "" : oid.dose_short, i, index++);
                    }
                }
                int colQty = index++;
                tm.setValueAt((order.qty.lastIndexOf(".0") == -1
                        || Integer.parseInt(order.qty.substring(order.qty.indexOf('.') + 1)) > 0
                        ? order.qty : order.qty.substring(0, order.qty.indexOf('.'))), i, colQty);
                // if not drug/service/supply or dispensed or calculated can not change qty or use stock and !NotVertify
                boolean isNotVertify = order.status.equals(OrderStatus.NOT_VERTIFY);
                boolean isUseHStock = order.getProperty("suffix_name") != null;
                if (!order.isEditableQty()
                        || order.status.equals(OrderStatus.DISPENSE)
                        || order.charge_complete.equals(Active.isEnable())
                        || (isUseHStock && !isNotVertify)) {
                    tm.addDisableEditingCell(i, colQty);
                }
                tm.setValueAt(Constant.doubleToDBString(sum), i, index++);
                tm.setValueAt(status, i, index++);
                boolean status_enable = !OrderStatus.DIS_CONTINUE.equals(order.status) && !order.isChildPackage();
                Hashtable ht_status = new Hashtable();
                ht_status.put("status", status_enable);
                ht_status.put("request", order.request.equals("1"));
                tm.setValueAt(ht_status, i, index++);

                tm.setValueAt(order.continue_order, i, index++);

                status_enable = !order.isChildPackage();
                ht_status = new Hashtable();
                ht_status.put("status", status_enable);
                ht_status.put("request", order.order_home.equals("1"));

                tm.setValueAt(ht_status, i, index++);
                if (isShowDoctor) {
                    tm.setValueAt(order.getProperty("doctorName"), i, index++);
                }
            }
            tableOrderList.setModel(tm);
            setTableOrderList(true);
        }
        if (tableOrderList.getRowCount() > 0) {
            tableOrderList.setRowSelectionInterval(0, 0);
            OrderItem oi = (OrderItem) theOrderItemV.get(0);
            getInfPanelOrder().setOrderItem(oi);
        } else {
            getInfPanelOrder().setOrderItem(null);
        }
        // ��ǧ�Թ�ҡ�Է����ѡ
        double limit = 0.0d;
        if (theHO.vVisitPayment != null && !theHO.vVisitPayment.isEmpty()) {
            Payment payment = (Payment) theHO.vVisitPayment.get(0);
            limit = Double.parseDouble(payment.money_limit == null || payment.money_limit.isEmpty() ? "0" : payment.money_limit);
        }
        double summary = 0.0d;
        for (int i = 0; i < tableOrderList.getRowCount(); i++) {
            String value = String.valueOf(tableOrderList.getValueAt(i, 3 + (isShowDose ? 1 : 0)));
            summary += Double.parseDouble(value == null || value.isEmpty() ? "0" : value);
        }
        lblTotal.setText(String.format("������Թ %,.2f �ҷ", summary));
        lblLimit.setBackground(lblTotal.getBackground());
        if (limit <= 0.0d) {
            lblLimit.setText("ǧ�Թ�Է�����ѡ - �ҷ");
            if (jButtonCal.isVisible()) {
                jButtonCal.setEnabled(true);
            }
        } else {
            lblLimit.setText(String.format("%sǧ�Թ�Է�����ѡ %,.2f �ҷ", limit >= summary ? "" : "�Թ", limit));
            lblLimit.setBackground(limit < summary ? Color.RED : lblTotal.getBackground());
            if (jButtonCal.isVisible()) {
                jButtonCal.setEnabled(limit >= summary || (limit < summary && theHO.vVisitPayment != null && theHO.vVisitPayment.size() > 1));
            }
        }
    }
    /*
     * �繡�����͡��¡�� item ������������ʴ���������´�ͧ item �����Ф��
     * default order
     *
     */
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDrugFavorite;
    private javax.swing.JButton btnPrintMarDrug;
    private javax.swing.JButton btnPrintMarFluid;
    private javax.swing.JButton btnStickerTube;
    private javax.swing.JButton btnViewLISResult;
    private javax.swing.ButtonGroup buttonGroupaKeySearch;
    protected com.hospital_os.utility.DateComboBox dateComboBoxEnd;
    protected com.hospital_os.utility.DateComboBox dateComboBoxStart;
    private javax.swing.JButton jButtonB3;
    private javax.swing.JButton jButtonBilling;
    protected javax.swing.JButton jButtonCal;
    protected javax.swing.JButton jButtonCancel;
    private javax.swing.JButton jButtonDispense;
    private javax.swing.JButton jButtonDrugOld;
    private javax.swing.JButton jButtonDrugRx;
    protected javax.swing.JButton jButtonDrugSet;
    protected javax.swing.JButton jButtonExecute;
    protected javax.swing.JButton jButtonRefresh;
    protected javax.swing.JButton jButtonSearch;
    private javax.swing.JButton jButtonSticker;
    private javax.swing.JButton jButtonVerify;
    private javax.swing.JToggleButton jCheckBoxContinue;
    protected javax.swing.JCheckBox jCheckBoxGroup;
    protected javax.swing.JCheckBox jCheckBoxPeriod;
    protected javax.swing.JToggleButton jCheckBoxShowCancel;
    protected javax.swing.JComboBox jComboBoxGroup;
    protected javax.swing.JComboBox jComboBoxGroup1;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabelTo;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanelAction;
    private javax.swing.JPanel jPanelCondition;
    private javax.swing.JPanel jPanelOrderList;
    private javax.swing.JPanel jPanelRightSide;
    private javax.swing.JPanel jPanelSearchItem;
    protected javax.swing.JRadioButton jRadioButtonBegin;
    protected javax.swing.JRadioButton jRadioButtonConsist;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPaneOrderList;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JSplitPane jSplitPane2;
    private javax.swing.JTextArea jTextAreaNote;
    protected javax.swing.JTextField jTextFieldKeyword;
    private javax.swing.JToggleButton jToggleButtonSelectAll;
    private javax.swing.JLabel lblLimit;
    private javax.swing.JLabel lblTotal;
    protected com.hosv3.gui.panel.detail.PDOrder pDOrder1;
    private javax.swing.JPanel panelOptionCategory;
    protected javax.swing.JPanel panelSearchOptions;
    protected com.hosv3.gui.component.HJTableSort tableItemList;
    protected com.hosv3.gui.component.HJTableSort tableOrderList;
    // End of variables declaration//GEN-END:variables
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JMenuItem jMenuItemVerify;
    private javax.swing.JMenuItem jMenuItemExecute;
    private javax.swing.JMenuItem jMenuItemDispense;
    private javax.swing.JMenuItem jMenuItemCancel;

    @Override
    public void setEnabled(boolean b) {
        jButtonCal.setEnabled(b);
        jButtonVerify.setEnabled(b);
        jButtonExecute.setEnabled(b);
        jButtonDispense.setEnabled(b);
        jButtonCancel.setEnabled(b);
        jButtonDrugRx.setEnabled(b);
        tableItemList.setEnabled(b);
        jCheckBoxContinue.setEnabled(b);
        btnDrugFavorite.setEnabled(b);
        jButtonDrugOld.setEnabled(b);
        jButtonDrugSet.setEnabled(b);
        jButtonSearch.setEnabled(b);
        jTextFieldKeyword.setEnabled(b);
        jCheckBoxGroup.setEnabled(b);
        tableOrderList.setEnabled(b);
        jButtonRefresh.setEnabled(b);
        jToggleButtonSelectAll.setEnabled(b);
        jCheckBoxPeriod.setEnabled(b);
        dateComboBoxEnd.setEnabled(b);
        dateComboBoxStart.setEnabled(b);
        jComboBoxGroup1.setEnabled(b);
        jRadioButtonBegin.setEnabled(b);
        jRadioButtonConsist.setEnabled(b);
        jButtonB3.setEnabled(b);
        getPanelOrder().setEnabled(b);
        btnStickerTube.setEnabled(b);
    }

    private void setLanguage(String msg) {
        GuiLang.setLanguage(jButtonCal);
        GuiLang.setLanguage(jButtonBilling);
        GuiLang.setLanguage(pDOrder1);
        GuiLang.setLanguage(jCheckBoxGroup);
        GuiLang.setLanguage(jCheckBoxPeriod);
        GuiLang.setLanguage(jLabelTo);
        GuiLang.setLanguage(jButtonRefresh);
        GuiLang.setLanguage(jToggleButtonSelectAll);
        GuiLang.setLanguage(jButtonVerify);
        GuiLang.setLanguage(jButtonExecute);
        GuiLang.setLanguage(jButtonCancel);
        GuiLang.setLanguage(jButtonDispense);
        GuiLang.setLanguage(jCheckBoxContinue);
        GuiLang.setLanguage(jButtonCal);
        GuiLang.setTextBundle(jPanelSearchItem);
        GuiLang.setTextBundle(jPanelOrderList);
        GuiLang.setLanguage(col_jTableItemList);
        GuiLang.setLanguage(this.jCheckBoxShowCancel);
        GuiLang.setLanguage(this.jRadioButtonBegin);
        GuiLang.setLanguage(this.jRadioButtonConsist);
        GuiLang.setLanguage(this.jButtonDrugOld);
        GuiLang.setLanguage(this.jButtonDrugSet);
        GuiLang.setLanguage(this.jButtonSearch);
        GuiLang.setLanguage(this.jLabel4);
        GuiLang.setLanguage(jButtonDrugRx);
        GuiLang.setLanguage(jButtonSticker);
    }

    public void setEnabledRead(boolean b) {
        jButtonSearch.setEnabled(b);
        jTextFieldKeyword.setEnabled(b);
        jCheckBoxGroup.setEnabled(b);
        tableOrderList.setEnabled(b);
        jButtonRefresh.setEnabled(b);
        jToggleButtonSelectAll.setEnabled(b);
        jCheckBoxPeriod.setEnabled(b);
        dateComboBoxEnd.setEnabled(b);
        dateComboBoxStart.setEnabled(b);
        jComboBoxGroup1.setEnabled(b);
        jRadioButtonBegin.setEnabled(b);
        jRadioButtonConsist.setEnabled(b);
        tableOrderList.setEnabled(b);
        tableItemList.setEnabled(b);
    }

///////////////////////////////////////////////////////////////////////
    @Override
    public Vector getOrderItemV() {
        Vector v_selected = new Vector();
        int[] row = this.tableOrderList.getSelectedRows();
        for (int i = 0; i < row.length; i++) {
            v_selected.add(theOrderItemV.get(row[i]));
        }
        return v_selected;
    }

    @Override
    public Item getItem() {
        int row = this.tableItemList.getSelectedRow();
        if (row != -1) {
            return (Item) this.theItemV.get(row);
        }
        return null;
    }

    @Override
    public OrderItemDrug getOrderItemDrug() {
        return this.getInfPanelOrder().getOrderItemDrug();
    }

    /**
     * ���¡������� Double-Click �����¡��
     */
    protected void doubleClickListLab(OrderItem oi) {
        if (oi.isLab()
                && (oi.status.equals(OrderStatus.REMAIN)
                || oi.status.equals(OrderStatus.REPORT)
                || oi.status.equals(OrderStatus.EXECUTE))) {
            theHD.showDialogShowLabResult(oi);
        }
    }

    /**
     * ���¡������� Double-Click �����¡��
     *
     * @param oi
     */
    protected void doubleClickListXray(OrderItem oi) {
        if (oi.isXray()
                && (oi.status.equals(OrderStatus.REPORT)
                || oi.status.equals(OrderStatus.EXECUTE))) {
            theHD.showDialogResultXray(oi);
        }
    }

    protected void doubleClickList(java.awt.event.MouseEvent evt) {
        if (evt.getClickCount() == 2) {
            OrderItem oi = getInfPanelOrder().getOrderItem();
            if (oi.isLab() && "1".equals(oi.secret)) {
                return;
            }
            this.getInfPanelOrder().saveOrderItem();
        }
    }

    /*
     * ManageVisit
     */
    @Override
    public void notifyAdmitVisit(String str, int status) {
        setVisit(theHO.theVisit);
        doRefreshOrderList();
    }

    /*
     * ��ԡ���͡�����¨ҡ�ش��ԡ��
     */
    @Override
    public void notifyReadVisit(String str, int status) {
        setVisit(theHO.theVisit);
        Vector v_item_service = theHC.theVitalControl.listItemByDxTemplate();
        if (!v_item_service.isEmpty()) {
            setItemV(v_item_service);
        }
        this.tableOrderList.clearSelection();
        doRefreshOrderList();
        if (!jCheckBoxPeriod.isSelected()) {
            dateComboBoxStart.setEnabled(false);
            dateComboBoxEnd.setEnabled(false);
        }
    }

    @Override
    public void notifyObservVisit(String str, int status) {
    }

    /*
     * �Ŵ��͡������
     */
    @Override
    public void notifyUnlockVisit(String str, int status) {
        setVisit(null);
    }

    @Override
    public void notifyVisitPatient(String str, int status) {
        doRefreshOrderList();
        setVisit(theHO.theVisit);
    }

    @Override
    public void notifyCheckDoctorTreament(String str, int status) {
    }

    public void notifyManageAppointment(String str, int status) {
    }

    @Override
    public void notifyManageDrugAllergy(String str, int status) {
        doRefreshOrderList();
    }

    @Override
    public void notifySavePatientPayment(String str, int status) {
    }

    /*
     * ManageOrder henbe_comment_checkusage
     */
    @Override
    public void notifyCheckAutoOrder(String str, int status) {
        doRefreshOrderList();
    }

    @Override
    public void notifyReceiveReturnDrug(String str, int status) {
    }

    @Override
    public void notifyDoctorOffDrug(String DoctorId, int status) {
        doRefreshOrderList();
    }

    @Override
    public void notifySaveOrderItem(String str, int status) {
        doRefreshOrderList();
        jTextFieldKeyword.requestFocus();
        jTextFieldKeyword.selectAll();
    }

    /*
     * ManageBilling
     */
    @Override
    public void notifyBillingInvoice(String str, int status) {
        doRefreshOrderList();
    }

    public void notifyBillingInvoiceComplete(String str, int status) {
        setVisit(theHO.theVisit);
    }

    @Override
    public void notifyCalculateAllBillingInvoice(String str, int status) {
    }

    @Override
    public void notifyCancelBillingInvoice(String str, int status) {
        doRefreshOrderList();
    }

    @Override
    public void notifyDischargeDoctor(String str, int status) {
        setVisit(theHO.theVisit);
    }

    @Override
    public void notifyVerifyOrderItem(String str, int status) {
        doRefreshOrderList();
    }

    @Override
    public void notifyPatientPaidMoney(String str, int status) {
    }

    @Override
    public void notifyDropVisit(String str, int status) {
        setVisit(theHO.theVisit);
    }

    @Override
    public void notifySendVisit(String str, int status) {
        setVisit(theHO.theVisit);
    }

    @Override
    public void notifyDischargeFinancial(String str, int status) {
        setVisit(theHO.theVisit);
    }

    @Override
    public void notifyCancelBill(String str, int status) {
    }

    @Override
    public void notifyReverseFinancial(String str, int status) {
        setVisit(theHO.theVisit);
    }

    @Override
    public void notifyReverseDoctor(String str, int status) {
    }

    @Override
    public void notifyDeleteVisitPayment(String str, int status) {
    }

    @Override
    public void notifyDeletePatientPayment(String str, int status) {
    }

    @Override
    public void notifySaveOrderItemInLab(String str, int status) {
    }

    @Override
    public void notifyDispenseOrderItem(String str, int status) {
        doRefreshOrderList();
    }

    @Override
    public void notifyCancelOrderItem(String str, int status) {
        doRefreshOrderList();
    }

    @Override
    public void notifyExecuteOrderItem(String str, int status) {
        doRefreshOrderList();
    }

    @Override
    public void notifyContinueOrderItem(String str, int status) {
        doRefreshOrderList();
    }

    @Override
    public void notifyReferOutLab(String msg, int status) {
    }

    @Override
    public void notifyReadPatient(String str, int status) {
        setVisit(theHO.theVisit);
    }

    @Override
    public void notifyReadFamily(String str, int status) {
        setVisit(theHO.theVisit);
    }

    @Override
    public void notifySavePatient(String str, int status) {
    }

    @Override
    public void notifyDeletePatient(String str, int status) {
    }

    @Override
    public void notifyPreviewAppointmentList(String str, int status) {
    }

    @Override
    public void notifyPreviewSelectDrugList(String str, int status) {
    }

    @Override
    public void notifyPrintAppointmentList(String str, int status) {
    }

    @Override
    public void notifyPrintChronicList(String str, int status) {
    }

    @Override
    public void notifyPrintDrugSticker(String str, int status) {
    }

    @Override
    public void notifyPrintOPDCard(String str, int status) {
    }

    @Override
    public void notifyPriviewChronicList(String str, int status) {
    }

    @Override
    public void notifyPrintSelectDrugList(String str, int status) {
    }

    @Override
    public void notifyPrintSumByBillingGroup(String str, int status) {
    }

    @Override
    public void notifyPreviewSumByBillingGroup(String str, int status) {
    }

    @Override
    public void notifyRemainDoctorDischarge(String str, int status) {
        setVisit(null);
    }

    @Override
    public void notifySendVisitBackWard(String str, int status) {
        setVisit(null);
    }

    @Override
    public void notifySaveReturnDrug(String str, int status) {
        doRefreshOrderList();
    }

    @Override
    public void notifySaveOrderRequest(String str, int status) {
        doRefreshOrderList();
    }

    @Override
    public void notifySaveAppointment(String str, int status) {
    }

    @Override
    public void notifyReverseAdmit(String str, int status) {
        setVisit(theHO.theVisit);
        doRefreshOrderList();
    }

    @Override
    public void notifyResetPatient(String str, int status) {
        setVisit(null);
    }

    @Override
    public void notifyAddLabReferIn(String str, int status) {
    }

    @Override
    public void notifyAddLabReferOut(String str, int status) {
    }

    @Override
    public void notifyDeleteFilmXray(String str, int status) {
    }

    @Override
    public void notifyDeleteLabOrder(String str, int status) {
    }

    @Override
    public void notifyDeleteLabResult(String str, int status) {
    }

    @Override
    public void notifyDeleteResultXray(String str, int status) {
    }

    @Override
    public void notifyDeleteXrayPosition(String str, int status) {
    }

    @Override
    public void notifyManagePatientLabReferIn(String str, int status) {
    }

    @Override
    public void notifyReportResultLab(String str, int status) {
    }

    @Override
    public void notifySaveFilmXray(String str, int status) {
    }

    @Override
    public void notifySaveLabResult(String str, int status) {
        doRefreshOrderList();
    }

    @Override
    public void notifySaveRemainLabResult(String str, int status) {
    }

    @Override
    public void notifySaveResultXray(String str, int status) {
        doRefreshOrderList();
    }

    @Override
    public void notifySaveXrayPosition(String str, int status) {
    }

    @Override
    public void notifySendResultLab(String str, int status) {
    }

    @Override
    public void notifyXrayReportComplete(String str, int status) {
        setVisit(theHO.theVisit);
        jButtonRefresh.doClick();
    }

    @Override
    public void notifyDeleteQueueLab(String str, int status) {
    }

    @Override
    public boolean execute(Object str) {
        int[] row = this.tableOrderList.getSelectedRows();
        if (str.toString().startsWith("req")) {
            return theHC.theOrderControl.saveOrderRequest(theOrderItemV, row, str.toString().equals("req1"));
        }

        if (str.toString().startsWith("con")) {
            return theHC.theOrderControl.saveOrderRequest(theOrderItemV, row, str.toString().equals("con1"));
        }

        if (str.toString().startsWith("home")) {
            return theHC.theOrderControl.saveOrderHome(theOrderItemV, row, str.toString().equals("home1"));
        }
        return false;
    }

    public void notifySaveBorrowFilmXray(String str, int status) {
    }

    @Override
    public void notifyAddDxTemplate(String str, int status) {
    }

    @Override
    public void notifyAddPrimarySymptom(String str, int status) {
    }

    @Override
    public void notifyDeleteMapVisitDxByVisitId(String str, int status) {
    }

    @Override
    public void notifyManagePhysicalExam(String str, int status) {
    }

    @Override
    public void notifyManagePrimarySymptom(String str, int status) {
    }

    @Override
    public void notifyManageVitalSign(String str, int status) {
    }

    @Override
    public void notifySaveDiagDoctor(String str, int status) {
        Runnable runnable = () -> {
            //        setVisit(theHO.theVisit);
            jTextAreaNote.setText(theVisit == null ? "" : theVisit.diagnosis_note);
            setItemV(theHC.theVitalControl.listItemByDxTemplate());
            //        doRefreshOrderList();
        };
        runnable.run();
    }

    @Override
    public void notifySaveDxNote(String str, int status) {
        Runnable runnable = () -> {
            jTextAreaNote.setText(theVisit == null ? "" : theVisit.diagnosis_note);
        };
        runnable.run();
    }

    @Override
    public void notifyAdmitLeaveHome(String str, int status) {
    }

    @Override
    public void notifySaveInformIntolerance(Object... object) {
        //
    }

    private void doPrintStickerAction(Collection orderItems) {
        Vector vOr = new Vector();
        for (Object obj : orderItems) {
            OrderItem oi = (OrderItem) obj;
            vOr.add(oi);
        }
        theHC.thePrintControl.printDrugSticker(theHO.theVisit, vOr, getItem(), getOrderItemDrug());
    }

    protected void doSelectedOrderList(java.awt.event.KeyEvent evt) {
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            int row = tableOrderList.getSelectedRow();
            if (row < 0) {
                return;
            }
            OrderItem oi = this.getInfPanelOrder().getOrderItem();
            if (oi.isEditableQty()
                    && !oi.status.equals(OrderStatus.DISPENSE) // �ѧ������
                    && !oi.charge_complete.equals(Active.isEnable())) {// �ѧ���Դ�Թ
                // update qty
                int colQty = 2 + (isShowDose ? 1 : 0);
                oi.qty = String.valueOf(tableOrderList.getValueAt(row, colQty));
                getInfPanelOrder().setOrderItem(oi);
                getInfPanelOrder().saveOrderItem();
            }
        }
        if (evt.getKeyCode() == KeyEvent.VK_SPACE) {
            OrderItem theOrderItem = this.getInfPanelOrder().getOrderItem();
            if (theOrderItem.isXray()
                    && (theOrderItem.status.equals("2") || theOrderItem.status.equals("4"))) {
                theHD.showDialogResultXray(theOrderItem);
                return;
            }
            if (theOrderItem.isLab()) {
                if (theOrderItem.status.equals("2") || theOrderItem.status.equals("4")) {
                    theHD.showDialogShowLabResult(theOrderItem);
                    return;
                }
            }
        }
        if (evt.getKeyCode() == KeyEvent.VK_UP || evt.getKeyCode() == KeyEvent.VK_DOWN) {
            tableItemList.clearSelection();
            int row = tableOrderList.getSelectedRow();
            if (row == -1) {
                return;
            }
            OrderItem theOrderItem = (OrderItem) theOrderItemV.get(row);
            getInfPanelOrder().setOrderItem(theOrderItem);
        }
        GActionAuthV gaav = theHO.theGActionAuthV;
        if (evt.getKeyCode() == KeyEvent.VK_DELETE && gaav.isReadPOrderButtonCancel()) {
            jButtonCancelActionPerformed(null);
        }
        if (evt.getKeyCode() == KeyEvent.VK_V && gaav.isReadPOrderButtonVerify()) {
            int[] row = tableOrderList.getSelectedRows();
            theHC.theOrderControl.verifyOrderItem(theOrderItemV, row);
        }
        if (evt.getKeyCode() == KeyEvent.VK_E && gaav.isReadPOrderButtonExecute()) {
            this.jButtonExecuteActionPerformed(null);
        }
        if (evt.getKeyCode() == KeyEvent.VK_D && gaav.isReadPOrderButtonDispense()) {
            int[] row = tableOrderList.getSelectedRows();
            theHC.theOrderControl.dispenseOrderItem(theOrderItemV, row);
        }
        if (evt.getKeyCode() == KeyEvent.VK_C && gaav.isReadPOrderButtonCalBill()) {
            this.doCalMoney();
        }
    }
    private static final Logger LOG = Logger.getLogger(PanelOrder.class.getName());

    protected void doSelectedItem(java.awt.event.MouseEvent evt) {
        tableOrderList.clearSelection();
        Item item_order = getItem();
        if (item_order == null) {
            return;
        }
        theHO.is_order = false;//amp:27/03/2549
        boolean is_ok = getInfPanelOrder().setItem(item_order);
        OrderItem theOrderItem = getInfPanelOrder().getOrderItem();
        if (!is_ok) {
            return;
        }
        if (theOrderItem.isLab()) {
            if ("1".equals(theOrderItem.secret)) {
                theOrderLabSecret = new OrderLabSecret();
            } else {
                getInfPanelOrder().saveOrderItem();
            }
        } else if (theOrderItem.isXray()) {
            getInfPanelOrder().saveOrderItem();
        } //amp:02/03/2549 ��������Ż���Դ�����
        else {
            doubleClickList(evt);
        }
    }

    protected void doRefreshOrderList() {
        int[] rows = getCurrentRowsOrder();
        theOrderItemV = loadOrderItems();
        setOrderV(theOrderItemV);
        setCurrentRowsOrder(rows);
    }

    protected Vector loadOrderItems() {
        boolean all = !jCheckBoxPeriod.isSelected();
        String df = dateComboBoxStart.getText();
        String dt = dateComboBoxEnd.getText();
        String type = Gutil.getGuiData(jComboBoxGroup1);
        return theHC.theOrderControl.listOrderItemByRange(all, df, dt, type, jCheckBoxShowCancel.isSelected());
    }

    private void doCalMoney() {
        if (jButtonCal.isVisible() && jButtonCal.isEnabled()) {
            try {
                int[] row = tableOrderList.getSelectedRows();
                if (theHO.vVisitPayment == null || theHO.vVisitPayment.isEmpty()) {
                    theUS.setStatus("������ �ѧ������Է�ԡ���ѡ��", UpdateStatus.WARNING);
                    return;
                }
                if (row.length == 0) {
                    theUS.setStatus("�ѧ��������͡��¡��", UpdateStatus.WARNING);
                    return;
                }

                boolean isGovofficalOrSocialSec = theHC.theVisitControl.isGovofficalOrSocialSec(theVisit);
                if (theHO.theVisit.visit_type.equals(VisitType.IPD) && isGovofficalOrSocialSec) {
                    theUS.setStatus("�������Ѻ��ԡ�����Է�Ԣ���Ҫ���/��Сѹ�ѧ����ͧ�кآ����Ż���������Ѻ admit ����Ѻ admit �ҡ", UpdateStatus.WARNING);
                    JOptionPane.showMessageDialog(theUS.getJFrame(),
                            "�������Ѻ��ԡ�����Է�Ԣ���Ҫ���/��Сѹ�ѧ����ͧ�кآ����Ż���������Ѻ admit ����Ѻ admit �ҡ",
                            "����͹",
                            JOptionPane.WARNING_MESSAGE);
                    return;
                }
                /*
                 * ��Ǩ�ͺ��ҤԴ�Թ�����ѧ �ѧ���Դ�Թ ��Ǩ�ͺ�����������ʶҹ�¡��ԡ
                 * ��� ����׹�ѹ
                 */
                //�Դ�Թ���¡�÷�� �ѧ���Դ�Թ ��� �׹�ѹ���� ���͵��Թ������� ������§ҹ������ ���ͨ�������
                //amp:17 �ѹ�Ҥ� 2548
                List<ItemSubgroupCheckCalculate> listItemSubgroupCheckCalculates = theHC.theLookupControl.listItemSubgroupCheckCalculates();
                Vector vorderItem = new Vector();
                for (int i = 0; i < row.length; i++) {
                    OrderItem oi = (OrderItem) theOrderItemV.get(row[i]);
                    if (oi.isChildPackage()) {
                        continue;
                    }
                    if (oi.charge_complete.equals("0")) {
                        // ��Ǩ�ͺ��ͧ���Թ��á�͹�ӹǳ�Թ��
                        for (ItemSubgroupCheckCalculate itemSubgroupCheckCalculate : listItemSubgroupCheckCalculates) {
                            if (itemSubgroupCheckCalculate.b_item_subgroup_id.equals(oi.item_group_code_category)) {
                                if (oi.status.equals(OrderStatus.NOT_VERTIFY)
                                        || oi.status.equals(OrderStatus.VERTIFY)) {
                                    theUS.setStatus("�������ö�ӹǳ�Թ��¡�÷�����͡�� ���ͧ�ҡ�ѧ�����Թ�����¡�� " + itemSubgroupCheckCalculate.item_subgroup_name + " (" + oi.common_name + ")", UpdateStatus.WARNING);
                                    return;
                                }
                            }
                        }
                        if (oi.status.equals(OrderStatus.VERTIFY)
                                || oi.status.equals(OrderStatus.DISPENSE)
                                || oi.status.equals(OrderStatus.REPORT)
                                || oi.status.equals(OrderStatus.EXECUTE)
                                || oi.status.equals(OrderStatus.REMAIN)) {
                            vorderItem.add(oi);
                        }
                    }
                }
                if (vorderItem.isEmpty()) {
                    //pu: 02/08/2549 : ��䢡�÷ӧҹ�ͧ�������͡������
                    if (this.jToggleButtonSelectAll.isSelected()) {
                        this.tableOrderList.selectAll();
                    }
                    String str = "��س����͡��¡�÷�� �����ʶҹ��׹�ѹ ���Թ��� ��§ҹ�� ���� ����ѧ���١�Դ�Թ";
                    theUS.setStatus(str, UpdateStatus.WARNING);
                    return;
                }
                theHD.showDialogCalBilling(vorderItem, jToggleButtonSelectAll.isSelected());
            } catch (Exception ex) {
                LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            }
        }
    }

    @Override
    public void notifyUpVPaymentPriority(String str, int status) {
        doRefreshOrderList();
        theHC.theOrderControl.updateOrderItemPriceByFirstPlan(theHO.theVisit, theHO.vVisitPayment, theHO.vOrderItem);
    }

    @Override
    public void notifyDownVPaymentPriority(String str, int status) {
        doRefreshOrderList();
        theHC.theOrderControl.updateOrderItemPriceByFirstPlan(theHO.theVisit, theHO.vVisitPayment, theHO.vOrderItem);
    }

    @Override
    public void notifySaveVPayment(String str, int status) {
        doRefreshOrderList();
    }

    @Override
    public void notifyDeleteVPayment(String str, int status) {
        doRefreshOrderList();
    }

    protected void doSearchAction() {
        //������¡�� Item ��� keyword �繪�ͧ��ҧ ��� ���Ҩҡ�������¡���ա�õ�꡶١�������
        String keyword = jTextFieldKeyword.getText();
        keyword = Gutil.CheckReservedWords(keyword);
        if (keyword.length() < 1 && !jCheckBoxGroup.isSelected()) {
            theUS.setStatus("�ӷ����㹡�ä�������Թ�", UpdateStatus.WARNING);
            return;
        }
        boolean begin_with = this.jRadioButtonBegin.isSelected();
        String grpId = jCheckBoxGroup.isSelected() ? ComboboxModel.getCodeComboBox(jComboBoxGroup) : "";
        theItemV = theHC.theOrderControl.listItem(grpId, keyword, begin_with);
        setItemV(theItemV);
    }

    private void doPrintStickerTube() {
        theHD.showPanelLabList(this.theHC, this);
    }

    protected void doCancelAction() {
        theHC.theOrderControl.cancelOrderItem(getOrderItemV());
    }

    protected void doDispenseAction() {
        int[] a = tableOrderList.getSelectedRows();
        theHC.theOrderControl.dispenseOrderItem(theOrderItemV, a);
    }

    protected void replacePDOrder(JPanel newPDOrder) {
        for (Component component : jPanel2.getComponents()) {
            if (component instanceof JPanel) {
                if (component.getName() != null && component.getName().equals("pdOrder")) {
                    jPanel2.remove(component);
                    newPDOrder.setName("pdOrder"); // NOI18N
                    GridBagConstraints gridBagConstraints = new java.awt.GridBagConstraints();
                    gridBagConstraints.gridx = 0;
                    gridBagConstraints.gridy = 0;
                    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
                    gridBagConstraints.weightx = 1.0;
                    gridBagConstraints.weighty = 1.0;
                    jPanel2.add(newPDOrder, gridBagConstraints);
                    break;
                }
            }
        }
    }

    protected JComponent getPanelOrder() {
        for (Component component : jPanel2.getComponents()) {
            if (component instanceof JPanel) {
                if (component.getName() != null && component.getName().equals("pdOrder")) {
                    return (JComponent) component;
                }
            }
        }
        return null;
    }

    protected InfPanelOrder getInfPanelOrder() {
        return (InfPanelOrder) getPanelOrder();
    }

    public List<String> getSelectedOrderIds() {
        List<String> selectedOrderIds = new ArrayList<>();
        int[] row = this.tableOrderList.getSelectedRows();
        for (int i = 0; i < row.length; i++) {
            selectedOrderIds.add(((OrderItem) theOrderItemV.get(row[i])).getObjectId());
        }
        return selectedOrderIds;
    }

    @Override
    public void notifyUpdatePatientPictureProfile(Object obj) {

    }

    @Override
    public ItemPrice getItemPrice(OrderItem oi, Payment payment) {
        try {
            return theHC.theOrderControl.intReadItemPriceByItem(oi, payment);
        } catch (Exception ex) {
            return null;
        }
    }

    protected void showDialogOrderSet() {
        theHD.showDialogOrderSet(theHO.theVisit);
    }

    private void handleResized() {
        Container parent = this.getParent();
        if (parent == null) {
            return;
        }
        jSplitPane2.setResizeWeight(0.5);
        int height = (int) parent.getHeight();
        if (height <= 740) {
            jSplitPane2.setDividerLocation(260);
        } else {
            jSplitPane2.setDividerLocation(-1);
        }
    }

    protected void doBeforeVerify() {

    }

    protected void doVerifySelectedItemAction() {
        if (!theHC.theOrderControl.verifyOrderItem(getOrderItemV())) {
            doRefreshOrderList();
        }
    }

    @Override
    public void showDialogStockReceiveDrug() {
    }

    @Override
    public void showDialogOrderSet(Visit theVisit) {
        theHD.showDialogOrderSet(theVisit);
    }

    @Override
    public void showDialogOrderSet(PropertyChangeListener listener) {
        theHD.showDialogOrderSet(listener);
    }

    protected void showDialogDrugFavorite() {
        theHD.showDialogDrugFavorite();
    }
}

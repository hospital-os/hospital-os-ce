/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.provider;

import java.sql.Connection;

/**
 *
 * @author Somprasong
 */
public interface VitalsignProvider {

    public String getServiceName();

    /**
     *
     * @param connection not commit in this method
     * @param hn
     * @param vn
     */
    public int doRefresh(Connection connection, String hn, String vn, String empId);
}

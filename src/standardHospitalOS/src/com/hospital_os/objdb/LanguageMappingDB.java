/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.LanguageMapping;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class LanguageMappingDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "828";

    public LanguageMappingDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public int insert(LanguageMapping obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO b_language_mapping(\n"
                    + "            b_language_mapping_id, b_language_id, f_patient_nation_id)\n"
                    + "    VALUES (?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getGenID(tableId));
            preparedStatement.setString(2, obj.b_language_id);
            preparedStatement.setString(3, obj.f_patient_nation_id);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(LanguageMapping obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM b_language_mapping\n");
            sql.append(" WHERE b_language_mapping_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int deleteByLanguageId(String languageId) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM b_language_mapping\n");
            sql.append(" WHERE b_language_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, languageId);
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public LanguageMapping getByLanguageIdAndNationId(String langId, String nationId) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_language_mapping where b_language_id = ? and f_patient_nation_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, langId);
            preparedStatement.setString(2, nationId);
            List<LanguageMapping> executeQuery = executeQuery(preparedStatement);
            return executeQuery.isEmpty() ? null : executeQuery.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<LanguageMapping> listByLanguageId(String langId) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_language_mapping where b_language_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, langId);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<LanguageMapping> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<LanguageMapping> list = new ArrayList<LanguageMapping>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                LanguageMapping obj = new LanguageMapping();
                obj.setObjectId(rs.getString("b_language_mapping_id"));
                obj.b_language_id = rs.getString("b_language_id");
                obj.f_patient_nation_id = rs.getString("f_patient_nation_id");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<Object[]> listMap(String languageId) throws Exception {
        List<Object[]> list = new ArrayList<Object[]>();
        ResultSet rs = null;
        PreparedStatement preparedStatement = null;
        try {
            // execute SQL stetement
            String sql = "select\n"
                    + "b_language_mapping.b_language_mapping_id as id\n"
                    + ", f_patient_nation.patient_nation_description as desc\n"
                    + "from b_language_mapping\n"
                    + "inner join f_patient_nation on f_patient_nation.f_patient_nation_id = b_language_mapping.f_patient_nation_id\n"
                    + "where b_language_mapping.b_language_id = ? "
                    + "order by f_patient_nation.patient_nation_description";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, languageId);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Object[] obj = new Object[2];
                obj[0] = (rs.getString("id"));
                obj[1] = rs.getString("desc");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
}

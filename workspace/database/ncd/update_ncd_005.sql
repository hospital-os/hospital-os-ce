--LabFU รหัสการตรวจทางห้องปฏิบัติการ เพิ่มคอลัม icd10tm รหัส 7 หลัก
ALTER TABLE b_item_lab_ncd_std ADD COLUMN lab_icd10tm character varying(7) NOT NUlL default '0';

BEGIN; 
UPDATE b_item_lab_ncd_std SET lab_number = '32203' 
                            , item_lab_ncd_std_description = 'Glucose(DTX Fasting)' 
                            , lab_ncd_note = 'Glucose(DTX Fasting)เจาะปลายนิ้วแบบอดอาหาร' 
                            , active = '1'
                            , lab_icd10tm = '0531101'
        WHERE b_item_lab_ncd_std_id = 'ncd201000000000002'; 
INSERT INTO b_item_lab_ncd_std SELECT 'ncd201000000000002','32203','Glucose(DTX Fasting)','Glucose(DTX Fasting)เจาะปลายนิ้วแบบอดอาหาร','1','0531101'
WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std WHERE b_item_lab_ncd_std_id = 'ncd201000000000002'); 
COMMIT;
BEGIN; 
UPDATE b_item_lab_ncd_std SET lab_number = '32203' 
                            , item_lab_ncd_std_description = 'Glucose(DTX NonFasting)' 
                            , lab_ncd_note = 'Glucose(DTX NonFasting)เจาะปลายนิ้วแบบไม่อดอาหาร' 
                            , active = '1'
                            , lab_icd10tm = '0531102'
        WHERE b_item_lab_ncd_std_id = 'ncd201000000000003'; 
INSERT INTO b_item_lab_ncd_std SELECT 'ncd201000000000003','32203','Glucose(DTX NonFasting)','Glucose(DTX NonFasting)เจาะปลายนิ้วแบบไม่อดอาหาร','1','0531102'
WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std WHERE b_item_lab_ncd_std_id = 'ncd201000000000003'); 
COMMIT;
BEGIN; 
UPDATE b_item_lab_ncd_std SET lab_number = '32203' 
                            , item_lab_ncd_std_description = 'Glucose(FBS)' 
                            , lab_ncd_note = 'Glucose(FBS)เจาะเส้นเลือดดำแบบอดอาหาร' 
                            , active = '1'
                            , lab_icd10tm = '0531002'
        WHERE b_item_lab_ncd_std_id = 'ncd201000000000004'; 
INSERT INTO b_item_lab_ncd_std SELECT 'ncd201000000000004','32203','Glucose(FBS)','Glucose(FBS)เจาะเส้นเลือดดำแบบอดอาหาร','1','0531002'
WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std WHERE b_item_lab_ncd_std_id = 'ncd201000000000004'); 
COMMIT;
BEGIN; 
UPDATE b_item_lab_ncd_std SET lab_number = '32203' 
                            , item_lab_ncd_std_description = 'Glucose(BS)' 
                            , lab_ncd_note = 'Glucose(BS)เจาะเส้นเลือดดำแบบไม่อดอาหาร' 
                            , active = '1'
                            , lab_icd10tm = '0531004'
        WHERE b_item_lab_ncd_std_id = 'ncd201000000000005'; 
INSERT INTO b_item_lab_ncd_std SELECT 'ncd201000000000005','32203','Glucose(BS)','Glucose(BS)เจาะเส้นเลือดดำแบบไม่อดอาหาร','1','0531004'
WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std WHERE b_item_lab_ncd_std_id = 'ncd201000000000005'); 
COMMIT;
BEGIN; 
UPDATE b_item_lab_ncd_std SET lab_number = '32401' 
                            , item_lab_ncd_std_description = 'Hb A1C' 
                            , lab_ncd_note = 'ฮีโมโกบิน A1C' 
                            , active = '1'
                            , lab_icd10tm = '0531601'
        WHERE b_item_lab_ncd_std_id = 'ncd201000000000006'; 
INSERT INTO b_item_lab_ncd_std SELECT 'ncd201000000000006','32401','Hb A1C','ฮีโมโกบิน A1C','1','0531601'
WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std WHERE b_item_lab_ncd_std_id = 'ncd201000000000006'); 
COMMIT;
BEGIN; 
UPDATE b_item_lab_ncd_std SET lab_number = '32501' 
                            , item_lab_ncd_std_description = 'Lipid  - Cholesterol' 
                            , lab_ncd_note = '' 
                            , active = '1'
                            , lab_icd10tm = '0541601'
        WHERE b_item_lab_ncd_std_id = 'ncd201000000000007'; 
INSERT INTO b_item_lab_ncd_std SELECT 'ncd201000000000007','32501','Lipid  - Cholesterol','','1','0541601'
WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std WHERE b_item_lab_ncd_std_id = 'ncd201000000000007'); 
COMMIT;
BEGIN; 
UPDATE b_item_lab_ncd_std SET lab_number = '32502' 
                            , item_lab_ncd_std_description = 'Lipid - TG (Triglyceride)' 
                            , lab_ncd_note = '' 
                            , active = '1'
                            , lab_icd10tm = '0546602'
        WHERE b_item_lab_ncd_std_id = 'ncd201000000000008'; 
INSERT INTO b_item_lab_ncd_std SELECT 'ncd201000000000008','32502','Lipid - TG (Triglyceride)','','1','0546602'
WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std WHERE b_item_lab_ncd_std_id = 'ncd201000000000008'); 
COMMIT;
BEGIN; 
UPDATE b_item_lab_ncd_std SET lab_number = '32503' 
                            , item_lab_ncd_std_description = 'Lipid - HDL-chol' 
                            , lab_ncd_note = '' 
                            , active = '1'
                            , lab_icd10tm = '0541202'
        WHERE b_item_lab_ncd_std_id = 'ncd201000000000009'; 
INSERT INTO b_item_lab_ncd_std SELECT 'ncd201000000000009','32503','Lipid - HDL-chol','','1','0541202'
WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std WHERE b_item_lab_ncd_std_id = 'ncd201000000000009'); 
COMMIT;
BEGIN; 
UPDATE b_item_lab_ncd_std SET lab_number = '32504' 
                            , item_lab_ncd_std_description = 'Lipid - LDL-chol (direct) สั่งรายการเดียว' 
                            , lab_ncd_note = '' 
                            , active = '1'
                            , lab_icd10tm = '0541402'
        WHERE b_item_lab_ncd_std_id = 'ncd201000000000010'; 
INSERT INTO b_item_lab_ncd_std SELECT 'ncd201000000000010','32504','Lipid - LDL-chol (direct) สั่งรายการเดียว','','1','0541402'
WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std WHERE b_item_lab_ncd_std_id = 'ncd201000000000010'); 
COMMIT;
BEGIN; 
UPDATE b_item_lab_ncd_std SET lab_number = '32201' 
                            , item_lab_ncd_std_description = 'BUN (Blood Urea Nitrogen)' 
                            , lab_ncd_note = '' 
                            , active = '1'
                            , lab_icd10tm = '0583001'
        WHERE b_item_lab_ncd_std_id = 'ncd201000000000011'; 
INSERT INTO b_item_lab_ncd_std SELECT 'ncd201000000000011','32201','BUN (Blood Urea Nitrogen)','','1','0583001'
WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std WHERE b_item_lab_ncd_std_id = 'ncd201000000000011'); 
COMMIT;
BEGIN; 
UPDATE b_item_lab_ncd_std SET lab_number = '32202' 
                            , item_lab_ncd_std_description = 'Creatinine (ในเลือด)' 
                            , lab_ncd_note = '' 
                            , active = '1'
                            , lab_icd10tm = '0581902'
        WHERE b_item_lab_ncd_std_id = 'ncd201000000000012'; 
INSERT INTO b_item_lab_ncd_std SELECT 'ncd201000000000012','32202','Creatinine (ในเลือด)','','1','0581902'
WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std WHERE b_item_lab_ncd_std_id = 'ncd201000000000012'); 
COMMIT;
BEGIN; 
UPDATE b_item_lab_ncd_std SET lab_number = '' 
                            , item_lab_ncd_std_description = 'Macroalbumin (ในปัสสาวะ)' 
                            , lab_ncd_note = '' 
                            , active = '1'
                            , lab_icd10tm = '0446203'
        WHERE b_item_lab_ncd_std_id = 'ncd201000000000013'; 
INSERT INTO b_item_lab_ncd_std SELECT 'ncd201000000000013','','Macroalbumin (ในปัสสาวะ)','','1','0446203'
WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std WHERE b_item_lab_ncd_std_id = 'ncd201000000000013'); 
COMMIT;
BEGIN; 
UPDATE b_item_lab_ncd_std SET lab_number = '' 
                            , item_lab_ncd_std_description = 'eGFR (ในปัสสาวะ)' 
                            , lab_ncd_note = '' 
                            , active = '1'
                            , lab_icd10tm = '0581904'
        WHERE b_item_lab_ncd_std_id = 'ncd201000000000014'; 
INSERT INTO b_item_lab_ncd_std SELECT 'ncd201000000000014','','eGFR (ในปัสสาวะ)','','1','0581904'
WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std WHERE b_item_lab_ncd_std_id = 'ncd201000000000014'); 
COMMIT;
BEGIN; 
UPDATE b_item_lab_ncd_std SET lab_number = '' 
                            , item_lab_ncd_std_description = 'Hb' 
                            , lab_ncd_note = '' 
                            , active = '1'
                            , lab_icd10tm = '0621401'
        WHERE b_item_lab_ncd_std_id = 'ncd201000000000015'; 
INSERT INTO b_item_lab_ncd_std SELECT 'ncd201000000000015','','Hb','','1','0621401'
WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std WHERE b_item_lab_ncd_std_id = 'ncd201000000000015'); 
COMMIT;
BEGIN; 
UPDATE b_item_lab_ncd_std SET lab_number = '32103' 
                            , item_lab_ncd_std_description = 'Potassium (K)' 
                            , lab_ncd_note = '' 
                            , active = '1'
                            , lab_icd10tm = '0511402'
        WHERE b_item_lab_ncd_std_id = 'ncd201000000000016'; 
INSERT INTO b_item_lab_ncd_std SELECT 'ncd201000000000016','32103','Potassium (K)','','1','0511402'
WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std WHERE b_item_lab_ncd_std_id = 'ncd201000000000016'); 
COMMIT;
BEGIN; 
UPDATE b_item_lab_ncd_std SET lab_number = '' 
                            , item_lab_ncd_std_description = 'UPCR (Urine protein creatinine ratio)' 
                            , lab_ncd_note = '' 
                            , active = '1'
                            , lab_icd10tm = '0440205'
        WHERE b_item_lab_ncd_std_id = 'ncd201000000000017'; 
INSERT INTO b_item_lab_ncd_std SELECT 'ncd201000000000017','','UPCR (Urine protein creatinine ratio)','','1','0440205'
WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std WHERE b_item_lab_ncd_std_id = 'ncd201000000000017'); 
COMMIT;
BEGIN; 
UPDATE b_item_lab_ncd_std SET lab_number = '' 
                            , item_lab_ncd_std_description = 'Bicarb' 
                            , lab_ncd_note = '' 
                            , active = '1'
                            , lab_icd10tm = '0510402'
        WHERE b_item_lab_ncd_std_id = 'ncd201000000000018'; 
INSERT INTO b_item_lab_ncd_std SELECT 'ncd201000000000018','','Bicarb','','1','0510402'
WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std WHERE b_item_lab_ncd_std_id = 'ncd201000000000018'); 
COMMIT;
BEGIN; 
UPDATE b_item_lab_ncd_std SET lab_number = '' 
                            , item_lab_ncd_std_description = 'Phosphate' 
                            , lab_ncd_note = '' 
                            , active = '1'
                            , lab_icd10tm = '0511202'
        WHERE b_item_lab_ncd_std_id = 'ncd201000000000019'; 
INSERT INTO b_item_lab_ncd_std SELECT 'ncd201000000000019','','Phosphate','','1','0511202'
WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std WHERE b_item_lab_ncd_std_id = 'ncd201000000000019'); 
COMMIT;
BEGIN; 
UPDATE b_item_lab_ncd_std SET lab_number = '' 
                            , item_lab_ncd_std_description = 'PTH' 
                            , lab_ncd_note = '' 
                            , active = '1'
                            , lab_icd10tm = '0614402'
        WHERE b_item_lab_ncd_std_id = 'ncd201000000000020'; 
INSERT INTO b_item_lab_ncd_std SELECT 'ncd201000000000020','','PTH','','1','0614402'
WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std WHERE b_item_lab_ncd_std_id = 'ncd201000000000020'); 
COMMIT;
BEGIN; 
UPDATE b_item_lab_ncd_std SET lab_number = '31004' 
                            , item_lab_ncd_std_description = 'Microalbumin (ในปัสสาวะ)' 
                            , lab_ncd_note = '' 
                            , active = '1'
                            , lab_icd10tm = '0440204'
        WHERE b_item_lab_ncd_std_id = 'ncd201000000000021'; 
INSERT INTO b_item_lab_ncd_std SELECT 'ncd201000000000021','31004','Microalbumin (ในปัสสาวะ)','','1','0440204'
WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std WHERE b_item_lab_ncd_std_id = 'ncd201000000000021'); 
COMMIT;
BEGIN; 
UPDATE b_item_lab_ncd_std SET lab_number = '31001' 
                            , item_lab_ncd_std_description = 'Creatinine (ในปัสสาวะ)' 
                            , lab_ncd_note = '' 
                            , active = '1'
                            , lab_icd10tm = '0581903'
        WHERE b_item_lab_ncd_std_id = 'ncd201000000000028'; 
INSERT INTO b_item_lab_ncd_std SELECT 'ncd201000000000028','31001','Creatinine (ในปัสสาวะ)','','1','0581903'
WHERE NOT EXISTS (SELECT 1 FROM b_item_lab_ncd_std WHERE b_item_lab_ncd_std_id = 'ncd201000000000028'); 
COMMIT;

INSERT INTO s_ncd_version VALUES ('9760000000005', '5', 'NCD Module', '1.2.1', '1.2.1', (select current_date) || ','|| (select current_time));

INSERT INTO s_script_update_log values ('NCD_Module','update_ncd_005.sql',(select current_date) || ','|| (select current_time),'Update DB NCD Module');
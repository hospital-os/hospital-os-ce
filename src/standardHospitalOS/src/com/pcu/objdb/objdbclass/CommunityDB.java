/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.objdb.objdbclass;

import com.hospital_os.usecase.connection.ConnectionInf;
import com.pcu.object.Community;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class CommunityDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "809";

    public CommunityDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public int insert(Community obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO t_health_community(\n"
                    + "            t_health_community_id, health_community_id, health_community_name, \n"
                    + "            health_community_register, health_community_soi, health_community_road, \n"
                    + "            t_health_village_id, health_community_person_id, health_volunteer_person_id, \n"
                    + "            health_volunteer, health_community_detail, active, user_record_id, \n"
                    + "            record_date_time, user_modify_id, modify_date_time)\n"
                    + "    VALUES (?, ?, ?, \n"
                    + "            ?, ?, ?, \n"
                    + "            ?, ?, ?, \n"
                    + "            ?, ?, ?, ?, \n"
                    + "            ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getGenID(tableId));
            preparedStatement.setString(2, obj.health_community_id);
            preparedStatement.setString(3, obj.health_community_name);
            preparedStatement.setString(4, obj.health_community_register);
            preparedStatement.setString(5, obj.health_community_soi);
            preparedStatement.setString(6, obj.health_community_road);
            preparedStatement.setString(7, obj.t_health_village_id);
            preparedStatement.setString(8, obj.health_community_person_id);
            preparedStatement.setString(9, obj.health_volunteer_person_id);
            preparedStatement.setString(10, obj.health_volunteer);
            preparedStatement.setString(11, obj.health_community_detail);
            preparedStatement.setString(12, obj.active);
            preparedStatement.setString(13, obj.user_record_id);
            preparedStatement.setString(14, obj.record_date_time);
            preparedStatement.setString(15, obj.user_modify_id);
            preparedStatement.setString(16, obj.modify_date_time);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(Community obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_health_community\n"
                    + "   SET health_community_id=?, health_community_name=?, \n"
                    + "       health_community_register=?, health_community_soi=?, health_community_road=?, \n"
                    + "       t_health_village_id=?, health_community_person_id=?, health_volunteer_person_id=?, \n"
                    + "       health_volunteer=?, health_community_detail=?, user_modify_id=?, modify_date_time=?\n");
            sql.append(" WHERE t_health_community_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.health_community_id);
            preparedStatement.setString(2, obj.health_community_name);
            preparedStatement.setString(3, obj.health_community_register);
            preparedStatement.setString(4, obj.health_community_soi);
            preparedStatement.setString(5, obj.health_community_road);
            preparedStatement.setString(6, obj.t_health_village_id);
            preparedStatement.setString(7, obj.health_community_person_id);
            preparedStatement.setString(8, obj.health_volunteer_person_id);
            preparedStatement.setString(9, obj.health_volunteer);
            preparedStatement.setString(10, obj.health_community_detail);
            preparedStatement.setString(11, obj.user_modify_id);
            preparedStatement.setString(12, obj.modify_date_time);
            preparedStatement.setString(13, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int inactive(Community obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_health_community\n");
            sql.append("   SET active=?, user_cancel_id=?, cancel_date_time=?\n");
            sql.append(" WHERE t_health_community_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.active);
            preparedStatement.setString(2, obj.user_cancel_id);
            preparedStatement.setString(3, obj.cancel_date_time);
            preparedStatement.setString(4, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public Community select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from t_health_community where t_health_community_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<Community> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<Community> selectByKeyword(String keyword) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from t_health_community where active = '1' and (health_community_id like ? or health_community_name like ?) order by health_community_id";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, "%" + keyword + "%");
            preparedStatement.setString(2, "%" + keyword + "%");
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<Community> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<Community> list = new ArrayList<Community>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Community obj = new Community();
                obj.setObjectId(rs.getString("t_health_community_id"));
                obj.health_community_id = rs.getString("health_community_id");
                obj.health_community_name = rs.getString("health_community_name");
                obj.health_community_register = rs.getString("health_community_register");
                obj.health_community_soi = rs.getString("health_community_soi");
                obj.health_community_road = rs.getString("health_community_road");
                obj.t_health_village_id = rs.getString("t_health_village_id");
                obj.health_community_person_id = rs.getString("health_community_person_id");
                obj.health_volunteer_person_id = rs.getString("health_volunteer_person_id");
                obj.health_volunteer = rs.getString("health_volunteer");
                obj.health_community_detail = rs.getString("health_community_detail");
                obj.active = rs.getString("active");
                obj.user_record_id = rs.getString("user_record_id");
                obj.record_date_time = rs.getString("record_date_time");
                obj.user_modify_id = rs.getString("user_modify_id");
                obj.modify_date_time = rs.getString("modify_date_time");
                obj.user_cancel_id = rs.getString("user_cancel_id");
                obj.cancel_date_time = rs.getString("cancel_date_time");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }
    public String selectMaxCommuId(String code8) throws Exception {
        String ret = "";
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try {
            String sql = "select max(to_number(health_community_id,'9999999999'))  from t_health_community where substr(health_community_id,0,9) = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, code8);
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                ret = rs.getString(1);
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
        return ret;
    }
    
}
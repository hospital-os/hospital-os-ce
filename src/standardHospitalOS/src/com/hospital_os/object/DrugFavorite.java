/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
public class DrugFavorite extends Persistent {

    public String doctor_id = "";
    public String b_visit_clinic_id = "";
    public String b_item_id = "";
    public String usage_special = "0";
    public String usage_text = "";
    public String caution = "";
    public String description = "";
    public String instruction_id = "";
    public String dose = "";
    public String use_uom_id = "";
    public String frequency_id = "";
    public String qty = "";
    public String purch_uom_id = "";

    // for module stock
    public String b_hstock_item_id;

    // object only
    public String itemName = "";
}

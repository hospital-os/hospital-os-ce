/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.FinanceInsurancePlan;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class FinanceInsurancePlanDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "815";

    public FinanceInsurancePlanDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public int insert(FinanceInsurancePlan obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO b_finance_insurance_plan(\n"
                    + "            b_finance_insurance_plan_id, code, description, b_finance_insurance_company_id, active, user_record_id, record_datetime, user_update_id, update_datetime)\n"
                    + "    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getGenID(tableId));
            preparedStatement.setString(2, obj.code);
            preparedStatement.setString(3, obj.description);
            preparedStatement.setString(4, obj.b_finance_insurance_company_id);
            preparedStatement.setString(5, obj.active);
            preparedStatement.setString(6, obj.user_record_id);
            preparedStatement.setString(7, obj.record_datetime);
            preparedStatement.setString(8, obj.user_update_id);
            preparedStatement.setString(9, obj.update_datetime);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(FinanceInsurancePlan obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE b_finance_insurance_plan\n");
            sql.append("   SET code=?, description=?, \n");
            sql.append("   b_finance_insurance_company_id=?, \n");
            sql.append("       active=?, user_update_id=?, update_datetime=?\n");
            sql.append(" WHERE b_finance_insurance_plan_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.code);
            preparedStatement.setString(2, obj.description);
            preparedStatement.setString(3, obj.b_finance_insurance_company_id);
            preparedStatement.setString(4, obj.active);
            preparedStatement.setString(5, obj.user_update_id);
            preparedStatement.setString(6, obj.update_datetime);
            preparedStatement.setString(7, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

//    public int delete(FinanceInsurancePlan obj) throws Exception {
//        PreparedStatement preparedStatement = null;
//        try {
//            StringBuilder sql = new StringBuilder();
//            sql.append("DELETE FROM b_finance_insurance_plan\n");
//            sql.append(" WHERE b_finance_insurance_plan_id = ?");
//            preparedStatement = connectionInf.ePQuery(sql.toString());
//            preparedStatement.setString(1, obj.getObjectId());
//            // execute update SQL stetement
//            return preparedStatement.executeUpdate();
//
//        } finally {
//            if (preparedStatement != null) {
//                preparedStatement.close();
//            }
//        }
//    }
//
    public int inactive(FinanceInsurancePlan obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE b_finance_insurance_plan\n");
            sql.append("   SET active=?, user_update_id=?, update_datetime=?\n");
            sql.append(" WHERE b_finance_insurance_plan_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, "0");
            preparedStatement.setString(2, obj.user_update_id);
            preparedStatement.setString(3, obj.update_datetime);
            preparedStatement.setString(4, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public FinanceInsurancePlan select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_finance_insurance_plan where b_finance_insurance_plan_id = ? order by description";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<FinanceInsurancePlan> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public FinanceInsurancePlan selectByCode(String code) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_finance_insurance_plan where code = ? order by description";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, code);
            List<FinanceInsurancePlan> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<FinanceInsurancePlan> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_finance_insurance_plan where active = '1' order by description";
            preparedStatement = connectionInf.ePQuery(sql);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<FinanceInsurancePlan> listByCompanyId(String companyId) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_finance_insurance_plan where active = ? and b_finance_insurance_company_id = ? order by description";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, "1");
            preparedStatement.setString(2, companyId);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<FinanceInsurancePlan> listByKeyword(String keyword, String active) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_finance_insurance_plan where active = ? and (upper(code) like upper(?) or upper(description) like upper(?)) order by description";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, active);
            preparedStatement.setString(2, "%" + (keyword == null || keyword.isEmpty() ? "" : keyword) + "%");
            preparedStatement.setString(3, "%" + (keyword == null || keyword.isEmpty() ? "" : keyword) + "%");
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<FinanceInsurancePlan> listByKeywordCompanyAndPlan(String keyword, String active) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select b_finance_insurance_plan.*, b_finance_insurance_company.company_name from b_finance_insurance_plan "
                    + "inner join b_finance_insurance_company on b_finance_insurance_company.b_finance_insurance_company_id = b_finance_insurance_plan.b_finance_insurance_company_id "
                    + "where b_finance_insurance_plan.active = ? and (upper(b_finance_insurance_company.company_name) like upper(?) or upper(b_finance_insurance_plan.description) like upper(?)) "
                    + "order by b_finance_insurance_company.company_name, b_finance_insurance_plan.description";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, active);
            preparedStatement.setString(2, "%" + (keyword == null || keyword.isEmpty() ? "" : keyword) + "%");
            preparedStatement.setString(3, "%" + (keyword == null || keyword.isEmpty() ? "" : keyword) + "%");
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<FinanceInsurancePlan> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<FinanceInsurancePlan> list = new ArrayList<FinanceInsurancePlan>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                FinanceInsurancePlan obj = new FinanceInsurancePlan();
                obj.setObjectId(rs.getString("b_finance_insurance_plan_id"));
                obj.code = rs.getString("code");
                obj.description = rs.getString("description");
                obj.b_finance_insurance_company_id = rs.getString("b_finance_insurance_company_id");
                obj.active = rs.getString("active");
                obj.user_record_id = rs.getString("user_record_id");
                obj.record_datetime = rs.getString("record_datetime");
                obj.user_update_id = rs.getString("user_update_id");
                obj.update_datetime = rs.getString("update_datetime");
                try {
                    obj.companyName = rs.getString("company_name");
                } catch (Exception ex) {
                }
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (FinanceInsurancePlan obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }
}

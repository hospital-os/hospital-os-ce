/*
 * LookupControl.java
 *
 * Created on 10 ���Ҥ� 2546, 14:08 �.
 */
package com.hosv3.control;

import com.hospital_os.object.*;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.usecase.connection.ModuleInfTool;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hospital_os.utility.ComboFix;
import com.hospital_os.utility.Constant;
import com.hosv3.object.*;
import com.hosv3.subject.HosSubject;
import com.hosv3.utility.DateTime;
import com.hosv3.utility.DateUtil;
import com.hosv3.utility.ResourceBundle;
import com.pcu.object.Disease;
import com.pcu.object.Home;
import com.pcu.object.Village;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.logging.Logger;

/**
 *
 * @author tong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class LookupControl {

    private static final Logger LOG = Logger.getLogger(BillingControl.class.getName());
    public ConnectionInf theConnectionInf;
    private final HosDB theHosDB;
    private final HosObject theHO;
    public LookupObject theLO;
    private UpdateStatus theUS;
    private final ComboFix theTextCurrentTime = new ComboFix();
    private String employeename;
    private HosControl theHC;

    public LookupControl(ConnectionInf con, HosObject ho, HosDB hdb, HosSubject hs, LookupObject lo) {
        theConnectionInf = con;
        theHosDB = hdb;
        theHO = ho;
        theLO = lo;
        theLO.theSite = readSite();
        theHO.initSite();
    }

    public void setHosControl(HosControl theHC) {
        this.theHC = theHC;
    }
    //henbe ������ 15/3/2549

    public void setUpdateStatus(UpdateStatus us) {
        theUS = us;
        refreshLookup();
    }

    public SequenceData getSequenceDataVN() {
        return theLO.theSqdVN;

    }

    public SequenceData getSequenceDataHN() {
        return theLO.theSqdHN;
    }

    public boolean isDateFuture(String date_in) {
        if (date_in.length() < 10) {
            return false;
        }
        date_in = date_in.substring(0, 10);
        if (date_in.length() > 11) {
            date_in += "," + theHO.date_time.substring(11);
            return DateUtil.isDateTimeFuture(date_in, theHO.date_time);
        } else {
            return DateUtil.isDateTimeFuture(date_in, theHO.date_time.substring(0, 10));
        }
    }
    //����Դ connection �����¹����пѧ�ѹ��������Դ connection ���

    public boolean isDateTimeFuture(String date_in) throws Exception {
        return DateUtil.isDateTimeFuture(date_in, intReadDateTime());
    }

    public String getNormalTextVN(String vn) {
        try {
            Long.parseLong(vn);
            if (vn.length() > 6) {
                return vn;
            }
        } catch (NumberFormatException ex) {
        }
        int slash = vn.indexOf('/');
        if (slash != -1) {
            String before = vn.substring(0, slash + 1);
            String after = vn.substring(slash + 1);
            if (after.startsWith("1")) {
                if (theLO.theSqdAN.active.equals("1")) {
                    return SequenceData.getDBText(theLO.theSqdAN.pattern, before + after.substring(1), theHO.date_time);
                } else {
                    return SequenceData.getDBText("1yy000000", before + after.substring(1), theHO.date_time);
                }
            }

        }
        if (theLO.theSqdVN.active.equals("1")) {
            return SequenceData.getDBText(theLO.theSqdVN.pattern, vn, theHO.date_time);
        } else {
            return SequenceData.getDBText("0yy000000", vn, theHO.date_time);
        }
    }

    public String getRenderTextVN(String vn) {
        if (theLO.theOption.unused_pattern.equals("1")) {
            return vn;
        }
        if (theLO.theSqdVN.active.equals("1")) {
            return SequenceData.getGuiText(theLO.theSqdVN.pattern, vn);
        } else {
            try {
                String year = vn.substring(1, 3);
                String number = vn.substring(3);
                return Integer.parseInt(number) + "/" + year;
            } catch (NumberFormatException ex) {
                return "";
            }
        }
    }

    public String getNormalTextHN(String hn) {
        try {
            if (theLO.theSqdHN.active.equals("1")) {
                String hn_pat = theLO.theSqdHN.pattern;
                if (hn_pat.contains("yy")) {
                    return SequenceData.getDBText(hn_pat, hn, theHO.date_time);
                }
            }
            if (!theLO.theOption.used_fixdigit_hn.equals("1")) {
                while (hn.length() < 6) {
                    hn = "0" + hn;
                }
                return "%" + hn;
            }
            Long.parseLong(hn);
            if (hn.length() > 6) {
                return hn;
            }
        } catch (NumberFormatException ex) {
        }
        return SequenceData.getDBText(theLO.theSqdHN.pattern, hn, theHO.date_time);
    }

    public String getRenderTextHN(String hn) {
        if (theLO.theOption.unused_pattern.equals("1")) {
            return hn;
        }
        if (theLO.theSqdHN.active.equals("1")) {
            return SequenceData.getGuiText(theLO.theSqdHN.pattern, hn);
        } else {
            try {
                return Integer.parseInt(hn) + "";
            } catch (NumberFormatException ex) {
                return "";
            }
        }
    }

    public Vector listAuthentication(String key) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theAuthenticationDB.selectByDescription(key);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    private List<MapWarningInformIntolerance> mapWarningInformIntolerances = null;

    public List<MapWarningInformIntolerance> listMapWarningInformIntolerance(boolean isReload) {
        if (!isReload && mapWarningInformIntolerances != null) {
            return mapWarningInformIntolerances;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            mapWarningInformIntolerances = theHosDB.theMapWarningInformIntoleranceDB.listMapped(null);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return mapWarningInformIntolerances;
    }

    private List<MapWarningDrugAllergy> mapWarningDrugAllergys = null;

    public List<MapWarningDrugAllergy> listMapWarningDrugAllergy(boolean isReload) {
        if (!isReload && mapWarningDrugAllergys != null) {
            return mapWarningDrugAllergys;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            mapWarningDrugAllergys = theHosDB.theMapWarningDrugAllergyDB.listMapped(null);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return mapWarningDrugAllergys;
    }

    private void updateCurrentDateTime() {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            intReadDateTime();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
    }

    public String intReadDateTime() throws Exception {
        return intReadDateTime(theConnectionInf);
    }

    public String intReadDateTime(ConnectionInf c2) throws Exception {
        java.sql.ResultSet rs = c2.eQuery("select CURRENT_TIME");
        if (rs.next()) {
            theTextCurrentTime.name = rs.getString(1);
        }
        theTextCurrentTime.name = DateTime.convertThaiTime(theTextCurrentTime.name);
        rs = c2.eQuery("select CURRENT_DATE");
        if (rs.next()) {
            theTextCurrentTime.code = rs.getString(1);
        }
        theTextCurrentTime.code = DateTime.convertThaiDate(theTextCurrentTime.code);
        theHO.date_time = theTextCurrentTime.code + "," + theTextCurrentTime.name;
        return theHO.date_time;
    }

    public String intReadDateTimeWithPattern(String pattern, Locale locale) throws Exception {
        return intReadDateTimeWithPattern(theConnectionInf, pattern, locale);
    }

    public String intReadDateTimeWithPattern(ConnectionInf c2, String pattern, Locale locale) throws Exception {
        java.sql.ResultSet rs = c2.eQuery("select CURRENT_TIMESTAMP");
        if (rs.next()) {
            java.sql.Timestamp timestamp = rs.getTimestamp(1);
            SimpleDateFormat sdf = new SimpleDateFormat(pattern, locale);
            return sdf.format(timestamp);
        } else {
            return null;
        }
    }

    public Date intReadTimeStamp() throws Exception {
        return intReadTimeStamp(theConnectionInf);
    }

    public Date intReadTimeStamp(ConnectionInf c2) throws Exception {
        Date date = null;
        java.sql.ResultSet rs = c2.eQuery("select CURRENT_TIMESTAMP");
        while (rs.next()) {
            date = rs.getTimestamp(1);
        }
        return date;
    }

    public Option readOption() {
        return readOption(false);
    }

    public Option readOption(boolean read_again) {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            intReadOption(read_again);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theOption;
    }

    public Option intReadOption() throws Exception {
        return intReadOption(false);
    }

    public Option intReadOption(boolean read_again) throws Exception {
        intReadSite(read_again);
        if (theLO.theOption == null) {
            theLO.theOption = theHosDB.theOptionDB.select();
        }
        return theLO.theOption;
    }

    public List<Printing> readOptionReport() {
        return readOptionReport(false);
    }

    public List<Printing> readOptionReport(boolean read_again) {
        this.readSite(read_again);
        if (theLO.printings == null) {
            theLO.printings = new ArrayList<Printing>();
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.printings.addAll(theHosDB.thePrintingDB.list());
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.printings;
    }

    public AutoReportBug readAutoReportBug() {
        AutoReportBug arb = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theAutoReportBug = theHosDB.theAutoReportBugDB.selectAll();
            arb = theLO.theAutoReportBug;
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return arb;
    }
    String address_code = "";
    Vector address_vector = new Vector();

    public Vector listCatAddressByCode(String code) {
        Vector vc = null;
        if (address_code.equals(code)) {
            return address_vector;
        }
        if (code.length() < 6) {
            return new Vector();
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            address_vector.removeAllElements();
            theLO.sChangwat = code.substring(0, 2) + "0000";
            address_vector.add(theHosDB.theFAddressDB.selectAmpurByCN("", theLO.sChangwat));
            theLO.sAmphur = code.substring(0, 4) + "00";
            address_vector.add(theHosDB.theFAddressDB.selectTambonByCN("", theLO.sAmphur));
            vc = address_vector;
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
            address_code = code;
        }
        return vc;
    }

    public FAddress2 readAddressById(String id) {
        FAddress2 a = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            a = theHosDB.theFAddressDB.selectByPK2(id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return a;
    }

    protected String intReadAddressString(String id) throws Exception {
        return intReadAddressString(null, id);
    }

    public String readAddressString(String id) {
        return readAddressString(null, id);
    }

    public String readAddressString(String topic, String id) {
        if (id == null || id.isEmpty()) {
            return "";
        }

        FAddress add = readAddressById(id);
        String ret = "";
        if (add != null) {
            ret = add.description;
            if (topic != null) {
                ret = (topic) + add.description;
            }
        }
        return ret;
    }

    protected String intReadAddressString(String topic, String id) throws Exception {
        FAddress add = theHosDB.theFAddressDB.selectByPK2(id);
        String ret = "";
        if (add != null) {
            ret = add.description;
            if (topic != null) {
                ret = (topic) + add.description;
            }
        }
        return ret;
    }

    /**
     * @param id
     * @return
     * @author henbe
     * @not deprecated can not read inactive record
     */
    public String readReligionString(String id) {
        Religion naTion = readReligionById(id);
        if (naTion != null) {
            return naTion.description;
        }
        return "";
    }

    /**
     * @param id
     * @return
     * @not deprecated henbe this function cannot read inactive record
     *
     */
    public String readOccupationString(String id) {
        Occupat naTion = readOccupatById(id);
        if (naTion != null) {
            return naTion.description;
        }
        return "";
    }

    public Occupation2 readOccupatById(String id) {
        return readOccupatById(id, false);
    }

    /**
     * @param id
     * @return
     * @author henbe
     * @not deprecared can not read inactive record
     */
    public String readNationString(String id) {
        Nation naTion = readNationById(id);
        if (naTion != null) {
            return naTion.description;
        }
        return "";
    }

    public Prefix readPrefixByName(String name) {
        Prefix pf = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            pf = theHosDB.thePrefixDB.selectByName(name);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        theConnectionInf.close();
        return pf;
    }

    public Prefix readPrefixByEqualName(String name) {
        Prefix pf = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            pf = theHosDB.thePrefixDB.selectByEqualName(name);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        theConnectionInf.close();
        return pf;
    }

    public Relation readRelationByName(String name) {
        Relation r = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            r = theHosDB.theRelationDB.selectByName(name);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return r;
    }

    /**
     * @param id
     * @return
     * @not deprecated henbe this function cannot read inactive record
     */
    public Relation2 readRelationById(String id) {
        return readRelationById(id, false);
    }

    protected String intReadPlanString(String id) throws Exception {
        String sPlan = "";
        Plan pf = theHosDB.thePlanDB.selectByPK(id);
        if (pf != null) {
            sPlan = pf.description;
        }
        return sPlan;
    }

    public String readPlanString(String id) {
        String sPlan = "";
        Plan pf = readPlanById(id);
        if (pf != null) {
            sPlan = pf.description;
        }
        return sPlan;
    }

    public String readPrefixString(String id) {
        String sPrefix = "";
        Prefix2 pf = readPrefixById(id);
        if (pf != null) {
            sPrefix = pf.description;
        }
        return sPrefix;
    }

    /**
     * @param id
     * @return
     * @not deprecated henbe this function cannot read inactive record
     */
    public Prefix2 readPrefixById(String id) {
        return readPrefixById(id, false);
    }

    /**
     * @param id
     * @return
     * @not deprecated henbe this function cannot read inactive record
     */
    public DrugInstruction2 readDrugInstructionById(String id) {
        return readDrugInstructionById(id, false);
    }

    /**
     * @author henbe
     * @not deprecated can not read inactive record
     */
    /**
     * @param id
     * @return
     * @not deprecated henbe this function cannot read inactive record
     */
    public String readUomString(String id) {
        Uom uom = readUomById(id, false);
        if (uom == null) {
            return "";
        } else {
            return uom.description;
        }
    }

    public Uom2 readUomById(String id) {
        return readUomById(id, false);
    }

    /**
     * @param id
     * @return
     * @not deprecated henbe this function cannot read inactive record
     */
    public DrugFrequency2 readDrugFrequencyById(String id) {
        return readDrugFrequencyById(id, false);
    }

    /**
     * ������¡�� Dose ��ͨҡ���� ������������´
     *
     * @param code �� String ����� primary key �ͧ Dose ��� ����ͧ��ä���
     * @return Object �ͧ Dose ��ͷ������ͷ�����
     * @Author pu
     * @date 07/08/2006
     */
    public DrugDoseShortcut readDrugDoseShortcutByCode(String code) {
        if ((code == null)) {
            return null;
        }
        if ((code.length() == 0)) {
            return null;
        }
        DrugDoseShortcut vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theDrugDoseShortcutDB.selectByPK(code);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * ������¡�� Dose ��ͨҡ���� ������������´
     *
     * @param description �� String �������������´�ͧ Dose ���
     * ����ͧ��ä���
     * @return Vector ����� Object �ͧ Dose ��ͷ������ͷ�����
     * @Author pu
     * @date 07/08/2006
     */
    public Vector listDrugDoseShortcutByName(String description) {
        if (description.trim().length() == 0) {
            return this.listDrugDoseShortcut();
        }
        Vector v = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            v = theHosDB.theDrugDoseShortcutDB.selectAllByPK(description, Active.isEnable());
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return v;
    }

    /**
     * ������¡�� Dose ���
     *
     * @return Vector ����� Object �ͧ Dose ��ͷ������ͷ�����
     *
     * @Author pu
     * @date 07/08/2006
     */
    public Vector listDrugDoseShortcut() {
        if (theLO.vDrugDoseShortcut != null && !theLO.vDrugDoseShortcut.isEmpty()) {
            return theLO.vDrugDoseShortcut;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.vDrugDoseShortcut = theHosDB.theDrugDoseShortcutDB.selectAllByPK("", Active.isEnable());
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.vDrugDoseShortcut;
    }

    /**
     * ������¡�� Item �ҡ���� ������������´
     *
     * @param description �� String �������������´�ͧ ��¡�� Item
     * ����ͧ��ä���
     * @return Vector ����� Object �ͧ��¡�� Item �������ͷ�����
     * @Author pu
     * @date 08/08/2006
     */
    public Vector listItemByName(String description) {
        Vector v = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            v = theHosDB.theItemDB.selectAllByPK(description, Active.isEnable());

            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return v;
    }

    public String readContractString(String id) {
        Contract plan = readContractById(id, false);
        if (plan != null) {
            return plan.description;
        }
        return ResourceBundle.getBundleGlobal("TEXT.NO.DATA");
    }

    public Contract readContractById(String id) {
        return readContractById(id, false);
    }

    /**
     * @return ��ª��ͷ���繨ѧ��Ѵ �� Vector �ͧ FAddress �Ѵ�͡�������
     * 12/11/47
     */
    public Vector listAddressCGW() {
        return listChangwat("");
    }
    Vector vXrayLeteral;

    public Vector listAllXrayLeteral() {
        if (vXrayLeteral != null && !vXrayLeteral.isEmpty()) {
            return vXrayLeteral;
        }
        vXrayLeteral = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vXrayLeteral = theHosDB.theXRayLeteralDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vXrayLeteral;
    }
    /**
     * not
     *
     * @deprecated ��ͧ�� list ���ǵ�����ª��� Object ���ǵ�ͧ�տѧ�ѹ read
     * �ӡѺ���� �� listXrayPosition() readXrayPositionById(String str)
     */
    Vector vXrayPosition;

    public Vector listAllXrayPosition() {

        if (vXrayPosition != null && !vXrayPosition.isEmpty()) {
            return vXrayPosition;
        }
        vXrayPosition = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vXrayPosition = theHosDB.theXRayPositionDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vXrayPosition;
    }

    /**
     * Function list ��ª��ͷ��������ͷ����� �� Vector �ͧ Address
     * �Ѵ�͡������� 12/11/47 �ѧ������������ҹ
     */
    /**
     * Function list ��ª��ͷ���繵Ӻŷ����� �� Vector �ͧ Address
     * �Ѵ�͡������� 12/11/47 �ѧ������������ҹ
     */
    /**
     *
     * @param keyword
     * @return ��ª��ͷ��������ͷ�����������ʨѧ��Ѵ �� Vector �ͧ FAddress
     * �Ѵ�͡������� 12/11/47 ����ǹ��� ��� �� theVectorBuffer
     * ���ѹ�Ф����������͹���� ��������Դ connection ������ա���� sql
     * �����ѧ�ҹ������ ����龺����ա�� open close connection ������� sql ��
     *
     */
    public Vector listAddressAmp(String keyword) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theFAddressDB.selectAmphur(keyword);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc == null ? new Vector() : vc;
    }

    /**
     *
     * @param ch
     * @param amp
     * @return ��ª��ͷ���繵Ӻŷ�����������ʨѧ��Ѵ������������ �� Vector
     * �ͧ FAddress �Ѵ�͡�����������´ UC ���� 12/11/47
     *
     */
    public Vector listAddressTmp(String ch, String amp) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theFAddressDB.selectTambon(amp);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * @return �������ѡ�ͧ����ʴ�������� �¡�����§ �����͸Ժ��
     * �Ѵ�͡������� 12/11/47
     */
    public Vector listBillingGroup() {
        if ((theLO.theBillingGroup != null)) {
            return theLO.theBillingGroup;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theBillingGroup = theHosDB.theBillingGroupDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theBillingGroup;
    }

    /**
     * @param authen_id
     * @return �Է����к���������� ��͸Ժ�� �Ѵ�͡������� 12/11/47
     */
    public Vector listGActionAuthByAid(String authen_id) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theGActionAuthDB.selectByAid(authen_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * @return �Է����к���������� ��͸Ժ�� �Ѵ�͡������� 12/11/47
     */
    public Vector listAuthentication() {
        if ((theLO.theAuthentication != null)) {
            return theLO.theAuthentication;
        }
        theLO.theAuthentication = listAuthentication("");
        return theLO.theAuthentication;
    }

    public Authentication readAuthenticationById(String id, boolean re_read) {
        Vector vp = listAuthentication();
        return (Authentication) readCommonInf(vp, id);
    }

    /**
     * @return ʶҹС�ùѴ ��͸Ժ�� �Ѵ�͡������� 12/11/47
     */
    public Vector listAppointmentStatus() {
        if ((theLO.theAppointmentStatus != null)) {
            return theLO.theAppointmentStatus;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theAppointmentStatus = theHosDB.theAppointmentStatusDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theAppointmentStatus;
    }

    /**
     * @return ��ª����дѺ�ͧ�����ҹ �Ѵ�͡������� 12/11/47
     */
    public Vector listLevel() {
        if ((theLO.theLevel != null)) {
            return theLO.theLevel;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theLevel = theHosDB.theLevelDB.selectAllByName();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theLevel;
    }

    /*
     * @return ��ª��ͨش��ԡ�÷���ѧ active ���� pu
     */
    public Vector listServicePoint() {
        if (theLO.vServicePoint == null) {
            theLO.vServicePoint = listServicePoint("");
        }
        return theLO.vServicePoint;
    }

    /*
     * @return ��ª��ͨش��ԡ�÷���ѧ active ���� pu
     */
    public Vector listServicePoint(String str) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theServicePointDB.selectAllByName(str, Active.isEnable());
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listServicePointIPD(String keyword) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theServicePointDB.selectAllIPDByName(keyword, Active.isEnable());
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * Function list ��ª��ͨش��ԡ�÷������ͧ�ç��Һ�� ¡��鹨ش��ԡ�÷����
     * lab ��� xray �Ѵ�͡������� 12/11/47
     *
     * @return
     */
    public Vector listServicePointwithOutXL() {
        if ((theLO.theServicePointWithOutXL != null)) {
            return theLO.theServicePointWithOutXL;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theServicePointWithOutXL = theHosDB.theServicePointDB.selectAllWithOutXL();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theServicePointWithOutXL;
    }

    /**
     *
     * @return ��ª��ͪ�Դ�ͧ�ش��ԡ������ �Ѵ�͡������� 12/11/47
     */
    public Vector listServiceSubType() {
        if ((theLO.theServiceSubType != null)) {
            return theLO.theServiceSubType;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theServiceSubType = theHosDB.theServiceSubTypeDB.selectAllByName();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theServiceSubType;
    }

    /**
     *
     * @return ��ª��ͪ�Դ�ͧ�ش��ԡ�� �Ѵ�͡������� 12/11/47
     */
    public Vector listServiceType() {
        if ((theLO.theServiceType != null)) {
            return theLO.theServiceType;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theServiceType = theHosDB.theServiceTypeDB.selectAllByName();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theServiceType;
    }

    /**
     * @return @Author : sumo
     * @date : 05/06/2549
     * @see : list ��������¡�á�����ҵðҹ���ʴ�� combo
     * @return : Object �ͧ��������¡�á�����ҵðҹ���ç�Ѻ������
     */
    public Vector listItem16Group() {
        if ((theLO.vItem16Group != null)) {
            return theLO.vItem16Group;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.vItem16Group = theHosDB.theItem16GroupDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.vItem16Group;
    }

    /**
     *
     * list ��ª��͡�����˭�ͧ����� order �Ѵ�͡������� 12/11/47
     *
     * @return
     */
    public Vector listCategoryGroup() {
        if ((theLO.theCategoryGroup != null)) {
            return theLO.theCategoryGroup;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theCategoryGroup = theHosDB.theCategoryGroupDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theCategoryGroup;
    }

    /**
     *
     * list ��ª��ͧ͢����� order �Ѵ�͡������� 12/11/47
     *
     * @return
     */
    public Vector listCategoryGroupItem() {
        return theLO.theCategoryGroupItem;
    }

    public Vector listItemLabType() {
        return theLO.theItemLabType;
    }

    /**
     *
     * list ��ª��ͧ͢����� order ����繡�����ͧ �� ��� �Ǫ�ѳ�� �Ѵ�͡�������
     * 12/11/47
     *
     * @return
     */
    public Vector listCategoryGroupItemDrugAndSupply() {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theCategoryGroupItemDB.selectDrugAndSupply();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public String getSChangwat() {
        return theLO.sChangwat;
    }

    public String getSAmphur() {
        return theLO.sAmphur;
    }

    /**
     *
     * list ��ª��ͧ͢ ����������(�����ʴ�������) �Ѵ�͡������� 12/11/47
     *
     * @return
     */
    public Vector listBillingGroupItem() {
        return theLO.theBillingGroupItem;
    }

    /**
     *
     * list ��¡�âͧ�������㹡������ �Ѵ�͡������� 12/11/47
     *
     * @return
     */
    public Vector listDrugFrequency() {
        return listDrugFrequency(true);
    }

    public Vector intListDrugFrequency(boolean active) throws Exception {
        theLO.theDrugFrequencyAll = theHosDB.theDrugFrequencyDB.selectAll();
        theLO.theDrugFrequency = new Vector();
        for (int i = 0; i < theLO.theDrugFrequencyAll.size(); i++) {
            DrugFrequency uom = (DrugFrequency) theLO.theDrugFrequencyAll.get(i);
            if (uom.active.equals("1")) {
                theLO.theDrugFrequency.add(uom);
            }
        }
        if (active) {
            return theLO.theDrugFrequency;
        } else {
            return theLO.theDrugFrequencyAll;
        }
    }

    public Vector listDrugFrequency(boolean active) {
        if ((theLO.theDrugFrequency != null && !theLO.theDrugFrequency.isEmpty() && active)) {
            return theLO.theDrugFrequency;
        } else if ((theLO.theDrugFrequencyAll != null && !active)) {
            return theLO.theDrugFrequencyAll;
        }

        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = intListDrugFrequency(active);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     *
     * list ��¡�âͧ���й�㹡������ �Ѵ�͡������� 12/11/47
     *
     * @return
     */
    public Vector listDrugInstruction() {
        return listDrugInstruction(true);
    }

    public Vector intListDrugInstruction(boolean active) throws Exception {
        theLO.theDrugInstructionAll = theHosDB.theDrugInstructionDB.selectAll();
        theLO.theDrugInstruction = theHosDB.theDrugInstructionDB.selectByCNA("", "1");
        if (active) {
            return theLO.theDrugInstruction;
        } else {
            return theLO.theDrugInstructionAll;
        }
    }

    public Vector listDrugInstruction(boolean active) {
        if (theLO.theDrugInstruction != null && active && !theLO.theDrugInstruction.isEmpty()) {
            return theLO.theDrugInstruction;
        } else if (theLO.theDrugInstructionAll != null && !active && !theLO.theDrugInstruction.isEmpty()) {
            return theLO.theDrugInstructionAll;
        }

        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = intListDrugInstruction(active);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listDrugInstruction(String keyword) {
        if (keyword == null || keyword.isEmpty() || keyword.equals("%")) {
            return listDrugInstruction();
        }
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theDrugInstructionDB.selectByCN(keyword);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listDrugFrequency(String key) {
        if (key == null || key.isEmpty() || key.equals("%")) {
            return listDrugFrequency();
        } else {
            Vector vc = null;
            theConnectionInf.open();
            try {
                theConnectionInf.getConnection().setAutoCommit(false);
                vc = theHosDB.theDrugFrequencyDB.selectByCN(key);
                theConnectionInf.getConnection().commit();
            } catch (Exception ex) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                try {
                    theConnectionInf.getConnection().rollback();
                } catch (SQLException ex1) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex1);
                }
            } finally {
                theConnectionInf.close();
            }
            return vc;
        }
    }

    /**
     * list ��¡�âͧ˹����� �Ѵ�͡������� 12/11/47
     *
     * @return
     */
    public Vector listUom() {
        return listUom(true);
    }

    public Vector intListUom(boolean active) throws Exception {
        theLO.theUomAll = theHosDB.theUomDB.selectAll();
        theLO.theUom = new Vector();
        for (int i = 0; i < theLO.theUomAll.size(); i++) {
            Uom uom = (Uom) theLO.theUomAll.get(i);
            if (uom.active.equals("1")) {
                theLO.theUom.add(uom);
            }
        }
        if (active) {
            return theLO.theUom;
        } else {
            return theLO.theUomAll;
        }
    }

    public Vector listUom(boolean active) {
        if (theLO.theUom != null && !theLO.theUom.isEmpty() && active) {
            return theLO.theUom;
        } else if (theLO.theUomAll != null && !theLO.theUomAll.isEmpty() && !active) {
            return theLO.theUomAll;
        }
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = intListUom(active);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * list ��¡�âͧ˹����� �Ѵ�͡������� 12/11/47
     *
     * @param keyword
     * @return
     */
    public Vector listUom(String keyword) {
        if (keyword == null || keyword.isEmpty() || keyword.equals("%")) {
            return listUom(true);
        }
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theUomDB.selectByDes(keyword);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listBlood() {
        if ((theLO.theBlood != null)) {
            return theLO.theBlood;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theBlood = theHosDB.theBloodGroupDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theBlood;

    }

    public Vector listFstatus() {
        if ((theLO.theFstatus != null)) {
            return theLO.theFstatus;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theFstatus = theHosDB.theFStatusDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theFstatus;

    }

    public Vector listLabor() {
        if ((theLO.theLabor != null)) {
            return theLO.theLabor;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theLabor = theHosDB.theLaborDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theLabor;
    }

    public Vector listMarriage() {
        return theLO.theMarriage;
    }

    public Vector listNation() {
        return this.copyVector(theLO.theNation);
    }

    public Vector listNation(String str) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theNationDB.selectByCN(str);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listOccupation() {
        return this.copyVector(theLO.theOccupation);
    }

    public Vector listOccupationByCodeName(String str) {

        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theOccupatDB.selectByCN(str);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listPrefix() {
        return copyVector(theLO.thePrefix);
    }

    public Vector listPrefix(String str) {

        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.thePrefixDB.selectByCN(str);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * ��㹡�ä鹢����Ţͧ�ѧ��Ѵ
     *
     * @return Vector of Changwat
     * @author Pongtorn (Henbe)
     * @date 04/04/49,14:57
     */
    public Vector listChangwat() {
        if (theLO.theChangwat != null) {
            return theLO.theChangwat;
        }
        theLO.theChangwat = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theChangwat = theHosDB.theFAddressDB.selectChangwat();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theChangwat;
    }

    public Vector listChangwat(String str) {
        if (str.isEmpty() || str.equals("%")) {
            return listChangwat();
        }
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theFAddressDB.selectChangwatByCN(str);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * ��㹡�ä鹢����Ţͧ�����
     *
     * @param str ����Ѻ���� ������ʨѧ��Ѵ
     * @param changwat
     * @return Vector of Tambon in Amphur
     * @author Pongtorn (Henbe)
     * @date 04/04/49,14:57
     */
    public Vector listAmpur(String str, String changwat) {
        if (changwat == null) {
            return null;
        }
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theFAddressDB.selectAmpurByCN(str, changwat);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theLO.sChangwat = changwat;
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * ��㹡�ä鹢����Ţͧ�����
     *
     * @param str ����Ѻ���� ������ʨѧ��Ѵ
     * @param changwat
     * @return Vector of Tambon in Amphur
     * @author Pongtorn (Henbe)
     * @date 04/04/49,14:57
     */
    public Vector listAmpurSuit(String str, String changwat) {
        if (changwat == null) {
            return null;
        }
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = new Vector();
            theLO.sChangwat = changwat;
            Vector amp = theHosDB.theFAddressDB.selectAmpurByCN(str, theLO.sChangwat);
            Vector tamb = new Vector();
            vc.add(amp);
            if (!amp.isEmpty()) {
                FAddress amp_addr = (FAddress) amp.get(0);
                theLO.sAmphur = amp_addr.getObjectId();
                tamb = theHosDB.theFAddressDB.selectTambonByCN(str, theLO.sAmphur);
            }
            vc.add(tamb);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theLO.sChangwat = changwat;
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * ��㹡�ä鹢����Ţͧ�Ӻ�
     *
     * @param str ����Ѻ���� ������������
     * @param amphur
     * @return Vector of Tambon in Amphur
     * @author Pongtorn (Henbe)
     * @date 04/04/49,14:57
     */
    public Vector listTambon(String str, String amphur) {
        if (amphur == null) {
            return null;
        }
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theFAddressDB.selectTambonByCN(str, amphur);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theLO.sAmphur = amphur;
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * @return @roseuid 3F83DB0D0119
     */
    public Vector listRace() {
        if ((theLO.theRace != null)) {
            return theLO.theRace;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theRace = theHosDB.theNationDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theRace;
    }

    /**
     * @return @roseuid 3F83DB0D0119
     */
    public Vector listRelation() {
        return this.copyVector(theLO.theRelation);
    }

    public Vector listRelation(String str) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theRelationDB.selectByName1(str);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * @return @roseuid 3F83DB0D0119
     */
    public Vector listReligion() {
        return theLO.theReligion;
    }

    /**
     * @return @roseuid 3F83DB0D0119
     */
    public Vector listLabResultGroup() {

        Vector v = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            v = theHosDB.theLabResultGroupDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return v;
    }

    /**
     * Vector Of ComboFix
     *
     * @return
     */
    public Vector listGender() {
        return theLO.theGender;
    }

    /**
     * @return @roseuid 3F83DB0D0119
     */
    public Vector listTypeArea() {
        if ((theLO.theTypeArea != null)) {
            return theLO.theTypeArea;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theTypeArea = theHosDB.theTypeAreaDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theTypeArea;
    }

    public Vector listPlan() {
        return listPlan("");
    }

    public Vector listPlan(String str) {
        if (str == null || str.trim().isEmpty() || str.trim().equals("%")) {
            return copyVector(theLO.thePlanActive);
        }
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.thePlanDB.selectByCNA(str, "1");
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listContractAdjustByContractId(String cid) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theContractAdjustDB.selectByContractId(cid);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public String readEmployeeNameById(String id) {
        Employee employee = null;
        if (id != null && !id.isEmpty()) {
            employee = readEmployeeById(id);
        }
        return employee != null ? employee.getName() : "";
    }

    /**
     * @author henbe
     * @not deprecated can not read inactive record
     */
    /**
     * ���Ǩ�ͺ�����ż����͹ login
     *
     * @param code
     * @return
     */
    public Employee readEmployeeByUsername(String code) {
        Employee employee = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            employee = theHosDB.theEmployeeDB.selectByUsername(code);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return employee;
    }

    public Vector listOfficeInCup(String key) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theOfficeInCupDB.selectByCN(key);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listWelfareInCup(String key) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theWelfareInCupDB.selectByCN(key);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public OfficeInCup readOfficeInCupByCode(String code) {
        OfficeInCup vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theOfficeInCupDB.selectByCode(code);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public OfficeInCup readWelfareInCupByCode(String code) {
        OfficeInCup vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theWelfareInCupDB.selectByCode(code);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public String readHospitalSByCode(String code) {
        Office off = readHospitalByCode(code);
        if (off != null) {
            return off.name;
        }
        return "��辺������";
    }

    public String readHospitalString(String code) {
        Office office = readHospitalByCode(code);
        if (office == null) {
            return "";
        } else {
            return office.name;
        }
    }

    public String intReadHospitalString(String code) throws Exception {
        Office office = theHosDB.theOfficeDB.selectByPK(code);
        if (office == null) {
            return "";
        } else {
            return office.name;
        }
    }

    public Office readHospitalByCode(String code) {
        if ((code == null)) {
            return null;
        }
        if ((code.length() == 0)) {
            return null;
        }
        if (code.equals(theHO.theSite.off_id)) {
            return theLO.theOffice;
        }

        Office vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theOfficeDB.selectByPK(code);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listClinic() {
        if (theLO.theClinic != null && !theLO.theClinic.isEmpty()) {
            return theLO.theClinic;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theClinic = theHosDB.theClinicDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theClinic;
    }

    public Vector listClinic(String name) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theClinicDB.selectAllByName(name, "1");
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector intListDrugDosePrintByUom(String uom_id) throws Exception {
        String sql = "select b_item_drug_dose.* from b_item_drug_dose"
                + " inner join b_item_drug_dose_map_uom "
                + " on b_item_drug_dose.b_item_drug_dose_id = b_item_drug_dose_map_uom.b_item_drug_dose_id"
                + " where b_item_drug_uom_id = '" + uom_id + "'";
        return theHosDB.theDrugDosePrint2DB.eQuery(sql);
    }

    public Vector listDrugDosePrintByUom(String uom_id) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = intListDrugDosePrintByUom(uom_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listDrugDosePrint() {
        return theLO.theDrugDosePrint;
    }

    /*
     * Henbe Pattern
     */
    public Vector listDrugDosePrint(String str, String active) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theDrugDosePrintDB.selectByKeyWord(str, active);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /*
     * Henbe Pattern
     */
    public Vector listGAction() {
        if ((theLO.theGAction != null)) {
            return theLO.theGAction;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theGAction = theHosDB.theGActionDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theGAction;
    }

    /*
     * Author amp
     */
    public Vector listNutritionTypeOld() {
        Vector vNutritionTypeOld = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vNutritionTypeOld = theHosDB.theNutritionTypeDB.selectOld();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vNutritionTypeOld;
    }

    /*
     * Author neung
     */
    public Vector listNutritionType() {
        if ((theLO.vNutritionType != null)) {
            return theLO.vNutritionType;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.vNutritionType = theHosDB.theNutritionTypeDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.vNutritionType;
    }

    /*
     * Henbe Pattern
     */
    public Vector listDischarge() {
        if ((theLO.theAccuseType != null)) {
            return theLO.theAccuseType;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theAccuseType = theHosDB.theAccuseTypeDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theAccuseType;
    }

    /*
     * Henbe Pattern
     */
    public Vector listBGroup() {
        return null;
    }

    /*
     * Henbe Pattern
     */
    public Vector listWaterType() {

        if ((theLO.thewatertype != null)) {
            return theLO.thewatertype;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.thewatertype = theHosDB.theWaterTypeDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.thewatertype;

    }

    /*
     * Henbe Pattern
     */
    public Vector listGarbage() {
        if ((theLO.theGarbage != null)) {
            return theLO.theGarbage;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theGarbage = theHosDB.theGarbageDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theGarbage;
    }

    /*
     * Henbe Pattern
     */
    public Vector listHCharac() {
        if ((theLO.theHCharac != null)) {
            return theLO.theHCharac;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theHCharac = theHosDB.theHCharacDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theHCharac;
    }

    /*
     * Henbe Pattern
     */
    public Vector listComCharac() {
        if ((theLO.theComCharac != null)) {
            return theLO.theComCharac;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theComCharac = theHosDB.theComCharacDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theComCharac;
    }

    /*
     * Author amp
     */
    public Vector listPtStatusType() {
        if ((theLO.thePtStatusType != null)) {
            return theLO.thePtStatusType;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.thePtStatusType = theHosDB.thePtStatusTypeDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.thePtStatusType;
    }

    /*
     * Author amp
     */
    public Vector listPtMobile() {
        if ((theLO.thePtmobieType != null)) {
            return theLO.thePtmobieType;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.thePtmobieType = theHosDB.thePtmobieTypeDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.thePtmobieType;
    }

    /*
     * Author amp
     */
    public Vector listAccuseType() {
        if ((theLO.theAccuseType != null)) {
            return theLO.theAccuseType;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theAccuseType = theHosDB.theAccuseTypeDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theAccuseType;
    }

    /*
     * Author amp
     */
    public Vector listAccproType() {
        if ((theLO.theAccproType != null)) {
            return theLO.theAccproType;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theAccproType = theHosDB.theAccproTypeDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theAccproType;
    }

    /*
     * Author amp
     */
    public Vector listAccinoutType() {
        if ((theLO.theInOutrdType != null)) {
            return theLO.theInOutrdType;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theInOutrdType = theHosDB.theInOutrdTypeDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theInOutrdType;
    }

    /*
     * Author amp
     */
    public Vector listAccrdType() {
        if ((theLO.theAccrdType != null)) {
            return theLO.theAccrdType;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theAccrdType = theHosDB.theAccrdTypeDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theAccrdType;
    }

    /*
     * Henbe Pattern
     */
    public Vector listTypeDish() {
        if ((theLO.theTypeDish != null)) {
            return theLO.theTypeDish;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theTypeDish = theHosDB.theTypeDishDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theTypeDish;
    }

    /*
     * Henbe Pattern
     */
    public Vector listFpType() {
        if ((theLO.theFpType != null)) {
            return theLO.theFpType;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theFpType = theHosDB.theFpTypeDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theFpType;
    }

    /*
     * Henbe Pattern
     */
    public Vector listNofp() {
        if ((theLO.theNofp != null)) {
            return theLO.theNofp;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theNofp = theHosDB.theNofpDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theNofp;
    }

    /*
     * Henbe Pattern
     */
    public Vector listResultStatus() {
        if ((theLO.theResultStatus != null)) {
            return theLO.theResultStatus;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theResultStatus = theHosDB.theResultStatusDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theResultStatus;
    }

    /*
     * Henbe Pattern
     */
    public Vector listBType() {
        if ((theLO.theBType != null)) {
            return theLO.theBType;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theBType = theHosDB.theBTypeDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theBType;
    }

    /*
     * Henbe Pattern
     */
    public Vector listBDoctor() {
        if ((theLO.theBdoctor != null)) {
            return theLO.theBdoctor;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theBdoctor = theHosDB.theBdoctorDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theBdoctor;
    }

    /*
     * Henbe Pattern
     */
    public Vector listHighRisk() {
        if ((theLO.theHighRisk != null)) {
            return theLO.theHighRisk;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theHighRisk = theHosDB.theHighRiskDB.selectAll();

            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theHighRisk;
    }

    /*
     * Henbe Pattern
     */
    public Vector listDayTimeType() {
        return null;
    }

    /*
     * Henbe Pattern
     */
    public Vector listRule() {
        return null;
    }

    public Vector listBEmployeeByAuthen(String[] authenIds) {
        return listBEmployeeByAuthen("", authenIds);
    }

    public Vector listBEmployeeByAuthen(String name, String[] authenIds) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theEmployeeDB.selectAuthenAllByName(name, "1", authenIds);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listDoctor(String keyword) {
        return listBEmployeeByAuthen(keyword, new String[]{Authentication.DOCTOR});
    }

    public Vector listDoctor() {
        if (theLO.theEmployee_loc != null) {
            return theLO.theEmployee_loc;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theEmployee_loc = theHosDB.theEmployeeDB.selectDoctor(Authentication.DOCTOR);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theEmployee_loc;
    }

    public List<CommonInf> listDoctorDiag() {
        if (theLO.allDiagDoctors != null && !theLO.allDiagDoctors.isEmpty()) {
            return theLO.allDiagDoctors;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.allDiagDoctors = new ArrayList<CommonInf>();
            theLO.allDiagDoctors.addAll(theHosDB.theEmployeeDB.selectDoctor(new String[]{
                Authentication.DOCTOR,
                Authentication.THAI_DOCTOR,
                Authentication.PHYSICAL_THERAPY}));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.allDiagDoctors;
    }

    public Vector listDoctorDiag(String keyword) {
        return listBEmployeeByAuthen(keyword,
                new String[]{Authentication.DOCTOR,
                    Authentication.THAI_DOCTOR,
                    Authentication.PHYSICAL_THERAPY});
    }

    public Vector listDoctorAppointment(Date date, String keyword) {
        boolean isModuleDentalActive = false;
        for (ModuleInfTool mi : theHC.getModules()) {
            String moduleName = mi.getClass().getName();
            if (moduleName.equals("com.dental.DentalModule")) {
                isModuleDentalActive = true;
                break;
            }
        }

        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String[] authen;
            if (theHO.theEmployee.authentication_id.equals(Authentication.NURSE)) {
                authen = new String[]{
                    Authentication.DOCTOR,
                    Authentication.THAI_DOCTOR};
                vc = intSelectDoctorAppointment(date, keyword, isModuleDentalActive, false, authen);
            } else if (theHO.theEmployee.authentication_id.equals(Authentication.DENT)) {
                authen = new String[]{
                    Authentication.DOCTOR};
                vc = intSelectDoctorAppointment(date, keyword, false, true, authen);
            } else {
                authen = new String[]{
                    Authentication.DOCTOR,
                    Authentication.THAI_DOCTOR};
                vc = intSelectDoctorAppointment(date, keyword, authen);
            }

            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector<Employee> intSelectDoctorAppointment(Date date, String keyword, String... ids) throws Exception {
        return intSelectDoctorAppointment(date, keyword, false, false, ids);
    }

    public Vector<Employee> intSelectDoctorAppointment(Date date, String keyword, boolean excludeDent, boolean isDentOnly, String... ids) throws Exception {
        String authens = "";
        for (String id : ids) {
            authens += authens.isEmpty() ? "'" + id + "'" : ",'" + id + "'";
        }
        StringBuilder sql = new StringBuilder();
        sql.append("select distinct(employee.*) from (select public.b_employee.*\n"
                + ", public.t_person.person_firstname\n"
                + ", public.t_person.person_lastname\n"
                + ", case when public.f_patient_prefix.f_patient_prefix_id is not null then case when public.f_patient_prefix.f_patient_prefix_id = '000' then '' else public.f_patient_prefix.patient_prefix_description end else '' end as prefix\n"
                + "from public.b_employee\n"
                + "inner join public.t_person on public.t_person.t_person_id = public.b_employee.t_person_id\n"
                + "left join public.f_patient_prefix on public.f_patient_prefix.f_patient_prefix_id = public.t_person.f_prefix_id\n");
        //***************** ����Ǩ�ͺ�ҡ��õ�駤���������ҧ���ᾷ���������
        boolean isUseDoctorSchedule = readOption().enable_schedule_doctor.equals("1");
        if (isUseDoctorSchedule && date != null) {
            sql.append("inner join sch.sch_schedule_doctor on sch.sch_schedule_doctor.doctor = public.b_employee.b_employee_id \n");
        }
        if (isDentOnly) {
            sql.append("inner join public.b_dental_employee on public.b_dental_employee.b_employee_id = public.b_employee.b_employee_id \n");
        }
        sql.append("where \n"
                + "public.b_employee.f_employee_authentication_id");
        sql.append(" in (");
        sql.append(authens);
        sql.append(") \n");
        sql.append("and public.b_employee.employee_active = '1'\n");
        if (isUseDoctorSchedule && date != null) {
            sql.append("and sch.sch_schedule_doctor.date = ?\n");
        }
        if (keyword != null && !keyword.isEmpty()) {
            sql.append("and (public.t_person.person_firstname ilike ? or public.t_person.person_lastname ilike ?)");
        }
        if (excludeDent) {
            sql.append("and public.b_employee.b_employee_id not in (select public.b_dental_employee.b_employee_id from public.b_dental_employee)\n");
        }
        sql.append("order by public.t_person.person_firstname, public.t_person.person_lastname) as employee order by employee.person_firstname, employee.person_lastname");
        PreparedStatement ePQuery = theConnectionInf.ePQuery(sql.toString());
        int index = 1;
        if (isUseDoctorSchedule && date != null) {
            ePQuery.setDate(index++, new java.sql.Date(date.getTime()));
        }
        if (keyword != null && !keyword.isEmpty()) {
            if (keyword.trim().contains(" ")) {
                ePQuery.setString(index++, "%" + keyword.substring(0, keyword.indexOf(" ")).trim() + "%");
                ePQuery.setString(index++, "%" + keyword.substring(keyword.indexOf(" ") + 1).trim() + "%");
            } else {
                ePQuery.setString(index++, "%" + keyword + "%");
                ePQuery.setString(index++, "%" + keyword + "%");
            }
        }
        return theHosDB.theEmployeeDB.eQuery(ePQuery.toString());
    }

    public Vector listLab() {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theEmployeeDB.selectNurse(Authentication.LAB);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listDrugSetOwner() {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theEmployeeDB.selectDrugSetOwner();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /*
     * fn
     */
    public Vector listDoctorSP(String spId) {
        return listEmployeeBySpid(spId);
    }

    public Vector listDoctorSPAndName(String spId, String name) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theLookupDB.selectBySpAndDoctorName(spId, name);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listEmployeeBySpid(String spId) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theLookupDB.selectBySpid(spId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listNurse() {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theEmployeeDB.selectNurse(Authentication.NURSE);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * ��㹡���觪��ͼ�������ͼ�黯Ժѵԧҹ�¡���Ѻ�����Ţͧ ������ѡ�ͧ���ҧ
     * b_employee
     *
     * @param employee_id �� String �ͧ ������ѡ�ͧ���ҧ b_employee
     * @return �� String �繪���-ʡ�Ţͧ��������ͼ�黯Ժѵ�
     * @author padungrat(tong)
     * @date 25/03/49,14:57 �����鹨ҡ vector �ͧ employee
     * ���֧����ҵ͹�Դ�����������������ͧ令���ըҡ�ҹ������������
     * Pongtorn(Henbe)
     */
    public String getEmployeeName(String employee_id) {
        employeename = ResourceBundle.getBundleGlobal("TEXT.N.A");
        Employee emp = this.readEmployeeById(employee_id); //henbe
        //tong emp = theHosDB.theEmployeeDB.getEmployeeName(employee_id, null);
        if (emp != null) {
            employeename = emp.person.person_firstname + " " + emp.person.person_lastname;
        }
        return employeename;
    }

    /**
     * ��㹡���觪���Clinic �¡���Ѻ�����Ţͧ ������ѡ�ͧ���ҧ b_clinic
     *
     * @param clinic_id �� String �ͧ ������ѡ�ͧ���ҧ b_clinic
     * @return �� String �繪��� clinic
     * @author padungrat(tong)
     * @date 19/04/49,11:05
     */
    public String getClinicName(String clinic_id) {
        employeename = ResourceBundle.getBundleGlobal("TEXT.N.A");
        Clinic clinic = this.readClinicById(clinic_id);
        if (clinic != null) {
            employeename = clinic.name;
        }
        return employeename;
    }

    public String getWardName(String wardId) {
        String wardName = ResourceBundle.getBundleGlobal("TEXT.N.A");
        Ward ward = this.readWardById(wardId);
        if (ward != null) {
            wardName = ward.description;
        }
        return wardName;
    }

    public Vector listDxType() {
        return theLO.theDxtype;
    }

    /**
     * �ʴ�ʶҹ��Һ�ŷ����� �Ѵ�͡������� 10/11/2547
     *
     * @param ch
     * @param amphur
     * @return
     */
    public Vector listOffice(String ch, String amphur) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if ((amphur.length() == 6)) {
                amphur = amphur.substring(2, 4);
            }
            if ((ch.length() == 6)) {
                ch = ch.substring(0, 2);
            }
            vc = theHosDB.theOfficeDB.selectByCA(ch, amphur);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * @return @roseuid 3F83DB0D0119
     */
    public Vector listContract() {
        return theLO.theContract;
    }

    /**
     * @param id
     * @return
     * @author henbe
     * @not deprecated can not read inactive record
     */
    public String readReferCauseString(String id) {
        ReferCause rc = readReferCauseById(id);
        if (rc != null) {
            return rc.refer_cause_description;
        } else {
            return "";
        }
    }

    public ReferCause readReferCauseById(String id) {
        return readReferCauseById(id, false);
    }

    //////////////////readDoseText////////for print
    //old code comment by noom
    /*
     * public String readDoseText(String dose,DrugInstruction di,Uom
     * du,DrugFrequency df) { String dose_ret=""; DrugDosePrint ddp =
     * readDrugDosePrintByValue(dose); if(di != null){ dose_ret = dose_ret +" "+
     * di.description; if(dose.equals(Active.isDisable())||dose.isEmpty()){
     * dose_ret = di.description; return dose_ret; } } if(ddp != null){ dose_ret
     * = dose_ret +" "+ddp.item_drug_dose_description; } else{ dose_ret =
     * dose_ret +" "+dose; } if(du != null){ dose_ret = dose_ret +"
     * "+du.description; } if(df != null){ dose_ret = dose_ret +"
     * "+df.description; } return dose_ret; }
     */
    //add new code by noom 20/12/2548
    //modify code by henbe 10/02/2548
    public String intReadDoseText(String dose, DrugInstruction di, Uom du, DrugFrequency df) throws Exception {
        dose = dose.lastIndexOf(".0") == -1
                || Integer.parseInt(dose.substring(dose.indexOf('.') + 1)) > 0
                ? dose : dose.substring(0, dose.indexOf('.'));
        String dose_ret = "";
        if (di != null) {
            dose_ret = dose_ret + " " + di.description;
            if (dose.equals(Active.isDisable()) || dose.isEmpty()) {
                dose_ret = di.description;
                return dose_ret;
            }
        }
        if (du != null) {
            //��š��Է�� ������ dose �� 1.5 ��� ���令��Ҥ���� 0.5 㹰ҹ������ ��Ҿ�����ʴ� 1 ��紤���
            if (dose.indexOf('.') != -1) {
                StringTokenizer st = new StringTokenizer(dose, ".");
                boolean flagFrist = true;
                String fistValue = "";
                String secondValue = "";
                while (st.hasMoreTokens()) {
                    if (flagFrist) {
                        fistValue = st.nextToken();
                        flagFrist = false;
                    } else {
                        secondValue = st.nextToken();
                    }
                }
                DrugDosePrint ddp1 = null;
                //intListDrugDosePrintByUom("0."+secondValue,du.getObjectId());
                Vector vdose = intListDrugDosePrintByUom(du.getObjectId());
                for (int i = 0; i < vdose.size(); i++) {
                    DrugDosePrint ddp = (DrugDosePrint) vdose.get(i);
                    if (ddp.item_drug_dose_value.equals("0." + secondValue)) {
                        ddp1 = ddp;
                    }
                }
                if (ddp1 == null && du.uom_id.equals("TAB")) {
                    ddp1 = readDrugDosePrintByValue("0." + secondValue);
                }
                //��Ҿ���ɢͧ dose ���Ƿӡ�� map ˹����ҷ���������� (1 ��紤���)
                if (ddp1 != null) {
                    //�ó��Թ 1 ���ӵ����š��Է��
                    if (Integer.parseInt(fistValue) >= 1) //dose_ret = dose_ret+" "+fistValue+" �Ѻ " +ddp1.item_drug_dose_description + du.description;
                    {
                        dose_ret = dose_ret + " " + fistValue + du.description + ddp1.item_drug_dose_description;
                    } else {
                        dose_ret = dose_ret + " " + ddp1.item_drug_dose_description + du.description;
                    }
                } else {
                    if (Integer.parseInt(fistValue) >= 1) {
                        //LionHeart ����͡���������ͧ��âͧ������� 04/05/2554
//                       dose_ret = dose_ret+" "+fistValue+" �Ѻ 0."+secondValue+" "+du.description ;
                        dose_ret = dose_ret + " " + fistValue + "." + secondValue + " " + du.description;
                    } else {
                        dose_ret = dose_ret + " " + dose + " " + du.description;
                    }
                }
            } else {
                dose_ret = dose_ret + " " + dose + " " + du.description;
            }
        }
        if (df != null) {
            dose_ret = dose_ret + " " + df.description;
        }

        return dose_ret;
    }
    //end add code by noom

    public String readShortDose(OrderItem oi, OrderItemDrug oid) {
        String doseAll;
        if ((oid.usage_special).equals(Active.isDisable())) {
            String dose = Constant.getShowDoubleString(oid.dose);
            String use_uom = readUomString(oid.use_uom);
            String instruction = "";
            DrugInstruction di = readDrugInstructionById(oid.instruction);
            if (di != null) {
                instruction = di.drug_instruction_id;
            }
            String frequency = "";
            DrugFrequency dfc = readDrugFrequencyById(oid.frequency);
            if (dfc != null) {
                frequency = dfc.drug_frequency_id;
            }
            String qty = Constant.getShowDoubleString(oi.qty);
            String purch_uom = readUomString(oid.purch_uom);
            doseAll = "   " + instruction + " " + dose + " " + use_uom + " " + frequency + " \t" + qty + " " + purch_uom;
        } else {
            String usage_text = oid.usage_text;
            String qty = Constant.getShowDoubleString(oi.qty);
            String purch_uom = readUomString(oid.purch_uom);
            doseAll = "   " + usage_text + " \t" + qty + " " + purch_uom;
        }
        return doseAll;
    }

    //////////////////readDoseText////////for print
    public String intReadDoseText(String dose, DrugInstruction di, Uom du) throws Exception {
        return intReadDoseText(dose, di, du, null);
    }

    public String readShortDoseText(OrderItemDrug oid) {
        if (oid == null) {
            return "";
        }
        String dose = Constant.getShowDoubleString(oid.dose);
        DrugInstruction di = readDrugInstructionById(oid.instruction);
        DrugFrequency df = readDrugFrequencyById(oid.frequency);
        Uom du = this.readUomById(oid.use_uom);
        return readShortDoseText(dose, di, du, df);
    }

    public String readShortDoseText(String dose, DrugInstruction di, Uom du, DrugFrequency df) {
        String dose_ret = "";
        DrugDosePrint ddp = readDrugDosePrintByValue(dose);
        if (di != null) {
            dose_ret = dose_ret + " " + di.drug_instruction_id;
            if (dose.equals(Active.isDisable()) || dose.isEmpty()) {
                dose_ret = di.drug_instruction_id;
                return dose_ret;
            }
        }
        if (ddp != null) {
            dose_ret = dose_ret + " " + ddp.item_drug_dose_description;
        } else {
            dose_ret = dose_ret + " " + dose;
        }
        if (du != null) {
            dose_ret = dose_ret + " " + du.uom_id;
        }
        if (df != null) {
            dose_ret = dose_ret + " " + df.drug_frequency_id;
        }
        return dose_ret;
    }

    /**
     * @param id
     * @return
     * @author henbe
     */
    public DrugDosePrint readDrugDosePrintById(String id) {
        return readDrugDosePrintById(id, false);
    }

    /**
     * @param id
     * @return
     * @author henbe
     */
    protected DrugDosePrint readDrugDosePrintByValue(String id) {
        Vector vp = this.listDrugDosePrint();
        DrugDosePrint p = null;
        if (vp == null) {
            return null;
        }
        for (int j = 0; j < vp.size(); j++) {
            p = (DrugDosePrint) vp.get(j);
            try {
                double ddp_value = Double.parseDouble(p.item_drug_dose_value);
                double input_value = Double.parseDouble(id);
                if (ddp_value == input_value) {
                    break;
                }
                p = null;
            } catch (Exception ex) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                break;
            }
        }
        return p;
    }

    /**
     * @param pk
     * @return
     * @author henbe
     * @not deprecared can not read inactive record it is fix data
     */
    public String readWardString(String pk) {
        String sPlan = "";
        Ward pf = readWardById(pk);
        if (pf != null) {
            sPlan = pf.description;
        }
        return sPlan;
    }

    /**
     * @param id
     * @return
     * @author henbe
     * @not deprecated can not read inactive record
     */
    public String readClinicSById(String id) {
        Clinic clinic = readClinicById(id, false);
        if (clinic == null) {
            return "";
        } else {
            return clinic.name;
        }
    }

    public Clinic readClinicById(String id) {
        return readClinicById(id, false);
    }

    /**
     * @param id
     * @return
     * @author henbe
     * @not deprecated can not read inactive record
     */
    public BillingGroupItem readBillingGroupItemById(String id) {
        return readBillingGroupItemById(id, false);
    }

    public Vector listLocalType() {
        if ((theLO.theLocalType != null)) {
            return theLO.theLocalType;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theLocalType = theHosDB.theLocalTypeDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theLocalType;
    }

    public Vector listLabSet() {
        if ((theLO.vItemlabset != null)) {
            return theLO.vItemlabset;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.vItemlabset = theHosDB.theLabGroupDB.selectAll();

            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.vItemlabset;
    }

    public Vector listEducate() {
        if ((theLO.theEducate != null)) {
            return theLO.theEducate;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theEducate = theHosDB.theEducateDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theEducate;
    }

    public Vector listVisitStatus() {
        return theLO.theVisitStatus;
    }

    public Vector listAnswer(int choose) {
        if ((choose == 1)) {
            if ((theLO.theAnswer == null)) {
                theLO.theAnswer = listAnswerInt(choose);
            }
            return theLO.theAnswer;
        }
        if ((choose == 2)) {
            if ((theLO.theAnswerHealth == null)) {
                theLO.theAnswerHealth = listAnswerInt(choose);
            }
            return theLO.theAnswerHealth;
        }
        if ((choose == 3)) {
            if ((theLO.theNormal == null)) {
                theLO.theNormal = listAnswerInt(choose);
            }
            return theLO.theNormal;
        }
        if ((choose == 4)) {
            if ((theLO.theMilkSeep == null)) {
                theLO.theMilkSeep = listAnswerInt(choose);
            }
            return theLO.theMilkSeep;
        }
        if ((choose == 5)) {
            if ((theLO.vVitK == null)) {
                theLO.vVitK = listAnswerInt(choose);
            }
            return theLO.vVitK;
        }
        if ((choose == 6)) {
            if ((theLO.vAsphyxia == null)) {
                theLO.vAsphyxia = listAnswerInt(choose);
            }
            return theLO.vAsphyxia;
        }
        return null;
    }

    private Vector listAnswerInt(int choose) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theAnswerDB.selectAll(choose);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listAncSection() {
        if ((theLO.theAncSection != null)) {
            return theLO.theAncSection;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theAncSection = theHosDB.theAncSectionDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theAncSection;
    }

    public Vector listOpdFormat() {
        if ((theLO.theOpdType != null)) {
            return theLO.theOpdType;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theOpdType = theHosDB.theOpdTypeDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theOpdType;
    }

    public Vector listPayer() {
        if ((theLO.thePayer != null)) {
            return theLO.thePayer;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.thePayer = theHosDB.thePayerDB.selectAllByName("", Active.isEnable());
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.thePayer;
    }

    public Vector listContractMethodReq() {
        if ((theLO.theContractMethod != null)) {
            return theLO.theContractMethod;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theContractMethod = theHosDB.theContractMethodDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theContractMethod;
    }

    public Vector listWard() {
        if ((theLO.theWard != null)) {
            return theLO.theWard;
        }
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (theLO.theWard == null || theLO.theWard.isEmpty()) {
                theLO.theWard = theHosDB.theWardDB.selectByNameActive("", Active.isEnable());
            }
            vc = theLO.theWard;
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public List<BVisitBed> listAvailabalBedByWardId(String wardId) {
        return listAvailabalBedByWardId(wardId, false);
    }

    public List<BVisitBed> listAvailabalBedByWardId(String wardId, boolean isShowUnknow) {
        List<BVisitBed> list = new ArrayList<BVisitBed>();
        if (isShowUnknow) {
            BVisitBed bVisitBed = new BVisitBed();
            bVisitBed.setObjectId("");
            bVisitBed.bed_number = "����к���§";
            list.add(bVisitBed);
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            list.addAll(theHosDB.theBVisitBedDB.listAvailabalByWardId(wardId));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public Site readSite() {
        return readSite(false);
    }

    public Site readSite(boolean read_db) {
        Site vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = intReadSite(read_db);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Site intReadSite(boolean read_db) throws Exception {
        if (theLO.theSite != null && !read_db) {
            return theLO.theSite;
        }
        theLO.theSite = theHosDB.theSiteDB.selectAll();
        theLO.theOption = theHosDB.theOptionDB.select();
        theHO.initSite();
        return theLO.theSite;
    }

    public String getTextCurrentDate() {
        updateCurrentDateTime();
        return theTextCurrentTime.code;
    }

    public String getTextCurrentTime() {
        updateCurrentDateTime();
        return theTextCurrentTime.name;
    }

    public String getTextCurrentDateTime() {
        updateCurrentDateTime();
        theHO.date_time = theTextCurrentTime.code + "," + theTextCurrentTime.name;
        return theTextCurrentTime.code + "," + theTextCurrentTime.name;
    }

    public List<CommonInf> listDischargeOpd() {
        if (theLO.vDischargeOpd != null && !theLO.vDischargeOpd.isEmpty()) {
            return theLO.vDischargeOpd;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.vDischargeOpd = theHosDB.theDischargeOpdDB.getComboboxDatasource();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.vDischargeOpd;
    }

    public List<CommonInf> listDischargeType() {
        if (theLO.vDischargeType != null && !theLO.vDischargeType.isEmpty()) {
            return theLO.vDischargeType;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.vDischargeType = theHosDB.theDischargeTypeDB.getComboboxDatasource();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.vDischargeType;
    }

    public List<CommonInf> listDischargeIpd() {
        if (theLO.vDischargeIpd != null && !theLO.vDischargeIpd.isEmpty()) {
            return theLO.vDischargeIpd;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.vDischargeIpd = theHosDB.theDischargeIpdDB.getComboboxDatasource();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.vDischargeIpd;
    }

    public Vector listIcd10GroupType() {
        if ((theLO.theIcd10GroupType != null)) {
            return theLO.theIcd10GroupType;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theIcd10GroupType = theHosDB.theIcd10GroupTypeDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theIcd10GroupType;
    }

//    /**
//     * @deprecated henbe unused
//     *
//     */
//    public Vector listPrefixWhereTlock2() {
//        /**
//         * tong comment ��ͧ�Ӥ���������ʴ��ء���� ����������ҷ��١��ͧ
//         */
//        theConnectionInf.open();
//        try {
//            theConnectionInf.getConnection().setAutoCommit(false);
//            theLO.thePrefixTlock2 = theHosDB.thePrefixDB.selectTlcok2();
//            theConnectionInf.getConnection().commit();
//        } catch (Exception ex) {
//            LOG.log(java.util.logging.Level.SEVERE, null, ex);
//            try {
//                theConnectionInf.getConnection().rollback();
//            } catch (SQLException ex1) {
//                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
//            }
//        } finally {
//            theConnectionInf.close();
//        }
//        return theLO.thePrefixTlock2;
//    }
    public Vector listTlock() {
        if ((theLO.theTlock != null)) {
            return theLO.theTlock;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theTlock = theHosDB.theTlockDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theTlock;

    }

    public Vector listFilmSize() {
        return theLO.theFilmSize;
    }

    public Vector listBirthPlace() {
        if ((theLO.theBirthPlace != null)) {
            return theLO.theBirthPlace;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theBirthPlace = theHosDB.theBirthPlaceDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theBirthPlace;
    }

    public Vector listConduct() {
        if ((theLO.theConduct != null)) {
            return theLO.theConduct;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theConduct = theHosDB.theConductDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theConduct;
    }

    public Vector listPostureBaby() {
        if ((theLO.thePostureBaby != null)) {
            return theLO.thePostureBaby;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.thePostureBaby = theHosDB.thePostureBabyDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.thePostureBaby;
    }

    public Vector listPregnantLevel() {
        if ((theLO.thePregnantLevel != null)) {
            return theLO.thePregnantLevel;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.thePregnantLevel = theHosDB.thePregnantLevelDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.thePregnantLevel;
    }

    public Vector listUterusLevel() {
        if ((theLO.theUterusLevel != null)) {
            return theLO.theUterusLevel;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theUterusLevel = theHosDB.theUterusLevelDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theUterusLevel;

    }

    public Vector listVitalTemplate(String pk, String point) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if ((point.equals(Active.isDisable())) || ("0".equals(point))) {
                vc = theHosDB.theVitalTemplate2DB.selectAllByName(pk);
            } else {
                vc = theHosDB.theVitalTemplate2DB.selectAllByServicePoint(pk, point);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listResultGiveBirth() {
        if ((theLO.theResultGiveBirth != null)) {
            return theLO.theResultGiveBirth;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theResultGiveBirth = theHosDB.theResultGiveBirthDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theResultGiveBirth;
    }

    public Vector listICD10Pregnant() {
        if (theLO.theICD10 != null && !theLO.theICD10.isEmpty()) {
            return theLO.theICD10;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theICD10 = theHosDB.theICD10DB.selectByPregnant();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theICD10;
    }

    public Vector listSew() {
        if ((theLO.theSew != null)) {
            return theLO.theSew;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theSew = theHosDB.theSewDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theSew;
    }

    public Vector listGroupChronic() {
        if ((theLO.theGroupChronic != null)) {
            return theLO.theGroupChronic;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theGroupChronic = theHosDB.theGroupChronicDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theGroupChronic;
    }

    public Vector listGroupChronic2() {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theGroupChronic = theHosDB.theGroupChronicDB.selectActivate();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theGroupChronic;
    }

    public Group504 readGroup504ById(String id) {
        Group504 g = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            g = theHosDB.theGroup504DB.selectByPk(id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return g;
    }

    public Group505 readGroup505ById(String id) {
        Group505 g = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            g = theHosDB.theGroup505DB.selectByPk(id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return g;
    }

    public Group506 readGroup506ById(String id) {
        Group506 g = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            g = theHosDB.theGroup506DB.selectByPk(id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return g;
    }

    public Vector listGroup504() {
        if ((theLO.theGroup504 != null)) {
            return theLO.theGroup504;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theGroup504 = theHosDB.theGroup504DB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theGroup504;
    }

    public Vector listGroup505() {
        if ((theLO.theGroup505 != null)) {
            return theLO.theGroup505;
        }

        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theGroup505 = theHosDB.theGroup505DB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theGroup505;
    }

    public Vector listGroup506() {
        if ((theLO.theGroup506 != null)) {
            return theLO.theGroup506;
        }

        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theGroup506 = theHosDB.theGroup506DB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theGroup506;
    }

    public Vector listOptype() {
        return theLO.theOptype;
    }

    public Vector listXrayLeteral() {
        if ((theLO.theXrayLeteral != null)) {
            return theLO.theXrayLeteral;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theXrayLeteral = theHosDB.theXRayLeteralDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theXrayLeteral;
    }

    public Vector listXrayPosition() {
        if ((theLO.theXrayPosition != null)) {
            return theLO.theXrayPosition;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theXrayPosition = theHosDB.theXRayPositionDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theXrayPosition;
    }

    public List listXrayModality() {
        if (theLO.theXrayModality != null && !theLO.theXrayModality.isEmpty()) {
            return theLO.theXrayModality;
        } else if (theLO.theXrayModality == null) {
            theLO.theXrayModality = new ArrayList<CommonInf>();
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theXrayModality.addAll(theHosDB.theBModalityDB.selectAll());
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theXrayModality;
    }

    public Vector listSQLTemplate() {
        if ((theLO.theSQLTemplate != null)) {
            return theLO.theSQLTemplate;
        }

        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theSQLTemplate = theHosDB.theSQLTemplateDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theSQLTemplate;
    }

    public Vector listSQLTemplateParam() {
        if ((theLO.theSQLTemplateParam != null)) {
            return theLO.theSQLTemplateParam;
        }

        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theSQLTemplateParam = theHosDB.theSQLTemplateParamDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theSQLTemplateParam;
    }

    /**
     * @param id
     * @return
     * @author henbe
     * @not deprecated can not read inactive record
     */
    public ServicePoint readServicePointById(String id) {
        return readServicePointById(id, false);
    }

    public int countStaffDoctorInServicePointByServiceID(String service_id) {
        int count = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            count = theHosDB.theServicePointDoctorDB.countBySerciveID(service_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return count;
    }

    public Vector selectIdnameEmployeeAll() {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theEmployeeDB.selectIdnameEmployeeAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * function ��Ǩ�ͺ��Ҩش��ԡ�ù������ͧ��Ǩ�������
     *
     * @param service_id
     * @return
     */
    public Vector listDoctorInServiceDoctor(String service_id) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theSpecialEmployeeDB.getDoctorInServiceDoctor(service_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * Creates a new instance of createPrefix
     *
     * @param preFix
     */
    public void savePrefix(Prefix preFix) {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (preFix.getObjectId() == null) {
                String maxcode = theHosDB.thePrefixDB.selectMaxCode();
                int len;
                try {
                    len = Integer.parseInt(maxcode);
                } catch (NumberFormatException ex) {
                    len = Integer.parseInt(theHosDB.thePrefixDB.selectCount());
                }
                preFix.setObjectId(String.valueOf(len + 1));
                theHosDB.thePrefixDB.insert(preFix);
            } else {
                theHosDB.thePrefixDB.update(preFix);
            }
            theLO.thePrefix = theHosDB.thePrefixDB.selectAll(Active.isEnable());
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.confirmBox(ResourceBundle.getBundleGlobal("TEXT.TRANSACTION.INSERT")
                    + ResourceBundle.getBundleGlobal("TEXT.FAIL")
                    + " "
                    + ResourceBundle.getBundleGlobal("TEXT.TRY.AGAIN"), UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
    }

    /**
     * Creates a new instance of createRelation ����͸Ժ�´����� ������
     * algorithm ����㹡�äӹǹ�ç���
     *
     * @param r
     */
    public void saveRelation(Relation r) {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            intSaveRelation(r);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
    }

    protected void intSaveRelation(Relation r) throws Exception {
        if ((r.getObjectId() == null)) {
            String max = theHosDB.theRelationDB.selectMaxCode();
            if (Integer.parseInt(max.substring(0, 1)) == 0
                    && Integer.parseInt(max.substring(1, 2)) != 9) {
                r.setObjectId("0" + String.valueOf(Integer.parseInt(max) + 1));
            } else {
                r.setObjectId(String.valueOf(Integer.parseInt(max) + 1));
            }
            theHosDB.theRelationDB.insert(r);
        } else {
            theHosDB.theRelationDB.update(r);
        }
        theLO.theRelation = theHosDB.theRelationDB.selectAll();
    }

    protected void intSaveBodyOrgan(BodyOrgan body_organ) throws Exception {
        if (body_organ.getObjectId() == null) {
            String max = theHosDB.theBodyOrganDB.selectMaxCode();
            if (max != null) {
                if (Integer.parseInt(max.substring(0, 1)) == 0
                        && Integer.parseInt(max.substring(1, 2)) != 9) {
                    body_organ.number = "0" + String.valueOf(Integer.parseInt(max) + 1);
                } else {
                    body_organ.number = String.valueOf(Integer.parseInt(max) + 1);
                }
                theHosDB.theBodyOrganDB.insert(body_organ);
            } else {
                body_organ.number = "01";
                theHosDB.theBodyOrganDB.insert(body_organ);
            }
        } else {
            theHosDB.theBodyOrganDB.update(body_organ);
        }
        theLO.vBodyOrgan = null;
    }

    public Vector listEmployeeXray() {
        if ((theLO.vTheEmployeeXray != null)) {
            return theLO.vTheEmployeeXray;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.vTheEmployeeXray = theHosDB.theEmployeeDB.selectAuthenAllByName("",
                    Active.isEnable(),
                    new String[]{Authentication.XRAY});
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.vTheEmployeeXray;
    }

    public Vector listEmployee() {
        return theLO.theEmployee;
    }

    /**
     * Function: �� Order Status
     *
     * @return
     */
    public Vector listOrderStatus() {
        return theLO.theOrderItemStatus;
    }

    public String readPatientAddress(Patient pt) {
        String address = "";
        if (pt == null) {
            return address;
        }
        if (pt.is_other_country.equals(Active.isEnable())) {
            return pt.other_address;
        }
        if (pt.house != null) {
            address += pt.house;
        }
        if (pt.village != null) {
            if (!pt.village.isEmpty()) {
                address = address + "  " + ResourceBundle.getBundleGlobal("TEXT.VILLAGE.NO") + " " + pt.village;
            }
        }
        if (pt.road != null) {
            if (!pt.road.isEmpty()) {
                address = address + "  " + ResourceBundle.getBundleGlobal("TEXT.ROAD") + " " + pt.road;
            }
        }
        address += readAddressCat(pt.tambon, pt.ampur, pt.changwat);
        return address;
    }

    public String intReadPatientAddress(Patient pt) throws Exception {
        String address = "";
        if (pt == null) {
            return address;
        }
        if (pt.is_other_country.equals(Active.isEnable())) {
            return pt.other_address;
        }
        if (pt.house != null) {
            address += pt.house;
        }
        if (pt.village != null) {
            if (!pt.village.isEmpty()) {
                address = address + "  " + ResourceBundle.getBundleGlobal("TEXT.VILLAGE.NO") + " " + pt.village;
            }
        }
        if (pt.road != null) {
            if (!pt.road.isEmpty()) {
                address = address + "  " + ResourceBundle.getBundleGlobal("TEXT.ROAD") + " " + pt.road;
            }
        }
        address += intReadAddressCat(pt.tambon, pt.ampur, pt.changwat);
        return address;
    }

    /*
     * @author pongtorn henbe �鹷������ҡ �ѧ��Ѵ ����� �Ӻ�
     */
    public String readAddressCat(String tambol, String amp, String chang) {
        try {
            String SUBDISTRICT = ResourceBundle.getBundleGlobal("TEXT.SHORT.SUBDISTRICT");
            String DISTRICT = ResourceBundle.getBundleGlobal("TEXT.SHORT.DISTRICT");
            String PROVINCE = ResourceBundle.getBundleGlobal("TEXT.SHORT.PROVINCE");
            if ("��ا෾��ҹ��".equals(chang)) {
                SUBDISTRICT = ResourceBundle.getBundleGlobal("TEXT.SHORT.SUBDISTRICT.BKK");
                DISTRICT = ResourceBundle.getBundleGlobal("TEXT.SHORT.DISTRICT.BKK");
                PROVINCE = "";
            }
            String address = "  " + readAddressString(SUBDISTRICT, tambol);
            address = address + "  " + readAddressString(DISTRICT, amp);
            address = address + "  " + readAddressString(PROVINCE, chang);
            return address;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            return null;
        }
    }

    public String intReadAddressCat(String tambol, String amp, String chang) throws Exception {
        String SUBDISTRICT = ResourceBundle.getBundleGlobal("TEXT.SHORT.SUBDISTRICT");
        String DISTRICT = ResourceBundle.getBundleGlobal("TEXT.SHORT.DISTRICT");
        String PROVINCE = ResourceBundle.getBundleGlobal("TEXT.SHORT.PROVINCE");
        if ("��ا෾��ҹ��".equals(chang)) {
            SUBDISTRICT = ResourceBundle.getBundleGlobal("TEXT.SHORT.SUBDISTRICT.BKK");
            DISTRICT = ResourceBundle.getBundleGlobal("TEXT.SHORT.DISTRICT.BKK");
            PROVINCE = "";
        }
        String address = "  " + intReadAddressString(SUBDISTRICT, tambol);
        address = address + "  " + intReadAddressString(DISTRICT, amp);
        address = address + "  " + intReadAddressString(PROVINCE, chang);
        return address;
    }

    public String readContactAddress(Patient pt) {
        String address = "";
        if (pt.house_contact != null) {
            if (!pt.house_contact.isEmpty()) {
                address += pt.house_contact;
            }
        }
        if (pt.village_contact != null) {
            if (!pt.village_contact.isEmpty()) {
                address = address + " " + ResourceBundle.getBundleGlobal("TEXT.VILLAGE.NO") + " " + pt.village_contact;
            }
        }
        if (pt.road_contact != null) {
            if (!pt.road_contact.isEmpty()) {
                address = address + " " + ResourceBundle.getBundleGlobal("TEXT.ROAD") + " " + pt.road_contact;
            }
        }
        address += readAddressCat(pt.tambon_contact, pt.ampur_contact, pt.changwat_contact);
        return address;
    }

    public String intReadContactAddress(Patient pt) throws Exception {
        String address = "";
        if (pt.house_contact != null) {
            if (!pt.house_contact.isEmpty()) {
                address += pt.house_contact;
            }
        }
        if (pt.village_contact != null) {
            if (!pt.village_contact.isEmpty()) {
                address = address + " " + ResourceBundle.getBundleGlobal("TEXT.VILLAGE.NO") + " " + pt.village_contact;
            }
        }
        if (pt.road_contact != null) {
            if (!pt.road_contact.isEmpty()) {
                address = address + " " + ResourceBundle.getBundleGlobal("TEXT.ROAD") + " " + pt.road_contact;
            }
        }
        address += intReadAddressCat(pt.tambon_contact, pt.ampur_contact, pt.changwat_contact);
        return address;
    }

    /**
     * @param id
     * @return
     * @author henbe
     * @not deprecated can not read inactive record
     */
    public CategoryGroupItem readCategoryGroupItemById(String id) {
        return readCategoryGroupItemById(id, false);
    }

    public String readCategoryGroupItemString(String id) {
        String sPlan = "";
        CategoryGroupItem pf = readCategoryGroupItemById(id, false);
        if (pf != null) {
            sPlan = pf.description;
        }
        return sPlan;
    }

    public CategoryGroupItem intReadCategoryGroupItemById(String id) throws Exception {
        return theHosDB.theCategoryGroupItemDB.selectByPK(id);
    }

    public Vector listTab() {
        if ((theLO.vTab != null)) {
            return theLO.vTab;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.vTab = theHosDB.theTabPanelDB.selectAllTab();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.vTab;
    }

    public Item readItemById(String pk) {
        Item item = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            item = theHosDB.theItemDB.selectByPK(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return item;
    }

    /*
     * @author Pongtorn(Henbe) @name check
     */
    protected void intSaveTransaction(Transfer transfer, String date_time) throws Exception {
        Transfer lastTransfer = (Transfer) theHO.vTransfer.get(theHO.vTransfer.size() - 1);
        if (lastTransfer.service_point_id.equals(transfer.service_point_id)) {
            if ((lastTransfer.ward_id != null || !lastTransfer.ward_id.isEmpty())
                    && (transfer.ward_id != null || !transfer.ward_id.isEmpty())
                    && !(lastTransfer.ward_id.equals(transfer.ward_id))) {
                // do not thing
            } else {
                return;
            }
        }
        /////////////////////////////////////////////////////////
        //this function must change old transaction to change status
        transfer.visit_id = theHO.theVisit.getObjectId();
        transfer.patient_id = theHO.theVisit.patient_id;
        transfer.status = Transfer.STATUS_WAIT;
        transfer.assign_time = date_time;
        transfer.service_start_time = "";//date_time.substring(date_time.indexOf(',')+1);;
        transfer.sender_id = theHO.theEmployee.getObjectId();
        if (!transfer.ward_id.isEmpty()) {
            transfer.status = Transfer.STATUS_COMPLETE;
        }
        theHosDB.theTransferDB.insert(transfer);
        theHO.vTransfer.add(transfer);
        //�Դ��ѡ��÷�����ͧ ������繤����ҧ���Ƿӧҹ�����  henbe
        if (!transfer.ward_id.isEmpty()) {
            return;
        }
        //////////////////////////////////////////////////////////
        //�ó��ùӼ������������кǹ��õ�ͧ��͡
        ServicePoint sp = theHosDB.theServicePointDB.selectByPK(transfer.service_point_id);
        Ward ward = theHosDB.theWardDB.selectByPK(transfer.service_point_id);
        if (theHO.theListTransfer == null) {
            QueueVisit qv = null;
            MapQueueVisit mapQV = theHosDB.theMapQueueVisitDB.selectByVisitID(theHO.theVisit.getObjectId());
            if ((mapQV != null)) {
                qv = theHosDB.theQueueVisitDB.selectByPK(mapQV.queue_visit);
            }
            ListTransfer lt;
            if (sp == null)//amp:/18/02/2549
            {
                lt = theHO.initListTransfer(transfer, ward.description, mapQV, qv);
            } else {
                lt = theHO.initListTransfer(transfer, sp.name, mapQV, qv);
            }
            theHosDB.theQueueTransferDB.insert(lt);
            theHO.theListTransfer = lt;
        } else {//�óա���觼�������ѧ�ش��ԡ����蹵�ͧ�Ŵ��͡
            theHO.theListTransfer.assign_time = transfer.assign_time;
            theHO.theListTransfer.doctor = transfer.doctor_code;
            theHO.theListTransfer.locking = "0";
            if (sp == null) {
                theHO.theListTransfer.servicepoint_id = ward.getObjectId();
                theHO.theListTransfer.name = ward.description;
            } else {
                theHO.theListTransfer.servicepoint_id = sp.getObjectId();
                theHO.theListTransfer.name = sp.name;
            }
            theHosDB.theQueueTransferDB.updateServiceTransfer(theHO.theListTransfer);
        }
    }

    public Vector listDxTemplateByName(String pk) {
        return listDxTemplateByName(pk, false);
    }
    //������ѹ��Ҵ����¡�ä鹤ӷ���鹵鹴��¡�͹���Ǥ��¤鹤ӷ���Сͺ���µ���ҷ���ѧ
    // ���͡óշ��ӷ���鹵鹴������������ѡ��¡��˹��

    public Vector listDxTemplateByName(String pk, boolean begin_with) {
        if (pk == null) {
            return null;
        }
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (!begin_with) {
                pk = "%" + pk;
            }
            vc = theHosDB.theDxTemplate2DB.selectAllByName(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listDxTemplateByVid(String vid) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theDxTemplate2DB.selectByVid(vid);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * list ���˵ء�� Refer sumo 14/3/2549
     *
     * @return
     */
    public Vector listReferCause() {
        return theLO.theReferCause;
    }

    public Vector listReferPriority() {
        return theLO.theReferPriority;
    }

    public Vector listReferType() {
        return theLO.theReferType;
    }

    public Vector listReferDisposition() {
        return theLO.theReferDisposition;
    }

    public Vector listICD9() {
        if ((theLO.theICD9 != null)) {
            return theLO.theICD9;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theICD9 = theHosDB.theICD9DB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theICD9;
    }

    /**
     * @param id
     * @return
     * @author henbe
     * @not deprecared can not read inactive record
     */
    public ICD9 readICD9ById(String id) {
        Vector vp = this.listICD9();
        ICD9 p = null;
        for (int j = 0; j < vp.size(); j++) {
            try {
                p = (ICD9) vp.get(j);
            } catch (Exception ex) {
                ComboFix cf = (ComboFix) vp.get(j);
                p = new ICD9();
                p.setObjectId(cf.code);
                p.description = cf.name;
            }
            if (p.getObjectId().equals(id)) {
                break;
            }
        }
        if (p != null && p.getObjectId().equals(id)) {
            return p;
        }
        return null;
    }

    public ICD10 readICD10ById(String pk) {
        if (pk == null || pk.isEmpty()) {
            return null;
        }
        ICD10 vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theICD10DB.selectByPK(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * list ���˵� ICD9 �ҡ���� Icd9 sumo 29/3/2549
     *
     * @param pk
     * @return
     */
    public Vector listICD9ByName(String pk) {
        if (pk == null) {
            return null;
        }
        Vector vc = new Vector();
        if (pk.isEmpty()) {
            return vc;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theICD9DB.selectAllByName(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * @Author: amp
     * @date: 07/04/2549
     * @param pk
     * @return
     */
    public Vector listBodyOrgan(String pk) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theBodyOrganDB.selectAllByNameActive(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * @return @author henbe
     * @not deprecated can not read inactive record
     */
    public Vector listQueueVisit() {
        return theLO.vQueueVisit;
    }

    public void refreshLookup() {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theSite = theHosDB.theSiteDB.selectAll();
            theLO.theOption = theHosDB.theOptionDB.select();
            theHO.initSite();
            theLO.theSqdHN = theHosDB.theSequenceDataDB.selectByPK("hn");
            theLO.theSqdVN = theHosDB.theSequenceDataDB.selectByPK("vn");
            theLO.theSqdAN = theHosDB.theSequenceDataDB.selectByPK("an");
            theLO.vQueueVisit = theHosDB.theQueueVisitDB.selectAllByName("", Active.isEnable());
            theLO.theOffice = theHosDB.theOfficeDB.selectByPK(theHO.theSite.off_id);
            theLO.vDrugDoseShortcut = theHosDB.theDrugDoseShortcutDB.selectAllByPK("", Active.isEnable());
            theLO.theChangwat = theHosDB.theFAddressDB.selectChangwat();
            theLO.theAuthentication = theHosDB.theAuthenticationDB.selectAuthenAll();
            theLO.theBlood = theHosDB.theBloodGroupDB.selectAll();
            theLO.theCategoryGroup = theHosDB.theCategoryGroupDB.selectAll();
            theLO.theCategoryGroupItem = theHosDB.theCategoryGroupItemDB.selectAll();
            theLO.theItemLabType = theHosDB.theItemLabTypeDB.selectAll();
            theLO.theClinic = theHosDB.theClinicDB.selectAll();
            theLO.theContract = theHosDB.theContractDB.selectAll();
            theLO.theDxtype = theHosDB.theDxtypeDB.selectAll();
            theLO.theEducate = theHosDB.theEducateDB.selectAll();
            theLO.theEmployee = theHosDB.theEmployeeDB.selectAllByName();
            theLO.theEmployee_loc = theHosDB.theEmployeeDB.selectDoctor(Authentication.DOCTOR);
            theLO.theFstatus = theHosDB.theFStatusDB.selectAll();
            theLO.theGender = theHosDB.theSexDB.selectAll();
            theLO.theIcd10GroupType = theHosDB.theIcd10GroupTypeDB.selectAll();
            theLO.theLabor = theHosDB.theLaborDB.selectAll();
            theLO.theMarriage = theHosDB.theMarryStatusDB.selectAll();
            theLO.theNation = theHosDB.theNationDB.selectAll();
            theLO.vNutritionType = theHosDB.theNutritionTypeDB.selectAll();
            theLO.theOccupation = theHosDB.theOccupatDB.selectAll();
            theLO.theOptype = theHosDB.theOptypeDB.selectAll();
            theLO.theOrderItemStatus = theHosDB.theOrderItemStatusDB.selectAll();
            theLO.thePlan = theHosDB.thePlanDB.selectByCN("");
            theLO.thePlanActive = theHosDB.thePlanDB.selectByCNA("", "1");
            theLO.thePrefix = theHosDB.thePrefixDB.selectAll(Active.isEnable());
            theLO.theRace = theHosDB.theNationDB.selectAll();
            theLO.theRelation = theHosDB.theRelationDB.selectAll();
            theLO.theReligion = theHosDB.theReligionDB.selectAll();
            theLO.vServicePoint = theHosDB.theServicePointDB.selectAllByName("", Active.isEnable());
            theLO.theTypeArea = theHosDB.theTypeAreaDB.selectAll();
            theLO.theWard = theHosDB.theWardDB.selectByNameActive("", Active.isEnable());
            theLO.thePayer = theHosDB.thePayerDB.selectAllByName("", Active.isEnable());
            theLO.theBillingGroupItem = theHosDB.theBillingGroupItemDB.selectAllByName("", Active.isEnable());
            theLO.theReferCause = theHosDB.theReferCauseDB.selectAll();
            theLO.theReferPriority = theHosDB.theReferPriorityDB.selectAll();
            theLO.theReferType = theHosDB.theReferTypeDB.selectAll();
            theLO.theReferDisposition = theHosDB.theReferDispositionDB.selectAll();
            theLO.theICD9 = theHosDB.theICD9DB.selectAll();
            theLO.vBodyOrgan = theHosDB.theBodyOrganDB.selectAll();//amp:10/04/2549
            theLO.vDisease = theHosDB.theDiseaseDB.selectAllCombobox();//amp:18/04/2549
            theLO.vNutritionTypeMap = theHosDB.theNutritionTypeMapDB.listNutritionMap();//amp:28/04/2549
            theLO.vItem16Group = theHosDB.theItem16GroupDB.selectAll();//sumo:05/06/2549
            theLO.vNCDGroup = theHosDB.theNCDGroupDB.selectAll();//amp:14/06/2549
            theLO.theGroupChronic = theHosDB.theGroupChronicDB.selectAll();//amp:17/06/2549
            theLO.vGuide = theHosDB.theGuideDB.selectAll();//sumo:04/08/2549 ���й�
            theLO.vCalDateAppointment = theHosDB.theCalDateAppointmentDB.selectAll(); // sumo:08/08/2549 : ��Ǥӹǳ�ѹ���Ѵ
            theLO.theDrugDosePrint = theHosDB.theDrugDosePrintDB.selectByKeyWord("", "1");
            theLO.theServicePoint = theHosDB.theServicePointDB.selectAll();
            theLO.theVisitStatus = theHosDB.theVisitStatusDB.selectAll();
            theLO.theFilmSize = theHosDB.theFilmSizeDB.selectAll();
            //3.9.37
            //refer in -out 20/06/2014
            theLO.vPtypeDis = theHosDB.thePTypeDisDB.selectAll();
            theLO.vReferResult = theHosDB.theReferResultDB.selectAll();
            theLO.vCareType = theHosDB.theCareTypeDB.selectAll();
            theLO.patientFamilyHistoryTypes = theHosDB.theFPatientFamilyHistoryDB.getComboboxDatasource();
            //////////////////////////////////////////////////////////
            this.intListDrugFrequency(false);
            this.intListDrugInstruction(false);
            this.intListUom(false);
            theLO.itemSubgroupCheckCalculates = theHosDB.theItemSubgroupCheckCalculateDB.listAll();
            theLO.printings = theHosDB.thePrintingDB.list();
            Village vill = theHosDB.theVillageDB.selectMoo0();
            if (vill != null) {
                Home home = theHosDB.theHomeDB.selectByNo("0", vill.getObjectId());
                if (home != null) {
                    theHO.home_out_side = home.getObjectId();
                }
            }
            // clear for next call
            if (theLO.insuranceCompanies != null) {
                theLO.insuranceCompanies.clear();
            }
            // clear for next call
            if (theLO.banks != null) {
                theLO.banks.clear();
            }
            // clear for next call
            if (theLO.paymentTypes != null) {
                theLO.paymentTypes.clear();
            }
            // clear for next call
            if (theLO.creditCardTypes != null) {
                theLO.creditCardTypes.clear();
            }
            // clear for next call
            if (theLO.govCodes != null) {
                theLO.govCodes.clear();
            }
            if (theLO.subInscls != null) {
                theLO.subInscls.clear();
            }
            if (theLO.relInscls != null) {
                theLO.relInscls.clear();
            }
            if (theLO.personForeignerTypes != null) {
                theLO.personForeignerTypes.clear();
            }
            if (theLO.productCategoryies != null) {
                theLO.productCategoryies.clear();
            }
            if (theLO.drugSpecPreps != null) {
                theLO.drugSpecPreps.clear();
            }
            if (theLO.itemManufacturers != null) {
                theLO.itemManufacturers.clear();
            }
            if (theLO.itemDistributors != null) {
                theLO.itemDistributors.clear();
            }
            if (theLO.allDiagDoctors != null) {
                theLO.allDiagDoctors.clear();
            }
            if (theLO.theXrayModality != null) {
                theLO.theXrayModality.clear();
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
    }

    public Vector listDisease() {
        return theLO.vDisease;
    }

    public Vector listDisease(String str) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theDiseaseDB.selectDiseaseByName(str);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listLabStd() {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.vLabStd = theHosDB.theLabStdDB.selectAllCombofix();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.vLabStd;
    }

    public Vector listLabStdDistinct() {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.vLabStd = theHosDB.theLabStdDB.selectAllCombofixDistinct();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.vLabStd;
    }

    public Vector listLabStdGroup(String grp) {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.vLabStd = theHosDB.theLabStdDB.selectByGroup(grp);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.vLabStd;
    }

    /**
     * @return @Author amp
     *
     * @date 14/06/2549
     */
    public Vector listNCDGroup() {
        return theLO.vNCDGroup;
    }

    public Office getOfficeById(String id) {
        Office v = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            v = theHosDB.theOfficeDB.selectByPK(id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return v;
    }

    /**
     * @param name
     * @return
     * @Author henbe
     * @date 14/06/2549
     */
    public Vector listOfficeByName(String name) {
        Vector v = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (name != null && !name.isEmpty() && !name.equals("%")) {
                v = theHosDB.theOfficeDB.selectByName(name);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return v;
    }

    /**
     * @param str
     * @return
     * @Author amp
     * @date 14/06/2549
     */
    public Vector listGroupChronicEng(String str) {
        Vector v = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            v = theHosDB.theGroupChronicDB.selectByCN(str);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return v;
    }

    public GroupChronic readGroupChronicByCode(String code) {
        if ((code == null)) {
            return null;
        }
        if ((code.length() == 0)) {
            return null;
        }
        GroupChronic gc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            gc = theHosDB.theGroupChronicDB.selectByPK(code);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return gc;
    }

    /**
     * @author: sumo
     * @date: 04/08/2549
     * @see : ��㹡�� list �����Ť��й�/�������آ�֡��
     * @param pk �������
     * @return Vector �ͧ Object Guide
     */
    public Vector listGuide(String pk) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theGuideDB.selectAllByName(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listOpinionRecommendation(String keyword) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<BOpinionRecommendation> list = theHosDB.theBOpinionRecommendationDB.listByKeyword(keyword);
            vc = new Vector(list);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listGuideNCF(String pk) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theGuideDB.selectByCodeName(pk, "1");
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listCalDateAppointment() {
        if ((theLO.vCalDateAppointment != null)) {
            return theLO.vCalDateAppointment;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.vCalDateAppointment = theHosDB.theCalDateAppointmentDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.vCalDateAppointment;
    }

    /**
     * @Author: amp
     * @date: 11/08/2549
     * @see: ������¡�õ�Ǫ��¹Ѵ
     * @param: ���͵�Ǫ��¹Ѵ
     * @return
     * @retrun: Vector �ͧ��Ǫ��¹Ѵ
     * @param name
     */
    public Vector listAppointmentTemplate(String name) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theAppointmentTemplateDB.selectAppointmentTemplate(name);
            AppointmentTemplate at = new AppointmentTemplate();
            at.setObjectId("");
            at.template_name = "����к�";
            vc.add(0, at);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * @Author: amp
     * @date: 11/08/2549
     * @see: ������¡�õ�Ǫ��¹Ѵ
     * @param: key_id ��Ǫ��¹Ѵ
     * @return
     * @retrun: AppointmentTemplate
     * @param pk
     */
    public AppointmentTemplate readAppointmentTemplate(String pk) {
        if ((pk == null)) {
            return null;
        }
        if ((pk.length() == 0)) {
            return null;
        }
        AppointmentTemplate apt = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            apt = theHosDB.theAppointmentTemplateDB.selectAppointmentTemplateByPK(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return apt;
    }

    /**
     * ���Ҽ�����㹤�� ����ش��ԡ�÷���к�
     *
     * @return Vector �������ª��ͼ����·������㹤��
     * @param service_point_id �� String �����ʢͧ�ش��ԡ��
     * @param employee_id_doctor �� String ��������ʢͧᾷ�������� �
     * �ش��ԡ�ù���
     * @param type �� String ����纻������ͧ����������� �����¹͡ ����
     * �������
     * @Author Pu
     * @Date 29/08/2549
     */
    public Vector listVisitQueueTransfer(String service_point_id, String employee_id_doctor, String type) {
        Vector v = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            v = theHosDB.theQueueTransferDB.listTransferVisitQueueByServicePoint(
                    service_point_id, employee_id_doctor, type);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return v;
    }

    /**
     * ���Ҽ�����㹤�� ward
     *
     * @return Vector �������ª��ͼ����·������㹤��
     * @param ward_id �� String �����ʢͧ�ش��ԡ�� ward
     * @Author Pu
     * @Date 29/08/2549
     */
    public Vector listVisitQueueWard(String ward_id) {
        Vector v = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            v = theHosDB.theListTransferDB.listQueueVisitInWard(ward_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return v;
    }

    public String getPatientName(ListTransfer listTransfer) {
        String name = readPrefixString(listTransfer.prefix);
        if (!name.isEmpty()) {
            name = "  " + name + " " + listTransfer.fname + "  " + listTransfer.lname;
        } else {
            name = "  " + listTransfer.fname + "  " + listTransfer.lname;
        }
        return name;
    }

    public String getPatientName(QueueDispense2 listTransfer) {
        String name = readPrefixString(listTransfer.prename);
        if (!name.isEmpty()) {
            name = "  " + name + " " + listTransfer.firstname + "  " + listTransfer.lastname;
        } else {
            name = "  " + listTransfer.firstname + "  " + listTransfer.lastname;
        }
        return name;
    }

    public String getPatientName(Patient patient) {
        String prefix = readPrefixString(patient.getFamily() != null ? patient.getFamily().f_prefix_id : patient.f_prefix_id);
        String firstName = patient.getFamily() != null ? patient.getFamily().patient_name : patient.patient_name;
        String lastName = patient.getFamily() != null ? patient.getFamily().patient_last_name : patient.patient_last_name;
        return prefix + firstName + " " + lastName;
    }

    public String getPersonName(Person person) {
        String prefix = readPrefixString(person.f_prefix_id);
        String firstName = person.person_firstname;
        String lastName = person.person_lastname;
        return prefix + firstName + " " + lastName;
    }

    public Vector listIcd10ByIdNameGroup(String pk, String group, boolean is_code) {
        return this.listIcd10ByIdNameGroup(pk, group, is_code, 1);
    }

    /**
     *
     * @param pk
     * @param group
     * @param is_code
     * @param active 1=active, 0=inactive, -1=all
     * @return
     */
    public Vector listIcd10ByIdNameGroup(String pk, String group, boolean is_code, int active) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (group == null) {
                group = "";
            }
            vc = theHosDB.theICD10DB.selectByIdUseGroup(pk, is_code, group, active);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * ���������ʴ�੾�С���� ICD10 ����ʴ�੾�� ICD10 3 ��ѡ�á
     * �����ʴ�੾�����ʷ���ըش�ȹ���
     *
     * @param pk
     * @param group
     * @param is_code
     * @param code ��Ǩ�ͺ���Ẻ���� true ����Ẻ���� false ����Ẻ���������
     * @return
     * @author pu 09/09/08
     */
    public Vector listIcd10ByGroup(String pk, String group, boolean is_code, boolean code) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
//            pk = Gutil.CheckReservedWords(pk);
//            String icd = pk + "%";
//            String des = "%" + pk + "%";
//            String odes = "%" + pk + "%";
            if (group == null) {
                group = "";
            }
//            if (is_code) {
//                vc = theHosDB.theICD10DB.selectByIdGroup(icd, "", "", group, code);
//            } else {
//                vc = theHosDB.theICD10DB.selectByIdGroup(icd, des, odes, group, code);
//            }
            vc = theHosDB.theICD10DB.selectByIdGroup(pk, is_code, group, code);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector copyVector(Vector v) {
        Vector vout = new Vector();
        // ������Ǩ�ͺ��� null return empty vector
        if (v != null) {
            for (int i = 0; i < v.size(); i++) {
                vout.add(v.get(i));
            }
        }
        return vout;
    }

    public Employee readEmployeeById(String id) {
        for (int i = 0; i < theLO.theEmployee.size(); i++) {
            Employee emp = (Employee) theLO.theEmployee.get(i);
            if (emp.getObjectId().equals(id)) {
                return emp;
            }
        }
        return null;
    }

    public MarryStatus readMarryStatusById(String id) {
        Vector vp = theLO.theMarriage;
        CommonInf readCommonInf = readCommonInf(vp, id);
        if (readCommonInf instanceof ComboFix) {
            ComboFix cf = (ComboFix) readCommonInf;
            MarryStatus marryStatus = new MarryStatus();
            marryStatus.setObjectId(cf.code);
            marryStatus.description = cf.name;
            return marryStatus;
        } else {
            return (MarryStatus) readCommonInf;
        }

    }

    public MarryStatus readMarryStatusByDesc(String desc) {
        Vector vp = theLO.theMarriage;
        CommonInf readCommonInf = readCommonInfByDesc(vp, desc);
        if (readCommonInf instanceof ComboFix) {
            ComboFix cf = (ComboFix) readCommonInf;
            MarryStatus marryStatus = new MarryStatus();
            marryStatus.setObjectId(cf.code);
            marryStatus.description = cf.name;
            return marryStatus;
        } else {
            return (MarryStatus) readCommonInf;
        }
    }

    public CategoryGroupItem readCategoryGroupItemById(String id, boolean re_read) {
        Vector vp = theLO.theCategoryGroupItem;
        return (CategoryGroupItem) readCommonInf(vp, id);
    }

    public ItemLabType readItemLabTypeById(String id, boolean re_read) {
        Vector vp = theLO.theItemLabType;
        return (ItemLabType) readCommonInf(vp, id);
    }

    public CommonInf readCommonInf(Vector vp, String id) {
        for (int j = 0; j < vp.size(); j++) {
            if (vp.get(j) instanceof CommonInf) {
                CommonInf p = (CommonInf) vp.get(j);
                if (p.getCode().equals(id)) {
                    return p;
                }
            } else if (vp.get(j) instanceof ComboFix) {
                ComboFix pp = (ComboFix) vp.get(j);
                if (pp.code.equals(id)) {
                    return pp;
                }
            } else {
                return null;
            }
        }
        return null;
    }

    public CommonInf readCommonInf(List vp, String id) {
        for (int j = 0; j < vp.size(); j++) {
            if (vp.get(j) instanceof CommonInf) {
                CommonInf p = (CommonInf) vp.get(j);
                if (p.getCode().equals(id)) {
                    return p;
                }
            } else if (vp.get(j) instanceof ComboFix) {
                ComboFix pp = (ComboFix) vp.get(j);
                if (pp.code.equals(id)) {
                    return pp;
                }
            } else {
                return null;
            }
        }
        return null;
    }

    public CommonInf readCommonInfByDesc(Vector vp, String desc) {
        for (int j = 0; j < vp.size(); j++) {
            if (vp.get(j) instanceof CommonInf) {
                CommonInf p = (CommonInf) vp.get(j);
                if (p.getName().equals(desc)) {
                    return p;
                }
            } else if (vp.get(j) instanceof ComboFix) {
                ComboFix pp = (ComboFix) vp.get(j);
                if (pp.name.equals(desc)) {
                    return pp;
                }
            } else {
                return null;
            }
        }
        return null;
    }

    public CommonInf readCommonInfByDesc(List vp, String desc) {
        for (int j = 0; j < vp.size(); j++) {
            if (vp.get(j) instanceof CommonInf) {
                CommonInf p = (CommonInf) vp.get(j);
                if (p.getName().equals(desc)) {
                    return p;
                }
            } else if (vp.get(j) instanceof ComboFix) {
                ComboFix pp = (ComboFix) vp.get(j);
                if (pp.name.equals(desc)) {
                    return pp;
                }
            } else {
                return null;
            }
        }
        return null;
    }

    public Clinic readClinicById(String id, boolean re_read) {
        Vector vp = theLO.theClinic;
        for (int j = 0; j < vp.size(); j++) {
            Clinic p = (Clinic) vp.get(j);
            if (p.getObjectId().equals(id)) {
                return p;
            }
        }
        return null;
    }

    public BillingGroupItem readBillingGroupItemById(String id, boolean re_read) {
        Vector vp = theLO.theBillingGroupItem;
        return (BillingGroupItem) readCommonInf(vp, id);
    }

    /**
     * @param id
     * @return
     * @author henbe
     * @not deprecared can not read inactive record it is fix data
     */
    public VisitStatus readVisitStatusById(String id) {
        Vector vp = theLO.theVisitStatus;
        VisitStatus p = null;
        for (int j = 0; j < vp.size(); j++) {
            try {
                p = (VisitStatus) vp.get(j);
            } catch (Exception ex) {
                ComboFix cf = (ComboFix) vp.get(j);
                p = new VisitStatus();
                p.setObjectId(cf.code);
                p.description = cf.name;
            }
            if (p.getObjectId().equals(id)) {
                break;
            }
        }
        if (p != null && p.getObjectId().equals(id)) {
            return p;
        }
        return null;

    }

    public Optype readOptypeById(String id) {

        for (int i = 0; i < theLO.theOptype.size(); i++) {
            Optype op = (Optype) theLO.theOptype.get(i);
            if (op.getObjectId().equals(id)) {
                return op;
            }
        }
        return null;
    }

    public Dxtype readDxtypeById(String id) {
//        for (int i = 0; i < theLO.theDxtype.size(); i++) {
//            Dxtype op = (Dxtype) theLO.theDxtype.get(i);
//            if (op.getObjectId().equals(id)) {
//                return op;
//            }
//        }
//        return null;
        Dxtype dxtype = null;
        Vector vp = theLO.theDxtype;
        CommonInf readCommonInf = readCommonInf(vp, id);
        if (readCommonInf != null) {
            dxtype = new Dxtype();
            dxtype.setObjectId(readCommonInf.getCode());
            dxtype.description = readCommonInf.getName();
        }
        return dxtype;
    }

    public FilmSize readFilmSizeById(String id) {
        Vector vp = theLO.theFilmSize;
        return (FilmSize) readCommonInf(vp, id);
    }

    public ServicePoint readServicePointById(String id, boolean re_read) {
        Vector vp = theLO.theServicePoint;
        return (ServicePoint) readCommonInf(vp, id);
    }

    public QueueVisit readQueueVisitById(String id) {
        Vector vp = theLO.vQueueVisit;
        return (QueueVisit) readCommonInf(vp, id);
    }

    public Disease readDiseaseById(String id) {
        Vector vp = theLO.vDisease;
        return (Disease) readCommonInf(vp, id);
    }

    public NCDGroup readNCDGroupById(String id) {
        Vector vp = theLO.vNCDGroup;
        return (NCDGroup) readCommonInf(vp, id);
    }

    public Religion readReligionById(String id) {
        Vector vp = theLO.theReligion;
        return (Religion) readCommonInf(vp, id);
    }

    public Religion readReligionByDesc(String desc) {
        Vector vp = theLO.theReligion;
        return (Religion) readCommonInfByDesc(vp, desc);
    }

    public Occupation2 readOccupatById(String id, boolean re_read) {
        Vector vp = theLO.theOccupation;
        return (Occupation2) readCommonInf(vp, id);
    }

    public Occupation2 readOccupatByDesc(String desc) {
        Vector vp = theLO.theOccupation;
        return (Occupation2) readCommonInfByDesc(vp, desc);
    }

    public Nation readNationById(String id) {
        Vector vp = theLO.theNation;
        return (Nation) readCommonInf(vp, id);
    }

    public Nation readNationByDesc(String desc) {
        Vector vp = theLO.theNation;
        return (Nation) readCommonInfByDesc(vp, desc);
    }

    public Relation2 readRelationById(String id, boolean re_read) {
        Vector vp = theLO.theRelation;
        return (Relation2) readCommonInf(vp, id);
    }

    public Relation2 readRelationByDesc(String descString) {
        Vector vp = theLO.theRelation;
        return (Relation2) readCommonInfByDesc(vp, descString);
    }

    public Educate readEducationById(String id, boolean re_read) {
        Vector vp = theLO.theEducate;
        return (Educate) readCommonInf(vp, id);
    }

    public Educate readEducationByDesc(String descString) {
        Vector vp = theLO.theEducate;
        return (Educate) readCommonInfByDesc(vp, descString);
    }

    public Prefix2 readPrefixById(String id, boolean re_read) {
        Vector vp = theLO.thePrefix;
        return (Prefix2) readCommonInf(vp, id);
    }

    public DrugInstruction2 readDrugInstructionById(String id, boolean re_read) {
        Vector vp = theLO.theDrugInstructionAll;
        return (DrugInstruction2) readCommonInf(vp, id);
    }

    public DrugInstruction2 readDrugInstructionByCode(String id) {
        Vector vp = theLO.theDrugInstructionAll;

        DrugInstruction2 p = null;
        for (int j = 0; j < vp.size(); j++) {
            p = (DrugInstruction2) vp.get(j);
            if (p.drug_instruction_id.equals(id)) {
                break;
            }
        }
        return p;
    }

    public Uom2 readUomById(String id, boolean re_read) {
        Uom2 vc = null;
        if (!re_read) {
            if (theLO.theUomAll != null && !theLO.theUomAll.isEmpty()) {
                Vector<Uom2> vp = theLO.theUomAll;
                vc = (Uom2) readCommonInf(vp, id);
                return vc;
            }
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (theLO.theUomAll.isEmpty()) {
                theLO.theUomAll = theHosDB.theUomDB.selectAll();
            }
            Vector vp = theLO.theUomAll;
            vc = (Uom2) readCommonInf(vp, id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public DrugFrequency2 readDrugFrequencyById(String id, boolean re_read) {
        Vector vp = theLO.theDrugFrequencyAll;
        return (DrugFrequency2) readCommonInf(vp, id);
    }

    /**
     * @param id
     * @return
     * @author henbe
     * @not deprecated can not read inactive record
     */
    public DrugFrequency2 readDrugFrequencyByCode(String id) {
        Vector vp = theLO.theDrugFrequencyAll;

        DrugFrequency2 p = null;
        for (int j = 0; j < vp.size(); j++) {
            p = (DrugFrequency2) vp.get(j);
            if (p.drug_frequency_id.equals(id)) {
                break;
            }
        }
        return p;
    }

    public Contract readContractById(String id, boolean re_read) {
        Vector vp = theLO.theContract;
        return (Contract) readCommonInf(vp, id);
    }

    public OrderItemStatus readOrderItemStatus(String id) {
        Vector vp = theLO.theOrderItemStatus;
        OrderItemStatus p = null;
        for (int j = 0; j < vp.size(); j++) {
            try {
                p = (OrderItemStatus) vp.get(j);
            } catch (Exception ex) {
                ComboFix cf = (ComboFix) vp.get(j);
                p = new OrderItemStatus();
                p.setObjectId(cf.code);
                p.description = cf.name;
            }
            if (p.getObjectId().equals(id)) {
                break;
            }
        }
        if (p != null && p.getObjectId().equals(id)) {
            return p;
        }
        return null;
    }

    /**
     * hosv4
     *
     * @param id
     * @return
     */
    public Plan readPlanById(String id) {
        for (int i = 0; i < theLO.thePlan.size(); i++) {
            Plan plan = (Plan) theLO.thePlan.get(i);
            if (plan.getObjectId().equals(id)) {
                return plan;
            }
        }
        return null;
    }

    public ReferCause readReferCauseById(String id, boolean re_read) {
        Vector vp = theLO.theReferCause;
        ReferCause p = null;
        for (int j = 0; j < vp.size(); j++) {
            p = (ReferCause) vp.get(j);
            if (p.getObjectId().equals(id)) {
                break;
            }
        }
        if (p != null && p.getObjectId().equals(id)) {
            return p;
        }
        return null;
    }

    /**
     * @param id
     * @return
     * @author Henbe Pongtorn
     * @name easy to find without seek in database
     * @read Sex String by id
     * @not deprecated bad function name
     */
    public String readSexSById(String id) {
        Vector vp = theLO.theGender;
        ComboFix p = null;
        for (int j = 0; j < vp.size(); j++) {
            p = (ComboFix) vp.get(j);
            if (p.code.equals(id)) {
                break;
            }
        }
        if (p != null && p.code.equals(id)) {
            return p.name;
        }
        return ResourceBundle.getBundleGlobal("TEXT.NOTFOUND");
    }

    public DrugDosePrint readDrugDosePrintById(String id, boolean re_read) {
        Vector vp = theLO.theDrugDosePrint;
        DrugDosePrint p = null;
        for (int j = 0; j < vp.size(); j++) {
            p = (DrugDosePrint) vp.get(j);
            if (p.getObjectId().equals(id)) {
                break;
            }
        }
        if (p != null && p.getObjectId().equals(id)) {
            return p;
        }
        return null;
    }

    public DischargeType readDischargeTypeById(String id) {
        List<CommonInf> vp = listDischargeType();
        for (int j = 0; j < vp.size(); j++) {
            DischargeType p = (DischargeType) vp.get(j);
            if (p.getObjectId().equals(id)) {
                return p;
            }
        }
        return null;
    }

    public DischargeIpd readDischargeIpdById(String id) {
        List<CommonInf> vp = listDischargeIpd();
        for (int j = 0; j < vp.size(); j++) {
            DischargeIpd p = (DischargeIpd) vp.get(j);
            if (p.getObjectId().equals(id)) {
                return p;
            }
        }
        return null;
    }

    public DischargeOpd readDischargeOpdById(String id) {
        List<CommonInf> vp = listDischargeOpd();
        DischargeOpd p = null;
        for (int j = 0; j < vp.size(); j++) {
            p = (DischargeOpd) vp.get(j);
            if (p.getObjectId().equals(id)) {
                break;
            }
        }
        if (p != null && p.getObjectId().equals(id)) {
            return p;
        }
        return null;
    }

    public Ward readWardById(String pk) {
        Vector vp = theLO.theWard;
        for (int j = 0; j < vp.size(); j++) {
            Ward p = (Ward) vp.get(j);
            if (p.getObjectId().equals(pk)) {
                return p;
            }
        }
        return null;
    }

    public Employee readEmployeeByUsername(String employee_id, String active) {
        for (int i = 0; i < theLO.theEmployee.size(); i++) {
            Employee emp = (Employee) theLO.theEmployee.get(i);
            if (emp.employee_id.equals(employee_id)
                    && emp.active.equals(active)) {
                return emp;
            }
        }
        return null;
    }

    public Vector listPrefixAll() {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.thePrefixDB.selectAll("");
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listCategoryGroupItem(String cat) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theCategoryGroupItemDB.selectByCategoryGroupCode(cat);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listDeathPregnancyStatus() {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theDeathPregnancyStatusDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listAlcoholFeq() {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theAlcoholFeqDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listAnsResult() {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theAnsResultDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listCigaretteFeq() {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theCigaretteFeqDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * Move from PCU Module 31102011
     *
     * @return
     */
    public Vector listRHGroup() {
        if ((theLO.vRHGroup != null)) {
            return theLO.vRHGroup;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.vRHGroup = theHosDB.theRHGroupDB.selectAllCombofix();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.vRHGroup;
    }

    /**
     * Move from PCU Module 31102011
     *
     * @return
     */
    public Vector listVillage() {
        return listVillage(false);
    }

    public Vector listVillage(boolean isUpdate) {
        if (!isUpdate && theLO.vVillage != null && !theLO.vVillage.isEmpty()) {
            return theLO.vVillage;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.vVillage = theHosDB.theVillageDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.vVillage;
    }

    public Vector listDischargeStatus() {
        if ((theLO.vDischarge != null)) {
            return theLO.vDischarge;
        }

        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.vDischarge = theHosDB.theDischargeDB.selectAll();
            theLO.vDischarge.add(0, new ComboFix("", ResourceBundle.getBundleGlobal("TEXT.N.A")));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.vDischarge;
    }

    public Vector<ComboFix> listUnusedReferNoByTypeId(String typeId) {
        Vector<ComboFix> vector = new Vector<ComboFix>();
        vector.add(new ComboFix());
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<String> list = theHosDB.theReferDB.listUnusedReferNoByTypeId(typeId);
            for (String string : list) {
                vector.add(new ComboFix(string, string));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vector;
    }

    public Vector<ComboFix> listEyesResultType() {
        if (theLO.vEyesResultType == null || theLO.vEyesResultType.isEmpty()) {
            theLO.vEyesResultType = new Vector();
            theConnectionInf.open();
            try {
                theConnectionInf.getConnection().setAutoCommit(false);
                theLO.vEyesResultType = theHosDB.theEyesResultTypeDB.getComboboxDatasources();
                theConnectionInf.getConnection().commit();
            } catch (Exception ex) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                try {
                    theConnectionInf.getConnection().rollback();
                } catch (SQLException ex1) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex1);
                }
            } finally {
                theConnectionInf.close();
            }
        }
        return theLO.vEyesResultType;
    }

    public Vector<ComboFix> listEyesDiagnosisType() {
        if (theLO.vEyesDiagnosisType == null || theLO.vEyesDiagnosisType.isEmpty()) {
            theLO.vEyesDiagnosisType = new Vector();
            theConnectionInf.open();
            try {
                theConnectionInf.getConnection().setAutoCommit(false);
                theLO.vEyesDiagnosisType = theHosDB.theEyesDiagnosisTypeDB.getComboboxDatasources();
                theConnectionInf.getConnection().commit();
            } catch (Exception ex) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                try {
                    theConnectionInf.getConnection().rollback();
                } catch (SQLException ex1) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex1);
                }
            } finally {
                theConnectionInf.close();
            }
        }
        return theLO.vEyesDiagnosisType;
    }

    public Vector<ComboFix> listFootExamAssessType() {
        if (theLO.vFootExamAssessType == null || theLO.vFootExamAssessType.isEmpty()) {
            theLO.vFootExamAssessType = new Vector();
            theConnectionInf.open();
            try {
                theConnectionInf.getConnection().setAutoCommit(false);
                theLO.vFootExamAssessType = theHosDB.theFootExamAssessTypeDB.getComboboxDatasources();
                theConnectionInf.getConnection().commit();
            } catch (Exception ex) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                try {
                    theConnectionInf.getConnection().rollback();
                } catch (SQLException ex1) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex1);
                }
            } finally {
                theConnectionInf.close();
            }
        }
        return theLO.vFootExamAssessType;
    }

    public Vector<ComboFix> listFootProtectType() {
        if (theLO.vFootProtectType == null || theLO.vFootProtectType.isEmpty()) {
            theLO.vFootProtectType = new Vector();
            theConnectionInf.open();
            try {
                theConnectionInf.getConnection().setAutoCommit(false);
                theLO.vFootProtectType = theHosDB.theFootProtectTypeDB.getComboboxDatasources();
                theConnectionInf.getConnection().commit();
            } catch (Exception ex) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                try {
                    theConnectionInf.getConnection().rollback();
                } catch (SQLException ex1) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex1);
                }
            } finally {
                theConnectionInf.close();
            }
        }
        return theLO.vFootProtectType;
    }

    public Vector<ComboFix> listFootScreenResult() {
        if (theLO.vFootScreenResult == null || theLO.vFootScreenResult.isEmpty()) {
            theLO.vFootScreenResult = new Vector();
            theConnectionInf.open();
            try {
                theConnectionInf.getConnection().setAutoCommit(false);
                theLO.vFootScreenResult = theHosDB.theFootScreenResultDB.getComboboxDatasources();
                theConnectionInf.getConnection().commit();
            } catch (Exception ex) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                try {
                    theConnectionInf.getConnection().rollback();
                } catch (SQLException ex1) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex1);
                }
            } finally {
                theConnectionInf.close();
            }
        }
        return theLO.vFootScreenResult;
    }

    public Vector<ComboFix> listFootProdoscopeType() {
        if (theLO.vFootProdoscopeType == null || theLO.vFootProdoscopeType.isEmpty()) {
            theLO.vFootProdoscopeType = new Vector();
            theConnectionInf.open();
            try {
                theConnectionInf.getConnection().setAutoCommit(false);
                theLO.vFootProdoscopeType = theHosDB.theFootProdoscopeTypeDB.getComboboxDatasources();
                theConnectionInf.getConnection().commit();
            } catch (Exception ex) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                try {
                    theConnectionInf.getConnection().rollback();
                } catch (SQLException ex1) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex1);
                }
            } finally {
                theConnectionInf.close();
            }
        }
        return theLO.vFootProdoscopeType;
    }

    public Vector<ComboFix> listAllergicReactionsType() {
        if (theLO.vAllergicReactionsType == null || theLO.vAllergicReactionsType.isEmpty()) {
            theLO.vAllergicReactionsType = new Vector();
            theConnectionInf.open();
            try {
                theConnectionInf.getConnection().setAutoCommit(false);
                theLO.vAllergicReactionsType = theHosDB.theAllergicReactionsTypeDB.getComboboxDatasources();
                theConnectionInf.getConnection().commit();
            } catch (Exception ex) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                try {
                    theConnectionInf.getConnection().rollback();
                } catch (SQLException ex1) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex1);
                }
            } finally {
                theConnectionInf.close();
            }
        }
        return theLO.vAllergicReactionsType;
    }

    public Vector<ComboFix> listWarningType() {
        if (theLO.vWarningType == null || theLO.vWarningType.isEmpty()) {
            theLO.vWarningType = new Vector();
            theConnectionInf.open();
            try {
                theConnectionInf.getConnection().setAutoCommit(false);
                theLO.vWarningType = theHosDB.theWarningTypeDB.getComboboxDatasources();
                theConnectionInf.getConnection().commit();
            } catch (Exception ex) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                try {
                    theConnectionInf.getConnection().rollback();
                } catch (SQLException ex1) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex1);
                }
            } finally {
                theConnectionInf.close();
            }
        }
        return theLO.vWarningType;
    }

    public List<CommonInf> listAllergyIcd10() {
        if (theLO.allergyIcd10s == null || theLO.allergyIcd10s.isEmpty()) {
            theLO.allergyIcd10s = new ArrayList<CommonInf>();
            theConnectionInf.open();
            try {
                theConnectionInf.getConnection().setAutoCommit(false);
                theLO.allergyIcd10s = theHosDB.theAllergyIcd10DB.getComboboxDatasource();
                theConnectionInf.getConnection().commit();
            } catch (Exception ex) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                try {
                    theConnectionInf.getConnection().rollback();
                } catch (SQLException ex1) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex1);
                }
            } finally {
                theConnectionInf.close();
            }
        }
        return theLO.allergyIcd10s;
    }

    public Vector<ComboFix> listAllergyType() {
        if (theLO.vAllergyType == null || theLO.vAllergyType.isEmpty()) {
            theLO.vAllergyType = new Vector();
            theConnectionInf.open();
            try {
                theConnectionInf.getConnection().setAutoCommit(false);
                theLO.vAllergyType = theHosDB.theAllergyTypeDB.getComboboxDatasources();
                theConnectionInf.getConnection().commit();
            } catch (Exception ex) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                try {
                    theConnectionInf.getConnection().rollback();
                } catch (SQLException ex1) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex1);
                }
            } finally {
                theConnectionInf.close();
            }
        }
        return theLO.vAllergyType;
    }

    public Vector<ComboFix> listNaranjoInterpretation() {
        if (theLO.vNaranjoInterpretation == null || theLO.vNaranjoInterpretation.isEmpty()) {
            theLO.vNaranjoInterpretation = new Vector();
            theConnectionInf.open();
            try {
                theConnectionInf.getConnection().setAutoCommit(false);
                theLO.vNaranjoInterpretation = theHosDB.theNaranjoInterpretationDB.getComboboxDatasources();
                theConnectionInf.getConnection().commit();
            } catch (Exception ex) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                try {
                    theConnectionInf.getConnection().rollback();
                } catch (SQLException ex1) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex1);
                }
            } finally {
                theConnectionInf.close();
            }
        }
        return theLO.vNaranjoInterpretation;
    }

    public List<NedReason> listNedReason() {
        if (theLO.vNedReason == null || theLO.vNedReason.isEmpty()) {
            theLO.vNedReason = new ArrayList<NedReason>();
            theConnectionInf.open();
            try {
                theConnectionInf.getConnection().setAutoCommit(false);
                theLO.vNedReason = theHosDB.theNedReasonDB.listAll();
                theConnectionInf.getConnection().commit();
            } catch (Exception ex) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                try {
                    theConnectionInf.getConnection().rollback();
                } catch (SQLException ex1) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex1);
                }
            } finally {
                theConnectionInf.close();
            }
        }
        return theLO.vNedReason;
    }

    public Vector listDrugDosageForm(String keyword) {
        return listDrugDosageForm(keyword, "1");
    }

    public Vector listDrugDosageForm(String keyword, String active) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theDrugDosageFormDB.selectByKeyword(keyword,
                    active == null || active.isEmpty() ? "1" : active);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public DrugDosageForm readDrugDosageFormById(String id) {
        return readDrugDosageFormById(id, false);
    }

    public DrugDosageForm readDrugDosageFormById(String id, boolean re_read) {
        DrugDosageForm vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (theLO.vDrugDosageForm == null || theLO.vDrugDosageForm.isEmpty()) {
                theLO.vDrugDosageForm = theHosDB.theDrugDosageFormDB.selectAll();
            }
            Vector vp = theLO.vDrugDosageForm;
            vc = (DrugDosageForm) readCommonInf(vp, id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public List<CommonInf> listAllergyLevels() {
        if (theLO.allergyLevels != null && !theLO.allergyLevels.isEmpty()) {
            return theLO.allergyLevels;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.allergyLevels = theHosDB.theAllergyLevelDB.getComboboxDatasource();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.allergyLevels;
    }

    public List<CommonInf> listAccidentPlaces() {
        if (theLO.accidentPlaces != null && !theLO.accidentPlaces.isEmpty()) {
            return theLO.accidentPlaces;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.accidentPlaces = theHosDB.theAccidentPlaceDB.getComboboxDatasource();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.accidentPlaces;
    }

    public List<CommonInf> listAccidentSysptomEyes() {
        if (theLO.accidentSysptomEyes != null && !theLO.accidentSysptomEyes.isEmpty()) {
            return theLO.accidentSysptomEyes;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.accidentSysptomEyes = theHosDB.theAccidentSysptomEyeDB.getComboboxDatasource();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.accidentSysptomEyes;
    }

    public AccidentSysptomEye getAccidentSysptomEye(String id) {
        AccidentSysptomEye ase = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ase = theHosDB.theAccidentSysptomEyeDB.select(id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ase;
    }

    public List<CommonInf> listAccidentSysptomMovements() {
        if (theLO.accidentSysptomMovements != null && !theLO.accidentSysptomMovements.isEmpty()) {
            return theLO.accidentSysptomMovements;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.accidentSysptomMovements = theHosDB.theAccidentSysptomMovementDB.getComboboxDatasource();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.accidentSysptomMovements;
    }

    public AccidentSysptomMovement getAccidentSysptomMovement(String id) {
        AccidentSysptomMovement asm = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            asm = theHosDB.theAccidentSysptomMovementDB.select(id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return asm;
    }

    public List<CommonInf> listAccidentSysptomSpeaks() {
        if (theLO.accidentSysptomSpeaks != null && !theLO.accidentSysptomSpeaks.isEmpty()) {
            return theLO.accidentSysptomSpeaks;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.accidentSysptomSpeaks = theHosDB.theAccidentSysptomSpeakDB.getComboboxDatasource();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.accidentSysptomSpeaks;
    }

    public AccidentSysptomSpeak getAccidentSysptomSpeak(String id) {
        AccidentSysptomSpeak ass = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ass = theHosDB.theAccidentSysptomSpeakDB.select(id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ass;
    }

    public List<CommonInf> listAccidentVisitTypes() {
        if (theLO.accidentVisitTypes != null && !theLO.accidentVisitTypes.isEmpty()) {
            return theLO.accidentVisitTypes;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.accidentVisitTypes = theHosDB.theAccidentVisitTypeDB.getComboboxDatasource();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.accidentVisitTypes;
    }

    public List<CommonInf> listAddressHouseTypes() {
        if (theLO.addressHouseTypes != null && !theLO.addressHouseTypes.isEmpty()) {
            return theLO.addressHouseTypes;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.addressHouseTypes = theHosDB.theAddressHouseTypeDB.getComboboxDatasource();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.addressHouseTypes;
    }

    public List<CommonInf> listAddressTypes() {
        if (theLO.addressTypes != null && !theLO.addressTypes.isEmpty()) {
            return theLO.addressTypes;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.addressTypes = theHosDB.theAddressTypeDB.getComboboxDatasource();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.addressTypes;
    }

    public List<CommonInf> listAllergyInformants() {
        if (theLO.allergyInformants != null && !theLO.allergyInformants.isEmpty()) {
            return theLO.allergyInformants;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.allergyInformants = theHosDB.theAllergyInformantDB.getComboboxDatasource();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.allergyInformants;
    }

    public List<CommonInf> listPersonForeigners() {
        if (theLO.personForeigners != null && !theLO.personForeigners.isEmpty()) {
            return theLO.personForeigners;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.personForeigners = theHosDB.thePersonForeignerTypeDB.getComboboxDatasource();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.personForeigners;
    }

    public List<CommonInf> listPersonVillageStatuss() {
        if (theLO.personVillageStatuss != null && !theLO.personVillageStatuss.isEmpty()) {
            return theLO.personVillageStatuss;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.personVillageStatuss = theHosDB.thePersonVillageStatusDB.getComboboxDatasource();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.personVillageStatuss;
    }

    public List<CommonInf> listProviderTypes() {
        if (theLO.providerTypes != null && !theLO.providerTypes.isEmpty()) {
            return theLO.providerTypes;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.providerTypes = theHosDB.theProviderTypeDB.getComboboxDatasource();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.providerTypes;
    }

    public List<CommonInf> listVisitServiceTypes() {
        if (theLO.visitServiceTypes != null && !theLO.visitServiceTypes.isEmpty()) {
            return theLO.visitServiceTypes;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.visitServiceTypes = theHosDB.theVisitServiceTypeDB.getComboboxDatasource();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.visitServiceTypes;
    }

    public List<CommonInf> listProviderCouncilCodes() {
        if (theLO.providerCouncilCodes != null && !theLO.providerCouncilCodes.isEmpty()) {
            return theLO.providerCouncilCodes;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.providerCouncilCodes = theHosDB.theProviderCouncilCodeDB.getComboboxDatasource();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.providerCouncilCodes;
    }

    public List<CommonInf> listPersonVillageStatus() {
        if (theLO.personVillageStatuses != null && !theLO.personVillageStatuses.isEmpty()) {
            return theLO.personVillageStatuses;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.personVillageStatuses = theHosDB.thePersonVillageStatusDB.getComboboxDatasource();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.personVillageStatuses;
    }

    public List<CommonInf> listItemLabSpecimens() {
        if (theLO.specimens != null && !theLO.specimens.isEmpty()) {
            return theLO.specimens;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.specimens = theHosDB.theSpecimenDB.getComboboxDatasource();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.specimens;
    }

    public List<CommonInf> listPersonAffiliateds() {
        if (theLO.personAffiliateds != null && !theLO.personAffiliateds.isEmpty()) {
            return theLO.personAffiliateds;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.personAffiliateds = theHosDB.thePersonAffiliatedDB.getComboboxDatasource();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.personAffiliateds;
    }

    public List<CommonInf> listPersonRanks() {
        if (theLO.personRanks != null && !theLO.personRanks.isEmpty()) {
            return theLO.personRanks;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.personRanks = theHosDB.thePersonRankDB.getComboboxDatasource();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.personRanks;
    }

    public List<CommonInf> listPersonJobTypes() {
        if (theLO.personJobTypes != null && !theLO.personJobTypes.isEmpty()) {
            return theLO.personJobTypes;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.personJobTypes = theHosDB.thePersonJobTypeDB.getComboboxDatasource();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.personJobTypes;
    }

    public List<CommonInf> listXrayResultSummaries() {
        if (theLO.xrayResultSummaries != null && !theLO.xrayResultSummaries.isEmpty()) {
            return theLO.xrayResultSummaries;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.xrayResultSummaries = theHosDB.theXrayResultSummaryDB.getComboboxDatasource();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.xrayResultSummaries;
    }

    public PaymentType readPaymentTypeById(String id) {
        for (int i = 0; i < theLO.paymentTypes.size(); i++) {
            PaymentType paymentType = (PaymentType) theLO.paymentTypes.get(i);
            if (paymentType.getObjectId().equals(id)) {
                return paymentType;
            }
        }
        return null;
    }

    public List<CommonInf> listPaymentTypes() {
        if (theLO.paymentTypes != null && !theLO.paymentTypes.isEmpty()) {
            return theLO.paymentTypes;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.paymentTypes = theHosDB.thePaymentTypeDB.getComboboxDatasource();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.paymentTypes;
    }

    public List<CommonInf> listPaymentTypes(String... typeIds) {
        List<CommonInf> list = new ArrayList<CommonInf>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            list.addAll(theHosDB.thePaymentTypeDB.getComboboxDatasource(typeIds));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public List<CommonInf> listInsuranceCompanies() {
        if (theLO.insuranceCompanies != null && !theLO.insuranceCompanies.isEmpty()) {
            return theLO.insuranceCompanies;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.insuranceCompanies = theHosDB.theFinanceInsuranceCompanyDB.getComboboxDatasource();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.insuranceCompanies;
    }

    public BankInfo readBankInfoById(String id) {
        for (int i = 0; i < theLO.banks.size(); i++) {
            BankInfo bankInfo = (BankInfo) theLO.banks.get(i);
            if (bankInfo.getObjectId().equals(id)) {
                return bankInfo;
            }
        }
        return null;
    }

    public List<CommonInf> listBanks() {
        if (theLO.banks != null && !theLO.banks.isEmpty()) {
            return theLO.banks;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.banks = theHosDB.theBankInfoDB.getComboboxDatasource();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.banks;
    }

    public List<CommonInf> listBanks(String keyword) {
        List<CommonInf> commonInfs = new ArrayList<CommonInf>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            commonInfs.addAll(theHosDB.theBankInfoDB.getComboboxDatasource(keyword));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return commonInfs;
    }

    public List<ItemSubgroupCheckCalculate> listItemSubgroupCheckCalculates() {
        if (theLO.itemSubgroupCheckCalculates != null && !theLO.itemSubgroupCheckCalculates.isEmpty()) {
            return theLO.itemSubgroupCheckCalculates;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.itemSubgroupCheckCalculates = theHosDB.theItemSubgroupCheckCalculateDB.listAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.itemSubgroupCheckCalculates;
    }

    public List<CommonInf> listCreditCardTypes() {
        return listCreditCardTypes(false);
    }

    public List<CommonInf> listCreditCardTypes(boolean isReload) {
        if (!isReload && theLO.creditCardTypes != null && !theLO.creditCardTypes.isEmpty()) {
            return theLO.creditCardTypes;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.creditCardTypes = theHosDB.theCreditCardTypeDB.getComboboxDatasource();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.creditCardTypes;
    }

    public Vector listPTypeDis() {
        return theLO.vPtypeDis;
    }

    public Vector listReferResult() {
        return theLO.vReferResult;
    }

    public Vector listCareType() {
        return theLO.vCareType;
    }

    public GovCode readGovCodeByCode(String code) {
        if ((code == null)) {
            return null;
        }
        if ((code.length() == 0)) {
            return null;
        }
        GovCode vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theGovCodeDB.select(code);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public GovCode readGovCodeById(String id) {
        for (int i = 0; i < theLO.govCodes.size(); i++) {
            GovCode obj = (GovCode) theLO.govCodes.get(i);
            if (obj.getObjectId().equals(id)) {
                return obj;
            }
        }
        return null;
    }

    public List<CommonInf> listGovCodes() {
        if (theLO.govCodes != null && !theLO.govCodes.isEmpty()) {
            return theLO.govCodes;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.govCodes = theHosDB.theGovCodeDB.getComboboxDatasource();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.govCodes;
    }

    public List<CommonInf> listGovCodes(String keyword) {
        List<CommonInf> commonInfs = new ArrayList<CommonInf>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            commonInfs.addAll(theHosDB.theGovCodeDB.getComboboxDatasource(keyword));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return commonInfs;
    }

    public SubInscl readSubInsclById(String id) {
        for (int i = 0; i < theLO.subInscls.size(); i++) {
            SubInscl obj = (SubInscl) theLO.subInscls.get(i);
            if (obj.getObjectId().equals(id)) {
                return obj;
            }
        }
        return null;
    }

    public List<CommonInf> listSubInscls() {
        if (theLO.subInscls != null && !theLO.subInscls.isEmpty()) {
            return theLO.subInscls;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.subInscls = theHosDB.theSubInsclDB.getComboboxDatasource();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.subInscls;
    }

    public List<CommonInf> listSubInscls(String keyword) {
        List<CommonInf> commonInfs = new ArrayList<CommonInf>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            commonInfs.addAll(theHosDB.theSubInsclDB.getComboboxDatasource(keyword));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return commonInfs;
    }

    public RelInscl readRelInsclById(String id) {
        for (int i = 0; i < theLO.relInscls.size(); i++) {
            RelInscl obj = (RelInscl) theLO.relInscls.get(i);
            if (obj.getObjectId().equals(id)) {
                return obj;
            }
        }
        return null;
    }

    public List<CommonInf> listRelInscls() {
        if (theLO.relInscls != null && !theLO.relInscls.isEmpty()) {
            return theLO.relInscls;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.relInscls = theHosDB.theRelInsclDB.getComboboxDatasource();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.relInscls;
    }

    public List<CommonInf> listRelInscls(String keyword) {
        List<CommonInf> commonInfs = new ArrayList<CommonInf>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            commonInfs.addAll(theHosDB.theRelInsclDB.getComboboxDatasource(keyword));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return commonInfs;
    }

    public List<CommonInf> listPersonForeignerType() {
        if (theLO.personForeignerTypes != null && !theLO.personForeignerTypes.isEmpty()) {
            return theLO.personForeignerTypes;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.personForeignerTypes = theHosDB.thePersonForeignerTypeDB.getComboboxDatasource();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.personForeignerTypes;
    }

    public List<CommonInf> listForeignerNoType() {
        List<CommonInf> commonInfs = new ArrayList<CommonInf>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            commonInfs.add(new ComboFix("0", "����к�"));
            commonInfs.add(new ComboFix("3", "˹ѧ��͵�ҧ����"));
            commonInfs.add(new ComboFix("4", "˹ѧ��͵�ҧ����/˹ѧ�������"));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return commonInfs;
    }

    public List<CommonInf> listICD10Accidents(String keyword, int limit) {
        List<CommonInf> commonInfs = new ArrayList<CommonInf>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector<ICD10> icd10s = theHosDB.theICD10DB.selectAccidentByIdName(keyword, limit);
            if (keyword.isEmpty()) {
                commonInfs.add(new ComboFix("", "00 : ������غѵ��˵�"));
            }
            for (ICD10 icd10 : icd10s) {
                commonInfs.add(new ComboFix(icd10.icd10_id, icd10.icd10_id + " : " + (icd10.icd10_description_th == null
                        || icd10.icd10_description_th.isEmpty() ? icd10.description : icd10.icd10_description_th)));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return commonInfs;
    }

    public List<CommonInf> listICD10ST(String keyword) {
        List<CommonInf> commonInfs = new ArrayList<CommonInf>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector<ICD10> icd10s = theHosDB.theICD10DB.selectStartWithST(keyword);
            for (ICD10 icd10 : icd10s) {
                commonInfs.add(new ComboFix(icd10.icd10_id, icd10.icd10_id + " : " + (icd10.icd10_description_th == null
                        || icd10.icd10_description_th.isEmpty() ? icd10.description : icd10.icd10_description_th)));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return commonInfs;
    }

    public ICD10 readICD10ByCode(String code) {
        if ((code == null)) {
            return null;
        }
        if ((code.length() == 0)) {
            return null;
        }
        ICD10 vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theICD10DB.selectAllByCode(code);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public BVisitBed readBVisitBed(String b_visit_bed_id) {
        BVisitBed vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theBVisitBedDB.selectById(b_visit_bed_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public List<CommonInf> listProductCategory() {
        if (theLO.productCategoryies != null && !theLO.productCategoryies.isEmpty()) {
            return theLO.productCategoryies;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.productCategoryies = theHosDB.theProductCategoryDB.getComboboxDatasource();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.productCategoryies;
    }

    public List<CommonInf> listDrugSpecPrep() {
        if (theLO.drugSpecPreps != null && !theLO.drugSpecPreps.isEmpty()) {
            return theLO.drugSpecPreps;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.drugSpecPreps = theHosDB.theDrugSpecPrepDB.getComboboxDatasource();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.drugSpecPreps;
    }

    public ItemManufacturer readItemManufacturerById(String id) {
        for (int i = 0; i < theLO.itemManufacturers.size(); i++) {
            ItemManufacturer im = (ItemManufacturer) theLO.itemManufacturers.get(i);
            if (im.getObjectId().equals(id)) {
                return im;
            }
        }
        return null;
    }

    public List<CommonInf> listItemManufacturers() {
        if (theLO.itemManufacturers != null && !theLO.itemManufacturers.isEmpty()) {
            return theLO.itemManufacturers;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.itemManufacturers = theHosDB.theItemManufacturerDB.getComboboxDatasource();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.itemManufacturers;
    }

    public List<CommonInf> listItemManufacturers(String keyword) {
        List<CommonInf> commonInfs = new ArrayList<CommonInf>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            commonInfs.addAll(theHosDB.theItemManufacturerDB.getComboboxDatasource(keyword));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return commonInfs;
    }

    public ItemDistributor readItemDistributorById(String id) {
        for (int i = 0; i < theLO.itemDistributors.size(); i++) {
            ItemDistributor im = (ItemDistributor) theLO.itemDistributors.get(i);
            if (im.getObjectId().equals(id)) {
                return im;
            }
        }
        return null;
    }

    public List<CommonInf> listItemDistributors() {
        if (theLO.itemDistributors != null && !theLO.itemDistributors.isEmpty()) {
            return theLO.itemDistributors;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.itemDistributors = theHosDB.theItemDistributorDB.getComboboxDatasource();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.itemDistributors;
    }

    public List<CommonInf> listItemDistributors(String keyword) {
        List<CommonInf> commonInfs = new ArrayList<CommonInf>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            commonInfs.addAll(theHosDB.theItemDistributorDB.getComboboxDatasource(keyword));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return commonInfs;
    }

    public List<CommonInf> listPatientFamilyHistoryType() {
        if (theLO.patientFamilyHistoryTypes == null || theLO.patientFamilyHistoryTypes.isEmpty()) {
            theLO.patientFamilyHistoryTypes = new ArrayList<CommonInf>();
            theConnectionInf.open();
            try {
                theConnectionInf.getConnection().setAutoCommit(false);
                theLO.patientFamilyHistoryTypes = theHosDB.theFPatientFamilyHistoryDB.getComboboxDatasource();
                theConnectionInf.getConnection().commit();
            } catch (Exception ex) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                try {
                    theConnectionInf.getConnection().rollback();
                } catch (SQLException ex1) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex1);
                }
            } finally {
                theConnectionInf.close();
            }
        }
        return theLO.patientFamilyHistoryTypes;
    }

    public List<ComboFix> listEDCTransactionCode(String type) {
        List<ComboFix> list = new ArrayList<ComboFix>();
        theConnectionInf.open();
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String sql = "select * from f_govoffical_plan_transaction_code where tx_type = ? order by id";
            preparedStatement = theConnectionInf.ePQuery(sql);
            preparedStatement.setString(1, type);
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                ComboFix comboFix = new ComboFix();
                comboFix.code = rs.getString("id");
                comboFix.name = rs.getString("description");
                list.add(comboFix);
            }
            theConnectionInf.getConnection().commit();
        } catch (SQLException ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex);
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex);
                }
            }
            theConnectionInf.close();
        }
        return list;
    }

    public List<ComboFix> listMedicalCertificateType() {
        List<ComboFix> list = new ArrayList<ComboFix>();
        theConnectionInf.open();
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String sql = "select * from f_medical_certificate_type";
            preparedStatement = theConnectionInf.ePQuery(sql);
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                ComboFix comboFix = new ComboFix();
                comboFix.code = rs.getString("id");
                comboFix.name = rs.getString("description");
                list.add(comboFix);
            }
            theConnectionInf.getConnection().commit();
        } catch (SQLException ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex);
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    LOG.log(java.util.logging.Level.SEVERE, null, ex);
                }
            }
            theConnectionInf.close();
        }
        return list;
    }

    public ComboFix getReferTypeById(String id) {
        List<ComboFix> list = listReferTypes();
        for (ComboFix comboFix : list) {
            if (comboFix.other.equals(id)) {
                return comboFix;
            }
        }
        return new ComboFix(null, "");
    }

    public ComboFix getReferTypeByCode(String code) {
        List<ComboFix> list = listReferTypes();
        for (ComboFix comboFix : list) {
            if (comboFix.code.equals(code)) {
                return comboFix;
            }
        }
        return new ComboFix(null, "");
    }

    public List<ComboFix> listReferTypes() {
        if (theLO.referTypes != null && !theLO.referTypes.isEmpty()) {
            return theLO.referTypes;
        }
        theConnectionInf.open();
        try {
            theLO.referTypes = new ArrayList<ComboFix>();
            theConnectionInf.getConnection().setAutoCommit(false);
            PreparedStatement preparedStatement = null;
            ResultSet rs = null;
            try {
                String sql = "select * from f_refer_type";
                preparedStatement = theConnectionInf.ePQuery(sql);
                rs = preparedStatement.executeQuery();
                while (rs.next()) {
                    ComboFix obj = new ComboFix();
                    obj.other = rs.getString("f_refer_type_id");
                    obj.name = rs.getString("description");
                    obj.code = rs.getString("code");
                    theLO.referTypes.add(obj);
                }
            } finally {
                if (rs != null) {
                    rs.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
            }
            theConnectionInf.getConnection().commit();
        } catch (SQLException ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.referTypes;
    }

    public List<CommonInf> listTariff() {
        if ((theLO.vTariff != null)) {
            return theLO.vTariff;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.vTariff = theHosDB.theBTariffDB.getComboboxDatasource();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.vTariff;
    }

    public Vector listItemDrugByName(String description) {
        Vector v = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            v = theHC.theSetupControl.listSearchItemDrug(description);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return v;
    }

    public int readScoreFromCardiovascularType(String id) {
        if (theLO.vCardiovascularType == null || theLO.vCardiovascularType.isEmpty() || id == null) {
            return 0;
        }
        for (CardiovascularType cardiovascularType : theLO.vCardiovascularType) {
            if (cardiovascularType.getObjectId().equals(id)) {
                return cardiovascularType.cap_score;
            }
        }
        return 0;
    }

    public List<CardiovascularType> listCardiovascularType() {
        if ((theLO.vCardiovascularType != null)) {
            return theLO.vCardiovascularType;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.vCardiovascularType = theHosDB.theCardiovascularTypeDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.vCardiovascularType;
    }

    public int readScoreFromAvpuType(String id) {
        if (theLO.vAvpuType == null || theLO.vAvpuType.isEmpty() || id == null) {
            return 0;
        }
        for (AvpuType avpuType : theLO.vAvpuType) {
            if (avpuType.getObjectId().equals(id)) {
                return avpuType.avpu_score;
            }
        }
        return 0;
    }

    public List<AvpuType> listAvpuType() {
        if ((theLO.vAvpuType != null)) {
            return theLO.vAvpuType;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.vAvpuType = theHosDB.theAvpuTypeDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.vAvpuType;
    }

    public int readScoreFromBehaviorType(String id) {
        if (theLO.vBehaviorType == null || theLO.vBehaviorType.isEmpty() || id == null) {
            return 0;
        }
        for (BehaviorType behaviorType : theLO.vBehaviorType) {
            if (behaviorType.getObjectId().equals(id)) {
                return behaviorType.behavior_score;
            }
        }
        return 0;
    }

    public List<BehaviorType> listBehaviorType() {
        if ((theLO.vBehaviorType != null)) {
            return theLO.vBehaviorType;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.vBehaviorType = theHosDB.theBehaviorTypeDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.vBehaviorType;
    }

    public Vector<ComboFix> listAnswer() {
        Vector<ComboFix> vc = new Vector<>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theAnswerDB.selectAllNotOrderBy(1);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector<ServicePoint> listServicePointOPD(String keyword) {
        Vector<ServicePoint> vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theServicePointDB.selectAllOPDByName(keyword, Active.isEnable());
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public List listLabOrderCause() {
        if ((theLO.theLabOrderCause != null)) {
            return theLO.theLabOrderCause;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.theLabOrderCause = theHosDB.theLabOrderCauseDB.list();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.theLabOrderCause;
    }

    public List<CommonInf> listCountries() {
        if (theLO.countries != null && !theLO.countries.isEmpty()) {
            return theLO.countries;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLO.countries = new ArrayList<>();
            theLO.countries.addAll(theHosDB.theFCountryDB.list());
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theLO.countries;
    }

    public List<CommonInf> listCountries(String keyword) {
        List<CommonInf> commonInfs = new ArrayList<>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            commonInfs.addAll(theHosDB.theFCountryDB.list(keyword));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return commonInfs;
    }

    public List<CommonInf> listLabAtkProductByKeyword(String keyword) {
        List<CommonInf> v = new ArrayList<>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (LabAtkProduct obj : theHosDB.theLabAtkProductDB.listByKeyword(keyword)) {
                v.add((CommonInf) obj);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return v;
    }

    public List<CategoryGroupItem> listCategoryGroupItemSpecial(String cat, boolean isSelectByParam) {
        List<CategoryGroupItem> list = new ArrayList<>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector<CategoryGroupItem> vc = theHosDB.theCategoryGroupItemDB.selectAllExceptCategoryGroupCode(cat, "1", isSelectByParam);
            for (CategoryGroupItem cgi : vc) {
                list.add(cgi);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public Date getCurrentTimeStamp() {
        Date date = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            date = intReadTimeStamp(theConnectionInf);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return date;
    }

    public List<CommonInf> listUcmItemUnit(String key) {
        List<CommonInf> list = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            list = theHosDB.theUcumItemUnitDB.getComboboxDatasource(key);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public List<CommonInf> listEdqmItemRoute(String key) {
        List<CommonInf> list = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            list = theHosDB.theEdqmItemRouteDB.getComboboxDatasource(key);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public List<CommonInf> listHl7VaccineCode(String keyword) {
        List<CommonInf> list = new ArrayList<>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            list.addAll(theHosDB.theHl7VaccineCodeDB.getComboboxDatasource(keyword));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public List<CommonInf> listHl7ActSite(String keyword) {
        List<CommonInf> commonInfs = new ArrayList<>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            commonInfs.addAll(theHosDB.theHl7ActSiteDB.getComboboxDatasource(keyword));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return commonInfs;
    }

    public List<CommonInf> listHl7ImmunizationRoute(String keyword) {
        List<CommonInf> commonInfs = new ArrayList<>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            commonInfs.addAll(theHosDB.theHl7ImmunizationRouteDB.getComboboxDatasource(keyword));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return commonInfs;
    }

    public List<CommonInf> listVisitAdmitType() {
        List<CommonInf> list = new ArrayList<>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            list.addAll(theHosDB.theVisitAdmitTypeDB.getComboboxDatasource());
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public List<CommonInf> listVisitAdmitSource() {
        List<CommonInf> list = new ArrayList<>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            list.addAll(theHosDB.theVisitAdmitSourceDB.getComboboxDatasource());
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public List<MMrcLevel> listMMrcLevel() {
        List<MMrcLevel> list = new ArrayList<>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            list = theHosDB.theMMrcLevelDB.selectAll();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public List<Employee> listDoctorByClinicAndName(String clinicId, String name) {
        List<Employee> vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = new ArrayList<>();
            vc.addAll(theHosDB.theEmployeeDB.selectByClinicAndDoctorName(clinicId, name));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public List<Employee> listDoctorByClinic(String clinicId) {
        List<Employee> vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = new ArrayList<>();
            vc.addAll(theHosDB.theEmployeeDB.selectByClinicAndDoctorName(clinicId, ""));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public boolean intCheckDoctorAndClinicSchSchedule(String doctorId, String clinicId) throws Exception {
        String sql = "select COUNT(*) > 0 AS result_found\n"
                + "from sch.sch_schedule \n"
                + "where doctor = ? and clinic = ?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, doctorId);
            ePQuery.setString(index++, clinicId);
            List<Object[]> list = theConnectionInf.eComplexQuery(ePQuery.toString());
            return (boolean) ((Object[]) list.get(0))[0];
        }
    }

}

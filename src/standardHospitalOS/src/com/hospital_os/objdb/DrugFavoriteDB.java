/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.DrugFavorite;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class DrugFavoriteDB {

    public ConnectionInf connectionInf;
    final public String tableId = "835";

    public DrugFavoriteDB(ConnectionInf db) {
        connectionInf = db;
    }

    public int insert(DrugFavorite obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO b_item_drug_favorite(\n"
                    + "            b_item_drug_favorite_id, doctor_id, b_visit_clinic_id, b_item_id, usage_special,"
                    + "usage_text, caution, description, instruction_id,"
                    + "dose, use_uom_id, frequency_id, qty, purch_uom_id, b_hstock_item_id)\n"
                    + "    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getGenID(tableId));
            preparedStatement.setString(2, obj.doctor_id);
            preparedStatement.setString(3, obj.b_visit_clinic_id);
            preparedStatement.setString(4, obj.b_item_id);
            preparedStatement.setString(5, obj.usage_special);
            preparedStatement.setString(6, obj.usage_text);
            preparedStatement.setString(7, obj.caution);
            preparedStatement.setString(8, obj.description);
            preparedStatement.setString(9, obj.instruction_id);
            preparedStatement.setString(10, obj.dose);
            preparedStatement.setString(11, obj.use_uom_id);
            preparedStatement.setString(12, obj.frequency_id);
            preparedStatement.setString(13, obj.qty);
            preparedStatement.setString(14, obj.purch_uom_id);
            preparedStatement.setString(15, obj.b_hstock_item_id);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(DrugFavorite obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE b_item_drug_favorite\n"
                    + "   SET usage_special=?, \n"
                    + "       usage_text=?, caution=?, description=?, \n"
                    + "       instruction_id=?, dose=?, use_uom_id=?, frequency_id=?, qty=?, \n"
                    + "       purch_uom_id=?\n"
                    + " WHERE b_item_drug_favorite_id=?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.usage_special);
            preparedStatement.setString(2, obj.usage_text);
            preparedStatement.setString(3, obj.caution);
            preparedStatement.setString(4, obj.description);
            preparedStatement.setString(5, obj.instruction_id);
            preparedStatement.setString(6, obj.dose);
            preparedStatement.setString(7, obj.use_uom_id);
            preparedStatement.setString(8, obj.frequency_id);
            preparedStatement.setString(9, obj.qty);
            preparedStatement.setString(10, obj.purch_uom_id);
            preparedStatement.setString(11, obj.getObjectId());
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(DrugFavorite obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM b_item_drug_favorite\n");
            sql.append(" WHERE b_item_drug_favorite_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public DrugFavorite selectByDoctorAndClinicAndItem(String doctorId, String clinicId, String itemId) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select b_item_drug_favorite.*, b_item.item_common_name as item_name from b_item_drug_favorite "
                    + "inner join b_item on b_item.b_item_id = b_item_drug_favorite.b_item_id "
                    + "where doctor_id = ? and b_visit_clinic_id = ? and b_item_drug_favorite.b_item_id = ? order by  b_item.item_common_name";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, doctorId);
            preparedStatement.setString(2, clinicId);
            preparedStatement.setString(3, itemId);
            List<DrugFavorite> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<DrugFavorite> selectByDoctorAndClinic(String doctorId, String clinicId) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select b_item_drug_favorite.*, b_item.item_common_name as item_name from b_item_drug_favorite "
                    + "inner join b_item on b_item.b_item_id = b_item_drug_favorite.b_item_id "
                    + "where doctor_id = ? and b_visit_clinic_id = ? order by  b_item.item_common_name";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, doctorId);
            preparedStatement.setString(2, clinicId);
            List<DrugFavorite> list = executeQuery(preparedStatement);
            return list;
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<DrugFavorite> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<DrugFavorite> list = new ArrayList<>();
        try (ResultSet rs = preparedStatement.executeQuery()) {
// execute SQL stetement
            while (rs.next()) {
                DrugFavorite obj = new DrugFavorite();
                obj.setObjectId(rs.getString("b_item_drug_favorite_id"));
                obj.b_visit_clinic_id = rs.getString("b_visit_clinic_id");
                obj.b_item_id = rs.getString("b_item_id");
                obj.doctor_id = rs.getString("doctor_id");
                obj.usage_special = rs.getString("usage_special");
                obj.usage_text = rs.getString("usage_text");
                obj.caution = rs.getString("caution");
                obj.description = rs.getString("description");
                obj.instruction_id = rs.getString("instruction_id");
                obj.dose = rs.getString("dose");
                obj.use_uom_id = rs.getString("use_uom_id");
                obj.frequency_id = rs.getString("frequency_id");
                obj.qty = rs.getString("qty");
                obj.purch_uom_id = rs.getString("purch_uom_id");
                try {
                    obj.itemName = rs.getString("item_name");
                } catch (SQLException ex) {
                }
                try {
                    obj.b_hstock_item_id = rs.getString("b_hstock_item_id");
                } catch (Exception ex) {
                }
                list.add(obj);
            }
            return list;
        }
    }
}

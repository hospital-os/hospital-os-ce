package com.hospital_os.objdb;

import com.hospital_os.object.Option;
import com.hospital_os.object.OptionDetail;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.util.Vector;

@SuppressWarnings("UseOfObsoleteCollectionType")
public class Option2DB extends OptionDB {

    OptionDetailDB theOptionDetailDB;

    public Option2DB(ConnectionInf db, OptionDetailDB oddb) {
        super(db);
//        theConnectionInf = db;
        setDepDB(oddb);
    }

    private void setDepDB(OptionDetailDB oddb) {
        theOptionDetailDB = oddb;
    }

    /**
     * @authen Henbe pongtorn @date 24/05/06 @������繿ѧ�ѹ�������ö�ѹ�֡
     * option �����ҡ������
     */
    @Override
    public int insert(Option o) throws Exception {
        Vector odv = o.getOptionDetailV();
        int ret = 0;
        ret += theOptionDetailDB.deleteAll();
        for (int i = 0; i < odv.size(); i++) {
            OptionDetail od = (OptionDetail) odv.get(i);
            ret += theOptionDetailDB.insert(od);
        }
        return ret;
    }

    @Override
    public int update(Option o) throws Exception {
        return insert(o);
    }
    //////////////////////////////////////////////////////////////////////////////

    @Override
    public int delete(Option o) throws Exception {
        return theOptionDetailDB.deleteAll();
    }
    //////////////////////////////////////////////////////////////////////////////

    public Option select() {
        try {
            Vector odv = theOptionDetailDB.selectAll();
            Option opt = new Option(odv);
            return opt;
        } catch (Exception e) {
            return new Option();
        }
    }
    //////////////////////////////////////////////////////////////////////////////    
}

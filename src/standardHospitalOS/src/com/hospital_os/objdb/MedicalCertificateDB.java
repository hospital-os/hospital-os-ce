/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.MedicalCertificate;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class MedicalCertificateDB {

    private final ConnectionInf connectionInf;

    public MedicalCertificateDB(ConnectionInf c) {
        this.connectionInf = c;
    }

    public int insert(MedicalCertificate obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO t_medical_certificate(\n"
                    + "  hn, vn, doctor_id,"
                    + "  f_medical_certificate_type_id, medical_certificate_no, issue_date,"
                    + "  details, active,"
                    + "  user_record_id, user_update_id)\n"
                    + "    VALUES (?, ?, ?, ?, ?, ?, ?::JSONB, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.hn);
            preparedStatement.setString(index++, obj.vn);
            preparedStatement.setString(index++, obj.doctor_id);
            preparedStatement.setInt(index++, obj.f_medical_certificate_type_id);
            preparedStatement.setString(index++, obj.medical_certificate_no);
            preparedStatement.setDate(index++, new java.sql.Date(obj.issue_date.getTime()));
            preparedStatement.setString(index++, obj.details);
            preparedStatement.setBoolean(index++, obj.active);
            preparedStatement.setString(index++, obj.user_record_id);
            preparedStatement.setString(index++, obj.user_update_id);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int updatePrintCountByNo(String no) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_medical_certificate\n");
            sql.append("   SET print_count=print_count::int+1\n");
            sql.append(" WHERE medical_certificate_no = ?");
            int index = 1;
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(index++, no);
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int inActive(MedicalCertificate obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_medical_certificate\n");
            sql.append("   SET active=?, user_cancel_id=?, cancel_datetime = current_timestamp\n");
            sql.append(" WHERE id = ?");
            int index = 1;
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setBoolean(index++, obj.active);
            preparedStatement.setString(index++, obj.user_update_id);
            preparedStatement.setInt(index++, Integer.parseInt(obj.getObjectId()));
            // execute update SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
//

    public MedicalCertificate select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select t_medical_certificate.*, f_medical_certificate_type.description as type_name\n"
                    + "from t_medical_certificate \n"
                    + "inner join f_medical_certificate_type on f_medical_certificate_type.id = t_medical_certificate.f_medical_certificate_type_id\n"
                    + "where id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<MedicalCertificate> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public MedicalCertificate selectByNumber(String no) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select t_medical_certificate.*, f_medical_certificate_type.description as type_name\n"
                    + "from t_medical_certificate \n"
                    + "inner join f_medical_certificate_type on f_medical_certificate_type.id = t_medical_certificate.f_medical_certificate_type_id\n"
                    + "where medical_certificate_no = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, no);
            List<MedicalCertificate> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<MedicalCertificate> select(String search, Integer... types) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select t_medical_certificate.*, f_medical_certificate_type.description as type_name\n"
                    + "from t_medical_certificate\n"
                    + "inner join f_medical_certificate_type on f_medical_certificate_type.id = t_medical_certificate.f_medical_certificate_type_id\n"
                    + "where true \n"
                    + "and (hn = ? or vn = ? or medical_certificate_no = ?) \n";
            if (types != null && types.length > 0) {
                sql += "and f_medical_certificate_type_id = any (?)\n";
            }
            sql += "and active  = true\n"
                    + "order by issue_date desc";

            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, search);
            preparedStatement.setString(2, search);
            preparedStatement.setString(3, search);
            if (types != null && types.length > 0) {
                preparedStatement.setArray(4, connectionInf.getConnection().createArrayOf("integer", types));
            }
            List<MedicalCertificate> list = executeQuery(preparedStatement);
            return list;
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public String selectMaxNoByType(int typeId) throws Exception {
        String ret = "0";
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            String sql = "Select MAX(medical_certificate_no) as max From t_medical_certificate where f_medical_certificate_type_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setInt(1, typeId);
            resultSet = connectionInf.eQuery(preparedStatement.toString());
            if (resultSet.next()) {
                ret = resultSet.getString(1);
            }
        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
        return ret;
    }

    public MedicalCertificate list(String hn, int typeId) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select t_medical_certificate.*, f_medical_certificate_type.description as type_name\n"
                    + "from t_medical_certificate \n"
                    + "inner join f_medical_certificate_type on f_medical_certificate_type.id = t_medical_certificate.f_medical_certificate_type_id\n"
                    + "where hn = ? "
                    + (typeId == 0 ? "" : "and f_medical_certificate_type_id = ?")
                    + " order by issue_date desc";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, hn);
            if (typeId != 0) {
                preparedStatement.setInt(2, typeId);
            }
            List<MedicalCertificate> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<MedicalCertificate> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<MedicalCertificate> list = new ArrayList<MedicalCertificate>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                MedicalCertificate obj = new MedicalCertificate();
                obj.setObjectId(rs.getString("id"));
                obj.hn = rs.getString("hn");
                obj.vn = rs.getString("vn");
                obj.doctor_id = rs.getString("doctor_id");
                obj.f_medical_certificate_type_id = rs.getInt("f_medical_certificate_type_id");
                obj.medical_certificate_no = rs.getString("medical_certificate_no");
                obj.issue_date = rs.getDate("issue_date");
                obj.details = rs.getString("details");
                obj.active = rs.getBoolean("active");
                obj.user_record_id = rs.getString("user_record_id");
                obj.record_datetime = rs.getTimestamp("record_datetime");
                obj.user_update_id = rs.getString("user_update_id");
                obj.update_datetime = rs.getTimestamp("update_datetime");
                obj.user_cancel_id = rs.getString("user_cancel_id");
                obj.cancel_datetime = rs.getTimestamp("cancel_datetime");
                try {
                    obj.typeName = rs.getString("type_name");
                } catch (Exception ex) {

                }
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }
}

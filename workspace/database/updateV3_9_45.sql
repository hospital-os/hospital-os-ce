CREATE OR REPLACE FUNCTION text_to_timestamp(text) RETURNS timestamp language plpgsql immutable as $$
BEGIN
        return case when length($1) >= 10 and ((substr($1,1,4)::int-543)||substr($1,5))::timestamp is not null then ((substr($1,1,4)::int-543)||substr($1,5))::timestamp end;
exception when others then
        return null;
END;$$;

-- for PP and NCD
ALTER TABLE b_site ADD COLUMN is_hospital_nhso boolean NOT NULL default false; --false = ไม่ใช่ไซต์ใน สปสช. กทม   ,true = ไซต์ใน สปสช. กทม

ALTER TABLE t_visit  ADD COLUMN ipd_discharge_doctor character varying(30);

alter table t_visit_primary_symptom add column visit_primary_symptom_check_date character varying(10);
alter table t_visit_primary_symptom add column visit_primary_symptom_check_time character varying(5);
update t_visit_primary_symptom set visit_primary_symptom_check_date = substring(record_date_time, 0, 11),
visit_primary_symptom_check_time = substring(record_date_time, 12, 5);


-- PCU Module
alter table t_health_anc add column is_anc_place_other character varying(1) NOT NUlL default '0';
update t_health_anc set is_anc_place_other = '1' 
where 
health_anc_notice is not null and
health_anc_notice <> '' and
substring(health_anc_notice, 0, 5) = '0000';

alter table t_health_anc add column anc_place_hcode character varying(5) NOT NUlL default '00000';
update t_health_anc set anc_place_hcode = b_site.b_visit_office_id
from b_site
where 
health_anc_notice is  null or
health_anc_notice = '' or
substring(health_anc_notice, 0, 5) <> '0000';

update t_health_anc set health_anc_notice = substring(health_anc_notice, 5)
where 
health_anc_notice is not null and
health_anc_notice <> '' and
substring(health_anc_notice, 0, 5) = '0000';

insert into f_health_anc_section values ('', 'ไม่อยู่ในช่วงของการฝากครรภ์');

ALTER TABLE t_health_visit_home
   ALTER COLUMN visit_home_problem TYPE text;
ALTER TABLE t_health_visit_home
   ALTER COLUMN visit_home_object TYPE text;
ALTER TABLE t_health_visit_home
   ALTER COLUMN visit_home_maintain TYPE text;
ALTER TABLE t_health_visit_home
   ALTER COLUMN visit_home_assess TYPE text;
ALTER TABLE t_health_visit_home
   ALTER COLUMN visit_home_plane TYPE text;
ALTER TABLE t_health_visit_home
   ALTER COLUMN visit_home_remark TYPE text;


-- update db version
INSERT INTO s_version VALUES ('9701000000078', '78', 'Hospital OS, Community Edition', '3.9.45', '3.28.0', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_45.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.45');
select  distinct
b_item.b_item_id as b_item_id
,b_item.b_item_id as "HospDrugCode"
,case when b_map_product_category.f_product_category_id is null then '' else b_map_product_category.f_product_category_id end as "ProductCat"
,case when b_map_drug_tmt.b_drug_tmt_tpucode is null then '' else b_map_drug_tmt.b_drug_tmt_tpucode end as "TMTID"
,'' as "SpecPrep"
,b_item.item_common_name as "GenericName"
,b_item.item_trade_name as "TradeName"
,'' as "DFSCode"
,case when b_item_drug_dosage_form.form_name is null then '' else b_item_drug_dosage_form.form_name end as "DosageForm"
,case when b_item_drug.drug_strength is null then '' else b_item_drug.drug_strength end  as "Strength"
,case when b_item_drug.drug_unit_packing is null then '' else b_item_drug.drug_unit_packing end as "Content"
,b_item_price.item_price as "UnitPrice"
,case when b_item_distributor.item_distributor_description is null then '' else b_item_distributor.item_distributor_description end  as "Distributor"
,case when b_item_manufacturer.item_manufacturer_description is null then '' else b_item_manufacturer.item_manufacturer_description end  as "Manufacture"
,case when b_item.b_item_subgroup_id  in ('1300000000001')
            or b_item_subgroup.item_subgroup_description ilike '%ยาในบัญชี%'-- ยาในบัญชียาหลัก
            then 'E' -- E : เป็นยาที่อยู่ในบัญชียาหลักแห่งชาติ (ED)
            else 'N' -- N : เป็นบาที่อยู่นอกบัญชียาหลักแห่งชาติ (Non?ED)
            end as "ISED"
,case when b_nhso_drugcode24.drugcode24::text is null then '' else b_nhso_drugcode24.drugcode24::text end as "NDC24"
,'' as "Packsize"
,'' as "PackPrice"
,unnest((case when b_item.item_active = '1' and t_log_export_drug_catalog.b_item_id is null
                then array['A']
           when b_item.item_active = '0' and t_log_export_drug_catalog.b_item_id is not null
                then array['D']
          else
           (case when b_item.item_active = '1' and t_log_export_drug_catalog.b_item_id is not null
                    and '2555-10-01' <= ((substr(b_item.modify_datetime::text,1,4)::int + 543) || substr(b_item.modify_datetime::text,5,6))
                    and to_char(b_item.modify_datetime,'YYYY-MM-DD,HH24:MI')  <> to_char(to_timestamp(t_log_export_drug_catalog.datechange,'DD/MM/YYYY HH24:MI'),'YYYY-MM-DD,HH24:MI')

                then array['E'] end)
           ||
           (case when b_item.item_active = '1' and t_log_export_drug_catalog.b_item_id is not null
                    and '2555-10-01' <= ((substr(b_item_price.record_datetime::text,1,4)::int + 543) || substr(b_item_price.record_datetime::text,5,6))
                    and to_char(b_item_price.record_datetime,'YYYY-MM-DD,HH24:MI')  <> to_char(to_timestamp(t_log_export_drug_catalog.dateeffective,'DD/MM/YYYY HH24:MI'),'YYYY-MM-DD,HH24:MI')
                then array['U'] end)
           end )) as "UpdateFlag"


,substr(b_item.modify_datetime::text,9,2)||'/'||substr(b_item.modify_datetime::text,6,2)
    ||'/'||(substr(b_item.modify_datetime::text,1,4)::int)||' '||substr(b_item.modify_datetime::text,12,5) as "DateChange"
,case when strpos(b_item_price.item_price_active_date, ',') = 0 and length(b_item_price.item_price_active_date) = 10
            then substr(b_item_price.item_price_active_date,9,2)||'/'||substr(b_item_price.item_price_active_date,6,2)
                    ||'/'||(substr(b_item_price.item_price_active_date,1,4)::int)-543||' 00:00'
        when strpos(b_item_price.item_price_active_date, ',') = 11
            then substr(b_item_price.item_price_active_date,9,2)||'/'||substr(b_item_price.item_price_active_date,6,2)
                    ||'/'||(substr(b_item_price.item_price_active_date,1,4)::int)-543||' '||substr(b_item_price.item_price_active_date,12,5)
            else '' end as "DateUpdate"
,substr(b_item_price.record_datetime::text,9,2)||'/'||substr(b_item_price.record_datetime::text,6,2)
    ||'/'||(substr(b_item_price.record_datetime::text,1,4)::int) ||' '||substr(b_item_price.record_datetime::text,12,5) as "DateEffective"


 from
b_item inner join b_map_drug_tmt on b_item.b_item_id = b_map_drug_tmt.b_item_id
left join b_item_drug on  b_item.b_item_id = b_item_drug.b_item_id
left join b_item_subgroup on b_item.b_item_subgroup_id = b_item_subgroup.b_item_subgroup_id
left join b_map_product_category on  b_item.b_item_id = b_map_product_category.b_item_id
left join b_item_drug_dosage_form on b_item_drug.b_item_drug_dosage_form_id = b_item_drug_dosage_form.b_item_drug_dosage_form_id
left join b_item_manufacturer on b_item_drug.b_item_manufacturer_id = b_item_manufacturer.b_item_manufacturer_id
left join b_item_distributor on b_item_drug.b_item_distributor_id = b_item_distributor.b_item_distributor_id


inner join (select distinct
                        b_item_price.b_item_id
                        ,b_item_price.item_price
                        ,b_item_price.item_price_active_date as item_price_active_date
                        ,b_item_price.record_datetime
                from b_item_price inner join
                        (select distinct
                                b_item_price.b_item_id as b_item_id
                                ,max(case when length(trim(item_price_active_date)) = 10  and item_price_active_date ilike '%-%'
                                                then item_price_active_date||',00:00:00'
                                            when length(trim(item_price_active_date)) = 16 and item_price_active_date ilike '%-%'
                                                then item_price_active_date||':00'
                                           else item_price_active_date end) as item_price_active_date
                        from b_item_price
                        group by
                                b_item_price.b_item_id) as max_price_active_date
                on b_item_price.b_item_id = max_price_active_date.b_item_id
                    and (case when length(trim(b_item_price.item_price_active_date)) = 10  and b_item_price.item_price_active_date ilike '%-%'
                                                then b_item_price.item_price_active_date||',00:00:00'
                                            when length(trim(b_item_price.item_price_active_date)) = 16 and b_item_price.item_price_active_date ilike '%-%'
                                                then b_item_price.item_price_active_date||':00'
                                           else b_item_price.item_price_active_date end) = max_price_active_date.item_price_active_date) as b_item_price
 on b_item.b_item_id = b_item_price.b_item_id


left join b_nhso_map_drug on b_item.b_item_id = b_nhso_map_drug.b_item_id
left join b_nhso_drugcode24 on  b_nhso_map_drug.b_nhso_drugcode24_id  = b_nhso_drugcode24.b_nhso_drugcode24_id
left join (select
                        t_log_export_drug_catalog.*
                from t_log_export_drug_catalog inner join t_log_export_drug_tmt
                            on t_log_export_drug_catalog.t_log_export_drug_tmt_id =t_log_export_drug_tmt.t_log_export_drug_tmt_id
                        inner join (select
                                                    t_log_export_drug_catalog.b_item_id
                                                    ,max(t_log_export_drug_tmt.record_date_time) as record_date_time
                                            from t_log_export_drug_catalog inner join t_log_export_drug_tmt
                                                        on t_log_export_drug_catalog.t_log_export_drug_tmt_id =t_log_export_drug_tmt.t_log_export_drug_tmt_id
                                            group by
                                                    t_log_export_drug_catalog.b_item_id) as max_export_drug_catalog
                            on t_log_export_drug_catalog.b_item_id = max_export_drug_catalog.b_item_id
                                and t_log_export_drug_tmt.record_date_time = max_export_drug_catalog.record_date_time) as t_log_export_drug_catalog
on b_item.b_item_id = t_log_export_drug_catalog.b_item_id


 where
((substr(b_item.modify_datetime::text,1,4)::int + 543) || substr(b_item.modify_datetime::text,5,6)) between ':start_date' and ':end_date'
or ((substr(b_item_price.record_datetime::text,1,4)::int + 543) || substr(b_item_price.record_datetime::text,5,6)) between ':start_date' and ':end_date'
    or ((substr(b_map_drug_tmt.record_datetime::text,1,4)::int + 543) || substr(b_map_drug_tmt.record_datetime::text,5,6)) between ':start_date' and ':end_date'


 order by
b_item.b_item_id asc
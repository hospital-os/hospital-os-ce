/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.utility;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.event.CellEditorListener;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author Somprasong
 */
public class TableButton extends JButton implements TableCellRenderer, TableCellEditor {

    private int selectedRow;
    private int selectedColumn;
    List<TableButtonListener> listener;

    public TableButton(String text) {
        super(text);
        setOpaque(true);
        listener = new ArrayList<TableButtonListener>();
        addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for (TableButtonListener l : listener) {
                    l.tableButtonClicked(selectedRow, selectedColumn);
                }
            }
        });
    }

    public void addTableButtonListener(TableButtonListener l) {
        listener.add(l);
    }

    public void removeTableButtonListener(TableButtonListener l) {
        listener.remove(l);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table,
            Object value, boolean isSelected, boolean hasFocus, int row, int col) {
        if (value instanceof Boolean) {
            boolean isVisible = (Boolean) value;
            this.setEnabled(isVisible);
        } else {
            this.setEnabled(false);
        }
        if (isSelected) {
            setForeground(table.getSelectionForeground());
            setBackground(table.getSelectionBackground());
        } else {
            setForeground(table.getForeground());
            setBackground(UIManager.getColor("Button.background"));
        }
        return this;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table,
            Object value, boolean isSelected, int row, int col) {
        if (value instanceof Boolean) {
            boolean isVisible = (Boolean) value;
            this.setEnabled(isVisible);
        } else {
            this.setEnabled(false);
        }
        if (isSelected) {
            setForeground(table.getSelectionForeground());
            setBackground(table.getSelectionBackground());
        } else {
            setForeground(table.getForeground());
            setBackground(table.getBackground());
        }
        selectedRow = row;
        selectedColumn = col;
        return this;
    }

    @Override
    public void addCellEditorListener(CellEditorListener arg0) {
    }

    @Override
    public void cancelCellEditing() {
    }

    @Override
    public Object getCellEditorValue() {
        return "";
    }

    @Override
    public boolean isCellEditable(EventObject arg0) {
        return isEnabled();
    }

    @Override
    public void removeCellEditorListener(CellEditorListener arg0) {
    }

    @Override
    public boolean shouldSelectCell(EventObject arg0) {
        return true;
    }

    @Override
    public boolean stopCellEditing() {
        return true;
    }
}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.EyesExam;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class EyeExamDB {

    public ConnectionInf theConnectionInf;
    final public String idtable = "998";

    public EyeExamDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public int insert(EyesExam o) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "INSERT INTO t_eyes_exam("
                    + "t_eyes_exam_id, t_visit_id, left_eyes_result, right_eyes_result, "
                    + "left_eyes_diagnosis, right_eyes_diagnosis, left_eyes_csme, right_eyes_csme, "
                    + "left_eyes_opticnerve, right_eyes_opticnerve, left_eyes_opticnerve_detail, "
                    + "right_eyes_opticnerve_detail, left_eyes_haemorrhage, right_eyes_haemorrhage, "
                    + "left_eyes_exudates, right_eyes_exudates, left_eyes_bg, right_eyes_bg, "
                    + "left_eyes_macula_edema, right_eyes_macula_edema, other_diseases_found, "
                    + "other_diseases_cataract, other_diseases_glaucoma, other_diseases_pterygium, "
                    + "other_diseases_other, other_diseases_other_detail, doctor_recommend, "
                    + "eyes_management, appointment_date, appointment_place, doctor_diag, "
                    + "screen_date, "
                    + "color_vision_result, color_vision_test_1_result, color_vision_test_2_result, color_vision_test_3_result, color_vision_test_4_result, "
                    + "color_vision_test_5_result, color_vision_test_6_result, color_vision_test_7_result, color_vision_test_8_result, color_vision_test_9_result, "
                    + "color_vision_test_10_result, color_vision_test_11_result, color_vision_test_12_result, color_vision_test_13_result, color_vision_test_14_result, "
                    + "color_vision_test_15_result, color_vision_test_16_result, color_vision_test_17_result, color_vision_test_18_result, color_vision_test_19_result, "
                    + "color_vision_test_20_result, color_vision_test_21_result, color_vision_test_22_result, color_vision_test_23_result, color_vision_test_24_result, "
                    + "color_vision_test_user_id, "
                    + "active, record_date_time, user_record_id, update_date_time, user_update_id)"
                    + "VALUES (?, ?, ?, ?, "
                    + "?, ?, ?, ?, "
                    + "?, ?, ?, "
                    + "?, ?, ?, "
                    + "?, ?, ?, ?, "
                    + "?, ?, ?, "
                    + "?, ?, ?, "
                    + "?, ?, ?, "
                    + "?, ?, ?, ?, "
                    + "?, "
                    + "?, ?, ?, ?, ?, "
                    + "?, ?, ?, ?, ?, "
                    + "?, ?, ?, ?, ?, "
                    + "?, ?, ?, ?, ?, "
                    + "?, ?, ?, ?, ?, "
                    + "?, "
                    + "?, ?, ?, ?, ?)";
            preparedStatement = theConnectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, o.getGenID(idtable));
            preparedStatement.setString(index++, o.t_visit_id);
            preparedStatement.setString(index++, o.left_eyes_result);
            preparedStatement.setString(index++, o.right_eyes_result);
            preparedStatement.setString(index++, o.left_eyes_diagnosis);
            preparedStatement.setString(index++, o.right_eyes_diagnosis);
            preparedStatement.setString(index++, o.left_eyes_csme);
            preparedStatement.setString(index++, o.right_eyes_csme);
            preparedStatement.setString(index++, o.left_eyes_opticnerve);
            preparedStatement.setString(index++, o.right_eyes_opticnerve);
            preparedStatement.setString(index++, o.left_eyes_opticnerve_detail);
            preparedStatement.setString(index++, o.right_eyes_opticnerve_detail);
            preparedStatement.setString(index++, o.left_eyes_haemorrhage);
            preparedStatement.setString(index++, o.right_eyes_haemorrhage);
            preparedStatement.setString(index++, o.left_eyes_exudates);
            preparedStatement.setString(index++, o.right_eyes_exudates);
            preparedStatement.setString(index++, o.left_eyes_bg);
            preparedStatement.setString(index++, o.right_eyes_bg);
            preparedStatement.setString(index++, o.left_eyes_macula_edema);
            preparedStatement.setString(index++, o.right_eyes_macula_edema);
            preparedStatement.setString(index++, o.other_diseases_found);
            preparedStatement.setString(index++, o.other_diseases_cataract);
            preparedStatement.setString(index++, o.other_diseases_glaucoma);
            preparedStatement.setString(index++, o.other_diseases_pterygium);
            preparedStatement.setString(index++, o.other_diseases_other);
            preparedStatement.setString(index++, o.other_diseases_other_detail);
            preparedStatement.setString(index++, o.doctor_recommend);
            preparedStatement.setString(index++, o.eyes_management);
            preparedStatement.setString(index++, o.appointment_date);
            preparedStatement.setString(index++, o.appointment_place);
            preparedStatement.setString(index++, o.doctor_diag);
            preparedStatement.setString(index++, o.screen_date);
            preparedStatement.setString(index++, o.color_vision_result);
            preparedStatement.setString(index++, o.color_vision_test_1_result);
            preparedStatement.setString(index++, o.color_vision_test_2_result);
            preparedStatement.setString(index++, o.color_vision_test_3_result);
            preparedStatement.setString(index++, o.color_vision_test_4_result);
            preparedStatement.setString(index++, o.color_vision_test_5_result);
            preparedStatement.setString(index++, o.color_vision_test_6_result);
            preparedStatement.setString(index++, o.color_vision_test_7_result);
            preparedStatement.setString(index++, o.color_vision_test_8_result);
            preparedStatement.setString(index++, o.color_vision_test_9_result);
            preparedStatement.setString(index++, o.color_vision_test_10_result);
            preparedStatement.setString(index++, o.color_vision_test_11_result);
            preparedStatement.setString(index++, o.color_vision_test_12_result);
            preparedStatement.setString(index++, o.color_vision_test_13_result);
            preparedStatement.setString(index++, o.color_vision_test_14_result);
            preparedStatement.setString(index++, o.color_vision_test_15_result);
            preparedStatement.setString(index++, o.color_vision_test_16_result);
            preparedStatement.setString(index++, o.color_vision_test_17_result);
            preparedStatement.setString(index++, o.color_vision_test_18_result);
            preparedStatement.setString(index++, o.color_vision_test_19_result);
            preparedStatement.setString(index++, o.color_vision_test_20_result);
            preparedStatement.setString(index++, o.color_vision_test_21_result);
            preparedStatement.setString(index++, o.color_vision_test_22_result);
            preparedStatement.setString(index++, o.color_vision_test_23_result);
            preparedStatement.setString(index++, o.color_vision_test_24_result);
            preparedStatement.setString(index++, o.color_vision_test_user_id);
            preparedStatement.setString(index++, o.active);
            preparedStatement.setString(index++, o.record_date_time);
            preparedStatement.setString(index++, o.user_record_id);
            preparedStatement.setString(index++, o.update_date_time);
            preparedStatement.setString(index++, o.user_update_id);

            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(EyesExam o) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "UPDATE t_eyes_exam "
                    + "SET left_eyes_result=?, right_eyes_result=?,  "
                    + "left_eyes_diagnosis=?, right_eyes_diagnosis=?, left_eyes_csme=?,  "
                    + "right_eyes_csme=?, left_eyes_opticnerve=?, right_eyes_opticnerve=?,  "
                    + "left_eyes_opticnerve_detail=?, right_eyes_opticnerve_detail=?,  "
                    + "left_eyes_haemorrhage=?, right_eyes_haemorrhage=?, left_eyes_exudates=?,  "
                    + "right_eyes_exudates=?, left_eyes_bg=?, right_eyes_bg=?, left_eyes_macula_edema=?,  "
                    + "right_eyes_macula_edema=?, other_diseases_found=?, other_diseases_cataract=?,  "
                    + "other_diseases_glaucoma=?, other_diseases_pterygium=?, other_diseases_other=?,  "
                    + "other_diseases_other_detail=?, doctor_recommend=?, eyes_management=?,  "
                    + "appointment_date=?, appointment_place=?, doctor_diag=?, screen_date=?,  "
                    + "color_vision_result=?, color_vision_test_1_result=?, color_vision_test_2_result=?, color_vision_test_3_result=?, color_vision_test_4_result=?, "
                    + "color_vision_test_5_result=?, color_vision_test_6_result=?, color_vision_test_7_result=?, color_vision_test_8_result=?, color_vision_test_9_result=?, "
                    + "color_vision_test_10_result=?, color_vision_test_11_result=?, color_vision_test_12_result=?, color_vision_test_13_result=?, color_vision_test_14_result=?, "
                    + "color_vision_test_15_result=?, color_vision_test_16_result=?, color_vision_test_17_result=?, color_vision_test_18_result=?, color_vision_test_19_result=?, "
                    + "color_vision_test_20_result=?, color_vision_test_21_result=?, color_vision_test_22_result=?, color_vision_test_23_result=?, color_vision_test_24_result=?, "
                    + "color_vision_test_user_id=?, update_date_time=?,user_update_id=? "
                    + "WHERE t_eyes_exam_id=?";
            preparedStatement = theConnectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, o.left_eyes_result);
            preparedStatement.setString(index++, o.right_eyes_result);
            preparedStatement.setString(index++, o.left_eyes_diagnosis);
            preparedStatement.setString(index++, o.right_eyes_diagnosis);
            preparedStatement.setString(index++, o.left_eyes_csme);
            preparedStatement.setString(index++, o.right_eyes_csme);
            preparedStatement.setString(index++, o.left_eyes_opticnerve);
            preparedStatement.setString(index++, o.right_eyes_opticnerve);
            preparedStatement.setString(index++, o.left_eyes_opticnerve_detail);
            preparedStatement.setString(index++, o.right_eyes_opticnerve_detail);
            preparedStatement.setString(index++, o.left_eyes_haemorrhage);
            preparedStatement.setString(index++, o.right_eyes_haemorrhage);
            preparedStatement.setString(index++, o.left_eyes_exudates);
            preparedStatement.setString(index++, o.right_eyes_exudates);
            preparedStatement.setString(index++, o.left_eyes_bg);
            preparedStatement.setString(index++, o.right_eyes_bg);
            preparedStatement.setString(index++, o.left_eyes_macula_edema);
            preparedStatement.setString(index++, o.right_eyes_macula_edema);
            preparedStatement.setString(index++, o.other_diseases_found);
            preparedStatement.setString(index++, o.other_diseases_cataract);
            preparedStatement.setString(index++, o.other_diseases_glaucoma);
            preparedStatement.setString(index++, o.other_diseases_pterygium);
            preparedStatement.setString(index++, o.other_diseases_other);
            preparedStatement.setString(index++, o.other_diseases_other_detail);
            preparedStatement.setString(index++, o.doctor_recommend);
            preparedStatement.setString(index++, o.eyes_management);
            preparedStatement.setString(index++, o.appointment_date);
            preparedStatement.setString(index++, o.appointment_place);
            preparedStatement.setString(index++, o.doctor_diag);
            preparedStatement.setString(index++, o.screen_date);
            preparedStatement.setString(index++, o.color_vision_result);
            preparedStatement.setString(index++, o.color_vision_test_1_result);
            preparedStatement.setString(index++, o.color_vision_test_2_result);
            preparedStatement.setString(index++, o.color_vision_test_3_result);
            preparedStatement.setString(index++, o.color_vision_test_4_result);
            preparedStatement.setString(index++, o.color_vision_test_5_result);
            preparedStatement.setString(index++, o.color_vision_test_6_result);
            preparedStatement.setString(index++, o.color_vision_test_7_result);
            preparedStatement.setString(index++, o.color_vision_test_8_result);
            preparedStatement.setString(index++, o.color_vision_test_9_result);
            preparedStatement.setString(index++, o.color_vision_test_10_result);
            preparedStatement.setString(index++, o.color_vision_test_11_result);
            preparedStatement.setString(index++, o.color_vision_test_12_result);
            preparedStatement.setString(index++, o.color_vision_test_13_result);
            preparedStatement.setString(index++, o.color_vision_test_14_result);
            preparedStatement.setString(index++, o.color_vision_test_15_result);
            preparedStatement.setString(index++, o.color_vision_test_16_result);
            preparedStatement.setString(index++, o.color_vision_test_17_result);
            preparedStatement.setString(index++, o.color_vision_test_18_result);
            preparedStatement.setString(index++, o.color_vision_test_19_result);
            preparedStatement.setString(index++, o.color_vision_test_20_result);
            preparedStatement.setString(index++, o.color_vision_test_21_result);
            preparedStatement.setString(index++, o.color_vision_test_22_result);
            preparedStatement.setString(index++, o.color_vision_test_23_result);
            preparedStatement.setString(index++, o.color_vision_test_24_result);
            preparedStatement.setString(index++, o.color_vision_test_user_id);
            preparedStatement.setString(index++, o.update_date_time);
            preparedStatement.setString(index++, o.user_update_id);
            preparedStatement.setString(index++, o.getObjectId());

            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(EyesExam o) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "UPDATE t_eyes_exam "
                    + "SET active='0',update_date_time=?,user_update_id=? "
                    + "WHERE t_eyes_exam_id=?";
            preparedStatement = theConnectionInf.ePQuery(sql);
            preparedStatement.setString(1, o.update_date_time);
            preparedStatement.setString(2, o.user_update_id);
            preparedStatement.setString(3, o.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public EyesExam findByVisitId(String visitId) throws Exception {
        String sql = "select * from t_eyes_exam where t_visit_id = ? and active = '1'";

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = theConnectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, visitId);
            List<EyesExam> list = eQuery(preparedStatement.toString());
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    private List<EyesExam> eQuery(String sql) throws Exception {
        List<EyesExam> list = new ArrayList<EyesExam>();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            EyesExam o = new EyesExam();
            o.setObjectId(rs.getString("t_eyes_exam_id"));
            o.t_visit_id = rs.getString("t_visit_id");
            o.left_eyes_result = rs.getString("left_eyes_result");
            o.right_eyes_result = rs.getString("right_eyes_result");
            o.left_eyes_diagnosis = rs.getString("left_eyes_diagnosis");
            o.right_eyes_diagnosis = rs.getString("right_eyes_diagnosis");
            o.left_eyes_csme = rs.getString("left_eyes_csme");
            o.right_eyes_csme = rs.getString("right_eyes_csme");
            o.left_eyes_opticnerve = rs.getString("left_eyes_opticnerve");
            o.right_eyes_opticnerve = rs.getString("right_eyes_opticnerve");
            o.left_eyes_opticnerve_detail = rs.getString("left_eyes_opticnerve_detail");
            o.right_eyes_opticnerve_detail = rs.getString("right_eyes_opticnerve_detail");
            o.left_eyes_haemorrhage = rs.getString("left_eyes_haemorrhage");
            o.right_eyes_haemorrhage = rs.getString("right_eyes_haemorrhage");
            o.left_eyes_exudates = rs.getString("left_eyes_exudates");
            o.right_eyes_exudates = rs.getString("right_eyes_exudates");
            o.left_eyes_bg = rs.getString("left_eyes_bg");
            o.right_eyes_bg = rs.getString("right_eyes_bg");
            o.left_eyes_macula_edema = rs.getString("left_eyes_macula_edema");
            o.right_eyes_macula_edema = rs.getString("right_eyes_macula_edema");
            o.other_diseases_found = rs.getString("other_diseases_found");
            o.other_diseases_cataract = rs.getString("other_diseases_cataract");
            o.other_diseases_glaucoma = rs.getString("other_diseases_glaucoma");
            o.other_diseases_pterygium = rs.getString("other_diseases_pterygium");
            o.other_diseases_other = rs.getString("other_diseases_other");
            o.other_diseases_other_detail = rs.getString("other_diseases_other_detail");
            o.doctor_recommend = rs.getString("doctor_recommend");
            o.eyes_management = rs.getString("eyes_management");
            o.appointment_date = rs.getString("appointment_date");
            o.appointment_place = rs.getString("appointment_place");
            o.doctor_diag = rs.getString("doctor_diag");
            o.screen_date = rs.getString("screen_date");
            o.color_vision_result = rs.getString("color_vision_result");
            o.color_vision_test_1_result = rs.getString("color_vision_test_1_result");
            o.color_vision_test_2_result = rs.getString("color_vision_test_2_result");
            o.color_vision_test_3_result = rs.getString("color_vision_test_3_result");
            o.color_vision_test_4_result = rs.getString("color_vision_test_4_result");
            o.color_vision_test_5_result = rs.getString("color_vision_test_5_result");
            o.color_vision_test_6_result = rs.getString("color_vision_test_6_result");
            o.color_vision_test_7_result = rs.getString("color_vision_test_7_result");
            o.color_vision_test_8_result = rs.getString("color_vision_test_8_result");
            o.color_vision_test_9_result = rs.getString("color_vision_test_9_result");
            o.color_vision_test_10_result = rs.getString("color_vision_test_10_result");
            o.color_vision_test_11_result = rs.getString("color_vision_test_11_result");
            o.color_vision_test_12_result = rs.getString("color_vision_test_12_result");
            o.color_vision_test_13_result = rs.getString("color_vision_test_13_result");
            o.color_vision_test_14_result = rs.getString("color_vision_test_14_result");
            o.color_vision_test_15_result = rs.getString("color_vision_test_15_result");
            o.color_vision_test_16_result = rs.getString("color_vision_test_16_result");
            o.color_vision_test_17_result = rs.getString("color_vision_test_17_result");
            o.color_vision_test_18_result = rs.getString("color_vision_test_18_result");
            o.color_vision_test_19_result = rs.getString("color_vision_test_19_result");
            o.color_vision_test_20_result = rs.getString("color_vision_test_20_result");
            o.color_vision_test_21_result = rs.getString("color_vision_test_21_result");
            o.color_vision_test_22_result = rs.getString("color_vision_test_22_result");
            o.color_vision_test_23_result = rs.getString("color_vision_test_23_result");
            o.color_vision_test_24_result = rs.getString("color_vision_test_24_result");
            o.color_vision_test_user_id = rs.getString("color_vision_test_user_id");
            o.active = rs.getString("active");
            o.record_date_time = rs.getString("record_date_time");
            o.user_record_id = rs.getString("user_record_id");
            o.update_date_time = rs.getString("update_date_time");
            o.user_update_id = rs.getString("user_update_id");
            list.add(o);
        }
        rs.close();
        return list;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.LabAtkProduct;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tanakrit
 */
public class LabAtkProductDB {

    private final ConnectionInf connectionInf;

    public LabAtkProductDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public LabAtkProduct select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_lab_atk_product where f_lab_atk_product_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<LabAtkProduct> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<LabAtkProduct> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_lab_atk_product order by f_lab_atk_product_id";
            preparedStatement = connectionInf.ePQuery(sql);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<LabAtkProduct> listByKeyword(String keyword) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_lab_atk_product \n"
                    + "where upper(lab_atk_device_name) like upper(?) or \n"
                    + "upper(lab_atk_fda_reg_no) like upper(?) \n"
                    + "order by f_lab_atk_product_id";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, "%" + (keyword == null || keyword.isEmpty() ? "" : keyword) + "%");
            preparedStatement.setString(2, "%" + (keyword == null || keyword.isEmpty() ? "" : keyword) + "%");
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<LabAtkProduct> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<LabAtkProduct> list = new ArrayList<>();
        try (ResultSet rs = preparedStatement.executeQuery()) {
            while (rs.next()) {
                LabAtkProduct obj = new LabAtkProduct();
                obj.setObjectId(rs.getString("f_lab_atk_product_id"));
                obj.lab_atk_device_name = rs.getString("lab_atk_device_name");
                obj.lab_atk_fda_reg_no = rs.getString("lab_atk_fda_reg_no");
                obj.lab_atk_manufacturer_code = rs.getString("lab_atk_manufacturer_code");
                obj.lab_atk_manufacturer_name = rs.getString("lab_atk_manufacturer_name");
                obj.lab_atk_manufacturer_country = rs.getString("lab_atk_manufacturer_country");
                obj.lab_atk_manufacturer_url = rs.getString("lab_atk_manufacturer_url");
                obj.lab_atk_product_code = rs.getString("lab_atk_product_code");
                obj.product_type = rs.getString("product_type");
                list.add(obj);
            }
            return list;
        }
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<>();
        for (LabAtkProduct obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }
}

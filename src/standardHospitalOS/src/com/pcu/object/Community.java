/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class Community extends Persistent {

    public String health_community_id = "";
    public String health_community_name = "";
    public String health_community_register = "0";
    public String health_community_soi = "";
    public String health_community_road = "";
    public String t_health_village_id = "";
    public String health_community_person_id = "";
    public String health_volunteer_person_id = "";
    public String health_volunteer = "";
    public String health_community_detail = "";
    public String active = "1";
    public String user_record_id = "";
    public String record_date_time = "";
    public String user_modify_id = "";
    public String modify_date_time = "";
    public String user_cancel_id = "";
    public String cancel_date_time = "";
}

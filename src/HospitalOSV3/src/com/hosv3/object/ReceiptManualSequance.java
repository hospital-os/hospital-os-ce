/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.object;

import com.hospital_os.usecase.connection.Persistent;
import java.text.DecimalFormat;
import java.util.Calendar;

/**
 *
 * @author LionHeart
 */
public class ReceiptManualSequance extends Persistent {

    String init = "";
    public String ip_address = init;
    public String pattern = init;
    public String day = init;
    public String month = init;
    public String service_point_seq = init;
    public String service_point = init;
    public String book_no = init;
    public String begin_no = init;
    public String end_no = init;
    public String receipt_qty = init;
    public String value = init;
    public String active = init;
//    public String receipt_sequence_clinic = init;

    public static String getDBText(String pattern, int value, String year2d) {
        //   Constant.println("public static String getDBText(String pattern,int value,String year2d)");
        //   Constant.println(pattern + " " + value + " " + year2d);
        String patt = pattern;
        String start = "";
        String year = "";
        if (pattern.indexOf("yy") != -1) {
            year = year2d;
            patt = pattern.substring(pattern.lastIndexOf("yy") + 2);
            start = pattern.substring(0, pattern.indexOf("yy"));
            Calendar c = Calendar.getInstance();
            c.setTimeInMillis(System.currentTimeMillis());
        }
        if (pattern.indexOf(".") != -1) {
            patt = pattern.substring(pattern.lastIndexOf(".") + 1);
            start = pattern.substring(0, pattern.indexOf("."));
        }
        DecimalFormat d = new DecimalFormat();
        d.applyPattern(patt);
        String show = start + year + d.format(value);
        return show;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.utility;

import com.hospital_os.usecase.connection.CommonInf;
import java.awt.Component;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;

/**
 *
 * @author sompr
 */
public class ComboCellRenderer extends DefaultListCellRenderer {

    @Override
    public Component getListCellRendererComponent(JList list,
            Object value, int index, boolean isSelected,
            boolean cellHasFocus) {
        if (value instanceof CommonInf) {
            setToolTipText(((CommonInf) value).getName());
        } else if (value instanceof ComboFix) {
            setToolTipText(((ComboFix) value).getName());
        } else {
            setToolTipText(value.toString());
        }
        return super.getListCellRendererComponent(list, value, index, isSelected,
                cellHasFocus);
    }
}

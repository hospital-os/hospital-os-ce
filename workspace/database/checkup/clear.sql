DROP TABLE b_item_lab_advice;

DROP TABLE b_item_lab_advice_set;

DROP TABLE b_item_checkup;

DROP TABLE t_checkup;

DROP TABLE t_checkup_personal;

DROP TABLE t_checkup_druguse;

DROP TABLE t_checkup_sight;


DROP TABLE t_checkup_hearing;

-- create s_checkup_version
DROP TABLE s_checkup_version;

DELETE FROM s_script_update_log WHERE script_update_log_filename = 'update_checkup_001.sql';

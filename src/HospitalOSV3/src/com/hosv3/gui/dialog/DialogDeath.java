/*
 * DialogAdmit.java
 *
 * Created on 8 ��Ȩԡ�¹ 2546, 14:14 �.
 */
package com.hosv3.gui.dialog;

import com.hospital_os.object.*;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hospital_os.utility.CellRendererHos;
import com.hospital_os.utility.ComboBoxBlueRenderer;
import com.hospital_os.utility.ComboboxModel;
import com.hospital_os.utility.Gutil;
import com.hospital_os.utility.TaBleModel;
import com.hosv3.control.*;
import com.hosv3.control.lookup.Icd10Lookup;
import com.hosv3.object.*;
import com.hosv3.utility.*;
import com.pcu.object.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.*;
import javax.swing.*;

/**
 *
 * @author tong
 */
@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public class DialogDeath extends javax.swing.JDialog implements UpdateStatus {

    private static final long serialVersionUID = 1L;
    private HosObject theHO;
    public boolean actionCommand = false;
    private LookupControl theLookupControl;
    private VisitControl theVisitControl;
    private DiagnosisControl theDiagnosisControl;
    private PatientControl thePatientControl;
    private Visit theVisit;
    private Patient thePatient;
    private Death theDeath;
    private Family theFamily;
    boolean mode_not_save = false;
    private final CellRendererHos dateRender = new CellRendererHos(CellRendererHos.DATE_TIME);
    private Vector vDeath;  //     * �红����� Vector �ҡ��ä��Ң����š�õ��  
    public static boolean closeDialog = false;
    private String[] col_jTableListDeath = {"����-ʡ��", "VN", "�ѹ���"};
    private CellRendererHos vnRender = new CellRendererHos(CellRendererHos.VN);
    private ComboBoxBlueRenderer blueRenderer = new ComboBoxBlueRenderer();

    public DialogDeath(HosControl hc, JFrame frm) {
        super(frm, true);
        initComponents();
        setControl(hc);
        setTitle(Constant.getTextBundle("�ѹ�֡�����š�õ��"));
        setLocationRelativeTo(null);
    }

    public DialogDeath(HosControl hc, UpdateStatus us) {
        super(us.getJFrame(), true);
        initComponents();
        setControl(hc);
        setTitle(Constant.getTextBundle("�ѹ�֡�����š�õ��"));
        setLocationRelativeTo(null);
    }

    private void setControl(HosControl hc) {
        theLookupControl = hc.theLookupControl;
        theVisitControl = hc.theVisitControl;
        theDiagnosisControl = hc.theDiagnosisControl;
        thePatientControl = hc.thePatientControl;
        theHO = hc.theHO;
        vnRender = new CellRendererHos(CellRendererHos.VN, theLookupControl.getSequenceDataVN().pattern);
        this.jTableListDeath.setGuiMode(true);
        setLanguage("");
        setComboBoxEdit(true);
        ComboboxModel.initComboBox(jComboBoxLocalType, theLookupControl.listLocalType());
        // ����� 3.9.16b01
        ComboboxModel.initComboBox(jComboBoxCaseDeathPreg, theLookupControl.listDeathPregnancyStatus());
        jButtonDel.setVisible(true);
        this.jTextFieldCaseDeath.setControl(new Icd10Lookup(theLookupControl), true);
        this.jTextFieldCdeatha.setControl(new Icd10Lookup(theLookupControl), true);
        this.jTextFieldCdeathb.setControl(new Icd10Lookup(theLookupControl), true);
        this.jTextFieldCdeathc.setControl(new Icd10Lookup(theLookupControl), true);
        this.jTextFieldCdeathd.setControl(new Icd10Lookup(theLookupControl), true);
        this.jTextFieldCOdiseae.setControl(new Icd10Lookup(theLookupControl), true);
        setTableListDeath(vDeath);
    }

    /**
     * dialog �����㹡���觢�ͤ������͹�����
     */
    @Override
    public void setStatus(String str, int status) {
        ThreadStatus theTT = new ThreadStatus(this.getJFrame(), this.jLabelStatus);
        theTT.start();
        str = Constant.getTextBundle(str);
        jLabelStatus.setText(" " + str);
        if (status == UpdateStatus.WARNING) {
            jLabelStatus.setBackground(Color.YELLOW);
        }
        if (status == UpdateStatus.COMPLETE) {
            jLabelStatus.setBackground(Color.GREEN);
        }
        if (status == UpdateStatus.ERROR) {
            jLabelStatus.setBackground(Color.RED);
        }
    }

    @Override
    public JFrame getJFrame() {
        return null;
    }

    /**
     * dialog �����㹡���������ӡ���չ�ѹ��觵�ҧ
     */
    @Override
    public boolean confirmBox(String str, int status) {
        int i = JOptionPane.showConfirmDialog(this, str, "��͹", JOptionPane.YES_NO_OPTION);
        return (i == JOptionPane.YES_OPTION);
    }

    /*
     * ����Ѻ��͹˹�Ҩ���������ա�úѹ�֡��������
     */
    private void setEnableAll(boolean var) {
        jComboBoxLocalType.setEnabled(var);
        jTextFieldCaseDeath.setEnabled(var);
        // 3.9.16b01
        jComboBoxCaseDeathPreg.setEnabled(var ? theFamily == null ? true : !"1".equals(theFamily.f_sex_id) : false);
        txtPregnantWeeks.setEditable(ComboboxModel.getCodeComboBox(jComboBoxCaseDeathPreg).equals("1"));
        jLabelHN.setEnabled(var);
        jLabelVisit.setEnabled(var);
        jTextFieldCdeatha.setEnabled(var);
        jTextFieldCdeathb.setEnabled(var);
        jTextFieldCdeathc.setEnabled(var);
        jTextFieldCdeathd.setEnabled(var);
        jTextFieldCOdiseae.setEnabled(var);
        dateComboBoxDdeath.setEnabled(var);
        jButtonSave.setEnabled(var);

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabelPlaceDeath = new javax.swing.JLabel();
        jComboBoxLocalType = new javax.swing.JComboBox();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabelHN = new javax.swing.JLabel();
        jLabelVisit = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jButtonSave = new javax.swing.JButton();
        jButtonCancel = new javax.swing.JButton();
        jButtonDel = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        dateComboBoxDdeath = new com.hospital_os.utility.DateComboBox();
        jLabel6 = new javax.swing.JLabel();
        jTextFieldTimeDeath = new com.hospital_os.utility.TimeTextField();
        jTextFieldCdeatha = new com.hosv3.gui.component.HosComboBox();
        jTextFieldCdeathb = new com.hosv3.gui.component.HosComboBox();
        jTextFieldCdeathc = new com.hosv3.gui.component.HosComboBox();
        jTextFieldCdeathd = new com.hosv3.gui.component.HosComboBox();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jTextFieldCOdiseae = new com.hosv3.gui.component.HosComboBox();
        jPanel6 = new javax.swing.JPanel();
        jLabelCaseDeath = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jTextFieldCaseDeath = new com.hosv3.gui.component.HosComboBox();
        jLabel3 = new javax.swing.JLabel();
        jComboBoxCaseDeathPreg = new javax.swing.JComboBox();
        txtPregnantWeeks = new com.hospital_os.utility.IntegerTextField();
        jPanel5 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableListDeath = new com.hosv3.gui.component.HJTableSort();
        jPanel10 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jButtonSearch = new javax.swing.JButton();
        jCheckBoxSearchByDate = new javax.swing.JCheckBox();
        hNTextFieldSearchHN = new javax.swing.JTextField();
        jPanel12 = new javax.swing.JPanel();
        dateComboBoxFrom = new com.hospital_os.utility.DateComboBox();
        dateComboBoxTo = new com.hospital_os.utility.DateComboBox();
        jLabelStatus = new javax.swing.JLabel();

        setPreferredSize(new java.awt.Dimension(800, 600));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel1.setMinimumSize(new java.awt.Dimension(550, 131));
        jPanel1.setPreferredSize(new java.awt.Dimension(550, 131));
        jPanel1.setLayout(new java.awt.GridBagLayout());

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Diag_Death_Detail"));
        jPanel4.setMinimumSize(new java.awt.Dimension(340, 100));
        jPanel4.setPreferredSize(new java.awt.Dimension(340, 100));
        jPanel4.setLayout(new java.awt.GridBagLayout());

        jPanel3.setLayout(new java.awt.GridBagLayout());

        jLabelPlaceDeath.setFont(jLabelPlaceDeath.getFont());
        jLabelPlaceDeath.setText("PlaceDeath");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel3.add(jLabelPlaceDeath, gridBagConstraints);

        jComboBoxLocalType.setBackground(new java.awt.Color(204, 255, 255));
        jComboBoxLocalType.setFont(jComboBoxLocalType.getFont());
        jComboBoxLocalType.setRenderer(blueRenderer);
        jComboBoxLocalType.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxLocalTypeActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel3.add(jComboBoxLocalType, gridBagConstraints);

        jLabel8.setFont(jLabel8.getFont());
        jLabel8.setText("HN");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jLabel8, gridBagConstraints);

        jLabel9.setFont(jLabel9.getFont());
        jLabel9.setText("VN");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jLabel9, gridBagConstraints);

        jLabelHN.setFont(jLabelHN.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jLabelHN, gridBagConstraints);

        jLabelVisit.setFont(jLabelVisit.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel3.add(jLabelVisit, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 0, 5);
        jPanel4.add(jPanel3, gridBagConstraints);

        jPanel2.setLayout(new java.awt.GridBagLayout());

        jButtonSave.setFont(jButtonSave.getFont());
        jButtonSave.setText("�ѹ�֡");
        jButtonSave.setMaximumSize(new java.awt.Dimension(72, 26));
        jButtonSave.setMinimumSize(new java.awt.Dimension(72, 26));
        jButtonSave.setPreferredSize(new java.awt.Dimension(72, 26));
        jButtonSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSaveActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        jPanel2.add(jButtonSave, gridBagConstraints);

        jButtonCancel.setFont(jButtonCancel.getFont());
        jButtonCancel.setText("¡��ԡ");
        jButtonCancel.setMaximumSize(new java.awt.Dimension(72, 26));
        jButtonCancel.setMinimumSize(new java.awt.Dimension(72, 26));
        jButtonCancel.setPreferredSize(new java.awt.Dimension(72, 26));
        jButtonCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        jPanel2.add(jButtonCancel, gridBagConstraints);

        jButtonDel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/minus16.gif"))); // NOI18N
        jButtonDel.setMaximumSize(new java.awt.Dimension(26, 26));
        jButtonDel.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonDel.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonDel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDelActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel2.add(jButtonDel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTHEAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel4.add(jPanel2, gridBagConstraints);

        jPanel7.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        jPanel4.add(jPanel7, gridBagConstraints);

        jPanel8.setLayout(new java.awt.GridBagLayout());

        jLabel13.setFont(jLabel13.getFont());
        jLabel13.setText("cdeath_a");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 0, 0, 0);
        jPanel8.add(jLabel13, gridBagConstraints);

        jLabel14.setFont(jLabel14.getFont());
        jLabel14.setText("cdeath_b");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 0, 0, 0);
        jPanel8.add(jLabel14, gridBagConstraints);

        jLabel15.setFont(jLabel15.getFont());
        jLabel15.setText("cdeath_c");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 0, 0, 0);
        jPanel8.add(jLabel15, gridBagConstraints);

        jLabel16.setFont(jLabel16.getFont());
        jLabel16.setText("cdeath_d");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 0, 0, 0);
        jPanel8.add(jLabel16, gridBagConstraints);

        jLabel17.setFont(jLabel17.getFont());
        jLabel17.setText("odiseae");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 0, 0, 0);
        jPanel8.add(jLabel17, gridBagConstraints);

        jPanel11.setLayout(new java.awt.GridBagLayout());

        jLabel12.setFont(jLabel12.getFont());
        jLabel12.setText("ddeath");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        jPanel11.add(jLabel12, gridBagConstraints);

        dateComboBoxDdeath.setBackground(new java.awt.Color(204, 255, 255));
        dateComboBoxDdeath.setFont(dateComboBoxDdeath.getFont());
        dateComboBoxDdeath.setMinimumSize(new java.awt.Dimension(100, 21));
        dateComboBoxDdeath.setPreferredSize(new java.awt.Dimension(100, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        jPanel11.add(dateComboBoxDdeath, gridBagConstraints);

        jLabel6.setFont(jLabel6.getFont());
        jLabel6.setText("TimeDeath");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        jPanel11.add(jLabel6, gridBagConstraints);

        jTextFieldTimeDeath.setToolTipText("");
        jTextFieldTimeDeath.setFont(jTextFieldTimeDeath.getFont());
        jTextFieldTimeDeath.setMaximumSize(new java.awt.Dimension(99, 21));
        jTextFieldTimeDeath.setMinimumSize(new java.awt.Dimension(45, 21));
        jTextFieldTimeDeath.setName("timeTextFieldTimeAppointment"); // NOI18N
        jTextFieldTimeDeath.setPreferredSize(new java.awt.Dimension(45, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel11.add(jTextFieldTimeDeath, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        jPanel8.add(jPanel11, gridBagConstraints);

        jTextFieldCdeatha.setBackground(new java.awt.Color(204, 255, 255));
        jTextFieldCdeatha.setFont(jTextFieldCdeatha.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 9, 0, 0);
        jPanel8.add(jTextFieldCdeatha, gridBagConstraints);

        jTextFieldCdeathb.setFont(jTextFieldCdeathb.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 9, 0, 0);
        jPanel8.add(jTextFieldCdeathb, gridBagConstraints);

        jTextFieldCdeathc.setFont(jTextFieldCdeathc.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 9, 0, 0);
        jPanel8.add(jTextFieldCdeathc, gridBagConstraints);

        jTextFieldCdeathd.setFont(jTextFieldCdeathd.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 9, 0, 0);
        jPanel8.add(jTextFieldCdeathd, gridBagConstraints);

        jLabel18.setFont(jLabel18.getFont());
        jLabel18.setText("( ICD-10 )");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(3, 5, 0, 0);
        jPanel8.add(jLabel18, gridBagConstraints);

        jLabel19.setFont(jLabel19.getFont());
        jLabel19.setText("( ICD-10 )");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(3, 5, 0, 0);
        jPanel8.add(jLabel19, gridBagConstraints);

        jLabel20.setFont(jLabel20.getFont());
        jLabel20.setText("( ICD-10 )");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(3, 5, 0, 0);
        jPanel8.add(jLabel20, gridBagConstraints);

        jLabel21.setFont(jLabel21.getFont());
        jLabel21.setText("( ICD-10 )");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(3, 5, 0, 0);
        jPanel8.add(jLabel21, gridBagConstraints);

        jLabel22.setFont(jLabel22.getFont());
        jLabel22.setText("( ICD-10 )");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(3, 5, 0, 0);
        jPanel8.add(jLabel22, gridBagConstraints);

        jTextFieldCOdiseae.setFont(jTextFieldCOdiseae.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 9, 0, 0);
        jPanel8.add(jTextFieldCOdiseae, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 0, 5);
        jPanel4.add(jPanel8, gridBagConstraints);

        jPanel6.setLayout(new java.awt.GridBagLayout());

        jLabelCaseDeath.setFont(jLabelCaseDeath.getFont());
        jLabelCaseDeath.setText("���˵ء�õ�� (ICD-10)");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel6.add(jLabelCaseDeath, gridBagConstraints);

        jLabel2.setFont(jLabel2.getFont());
        jLabel2.setText("PregnancyLength");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 9, 0, 0);
        jPanel6.add(jLabel2, gridBagConstraints);

        jLabel1.setFont(jLabel1.getFont());
        jLabel1.setText("weeks");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 0, 0);
        jPanel6.add(jLabel1, gridBagConstraints);

        jTextFieldCaseDeath.setBackground(new java.awt.Color(204, 255, 255));
        jTextFieldCaseDeath.setFont(jTextFieldCaseDeath.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 9, 0, 0);
        jPanel6.add(jTextFieldCaseDeath, gridBagConstraints);

        jLabel3.setFont(jLabel3.getFont());
        jLabel3.setText("ʶҹС�õ�駤����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 9, 0, 0);
        jPanel6.add(jLabel3, gridBagConstraints);

        jComboBoxCaseDeathPreg.setBackground(new java.awt.Color(204, 255, 255));
        jComboBoxCaseDeathPreg.setFont(jComboBoxCaseDeathPreg.getFont());
        jComboBoxCaseDeathPreg.setRenderer(blueRenderer);
        jComboBoxCaseDeathPreg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxCaseDeathPregActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 0, 0);
        jPanel6.add(jComboBoxCaseDeathPreg, gridBagConstraints);

        txtPregnantWeeks.setEditable(false);
        txtPregnantWeeks.setDigits(2);
        txtPregnantWeeks.setFont(txtPregnantWeeks.getFont());
        txtPregnantWeeks.setMinimumSize(new java.awt.Dimension(40, 21));
        txtPregnantWeeks.setPreferredSize(new java.awt.Dimension(40, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 0, 0);
        jPanel6.add(txtPregnantWeeks, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 0, 5);
        jPanel4.add(jPanel6, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weighty = 1.0;
        jPanel1.add(jPanel4, gridBagConstraints);

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("Diag_Death_Search"));
        jPanel5.setMinimumSize(new java.awt.Dimension(280, 90));
        jPanel5.setPreferredSize(new java.awt.Dimension(280, 90));
        jPanel5.setLayout(new java.awt.GridBagLayout());

        jPanel9.setMinimumSize(new java.awt.Dimension(220, 90));
        jPanel9.setPreferredSize(new java.awt.Dimension(220, 90));
        jPanel9.setLayout(new java.awt.GridBagLayout());

        jScrollPane1.setMinimumSize(new java.awt.Dimension(100, 24));
        jScrollPane1.setPreferredSize(new java.awt.Dimension(100, 404));

        jTableListDeath.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTableListDeath.setFillsViewportHeight(true);
        jTableListDeath.setFont(jTableListDeath.getFont());
        jTableListDeath.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTableListDeathMouseReleased(evt);
            }
        });
        jTableListDeath.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTableListDeathKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(jTableListDeath);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel9.add(jScrollPane1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 0);
        jPanel5.add(jPanel9, gridBagConstraints);

        jPanel10.setLayout(new java.awt.GridBagLayout());

        jLabel4.setFont(jLabel4.getFont());
        jLabel4.setText("���� HN");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 2);
        jPanel10.add(jLabel4, gridBagConstraints);

        jButtonSearch.setFont(jButtonSearch.getFont());
        jButtonSearch.setText("Search");
        jButtonSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSearchActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 2);
        jPanel10.add(jButtonSearch, gridBagConstraints);

        jCheckBoxSearchByDate.setFont(jCheckBoxSearchByDate.getFont());
        jCheckBoxSearchByDate.setSelected(true);
        jCheckBoxSearchByDate.setText("Date");
        jCheckBoxSearchByDate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxSearchByDateActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        jPanel10.add(jCheckBoxSearchByDate, gridBagConstraints);

        hNTextFieldSearchHN.setFont(hNTextFieldSearchHN.getFont());
        hNTextFieldSearchHN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hNTextFieldSearchHNActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        jPanel10.add(hNTextFieldSearchHN, gridBagConstraints);

        jPanel12.setLayout(new java.awt.GridBagLayout());

        dateComboBoxFrom.setFont(dateComboBoxFrom.getFont());
        dateComboBoxFrom.setMinimumSize(new java.awt.Dimension(90, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 2);
        jPanel12.add(dateComboBoxFrom, gridBagConstraints);

        dateComboBoxTo.setFont(dateComboBoxTo.getFont());
        dateComboBoxTo.setMinimumSize(new java.awt.Dimension(90, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 10, 1, 2);
        jPanel12.add(dateComboBoxTo, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel10.add(jPanel12, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        jPanel5.add(jPanel10, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel1.add(jPanel5, gridBagConstraints);

        jLabelStatus.setFont(jLabelStatus.getFont());
        jLabelStatus.setMaximumSize(new java.awt.Dimension(4, 24));
        jLabelStatus.setMinimumSize(new java.awt.Dimension(4, 20));
        jLabelStatus.setOpaque(true);
        jLabelStatus.setPreferredSize(new java.awt.Dimension(4, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        jPanel1.add(jLabelStatus, gridBagConstraints);

        getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void hNTextFieldSearchHNActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hNTextFieldSearchHNActionPerformed
        searchDeath();
        if (this.jTableListDeath.getRowCount() > 0) {
            jTableListDeath.setRowSelectionInterval(0, 0);
            this.selectListDeath();
        }
    }//GEN-LAST:event_hNTextFieldSearchHNActionPerformed

    private void jCheckBoxSearchByDateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxSearchByDateActionPerformed
        boolean b = this.jCheckBoxSearchByDate.isSelected();
        this.dateComboBoxFrom.setEnabled(b);
        this.dateComboBoxTo.setEnabled(b);
        searchDeath();
        if (this.jTableListDeath.getRowCount() > 0) {
            jTableListDeath.setRowSelectionInterval(0, 0);
            this.selectListDeath();
        }
    }//GEN-LAST:event_jCheckBoxSearchByDateActionPerformed

    private void jTableListDeathKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTableListDeathKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_UP || evt.getKeyCode() == KeyEvent.VK_DOWN) {
            selectListDeath();
        }
    }//GEN-LAST:event_jTableListDeathKeyReleased

    private void jButtonCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelActionPerformed
        dispose();
    }//GEN-LAST:event_jButtonCancelActionPerformed
    private void jButtonSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSaveActionPerformed
        this.doSaveOrUpdate();
    }//GEN-LAST:event_jButtonSaveActionPerformed
    private void jTableListDeathMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableListDeathMouseReleased
        selectListDeath();
    }//GEN-LAST:event_jTableListDeathMouseReleased

    private void jButtonSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSearchActionPerformed
        searchDeath();
        if (this.jTableListDeath.getRowCount() > 0) {
            jTableListDeath.setRowSelectionInterval(0, 0);
            this.selectListDeath();
        }
    }//GEN-LAST:event_jButtonSearchActionPerformed

    private void jButtonDelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDelActionPerformed
        deleteDeath();
    }//GEN-LAST:event_jButtonDelActionPerformed

    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        closeDialog = true;
        dispose();
    }//GEN-LAST:event_closeDialog

    private void jComboBoxCaseDeathPregActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxCaseDeathPregActionPerformed
        txtPregnantWeeks.setEditable(ComboboxModel.getCodeComboBox(jComboBoxCaseDeathPreg).equals("1"));
    }//GEN-LAST:event_jComboBoxCaseDeathPregActionPerformed

    private void jComboBoxLocalTypeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxLocalTypeActionPerformed
        this.doValidateUI();
    }//GEN-LAST:event_jComboBoxLocalTypeActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.hospital_os.utility.DateComboBox dateComboBoxDdeath;
    private com.hospital_os.utility.DateComboBox dateComboBoxFrom;
    private com.hospital_os.utility.DateComboBox dateComboBoxTo;
    private javax.swing.JTextField hNTextFieldSearchHN;
    private javax.swing.JButton jButtonCancel;
    private javax.swing.JButton jButtonDel;
    private javax.swing.JButton jButtonSave;
    private javax.swing.JButton jButtonSearch;
    private javax.swing.JCheckBox jCheckBoxSearchByDate;
    private javax.swing.JComboBox jComboBoxCaseDeathPreg;
    private javax.swing.JComboBox jComboBoxLocalType;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelCaseDeath;
    private javax.swing.JLabel jLabelHN;
    private javax.swing.JLabel jLabelPlaceDeath;
    private javax.swing.JLabel jLabelStatus;
    private javax.swing.JLabel jLabelVisit;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private com.hosv3.gui.component.HJTableSort jTableListDeath;
    private com.hosv3.gui.component.HosComboBox jTextFieldCOdiseae;
    private com.hosv3.gui.component.HosComboBox jTextFieldCaseDeath;
    private com.hosv3.gui.component.HosComboBox jTextFieldCdeatha;
    private com.hosv3.gui.component.HosComboBox jTextFieldCdeathb;
    private com.hosv3.gui.component.HosComboBox jTextFieldCdeathc;
    private com.hosv3.gui.component.HosComboBox jTextFieldCdeathd;
    private com.hospital_os.utility.TimeTextField jTextFieldTimeDeath;
    private com.hospital_os.utility.IntegerTextField txtPregnantWeeks;
    // End of variables declaration//GEN-END:variables
    /*
     * �ѹ�� ��� GO ��� OG
     */

    private void getDeath(Death theDeath) {
        if (theDeath == null) {
            return;
        }
        // Somprasong cdeath ��੾������ icd10 ����� PK �ͧ table icd10 �֧��ͧ��Ẻ���
        String[] details = jTextFieldCaseDeath.getDetail().split(":");
        theDeath.cdeath = details[0];
        theDeath.pdeath = ComboboxModel.getCodeComboBox(jComboBoxLocalType);
        theDeath.ddeath = dateComboBoxDdeath.getText() + "," + jTextFieldTimeDeath.getText();
        theDeath.cdeath_a = getTextCombo(jTextFieldCdeatha.getSelectedItem());
        theDeath.cdeath_b = getTextCombo(jTextFieldCdeathb.getSelectedItem());
        theDeath.cdeath_c = getTextCombo(jTextFieldCdeathc.getSelectedItem());
        theDeath.cdeath_d = getTextCombo(jTextFieldCdeathd.getSelectedItem());
        theDeath.odiseae = jTextFieldCOdiseae.getDetail().split(":")[0];
        theDeath.death_pregnancy_status = jComboBoxCaseDeathPreg.isEnabled() ? ComboboxModel.getCodeComboBox(jComboBoxCaseDeathPreg) : "";
        theDeath.wpreg = theDeath.death_pregnancy_status.equals("1") ? txtPregnantWeeks.getText() : "";
        theDeath.active = "1";
        theDeath.user_record = theHO.theEmployee.getObjectId();
    }

    private String getTextCombo(Object obj) {
        if (obj == null) {
            return "";
        }
        try {
            ICD10 icd10 = (ICD10) obj;
            return icd10.getObjectId();
        } catch (Exception e) {
        }
        return "";
    }

    /**
     * ���Ң����� Death input : hn dateFrom dateTo outup : �š�ä��� Vector
     * vDeath
     */
    private void searchDeath() {
        String dateFrom = dateComboBoxFrom.getText();
        String dateTo = dateComboBoxTo.getText();
        String hn = hNTextFieldSearchHN.getText();
        boolean by_date = this.jCheckBoxSearchByDate.isSelected();
        if (!by_date) {
            vDeath = theDiagnosisControl.listDeath(hn);
        } else {
            vDeath = theDiagnosisControl.listDeath(dateFrom, dateTo, hn);
        }
        setTableListDeath(vDeath);
    }

    /**
     * �ʴ������š�ä��Ң����š�õ�� �¹� vector ���ʴ� input :vector vDeath
     * output :
     */
    private void setTableListDeath(Vector vdeath) {
        TaBleModel tm;

        if (vdeath == null || vdeath.isEmpty()) {
            tm = new TaBleModel(col_jTableListDeath, 0);
            jTableListDeath.setModel(tm);
            return;
        }
        tm = new TaBleModel(col_jTableListDeath, vdeath.size());
        for (int i = 0; i < vdeath.size(); i++) {
            Death aDeath = (Death) vdeath.get(i);
            tm.setValueAt(aDeath.family_name, i, 0);
            tm.setValueAt(aDeath.vn, i, 1);
            tm.setValueAt(DateUtil.getDateFromText(aDeath.ddeath), i, 2);

            jTableListDeath.setModel(tm);
        }

        jTableListDeath.getColumnModel().getColumn(0).setWidth(50);
        jTableListDeath.getColumnModel().getColumn(1).setWidth(20);
        jTableListDeath.getColumnModel().getColumn(1).setCellRenderer(vnRender);
        jTableListDeath.getColumnModel().getColumn(2).setWidth(50);
        jTableListDeath.getColumnModel().getColumn(2).setCellRenderer(dateRender);
    }

    private void selectListDeath() {
        int row = jTableListDeath.getSelectedRow();
        if (vDeath == null || vDeath.isEmpty() || row < 0) {
            return;
        }
        theDeath = (Death) vDeath.get(row);
        theFamily = thePatientControl.readFamilyByFamilyIdRet(theDeath.family_id);
        thePatient = thePatientControl.readPatientByPatientIdRet(theDeath.patient_id);
        theVisit = theVisitControl.readVisitByVnRet(theDeath.vn);
        setDeath(thePatient, theVisit, theDeath, theFamily);
    }

    /*
     * �ӡ��ź�����š�õ�� input : object death
     */
    private void deleteDeath() {
        boolean ret = theDiagnosisControl.deleteDeath(theDeath, theVisit, thePatient, theFamily, this);
        if (!ret) {
            return;
        }
        this.searchDeath();
        if (this.jTableListDeath.getRowCount() > 0) {
            jTableListDeath.setRowSelectionInterval(0, 0);
            selectListDeath();
        } else {
            if (thePatient != null) {
                this.setDeath(thePatient, theVisit, theHO.initDeath(), theFamily);
            }
        }
    }

    /**
     * Jao �Ѻ Object ����Ҩҡ PCU 07/04/2549
     */
    private void setDeath(Patient p, Visit v, Death d, Family fm) {
        // set global value
        theVisit = v;
        thePatient = p;
        theFamily = fm;
        theDeath = d;
        // clear ui
        jLabelHN.setText("");
        jLabelVisit.setText("");
        jTextFieldCaseDeath.setText("");
        jTextFieldCdeatha.setText("");
        jTextFieldCdeathb.setText("");
        jTextFieldCdeathc.setText("");
        jTextFieldCdeathd.setText("");
        jTextFieldCOdiseae.setText("");
        Gutil.setGuiData(jComboBoxCaseDeathPreg, "0");
        Gutil.setGuiData(jComboBoxLocalType, "2");
        txtPregnantWeeks.setText("");
        txtPregnantWeeks.setEditable(false);
        // set HN text
        String patient_name = "";
        if (thePatient != null) {
            patient_name += theLookupControl.getRenderTextHN(thePatient.hn) + " ";
        }
        if (theFamily != null) {
            patient_name += theFamily.patient_name + " " + theFamily.patient_last_name;
            jComboBoxCaseDeathPreg.setEnabled(!theFamily.f_sex_id.equals(Sex.isMAN()));
        }
        jLabelHN.setText(patient_name);
        // init visit data
        if (theVisit != null) {
            // set vn text
            jLabelVisit.setText(theLookupControl.getRenderTextVN(v.vn));
            Gutil.setGuiData(jComboBoxCaseDeathPreg, theVisit.pregnant.equals("1") ? "1" : "0");
        }
        // set death data
        if (theDeath != null) {
            ICD10 icd10 = theDiagnosisControl.getICD10ByNumber(theDeath.cdeath);
            jTextFieldCaseDeath.setText(icd10 == null ? "" : icd10.getObjectId());
            jTextFieldCdeatha.setText(theDeath.cdeath_a);
            jTextFieldCdeathb.setText(theDeath.cdeath_b);
            jTextFieldCdeathc.setText(theDeath.cdeath_c);
            jTextFieldCdeathd.setText(theDeath.cdeath_d);
            icd10 = theDiagnosisControl.getICD10ByNumber(theDeath.odiseae);
            jTextFieldCOdiseae.setText(icd10 == null ? "" : icd10.getObjectId());
            if (theDeath.ddeath.length() == 10) {
                jTextFieldTimeDeath.setText("");
            } else if (theDeath.ddeath.length() == 16) {
                jTextFieldTimeDeath.setText(theDeath.ddeath.substring(11, 16));
            }
            dateComboBoxDdeath.setText(DateUtil.convertFieldDate(theDeath.ddeath));
            Gutil.setGuiData(jComboBoxLocalType, theDeath.pdeath);
            Gutil.setGuiData(jComboBoxCaseDeathPreg, theDeath.death_pregnancy_status.isEmpty() ? "0" : theDeath.death_pregnancy_status);
            txtPregnantWeeks.setText(theDeath.wpreg);
        }
        txtPregnantWeeks.setEditable(ComboboxModel.getCodeComboBox(jComboBoxCaseDeathPreg).equals("1"));
        doValidateUI();
    }

    public boolean showDialog(Death dt) {
        mode_not_save = true;
        thePatient = theHO.thePatient;
        theVisit = theHO.theVisit;
        theFamily = theHO.theFamily;
        this.setStatus("", UpdateStatus.NORMAL);
        setDeath(thePatient, theVisit, dt, theFamily);
        setVisible(true);
        setEnableAll(true);
        return actionCommand;
    }

    public boolean showDialog(Family fm, Patient pt, Visit vs) {
        thePatient = pt;
        theVisit = vs;
        theFamily = fm;
        this.setStatus("", UpdateStatus.NORMAL);
        this.jCheckBoxSearchByDate.setSelected(false);
        this.dateComboBoxFrom.setEnabled(false);
        this.dateComboBoxTo.setEnabled(false);
        if (thePatient != null) {
            hNTextFieldSearchHN.setText(thePatient.hn);
        } else if (theFamily != null) {
            hNTextFieldSearchHN.setText(theFamily.patient_name);
        }
        this.searchDeath();
        if (jTableListDeath.getRowCount() == 0) {
            if (thePatient != null) {
                this.setDeath(pt, vs, theHO.initDeath(), fm);
            }
        } else {
            jTableListDeath.setRowSelectionInterval(0, 0);
            this.selectListDeath();
        }

        setVisible(true);
        if (actionCommand) {
            return true;
        }
        return false;
    }

    /*
     * Jao PCU ���� Function �����¡ Dialog 07/04/2549
     */
    public boolean showDialogForPCU(Death de, Family fm) {
        setDeath(null, null, de, fm);
        setLanguage("");
        setEnableAll(true);
        if (actionCommand) {
            return true;
        }
        System.gc();
        this.setSize(640, 520);
        this.setTitle(Constant.getTextBundle("�ѹ�֡�����š�õ��"));
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        return false;
    }

    private void setComboBoxEdit(boolean b) {
        dateComboBoxDdeath.setEditable(b);
        dateComboBoxFrom.setEditable(b);
        dateComboBoxTo.setEditable(b);
    }

    public void setLanguage(String msg) {
        GuiLang.setLanguage(jCheckBoxSearchByDate);//�ѹ������
        GuiLang.setLanguage(jButtonSearch);//��������
        GuiLang.setLanguage(jButtonSave);//�����ѹ�֡
        GuiLang.setLanguage(jLabelPlaceDeath);//ʶҹ������-�͡�ç��Һ��
        GuiLang.setLanguage(jLabelCaseDeath);//���˵ء�õ��
        GuiLang.setLanguage(jLabel12);//�ѹ�����
        GuiLang.setLanguage(jLabel13);//�ä��������˵ء�õ�� a 
        GuiLang.setLanguage(jLabel14);//�ä��������˵ء�õ�� b
        GuiLang.setLanguage(jLabel15);//�ä��������˵ء�õ�� c
        GuiLang.setLanguage(jLabel16);//�ä��������˵ء�õ�� d
        GuiLang.setLanguage(jLabel17);//�ä����������蹷����˵�˹ع
        GuiLang.setLanguage(jLabel4);
        GuiLang.setLanguage(jLabel8);
        GuiLang.setLanguage(jLabel9);
        GuiLang.setLanguage(jLabel2);//�ӹǹ��駤����
        GuiLang.setLanguage(jLabel1);//�Ѻ����
        GuiLang.setLanguage(jButtonCancel);
        GuiLang.setLanguage(col_jTableListDeath);
        GuiLang.setTextBundle(jPanel4);
        GuiLang.setTextBundle(jPanel5);
        GuiLang.setLanguage(jLabel6);
    }

    private boolean checkAddData() {
        if (jTextFieldCaseDeath.getText().isEmpty() && !("2".equals(ComboboxModel.getCodeComboBox(jComboBoxLocalType)))) {
            setStatus("��س��к����˵ء�õ�� (ICD-10)", UpdateStatus.WARNING);
            return false;
        }
        return true;
    }

    private void doSaveOrUpdate() {
        if (checkAddData()) {
            getDeath(theDeath);
            this.actionCommand = true;
            if (this.mode_not_save) {
                dispose();
            } else {
                theDiagnosisControl.saveDeath(thePatient, theFamily, theDeath, this);
                this.searchDeath();
            }
        }
    }

    private void doValidateUI() {
        setBlueField("2".equals(ComboboxModel.getCodeComboBox(jComboBoxLocalType)) ? new Color(255, 255, 255) : new Color(204, 255, 255));
    }

    private void setBlueField(Color color) {
        jTextFieldCaseDeath.setBackground(color);
        jTextFieldCaseDeath.getEditor().getEditorComponent().setBackground(color);
        ((JTextField) jTextFieldCaseDeath.getEditor().getEditorComponent()).setOpaque(true);
        ((JTextField) jTextFieldCaseDeath.getEditor().getEditorComponent()).setBackground(color);

        jTextFieldCdeatha.setBackground(color);
        jTextFieldCdeatha.getEditor().getEditorComponent().setBackground(color);
        ((JTextField) jTextFieldCdeatha.getEditor().getEditorComponent()).setOpaque(true);
        ((JTextField) jTextFieldCdeatha.getEditor().getEditorComponent()).setBackground(color);
    }
}

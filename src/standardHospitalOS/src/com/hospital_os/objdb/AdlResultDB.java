/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.AdlResult;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sompr
 */
public class AdlResultDB {

    private final ConnectionInf connectionInf;

    public AdlResultDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public AdlResult selectByValue(int value) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_adl_result where (? BETWEEN min_score AND max_score) limit 1";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setInt(1, value);
            List<AdlResult> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<AdlResult> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_adl_result";
            preparedStatement = connectionInf.ePQuery(sql);

            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<AdlResult> list(String... ids) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_adl_result where f_adl_result_id in (?) order by f_adl_result_id";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setArray(1, connectionInf.getConnection().createArrayOf("varchar", ids));
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<AdlResult> listByKeyword(String keyword) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_adl_result where description ilike ? order by description";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, "%" + (keyword == null || keyword.isEmpty() ? "" : keyword) + "%");
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<AdlResult> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<AdlResult> list = new ArrayList<>();
        try (ResultSet rs = preparedStatement.executeQuery()) {
            while (rs.next()) {
                AdlResult obj = new AdlResult();
                obj.setObjectId(rs.getString("f_adl_result_id"));
                obj.description = rs.getString("description");
                obj.min_score = rs.getInt("min_score");
                obj.max_score = rs.getInt("max_score");
                list.add(obj);
            }
            return list;
        }
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<>();
        for (AdlResult obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }

    public List<CommonInf> getComboboxDatasource(String... ids) throws Exception {
        List<CommonInf> list = new ArrayList<>();
        for (AdlResult obj : list(ids)) {
            list.add((CommonInf) obj);
        }
        return list;
    }
}

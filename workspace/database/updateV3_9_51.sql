ALTER TABLE public.t_visit_vital_sign
   ADD COLUMN visit_vital_sign_spo2 character varying(6);

insert into f_gui_action (f_gui_action_id, gui_action_name) VALUES ('2604', 'ดูไฟล์ผลตรวจจาก EKG');

-- update db version
INSERT INTO s_version VALUES ('9701000000084', '84', 'Hospital OS, Community Edition', '3.9.51', '3.32.0', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_51.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.51');
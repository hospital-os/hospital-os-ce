/*
 * SolveEmptyFamilyThread.java
 *
 * Created on 16 �á�Ҥ� 2550, 9:06 �.
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package com.hosv3.control.thread;

import com.hospital_os.objdb.ChronicDB;
import com.hospital_os.objdb.PatientDB;
import com.hospital_os.objdb.SequenceDataDB;
import com.hospital_os.object.Patient;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hosv3.control.HosControl;
import com.hosv3.control.HosDB;
import com.hosv3.control.SystemControl;
import com.hosv3.objdb.Death2DB;
import com.hosv3.objdb.PatientPayment2DB;
import com.hosv3.objdb.Surveil2DB;
import com.hosv3.object.HosObject;
import com.hosv3.utility.ResourceBundle;
import com.pcu.objdb.objdbclass.FamilyDB;
import com.pcu.objdb.objdbclass.HomeDB;
import com.pcu.objdb.objdbclass.VillageDB;
import com.pcu.object.Family;
import com.pcu.object.Home;
import com.pcu.object.Village;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Aut
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class SolveEmptyFamilyThread extends ControlThread {

    /**
     * Creates a new instance of SolveEmptyFamilyThread
     */
    int total_number;

    public SolveEmptyFamilyThread() {
        this.setDaemon(true);
    }

    @Override
    public void setControl(ConnectionInf con, HosDB hdb, HosObject ho, UpdateStatus us, Object control) {
        theConnectionInf = con;
        theHosDB = hdb;
        theHO = ho;
        theUS = us;
    }

    public void setTotalNumber(int tot) {
        total_number = tot;
    }

    @Override
    protected void runTask() {
        ConnectionInf theConnectionInf2 = theConnectionInf.getClone();
        theConnectionInf2.open();
        try {
            FamilyDB familyDB = new FamilyDB(theConnectionInf2);
            PatientDB patientDB = new PatientDB(theConnectionInf2, familyDB);
            VillageDB villageDB = new VillageDB(theConnectionInf2);
            HomeDB homeDB = new HomeDB(theConnectionInf2);
            PatientPayment2DB patientPaymentDB = new PatientPayment2DB(theConnectionInf2);
            ChronicDB chronicDB = new ChronicDB(theConnectionInf2);
            Surveil2DB surveilDB = new Surveil2DB(theConnectionInf2);
            Death2DB deathDB = new Death2DB(theConnectionInf2);
            SequenceDataDB sequenceDataDB = new SequenceDataDB(theConnectionInf2);
            theConnectionInf2.getConnection().setAutoCommit(false);
            //��Ǩ�ͺ��Ъҡë�Өҡ�Ţ�ѵû�ЪҪ�
            if (theUS.confirmBox(ResourceBundle.getBundleGlobal("TEXT.CONFIRM")
                    + ResourceBundle.getBundleText("com.hosv3.control.thread.SolveEmptyFamilyThread.REMOVE.CID.SPACE"), UpdateStatus.WARNING)) {
                theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.thread.SolveEmptyFamilyThread.REMOVE.CID.SPACE"), UpdateStatus.WARNING);
                String sql = "select count(*) from t_health_family where length(patient_pid)>13";
                ResultSet rs = theConnectionInf2.eQuery(sql);
                rs.next();
                if (rs.getInt(1) > 0) {
                    sql = "update t_health_family set patient_pid = trim(patient_pid) where patient_pid in "
                            + "(select patient_pid from ( select length(patient_pid) as len,patient_pid from t_health_family )"
                            + " as q1 where q1.len>13);";
                    theConnectionInf2.eUpdate(sql);
                    sql = "update t_patient set patient_pid = trim(patient_pid) where patient_pid in (select patient_pid from "
                            + "(select length(patient_pid) as len,patient_pid from t_patient )"
                            + " as q1 where q1.len>13);";
                    theConnectionInf2.eUpdate(sql);
                }
            }
            if (theUS.confirmBox(ResourceBundle.getBundleText("com.hosv3.control.thread.SolveEmptyFamilyThread.CONFIRM.CLEAR.DUPLICATE.CID"),
                    UpdateStatus.WARNING)) {
                int res = 0;
                ResultSet rs = theConnectionInf2.eQuery("select * from ("
                        + "select patient_pid,count(*) as cnt from t_health_family where patient_pid<>'' group by patient_pid"
                        + ") as q where cnt>1");

                while (rs.next()) {
                    String pid = rs.getString(1);
                    res += theConnectionInf2.eUpdate("update t_health_family set patient_pid = '' where patient_pid = '"
                            + pid + "'");
                }
                rs = theConnectionInf2.eQuery("select * from ("
                        + "select patient_pid,count(*) as cnt from t_patient where patient_pid<>'' group by patient_pid"
                        + ") as q where cnt>1");

                while (rs.next()) {
                    String pid = rs.getString(1);
                    res += theConnectionInf2.eUpdate("update t_patient set patient_pid = '' where patient_pid = '"
                            + pid + "'");
                }

                theConnectionInf2.eUpdate("update t_patient set "
                        + " t_health_family_id = '' "
                        + " where t_health_family_id in (select  t_health_family_id from t_patient "
                        + " where trim(t_health_family_id) <> '' "
                        + " group by t_health_family_id having count(t_health_family_id) > 1)");
            }
            //�鹼����·������ѹ��Ѻ��Ъҡ� �µ�Ǩ�ͺ�ҡ�Ţ�ѵû�ЪҪ� ��зӡ��������§��������ѹ��
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.thread.SolveEmptyFamilyThread.FIND.PERSON.BY.CID"), UpdateStatus.WARNING);
            java.sql.ResultSet rs = theConnectionInf2.eQuery(
                    " select t_health_family.t_health_family_id "
                    + "   ,t_patient.t_patient_id "
                    + " from t_patient "
                    + "   inner join t_health_family on t_patient.patient_pid = t_health_family.patient_pid "
                    + " where t_patient.t_health_family_id ='' "
                    + "   and t_patient.patient_pid <> ''"
                    + "   and t_patient.patient_active = '1' ");
            int ii = 0;
            while (rs.next()) {
                String family_id = rs.getString(1);
                String patient_id = rs.getString(2);
                patientDB.updateFidByPtid(family_id, patient_id);
                if (ii++ % 100 == 0) {
                    theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.thread.SolveEmptyFamilyThread.FIND.PERSON.BY.CID") + " " + ii /*+ " : " 
                             * + ptid_vector.size()
                             */, UpdateStatus.WARNING);
                }
            }
            ///////////////////////////////////////////////////////////////////////////
            //�鹼����·���ѧ����繻�Ъҡ� ��зӡ�����ҧ��Ъҡ����ѹ��
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.thread.SolveEmptyFamilyThread.FIND.AND.CREATE.PERSON"), UpdateStatus.WARNING);


            Village vill_0 = villageDB.selectMoo0();
            if (vill_0 == null) {
                vill_0 = theHO.initVillage("0");
                villageDB.insert(vill_0);
            }
            Home home_0 = homeDB.selectByNo("0", vill_0.getObjectId());
            if (home_0 == null) {
                home_0 = theHO.initHome("0", vill_0);
                home_0.home_staff_record = theHO.theEmployee.getObjectId();
                home_0.home_record_date_time = theHO.date_time;
                homeDB.insert(home_0);
            }
            Vector vvillage = villageDB.selectActive();
            rs.close();

            rs = theConnectionInf2.eQuery(SystemControl.SQL_CHECK_PATIENT_NO_FAMILY);

            int i = 0;
            while (rs.next()) {
                i++;
                Patient patient = new Patient();
                PatientDB.getObject(PatientDB.getMapObject(), patient, rs);
                // Somprasong 20150918 not use
//                Village vill = vill_0;
//                for (int j = 0; j < vvillage.size(); j++) {
//                    Village village = (Village) vvillage.get(j);
//                    if (patient.village.equals(village.village_moo)
//                            && patient.tambon.equals(village.village_tambon)) {
//                        vill = village;
//                    }
//                }
                try {
                    Family fm = patient.getFamily();
                    fm.modify_date_time = theHO.date_time;
                    fm.staff_modify = theHO.theEmployee.getObjectId();
                    fm.hn_hcis = sequenceDataDB.updateSequence("hn_hcis", true);
                    fm.record_date_time = theHO.date_time;
                    fm.staff_record = theHO.theEmployee.getObjectId();
                    familyDB.insert(fm);
                    //��Ǩ�ͺ��Ҷ������Һ�ҹ���ѹ�֡���ʢͧ��Һ�ҹŧ�㹢����ź�ҹ����
                    patientDB.updateFidByPtid(fm.getObjectId(), patient.getObjectId());
                    patientPaymentDB.updateFidByPtid(fm.getObjectId(), patient.getObjectId());
                    chronicDB.updateFidByPtid(fm.getObjectId(), patient.getObjectId());
                    surveilDB.updateFidByPtid(fm.getObjectId(), patient.getObjectId());
                    deathDB.updateFidByPtid(fm.getObjectId(), patient.getObjectId());
                } catch (Exception ex) {
                    LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
                }
                if (i % 100 == 0) {
                    theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.thread.SolveEmptyFamilyThread.FIND.AND.CREATE.PERSON")
                            + " " + i + " : ", UpdateStatus.WARNING);
                }
            }
            theConnectionInf2.getConnection().commit();
            theUS.confirmBox(ResourceBundle.getBundleText("com.hosv3.control.SystemControl.RESOLVE.EMPTY.PERSON") + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.UNKONW, ResourceBundle.getBundleText("com.hosv3.control.SystemControl.RESOLVE.EMPTY.PERSON"));
            Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf2.getConnection().rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            try {
                theConnectionInf2.getConnection().close();
            } catch (SQLException ex) {
                Logger.getLogger(SolveEmptyFamilyThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    private static final Logger LOG = Logger.getLogger(SolveEmptyFamilyThread.class.getName());
}

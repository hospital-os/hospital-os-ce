
CREATE TRIGGER update_hstock_order_return 
AFTER UPDATE ON t_order_return 
FOR EACH ROW
WHEN ((OLD.receive_qty is null)
      AND (NEW.receive_qty is not null
            OR NEW.receive_qty > 0)) 
EXECUTE PROCEDURE update_hstock_order_return();

ALTER TABLE public.b_hstock_item_price ADD item_price_ipd float8 NOT NULL DEFAULT 0;

BEGIN;
INSERT INTO f_hstock_user_authen SELECT '0210','เบิกใช้ในหน่วยงาน'
WHERE NOT EXISTS (SELECT 1 FROM f_hstock_user_authen WHERE f_hstock_user_authen_id = '0210'); 
COMMIT;
BEGIN;
INSERT INTO f_hstock_user_authen SELECT '0211','ตัดใช้ในหน่วยงาน'
WHERE NOT EXISTS (SELECT 1 FROM f_hstock_user_authen WHERE f_hstock_user_authen_id = '0211'); 
COMMIT;

CREATE TABLE IF NOT EXISTS b_hstock_department (
    b_hstock_department_id      CHARACTER VARYING(255) NOT NULL,
    hstock_department_code      CHARACTER VARYING(255) NOT NULL,
    hstock_department_name      TEXT NOT NULL,
    active                  CHARACTER VARYING(1) NOT NULL default '1',
    record_datetime         timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    user_record_id          CHARACTER VARYING(255) NOT NULL,
    update_datetime         timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    user_update_id          CHARACTER VARYING(255) NOT NULL,
    CONSTRAINT b_hstock_department_pk PRIMARY KEY (b_hstock_department_id),
    CONSTRAINT b_hstock_department_unique UNIQUE (hstock_department_code),
    CONSTRAINT user_record_id_fk FOREIGN KEY (user_record_id) REFERENCES b_employee (b_employee_id) ON DELETE SET NULL,
    CONSTRAINT user_update_id_fk FOREIGN KEY (user_update_id) REFERENCES b_employee (b_employee_id) ON DELETE SET NULL
);

-- ประเภทการขอเบิกใช้
CREATE TABLE IF NOT EXISTS f_hstock_used_method (   
    f_hstock_used_method_id             CHARACTER VARYING(4) NOT NULL,
    description                         text NOT NULL,  
    CONSTRAINT f_hstock_used_method_pk PRIMARY KEY (f_hstock_used_method_id)
);

INSERT INTO f_hstock_used_method VALUES ('1','เบิกใช้ในหน่วยงาน');

-- สถานะการเบิกใช้
CREATE TABLE IF NOT EXISTS f_hstock_used_status (   
    f_hstock_used_status_id             CHARACTER VARYING(4) NOT NULL,
    description                         text NOT NULL,  
    CONSTRAINT f_hstock_used_status_pk PRIMARY KEY (f_hstock_used_status_id)
);

INSERT INTO f_hstock_used_status VALUES ('1','เบิกใช้ในหน่วยงาน'),('2','ตัดใช้ในหน่วยงาน'),('3','ยกเลิก');

-- เก็บข้อมูลขอเบิกใช้ในหน่วยงาน
CREATE TABLE IF NOT EXISTS t_hstock_used_department (   
    t_hstock_used_department_id     CHARACTER VARYING(50) NOT NULL,
    b_hstock_id                     CHARACTER VARYING(50) NOT NULL, -- b_hstock_id
    b_hstock_department_id          CHARACTER VARYING(50) NOT NULL, -- b_hstock_department_id
    used_department_number          text NOT NULL,
    f_hstock_used_method_id         CHARACTER VARYING(4) NOT NULL,
    f_hstock_used_status_id         CHARACTER VARYING(4) NOT NULL,
    request_datetime                timestamp with time zone NOT NULL DEFAULT current_timestamp,
    record_datetime                 timestamp with time zone NOT NULL DEFAULT current_timestamp,
    user_record_id                  CHARACTER VARYING(50) NOT NULL,
    update_datetime                 timestamp with time zone NOT NULL DEFAULT current_timestamp,
    user_update_id                  CHARACTER VARYING(50) NOT NULL,
    cancel_datetime                 timestamp with time zone,
    user_cancel_id                  CHARACTER VARYING(50),
    
    CONSTRAINT t_hstock_used_department_pk PRIMARY KEY (t_hstock_used_department_id),  
    CONSTRAINT t_hstock_used_department_hstock_fk FOREIGN KEY (b_hstock_id) 
        REFERENCES b_hstock (b_hstock_id) ON UPDATE NO ACTION ON DELETE NO ACTION,  
    CONSTRAINT t_hstock_used_department_method_fk FOREIGN KEY (f_hstock_used_method_id) 
        REFERENCES f_hstock_used_method (f_hstock_used_method_id) ON UPDATE NO ACTION ON DELETE NO ACTION,  
    CONSTRAINT t_hstock_used_department_status_fk FOREIGN KEY (f_hstock_used_status_id) 
        REFERENCES f_hstock_used_status (f_hstock_used_status_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT user_record_id_fk FOREIGN KEY (user_record_id) 
        REFERENCES b_employee (b_employee_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT user_update_id_fk FOREIGN KEY (user_update_id) 
        REFERENCES b_employee (b_employee_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT user_cancel_id_fk FOREIGN KEY (user_cancel_id) 
        REFERENCES b_employee (b_employee_id) ON UPDATE NO ACTION ON DELETE NO ACTION
);
-- sequence และ trigger ออกเลขใบขอเบิกใช้ในหน่วยงาน  
CREATE SEQUENCE IF NOT EXISTS used_department_number_request;

CREATE OR REPLACE FUNCTION t_hstock_generate_used_department_number()
RETURNS trigger AS $$
DECLARE 
BEGIN
        IF (select max(substr(used_department_number,2,4)) from t_hstock_used_department) = (EXTRACT(YEAR  FROM current_date) + 543)::text THEN
            NEW.used_department_number := 'C'||(EXTRACT(YEAR  FROM current_date) + 543)::text||TO_CHAR(current_date,'-MM-')|| lpad( nextval('used_department_number_request'::regclass)::text,5,'0');    
        ELSE
            ALTER SEQUENCE used_department_number_request RESTART WITH 1;
            NEW.used_department_number := 'C'||(EXTRACT(YEAR  FROM current_date) + 543)::text||TO_CHAR(current_date,'-MM-')|| lpad( nextval('used_department_number_request'::regclass)::text,5,'0');
        END IF;
        RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

CREATE TRIGGER t_hstock_generate_used_department_number
BEFORE INSERT ON t_hstock_used_department
FOR EACH ROW 
EXECUTE PROCEDURE t_hstock_generate_used_department_number();

CREATE TABLE IF NOT EXISTS t_hstock_used_department_item (  
    t_hstock_used_department_item_id    CHARACTER VARYING(50) NOT NULL,
    t_hstock_used_department_id     CHARACTER VARYING(50) NOT NULL,
    b_item_id                           CHARACTER VARYING(50) NOT NULL,     
    b_hstock_item_id                    CHARACTER VARYING(50) NOT NULL, 
    req_qty                             float8 NOT NULL default 0,
    partial_pay                         CHARACTER VARYING(1) NOT NULL  default '0', --1 จ่ายบางส่วน
    record_datetime                     timestamp with time zone NOT NULL DEFAULT current_timestamp,
    user_record_id                      CHARACTER VARYING(50) NOT NULL,
    update_datetime                     timestamp with time zone NOT NULL DEFAULT current_timestamp,
    user_update_id                      CHARACTER VARYING(50) NOT NULL,
    
    CONSTRAINT t_hstock_used_department_item_pk PRIMARY KEY (t_hstock_used_department_item_id),  
    CONSTRAINT b_item_id_fk FOREIGN KEY (b_item_id) 
        REFERENCES b_item (b_item_id) ON UPDATE NO ACTION ON DELETE NO ACTION,  
    CONSTRAINT b_hstock_item_id_fk FOREIGN KEY (b_hstock_item_id) 
        REFERENCES b_hstock_item (b_hstock_item_id) ON UPDATE NO ACTION ON DELETE NO ACTION,  
    CONSTRAINT t_hstock_used_department_id_fk FOREIGN KEY (t_hstock_used_department_id) 
        REFERENCES t_hstock_used_department (t_hstock_used_department_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT user_record_id_fk FOREIGN KEY (user_record_id) 
        REFERENCES b_employee (b_employee_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT user_update_id_fk FOREIGN KEY (user_update_id) 
        REFERENCES b_employee (b_employee_id) ON UPDATE NO ACTION ON DELETE NO ACTION
);

BEGIN;
INSERT INTO f_hstock_adjust_type SELECT '21','ตัดจ่ายใช้ในหน่วยงาน','1','0','1'
WHERE NOT EXISTS (SELECT 1 FROM f_hstock_adjust_type WHERE f_hstock_adjust_type_id = '21'); 
COMMIT;

ALTER TABLE t_hstock_card ADD COLUMN IF NOT EXISTS t_hstock_used_department_item_id CHARACTER VARYING(50) DEFAULT NULL;
ALTER TABLE t_hstock_card ADD CONSTRAINT t_hstock_used_department_item_id_fk FOREIGN KEY(t_hstock_used_department_item_id)
    REFERENCES t_hstock_used_department_item (t_hstock_used_department_item_id) ON UPDATE NO ACTION ON DELETE NO ACTION;

CREATE TABLE IF NOT EXISTS b_hstock_purchase_reason (
    b_hstock_purchase_reason_id     CHARACTER VARYING(255) NOT NULL,
    hstock_purchase_reason_code     CHARACTER VARYING(255) NOT NULL,
    hstock_purchase_reason_name     TEXT NOT NULL,
    active                          CHARACTER VARYING(1) NOT NULL default '1',
    record_datetime                 timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    user_record_id                  CHARACTER VARYING(255) NOT NULL,
    update_datetime                 timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    user_update_id                  CHARACTER VARYING(255) NOT NULL,
    CONSTRAINT hstock_purchase_reason_pk PRIMARY KEY (b_hstock_purchase_reason_id),
    CONSTRAINT hstock_purchase_reason_unique UNIQUE (hstock_purchase_reason_code),
    CONSTRAINT user_record_id_fk FOREIGN KEY (user_record_id) REFERENCES b_employee (b_employee_id) ON DELETE SET NULL,
    CONSTRAINT user_update_id_fk FOREIGN KEY (user_update_id) REFERENCES b_employee (b_employee_id) ON DELETE SET NULL
);

BEGIN;
INSERT INTO f_hstock_user_authen SELECT '0212','ขอซื้อ'
WHERE NOT EXISTS (SELECT 1 FROM f_hstock_user_authen WHERE f_hstock_user_authen_id = '0212'); 
COMMIT;
BEGIN;
INSERT INTO f_hstock_user_authen SELECT '0213','อนุมัติขอซื้อ'
WHERE NOT EXISTS (SELECT 1 FROM f_hstock_user_authen WHERE f_hstock_user_authen_id = '0213'); 
COMMIT;


CREATE TABLE IF NOT EXISTS f_hstock_requisition_status (    
    f_hstock_requisition_status_id      CHARACTER VARYING(4) NOT NULL,
    description                         text NOT NULL,  
    CONSTRAINT f_hstock_requisition_status_pk PRIMARY KEY (f_hstock_requisition_status_id)
);

INSERT INTO f_hstock_requisition_status VALUES ('1','ขอซื้อ'),('2','อนุมัติขอซื้อ'),('3','ยกเลิก');

CREATE TABLE IF NOT EXISTS t_hstock_requisition (   
    t_hstock_requisition_id         CHARACTER VARYING(50) NOT NULL,
    b_hstock_id                     CHARACTER VARYING(50) NOT NULL, -- b_hstock_id
    requisition_number              text NOT NULL,
    f_hstock_requisition_status_id  CHARACTER VARYING(4) NOT NULL,
    request_datetime                timestamp with time zone NOT NULL DEFAULT current_timestamp,
    approve_datetime                timestamp with time zone,
    user_approve_id                 CHARACTER VARYING(50),
    record_datetime                 timestamp with time zone NOT NULL DEFAULT current_timestamp,
    user_record_id                  CHARACTER VARYING(50) NOT NULL,
    update_datetime                 timestamp with time zone NOT NULL DEFAULT current_timestamp,
    user_update_id                  CHARACTER VARYING(50) NOT NULL,
    cancel_datetime                 timestamp with time zone,
    user_cancel_id                  CHARACTER VARYING(50),
    
    CONSTRAINT t_hstock_requisition_pk PRIMARY KEY (t_hstock_requisition_id),  
    CONSTRAINT t_hstock_requisition_hstock_fk FOREIGN KEY (b_hstock_id) 
        REFERENCES b_hstock (b_hstock_id) ON UPDATE NO ACTION ON DELETE NO ACTION,  
    CONSTRAINT f_hstock_requisition_status_fk FOREIGN KEY (f_hstock_requisition_status_id) 
        REFERENCES f_hstock_requisition_status (f_hstock_requisition_status_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT user_approve_id_fk FOREIGN KEY (user_approve_id) 
        REFERENCES b_employee (b_employee_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT user_record_id_fk FOREIGN KEY (user_record_id) 
        REFERENCES b_employee (b_employee_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT user_update_id_fk FOREIGN KEY (user_update_id) 
        REFERENCES b_employee (b_employee_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT user_cancel_id_fk FOREIGN KEY (user_cancel_id) 
        REFERENCES b_employee (b_employee_id) ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE SEQUENCE IF NOT EXISTS requisition_number_request;

CREATE OR REPLACE FUNCTION t_hstock_generate_requisition_number()
RETURNS trigger AS $$
DECLARE 
BEGIN
        IF (select max(substr(requisition_number,3,4)) from t_hstock_requisition) = (EXTRACT(YEAR  FROM current_date) + 543)::text THEN
            NEW.requisition_number := 'PR'||(EXTRACT(YEAR  FROM current_date) + 543)::text||TO_CHAR(current_date,'-MM-')|| lpad( nextval('requisition_number_request'::regclass)::text,5,'0');    
        ELSE
            ALTER SEQUENCE requisition_number_request RESTART WITH 1;
            NEW.requisition_number := 'PR'||(EXTRACT(YEAR  FROM current_date) + 543)::text||TO_CHAR(current_date,'-MM-')|| lpad( nextval('requisition_number_request'::regclass)::text,5,'0');
        END IF;
        RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

CREATE TRIGGER t_hstock_generate_requisition_number
BEFORE INSERT ON t_hstock_requisition
FOR EACH ROW 
EXECUTE PROCEDURE t_hstock_generate_requisition_number();

CREATE TABLE IF NOT EXISTS t_hstock_requisition_item (  
    t_hstock_requisition_item_id    CHARACTER VARYING(50) NOT NULL,
    t_hstock_requisition_id         CHARACTER VARYING(50) NOT NULL,
    b_item_id                       CHARACTER VARYING(50) NOT NULL,     
    b_hstock_item_id                CHARACTER VARYING(50) NOT NULL, 
    order_status                    CHARACTER VARYING(1) NOT NULL default '0', -- 0 = ยังไม่สั่งซื้อ , 1 = สั่งซื้อแล้ว
    b_item_drug_uom_big_id          CHARACTER VARYING(255),  
    b_item_drug_uom_small_id        CHARACTER VARYING(255),  
    big_unit_rate                   INTEGER NOT NULL default 1,
    small_unit_rate                 INTEGER NOT NULL default 1,
    req_qty                         float8 NOT NULL default 0,
    vat                             INTEGER NOT NULL,   
    vat_action                      CHARACTER VARYING(1) NOT NULL default '1', -- 1 = รวม VAT , 0 = ยังไม่รวม VAT
    big_unit_price                  DOUBLE PRECISION NOT NULL,
    b_item_manufacturer_id          CHARACTER VARYING(255),
    b_item_distributor_id           CHARACTER VARYING(255),
    b_hstock_purchase_reason_id     CHARACTER VARYING(255),
    note                            TEXT,
    record_datetime                 timestamp with time zone NOT NULL DEFAULT current_timestamp,
    user_record_id                  CHARACTER VARYING(50) NOT NULL,
    update_datetime                 timestamp with time zone NOT NULL DEFAULT current_timestamp,
    user_update_id                  CHARACTER VARYING(50) NOT NULL,
    
    CONSTRAINT t_hstock_requisition_item_pk PRIMARY KEY (t_hstock_requisition_item_id),  
    CONSTRAINT b_item_id_fk FOREIGN KEY (b_item_id) 
        REFERENCES b_item (b_item_id) ON UPDATE NO ACTION ON DELETE NO ACTION,  
    CONSTRAINT b_hstock_item_id_fk FOREIGN KEY (b_hstock_item_id) 
        REFERENCES b_hstock_item (b_hstock_item_id) ON UPDATE NO ACTION ON DELETE NO ACTION,  
    CONSTRAINT t_hstock_requisition_id_fk FOREIGN KEY (t_hstock_requisition_id) 
        REFERENCES t_hstock_requisition (t_hstock_requisition_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT b_item_drug_uom_big_id_fk FOREIGN KEY (b_item_drug_uom_big_id) 
        REFERENCES b_item_drug_uom (b_item_drug_uom_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT b_item_drug_uom_small_id_fk FOREIGN KEY (b_item_drug_uom_small_id) 
        REFERENCES b_item_drug_uom (b_item_drug_uom_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT b_item_manufacturer_id_fk FOREIGN KEY (b_item_manufacturer_id) 
        REFERENCES b_item_manufacturer (b_item_manufacturer_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT b_item_distributor_id_fk FOREIGN KEY (b_item_distributor_id) 
        REFERENCES b_item_distributor (b_item_distributor_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT b_hstock_purchase_reason_id_fk FOREIGN KEY (b_hstock_purchase_reason_id) 
        REFERENCES b_hstock_purchase_reason (b_hstock_purchase_reason_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT user_record_id_fk FOREIGN KEY (user_record_id) 
        REFERENCES b_employee (b_employee_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT user_update_id_fk FOREIGN KEY (user_update_id) 
        REFERENCES b_employee (b_employee_id) ON UPDATE NO ACTION ON DELETE NO ACTION
);

BEGIN;
INSERT INTO f_hstock_user_authen SELECT '0214','สั่งซื้อ'
WHERE NOT EXISTS (SELECT 1 FROM f_hstock_user_authen WHERE f_hstock_user_authen_id = '0214'); 
COMMIT;

CREATE TABLE IF NOT EXISTS f_hstock_order_status (  
    f_hstock_order_status_id        CHARACTER VARYING(4) NOT NULL,
    description                     text NOT NULL,  
    CONSTRAINT f_hstock_order_status_pk PRIMARY KEY (f_hstock_order_status_id)
);

INSERT INTO f_hstock_order_status VALUES ('1','สั่งซื้อ'),('2','รับเข้า'),('3','ยกเลิก');

CREATE TABLE IF NOT EXISTS t_hstock_order ( 
    t_hstock_order_id           CHARACTER VARYING(50) NOT NULL,
    b_hstock_id                 CHARACTER VARYING(50) NOT NULL, -- b_hstock_id
    order_number                text NOT NULL,
    b_item_distributor_id       CHARACTER VARYING(50) NOT NULL,
    f_hstock_order_status_id    CHARACTER VARYING(4) NOT NULL,
    order_datetime              timestamp with time zone NOT NULL DEFAULT current_timestamp,
    record_datetime             timestamp with time zone NOT NULL DEFAULT current_timestamp,
    user_record_id              CHARACTER VARYING(50) NOT NULL,
    update_datetime             timestamp with time zone NOT NULL DEFAULT current_timestamp,
    user_update_id              CHARACTER VARYING(50) NOT NULL,
    cancel_datetime             timestamp with time zone,
    user_cancel_id              CHARACTER VARYING(50),
    
    CONSTRAINT t_hstock_order_pk PRIMARY KEY (t_hstock_order_id),  
    CONSTRAINT t_hstock_order_hstock_fk FOREIGN KEY (b_hstock_id) 
        REFERENCES b_hstock (b_hstock_id) ON UPDATE NO ACTION ON DELETE NO ACTION,  
    CONSTRAINT b_item_distributor_fk FOREIGN KEY (b_item_distributor_id) 
        REFERENCES b_item_distributor (b_item_distributor_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT f_hstock_order_status_fk FOREIGN KEY (f_hstock_order_status_id) 
        REFERENCES f_hstock_order_status (f_hstock_order_status_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT user_record_id_fk FOREIGN KEY (user_record_id) 
        REFERENCES b_employee (b_employee_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT user_update_id_fk FOREIGN KEY (user_update_id) 
        REFERENCES b_employee (b_employee_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT user_cancel_id_fk FOREIGN KEY (user_cancel_id) 
        REFERENCES b_employee (b_employee_id) ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE SEQUENCE IF NOT EXISTS generate_order_number;

CREATE OR REPLACE FUNCTION t_hstock_generate_order_number()
RETURNS trigger AS $$
DECLARE 
BEGIN
        IF (select max(substr(order_number,3,4)) from t_hstock_order) = (EXTRACT(YEAR  FROM current_date) + 543)::text THEN
            NEW.order_number := 'PO'||(EXTRACT(YEAR  FROM current_date) + 543)::text||TO_CHAR(current_date,'-MM-')|| lpad( nextval('generate_order_number'::regclass)::text,5,'0');    
        ELSE
            ALTER SEQUENCE generate_order_number RESTART WITH 1;
            NEW.order_number := 'PO'||(EXTRACT(YEAR  FROM current_date) + 543)::text||TO_CHAR(current_date,'-MM-')|| lpad( nextval('generate_order_number'::regclass)::text,5,'0');
        END IF;
        RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

CREATE TRIGGER t_hstock_generate_order_number
BEFORE INSERT ON t_hstock_order
FOR EACH ROW 
EXECUTE PROCEDURE t_hstock_generate_order_number();

CREATE TABLE IF NOT EXISTS t_hstock_order_item (    
    t_hstock_order_item_id          CHARACTER VARYING(50) NOT NULL,
    t_hstock_order_id               CHARACTER VARYING(50) NOT NULL,
    b_item_id                       CHARACTER VARYING(50) NOT NULL,     
    b_hstock_item_id                CHARACTER VARYING(50) NOT NULL, 
    receive_status                  CHARACTER VARYING(1) NOT NULL default '0', -- 0 = ยังไม่รับเข้า , 1 = รับเข้าแล้ว , 2 = รับเข้าบางส่วน
    b_item_manufacturer_id          CHARACTER VARYING(255),
    b_item_drug_uom_big_id          CHARACTER VARYING(255),  
    b_item_drug_uom_small_id        CHARACTER VARYING(255),  
    big_unit_rate                   INTEGER NOT NULL default 1,
    small_unit_rate                 INTEGER NOT NULL default 1,
    req_qty                         float8 NOT NULL default 0,
    free_qty                        float8 NOT NULL default 0,
    vat                             INTEGER NOT NULL,   
    vat_action                      CHARACTER VARYING(1) NOT NULL default '1', -- 1 = รวม VAT , 0 = ยังไม่รวม VAT
    big_unit_price                  DOUBLE PRECISION NOT NULL default 0,
    discount                        DOUBLE PRECISION NOT NULL default 0,
    due_date                        DATE NOT NULL,
    note                            TEXT,
    t_hstock_requisition_item_id    CHARACTER VARYING(50),
    record_datetime                 timestamp with time zone NOT NULL DEFAULT current_timestamp,
    user_record_id                  CHARACTER VARYING(50) NOT NULL,
    update_datetime                 timestamp with time zone NOT NULL DEFAULT current_timestamp,
    user_update_id                  CHARACTER VARYING(50) NOT NULL,
    receive_datetime                timestamp with time zone,
    user_receive_id                 CHARACTER VARYING(50),
    
    CONSTRAINT t_hstock_order_item_pk PRIMARY KEY (t_hstock_order_item_id),  
    CONSTRAINT t_hstock_order_id_fk FOREIGN KEY (t_hstock_order_id) 
        REFERENCES t_hstock_order (t_hstock_order_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT b_item_id_fk FOREIGN KEY (b_item_id) 
        REFERENCES b_item (b_item_id) ON UPDATE NO ACTION ON DELETE NO ACTION,  
    CONSTRAINT b_hstock_item_id_fk FOREIGN KEY (b_hstock_item_id) 
        REFERENCES b_hstock_item (b_hstock_item_id) ON UPDATE NO ACTION ON DELETE NO ACTION,  
    CONSTRAINT b_item_manufacturer_id_fk FOREIGN KEY (b_item_manufacturer_id) 
        REFERENCES b_item_manufacturer (b_item_manufacturer_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT b_item_drug_uom_big_id_fk FOREIGN KEY (b_item_drug_uom_big_id) 
        REFERENCES b_item_drug_uom (b_item_drug_uom_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT b_item_drug_uom_small_id_fk FOREIGN KEY (b_item_drug_uom_small_id) 
        REFERENCES b_item_drug_uom (b_item_drug_uom_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT t_hstock_requisition_item_id_fk FOREIGN KEY (t_hstock_requisition_item_id) 
        REFERENCES t_hstock_requisition_item (t_hstock_requisition_item_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT user_record_id_fk FOREIGN KEY (user_record_id) 
        REFERENCES b_employee (b_employee_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT user_update_id_fk FOREIGN KEY (user_update_id) 
        REFERENCES b_employee (b_employee_id) ON UPDATE NO ACTION ON DELETE NO ACTION
);

BEGIN;
INSERT INTO f_hstock_user_authen SELECT '0215','รับเข้าจากการสั่งซื้อ'
WHERE NOT EXISTS (SELECT 1 FROM f_hstock_user_authen WHERE f_hstock_user_authen_id = '0215'); 
COMMIT;

ALTER TABLE t_hstock_order_item ADD COLUMN IF NOT EXISTS receive_qty float8 NOT NULL default 0;
ALTER TABLE t_hstock_order_item ADD COLUMN IF NOT EXISTS receive_free_qty float8 NOT NULL default 0;
ALTER TABLE t_hstock_card ADD COLUMN IF NOT EXISTS t_hstock_order_item_id CHARACTER VARYING(50) DEFAULT NULL;
ALTER TABLE t_hstock_card ADD CONSTRAINT t_hstock_order_item_id_fk FOREIGN KEY(t_hstock_order_item_id)
    REFERENCES t_hstock_order_item (t_hstock_order_item_id) ON UPDATE NO ACTION ON DELETE NO ACTION;

drop function if exists update_hstock_order_return() cascade;

INSERT INTO s_stock_version (version_app, version_db, description)VALUES ('2.0.2', '2.0.2', 'Update Stock Module');
INSERT INTO s_script_update_log values ('Stock_Module','update_stock_003.sql',(select current_date) || ','|| (select current_time),'Update Stock Module');

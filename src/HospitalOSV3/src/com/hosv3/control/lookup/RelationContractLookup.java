/*
 * RelationContract.java
 *
 * Created on 18 ���Ҥ� 2548, 10:59 �.
 */
package com.hosv3.control.lookup;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.LookupControlInf;
import com.hosv3.control.LookupControl;

/**
 *
 * @not deprecated because use henbe package
 *
 * @author sumo1
 */
public class RelationContractLookup implements LookupControlInf {

    private LookupControl theLookup;

    /**
     * Creates a new instance of PrefixLookup
     */
    public RelationContractLookup(LookupControl lookup) {
        theLookup = lookup;
    }

    @Override
    @SuppressWarnings("UseOfObsoleteCollectionType")
    public java.util.Vector listData(String str) {
        if (str == null || str.equals("") || str.equals("%")) {
            return theLookup.listRelation();
        } else {
            return theLookup.listRelation(str);
        }
    }

    @Override
    public CommonInf readHosData(String str) {
        return theLookup.readRelationById(str);
    }
}

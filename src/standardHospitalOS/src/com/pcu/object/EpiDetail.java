/*
 * EpiDetail.java
 *
 * Created on 24 �Զع�¹ 2548, 10:35 �.
 */
package com.pcu.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Administrator
 */
@SuppressWarnings("ClassWithoutLogger")
public class EpiDetail extends Persistent {

    private static final long serialVersionUID = 1L;
    private static String init = "";
    public String epi_id = init;
    public String epi_set_id = init;
    public String epi_start = init;
    public String epi_exp = init;
    public String patient_id = init;
    public String visit_id = init;
    public String staff_record = init;
    public String record_date_time = init;
    public String active = init;
    public String lot = init;
    public String family_id = init;
    public String epi_standard = init;
    public String b_item_manufacturer_id;
    public String act_site_code;
    public String immunization_route_code;

    /**
     * Creates a new instance of EpiDetail
     */
    public EpiDetail() {
    }
}

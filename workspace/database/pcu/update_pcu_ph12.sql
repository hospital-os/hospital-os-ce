-- issues#619
-- เก็บข้อมูลบริษัทผู้ผลิต (b_item_manufacture_id) 
ALTER TABLE t_health_epi_detail ADD IF NOT EXISTS b_item_manufacturer_id VARCHAR(255) DEFAULT NULL;

-- เก็บข้อมูลตำแหน่งที่ฉีด (code) 
ALTER TABLE t_health_epi_detail ADD IF NOT EXISTS act_site_code VARCHAR(50) DEFAULT '0';

-- เก็บข้อมูลวิธีการฉีด (code) 
ALTER TABLE t_health_epi_detail ADD IF NOT EXISTS immunization_route_code VARCHAR(50) DEFAULT '0';

-- เปลี่ยนชื่อ sequence ให้ข้อมูลการรับวัคซีน ใช้ sequence ร่วมกับวัคซีน Covid-19 
ALTER SEQUENCE IF EXISTS vaccine_covid19_req RENAME TO vaccine_seq;
-- เพิ่มเก็บ sequence ในตาราง t_health_epi_detail 
ALTER TABLE t_health_epi_detail ADD IF NOT EXISTS ref_code INTEGER NOT NULL DEFAULT nextval('vaccine_seq'::regclass);  

-- เก็บข้อมูลตำแหน่งที่ฉีด (code) 
ALTER TABLE t_health_vaccine_covid19 ADD IF NOT EXISTS act_site_code VARCHAR(50) DEFAULT '0';
-- เก็บข้อมูลวิธีการฉีด (code) 
ALTER TABLE t_health_vaccine_covid19 ADD IF NOT EXISTS immunization_route_code VARCHAR(50) DEFAULT '0';

-- update db version
INSERT INTO s_health_version VALUES ('9710000000012','12','PCU, Community Edition','1.36.0','1.6.0', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

insert into s_script_update_log values ('pcu','update_pcu_ph11.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'update PCU ph11 -> ph12');
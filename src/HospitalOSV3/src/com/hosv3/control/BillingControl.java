/*
 * BillingControl.java
 *
 * Created on 17 ���Ҥ� 2546, 17:04 �.
 */
package com.hosv3.control;

import com.hospital_os.object.*;
import com.hospital_os.object.specialQuery.SpecialQueryBillingReceipt;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hospital_os.utility.ComplexDataSource;
import com.hosv3.gui.dialog.ConfirmCancelDialog;
import com.hosv3.gui.dialog.DialogPasswd;
import com.hosv3.gui.panel.transaction.inf.PanelOrderInf;
import com.hosv3.object.HosObject;
import com.hosv3.object.LookupObject;
import com.hosv3.object.ReceiptBookSeq;
import com.hosv3.object.ReceiptSequance;
import com.hosv3.subject.HosSubject;
import com.hosv3.utility.Constant;
import com.hosv3.utility.DateUtil;
import com.hosv3.utility.ResourceBundle;
import java.net.InetAddress;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import sd.util.datetime.DateTimeUtil;

//import javax.swing.*;
/**
 *
 * @author tong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class BillingControl {

    private static final Logger LOG = Logger.getLogger(BillingControl.class.getName());
    private ConnectionInf theConnectionInf;
    private HosDB theHosDB;
    private HosObject theHO;
    private static LookupObject theLO;
    private HosSubject theHS;
    private UpdateStatus theUS;
    private static LookupControl theLookupControl;
    private Vector vOrderBilling;
    private VisitControl theVisitControl;
    private HosControl hosControl;

    /**
     * Creates a new instance of BillingControl
     */
    /**
     * Creates a new instance of BillingControl
     */
    public BillingControl(ConnectionInf c, HosObject ho, HosDB hdb, HosSubject hs, LookupObject lo) {
        theConnectionInf = c;
        theHosDB = hdb;
        theHO = ho;
        theLO = lo;
        theHS = hs;
    }

    public void setDepControl(LookupControl lc, OrderControl oc, VisitControl vc) {
//        theOrderControl = oc;
        theLookupControl = lc;
        theVisitControl = vc;
    }

    public void setSystemControl(SystemControl systemControl) {
//        theSystemControl = systemControl;
    }

    public void setUpdateStatus(UpdateStatus us) {
        theUS = us;
    }

    public void setHosControl(HosControl hosControl) {
        this.hosControl = hosControl;
    }

    public static String calBil(String da) {
        String data = "0";
        try {
            double d = Double.parseDouble(da);
            d = theLookupControl.readOption().use_money_ceil.equals("1") ? Math.ceil(d) : d;
            data = String.valueOf(d);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
        }
        return data;
    }

    /**
     * hosv4
     *
     * @param visit_id
     * @param show_cancel
     * @return
     */
    public Vector listBillingByVisitId(String visit_id, boolean show_cancel) {
        if (visit_id == null || visit_id.isEmpty()) {
            return null;
        }
        Vector v = null;

        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (show_cancel) {
                v = theHosDB.theBillingDB.selectAllByVisitId(visit_id);
            } else {
                v = theHosDB.theBillingDB.selectByVisitId(visit_id);
            }
            theHO.vBilling = v;
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return v;
    }

    public Vector listBillingByVisitId(String visit_id) {
        return listBillingByVisitId(visit_id, false);
    }

    /**
     * function �ӹǳ ���ʹ��ҧ���Тͧ������
     */
    public String getBillRemaining(Vector vbilling) {
        String data = "";
        if (vbilling != null) {
            double total = 0.0d;
            for (int i = 0; i < vbilling.size(); i++) {
                Billing billing = (Billing) vbilling.get(i);
                if (billing != null) {
                    double remain = Double.parseDouble(billing.remain);
                    total += remain;
                }
            }
            if (total > 0) {
                data = MessageFormat.format(ResourceBundle.getBundleText("com.hosv3.control.BillingControl.REMAIN_PRICE"), String.valueOf(total));
            }
        }
        return data;
    }

    public String calBillingPatientRemainByPatientID(String patient_id) {
        Vector vc = listBillingByPatientId(patient_id);
        return getBillRemaining(vc);
    }

    public Vector listBillingByPatientId(String patient_id) {
        theConnectionInf.open();
        Vector v = null;
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            v = theHosDB.theBillingDB.selectByPatientId(patient_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return v;
    }

    public Vector listSubGroupByBillingId(String billing_id) {
        if (billing_id == null || billing_id.isEmpty()) {
            return null;
        }
        Vector v = null;

        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            v = theHosDB.theBillingSubgroupDB.selectByBillingId(billing_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return v;
    }

//    public String billFromPatient(Billing b) {
//        String result = "BillFromPatient InComplete";
//        theConnectionInf.open();
//        try {
//            theConnectionInf.getConnection().setAutoCommit(false);
//            String date_time = theLookupControl.intReadDateTime();
//            b.financial_date = date_time;
//            b.active = Active.isEnable();
//
//            if (b.getObjectId() == null) {
//                b.generateOID(0);
//                theHosDB.theBillingDB.insert(b);
//            } else {
//                theHosDB.theBillingDB.update(b);
//            }
//            result = "BillFromPatient Complete";
//            theConnectionInf.getConnection().commit();
//        } catch (Exception ex) {
//            LOG.log(java.util.logging.Level.SEVERE, null, ex);
//            try {
//                theConnectionInf.getConnection().rollback();
//            } catch (SQLException ex1) {
//                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
//            }
//        } finally {
//            theConnectionInf.close();
//        }
//        return result;
//    }
//    public String paybackDept(Billing b)///gui will set Paid
//    {
//        String result = "PaybackDept InComplete";
//        theConnectionInf.open();
//        try {
//            theConnectionInf.getConnection().setAutoCommit(false);
//            String date_time = theLookupControl.intReadDateTime();
//            b.financial_date = date_time;
//            b.payback = "1";
//            b.remain = "0";
//            b.generateOID(0);
//            theHosDB.theBillingDB.insert(b);
//            result = "PaybackDept Complete";
//            theConnectionInf.getConnection().commit();
//        } catch (Exception ex) {
//            LOG.log(java.util.logging.Level.SEVERE, null, ex);
//            try {
//                theConnectionInf.getConnection().rollback();
//            } catch (SQLException ex1) {
//                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
//            }
//        } finally {
//            theConnectionInf.close();
//        }
//        return result;
//    }
    /**
     * vorder �������ҹ
     */
    public void billingInvoice(Vector[] avector, Vector vorder) {
        billingInvoice(avector, vorder, false);
    }

    /**
     * vorder �������ҹ
     */
    public void billingInvoice(Vector[] avector, Vector vorder, boolean all) {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            isComplete = intBillingInvoice(avector, vorder, all);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.BillingControl.CALCULATE_INVOICE")
                    + ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theBillingSubject.notifyBillingInvoice(ResourceBundle.getBundleText("com.hosv3.control.BillingControl.CALCULATE_INVOICE")
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        }
    }

    public boolean saveBillingInvoice(Vector[] vBois) {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            intSaveBillingInvoice(vBois);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.BillingControl.SAVE_INVOICE")
                    + ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theBillingSubject.notifyBillingInvoice(ResourceBundle.getBundleText("com.hosv3.control.BillingControl.SAVE_INVOICE")
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        }
        return isComplete;
    }

    public void intSaveBillingInvoice(Vector[] vBois) throws Exception {
        Vector vbillingin = theHosDB.theBillingInvoiceDB.selectByVisitId(theHO.theVisit.getObjectId());
        int invoice_no = 1;
        if (!vbillingin.isEmpty()) {
            BillingInvoice bi = (BillingInvoice) vbillingin.get(0);
            invoice_no = Integer.parseInt(bi.billing_invoice_no) + 1;
        }
        //�ӡ�úѹ�֡�����š�äԴ�Թ������¡��
        // sort boi by payment and category
        for (Vector vc : vBois) {
            //�Ң�������� billing order item
            if (vc.isEmpty()) {
                continue;
            }
            //�Ң�������� billing invoice
            BillingInvoice bi = BillingInvoice.initFromBoiV(vc);
            bi.billing_invoice_no = String.valueOf(invoice_no);
            bi.invoice_no = String.valueOf(invoice_no);
            bi.invoice_date = theHO.date_time;
            bi.staff_billing_invoice = theHO.theEmployee.getObjectId();
            theHosDB.theBillingInvoiceDB.insert(bi);
            for (int i = 0; i < vc.size(); i++) {
                BillingOrderItem boi = (BillingOrderItem) vc.get(i);
                BillingInvoiceItem bii = BillingInvoiceItem.initFromBoi(boi);
                bii.billing_invoice_id = bi.getObjectId();
                theHosDB.theBillingInvoiceItemDB.insert(bii);
                boi.theOrderItem.order_complete = "1";
                boi.theOrderItem.charge_complete = "1";
                theHosDB.theOrderItemDB.update(boi.theOrderItem);

                if (boi.theOrderItem.isParentPackage()) {
                    Vector<OrderItem> listChild = theHosDB.theOrderItemDB.selectByOrderIdJoinOrderPackage(boi.theOrderItem.getObjectId());
                    for (OrderItem orderItem : listChild) {
                        orderItem.order_complete = "1";
                        orderItem.charge_complete = "1";
//                        orderItem. = theHO.theEmployee.getObjectId();
                        theHosDB.theOrderItemDB.update(orderItem);
                    }

                }
            }
            //�Ң�������� item subgroup
            Vector vbis = BillingInvoiceSubgroup.initFromBoiV(vc, theLO.theBillingGroupItem);
            for (int i = 0; i < vbis.size(); i++) {
                BillingInvoiceSubgroup bis = (BillingInvoiceSubgroup) vbis.get(i);
                bis.billing_invoice_id = bi.getObjectId();
                theHosDB.theBillingInvoiceSubgroupDB.insert(bis);
            }
        }
    }

    /**
     * vorder �������ҹ
     */
    public boolean intBillingInvoice(Vector[] avector, Vector vorder, boolean all) throws Exception {
        String visit_id = theHO.theVisit.getObjectId();
        Vector avBilling = calBilling(avector);//�ӹǹ�Դ���¹�//vector of vBillingInvoice
        Vector avbillingItem = calBillingItem(avector);//vector of vector
        Vector avbillingSubgroup = calBillingSubgroup(avector);//vector of vector

        Vector vbillingin = theHosDB.theBillingInvoiceDB.selectByVisitId(visit_id);
        int invoice_no = 1;
        if (!vbillingin.isEmpty()) {
            BillingInvoice bi = (BillingInvoice) vbillingin.get(0);
            invoice_no = Integer.parseInt(bi.billing_invoice_no) + 1;
        }
        //�ӡ�úѹ�֡�����š�äԴ�Թ������¡��
        // �ӹǹ billing invoice �Դ����
        for (int i = 0; i < avector.length; i++) {
            BillingInvoice bi = (BillingInvoice) avBilling.get(i);
            if (bi != null) {
                //�ӡ�äӹǳ��������ҤԴ�Թ仨ӹǹ����˹����
                bi.billing_invoice_no = String.valueOf(invoice_no);
                bi.invoice_no = String.valueOf(invoice_no);
                bi.staff_billing_invoice = theHO.theEmployee.getObjectId();
                theHosDB.theBillingInvoiceDB.insert(bi);
            }
            //��äԴ�Թ���Ф��駡������¡�� item ���ᵡ��ҧ�ѹ
            Vector vbillingItem = (Vector) avbillingItem.get(i);
            for (int j = 0; j < vbillingItem.size(); j++) {
                BillingInvoiceItem bii = (BillingInvoiceItem) vbillingItem.get(j);
                bii.billing_invoice_id = bi != null ? bi.getObjectId() : "";
                if (bii.draw == null) {
                    bii.draw = "0";
                }
                theHosDB.theBillingInvoiceItemDB.insert(bii);
                // Constant.println("��ͧ�ըӹǹ��ҡѺ��¡�� order ���Դ�Թ vorder.size()" + vorder.size());
                theHosDB.theOrderItemDB.updateCObyId(Active.isEnable(), Active.isEnable(), bii.order_item_id);
            }
            //��äԴ�Թ���Ф��駡������¡�á�������·��ᵡ��ҧ�ѹ
            Vector vbillingSubgroup = (Vector) avbillingSubgroup.get(i);
            for (int k = 0; k < vbillingSubgroup.size(); k++) {
                BillingInvoiceSubgroup bis = (BillingInvoiceSubgroup) vbillingSubgroup.get(k);
                bis.billing_invoice_id = bi != null ? bi.getObjectId() : "";
                theHosDB.theBillingInvoiceSubgroupDB.insert(bis);
            }
        }
        return true;
    }

    /**
     * return Vector �ͧ BillingInvoice
     */
    private Vector calBilling(Vector[] avector) {
        Vector avBilling = new Vector();
        for (int i = 0; i < avector.length; i++) {
            Vector vboi = avector[i];
            avBilling.add(BillingInvoice.initFromBoiV(vboi));
        }
        return avBilling;
    }

    /**
     * return Vector �ͧ Vector �ͧ BillingInvoiceItem �� Vector �á����
     * Vector �ͧ �Է�ԡ���ѡ��
     */
    private Vector calBillingItem(Vector[] avector) {
        if (avector == null) {
            return null;
        }
        Vector avbillingItem = new Vector();
        for (int i = 0; i < avector.length; i++) {
            Vector vorder = avector[i];
            int checkvalue = 0;
            Vector billingItem = new Vector();
            for (int j = 0; j < vorder.size(); j++) {
                BillingOrderItem boi = (BillingOrderItem) vorder.get(j);
                billingItem.add(BillingInvoiceItem.initFromBoi(boi));
                checkvalue += 1;
            }
            // if(checkvalue > 0)
            avbillingItem.add(billingItem);
        }
        return avbillingItem;
    }

    private Vector calBillingSubgroup(Vector[] avector) {
        if (avector == null) {
            return null;
        }
        Vector avbillingSubgroup = new Vector();
        Vector vBillingGroupItem = theLookupControl.listBillingGroupItem();

        for (int i = 0; i < avector.length; i++) {
            Vector vorder = avector[i];
            int checkvaluea = 0;
            Vector billingSubgroup = new Vector();
            int orders = 0;
            //�ӡ��ǹ�ٻ���͵�Ǩ�ͺ�����¡�� order �������㹡���� billing �˹
            for (int k = 0; k < vBillingGroupItem.size(); k++) {
                BillingGroupItem cgi = (BillingGroupItem) vBillingGroupItem.get(k);
                double patientprice = 0.00;
                double payerprice = 0.00;
                double patientprice_draw = 0.00;
                double payerprice_draw = 0.00;
                int checkvalue = 0;
                int checkvalue_draw = 0;
                //��������� ��¡�ù���ա���� billing �ç�ѹ��������ҵç�ѹ�����ӡ�úǡ�����ҨФú������� billing
                BillingOrderItem boi = null;
                for (int j = 0; j < vorder.size(); j++) {
                    boi = (BillingOrderItem) vorder.get(j);
                    if (boi.item_group_code_billing.equals(cgi.getObjectId())) {
                        if (boi.draw.equals("0")) {
                            patientprice += Double.parseDouble(boi.patientshare);
                            payerprice += Double.parseDouble(boi.payershare);
                            checkvalue += 1;
                            orders += 1;
                        } else {
                            patientprice_draw += Double.parseDouble(boi.patientshare);
                            payerprice_draw += Double.parseDouble(boi.payershare);
                            checkvalue_draw += 1;
                        }
                    }
                }
                //����ͤú���ǡ�ӡ������ҡѺ�������¡��
                //��Ǩ�ͺ��͹��� ���Ź�ٻ����բ�������������������ͷ�����੾�Т����ŷ���դ����ҹ��
                if (checkvalue > 0) {
                    BillingInvoiceSubgroup bis = new BillingInvoiceSubgroup();
                    bis.billing_group_item_id = cgi.getObjectId();
                    bis.category_group_item_id = "";
                    bis.billing_invoice_id = "";
                    bis.patient_id = boi != null ? boi.patient_id : "";
                    bis.visit_id = boi != null ? boi.visit_id : "";
                    bis.payer_share = Constant.doubleToDBString(payerprice);
                    bis.patient_share = Constant.doubleToDBString(patientprice);
                    bis.payer_share_ceil = Constant.doubleToDBString(payerprice);
                    bis.patient_share_ceil = Constant.doubleToDBString(patientprice);
                    bis.total = Constant.doubleToDBString(payerprice + patientprice);
                    bis.payment_id = boi != null ? boi.payment_id : "";
                    bis.draw = "0";
                    bis.active = Active.isEnable();
                    billingSubgroup.add(bis);
                    checkvaluea += 1;
                }
                if (checkvalue_draw > 0) {
                    BillingInvoiceSubgroup bis = new BillingInvoiceSubgroup();
                    bis.billing_group_item_id = cgi.getObjectId();
                    bis.category_group_item_id = "";
                    bis.billing_invoice_id = "";
                    bis.patient_id = boi != null ? boi.patient_id : "";
                    bis.visit_id = boi != null ? boi.visit_id : "";
                    bis.payer_share = Constant.doubleToDBString(payerprice_draw);
                    bis.patient_share = Constant.doubleToDBString(patientprice_draw);
                    bis.payer_share_ceil = Constant.doubleToDBString(payerprice_draw);
                    bis.patient_share_ceil = Constant.doubleToDBString(patientprice_draw);
                    bis.total = Constant.doubleToDBString(payerprice_draw + patientprice_draw);
                    bis.payment_id = boi != null ? boi.payment_id : "";
                    bis.draw = "1";
                    bis.active = Active.isEnable();
                    billingSubgroup.add(bis);
                }
            }
            avbillingSubgroup.add(billingSubgroup);
        }

        return avbillingSubgroup;
    }

    /**
     * ��㹡���ҡ��������騨ҡ��¡�õ�Ǩ�ѡ������� ��������ú�ҧ public Vector
     * readBillingGroupItem(Vector order) { //boi =
     * (BillingOrderItem)order.get(0); //orderitem = new OrderItem();
     * //orderitem =
     * (OrderItem)theVisitControl.readOrderItemByKeyID(boi.order_item_id);
     * for(int i = order.size() -1 ; i >= 0 ; i--) { BillingOrderItem boi =
     * (BillingOrderItem)order.get(i); } return null;
     *
     * } *
     * /**
     * hosv4 return �� Vector �ͧ BillingInvoice
     */
    public Vector listBillingInvoiceByVisitID(String visit_id) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theBillingInvoiceDB.selectByVisitId(visit_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * return �� Vector �ͧ BillingInvoicesubgroup
     */
    public Vector readBillingInvoiceSubgroupByVisitIDBillingIDNULL(String visit_id) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theBillingInvoiceSubgroupDB.selectByVisitIdBillingIDNULL(visit_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * return �� Vector �ͧ BillingInvoice
     */
    public Vector readBillingInvoiceByVisitIDBillingIDNULL(String visit_id) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theBillingInvoiceDB.selectByVisitIdBillingIDNULL(visit_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * return �� Vector �ͧ BillingInvoiceSubgroup
     */
    public Vector readBillingInvoiceSubgroupByVisitID(String visit_id) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theBillingInvoiceSubgroupDB.selectByVisitId(visit_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     * return �� Vector �ͧ BillingInvoiceSubgroup
     */
    public Vector readBillingInvoiceSubgroupByVisitIDBillingID(String visit_id, String billing_id) {
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theBillingInvoiceSubgroupDB.selectByVisitIdBillingID(visit_id, billing_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    /**
     *
     * @not deprecated henbe unused henbe_error wait for modify vc is Vector of
     * BillingInvoice //vc =
     * readBillingInvoiceByVisitIDBillingIDNULL(visit.getObjectId());
     * //�ӹǳ�Ҽ�������������ʹ�������Է�ԡ���ѡ������ ����¡�� Billing
     * Invoice Subgroup ��� visit id
     */
    public void calculateAllBillingInvoice(Vector biV_in, String inv_no) {
        calculateAllBillingInvoice(biV_in, inv_no, -2);
    }

    /**
     *
     * @param biV_in
     * @param inv_no
     * @param row
     * @not deprecated henbe unused
     * �ѧ��ҹ�����ͧ�͡���˵ش�����ҷ����֧��������ҹ���� hosv4
     */
    public void calculateAllBillingInvoice(Vector biV_in, String inv_no, int row) {
        boolean ret = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ret = intCalculateAllBillingInvoice(biV_in, inv_no, row);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT, ResourceBundle.getBundleText("com.hosv3.control.BillingControl.CALCULATE.BILLING.INVOICE"));
        } finally {
            theConnectionInf.close();
        }
        if (ret) {
            theHS.theBillingSubject.notifyCalculateAllBillingInvoice(
                    ResourceBundle.getBundleText("com.hosv3.control.BillingControl.CALCULATE.BILLING.INVOICE") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, ResourceBundle.getBundleText("com.hosv3.control.BillingControl.CALCULATE.BILLING.INVOICE"));
        }
    }

    /**
     * hosv4
     */
    public boolean intCalculateAllBillingInvoice(Vector biV_in, String inv_no, int row) throws Exception {
        if (biV_in == null || biV_in.isEmpty()) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.BillingControl.VALIDATE.NO.INVOICE"), UpdateStatus.WARNING);
            return false;
        }
        if (row == -1) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.BillingControl.VALIDATE.NOT.CHOOSE.INVOICE"), UpdateStatus.WARNING);
            return false;
        }
        if (row > -1) {
            BillingInvoice bi = (BillingInvoice) biV_in.get(row);
            inv_no = bi.billing_invoice_no;
        }
        if (inv_no.isEmpty()) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.BillingControl.VALIDATE.INVOICE.NUMBER"), UpdateStatus.WARNING);
            return false;
        }
        boolean found_bi = false;
        for (int i = 0, size = biV_in.size(); i < size; i++) {
            BillingInvoice bi_in = (BillingInvoice) biV_in.get(i);
            if (!bi_in.billing_complete.equals("1")) {
                found_bi = true;
            }
        }
        if (!found_bi) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.BillingControl.VALIDATE.NO.INVOICE.NOTPAY"), UpdateStatus.WARNING);
            return false;
        }

        String date_time = theLookupControl.intReadDateTime();
        double patientprice = 0;
        double payerprice = 0;
        Billing billing = HosObject.initBilling();
        theHosDB.theBillingDB.insert(billing);//for get new ObjectId();
        //�� �ӹǹ�Թ �ͧ Billing Invoice ������ػ�ӹǹ�Թŧ Billing
        boolean isCeil = hosControl.theLookupControl.readOption().use_money_ceil.equals("1");
        for (int i = 0, size = biV_in.size(); i < size; i++) {
            BillingInvoice bi_in = (BillingInvoice) biV_in.get(i);
            //���͡��੾����¡�÷���ѧ����ա�äԴ�Թ �ѧ������Ţ bill
            //�ӡ�úѹ�֡���¡�� billingInvoice
            if (!bi_in.billing_complete.equals("1")) {
                patientprice += Double.parseDouble(isCeil
                        ? bi_in.patient_share_ceil
                        : bi_in.patient_share);
                payerprice += Double.parseDouble(isCeil
                        ? bi_in.payer_share_ceil
                        : bi_in.payer_share);
                theHosDB.theBillingInvoiceDB.updateBC(bi_in, billing.getObjectId(), Active.isEnable());
                //�ӡ�úѹ�֡���¡�� billingInvoiceSubgroup
                //update BillingID by billingInvoiceID
                theHosDB.theBillingInvoiceSubgroupDB.updateBbyBi(
                        billing.getObjectId(), bi_in.getObjectId());
            }
        }
        billing.receipt_date = date_time;
        billing.staff_financial = theHO.theEmployee.getObjectId();
        billing.patient_id = theHO.thePatient.getObjectId();
        billing.visit_id = theHO.theVisit.getObjectId();
        billing.patient_share = Constant.doubleToDBString(patientprice);
        billing.payer_share = Constant.doubleToDBString(payerprice);
        billing.remain = billing.patient_share;
        billing.total = Constant.doubleToDBString(patientprice + payerprice);
        theHosDB.theBillingDB.update(billing);
        theHO.vBillingInvoice = biV_in;
        if (theHO.vBilling == null) {
            theHO.vBilling = new Vector();
        }
        theHO.vBilling.add(billing);
        return true;
    }

    public String intGenReceiptNumber() throws Exception {
        String receipt_no;
        // generate reciept number
        if (theLookupControl.readOption().receipt_sequance.equals(Active.isEnable())) {
            InetAddress thisIp = InetAddress.getLocalHost();
            ReceiptSequance sd = theHosDB.theReceiptSequanceDB.selectByIP(thisIp.getHostAddress());
            if (sd == null) {
                theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.BillingControl.VALIDATE.NOT.MATCH.IP"), UpdateStatus.WARNING);
                throw new Exception("not match ip");
            }
            receipt_no = theHosDB.theReceiptSequanceDB.updateSequence(thisIp.getHostAddress());
            if (theHosDB.theReceiptDB.selectByReceiptNo(receipt_no) != null) {
                JOptionPane.showMessageDialog(null,
                        ResourceBundle.getBundleText("com.hosv3.control.BillingControl.VALIDATE.DUPLICATE.INVOICE.NUMBER"),
                        ResourceBundle.getBundleGlobal("TEXT.WARNING"),
                        JOptionPane.WARNING_MESSAGE);
                throw new Exception("dup");
            }
        } else {
            receipt_no = theHosDB.theSequenceDataDB.updateSequence("rn", true);
        }
        return receipt_no;
    }

    public boolean patientPaidMoney(Billing billing,
            Vector vBillingInvoiceSubgroup,
            String strPay,
            String paymentTypeId,
            String bankInfoId,
            String cardTypeId,
            String accountName,
            String accountNo,
            Date transactionDate,
            UpdateStatus us,
            String print_name_type,
            String print_name) {
        if (strPay.isEmpty()) {
            us.setStatus(ResourceBundle.getBundleText("com.hosv3.control.BillingControl.VALIDATE.AMOUNT"), UpdateStatus.WARNING);
            return false;
        }
        double pay;
        try {
            pay = Double.parseDouble(strPay);
        } catch (Exception e) {
            us.setStatus(ResourceBundle.getBundleText("com.hosv3.control.BillingControl.VALIDATE.AMOUNT.TYPE"), UpdateStatus.WARNING);
            return false;
        }
        if (pay <= 0) {   //�Ѻ�����Թ�ҡ������
            us.setStatus(ResourceBundle.getBundleText("com.hosv3.control.BillingControl.VALIDATE.AMOUNT.MORE.ZERO"), UpdateStatus.WARNING);
            return false;
        }
        if (billing == null) {
            us.setStatus(ResourceBundle.getBundleText("com.hosv3.control.BillingControl.VALIDATE.NO.PAYMENT.INFORMATION"), UpdateStatus.WARNING);
            return false;
        }
        double mustPaid;
        try {
            // �ʹ����ѡ��ǹŴ����������
            mustPaid = Double.parseDouble(billing.patient_share) - Double.parseDouble(billing.total_discount);
            if (mustPaid == 0) {
                us.setStatus(ResourceBundle.getBundleText("com.hosv3.control.BillingControl.VALIDATE.NO.ITEM.TO.PAID"), UpdateStatus.WARNING);
                return false;
            }
        } catch (Exception e) {
            us.setStatus(ResourceBundle.getBundleText("com.hosv3.control.BillingControl.INVALID.PAID.TYPE"), UpdateStatus.WARNING);
            return false;
        }
        double paid;
        try {
            paid = Double.parseDouble(billing.paid);
        } catch (Exception e) {
            us.setStatus(ResourceBundle.getBundleText("com.hosv3.control.BillingControl.INVALID.PAY.TYPE"), UpdateStatus.WARNING);
            return false;
        }
        //��Ǩ�ͺ��Ҽ����ª��е���Ҥ�������� �ҡ���кѹ�֡�ӹǹ�Թ������ �ҡ��кѹ�֡�Ҥ��ط��
        if (pay + paid > mustPaid) {
            pay = mustPaid - paid;
        }

        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            intPatientPaidMoney(billing, vBillingInvoiceSubgroup, pay,
                    paymentTypeId, print_name_type, print_name,
                    accountName, accountNo, bankInfoId, cardTypeId, transactionDate);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            if (!(ex.getMessage() != null
                    && (ex.getMessage().equals("cn")
                    || ex.getMessage().equals("dup")
                    || ex.getMessage().equals("not match ip")))) {
                hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT, ResourceBundle.getBundleGlobal("TEXT.PAYMENT"));
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theBillingSubject.notifyPatientPaidMoney(ResourceBundle.getBundleGlobal("TEXT.PAYMENT") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, ResourceBundle.getBundleGlobal("TEXT.PAYMENT"));
        }
        return isComplete;
    }

    public void intPatientPaidMoney(Billing billing,
            Vector vBillingInvoiceSubgroup,
            double pay,
            String paymentTypeId,
            String print_name_type,
            String print_name,
            String accountName,
            String accountNo,
            String bankInfoId,
            String cardTypeId,
            Date transactionDate) throws Exception {
        Receipt theReceipt = theHO.initReceipt(billing, pay);
        // generate reciept number
        theReceipt.receipt_no = intGenReceiptNumber();
        theReceipt.receipt_model = paymentTypeId;
        theReceipt.print_name_type = print_name_type;
        theReceipt.print_name = print_name;
        Vector<BillingInvoice> billingInvoices = theHosDB.theBillingInvoiceDB.selectByBillingID(billing.getObjectId());
        theReceipt.t_billing_invoice_ids = new String[billingInvoices.size()];
        for (int i = 0; i < theReceipt.t_billing_invoice_ids.length; i++) {
            theReceipt.t_billing_invoice_ids[i] = billingInvoices.get(i).getObjectId();
        }
        // ��Ǩ�ͺ��������ê����Թ
        PaymentType paymentType = theHosDB.thePaymentTypeDB.select(paymentTypeId);
        if ("1".equals(paymentType.require_account_name)) {
            if (accountName == null || accountName.isEmpty()) {
                theUS.setStatus("��س��к�" + paymentType.lbl_account_name, UpdateStatus.WARNING);
                throw new Exception("cn");
            }
            theReceipt.account_name = accountName;
        }
        if ("1".equals(paymentType.require_account_no)) {
            if (accountNo == null || accountNo.isEmpty()) {
                theUS.setStatus("��س��к�" + paymentType.lbl_account_no, UpdateStatus.WARNING);
                throw new Exception("cn");
            } else if (paymentType.limit_account_number > 0 && accountNo.length() < paymentType.limit_account_number) {
                theUS.setStatus("��س��к�" + paymentType.lbl_account_no + "���ú " + paymentType.limit_account_number + " ��ѡ", UpdateStatus.WARNING);
                throw new Exception("cn");
            }
            theReceipt.account_number = accountNo;
        }
        if ("1".equals(paymentType.require_bank_info)) {
            if (bankInfoId == null || bankInfoId.isEmpty()) {
                theUS.setStatus("��س��к�" + paymentType.lbl_bank_info, UpdateStatus.WARNING);
                throw new Exception("cn");
            }
            theReceipt.b_bank_info_id = bankInfoId;
        }
        if ("1".equals(paymentType.require_card_type)) {
            if (cardTypeId == null || cardTypeId.isEmpty()) {
                theUS.setStatus("��س��к�" + paymentType.lbl_card_type, UpdateStatus.WARNING);
                throw new Exception("cn");
            }
            theReceipt.b_credit_card_type_id = cardTypeId;
        }
        if ("1".equals(paymentType.require_payment_date)) {
            if (transactionDate == null) {
                theUS.setStatus("��س��к�" + paymentType.lbl_payment_date, UpdateStatus.WARNING);
                throw new Exception("cn");
            }
            theReceipt.transaction_date = transactionDate;
        }

        theHosDB.theReceiptDB.insert(theReceipt);
        //1 insert ������ŧ���ҧ receipt
        //3 update �����ŷ������ͨҡ���ź����ŧ���ҧ billing
        //����¡�ä�ҧ�����ҤԴ��Ҫ��Фú�����ѧ ��Ǩ�ͺ��� ��¡�÷��ź����դ�ҹ��¡��� 0 �������
//            //���¤ú㹤������������������¤���
//            boolean full_filling = false;
        // set ��������º��������
        double remain = Double.parseDouble(billing.remain);
        if (remain == 0) {
            billing.paid = Constant.doubleToDBString(Double.parseDouble(billing.patient_share) - Double.parseDouble(billing.total_discount));
        } else {//�����ѧ���ú
            billing.paid = Constant.doubleToDBString(Double.parseDouble(billing.paid) + pay);
        }
        // update data
        theHosDB.theBillingDB.update(billing);
        for (int i = 0; i < vBillingInvoiceSubgroup.size(); i++) {
            BillingInvoiceSubgroup bis = (BillingInvoiceSubgroup) vBillingInvoiceSubgroup.get(i);
            // update final_patient_share
            bis.final_patient_share = Constant.doubleToDBString(Double.parseDouble(bis.patient_share_ceil) - Double.parseDouble(bis.discount));
            theHosDB.theBillingInvoiceSubgroupDB.update(bis);
        }
        double moneyForPay = pay;
        // ��ͧǹ�ٻ��Ǩ�ͺ��ê����Թ���С���� (billing invoice subgroup) // (comment ���)ǹ�ٻ���͵�Ǩ�ͺ�������¡�����˹������� ��������ú�ҧ ��������� Line �����
        for (int i = 0; i < vBillingInvoiceSubgroup.size(); i++) {
            BillingInvoiceSubgroup bis = (BillingInvoiceSubgroup) vBillingInvoiceSubgroup.get(i);
            double ptMustPaid = Double.parseDouble(bis.final_patient_share);
            //����¡���Ѻ�����Թ���駡�͹��Ҫ��Ф����������Ǻ�ҧ
            Vector vrsg = theHosDB.theReceiptSubgroupDB.selectByvisitIdBillingGroupItem(
                    billing.visit_id, bis.getObjectId());
            //����Ҥ����¡���Ѻ������¡���ش���ª����������� ��������ա�������
            double paidInGroup = 0; // �������ҡ�ê����Թ���駷���ҹ� �� ���С�����������������
            for (int j = 0; vrsg != null && j < vrsg.size(); j++) {
                ReceiptSubgroup rsg = (ReceiptSubgroup) vrsg.get(j);
                paidInGroup += Double.parseDouble(rsg.paid);
            }
            // ��ҡ�������� �ѧ�������ú ������ʹ�Թ����Ѻ����������ѡ�ҡ��ê���㹡������͹˹�ҹ�������� ������ҡ���� 0
            if (paidInGroup < ptMustPaid && moneyForPay > 0.0d) {
                double groupRemain = ptMustPaid - paidInGroup;
                //����ͼ����¨��¤ú ��ͧ�¡��¡�÷������¨��¤ú�����¡�����¡�������
                //����Թ�ҡ���� ��ǹ����ͧ��������ѡ��ǹ�����¤��駷�������͡� �������ǹ����ͧ����
                if (moneyForPay > groupRemain) {
                    moneyForPay -= groupRemain;
                } //����Թ���¡��� �������ҷ�����������
                else {
                    groupRemain = moneyForPay;
                    moneyForPay = 0.0;
                }
                ReceiptSubgroup rs = theHO.initReceiptSubgroup(bis, theReceipt, groupRemain);
                theHosDB.theReceiptSubgroupDB.insert(rs);
                intPatientPaidMoneyInReceiptItem(rs, bis, theReceipt);
            }
        }
        theHO.thePrintReceipt = theReceipt;
        theHO.vBillingPatient = theHosDB.theBillingDB.selectByPatientId(theHO.thePatient.getObjectId());
    }

    public int countReceiptByBilling(Billing billing) {
        int count = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            count = theHosDB.theReceiptDB.selectCountByBillingId(billing.getObjectId());
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return count;
    }

    protected void intPatientPaidMoneyInReceiptItem(ReceiptSubgroup rs, BillingInvoiceSubgroup bis, Receipt receipt) throws Exception {
        //����¡�õ�Ǩ�ѡ�� �����ǹ�ٻ ���͵�Ǩ�ͺ����� �����Թ�ѧ
        double moneyForPay = Double.parseDouble(rs.paid);
        Vector vBillingInvoiceItem = theHosDB.theBillingInvoiceItemDB.listBillingInvoiceItemByVisitIdBillingInvoiceIDBillingGroupItemId(bis.visit_id,
                bis.billing_invoice_id,
                bis.billing_group_item_id);
        if (vBillingInvoiceItem == null) {
            return;
        }
        for (int i = 0, size = vBillingInvoiceItem.size(); i < size; i++) {
            // ������ʹ�������� ��¡�÷�����������ͧ�ѹ�֡�
            if (moneyForPay > 0.0d) {
                BillingInvoiceItem bii = (BillingInvoiceItem) vBillingInvoiceItem.get(i);
                Vector vBillingReceiptItem = theHosDB.theReceiptItemDB.listReceiptItemByVisitIdBillingInvoiceItemID(bis.visit_id, bii.getObjectId());
                //��Ǩ�ͺ�����¡�õ�Ǩ�ѡ�ҹ���� �Ѻ�Թ�����ѧ ���Ѻ�Թ���� �ӡ��Ź�ٻ���� �ӹǳ��� �Ѻ�Թ�ú�����ѧ ��Ҩ��ջѭ�ҵç���
                if (vBillingReceiptItem != null) {
                    double allPaid = 0;
                    for (int j = 0; j < vBillingReceiptItem.size(); j++) {
                        ReceiptItem bri = (ReceiptItem) vBillingReceiptItem.get(j);
                        allPaid += Double.parseDouble(bri.paid);
                    }
                    double total = Double.parseDouble(bii.patient_share);
                    if (total <= 0) {
                        continue; // �������¡�÷�����ʹ�����µ�ͧ����
                    }
                    //total �ӹǹ�Թ����ҧ���¢ͧ���˹����¡�ù�� �ҡ����������ҡѺ
                    //last_paid �ӹǹ�Թ�������ҷ��������¡�� item ���  ���㹡��������ѧ��˹������
                    double remain = total - allPaid;
                    if (remain > 0) {
                        //money �ӹǹ�Թ�������� �ҡ���������ҡѺ  total �ӹǹ�Թ����ҧ����
                        if (moneyForPay >= remain) {
                            moneyForPay = moneyForPay - remain;
                        } else {
                            remain = moneyForPay;
                            moneyForPay = 0;
                        }
                        ReceiptItem bri = theHO.initReceiptItem(bii, receipt, remain);
                        theHosDB.theReceiptItemDB.insert(bri);
                    }
                } //�����á�ͧ����Ѻ�����Թ �ͧ������ visit ��� �ӧҹ��������
                else {
                    double remain = Double.parseDouble(bii.patient_share);
                    if (remain <= 0) {
                        continue; // �������¡�÷�����ʹ�����µ�ͧ����
                    }
                    //�Թ�������¨��� �ҡ���� �Թ����ͧ����
                    if (moneyForPay >= remain) {
                        moneyForPay = moneyForPay - remain;
                    } //�Թ�������¨��� ���¡��� �Թ����ͧ����
                    else {
                        remain = moneyForPay;
                        moneyForPay = 0;
                    }
                    ReceiptItem bri = theHO.initReceiptItem(bii, receipt, remain);
                    theHosDB.theReceiptItemDB.insert(bri);
                }
            }
        }
    }

//    /**
//     * fuction
//     */
//    public float readBillingReceiptSubgroupByVisitIDBillingInvoiceGroupItem(String visit_id, String billinginvoicesubgroup_id) {
//        float patientpaid = 0;
//        theConnectionInf.open();
//        try {
//            Vector vc = theHosDB.theReceiptSubgroupDB.selectByvisitIdBillingGroupItem(visit_id, billinginvoicesubgroup_id);
//            if (vc != null) {
//                for (int i = 0, size = vc.size(); i < size; i++) {
//                    ReceiptSubgroup rs = new ReceiptSubgroup();
//                    rs = (ReceiptSubgroup) vc.get(i);
//                    patientpaid = patientpaid + Float.parseFloat(rs.paid);
//                    rs = null;
//                }
//            } else {
//                patientpaid = 0;
//            }
//            vc = null;
//        } catch (Exception ex) {
//            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
//            patientpaid = 0;
//        }
//        theConnectionInf.close();
//        return patientpaid;
//    }
    /**
     * hosv4 function ��㹡�� ¡��ԡ ��¡�÷����ӹǳ�Թ���� �����¡�ù��
     * ���Ѻ������������ billing_id ������¹ active �� 0 henbe_error wait
     * for new pattern //�ӡ��ź�����š�äԴ�Թ�͡������
     * ����ѧ�����ӹǳ����������� //amp �����ѹ�� exception ����� size =
     * row //insignificant //�����¡�ù���ѧ���١����Թ������ź //henbe_error
     * wait for tunning
     */
    public void cancelBillingInvoice(Vector theBillingInvoice, String number) {
        if (number.isEmpty()) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.BillingControl.VALIDATE.INVOICE.NUMBER"), UpdateStatus.WARNING);
            return;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (theBillingInvoice == null) {
                theBillingInvoice = new Vector();
            }
            for (int i = 0, size = theBillingInvoice.size(); i < size; i++) {
                BillingInvoice bi = (BillingInvoice) theBillingInvoice.get(i);
                if (bi.billing_invoice_no.equals(number)) {
                    Billing bill = theHosDB.theBillingDB.selectByPK(bi.billing_id);
                    if (bill != null && bill.active.equals("1")) {//.billing_complete.equals(Active.isEnable())) {
                        theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.BillingControl.NOT.DELETE.INVOICE.THAT.HAVE.BILLING"), UpdateStatus.WARNING);
                        return;
                    }
                }
            }
            if (!theUS.confirmBox(ResourceBundle.getBundleText("com.hosv3.control.BillingControl.CONFIRM.CANCEL.INVOICE"), UpdateStatus.WARNING)) {
                return;
            }
            for (int i = theBillingInvoice.size() - 1; i >= 0; i--) {
                BillingInvoice bi = (BillingInvoice) theBillingInvoice.get(i);
                if (bi.billing_invoice_no.equals(number)) {
                    intCancelBillingInvoice(theBillingInvoice, bi);
                    theBillingInvoice.remove(i);
                }
            }
            theHO.vBillingInvoice = theBillingInvoice;
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.UNKONW, ResourceBundle.getBundleText("com.hosv3.control.BillingControl.CANCEL.INVOICE"));
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theBillingSubject.notifyCancelBillingInvoice(
                    ResourceBundle.getBundleText("com.hosv3.control.BillingControl.CANCEL.INVOICE") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.UNKONW, ResourceBundle.getBundleText("com.hosv3.control.BillingControl.CANCEL.INVOICE"));
        }
    }

    /**
     * hosv4
     */
    private boolean intCancelBillingInvoice(Vector theBillingInvoice, BillingInvoice bi)
            throws Exception {
        List<Object[]> vOrder = new ArrayList<>();
        String sql = "SELECT t_billing_invoice_item.t_billing_invoice_item_id\n"
                + "    ,t_order.t_order_id\n"
                + "FROM t_billing_invoice_item\n"
                + "    INNER JOIN t_order ON t_billing_invoice_item.t_order_item_id = t_order.t_order_id\n"
                + "   WHERE t_billing_invoice_id = ?\n"
                + "     AND  billing_invoice_item_active = ?\n"
                + "UNION\n"
                + "select t_billing_invoice_item.t_billing_invoice_item_id\n"
                + "    ,t_order_package.t_order_sub_id\n"
                + "from t_order_package\n"
                + "    inner join t_order on t_order_package.t_order_id = t_order.t_order_id\n"
                + "    inner join t_billing_invoice_item ON t_order.t_order_id = t_billing_invoice_item.t_order_item_id\n"
                + "   WHERE t_billing_invoice_id = ?\n"
                + "     AND  billing_invoice_item_active = ?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, bi.getObjectId());
            ePQuery.setString(index++, "1");
            ePQuery.setString(index++, bi.getObjectId());
            ePQuery.setString(index++, "1");
            vOrder = theConnectionInf.eComplexQuery(ePQuery.toString());
        }

        for (int i = 0; vOrder != null && i < vOrder.size(); i++) {
            Object[] order = (Object[]) vOrder.get(i);
            //update oi.charge_complete order_complete by order_item_id
            theHosDB.theOrderItemDB.updateCObyId(Active.isDisable(), Active.isEnable(), (String) order[1]);
        }
        if (!bi.billing_id.isEmpty()) {//�¼�ҹ��äԴ�Թ����
            bi.active = Active.isDisable();
            theHosDB.theBillingInvoiceDB.update(bi);
            //udpate active by billing_invoice_id
            theHosDB.theBillingInvoiceSubgroupDB.updateAbyBi("0", bi.getObjectId());
            theHosDB.theBillingInvoiceItemDB.updateAbyBi("0", bi.getObjectId());
        } else {   //1����� �� delete billing_invoice
            theHosDB.theBillingInvoiceDB.delete(bi);
            theHosDB.theBillingInvoiceSubgroupDB.deletebyBi(bi.getObjectId());
            theHosDB.theBillingInvoiceItemDB.deletebyBi(bi.getObjectId());
        }
        return true;
    }

    /**
     * function ���Ң����Ţͧ BillingInvoiceItem �� ��ä��ҵ�� visit_id ���
     * billing_invoice_id
     */
    public Vector listBillingInvoiceItemByVisitIdBillingInvoiceID(String visit_id, String billing_invoice_id) {
        Vector result = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theBillingInvoiceItemDB.listBillingInvoiceItemByVisitIdBillingInvoiceID(visit_id, billing_invoice_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    /**
     * function ���Ң����Ţͧ BillingInvoiceSubgroup �� ��ä��ҵ�� visit_id
     * ��� billing_invoice_id
     */
    public Vector listBillingInvoiceSubgroupByVisitIdBillingInvoiceID(String visit_id, String billing_invoice_id) {
        Vector result = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theBillingInvoiceSubgroupDB.listBillingInvoiceSubgroupByVisitIdBillingInvoiceID(visit_id, billing_invoice_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    /**
     * function ���Ң����Ţͧ BillingInvoiceItem �� ��ä��ҵ�� visit_id ���
     * billing_invoice_id
     */
    public Vector listBillingInvoiceByVisitIdBillingID(String visit_id, String billing_id) {
        Vector result = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theBillingInvoiceDB.listBillingInvoiceByVisitIdBillingID(visit_id, billing_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    /**
     * function ���Ң����Ţͧ BillingInvoiceSubgroup �� ��ä��ҵ�� visit_id
     * ��� billing_invoice_id
     */
    public Vector listBillingInvoiceSubgroupByVisitIdBillingID(String visit_id, String billing_id) {
        Vector result = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theBillingInvoiceSubgroupDB.listBillingInvoiceSubgroupByVisitIdBillingID(visit_id, billing_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    /**
     * function ���Ң����Ţͧ BillingInvoiceSubgroup �� ��ä��ҵ�� visit_id
     * ��� billing_invoice_id
     */
    public Vector listBillingInvoiceItemByVisitIdBillingID(String visit_id, String billing_id) {
        Vector result = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theBillingInvoiceItemDB.listBillingInvoiceItemByVisitIdBillingID(visit_id, billing_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }

        return result;
    }

    /**
     * function ���Ң����Ţͧ BillingInvoiceSubgroup �� ��ä��ҵ�� visit_id
     * ��� billing_invoice_id
     */
    public Vector listReceiptByVisitIdBillingID(String visit_id, String billing_id) {
        Vector result = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theReceiptDB.listReceiptByVisitIdBillingID(visit_id, billing_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }

        return result;
    }

    /**
     * function ���Ң����Ţͧ BillingInvoiceSubgroup �� ��ä��ҵ�� visit_id
     * ��� billing_invoice_id
     */
    public Vector listReceiptSubgroupByVisitIdReceiptID(String visit_id, String receipt_id) {
        Vector result = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theReceiptSubgroupDB.listReceiptSubgroupByVisitIdReceiptID(visit_id, receipt_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }

        return result;
    }

    /**
     * function ��㹡�� update BillingInvoiceItem
     */
    public int updateBillingInvoiceItem(BillingInvoiceItem bi) {
        int result = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theBillingInvoiceItemDB.update(bi);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }

        return result;
    }

    /**
     * function ��㹡�� update BillingInvoiceSubgroup
     */
    public int updateBillingInvoiceSubgroup(BillingInvoiceSubgroup bis) {
        int result = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theBillingInvoiceSubgroupDB.update(bis);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }

        return result;
    }

    /**
     * function ��㹡�� delete BillingInvoiceItem
     */
    int deleteBillingInvoiceItem(BillingInvoiceItem bi) {
        int result = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theBillingInvoiceItemDB.delete(bi);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    /**
     * function ��㹡�� delete BillingInvoiceSubgroup
     */
    int deleteBillingInvoiceSubgroup(BillingInvoiceSubgroup bis) {
        int result = 0;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theBillingInvoiceSubgroupDB.delete(bis);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    public Billing selectBillingById(String id) {
        Billing theBilling = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theBilling = theHosDB.theBillingDB.selectByPK(id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theBilling;
    }

    /**
     * function ź��¡�á�äԴ�Թ hosv4
     */
    public void cancelBill(Vector theBilling, int row) {
        if (row == -1) {
            theUS.setStatus(ResourceBundle.getBundleGlobal("TEXT.PLEASE.SELECT.ITEM"), UpdateStatus.WARNING);
            return;
        }
        Billing billing = (Billing) theBilling.get(row);
        billing = selectBillingById(billing.getObjectId());
        if ("0".equals(billing.active)) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.BillingControl.ALREADY.CANCEL.BILLING"), UpdateStatus.WARNING);
            return;
        }
        if (billing.f_api_payment_status_id == ApiPaymentStatus.PENDING) {
            if (theLookupControl.readOption().enable_api_payment_gateway.equals(Active.isEnable())) {
                boolean retb = DialogPasswd.showDialog(theHO,
                        theUS,
                        theLookupControl.readOption().passwd_cancel_receipt_payment_gateway,
                        "¡��ԡ��¡�����˹�� ����ա���觢����ż�ҹ�к�����Ѻ�����Թ�͹�Ź� ������Է����ҹ�鹷�������öź��");
                if (!retb) {
                    theUS.setStatus(ResourceBundle.getBundleGlobal("VALIDATE.INCORECT.PASSWORD"), UpdateStatus.WARNING);
                    return;
                }
            } else {
                theUS.setStatus(ResourceBundle.getBundleText("��سҵ�駤���Դ�����ҹ����������� ���ͧ�ҡ��¡�ê����Թ�ա���觢����š���Ѻ����"), UpdateStatus.WARNING);
                return;
            }
        }
        // Somprasong 26102011 �����˵ؼš��¡��ԡ
        ConfirmCancelDialog confirmCancelDialog = new ConfirmCancelDialog(null, true);
        if (confirmCancelDialog.showDialog() == 0) {
            return;
        }

        try {
            if (theLookupControl.readOption().cancel_receipt.equals(Active.isEnable())) {
                Date now = hosControl.theLookupControl.intReadTimeStamp();
                String checkTime = theLookupControl.readOption().Cancel_Receipt_Time;
                Date checkDate = DateUtil.convertStringToDate(
                        billing.receipt_date.substring(0, 11) + checkTime,
                        "yyyy-MM-dd,HH:mm",
                        DateUtil.LOCALE_TH);
                Calendar c = Calendar.getInstance();
                c.setTime(checkDate);
                c.add(Calendar.DATE, 1); // Add 1 day
                Date checkNextDate = c.getTime();
                Date receiptDate = DateUtil.convertStringToDate(
                        billing.receipt_date,
                        "yyyy-MM-dd,HH:mm:ss",
                        DateUtil.LOCALE_TH);
                if ((receiptDate.before(checkDate) && now.after(checkDate))
                        || (receiptDate.after(checkDate) && now.after(checkNextDate))) {
                    theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.BillingControl.ENTER.PASSWORD.OVER24HRS"), UpdateStatus.WARNING);
                    boolean retb = DialogPasswd.showDialog(theHO, theUS, theLookupControl.readOption().passwd_cancel_receipt, "¡��ԡ��¡���Թ���� " + checkTime + " �ͧ�ѹ����Ѻ�������� ������Է����ҹ�鹷�������öź��");
                    if (!retb) {
                        theUS.setStatus(ResourceBundle.getBundleGlobal("VALIDATE.INCORECT.PASSWORD"), UpdateStatus.WARNING);
                        return;
                    }
                }
            }
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
        }

        Visit visit = theHO.theVisit;
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String date_time = theLookupControl.intReadDateTime();
            //update billing_id complete by billing_id
            theHosDB.theBillingInvoiceDB.updateBCbyB(billing.getObjectId(), "0", billing.getObjectId());

            double paid = Double.parseDouble(billing.paid);
            if (paid > 0) {
                billing.active = "0";
                billing.staff_cancle_financial = theHO.theEmployee.getObjectId();
                billing.financial_cancle_date_time = date_time;
                theHosDB.theBillingDB.update(billing);
                //�ӡ������¹�ŧ�����Ţͧ���ҧ receipt.active ����� 0
                Vector vr = theHosDB.theReceiptDB.listReceiptByVisitIdBillingID(
                        visit.getObjectId(), billing.getObjectId());
                for (int i = 0, size = vr.size(); i < size; i++) {
                    Receipt r = (Receipt) vr.get(i);
                    //update active by receipt_id
                    theHosDB.theReceiptSubgroupDB.updateAbyR("0", r.getObjectId());
                    theHosDB.theReceiptItemDB.updateAbyR("0", r.getObjectId());
                    r.setObjectId(null);
                    r.receipt_date = theHO.date_time;
                    r.active = "0";
                    r.staff_receipt = theHO.theEmployee.getObjectId();
                    r.paid = Constant.doubleToDBString(0 - Double.parseDouble(r.paid));
                    r.cancel_reason = confirmCancelDialog.getReason();
                    theHosDB.theReceiptDB.insert(r);
                }
                //update active by billing_id
                theHosDB.theReceiptDB.updateAbyB("0", billing.getObjectId(), confirmCancelDialog.getReason());
            } else {   //�ӡ��ź�������͡�ҡ���ҧ billing
                theHosDB.theBillingDB.delete(billing);
            }
            theHO.vBilling.remove(row);
            theHO.vBillingPatient = theHosDB.theBillingDB.selectByPatientId(theHO.thePatient.getObjectId());
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.UNKONW, ResourceBundle.getBundleText("com.hosv3.control.BillingControl.CANCEL.PAYMENT"));
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theBillingSubject.notifyCancelBill(ResourceBundle.getBundleText("com.hosv3.control.BillingControl.CANCEL.PAYMENT") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.UNKONW, ResourceBundle.getBundleText("com.hosv3.control.BillingControl.CANCEL.PAYMENT"));
        }
    }

    /**
     * function ojika pongtorn ����Ѻ����������稷�駷���ԡ�� ��� �����
     * �Ӣ����Ũҡ��ä�����������稷������ҷӡ�èѴ�������������¡�������
     * �����ʹ�������Է�����ú�ҧ ������ѹ����
     * ��觡�÷ӧҹ�ٻẺ��������ҧ��觷��з��� Ẻ special query
     * ����������㹡�÷Ӥ������㨹� 18/4/2549 henbe modify
     */
    protected Vector intConvertDataForPrintBilling(Vector vBillingReceipt) {
        Vector vc = new Vector();
        if (vBillingReceipt == null) {
            return vc;
        }
        SpecialQueryBillingReceipt theBillingReceiptAll = null;
        for (int i = 0, size = vBillingReceipt.size(); i < size; i++) {
            SpecialQueryBillingReceipt theBillingReceipt = (SpecialQueryBillingReceipt) vBillingReceipt.get(i);
            //������ͤ�� i �� 0 ������ Loop �����á
            //��¡�����˹���ѹ�á���ҷӡ�úѹ�֡ŧ� billingAll
            if (i == 0) {
                //�纡������¡�����������
                theBillingReceiptAll = new SpecialQueryBillingReceipt();
                theBillingReceiptAll.paidDraw = "0";
                theBillingReceiptAll.paidNotDraw = "0";
                theBillingReceiptAll.billing = theBillingReceipt.billing;
                //�����ŵ�ǹ������ջ���ª�����������������Է�Էء�Է�Ԩ��ͧ����ѹ���
                theBillingReceiptAll.plan = theBillingReceipt.plan;
                if (theBillingReceipt.draw.equals(Active.isEnable())) {
                    theBillingReceiptAll.paidDraw = theBillingReceipt.paid;
                } else {
                    theBillingReceiptAll.paidNotDraw = theBillingReceipt.paid;
                }
                vc.add(theBillingReceiptAll);
            } //���������áѹ����
            //��¡�����˹���ѹ�Ѵ�ҹ��ҷӡ�����º��º�Ѻ��¡�á�͹˹��
            else {
                String billingNameNew;
                SpecialQueryBillingReceipt theBillingReceiptCheck = (SpecialQueryBillingReceipt) vBillingReceipt.get(i - 1);
                String billingNameOld = theBillingReceiptCheck.billing;
                billingNameNew = theBillingReceipt.billing;
                //�����ŵ�ǹ������ջ���ª�����������������Է�Էء�Է�Ԩ��ͧ����ѹ���
                theBillingReceiptAll.plan = theBillingReceipt.plan;
                //����繡������¡����������ǡѹ��зӡ������������¢ͧ�������¡�ù����Ҵ��¡ѹ������������������ѹ����
                //������ѹ���������ѹ����ջѭ������ѹ�Դ�ҡ�������
                if (billingNameNew.equals(billingNameOld)) {
                    if (theBillingReceipt.draw.equals(Active.isEnable())) {
                        theBillingReceiptAll.paidDraw
                                = Constant.addStringNumber(theBillingReceiptAll.paidDraw, theBillingReceipt.paid);
                    } else {
                        theBillingReceiptAll.paidNotDraw
                                = Constant.addStringNumber(theBillingReceiptAll.paidNotDraw, theBillingReceipt.paid);
                    }
                } //�������繡������¡����������ǡѹ��з� ���ҧ line �����������������¡��¡�������ҧ�Ѵਹ
                else {
                    theBillingReceiptAll = new SpecialQueryBillingReceipt();
                    theBillingReceiptAll.paidDraw = "0";
                    theBillingReceiptAll.paidNotDraw = "0";
                    theBillingReceiptAll.billing = theBillingReceipt.billing;
                    //�����ŵ�ǹ������ջ���ª�����������������Է�Էء�Է�Ԩ��ͧ����ѹ���
                    theBillingReceiptAll.plan = theBillingReceipt.plan;
                    if (theBillingReceipt.draw.equals(Active.isEnable())) {
                        theBillingReceiptAll.paidDraw = theBillingReceipt.paid;
                    } else {
                        theBillingReceiptAll.paidNotDraw = theBillingReceipt.paid;
                    }
                    vc.add(theBillingReceiptAll);
                }
            }
        }
        return vc;
    }

    /**
     * function ojika �֧�����š���Ѻ�����Թ����������������Ҩ����������
     */
    protected Vector dataReceiptItemForPrintBilling(String visit_id, String receiptId) {
        Vector vector = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vector = theHosDB.theSpecialQueryBillingReceiptDB.selectBillingByVisitIdReceiptId(visit_id, receiptId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vector;
    }

    /**
     * function ojika �֧����������Ѻ��þ���������
     */
    public Vector dataForPrintBilling(Visit visit, Billing billing) {
        Vector vPrintBilling = new Vector();
        Vector vReceipt = listReceiptByVisitIdReceiptIdOrderBy(visit.getObjectId(), billing.getObjectId());
        Vector vReceiptSubgroup;
        if (vReceipt != null) {
            vReceiptSubgroup = listReceiptSubgroupByVisitIdReceiptID(visit.getObjectId(), ((Receipt) vReceipt.get(0)).getObjectId());
        } else {
            vReceiptSubgroup = null;
        }
        if (vReceiptSubgroup != null) {
            boolean isCeil = theLookupControl.readOption().use_money_ceil.equals("1");
            for (int i = 0, size = vReceiptSubgroup.size(); i < size; i++) {
                PrintBilling thePrintBilling = new PrintBilling();
                ReceiptSubgroup theReceiptSubgroup = (ReceiptSubgroup) vReceiptSubgroup.get(i);
                double paidSubgroupNow = Double.parseDouble(theReceiptSubgroup.paid);
                // �����Ҩӹǹ�������¨��¡Ѻ�������������������
                Vector vSumBillingInvoiceSubgroupPaid = listReceiptSubgroupByVisitIdBillingGroupItem(visit.getObjectId(), theReceiptSubgroup.billing_invoice_subgroup_id);
                double checkPaidNow = paidSubgroupNow;
                // �Ҩӹǹ�������¨��·������ͧ��¡�� ������駷���������ش����
//                double sumPaidSubgroup = 0;
//                for (int k = 0; k < vSumBillingInvoiceSubgroupPaid.size(); k++) {
//                    sumPaidSubgroup += Double.parseDouble(((ReceiptSubgroup) vSumBillingInvoiceSubgroupPaid.get(k)).paid);
//                }
                double sumPaidUnDraw = 0.0d;
                double sumPaidDraw = 0.0d;
                // �鹢�����㹵��ҧ���͹��������Ѻ��¡�� Item ����ա�äԴ�Թ
                BillingInvoiceSubgroup theBillingInvoiceSubgroup = listBillingInvoiceSubgroupByPk(theReceiptSubgroup.billing_invoice_subgroup_id);
                // �����¡�� Item �������ԡ�������ԡ�����
                Vector vBillingInvoiceItem = listBillingInvoiceItemByVisitIdBillingInvoiceIDBillingGroupItemId(visit.getObjectId(), theBillingInvoiceSubgroup.billing_invoice_id, theBillingInvoiceSubgroup.billing_group_item_id);
                double checkPaidNowTest = checkPaidNow;
                for (int j = 0; j < vBillingInvoiceItem.size(); j++) {
                    double checkPaidDraw;
                    BillingInvoiceItem theBillingInvoiceItem = (BillingInvoiceItem) vBillingInvoiceItem.get(j);
                    double fixPaidItem = Double.parseDouble(theBillingInvoiceItem.patient_share);
                    if (checkPaidNowTest != 0) {
                        if (checkPaidNowTest >= fixPaidItem) {
                            checkPaidNowTest -= fixPaidItem;
                            // 22 Jan 2005
                            checkPaidDraw = fixPaidItem;
                        } else {
                            // 22 Jan 2005
                            checkPaidDraw = checkPaidNowTest;
                            checkPaidNowTest = 0;
                        }
                    } else {
                        if (checkPaidNowTest >= fixPaidItem) {
                            checkPaidNowTest -= fixPaidItem;
                            checkPaidDraw = fixPaidItem;
                        } else {
                            checkPaidDraw = checkPaidNowTest;
                            checkPaidNowTest = 0;
                        }
                        //checkPaidDraw = 0;
                    }
                    if ((theBillingInvoiceItem.draw).equals(Active.isDisable())) {
                        // �ԡ�����
                        sumPaidUnDraw += checkPaidDraw;
                    } else {
                        // �ԡ��
                        sumPaidDraw += checkPaidDraw;
                    }
                }
                // �֧���͵�� Line �����
                BillingGroupItem theBillingGroupItem = new BillingGroupItem();
                try {
                    theBillingGroupItem = theHosDB.theBillingGroupItemDB.selectByPK(theBillingInvoiceSubgroup.billing_group_item_id);
                } catch (Exception e) {
                    LOG.log(java.util.logging.Level.WARNING, null, e);
                }
                thePrintBilling.no = String.valueOf(i);
                thePrintBilling.billGroup = theBillingGroupItem.description;
                thePrintBilling.draw = isCeil
                        ? Constant.calBil(Constant.doubleToDBString(sumPaidDraw))
                        : Constant.doubleToDBString(sumPaidDraw);
                thePrintBilling.unDraw = isCeil
                        ? Constant.calBil(Constant.doubleToDBString(sumPaidUnDraw))
                        : Constant.doubleToDBString(sumPaidUnDraw);
                vPrintBilling.add(thePrintBilling);
            }
        }
        return vPrintBilling;
    }

    /**
     * function tong
     */
    public Vector listBillingInvoiceItemByVisitIdBillingGroupItemId(String visit_id, String billing_group_item_id) {
        Vector result = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theBillingInvoiceItemDB.listBillingInvoiceItemByVisitIdBillingGroupItemId(visit_id, billing_group_item_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    /**
     * function ojika
     */
    public Vector listBillingInvoiceItemByVisitIdBillingInvoiceIDBillingGroupItemId(String visit_id, String billing_id, String billing_group_item_id) {
        Vector result = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theBillingInvoiceItemDB.listBillingInvoiceItemByVisitIdBillingInvoiceIDBillingGroupItemId(visit_id, billing_id, billing_group_item_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    /**
     * function ojika
     */
    public BillingInvoiceSubgroup listBillingInvoiceSubgroupByPk(String pk) {
        BillingInvoiceSubgroup theBillingInvoiceSubgroup = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theBillingInvoiceSubgroup = theHosDB.theBillingInvoiceSubgroupDB.selectByPK(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theBillingInvoiceSubgroup;
    }

    public Vector listReceiptByVisitIdReceiptIdOrderBy(String visit_id, String billing_id) {
        Vector result = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theReceiptDB.listReceiptByVisitIdReceiptIdOrderBy(visit_id, billing_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    /**
     * function
     *
     */
    public Vector listReceiptSubgroupByVisitIdBillingGroupItem(String visit_id, String billing_invoice_subgroup_id) {
        Vector result = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theReceiptSubgroupDB.selectByvisitIdBillingGroupItem(visit_id, billing_invoice_subgroup_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    /**
     * hosv4 function return Vector �ͧ Object Billing
     *
     * @not deprecated henbe unused theGPS
     */
    public Vector listBillingByPatientID(String patient_id) {
        theHO.objectid = patient_id;
        Vector vc = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theBillingDB.selectByPatientId(patient_id);
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.UNKONW, ResourceBundle.getBundleText("com.hosv3.control.BillingControl.SHOW.PAYMENT.HISTORY"));
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.UNKONW, ResourceBundle.getBundleText("com.hosv3.control.BillingControl.SHOW.PAYMENT.HISTORY"));
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public int countBillingInvoiceNotCompleteByVisitID(String visit_id) {
        int i = -1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            i = theHosDB.theBillingInvoiceDB.countBillingInvoiceNotCompleteByVisitID(visit_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return i;
    }

    /**
     * ��㹡�äӹǳ�� �ӹǹ�Թ�������µ�ͧ���� ��Шӹǹ�Թ����Է�Ԫ������
     * �¨Фӹǳ�Ҥ����������ó�
     * ���������ա�����Է��������Է��㹡������Ѻ��ԡ��
     * ���Է���ش���¨����Է�Ԫ����Թ�ͧ ��¡�÷������µ�ͧ����
     * ��仵��������Է�Ԫ����Թ�ͧ �ҡ�Է�Թ���������ǹŴ ���
     * ��¡�÷�������仵��������Է�Թ�� �����仵�����������Թ�ͧ
     * �ҡ�Է�Թ������ǹŴ ��¡�÷������¨е�ͧ���Ш�仵��������Է��
     * �����Թ�ͧ �ҡ�� 2 �Է�� �Է��˹������ǹŴ �ա�Է���������ǹŴ
     * ��¡�÷������ǹŴ������Է�Է������ǹŴ �����¡�÷������µ�ͧ�����ͧ
     * ���������Է�� �����Թ�ͧ �ҡ�� 2 �Է�� �Է��˹���������ǹŴ
     * �ա�Է������ǹŴ ��¡�÷������ǹŴ������Է�Է������ǹŴ
     * �����¡�÷������µ�ͧ�����ͧ ���������Է�� �����Թ�ͧ �ҡ�� 2 �Է��
     * �Է��˹���������ǹŴ �ա�Է���������ǹŴ
     * ��¡�äԴ�Թ��仵��������Է���á �ҡ�� 2 �Է�� �Է��˹������ǹŴ
     * �ա�Է�������ǹŴ ��¡�÷������ǹŴ��仵�����Է���á
     * �����¡�÷���������ǹŴ ��仵�����Է�� �Ѵ� ���������¡�ù������ǹŴ
     * ��㹷���ش��¡�ù���������ö��仵���������¡��˹����¡���� ����
     * �Է�� �����Թ�ͧ
     *
     * ��Ѻ�� �¡�� ���Է���ش����(�Է�Ԫ����Թ) �������¡������
     * ���������Ѻ�Է���á ���� �Է�Ԫ����Թ�ͧ������Դ
     * �����Ҽ����ҹ���¡��¡�ù���������Է�Ԫ����Թ�ͧ
     *
     * @deprecated henbe unused
     */
    public Vector intListBillingOrderItem(String visit_id) throws Exception {
        Vector vret = new Vector();
        String sql = " select t_order.t_order_id"
                + "    , b_item.b_item_id"
                + "    , t_order.order_common_name"
                + "    , t_order.order_price*t_order.order_qty*cast(contract_condition_adjustment as integer)/100 as payershare"
                + "    , t_order.order_price*t_order.order_qty*(100-cast(contract_condition_adjustment as integer))/100 as patientshare"
                + "    , t_visit_payment.b_contract_plans_id"
                + "    , t_visit_payment.t_visit_payment_id"
                + "    , b_contract_condition.contract_condition_draw"
                + "    , t_order.order_request"
                + "    , t_visit.t_visit_id"
                + "    , t_visit.t_patient_id"
                + "    , '' as billing_id"
                + "    , t_order.b_item_subgroup_id"
                + "    , t_order.order_charge_complete"
                + "    , t_order.b_item_billing_subgroup_id"
                + " from (select * from t_order where t_order.t_visit_id = '" + visit_id + "'"
                + "        and t_order.order_charge_complete = '0') as t_order "
                + "    left join b_item on t_order.b_item_id = b_item.b_item_id"
                + "    left join t_visit on (t_visit.t_visit_id = t_order.t_visit_id)"
                + "    left join t_visit_payment on (t_visit_payment.t_visit_id = t_visit.t_visit_id"
                + "        and t_visit_payment.visit_payment_priority = '0')"
                + "    left join b_contract_plans on b_contract_plans.b_contract_plans_id = t_visit_payment.b_contract_plans_id"
                + "    left join b_contract_condition on (b_contract_condition.b_contract_id = b_contract_plans.b_contract_id"
                + "        and t_order.b_item_subgroup_id = b_contract_condition.b_item_subgroup_id)";
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            BillingOrderItem boi = new BillingOrderItem();
            boi.order_item_id = rs.getString("t_order_id");
            boi.item_id = rs.getString("b_item_id");
            boi.common_name = rs.getString("order_common_name");
            boi.payershare = rs.getString("payershare");
            boi.patientshare = rs.getString("patientshare");
            boi.plan_id = rs.getString("b_contract_plans_id");
            boi.payment_id = rs.getString("t_visit_payment_id");
            boi.draw = rs.getString("contract_condition_draw");
            boi.request = rs.getString("order_request");
            boi.visit_id = rs.getString("t_visit_id");
            boi.patient_id = rs.getString("t_patient_id");
            boi.billing_id = rs.getString("billing_id");
            boi.item_group_code_category = rs.getString("b_item_subgroup_id");
            boi.charge_complete = rs.getString("order_charge_complete");
            boi.item_group_code_billing = rs.getString("b_item_billing_subgroup_id");
            vret.add(boi);
        }
        return vret;
    }

    public Vector[] getBoiFromBoi(Vector[] vBoi) {
        //���Է�ԡ���ѡ�Ңͧ�����¡�͹
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (int i = 0; i < vBoi.length; i++) {
                Payment payment = (Payment) theHO.vVisitPayment.get(i);
                getBoiFromBoi(vBoi[i], payment, theHO.theVisit.isVisitTypeOPD());
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vBoi;
    }

    private Vector getBoiFromBoi(Vector vBoi, Payment payment, boolean isOPD) throws Exception {
        Vector contractV = theHosDB.theContractAdjustDB.selectByContractId(payment.contract_kid);
        PlanClaimPrice planClaimPrice = theHosDB.thePlanClaimPriceDB.selectByPlanId(payment.plan_kid);
        // ������Է�����Ҥ��ԡ���������
        boolean isPlanUsePriceClaim = planClaimPrice != null;
        for (int j = 0; j < vBoi.size(); j++) {
            BillingOrderItem boi = (BillingOrderItem) vBoi.get(j);
            //����ǹŴ�ͧ�Է�Թ�� ����¡�� orderitem ������������Է���������
            String adjust = "0";
            for (int i = 0; i < contractV.size(); i++) {
                ContractAdjust ca = (ContractAdjust) contractV.get(i);
                if (ca.covered_id.equals(boi.item_group_code_category)) {
                    adjust = ca.adjustment;
                    //�˵ط���ͧ comment ������Ҽ������繤����͡���ǵͺ��ŧ
                    break;
                }
            }
            //��ͧ੾����¡�÷������ǹŴ ��� ��¡�� order ��������͵�Ǩ
            OrderItem orderItem = boi.theOrderItem;

            if (!orderItem.isService() && !orderItem.isDental() && !orderItem.isChildPackage()) {
                ItemPrice ip;
                if ((orderItem.isDrug() || orderItem.isSupply())
                        && hosControl.theHP.aPanelOrder_Stock != null) {
                    PanelOrderInf orderInf = (PanelOrderInf) hosControl.theHP.aPanelOrder_Stock;
                    ip = orderInf.getItemPrice(orderItem, payment);
                } else {
                    ip = hosControl.theOrderControl.intReadItemPriceByItem(orderItem, payment);
                }
                orderItem.order_price_type = isOPD ? "0" : "1";
                orderItem.price = orderItem.isEditedPrice() // �������Ҥ�����������ͧ�Ѿഷ����Է��
                        ? orderItem.price
                        : Constant.doubleToDBString(orderItem.isOPDPrice()
                                ? ip.price
                                : ip.price_ipd);
                if (orderItem.isXray()) {
                    orderItem.price = Constant.doubleToDBString(
                            Double.parseDouble(orderItem.price) + orderItem.order_xray_film_price);
                }
                orderItem.order_cost = Constant.doubleToDBString(ip.price_cost);
                orderItem.order_share_doctor = ip.item_share_doctor;
                orderItem.order_share_hospital = ip.item_share_hospital;
                orderItem.order_editable_price = ip.item_editable_price;
                orderItem.order_limit_price_min = ip.item_limit_price_min;
                orderItem.order_limit_price_max = ip.item_limit_price_max;
                orderItem.use_price_claim = ip.use_price_claim;
                orderItem.order_price_claim = ip.item_price_claim;
            }
            theHO.updateBillingOrderItem(boi, orderItem, adjust, isPlanUsePriceClaim);
//            double price = Constant.round(isCeil
//                    ? Math.ceil(Double.parseDouble(orderItem.qty) * Double.parseDouble(orderItem.price))
//                    : (Double.parseDouble(orderItem.qty) * Double.parseDouble(orderItem.price)));
//            double payerprice
//                    = orderItem.use_price_claim
//                            ? Constant.round(isCeil ? Math.ceil(orderItem.order_price_claim) : orderItem.order_price_claim)
//                            : Constant.round(isCeil
//                                    ? Math.floor(price * Double.parseDouble(
//                                            adjust == null || adjust.isEmpty() || adjust.equals("-") ? "0" : adjust) / 100)
//                                    : (price * Double.parseDouble(
//                                            adjust == null || adjust.isEmpty() || adjust.equals("-") ? "0" : adjust) / 100));
//            double patientprice = Constant.round(price - payerprice);
//            //��Ң͵�Ǩ������Ŵ��ФԴ�Թ
//            if (orderItem.request.equals("1")) {
//                patientprice = price;
//                payerprice = 0;
//            }
//            boi.payershare = Constant.doubleToDBString(payerprice);
//            boi.patientshare = Constant.doubleToDBString(patientprice);
        }
        return vBoi;
    }

    public Vector[] getBoiFromOrder(Vector vOrder) {
        //���Է�ԡ���ѡ�Ңͧ�����¡�͹
        Vector[] vboi = new Vector[theHO.vVisitPayment.size()];
        for (int i = 0; i < vboi.length; i++) {
            vboi[i] = new Vector();
        }
        Payment payment = (Payment) theHO.vVisitPayment.get(0);
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            PlanClaimPrice planClaimPrice = theHosDB.thePlanClaimPriceDB.selectByPlanId(payment.plan_kid);
            Vector contractV = theHosDB.theContractAdjustDB.selectByContractId(payment.contract_kid);
            for (int i = 0; i < vOrder.size(); i++) {
                OrderItem orderItem = (OrderItem) vOrder.get(i);
                BillingOrderItem boi = theHO.initBillingOrderItem(orderItem, payment, planClaimPrice, contractV);
                vboi[0].add(boi);
            }
            theConnectionInf.getConnection().commit();
            return vboi;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            return null;
        } finally {
            theConnectionInf.close();
        }
    }

    public Vector[] calBillingVisit(Vector vTemp, boolean all) {
        vOrderBilling = vTemp;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            //���Է�ԡ���ѡ�Ңͧ�����¡�͹
            Vector vpayment = theHO.vVisitPayment;
            Payment payment = (Payment) vpayment.get(0);
            PlanClaimPrice planClaimPrice = theHosDB.thePlanClaimPriceDB.selectByPlanId(payment.plan_kid);
            Vector contractV = theHosDB.theContractAdjustDB.selectByContractId(payment.contract_kid);
            if (contractV.isEmpty()) {
                theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.BillingControl.MISSING.DISCOUNT.BY.PLAN"), UpdateStatus.WARNING);
            }
            //copy vector �����������Դ�š�з��Ѻ order ��¡������������
            Vector vOrder = new Vector();
            vOrder.addAll(vTemp);
//            for (int i = 0, size = vTemp.size(); i < size; i++) {
//                vOrder.add(vTemp.get(i));
//            }
            // ����¡�� Order �¡����Է����ǹŴ �¡��ź��¡��Order ����͡�ҡ vector
            // ������ŧ� vector �ͧ�Է�ԡ���ѡ�ҹ������
            Vector[] avorderItem = new Vector[vpayment.size()];
            for (int i = 0; i < vpayment.size(); i++) {
                avorderItem[i] = new Vector();
            }
            //loop of order
//            SpecialContrctAdjustByVGaCT special = null;
            for (int j = vOrder.size() - 1; j >= 0; j--) {
                OrderItem orderItem = (OrderItem) vOrder.get(j);
                BillingOrderItem boi = theHO.initBillingOrderItem(orderItem, payment, planClaimPrice, contractV);
                vOrder.remove(j);
                avorderItem[0].add(boi);
//                String cat_id = orderItem.item_group_code_category;
            }
            //end for loop �ͧ ��¡�÷���ͧ��äԴ�Թ
            theConnectionInf.getConnection().commit();
            return avorderItem;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            return null;
        } finally {
            theConnectionInf.close();
        }
    }
//
//    /**
//     * /**
//     * ��㹡�äӹǳ�� �ӹǹ�Թ�������µ�ͧ���� ��Шӹǹ�Թ����Է�Ԫ������
//     * �¨Фӹǳ�Ҥ����������ó�
//     * ���������ա�����Է��������Է��㹡������Ѻ��ԡ��
//     * ���Է���ش���¨����Է�Ԫ����Թ�ͧ ��¡�÷������µ�ͧ����
//     * ��仵��������Է�Ԫ����Թ�ͧ �ҡ�Է�Թ���������ǹŴ ���
//     * ��¡�÷�������仵��������Է�Թ�� �����仵�����������Թ�ͧ
//     * �ҡ�Է�Թ������ǹŴ ��¡�÷������¨е�ͧ���Ш�仵��������Է��
//     * �����Թ�ͧ �ҡ�� 2 �Է�� �Է��˹������ǹŴ �ա�Է���������ǹŴ
//     * ��¡�÷������ǹŴ������Է�Է������ǹŴ �����¡�÷������µ�ͧ�����ͧ
//     * ���������Է�� �����Թ�ͧ �ҡ�� 2 �Է�� �Է��˹���������ǹŴ
//     * �ա�Է������ǹŴ ��¡�÷������ǹŴ������Է�Է������ǹŴ
//     * �����¡�÷������µ�ͧ�����ͧ ���������Է�� �����Թ�ͧ �ҡ�� 2 �Է��
//     * �Է��˹���������ǹŴ �ա�Է���������ǹŴ
//     * ��¡�äԴ�Թ��仵��������Է���á �ҡ�� 2 �Է�� �Է��˹������ǹŴ
//     * �ա�Է�������ǹŴ ��¡�÷������ǹŴ��仵�����Է���á
//     * �����¡�÷���������ǹŴ ��仵�����Է�� �Ѵ� ���������¡�ù������ǹŴ
//     * ��㹷���ش��¡�ù���������ö��仵���������¡��˹����¡���� ����
//     * �Է�� �����Թ�ͧ
//     *
//     * �ӹǳ��¡������
//     */
//    public Vector[] calBillingVisitCont(Vector[] avector) {
//        theConnectionInf.open();
//        try {
//            theConnectionInf.getConnection().setAutoCommit(false);
//            //ǹ�ٻ�Է�ԡ���ѡ��
//            //ǹ�ٻ order ���������Է��
//            Vector contractV[] = new Vector[theHO.vVisitPayment.size()];
//            for (int i = 0; i < theHO.vVisitPayment.size(); i++) {
//                Payment payment = (Payment) theHO.vVisitPayment.get(i);
//                contractV[i] = theHosDB.theContractAdjustDB.selectByContractId(payment.contract_kid);
//            }
//            //����ӹǹ�Է��
//            for (int i = 0; i < avector.length; i++) {
//                Vector vorder_item = avector[i];
//                for (int j = vorder_item.size() - 1; j >= 0; j--) {
//                    //����¡�� Order ��������Һ�ӹǹ ����Ҥ�
//                    //����ǹŴ�ͧ�Է�Է������� BollingOrderItem
//                    //�����辺���¡����ǹŴ�������ǹŴ�� 0
//                    BillingOrderItem boi = (BillingOrderItem) vorder_item.get(j);
//                    OrderItem orderitem = getOrderItemBill(boi.order_item_id);
//                    ////////////////////////////////////////////////////////////////////
//                    String adjust = "0";
//                    for (int k = 0; k < contractV[i].size(); k++) {
//                        ContractAdjust ca = (ContractAdjust) contractV[i].get(k);
//                        if (ca.covered_id.equals(orderitem.item_group_code_category)) {
//                            adjust = ca.adjustment;
//                            break;
//                        }
//                    }
//                    theHO.updateBillingOrderItem(boi, orderitem, adjust);
//                    vorder_item.remove(j);
//                    vorder_item.add(boi);
//                    avector[i] = vorder_item;
//                }
//            }
//            theConnectionInf.getConnection().commit();
//            return avector;
//        } catch (Exception ex) {
//            LOG.log(java.util.logging.Level.SEVERE, null, ex);
//            try {
//                theConnectionInf.getConnection().rollback();
//            } catch (SQLException ex1) {
//                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
//            }
//            return null;
//        } finally {
//            theConnectionInf.close();
//        }
//    }

//    private OrderItem getOrderItemBill(String order_item_id) throws Exception {
//        for (int i = 0; i < vOrderBilling.size(); i++) {
//            OrderItem oi = (OrderItem) vOrderBilling.get(i);
//            if (oi.getObjectId().equals(order_item_id)) {
//                return oi;
//            }
//        }
//        return theHosDB.theOrderItemDB.selectByPK(order_item_id);
//    }
    /**
     * avOrderItem �������ҹ����
     */
    public void dischargeFinancial(Vector[] avOrderItem, Vector vOrderItem, String string) {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            isComplete = theVisitControl.intDischargeFinancial();
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus(ResourceBundle.getBundleGlobal("TEXT.FINANCE.DISCHARGE") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);

        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVisitSubject.notifyDischargeFinancial(ResourceBundle.getBundleGlobal("TEXT.FINANCE.DISCHARGE") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        }
    }

    public List<ComplexDataSource> listDSInsurancePlanByBillingInvoiceId(String id) {
        List<ComplexDataSource> list = new ArrayList<ComplexDataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> listDatas = theHosDB.theVisitInsurancePlanDB.listClaimInfoByBillingInvoiceId(id);
            for (Object[] objs : listDatas) {
                list.add(new ComplexDataSource(new String[]{
                    objs[0] == null ? "" : String.valueOf(objs[0]),
                    objs[6] == null ? "" : String.valueOf(objs[6])
                },
                        new String[]{
                            objs[1] == null ? "" : String.valueOf(objs[1]),
                            objs[2] == null ? "" : String.valueOf(objs[2]),
                            objs[3] == null ? "" : String.valueOf(objs[3]),
                            objs[4] == null ? "" : String.valueOf(objs[4]),
                            objs[5] == null ? "" : String.valueOf(objs[5])
                        }));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            Logger.getLogger(VisitControl.class.getName()).log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(VisitControl.class.getName()).log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public int createFaxClaim(String billingInvoiceID, String visitInsurancePlanID) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            VisitInsurancePlan vip = theHosDB.theVisitInsurancePlanDB.select(visitInsurancePlanID);
            FinanceInsuranceCompany fic = theHosDB.theFinanceInsuranceCompanyDB.select(vip.b_finance_insurance_company_id);
            InsuranceClaim ic = new InsuranceClaim();
            ic.t_billing_invoice_id = billingInvoiceID;
            ic.t_visit_insurance_plan_id = visitInsurancePlanID;
            ic.status_insurance_approve = "1";
            ic.record_datetime = theLookupControl.intReadDateTime();
            ic.user_record_id = theHO.theEmployee.getObjectId();
            ic.update_datetime = theLookupControl.intReadDateTime();
            ic.user_update_id = theHO.theEmployee.getObjectId();

            ic.alert_date = fic == null ? com.hospital_os.utility.DateUtil.getDateFromText(theLookupControl.intReadDateTime())
                    : com.hospital_os.utility.DateUtil.getDateFromText(com.hospital_os.utility.DateUtil.addDay(theLookupControl.intReadDateTime(), fic.alert_repay_day));
            ic.status_insurance_receive = "1";
            theHosDB.theInsuranceClaimDB.insert(ic);
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        if (res == 1) {
            theHS.theBillingSubject.notifyBillingInvoice(ResourceBundle.getBundleText("com.hosv3.control.BillingControl.CALCULATE.COST.INVOICE") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        }
        return res;
    }

    public InsuranceClaim getInsuranceClaimById(String id) {
        InsuranceClaim ic = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ic = theHosDB.theInsuranceClaimDB.select(id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ic;
    }

    public VisitInsurancePlan getVisitInsurancePlanById(String id) {
        VisitInsurancePlan vip = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vip = theHosDB.theVisitInsurancePlanDB.select(id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vip;
    }

    public List<ComplexDataSource> listDSClaimInfo(String insuranceClaimID) {
        List<ComplexDataSource> list = new ArrayList<ComplexDataSource>();
        theConnectionInf.open();
        try {
            DecimalFormat df = new DecimalFormat("##.00");
            theConnectionInf.getConnection().setAutoCommit(false);
            List<Object[]> listDatas = theHosDB.theInsuranceClaimSubgroupDB.listClaimInfo(insuranceClaimID);
            for (Object[] objs : listDatas) {
                double price = Double.valueOf(df.format(objs[3] == null ? 0.0d : Double.parseDouble(String.valueOf(objs[3])))).doubleValue();
                double discount = Double.valueOf(df.format(objs[4] == null ? 0.0d : Double.parseDouble(String.valueOf(objs[4])))).doubleValue();
                double paid = Double.valueOf(df.format(price - discount)).doubleValue();
                list.add(new ComplexDataSource(new String[]{
                    objs[0] == null ? "" : String.valueOf(objs[0]),
                    objs[1] == null ? "" : String.valueOf(objs[1])},
                        new Object[]{
                            objs[2] == null ? "" : String.valueOf(objs[2]),
                            price,
                            discount,
                            paid,
                            0.0d
                        }));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            Logger.getLogger(VisitControl.class.getName()).log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(VisitControl.class.getName()).log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public int saveClaimInfo(InsuranceClaim insuranceClaim, List<InsuranceClaimSubgroup> list) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            insuranceClaim.update_datetime = theLookupControl.intReadDateTime();
            insuranceClaim.user_update_id = theHO.theEmployee.getObjectId();
            theHosDB.theInsuranceClaimDB.updateApprove(insuranceClaim);
            for (InsuranceClaimSubgroup ics : list) {
                if (ics.record_datetime.isEmpty()) {
                    ics.record_datetime = theLookupControl.intReadDateTime();
                }
                ics.user_record_id = theHO.theEmployee.getObjectId();
                theHosDB.theInsuranceClaimSubgroupDB.insert(ics);
                // update payer paid in t_billing_invoice_billing_subgroup
                BillingInvoiceSubgroup bis = theHosDB.theBillingInvoiceSubgroupDB.selectByPK(ics.t_billing_invoice_billing_subgroup_id);
                bis.payer_share = Constant.doubleToDBString(Double.parseDouble(Constant.doubleToDBString(Double.parseDouble(bis.payer_share))) + ics.insurance_discount + ics.insurance_pay);
                bis.payer_share_ceil = Constant.doubleToDBString(Double.parseDouble(Constant.doubleToDBString(Double.parseDouble(bis.payer_share_ceil))) + ics.insurance_discount + ics.insurance_pay);
                bis.patient_share = Constant.doubleToDBString(Double.parseDouble(Constant.doubleToDBString(Double.parseDouble(bis.patient_share))) - (ics.insurance_discount + ics.insurance_pay));
                bis.patient_share_ceil = Constant.doubleToDBString(Double.parseDouble(Constant.doubleToDBString(Double.parseDouble(bis.patient_share_ceil))) - (ics.insurance_discount + ics.insurance_pay));
                theHosDB.theBillingInvoiceSubgroupDB.update(bis);
                // update t_billing_invoice
                BillingInvoice billingInvoice = theHosDB.theBillingInvoiceDB.selectByPK(bis.billing_invoice_id);
                billingInvoice.payer_share = Constant.doubleToDBString(Double.parseDouble(Constant.doubleToDBString(Double.parseDouble(billingInvoice.payer_share))) + ics.insurance_discount + ics.insurance_pay);
                billingInvoice.payer_share_ceil = Constant.doubleToDBString(Double.parseDouble(Constant.doubleToDBString(Double.parseDouble(billingInvoice.payer_share_ceil))) + ics.insurance_discount + ics.insurance_pay);
                billingInvoice.patient_share = Constant.doubleToDBString(Double.parseDouble(Constant.doubleToDBString(Double.parseDouble(billingInvoice.patient_share))) - (ics.insurance_discount + ics.insurance_pay));
                billingInvoice.patient_share_ceil = Constant.doubleToDBString(Double.parseDouble(Constant.doubleToDBString(Double.parseDouble(billingInvoice.patient_share_ceil))) - (ics.insurance_discount + ics.insurance_pay));
                theHosDB.theBillingInvoiceDB.update(billingInvoice);
                // update t_billing
                Billing billing = theHosDB.theBillingDB.selectByPK(bis.billing_id);
                billing.payer_share = Constant.doubleToDBString(Double.parseDouble(Constant.doubleToDBString(Double.parseDouble(billing.payer_share))) + ics.insurance_discount + ics.insurance_pay);
                billing.patient_share = Constant.doubleToDBString(Double.parseDouble(Constant.doubleToDBString(Double.parseDouble(billing.patient_share))) - (ics.insurance_discount + ics.insurance_pay));
                billing.remain = Constant.doubleToDBString(Double.parseDouble(Constant.doubleToDBString(Double.parseDouble(billing.remain))) - (ics.insurance_discount + ics.insurance_pay));
                theHosDB.theBillingDB.update(billing);
            }
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        if (res == 1) {
            theHS.theBillingSubject.notifyBillingInvoice(ResourceBundle.getBundleText("com.hosv3.control.BillingControl.CALCULATE.COST.INVOICE") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        }
        return res;
    }

    public List<FinanceInsurancePlan> listFinanceInsurancePlanByCompanyId(String companyId) {
        List<FinanceInsurancePlan> list = new ArrayList<FinanceInsurancePlan>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            list.addAll(this.theHosDB.theFinanceInsurancePlanDB.listByCompanyId(companyId));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public List<ComplexDataSource> listRemainFaxClaim(String companyId, String planId, Date startDate, Date endDate, String faxClaimCode, String insurance) {
        List<ComplexDataSource> list = new ArrayList<>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String sql = "select \n"
                    + "t_insurance_claim.t_insurance_claim_id\n"
                    + ", t_visit.visit_hn\n"
                    + ", t_visit.visit_vn\n"
                    + ", case when f_patient_prefix.patient_prefix_description is null then '' else f_patient_prefix.patient_prefix_description end\n"
                    + " || t_patient.patient_firstname || ' ' || t_patient.patient_lastname as patien_name\n"
                    + ", t_visit.visit_begin_visit_time\n"
                    + ", t_insurance_claim.claim_code\n"
                    + ", sum(t_insurance_claim_subgroup.insurance_pay)\n"
                    + ", t_visit_insurance_plan.b_finance_insurance_company_id\n"
                    + "from t_insurance_claim\n"
                    + "inner join t_billing_invoice on t_billing_invoice.t_billing_invoice_id = t_insurance_claim.t_billing_invoice_id and t_billing_invoice.billing_invoice_complete = '1'\n"
                    + "inner join t_visit_insurance_plan on t_visit_insurance_plan.t_visit_insurance_plan_id = t_insurance_claim.t_visit_insurance_plan_id and t_visit_insurance_plan.active = '1'\n"
                    + "inner join t_visit_payment on t_visit_payment.t_visit_payment_id = t_visit_insurance_plan.t_visit_payment_id and t_visit_payment.visit_payment_active = '1'\n"
                    + "inner join t_visit on t_visit.t_visit_id = t_visit_payment.t_visit_id and t_visit.f_visit_status_id <> '4'\n"
                    + "inner join t_patient on t_patient.patient_hn = t_visit.visit_hn\n"
                    + "left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_patient.f_patient_prefix_id\n"
                    + "inner join t_insurance_claim_subgroup on t_insurance_claim_subgroup.t_insurance_claim_id = t_insurance_claim.t_insurance_claim_id\n"
                    + "inner join b_map_finance_insurance on b_map_finance_insurance.b_contract_plans_id =t_visit_payment.b_contract_plans_id\n"
                    + "where\n"
                    + "t_insurance_claim.status_insurance_approve = '0'\n"
                    + "and t_insurance_claim.status_insurance_receive = '1'\n";
            if (companyId != null && !companyId.isEmpty()) {
                sql += "and t_visit_insurance_plan.b_finance_insurance_company_id = ?\n";
            }
            if (planId != null && !planId.isEmpty()) {
                sql += "and t_visit_insurance_plan.b_finance_insurance_plan_id = ?\n";
            }
            if (startDate != null && endDate != null) {
                sql += "and t_insurance_claim.claim_code_datetime between ? and ?\n";
            }
            if (faxClaimCode != null && !faxClaimCode.isEmpty()) {
                sql += "and t_insurance_claim.claim_code like ?\n";
            }
            if (insurance != null && !insurance.isEmpty()) {
                sql += "and b_map_finance_insurance.b_map_finance_insurance_id = ?";
            }
            sql += "group by\n"
                    + "t_insurance_claim.t_insurance_claim_id\n"
                    + ", t_visit.visit_hn\n"
                    + ", t_visit.visit_vn\n"
                    + ", t_patient.patient_firstname\n"
                    + ", t_patient.patient_lastname\n"
                    + ", f_patient_prefix.patient_prefix_description\n"
                    + ", t_visit.visit_begin_visit_time\n"
                    + ", t_insurance_claim.claim_code\n"
                    + ", t_visit_insurance_plan.b_finance_insurance_company_id\n"
                    + "order by t_insurance_claim.claim_code";

            PreparedStatement ps = theConnectionInf.getConnection().prepareStatement(sql);
            int index = 1;
            if (companyId != null && !companyId.isEmpty()) {
                ps.setString(index++, companyId);
            }
            if (planId != null && !planId.isEmpty()) {
                ps.setString(index++, planId);
            }
            if (startDate != null && endDate != null) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(startDate);
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                ps.setTimestamp(index++, new java.sql.Timestamp(calendar.getTimeInMillis()));
                calendar.setTime(endDate);
                calendar.set(Calendar.HOUR_OF_DAY, 23);
                calendar.set(Calendar.MINUTE, 59);
                calendar.set(Calendar.SECOND, 59);
                ps.setTimestamp(index++, new java.sql.Timestamp(calendar.getTimeInMillis()));
            }
            if (faxClaimCode != null && !faxClaimCode.isEmpty()) {
                ps.setString(index++, "%" + faxClaimCode + "%");
            }
            if (insurance != null && !insurance.isEmpty()) {
                ps.setString(index++, insurance);
            }
            List<Object[]> eComplexQuery = theConnectionInf.eComplexQuery(ps.toString());
            for (Object[] objects : eComplexQuery) {
                list.add(new ComplexDataSource(
                        new String[]{
                            String.valueOf(objects[0]), String.valueOf(objects[7])},
                        new Object[]{
                            String.valueOf(objects[1]), String.valueOf(objects[2]), String.valueOf(objects[3]),
                            String.valueOf(objects[4]), String.valueOf(objects[5]), Double.parseDouble(String.valueOf(objects[6]))
                        }));

            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public FinanceInsuranceCompany getFinanceInsuranceCompanyById(String companyId) {
        FinanceInsuranceCompany fic = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            fic = (this.theHosDB.theFinanceInsuranceCompanyDB.select(companyId));
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return fic;
    }

    public int doSaveInsuranceClaimBilling(InsuranceClaimBilling icb, List<ComplexDataSource> claimIds) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            icb.record_datetime = theLookupControl.intReadDateTime();
            icb.user_record_id = theHO.theEmployee.getObjectId();
            theHosDB.theInsuranceClaimBillingDB.insert(icb);
            for (ComplexDataSource complexDataSource : claimIds) {
                InsuranceClaimBillingItem icbi = new InsuranceClaimBillingItem();
                icbi.t_insurance_claim_billing_id = icb.getObjectId();
                icbi.t_insurance_claim_id = String.valueOf(((Object[]) complexDataSource.getId())[0]);
                icbi.total_payment = Double.parseDouble(complexDataSource.getValue(5).toString());
                icbi.record_datetime = theLookupControl.intReadDateTime();
                icbi.user_record_id = theHO.theEmployee.getObjectId();
                theHosDB.theInsuranceClaimBillingItemDB.insert(icbi);

                InsuranceClaim ic = new InsuranceClaim();
                ic.setObjectId(icbi.t_insurance_claim_id);
                ic.status_insurance_receive = "0";
                ic.update_datetime = theLookupControl.intReadDateTime();
                ic.user_update_id = theHO.theEmployee.getObjectId();
                theHosDB.theInsuranceClaimDB.updateReceiveStatus(ic);
            }
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public List<String> createClaimBillingReceipt(String claimBillingId, String companyName, int type) {
        List<String> receiptNumbers = new ArrayList<String>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (type == 0) {
                String sql = "select\n"
                        + "t_patient.t_patient_id\n"
                        + ", case when f_patient_prefix.patient_prefix_description is null then '' else f_patient_prefix.patient_prefix_description end\n"
                        + "|| t_patient.patient_firstname || ' ' || t_patient.patient_lastname as patientName\n"
                        + "from t_insurance_claim_billing\n"
                        + "inner join t_insurance_claim_billing_item on t_insurance_claim_billing_item.t_insurance_claim_billing_id = t_insurance_claim_billing.t_insurance_claim_billing_id\n"
                        + "inner join t_insurance_claim on t_insurance_claim.t_insurance_claim_id = t_insurance_claim_billing_item.t_insurance_claim_id\n"
                        + "inner join t_visit_insurance_plan on t_visit_insurance_plan.t_visit_insurance_plan_id = t_insurance_claim.t_visit_insurance_plan_id\n"
                        + "inner join t_visit_payment on t_visit_payment.t_visit_payment_id = t_visit_insurance_plan.t_visit_payment_id\n"
                        + "inner join t_visit on t_visit.t_visit_id = t_visit_payment.t_visit_id\n"
                        + "inner join t_patient on t_patient.patient_hn = t_visit.visit_hn\n"
                        + "left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_patient.f_patient_prefix_id\n"
                        + "where t_insurance_claim_billing.t_insurance_claim_billing_id = ?\n"
                        + "group by \n"
                        + "t_patient.t_patient_id\n"
                        + ", t_patient.patient_firstname\n"
                        + ", t_patient.patient_lastname\n"
                        + ", f_patient_prefix.patient_prefix_description";
                PreparedStatement ps = theConnectionInf.getConnection().prepareStatement(sql);

                ps.setString(1, claimBillingId);
                List<Object[]> eComplexQuery = theConnectionInf.eComplexQuery(ps.toString());
                for (Object[] objects : eComplexQuery) {
                    InsuranceClaimBillingReceipt icbr = new InsuranceClaimBillingReceipt();
                    icbr.t_insurance_claim_billing_id = claimBillingId;
                    icbr.receipt_type = String.valueOf(type);
                    icbr.receipt_number = intGenReceiptNumber();
                    icbr.receipt_display_name = String.valueOf(objects[1]);
                    icbr.t_patient_id = String.valueOf(objects[0]);
                    icbr.record_datetime = theLookupControl.intReadDateTime();
                    icbr.user_record_id = theHO.theEmployee.getObjectId();
                    theHosDB.theInsuranceClaimReceiptDB.insert(icbr);
                    receiptNumbers.add(icbr.receipt_number);
                }
            } else {
                InsuranceClaimBillingReceipt icbr = new InsuranceClaimBillingReceipt();
                icbr.t_insurance_claim_billing_id = claimBillingId;
                icbr.receipt_type = String.valueOf(type);
                icbr.receipt_number = intGenReceiptNumber();
                icbr.receipt_display_name = companyName;
                icbr.record_datetime = theLookupControl.intReadDateTime();
                icbr.user_record_id = theHO.theEmployee.getObjectId();
                theHosDB.theInsuranceClaimReceiptDB.insert(icbr);
                receiptNumbers.add(icbr.receipt_number);
            }

            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return receiptNumbers;
    }

    public int updatePrintCountClaimBillingReceipt(String... receiptNumbers) {
        int res = 1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theInsuranceClaimReceiptDB.updatePrintCount(receiptNumbers);
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            res = 0;
        } finally {
            theConnectionInf.close();
        }
        return res;
    }

    public List<ComplexDataSource> listInsurancePaymentHistory(Date startDate, Date endDate) {
        List<ComplexDataSource> list = new ArrayList<ComplexDataSource>();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String sql = "select\n"
                    + "t_insurance_claim_billing.t_insurance_claim_billing_id\n"
                    + ", t_insurance_claim_billing.payment_date\n"
                    + ", b_finance_insurance_company.company_name\n"
                    + ", t_insurance_claim_receipt.receipt_number\n"
                    + ", t_insurance_claim_receipt.receipt_display_name\n"
                    + ", case when t_insurance_claim_receipt.receipt_type = '0' then receipt_patient.debt \n"
                    + "else t_insurance_claim_billing.total_debt - t_insurance_claim_billing.total_discount end as total_debt\n"
                    + ", case when t_insurance_claim_receipt.receipt_type = '0' then receipt_patient.tax \n"
                    + "else t_insurance_claim_billing.total_tax end as total_tax\n"
                    + ", case when t_insurance_claim_receipt.receipt_type = '0' then receipt_patient.payment\n"
                    + "else t_insurance_claim_billing.total_payment end as total_payment\n"
                    + ", t_insurance_claim_receipt.receipt_type\n"
                    + "from t_insurance_claim_billing\n"
                    + "inner join b_finance_insurance_company on b_finance_insurance_company.b_finance_insurance_company_id = t_insurance_claim_billing.b_finance_insurance_company_id\n"
                    + "left join t_insurance_claim_receipt on t_insurance_claim_receipt.t_insurance_claim_billing_id = t_insurance_claim_billing.t_insurance_claim_billing_id\n"
                    + "left join (select \n"
                    + "t_patient.t_patient_id as t_patient_id\n"
                    + ", t_insurance_claim_billing.t_insurance_claim_billing_id as t_insurance_claim_billing_id\n"
                    + ", sum(t_insurance_claim_billing_item.total_payment) - t_insurance_claim_billing.total_discount as debt\n"
                    + ", ((sum(t_insurance_claim_billing_item.total_payment) - t_insurance_claim_billing.total_discount) * t_insurance_claim_billing.total_vat)/100 as tax\n"
                    + ", (sum(t_insurance_claim_billing_item.total_payment) - t_insurance_claim_billing.total_discount) - (((sum(t_insurance_claim_billing_item.total_payment) - t_insurance_claim_billing.total_discount) * t_insurance_claim_billing.total_vat)/100) as payment\n"
                    + "from \n"
                    + "t_insurance_claim_billing\n"
                    + "inner join t_insurance_claim_billing_item on t_insurance_claim_billing_item.t_insurance_claim_billing_id = t_insurance_claim_billing.t_insurance_claim_billing_id\n"
                    + "inner join t_insurance_claim on t_insurance_claim.t_insurance_claim_id = t_insurance_claim_billing_item.t_insurance_claim_id\n"
                    + "inner join t_visit_insurance_plan on t_visit_insurance_plan.t_visit_insurance_plan_id = t_insurance_claim.t_visit_insurance_plan_id\n"
                    + "inner join t_visit_payment on t_visit_payment.t_visit_payment_id = t_visit_insurance_plan.t_visit_payment_id\n"
                    + "inner join t_visit on t_visit.t_visit_id = t_visit_payment.t_visit_id\n"
                    + "inner join t_patient on t_patient.patient_hn = t_visit.visit_hn\n"
                    + "group by \n"
                    + "t_insurance_claim_billing.t_insurance_claim_billing_id\n"
                    + ", t_patient.t_patient_id\n"
                    + ",t_insurance_claim_billing.total_vat) as receipt_patient on receipt_patient.t_patient_id = t_insurance_claim_receipt.t_patient_id\n"
                    + "and receipt_patient.t_insurance_claim_billing_id = t_insurance_claim_billing.t_insurance_claim_billing_id\n"
                    + "where t_insurance_claim_billing.payment_date between ? and ?\n"
                    + "order by payment_date desc, t_insurance_claim_receipt.receipt_number desc";
            PreparedStatement ps = theConnectionInf.getConnection().prepareStatement(sql);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(startDate);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            ps.setTimestamp(1, new java.sql.Timestamp(calendar.getTimeInMillis()));
            calendar.setTime(endDate);
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            ps.setTimestamp(2, new java.sql.Timestamp(calendar.getTimeInMillis()));

            List<Object[]> eComplexQuery = theConnectionInf.eComplexQuery(ps.toString());
            for (Object[] objects : eComplexQuery) {
                list.add(new ComplexDataSource(
                        String.valueOf(objects[0]),
                        new Object[]{
                            String.valueOf(objects[1]), String.valueOf(objects[2]), objects[3] == null ? "" : String.valueOf(objects[3]), objects[4] == null ? "" : String.valueOf(objects[4]),
                            Double.parseDouble(String.valueOf(objects[5])), Double.parseDouble(String.valueOf(objects[6])), Double.parseDouble(String.valueOf(objects[7])), objects[8] == null ? "" : String.valueOf(objects[8])
                        }));
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return list;
    }

    public CreditCardInfo getCreditCardInfoByCreditCardNo(String number) {
        CreditCardInfo cci = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            cci = theHosDB.theCreditCardInfoDB.selectByPatternNumber(number.length() > 6 ? number.substring(0, 6) : number);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return cci;
    }

    public String getCurrentBookNumberInfo() {
        String msg = "";
//        if(theHO.theVisit == null || theHO.theVisit.getObjectId() == null){
//            return msg;
//        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (theLookupControl.readOption().receipt_sequance.equals(Active.isEnable())) {
                InetAddress thisIp = InetAddress.getLocalHost();
                ReceiptSequance sd = theHosDB.theReceiptSequanceDB.selectByIP(thisIp.getHostAddress());
                if (sd == null) {
                    return " ������Է����͡����� ���ͧ�ҡ IP Address ���ç�Ѻ����駤����� ";
                }
                // ���Ţ��� �������
                int bookNumber = Integer.parseInt(sd.book_no == null || sd.book_no.isEmpty() ? "0" : sd.book_no);
                int minNumber = Integer.parseInt(sd.begin_no == null || sd.begin_no.isEmpty() ? "0" : sd.begin_no);
                int maxNumber = Integer.parseInt(sd.end_no == null || sd.end_no.isEmpty() ? "0" : sd.end_no);
                ReceiptBookSeq bookSeq = theHosDB.theReceiptBookSeqDB.selectByBookNumber(bookNumber);
                if (bookSeq == null) {
                    bookSeq = new ReceiptBookSeq();
                    bookSeq.book_number = bookNumber;
                    bookSeq.current_seq = theHosDB.theReceiptDB.findLastNumberFromBookNo(String.valueOf(bookNumber));
                    theHosDB.theReceiptBookSeqDB.insert(bookSeq);
                }
                int lastNumber = bookSeq.current_seq;
                int nextNumber = lastNumber == 0 ? minNumber : lastNumber + 1;
                if (maxNumber == lastNumber || !(minNumber <= nextNumber && nextNumber <= maxNumber)) {
                    return " ����稷���駤������������ ������� ��سҵ�駤������������� ��С�͹���Թ��þ���� ";
                }
                msg = " ������� %s �Ţ������������ش %s �ѹ����͡ %s �Ţ�������稶Ѵ� %s ";
                msg = String.format(msg, sd.book_no,
                        bookSeq.current_seq,
                        DateTimeUtil.getString(bookSeq.update_ts, "dd/MM/yyyy", DateTimeUtil.LOCALE_TH),
                        nextNumber);
            }
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }

        return msg == null ? "" : msg;
    }

    public Map<String, Object> getBillingForEDC(String visitId) {
        Map<String, Object> hm = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String sql = "select \n"
                    + "t_visit.visit_hn as hn\n"
                    + ",sum(t_billing_invoice.billing_invoice_total)::decimal(12,2)::text as amount\n"
                    + ",t_visit.t_visit_id\n"
                    + ",t_visit_payment.t_visit_payment_id\n"
                    + ",t_visit_govoffical_plan.t_visit_govoffical_plan_id\n"
                    + ",t_visit_govoffical_plan.govoffical_type\n"
                    + ",t_visit_govoffical_plan.govoffical_number\n"
                    + ",t_visit_govoffical_plan.invoice_number\n"
                    + "from t_visit\n"
                    + "inner join t_billing_invoice on t_visit.t_visit_id = t_billing_invoice.t_visit_id\n"
                    + "inner join t_visit_payment on (t_billing_invoice.t_payment_id = t_visit_payment.t_visit_payment_id\n"
                    + "    and t_visit_payment.visit_payment_active = '1'\n"
                    + "       and t_visit_payment.b_contract_plans_id \n"
                    + "        in (select b_map_contract_plans_govoffical.b_contract_plans_id\n"
                    + "                 from b_map_contract_plans_govoffical))\n"
                    + "left join t_visit_govoffical_plan on t_visit_govoffical_plan.t_visit_payment_id = t_visit_payment.t_visit_payment_id\n"
                    + "    and t_visit_govoffical_plan.active = '1'\n"
                    + "where t_billing_invoice.billing_invoice_active = '1'\n"
                    + "and t_visit.t_visit_id = ?\n"
                    + "group by\n"
                    + "t_visit.visit_hn\n"
                    + ",t_visit.t_visit_id\n"
                    + ",t_visit_payment.t_visit_payment_id\n"
                    + ",t_visit_govoffical_plan.t_visit_govoffical_plan_id";
            PreparedStatement ps = theConnectionInf.getConnection().prepareStatement(sql);
            ps.setString(1, visitId);

            List<Map<String, Object>> results = theConnectionInf.eComplexQueryWithColumn(ps.toString());
            hm = results.isEmpty() ? null : results.get(0);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return hm;
    }

    public boolean saveApproveCode(VisitGovOfficalPlan vgop) {
        if (vgop == null) {
            return false;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vgop.user_update_id = theHO.theEmployee.getObjectId();
            if (vgop.getObjectId() == null) {
                vgop.user_record_id = theHO.theEmployee.getObjectId();
                theHosDB.theVisitGovOfficalPlanDB.insert(vgop);
            } else {
                if (vgop.approve_status.equals("A")) {
                    theHosDB.theVisitGovOfficalPlanDB.updateApproveCode(vgop);
                } else {
                    theHosDB.theVisitGovOfficalPlanDB.voidApproveCode(vgop);
                }
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            theUS.setStatus("��úѹ�֡�Դ��Ҵ", UpdateStatus.ERROR);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theUS.setStatus("��úѹ�֡�������", UpdateStatus.COMPLETE);
            theHS.theVPaymentSubject.notifySaveVPayment(com.hosv3.utility.ResourceBundle.getBundleText("��úѹ�֡�Է�ԡ���ѡ���������"), UpdateStatus.COMPLETE);
        }
        return isComplete;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.ItemMapDoctorFee;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.Gutil;
import java.sql.ResultSet;
import java.util.List;
import java.util.Vector;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class ItemMapDoctorFeeDB {

    private final ConnectionInf theConnectionInf;
    private final String tableId = "813"; 

    public ItemMapDoctorFeeDB(ConnectionInf db) {
        theConnectionInf = db;
    }
    
    public int insert(ItemMapDoctorFee obj) throws Exception {
        String sql = "INSERT INTO b_map_item_df( "
                + "b_map_item_df_id, b_item_id) "
                + "VALUES ('%s', '%s')";
        sql = String.format(sql, obj.getGenID(tableId), obj.b_item_id);
        return theConnectionInf.eUpdate(sql);
    }

    public int delete(ItemMapDoctorFee itemMapDoctorFee) throws Exception {
        String sql = "delete from b_map_item_df where b_map_item_df_id = '%s'";
        return theConnectionInf.eUpdate(String.format(sql, itemMapDoctorFee.getObjectId()));
    }

    public ItemMapDoctorFee selectById(String id) throws Exception {
        String sql = "select * from b_map_item_df where b_map_item_df_id = '%s'";
        Vector eQuery = eQuery(String.format(sql, id));
        return (ItemMapDoctorFee) (eQuery.isEmpty() ? null : eQuery.get(0));
    }
    
    public ItemMapDoctorFee selectByItemId(String id) throws Exception {
        String sql = "select * from b_map_item_df where b_item_id = '%s'";
        Vector eQuery = eQuery(String.format(sql, id));
        return (ItemMapDoctorFee) (eQuery.isEmpty() ? null : eQuery.get(0));
    }

    public Vector<ItemMapDoctorFee> listAll() throws Exception {
        String sql = "select * from b_map_item_df";
        Vector eQuery = eQuery(sql);
        return eQuery;
    }

    public Vector eQuery(String sql) throws Exception {
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        while (rs.next()) {
            ItemMapDoctorFee p = new ItemMapDoctorFee();
            p.setObjectId(rs.getString("b_map_item_df_id"));
            p.b_item_id = rs.getString("b_item_id");
            list.add(p);
        }
        rs.close();
        return list;
    }

    public List<Object[]> listByKeyword(String keyword) throws Exception {
        String sql = "select b_map_item_df.b_map_item_df_id, b_item.item_number, b_item.item_common_name from b_map_item_df "
                + "inner join b_item on b_map_item_df.b_item_id = b_item.b_item_id "
                + "where UPPER(b_item.item_common_name) like UPPER('%s') order by b_item.item_common_name";
        return theConnectionInf.eComplexQuery(String.format(sql, keyword == null || keyword.isEmpty() ? "%" : ("%"+Gutil.CheckReservedWords(keyword)+"%")));
    }

    public List<Object[]> listNotMapByKeyword(String keyword) throws Exception {
        String sql = "select b_item.b_item_id, b_item.item_number, b_item.item_common_name from b_item "
                //+ "inner join b_item_subgroup on b_item_subgroup.b_item_subgroup_id = b_item.b_item_subgroup_id and b_item_subgroup.f_item_group_id = '1' "
                + "where b_item.item_active = '1' "
                + "and b_item.b_item_id not in (select b_item_id from b_map_item_df) "
                + "and UPPER(b_item.item_common_name) like UPPER('%s') order by b_item.item_common_name";
        return theConnectionInf.eComplexQuery(String.format(sql, keyword == null || keyword.isEmpty() ? "%" : ("%"+Gutil.CheckReservedWords(keyword)+"%")));
    }
}

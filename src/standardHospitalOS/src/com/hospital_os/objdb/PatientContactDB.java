/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.PatientContact;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class PatientContactDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "802";

    public PatientContactDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public int insert(PatientContact obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO t_patient_contact(\n");
            sql.append("            t_patient_contact_id, t_patient_id, patient_contact_office_name, \n");
            sql.append("            patient_contact_office_tel, patient_contact_email, patient_contact_firstname, \n");
            sql.append("            patient_contact_lastname, f_patient_relation_id, patient_contact_sex_id, \n");
            sql.append("            patient_contact_house, patient_contact_moo, patient_contact_road, \n");
            sql.append("            patient_contact_tambon, patient_contact_amphur, patient_contact_changwat, \n");
            sql.append("            patient_contact_telephone, patient_contact_mobile, patient_contact_detail_etc, \n");
            sql.append("            active, user_record_id, record_date_time, user_modify_id, modify_date_time)\n");
            sql.append("    VALUES (?, ?, ?, \n");
            sql.append("            ?, ?, ?, \n");
            sql.append("            ?, ?, ?, \n");
            sql.append("            ?, ?, ?, \n");
            sql.append("            ?, ?, ?, \n");
            sql.append("            ?, ?, ?, \n");
            sql.append("            ?, ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getGenID(tableId));
            preparedStatement.setString(2, obj.t_patient_id);
            preparedStatement.setString(3, obj.patient_contact_office_name);
            preparedStatement.setString(4, obj.patient_contact_office_tel);
            preparedStatement.setString(5, obj.patient_contact_email);
            preparedStatement.setString(6, obj.patient_contact_firstname);
            preparedStatement.setString(7, obj.patient_contact_lastname);
            preparedStatement.setString(8, obj.f_patient_relation_id);
            preparedStatement.setString(9, obj.patient_contact_sex_id);
            preparedStatement.setString(10, obj.patient_contact_house);
            preparedStatement.setString(11, obj.patient_contact_moo);
            preparedStatement.setString(12, obj.patient_contact_road);
            preparedStatement.setString(13, obj.patient_contact_tambon);
            preparedStatement.setString(14, obj.patient_contact_amphur);
            preparedStatement.setString(15, obj.patient_contact_changwat);
            preparedStatement.setString(16, obj.patient_contact_telephone);
            preparedStatement.setString(17, obj.patient_contact_mobile);
            preparedStatement.setString(18, obj.patient_contact_detail_etc);
            preparedStatement.setString(19, obj.active);
            preparedStatement.setString(20, obj.user_record_id);
            preparedStatement.setString(21, obj.record_date_time);
            preparedStatement.setString(22, obj.user_modify_id);
            preparedStatement.setString(23, obj.modify_date_time);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(PatientContact obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_patient_contact\n");
            sql.append("   SET t_patient_id=?, patient_contact_office_name=?, \n");
            sql.append("       patient_contact_office_tel=?, patient_contact_email=?, patient_contact_firstname=?, \n");
            sql.append("       patient_contact_lastname=?, f_patient_relation_id=?, patient_contact_sex_id=?, \n");
            sql.append("       patient_contact_house=?, patient_contact_moo=?, patient_contact_road=?, \n");
            sql.append("       patient_contact_tambon=?, patient_contact_amphur=?, patient_contact_changwat=?, \n");
            sql.append("       patient_contact_telephone=?, patient_contact_mobile=?, patient_contact_detail_etc=?, \n");
            sql.append("       active=?, user_modify_id=?, \n");
            sql.append("       modify_date_time=?\n");
            sql.append(" WHERE t_patient_contact_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.t_patient_id);
            preparedStatement.setString(2, obj.patient_contact_office_name);
            preparedStatement.setString(3, obj.patient_contact_office_tel);
            preparedStatement.setString(4, obj.patient_contact_email);
            preparedStatement.setString(5, obj.patient_contact_firstname);
            preparedStatement.setString(6, obj.patient_contact_lastname);
            preparedStatement.setString(7, obj.f_patient_relation_id);
            preparedStatement.setString(8, obj.patient_contact_sex_id);
            preparedStatement.setString(9, obj.patient_contact_house);
            preparedStatement.setString(10, obj.patient_contact_moo);
            preparedStatement.setString(11, obj.patient_contact_road);
            preparedStatement.setString(12, obj.patient_contact_tambon);
            preparedStatement.setString(13, obj.patient_contact_amphur);
            preparedStatement.setString(14, obj.patient_contact_changwat);
            preparedStatement.setString(15, obj.patient_contact_telephone);
            preparedStatement.setString(16, obj.patient_contact_mobile);
            preparedStatement.setString(17, obj.patient_contact_detail_etc);
            preparedStatement.setString(18, obj.active);
            preparedStatement.setString(19, obj.user_modify_id);
            preparedStatement.setString(20, obj.modify_date_time);
            preparedStatement.setString(21, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int inactive(PatientContact obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_patient_contact\n");
            sql.append("   SET active=?, user_modify_id=?, modify_date_time=?\n");
            sql.append(" WHERE t_patient_contact_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, "0");
            preparedStatement.setString(2, obj.user_modify_id);
            preparedStatement.setString(3, obj.modify_date_time);
            preparedStatement.setString(4, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "delete from t_patient_contact where t_patient_contact_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            // execute delete SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public PatientContact select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from t_patient_contact where t_patient_contact_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<PatientContact> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<PatientContact> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<PatientContact> list = new ArrayList<PatientContact>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                PatientContact obj = new PatientContact();
                obj.setObjectId(rs.getString("t_patient_contact_id"));
                obj.t_patient_id = rs.getString("t_patient_id");
                obj.patient_contact_office_name = rs.getString("patient_contact_office_name");
                obj.patient_contact_office_tel = rs.getString("patient_contact_office_tel");
                obj.patient_contact_email = rs.getString("patient_contact_email");
                obj.patient_contact_firstname = rs.getString("patient_contact_firstname");
                obj.patient_contact_lastname = rs.getString("patient_contact_lastname");
                obj.f_patient_relation_id = rs.getString("f_patient_relation_id");
                obj.patient_contact_sex_id = rs.getString("patient_contact_sex_id");
                obj.patient_contact_house = rs.getString("patient_contact_house");
                obj.patient_contact_moo = rs.getString("patient_contact_moo");
                obj.patient_contact_road = rs.getString("patient_contact_road");
                obj.patient_contact_tambon = rs.getString("patient_contact_tambon");
                obj.patient_contact_amphur = rs.getString("patient_contact_amphur");
                obj.patient_contact_changwat = rs.getString("patient_contact_changwat");
                obj.patient_contact_telephone = rs.getString("patient_contact_telephone");
                obj.patient_contact_mobile = rs.getString("patient_contact_mobile");
                obj.patient_contact_detail_etc = rs.getString("patient_contact_detail_etc");
                obj.active = rs.getString("active");
                obj.user_record_id = rs.getString("user_record_id");
                obj.record_date_time = rs.getString("record_date_time");
                obj.user_modify_id = rs.getString("user_modify_id");
                obj.modify_date_time = rs.getString("modify_date_time");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }
}

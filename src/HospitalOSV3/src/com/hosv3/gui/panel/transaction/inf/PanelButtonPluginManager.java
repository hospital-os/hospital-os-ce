/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.gui.panel.transaction.inf;

import java.util.Iterator;
import java.util.ServiceLoader;
import java.util.logging.Logger;

/**
 *
 * @author Somprasong
 */
public class PanelButtonPluginManager {
    
    private static final Logger LOG = Logger.getLogger(PanelButtonPluginManager.class.getName());
    private static PanelButtonPluginManager service;
    private ServiceLoader<PanelButtonPluginProvider> loader;

    /**
     * Creates a new instance of PanelButtonPluginManager
     */
    private PanelButtonPluginManager() {
        loader = ServiceLoader.load(PanelButtonPluginProvider.class);
    }

    /**
     * Retrieve the singleton static instance of PanelButtonPluginManager.
     */
    public static PanelButtonPluginManager getInstance() {
        if (service == null) {
            service = new PanelButtonPluginManager();
        }
        return service;
    }

    public ServiceLoader<PanelButtonPluginProvider> getServiceLoader() {
        return loader;
    }

    public Iterator<PanelButtonPluginProvider> getServices() {
        return loader.iterator();
    }

    public boolean isEmpty() {
        Iterator<PanelButtonPluginProvider> iterator = getServices();
        return !iterator.hasNext();
    }
}

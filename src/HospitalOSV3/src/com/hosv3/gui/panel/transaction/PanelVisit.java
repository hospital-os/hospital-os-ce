/*
 * PanelVisit_1.java
 *
 * Created on 29 �ԧ�Ҥ� 2548, 9:51 �.
 */
package com.hosv3.gui.panel.transaction;

import com.hospital_os.object.*;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hospital_os.utility.CellRendererToolTipText;
import com.hospital_os.utility.CellRendererVisitPayment;
import com.hospital_os.utility.ComboboxModel;
import com.hospital_os.utility.ComplexDataSource;
import com.hospital_os.utility.DataSource;
import com.hospital_os.utility.Gutil;
import com.hospital_os.utility.TaBleModel;
import com.hospital_os.utility.TableModelComplexDataSource;
import com.hosv3.control.HosControl;
import com.hosv3.control.PrintControl;
import com.hosv3.gui.dialog.HosDialog;
import com.hosv3.gui.panel.transaction.inf.PanelButtonPluginManager;
import com.hosv3.gui.panel.transaction.inf.PanelButtonPluginProvider;
import com.hosv3.object.GActionAuthV;
import com.hosv3.object.HosObject;
import com.hosv3.usecase.transaction.ManagePatientResp;
import com.hosv3.usecase.transaction.ManageServiceLoader;
import com.hosv3.usecase.transaction.ManageVPaymentResp;
import com.hosv3.usecase.transaction.ManageVisitResp;
import com.hosv3.utility.DateUtil;
import com.hosv3.utility.GuiLang;
import com.hosv3.utility.search.DialogSearchData;
import com.pcu.object.Family;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.logging.Logger;
import javax.swing.JButton;

/**
 *
 * @author Administrator
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class PanelVisit extends javax.swing.JPanel implements ManageVisitResp,
        ManagePatientResp, ManageVPaymentResp, ManageServiceLoader {

    private static final Logger LOG = Logger.getLogger(PanelVisit.class.getName());
    private static final long serialVersionUID = 1L;
    private HosObject theHO;
    private HosControl theHC;
    public HosDialog theHD;
    private UpdateStatus theUS;
    private Visit theVisit;
    private Patient thePatient;
    private Family theFamily;
    private Payment thePaymentNow;
    private Vector thePlan;
    public Vector vPatientPayment;
    public Vector vVisitPayment;
    private boolean IOption = false;
    private final String[] column_jTableVisitPayment = {"�Ţ���ѵ�", "�Է��", "��ǹŴ"};
    private final String[] column_jTablePatientPayment = {"�Ţ���ѵ�", "�Է��", "�ѹ����Ѿഷ"};
    private final String[] column_jTablePlan = {"�Է�ԡ���ѡ��"};
    //��㹡���ʴ� tooltiptext �ͧ �Է�ԡ���ѡ��
    private final CellRendererToolTipText theCellRendererTooltip = new CellRendererToolTipText(true);
    private final CellRendererVisitPayment theCellRendererVisitPayment = new CellRendererVisitPayment(true);
    private Option theOption;
    private final TableModelComplexDataSource tmcds = new TableModelComplexDataSource(new String[]{
        "����ѷ",
        "Ἱ"
    });
    private final DialogSearchData dialogSearchInsurancePlan = new DialogSearchData(null, true);
    private final DialogSearchData dialogSearchGovCode = new DialogSearchData(null, true);

    /**
     * Creates new form PanelVisit_1
     */
    public PanelVisit() {
        initComponents();
        setLanguage(null);
        dateComboBoxFrom.setEditable(true);
        dateComboBoxTo.setEditable(true);
        dateComboBoxFrom.setText("");
        dateComboBoxTo.setText("");
        theCellRendererVisitPayment.setFont(jLabel2.getFont());
        jButtonUp2.setVisible(false);
        jButtonDown1.setVisible(false);
        rdVisitType1.setSelected(true);
        // �Է�Ի�Сѹ
        btnShowInsurancePlan.setVisible(false);
        panelInsurance.setVisible(false);
        // ��Ңͧ�Է�Ԣ���Ҫ���
        panelExtraGovPlan.setVisible(false);
        // ��Ңͧ�Է�Ի�Сѹ�ѧ��
        panelExtraSocialsecPlan.setVisible(false);
    }

    /*
     * ૵���Control��ҧ� �ӡ��ŧ����¹�Ѻ Subject ������㹡�� Notify
     *
     */
    public void setControl(HosControl hc, UpdateStatus us) {
        theHC = hc;
        theHO = hc.theHO;
        theUS = us;
        hc.theHS.thePatientSubject.attachManagePatient(this);
        hc.theHS.theVisitSubject.attachManageVisit(this);
        hc.theHS.theVPaymentSubject.attach(this);
        hc.theHS.theServiceLoaderSubject.attachManage(this);
        ////////////////////////////////////////////////////////////////
        //૵����������
        setAuthentication(theHO.theEmployee);
        initComboBox();
        initAuthen();
        jButtonSearchAllPlanActionPerformed(null);
        setEnabled(false);
        jCheckBoxVisitDate.setSelected(false);
        jCheckBoxVisitDateActionPerformed(null);
        dialogSearchInsurancePlan.setDatasource(theHC.theAllDialogDatasourceControl.getDatasourceInsurancePlan(), false, false);
        dialogSearchGovCode.setDatasource(theHC.theAllDialogDatasourceControl.getDatasourceGovCode(), false, true);
        ComboboxModel.initComboBox(cbSubinscl, theHC.theLookupControl.listSubInscls());
        ComboboxModel.initComboBox(cbRelinscl, theHC.theLookupControl.listRelInscls());
        ComboboxModel.initComboBox(jComboBoxTariff, theHC.theLookupControl.listTariff());
    }

    public void initAuthen() {
        GActionAuthV gaav = theHO.theGActionAuthV;
        jTablePlan.setEnabled(gaav.isReadTabVisitSaveNewPayment());
        jButtonDelete.setVisible(gaav.isReadTabVisitSaveNewPayment());
        jButtonSaveVisitPayment.setVisible(gaav.isReadTabVisitSaveNewPayment());
    }

    public void initComboBox() {
        theOption = theHC.theLookupControl.readOption(true);
        IOption = Gutil.isSelected(theHC.theLookupControl.readOption().inqueuevisit);
        thePlan = theHC.theLookupControl.listPlan();
        String command = theHC.theLookupControl.readOption().b1_command;
        String ttt = theHC.theLookupControl.readOption().b1_description;
        String icon = theHC.theLookupControl.readOption().b1_icon;
        this.jButtonB1.setVisible(!command.isEmpty());
        this.jButtonB1.setToolTipText(GuiLang.setLanguage(ttt));
        if (!icon.isEmpty()) {
            this.jButtonB1.setIcon(new javax.swing.ImageIcon("resources/image/" + icon));
            this.jButtonB1.setText("");
        }
    }

    public void setLanguage(String msg) {
        GuiLang.setLanguage(jButtonDelete);
        GuiLang.setLanguage(jButtonDown);
        GuiLang.setLanguage(jButtonDown1);
        GuiLang.setLanguage(jButtonHosMain);
        GuiLang.setLanguage(jButtonHosReferIn);
        GuiLang.setLanguage(jButtonHosSub);
        GuiLang.setLanguage(jButtonSaveVtPayment);
        GuiLang.setLanguage(jButtonUp);
        GuiLang.setLanguage(jButtonUp2);
        GuiLang.setLanguage(jCheckBoxShowCancel);
        GuiLang.setLanguage(jCheckBoxVisitDate);
        GuiLang.setLanguage(jLabel10);
        GuiLang.setLanguage(jPanel2);
        GuiLang.setLanguage(jPanel3);
        GuiLang.setLanguage(jPanel4);
        GuiLang.setLanguage(jPanelPlan);
        GuiLang.setLanguage(this.column_jTablePatientPayment);
        GuiLang.setLanguage(this.column_jTablePlan);
        GuiLang.setLanguage(this.column_jTableVisitPayment);
        GuiLang.setLanguage(this.jButtonDeletePp);
        GuiLang.setLanguage(this.jButtonPrintOPDCard);
        GuiLang.setLanguage(this.jButtonSavePtPayment);
        GuiLang.setLanguage(this.jButtonSaveVisitPayment);
        GuiLang.setLanguage(this.jButtonSearchAllPlan);
        GuiLang.setLanguage(this.jButtonVisit);
        GuiLang.setLanguage(this.jLabel1);
        GuiLang.setLanguage(this.jLabel2);
        GuiLang.setLanguage(this.jLabel3);
        GuiLang.setLanguage(this.jLabel4);
        GuiLang.setLanguage(this.jLabel41);
        //GuiLang.setLanguage(this.jLabel5);
        //GuiLang.setLanguage(this.jLabel6);
        GuiLang.setLanguage(this.jLabel8);
        GuiLang.setLanguage(this.jLabel9);
        GuiLang.setLanguage(this.jLabelMoneyLimit);
    }

    /**
     * ૵��� Hotdialog
     *
     * @param hd
     */
    public void setDialog(HosDialog hd) {
        theHD = hd;
    }

    public void setPlan(Plan p) {
        setPayment(theHO.initPayment(p));
    }

    public void jButtonUpActionPerformed() {
        jButtonUpActionPerformed(null);
    }

    public void jButtonVisitActionPerformed() {
        jButtonVisitActionPerformed(null);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        jPanel2 = new javax.swing.JPanel();
        jPanel11 = new javax.swing.JPanel();
        jTextFieldSearchPlan = new javax.swing.JTextField();
        jButtonSearchAllPlan = new javax.swing.JButton();
        jScrollPane11 = new javax.swing.JScrollPane();
        jTablePlan = new com.hosv3.gui.component.HJTableSort();
        jPanel4 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jLabel1 = new javax.swing.JLabel();
        jPanel14 = new javax.swing.JPanel();
        jButtonVisit = new javax.swing.JButton();
        jButtonPrintOPDCard = new javax.swing.JButton();
        jPanel17 = new javax.swing.JPanel();
        dateComboBoxCheck = new com.hospital_os.utility.DateComboBox();
        jLabel10 = new javax.swing.JLabel();
        timeTextFieldCheck = new com.hospital_os.utility.TimeTextField();
        jLabel11 = new javax.swing.JLabel();
        jCheckBoxVisitDate = new javax.swing.JCheckBox();
        panelButtonPlugin = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jButtonSavePtPayment = new javax.swing.JButton();
        jButtonDelete = new javax.swing.JButton();
        jButtonSaveVisitPayment = new javax.swing.JButton();
        btnShowInsurancePlan = new javax.swing.JToggleButton();
        jPanelPlan = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jTextFieldCardID = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabelMoneyLimit = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        dateComboBoxFrom = new com.hospital_os.utility.DateComboBox();
        dateComboBoxTo = new com.hospital_os.utility.DateComboBox();
        jPanel7 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        txtHosRefer = new javax.swing.JTextField();
        jButtonHosMain = new javax.swing.JButton();
        jButtonHosSub = new javax.swing.JButton();
        txtHosPrimary = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtHosReferCode = new javax.swing.JTextField();
        txtHosPrimaryCode = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        txtHosRegCode = new javax.swing.JTextField();
        txtHosReg = new javax.swing.JTextField();
        jButtonHosReg = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel41 = new javax.swing.JLabel();
        jLabelPlan = new javax.swing.JLabel();
        jLabel66 = new javax.swing.JLabel();
        jComboBoxTariff = new com.hosv3.gui.component.HosComboBox();
        panelExtraGovPlan = new javax.swing.JPanel();
        cbGovType = new javax.swing.JComboBox();
        txtGovNo = new javax.swing.JTextField();
        panelExtraGovPlanDetail = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        jPanel20 = new javax.swing.JPanel();
        txtGovCode = new javax.swing.JTextField();
        txtGovName = new javax.swing.JTextField();
        btnBrowseGov = new javax.swing.JButton();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        cbSubinscl = new javax.swing.JComboBox();
        cbRelinscl = new javax.swing.JComboBox();
        txtOwnname = new javax.swing.JTextField();
        txtOwnrPID = new javax.swing.JTextField();
        panelExtraSocialsecPlan = new javax.swing.JPanel();
        jLabel27 = new javax.swing.JLabel();
        txtSocialsecNo = new javax.swing.JTextField();
        jPanel6 = new javax.swing.JPanel();
        jScrollPane12 = new javax.swing.JScrollPane();
        jTableVisitPayment = new com.hosv3.gui.component.HJTableSort();
        jPanel16 = new javax.swing.JPanel();
        jButtonDown = new javax.swing.JButton();
        jButtonUp = new javax.swing.JButton();
        jCheckBoxShowCancel = new javax.swing.JCheckBox();
        jButtonB1 = new javax.swing.JButton();
        jPanel12 = new javax.swing.JPanel();
        rdVisitType1 = new javax.swing.JRadioButton();
        rdVisitType2 = new javax.swing.JRadioButton();
        rdVisitType3 = new javax.swing.JRadioButton();
        rdVisitType4 = new javax.swing.JRadioButton();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        panelReferHCODE = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        integerTextFieldHosRefer = new javax.swing.JTextField();
        jTextFieldHosRefer = new javax.swing.JTextField();
        jButtonHosReferIn = new javax.swing.JButton();
        rdVisitType5 = new javax.swing.JRadioButton();
        jLabel22 = new javax.swing.JLabel();
        jPanel13 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jRadioButtonIn = new javax.swing.JRadioButton();
        jRadioButtonOut = new javax.swing.JRadioButton();
        panelInsurance = new javax.swing.JPanel();
        btnAddInsurancePlan = new javax.swing.JButton();
        btnDelInsurancePlan = new javax.swing.JButton();
        btnDownInsurancePlan = new javax.swing.JButton();
        btnUpInsurancePlan = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        tableInsurancePlan = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTablePatientPayment = new com.hosv3.gui.component.HJTableSort();
        jPanel15 = new javax.swing.JPanel();
        jButtonDeletePp = new javax.swing.JButton();
        jButtonDown1 = new javax.swing.JButton();
        jButtonUp2 = new javax.swing.JButton();
        jButtonSaveVtPayment = new javax.swing.JButton();

        setLayout(new java.awt.GridBagLayout());

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("��¡���Է�ԡ���ѡ��"));
        jPanel2.setMinimumSize(new java.awt.Dimension(120, 240));
        jPanel2.setPreferredSize(new java.awt.Dimension(120, 240));
        jPanel2.setLayout(new java.awt.GridBagLayout());

        jPanel11.setLayout(new java.awt.GridBagLayout());

        jTextFieldSearchPlan.setFont(jTextFieldSearchPlan.getFont());
        jTextFieldSearchPlan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldSearchPlanActionPerformed(evt);
            }
        });
        jTextFieldSearchPlan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldSearchPlanKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 5);
        jPanel11.add(jTextFieldSearchPlan, gridBagConstraints);

        jButtonSearchAllPlan.setFont(jButtonSearchAllPlan.getFont());
        jButtonSearchAllPlan.setText("�Է�Է�����");
        jButtonSearchAllPlan.setMargin(new java.awt.Insets(1, 1, 1, 1));
        jButtonSearchAllPlan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSearchAllPlanActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel11.add(jButtonSearchAllPlan, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        jPanel2.add(jPanel11, gridBagConstraints);

        jTablePlan.setFillsViewportHeight(true);
        jTablePlan.setFont(jTablePlan.getFont());
        jTablePlan.setRowHeight(30);
        jTablePlan.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTablePlanMouseReleased(evt);
            }
        });
        jTablePlan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTablePlanKeyReleased(evt);
            }
        });
        jScrollPane11.setViewportView(jTablePlan);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 0, 0, 0);
        jPanel2.add(jScrollPane11, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 0.7;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 0);
        add(jPanel2, gridBagConstraints);

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("�����š������Ѻ��ԡ��"));
        jPanel4.setMinimumSize(new java.awt.Dimension(420, 287));
        jPanel4.setPreferredSize(new java.awt.Dimension(420, 287));
        jPanel4.setLayout(new java.awt.GridBagLayout());

        jPanel9.setMinimumSize(new java.awt.Dimension(213, 100));
        jPanel9.setPreferredSize(new java.awt.Dimension(213, 100));
        jPanel9.setLayout(new java.awt.GridBagLayout());

        jScrollPane2.setMinimumSize(new java.awt.Dimension(22, 38));
        jScrollPane2.setPreferredSize(new java.awt.Dimension(22, 38));

        jTextArea1.setFont(jTextArea1.getFont());
        jTextArea1.setLineWrap(true);
        jTextArea1.setWrapStyleWord(true);
        jTextArea1.setMinimumSize(new java.awt.Dimension(100, 50));
        jTextArea1.setPreferredSize(new java.awt.Dimension(100, 10));
        jScrollPane2.setViewportView(jTextArea1);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 0, 0);
        jPanel9.add(jScrollPane2, gridBagConstraints);

        jLabel1.setFont(jLabel1.getFont().deriveFont(jLabel1.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel1.setText("�����˵�");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 0, 0, 0);
        jPanel9.add(jLabel1, gridBagConstraints);

        jPanel14.setLayout(new java.awt.GridBagLayout());

        jButtonVisit.setFont(jButtonVisit.getFont());
        jButtonVisit.setText("Visit");
        jButtonVisit.setMargin(new java.awt.Insets(2, 7, 2, 7));
        jButtonVisit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonVisitActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        jPanel14.add(jButtonVisit, gridBagConstraints);

        jButtonPrintOPDCard.setFont(jButtonPrintOPDCard.getFont());
        jButtonPrintOPDCard.setText("OPD Card");
        jButtonPrintOPDCard.setMargin(new java.awt.Insets(2, 7, 2, 7));
        jButtonPrintOPDCard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPrintOPDCardActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        jPanel14.add(jButtonPrintOPDCard, gridBagConstraints);

        jPanel17.setLayout(new java.awt.GridBagLayout());

        dateComboBoxCheck.setEnabled(false);
        dateComboBoxCheck.setFont(dateComboBoxCheck.getFont());
        dateComboBoxCheck.setMinimumSize(new java.awt.Dimension(100, 25));
        dateComboBoxCheck.setPreferredSize(new java.awt.Dimension(100, 25));
        dateComboBoxCheck.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                dateComboBoxCheckKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 4, 0, 3);
        jPanel17.add(dateComboBoxCheck, gridBagConstraints);

        jLabel10.setFont(jLabel10.getFont());
        jLabel10.setText("�.");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 1, 0, 1);
        jPanel17.add(jLabel10, gridBagConstraints);

        timeTextFieldCheck.setEnabled(false);
        timeTextFieldCheck.setFont(timeTextFieldCheck.getFont());
        timeTextFieldCheck.setMaximumSize(new java.awt.Dimension(46, 21));
        timeTextFieldCheck.setMinimumSize(new java.awt.Dimension(46, 21));
        timeTextFieldCheck.setPreferredSize(new java.awt.Dimension(46, 21));
        timeTextFieldCheck.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                timeTextFieldCheckMouseClicked(evt);
            }
        });
        timeTextFieldCheck.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                timeTextFieldCheckKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 1, 0, 3);
        jPanel17.add(timeTextFieldCheck, gridBagConstraints);

        jLabel11.setFont(jLabel11.getFont());
        jLabel11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/clock.gif"))); // NOI18N
        jLabel11.setToolTipText("���ҷ���Ǩ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 1, 0, 3);
        jPanel17.add(jLabel11, gridBagConstraints);

        jCheckBoxVisitDate.setFont(jCheckBoxVisitDate.getFont().deriveFont(jCheckBoxVisitDate.getFont().getStyle() | java.awt.Font.BOLD));
        jCheckBoxVisitDate.setText("�ѹ���");
        jCheckBoxVisitDate.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        jCheckBoxVisitDate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxVisitDateActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel17.add(jCheckBoxVisitDate, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel14.add(jPanel17, gridBagConstraints);

        panelButtonPlugin.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        gridBagConstraints.weightx = 1.0;
        jPanel14.add(panelButtonPlugin, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 0, 0, 0);
        jPanel9.add(jPanel14, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel4.add(jPanel9, gridBagConstraints);

        jPanel1.setLayout(new java.awt.GridBagLayout());

        jButtonSavePtPayment.setFont(jButtonSavePtPayment.getFont());
        jButtonSavePtPayment.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/Move.gif"))); // NOI18N
        jButtonSavePtPayment.setToolTipText("�������Է�Ի�Шӵ�Ǽ�����");
        jButtonSavePtPayment.setMaximumSize(new java.awt.Dimension(26, 26));
        jButtonSavePtPayment.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonSavePtPayment.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonSavePtPayment.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSavePtPaymentActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel1.add(jButtonSavePtPayment, gridBagConstraints);

        jButtonDelete.setFont(jButtonDelete.getFont());
        jButtonDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/minus16.gif"))); // NOI18N
        jButtonDelete.setToolTipText("ź�Է������ѡ��");
        jButtonDelete.setMaximumSize(new java.awt.Dimension(26, 26));
        jButtonDelete.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonDelete.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDeleteActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel1.add(jButtonDelete, gridBagConstraints);

        jButtonSaveVisitPayment.setFont(jButtonSaveVisitPayment.getFont());
        jButtonSaveVisitPayment.setText("�ѹ�֡");
        jButtonSaveVisitPayment.setToolTipText("�ѹ�֡�Է������ѡ��\n");
        jButtonSaveVisitPayment.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSaveVisitPaymentActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel1.add(jButtonSaveVisitPayment, gridBagConstraints);

        btnShowInsurancePlan.setText("�к�Ἱ��Сѹ");
        btnShowInsurancePlan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnShowInsurancePlanActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel1.add(btnShowInsurancePlan, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel4.add(jPanel1, gridBagConstraints);

        jPanelPlan.setBackground(java.awt.SystemColor.window);
        jPanelPlan.setMinimumSize(new java.awt.Dimension(332, 180));
        jPanelPlan.setPreferredSize(new java.awt.Dimension(332, 180));
        jPanelPlan.setLayout(new java.awt.GridBagLayout());

        jPanel8.setBackground(java.awt.SystemColor.window);
        jPanel8.setLayout(new java.awt.GridBagLayout());

        jLabel3.setFont(jLabel3.getFont());
        jLabel3.setText("ǧ�Թ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel8.add(jLabel3, gridBagConstraints);

        jTextFieldCardID.setFont(jTextFieldCardID.getFont());
        jTextFieldCardID.setMinimumSize(new java.awt.Dimension(185, 21));
        jTextFieldCardID.setPreferredSize(new java.awt.Dimension(185, 21));
        jTextFieldCardID.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextFieldCardIDFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel8.add(jTextFieldCardID, gridBagConstraints);

        jLabel9.setFont(jLabel9.getFont());
        jLabel9.setText("�ҷ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel8.add(jLabel9, gridBagConstraints);

        jLabelMoneyLimit.setFont(jLabelMoneyLimit.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel8.add(jLabelMoneyLimit, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        jPanelPlan.add(jPanel8, gridBagConstraints);

        jPanel10.setBackground(java.awt.SystemColor.window);
        jPanel10.setLayout(new java.awt.GridBagLayout());

        jLabel4.setFont(jLabel4.getFont());
        jLabel4.setText("�ѹ����������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel10.add(jLabel4, gridBagConstraints);

        dateComboBoxFrom.setFont(dateComboBoxFrom.getFont());
        dateComboBoxFrom.setMinimumSize(new java.awt.Dimension(90, 23));
        dateComboBoxFrom.setPreferredSize(new java.awt.Dimension(90, 23));
        dateComboBoxFrom.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dateComboBoxFromActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 15;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel10.add(dateComboBoxFrom, gridBagConstraints);

        dateComboBoxTo.setFont(dateComboBoxTo.getFont());
        dateComboBoxTo.setMinimumSize(new java.awt.Dimension(90, 23));
        dateComboBoxTo.setPreferredSize(new java.awt.Dimension(90, 23));
        dateComboBoxTo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dateComboBoxToActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 15;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel10.add(dateComboBoxTo, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanelPlan.add(jPanel10, gridBagConstraints);

        jPanel7.setBackground(java.awt.SystemColor.window);
        jPanel7.setMinimumSize(new java.awt.Dimension(226, 69));
        jPanel7.setOpaque(false);
        jPanel7.setPreferredSize(new java.awt.Dimension(226, 69));
        jPanel7.setRequestFocusEnabled(false);
        jPanel7.setLayout(new java.awt.GridBagLayout());

        jLabel5.setFont(jLabel5.getFont().deriveFont(jLabel5.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel5.setText("ʶҹ��Һ�Ż������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jLabel5, gridBagConstraints);

        txtHosRefer.setEditable(false);
        txtHosRefer.setFont(txtHosRefer.getFont());
        txtHosRefer.setBorder(null);
        txtHosRefer.setMinimumSize(new java.awt.Dimension(4, 21));
        txtHosRefer.setPreferredSize(new java.awt.Dimension(4, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(txtHosRefer, gridBagConstraints);

        jButtonHosMain.setFont(jButtonHosMain.getFont());
        jButtonHosMain.setText("...");
        jButtonHosMain.setToolTipText("ʶҹ��Һ����ѡ");
        jButtonHosMain.setBorder(null);
        jButtonHosMain.setMaximumSize(new java.awt.Dimension(22, 22));
        jButtonHosMain.setMinimumSize(new java.awt.Dimension(22, 22));
        jButtonHosMain.setPreferredSize(new java.awt.Dimension(22, 22));
        jButtonHosMain.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonHosMainActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jButtonHosMain, gridBagConstraints);

        jButtonHosSub.setFont(jButtonHosSub.getFont());
        jButtonHosSub.setText("...");
        jButtonHosSub.setToolTipText("ʶҹ��Һ���ͧ");
        jButtonHosSub.setBorder(null);
        jButtonHosSub.setMaximumSize(new java.awt.Dimension(22, 22));
        jButtonHosSub.setMinimumSize(new java.awt.Dimension(22, 22));
        jButtonHosSub.setPreferredSize(new java.awt.Dimension(22, 22));
        jButtonHosSub.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonHosSubActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jButtonHosSub, gridBagConstraints);

        txtHosPrimary.setEditable(false);
        txtHosPrimary.setFont(txtHosPrimary.getFont());
        txtHosPrimary.setBorder(null);
        txtHosPrimary.setMinimumSize(new java.awt.Dimension(4, 21));
        txtHosPrimary.setPreferredSize(new java.awt.Dimension(4, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(txtHosPrimary, gridBagConstraints);

        jLabel6.setFont(jLabel6.getFont().deriveFont(jLabel6.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel6.setText("ʶҹ��Һ�ŷ���Ѻ����觵��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jLabel6, gridBagConstraints);

        txtHosReferCode.setBackground(new java.awt.Color(204, 255, 255));
        txtHosReferCode.setDocument(new sd.comp.textfield.JTextFieldLimit(5));
        txtHosReferCode.setFont(txtHosReferCode.getFont());
        txtHosReferCode.setMaximumSize(new java.awt.Dimension(50, 21));
        txtHosReferCode.setMinimumSize(new java.awt.Dimension(50, 21));
        txtHosReferCode.setPreferredSize(new java.awt.Dimension(50, 21));
        txtHosReferCode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtHosReferCodeActionPerformed(evt);
            }
        });
        txtHosReferCode.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtHosReferCodeFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtHosReferCodeFocusLost(evt);
            }
        });
        txtHosReferCode.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtHosReferCodeKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(txtHosReferCode, gridBagConstraints);

        txtHosPrimaryCode.setDocument(new sd.comp.textfield.JTextFieldLimit(5));
        txtHosPrimaryCode.setFont(txtHosPrimaryCode.getFont());
        txtHosPrimaryCode.setMaximumSize(new java.awt.Dimension(50, 21));
        txtHosPrimaryCode.setMinimumSize(new java.awt.Dimension(50, 21));
        txtHosPrimaryCode.setPreferredSize(new java.awt.Dimension(50, 21));
        txtHosPrimaryCode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtHosPrimaryCodeActionPerformed(evt);
            }
        });
        txtHosPrimaryCode.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtHosPrimaryCodeFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtHosPrimaryCodeFocusLost(evt);
            }
        });
        txtHosPrimaryCode.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtHosPrimaryCodeKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(txtHosPrimaryCode, gridBagConstraints);

        jLabel16.setFont(jLabel16.getFont().deriveFont(jLabel16.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel16.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel16.setText("ʶҹ��Һ�Ż�Ш�");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jLabel16, gridBagConstraints);

        txtHosRegCode.setDocument(new sd.comp.textfield.JTextFieldLimit(5));
        txtHosRegCode.setFont(txtHosRegCode.getFont());
        txtHosRegCode.setMaximumSize(new java.awt.Dimension(50, 21));
        txtHosRegCode.setMinimumSize(new java.awt.Dimension(50, 21));
        txtHosRegCode.setPreferredSize(new java.awt.Dimension(50, 21));
        txtHosRegCode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtHosRegCodeActionPerformed(evt);
            }
        });
        txtHosRegCode.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtHosRegCodeFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtHosRegCodeFocusLost(evt);
            }
        });
        txtHosRegCode.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtHosRegCodeKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(txtHosRegCode, gridBagConstraints);

        txtHosReg.setEditable(false);
        txtHosReg.setFont(txtHosReg.getFont());
        txtHosReg.setBorder(null);
        txtHosReg.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        txtHosReg.setMinimumSize(new java.awt.Dimension(4, 21));
        txtHosReg.setPreferredSize(new java.awt.Dimension(4, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(txtHosReg, gridBagConstraints);

        jButtonHosReg.setFont(jButtonHosReg.getFont());
        jButtonHosReg.setText("...");
        jButtonHosReg.setToolTipText("ʶҹ��Һ�Ż�Ш�");
        jButtonHosReg.setBorder(null);
        jButtonHosReg.setMaximumSize(new java.awt.Dimension(22, 22));
        jButtonHosReg.setMinimumSize(new java.awt.Dimension(22, 22));
        jButtonHosReg.setPreferredSize(new java.awt.Dimension(22, 22));
        jButtonHosReg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonHosRegActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jButtonHosReg, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanelPlan.add(jPanel7, gridBagConstraints);

        jLabel2.setFont(jLabel2.getFont().deriveFont(jLabel2.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel2.setText("�Ţ���ѵ�");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelPlan.add(jLabel2, gridBagConstraints);

        jLabel41.setFont(jLabel41.getFont().deriveFont(jLabel41.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel41.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel41.setText("�ѹ����͡�ѵ�");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelPlan.add(jLabel41, gridBagConstraints);

        jLabelPlan.setFont(jLabelPlan.getFont().deriveFont(jLabelPlan.getFont().getStyle() | java.awt.Font.BOLD));
        jLabelPlan.setText("�Է�ԡ���Ѻ��ԡ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelPlan.add(jLabelPlan, gridBagConstraints);

        jLabel66.setFont(jLabel66.getFont().deriveFont(jLabel66.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel66.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel66.setText("�ѵ���Ҥ�");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelPlan.add(jLabel66, gridBagConstraints);

        jComboBoxTariff.setEnabled(false);
        jComboBoxTariff.setFont(jComboBoxTariff.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelPlan.add(jComboBoxTariff, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel4.add(jPanelPlan, gridBagConstraints);

        panelExtraGovPlan.setBorder(javax.swing.BorderFactory.createTitledBorder("��������Ңͧ�Է�Ԣ���Ҫ���/ͻ�"));
        panelExtraGovPlan.setLayout(new java.awt.GridBagLayout());

        cbGovType.setFont(cbGovType.getFont());
        cbGovType.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "����к�", "�Ţ͹��ѵ�", "�Ţ˹ѧ���" }));
        cbGovType.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbGovTypeActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraGovPlan.add(cbGovType, gridBagConstraints);

        txtGovNo.setFont(txtGovNo.getFont());
        txtGovNo.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtGovNoFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraGovPlan.add(txtGovNo, gridBagConstraints);

        panelExtraGovPlanDetail.setLayout(new java.awt.GridBagLayout());

        jLabel17.setFont(jLabel17.getFont());
        jLabel17.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel17.setText("���ѧ�Ѵ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraGovPlanDetail.add(jLabel17, gridBagConstraints);

        jPanel20.setLayout(new java.awt.GridBagLayout());

        txtGovCode.setDocument(new sd.comp.textfield.NumberDocument(5));
        txtGovCode.setFont(txtGovCode.getFont());
        txtGovCode.setMaximumSize(new java.awt.Dimension(50, 21));
        txtGovCode.setMinimumSize(new java.awt.Dimension(50, 21));
        txtGovCode.setPreferredSize(new java.awt.Dimension(50, 21));
        txtGovCode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtGovCodeActionPerformed(evt);
            }
        });
        txtGovCode.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtGovCodeFocusGained(evt);
            }
        });
        txtGovCode.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtGovCodeKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel20.add(txtGovCode, gridBagConstraints);

        txtGovName.setEditable(false);
        txtGovName.setFont(txtGovName.getFont());
        txtGovName.setMinimumSize(new java.awt.Dimension(4, 21));
        txtGovName.setPreferredSize(new java.awt.Dimension(4, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 0);
        jPanel20.add(txtGovName, gridBagConstraints);

        btnBrowseGov.setFont(btnBrowseGov.getFont());
        btnBrowseGov.setText("...");
        btnBrowseGov.setToolTipText("ʶҹ��Һ���ͧ");
        btnBrowseGov.setBorder(null);
        btnBrowseGov.setMaximumSize(new java.awt.Dimension(22, 22));
        btnBrowseGov.setMinimumSize(new java.awt.Dimension(22, 22));
        btnBrowseGov.setPreferredSize(new java.awt.Dimension(22, 22));
        btnBrowseGov.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBrowseGovActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 0);
        jPanel20.add(btnBrowseGov, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraGovPlanDetail.add(jPanel20, gridBagConstraints);

        jLabel18.setFont(jLabel18.getFont());
        jLabel18.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel18.setText("���� ���ʡ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraGovPlanDetail.add(jLabel18, gridBagConstraints);

        jLabel19.setFont(jLabel19.getFont());
        jLabel19.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel19.setText("�Ţ��Шӵ�ǻ�ЪҪ�");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraGovPlanDetail.add(jLabel19, gridBagConstraints);

        jLabel20.setFont(jLabel20.getFont());
        jLabel20.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel20.setText("�������Է��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraGovPlanDetail.add(jLabel20, gridBagConstraints);

        jLabel21.setFont(jLabel21.getFont());
        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel21.setText("��������ѹ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraGovPlanDetail.add(jLabel21, gridBagConstraints);

        cbSubinscl.setFont(cbSubinscl.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraGovPlanDetail.add(cbSubinscl, gridBagConstraints);

        cbRelinscl.setFont(cbRelinscl.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraGovPlanDetail.add(cbRelinscl, gridBagConstraints);

        txtOwnname.setFont(txtOwnname.getFont());
        txtOwnname.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtOwnnameFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraGovPlanDetail.add(txtOwnname, gridBagConstraints);

        txtOwnrPID.setFont(txtOwnrPID.getFont());
        txtOwnrPID.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtOwnrPIDFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraGovPlanDetail.add(txtOwnrPID, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        panelExtraGovPlan.add(panelExtraGovPlanDetail, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel4.add(panelExtraGovPlan, gridBagConstraints);

        panelExtraSocialsecPlan.setBorder(javax.swing.BorderFactory.createTitledBorder("��������Ңͧ�Է�Ի�Сѹ�ѧ��"));
        panelExtraSocialsecPlan.setLayout(new java.awt.GridBagLayout());

        jLabel27.setText("�Ţ͹��ѵ�");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraSocialsecPlan.add(jLabel27, gridBagConstraints);

        txtSocialsecNo.setFont(txtSocialsecNo.getFont());
        txtSocialsecNo.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtSocialsecNoFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelExtraSocialsecPlan.add(txtSocialsecNo, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel4.add(panelExtraSocialsecPlan, gridBagConstraints);

        jPanel6.setMinimumSize(new java.awt.Dimension(48, 110));
        jPanel6.setPreferredSize(new java.awt.Dimension(483, 110));
        jPanel6.setLayout(new java.awt.GridBagLayout());

        jTableVisitPayment.setFillsViewportHeight(true);
        jTableVisitPayment.setFont(jTableVisitPayment.getFont());
        jTableVisitPayment.setRowHeight(30);
        jTableVisitPayment.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTableVisitPaymentMouseReleased(evt);
            }
        });
        jTableVisitPayment.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTableVisitPaymentKeyReleased(evt);
            }
        });
        jScrollPane12.setViewportView(jTableVisitPayment);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jScrollPane12, gridBagConstraints);

        jPanel16.setLayout(new java.awt.GridBagLayout());

        jButtonDown.setFont(jButtonDown.getFont());
        jButtonDown.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/down.gif"))); // NOI18N
        jButtonDown.setToolTipText("����͹ŧ\n");
        jButtonDown.setEnabled(false);
        jButtonDown.setMaximumSize(new java.awt.Dimension(26, 26));
        jButtonDown.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonDown.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonDown.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDownActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 2, 0);
        jPanel16.add(jButtonDown, gridBagConstraints);

        jButtonUp.setFont(jButtonUp.getFont());
        jButtonUp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/up.gif"))); // NOI18N
        jButtonUp.setToolTipText("����͹���\n");
        jButtonUp.setEnabled(false);
        jButtonUp.setMaximumSize(new java.awt.Dimension(26, 26));
        jButtonUp.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonUp.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonUp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonUpActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 2, 0);
        jPanel16.add(jButtonUp, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jPanel16, gridBagConstraints);

        jCheckBoxShowCancel.setFont(jCheckBoxShowCancel.getFont());
        jCheckBoxShowCancel.setText("�ʴ��Է�Է��١¡��ԡ");
        jCheckBoxShowCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxShowCancelActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jCheckBoxShowCancel, gridBagConstraints);

        jButtonB1.setFont(jButtonB1.getFont());
        jButtonB1.setText("B1");
        jButtonB1.setToolTipText("");
        jButtonB1.setMaximumSize(new java.awt.Dimension(26, 26));
        jButtonB1.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonB1.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonB1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonB1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jButtonB1, gridBagConstraints);

        jPanel12.setBorder(javax.swing.BorderFactory.createTitledBorder("������������Ѻ��ԡ��"));
        jPanel12.setLayout(new java.awt.GridBagLayout());

        buttonGroup2.add(rdVisitType1);
        rdVisitType1.setFont(rdVisitType1.getFont());
        rdVisitType1.setSelected(true);
        rdVisitType1.setToolTipText("���Ѻ��ԡ���ͧ"); // NOI18N
        rdVisitType1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rdVisitType1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdVisitType1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel12.add(rdVisitType1, gridBagConstraints);

        buttonGroup2.add(rdVisitType2);
        rdVisitType2.setFont(rdVisitType2.getFont());
        rdVisitType2.setToolTipText("���Ѻ��ԡ�õ���Ѵ����"); // NOI18N
        rdVisitType2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rdVisitType2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdVisitType2ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel12.add(rdVisitType2, gridBagConstraints);

        buttonGroup2.add(rdVisitType3);
        rdVisitType3.setFont(rdVisitType3.getFont());
        rdVisitType3.setToolTipText("���Ѻ����觵�ͨҡʶҹ��Һ�����"); // NOI18N
        rdVisitType3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rdVisitType3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdVisitType3ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel12.add(rdVisitType3, gridBagConstraints);

        buttonGroup2.add(rdVisitType4);
        rdVisitType4.setFont(rdVisitType4.getFont());
        rdVisitType4.setToolTipText("���Ѻ����觵�Ǩҡ��ԡ�� EMS"); // NOI18N
        rdVisitType4.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rdVisitType4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdVisitType4ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel12.add(rdVisitType4, gridBagConstraints);

        jLabel12.setFont(jLabel12.getFont());
        jLabel12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/visit_type_02.png"))); // NOI18N
        jLabel12.setToolTipText("���Ѻ��ԡ�õ���Ѵ����"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel12.add(jLabel12, gridBagConstraints);

        jLabel13.setFont(jLabel13.getFont());
        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/visit_type_03.png"))); // NOI18N
        jLabel13.setToolTipText("���Ѻ����觵�ͨҡʶҹ��Һ�����"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel12.add(jLabel13, gridBagConstraints);

        jLabel14.setFont(jLabel14.getFont());
        jLabel14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/visit_type_04.png"))); // NOI18N
        jLabel14.setToolTipText("���Ѻ����觵�Ǩҡ��ԡ�� EMS"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel12.add(jLabel14, gridBagConstraints);

        jLabel15.setFont(jLabel15.getFont());
        jLabel15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/visit_type_01.png"))); // NOI18N
        jLabel15.setToolTipText(" ���Ѻ��ԡ���ͧ"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel12.add(jLabel15, gridBagConstraints);

        panelReferHCODE.setLayout(new java.awt.GridBagLayout());

        jLabel8.setFont(jLabel8.getFont().deriveFont(jLabel8.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel8.setText("�Ѻ�Ҩҡ þ.");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelReferHCODE.add(jLabel8, gridBagConstraints);

        integerTextFieldHosRefer.setBackground(new java.awt.Color(204, 255, 255));
        integerTextFieldHosRefer.setFont(integerTextFieldHosRefer.getFont());
        integerTextFieldHosRefer.setMaximumSize(new java.awt.Dimension(55, 22));
        integerTextFieldHosRefer.setMinimumSize(new java.awt.Dimension(55, 22));
        integerTextFieldHosRefer.setPreferredSize(new java.awt.Dimension(55, 22));
        integerTextFieldHosRefer.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                integerTextFieldHosReferFocusLost(evt);
            }
        });
        integerTextFieldHosRefer.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                integerTextFieldHosReferKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelReferHCODE.add(integerTextFieldHosRefer, gridBagConstraints);

        jTextFieldHosRefer.setEditable(false);
        jTextFieldHosRefer.setFont(jTextFieldHosRefer.getFont());
        jTextFieldHosRefer.setBorder(null);
        jTextFieldHosRefer.setMinimumSize(new java.awt.Dimension(100, 22));
        jTextFieldHosRefer.setPreferredSize(new java.awt.Dimension(100, 22));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelReferHCODE.add(jTextFieldHosRefer, gridBagConstraints);

        jButtonHosReferIn.setFont(jButtonHosReferIn.getFont());
        jButtonHosReferIn.setText("...");
        jButtonHosReferIn.setToolTipText("�ç��Һ�ŷ��Refer");
        jButtonHosReferIn.setBorder(null);
        jButtonHosReferIn.setMaximumSize(new java.awt.Dimension(22, 22));
        jButtonHosReferIn.setMinimumSize(new java.awt.Dimension(22, 22));
        jButtonHosReferIn.setPreferredSize(new java.awt.Dimension(22, 22));
        jButtonHosReferIn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonHosReferInActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelReferHCODE.add(jButtonHosReferIn, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 10;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel12.add(panelReferHCODE, gridBagConstraints);

        buttonGroup2.add(rdVisitType5);
        rdVisitType5.setFont(rdVisitType5.getFont());
        rdVisitType5.setToolTipText("�Ѻ��ԡ���Ҹ�ó�آ�ҧ�� (Telehealth/Telemedicine)"); // NOI18N
        rdVisitType5.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rdVisitType5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdVisitType5ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel12.add(rdVisitType5, gridBagConstraints);

        jLabel22.setFont(jLabel22.getFont());
        jLabel22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/telemed_icon.png"))); // NOI18N
        jLabel22.setToolTipText("�Ѻ��ԡ���Ҹ�ó�آ�ҧ�� (Telehealth/Telemedicine)"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 9;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel12.add(jLabel22, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jPanel12, gridBagConstraints);

        jPanel13.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel13.setLayout(new java.awt.GridBagLayout());

        jLabel7.setFont(jLabel7.getFont().deriveFont(jLabel7.getFont().getStyle() | java.awt.Font.BOLD));
        jLabel7.setText("ʶҹ����Ѻ��ԡ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel13.add(jLabel7, gridBagConstraints);

        jPanel5.setLayout(new java.awt.GridBagLayout());

        buttonGroup1.add(jRadioButtonIn);
        jRadioButtonIn.setFont(jRadioButtonIn.getFont());
        jRadioButtonIn.setSelected(true);
        jRadioButtonIn.setText("����ԡ���˹���");
        jPanel5.add(jRadioButtonIn, new java.awt.GridBagConstraints());

        buttonGroup1.add(jRadioButtonOut);
        jRadioButtonOut.setFont(jRadioButtonOut.getFont());
        jRadioButtonOut.setText("����ԡ�ù͡˹���");
        jPanel5.add(jRadioButtonOut, new java.awt.GridBagConstraints());

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel13.add(jPanel5, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jPanel13, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel4.add(jPanel6, gridBagConstraints);

        panelInsurance.setBorder(javax.swing.BorderFactory.createTitledBorder("Ἱ��Сѹ"));
        panelInsurance.setLayout(new java.awt.GridBagLayout());

        btnAddInsurancePlan.setFont(btnAddInsurancePlan.getFont());
        btnAddInsurancePlan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/btn_add24.png"))); // NOI18N
        btnAddInsurancePlan.setMaximumSize(new java.awt.Dimension(26, 26));
        btnAddInsurancePlan.setMinimumSize(new java.awt.Dimension(26, 26));
        btnAddInsurancePlan.setPreferredSize(new java.awt.Dimension(26, 26));
        btnAddInsurancePlan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddInsurancePlanActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelInsurance.add(btnAddInsurancePlan, gridBagConstraints);

        btnDelInsurancePlan.setFont(btnDelInsurancePlan.getFont());
        btnDelInsurancePlan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/btn_delete24.png"))); // NOI18N
        btnDelInsurancePlan.setMaximumSize(new java.awt.Dimension(26, 26));
        btnDelInsurancePlan.setMinimumSize(new java.awt.Dimension(26, 26));
        btnDelInsurancePlan.setPreferredSize(new java.awt.Dimension(26, 26));
        btnDelInsurancePlan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDelInsurancePlanActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelInsurance.add(btnDelInsurancePlan, gridBagConstraints);

        btnDownInsurancePlan.setFont(btnDownInsurancePlan.getFont());
        btnDownInsurancePlan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/down.gif"))); // NOI18N
        btnDownInsurancePlan.setMaximumSize(new java.awt.Dimension(26, 26));
        btnDownInsurancePlan.setMinimumSize(new java.awt.Dimension(26, 26));
        btnDownInsurancePlan.setPreferredSize(new java.awt.Dimension(26, 26));
        btnDownInsurancePlan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDownInsurancePlanActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelInsurance.add(btnDownInsurancePlan, gridBagConstraints);

        btnUpInsurancePlan.setFont(btnUpInsurancePlan.getFont());
        btnUpInsurancePlan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/up.gif"))); // NOI18N
        btnUpInsurancePlan.setMaximumSize(new java.awt.Dimension(26, 26));
        btnUpInsurancePlan.setMinimumSize(new java.awt.Dimension(26, 26));
        btnUpInsurancePlan.setPreferredSize(new java.awt.Dimension(26, 26));
        btnUpInsurancePlan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpInsurancePlanActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelInsurance.add(btnUpInsurancePlan, gridBagConstraints);

        tableInsurancePlan.setFont(tableInsurancePlan.getFont());
        tableInsurancePlan.setModel(tmcds);
        tableInsurancePlan.setFillsViewportHeight(true);
        tableInsurancePlan.setRowHeight(30);
        tableInsurancePlan.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tableInsurancePlan.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tableInsurancePlanMouseReleased(evt);
            }
        });
        tableInsurancePlan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tableInsurancePlanKeyReleased(evt);
            }
        });
        jScrollPane4.setViewportView(tableInsurancePlan);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelInsurance.add(jScrollPane4, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel4.add(panelInsurance, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 0.3;
        gridBagConstraints.insets = new java.awt.Insets(3, 0, 3, 3);
        add(jPanel4, gridBagConstraints);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("�Է�Ի�Шӵ�Ǽ�����"));
        jPanel3.setMinimumSize(new java.awt.Dimension(120, 160));
        jPanel3.setPreferredSize(new java.awt.Dimension(120, 160));
        jPanel3.setLayout(new java.awt.GridBagLayout());

        jTablePatientPayment.setFillsViewportHeight(true);
        jTablePatientPayment.setFont(jTablePatientPayment.getFont());
        jTablePatientPayment.setRowHeight(30);
        jTablePatientPayment.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTablePatientPaymentMouseReleased(evt);
            }
        });
        jTablePatientPayment.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTablePatientPaymentKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(jTablePatientPayment);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel3.add(jScrollPane1, gridBagConstraints);

        jPanel15.setLayout(new java.awt.GridBagLayout());

        jButtonDeletePp.setFont(jButtonDeletePp.getFont());
        jButtonDeletePp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/minus16.gif"))); // NOI18N
        jButtonDeletePp.setToolTipText("ź�Է����Шӵ��");
        jButtonDeletePp.setMaximumSize(new java.awt.Dimension(26, 26));
        jButtonDeletePp.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonDeletePp.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonDeletePp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDeletePpActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTH;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel15.add(jButtonDeletePp, gridBagConstraints);

        jButtonDown1.setFont(jButtonDown1.getFont());
        jButtonDown1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/down.gif"))); // NOI18N
        jButtonDown1.setToolTipText("����͹ŧ");
        jButtonDown1.setMaximumSize(new java.awt.Dimension(26, 26));
        jButtonDown1.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonDown1.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonDown1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDown1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel15.add(jButtonDown1, gridBagConstraints);

        jButtonUp2.setFont(jButtonUp2.getFont());
        jButtonUp2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/up.gif"))); // NOI18N
        jButtonUp2.setToolTipText("����͹���");
        jButtonUp2.setMaximumSize(new java.awt.Dimension(26, 26));
        jButtonUp2.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonUp2.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonUp2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonUp2ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel15.add(jButtonUp2, gridBagConstraints);

        jButtonSaveVtPayment.setFont(jButtonSaveVtPayment.getFont());
        jButtonSaveVtPayment.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/Moveback.gif"))); // NOI18N
        jButtonSaveVtPayment.setToolTipText("�ѹ�֡���Է��㹡���ѡ�Ҥ��駹��");
        jButtonSaveVtPayment.setMaximumSize(new java.awt.Dimension(26, 26));
        jButtonSaveVtPayment.setMinimumSize(new java.awt.Dimension(26, 26));
        jButtonSaveVtPayment.setPreferredSize(new java.awt.Dimension(26, 26));
        jButtonSaveVtPayment.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSaveVtPaymentActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel15.add(jButtonSaveVtPayment, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 0);
        jPanel3.add(jPanel15, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 0.7;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 0);
        add(jPanel3, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jCheckBoxVisitDateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxVisitDateActionPerformed
        dateComboBoxCheck.setEnabled(jCheckBoxVisitDate.isSelected());
        timeTextFieldCheck.setEnabled(jCheckBoxVisitDate.isSelected());
    }//GEN-LAST:event_jCheckBoxVisitDateActionPerformed

    private void timeTextFieldCheckMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_timeTextFieldCheckMouseClicked
        timeTextFieldCheck.selectAll();
    }//GEN-LAST:event_timeTextFieldCheckMouseClicked

    private void timeTextFieldCheckKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_timeTextFieldCheckKeyReleased
    }//GEN-LAST:event_timeTextFieldCheckKeyReleased

    private void dateComboBoxCheckKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_dateComboBoxCheckKeyReleased
    }//GEN-LAST:event_dateComboBoxCheckKeyReleased

    private void jButtonB1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonB1ActionPerformed
        String command = theHC.theLookupControl.readOption().b1_command;
        try {
            Process proc = Runtime.getRuntime().exec(command);
        } catch (IOException ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
        }
    }//GEN-LAST:event_jButtonB1ActionPerformed

    private void jCheckBoxShowCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxShowCancelActionPerformed
        checkBoxShowCancelVisitPayment(this.jCheckBoxShowCancel.isSelected());
        this.theHO.showVisitPaymentCancel = this.jCheckBoxShowCancel.isSelected();
    }//GEN-LAST:event_jCheckBoxShowCancelActionPerformed

    private void jButtonSaveVtPaymentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSaveVtPaymentActionPerformed
        int select = jTablePatientPayment.getSelectedRow();
        if (select < 0) {
            theUS.setStatus("��س����͡�Է������ͧ��úѹ�֡���Է��㹡�кǹ���", UpdateStatus.WARNING);
            return;
        }
        PatientPayment payment = (PatientPayment) vPatientPayment.get(select);
        payment.setObjectId(null);
        saveVisitPayment(payment);

    }//GEN-LAST:event_jButtonSaveVtPaymentActionPerformed

    private void dateComboBoxToFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_dateComboBoxToFocusLost
    }//GEN-LAST:event_dateComboBoxToFocusLost

    private void dateComboBoxToActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dateComboBoxToActionPerformed
        if (dateComboBoxTo.getText() != null && !"".equals(dateComboBoxTo.getText())) {
            /**
             * fromDate ���ѹ�͡�ѵ� toDate ���ѹ������غѵ�
             */
            String fromDate = dateComboBoxFrom.getText();
            if (fromDate == null || fromDate.isEmpty()) {
                return;
            }
            String toDate = dateComboBoxTo.getText();
            if (toDate == null || toDate.isEmpty()) {
                toDate = dateComboBoxFrom.getText();
            }
            /**
             * yearFrom �纻��ѹ�͡�ѵ� monthFrom ����͹�ѹ�͡�ѵ� dateFrom
             * ���ѹ�ѹ�͡�ѵ� yearTo �纻��ѹ������غѵ� monthTo
             * ����͹�ѹ������غѵ� dateTo ���ѹ����ѹ������غѵ�
             */
            int yearFrom = Integer.parseInt(fromDate.substring(0, 4));
            int monthFrom = Integer.parseInt(fromDate.substring(5, 7));
            int dateFrom = Integer.parseInt(fromDate.substring(8, 10));
            int yearTo = Integer.parseInt(toDate.substring(0, 4));
            int monthTo = Integer.parseInt(toDate.substring(5, 7));
            int dateTo = Integer.parseInt(toDate.substring(8, 10));
            if (yearTo < yearFrom) {
                theUS.setStatus("�ѹ������غѵ��������ö�繻���͹��ѧ�ҡ�ѹ�͡�ѵ�",
                        UpdateStatus.WARNING);
                return;
            }
            if (yearTo == yearFrom) {
                if (monthTo < monthFrom) {
                    theUS.setStatus("�ѹ������غѵ��������ö����͹��͹��ѧ�ҡ�ѹ�͡�ѵ�",
                            UpdateStatus.WARNING);
                    return;
                }
                if (monthTo == monthFrom) {
                    if (dateTo < dateFrom) {
                        theUS.setStatus("�ѹ������غѵ��������ö���ѹ�����͹��ѧ�ҡ�ѹ�͡�ѵ�",
                                UpdateStatus.WARNING);
                    }
                }
            }
        }
    }//GEN-LAST:event_dateComboBoxToActionPerformed

    private void dateComboBoxFromActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dateComboBoxFromActionPerformed

    }//GEN-LAST:event_dateComboBoxFromActionPerformed

    private void integerTextFieldHosReferKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_integerTextFieldHosReferKeyReleased
        if (integerTextFieldHosRefer.getText().length() == 5) {
            Office office = theHC.theLookupControl.readHospitalByCode(integerTextFieldHosRefer.getText());
            if (office == null) {
                return;
            }
            if (office.getObjectId() != null) {
                jTextFieldHosRefer.setText(office.name);
                if (theVisit != null) {
                    theVisit.refer_in = office.getObjectId();
                }
            }
            integerTextFieldHosRefer.transferFocus();
        }
    }//GEN-LAST:event_integerTextFieldHosReferKeyReleased

    private void txtHosPrimaryCodeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtHosPrimaryCodeKeyReleased
        this.doSearchHosPrimaryFromCode();
    }//GEN-LAST:event_txtHosPrimaryCodeKeyReleased

    private void txtHosReferCodeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtHosReferCodeKeyReleased
        this.doSearchHosReferFromCode();
    }//GEN-LAST:event_txtHosReferCodeKeyReleased

    private void jTextFieldSearchPlanKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldSearchPlanKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_DOWN) {
            if (jTablePlan.getRowCount() > 0) {
                jTablePlan.requestFocus();
                jTablePlan.setRowSelectionInterval(0, 0);
            }
        }
    }//GEN-LAST:event_jTextFieldSearchPlanKeyReleased

    private void jTableVisitPaymentKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTableVisitPaymentKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_UP || evt.getKeyCode() == KeyEvent.VK_DOWN) {
            jTableVisitPaymentMouseReleased(null);
        }
    }//GEN-LAST:event_jTableVisitPaymentKeyReleased

    private void jTableVisitPaymentMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableVisitPaymentMouseReleased
        this.jTablePatientPayment.clearSelection();
        this.jTablePlan.clearSelection();
        //this.jTableVisitPayment.clearSelection();
        int row = jTableVisitPayment.getSelectedRow();
        if (row == -1) {
            return;
        }
        thePaymentNow = (Payment) vVisitPayment.get(row);
        setPayment(thePaymentNow);
    }//GEN-LAST:event_jTableVisitPaymentMouseReleased

    private void jTablePatientPaymentMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTablePatientPaymentMouseReleased
        //doSelectedPatientPayment();
        this.jTablePlan.clearSelection();
        this.jTableVisitPayment.clearSelection();
        Payment payment = (Payment) vPatientPayment.get(jTablePatientPayment.getSelectedRow());
        Payment paymentTemp = (Payment) payment.clone();
        paymentTemp.setObjectId(null);
        setPayment(paymentTemp);
        jTableVisitPayment.clearSelection();
    }//GEN-LAST:event_jTablePatientPaymentMouseReleased

    private void jTablePatientPaymentKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTablePatientPaymentKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_UP || evt.getKeyCode() == KeyEvent.VK_DOWN) {
            jTablePatientPaymentMouseReleased(null);
        }
        if (evt.getKeyCode() == KeyEvent.VK_SPACE) {
            this.jButtonSaveVtPaymentActionPerformed(null);
        }
    }//GEN-LAST:event_jTablePatientPaymentKeyReleased

    private void jTablePlanKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTablePlanKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_UP || evt.getKeyCode() == KeyEvent.VK_DOWN) {
            jTablePlanMouseReleased(null);
        }
        if (evt.getKeyCode() == KeyEvent.VK_SPACE) {
            this.jButtonSaveVisitPaymentActionPerformed(null);
        }
    }//GEN-LAST:event_jTablePlanKeyReleased

    private void jTablePlanMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTablePlanMouseReleased
        this.jTablePatientPayment.clearSelection();
        //this.jTablePlan.clearSelection();
        this.jTableVisitPayment.clearSelection();
        Plan p = (Plan) thePlan.get(jTablePlan.getSelectedRow());
        setPlan(p);
    }//GEN-LAST:event_jTablePlanMouseReleased

    private void integerTextFieldHosReferFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_integerTextFieldHosReferFocusLost
        Office office = theHC.theLookupControl.readHospitalByCode(integerTextFieldHosRefer.getText());
        if (office == null) {
            integerTextFieldHosRefer.setText("");
            jTextFieldHosRefer.setText("");
            theUS.setStatus("��辺ʶҹ��Һ�ŷ��ç�Ѻ���ʷ���к� ��سҵ�Ǩ�ͺ�����ա����", UpdateStatus.WARNING);
            if (theVisit != null) {
                theVisit.refer_in = "";
            }
        } else {
            if (office.getObjectId().equals(theHO.theSite.getObjectId())) {
                txtHosReferCode.setText("");
                txtHosRefer.setText("");
                theUS.setStatus("ʶҹ��Һ�ŷ���Ѻ�ҵç�Ѻ����ʶҹ��Һ�ŷ����ѧ����ԡ�� ��سҵ�Ǩ�ͺ�����ա����", UpdateStatus.WARNING);
                if (theVisit != null) {
                    theVisit.refer_in = "";
                }
            } else {
                jTextFieldHosRefer.setText(office.name);
                if (theVisit != null) {
                    theVisit.refer_in = office.getObjectId();
                }
            }
        }
    }//GEN-LAST:event_integerTextFieldHosReferFocusLost

    private void txtHosPrimaryCodeFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtHosPrimaryCodeFocusLost
        this.doSearchHosPrimaryFromCode();
    }//GEN-LAST:event_txtHosPrimaryCodeFocusLost

    private void txtHosReferCodeFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtHosReferCodeFocusLost
        this.doSearchHosReferFromCode();
    }//GEN-LAST:event_txtHosReferCodeFocusLost

    private void jButtonPrintOPDCardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPrintOPDCardActionPerformed
        int printPreview = PrintControl.MODE_PRINT;
        //��ͧ�տѧ���鹹������������ clear �����ŷ�������
        checkBoxShowCancelVisitPayment(false);
        theHC.thePrintControl.printOPDCard(printPreview, vVisitPayment);
    }//GEN-LAST:event_jButtonPrintOPDCardActionPerformed

    private void jButtonVisitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonVisitActionPerformed
        this.doVisitPatient();
    }//GEN-LAST:event_jButtonVisitActionPerformed

    private void jButtonHosReferInActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonHosReferInActionPerformed
        if (theVisit != null) {
            theUS.setStatus("�������������кǹ��������������ö���ʶҹ��Һ�ŷ��������", UpdateStatus.WARNING);
            return;
        }
        Office office = new Office();
        office.setObjectId("");
        if (theHD.showDialogOffice(office)) {
            this.jTextFieldHosRefer.setText(office.getName());
            this.integerTextFieldHosRefer.setText(office.getObjectId());
        }
    }//GEN-LAST:event_jButtonHosReferInActionPerformed

    private void jButtonSaveVisitPaymentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSaveVisitPaymentActionPerformed
        this.doSaveVisitPayment();
    }//GEN-LAST:event_jButtonSaveVisitPaymentActionPerformed

    public void saveVisitPayment(Payment thePaymentNow) {
        if (theHO.thePatient == null && theHO.theFamily == null) {
            theUS.setStatus("�������ѧ�����١���͡ �������ö�����Է����", UpdateStatus.WARNING);
            return;
        }
        if (theHO.theVisit != null && theHO.theVisit.is_discharge_money.equals("1")) {
            theUS.setStatus("�����¨�˹��·ҧ����Թ�����������ö����Է����", UpdateStatus.WARNING);
            return;
        }
        if (thePaymentNow == null) {
            theUS.setStatus("��س����͡�Է�ԡ���ѡ��", UpdateStatus.WARNING);
            return;
        }
        if (thePaymentNow.plan_kid == null || thePaymentNow.plan_kid.isEmpty()) {
            theUS.setStatus("��س����͡�Է�ԡ���ѡ��", UpdateStatus.WARNING);
            return;
        }
        if (vVisitPayment == null) {
            vVisitPayment = new Vector();
        }
        Date dateins = DateUtil.getDateFromText(thePaymentNow.card_ins_date);
        Date dateexp = DateUtil.getDateFromText(thePaymentNow.card_exp_date);
        if (dateins != null && dateexp != null) {
            int date_valid = DateUtil.countDateDiff(thePaymentNow.card_ins_date, thePaymentNow.card_exp_date);
            if (date_valid > 0) {
                theUS.setStatus("�ѹ����͡�ѵ�����ѹ�����������ժ�ǧ������١��ͧ", UpdateStatus.WARNING);
                dateComboBoxTo.requestFocus();
                return;
            }
        }
        if (dateins != null) {
            int date_valid = DateUtil.countDateDiff(thePaymentNow.card_ins_date, theHO.date_time);
            if (date_valid > 0) {
                theUS.setStatus("�ѹ����͡�ѵõ�ͧ������ѹ����͹Ҥ�", UpdateStatus.WARNING);
                dateComboBoxFrom.requestFocus();
                return;
            }
        }
        if (thePaymentNow.hosp_main.isEmpty()) {
            theUS.setStatus("��سҡ�͡����ʶҹ��Һ�ŷ���Ѻ����觵��", UpdateStatus.WARNING);
            txtHosReferCode.requestFocus();
            return;
        }
        thePaymentNow.visitGovOfficalPlan = getVisitGovOffical();
        if (thePaymentNow.visitGovOfficalPlan != null) {
            if (thePaymentNow.visitGovOfficalPlan.govoffical_type == 1) {
                if (thePaymentNow.visitGovOfficalPlan.govoffical_number == null || thePaymentNow.visitGovOfficalPlan.govoffical_number.isEmpty()) {
                    theUS.setStatus("��س��к����� Claim Code/�Ţ͹��ѵ�", UpdateStatus.WARNING);
                    txtGovNo.requestFocus();
                    return;
                }
            } else if (thePaymentNow.visitGovOfficalPlan.govoffical_type == 2) {
                if (thePaymentNow.visitGovOfficalPlan.govoffical_number == null || thePaymentNow.visitGovOfficalPlan.govoffical_number.isEmpty()) {
                    theUS.setStatus("�Ţ���˹ѧ��� (�Ҿ������� Refer)", UpdateStatus.WARNING);
                    txtGovNo.requestFocus();
                    return;
                }
                if (thePaymentNow.visitGovOfficalPlan.f_govcode_id == null || thePaymentNow.visitGovOfficalPlan.f_govcode_id.isEmpty()) {
                    theUS.setStatus("��س��к�����˹��§ҹ���ѧ�Ѵ�ͧ������Է��", UpdateStatus.WARNING);
                    txtGovCode.requestFocus();
                    return;
                }
                if (thePaymentNow.visitGovOfficalPlan.ownname == null || thePaymentNow.visitGovOfficalPlan.ownname.isEmpty()) {
                    theUS.setStatus("��س��кت��� ���ʡ�Ţͧ������Է�Ԣ���Ҫ���/ͻ�", UpdateStatus.WARNING);
                    txtOwnname.requestFocus();
                    return;
                }
                if (thePaymentNow.visitGovOfficalPlan.ownrpid == null || thePaymentNow.visitGovOfficalPlan.ownrpid.isEmpty()) {
                    theUS.setStatus("��س��к��Ţ��Шӵ�ǻ�ЪҪ��ͧ������Է�Ԣ���Ҫ���/ͻ�", UpdateStatus.WARNING);
                    txtOwnrPID.requestFocus();
                    return;
                }
                if (thePaymentNow.visitGovOfficalPlan.f_subinscl_id == null || thePaymentNow.visitGovOfficalPlan.f_subinscl_id.isEmpty()) {
                    theUS.setStatus("��س��кػ������Է��", UpdateStatus.WARNING);
                    cbSubinscl.requestFocus();
                    return;
                }
                if (thePaymentNow.visitGovOfficalPlan.f_relinscl_id == null || thePaymentNow.visitGovOfficalPlan.f_relinscl_id.isEmpty()) {
                    theUS.setStatus("��س��кؤ�������ѹ��", UpdateStatus.WARNING);
                    cbRelinscl.requestFocus();
                    return;
                }
            }
        }
        thePaymentNow.visitSocialsecPlan = getVisitSocialsec();
        if (theHO.theVisit != null) {
            theHC.theVisitControl.saveVPayment(thePaymentNow, vVisitPayment);
            return;
        }
        int i;
        for (i = 0; i < vVisitPayment.size(); i++) {
            Payment p = ((Payment) vVisitPayment.get(i));
            if (p.plan_kid.equals(thePaymentNow.plan_kid)) {
                break;
            }
        }
        int cur_row;
        if (i != vVisitPayment.size()) {
            vVisitPayment.removeElementAt(i);
            vVisitPayment.insertElementAt(thePaymentNow, i);
            cur_row = i;
        } else {
            vVisitPayment.add(thePaymentNow);
            cur_row = vVisitPayment.size() - 1;
        }
        setVisitPaymentV(vVisitPayment);
        this.jTableVisitPayment.setRowSelectionInterval(cur_row, cur_row);
        this.jTableVisitPaymentMouseReleased(null);
    }

    private VisitGovOfficalPlan getVisitGovOffical() {
        if (panelExtraGovPlan.isVisible()) {
            int type = cbGovType.getSelectedIndex();
            if (thePaymentNow.visitGovOfficalPlan == null
                    || (thePaymentNow.visitGovOfficalPlan.getObjectId() == null
                    && type == 0)) {
                return null;
            } else {
                thePaymentNow.visitGovOfficalPlan.govoffical_type = type;
                thePaymentNow.visitGovOfficalPlan.govoffical_number = type == 0 ? "" : txtGovNo.getText().trim();
                thePaymentNow.visitGovOfficalPlan.f_govcode_id = type != 2 ? "" : txtGovCode.getText().trim();
                thePaymentNow.visitGovOfficalPlan.ownname = type != 2 ? "" : txtOwnname.getText().trim();
                thePaymentNow.visitGovOfficalPlan.ownrpid = type != 2 ? "" : txtOwnrPID.getText().trim();
                thePaymentNow.visitGovOfficalPlan.f_subinscl_id = type != 2 ? "" : ComboboxModel.getCodeComboBox(cbSubinscl);
                thePaymentNow.visitGovOfficalPlan.f_relinscl_id = type != 2 ? "" : ComboboxModel.getCodeComboBox(cbRelinscl);
                return thePaymentNow.visitGovOfficalPlan;
            }
        } else {
            return null;
        }
    }

    private VisitSocialsecPlan getVisitSocialsec() {
        if (panelExtraSocialsecPlan.isVisible()) {
            thePaymentNow.visitSocialsecPlan.socialsec_number = txtSocialsecNo.getText().trim();
            return thePaymentNow.visitSocialsecPlan;
        } else {
            return null;
        }
    }

    private void jButtonHosSubActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonHosSubActionPerformed
        Office office = new Office();
        office.setObjectId(thePaymentNow.hosp_sub);
        if (theHD.showDialogOffice(office)) {
            txtHosPrimary.setText(office.getName());
            txtHosPrimaryCode.setText(office.getObjectId());
        }
    }//GEN-LAST:event_jButtonHosSubActionPerformed

    private void jButtonHosMainActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonHosMainActionPerformed
        Office office = new Office();
        office.setObjectId(thePaymentNow.hosp_main);
        if (theHD.showDialogOffice(office)) {
            txtHosRefer.setText(office.getName());
            txtHosReferCode.setText(office.getObjectId());
        }
    }//GEN-LAST:event_jButtonHosMainActionPerformed

    private void jButtonSavePtPaymentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSavePtPaymentActionPerformed
        int select = jTableVisitPayment.getSelectedRow();
        if (select < 0) {
            theUS.setStatus("��س����͡�Է������ͧ��úѹ�֡", UpdateStatus.WARNING);
            return;
        }
        if (vPatientPayment == null) {
            vPatientPayment = new Vector();
        }
        Payment payment = (Payment) vVisitPayment.get(select);
        theHC.thePatientControl.savePatientPayment(vPatientPayment, payment);
    }//GEN-LAST:event_jButtonSavePtPaymentActionPerformed

    private void jButtonDownActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDownActionPerformed
        int select = jTableVisitPayment.getSelectedRow();
        if (select == -1 || select + 1 > vVisitPayment.size() - 1) {
            return;
        }
        Payment p = (Payment) vVisitPayment.get(select);
        //��Ǩ�ͺ������ҹ��� visit �����ѧ
        boolean res = false;
        if (p.getObjectId() != null) {//��ҹ��� visit ����
            res = theHC.theVisitControl.downVPaymentPriority(vVisitPayment, select);
        } else { //����ҹ��� visit
            vVisitPayment.remove(select);
            vVisitPayment.add(select + 1, p);
        }
        setVisitPaymentV(vVisitPayment);
        if (res) {
            jTableVisitPayment.addRowSelectionInterval(select + 1, select + 1);
        }
    }//GEN-LAST:event_jButtonDownActionPerformed

    private void jButtonDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDeleteActionPerformed
        int[] row = jTableVisitPayment.getSelectedRows();
        if (theHO.theVisit == null) {
            for (int i = 0; i < row.length; i++) {
                this.vVisitPayment.remove(row[i]);
            }
            this.setVisitPaymentV(vVisitPayment);
        } else {
            theHC.theVisitControl.deleteVPayment(vVisitPayment, row, theHO.vBillingInvoice);
        }
    }//GEN-LAST:event_jButtonDeleteActionPerformed

    private void jButtonUpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonUpActionPerformed
        int select = jTableVisitPayment.getSelectedRow();
        if (select <= 0) {
            return;
        }
        Payment p = (Payment) vVisitPayment.get(select);
        boolean res = false;
        if (p.getObjectId() != null) {
            res = theHC.theVisitControl.upVPaymentPriority(vVisitPayment, select);
        } else {
            vVisitPayment.remove(select);
            vVisitPayment.add(select - 1, p);
        }
        setVisitPaymentV(vVisitPayment);
        if (res) {
            jTableVisitPayment.addRowSelectionInterval(select - 1, select - 1);
        }
    }//GEN-LAST:event_jButtonUpActionPerformed

    private void jButtonDown1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDown1ActionPerformed
        int row = jTablePatientPayment.getSelectedRow();
        int size = jTablePatientPayment.getRowCount();
        theHC.thePatientControl.downPriorityPatientPayment(vPatientPayment, row);
        if (row + 1 >= size) {
            jTablePatientPayment.setRowSelectionInterval(size - 1, size - 1);
            return;
        }
        jTablePatientPayment.setRowSelectionInterval(row + 1, row + 1);
    }//GEN-LAST:event_jButtonDown1ActionPerformed

    private void jButtonDeletePpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDeletePpActionPerformed
        int[] select = jTablePatientPayment.getSelectedRows();
        theHC.thePatientControl.deletePatientPayment(vPatientPayment, select);
    }//GEN-LAST:event_jButtonDeletePpActionPerformed

    private void jButtonUp2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonUp2ActionPerformed
        int row = jTablePatientPayment.getSelectedRow();
        theHC.thePatientControl.upPriorityPatientPayment(vPatientPayment, row);
        if (row - 1 <= 0) {
            jTablePatientPayment.setRowSelectionInterval(0, 0);
            return;
        }
        jTablePatientPayment.setRowSelectionInterval(row - 1, row - 1);
    }//GEN-LAST:event_jButtonUp2ActionPerformed

    private void jButtonSearchAllPlanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSearchAllPlanActionPerformed
        String strplan = jTextFieldSearchPlan.getText();
        if (strplan.isEmpty()) {
            thePlan = theHC.theLookupControl.listPlan();
            setPlanV(thePlan);
        } else {
            thePlan = theHC.theLookupControl.listPlan(strplan);
            setPlanV(thePlan);
        }
    }//GEN-LAST:event_jButtonSearchAllPlanActionPerformed

    private void jTextFieldSearchPlanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldSearchPlanActionPerformed
        jButtonSearchAllPlanActionPerformed(null);
    }//GEN-LAST:event_jTextFieldSearchPlanActionPerformed

    private void exitForm(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_exitForm
        System.exit(0);
    }//GEN-LAST:event_exitForm

    private void rdVisitType1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdVisitType1ActionPerformed
        this.validateVisitTypeUI();
    }//GEN-LAST:event_rdVisitType1ActionPerformed

    private void rdVisitType2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdVisitType2ActionPerformed
        this.validateVisitTypeUI();
    }//GEN-LAST:event_rdVisitType2ActionPerformed

    private void rdVisitType3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdVisitType3ActionPerformed
        this.validateVisitTypeUI();
    }//GEN-LAST:event_rdVisitType3ActionPerformed

    private void rdVisitType4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdVisitType4ActionPerformed
        this.validateVisitTypeUI();
    }//GEN-LAST:event_rdVisitType4ActionPerformed

    private void btnShowInsurancePlanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnShowInsurancePlanActionPerformed
        panelInsurance.setVisible(btnShowInsurancePlan.isVisible() && btnShowInsurancePlan.isSelected());
    }//GEN-LAST:event_btnShowInsurancePlanActionPerformed

    private void btnAddInsurancePlanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddInsurancePlanActionPerformed
        this.doAddInsurancePlan();
    }//GEN-LAST:event_btnAddInsurancePlanActionPerformed

    private void btnDelInsurancePlanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDelInsurancePlanActionPerformed
        this.doDelInsurancePlan();
    }//GEN-LAST:event_btnDelInsurancePlanActionPerformed

    private void btnUpInsurancePlanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpInsurancePlanActionPerformed
        this.doUpInsurancePlan();
    }//GEN-LAST:event_btnUpInsurancePlanActionPerformed

    private void btnDownInsurancePlanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDownInsurancePlanActionPerformed
        this.doDownInsurancePlan();
    }//GEN-LAST:event_btnDownInsurancePlanActionPerformed

    private void tableInsurancePlanKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tableInsurancePlanKeyReleased
        this.doSelectedInsurancePlan();
    }//GEN-LAST:event_tableInsurancePlanKeyReleased

    private void tableInsurancePlanMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableInsurancePlanMouseReleased
        this.doSelectedInsurancePlan();
    }//GEN-LAST:event_tableInsurancePlanMouseReleased

    private void txtHosRegCodeFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtHosRegCodeFocusLost
        this.doSearchHosRegFromCode();
    }//GEN-LAST:event_txtHosRegCodeFocusLost

    private void txtHosRegCodeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtHosRegCodeKeyReleased
        this.doSearchHosRegFromCode();
    }//GEN-LAST:event_txtHosRegCodeKeyReleased

    private void jButtonHosRegActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonHosRegActionPerformed
        Office office = new Office();
        office.setObjectId(thePaymentNow.hosp_reg);
        if (theHD.showDialogOffice(office)) {
            txtHosReg.setText(office.getName());
            txtHosRegCode.setText(office.getObjectId());
        }
    }//GEN-LAST:event_jButtonHosRegActionPerformed

    private void cbGovTypeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbGovTypeActionPerformed
        this.doSelectGovType();
    }//GEN-LAST:event_cbGovTypeActionPerformed

    private void txtGovCodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtGovCodeActionPerformed
        this.doFindGovName(txtGovCode.getText().trim());
    }//GEN-LAST:event_txtGovCodeActionPerformed

    private void txtGovCodeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtGovCodeKeyReleased
        if (txtGovCode.getText().length() == 5) {
            this.doFindGovName(txtGovCode.getText().trim());
        }
    }//GEN-LAST:event_txtGovCodeKeyReleased

    private void btnBrowseGovActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBrowseGovActionPerformed
        List<DataSource> sources = dialogSearchGovCode.openDialog();
        if (!sources.isEmpty()) {
            txtGovCode.setText(((GovCode) sources.get(0).getId()).getCode());
            txtGovName.setText(sources.get(0).getValue());
        }
    }//GEN-LAST:event_btnBrowseGovActionPerformed

    private void txtGovNoFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtGovNoFocusGained
        txtGovNo.selectAll();

    }//GEN-LAST:event_txtGovNoFocusGained

    private void txtGovCodeFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtGovCodeFocusGained
        txtGovCode.selectAll();
    }//GEN-LAST:event_txtGovCodeFocusGained

    private void txtOwnnameFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtOwnnameFocusGained
        txtOwnname.selectAll();
    }//GEN-LAST:event_txtOwnnameFocusGained

    private void txtOwnrPIDFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtOwnrPIDFocusGained
        txtOwnrPID.selectAll();
    }//GEN-LAST:event_txtOwnrPIDFocusGained

    private void jTextFieldCardIDFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextFieldCardIDFocusGained
        jTextFieldCardID.selectAll();
    }//GEN-LAST:event_jTextFieldCardIDFocusGained

    private void txtHosReferCodeFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtHosReferCodeFocusGained
        txtHosReferCode.selectAll();
    }//GEN-LAST:event_txtHosReferCodeFocusGained

    private void txtHosPrimaryCodeFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtHosPrimaryCodeFocusGained
        txtHosPrimaryCode.selectAll();
    }//GEN-LAST:event_txtHosPrimaryCodeFocusGained

    private void txtHosRegCodeFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtHosRegCodeFocusGained
        txtHosRegCode.selectAll();
    }//GEN-LAST:event_txtHosRegCodeFocusGained

    private void txtHosPrimaryCodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtHosPrimaryCodeActionPerformed
        this.doSearchHosPrimaryFromCode();
    }//GEN-LAST:event_txtHosPrimaryCodeActionPerformed

    private void txtHosReferCodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtHosReferCodeActionPerformed
        this.doSearchHosReferFromCode();
    }//GEN-LAST:event_txtHosReferCodeActionPerformed

    private void txtHosRegCodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtHosRegCodeActionPerformed
        this.doSearchHosRegFromCode();
    }//GEN-LAST:event_txtHosRegCodeActionPerformed

    private void txtSocialsecNoFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtSocialsecNoFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSocialsecNoFocusGained

    private void rdVisitType5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdVisitType5ActionPerformed
        this.validateVisitTypeUI();
    }//GEN-LAST:event_rdVisitType5ActionPerformed
    /**
     * @param args the command line arguments
     */
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddInsurancePlan;
    private javax.swing.JButton btnBrowseGov;
    private javax.swing.JButton btnDelInsurancePlan;
    private javax.swing.JButton btnDownInsurancePlan;
    private javax.swing.JToggleButton btnShowInsurancePlan;
    private javax.swing.JButton btnUpInsurancePlan;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JComboBox cbGovType;
    private javax.swing.JComboBox cbRelinscl;
    private javax.swing.JComboBox cbSubinscl;
    private com.hospital_os.utility.DateComboBox dateComboBoxCheck;
    private com.hospital_os.utility.DateComboBox dateComboBoxFrom;
    private com.hospital_os.utility.DateComboBox dateComboBoxTo;
    private javax.swing.JTextField integerTextFieldHosRefer;
    private javax.swing.JButton jButtonB1;
    private javax.swing.JButton jButtonDelete;
    private javax.swing.JButton jButtonDeletePp;
    private javax.swing.JButton jButtonDown;
    private javax.swing.JButton jButtonDown1;
    private javax.swing.JButton jButtonHosMain;
    private javax.swing.JButton jButtonHosReferIn;
    private javax.swing.JButton jButtonHosReg;
    private javax.swing.JButton jButtonHosSub;
    private javax.swing.JButton jButtonPrintOPDCard;
    private javax.swing.JButton jButtonSavePtPayment;
    private javax.swing.JButton jButtonSaveVisitPayment;
    private javax.swing.JButton jButtonSaveVtPayment;
    private javax.swing.JButton jButtonSearchAllPlan;
    private javax.swing.JButton jButtonUp;
    private javax.swing.JButton jButtonUp2;
    private javax.swing.JButton jButtonVisit;
    private javax.swing.JCheckBox jCheckBoxShowCancel;
    private javax.swing.JCheckBox jCheckBoxVisitDate;
    private com.hosv3.gui.component.HosComboBox jComboBoxTariff;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel66;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelMoneyLimit;
    private javax.swing.JLabel jLabelPlan;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JPanel jPanelPlan;
    private javax.swing.JRadioButton jRadioButtonIn;
    private javax.swing.JRadioButton jRadioButtonOut;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane12;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private com.hosv3.gui.component.HJTableSort jTablePatientPayment;
    public com.hosv3.gui.component.HJTableSort jTablePlan;
    public com.hosv3.gui.component.HJTableSort jTableVisitPayment;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextField jTextFieldCardID;
    private javax.swing.JTextField jTextFieldHosRefer;
    private javax.swing.JTextField jTextFieldSearchPlan;
    private javax.swing.JPanel panelButtonPlugin;
    private javax.swing.JPanel panelExtraGovPlan;
    private javax.swing.JPanel panelExtraGovPlanDetail;
    private javax.swing.JPanel panelExtraSocialsecPlan;
    private javax.swing.JPanel panelInsurance;
    private javax.swing.JPanel panelReferHCODE;
    private javax.swing.JRadioButton rdVisitType1;
    private javax.swing.JRadioButton rdVisitType2;
    private javax.swing.JRadioButton rdVisitType3;
    private javax.swing.JRadioButton rdVisitType4;
    private javax.swing.JRadioButton rdVisitType5;
    private javax.swing.JTable tableInsurancePlan;
    private com.hospital_os.utility.TimeTextField timeTextFieldCheck;
    private javax.swing.JTextField txtGovCode;
    private javax.swing.JTextField txtGovName;
    private javax.swing.JTextField txtGovNo;
    private javax.swing.JTextField txtHosPrimary;
    private javax.swing.JTextField txtHosPrimaryCode;
    private javax.swing.JTextField txtHosRefer;
    private javax.swing.JTextField txtHosReferCode;
    private javax.swing.JTextField txtHosReg;
    private javax.swing.JTextField txtHosRegCode;
    private javax.swing.JTextField txtOwnname;
    private javax.swing.JTextField txtOwnrPID;
    private javax.swing.JTextField txtSocialsecNo;
    // End of variables declaration//GEN-END:variables

    /**
     * ��㹡���Ң�������¡���Է�Է��١¡��ԡ��ѧ�ҡ��ä�ԡ���͡ CheckBox
     *
     * @author padungrat(tong)
     * @param select �� boolean ����˹���������͡����ʴ���¡�÷��١¡��ԡ
     * @date 05/04/2549,09:44
     */
    public void checkBoxShowCancelVisitPayment(boolean select) {
        //��Ǩ�ͺ��Ҽ����������ʶҹ� visit �������
        if (this.theHO.theVisit != null && this.theHO.theVisit.getObjectId() != null) {
            //��Ǩ�ͺ������͡��¡��
            if (select) {
                for (int i = vVisitPayment.size() - 1; i >= 0; i--) {
                    Payment payment = (Payment) vVisitPayment.get(i);
                    if (!Gutil.isSelected(payment.visit_payment_active)) {
                        vVisitPayment.remove(i);
                    }
                }
                Vector vc = this.theHC.theVisitControl.listVisitPaymentCancel(this.theHO.theVisit.getObjectId());
                if (vc != null) {
                    for (int i = 0, size = vc.size(); i < size; i++) {
                        Payment payment = (Payment) vc.get(i);
                        vVisitPayment.add(payment);
                    }
                }
            } else {
                for (int i = vVisitPayment.size() - 1; i >= 0; i--) {
                    Payment payment = (Payment) vVisitPayment.get(i);
                    if (!Gutil.isSelected(payment.visit_payment_active)) {
                        vVisitPayment.remove(i);
                    }
                }
            }
            jCheckBoxShowCancel.setSelected(select);
            setVisitPaymentV(vVisitPayment);
        } else {
            jCheckBoxShowCancel.setSelected(false);
            theUS.setStatus("�ѧ��������͡�����·������㹡�кǹ���", UpdateStatus.WARNING);
        }
    }

    /**
     * @deprecated henbe �Ҩҡ�˹����¹����
     */
    private void setAuthentication(Employee em) {
        if (em.authentication_id.equals(Authentication.IPD)) {
            jButtonVisit.setEnabled(false);
            jButtonHosReferIn.setEnabled(false);
        }
    }

    /*
     * ૵����object payment
     */
    private Payment getPayment() {
        thePaymentNow.card_ins_date = dateComboBoxFrom.getText();
        thePaymentNow.card_exp_date = dateComboBoxTo.getText();
        thePaymentNow.card_id = jTextFieldCardID.getText();
        thePaymentNow.b_tariff_id = ComboboxModel.getCodeComboBox(jComboBoxTariff);
        thePaymentNow.visit_payment_active = "1";
        //ʻʪ �觾������������� sub ����ç��Һ����ѡ main ����ç��Һ���觵��
        thePaymentNow.hosp_sub = this.txtHosPrimaryCode.getText();
        thePaymentNow.hosp_main = this.txtHosReferCode.getText();
        thePaymentNow.hosp_reg = this.txtHosRegCode.getText();
        return thePaymentNow;
    }

    /*
     * ૵����object payment ����GUI
     *
     */
    private void setPayment(Payment pm) {
        btnShowInsurancePlan.setVisible(false);
        panelInsurance.setVisible(false);
        panelExtraGovPlan.setVisible(false);
        panelExtraSocialsecPlan.setVisible(false);
        tmcds.clearTable();
        thePaymentNow = pm;
        if (thePaymentNow == null) {
            thePaymentNow = new Payment();
        }
        // ��������Ңͧ�Է�Ԣ���Ҫ���
        boolean isGovOfficalPlan = thePaymentNow.plan_kid == null ? false : theHC.theVisitControl.isGovOfficalPlanMapping(thePaymentNow.plan_kid);
        // ��������Ңͧ�Է�Ի�Сѹ�ѧ��
        boolean isSocialsecPlan = thePaymentNow.plan_kid == null ? false : theHC.theVisitControl.isSocialsecPlanMapping(thePaymentNow.plan_kid);
        if (thePaymentNow.getObjectId() != null) {
            // �Է�Ի�Сѹ
            if (thePaymentNow.plan_kid == null ? false : theHC.theVisitControl.isInsuranceMapping(thePaymentNow.plan_kid)) {
                btnShowInsurancePlan.setSelected(false);
                btnShowInsurancePlan.setVisible(true);
                tmcds.setComplexDataSources(theHC.theVisitControl.listDSInsurancePlanByVisitPaymentId(thePaymentNow.getObjectId()));
                tmcds.fireTableDataChanged();
                btnAddInsurancePlan.setEnabled(true);
                btnDelInsurancePlan.setEnabled(false);
                btnUpInsurancePlan.setEnabled(false);
                btnDownInsurancePlan.setEnabled(false);
            }
        }
        dateComboBoxFrom.setText(DateUtil.convertFieldDate(thePaymentNow.card_ins_date));
        // not default exp date
        if (thePaymentNow.card_exp_date == null
                || thePaymentNow.card_exp_date.isEmpty()) {
            dateComboBoxTo.clearText();
        } else {
            dateComboBoxTo.setText(DateUtil.convertFieldDate(thePaymentNow.card_exp_date));
        }
        ComboboxModel.setCodeComboBox(jComboBoxTariff, thePaymentNow.b_tariff_id);
        jTextFieldCardID.setText(thePaymentNow.card_id);
        jLabelMoneyLimit.setText(thePaymentNow.money_limit);
        Office office = theHC.theLookupControl.readHospitalByCode(thePaymentNow.hosp_main);
        String hmain;
        String idmain;
        if (office != null) {
            hmain = office.getName();
            idmain = office.getObjectId();
        } else {
            // incase of social security webservice data from ʻʪ have no parameter of primary hospital so set it to current site
            idmain = theHO.theSite.off_id;
            Office officeSocialSecurity = theHC.theLookupControl.readHospitalByCode(idmain);
            hmain = officeSocialSecurity.getName();
            idmain = officeSocialSecurity.getCode();

        }
        office = theHC.theLookupControl.readHospitalByCode(thePaymentNow.hosp_sub);
        String hsub = "";
        String idsub = "";
        if (office != null) {
            hsub = office.getName();
            idsub = office.getObjectId();
        }
        office = theHC.theLookupControl.readHospitalByCode(thePaymentNow.hosp_reg);
        String hReg = "";
        String idReg = "";
        if (office != null) {
            hReg = office.getName();
            idReg = office.getObjectId();
        }
        txtHosRefer.setText(hmain);
        txtHosPrimary.setText(hsub);
        txtHosReg.setText(hReg);
        txtHosReferCode.setText(idmain);
        txtHosPrimaryCode.setText(idsub);
        txtHosRegCode.setText(idReg);
        Plan plan = theHC.theLookupControl.readPlanById(thePaymentNow.plan_kid);
        String title_border = GuiLang.setLanguage("��س����͡�Է�ԡ���ѡ��");
        if (plan != null) {
            title_border = GuiLang.setLanguage("�Է��") + " : " + plan.description;
        }
        this.jLabelPlan.setText(title_border);

        if (isGovOfficalPlan) {
            if (thePaymentNow.visitGovOfficalPlan == null) {
                thePaymentNow.visitGovOfficalPlan = theHC.theVisitControl.getVisitGovOfficalPlanByPaymentId(thePaymentNow.getObjectId());
            }
            panelExtraGovPlan.setVisible(true);
            cbGovType.setSelectedIndex(0);
            this.doSetVisitGovOfficalPlan();
        } else {
            thePaymentNow.visitGovOfficalPlan = null;
        }

        if (isSocialsecPlan) {
            if (thePaymentNow.visitSocialsecPlan == null) {
                thePaymentNow.visitSocialsecPlan = theHC.theVisitControl.getVisitSocialsecPlanByPaymentId(thePaymentNow.getObjectId());
            }
            panelExtraSocialsecPlan.setVisible(true);
            this.doSetVisitSocialsecPlan();
        } else {
            thePaymentNow.visitSocialsecPlan = null;
        }
    }

    private void doSetVisitGovOfficalPlan() {
        if (thePaymentNow.visitGovOfficalPlan == null) {
            thePaymentNow.visitGovOfficalPlan = new VisitGovOfficalPlan();
        }
        cbGovType.setSelectedIndex(thePaymentNow.visitGovOfficalPlan.govoffical_type);
        txtGovNo.setText(thePaymentNow.visitGovOfficalPlan.govoffical_number);
        doFindGovName(thePaymentNow.visitGovOfficalPlan.f_govcode_id);
        txtOwnname.setText(thePaymentNow.visitGovOfficalPlan.ownname);
        txtOwnrPID.setText(thePaymentNow.visitGovOfficalPlan.ownrpid);
        Gutil.setGuiData(cbSubinscl, thePaymentNow.visitGovOfficalPlan.f_subinscl_id != null || !thePaymentNow.visitGovOfficalPlan.f_subinscl_id.isEmpty()
                ? thePaymentNow.visitGovOfficalPlan.f_subinscl_id
                : "01"); // default ����Ҫ���
        Gutil.setGuiData(cbRelinscl, thePaymentNow.visitGovOfficalPlan.f_relinscl_id != null || !thePaymentNow.visitGovOfficalPlan.f_relinscl_id.isEmpty()
                ? thePaymentNow.visitGovOfficalPlan.f_relinscl_id
                : "0"); // default ���ͧ
    }

    private void doFindGovName(String code) {
        GovCode office = code != null && !code.isEmpty()
                ? theHC.theLookupControl.readGovCodeByCode(code)
                : null;
        txtGovCode.setText(office != null ? office.getCode() : "");
        txtGovName.setText(office != null ? office.getName() : "");
    }

    private void doSetVisitSocialsecPlan() {
        if (thePaymentNow.visitSocialsecPlan == null) {
            thePaymentNow.visitSocialsecPlan = new VisitSocialsecPlan();
        }
        txtSocialsecNo.setText(thePaymentNow.visitSocialsecPlan.socialsec_number);
    }

    /*
     * �ӡ��update���� object plan ���Ѻ GUI
     */
    private void setPlanV(Vector pv) {
        this.thePlan = pv;
        TaBleModel tm;
        jTableVisitPayment.getSelectionModel().clearSelection();
        jTablePatientPayment.getSelectionModel().clearSelection();
        if (thePlan == null) {
            thePlan = new Vector();
            tm = new TaBleModel(column_jTablePlan, 0);
            jTablePlan.setModel(tm);
            return;
        }
        tm = new TaBleModel(column_jTablePlan, thePlan.size());
        for (int i = 0, size = thePlan.size(); i < size; i++) {
            Plan plan = (Plan) thePlan.get(i);
            tm.setValueAt(plan.description, i, 0);
            if (i == 0) {
                setPlan(plan);
            }
        }
        jTablePlan.setModel(tm);
        jTablePlan.getColumnModel().getColumn(0).setPreferredWidth(300);
        if (thePlan.size() > 0) {
            jTablePlan.setRowSelectionInterval(0, 0);
        }
    }

    private void getVisit(Visit visit) {
        visit.visit_note = jTextArea1.getText();
        visit.refer_in = rdVisitType3.isSelected() ? integerTextFieldHosRefer.getText() : "";
        visit.f_visit_service_type_id = rdVisitType1.isSelected() ? "1"
                : rdVisitType2.isSelected() ? "2"
                : rdVisitType3.isSelected() ? "3"
                : rdVisitType4.isSelected() ? "4"
                : "5";
        if (this.jCheckBoxVisitDate.isSelected()) {
            visit.begin_visit_time = this.dateComboBoxCheck.getText() + "," + this.timeTextFieldCheck.getText() + ":00";
        }
        theVisit.service_location = jRadioButtonIn.isSelected() ? "1" : "2";
    }

    /*
     * �ӡ�� update ������object patientpayment ���ѺGUI
     *
     */
    private void setPatientPaymentV(Vector pp) {
        this.vPatientPayment = pp;
        TaBleModel tm;
        jTableVisitPayment.getSelectionModel().clearSelection();
        jTablePlan.getSelectionModel().clearSelection();
        if ((vPatientPayment == null)) {
            vPatientPayment = new Vector();
            tm = new TaBleModel(column_jTablePatientPayment, 0);
            jTablePatientPayment.setModel(tm);
            return;
        }
        tm = new TaBleModel(column_jTablePatientPayment, vPatientPayment.size());
        for (int i = 0, size = vPatientPayment.size(); i < size; i++) {
            PatientPayment pd = (PatientPayment) vPatientPayment.get(i);
            String p = theHC.theLookupControl.readPlanString(pd.plan_kid);
            tm.setValueAt(pd.card_id, i, 0);
            tm.setValueAt(p, i, 1);
            String cpd = pd.checkplan_date;
            // Somprasong �Դ exception ����͢��������繤����ҧ
            if ((cpd != null && !cpd.isEmpty()) && cpd.length() == 10) {
                cpd = cpd.substring(8, 10) + "/" + cpd.substring(5, 7) + "/" + cpd.substring(2, 4);
            }
            tm.setValueAt(cpd, i, 2);
        }
        jTablePatientPayment.setModel(tm);
        jTablePatientPayment.getColumnModel().getColumn(0).setPreferredWidth(120);
        jTablePatientPayment.getColumnModel().getColumn(1).setPreferredWidth(230);
        jTablePatientPayment.getColumnModel().getColumn(2).setPreferredWidth(100);
    }

    /*
     * �ӡ�� update ���� object Visitpayment ���Ѻ Gui
     */
    private void setVisitPaymentV(Vector v) {
        this.vVisitPayment = v;
        if ((vVisitPayment == null) || vVisitPayment.isEmpty()) {
            vVisitPayment = new Vector();
            TaBleModel tm = new TaBleModel(column_jTableVisitPayment, 0);
            jTableVisitPayment.setModel(tm);
            return;
        }
        TaBleModel tm = new TaBleModel(column_jTableVisitPayment, vVisitPayment.size());
        for (int i = 0, size = vVisitPayment.size(); i < size; i++) {
            Payment pd = ((Payment) vVisitPayment.get(i));
            tm.setValueAt(pd.card_id, i, 0);
            Plan plan = theHC.theLookupControl.readPlanById(pd.plan_kid);
            tm.setValueAt(theHC.theVisitControl.getTextVisitPayment(pd, plan), i, 1);
            tm.setValueAt(theHC.theLookupControl.readContractString(pd.contract_kid), i, 2);
        }
        jTableVisitPayment.setModel(tm);
        jTableVisitPayment.getColumnModel().getColumn(0).setPreferredWidth(200);
        jTableVisitPayment.getColumnModel().getColumn(0).setCellRenderer(theCellRendererTooltip);
        jTableVisitPayment.getColumnModel().getColumn(1).setPreferredWidth(300);
        jTableVisitPayment.getColumnModel().getColumn(1).setCellRenderer(theCellRendererVisitPayment);
        jTableVisitPayment.getColumnModel().getColumn(2).setPreferredWidth(300);
    }

    private void setVisit(Visit v) {
        jTextFieldHosRefer.setText("");
        jTextArea1.setText("");
        integerTextFieldHosRefer.setText("");
        rdVisitType1.setSelected(true);
        jRadioButtonIn.setSelected(true);
        theVisit = v;
        if (theVisit == null) {
            if (theHO.thePatient == null && theHO.theFamily == null) {
                setEnabled(false);
            } else {
                setEnabled(true);
            }
            return;
        }
        if (theVisit.service_location.equals("1")) {
            jRadioButtonIn.setSelected(true);
        } else {
            jRadioButtonOut.setSelected(true);
        }
        if (theVisit.f_visit_service_type_id.equals("1")) {
            rdVisitType1.setSelected(true);
        } else if (theVisit.f_visit_service_type_id.equals("2")) {
            rdVisitType2.setSelected(true);
        } else if (theVisit.f_visit_service_type_id.equals("3")) {
            rdVisitType3.setSelected(true);
            Office office = theHC.theLookupControl.readHospitalByCode(theVisit.refer_in);
            if (office != null) {
                jTextFieldHosRefer.setText(office.getName());
                integerTextFieldHosRefer.setText(office.getCode());
            }
        } else if (theVisit.f_visit_service_type_id.equals("4")) {
            rdVisitType4.setSelected(true);
        } else {
            rdVisitType5.setSelected(true);
        }

        jTextArea1.setText(theVisit.visit_note);
        if (theVisit.isLockingByOther(theHO.theEmployee.getObjectId())
                || theVisit.isDischargeMoney()
                || theVisit.isDropVisit()) {
            setEnabled(false);
            return;
        }
        dateComboBoxCheck.setText(DateUtil.convertFieldDate(theVisit.begin_visit_time.substring(0, 10)));
        timeTextFieldCheck.setText(theVisit.begin_visit_time.substring(11, 16));
        setEnabled(true);
        validateVisitTypeUI();
        setEnabledVisit(false);
    }

    /*
     * ��˹����૵����
     */
    public void setEnabledPPayment(boolean author) {
        jButtonDeletePp.setEnabled(author);
        jButtonSavePtPayment.setEnabled(author);
        jButtonDown1.setEnabled(author);
        jButtonUp2.setEnabled(author);
        jButtonSaveVtPayment.setEnabled(author);
    }

    @Override
    public void setEnabled(boolean author) {
        jButtonPrintOPDCard.setEnabled(author);
        jTextArea1.setEnabled(author);
        setEnabledPPayment(author);
        setEnabledVPayment(author);
        setEnabledVisit(author);
    }

    public void setEnabledVisit(boolean author) {
        jButtonHosReferIn.setEnabled(author);
        jTextFieldHosRefer.setEnabled(author);
        jButtonVisit.setEnabled(author);
        jTextArea1.setEnabled(author);
        integerTextFieldHosRefer.setEnabled(author);
        jCheckBoxVisitDate.setEnabled(author);
        dateComboBoxCheck.setEnabled(jCheckBoxVisitDate.isSelected());
        timeTextFieldCheck.setEnabled(jCheckBoxVisitDate.isSelected());
        jRadioButtonIn.setEnabled(author);
        jRadioButtonOut.setEnabled(author);
        rdVisitType1.setEnabled(author);
        rdVisitType2.setEnabled(author);
        rdVisitType3.setEnabled(author);
        rdVisitType4.setEnabled(author);
        rdVisitType5.setEnabled(author);
    }

    public void setEnabledVPayment(boolean author) {
        txtHosReferCode.setEnabled(author);
        txtHosPrimaryCode.setEnabled(author);
        txtHosRegCode.setEnabled(author);
        boolean isEnableTariff = "1".equals(theHC.theLookupControl.readOption(false).enable_tariff);
        jComboBoxTariff.setEnabled(author && isEnableTariff);
        jTextFieldCardID.setEnabled(author);
        dateComboBoxFrom.setEnabled(author);
        dateComboBoxTo.setEnabled(author);
        jButtonHosReferIn.setEnabled(author);
        txtHosRefer.setEnabled(author);
        txtHosPrimary.setEnabled(author);
        txtHosReg.setEnabled(author);
        jButtonHosMain.setEnabled(author);
        jButtonHosSub.setEnabled(author);
        jButtonHosReg.setEnabled(author);
        jButtonSaveVisitPayment.setEnabled(author);
        jButtonDelete.setEnabled(author);
        jButtonUp.setEnabled(author);
        jButtonDown.setEnabled(author);
        jCheckBoxShowCancel.setEnabled(author);
        jTableVisitPayment.setEnabled(author);
        jButtonB1.setEnabled(author);
    }

    public void setHosObject(HosObject ho) {
        jCheckBoxVisitDateActionPerformed(null);
        setPayment(null);
        if (ho.thePatient != null) {
            setPatient(ho.thePatient);
        } else {
            setFamily(ho.theFamily);
        }
        setVisit(ho.theVisit);
        setPatientPaymentV(ho.vPatientPayment);
        setVisitPaymentV(ho.vVisitPayment);
        // check  button plugin isvisble when select patient or visit
        for (PanelButtonPluginProvider buttonPluginProvider : buttonPluginProviders) {
            buttonPluginProvider.getButton(PanelVisit.class).setVisible(buttonPluginProvider.isVisible());
        }
        //�ҡ�繼����·�����������㹡�кǹ��á������Է��������ʴ������繹�
        if (thePatient != null && theVisit == null) {
            Vector vv = theHC.thePatientControl.listOldPaymentVisitBypatientID(thePatient.getObjectId());
            for (int i = 0; i < vv.size(); i++) {
                Payment pm = (Payment) vv.get(i);
                pm.setObjectId(null);
            }
            setVisitPaymentV(vv);
        }
    }

    public void setPatient(Patient p) {
        thePatient = p;
        setEnabled(thePatient != null);
    }

    public void setFamily(Family f) {
        theFamily = f;
        setEnabled(theFamily != null);
    }

    /*
     * clean function instant
     */
    public void gc() {
        theHC.theHS.thePatientSubject.detach(this);
        theHC.theHS.theVisitSubject.detach(this);
        theHC.theHS.theVPaymentSubject.detach(this);
        theHC.theHS.theServiceLoaderSubject.detach(this);
    }

    @Override
    public void notifyAdmitVisit(String str, int status) {
    }

    @Override
    public void notifyDischargeDoctor(String str, int status) {
        setHosObject(theHO);
    }

    @Override
    public void notifyObservVisit(String str, int status) {
    }

    @Override
    public void notifyUnlockVisit(String str, int status) {
        setHosObject(theHO);
        jCheckBoxVisitDate.setSelected(false);
        dateComboBoxCheck.setEnabled(false);
        timeTextFieldCheck.setEnabled(false);
        dateComboBoxCheck.setText(DateUtil.convertFieldDate(theHO.date_time));
        timeTextFieldCheck.setText(theHO.date_time.substring(11));
    }

    @Override
    public void notifyVisitPatient(String str, int status) {
        setHosObject(theHO);
    }

    @Override
    public void notifyManageDrugAllergy(String str, int status) {
    }

    @Override
    public void notifySavePatientPayment(String str, int status) {
        setPayment(null);
        vPatientPayment = theHC.thePatientControl.listPatientPayment();
        setPatientPaymentV(vPatientPayment);
    }

    @Override
    public void notifyReadVisit(String str, int status) {
        setHosObject(theHO);
        jButtonSearchAllPlanActionPerformed(null);
    }

    @Override
    public void notifyCheckDoctorTreament(String msg, int state) {
    }

    @Override
    public void notifyDropVisit(String str, int status) {
        setHosObject(theHO);
    }

    @Override
    public void notifySendVisit(String str, int status) {
        setHosObject(theHO);
    }

    @Override
    public void notifyDischargeFinancial(String str, int status) {
        setHosObject(theHO);
    }

    @Override
    public void notifyReverseFinancial(String str, int status) {
        setHosObject(theHO);
    }

    @Override
    public void notifyReverseDoctor(String str, int status) {
    }

    @Override
    public void notifyDeletePatientPayment(String str, int status) {
        setPayment(null);
        vPatientPayment = theHC.thePatientControl.listPatientPayment();
        setPatientPaymentV(vPatientPayment);
    }

    @Override
    public void notifyDeleteVisitPayment(String str, int status) {
        setPayment(null);
        setVisitPaymentV(theHO.vVisitPayment);
    }

    @Override
    public void notifyDeletePatient(String str, int status) {
        setHosObject(theHO);
    }

    @Override
    public void notifyReadPatient(String str, int status) {
        setHosObject(theHO);
    }

    @Override
    public void notifyReadFamily(String str, int status) {
        setHosObject(theHO);
    }

    @Override
    /**
     * Creates a new instance of createPatientAllergy
     */
    public void notifySavePatient(String str, int status) {
        setHosObject(theHO);
    }

    @Override
    public void notifyRemainDoctorDischarge(String str, int status) {
        setHosObject(theHO);
    }

    @Override
    public void notifySendVisitBackWard(String str, int status) {
        setHosObject(theHO);
    }

    @Override
    public void notifyDeleteVPayment(String str, int status) {
        this.setVisitPaymentV(theHO.vVisitPayment);
    }

    @Override
    public void notifyDownVPaymentPriority(String str, int status) {
        this.setVisitPaymentV(theHO.vVisitPayment);
        // xxx
    }

    @Override
    public void notifySaveVPayment(String str, int status) {
        this.setVisitPaymentV(theHO.vVisitPayment);
    }

    @Override
    public void notifyUpVPaymentPriority(String str, int status) {
        this.setVisitPaymentV(theHO.vVisitPayment);
    }

    @Override
    public void notifySaveAppointment(String str, int status) {
    }

    @Override
    public void notifyReverseAdmit(String str, int status) {
    }

    @Override
    public void notifyResetPatient(String str, int status) {
        setHosObject(theHO);
    }

    @Override
    public void notifyAdmitLeaveHome(String str, int status) {
    }

    @Override
    public void notifySaveInformIntolerance(Object... object) {
        //
    }

    private void validateVisitTypeUI() {
        panelReferHCODE.setVisible(rdVisitType3.isSelected());
    }

    private void doAddInsurancePlan() {
        List<DataSource> sources = dialogSearchInsurancePlan.openDialog();
        List<VisitInsurancePlan> list = new ArrayList<VisitInsurancePlan>();
        for (DataSource dataSource : sources) {
            VisitInsurancePlan vip = new VisitInsurancePlan();
            FinanceInsurancePlan fip = (FinanceInsurancePlan) dataSource.getId();
            vip.b_finance_insurance_company_id = fip.b_finance_insurance_company_id;
            vip.b_finance_insurance_plan_id = fip.getObjectId();
            vip.t_visit_payment_id = thePaymentNow.getObjectId();
            list.add(vip);
        }
        if (!list.isEmpty()) {
            theHC.theVisitControl.addInsurancePlans(list);
            tmcds.clearTable();
            tmcds.setComplexDataSources(theHC.theVisitControl.listDSInsurancePlanByVisitPaymentId(thePaymentNow.getObjectId()));
            tmcds.fireTableDataChanged();
        }
    }

    private void doDelInsurancePlan() {
        VisitInsurancePlan vip = (VisitInsurancePlan) tmcds.getSelectedId(tableInsurancePlan.getSelectedRow());
        theHC.theVisitControl.deleteInsurancePlan(vip);
        tmcds.clearTable();
        tmcds.setComplexDataSources(theHC.theVisitControl.listDSInsurancePlanByVisitPaymentId(thePaymentNow.getObjectId()));
        tmcds.fireTableDataChanged();
    }

    private void doUpInsurancePlan() {
        int row = tableInsurancePlan.getSelectedRow();
        ComplexDataSource get = tmcds.getData().get(row);
        tmcds.getData().remove(row);
        tmcds.getData().add(row - 1, get);
        tmcds.fireTableDataChanged();
        tableInsurancePlan.setRowSelectionInterval(row - 1, row - 1);
        doSelectedInsurancePlan();
        repriorityInsurancePlan();
    }

    private void doDownInsurancePlan() {
        int row = tableInsurancePlan.getSelectedRow();
        ComplexDataSource get = tmcds.getData().get(row);
        tmcds.getData().remove(row);
        tmcds.getData().add(row + 1, get);
        tmcds.fireTableDataChanged();
        tableInsurancePlan.setRowSelectionInterval(row + 1, row + 1);
        doSelectedInsurancePlan();
        repriorityInsurancePlan();
    }

    private void repriorityInsurancePlan() {
        List<ComplexDataSource> data = tmcds.getData();
        List<VisitInsurancePlan> vips = new ArrayList<VisitInsurancePlan>();
        for (ComplexDataSource complexDataSource : data) {
            vips.add((VisitInsurancePlan) complexDataSource.getId());
        }
        theHC.theVisitControl.repriorityInsurancePlan(vips);
    }

    private void doSelectedInsurancePlan() {
        btnDelInsurancePlan.setEnabled(tableInsurancePlan.getSelectedRowCount() > 0);
        btnUpInsurancePlan.setEnabled(tableInsurancePlan.getSelectedRowCount() > 0 && tableInsurancePlan.getSelectedRow() != 0);
        btnDownInsurancePlan.setEnabled(tableInsurancePlan.getSelectedRowCount() > 0 && tableInsurancePlan.getSelectedRow() != tableInsurancePlan.getRowCount() - 1);
    }

    private void doSelectGovType() {
        int index = cbGovType.getSelectedIndex();
        switch (index) {
            case 1:
                txtGovNo.setEnabled(true);
                panelExtraGovPlanDetail.setVisible(false);
                break;
            case 2:
                txtGovNo.setEnabled(true);
                panelExtraGovPlanDetail.setVisible(true);
                txtGovNo.requestFocus();
                break;
            default:
                txtGovNo.setEnabled(false);
                panelExtraGovPlanDetail.setVisible(false);
                txtGovNo.requestFocus();
                break;
        }
    }

    private void doVisitPatient() {
        if (theHO.theVisit != null) {
            theUS.setStatus("�������������кǹ�������", UpdateStatus.WARNING);
            return;
        }
        //����Ѻ��Ъҡ÷���ѧ������繼�����
        if (theHO.thePatient != null && theHO.thePatient.discharge_status_id.equals(Dischar.DEATH)) {
            theUS.setStatus("���������ª��Ե�����������ö�Ӽ������������кǹ�����", UpdateStatus.WARNING);
            return;
        }
        //����Ѻ��Ъҡ÷���ѧ������繼�����
        if (theHO.thePatient != null && theHO.thePatient.active.equals("0")) {
            theUS.setStatus("UC:�����ż����¶١¡��ԡ�����������ö�Ӽ������������кǹ�����", UpdateStatus.WARNING);
            return;
        }
        if (vVisitPayment == null || vVisitPayment.isEmpty()) {
            Plan plan = theHC.theLookupControl.readPlanById(Plan.SELF_PAY);
            if (plan == null) {
                theUS.setStatus("��سҺѹ�֡�Է�ԡ���ѡ�ҡ�͹ �Ӽ������������кǹ���", UpdateStatus.WARNING);
                return;
            }
            setPlan(plan);
            this.doSaveVisitPayment();
        }
        if (jCheckBoxVisitDate.isSelected() && theOption.future_date_visit.equals("0")) {
            int date_valid = DateUtil.countDateDiff(dateComboBoxCheck.getText(), theHO.date_time);
            if (date_valid > 0) {
                theUS.setStatus("�ѹ��� Visit ��ͧ������ѹ����͹Ҥ�", UpdateStatus.WARNING);
                return;
            }
        }
        theVisit = new Visit();
        getVisit(theVisit);
        QueueVisit qv = new QueueVisit();
        //amp:25/02/2549 ������ uc ��� order ��ǧ˹�Ҩҡ��ùѴ ������Ǣ�ͧ �֧��ͧ�ʴ��ء����
        Vector vWaitAppointment = null;
        //����Ѻ��Ъҡ÷���ѧ������繼�����
        if (theHO.thePatient != null) {
            vWaitAppointment = theHC.thePatientControl.listWaitAppointment(theHO.thePatient.getObjectId());
        }
        Vector chooseApp = new Vector();
        if (IOption || vWaitAppointment != null) {
            boolean ret = theHD.showDialogQueueVisit(theVisit, qv, 0, IOption, vWaitAppointment, chooseApp);
            if (!ret) {
                return;
            }
        }
        boolean isComplete;
        if (chooseApp.isEmpty()) {
            isComplete = theHC.theVisitControl.visitPatient(theVisit, vVisitPayment, qv);
        } else {
            Appointment app = (Appointment) chooseApp.get(0);
            isComplete = theHC.theVisitControl.visitPatientApp(theVisit, vVisitPayment, qv, app);
        }
        setEnabledVisit(!isComplete);
    }

    private void doSearchHosPrimaryFromCode() {
        if (txtHosPrimaryCode.getText().trim().length() == 5) {
            Office office = theHC.theLookupControl.readHospitalByCode(txtHosPrimaryCode.getText());
            if (office == null) {
                txtHosPrimary.setText("");
                txtHosPrimaryCode.requestFocus();
                txtHosPrimaryCode.selectAll();
                theUS.setStatus("��辺ʶҹ��Һ�Ż�����Է��ç�Ѻ���ʷ���к� ��سҵ�Ǩ�ͺ�����ա����", UpdateStatus.WARNING);
                return;
            }
            if (office.getObjectId() != null) {
                txtHosPrimaryCode.setText(office.getCode());
                txtHosPrimary.setText(office.name);
            }
        }
    }

    private void doSearchHosReferFromCode() {
        if (txtHosReferCode.getText().trim().length() == 5) {
            Office office = theHC.theLookupControl.readHospitalByCode(txtHosReferCode.getText());
            if (office == null) {
                txtHosRefer.setText("");
                txtHosReferCode.requestFocus();
                txtHosReferCode.selectAll();
                theUS.setStatus("��辺ʶҹ��Һ�ŷ���Ѻ����觵�ͷ��ç�Ѻ���ʷ���к� ��سҵ�Ǩ�ͺ�����ա����", UpdateStatus.WARNING);
                return;
            }
            if (office.getObjectId() != null) {
                txtHosReferCode.setText(office.getCode());
                txtHosRefer.setText(office.name);
            }
        }
    }

    private void doSearchHosRegFromCode() {
        if (txtHosRegCode.getText().trim().length() == 5) {
            Office office = theHC.theLookupControl.readHospitalByCode(txtHosRegCode.getText());
            if (office == null) {
                txtHosReg.setText("");
                txtHosRegCode.requestFocus();
                txtHosRegCode.selectAll();
                theUS.setStatus("��辺ʶҹ��Һ�Ż�Шӷ��ç�Ѻ���ʷ���к� ��سҵ�Ǩ�ͺ�����ա����", UpdateStatus.WARNING);
                return;
            }
            if (office.getObjectId() != null) {
                txtHosRegCode.setText(office.getCode());
                txtHosReg.setText(office.name);
            }
        }
    }

    @Override
    public void notifyUpdatePatientPictureProfile(Object obj) {

    }

    @Override
    public void notifyLoaderService() {
        this.serviceLoader();
    }

    private final List<PanelButtonPluginProvider> buttonPluginProviders = new ArrayList<>();

    private void serviceLoader() {
        panelButtonPlugin.removeAll();
        Iterator<PanelButtonPluginProvider> iterator = PanelButtonPluginManager.getInstance().getServices();
        String className = this.getClass().getName();
        // get buttons to list
        List<JButton> buttons = new ArrayList<>();
        while (iterator.hasNext()) {
            PanelButtonPluginProvider provider = iterator.next();
            if (provider.isOwnerPanel(PanelVisit.class)) {
                JButton button = provider.getButton(PanelVisit.class);
                boolean isVisible = provider.isVisible();
                LOG.log(java.util.logging.Level.INFO, "{0} get button from {1}, visible: {2}", new Object[]{className, provider.getClass().getName(), isVisible});
                button.setVisible(isVisible);
                button.setMaximumSize(new Dimension(provider.getButtonWidth(), provider.getButtonHeight()));
                button.setMinimumSize(new Dimension(provider.getButtonWidth(), provider.getButtonHeight()));
                button.setPreferredSize(new Dimension(provider.getButtonWidth(), provider.getButtonHeight()));
                buttonPluginProviders.add(provider);
                int index = provider.getIndex();
                if (index > buttons.size()) {
                    buttons.add(button);
                } else {
                    buttons.add(index, button);
                }
            }
        }
        if (buttons.isEmpty()) {
            LOG.log(java.util.logging.Level.INFO, "{0}  no button plugin!", className);
        } else {
            LOG.log(java.util.logging.Level.INFO, "{0}  found {1} buttons plugin!", new Object[]{className, buttons.size()});
        }
        // add all plugin buttons to panel
        int iCount = 0;
        for (JButton button : buttons) {
            GridBagConstraints gridBagConstraints = new java.awt.GridBagConstraints();
            gridBagConstraints.gridx = iCount;
            gridBagConstraints.gridy = 0;
            gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
            gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
            gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
            if (iCount == 0) {
                gridBagConstraints.weightx = 1.0;
            }
            panelButtonPlugin.add(button, gridBagConstraints);
            iCount++;
//            LOG.info(className + "  add: " + button.toString() + "\n"
//                    + "gridx:" + gridBagConstraints.gridx
//                    + ", gridy:" + gridBagConstraints.gridy
//                    + ", weightx:" + gridBagConstraints.weightx
//                    + ", weighty:" + gridBagConstraints.weighty);
        }
        panelButtonPlugin.revalidate();
        panelButtonPlugin.repaint();
    }

    private void doSaveVisitPayment() {
        saveVisitPayment(getPayment());
        if (vPatientPayment == null || this.vPatientPayment.isEmpty()) {
            theHC.thePatientControl.savePatientPayment(vPatientPayment, thePaymentNow);
        }
    }
}

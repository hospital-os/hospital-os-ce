package com.hospital_os.objdb;

import com.hospital_os.object.CategoryGroupItem;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.Gutil;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public class CategoryGroupItemDB {

    public ConnectionInf theConnectionInf;
    public CategoryGroupItem dbObj;
    final private String idtable = "130";/*
     * "124";
     */


    /**
     * @param db
     * @roseuid 3F65897F0326
     */
    public CategoryGroupItemDB(ConnectionInf db) {
        theConnectionInf = db;
        dbObj = new CategoryGroupItem();
        initConfig();
    }

    private boolean initConfig() {
        dbObj.table = "b_item_subgroup";
        dbObj.pk_field = "b_item_subgroup_id";
        dbObj.category_group_item_id = "item_subgroup_number";
        dbObj.description = "item_subgroup_description";
        dbObj.category_group_code = "f_item_group_id";
        dbObj.active = "item_subgroup_active";
        return true;
    }

    /**
     * @param o
     * @return int
     * @throws Exception
     * @roseuid 3F6574DE0394
     */
    public int insert(CategoryGroupItem o) throws Exception {
        CategoryGroupItem p = o;
        p.generateOID(idtable);
        StringBuffer sql = new StringBuffer("insert into ").append(dbObj.table).append(" (").
                append(dbObj.pk_field).
                append(" ,").append(dbObj.category_group_item_id).
                append(" ,").append(dbObj.description).
                append(" ,").append(dbObj.category_group_code).
                append(" ,").append(dbObj.active).
                append(" ) values('").
                append(p.getObjectId()).
                append("','").append(Gutil.CheckReservedWords(p.category_group_item_id)).
                append("','").append(Gutil.CheckReservedWords(p.description)).
                append("','").append(p.category_group_code).
                append("','").append(p.active).append("')");
        return theConnectionInf.eUpdate(sql.toString());
    }

    public int update(CategoryGroupItem o) throws Exception {
        CategoryGroupItem p = o;
        StringBuffer sql = new StringBuffer("update ").append(dbObj.table).
                append(" set ").
                append(dbObj.category_group_item_id).append("='").append(Gutil.CheckReservedWords(p.category_group_item_id)).
                append("', ").append(dbObj.description).append("='").append(Gutil.CheckReservedWords(p.description)).
                append("', ").append(dbObj.category_group_code).append("='").append(p.category_group_code).
                append("', ").append(dbObj.active).append("='").append(p.active).
                append("' where ").append(dbObj.pk_field).append("='").append(p.getObjectId()).append("'");
        return theConnectionInf.eUpdate(sql.toString());
    }

    /*
     * ////////////////////////////////////////////////////////////////////////////
     */
    public int delete(CategoryGroupItem o) throws Exception {
        StringBuffer sql = new StringBuffer("delete from ").append(dbObj.table).
                append(" where ").append(dbObj.pk_field).append("='").append(o.getObjectId()).append("'");
        return theConnectionInf.eUpdate(sql.toString());
    }

    /*
     * ////////////////////////////////////////////////////////////////////////////
     */
    public CategoryGroupItem selectByPK(String pk) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.pk_field).append(" = '").append(pk).append("'");
        Vector v = eQuery(sql.toString());
        if (v.isEmpty()) {
            return null;
        } else {
            return (CategoryGroupItem) v.get(0);
        }
    }

    /*
     * ////////////////////////////////////////////////////////////////////////////
     */
    public CategoryGroupItem selectByCode(String code) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").
                append(dbObj.category_group_item_id).append(" = '").append(Gutil.CheckReservedWords(code)).append("'");
        Vector v = eQuery(sql.toString());
        if (v.isEmpty()) {
            return null;
        } else {
            return (CategoryGroupItem) v.get(0);
        }
    }

    /*
     * ////////////////////////////////////////////////////////////////////////////
     */
    public Vector selectByCategoryGroupCode(String cgc) throws Exception {
        return selectByCategoryGroupCode(cgc, "");
    }

    public Vector selectByCategoryGroupCode(String cgc, String active) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table);
        if (!cgc.isEmpty()) {
            sql.append(" where ").append(dbObj.category_group_code).append(" = '").append(cgc).append("'");
        }

        if (!active.isEmpty()) {
            sql.append(" and ").append(dbObj.active).append(" = '").append(active).append("'");
        }

        sql.append(" order by ").append(dbObj.category_group_item_id);
        return eQuery(sql.toString());
    }

    public Vector selectAllByName(String pk, String active) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).append(" where ");
        if (!pk.isEmpty()) {
            pk = Gutil.CheckReservedWords(pk);
            sql.append("(").append(dbObj.category_group_item_id).append(" like '%").append(pk).append("%'").
                    append(" or UPPER(").append(dbObj.description).append(") like UPPER('%").append(pk).append("%')").
                    append(") and ");
        }
        sql.append(dbObj.active).append(" = '").append(active).append("'").append("order by ").append(dbObj.category_group_item_id);

        Vector v = eQuery(sql.toString());
        if (v.isEmpty()) {
            return null;
        } else {
            return v;
        }
    }

    public Vector selectEqName(String pk, String active) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).append(" where ");
        if (!pk.isEmpty()) {
            pk = Gutil.CheckReservedWords(pk);
            sql.append("(UPPER(").append(dbObj.category_group_item_id).append(") = UPPER('").append(pk).append("')").append("").append(
                    " or UPPER(").append(dbObj.description).append(") = UPPER('").append(pk).append("')").append(") and ");
        }
        sql.append(dbObj.active).append(" = '").append(active).append("'").append("order by ").append(dbObj.category_group_item_id);
        return eQuery(sql.toString());
    }

    /*
     * ////////////////////////////////////////////////////////////////////////////
     */
    public Vector selectAll() throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.active).append(" = '1'").append(" order by ").append(dbObj.description);
        Vector v = eQuery(sql.toString());
        if (v.isEmpty()) {
            return null;
        } else {
            return v;
        }

    }

    public Vector selectDrugAndSupply() throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).
                append(" where ").append(dbObj.category_group_code).append(" = '1' or ").append(dbObj.category_group_code).
                append(" = '4' order by ").append(dbObj.category_group_item_id);

        Vector v = eQuery(sql.toString());
        if (v.isEmpty()) {
            return null;
        } else {
            return v;
        }
    }

//    /**
//     * @deprecated henbe unused
//     *
//     */
//    public Vector veQuery(String sql) throws Exception {
//        ComboFix p;
//        Vector list = new Vector();
//        ResultSet rs = theConnectionInf.eQuery(sql.toString());
//        while (rs.next()) {
//            p = new ComboFix();
//            p.code = rs.getString(dbObj.pk_field);
//            p.name = rs.getString(dbObj.description);
//            list.add(p);
//        }
//        rs.close();
//        return list;
//    }

    /*
     * ////////////////////////////////////////////////////////////////////////////
     */
    public Vector eQuery(String sql) throws Exception {
        CategoryGroupItem p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        while (rs.next()) {
            p = new CategoryGroupItem();
            p.setObjectId(rs.getString(dbObj.pk_field));
            p.category_group_item_id = rs.getString(dbObj.category_group_item_id);
            p.description = rs.getString(dbObj.description);
            p.category_group_code = rs.getString(dbObj.category_group_code);
            p.active = rs.getString(dbObj.active);

            list.add(p);
        }
        rs.close();
        return list;
    }

    /*
     * ////////////////////////////////////////////////////////////////////////////
     */
    public CategoryGroupItem selectServiceById(String id) throws Exception {
        String sql = "select * from b_item_subgroup where b_item_subgroup_id = '" + id + "' and f_item_group_id = '5'";
        Vector v = eQuery(sql);
        return (CategoryGroupItem) (v.isEmpty() ? null : v.get(0));
    }

    public Vector selectAllByName(String pk, String active, String grpCode) throws Exception {
        StringBuffer sql = new StringBuffer("select * from ").append(dbObj.table).append(" where ");
        if (!pk.isEmpty()) {
            pk = Gutil.CheckReservedWords(pk);
            sql.append("(").append(dbObj.category_group_item_id).append(" like '%").append(pk).append("%'").
                    append(" or UPPER(").append(dbObj.description).append(") like UPPER('%").append(pk).append("%')").
                    append(") and ");
        }
        sql.append(dbObj.active).append(" = '").append(active).append("'");
        sql.append(dbObj.category_group_code).append(" = '").append(grpCode).append("'");
        sql.append("order by ").append(dbObj.category_group_item_id);

        Vector v = eQuery(sql.toString());
        if (v.isEmpty()) {
            return null;
        } else {
            return v;
        }
    }

    public CategoryGroupItem selectByItemId(String itemId) throws Exception {
        String sql = "select \n"
                + "b_item_subgroup.*\n"
                + "from b_item_subgroup\n"
                + "inner join b_item on b_item.b_item_subgroup_id = b_item_subgroup.b_item_subgroup_id\n"
                + "where b_item.b_item_id = ?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, itemId);
            Vector v = eQuery(ePQuery.toString());
            return (CategoryGroupItem) (v.isEmpty() ? null : v.get(0));
        }
    }

    public Vector<CategoryGroupItem> selectAllExceptCategoryGroupCode(String cgc, String active, boolean isSelectBy) throws Exception {
        String sql = "select * from b_item_subgroup \n";
        if (!cgc.isEmpty()) {
            sql += " where f_item_group_id " + (isSelectBy ? "=" : "<>") + " ? \n";
        }
        if (!active.isEmpty()) {
            sql += " and item_subgroup_active=? \n";
        }
        sql += " order by item_subgroup_description ";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            if (!cgc.isEmpty()) {
                ePQuery.setString(index++, cgc);
            }
            if (!active.isEmpty()) {
                ePQuery.setString(index++, active);
            }
            return eQuery(ePQuery.toString());
        }
    }
}

/*
 * DialogAppointmentTemplate.java
 *
 * Created on 10 �ԧ�Ҥ� 2549, 09:57 �.
 *
 */
package com.hosv3.gui.dialog;

import com.hospital_os.object.AppointmentTemplate;
import com.hospital_os.object.AppointmentTemplateItem;
import com.hospital_os.object.Employee;
import com.hospital_os.object.Item;
import com.hospital_os.object.QueueVisit;
import com.hospital_os.object.ServicePoint;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hospital_os.utility.CellRendererHos;
import com.hospital_os.utility.ComboboxModel;
import com.hospital_os.utility.ComplexDataSource;
import com.hospital_os.utility.Gutil;
import com.hospital_os.utility.TaBleModel;
import com.hosv3.control.HosControl;
import com.hosv3.control.lookup.R53AppApTypeLookup;
import com.hosv3.gui.component.TimeListChooser;
import com.hosv3.utility.Constant;
import com.hosv3.utility.DateUtil;
import com.hosv3.utility.GuiLang;
import com.hosv3.utility.ThreadStatus;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author amp
 */
@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public class DialogAppointmentTemplate extends JFrame implements UpdateStatus, PropertyChangeListener {

    private static final long serialVersionUID = 1L;
    private HosControl theHC;
    public boolean actionCommand = false;
    private JFrame aMain;
    private String[] col_head = {"���͵�Ǫ��¹Ѵ", "ᾷ��", "��ǧ����"};
    private QueueVisit theQueueVisit;
    private boolean isUseQueueVisit;
    private Vector sPoint;
    private Vector vAppointmentTemplateItem;
    private Vector vItem;
    private Vector vAppointmentTemplate;
    private AppointmentTemplate theAppointmentTemplate;
    private final JPopupMenu popupStart;
    private final JPopupMenu popupEnd;
    private boolean timeSelectedStart;
    private boolean timeSelectedEnd;
    private final HosDialog theHD;
    private CellRendererHos cellRendererHos = new CellRendererHos(CellRendererHos.TOOL_TIP_TEXT);
    public final static String CARD_NORMAL = "CARD_NORMAL";
    public final static String CARD_USESET = "CARD_USESET";
    protected CardLayout CardLayout;
    private final String[] colSetHeader = {"���駷��", "�ӹǹ�ѹ�Ѵ�", "�ѹ���Ѵ", "���ҹѴ�������", "���ҹѴ����ش", "��������´"};
    private final CellRendererHos rightRender = new CellRendererHos(CellRendererHos.ALIGNMENT_RIGHT);
    private final CellRendererHos centerRender = new CellRendererHos(CellRendererHos.ALIGNMENT_CENTER);
    private List<ComplexDataSource> listDetail;

    /**
     * Creates new form DialogAppointmentTemplate
     */
    @SuppressWarnings("LeakingThisInConstructor")
    public DialogAppointmentTemplate(HosControl hc, UpdateStatus us, HosDialog hd) {
        aMain = us.getJFrame();
        setIconImage(aMain.getIconImage());
        theHC = hc;
        theHD = hd;
        initComponents();
        updateOGComponent();
        setDialog();

        CardLayout = (CardLayout) jPanel4.getLayout();

        //amp:13/03/2549 ���������������´�յ�Ǫ��¢ͧ�ҡ�����ͧ��
        jTextAreaDescription.setControl(new com.hosv3.control.lookup.VitalTemplateLookup(theHC.theLookupControl));
        jTextAreaDescription.setJFrame(this);
        theHC.theHS.theBalloonSubject.attachBalloon(jTextAreaDescription);

        for (Component component : panelChooseTimeStart.getComponents()) {
            if (component instanceof JPanel) {
                JPanel panel = (JPanel) component;
                for (Component subComponent : panel.getComponents()) {
                    if (subComponent instanceof JButton) {
                        final JButton jb = (JButton) subComponent;
                        jb.addActionListener(new java.awt.event.ActionListener() {
                            @Override
                            public void actionPerformed(java.awt.event.ActionEvent evt) {
                                timeTextFieldTimeAppointment.setText(jb.getText());
                            }
                        });
                    }
                }
            }
        }

        // set popup time chooser
        popupStart = new JPopupMenu() {
            private static final long serialVersionUID = 1L;

            @Override
            public void setVisible(boolean b) {
                Boolean isCanceled = (Boolean) getClientProperty(
                        "JPopupMenu.firePopupMenuCanceled");

                if (b || (!b && timeSelectedStart)
                        || ((isCanceled != null) && !b && isCanceled.booleanValue())) {
                    super.setVisible(b);
                }
            }
        };
        TimeListChooser tlcStart = new TimeListChooser();
        timeTextFieldTimeAppointment.setComponentPopupMenu(popupStart);
        popupStart.add(tlcStart);
        tlcStart.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (evt.getPropertyName().equals("timeChooser")) {
                    timeSelectedStart = true;
                    popupStart.setVisible(false);
                    timeTextFieldTimeAppointment.setText((String) evt.getNewValue());
                }
            }
        });

        timeTextFieldTimeAppointment.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                timeTextFieldTimeAppointmentEnd.setText(timeTextFieldTimeAppointment.getText());
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                timeTextFieldTimeAppointmentEnd.setText(timeTextFieldTimeAppointment.getText());
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                timeTextFieldTimeAppointmentEnd.setText(timeTextFieldTimeAppointment.getText());
            }
        });

        for (Component component : panelChooseTimeEnd.getComponents()) {
            if (component instanceof JPanel) {
                JPanel panel = (JPanel) component;
                for (Component subComponent : panel.getComponents()) {
                    if (subComponent instanceof JButton) {
                        final JButton jb = (JButton) subComponent;
                        jb.addActionListener(new java.awt.event.ActionListener() {
                            @Override
                            public void actionPerformed(java.awt.event.ActionEvent evt) {
                                timeTextFieldTimeAppointmentEnd.setText(jb.getText());
                                validateTimeEnd();
                            }
                        });
                    }
                }
            }
        }

        popupEnd = new JPopupMenu() {
            private static final long serialVersionUID = 1L;

            @Override
            public void setVisible(boolean b) {
                Boolean isCanceled = (Boolean) getClientProperty(
                        "JPopupMenu.firePopupMenuCanceled");

                if (b || (!b && timeSelectedEnd)
                        || ((isCanceled != null) && !b && isCanceled.booleanValue())) {
                    super.setVisible(b);
                }
            }
        };
        TimeListChooser tlcEnd = new TimeListChooser();
        timeTextFieldTimeAppointmentEnd.setComponentPopupMenu(popupEnd);
        popupEnd.add(tlcEnd);
        tlcEnd.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (evt.getPropertyName().equals("timeChooser")) {
                    timeSelectedEnd = true;
                    popupEnd.setVisible(false);
                    timeTextFieldTimeAppointmentEnd.setText((String) evt.getNewValue());
                    validateTimeEnd();
                }
            }
        });
    }

    /**
     * dialog �����㹡���觢�ͤ������͹�����
     */
    @Override
    public void setStatus(String str, int status) {
        ThreadStatus theTT = new ThreadStatus(this, this.jLabelStatus);
        theTT.start();
        str = Constant.getTextBundle(str);
        jLabelStatus.setText(" " + str);
        if (status == UpdateStatus.WARNING) {
            jLabelStatus.setBackground(Color.YELLOW);
        }
        if (status == UpdateStatus.COMPLETE) {
            jLabelStatus.setBackground(Color.GREEN);
        }
        if (status == UpdateStatus.ERROR) {
            jLabelStatus.setBackground(Color.RED);
        }
    }

    /**
     * dialog �����㹡���������ӡ���չ�ѹ��觵�ҧ
     */
    @Override
    public boolean confirmBox(String str, int status) {
        int i = JOptionPane.showConfirmDialog(this, str, Constant.getTextBundle("��͹"), JOptionPane.YES_NO_OPTION);
        return (i == JOptionPane.YES_OPTION);
    }

    /**
     * init component �ӡ��૵������Ѻ component ��ҧ� neung
     */
    private void updateOGComponent() {
        Vector v = theHC.theLookupControl.listServicePoint();
        ServicePoint cf = new ServicePoint();
        cf.setObjectId("0");
        cf.name = Constant.getTextBundle("������");
        sPoint = new Vector();
        sPoint.add(cf);
        for (int i = 0, size = v.size(); i < size; i++) {
            sPoint.add(v.get(i));
        }
        ComboboxModel.initComboBox(jComboBoxServicePoint, theHC.theLookupControl.listServicePoint());
        ComboboxModel.initComboBox(jComboBoxDoctor, theHC.theLookupControl.listDoctor());
        ComboboxModel.initComboBox(jComboBoxClinic, theHC.theLookupControl.listClinic());
        jComboBoxApType53.setControl(new R53AppApTypeLookup(theHC.theLookupControl.theConnectionInf), false);
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        jLabelStatus = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new com.hosv3.gui.component.HJTableSort();
        jPanelSearch = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        jTextFieldSearch = new javax.swing.JTextField();
        jPanel10 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        btnAdd = new javax.swing.JButton();
        jButtonDel = new javax.swing.JButton();
        jButtonSave = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextAreaDescription = new com.hosv3.gui.component.BalloonTextArea();
        jComboBoxDoctor = new javax.swing.JComboBox();
        jPanel5 = new javax.swing.JPanel();
        btnQueueVisit = new javax.swing.JButton();
        lblQueueName = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        chkUseAutoVisit = new javax.swing.JCheckBox();
        jPanel22 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTableItem = new com.hosv3.gui.component.HJTableSort();
        jPanel23 = new javax.swing.JPanel();
        jButtonDelOrder = new javax.swing.JButton();
        jButtonAddOrder = new javax.swing.JButton();
        btnDrugSetList = new javax.swing.JButton();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTableAppointmentOrder = new com.hosv3.gui.component.HJTableSort();
        jPanel16 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTextFieldSearchOrder = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jTextFieldAppointmentTemplateName = new javax.swing.JTextField();
        jCheckBoxUseSet = new javax.swing.JCheckBox();
        jTextFieldSetName = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jComboBoxServicePoint = new javax.swing.JComboBox();
        jLabel12 = new javax.swing.JLabel();
        jComboBoxClinic = new javax.swing.JComboBox();
        jTextFieldApType = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        jComboBoxApType53 = new com.hosv3.gui.component.HosComboBox();
        jPanel4 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        panelNextAppDay = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        lblNextDay = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jToggleButton1 = new javax.swing.JToggleButton();
        jToggleButton2 = new javax.swing.JToggleButton();
        jToggleButton3 = new javax.swing.JToggleButton();
        jToggleButton4 = new javax.swing.JToggleButton();
        jToggleButton5 = new javax.swing.JToggleButton();
        jToggleButton6 = new javax.swing.JToggleButton();
        jToggleButton7 = new javax.swing.JToggleButton();
        jToggleButton8 = new javax.swing.JToggleButton();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jPanelTime = new javax.swing.JPanel();
        timeTextFieldTimeAppointment = new com.hospital_os.utility.TimeTextField();
        timeTextFieldTimeAppointmentEnd = new com.hospital_os.utility.TimeTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        panelChooseTimeStart = new javax.swing.JPanel();
        panelChooseTimeStart1 = new javax.swing.JPanel();
        jButton6 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();
        jButton12 = new javax.swing.JButton();
        panelChooseTimeEnd = new javax.swing.JPanel();
        panelChooseTimeEnd2 = new javax.swing.JPanel();
        jButton36 = new javax.swing.JButton();
        jButton38 = new javax.swing.JButton();
        jButton40 = new javax.swing.JButton();
        jButton42 = new javax.swing.JButton();
        jPanelUseSet = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        integerTextFieldTime = new com.hospital_os.utility.IntegerTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        integerTextFieldNextDay = new com.hospital_os.utility.IntegerTextField();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        timeTextFieldTime = new com.hospital_os.utility.TimeTextField();
        timeTextFieldEndTime = new com.hospital_os.utility.TimeTextField();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jdcDateAppointment = new sd.comp.jcalendar.JDateChooser();
        jButtonCreate = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTableDetail = new com.hosv3.gui.component.HJTableSort();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("��Ǫ��¹Ѵ"); // NOI18N
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(new java.awt.GridBagLayout());

        jLabelStatus.setFont(jLabelStatus.getFont());
        jLabelStatus.setText("Status");
        jLabelStatus.setMaximumSize(new java.awt.Dimension(4, 24));
        jLabelStatus.setMinimumSize(new java.awt.Dimension(4, 20));
        jLabelStatus.setOpaque(true);
        jLabelStatus.setPreferredSize(new java.awt.Dimension(4, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        getContentPane().add(jLabelStatus, gridBagConstraints);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("���ҵ�Ǫ��¹Ѵ"));
        jPanel3.setMinimumSize(new java.awt.Dimension(290, 107));
        jPanel3.setPreferredSize(new java.awt.Dimension(250, 165));
        jPanel3.setRequestFocusEnabled(false);
        jPanel3.setLayout(new java.awt.GridBagLayout());

        jScrollPane1.setMinimumSize(new java.awt.Dimension(100, 22));
        jScrollPane1.setPreferredSize(new java.awt.Dimension(100, 80));

        jTable1.setFillsViewportHeight(true);
        jTable1.setFont(jTable1.getFont());
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTable1MouseReleased(evt);
            }
        });
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTable1KeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 12, 12, 12);
        jPanel3.add(jScrollPane1, gridBagConstraints);

        jPanelSearch.setLayout(new java.awt.GridBagLayout());

        jLabel15.setFont(jLabel15.getFont());
        jLabel15.setText("����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 1);
        jPanelSearch.add(jLabel15, gridBagConstraints);

        jTextFieldSearch.setFont(jTextFieldSearch.getFont());
        jTextFieldSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldSearchKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        jPanelSearch.add(jTextFieldSearch, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(12, 12, 0, 12);
        jPanel3.add(jPanelSearch, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(12, 12, 0, 0);
        getContentPane().add(jPanel3, gridBagConstraints);

        jPanel10.setBorder(javax.swing.BorderFactory.createTitledBorder("��������´��Ǫ��¹Ѵ"));
        jPanel10.setMinimumSize(new java.awt.Dimension(400, 946));
        jPanel10.setPreferredSize(new java.awt.Dimension(400, 165));
        jPanel10.setRequestFocusEnabled(false);
        jPanel10.setLayout(new java.awt.GridBagLayout());

        jPanel1.setLayout(new java.awt.GridBagLayout());

        btnAdd.setFont(btnAdd.getFont());
        btnAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/add.png"))); // NOI18N
        btnAdd.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnAdd.setMaximumSize(new java.awt.Dimension(32, 32));
        btnAdd.setMinimumSize(new java.awt.Dimension(32, 32));
        btnAdd.setPreferredSize(new java.awt.Dimension(32, 32));
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        jPanel1.add(btnAdd, gridBagConstraints);

        jButtonDel.setFont(jButtonDel.getFont());
        jButtonDel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/delete.png"))); // NOI18N
        jButtonDel.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonDel.setMaximumSize(new java.awt.Dimension(32, 32));
        jButtonDel.setMinimumSize(new java.awt.Dimension(32, 32));
        jButtonDel.setPreferredSize(new java.awt.Dimension(32, 32));
        jButtonDel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDelActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        jPanel1.add(jButtonDel, gridBagConstraints);

        jButtonSave.setFont(jButtonSave.getFont());
        jButtonSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/save.png"))); // NOI18N
        jButtonSave.setMargin(new java.awt.Insets(2, 4, 2, 4));
        jButtonSave.setMaximumSize(new java.awt.Dimension(32, 32));
        jButtonSave.setMinimumSize(new java.awt.Dimension(32, 32));
        jButtonSave.setPreferredSize(new java.awt.Dimension(32, 32));
        jButtonSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSaveActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.weightx = 1.0;
        jPanel1.add(jButtonSave, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel10.add(jPanel1, gridBagConstraints);

        jPanel7.setLayout(new java.awt.GridBagLayout());

        jLabel6.setFont(jLabel6.getFont());
        jLabel6.setText("��������´");
        jLabel6.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jLabel6, gridBagConstraints);

        jLabel11.setFont(jLabel11.getFont());
        jLabel11.setText("ᾷ����Ѵ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jLabel11, gridBagConstraints);

        jScrollPane2.setMaximumSize(new java.awt.Dimension(250, 150));
        jScrollPane2.setMinimumSize(new java.awt.Dimension(50, 30));
        jScrollPane2.setPreferredSize(new java.awt.Dimension(50, 30));

        jTextAreaDescription.setFont(jTextAreaDescription.getFont());
        jScrollPane2.setViewportView(jTextAreaDescription);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 0.4;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jScrollPane2, gridBagConstraints);

        jComboBoxDoctor.setFont(jComboBoxDoctor.getFont());
        jComboBoxDoctor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxDoctorActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jComboBoxDoctor, gridBagConstraints);

        jPanel5.setPreferredSize(new java.awt.Dimension(87, 35));
        jPanel5.setLayout(new java.awt.GridBagLayout());

        btnQueueVisit.setFont(btnQueueVisit.getFont());
        btnQueueVisit.setText("���");
        btnQueueVisit.setEnabled(false);
        btnQueueVisit.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnQueueVisit.setMaximumSize(new java.awt.Dimension(26, 26));
        btnQueueVisit.setMinimumSize(new java.awt.Dimension(26, 26));
        btnQueueVisit.setPreferredSize(new java.awt.Dimension(26, 26));
        btnQueueVisit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnQueueVisitActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(btnQueueVisit, gridBagConstraints);

        lblQueueName.setFont(lblQueueName.getFont());
        lblQueueName.setText("����к�");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(lblQueueName, gridBagConstraints);

        jLabel13.setFont(jLabel13.getFont());
        jLabel13.setText("���͡���");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(jLabel13, gridBagConstraints);

        chkUseAutoVisit.setFont(chkUseAutoVisit.getFont());
        chkUseAutoVisit.setText("�Դ Visit �ѵ��ѵ�");
        chkUseAutoVisit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkUseAutoVisitActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel5.add(chkUseAutoVisit, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jPanel5, gridBagConstraints);

        jPanel22.setLayout(new java.awt.GridBagLayout());

        jTableItem.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTableItem.setFillsViewportHeight(true);
        jTableItem.setFont(jTableItem.getFont());
        jTableItem.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTableItemMouseReleased(evt);
            }
        });
        jTableItem.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTableItemKeyReleased(evt);
            }
        });
        jScrollPane5.setViewportView(jTableItem);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel22.add(jScrollPane5, gridBagConstraints);

        jPanel23.setLayout(new java.awt.GridBagLayout());

        jButtonDelOrder.setFont(jButtonDelOrder.getFont());
        jButtonDelOrder.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/Back16.gif"))); // NOI18N
        jButtonDelOrder.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonDelOrder.setMaximumSize(new java.awt.Dimension(32, 32));
        jButtonDelOrder.setMinimumSize(new java.awt.Dimension(32, 32));
        jButtonDelOrder.setPreferredSize(new java.awt.Dimension(32, 32));
        jButtonDelOrder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDelOrderActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel23.add(jButtonDelOrder, gridBagConstraints);

        jButtonAddOrder.setFont(jButtonAddOrder.getFont());
        jButtonAddOrder.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/Forward16.gif"))); // NOI18N
        jButtonAddOrder.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonAddOrder.setMaximumSize(new java.awt.Dimension(32, 32));
        jButtonAddOrder.setMinimumSize(new java.awt.Dimension(32, 32));
        jButtonAddOrder.setPreferredSize(new java.awt.Dimension(32, 32));
        jButtonAddOrder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAddOrderActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel23.add(jButtonAddOrder, gridBagConstraints);

        btnDrugSetList.setFont(btnDrugSetList.getFont());
        btnDrugSetList.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/drug_set_24.png"))); // NOI18N
        btnDrugSetList.setToolTipText("��¡�êش��Ǩ");
        btnDrugSetList.setMaximumSize(new java.awt.Dimension(32, 32));
        btnDrugSetList.setMinimumSize(new java.awt.Dimension(32, 32));
        btnDrugSetList.setPreferredSize(new java.awt.Dimension(32, 32));
        btnDrugSetList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDrugSetListActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel23.add(btnDrugSetList, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.weighty = 1.0;
        jPanel22.add(jPanel23, gridBagConstraints);

        jTableAppointmentOrder.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTableAppointmentOrder.setFillsViewportHeight(true);
        jTableAppointmentOrder.setFont(jTableAppointmentOrder.getFont());
        jScrollPane6.setViewportView(jTableAppointmentOrder);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel22.add(jScrollPane6, gridBagConstraints);

        jPanel16.setLayout(new java.awt.GridBagLayout());

        jLabel1.setFont(jLabel1.getFont());
        jLabel1.setText("����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel16.add(jLabel1, gridBagConstraints);

        jTextFieldSearchOrder.setFont(jTextFieldSearchOrder.getFont());
        jTextFieldSearchOrder.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldSearchOrderKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel16.add(jTextFieldSearchOrder, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel22.add(jPanel16, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 0.6;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jPanel22, gridBagConstraints);

        jLabel9.setFont(jLabel9.getFont());
        jLabel9.setText("�Ѵ������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jLabel9, gridBagConstraints);

        jLabel17.setFont(jLabel17.getFont());
        jLabel17.setText("���͵�Ǫ��¹Ѵ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jLabel17, gridBagConstraints);

        jPanel6.setLayout(new java.awt.GridBagLayout());

        jTextFieldAppointmentTemplateName.setFont(jTextFieldAppointmentTemplateName.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jTextFieldAppointmentTemplateName, gridBagConstraints);

        jCheckBoxUseSet.setFont(jCheckBoxUseSet.getFont());
        jCheckBoxUseSet.setText("�Ӫش�Ѵ");
        jCheckBoxUseSet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxUseSetActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jCheckBoxUseSet, gridBagConstraints);

        jTextFieldSetName.setEnabled(false);
        jTextFieldSetName.setMinimumSize(new java.awt.Dimension(260, 24));
        jTextFieldSetName.setPreferredSize(new java.awt.Dimension(260, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel6.add(jTextFieldSetName, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.weightx = 1.0;
        jPanel7.add(jPanel6, gridBagConstraints);

        jLabel10.setFont(jLabel10.getFont());
        jLabel10.setText("�Ѵ��ѧ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jLabel10, gridBagConstraints);

        jComboBoxServicePoint.setFont(jComboBoxServicePoint.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jComboBoxServicePoint, gridBagConstraints);

        jLabel12.setFont(jLabel12.getFont());
        jLabel12.setText("��Թԡ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jLabel12, gridBagConstraints);

        jComboBoxClinic.setFont(jComboBoxClinic.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jComboBoxClinic, gridBagConstraints);

        jTextFieldApType.setFont(jTextFieldApType.getFont());
        jTextFieldApType.setPreferredSize(new java.awt.Dimension(50, 21));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jTextFieldApType, gridBagConstraints);

        jLabel16.setFont(jLabel16.getFont());
        jLabel16.setText("�������Ѵ");
        jLabel16.setToolTipText("����Ѻ�͡��§ҹ 18 ���");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jLabel16, gridBagConstraints);

        jComboBoxApType53.setBackground(new java.awt.Color(204, 255, 255));
        jComboBoxApType53.setFont(jComboBoxApType53.getFont());
        jComboBoxApType53.setMinimumSize(new java.awt.Dimension(200, 24));
        jComboBoxApType53.setPreferredSize(new java.awt.Dimension(200, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel7.add(jComboBoxApType53, gridBagConstraints);

        jPanel4.setLayout(new java.awt.CardLayout());

        jPanel9.setLayout(new java.awt.GridBagLayout());

        panelNextAppDay.setLayout(new java.awt.GridBagLayout());

        jLabel2.setFont(jLabel2.getFont());
        jLabel2.setText("�ѹ���Ѵ");
        jLabel2.setToolTipText("�ѹ���Ѵ (�Ѻ�ҡ�ѹ�Ѩ�غѹ)");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelNextAppDay.add(jLabel2, gridBagConstraints);

        lblNextDay.setFont(lblNextDay.getFont().deriveFont(lblNextDay.getFont().getStyle() | java.awt.Font.BOLD));
        lblNextDay.setForeground(new java.awt.Color(0, 0, 204));
        lblNextDay.setText("0");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 12, 2, 12);
        panelNextAppDay.add(lblNextDay, gridBagConstraints);

        jLabel4.setFont(jLabel4.getFont());
        jLabel4.setText("�Ѻ�ҡ�ѹ�Ѩ�غѹ");
        jLabel4.setToolTipText("�ѹ���Ѵ (�Ѻ�ҡ�ѹ�Ѩ�غѹ)");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelNextAppDay.add(jLabel4, gridBagConstraints);

        jPanel2.setLayout(new java.awt.GridBagLayout());

        buttonGroup2.add(jToggleButton1);
        jToggleButton1.setFont(jToggleButton1.getFont());
        jToggleButton1.setText("1W");
        jToggleButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButton1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel2.add(jToggleButton1, gridBagConstraints);

        buttonGroup2.add(jToggleButton2);
        jToggleButton2.setFont(jToggleButton2.getFont());
        jToggleButton2.setText("2W");
        jToggleButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButton2ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel2.add(jToggleButton2, gridBagConstraints);

        buttonGroup2.add(jToggleButton3);
        jToggleButton3.setFont(jToggleButton3.getFont());
        jToggleButton3.setText("3W");
        jToggleButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButton3ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel2.add(jToggleButton3, gridBagConstraints);

        buttonGroup2.add(jToggleButton4);
        jToggleButton4.setFont(jToggleButton4.getFont());
        jToggleButton4.setText("4W");
        jToggleButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButton4ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel2.add(jToggleButton4, gridBagConstraints);

        buttonGroup2.add(jToggleButton5);
        jToggleButton5.setFont(jToggleButton5.getFont());
        jToggleButton5.setText("6W");
        jToggleButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButton5ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel2.add(jToggleButton5, gridBagConstraints);

        buttonGroup2.add(jToggleButton6);
        jToggleButton6.setFont(jToggleButton6.getFont());
        jToggleButton6.setText("8W");
        jToggleButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButton6ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel2.add(jToggleButton6, gridBagConstraints);

        buttonGroup2.add(jToggleButton7);
        jToggleButton7.setFont(jToggleButton7.getFont());
        jToggleButton7.setText("3M");
        jToggleButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButton7ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel2.add(jToggleButton7, gridBagConstraints);

        buttonGroup2.add(jToggleButton8);
        jToggleButton8.setFont(jToggleButton8.getFont());
        jToggleButton8.setText("6M");
        jToggleButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButton8ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel2.add(jToggleButton8, gridBagConstraints);

        jButton1.setFont(jButton1.getFont());
        jButton1.setText("+1W");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel2.add(jButton1, gridBagConstraints);

        jButton2.setFont(jButton2.getFont());
        jButton2.setText("+4W");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel2.add(jButton2, gridBagConstraints);

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/restore-icon.png"))); // NOI18N
        jButton3.setToolTipText("��ҧ����ѹ���Ѵ");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel2.add(jButton3, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        panelNextAppDay.add(jPanel2, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        jPanel9.add(panelNextAppDay, gridBagConstraints);

        jPanelTime.setLayout(new java.awt.GridBagLayout());

        timeTextFieldTimeAppointment.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        timeTextFieldTimeAppointment.setToolTipText("");
        timeTextFieldTimeAppointment.setFont(timeTextFieldTimeAppointment.getFont());
        timeTextFieldTimeAppointment.setMinimumSize(new java.awt.Dimension(45, 24));
        timeTextFieldTimeAppointment.setName("timeTextFieldTimeAppointment"); // NOI18N
        timeTextFieldTimeAppointment.setPreferredSize(new java.awt.Dimension(45, 24));
        timeTextFieldTimeAppointment.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                timeTextFieldTimeAppointmentActionPerformed(evt);
            }
        });
        timeTextFieldTimeAppointment.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                timeTextFieldTimeAppointmentFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                timeTextFieldTimeAppointmentFocusLost(evt);
            }
        });
        timeTextFieldTimeAppointment.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                timeTextFieldTimeAppointmentKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelTime.add(timeTextFieldTimeAppointment, gridBagConstraints);

        timeTextFieldTimeAppointmentEnd.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        timeTextFieldTimeAppointmentEnd.setToolTipText("");
        timeTextFieldTimeAppointmentEnd.setFont(timeTextFieldTimeAppointmentEnd.getFont());
        timeTextFieldTimeAppointmentEnd.setMinimumSize(new java.awt.Dimension(45, 24));
        timeTextFieldTimeAppointmentEnd.setName("timeTextFieldTimeAppointmentEnd"); // NOI18N
        timeTextFieldTimeAppointmentEnd.setPreferredSize(new java.awt.Dimension(45, 24));
        timeTextFieldTimeAppointmentEnd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                timeTextFieldTimeAppointmentEndActionPerformed(evt);
            }
        });
        timeTextFieldTimeAppointmentEnd.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                timeTextFieldTimeAppointmentEndFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                timeTextFieldTimeAppointmentEndFocusLost(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelTime.add(timeTextFieldTimeAppointmentEnd, gridBagConstraints);

        jLabel3.setText("���ҹѴ����ش");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelTime.add(jLabel3, gridBagConstraints);

        jLabel5.setFont(jLabel5.getFont());
        jLabel5.setText("���ҹѴ�������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelTime.add(jLabel5, gridBagConstraints);

        panelChooseTimeStart.setLayout(new java.awt.GridBagLayout());

        panelChooseTimeStart1.setLayout(new javax.swing.BoxLayout(panelChooseTimeStart1, javax.swing.BoxLayout.LINE_AXIS));

        jButton6.setFont(jButton6.getFont());
        jButton6.setText("08:00");
        panelChooseTimeStart1.add(jButton6);

        jButton8.setFont(jButton8.getFont());
        jButton8.setText("09:00");
        panelChooseTimeStart1.add(jButton8);

        jButton10.setFont(jButton10.getFont());
        jButton10.setText("10:00");
        panelChooseTimeStart1.add(jButton10);

        jButton12.setFont(jButton12.getFont());
        jButton12.setText("11:00");
        panelChooseTimeStart1.add(jButton12);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelChooseTimeStart.add(panelChooseTimeStart1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        jPanelTime.add(panelChooseTimeStart, gridBagConstraints);

        panelChooseTimeEnd.setLayout(new java.awt.GridBagLayout());

        panelChooseTimeEnd2.setLayout(new javax.swing.BoxLayout(panelChooseTimeEnd2, javax.swing.BoxLayout.LINE_AXIS));

        jButton36.setFont(jButton36.getFont());
        jButton36.setText("13:00");
        panelChooseTimeEnd2.add(jButton36);

        jButton38.setFont(jButton38.getFont());
        jButton38.setText("14:00");
        panelChooseTimeEnd2.add(jButton38);

        jButton40.setFont(jButton40.getFont());
        jButton40.setText("15:00");
        panelChooseTimeEnd2.add(jButton40);

        jButton42.setFont(jButton42.getFont());
        jButton42.setText("16:00");
        panelChooseTimeEnd2.add(jButton42);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        panelChooseTimeEnd.add(panelChooseTimeEnd2, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        jPanelTime.add(panelChooseTimeEnd, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel9.add(jPanelTime, gridBagConstraints);

        jPanel4.add(jPanel9, "CARD_NORMAL");

        jPanelUseSet.setPreferredSize(new java.awt.Dimension(270, 100));
        jPanelUseSet.setLayout(new java.awt.GridBagLayout());

        jLabel7.setFont(jLabel7.getFont());
        jLabel7.setText("�Ѵ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelUseSet.add(jLabel7, gridBagConstraints);

        jPanel8.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jPanel8.setFont(jPanel8.getFont());
        jPanel8.setLayout(new java.awt.GridBagLayout());

        integerTextFieldTime.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        integerTextFieldTime.setText("0");
        integerTextFieldTime.setMinimumSize(new java.awt.Dimension(36, 24));
        integerTextFieldTime.setPreferredSize(new java.awt.Dimension(36, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel8.add(integerTextFieldTime, gridBagConstraints);

        jLabel8.setFont(jLabel8.getFont());
        jLabel8.setText("����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel8.add(jLabel8, gridBagConstraints);

        jLabel14.setFont(jLabel14.getFont());
        jLabel14.setText("�ء�");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel8.add(jLabel14, gridBagConstraints);

        integerTextFieldNextDay.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        integerTextFieldNextDay.setText("0");
        integerTextFieldNextDay.setMinimumSize(new java.awt.Dimension(36, 24));
        integerTextFieldNextDay.setPreferredSize(new java.awt.Dimension(36, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel8.add(integerTextFieldNextDay, gridBagConstraints);

        jLabel18.setText("�ѹ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel8.add(jLabel18, gridBagConstraints);

        jLabel19.setFont(jLabel19.getFont());
        jLabel19.setText("������ѹ���");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel8.add(jLabel19, gridBagConstraints);

        timeTextFieldTime.setText("08:00");
        timeTextFieldTime.setMinimumSize(new java.awt.Dimension(45, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel8.add(timeTextFieldTime, gridBagConstraints);

        timeTextFieldEndTime.setText("16:00");
        timeTextFieldEndTime.setMinimumSize(new java.awt.Dimension(45, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 11;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel8.add(timeTextFieldEndTime, gridBagConstraints);

        jLabel20.setFont(jLabel20.getFont());
        jLabel20.setText("�.");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 9;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel8.add(jLabel20, gridBagConstraints);

        jLabel21.setFont(jLabel21.getFont());
        jLabel21.setText("�.");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel8.add(jLabel21, gridBagConstraints);

        jLabel22.setFont(jLabel22.getFont());
        jLabel22.setText("����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel8.add(jLabel22, gridBagConstraints);

        jLabel23.setFont(jLabel23.getFont());
        jLabel23.setText("�֧");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 2, 2);
        jPanel8.add(jLabel23, gridBagConstraints);

        jdcDateAppointment.setEnableFuture(true);
        jdcDateAppointment.setFont(jdcDateAppointment.getFont());
        jdcDateAppointment.setMinimumSize(new java.awt.Dimension(130, 24));
        jdcDateAppointment.setPreferredSize(new java.awt.Dimension(130, 24));
        jdcDateAppointment.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jdcDateAppointmentPropertyChange(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel8.add(jdcDateAppointment, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelUseSet.add(jPanel8, gridBagConstraints);

        jButtonCreate.setText("���ҧ");
        jButtonCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCreateActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelUseSet.add(jButtonCreate, gridBagConstraints);

        jTableDetail.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTableDetailKeyReleased(evt);
            }
        });
        jScrollPane3.setViewportView(jTableDetail);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelUseSet.add(jScrollPane3, gridBagConstraints);

        jPanel4.add(jPanelUseSet, "CARD_USESET");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel7.add(jPanel4, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanel10.add(jPanel7, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(12, 5, 0, 12);
        getContentPane().add(jPanel10, gridBagConstraints);

        setSize(new java.awt.Dimension(1097, 625));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jTextFieldSearchKeyReleased(java.awt.event.KeyEvent evt)//GEN-FIRST:event_jTextFieldSearchKeyReleased
    {//GEN-HEADEREND:event_jTextFieldSearchKeyReleased
        if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
            searchAppointmentTemplate();
        }
        if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_DOWN) {
            jTable1.requestFocus();
        }
        if (jTextFieldSearch.getText().length() > 1) {
            searchAppointmentTemplate();
        }
    }//GEN-LAST:event_jTextFieldSearchKeyReleased

    private void jTableItemMouseReleased(java.awt.event.MouseEvent evt)//GEN-FIRST:event_jTableItemMouseReleased
    {//GEN-HEADEREND:event_jTableItemMouseReleased
        if (evt.getClickCount() == 2) {
            jButtonAddOrderActionPerformed(null);
            jTextFieldSearchOrder.requestFocus();
        }
    }//GEN-LAST:event_jTableItemMouseReleased

    private void jTableItemKeyReleased(java.awt.event.KeyEvent evt)//GEN-FIRST:event_jTableItemKeyReleased
    {//GEN-HEADEREND:event_jTableItemKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_SPACE) {
            jButtonAddOrderActionPerformed(null);
            jTextFieldSearchOrder.requestFocus();
        }
    }//GEN-LAST:event_jTableItemKeyReleased

    private void jButtonDelOrderActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonDelOrderActionPerformed
    {//GEN-HEADEREND:event_jButtonDelOrderActionPerformed
        int row[] = jTableAppointmentOrder.getSelectedRows();
        if (row.length == 0) {
            setStatus("�ѧ����բ�����", UpdateStatus.WARNING);
            return;
        }
        theHC.thePatientControl.deleteAppointmentTemplateItem(vAppointmentTemplateItem, row);
        this.setTableAppointmentTemplateItem(vAppointmentTemplateItem);
    }//GEN-LAST:event_jButtonDelOrderActionPerformed

    private void jTextFieldSearchOrderKeyReleased(java.awt.event.KeyEvent evt)//GEN-FIRST:event_jTextFieldSearchOrderKeyReleased
    {//GEN-HEADEREND:event_jTextFieldSearchOrderKeyReleased
        if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
            searchItem();
        }
        if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_DOWN) {
            jTableItem.requestFocus();
        }
        if (jTextFieldSearchOrder.getText().length() > 1) {
            searchItem();
        }
    }//GEN-LAST:event_jTextFieldSearchOrderKeyReleased

    private void jButtonAddOrderActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonAddOrderActionPerformed
    {//GEN-HEADEREND:event_jButtonAddOrderActionPerformed
        int[] row = jTableItem.getSelectedRows();
        if (row.length == 0) {
            setStatus("�ѧ��������͡��¡�� Item", UpdateStatus.WARNING);
            return;
        }
        if (vAppointmentTemplateItem == null) {
            vAppointmentTemplateItem = new Vector();
        }
        Item item;
        AppointmentTemplateItem apti;
        for (int i = 0, size = row.length; i < size; i++) {
            item = (Item) vItem.get(row[i]);
            if (vAppointmentTemplateItem.isEmpty())//�ͺ�á��� add �����
            {
                apti = new AppointmentTemplateItem();
                apti.item_id = item.getObjectId();
                apti.item_common_name = item.common_name;
                vAppointmentTemplateItem.add(apti);
            } else {
                boolean isSame = false;
                for (int j = 0; j < vAppointmentTemplateItem.size(); j++) {
                    if (item.getObjectId().equals(((AppointmentTemplateItem) vAppointmentTemplateItem.get(j)).item_id)) {
                        isSame = true;
                    }
                }
                if (!isSame) {
                    apti = new AppointmentTemplateItem();
                    apti.item_id = item.getObjectId();
                    apti.item_common_name = item.common_name;
                    vAppointmentTemplateItem.add(apti);
                }
            }
        }
        setTableAppointmentTemplateItem(vAppointmentTemplateItem);
    }//GEN-LAST:event_jButtonAddOrderActionPerformed

    private void btnQueueVisitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnQueueVisitActionPerformed
        if (theQueueVisit == null) {
            theQueueVisit = new QueueVisit();
        }
        if (theAppointmentTemplate != null) {
            theQueueVisit.setObjectId(theAppointmentTemplate.queue_visit_id);
        }
        DialogQueueVisit.showDialog(theHC, this, 1, theQueueVisit);
        lblQueueName.setText(theQueueVisit.description);
    }//GEN-LAST:event_btnQueueVisitActionPerformed

    private void dateComboBoxDateAppointmentFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_dateComboBoxDateAppointmentFocusLost
    }//GEN-LAST:event_dateComboBoxDateAppointmentFocusLost

    private void jButtonDelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDelActionPerformed
        if (theHC.thePatientControl.deleteAppointmentTemplate(theAppointmentTemplate, vAppointmentTemplateItem)) {
            btnAdd.doClick();
            searchAppointmentTemplate();
        }
    }//GEN-LAST:event_jButtonDelActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        setVisible(false);
        dispose();
    }//GEN-LAST:event_formWindowClosing

    private void comboBoxClinic1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxClinic1ActionPerformed
    }//GEN-LAST:event_comboBoxClinic1ActionPerformed

	private void jTable1MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseReleased
        int row = jTable1.getSelectedRow();
        if (row == -1) {
            return;
        }
        setAppointmentTemplate((AppointmentTemplate) vAppointmentTemplate.get(row));
    }//GEN-LAST:event_jTable1MouseReleased

    private void jTable1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_SPACE) {
            jTable1MouseReleased(null);
        }
    }//GEN-LAST:event_jTable1KeyReleased

    private void jButtonSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSaveActionPerformed
        if ("".equals(jTextFieldAppointmentTemplateName.getText())) {
            setStatus("��س��кت��͵�Ǫ���", UpdateStatus.WARNING);
            return;
        }
        if (jTextFieldSetName.isEnabled() && "".equals(jTextFieldSetName.getText())) {
            setStatus("��س��кت��͹Ѵ", UpdateStatus.WARNING);
            return;
        }

        actionCommand = true;
        getAppointmentTemplate();
        theHC.thePatientControl.saveAppointmentTemplate(theAppointmentTemplate, vAppointmentTemplateItem);
        searchAppointmentTemplate();
        setStatus("�ѹ�֡��Ǫ��¹Ѵ�������", UpdateStatus.COMPLETE);
    }//GEN-LAST:event_jButtonSaveActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        actionCommand = false;
        jTable1.clearSelection();
        theQueueVisit = null;
        setAppointmentTemplate(initAppointmentTemplate());
        jTextFieldAppointmentTemplateName.requestFocus();
        clearSet();
    }//GEN-LAST:event_btnAddActionPerformed

    private void jComboBoxDoctorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxDoctorActionPerformed
        Employee employee = (Employee) jComboBoxDoctor.getSelectedItem();
        if (employee != null) {
            Gutil.setGuiData(jComboBoxClinic, employee.b_visit_clinic_id);
            Gutil.setGuiData(jComboBoxServicePoint, employee.default_service_id);
        }
    }//GEN-LAST:event_jComboBoxDoctorActionPerformed

    private void timeTextFieldTimeAppointmentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_timeTextFieldTimeAppointmentActionPerformed
        timeTextFieldTimeAppointment.actionPerformed(evt);
    }//GEN-LAST:event_timeTextFieldTimeAppointmentActionPerformed

    private void timeTextFieldTimeAppointmentFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_timeTextFieldTimeAppointmentFocusGained
        timeTextFieldTimeAppointment.selectAll();
    }//GEN-LAST:event_timeTextFieldTimeAppointmentFocusGained

    private void timeTextFieldTimeAppointmentFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_timeTextFieldTimeAppointmentFocusLost
        if (timeTextFieldTimeAppointment.getText() == null
                || timeTextFieldTimeAppointment.getText().isEmpty()
                || timeTextFieldTimeAppointment.getText().length() < 5) {
            this.setStatus("��سҡ�͡���ҷ��Ѵ����", UpdateStatus.WARNING);
            return;
        }
    }//GEN-LAST:event_timeTextFieldTimeAppointmentFocusLost

    private void timeTextFieldTimeAppointmentKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_timeTextFieldTimeAppointmentKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            timeTextFieldTimeAppointmentEnd.requestFocus();
        }
    }//GEN-LAST:event_timeTextFieldTimeAppointmentKeyReleased

    private void timeTextFieldTimeAppointmentEndActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_timeTextFieldTimeAppointmentEndActionPerformed
        timeTextFieldTimeAppointmentEnd.actionPerformed(evt);
        validateTimeEnd();
    }//GEN-LAST:event_timeTextFieldTimeAppointmentEndActionPerformed

    private void timeTextFieldTimeAppointmentEndFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_timeTextFieldTimeAppointmentEndFocusGained
        timeTextFieldTimeAppointmentEnd.selectAll();
    }//GEN-LAST:event_timeTextFieldTimeAppointmentEndFocusGained

    private void timeTextFieldTimeAppointmentEndFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_timeTextFieldTimeAppointmentEndFocusLost
        validateTimeEnd();
    }//GEN-LAST:event_timeTextFieldTimeAppointmentEndFocusLost

    private void btnDrugSetListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDrugSetListActionPerformed
        theHD.showDialogOrderSet(this);
    }//GEN-LAST:event_btnDrugSetListActionPerformed

    private void chkUseAutoVisitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkUseAutoVisitActionPerformed
//        if (isUseQueueVisit) {
//            btnQueueVisit.setEnabled(chkUseAutoVisit.isSelected());
//        }
        lblQueueName.setText("����к�");
    }//GEN-LAST:event_chkUseAutoVisitActionPerformed

    private void jToggleButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButton1ActionPerformed
        if (jToggleButton1.isSelected()) {
            lblNextDay.setText(String.valueOf(1 * 7));
        } else {
            lblNextDay.setText("0");
        }
    }//GEN-LAST:event_jToggleButton1ActionPerformed

    private void jToggleButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButton2ActionPerformed
        if (jToggleButton2.isSelected()) {
            lblNextDay.setText(String.valueOf(2 * 7));
        } else {
            lblNextDay.setText("0");
        }
    }//GEN-LAST:event_jToggleButton2ActionPerformed

    private void jToggleButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButton3ActionPerformed
        if (jToggleButton3.isSelected()) {
            lblNextDay.setText(String.valueOf(3 * 7));
        } else {
            lblNextDay.setText("0");
        }
    }//GEN-LAST:event_jToggleButton3ActionPerformed

    private void jToggleButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButton4ActionPerformed
        if (jToggleButton4.isSelected()) {
            lblNextDay.setText(String.valueOf(4 * 7));
        } else {
            lblNextDay.setText("0");
        }
    }//GEN-LAST:event_jToggleButton4ActionPerformed

    private void jToggleButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButton5ActionPerformed
        if (jToggleButton5.isSelected()) {
            lblNextDay.setText(String.valueOf(6 * 7));
        } else {
            lblNextDay.setText("0");
        }
    }//GEN-LAST:event_jToggleButton5ActionPerformed

    private void jToggleButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButton6ActionPerformed
        if (jToggleButton6.isSelected()) {
            lblNextDay.setText(String.valueOf(8 * 7));
        } else {
            lblNextDay.setText("0");
        }
    }//GEN-LAST:event_jToggleButton6ActionPerformed

    private void jToggleButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButton7ActionPerformed
        if (jToggleButton7.isSelected()) {
            lblNextDay.setText(String.valueOf(12 * 7));
        } else {
            lblNextDay.setText("0");
        }
    }//GEN-LAST:event_jToggleButton7ActionPerformed

    private void jToggleButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButton8ActionPerformed
        if (jToggleButton8.isSelected()) {
            lblNextDay.setText(String.valueOf(24 * 7));
        } else {
            lblNextDay.setText("0");
        }
    }//GEN-LAST:event_jToggleButton8ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        int currentDay = lblNextDay.getText().trim().isEmpty() ? 0 : Integer.parseInt(lblNextDay.getText());
        lblNextDay.setText(String.valueOf(currentDay + 7));
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        int currentDay = lblNextDay.getText().trim().isEmpty() ? 0 : Integer.parseInt(lblNextDay.getText());
        lblNextDay.setText(String.valueOf(currentDay + 28));
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        buttonGroup2.clearSelection();
        lblNextDay.setText("0");
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jCheckBoxUseSetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxUseSetActionPerformed
        CardLayout.show(jPanel4, jCheckBoxUseSet.isSelected() ? CARD_USESET : CARD_NORMAL);
        jTextFieldSetName.setEnabled(jCheckBoxUseSet.isSelected());
    }//GEN-LAST:event_jCheckBoxUseSetActionPerformed

    private void jdcDateAppointmentPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jdcDateAppointmentPropertyChange
        if (jdcDateAppointment.getDate() != null) {
            Date date = jdcDateAppointment.getDate();
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            c.add(Calendar.DAY_OF_YEAR, 1);
        }
    }//GEN-LAST:event_jdcDateAppointmentPropertyChange

    private void jButtonCreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCreateActionPerformed
        doCreate();
    }//GEN-LAST:event_jButtonCreateActionPerformed

    private void jTableDetailKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTableDetailKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            doChange();
        }
    }//GEN-LAST:event_jTableDetailKeyReleased

    /**
     * @Author: amp
     * @date: 10/8/2549
     * @see: set �������������Ѻ AppointmentTemplate
     * @return: AppointmentTemplate
     */
    private AppointmentTemplate initAppointmentTemplate() {
        AppointmentTemplate at = new AppointmentTemplate();
        at.time = theHC.theLookupControl.getTextCurrentDateTime();
        at.time_end = at.time;
        at.aptype = "��Ǩ�ѡ�ҵ�����ͧ";
        at.appointment_type = "";
        at.doctor = Gutil.getGuiData(jComboBoxDoctor);
        at.clinic = Gutil.getGuiData(jComboBoxClinic);
        at.service_point = Gutil.getGuiData(jComboBoxServicePoint);
        Employee employee = theHC.theLookupControl.readEmployeeById(Gutil.getGuiData(jComboBoxDoctor));
        if (employee != null && employee.b_visit_clinic_id != null && !employee.b_visit_clinic_id.isEmpty()) {
            at.clinic = employee.b_visit_clinic_id;
        }
        at.auto_visit = "1";
        at.next_day = 0;
        return at;
    }

    /**
     * @Author: amp
     * @date: 10/9/2549
     * @see: �Ӥ�ҷ��������� Object �ʴ��� GUI
     */
    public void setAppointmentTemplate(AppointmentTemplate at) {
        theAppointmentTemplate = at;
        jTextFieldAppointmentTemplateName.setText(theAppointmentTemplate.template_name);
        buttonGroup2.clearSelection();
        lblNextDay.setText(String.valueOf(theAppointmentTemplate.next_day));
        timeTextFieldTimeAppointment.setText(theAppointmentTemplate.time);
        timeTextFieldTimeAppointmentEnd.setText(theAppointmentTemplate.time_end);
        jTextFieldApType.setText(theAppointmentTemplate.aptype);
        Gutil.setGuiData(jComboBoxApType53, theAppointmentTemplate.appointment_type);
        Gutil.setGuiData(jComboBoxDoctor, theAppointmentTemplate.doctor);
        Gutil.setGuiData(jComboBoxClinic, theAppointmentTemplate.clinic);
        Gutil.setGuiData(jComboBoxServicePoint, theAppointmentTemplate.service_point);
        jTextAreaDescription.setText(theAppointmentTemplate.description);
        chkUseAutoVisit.setSelected(theAppointmentTemplate.auto_visit.equals("1"));
//        btnQueueVisit.setEnabled(chkUseAutoVisit.isSelected());
        lblQueueName.setText("����к�");
        if (isUseQueueVisit) {
            QueueVisit qv = theHC.theLookupControl.readQueueVisitById(theAppointmentTemplate.queue_visit_id);
            if (qv != null) {
                lblQueueName.setText(qv.description);
            }
        }
        if (AppointmentTemplate.DM.equals(theAppointmentTemplate.getObjectId())
                || AppointmentTemplate.HT.equals(theAppointmentTemplate.getObjectId())
                || AppointmentTemplate.H.equals(theAppointmentTemplate.getObjectId())) {
            jTextFieldAppointmentTemplateName.setEnabled(false);
            jButtonDel.setEnabled(false);
        } else {
            jTextFieldAppointmentTemplateName.setEnabled(true);
            jButtonDel.setEnabled(true);
        }

        jCheckBoxUseSet.setSelected(theAppointmentTemplate.template_appointment_use_set);
        jCheckBoxUseSetActionPerformed(null);
        jTextFieldSetName.setText(theAppointmentTemplate.template_appointment_set_name);
        setDetail(theAppointmentTemplate.template_appointment_detail);

        // clear search item table
        setTableItem(null);
        //��Ҥ�� commonname �Ҵ���
        vAppointmentTemplateItem = theHC.thePatientControl.listAppointmentTemplateItem(theAppointmentTemplate.getObjectId());
        setTableAppointmentTemplateItem(vAppointmentTemplateItem);
    }

    /**
     * @Author: amp
     * @date: 10/09/2549
     * @see: �ʴ���¡�� item ��������ǧ˹��
     * @param:
     */
    private void setTableAppointmentTemplateItem(Vector vc) {
        String[] column = {GuiLang.setLanguage("��¡�õ�Ǩ�ѡ����ǧ˹��")};
        TaBleModel tm;
        if (vc != null) {
            tm = new TaBleModel(column, vc.size());
            for (int i = 0, size = vc.size(); i < size; i++) {
                AppointmentTemplateItem apti = (AppointmentTemplateItem) vc.get(i);
                tm.setValueAt(apti.item_common_name, i, 0);
            }
        } else {
            tm = new TaBleModel(column, 0);
        }
        jTableAppointmentOrder.setModel(tm);
    }

    /**
     * @Author: amp
     * @date: 10/09/2549
     * @see: ���Dialog
     */
    public void showDialog() {
        searchAppointmentTemplate();
        isUseQueueVisit = Gutil.isSelected(theHC.theLookupControl.readOption().inqueuevisit);
        setEnableAll(true);
        btnAdd.doClick();
        clearSet();
        setVisible(true);
    }

    /**
     * @Author: amp
     * @date: 10/09/2549
     * @see: ���ҵ�Ǫ��¹Ѵ
     */
    private void searchAppointmentTemplate() {
        vAppointmentTemplate = theHC.thePatientControl.listAppointmentTemplateByName(jTextFieldSearch.getText());
        setTableAppointmentTemplate(vAppointmentTemplate);
    }

    /**
     * @Author: amp
     * @date: 10/08/2549
     * @see: �ʴ���Ǫ��¹Ѵ�����㹵��ҧ
     * @param: Vector ��Ǫ��¹Ѵ (AppointmentTemplate)
     */
    private void setTableAppointmentTemplate(Vector vc) {
        TaBleModel tm;
        if (vc == null || vc.isEmpty()) {
            tm = new TaBleModel(col_head, 0);
            jTable1.setModel(tm);
            return;
        }
        tm = new TaBleModel(col_head, vc.size());
        AppointmentTemplate apt;
        for (int i = 0; i < vc.size(); i++) {
            apt = (AppointmentTemplate) vc.get(i);
            tm.setValueAt(apt.template_name, i, 0);
            tm.setValueAt(theHC.theLookupControl.getEmployeeName(apt.doctor), i, 1);
            tm.setValueAt(apt.time + (apt.time_end == null || apt.time_end.isEmpty() ? "" : (" - " + apt.time_end)), i, 2);
        }
        jTable1.setModel(tm);
        jTable1.getColumnModel().getColumn(0).setPreferredWidth(100);
        jTable1.getColumnModel().getColumn(0).setCellRenderer(cellRendererHos);
        jTable1.getColumnModel().getColumn(1).setPreferredWidth(100);
        jTable1.getColumnModel().getColumn(1).setCellRenderer(cellRendererHos);
        jTable1.getColumnModel().getColumn(2).setPreferredWidth(100);
        jTable1.getColumnModel().getColumn(2).setCellRenderer(cellRendererHos);
    }

    /**
     * @Author : amp
     * @date : 10/09/2549
     * @see : �ʴ���¡�� item ������
     * @param: Vector �ͧ item
     */
    private void setTableItem(Vector vc) {
        vItem = vc;
        String[] column = {GuiLang.setLanguage("��¡�õ�Ǩ�ѡ��")};
        TaBleModel tm;
        if (vc == null) {
            tm = new TaBleModel(column, 0);
            jTableItem.setModel(tm);
            return;
        }
        tm = new TaBleModel(column, vc.size());
        for (int i = 0, size = vc.size(); i < size; i++) {
            Item item = (Item) vc.get(i);
            tm.setValueAt(item.common_name, i, 0);
        }
        jTableItem.setModel(tm);
    }

    @Override
    public JFrame getJFrame() {
        return this;
    }

    /**
     * ૵�������Dialog
     */
    private void setDialog() {
        setSize(1080, 735);
        setTitle(Constant.getTextBundle("��Ǫ��¹Ѵ"));
        setLocationRelativeTo(null);
    }

    /**
     * @Author: amp
     * @date: 9/8/2549
     * @see: ���� Item ¡����Ż���Դ
     * @param: �ӹǹ week
     */
    private void searchItem() {
        String search = jTextFieldSearchOrder.getText();
        setTableItem(theHC.theOrderControl.listItemByGroup("", search));
    }

    /**
     * @Author: amp
     * @date: 10/08/2549
     * @see: set ��Һ� GUI ���Ѻ Object
     */
    public void getAppointmentTemplate() {
        theAppointmentTemplate.template_name = jTextFieldAppointmentTemplateName.getText();
        theAppointmentTemplate.next_day = lblNextDay.getText().trim().isEmpty() ? 0 : Integer.parseInt(lblNextDay.getText());
        theAppointmentTemplate.time = timeTextFieldTimeAppointment.getText();
        theAppointmentTemplate.time_end = timeTextFieldTimeAppointmentEnd.getText();
        theAppointmentTemplate.aptype = jTextFieldApType.getText();
        theAppointmentTemplate.appointment_type = Gutil.getGuiData(jComboBoxApType53);
        theAppointmentTemplate.service_point = Gutil.getGuiData(jComboBoxServicePoint);
        theAppointmentTemplate.doctor = Gutil.getGuiData(jComboBoxDoctor);
        theAppointmentTemplate.clinic = Gutil.getGuiData(jComboBoxClinic);
        theAppointmentTemplate.description = jTextAreaDescription.getText();
        if (chkUseAutoVisit.isSelected()) {
            theAppointmentTemplate.auto_visit = "1";
        } else {
            theAppointmentTemplate.auto_visit = "0";
        }
        theAppointmentTemplate.queue_visit_id = null;
        if (theQueueVisit != null) {
            theAppointmentTemplate.queue_visit_id = theQueueVisit.getObjectId();
        }
        if (jCheckBoxUseSet.isSelected()) {
            theAppointmentTemplate.template_appointment_use_set = jCheckBoxUseSet.isSelected();
            theAppointmentTemplate.template_appointment_times = Integer.parseInt(integerTextFieldTime.getText());
            theAppointmentTemplate.template_appointment_set_name = jTextFieldSetName.getText();
            theAppointmentTemplate.template_appointment_detail = getDetail();
        }
    }

    /**
     * ૵������ҧ�
     */
    private void setEnableAll(boolean b) {
        timeTextFieldTimeAppointment.setEditable(b);
        timeTextFieldTimeAppointmentEnd.setEditable(b);
        btnAdd.setEnabled(b);
        jTextFieldApType.setEnabled(b);
        jComboBoxApType53.setEnabled(b);
        jComboBoxServicePoint.setEnabled(b);
        jComboBoxDoctor.setEnabled(b);
        jComboBoxClinic.setEnabled(b);
        jTextAreaDescription.setEnabled(b);
        jButtonSave.setEnabled(b);
        jButtonDel.setEnabled(b);
        this.chkUseAutoVisit.setEnabled(b);
        this.btnQueueVisit.setEnabled(b);
        jTextFieldSearchOrder.setEnabled(b);
        jButtonAddOrder.setEnabled(b);
        jButtonDelOrder.setEnabled(b);
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnDrugSetList;
    private javax.swing.JButton btnQueueVisit;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JCheckBox chkUseAutoVisit;
    private com.hospital_os.utility.IntegerTextField integerTextFieldNextDay;
    private com.hospital_os.utility.IntegerTextField integerTextFieldTime;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton12;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton36;
    private javax.swing.JButton jButton38;
    private javax.swing.JButton jButton40;
    private javax.swing.JButton jButton42;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButtonAddOrder;
    private javax.swing.JButton jButtonCreate;
    private javax.swing.JButton jButtonDel;
    private javax.swing.JButton jButtonDelOrder;
    private javax.swing.JButton jButtonSave;
    private javax.swing.JCheckBox jCheckBoxUseSet;
    private com.hosv3.gui.component.HosComboBox jComboBoxApType53;
    private javax.swing.JComboBox jComboBoxClinic;
    private javax.swing.JComboBox jComboBoxDoctor;
    private javax.swing.JComboBox jComboBoxServicePoint;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelStatus;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel23;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JPanel jPanelSearch;
    private javax.swing.JPanel jPanelTime;
    private javax.swing.JPanel jPanelUseSet;
    protected javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    protected com.hosv3.gui.component.HJTableSort jTable1;
    private com.hosv3.gui.component.HJTableSort jTableAppointmentOrder;
    private com.hosv3.gui.component.HJTableSort jTableDetail;
    private com.hosv3.gui.component.HJTableSort jTableItem;
    private com.hosv3.gui.component.BalloonTextArea jTextAreaDescription;
    private javax.swing.JTextField jTextFieldApType;
    private javax.swing.JTextField jTextFieldAppointmentTemplateName;
    private javax.swing.JTextField jTextFieldSearch;
    private javax.swing.JTextField jTextFieldSearchOrder;
    private javax.swing.JTextField jTextFieldSetName;
    private javax.swing.JToggleButton jToggleButton1;
    private javax.swing.JToggleButton jToggleButton2;
    private javax.swing.JToggleButton jToggleButton3;
    private javax.swing.JToggleButton jToggleButton4;
    private javax.swing.JToggleButton jToggleButton5;
    private javax.swing.JToggleButton jToggleButton6;
    private javax.swing.JToggleButton jToggleButton7;
    private javax.swing.JToggleButton jToggleButton8;
    private sd.comp.jcalendar.JDateChooser jdcDateAppointment;
    private javax.swing.JLabel lblNextDay;
    private javax.swing.JLabel lblQueueName;
    private javax.swing.JPanel panelChooseTimeEnd;
    private javax.swing.JPanel panelChooseTimeEnd2;
    private javax.swing.JPanel panelChooseTimeStart;
    private javax.swing.JPanel panelChooseTimeStart1;
    private javax.swing.JPanel panelNextAppDay;
    private com.hospital_os.utility.TimeTextField timeTextFieldEndTime;
    private com.hospital_os.utility.TimeTextField timeTextFieldTime;
    private com.hospital_os.utility.TimeTextField timeTextFieldTimeAppointment;
    private com.hospital_os.utility.TimeTextField timeTextFieldTimeAppointmentEnd;
    // End of variables declaration//GEN-END:variables

    private void validateTimeEnd() {
        if (timeTextFieldTimeAppointmentEnd.getText().isEmpty()
                || timeTextFieldTimeAppointmentEnd.getText().length() < 5) {
            this.setStatus("��سҡ�͡��������ش��ùѴ����", UpdateStatus.WARNING);
            jButtonSave.setEnabled(false);
        } else {
            /**
             * currTime �����һѨ�غѹ printTime �����ҷ����������
             */
            String currTime = timeTextFieldTimeAppointment.getTextTime();
            String printTime = timeTextFieldTimeAppointmentEnd.getTextTime();

            /**
             * hhCurr �纪�������Ѩ�غѹ mmCurr �纹ҷջѨ�غѹ hhPrint
             * �纪������������� mmPrint �纹ҷշ������
             */
            int hhCurr = Integer.parseInt(currTime.substring(0, 2));
            int mmCurr = Integer.parseInt(currTime.substring(3, 5));
            int hhPrint = Integer.parseInt(printTime.substring(0, 2));
            int mmPrint = Integer.parseInt(printTime.substring(3, 5));
            if (hhPrint > hhCurr) {
                jButtonSave.setEnabled(true);
            }
            if (hhPrint < hhCurr) {
                JOptionPane.showMessageDialog(this, Constant.getTextBundle("�������ö��˹���������ش��ùѴ���¡������ҹѴ��"), Constant.getTextBundle("��͹"), JOptionPane.WARNING_MESSAGE);
                jButtonSave.setEnabled(false);
                timeTextFieldTimeAppointmentEnd.setText(timeTextFieldTimeAppointment.getText());
            }
            if (hhPrint == hhCurr) {
                if (mmPrint >= mmCurr) {
                    jButtonSave.setEnabled(true);
                } else {
                    JOptionPane.showMessageDialog(this, Constant.getTextBundle("�������ö��˹���������ش��ùѴ���¡������ҹѴ��"), Constant.getTextBundle("��͹"), JOptionPane.WARNING_MESSAGE);
                    jButtonSave.setEnabled(false);
                    timeTextFieldTimeAppointmentEnd.setText(timeTextFieldTimeAppointment.getText());
                }
            }
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName().equals("AddItems")) {
            Map<String, Item> map = (Map<String, Item>) evt.getNewValue();
            for (String key : map.keySet()) {
                doAddAppointmentTemplateItem(key, map.get(key));
            }
        }
    }

    private void doAddAppointmentTemplateItem(String itemSetId, Item item) {
        if (vAppointmentTemplateItem == null) {
            vAppointmentTemplateItem = new Vector();
        }
        if (vAppointmentTemplateItem.isEmpty())//�ͺ�á��� add �����
        {
            AppointmentTemplateItem appointmentTemplateItem = new AppointmentTemplateItem();
            appointmentTemplateItem.item_id = item.getObjectId();
            appointmentTemplateItem.item_common_name = item.common_name;
            if (itemSetId != null && !itemSetId.trim().isEmpty()) {
                appointmentTemplateItem.b_item_set_id = itemSetId;
            }
            vAppointmentTemplateItem.add(appointmentTemplateItem);
        } else {
            boolean isSame = false;
            for (int j = 0; j < vAppointmentTemplateItem.size(); j++) {
                if (item.getObjectId().equals(((AppointmentTemplateItem) vAppointmentTemplateItem.get(j)).item_id)) {
                    isSame = true;
                }
            }
            if (!isSame) {
                AppointmentTemplateItem appointmentOrder = new AppointmentTemplateItem();
                appointmentOrder.item_id = item.getObjectId();
                appointmentOrder.item_common_name = item.common_name;
                if (itemSetId != null && !itemSetId.trim().isEmpty()) {
                    appointmentOrder.b_item_set_id = itemSetId;
                }
                vAppointmentTemplateItem.add(appointmentOrder);
            }
        }
        setTableAppointmentTemplateItem(vAppointmentTemplateItem);
    }

    private void doCreate() {
        int time = -1;
        try {
            time = Integer.parseInt(integerTextFieldTime.getText());
            if (time <= 0) {
                this.setStatus("��سҡ�͡�ӹǹ���駷��Ѵ", UpdateStatus.WARNING);
                return;
            }
        } catch (NumberFormatException e) {
            this.setStatus("��سҡ�͡�ӹǹ���駷��Ѵ", UpdateStatus.WARNING);
            return;
        }
        int nextDays = -1;
        try {
            nextDays = Integer.parseInt(integerTextFieldNextDay.getText());
            if (nextDays <= 0) {
                this.setStatus("��سҡ�͡�ӹǹ�ѹ�Ѵ价��Ѵ", UpdateStatus.WARNING);
                return;
            }
        } catch (NumberFormatException e) {
            this.setStatus("��سҡ�͡�ӹǹ�ѹ�Ѵ价��Ѵ", UpdateStatus.WARNING);
            return;
        }
        java.util.List<ComplexDataSource> dataSource = new ArrayList<>();
        for (int i = 1; i <= time; i++) {
            String date = DateUtil.convertDateToString(jdcDateAppointment.getDate(), "yyyy/MM/dd", DateUtil.LOCALE_TH);
            if (i > 1) {
                nextDays += Integer.parseInt(integerTextFieldNextDay.getText());
                date = DateUtil.calDatefuture(date, nextDays);
            } else {
                nextDays = 0;
            }
            Object[] detail = new Object[]{nextDays, date, timeTextFieldTime.getTextTime(), timeTextFieldEndTime.getTextTime(), ""};
            dataSource.add(new ComplexDataSource(i, detail));
        }
        listDetail = dataSource;
        setTableAppointmentTempateSet();

    }

    private void setTableAppointmentTempateSet() {
        TaBleModel tm;
        if (listDetail != null || !listDetail.isEmpty()) {
            tm = new TaBleModel(colSetHeader, listDetail.size());
            tm.setEditingCol(1, 3, 4, 5);
            for (int i = 0, size = listDetail.size(); i < size; i++) {
                ComplexDataSource data = (ComplexDataSource) listDetail.get(i);
                String showDate = DateUtil.convertDatePattern(data.getValue(1).toString(), "yyyy/MM/dd", "E dd MMMM yyyy");
                tm.setValueAt(data.getId(), i, 0);
                tm.setValueAt(data.getValue(0), i, 1);
                tm.setValueAt(showDate, i, 2);
                tm.setValueAt(data.getValue(2), i, 3);
                tm.setValueAt(data.getValue(3), i, 4);
                tm.setValueAt(data.getValue(4), i, 5);
            }
        } else {
            tm = new TaBleModel(colSetHeader, 0);
        }
        jTableDetail.setModel(tm);
        jTableDetail.getColumnModel().getColumn(0).setPreferredWidth(10);
        jTableDetail.getColumnModel().getColumn(0).setCellRenderer(centerRender);
        jTableDetail.getColumnModel().getColumn(1).setPreferredWidth(60);
        jTableDetail.getColumnModel().getColumn(1).setCellRenderer(rightRender);
        jTableDetail.getColumnModel().getColumn(2).setPreferredWidth(120);
        jTableDetail.getColumnModel().getColumn(3).setPreferredWidth(50);
        jTableDetail.getColumnModel().getColumn(4).setPreferredWidth(50);
        jTableDetail.getColumnModel().getColumn(5).setPreferredWidth(150);
    }

    private void doChange() {
        int selectedRow = jTableDetail.getSelectedRow();
        int selectedCol = jTableDetail.getSelectedColumn();
        ComplexDataSource tcds = (ComplexDataSource) listDetail.get(selectedRow);

        if (selectedRow == 0 && selectedCol != 5) {
            this.setStatus("����ö��䢢����Ţͧ���駷�� 1 ��੾�� ��������´", UpdateStatus.WARNING);
            jTableDetail.setValueAt(tcds.getValue(selectedCol - 1), selectedRow, selectedCol);
            return;
        }
        Object obj = jTableDetail.getValueAt(selectedRow, selectedCol);

        switch (selectedCol) {
            case 1:
                int nextDays = Integer.parseInt(obj.toString());
                if (nextDays <= 0) {
                    this.setStatus("��سҡ�͡�ӹǹ�ѹ�Ѵ价��Ѵ", UpdateStatus.WARNING);
                    jTableDetail.setValueAt(tcds.getValue(0), selectedRow, 1);
                    return;
                }
                String startDate = listDetail.get(0).getValue(1).toString();
                String nextDate = DateUtil.calDatefuture(startDate, nextDays);
                String showDate = DateUtil.convertDatePattern(nextDate, "yyyy/MM/dd", "E dd MMMM yyyy");
                tcds.setValue(0, nextDays);
                tcds.setValue(1, nextDate);
                jTableDetail.setValueAt(obj, selectedRow, 1);
                jTableDetail.setValueAt(showDate, selectedRow, 2);
                break;
            case 3:
                String time = obj.toString();
                if (!checkTimeFormat(time)) {
                    jTableDetail.setValueAt(tcds.getValue(2), selectedRow, 3);
                    return;
                }
                tcds.setValue(2, time);
                jTableDetail.setValueAt(time, selectedRow, 3);
                break;
            case 4:
                String endTime = obj.toString();
                String startTime = tcds.getValue(2).toString();
                if (!checkTimeFormat(endTime)) {
                    jTableDetail.setValueAt(tcds.getValue(3), selectedRow, 4);
                    return;
                }
                if (!DateUtil.checkTime(endTime, startTime)) {
                    this.setStatus("���ҹѴ����ش ��ͧ�ҡ�������ҹѴ�������", UpdateStatus.WARNING);
                    jTableDetail.setValueAt(tcds.getValue(3), selectedRow, 4);
                    return;
                }
                tcds.setValue(3, endTime);
                jTableDetail.setValueAt(endTime, selectedRow, 4);
                break;
            case 5:
                String text = obj.toString();
                tcds.setValue(4, text);
                jTableDetail.setValueAt(text, selectedRow, 5);
                break;
        }
    }

    private boolean checkTimeFormat(String time) {
        if (time.isEmpty() || !time.contains(":") || !(time.length() >= 5)) {
            this.setStatus("��سҡ�͡���ҷ��������� �ٻẺ HH:mm ", UpdateStatus.WARNING);
            return false;
        }
        try {
            String hh = time.substring(0, 2);
            String mm = time.substring(3, 5);
            if (Integer.parseInt(hh) > 24 || Integer.parseInt(mm) > 60) {
                this.setStatus("��͡�������١��ͧ ", UpdateStatus.WARNING);
                return false;
            }
        } catch (Exception e) {
            this.setStatus("��͡�ٻẺ�������١��ͧ", UpdateStatus.WARNING);
            return false;
        }
        return true;
    }

    private String getDetail() {
        JSONObject jsonData = new JSONObject();
        JSONArray arrayDetail = new JSONArray();
        for (int i = 0; i < listDetail.size(); i++) {
            ComplexDataSource tcds = listDetail.get(i);
            JSONObject data = new JSONObject();
            data.put("times", tcds.getId());
            JSONObject obj = new JSONObject();
            Object[] values = tcds.getValues();
            obj.put("days", values[0]);
            obj.put("date", values[1].toString());
            obj.put("start_time", values[2].toString());
            obj.put("end_time", values[3].toString());
            obj.put("description", values[4].toString());
            data.put("detail", obj);
            arrayDetail.put(i, data);
        }
        jsonData.put("data", arrayDetail);
        return jsonData.toString();
    }

    private void setDetail(String detailJSON) {
        listDetail = new ArrayList<>();
        if (detailJSON != null && !detailJSON.isEmpty()) {
            JSONArray arrayDetail = new JSONObject(detailJSON).getJSONArray("data");
            for (int i = 0; i < arrayDetail.length(); i++) {
                JSONObject data = arrayDetail.getJSONObject(i);
                Object times = data.get("times");
                JSONObject obj = data.getJSONObject("detail");
                Object[] values = new Object[]{
                    (Object) obj.getInt("days"), obj.get("date"),
                    obj.get("start_time"),
                    obj.get("end_time"),
                    obj.get("description")
                };
                listDetail.add(new ComplexDataSource(times, values));
            }
        }
        setTableAppointmentTempateSet();
    }

    private void clearSet() {
        integerTextFieldTime.setText("0");
        integerTextFieldNextDay.setText("0");
        jdcDateAppointment.setDate(new Date());
        timeTextFieldTime.setText("08:00");
        timeTextFieldEndTime.setText("16:00");
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.utility;

import java.util.Comparator;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class DataSource implements Comparator<DataSource>{

    private Object id;
    private String value;

    public DataSource() {
    }

    public DataSource(Object id, String value) {
        this.id = id;
        this.value = value;
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value;
    }
    
//    @Override
//    public int compare(CompareObject o1, CompareObject o2) {
//       
//        if (comparePersonid == 0) {
//            int compareDateServ = o1.getUpdateDate().compareTo(o2.getUpdateDate());
//            return compareDateServ;
//        } else {
//            return comparePersonid;
//        }
//    }

    @Override
    public int compare(DataSource o1, DataSource o2) {
         return String.valueOf(o1.getId()).compareTo(String.valueOf(o2.getId()));
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class InsuranceClaimSubgroup extends Persistent {

    public String t_insurance_claim_id = "";
    public String b_item_billing_subgroup_id = "";
    public String t_billing_invoice_billing_subgroup_id = "";
    public double subgroup_total = 0.0d;
    public double insurance_discount = 0.0d;
    public double insurance_pay = 0.0d;
    public double subgroup_remain = 0.0d;
    public String record_datetime = "";
    public String user_record_id = "";
    // object only
    public String billingSubgroupName = "";
}

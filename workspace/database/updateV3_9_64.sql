-- #284
BEGIN;
INSERT INTO f_gui_action SELECT '0423','ตัวช่วยนัด',''
WHERE NOT EXISTS (SELECT 1 FROM f_gui_action WHERE f_gui_action_id = '0423'); 
COMMIT;

-- เพิ่ม field
ALTER TABLE b_template_appointment ADD COLUMN IF NOT EXISTS template_appointment_use_set BOOLEAN NOT NULL DEFAULT FALSE;
ALTER TABLE b_template_appointment ADD COLUMN IF NOT EXISTS template_appointment_times INTEGER NOT NULL DEFAULT 0;
ALTER TABLE b_template_appointment ADD COLUMN IF NOT EXISTS template_appointment_detail JSONB DEFAULT NULL;
ALTER TABLE b_template_appointment ADD COLUMN IF NOT EXISTS template_appointment_set_name VARCHAR(255);

-- #286
ALTER TABLE t_patient_appointment ADD COLUMN IF NOT EXISTS patient_appointment_use_set BOOLEAN NOT NULL DEFAULT FALSE;
ALTER TABLE t_patient_appointment ADD COLUMN IF NOT EXISTS patient_appointment_name VARCHAR(255);
ALTER TABLE t_patient_appointment ADD COLUMN IF NOT EXISTS b_template_appointment_id VARCHAR(255);

CREATE SEQUENCE IF NOT EXISTS patient_appointment_req;
ALTER TABLE t_patient_appointment ADD COLUMN IF NOT EXISTS ref_code INTEGER NOT NULL DEFAULT nextval('patient_appointment_req'::regclass);

BEGIN;
INSERT INTO r_rp1853_aptype SELECT 'C19','Vaccine COVID-19','วัคซีน Covid 19'
WHERE NOT EXISTS (SELECT 1 FROM r_rp1853_aptype WHERE id = 'C19');
COMMIT;

update b_template_appointment set template_appointment_queue_visit_id = 
case 
	when template_appointment_queue_visit_id = '' then null 
	else template_appointment_queue_visit_id
end;

-- 288
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
ALTER TABLE t_patient ADD COLUMN patient_guid UUID NOT NULL DEFAULT uuid_generate_v4();
ALTER TABLE t_visit ADD COLUMN visit_guid UUID NOT NULL DEFAULT uuid_generate_v4();

INSERT INTO b_restful_url (b_restful_url_id, restful_code, restful_url, user_update_id, is_system)
VALUES  ('999' || (select b_visit_office_id from b_site) || 'cvp1.moph','cvp1.moph','http://' || host(inet_server_addr()) ||':9090?x-api-key=[YOUR_API_KEY]',(select rpad('157'||b_visit_office_id,18,'0')  from b_site), true);

-- update db version
INSERT INTO s_version VALUES ('9701000000098', '98', 'Hospital OS, Community Edition', '3.9.64', '3.44.0', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_64.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.64');
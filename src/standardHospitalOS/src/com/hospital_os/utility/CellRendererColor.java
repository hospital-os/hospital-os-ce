/* 
 * ColorRenderer.java (compiles with releases 1.2, 1.3, and 1.4) is used by 
 * TableDialogEditDemo.java.
 */
package com.hospital_os.utility;

import java.awt.Color;
import java.awt.Component;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.table.TableCellRenderer;

@SuppressWarnings("ClassWithoutLogger")
public class CellRendererColor extends JLabel implements TableCellRenderer {

    private static final long serialVersionUID = 1L;
    Border unselectedBorder = null;
    Border selectedBorder = null;
    boolean isBordered = true;

    public CellRendererColor(boolean isBordered) {
        this.isBordered = isBordered;
        setOpaque(true);
    }

    @Override
    public Component getTableCellRendererComponent(
            JTable table, Object color,
            boolean isSelected, boolean hasFocus,
            int row, int column) {

        String dd = (String) color;
        String col = dd.substring(0, dd.indexOf('|'));
        String ss = dd.substring(dd.indexOf('|') + 1);
        Color newColor = Gutil.reconvertColor(col);
        setBackground(newColor);
        if (isBordered) {
            if (isSelected) {
                if (selectedBorder == null) {
                    selectedBorder = BorderFactory.createMatteBorder(2, 5, 2, 5,
                            table.getSelectionBackground());
                }
                setBorder(selectedBorder);
            } else {
                if (unselectedBorder == null) {
                    unselectedBorder = BorderFactory.createMatteBorder(2, 5, 2, 5,
                            table.getBackground());
                }
                setBorder(unselectedBorder);
            }
        }
        setToolTipText(ss);
        return this;
    }
}

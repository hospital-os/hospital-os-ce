/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class PrintingOtherLanguage extends Persistent {

    public String b_printing_id = "";
    public String b_language_id = "";
    public String jrxml = "";
    // object only
    public String language = "";
}

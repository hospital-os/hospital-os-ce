/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.objdb.objdbclass;

import com.hospital_os.usecase.connection.ConnectionInf;
import com.pcu.object.Cancer;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class CancerDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "808";

    public CancerDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public int insert(Cancer obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO t_health_cancer(\n"
                    + "            t_health_cancer_id, t_person_id, t_visit_id, health_cancer_survey_date, health_cancer_breast_exam, \n"
                    + "            health_cancer_cervix_exam, health_cancer_cervix_method, health_cancer_breast_exam_note, \n"
                    + "            health_cancer_cervix_exam_note, health_cancer_note, health_cancer_first_check, \n"
                    + "            active, user_record_id, record_date_time, user_modify_id, modify_date_time)\n"
                    + "    VALUES (?, ?, ?, ?, ?, \n"
                    + "            ?, ?, ?, \n"
                    + "            ?, ?, ?, \n"
                    + "            ?, ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getGenID(tableId));
            preparedStatement.setString(2, obj.t_person_id);
            preparedStatement.setString(3, obj.t_visit_id);
            preparedStatement.setString(4, obj.health_cancer_survey_date);
            preparedStatement.setString(5, obj.health_cancer_breast_exam);
            preparedStatement.setString(6, obj.health_cancer_cervix_exam);
            preparedStatement.setString(7, obj.health_cancer_cervix_method);
            preparedStatement.setString(8, obj.health_cancer_breast_exam_note);
            preparedStatement.setString(9, obj.health_cancer_cervix_exam_note);
            preparedStatement.setString(10, obj.health_cancer_note);
            preparedStatement.setString(11, obj.health_cancer_first_check);
            preparedStatement.setString(12, obj.active);
            preparedStatement.setString(13, obj.user_record_id);
            preparedStatement.setString(14, obj.record_date_time);
            preparedStatement.setString(15, obj.user_modify_id);
            preparedStatement.setString(16, obj.modify_date_time);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(Cancer obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_health_cancer\n"
                    + "   SET health_cancer_survey_date=?, \n"
                    + "       health_cancer_breast_exam=?, health_cancer_cervix_exam=?, health_cancer_cervix_method=?, \n"
                    + "       health_cancer_breast_exam_note=?, health_cancer_cervix_exam_note=?, \n"
                    + "       health_cancer_note=?, health_cancer_first_check=?, active=?, \n"
                    + "       user_modify_id=?, modify_date_time=?, record_date_time=?\n");
            sql.append(" WHERE t_health_cancer_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.health_cancer_survey_date);
            preparedStatement.setString(2, obj.health_cancer_breast_exam);
            preparedStatement.setString(3, obj.health_cancer_cervix_exam);
            preparedStatement.setString(4, obj.health_cancer_cervix_method);
            preparedStatement.setString(5, obj.health_cancer_breast_exam_note);
            preparedStatement.setString(6, obj.health_cancer_cervix_exam_note);
            preparedStatement.setString(7, obj.health_cancer_note);
            preparedStatement.setString(8, obj.health_cancer_first_check);
            preparedStatement.setString(9, obj.active);
            preparedStatement.setString(10, obj.user_modify_id);
            preparedStatement.setString(11, obj.modify_date_time);
            preparedStatement.setString(12, obj.record_date_time);
            preparedStatement.setString(13, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int inactive(Cancer obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_health_cancer\n");
            sql.append("   SET active=?, user_cancel_id=?, cancel_date_time=?\n");
            sql.append(" WHERE t_health_cancer_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.active);
            preparedStatement.setString(2, obj.user_cancel_id);
            preparedStatement.setString(3, obj.cancel_date_time);
            preparedStatement.setString(4, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public Cancer select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from t_health_cancer where t_health_cancer_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<Cancer> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<Cancer> listByPersonId(String pid) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from t_health_cancer where t_person_id = ? and active = '1'";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, pid);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<Cancer> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<Cancer> list = new ArrayList<Cancer>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Cancer obj = new Cancer();
                obj.setObjectId(rs.getString("t_health_cancer_id"));
                obj.t_person_id = rs.getString("t_person_id");
                obj.t_visit_id = rs.getString("t_visit_id");
                obj.health_cancer_survey_date = rs.getString("health_cancer_survey_date");
                obj.health_cancer_breast_exam = rs.getString("health_cancer_breast_exam");
                obj.health_cancer_cervix_exam = rs.getString("health_cancer_cervix_exam");
                obj.health_cancer_cervix_method = rs.getString("health_cancer_cervix_method");
                obj.health_cancer_breast_exam_note = rs.getString("health_cancer_breast_exam_note");
                obj.health_cancer_cervix_exam_note = rs.getString("health_cancer_cervix_exam_note");
                obj.health_cancer_note = rs.getString("health_cancer_note");
                obj.health_cancer_first_check = rs.getString("health_cancer_first_check");
                obj.active = rs.getString("active");
                obj.user_record_id = rs.getString("user_record_id");
                obj.record_date_time = rs.getString("record_date_time");
                obj.user_modify_id = rs.getString("user_modify_id");
                obj.modify_date_time = rs.getString("modify_date_time");
                obj.user_cancel_id = rs.getString("user_cancel_id");
                obj.cancel_date_time = rs.getString("cancel_date_time");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author tanakrit
 */
public class OrderPackage extends Persistent {

    public String t_order_id;
    public String t_order_sub_id;
    public int order_seq = 0;
    public String active = "1";
}

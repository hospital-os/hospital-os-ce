/*
 * CellRendererOrderItem.java
 *
 * Created on 31 �ԧ�Ҥ� 2548, 10:53 �.
 */
package com.hospital_os.utility;

import com.hospital_os.object.Item;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author kingland
 */
@SuppressWarnings("ClassWithoutLogger")
public class CellRendererItem extends JLabel implements TableCellRenderer {

    private static final long serialVersionUID = 1L;
    boolean isBordered = true;

    /**
     * Creates a new instance of CellRendererOrderItem
     */
    public CellRendererItem(boolean isBordered) {
        this.isBordered = isBordered;
        setOpaque(true);
    }

    @Override
    public java.awt.Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        if (value instanceof Item) {
            Item i = (Item) value;
            setText(i.common_name);
            setToolTipText(i.common_name);
        } else if (value instanceof String[]) {
            String[] tmp = (String[]) value;
            setText(tmp[0]);
            setToolTipText("<html><body><font color=green><b>"
                    + tmp[1]
                    + "</b></font><br><font color=blue><b>"
                    + tmp[0]
                    + "</b></font></body><html>");
        }
        if (isSelected) {
            this.setBackground(table.getSelectionBackground());
            this.setForeground(table.getSelectionForeground());
        } else {
            this.setBackground(table.getBackground());
            this.setForeground(table.getForeground());
        }
        return this;
    }
}

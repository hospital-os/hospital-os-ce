/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.control.lookup;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.LookupControlInf;
import com.hosv3.control.LookupControl;
import java.util.Vector;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public class DosageFormLookup implements LookupControlInf {

    private LookupControl thePC;

    public DosageFormLookup(LookupControl thePC) {
        this.thePC = thePC;
    }

    @Override
    public CommonInf readHosData(String pk) {
        return thePC.readDrugDosageFormById(pk);
    }

    @Override
    public Vector listData(String str) {
        Vector vc = thePC.listDrugDosageForm(str);
        return vc;
    }
}

/*
 * PrintSummaryReport2.java
 *
 * Created on 9 �á�Ҥ� 2551, 10:47 �.
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package com.hosv3.object.printobject;

import com.printing.object.SummaryReport.PrintSummaryReport;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Pu
 */
public class PrintSummaryReport2 extends PrintSummaryReport {

    public String picd10_primary = "icd10_primary";
    public String picd10_comorbidity = "icd10_comorbidity";
    public String picd10_complication = "icd10_complication";
    public String picd10_external = "icd10_external";
    public String picd10_other = "icd10_other";
    public String visit_age = "visit_age";
    public String visit_age_year = "visit_age_year";
    public String visit_age_month = "visit_age_month";
    public String visit_age_day = "visit_age_day";
    public String patient_phone_number = "patient_phone_number";
    public String patient_mobile_phone_number = "patient_mobile_phone_number";
    public String patient_email = "patient_email";
    public Map printSummaryReport2;

    /**
     * Creates a new instance of PrintSummaryReport2
     */
    public PrintSummaryReport2() {
        printSummaryReport2 = new HashMap();
    }

    public void setPrimaryICD10(String name) {
        setMap(picd10_primary, name);
    }

    public void setComorbidityICD10(String name) {
        setMap(picd10_comorbidity, name);
    }

    public void setComplicationICD10(String name) {
        setMap(picd10_complication, name);
    }

    public void setExternalICD10(String name) {
        setMap(picd10_external, name);
    }

    public void setOtherICD10(String name) {
        setMap(picd10_other, name);
    }
    
    public void setVisitAge(String name) {
        setMap(visit_age, name);
    }
    
    public void setVisitAgeYear(String name) {
        setMap(visit_age_year, name);
    }
    
    public void setVisitAgeMonth(String name) {
        setMap(visit_age_month, name);
    }
    
    public void setVisitAgeDay(String name) {
        setMap(visit_age_day, name);
    }
    
    public void setPhoneNumber(String name) {
        setMap(patient_phone_number, name);
    }
    
    public void setMobilePhoneNumber(String name) {
        setMap(patient_mobile_phone_number, name);
    }
    
    public void setEmail(String name) {
        setMap(patient_email, name);
    }

    @Override
    public void setMap(String Param, String Data) {
        printSummaryReport2.put(Param, Data);
    }

    @Override
    public Map getData() {
        return printSummaryReport2;
    }
}

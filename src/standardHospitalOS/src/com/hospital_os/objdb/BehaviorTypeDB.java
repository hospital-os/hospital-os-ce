/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.BehaviorType;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tanakrit
 */
public class BehaviorTypeDB {

    public ConnectionInf theConnectionInf;

    public BehaviorTypeDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public List<BehaviorType> selectAll() throws Exception {
        String sql = "select * from f_behavior_type";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            return executeQuery(ePQuery);
        }
    }

    public BehaviorType selectByPK(String pkId) throws Exception {
        String sql = "select * from f_behavior_type \n"
                + " where f_behavior_type_id = ?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, pkId);
            List<BehaviorType> v = executeQuery(ePQuery);
            return (BehaviorType) (v.isEmpty() ? null : v.get(0));
        }
    }

    public List<BehaviorType> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<BehaviorType> list = new ArrayList<>();
        try (ResultSet rs = preparedStatement.executeQuery()) {
            while (rs.next()) {
                BehaviorType p = new BehaviorType();
                p.setObjectId(rs.getString("f_behavior_type_id"));
                p.description = rs.getString("description");
                p.behavior_score = rs.getInt("behavior_score");
                list.add(p);
            }
            return list;
        }
    }
}

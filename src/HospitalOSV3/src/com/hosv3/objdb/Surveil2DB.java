package com.hosv3.objdb;

import com.hospital_os.objdb.SurveilDB;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.util.Vector;

@SuppressWarnings("UseOfObsoleteCollectionType")
public class Surveil2DB extends SurveilDB {

    public Surveil2DB(ConnectionInf db) {
        super(db);
    }

    @Override
    public Vector selectByHnDate(String hn, String dateFrom, String dateTo) throws Exception {
        String sql;
        if (hn.trim().length() != 0) {
            sql = "select * from " + dbObj.table
                    + " where " + dbObj.hn + "='" + hn + "' and " + dbObj.illdate
                    + " >= '" + dateFrom + "' and " + dbObj.illdate
                    + " <= '" + dateTo + "' order by " + dbObj.illdate + " desc ";
        } else {
            sql = "select * from " + dbObj.table
                    + " where " + dbObj.illdate
                    + " >= '" + dateFrom + "' and " + dbObj.illdate
                    + " <= '" + dateTo + "' order by " + dbObj.illdate + " desc ";
        }

        Vector vc = eQuery(sql);

        if (vc.isEmpty()) {
            return null;
        } else {
            return vc;
        }
    }

    @Override
    public Vector selectByStatusDate(String status, String dateFrom, String dateTo) throws Exception {
        Vector vc = new Vector();
        String sql;
        if (status.equalsIgnoreCase("0") || status.equalsIgnoreCase("")) {
            sql = "select * from " + dbObj.table
                    + " where " + dbObj.illdate
                    + " >= '" + dateFrom + "' and " + dbObj.illdate
                    + " <= '" + dateTo + "' order by " + dbObj.illdate + " desc ";
        } else {
            sql = "select * from " + dbObj.table
                    + " where " + dbObj.patient_status + "='" + status + "' and " + dbObj.illdate
                    + " >= '" + dateFrom + "' and " + dbObj.illdate
                    + " <= '" + dateTo + "' order by " + dbObj.illdate + " desc ";
        }

        return eQuery(sql);
    }

    @Override
    public Vector selectAll() throws Exception {
        String sql = "select * from " + dbObj.table
                + " order by " + dbObj.illdate + " desc ";


        Vector vc = eQuery(sql);

        if (vc.isEmpty()) {
            return null;
        } else {
            return vc;
        }
    }
}

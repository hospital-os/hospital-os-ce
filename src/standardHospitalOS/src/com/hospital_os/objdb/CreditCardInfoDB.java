/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.CreditCardInfo;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class CreditCardInfoDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "837";

    public CreditCardInfoDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public int insert(CreditCardInfo obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO b_credit_card_info(\n"
                    + "            b_credit_card_info_id, b_bank_info_id, b_credit_card_type_id, pattern_number)\n"
                    + "    VALUES (?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getGenID(tableId));
            preparedStatement.setString(2, obj.b_bank_info_id);
            preparedStatement.setString(3, obj.b_credit_card_type_id);
            preparedStatement.setString(4, obj.pattern_number);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(CreditCardInfo obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE b_credit_card_info\n");
            sql.append("   SET b_bank_info_id=?, b_credit_card_type_id=?, \n");
            sql.append("       pattern_number=?\n");
            sql.append(" WHERE b_credit_card_info_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.b_bank_info_id);
            preparedStatement.setString(2, obj.b_credit_card_type_id);
            preparedStatement.setString(3, obj.pattern_number);
            preparedStatement.setString(6, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(CreditCardInfo obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM b_credit_card_info\n");
            sql.append(" WHERE b_credit_card_info_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public CreditCardInfo select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select b_credit_card_info.*, b_credit_card_type.description as type ,b_bank_info.description as bank from b_credit_card_info \n"
                    + "inner join b_credit_card_type on b_credit_card_type.b_credit_card_type_id = b_credit_card_info.b_credit_card_type_id \n"
                    + "inner join b_bank_info on b_bank_info.b_bank_info_id = b_credit_card_info.b_bank_info_id \n"
                    + "where b_credit_card_info_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<CreditCardInfo> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public CreditCardInfo selectByPatternNumber(String patternNumber) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select b_credit_card_info.*, b_credit_card_type.description as type ,b_bank_info.description as bank from b_credit_card_info \n"
                    + "inner join b_credit_card_type on b_credit_card_type.b_credit_card_type_id = b_credit_card_info.b_credit_card_type_id \n"
                    + "inner join b_bank_info on b_bank_info.b_bank_info_id = b_credit_card_info.b_bank_info_id \n"
                    + "where b_credit_card_info.pattern_number = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, patternNumber.length() > 6 ? patternNumber.substring(0, 6) : patternNumber);
            List<CreditCardInfo> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<CreditCardInfo> listByBankInfoId(String bakInfoId) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select b_credit_card_info.*, b_credit_card_type.description as type ,b_bank_info.description as bank from b_credit_card_info \n"
                    + "inner join b_credit_card_type on b_credit_card_type.b_credit_card_type_id = b_credit_card_info.b_credit_card_type_id \n"
                    + "inner join b_bank_info on b_bank_info.b_bank_info_id = b_credit_card_info.b_bank_info_id \n"
                    + "where b_credit_card_info.b_bank_info_id = ? order by b_credit_card_type.description";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, bakInfoId);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<CreditCardInfo> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<CreditCardInfo> list = new ArrayList<CreditCardInfo>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                CreditCardInfo obj = new CreditCardInfo();
                obj.setObjectId(rs.getString("b_credit_card_info_id"));
                obj.b_bank_info_id = rs.getString("b_bank_info_id");
                obj.b_credit_card_type_id = rs.getString("b_credit_card_type_id");
                obj.pattern_number = rs.getString("pattern_number");
                try {
                    obj.bankName = rs.getString("bank");
                } catch (Exception ex) {
                }
                try {
                    obj.creditCardType = rs.getString("type");
                } catch (Exception ex) {
                }
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }
}

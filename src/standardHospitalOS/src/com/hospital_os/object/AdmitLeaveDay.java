/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
public class AdmitLeaveDay extends Persistent{
    
    public String t_visit_id = "";
    public int leave_seq = 1;
    public String date_out = "";
    public String time_out = "";
    public String comeback = "0";
    public String date_in = "";
    public String time_in = "";
    public String leave_cause = "";
    public String doctor_approve = "";
    public String staff_record = "";
    public String staff_modify = "";
    public String record_datetime = "";
    public String modify_datetime = "";
    public String active = "1";

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getComeback() {
        return comeback;
    }

    public void setComeback(String comeback) {
        this.comeback = comeback;
    }

    public String getDate_in() {
        return date_in;
    }

    public void setDate_in(String date_in) {
        this.date_in = date_in;
    }

    public String getDate_out() {
        return date_out;
    }

    public void setDate_out(String date_out) {
        this.date_out = date_out;
    }

    public String getDoctor_approve() {
        return doctor_approve;
    }

    public void setDoctor_approve(String doctor_approve) {
        this.doctor_approve = doctor_approve;
    }

    public String getLeave_cause() {
        return leave_cause;
    }

    public void setLeave_cause(String leave_cause) {
        this.leave_cause = leave_cause;
    }

    public int getLeave_seq() {
        return leave_seq;
    }

    public void setLeave_seq(int leave_seq) {
        this.leave_seq = leave_seq;
    }

    public String getModify_datetime() {
        return modify_datetime;
    }

    public void setModify_datetime(String modify_datetime) {
        this.modify_datetime = modify_datetime;
    }

    public String getRecord_datetime() {
        return record_datetime;
    }

    public void setRecord_datetime(String record_datetime) {
        this.record_datetime = record_datetime;
    }

    public String getStaff_modify() {
        return staff_modify;
    }

    public void setStaff_modify(String staff_modify) {
        this.staff_modify = staff_modify;
    }

    public String getStaff_record() {
        return staff_record;
    }

    public void setStaff_record(String staff_record) {
        this.staff_record = staff_record;
    }

    public String getT_visit_id() {
        return t_visit_id;
    }

    public void setT_visit_id(String t_visit_id) {
        this.t_visit_id = t_visit_id;
    }

    public String getTime_in() {
        return time_in;
    }

    public void setTime_in(String time_in) {
        this.time_in = time_in;
    }

    public String getTime_out() {
        return time_out;
    }

    public void setTime_out(String time_out) {
        this.time_out = time_out;
    }
    
    
}

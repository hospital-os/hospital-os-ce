INSERT INTO f_egfr_formula VALUES ('5','Schwartz Equation สำหรับเด็ก (<18 ปี)', 'mL/min/1.73 m2');
update f_egfr_formula set formula_name = 'CKD-EPI สำหรับผู้ใหญ่' where f_egfr_formula_id = '3';

INSERT INTO s_egfr_version VALUES ('2', '2', 'eGFR Module', '1.2.5', '1.0.1', (select current_date) || ','|| (select current_time));

INSERT INTO s_script_update_log values ('eGFR_Module','update_egfr_002.sql',(select current_date) || ','|| (select current_time),'Update eGFR Module');

/*
 * DialogOrderHistory.java
 *
 * Created on 28 ����¹ 2547, 11:10 �.
 */
package com.hosv3.gui.dialog;

import com.hospital_os.object.*;
import com.hospital_os.object.specialQuery.SpecialQueryOrderHistoryDrug;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hospital_os.utility.CellRendererDoctorDx;
import com.hospital_os.utility.CellRendererHos;
import com.hospital_os.utility.CellRendererToolTipText;
import com.hospital_os.utility.TaBleModel;
import com.hosv3.control.*;
import com.hosv3.object.HosObject;
import com.hosv3.object.printobject.PrintFileName;
import com.hosv3.utility.Constant;
import com.hosv3.utility.DateUtil;
import com.hosv3.utility.GuiLang;
import com.hosv3.utility.TableRenderer;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Toolkit;
import java.util.Vector;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JasperPrint;

/**
 *
 * @author amp
 */
@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public class DialogHistoryOrder extends javax.swing.JFrame
        implements UpdateStatus {

    private static final long serialVersionUID = 1L;
    HosObject theHO;
    UpdateStatus theUS;
    public boolean actionCommand = false;
    /**
     * ��㹡�� query �����Ţͧ ��¡�� order
     */
    private final OrderControl theOrderControl;
    /**
     * ��㹡�� query �����Ţͧ ��¡�� combobox
     */
    LookupControl theLookupControl;
    private final HosControl theHC;
    /**
     * �� Object �ͧ patient
     */
    private Patient thePatient;
    /**
     * ��㹡�õ�Ǩ�ͺ��� Double-Click
     */
    public long firstClickTime = 0;
    private SpecialQueryOrderHistoryDrug theSpecialQueryOrderHistoryDrug;
    public SetupControl theSetupControl;
    public static boolean closeDialog = false;
    private final String[] col_jTableListOrder = {"�ѹ������",
        "�������ѭ",
        "�ӹǹ",
        "ʶҹ�",
        "Dose �������"};
    private final CellRendererHos dateRender = new CellRendererHos(CellRendererHos.DATE_TIME);
    private final CellRendererDoctorDx rendererDoctorDx = new CellRendererDoctorDx();
    private final CellRendererToolTipText rendererToolTipText = new CellRendererToolTipText(true);
    private Vector vOrder;
    private Vector vXray;
    private Vector vLab;
    private Vector vVitalSign;
    private DialogLISResult theDialogLISResult;
    private DialogEKGResult theDialogEKGResult;
    private DialogCumulativeLabResult theDialogCumulativeLabResult;
    private Vector visits;
    private HosDialog theHD;

    /**
     * Creates new form DialogOrderHistory
     */
    public DialogHistoryOrder(HosControl hc, UpdateStatus us, HosDialog hd) {
        setIconImage(us.getJFrame().getIconImage());
        theUS = us;
        theHC = hc;
        theHD = hd;
        theLookupControl = hc.theLookupControl;
        theOrderControl = hc.theOrderControl;
        theSetupControl = hc.theSetupControl;
        theHO = hc.theHO;
        initComponents();
        setDialog();
        jCheckBoxDrug.setSelected(theHO.scrollPaneDrug);
        jCheckBoxVitalSign.setSelected(theHO.scrollPaneVitalSign);
        jCheckBoxLab.setSelected(theHO.scrollPaneLab);
        jCheckBoxXray.setSelected(theHO.scrollPaneXray);
        setLanguage();
        // set visible button
        btnViewLISResult.setVisible(theLookupControl.readOption().enable_lis_result_viewer.equals("1")
                && theHC.theHO.theGActionAuthV.isReadPVitalViewLISResultFile());
        btnViewEKGResult.setVisible(theLookupControl.readOption().enable_ekg_result_viewer.equals("1")
                && theHC.theHO.theGActionAuthV.isReadPVitalViewEKGResultFile());
    }

    private void setLanguage() {
        GuiLang.setLanguage(this.jCheckBoxDrug);
        GuiLang.setLanguage(this.jCheckBoxLab);
        GuiLang.setLanguage(this.jCheckBoxVitalSign);
        GuiLang.setLanguage(this.jCheckBoxXray);
        GuiLang.setLanguage(this.jPanel1);
        GuiLang.setLanguage(this.jPanelSearch);
        GuiLang.setLanguage(this.jTableListVisit);
        GuiLang.setLanguage(this.jTableListLab);
        GuiLang.setLanguage(this.jTableListOrder);
        GuiLang.setLanguage(this.jTableListVitalSign);
        GuiLang.setLanguage(this.jTableListXray);
        GuiLang.setLanguage(this.col_jTableListOrder);
        GuiLang.setLanguage(this.jScrollPane6);
        GuiLang.setLanguage(this.jScrollPaneDrug);
        GuiLang.setLanguage(this.jScrollPaneLab);
        GuiLang.setLanguage(this.jScrollPaneVitalSign);
        GuiLang.setLanguage(this.jScrollPaneXray);
    }
    //////////////////////////////////////////////////////////////////////////

    /**
     * dialog �����㹡���觢�ͤ������͹�����
     */
    @Override
    public void setStatus(String str, int status) {
        str = Constant.getTextBundle(str);
        JOptionPane.showMessageDialog(this, str, Constant.getTextBundle("��͹"), JOptionPane.ERROR_MESSAGE);
    }
    //////////////////////////////////////////////////////////////////////////

    /**
     * dialog �����㹡���������ӡ���չ�ѹ��觵�ҧ
     */
    @Override
    public boolean confirmBox(String str, int status) {
        int i = JOptionPane.showConfirmDialog(this, str, Constant.getTextBundle("��͹"), JOptionPane.YES_NO_OPTION);
        return (i == JOptionPane.YES_OPTION);
    }

    @Override
    public JFrame getJFrame() {
        return this;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jSplitPane1 = new javax.swing.JSplitPane();
        jPanelSearch = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTableListVisit = new com.hosv3.gui.component.HJTableSort();
        jPanel2 = new javax.swing.JPanel();
        jCheckBoxVitalSign = new javax.swing.JCheckBox();
        jCheckBoxDrug = new javax.swing.JCheckBox();
        jCheckBoxLab = new javax.swing.JCheckBox();
        jCheckBoxXray = new javax.swing.JCheckBox();
        btnViewLISResult = new javax.swing.JButton();
        btnViewEKGResult = new javax.swing.JButton();
        tab = new javax.swing.JTabbedPane();
        hosOSJasperViewerPanel1 = new com.hosv3.gui.component.HosOSJasperViewerPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPaneDrug = new javax.swing.JScrollPane();
        jTableListOrder = new com.hosv3.gui.component.HJTableSort();
        jScrollPaneVitalSign = new javax.swing.JScrollPane();
        jTableListVitalSign = new com.hosv3.gui.component.HJTableSort();
        jScrollPaneXray = new javax.swing.JScrollPane();
        jTableListXray = new com.hosv3.gui.component.HJTableSort();
        panelLab = new javax.swing.JPanel();
        jScrollPaneLab = new javax.swing.JScrollPane();
        jTableListLab = new com.hosv3.gui.component.HJTableSort();
        btnOpenCumulativeLab = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(1200, 900));
        setSize(new java.awt.Dimension(1200, 900));
        getContentPane().setLayout(new java.awt.GridBagLayout());

        jSplitPane1.setDividerLocation(500);

        jPanelSearch.setLayout(new java.awt.GridBagLayout());

        jScrollPane6.setBorder(javax.swing.BorderFactory.createTitledBorder("����Ѻ��ԡ��"));
        jScrollPane6.setMinimumSize(new java.awt.Dimension(100, 22));
        jScrollPane6.setPreferredSize(new java.awt.Dimension(100, 80));

        jTableListVisit.setFillsViewportHeight(true);
        jTableListVisit.setFont(jTableListVisit.getFont());
        jTableListVisit.setRowHeight(30);
        jTableListVisit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTableListVisitMouseReleased(evt);
            }
        });
        jTableListVisit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTableListVisitKeyReleased(evt);
            }
        });
        jScrollPane6.setViewportView(jTableListVisit);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelSearch.add(jScrollPane6, gridBagConstraints);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("��������´����"));
        jPanel2.setLayout(new java.awt.GridBagLayout());

        jCheckBoxVitalSign.setFont(jCheckBoxVitalSign.getFont());
        jCheckBoxVitalSign.setText("�š���Ѵ VitalSign");
        jCheckBoxVitalSign.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxVitalSignActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 1);
        jPanel2.add(jCheckBoxVitalSign, gridBagConstraints);

        jCheckBoxDrug.setFont(jCheckBoxDrug.getFont());
        jCheckBoxDrug.setText("��õ�Ǩ�ѡ�ҷ������");
        jCheckBoxDrug.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxDrugActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 1);
        jPanel2.add(jCheckBoxDrug, gridBagConstraints);

        jCheckBoxLab.setFont(jCheckBoxLab.getFont());
        jCheckBoxLab.setText("�ŷҧ��ͧ��Ժѵԡ��");
        jCheckBoxLab.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxLabActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 1);
        jPanel2.add(jCheckBoxLab, gridBagConstraints);

        jCheckBoxXray.setFont(jCheckBoxXray.getFont());
        jCheckBoxXray.setText("��¡�õ�Ǩ��硫����");
        jCheckBoxXray.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxAllGroup3ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 1);
        jPanel2.add(jCheckBoxXray, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelSearch.add(jPanel2, gridBagConstraints);

        btnViewLISResult.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/folderlab@24.png"))); // NOI18N
        btnViewLISResult.setMaximumSize(new java.awt.Dimension(32, 32));
        btnViewLISResult.setMinimumSize(new java.awt.Dimension(32, 32));
        btnViewLISResult.setPreferredSize(new java.awt.Dimension(32, 32));
        btnViewLISResult.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewLISResultActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelSearch.add(btnViewLISResult, gridBagConstraints);

        btnViewEKGResult.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/folder-ekg@24.png"))); // NOI18N
        btnViewEKGResult.setMaximumSize(new java.awt.Dimension(32, 32));
        btnViewEKGResult.setMinimumSize(new java.awt.Dimension(32, 32));
        btnViewEKGResult.setPreferredSize(new java.awt.Dimension(32, 32));
        btnViewEKGResult.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewEKGResultActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelSearch.add(btnViewEKGResult, gridBagConstraints);

        jSplitPane1.setLeftComponent(jPanelSearch);

        tab.setFont(tab.getFont());
        tab.addTab("��¡�õ�Ǩ�ѡ�Ҽ�����", hosOSJasperViewerPanel1);

        jScrollPane1.setMinimumSize(new java.awt.Dimension(100, 22));
        jScrollPane1.setPreferredSize(new java.awt.Dimension(100, 80));

        jPanel1.setLayout(new java.awt.GridBagLayout());

        jScrollPaneDrug.setBorder(javax.swing.BorderFactory.createTitledBorder("��¡����"));
        jScrollPaneDrug.setMinimumSize(new java.awt.Dimension(100, 22));
        jScrollPaneDrug.setPreferredSize(new java.awt.Dimension(100, 80));

        jTableListOrder.setFillsViewportHeight(true);
        jTableListOrder.setFont(jTableListOrder.getFont());
        jTableListOrder.setRowHeight(30);
        jTableListOrder.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTableListOrderMouseReleased(evt);
            }
        });
        jTableListOrder.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTableListOrderKeyReleased(evt);
            }
        });
        jScrollPaneDrug.setViewportView(jTableListOrder);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel1.add(jScrollPaneDrug, gridBagConstraints);

        jScrollPaneVitalSign.setBorder(javax.swing.BorderFactory.createTitledBorder("��¡�õ�Ǩ��ҧ���"));
        jScrollPaneVitalSign.setMinimumSize(new java.awt.Dimension(100, 22));
        jScrollPaneVitalSign.setPreferredSize(new java.awt.Dimension(100, 80));

        jTableListVitalSign.setFillsViewportHeight(true);
        jTableListVitalSign.setFont(jTableListVitalSign.getFont());
        jTableListVitalSign.setRowHeight(30);
        jTableListVitalSign.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTableListVitalSignMouseReleased(evt);
            }
        });
        jTableListVitalSign.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTableListVitalSignKeyReleased(evt);
            }
        });
        jScrollPaneVitalSign.setViewportView(jTableListVitalSign);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel1.add(jScrollPaneVitalSign, gridBagConstraints);

        jScrollPaneXray.setBorder(javax.swing.BorderFactory.createTitledBorder("��õ�Ǩ��硫����"));
        jScrollPaneXray.setMinimumSize(new java.awt.Dimension(100, 22));
        jScrollPaneXray.setPreferredSize(new java.awt.Dimension(100, 80));

        jTableListXray.setFillsViewportHeight(true);
        jTableListXray.setFont(jTableListXray.getFont());
        jTableListXray.setRowHeight(30);
        jTableListXray.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTableListXrayMouseReleased(evt);
            }
        });
        jTableListXray.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTableListXrayKeyReleased(evt);
            }
        });
        jScrollPaneXray.setViewportView(jTableListXray);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel1.add(jScrollPaneXray, gridBagConstraints);

        panelLab.setLayout(new java.awt.GridBagLayout());

        jScrollPaneLab.setBorder(javax.swing.BorderFactory.createTitledBorder("�š�õ�Ǩ���"));
        jScrollPaneLab.setMinimumSize(new java.awt.Dimension(100, 22));
        jScrollPaneLab.setPreferredSize(new java.awt.Dimension(100, 80));

        jTableListLab.setFillsViewportHeight(true);
        jTableListLab.setFont(jTableListLab.getFont());
        jTableListLab.setRowHeight(30);
        jTableListLab.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTableListLabMouseReleased(evt);
            }
        });
        jTableListLab.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTableListLabKeyReleased(evt);
            }
        });
        jScrollPaneLab.setViewportView(jTableListLab);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        panelLab.add(jScrollPaneLab, gridBagConstraints);

        btnOpenCumulativeLab.setText("Cumulative Lab Result");
        btnOpenCumulativeLab.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOpenCumulativeLabActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelLab.add(btnOpenCumulativeLab, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel1.add(panelLab, gridBagConstraints);

        jScrollPane1.setViewportView(jPanel1);

        tab.addTab("��������´����", jScrollPane1);

        jSplitPane1.setRightComponent(tab);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        getContentPane().add(jSplitPane1, gridBagConstraints);

        setSize(new java.awt.Dimension(1145, 561));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jTableListOrderMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableListOrderMouseReleased
        //showResult();
    }//GEN-LAST:event_jTableListOrderMouseReleased

    private void jTableListOrderKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTableListOrderKeyReleased
    }//GEN-LAST:event_jTableListOrderKeyReleased

    private void jTableListVisitMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableListVisitMouseReleased
        this.doUpdateVisitSlip();
    }//GEN-LAST:event_jTableListVisitMouseReleased

    private void jTableListVisitKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTableListVisitKeyReleased
        this.doUpdateVisitSlip();
    }//GEN-LAST:event_jTableListVisitKeyReleased

    private void jTableListVitalSignMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableListVitalSignMouseReleased
    }//GEN-LAST:event_jTableListVitalSignMouseReleased

    private void jTableListVitalSignKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTableListVitalSignKeyReleased
    }//GEN-LAST:event_jTableListVitalSignKeyReleased

    private void jTableListXrayMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableListXrayMouseReleased
        int row = jTableListXray.getSelectedRow();
        if (row >= 0 && evt.getClickCount() == 2) {
            String[] str = (String[]) vXray.get(row);
            this.doubleClickListXray(str[str.length - 1]);
        }
    }//GEN-LAST:event_jTableListXrayMouseReleased

    private void jTableListXrayKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTableListXrayKeyReleased
    }//GEN-LAST:event_jTableListXrayKeyReleased

    private void jTableListLabMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableListLabMouseReleased
    }//GEN-LAST:event_jTableListLabMouseReleased

    private void jTableListLabKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTableListLabKeyReleased
    }//GEN-LAST:event_jTableListLabKeyReleased

    private void jCheckBoxAllGroup3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxAllGroup3ActionPerformed
        setShowCheckBox();
    }//GEN-LAST:event_jCheckBoxAllGroup3ActionPerformed

    private void jCheckBoxLabActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxLabActionPerformed
        setShowCheckBox();
    }//GEN-LAST:event_jCheckBoxLabActionPerformed

    private void jCheckBoxDrugActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxDrugActionPerformed
        setShowCheckBox();
    }//GEN-LAST:event_jCheckBoxDrugActionPerformed

    private void jCheckBoxVitalSignActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxVitalSignActionPerformed
        setShowCheckBox();
    }//GEN-LAST:event_jCheckBoxVitalSignActionPerformed

	private void comboBoxClinic1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxClinic1ActionPerformed
    }//GEN-LAST:event_comboBoxClinic1ActionPerformed

    private void btnViewLISResultActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewLISResultActionPerformed
        if (theDialogLISResult == null) {
            theDialogLISResult = new DialogLISResult(null, true);
            theDialogLISResult.setControl(theHC);
            theDialogLISResult.setSize(800, 450);
        }
        theDialogLISResult.openDialog();
    }//GEN-LAST:event_btnViewLISResultActionPerformed

    private void btnViewEKGResultActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewEKGResultActionPerformed
        if (theDialogEKGResult == null) {
            theDialogEKGResult = new DialogEKGResult(null, true);
            theDialogEKGResult.setControl(theHC);
            theDialogEKGResult.setSize(800, 450);
        }
        theDialogEKGResult.openDialog();
    }//GEN-LAST:event_btnViewEKGResultActionPerformed

    private void btnOpenCumulativeLabActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOpenCumulativeLabActionPerformed
        if (theDialogCumulativeLabResult == null) {
            theDialogCumulativeLabResult = new DialogCumulativeLabResult(null, true);
            theDialogCumulativeLabResult.setControl(theHC);
        }
        theDialogCumulativeLabResult.openDialog(thePatient);
    }//GEN-LAST:event_btnOpenCumulativeLabActionPerformed

    public boolean showDialog(Patient p) {
        if (p == null) {
            theUS.setStatus("��س����͡������", UpdateStatus.WARNING);
            return false;
        }
        if (!p.getObjectId().equals(theHO.opatient)) {
            theHO.scrollPaneDrug = false;
            theHO.scrollPaneVitalSign = false;
            theHO.scrollPaneLab = false;
            theHO.scrollPaneXray = false;
            jCheckBoxDrug.setSelected(theHO.scrollPaneDrug);
            jCheckBoxVitalSign.setSelected(theHO.scrollPaneVitalSign);
            jCheckBoxLab.setSelected(theHO.scrollPaneLab);
            jCheckBoxXray.setSelected(theHO.scrollPaneXray);
            theHO.opatient = p.getObjectId();
        }
        thePatient = p;
        setPatient(p);
        setLanguage();
        setVisible(true);
        return closeDialog;
    }

    /**
     * set table OrderHistory �����ҡ��ä��ҵ�� case ��ҧ�
     *
     * @deprecated �ѧ�ѹ�ա�� ��������¤������������ special query
     * ��§�������Ǿ�
     */
    private void setOrderItemV(Vector vOrderHistory) {
        TaBleModel tm;
        if (vOrderHistory != null) {
            tm = new TaBleModel(col_jTableListOrder, vOrderHistory.size());
            for (int i = 0; i < vOrderHistory.size(); i++) {
                //������ ������繤���իѺ��͹ special_query �ա���
                OrderItem oi = (OrderItem) vOrderHistory.get(i);
                tm.setValueAt(DateUtil.getDateFromText(oi.vertify_time), i, 0);
                tm.setValueAt(oi.common_name, i, 1);
                tm.setValueAt(oi.qty, i, 2);
                OrderItemStatus ois = theLookupControl.readOrderItemStatus(oi.status);
                tm.setValueAt(ois.description, i, 3);

                if (oi.isDrug()) {
                    theSpecialQueryOrderHistoryDrug = theOrderControl.selectOrderItemDrugByOrderItemId(oi.getObjectId());
                }

                String dose_all = "";
                if (theSpecialQueryOrderHistoryDrug != null) {
                    if ((theSpecialQueryOrderHistoryDrug.special).equals("0")) {
                        dose_all = theSpecialQueryOrderHistoryDrug.instruction
                                + " " + theSpecialQueryOrderHistoryDrug.dose
                                + " " + theSpecialQueryOrderHistoryDrug.use_uom
                                + " " + theSpecialQueryOrderHistoryDrug.frequency
                                + " " + oi.qty
                                + " " + theSpecialQueryOrderHistoryDrug.purch_uom;
                    } else {
                        dose_all = theSpecialQueryOrderHistoryDrug.special_text + " " + oi.qty + " " + theSpecialQueryOrderHistoryDrug.purch_uom;
                    }
                }
                tm.setValueAt(dose_all, i, 4);
            }
        } else {
            tm = new TaBleModel(col_jTableListOrder, 0);
        }
        jTableListOrder.setModel(tm);
        jTableListOrder.getColumnModel().getColumn(0).setCellRenderer(dateRender);
        jTableListOrder.getColumnModel().getColumn(0).setPreferredWidth(120);
        jTableListOrder.getColumnModel().getColumn(1).setPreferredWidth(150);
        jTableListOrder.getColumnModel().getColumn(2).setPreferredWidth(30);
        jTableListOrder.getColumnModel().getColumn(2).setCellRenderer(TableRenderer.getRendererRight());
        jTableListOrder.getColumnModel().getColumn(3).setPreferredWidth(50);
        jTableListOrder.getColumnModel().getColumn(4).setPreferredWidth(150);
    }

    /**
     * �� show �Ţͧ Lab ���� XRay �����§ҹ�����º�������� ���������:
     * category_group,status �ͧ���ҧ OrderItem �������͡: �ŷ����ҡ�����§ҹ
     */
    private void setDialog() {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int width = screenSize.width >= 960 ? 960 : screenSize.width;
        int height = screenSize.height >= 600 ? 600 : screenSize.height;
        setSize(width, height);
        setTitle(Constant.getTextBundle("����ѵԡ����觵�Ǩ�ѡ��"));
        setLocationRelativeTo(null);
        setExtendedState(Frame.MAXIMIZED_BOTH);
    }

    public void setPatient(Patient pt) {
        this.vLab = null;
        this.vOrder = null;
        this.vVitalSign = null;
        this.vXray = null;
        this.thePatient = pt;
        listTableData();
        setShowCheckBox();
    }

    private void setShowCheckBox() {
        tab.setSelectedIndex(1);
        this.jScrollPaneDrug.setVisible(false);
        this.jScrollPaneVitalSign.setVisible(false);
        this.panelLab.setVisible(false);
        this.jScrollPaneXray.setVisible(false);
        if (this.jCheckBoxDrug.isSelected()) {
            if (vOrder == null) {
                vOrder = theOrderControl.listOrderHistoryByGroupPatientId(
                        CategoryGroup.isDrug(), thePatient.getObjectId());
                setOrderItemV(vOrder);
            }
            this.jScrollPaneDrug.setVisible(true);
            theHO.scrollPaneDrug = true;
        } else {
            theHO.scrollPaneDrug = false;
        }
        if (this.jCheckBoxVitalSign.isSelected()) {
            if (vVitalSign == null) {
                vVitalSign = theOrderControl.listEVitalSign(thePatient.getObjectId());
                setVitalSignV(vVitalSign);
            }
            this.jScrollPaneVitalSign.setVisible(true);
            theHO.scrollPaneVitalSign = true;
        } else {
            theHO.scrollPaneVitalSign = false;
        }
        if (this.jCheckBoxLab.isSelected()) {
            if (vLab == null) {
                vLab = theOrderControl.listELab(thePatient.getObjectId());
                setLabV(vLab);
            }
            this.panelLab.setVisible(true);
            theHO.scrollPaneLab = true;
        } else {
            theHO.scrollPaneLab = false;
        }
        if (this.jCheckBoxXray.isSelected()) {
            if (vXray == null) {
                vXray = theOrderControl.listEXray(thePatient.getObjectId());
                setXrayV(vXray);
            }
            this.jScrollPaneXray.setVisible(true);
            theHO.scrollPaneXray = true;
        } else {
            theHO.scrollPaneXray = false;
        }
        this.jPanel1.updateUI();
    }

    private void setVisitV(Vector vVisit) {
        this.visits = vVisit;
        if (vVisit == null) {
            TaBleModel tm = new TaBleModel(null, 0);
            jTableListVisit.setModel(tm);
            return;
        }
        String[] col = {"�ѹ���-����", "type", "Dx", "Dx Note"};
        GuiLang.setLanguage(col);
        TaBleModel tm = new TaBleModel(col, vVisit.size());
        for (int i = vVisit.size() - 1, j = 0; i >= 0; i--, j++) {
            Visit str = (Visit) vVisit.get(i);
            tm.setValueAt(DateUtil.getDateFromText(str.begin_visit_time), j, 0);
            if (str.visit_type.equals("1")) {
                tm.setValueAt("�", j, 1);
            } else {
                tm.setValueAt("�͡", j, 1);
            }
            tm.setValueAt(str.doctor_dx, j, 2);
            tm.setValueAt(str.diagnosis_note, j, 3);
        }
        jTableListVisit.setModel(tm);
        jTableListVisit.getColumnModel().getColumn(0).setCellRenderer(dateRender);
        jTableListVisit.getColumnModel().getColumn(0).setPreferredWidth(1500);
        jTableListVisit.getColumnModel().getColumn(1).setCellRenderer(rendererToolTipText);
        jTableListVisit.getColumnModel().getColumn(1).setPreferredWidth(550);
        jTableListVisit.getColumnModel().getColumn(2).setCellRenderer(rendererDoctorDx);
        jTableListVisit.getColumnModel().getColumn(2).setPreferredWidth(1500);
        jTableListVisit.getColumnModel().getColumn(3).setCellRenderer(rendererDoctorDx);
        jTableListVisit.getColumnModel().getColumn(3).setPreferredWidth(1500);
    }

    private void setVitalSignV(Vector vVitalSign) {
        String[] col = {"�ѹ���-����", "���˹ѡ", "��ǹ�٧", "BMI", "�س�����", "�����ѹ", "�վ��", "�������", "�ͺ���", "�ͺ��⾡"};
        GuiLang.setLanguage(col);
        TaBleModel tm = new TaBleModel(col, vVitalSign.size());
        for (int i = 0; i < vVitalSign.size(); i++) {
            String[] str = (String[]) vVitalSign.get(i);
            for (int j = 0; j < str.length; j++) {
                if (j == 0) {
                    tm.setValueAt(DateUtil.getDateFromText(str[j]), i, j);
                } else {
                    tm.setValueAt(str[j], i, j);
                }
            }
        }
        this.jTableListVitalSign.setModel(tm);
        jTableListVitalSign.getColumnModel().getColumn(0).setPreferredWidth(220);
        jTableListVitalSign.getColumnModel().getColumn(0).setCellRenderer(dateRender);
        for (int i = 1; i < 10; i++) {
            jTableListVitalSign.getColumnModel().getColumn(i).setCellRenderer(TableRenderer.getRendererRight());
        }
    }

    private void setLabV(Vector vLab) {
        String[] col = {"�ѹ���-����", "�����", "����", "��", "��һ���"};
        GuiLang.setLanguage(col);
        TaBleModel tm = new TaBleModel(col, vLab.size());
        String old_group = null;
        for (int i = 0; i < vLab.size(); i++)//row
        {
            String[] str = (String[]) vLab.get(i);
            for (int j = 0; j < str.length; j++)//column
            {
                switch (j) {
                    case 0:
                        tm.setValueAt(DateUtil.getDateFromText(str[j]), i, j);
                        break;
                    case 1:
                        if (!str[j].equals(old_group)) {
                            tm.setValueAt(str[j], i, j);
                        } else {
                            tm.setValueAt("", i, j);
                        }
                        old_group = str[j];
                        break;
                    default:
                        tm.setValueAt(str[j], i, j);
                        break;
                }
            }
        }
        this.jTableListLab.setModel(tm);
        jTableListLab.getColumnModel().getColumn(0).setCellRenderer(dateRender);
        jTableListLab.getColumnModel().getColumn(0).setPreferredWidth(220);
        jTableListLab.getColumnModel().getColumn(1).setPreferredWidth(150);
        jTableListLab.getColumnModel().getColumn(2).setPreferredWidth(150);
        jTableListLab.getColumnModel().getColumn(3).setPreferredWidth(200);
        jTableListLab.getColumnModel().getColumn(3).setCellRenderer(TableRenderer.getRendererRight());
        jTableListLab.getColumnModel().getColumn(4).setPreferredWidth(200);
        jTableListLab.getColumnModel().getColumn(4).setCellRenderer(TableRenderer.getRendererRight());
    }

    private void setXrayV(Vector vXray) {
        String[] col = {"�ѹ���-����", "��¡��", "�š�õ�Ǩ"};
        GuiLang.setLanguage(col);
        TaBleModel tm = new TaBleModel(col, vXray.size());
        for (int i = 0; i < vXray.size(); i++) {
            String[] str = (String[]) vXray.get(i);
            for (int j = 0; j < str.length; j++) {
                if (j == str.length - 1) {
                    break;
                } else if (j == 0) {
                    tm.setValueAt(DateUtil.getDateFromText(str[j]), i, j);
                } else {
                    tm.setValueAt(str[j], i, j);
                }
            }
        }
        this.jTableListXray.setModel(tm);
        jTableListXray.getColumnModel().getColumn(0).setCellRenderer(dateRender);
        jTableListXray.getColumnModel().getColumn(0).setPreferredWidth(550);
        jTableListXray.getColumnModel().getColumn(1).setPreferredWidth(1500);
        jTableListXray.getColumnModel().getColumn(2).setPreferredWidth(1500);
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnOpenCumulativeLab;
    private javax.swing.JButton btnViewEKGResult;
    private javax.swing.JButton btnViewLISResult;
    private com.hosv3.gui.component.HosOSJasperViewerPanel hosOSJasperViewerPanel1;
    private javax.swing.JCheckBox jCheckBoxDrug;
    private javax.swing.JCheckBox jCheckBoxLab;
    private javax.swing.JCheckBox jCheckBoxVitalSign;
    private javax.swing.JCheckBox jCheckBoxXray;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanelSearch;
    protected javax.swing.JScrollPane jScrollPane1;
    protected javax.swing.JScrollPane jScrollPane6;
    protected javax.swing.JScrollPane jScrollPaneDrug;
    protected javax.swing.JScrollPane jScrollPaneLab;
    protected javax.swing.JScrollPane jScrollPaneVitalSign;
    protected javax.swing.JScrollPane jScrollPaneXray;
    private javax.swing.JSplitPane jSplitPane1;
    protected com.hosv3.gui.component.HJTableSort jTableListLab;
    protected com.hosv3.gui.component.HJTableSort jTableListOrder;
    protected com.hosv3.gui.component.HJTableSort jTableListVisit;
    protected com.hosv3.gui.component.HJTableSort jTableListVitalSign;
    protected com.hosv3.gui.component.HJTableSort jTableListXray;
    private javax.swing.JPanel panelLab;
    private javax.swing.JTabbedPane tab;
    // End of variables declaration//GEN-END:variables

    private Visit getSelectedVisit() {
        if (jTableListVisit.getSelectedRow() < 0) {
            return null;
        }
        Visit visit = (Visit) visits.get((visits.size() - 1) - jTableListVisit.getSelectedRow());
        return visit;
    }

    private void doUpdateVisitSlip() {
        tab.setSelectedIndex(0);
        try {
            Visit visit = getSelectedVisit();
            visit = theHC.theVisitControl.readVisitByVidRet(visit.getObjectId());
            JasperPrint jasperPrint = theHC.thePrintControl.getJasperPrint(PrintFileName.getFileName(8),
                    theHC.thePrintControl.getParameterPrintVisitSlipNew(visit),
                    theHC.thePrintControl.getDataSourcePrintVisitSlipNew(visit));
            hosOSJasperViewerPanel1.setJasperPrint(jasperPrint);
        } catch (Exception ex) {
            hosOSJasperViewerPanel1.setJasperPrint(null);
            Logger.getLogger(DialogHistoryOrder.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }

    private void listTableData() {
        Vector<Visit> visits = new Vector<>();
        for (int i = 0; i < theHO.vVisit.size(); i++) {
            Visit visit = (Visit) theHO.vVisit.get(i);
            if (!visit.isDropVisit()) {
                visits.add((Visit) theHO.vVisit.get(i));
            }
        }
        setVisitV(visits);
    }

    private void doubleClickListXray(String orderId) {
        OrderItem oi = theHC.theOrderControl.readOrderItemById(orderId);
        if (oi.isXray()
                & (oi.status.equals(OrderStatus.REPORT)
                || oi.status.equals(OrderStatus.EXECUTE))) {

            theHD.showDialogResultXray(oi);
        }
    }
}

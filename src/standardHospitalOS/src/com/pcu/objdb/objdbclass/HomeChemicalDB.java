/*
 * HomeChemicalDB.java
 *
 * Created on 13 �Զع�¹ 2548, 15:17 �.
 */
package com.pcu.objdb.objdbclass;

import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.ComboFix;
import com.pcu.object.HomeChemical;
import java.sql.ResultSet;
import java.util.Vector;

/**
 *
 * @author Administrator
 */
@SuppressWarnings({"ClassWithoutLogger", "UseOfObsoleteCollectionType"})
public class HomeChemicalDB {

    /**
     * Creates a new instance of HomeChemicalDB
     */
    public HomeChemicalDB() {
    }
    public ConnectionInf theConnectionInf;
    public HomeChemical dbObj;

    public HomeChemicalDB(ConnectionInf db) {
        theConnectionInf = db;
        dbObj = new HomeChemical();
        initConfig();
    }

    public boolean initConfig() {
        dbObj.table = "f_health_home_chemical";
        dbObj.pk_field = "f_health_home_chemical_id";
        dbObj.description = "health_home_chemical_description";

        return true;
    }

    public Vector selectHomeChemical() throws Exception {
        Vector vHomeChemical = new Vector();

        String sql = "select * from "
                + dbObj.table;

        vHomeChemical = veQuery(sql);

        if (vHomeChemical.size() == 0) {
            return null;
        } else {
            return vHomeChemical;
        }
    }

    public Vector eQuery(String sql) throws Exception {
        HomeChemical p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            p = new HomeChemical();
            p.setObjectId(rs.getString(dbObj.pk_field));
            p.description = rs.getString(dbObj.description);
            list.add(p);
        }
        rs.close();
        return list;
    }

    public Vector veQuery(String sql) throws Exception {
        ComboFix p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            p = new ComboFix();
            p.code = rs.getString(dbObj.pk_field);
            p.name = rs.getString(dbObj.description);
            list.add(p);
        }
        rs.close();
        return list;
    }
}

insert into f_gui_action (f_gui_action_id, gui_action_name) VALUES ('5a05', 'Modality');

CREATE TABLE b_modality
(
  b_modality_id character varying(30) NOT NULL,
  modality_code text NOT NULL,
  modality_description text,
  active character varying(1) NOT NULL DEFAULT '1'::character varying,
  record_date_time    TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
  user_record_id      CHARACTER VARYING(30) NOT NULL,
  update_date_time    TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
  user_update_id      CHARACTER VARYING(30) NOT NULL,
  CONSTRAINT b_modality_pkey PRIMARY KEY (b_modality_id)
);

insert into b_modality (b_modality_id, modality_code, user_record_id, user_update_id)
values ('863000000', 'BM', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('863000001', 'CR', 'Computed Radiography', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('863000002', 'CT', 'Computed Tomography', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('863000003', 'MR', 'Magnetic Resonance', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('863000004', 'NM', 'Nuclear Medicine', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('863000005', 'US', 'Ultrasound', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('863000006', 'OT', 'Other', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('863000007', 'BI', 'Biomagnetic Imaging', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('863000008', 'CD', 'Color Flow Doppler', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('863000009', 'DD', 'Duplex Doppler', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('8630000010', 'DG', 'Diaphanography', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('8630000011', 'ES', 'Endoscopy', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('8630000012', 'LS', 'Laser Surface Scan', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('8630000013', 'PT', 'Positron Emission Tomography', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('8630000014', 'RG', 'Radiographic Imaging', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('8630000015', 'ST', 'Single-photon Emission Computed Tomography (SPECT)', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('8630000016', 'TG', 'Thermography', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('8630000017', 'XA', 'X-Ray Angiography', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('8630000018', 'RF', 'Radio Fluoroscopy', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('8630000019', 'RTIMAGE', 'Radiotherapy Image', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('8630000020', 'RTDOSE', 'Radiotherapy Dose', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('8630000021', 'RTSTRUCT', 'RadioTherapy Structure Set', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('8630000022', 'RTPLAN', 'Radiotherapy Plan', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('8630000023', 'RTRECORD', 'Radiotherapy Treatment Record', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('8630000024', 'HC', 'Hard Copy', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('8630000025', 'DX', 'Digital Radiography', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('8630000026', 'MG', 'Mammography', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('8630000027', 'IO', 'Intra-oral Radiography', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('8630000028', 'PX', 'Panoramic X-Ray', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('8630000029', 'GM', 'General Microscopy', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('8630000030', 'SM', 'Slide Microscopy', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('8630000031', 'XC', 'External-camera Photography', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('8630000032', 'PR', 'Presentation State', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('8630000033', 'AU', 'Audio', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('8630000034', 'ECG', 'Electrocardiography', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('8630000035', 'EPS', 'Cardiac Electrophysiology', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('8630000036', 'HD', 'Hemodynamic Waveform', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('8630000037', 'SR', 'Structured Report Document', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('8630000038', 'IVUS', 'Intravascular Ultrasound', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('8630000039', 'OP', 'Ophthalmic Photography', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('8630000040', 'SMR', 'Stereometric Relationship', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('8630000041', 'SC', 'Secondary Capture', 'system', 'system');

insert into b_modality (b_modality_id, modality_code, modality_description, user_record_id, user_update_id)
values ('8630000042', 'SD', 'Scanned Document', 'system', 'system');

CREATE TABLE b_item_xray
(
  b_item_xray_id character varying(30) NOT NULL,
  b_item_id character varying(30) NOT NULL,
  b_modality_id character varying(30),
  record_date_time    TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
  user_record_id      CHARACTER VARYING(30) NOT NULL,
  update_date_time    TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
  user_update_id      CHARACTER VARYING(30) NOT NULL,
  CONSTRAINT b_item_xray_pkey PRIMARY KEY (b_item_xray_id),
  CONSTRAINT b_item_xray_b_item_id_b_modality_id_key UNIQUE (b_item_id, b_modality_id)
); 

insert into b_item_xray (b_item_xray_id, b_item_id, user_record_id, user_update_id, b_modality_id) 
select '863' || b_item.b_item_id, b_item.b_item_id, '1578299109445', '1578299109445', '' from b_item where b_item_subgroup_id 
in (select b_item_subgroup_id from b_item_subgroup where b_item_subgroup.f_item_group_id = '3');

CREATE TABLE t_order_xray
(
  t_order_xray_id character varying(30) NOT NULL,
  t_visit_id character varying(30) NOT NULL,
  t_order_id character varying(30),
  b_modality_id character varying(30) NOT NULL,
  accession_number text NOT NULL,
  priority character varying(1) NOT NULL DEFAULT 'O'::character varying, -- E for Emergency, I for IPD, O for OPD
  order_status character varying(1) NOT NULL DEFAULT '1'::character varying, -- 1 for new, 0 for cancel
  pacs_status character varying(1) NOT NULL DEFAULT '0'::character varying, -- 0 for unsend, 1 for sent  
  doctor_id character varying(30),
  order_datetime timestamp without time zone NOT NULL,
  executor_id character varying(30) NOT NULL,
  execute_datetime timestamp without time zone NOT NULL,
  cancel_id character varying(30),
  cancel_datetime timestamp without time zone,
  record_datetime timestamp without time zone NOT NULL DEFAULT now(),
  dicom_status character varying(1) NOT NULL DEFAULT '0'::character varying, -- 0 for no dicom, 1 for otherwise 
  dicom_text text,
  CONSTRAINT t_order_xray_pkey PRIMARY KEY (t_order_xray_id),
  CONSTRAINT t_order_xray_accession_number_key UNIQUE (accession_number),
  CONSTRAINT t_order_xray_t_order_id_key UNIQUE (t_order_id)
);

ALTER TABLE t_result_xray ADD COLUMN  pacs_status character varying(1) NOT NULL DEFAULT '0'::character varying; -- 0 for unsend, 1 for sent  
ALTER TABLE t_result_xray ADD COLUMN  pacs_datetime timestamp without time zone;
UPDATE t_result_xray SET pacs_status = '1';
ALTER TABLE t_result_xray ADD COLUMN  result_xray_complete_datetime timestamp without time zone;
CREATE OR REPLACE FUNCTION update_result_xray_complete_datetime() RETURNS trigger AS
$BODY$
      BEGIN
            UPDATE  t_result_xray
            SET result_xray_complete_datetime = current_timestamp
            WHERE t_result_xray.t_result_xray_id = NEW.t_result_xray_id;
        RETURN NULL;
      END;
$BODY$
LANGUAGE 'plpgsql';

--Create trigger update_result_xray_complete
CREATE TRIGGER update_result_xray_complete
AFTER UPDATE ON t_result_xray
FOR EACH ROW
WHEN (OLD.result_xray_complete = '0' and NEW.result_xray_complete = '1')
EXECUTE PROCEDURE update_result_xray_complete_datetime();


-- update db version
INSERT INTO s_version VALUES ('9701000000075', '75', 'Hospital OS, Community Edition', '3.9.42', '3.26.050815', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_42.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.42');
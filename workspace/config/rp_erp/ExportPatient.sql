select 
--value
--p.patient_hn as hn  
case when p.patient_phone_number is null or p.patient_phone_number=''
    then p.patient_patient_mobile_phone
else p.patient_phone_number
end as hn
--name
,p.patient_firstname || ' ' || p.patient_lastname as patient_name
--groupvalue
,'Standard' as groupvalue
--address1
, p.patient_house ||
case when p.patient_moo is not null and p.patient_moo <> ''
then ' �.' || p.patient_moo
else ''
end 
||
case when p.patient_road is not null and p.patient_road <> ''
then ' �.' || p.patient_road
else ''
end 
||
case when ft.address_description is not null and ft.address_description <> ''
then  ' �.' || ft.address_description
else ''
end 
||
case when fa.address_description is not null and fa.address_description <> ''
then  ' �.' || fa.address_description
else ''
end as address1
--city
,fc.address_description as city
--countrycode
,'TH' as countrycode
--title
,pp.patient_prefix_description as name_prefix
--contactname
,p.patient_firstname || ' ' || p.patient_lastname as patient_name
--phone
,case when p.patient_phone_number is null or p.patient_phone_number=''
    then p.patient_patient_mobile_phone
else p.patient_phone_number
end as phone
--birthday
,
substring(p.patient_birthday,6,2) ||
substring(p.patient_birthday,9,2) ||
--(cast(substring(p.patient_birthday,1,4) as numeric)-543) as birthday
substring(p.patient_birthday,1,4) as birthday
--bpcontactgreeting
,pp.patient_prefix_description as name_prefix
from 
t_patient p
inner join t_health_family hf on p.patient_firstname = hf.patient_name
inner join f_patient_prefix pp on (p.f_patient_prefix_id = pp.f_patient_prefix_id and pp.active='1')
left join f_address ft on p.patient_tambon = ft.f_address_id
left join f_address fa on p.patient_amphur = fa.f_address_id
left join f_address fc on p.patient_changwat = fc.f_address_id
where 
p.patient_active = '1'
and
hf.health_family_active = '1'
and
(
substring(p.patient_record_date_time,1,10) = ?
or
substring(p.patient_update_date_time,1,10) = ?
)

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class PaymentType extends Persistent implements CommonInf {

    public String description = "";
    public String require_bank_info = "0";
    public String require_account_name = "0";
    public String require_account_no = "0";
    public String require_card_type = "0";
    public String require_payment_date = "0";
    public String lbl_bank_info = "";
    public String lbl_account_name = "";
    public String lbl_account_no = "";
    public String lbl_card_type = "";
    public String lbl_payment_date = "";
    public int limit_account_number = 0;
    public String is_auto_check_bank = "0";

    @Override
    public String getCode() {
        return getObjectId();
    }

    @Override
    public String getName() {
        return description;
    }

    @Override
    public String toString() {
        return description;
    }
}

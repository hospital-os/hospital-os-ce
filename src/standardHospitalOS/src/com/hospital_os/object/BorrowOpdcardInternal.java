/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;
import java.util.Date;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class BorrowOpdcardInternal extends Persistent {

    public String t_patient_id = "";
    public String t_visit_id = "";
    public String status = "0";
    public Date takeout_datetime = null;
    public String user_takeout_id = "";
    public Date return_datetime = null;
    public String user_return_id = "";
    // in object only
    public String hn = "";
    public String name = "";
    public String lastestServicePoint = "";
}

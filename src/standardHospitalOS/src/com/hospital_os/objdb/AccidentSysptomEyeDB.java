/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.AccidentSysptomEye;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class AccidentSysptomEyeDB {

    private final ConnectionInf connectionInf;

    public AccidentSysptomEyeDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public AccidentSysptomEye select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_accident_symptom_eye where f_accident_symptom_eye_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<AccidentSysptomEye> executeQuery = executeQuery(preparedStatement);
            return executeQuery.isEmpty() ? null : executeQuery.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<AccidentSysptomEye> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_accident_symptom_eye";
            preparedStatement = connectionInf.ePQuery(sql);

            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<AccidentSysptomEye> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<AccidentSysptomEye> list = new ArrayList<AccidentSysptomEye>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                AccidentSysptomEye obj = new AccidentSysptomEye();
                obj.setObjectId(rs.getString("f_accident_symptom_eye_id"));
                obj.description = rs.getString("accident_symptom_eye_description");
                obj.accident_symptom_eye_score = rs.getString("accident_symptom_eye_score");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (AccidentSysptomEye obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }
}

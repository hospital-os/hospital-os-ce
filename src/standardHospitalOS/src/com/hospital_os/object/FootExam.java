/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
public class FootExam extends Persistent {

    public String t_visit_id = "";
    public String foot_ulcers = "";
    public String foot_ulcers_detail = "";
    public String lose_sensation = "";
    public String shoes_regularly = "";
    public String shoes_regularly_other_detail = "";
    public String active = "1";
    public String record_date_time = "";
    public String user_record_id = "";
    public String update_date_time = "";
    public String user_update_id = "";
}

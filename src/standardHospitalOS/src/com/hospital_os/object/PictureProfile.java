/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;
import java.util.Date;

/**
 *
 * @author Somprasong
 */
public class PictureProfile extends Persistent {

    public String t_person_id = "";
    public byte[] picture_profile = null;
    public Date update_date_time = new Date();
    public String user_update_id = "";
}

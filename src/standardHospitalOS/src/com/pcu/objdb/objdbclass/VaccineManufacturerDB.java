/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.objdb.objdbclass;

import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.ComboFix;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tanakrit
 */
public class VaccineManufacturerDB {

    public ConnectionInf theConnectionInf;

    public VaccineManufacturerDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public List<ComboFix> selectAll() throws Exception {
        String sql = "select * from f_vaccine_manufacturer where active = '1' order by f_vaccine_manufacturer_id";
        return veQuery(sql);
    }

    public List<ComboFix> veQuery(String sql) throws Exception {
        ComboFix p;
        List<ComboFix> list = new ArrayList<>();
        try (ResultSet rs = theConnectionInf.eQuery(sql)) {
            while (rs.next()) {
                p = new ComboFix();
                p.code = rs.getString("f_vaccine_manufacturer_id");
                p.name = rs.getString("vaccine_manufacturer_name");
                list.add(p);
            }
        }
        return list;
    }
}

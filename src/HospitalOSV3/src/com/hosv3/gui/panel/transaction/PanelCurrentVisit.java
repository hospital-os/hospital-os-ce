/*
 * PanelCurrentVisit.java
 *
 * Created on 8 ����Ҥ� 2548, 21:09 �.
 */
package com.hosv3.gui.panel.transaction;

import com.hospital_os.object.Active;
import com.hospital_os.object.AdmitLeaveDay;
import com.hospital_os.object.AllergicReactions;
import com.hospital_os.object.AppointmentStatus;
import com.hospital_os.object.Authentication;
import com.hospital_os.object.Dischar;
import com.hospital_os.object.DischargeOpd;
import com.hospital_os.object.DischargeType;
import com.hospital_os.object.Employee;
import com.hospital_os.object.NCD;
import com.hospital_os.object.Office;
import com.hospital_os.object.OrderItemReceiveDrug;
import com.hospital_os.object.Patient;
import com.hospital_os.object.Patient2qPlus;
import com.hospital_os.object.PatientAdl;
import com.hospital_os.object.PatientPayment;
import com.hospital_os.object.Payment;
import com.hospital_os.object.PersonalDisease;
import com.hospital_os.object.PictureProfile;
import com.hospital_os.object.Plan;
import com.hospital_os.object.ServicePoint;
import com.hospital_os.object.ServiceType;
import com.hospital_os.object.Transfer;
import com.hospital_os.object.Visit;
import com.hospital_os.object.VisitType;
import com.hospital_os.object.Ward;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hospital_os.utility.ComboboxModel;
import com.hospital_os.utility.Gutil;
import com.hosv3.control.HosControl;
import com.hosv3.control.LookupControl;
import com.hosv3.gui.dialog.HosDialog;
import com.hosv3.gui.dialog.PanelPatientHistory;
import com.hosv3.gui.panel.transaction.inf.PanelButtonPluginManager;
import com.hosv3.gui.panel.transaction.inf.PanelButtonPluginProvider;
import com.hosv3.object.HosObject;
import com.hosv3.subject.HosSubject;
import com.hosv3.usecase.transaction.ManageBillingResp;
import com.hosv3.usecase.transaction.ManageLabXrayResp;
import com.hosv3.usecase.transaction.ManageOrderResp;
import com.hosv3.usecase.transaction.ManagePatientResp;
import com.hosv3.usecase.transaction.ManageServiceLoader;
import com.hosv3.usecase.transaction.ManageVPaymentResp;
import com.hosv3.usecase.transaction.ManageVisitResp;
import com.hosv3.usecase.transaction.ManageVitalResp;
import com.hosv3.usecase.transaction.ManagerPatientHistoryResp;
import com.hosv3.usecase.transaction.NotificationObserver;
import com.hosv3.utility.Constant;
import com.hosv3.utility.DateUtil;
import com.hosv3.utility.GuiLang;
import com.hosv3.utility.ImageUtils;
import com.pcu.object.Family;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.image.BufferedImage;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 * l.
 *
 * @author administrator
 */
@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public class PanelCurrentVisit extends javax.swing.JPanel
        implements ManagePatientResp, ManageVisitResp,
        ManageVitalResp, ManageOrderResp, ManageBillingResp,
        ManageLabXrayResp, ManageVPaymentResp,
        NotificationObserver, ManageServiceLoader, ManagerPatientHistoryResp {

    private static final Logger LOG = Logger.getLogger(PanelCurrentVisit.class.getName());

    static final long serialVersionUID = 0;
    private HosObject theHO;
    private HosControl theHC;
    private HosSubject theHS;
    private HosDialog theHD;
    private UpdateStatus theUS;
    private Visit theVisit;
    private Patient thePatient;
    private Vector theTransferV;
    private final Transfer theTransfer = new Transfer();
    private LookupControl theLookupControl;
    private Family theFamily;
    private PanelPatientHistory thePPH;
    private final Map<String, JComponent> mapNotify = new HashMap<String, JComponent>();

    /**
     * Creates new form PanelCurrentVisit
     */
    public PanelCurrentVisit() {
        initComponents();
        lblPictureProfile.setComponentPopupMenu(popupMenuPicProfile);
        jLabelReturnDrug.setVisible(false);
        jLabelPtAllergyValue.setVisible(false);
        jLabelDischarge.setVisible(false);
        jLabelAdmitLeaveHome.setVisible(false);// Somprasong 211111
        jLabelRemain.setVisible(false);
        jLabelDoctor.setVisible(false);
        jLabelLockUser.setVisible(false);
        jLabelPersonalDisease.setVisible(false);
        jLabelAppointment.setVisible(false);
        btnNote.setEnabled(false);
        lblInformIntolerance.setVisible(false);
        jLabelPatientAdl.setVisible(false);
        jLabelPatient2qPlus.setVisible(false);
    }

    public void setControl(HosControl hc, UpdateStatus us) {
        theHC = hc;
        theHO = hc.theHO;
        theHS = hc.theHS;
        theUS = us;

        theLookupControl = hc.theLookupControl;

        theHS.thePatientSubject.attachManagePatient(this);
        theHS.theVisitSubject.attachManageVisit(this);
        theHS.theVitalSubject.attachManageVital(this);
        theHS.theOrderSubject.attachManageOrder(this);
        theHS.theBillingSubject.attachManageBilling(this);
        theHS.theResultSubject.attachManageLab(this);
        theHS.theResultSubject.attachManageXray(this);
        theHS.theVPaymentSubject.attach(this);
        theHS.theCurrentNotificationSubject.addObserver(this);
        theHS.theServiceLoaderSubject.attachManage(this);
        theHS.theAdlAnd2QPlusSubject.attach(this);

        if (theHO.theEmployee.authentication_id.equals(Authentication.XRAY)
                || theHO.theEmployee.authentication_id.equals(Authentication.LAB)) {
            jComboBoxSendToLocation.setEnabled(false);
            jButtonSend.setEnabled(false);
        }
        initComboBox();
        setVisit(null);
        setLanguage(null);
    }

    /*
     * neung �ӡ�õ�Ǩ�ͺ��Ҽ�������� ���������ҷ�ö�������ҧ
     * ��зӡ��૵visitble��������
     */
    public void initComboBox() {
        ComboboxModel.initComboBox(jComboBoxSendToLocation, theHC.theLookupControl.listServicePointwithOutXL());
    }

    public void setDialog(HosDialog hd) {
        theHD = hd;
    }

    @Override
    public void setEnabled(boolean b) {
        boolean is_labxray = theHO.theEmployee.authentication_id.equals(Authentication.LAB)
                || theHO.theEmployee.authentication_id.equals(Authentication.XRAY);
        b = b && !is_labxray;
        jComboBoxSendToLocation.setEnabled(b);
        ServicePoint sp = (ServicePoint) jComboBoxSendToLocation.getSelectedItem();
        boolean is_diag = sp.service_type_id.equals(ServiceType.DIAG);
        jComboBoxSendToDoctor.setEnabled(b && is_diag);
        //henbe comment 21/3/2549
        jButtonSend.setEnabled(b);
        //jButtonUnlock.setEnabled(b);
    }

    private void getTransfer(Transfer t) {
        t.service_point_id = Gutil.getGuiData(jComboBoxSendToLocation);
        t.doctor_code = "";
        if (jComboBoxSendToDoctor.isEnabled()) {
            t.doctor_code = Gutil.getGuiData(jComboBoxSendToDoctor);
        }

    }

    private void setAllergyV(Vector allergy) {
        if (!"".equals(theHO.orderSecret))//amp:07/03/2549 ���ͫ�͹���ͼ�����㹡ó����Ż���Դ
        {
            jLabelPtAllergyValue.setVisible(false);
            return;
        }
        if (allergy == null || allergy.isEmpty()) {
            jLabelPtAllergyValue.setVisible(false);
            this.jLabelPtAllergyValue.setToolTipText("");
            return;
        }
        jLabelPtAllergyValue.setVisible(true);
        String drugAllergy = "<HTML><BODY BGCOLOR = #FFFFFF ><FONT COLOR = RED><B>"
                + theHC.thePrintControl.getPatientAllergy(allergy, "<br>")
                + "<B></FONT></BODY></HTML>";
        jLabelPtAllergyValue.setToolTipText(drugAllergy);
    }

    private void setInformIntolerance() {
        lblInformIntolerance.setVisible(false);
        if (theHC.theHO.thePatient != null) {
            List<AllergicReactions> ars = theHC.thePatientControl.listAllergicReactionsUnCheckedByPId(theHC.theHO.thePatient.getObjectId());
            lblInformIntolerance.setVisible(ars.size() > 0);
            String toolTip = "";
            for (int i = 0; i < ars.size(); i++) {
                AllergicReactions ar = ars.get(i);
                toolTip += (i + 1) + ". " + ar.allergic_reactions_list.replaceAll("\n", "<br>") + "<br>";
            }
            toolTip = "<HTML><BODY BGCOLOR = #FFFFFF ><FONT COLOR = \"#FFA000\"><B>"
                    + toolTip
                    + "</B></FONT></BODY></HTML>";
            lblInformIntolerance.setToolTipText(toolTip);
        }
    }

    private void setPersonalDiseaseV(Vector allergy) {
        jLabelPersonalDisease.setVisible(false);
        if (allergy == null || allergy.isEmpty()) {
            return;
        }
        String drug = "";
        jLabelPersonalDisease.setVisible(true);
        for (int i = 0; i < allergy.size(); i++) {
            PersonalDisease da = (PersonalDisease) allergy.get(i);
            drug = drug + (i + 1) + ". " + da.description + "<BR>";
        }
        drug = "<HTML><BODY>" + drug + "</BODY></HTML>";
        jLabelPersonalDisease.setToolTipText(drug);
    }

    private void setPatientAdl(PatientAdl thePatientAdl) {
        jLabelPatientAdl.setVisible(false);
        if (thePatientAdl == null || thePatientAdl.getObjectId() == null) {
            return;
        }
        jLabelPatientAdl.setVisible(true);
        jLabelPatientAdl.setText("ADL" + thePatientAdl.f_adl_result_id);
    }

    private void setPatient2qPlus(Patient2qPlus thePatient2qPlus) {
        jLabelPatient2qPlus.setVisible(false);
        if (thePatient2qPlus == null || thePatient2qPlus.getObjectId() == null) {
            return;
        }
        jLabelPatient2qPlus.setVisible(true);
        jLabelPatient2qPlus.setText(thePatient2qPlus.getDescription());
    }

    /**
     * @Author : amp
     * @date : 15/02/2549
     * @see : �ʴ�����ᾷ�����������Ҿ�
     */
    private void setDoctor(Vector vTransfer) {
        if (this.theVisit == null) {
            jLabelDoctor.setVisible(false);
            return;
        }
        if (vTransfer == null) {
            jLabelDoctor.setVisible(false);
            return;
        }
        // Henbe ���¿ѧ�ѹ��ᾷ��ͧ��������� theHO.getDoctorInVisit(); ����������͹� �������ҧ��
        // ��������ѹ�ջ���ª���ҡ�����������
        Vector vDoctor = theHO.getDoctorInVisit();
        if (vDoctor == null || vDoctor.isEmpty()) {
            jLabelDoctor.setVisible(false);
            return;
        }
        jLabelDoctor.setVisible(true);
        String nameDoctor = "";
        for (int i = 0, size = vDoctor.size(); i < size; i++) {
            Employee em = theHC.theLookupControl.readEmployeeById((String) vDoctor.get(i));
            if (em != null) {
                if (i == 0) {
                    nameDoctor = em.getName();
                } else {
                    nameDoctor = nameDoctor + "<BR>" + em.getName();
                }
            }
        }
        jLabelDoctor.setToolTipText("<HTML><BODY BGCOLOR = #FFFFFF ><FONT COLOR = RED><B>" + nameDoctor + "<B></FONT></BODY></HTML>");
    }

    private void setPatient(Patient p) {
        thePatient = p;
        jTextFieldHN.setText("");
        jLabelNCDCode.setText("");
        jLabelRemain.setVisible(false);
        lblWardName.setText("");
        lblBedNumber.setText("");
        panelVisitBedInfo.setVisible(false);
        setPictureProfile(null);
        if (p == null) {
            return;
        }

        if (!"".equals(p.hn)) {
            jTextFieldHN.setText(theLookupControl.getRenderTextHN(p.hn));
        }
        setTextNCD();
        String data = theHC.theBillingControl.getBillRemaining(theHO.vBillingPatient);
        if (data.length() > 0) {
            jLabelRemain.setVisible(true);
            jLabelRemain.setToolTipText(data);
        }
        if (theHO.theGActionAuthV.isReadViewPatientPictureProfile()
                && thePatient.getFamily().pictureProfile != null && thePatient.getFamily().pictureProfile.picture_profile != null
                && thePatient.getFamily().pictureProfile.picture_profile.length != 0) {
            BufferedImage image = ImageUtils.toBufferedImage(
                    thePatient.getFamily().pictureProfile.picture_profile);
            setPictureProfile(image);
        }
    }

    private void setFamily(Family person) {
        theFamily = person;
        if (theFamily == null) {
            jLabelPatientNameValue.setText("");
            jLabelAge.setText("");
            jLabelDischarge.setVisible(false);
            return;
        }
        String name = theHC.theLookupControl.readPrefixString(theFamily.f_prefix_id)
                + theFamily.patient_name + "  " + theFamily.patient_last_name + " ";
        if (theFamily.active.equals("0")) {
            name += " (¡��ԡ)";
        }

        jLabelPatientNameValue.setText(name);
        if (theFamily.patient_birthday != null && !theFamily.patient_birthday.isEmpty()) {
            if (theFamily.patient_birthday_true.equals("1")) {
                String age = DateUtil.calculateAgeLong(theFamily.patient_birthday, theHO.date_time).trim();;
                if (theHC.theLO.theOption.increase_newborn_age.equals("1") && age.equals("0 �ѹ")) {
                    age = "1 �ѹ";
                }
                jLabelAge.setText(Constant.getTextBundle("����") + " " + age);
            } else {
                String age = DateUtil.calculateAge(theFamily.patient_birthday, theHO.date_time).trim();;
                if (theHC.theLO.theOption.increase_newborn_age.equals("1") && age.equals("0 �ѹ")) {
                    age = "1 �ѹ";
                }
                jLabelAge.setText(Constant.getTextBundle("����") + " "
                        + age + " " + Constant.getTextBundle("��"));
            }
        }
        jLabelDischarge.setVisible(theFamily.discharge_status_id.equals(Dischar.DEATH));
    }

    private boolean setPaymentV(Vector p) {
        if (theVisit == null) {
            jLabelPlan.setText("");
            jLabelPlan.setToolTipText("");
            return false;
        }
        jLabelPlan.setText(Constant.getTextBundle("��辺�Է������ѡ��"));
        jLabelPlan.setToolTipText(null);
        //////////////////////////////////////////////////////////
        Vector thePayment = p;
        String ttt = new String();
        if (thePayment == null) {
            thePayment = new Vector();
        }

        for (int i = 0, size = thePayment.size(); i < size; i++) {
            Payment pm = (Payment) thePayment.get(i);
            if (pm.visit_payment_active.equals("1")) {
                String plan_desc = theHC.theLookupControl.readPlanString(pm.plan_kid);
                if (i == 0) {
                    jLabelPlan.setText(plan_desc);
                }
                ttt = ttt + "," + plan_desc;
            }
        }
        //remove , from ttt
        if (ttt.length() > 1) {
            ttt = ttt.substring(1);
        }
        jLabelPlan.setToolTipText(ttt);
        return true;
    }

    private void setPatientPaymentV(Vector p) {
        if (p == null || p.isEmpty()) {
            jLabelPlan.setText("");
            jLabelPlan.setToolTipText("");
            return;
        }
        jLabelPlan.setText(Constant.getTextBundle("��辺�Է������ѡ��"));
        jLabelPlan.setToolTipText(null);
        //////////////////////////////////////////////////////////
        Vector thePayment = p;
        String ttt = new String();
        String plan_desc = Constant.getTextBundle("��辺㹰ҹ������");
        for (int i = 0, size = thePayment.size(); i < size; i++) {
            String plan_kid_visit = ((PatientPayment) thePayment.get(i)).plan_kid;
            Plan plan = theHC.theLookupControl.readPlanById(plan_kid_visit);
            ///////////////////////////////////////////////////////////
            if (plan != null) {
                plan_desc = plan.description;
            }
            ///////////////////////////////////////////////////////////
            if (i == 0) {
                jLabelPlan.setText(plan_desc);
            }
            ttt = ttt + "," + plan_desc;
        }
        //remove , from ttt
        if (ttt.length() > 1) {
            ttt = ttt.substring(1);
        }
        jLabelPlan.setToolTipText(ttt);
    }

    private void setVisit(Visit visit) {
        theVisit = visit;
        if (theVisit == null) {
            jTextFieldVN.setText("");
            jLabelDx.setText("...");
            jTextFieldDDoctor.setVisible(false);
            jTextFieldDBilling.setVisible(false);
            lblOPDIPD.setText("");
            lblOPDIPD.setToolTipText(null);
            jLabelReturnDrug.setVisible(false);
            jLabelDoctor.setVisible(false);
            //sumo:17/06/2549 ���ͫ�͹ʶҹС����͡������
            jLabelLockUser.setToolTipText("");
            jLabelLockUser.setVisible(false);
            jLabelRefer.setVisible(false);
            jLabelVisitDate.setVisible(false);
            jLabelAppointment.setVisible(false);
            jLabelAdmitLeaveHome.setVisible(false);// Somprasong 211111
            setEnabled(false);
            lblWardName.setText("");
            lblBedNumber.setText("");
            panelVisitBedInfo.setVisible(false);
            return;
        }
        // if have visit data age must calulate base on visit date time
        if (theFamily.patient_birthday != null && !theFamily.patient_birthday.isEmpty()) {
            if (theFamily.patient_birthday_true.equals("1")) {
                String age = DateUtil.calculateAgeLong(theFamily.patient_birthday, theVisit.begin_visit_time).trim();;
                if (theHC.theLO.theOption.increase_newborn_age.equals("1") && age.equals("0 �ѹ")) {
                    age = "1 �ѹ";
                }
                jLabelAge.setText(Constant.getTextBundle("����") + " " + age);
            } else {
                String age = DateUtil.calculateAge(theFamily.patient_birthday, theVisit.begin_visit_time).trim();;
                if (theHC.theLO.theOption.increase_newborn_age.equals("1") && age.equals("0 �ѹ")) {
                    age = "1 �ѹ";
                }
                jLabelAge.setText(Constant.getTextBundle("����") + " "
                        + age + " " + Constant.getTextBundle("��"));
            }
        }
        jLabelDx.setText(Gutil.getTextLabel(theVisit.doctor_dx));
        jLabelDx.setToolTipText(Gutil.getToolTipText(theVisit.doctor_dx));
        jTextFieldVN.setText(theLookupControl.getRenderTextVN(theVisit.vn));

        Employee e = theHC.theLookupControl.readEmployeeById(theVisit.lock_user);
        String name;
        if (e != null) {
            name = e.person.person_firstname + "   " + e.person.person_lastname;
        } else {
            name = theVisit.lock_user;
        }
        if (theVisit.isDropVisit()) {
            jLabelPatientNameValue.setText(jLabelPatientNameValue.getText() + " (¡��ԡ����Ѻ��ԡ��)");
        }
        String final_appoint_date = theHC.thePatientControl.getFinalAppointDate(theUS);
        int appoint_day = DateUtil.countDateDiff(final_appoint_date, theHC.theConnectionInf);
        if (appoint_day > 0) {
            jLabelAppointment.setVisible(true);
            jLabelAppointment.setText(appoint_day + " " + Constant.getTextBundle(" �ѹ �֧�ѹ���Ѵ"));
        } else {
            jLabelAppointment.setVisible(false);
        }
        if (theHO.theFinalAppointMent != null && theHO.theFinalAppointMent.status.equals(AppointmentStatus.CANCEL)) {
            jLabelAppointment.setVisible(false);
        }
        jLabelLockUser.setVisible(false);
        if (theVisit.locking.equals("1")) {
            if (!theVisit.lock_user.equals(theHO.theEmployee.getObjectId())) {
                jLabelLockUser.setVisible(true);
                jLabelLockUser.setText(name);
                jLabelLockUser.setToolTipText(Constant.getTextBundle("��͡�� ") + name + " " + Constant.getTextBundle("�����") + " "
                        + DateUtil.getDateToString(DateUtil.getDateFromText(theVisit.lock_time), true));
            }
        }
        jLabelRefer.setVisible(false);
        if (theHO.theReferIn != null) {
            Office office = theHC.theLookupControl.readHospitalByCode(theHO.theReferIn.office_refer);
            if (office != null) {
                jLabelRefer.setToolTipText(Constant.getTextBundle("�Ѻ�ҡ") + " " + office.name);
            }
            jLabelRefer.setVisible(true);
        }
        if (theHO.theReferOut != null) {
            Office office = theHC.theLookupControl.readHospitalByCode(theHO.theReferOut.office_refer);
            if (office != null) {
                if (theHO.theReferIn != null) {
                    jLabelRefer.setToolTipText(jLabelRefer.getToolTipText() + Constant.getTextBundle("�觵��") + " " + office.name);
                } else {
                    jLabelRefer.setToolTipText(Constant.getTextBundle("�觵��") + " " + office.name);
                }
            }
            jLabelRefer.setVisible(true);
        }

        jLabelVisitDate.setVisible(true);
        Date date = DateUtil.getDateFromText(visit.begin_visit_time);
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yy");
        jLabelVisitDate.setText(formatter.format(date));

        if (visit.is_discharge_money.equals(Active.isEnable())) {
            jTextFieldDBilling.setToolTipText(Constant.getTextBundle("��˹��·ҧ����Թ�������"));
            jTextFieldDBilling.setBackground(Color.red);
        } else {
            jTextFieldDBilling.setToolTipText(Constant.getTextBundle("�ͨ�˹��·ҧ����Թ"));
            jTextFieldDBilling.setBackground(Color.green);
        }
        jTextFieldDBilling.setVisible(true);

        jTextFieldDDoctor.setToolTipText(Constant.getTextBundle("�ͨ�˹��·ҧ���ᾷ��"));
        jTextFieldDDoctor.setBackground(Color.green);
        jTextFieldDDoctor.setVisible(true);
        if (visit.is_discharge_doctor.equals(Active.isEnable())) {
            String display;// = Constant.getTextBundle("��˹��·ҧ���ᾷ������");
            if (theVisit.visit_type.equals(VisitType.IPD)) {
                display = theVisit.discharge_ipd_type;
                DischargeType disI = theHC.theLookupControl.readDischargeTypeById(display);
                if (disI != null) {
                    display = disI.description;
                }
            } else {
                display = theVisit.discharge_opd_status;
                DischargeOpd disO = theHC.theLookupControl.readDischargeOpdById(display);
                if (disO != null) {
                    display = disO.description;
                    if (disO.getObjectId().equals(DischargeOpd.REFER)) {
                        display += theHC.theLookupControl.readHospitalSByCode(theVisit.refer_out);
                    }
                }
            }
            jTextFieldDDoctor.setToolTipText(display);
            jTextFieldDDoctor.setBackground(Color.red);
        }

        lblWardName.setText("");
        lblBedNumber.setText("");
        panelVisitBedInfo.setVisible(false);
        if (visit.visit_type.equals(VisitType.IPD)) {
            String status_ipd = " IPD ";
            if (visit.is_discharge_ipd.equals("1")) {
                status_ipd = " IPD Disch ";
            }
            lblOPDIPD.setText(status_ipd);
            lblOPDIPD.setToolTipText(status_ipd);
            lblOPDIPD.setForeground(Color.RED);
            jTextFieldVN.setForeground(Color.RED);

            lblWardName.setText(theHO.theVisitBed != null ? theHO.theVisitBed.wardName
                    : theLookupControl.getWardName(visit.ward));
            lblBedNumber.setText(theHO.theVisitBed != null ? theHO.theVisitBed.bed_number : "N/A");
            panelVisitBedInfo.setVisible(true);
        } else {
            jTextFieldVN.setForeground(Color.BLACK);
            lblOPDIPD.setText(" OPD ");
            lblOPDIPD.setToolTipText("OPD");
            lblOPDIPD.setForeground(Color.BLACK);
            if (visit.observe.equals("1")) {
                lblOPDIPD.setText(" " + Constant.getTextBundle("�ҡ�͹") + " ");
                lblOPDIPD.setToolTipText(Constant.getTextBundle("�ҡ�͹"));
            }
        }

        //list OrderItemReceiveDrug by Visit_id ReceiveQty
        Vector voird = theHO.vOrderItemReceiveDrug;

        boolean is_return_drug = false;
        // ��� voird �����ҡѺ null sumo 25/08/2549
        if (voird != null) {
            for (int i = 0, size = voird.size(); i < size; i++) {
                OrderItemReceiveDrug oird = (OrderItemReceiveDrug) voird.get(i);
                if (oird.qty_receive.equals("null") || oird.qty_receive.isEmpty()
                        || Double.parseDouble(oird.qty_receive) < Double.parseDouble(oird.qty_return)) {
                    is_return_drug = true;
                }
            }
        }
        jLabelReturnDrug.setVisible(is_return_drug);
        //�������͹��Ѻ�ҧ���ᾷ�� �����·������ʶҹ����ª��Ե �е�ͧ����ʴ��ٻ��ǡ���š
        if (theHO.theFamily.discharge_status_id.equals("0")) {
            jLabelDischarge.setVisible(false);
        }
        // SOmprasong 21111  �ʴ�ʶҹз�����ٻ��ҹ  �óշ���繪�ǧ���ҷ�褹���ҡ�Ѻ��ҹ
        AdmitLeaveDay admitLeaveDay = theHC.theVisitControl.findLastestByVisitId(theVisit.getObjectId());
        jLabelAdmitLeaveHome.setVisible(admitLeaveDay != null && "0".equals(admitLeaveDay.comeback));

        if (theVisit.isLockingByOther(theHO.theEmployee.getObjectId())
                || theVisit.isOutProcess()
                || theVisit.isDischargeMoney()
                || theVisit.isInStat()
                || theVisit.isDropVisit()) {
            setEnabled(false);
            return;
        }
        setEnabled(true);
    }

    /**
     * @Author: amp date: 4/8/2549 see: �ʴ������Ţ NCD ��Ҽ���������㹡����
     * NCD
     */
    private void setTextNCD() {
        jLabelNCDCode.setText("");
        if (theHO.vNCD != null) {
            String ncd_code = "";
            for (int i = 0, size = theHO.vNCD.size(); i < size; i++) {
                NCD ncd_data = (NCD) theHO.vNCD.get(i);
                if (i == 0) {
                    ncd_code += ncd_data.ncd_number;
                    jLabelNCDCode.setText("NCD: " + ncd_code);
                } else {
                    jLabelNCDCode.setText(jLabelNCDCode.getText() + "...");
                    ncd_code = ncd_code + ", " + ncd_data.ncd_number;
                }
            }
            jLabelNCDCode.setToolTipText("NCD: " + ncd_code);
        }
    }

    public String getServicePointName(Transfer tran) {
        ServicePoint sp = theHC.theLookupControl.readServicePointById(tran.service_point_id);
        Ward ward = theHC.theLookupControl.readWardById(tran.ward_id);
        if (sp != null) {
            return sp.name;
        } else if (ward != null) {
            return ward.description;
        } else {
            return "����բ�����";
        }
    }

    private void setTransferV(Vector theTransfer) {
        theTransferV = theTransfer;
        jLabelLocationValue.setText("");
        jLabelLocationValue.setToolTipText(null);
        jLabelLastLocationValue.setText("");
        jLabelLastLocationValue.setToolTipText(null);
        setDoctor(theTransfer);
        if (theTransfer == null || theTransfer.isEmpty()) {
            return;
        }

        Transfer t = (Transfer) theTransferV.get(theTransferV.size() - 1);
        String sp_name = this.getServicePointName(t);
        jLabelLocationValue.setText(sp_name);
        jLabelLocationValue.setToolTipText(sp_name);

        if (theTransferV.size() < 2) {
            return;
        }
        t = (Transfer) theTransferV.get(theTransferV.size() - 2);
        sp_name = this.getServicePointName(t);
        jLabelLastLocationValue.setText(sp_name);
        jLabelLastLocationValue.setToolTipText(sp_name);

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        popupMenuPicProfile = new javax.swing.JPopupMenu();
        miViewPicProfile = new javax.swing.JMenuItem();
        miChangePicProfile = new javax.swing.JMenuItem();
        miDeletePicProfile = new javax.swing.JMenuItem();
        jPanelPatientInfo1 = new javax.swing.JPanel();
        jLabelHN = new javax.swing.JLabel();
        jTextFieldHN = new javax.swing.JTextField();
        jLabelVN = new javax.swing.JLabel();
        jLabelAN = new javax.swing.JLabel();
        jTextFieldVN = new javax.swing.JTextField();
        jLabelPatientNameValue = new javax.swing.JLabel();
        jLabelAge = new javax.swing.JLabel();
        jLabelNCDCode = new javax.swing.JLabel();
        panelVisitBedInfo = new javax.swing.JPanel();
        lblWard = new javax.swing.JLabel();
        lblWardName = new javax.swing.JLabel();
        lblBed = new javax.swing.JLabel();
        lblBedNumber = new javax.swing.JLabel();
        jLabelVisitDate = new javax.swing.JLabel();
        lblOPDIPD = new javax.swing.JLabel();
        jTextFieldDDoctor = new javax.swing.JLabel();
        jTextFieldDBilling = new javax.swing.JLabel();
        jPanelPatientInfo2 = new javax.swing.JPanel();
        jLabelLocation = new javax.swing.JLabel();
        jLabelLocationValue = new javax.swing.JLabel();
        jLabelLastLocation = new javax.swing.JLabel();
        jLabelLastLocationValue = new javax.swing.JLabel();
        jLabelDxLbl = new javax.swing.JLabel();
        jLabelDx = new javax.swing.JLabel();
        jLabelPtAllergyValue = new javax.swing.JLabel();
        jLabelPlanLbl = new javax.swing.JLabel();
        jLabelPlan = new javax.swing.JLabel();
        jLabelReturnDrug = new javax.swing.JLabel();
        jLabelDischarge = new javax.swing.JLabel();
        jLabelDoctor = new javax.swing.JLabel();
        jLabelPersonalDisease = new javax.swing.JLabel();
        jLabelAdmitLeaveHome = new javax.swing.JLabel();
        lblInformIntolerance = new javax.swing.JLabel();
        jLabelPatientAdl = new javax.swing.JLabel();
        jLabelPatient2qPlus = new javax.swing.JLabel();
        jPanelLine = new javax.swing.JPanel();
        jLabelSendToLocation = new javax.swing.JLabel();
        jLabelSendToDoctor = new javax.swing.JLabel();
        jComboBoxSendToDoctor = new javax.swing.JComboBox();
        jButtonSend = new javax.swing.JButton();
        jButtonUnlock = new javax.swing.JButton();
        jLabelLockUser = new javax.swing.JLabel();
        jLabelRemain = new javax.swing.JLabel();
        jComboBoxSendToLocation = new javax.swing.JComboBox();
        jLabelRefer = new javax.swing.JLabel();
        jButtonNext = new javax.swing.JButton();
        jButtonPrev = new javax.swing.JButton();
        jLabelAppointment = new javax.swing.JLabel();
        btnNote = new javax.swing.JButton();
        panelNotification = new javax.swing.JPanel();
        panelButtonPlugin = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        lblPictureProfile = new javax.swing.JLabel();

        miViewPicProfile.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/find_24.png"))); // NOI18N
        miViewPicProfile.setText("���Ҿ������");
        miViewPicProfile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miViewPicProfileActionPerformed(evt);
            }
        });
        popupMenuPicProfile.add(miViewPicProfile);

        miChangePicProfile.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/camera24.png"))); // NOI18N
        miChangePicProfile.setText("����¹�Ҿ������");
        miChangePicProfile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miChangePicProfileActionPerformed(evt);
            }
        });
        popupMenuPicProfile.add(miChangePicProfile);

        miDeletePicProfile.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/cancel_24.png"))); // NOI18N
        miDeletePicProfile.setText("ź�Ҿ������");
        miDeletePicProfile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miDeletePicProfileActionPerformed(evt);
            }
        });
        popupMenuPicProfile.add(miDeletePicProfile);

        setMinimumSize(new java.awt.Dimension(896, 85));
        setPreferredSize(new java.awt.Dimension(896, 85));
        setLayout(new java.awt.GridBagLayout());

        jPanelPatientInfo1.setMinimumSize(new java.awt.Dimension(358, 26));
        jPanelPatientInfo1.setPreferredSize(new java.awt.Dimension(358, 26));
        jPanelPatientInfo1.setLayout(new java.awt.GridBagLayout());

        jLabelHN.setFont(jLabelHN.getFont());
        jLabelHN.setText("HN");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelPatientInfo1.add(jLabelHN, gridBagConstraints);

        jTextFieldHN.setFont(jTextFieldHN.getFont());
        jTextFieldHN.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        jTextFieldHN.setMinimumSize(new java.awt.Dimension(80, 20));
        jTextFieldHN.setPreferredSize(new java.awt.Dimension(80, 20));
        jTextFieldHN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldHNActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelPatientInfo1.add(jTextFieldHN, gridBagConstraints);

        jLabelVN.setFont(jLabelVN.getFont());
        jLabelVN.setText("VN");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelPatientInfo1.add(jLabelVN, gridBagConstraints);

        jLabelAN.setFont(jLabelAN.getFont());
        jLabelAN.setForeground(new java.awt.Color(255, 0, 0));
        jLabelAN.setText("AN");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelPatientInfo1.add(jLabelAN, gridBagConstraints);

        jTextFieldVN.setFont(jTextFieldVN.getFont());
        jTextFieldVN.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        jTextFieldVN.setToolTipText("");
        jTextFieldVN.setMinimumSize(new java.awt.Dimension(80, 20));
        jTextFieldVN.setPreferredSize(new java.awt.Dimension(80, 20));
        jTextFieldVN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldVNActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelPatientInfo1.add(jTextFieldVN, gridBagConstraints);

        jLabelPatientNameValue.setFont(jLabelPatientNameValue.getFont());
        jLabelPatientNameValue.setText("     ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelPatientInfo1.add(jLabelPatientNameValue, gridBagConstraints);

        jLabelAge.setFont(jLabelAge.getFont());
        jLabelAge.setText("     ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelPatientInfo1.add(jLabelAge, gridBagConstraints);

        jLabelNCDCode.setFont(jLabelNCDCode.getFont());
        jLabelNCDCode.setForeground(java.awt.Color.blue);
        jLabelNCDCode.setText("     ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelPatientInfo1.add(jLabelNCDCode, gridBagConstraints);

        panelVisitBedInfo.setLayout(new java.awt.GridBagLayout());

        lblWard.setFont(lblWard.getFont());
        lblWard.setForeground(new java.awt.Color(255, 0, 0));
        lblWard.setText("����");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelVisitBedInfo.add(lblWard, gridBagConstraints);

        lblWardName.setFont(lblWardName.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelVisitBedInfo.add(lblWardName, gridBagConstraints);

        lblBed.setFont(lblBed.getFont());
        lblBed.setForeground(new java.awt.Color(255, 0, 0));
        lblBed.setText("��§");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 7, 2, 2);
        panelVisitBedInfo.add(lblBed, gridBagConstraints);

        lblBedNumber.setFont(lblBedNumber.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelVisitBedInfo.add(lblBedNumber, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        jPanelPatientInfo1.add(panelVisitBedInfo, gridBagConstraints);

        jLabelVisitDate.setFont(jLabelVisitDate.getFont().deriveFont(jLabelVisitDate.getFont().getStyle() | java.awt.Font.BOLD, jLabelVisitDate.getFont().getSize()+2));
        jLabelVisitDate.setForeground(java.awt.Color.blue);
        jLabelVisitDate.setText("�ѹ����Ѻ��ԡ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 9;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelPatientInfo1.add(jLabelVisitDate, gridBagConstraints);

        lblOPDIPD.setFont(lblOPDIPD.getFont().deriveFont(lblOPDIPD.getFont().getStyle() | java.awt.Font.BOLD, lblOPDIPD.getFont().getSize()+1));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelPatientInfo1.add(lblOPDIPD, gridBagConstraints);

        jTextFieldDDoctor.setFont(jTextFieldDDoctor.getFont());
        jTextFieldDDoctor.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jTextFieldDDoctor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/dd_wait.gif"))); // NOI18N
        jTextFieldDDoctor.setMaximumSize(new java.awt.Dimension(48, 24));
        jTextFieldDDoctor.setMinimumSize(new java.awt.Dimension(48, 24));
        jTextFieldDDoctor.setOpaque(true);
        jTextFieldDDoctor.setPreferredSize(new java.awt.Dimension(48, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 11;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelPatientInfo1.add(jTextFieldDDoctor, gridBagConstraints);

        jTextFieldDBilling.setFont(jTextFieldDBilling.getFont());
        jTextFieldDBilling.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jTextFieldDBilling.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/mo_ney.gif"))); // NOI18N
        jTextFieldDBilling.setMaximumSize(new java.awt.Dimension(48, 24));
        jTextFieldDBilling.setMinimumSize(new java.awt.Dimension(48, 24));
        jTextFieldDBilling.setOpaque(true);
        jTextFieldDBilling.setPreferredSize(new java.awt.Dimension(48, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelPatientInfo1.add(jTextFieldDBilling, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(3, 2, 1, 2);
        add(jPanelPatientInfo1, gridBagConstraints);

        jPanelPatientInfo2.setLayout(new java.awt.GridBagLayout());

        jLabelLocation.setFont(jLabelLocation.getFont());
        jLabelLocation.setForeground(java.awt.Color.red);
        jLabelLocation.setText("�ش�Ѩ�غѹ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 1, 1);
        jPanelPatientInfo2.add(jLabelLocation, gridBagConstraints);

        jLabelLocationValue.setFont(jLabelLocationValue.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelPatientInfo2.add(jLabelLocationValue, gridBagConstraints);

        jLabelLastLocation.setFont(jLabelLastLocation.getFont());
        jLabelLastLocation.setForeground(java.awt.Color.red);
        jLabelLastLocation.setText("�ش����ҹ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 1);
        jPanelPatientInfo2.add(jLabelLastLocation, gridBagConstraints);

        jLabelLastLocationValue.setFont(jLabelLastLocationValue.getFont());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelPatientInfo2.add(jLabelLastLocationValue, gridBagConstraints);

        jLabelDxLbl.setFont(jLabelDxLbl.getFont());
        jLabelDxLbl.setForeground(java.awt.Color.red);
        jLabelDxLbl.setText("Dx: ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 1);
        jPanelPatientInfo2.add(jLabelDxLbl, gridBagConstraints);

        jLabelDx.setFont(jLabelDx.getFont());
        jLabelDx.setText("....");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelPatientInfo2.add(jLabelDx, gridBagConstraints);

        jLabelPtAllergyValue.setBackground(new java.awt.Color(255, 204, 153));
        jLabelPtAllergyValue.setFont(jLabelPtAllergyValue.getFont().deriveFont(jLabelPtAllergyValue.getFont().getStyle() | java.awt.Font.BOLD, jLabelPtAllergyValue.getFont().getSize()+2));
        jLabelPtAllergyValue.setForeground(java.awt.Color.red);
        jLabelPtAllergyValue.setText("�բ���������");
        jLabelPtAllergyValue.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabelPtAllergyValue.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 255, 255), 1, true));
        jLabelPtAllergyValue.setOpaque(true);
        jLabelPtAllergyValue.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jLabelPtAllergyValueMouseReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelPatientInfo2.add(jLabelPtAllergyValue, gridBagConstraints);

        jLabelPlanLbl.setFont(jLabelPlanLbl.getFont());
        jLabelPlanLbl.setForeground(new java.awt.Color(255, 0, 0));
        jLabelPlanLbl.setText("�Է��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 5, 1, 1);
        jPanelPatientInfo2.add(jLabelPlanLbl, gridBagConstraints);

        jLabelPlan.setFont(jLabelPlan.getFont());
        jLabelPlan.setText("....");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelPatientInfo2.add(jLabelPlan, gridBagConstraints);

        jLabelReturnDrug.setForeground(java.awt.Color.red);
        jLabelReturnDrug.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/return_drug.jpg"))); // NOI18N
        jLabelReturnDrug.setToolTipText("�׹��");
        jLabelReturnDrug.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelPatientInfo2.add(jLabelReturnDrug, gridBagConstraints);

        jLabelDischarge.setForeground(java.awt.Color.red);
        jLabelDischarge.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/Skull.gif"))); // NOI18N
        jLabelDischarge.setToolTipText("���������ª��Ե");
        jLabelDischarge.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabelDischarge.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jLabelDischargeMouseReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 17;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelPatientInfo2.add(jLabelDischarge, gridBagConstraints);

        jLabelDoctor.setForeground(new java.awt.Color(0, 0, 204));
        jLabelDoctor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/meet_doctor.jpg"))); // NOI18N
        jLabelDoctor.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabelDoctor.setAlignmentY(0.0F);
        jLabelDoctor.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 18;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelPatientInfo2.add(jLabelDoctor, gridBagConstraints);

        jLabelPersonalDisease.setBackground(new java.awt.Color(255, 204, 153));
        jLabelPersonalDisease.setFont(jLabelPersonalDisease.getFont().deriveFont(jLabelPersonalDisease.getFont().getStyle() | java.awt.Font.BOLD, jLabelPersonalDisease.getFont().getSize()+2));
        jLabelPersonalDisease.setForeground(new java.awt.Color(255, 0, 0));
        jLabelPersonalDisease.setText("�ä��Шӵ��");
        jLabelPersonalDisease.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 255, 255), 1, true));
        jLabelPersonalDisease.setOpaque(true);
        jLabelPersonalDisease.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jLabelPersonalDiseaseMouseReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 13;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelPatientInfo2.add(jLabelPersonalDisease, gridBagConstraints);

        jLabelAdmitLeaveHome.setForeground(java.awt.Color.blue);
        jLabelAdmitLeaveHome.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/home_icon.png"))); // NOI18N
        jLabelAdmitLeaveHome.setToolTipText("�������ҡ�Ѻ��ҹ");
        jLabelAdmitLeaveHome.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabelAdmitLeaveHome.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jLabelAdmitLeaveHomeMouseReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 19;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelPatientInfo2.add(jLabelAdmitLeaveHome, gridBagConstraints);

        lblInformIntolerance.setBackground(new java.awt.Color(255, 255, 204));
        lblInformIntolerance.setFont(lblInformIntolerance.getFont().deriveFont(lblInformIntolerance.getFont().getStyle() | java.awt.Font.BOLD, lblInformIntolerance.getFont().getSize()+2));
        lblInformIntolerance.setForeground(new java.awt.Color(244, 132, 13));
        lblInformIntolerance.setText("������/�������������");
        lblInformIntolerance.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        lblInformIntolerance.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 255, 255), 1, true));
        lblInformIntolerance.setOpaque(true);
        lblInformIntolerance.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                lblInformIntoleranceMouseReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 11;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelPatientInfo2.add(lblInformIntolerance, gridBagConstraints);

        jLabelPatientAdl.setBackground(new java.awt.Color(255, 204, 153));
        jLabelPatientAdl.setFont(jLabelPatientAdl.getFont().deriveFont(jLabelPatientAdl.getFont().getStyle() | java.awt.Font.BOLD, jLabelPatientAdl.getFont().getSize()+2));
        jLabelPatientAdl.setForeground(java.awt.Color.red);
        jLabelPatientAdl.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelPatientAdl.setText("ADL");
        jLabelPatientAdl.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 255, 255), 1, true));
        jLabelPatientAdl.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabelPatientAdl.setOpaque(true);
        jLabelPatientAdl.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jLabelPatientAdlMouseReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelPatientInfo2.add(jLabelPatientAdl, gridBagConstraints);

        jLabelPatient2qPlus.setBackground(new java.awt.Color(255, 204, 153));
        jLabelPatient2qPlus.setFont(jLabelPatient2qPlus.getFont().deriveFont(jLabelPatient2qPlus.getFont().getStyle() | java.awt.Font.BOLD, jLabelPatient2qPlus.getFont().getSize()+2));
        jLabelPatient2qPlus.setForeground(java.awt.Color.red);
        jLabelPatient2qPlus.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelPatient2qPlus.setText("2QPlus");
        jLabelPatient2qPlus.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 255, 255), 1, true));
        jLabelPatient2qPlus.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabelPatient2qPlus.setOpaque(true);
        jLabelPatient2qPlus.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jLabelPatient2qPlusMouseReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 15;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanelPatientInfo2.add(jLabelPatient2qPlus, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 1, 2);
        add(jPanelPatientInfo2, gridBagConstraints);

        jPanelLine.setMinimumSize(new java.awt.Dimension(516, 30));
        jPanelLine.setPreferredSize(new java.awt.Dimension(556, 30));
        jPanelLine.setLayout(new java.awt.GridBagLayout());

        jLabelSendToLocation.setFont(jLabelSendToLocation.getFont());
        jLabelSendToLocation.setText("����ѧ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelLine.add(jLabelSendToLocation, gridBagConstraints);

        jLabelSendToDoctor.setFont(jLabelSendToDoctor.getFont());
        jLabelSendToDoctor.setText("ᾷ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelLine.add(jLabelSendToDoctor, gridBagConstraints);

        jComboBoxSendToDoctor.setEditable(true);
        jComboBoxSendToDoctor.setFont(jComboBoxSendToDoctor.getFont());
        jComboBoxSendToDoctor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxSendToDoctorActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelLine.add(jComboBoxSendToDoctor, gridBagConstraints);

        jButtonSend.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/send.gif"))); // NOI18N
        jButtonSend.setToolTipText("�觼�����");
        jButtonSend.setMaximumSize(new java.awt.Dimension(52, 28));
        jButtonSend.setMinimumSize(new java.awt.Dimension(52, 28));
        jButtonSend.setPreferredSize(new java.awt.Dimension(52, 28));
        jButtonSend.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSendActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanelLine.add(jButtonSend, gridBagConstraints);

        jButtonUnlock.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/Unlock.png"))); // NOI18N
        jButtonUnlock.setToolTipText("�Ŵ��͡");
        jButtonUnlock.setMaximumSize(new java.awt.Dimension(52, 28));
        jButtonUnlock.setMinimumSize(new java.awt.Dimension(52, 28));
        jButtonUnlock.setPreferredSize(new java.awt.Dimension(52, 28));
        jButtonUnlock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonUnlockActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 1, 0, 0);
        jPanelLine.add(jButtonUnlock, gridBagConstraints);

        jLabelLockUser.setBackground(java.awt.Color.white);
        jLabelLockUser.setForeground(java.awt.Color.red);
        jLabelLockUser.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelLockUser.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/lock.gif"))); // NOI18N
        jLabelLockUser.setToolTipText("�����¶١��͡");
        jLabelLockUser.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 9;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelLine.add(jLabelLockUser, gridBagConstraints);

        jLabelRemain.setFont(jLabelRemain.getFont().deriveFont(jLabelRemain.getFont().getStyle() | java.awt.Font.BOLD, jLabelRemain.getFont().getSize()+2));
        jLabelRemain.setForeground(java.awt.Color.red);
        jLabelRemain.setText("��ҧ����");
        jLabelRemain.setToolTipText("��ҧ����");
        jLabelRemain.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jLabelRemainMouseReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 11;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelLine.add(jLabelRemain, gridBagConstraints);

        jComboBoxSendToLocation.setFont(jComboBoxSendToLocation.getFont());
        jComboBoxSendToLocation.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxSendToLocationActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        jPanelLine.add(jComboBoxSendToLocation, gridBagConstraints);

        jLabelRefer.setFont(jLabelRefer.getFont().deriveFont(jLabelRefer.getFont().getStyle() | java.awt.Font.BOLD, jLabelRefer.getFont().getSize()+2));
        jLabelRefer.setForeground(new java.awt.Color(102, 102, 0));
        jLabelRefer.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabelRefer.setText("Refer");
        jLabelRefer.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jLabelReferMouseReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 15, 0, 2);
        jPanelLine.add(jLabelRefer, gridBagConstraints);

        jButtonNext.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/forward.gif"))); // NOI18N
        jButtonNext.setToolTipText("����ѵԤ��駶Ѵ�");
        jButtonNext.setMaximumSize(new java.awt.Dimension(32, 32));
        jButtonNext.setMinimumSize(new java.awt.Dimension(32, 32));
        jButtonNext.setPreferredSize(new java.awt.Dimension(32, 32));
        jButtonNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNextActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 14;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 1, 0, 0);
        jPanelLine.add(jButtonNext, gridBagConstraints);

        jButtonPrev.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hospital_os/images/back.gif"))); // NOI18N
        jButtonPrev.setToolTipText("����ѵԤ��駡�͹˹��");
        jButtonPrev.setMaximumSize(new java.awt.Dimension(32, 32));
        jButtonPrev.setMinimumSize(new java.awt.Dimension(32, 32));
        jButtonPrev.setPreferredSize(new java.awt.Dimension(32, 32));
        jButtonPrev.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPrevActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 13;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 1, 0, 0);
        jPanelLine.add(jButtonPrev, gridBagConstraints);

        jLabelAppointment.setFont(jLabelAppointment.getFont().deriveFont(jLabelAppointment.getFont().getStyle() | java.awt.Font.BOLD, jLabelAppointment.getFont().getSize()+2));
        jLabelAppointment.setForeground(new java.awt.Color(0, 51, 255));
        jLabelAppointment.setText("�ѹ �֧�ѹ���Ѵ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        jPanelLine.add(jLabelAppointment, gridBagConstraints);

        btnNote.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/Notes-icon.png"))); // NOI18N
        btnNote.setToolTipText("�ѹ�֡��ͤ���");
        btnNote.setMaximumSize(new java.awt.Dimension(52, 28));
        btnNote.setMinimumSize(new java.awt.Dimension(52, 28));
        btnNote.setPreferredSize(new java.awt.Dimension(52, 28));
        btnNote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNoteActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 1, 0, 0);
        jPanelLine.add(btnNote, gridBagConstraints);

        panelNotification.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 12;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 2);
        jPanelLine.add(panelNotification, gridBagConstraints);

        panelButtonPlugin.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanelLine.add(panelButtonPlugin, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 1, 2);
        add(jPanelLine, gridBagConstraints);

        jPanel1.setLayout(new java.awt.GridBagLayout());

        lblPictureProfile.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPictureProfile.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/no_picture_profile.png"))); // NOI18N
        lblPictureProfile.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        lblPictureProfile.setMaximumSize(new java.awt.Dimension(80, 80));
        lblPictureProfile.setMinimumSize(new java.awt.Dimension(80, 80));
        lblPictureProfile.setPreferredSize(new java.awt.Dimension(80, 80));
        lblPictureProfile.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblPictureProfileMouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel1.add(lblPictureProfile, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        add(jPanel1, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jLabelDischargeMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabelDischargeMouseReleased
        theHD.showDialogDeath();
    }//GEN-LAST:event_jLabelDischargeMouseReleased

    private void jLabelReferMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabelReferMouseReleased
        theHD.showDialogReferIn(theHO.theVisit);
    }//GEN-LAST:event_jLabelReferMouseReleased

    private void jLabelRemainMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabelRemainMouseReleased
        if (theHO.thePatient == null) {
            theUS.setStatus("�ѧ��������͡������", UpdateStatus.WARNING);
            return;
        }
        theHD.showDialogHistoryBilling(theHO.thePatient);
    }//GEN-LAST:event_jLabelRemainMouseReleased

    private void jLabelPersonalDiseaseMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabelPersonalDiseaseMouseReleased
        if (thePPH == null) {
            thePPH = new PanelPatientHistory(theHC, theUS);
        }
        thePPH.showDialog(0);
    }//GEN-LAST:event_jLabelPersonalDiseaseMouseReleased

    private void jLabelPtAllergyValueMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabelPtAllergyValueMouseReleased
        if (thePPH == null) {
            thePPH = new PanelPatientHistory(theHC, theUS);
        }
        thePPH.showDialog(2);
    }//GEN-LAST:event_jLabelPtAllergyValueMouseReleased

    private void jTextFieldHNActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldHNActionPerformed
        setCursor(Constant.CUR_WAIT);

        int i = theHC.thePatientControl.readPatientByHn(jTextFieldHN.getText());
        if (i > 1) {
            theHD.showDialogSearchPatient("", "", jTextFieldHN.getText(), "", "");
        } else if (i == 0) {
            jTextFieldHN.setText("");
        }
        setCursor(Constant.CUR_DEFAULT);
    }//GEN-LAST:event_jTextFieldHNActionPerformed

    private void jButtonNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonNextActionPerformed
        theHC.theVisitControl.readNextVisit(thePatient, theVisit);
    }//GEN-LAST:event_jButtonNextActionPerformed

    private void jButtonPrevActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPrevActionPerformed
        theHC.theVisitControl.readPreviousVisit(thePatient, theVisit);
    }//GEN-LAST:event_jButtonPrevActionPerformed

    private void jComboBoxSendToLocationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxSendToLocationActionPerformed
        ServicePoint sp = (ServicePoint) jComboBoxSendToLocation.getSelectedItem();
        if (sp == null) {
            return;
        }

        if (sp.service_type_id.equals(ServiceType.DIAG)) {
            Vector sendToDoctorV = theHC.theLookupControl.listDoctorSP(sp.getObjectId());
            if (sendToDoctorV != null && !sendToDoctorV.isEmpty() && sendToDoctorV.size() > 1) {
                Employee undefine = new Employee();
                undefine.setObjectId("");
                undefine.person.person_firstname = Constant.getTextBundle("����к�");
                sendToDoctorV.add(0, undefine);
            }
            if (sendToDoctorV == null) {
                sendToDoctorV = new Vector();
            }
            jComboBoxSendToDoctor.setEnabled(true);
            ComboboxModel.initComboBox(jComboBoxSendToDoctor, sendToDoctorV);
        } else {
            jComboBoxSendToDoctor.setEnabled(false);
        }
    }//GEN-LAST:event_jComboBoxSendToLocationActionPerformed

    private void jButtonUnlockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonUnlockActionPerformed
        theHC.theVisitControl.unlockVisit();

    }//GEN-LAST:event_jButtonUnlockActionPerformed

    private void jButtonSendActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSendActionPerformed
        getTransfer(theTransfer);
        theHC.theVisitControl.sendVisit(theTransfer);
    }//GEN-LAST:event_jButtonSendActionPerformed

    private void jTextFieldVNActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldVNActionPerformed
        theHC.theVisitControl.readVisitPatientByVn(jTextFieldVN.getText());
    }//GEN-LAST:event_jTextFieldVNActionPerformed

    private void jComboBoxSendToDoctorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxSendToDoctorActionPerformed
        if (evt.getActionCommand().equals("comboBoxEdited")) {
            ServicePoint sp = (ServicePoint) jComboBoxSendToLocation.getSelectedItem();
            if (sp == null) {
                return;
            }
            String keyword = String.valueOf(jComboBoxSendToDoctor.getSelectedItem());
            if (jComboBoxSendToDoctor.getSelectedIndex() >= 0) {
                Employee employee = (Employee) jComboBoxSendToDoctor.getSelectedItem();
                if (employee.getName().trim().equals(keyword.trim())) {
                    return;
                }
            }
            Vector sendToDoctorV = theHC.theLookupControl.listDoctorSPAndName(sp.getObjectId(), keyword);
            if (sendToDoctorV != null && !sendToDoctorV.isEmpty() && sendToDoctorV.size() > 1) {
                Employee undefine = new Employee();
                undefine.setObjectId("");
                undefine.person.person_firstname = Constant.getTextBundle("����к�");
                if (keyword.trim().isEmpty()) {
                    sendToDoctorV.add(0, undefine);
                }
            }
            if (sendToDoctorV == null) {
                sendToDoctorV = new Vector();
            }
            jComboBoxSendToDoctor.setEnabled(true);
            ComboboxModel.initComboBox(jComboBoxSendToDoctor, sendToDoctorV);
        }
    }//GEN-LAST:event_jComboBoxSendToDoctorActionPerformed

    private void btnNoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNoteActionPerformed
        theHD.showDialogListNotifyNote(theHO.thePatient.hn);
    }//GEN-LAST:event_btnNoteActionPerformed

    private void jLabelAdmitLeaveHomeMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabelAdmitLeaveHomeMouseReleased
        theHD.showLeaveTheHouseDialog(theHO.theVisit);
    }//GEN-LAST:event_jLabelAdmitLeaveHomeMouseReleased

    private void lblInformIntoleranceMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblInformIntoleranceMouseReleased
        theHD.showInformIntoleranceDialog(theHO.thePatient);
    }//GEN-LAST:event_lblInformIntoleranceMouseReleased

    private void lblPictureProfileMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblPictureProfileMouseClicked
        if (evt.getClickCount() >= 2) {
            this.doViewPictureProfile();
        }
    }//GEN-LAST:event_lblPictureProfileMouseClicked

    private void miViewPicProfileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miViewPicProfileActionPerformed
        this.doViewPictureProfile();
    }//GEN-LAST:event_miViewPicProfileActionPerformed

    private void miChangePicProfileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miChangePicProfileActionPerformed
        this.doChangePictureProfile();
    }//GEN-LAST:event_miChangePicProfileActionPerformed

    private void miDeletePicProfileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miDeletePicProfileActionPerformed
        this.doDeletePictureProfile();
    }//GEN-LAST:event_miDeletePicProfileActionPerformed

    private void jLabelPatientAdlMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabelPatientAdlMouseReleased
        if (thePPH == null) {
            thePPH = new PanelPatientHistory(theHC, theUS);
        }
        thePPH.showDialog(3, 0);
    }//GEN-LAST:event_jLabelPatientAdlMouseReleased

    private void jLabelPatient2qPlusMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabelPatient2qPlusMouseReleased
        if (thePPH == null) {
            thePPH = new PanelPatientHistory(theHC, theUS);
        }
        thePPH.showDialog(3, 1);
    }//GEN-LAST:event_jLabelPatient2qPlusMouseReleased
    /*
     * //���¡ function ���Ҽ����¨ҡ�Ţ HN �ҡ��� notify ��ѧ panel ��ҧ�
     * //�óշ�辺�����ҡ���� 1 ��������Т��˹�Ҩͤ��Ҽ����������
     */
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnNote;
    private javax.swing.JButton jButtonNext;
    private javax.swing.JButton jButtonPrev;
    private javax.swing.JButton jButtonSend;
    private javax.swing.JButton jButtonUnlock;
    private javax.swing.JComboBox jComboBoxSendToDoctor;
    private javax.swing.JComboBox jComboBoxSendToLocation;
    private javax.swing.JLabel jLabelAN;
    private javax.swing.JLabel jLabelAdmitLeaveHome;
    private javax.swing.JLabel jLabelAge;
    private javax.swing.JLabel jLabelAppointment;
    private javax.swing.JLabel jLabelDischarge;
    private javax.swing.JLabel jLabelDoctor;
    private javax.swing.JLabel jLabelDx;
    private javax.swing.JLabel jLabelDxLbl;
    private javax.swing.JLabel jLabelHN;
    private javax.swing.JLabel jLabelLastLocation;
    private javax.swing.JLabel jLabelLastLocationValue;
    private javax.swing.JLabel jLabelLocation;
    private javax.swing.JLabel jLabelLocationValue;
    private javax.swing.JLabel jLabelLockUser;
    private javax.swing.JLabel jLabelNCDCode;
    private javax.swing.JLabel jLabelPatient2qPlus;
    private javax.swing.JLabel jLabelPatientAdl;
    private javax.swing.JLabel jLabelPatientNameValue;
    private javax.swing.JLabel jLabelPersonalDisease;
    private javax.swing.JLabel jLabelPlan;
    private javax.swing.JLabel jLabelPlanLbl;
    private javax.swing.JLabel jLabelPtAllergyValue;
    private javax.swing.JLabel jLabelRefer;
    private javax.swing.JLabel jLabelRemain;
    private javax.swing.JLabel jLabelReturnDrug;
    private javax.swing.JLabel jLabelSendToDoctor;
    private javax.swing.JLabel jLabelSendToLocation;
    private javax.swing.JLabel jLabelVN;
    private javax.swing.JLabel jLabelVisitDate;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanelLine;
    private javax.swing.JPanel jPanelPatientInfo1;
    private javax.swing.JPanel jPanelPatientInfo2;
    private javax.swing.JLabel jTextFieldDBilling;
    private javax.swing.JLabel jTextFieldDDoctor;
    private javax.swing.JTextField jTextFieldHN;
    private javax.swing.JTextField jTextFieldVN;
    private javax.swing.JLabel lblBed;
    private javax.swing.JLabel lblBedNumber;
    private javax.swing.JLabel lblInformIntolerance;
    private javax.swing.JLabel lblOPDIPD;
    private javax.swing.JLabel lblPictureProfile;
    private javax.swing.JLabel lblWard;
    private javax.swing.JLabel lblWardName;
    private javax.swing.JMenuItem miChangePicProfile;
    private javax.swing.JMenuItem miDeletePicProfile;
    private javax.swing.JMenuItem miViewPicProfile;
    private javax.swing.JPanel panelButtonPlugin;
    private javax.swing.JPanel panelNotification;
    private javax.swing.JPanel panelVisitBedInfo;
    private javax.swing.JPopupMenu popupMenuPicProfile;
    // End of variables declaration//GEN-END:variables

    private void setHosObject(HosObject ho) {
        theHO = ho;
        setFamily(theHO.theFamily);
        setPatient(theHO.thePatient);
        setVisit(theHO.theVisit);
        boolean res = setPaymentV(theHO.vVisitPayment);
        if (!res) {
            setPatientPaymentV(theHO.vPatientPayment);
        }
        setTransferV(theHO.vTransfer);
        setAllergyV(theHO.vDrugAllergy);
        setPersonalDiseaseV(theHO.vPersonalDisease);
        setPatientAdl(theHO.thePatientAdl);
        setPatient2qPlus(theHO.thePatient2qPlus);
        setInformIntolerance();
        jButtonPrev.setEnabled(true);
        jButtonNext.setEnabled(true);
        //���ͫ�͹���ͼ�����㹡ó����Ż���Դ
        if (theHO.theVisit != null
                && theHO.orderSecret != null
                && !"".equals(theHO.orderSecret)) {
            lblOPDIPD.setText(" *** ");
            jLabelDx.setText("*********");
            jTextFieldHN.setText("*********");//�ѹ���ʴ��繪�ͧ��ҧ᷹ �����ѹ���Ѻ੾�е���Ţ��ҹ��
            jTextFieldVN.setText("*********");
            jLabelPatientNameValue.setText(" *** " + theHO.specimenCode);
            jLabelAge.setText(Constant.getTextBundle("���� *** �� "));
            jLabelPlan.setText("*********");
            jLabelDischarge.setVisible(false);
            jLabelRemain.setVisible(false);
            jLabelDoctor.setVisible(false);
            this.jLabelNCDCode.setText("");
            this.jLabelPlan.setText("");
            if (theHO.isQueueLab) {
                setPictureProfile(null);
                jButtonPrev.setEnabled(false);
                jButtonNext.setEnabled(false);
            }
        }
        this.serviceLoader();
    }

    @Override
    public void notifyManageDrugAllergy(String str, int status) {
        setAllergyV(theHO.vDrugAllergy);
        setPersonalDiseaseV(theHO.vPersonalDisease);
    }

    @Override
    public void notifyReadVisit(String str, int status) {
        setHosObject(theHO);
        btnNote.setEnabled(true);
        if (theHO.theVisit != null) {
            if (theHO.theEmployee.authentication_id.equals(Authentication.REGIST)) {
                theHD.showDialogListNotifyNote(theHO.thePatient.hn, theHO.theVisit.getObjectId(), 1);
            } else {
                theHD.showDialogListNotifyNote(theHO.thePatient.hn, theHO.theVisit.getObjectId(), 3);
            }
        }
    }

    @Override
    public void notifyUnlockVisit(String str, int status) {
        setHosObject(theHO);
        this.jTextFieldHN.requestFocus();
        btnNote.setEnabled(false);
    }

    @Override
    public void notifyVisitPatient(String str, int status) {
        setHosObject(theHO);
        btnNote.setEnabled(true);
        // Somprasong 19-04-2013 ������õ�Ǩ�ͺ��� notify ��ͧ����Ҩҡ��ú�·֡ NCD
        if (!"NCD".equals(str) && theHO.theVisit != null) {
            theHD.showDialogListNotifyNote(theHO.thePatient.hn, theHO.theVisit.getObjectId(), 2);
        }
    }

    @Override
    public void notifyAdmitVisit(String str, int status) {
        setHosObject(theHO);
    }

    @Override
    public void notifyDischargeDoctor(String str, int status) {
        setHosObject(theHO);
    }

    public void notifyAddItemDrugAllergy(String str, int status) {
    }

    @Override
    public void notifyDropVisit(String str, int status) {
        setHosObject(theHO);
        btnNote.setEnabled(false);
    }

    @Override
    public void notifySendVisit(String str, int status) {
        setHosObject(theHO);
        btnNote.setEnabled(false);
    }

    @Override
    public void notifyDischargeFinancial(String str, int status) {
        setHosObject(theHO);
        btnNote.setEnabled(false);
    }

    @Override
    public void notifyReverseFinancial(String str, int status) {
        setVisit(theHO.theVisit);
    }

    @Override
    public void notifyReverseDoctor(String str, int status) {
        setVisit(theHO.theVisit);
    }

    @Override
    public void notifyObservVisit(String str, int status) {
        setVisit(theHO.theVisit);
    }

    @Override
    public void notifyCheckDoctorTreament(String str, int status) {
    }

    @Override
    public void notifySavePatientPayment(String str, int status) {
    }

    @Override
    public void notifyDeletePatientPayment(String str, int status) {
    }

    @Override
    public void notifyDeleteVisitPayment(String str, int status) {
    }

    @Override
    public void notifyAddDxTemplate(String str, int status) {
    }

    @Override
    public void notifyReadPatient(String str, int status) {
        setHosObject(theHO);
        btnNote.setEnabled(theHO.thePatient != null);
        if (theHO.theEmployee.authentication_id.equals(Authentication.REGIST)
                && theHO.thePatient != null) {
            theHD.showDialogListNotifyNote(theHO.thePatient.hn, "", 1);
        } else if (theHO.theVisit != null) {
            theHD.showDialogListNotifyNote(theHO.thePatient.hn, theHO.theVisit.getObjectId(), 3);
        }
    }

    @Override
    public void notifyReadFamily(String str, int status) {
        setHosObject(theHO);
        btnNote.setEnabled(theHO.theFamily != null);
    }

    @Override
    public void notifySavePatient(String str, int status) {
        setHosObject(theHO);
        btnNote.setEnabled(theHO.thePatient != null);
    }

    @Override
    public void notifyDeletePatient(String str, int status) {
        setFamily(null);
        setPatient(null);
        setAllergyV(null);
        setPersonalDiseaseV(null);
        setPatientAdl(null);
        setPatient2qPlus(null);
        btnNote.setEnabled(false);
    }

    @Override
    public void notifyAddPrimarySymptom(String str, int status) {
    }

    @Override
    public void notifyDeleteMapVisitDxByVisitId(String str, int status) {
    }

    @Override
    public void notifyManagePhysicalExam(String str, int status) {
    }

    @Override
    public void notifyManagePrimarySymptom(String str, int status) {
    }

    @Override
    public void notifyManageVitalSign(String str, int status) {
    }

    @Override
    public void notifySaveDiagDoctor(String str, int status) {
        jLabelDx.setText(Gutil.getTextLabel(theHO.theVisit.doctor_dx));
        jLabelDx.setToolTipText(Gutil.getToolTipText(theHO.theVisit.doctor_dx));
    }

    @Override
    public void notifySendVisitBackWard(String str, int status) {
        setHosObject(theHO);
    }

    @Override
    public void notifyRemainDoctorDischarge(String str, int status) {
        setHosObject(theHO);
    }

    @Override
    public void notifyCancelOrderItem(String str, int status) {
    }

    @Override
    public void notifyCheckAutoOrder(String str, int status) {
    }

    @Override
    public void notifyContinueOrderItem(String str, int status) {
    }

    @Override
    public void notifyDispenseOrderItem(String str, int status) {
        setHosObject(theHO);
    }

    @Override
    public void notifyDoctorOffDrug(String DoctorId, int status) {
    }

    @Override
    public void notifyExecuteOrderItem(String str, int status) {
    }

    @Override
    public void notifyReceiveReturnDrug(String str, int status) {
        setVisit(theHO.theVisit);
    }

    @Override
    public void notifyReferOutLab(String msg, int status) {
    }

    @Override
    public void notifySaveOrderItem(String str, int status) {
    }

    @Override
    public void notifySaveOrderItemInLab(String str, int status) {
    }

    @Override
    public void notifySaveReturnDrug(String str, int status) {
        setVisit(theHO.theVisit);
    }

    @Override
    public void notifyVerifyOrderItem(String str, int status) {
    }

    @Override
    public void notifyBillingInvoice(String str, int status) {
    }

    @Override
    public void notifyCalculateAllBillingInvoice(String str, int status) {
    }

    @Override
    public void notifyCancelBill(String str, int status) {
        setHosObject(theHO);
    }

    @Override
    public void notifyCancelBillingInvoice(String str, int status) {
    }

    @Override
    public void notifyPatientPaidMoney(String str, int status) {
        setHosObject(theHO);
    }

    @Override
    public void notifyDeleteFilmXray(String str, int status) {
    }

    @Override
    public void notifyDeleteLabOrder(String str, int status) {
    }

    @Override
    public void notifyDeleteLabResult(String str, int status) {
    }

    @Override
    public void notifyDeleteXrayPosition(String str, int status) {
    }

    @Override
    public void notifyManagePatientLabReferIn(String str, int status) {
    }

    @Override
    public void notifyReportResultLab(String str, int status) {
        setHosObject(theHO);
    }

    @Override
    public void notifySaveFilmXray(String str, int status) {
    }

    @Override
    public void notifySaveLabResult(String str, int status) {
    }

    @Override
    public void notifySaveResultXray(String str, int status) {
    }

    @Override
    public void notifySaveXrayPosition(String str, int status) {
    }

    @Override
    public void notifyXrayReportComplete(String str, int status) {
        setHosObject(theHO);
    }

    @Override
    public void notifyDeleteResultXray(String str, int status) {
    }

    @Override
    public void notifySaveOrderRequest(String str, int status) {
    }

    @Override
    public void notifySaveAppointment(String str, int status) {
    }

    @Override
    public void notifyAddLabReferOut(String str, int status) {
    }

    @Override
    public void notifyAddLabReferIn(String str, int status) {
    }

    @Override
    public void notifyReverseAdmit(String str, int status) {
        setVisit(theHO.theVisit);
    }

    @Override
    public void notifyResetPatient(String str, int status) {
        setHosObject(theHO);
    }

    @Override
    public void notifySaveRemainLabResult(String str, int status) {
        setHosObject(theHO);
    }

    @Override
    public void notifySendResultLab(String str, int status) {
        setHosObject(theHO);
    }

    @Override
    public void notifyDeleteVPayment(String str, int status) {
        setPaymentV(theHO.vVisitPayment);
    }

    @Override
    public void notifyDownVPaymentPriority(String str, int status) {
        setPaymentV(theHO.vVisitPayment);
    }

    @Override
    public void notifySaveVPayment(String str, int status) {
        setPaymentV(theHO.vVisitPayment);
    }

    @Override
    public void notifyUpVPaymentPriority(String str, int status) {
        setPaymentV(theHO.vVisitPayment);
    }

    @Override
    public void notifyDeleteQueueLab(String str, int status) {
        setHosObject(theHO);
    }

    @Override
    public void notifyAdmitLeaveHome(String str, int status) {
        setVisit(theHO.theVisit);
    }

    public void setLanguage(String str) {
        GuiLang.setLanguage(jButtonSend);
        GuiLang.setLanguage(jButtonUnlock);
        GuiLang.setLanguage(jLabelAge);
        GuiLang.setLanguage(jLabelAN);
        GuiLang.setLanguage(jLabelDischarge);
        GuiLang.setLanguage(jLabelDx);
        GuiLang.setLanguage(jLabelDxLbl);
        GuiLang.setLanguage(jLabelHN);
        GuiLang.setLanguage(jLabelLastLocation);
        GuiLang.setLanguage(jLabelLastLocationValue);
        GuiLang.setLanguage(jLabelLocation);
        GuiLang.setLanguage(jLabelLocationValue);
        GuiLang.setLanguage(jLabelLockUser);
        GuiLang.setLanguage(jLabelPatientNameValue);
        GuiLang.setLanguage(jLabelPlan);
        GuiLang.setLanguage(jLabelPlanLbl);
        GuiLang.setLanguage(jLabelPtAllergyValue);
        GuiLang.setLanguage(jLabelRemain);
        GuiLang.setLanguage(jLabelReturnDrug);
        GuiLang.setLanguage(jLabelSendToDoctor);
        GuiLang.setLanguage(jLabelSendToLocation);
        GuiLang.setLanguage(jLabelVN);
        GuiLang.setLanguage(jLabelRemain);
        GuiLang.setLanguage(jButtonNext);
        GuiLang.setLanguage(jButtonPrev);
        Font font = jLabelPatientNameValue.getFont();
        jLabelPatientNameValue.setFont(new Font(font.getFontName(), Font.BOLD, font.getSize()));
    }

    @Override
    public void notifySaveInformIntolerance(Object... object) {
        setInformIntolerance();
    }

    @Override
    public void notifyUpdatePatientPictureProfile(Object obj) {
        if (thePatient != null
                && thePatient.getFamily().pictureProfile != null
                && thePatient.getFamily().pictureProfile.picture_profile != null
                && thePatient.getFamily().pictureProfile.picture_profile.length != 0) {
            BufferedImage image = ImageUtils.toBufferedImage(
                    thePatient.getFamily().pictureProfile.picture_profile
            );
            setPictureProfile(image);
        } else {
            setPictureProfile(null);
        }
    }

    @Override
    public void add(String key, JComponent component) {
        if (key != null && component != null) {
            mapNotify.put(key, component);
            GridBagConstraints gridBagConstraints = new java.awt.GridBagConstraints();
            gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
            gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
            gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 3);
            panelNotification.add(component, gridBagConstraints);
            update();
        }
    }

    @Override
    public void remove(String key) {
        JComponent component = mapNotify.get(key);
        if (component != null) {
            panelNotification.remove(component);
            update();
            mapNotify.remove(key);
        }
    }

    @Override
    public void update() {
        panelNotification.revalidate();
        panelNotification.repaint();
    }

    @Override
    public void show(String key) {
        JComponent component = mapNotify.get(key);
        if (component != null) {
            component.setVisible(true);
        }
    }

    @Override
    public void hide(String key) {
        JComponent component = mapNotify.get(key);
        if (component != null) {
            component.setVisible(false);
        }
    }

    private void doChangePictureProfile() {
        if (thePatient != null) {
            if (theHO.theGActionAuthV.isWriteEditPatientPictureProfile()) {
                BufferedImage image = theHD.doOpenWebcamSnapshotDialog(thePatient);
                if (image != null) {
                    if (thePatient.getFamily().pictureProfile == null) {
                        thePatient.getFamily().pictureProfile = new PictureProfile();
                        thePatient.getFamily().pictureProfile.t_person_id = thePatient.getFamily().getObjectId();
                    }
                    thePatient.getFamily().pictureProfile.picture_profile = ImageUtils.toByteArray(image);
                    theHC.thePatientControl.updatePatientPictureProfile(thePatient.getFamily().pictureProfile);
                }
            } else {
                theUS.setStatus("������Է�� ����/ź ���Ҿ������", UpdateStatus.WARNING);
            }
        }
    }

    private void setPictureProfile(BufferedImage image) {
        if (image == null) {
            lblPictureProfile.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/no_picture_profile.png")));
        } else {
            byte[] imageInByte = ImageUtils.toByteArray(ImageUtils.fit(image, 80, 80));
            if (imageInByte != null) {
                lblPictureProfile.setIcon(new ImageIcon(imageInByte));
            }
        }
    }

    private void doDeletePictureProfile() {
        if (thePatient != null) {
            if (theHO.theGActionAuthV.isWriteEditPatientPictureProfile()) {
                thePatient.getFamily().pictureProfile.picture_profile = null;
                if (1 == theHC.thePatientControl.updatePatientPictureProfile(thePatient.getFamily().pictureProfile)) {
                    setPictureProfile(null);
                }
            } else {
                theUS.setStatus("������Է�� ����/ź ���Ҿ������", UpdateStatus.WARNING);
            }
        }

    }

    private void doViewPictureProfile() {
        if (thePatient != null
                && thePatient.getFamily().pictureProfile != null
                && thePatient.getFamily().pictureProfile.picture_profile != null
                && thePatient.getFamily().pictureProfile.picture_profile.length != 0) {
            if (theHO.theGActionAuthV.isReadViewPatientPictureProfile()) {
                JLabel label = new JLabel(new ImageIcon(
                        thePatient.getFamily().pictureProfile.picture_profile
                ));
                JOptionPane.showMessageDialog(theUS.getJFrame(), label, "Preview",
                        JOptionPane.PLAIN_MESSAGE);
            } else {
                theUS.setStatus("������Է�Դ��Ҿ������", UpdateStatus.WARNING);
            }
        }
    }

    @Override
    public void notifyLoaderService() {
        this.serviceLoader();
    }

    private final List<PanelButtonPluginProvider> buttonPluginProviders = new ArrayList<>();

    public List<PanelButtonPluginProvider> getbuttonPluginProviders() {
        return buttonPluginProviders;
    }

    private void serviceLoader() {
        panelButtonPlugin.removeAll();
        Iterator<PanelButtonPluginProvider> iterator = PanelButtonPluginManager.getInstance().getServices();
        String className = this.getClass().getName();
        // get buttons to list
        List<JButton> buttons = new ArrayList<>();
        while (iterator.hasNext()) {
            PanelButtonPluginProvider provider = iterator.next();
            if (provider.isOwnerPanel(PanelCurrentVisit.class)) {
                JButton button = provider.getButton(PanelCurrentVisit.class);
                boolean isVisible = provider.isVisible();
                LOG.log(java.util.logging.Level.INFO, "{0} get button from {1}, visible: {2}", new Object[]{className, provider.getClass().getName(), isVisible});
                if (!isVisible) {
                    continue;
                }
                // ��ͤ��� ���Ҩ����¹������� visit �֧��ͧ����¹�ա�ͺ
                button.setText(provider.getButtonText());
                button.setToolTipText(provider.getButtonTooltip());
                button.setIcon(provider.getIcon());
                button.setForeground(provider.getButtonTextColor());
                button.setVisible(isVisible);
                button.setEnabled(true);
                button.setMaximumSize(new Dimension(provider.getButtonWidth(), provider.getButtonHeight()));
                button.setMinimumSize(new Dimension(provider.getButtonWidth(), provider.getButtonHeight()));
                button.setPreferredSize(new Dimension(provider.getButtonWidth(), provider.getButtonHeight()));
                buttonPluginProviders.add(provider);
                int index = provider.getIndex();
                if (index > buttons.size()) {
                    buttons.add(button);
                } else {
                    buttons.add(index, button);
                }
            }
        }
        if (buttons.isEmpty()) {
            LOG.log(java.util.logging.Level.INFO, "{0}  no button plugin!", className);
        } else {
            LOG.log(java.util.logging.Level.INFO, "{0}  found {1} buttons plugin!", new Object[]{className, buttons.size()});
        }
        // add all plugin buttons to panel
        buttons.forEach((button) -> {
            GridBagConstraints gridBagConstraints = new java.awt.GridBagConstraints();
            gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
            gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
            gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 3);
            panelButtonPlugin.add(button, gridBagConstraints);
        });
        panelButtonPlugin.revalidate();
        panelButtonPlugin.repaint();
    }

    @Override
    public void notifySaveDxNote(String str, int status) {

    }

    @Override
    public void notifySaveAdlAnd2QPlus(boolean isNew, String str) {
        setPatientAdl(theHO.thePatientAdl);
        setPatient2qPlus(theHO.thePatient2qPlus);
    }
}

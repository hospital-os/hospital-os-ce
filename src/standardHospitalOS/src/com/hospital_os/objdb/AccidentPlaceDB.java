/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.AccidentPlace;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class AccidentPlaceDB {

    private final ConnectionInf connectionInf;

    public AccidentPlaceDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public List<AccidentPlace> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_accident_place";
            preparedStatement = connectionInf.ePQuery(sql);
            
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<AccidentPlace> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<AccidentPlace> list = new ArrayList<AccidentPlace>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                AccidentPlace obj = new AccidentPlace();
                obj.setObjectId(rs.getString("f_accident_place_id"));
                obj.description = rs.getString("f_accident_place_description");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (AccidentPlace obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }
}

ALTER TABLE public.t_patient ADD patient_firstname_eng character varying(255) NULL;
ALTER TABLE public.t_patient ADD patient_lastname_eng character varying(255) NULL;
ALTER TABLE public.t_patient ADD patient_postcode character varying(5) NULL;
ALTER TABLE public.t_patient ADD patient_contact_postcode character varying(5) NULL;

ALTER TABLE public.t_health_family ADD skin_color varchar(255) NULL;

ALTER TABLE public.t_person_foreigner ADD employer_address_house varchar(255) NULL;
ALTER TABLE public.t_person_foreigner ADD employer_address_moo varchar(255) NULL;
ALTER TABLE public.t_person_foreigner ADD employer_address_road varchar(255) NULL;
ALTER TABLE public.t_person_foreigner ADD employer_address_tambon varchar(255) NULL;
ALTER TABLE public.t_person_foreigner ADD employer_address_amphur varchar(255) NULL;
ALTER TABLE public.t_person_foreigner ADD employer_address_changwat varchar(255) NULL;
ALTER TABLE public.t_person_foreigner ADD employer_address_postcode varchar(255) NULL;
ALTER TABLE public.t_person_foreigner ADD employer_contact_phone_number varchar(255) NULL;
ALTER TABLE public.t_person_foreigner ADD employer_contact_mobile_phone_number varchar(255) NULL;

-- update db version
INSERT INTO s_version VALUES ('9701000000087', '87', 'Hospital OS, Community Edition', '3.9.54', '3.34.0', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_53.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.54');
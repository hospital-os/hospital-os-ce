/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.LabOrderCause;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class LabOrderCauseDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "808";

    public LabOrderCauseDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public int insert(LabOrderCause obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO b_lab_order_cause(\n"
                    + "            b_lab_order_cause_id, code, description, active, user_record_id, user_update_id)\n"
                    + "    VALUES (?, ?, ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getGenID(tableId));
            preparedStatement.setString(2, obj.code);
            preparedStatement.setString(3, obj.description);
            preparedStatement.setBoolean(4, obj.active.equals("1"));
            preparedStatement.setString(5, obj.user_record_id);
            preparedStatement.setString(6, obj.user_update_id);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(LabOrderCause obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE b_lab_order_cause\n");
            sql.append("   SET code=?, description=?, \n");
            sql.append("       active=?, user_update_id=?, update_date_time=current_timestamp\n");
            sql.append(" WHERE b_lab_order_cause_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.code);
            preparedStatement.setString(2, obj.description);
            preparedStatement.setBoolean(3, obj.active.equals("1"));
            preparedStatement.setString(4, obj.user_update_id);
            preparedStatement.setString(5, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(LabOrderCause obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM b_lab_order_cause\n");
            sql.append(" WHERE b_lab_order_cause_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int inactive(LabOrderCause obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE b_lab_order_cause\n");
            sql.append("   SET active=false, user_update_id=?, update_date_time=current_timestamp\n");
            sql.append(" WHERE b_lab_order_cause_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.user_update_id);
            preparedStatement.setString(2, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public LabOrderCause select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_lab_order_cause where b_lab_order_cause_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<LabOrderCause> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public LabOrderCause selectByCode(String code) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_lab_order_cause where code = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, code);
            List<LabOrderCause> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<LabOrderCause> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_lab_order_cause where active = '1' order by code::int";
            preparedStatement = connectionInf.ePQuery(sql);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<LabOrderCause> listByKeyword(String keyword, String active) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_lab_order_cause where active = ? and (upper(code) like upper(?) or upper(description) like upper(?)) order by code::int";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setBoolean(1, active.equals("1"));
            preparedStatement.setString(2, "%" + (keyword == null || keyword.isEmpty() ? "" : keyword) + "%");
            preparedStatement.setString(3, "%" + (keyword == null || keyword.isEmpty() ? "" : keyword) + "%");
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<LabOrderCause> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<LabOrderCause> list = new ArrayList<>();
        try (ResultSet rs = preparedStatement.executeQuery()) {
            // execute SQL stetement
            while (rs.next()) {
                LabOrderCause obj = new LabOrderCause();
                obj.setObjectId(rs.getString("b_lab_order_cause_id"));
                obj.code = rs.getString("code");
                obj.description = rs.getString("description");
                obj.active = rs.getBoolean("active") ? "1" : "0";
                obj.user_record_id = rs.getString("user_record_id");
                obj.record_date_time = rs.getDate("record_date_time");
                obj.user_update_id = rs.getString("user_update_id");
                obj.update_date_time = rs.getDate("update_date_time");
                list.add(obj);
            }
            return list;
        }
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<>();
        for (LabOrderCause obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }
}

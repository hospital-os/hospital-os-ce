/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
public class FootResult extends Persistent {

    public String t_visit_id = "";
    public String f_foot_screen_result_id = "";
    public String appoint_date = "";
    public String assessor = "";
    public String assessor_date = "";
    public String active = "1";
    public String record_date_time = "";
    public String user_record_id = "";
    public String update_date_time = "";
    public String user_update_id = "";
}

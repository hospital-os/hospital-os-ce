/*Source file: E:\\Job\\DrugReportSep\\code\\com\\hospital_os\\control\\PrescribeDB.java
 */
package com.hospital_os.objdb;

import com.hospital_os.object.*;
import com.hospital_os.usecase.connection.*;
import java.sql.*;
import java.util.*;

@SuppressWarnings("UseOfObsoleteCollectionType")
public class MapVisitDxDB {

    public ConnectionInf theConnectionInf;
    public MapVisitDx dbObj;
    String strSql = null;
    ResultSet rs = null;
    // MapVisitDx tempMapVisitDx = null;
    int countrow = 0;
    boolean result = false;
    Vector vcValue = null;
    final public String idtable = "183";/*"226";
     */


    /**
     * @param ConnectionInf db
     * @roseuid 3F65897F0326
     */
    public MapVisitDxDB(ConnectionInf db) {
        theConnectionInf = db;
        dbObj = new MapVisitDx();
        initConfig();
    }

    private boolean initConfig() {
        dbObj.table = "t_visit_diag_map";
        dbObj.visit_id = "t_visit_id";
        dbObj.pk_field = "t_visit_diag_map_id";

        dbObj.dx_template_id = "b_template_dx_id";
        /**
         * ������ѡ�ͧ���ҧ
         */
        //dbObj.t_visit_diag_map_id  = "t_visit_diag_map_id";
        /**
         * Dx
         */
        dbObj.visit_diag_map_dx = "visit_diag_map_dx";
        /**
         * ���� ICD10 �ͧ��� map (�����)
         */
        dbObj.visit_diag_map_icd = "visit_diag_map_icd";
        /**
         * ���ʢͧ���ŧ Dx (���繷�駾�Һ�����ᾷ��)
         */
        dbObj.visit_diag_map_staff = "visit_diag_map_staff";
        /**
         * ���ҷ��ŧ
         */
        dbObj.visit_diag_map_date_time = "visit_diag_map_date_time";

        /**
         * ���ʢͧ���ҧ t_patient
         */
        dbObj.t_patient_id = "t_patient_id";
        /**
         * �óշ��١¡��ԡ ���Ͷ١ź������¹�� 0
         */
        dbObj.visit_diag_map_active = "visit_diag_map_active";
        dbObj.visit_diag_map_staff_cancel = "visit_diag_map_staff_cancel";
        dbObj.visit_diag_map_cancel_date_time = "visit_diag_map_cancel_date_time";

        return true;
    }

    /**
     * @param cmd
     * @param o
     * @return int
     * @roseuid 3F6574DE0394
     */
    public int insert(MapVisitDx p) throws Exception {

        p.generateOID(idtable);
        String sql = "insert into t_visit_diag_map ("
                + "               t_visit_diag_map_id, t_visit_id , b_template_dx_id"
                + "              ,t_patient_id, visit_diag_map_active"
                + "              ,visit_diag_map_dx, visit_diag_map_icd"
                + "              ,visit_diag_map_staff, visit_diag_map_date_time"
                + "              ,visit_diag_map_staff_cancel, visit_diag_map_cancel_date_time)"
                + "       values (?, ?, ?, "
                + "               ?, ?, "
                + "               ?, ?, "
                + "               ?, ?,"
                + "               ?, ?)";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, p.getObjectId());
            ePQuery.setString(index++, p.visit_id);
            ePQuery.setString(index++, p.dx_template_id);
            ePQuery.setString(index++, p.t_patient_id);
            ePQuery.setString(index++, p.visit_diag_map_active);
            ePQuery.setString(index++, p.visit_diag_map_dx);
            ePQuery.setString(index++, p.visit_diag_map_icd);
            ePQuery.setString(index++, p.visit_diag_map_staff);
            ePQuery.setString(index++, p.visit_diag_map_date_time);
            ePQuery.setString(index++, p.visit_diag_map_staff_cancel);
            ePQuery.setString(index++, p.visit_diag_map_cancel_date_time);
            return ePQuery.executeUpdate();
        }
    }

    /*////////////////////////////////////////////////////////////////////////////
     */
    public int update(MapVisitDx p) throws Exception {
        String sql = "update t_visit_diag_map"
                + "      set t_visit_id=?, b_template_dx_id=?, t_patient_id=?"
                + "         ,visit_diag_map_active=?"
                + "         ,visit_diag_map_dx=?, visit_diag_map_icd=?"
                + "         ,visit_diag_map_staff_cancel=?, visit_diag_map_cancel_date_time=?"
                + "    where t_visit_diag_map_id=?";

        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, p.visit_id);
            ePQuery.setString(index++, p.dx_template_id);
            ePQuery.setString(index++, p.t_patient_id);
            ePQuery.setString(index++, p.visit_diag_map_active);
            ePQuery.setString(index++, p.visit_diag_map_dx);
            ePQuery.setString(index++, p.visit_diag_map_icd);
            ePQuery.setString(index++, p.visit_diag_map_staff_cancel);
            ePQuery.setString(index++, p.visit_diag_map_cancel_date_time);
            ePQuery.setString(index++, p.getObjectId());
            return ePQuery.executeUpdate();
        }
    }

    /**
     * ��㹡���Ң����ŷ���ա�� map dx �ҡ���ҧ �� �����Ţ visit_id ��� ʶҹ�
     *
     * @param visit_id �� String �ͧ ������ѡ�ͧ���ҧ t_visit
     * @param active �� String �ͧ ��� ���ҷ���ͧ��� ����� 1 �����੾�з��
     * active ����� 0 �����੾�з�� inactive
     * @return �� Vector �ͧ Object MapVisitDx
     * @auther padungrat(tong)
     * @date 23/03/49
     */
    public Vector selectMapVisitDxByVisitID(String visit_id, String active) throws Exception {
        String sql = "SELECT t_visit_diag_map.*,b_template_dx.template_dx_thaidescription \n"
                + "FROM t_visit_diag_map \n"
                + "left join b_template_dx on b_template_dx.b_template_dx_id=t_visit_diag_map.b_template_dx_id \n"
                + "WHERE t_visit_id=? AND visit_diag_map_active=? \n"
                + "order by text_to_timestamp(t_visit_diag_map.visit_diag_map_date_time) ";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, visit_id);
            ePQuery.setString(index++, active);
            return eQuery(ePQuery.toString());
        }
    }

    /**
     * ��㹡�� update ʶҹ������ active 1 ���� 0 ������͹䢷���˹� ���
     * key_id �ͧ���ҧ
     *
     * @param key_id �� String ��������ѡ�ͧ���ҧ t_map_visit_dx
     * @param active �� String ��㹡�á�˹������ 1 ��� ��ҹ, 0 ���
     * �����ҹ
     * @param employee_id �� String ��������ѡ�ͧ ���¡��ԡ
     * @param cancel_time �� String �����ҷ��¡��ԡ
     * @return �� int
     * @author padungrat(tong)
     * @date 23/03/49
     */
    public int updateActive(String key_id, String active, String employee_id, String cancel_time) throws Exception {
        strSql = "UPDATE " + dbObj.table + " SET "
                + " " + dbObj.visit_diag_map_active + " ='" + active + "'";
        if (employee_id != null && employee_id.trim().length() > 0) {
            strSql = strSql + ", " + dbObj.visit_diag_map_staff_cancel + "='" + employee_id + "'"
                    + "," + dbObj.visit_diag_map_cancel_date_time + "='" + cancel_time + "'";
        }
        strSql = strSql + " WHERE " + dbObj.pk_field + " ='" + key_id + "'";
        return theConnectionInf.eUpdate(strSql);
    }

    /**
     * ��㹡�� update ʶҹ������ active 1 ���� 0 ��� ���͹䢷���˹�
     *
     * @param visit_id �� String ��������ѡ�ͧ���ҧ t_visit
     * @param b_template_dx_id �� String ��������ѡ�ͧ���ҧ b_template_dx
     * @param active �� String ��㹡�á�˹������ 1 ��ҹ, 0 �����ҹ
     * @param employee_id �� String ��������ѡ�ͧ ���¡��ԡ
     * @param cancel_time �� String �����ҷ��¡��ԡ
     * @return �� String �ͧ Guide_id ���㹡��ź�����Ť��й�
     * @author padungrat(tong)
     * @modify sumo
     * @date 10/08/49
     */
    public String updateActive(String visit_id, String b_template_dx_id, String active, String employee_id, String cancel_time) throws Exception {
        return updateActive(visit_id, b_template_dx_id, active, employee_id, cancel_time, "");
    }

    /**
     * ��㹡�� update ʶҹ������ active 1 ���� 0 ��� ���͹䢷���˹�
     *
     * @param visit_id �� String ��������ѡ�ͧ���ҧ t_visit
     * @param b_template_dx_id �� String ��������ѡ�ͧ���ҧ b_template_dx
     * @param active �� String ��㹡�á�˹������ 1 ��ҹ, 0 �����ҹ
     * @param employee_id �� String ��������ѡ�ͧ ���¡��ԡ
     * @param cancel_time �� String �����ҷ��¡��ԡ
     * @param code �� String �� Code �����ҹ���¡��ԡ
     * @author padungrat(tong)
     * @modify sumo
     * @date 10/08/49 not
     * @deprecated henbe �Ѻ��͹�Թ�����Ѻ package db
     */
    public String updateActive(String visit_id, String b_template_dx_id, String active, String employee_id, String cancel_time, String code) throws Exception {

        String sql1 = " WHERE " + dbObj.visit_id + " ='" + visit_id + "' ";
        if (b_template_dx_id != null && b_template_dx_id.trim().length() > 0) {
            sql1 = sql1 + " AND " + dbObj.dx_template_id + " ='" + b_template_dx_id + "'";
        }
        if (code != null && code.trim().length() > 0) {
            sql1 = sql1 + " AND " + dbObj.visit_diag_map_icd + " ='" + code + "'";
        }

        strSql = "UPDATE " + dbObj.table + " SET "
                + " " + dbObj.visit_diag_map_active + " ='" + active + "'";
        if (employee_id != null && employee_id.trim().length() > 0) {
            strSql = strSql + ", " + dbObj.visit_diag_map_staff_cancel + "='" + employee_id + "'"
                    + "," + dbObj.visit_diag_map_cancel_date_time + "='" + cancel_time + "'";
        }

        strSql = strSql + sql1;
        theConnectionInf.eUpdate(strSql);
        return "";
    }

    /**
     * ��㹡������Ң�������١�ѹ�֡ŧ����������ѧ
     * ��Һѹ�֡���Ǩ�������͡���� true ����ѧ���ѹ�֡��������͡����
     * false
     *
     * @param visit_id �� String ��������ѡ�ͧ���ҧ t_visit
     * @param b_template_dx_id �� String ��������ѡ�ͧ���ҧ b_template_dx
     * @param active �� String ��㹡�á�˹������ 1 ��ҹ, 0 �����ҹ
     * @return �� boolean ��Һѹ�֡���Ǩ�������͡���� true
     * ����ѧ���ѹ�֡��������͡���� false
     * @author padungrat(tong)
     * @date 22/03/49 not@deprecated ��ͧ�ѧ�ѹ��ͧ��鹵鹴��� select ��ҹ���
     * package objdb
     */
    public boolean checkDataInDB(String visit_id, String b_template_dx_id, String active) throws Exception {
        result = true;
        strSql = "SELECT count(" + dbObj.pk_field + ") as count FROM " + dbObj.table + ""
                + " WHERE " + dbObj.visit_id + " ='" + visit_id + "'"
                + " AND " + dbObj.dx_template_id + " ='" + b_template_dx_id + "'"
                + " AND " + dbObj.visit_diag_map_active + " ='" + active + "'";

        rs = theConnectionInf.eQuery(strSql);
        countrow = 0;
        while (rs.next()) {
            countrow = rs.getInt(1);
        }
        rs = null;
        if (countrow == 0) {
            result = false;
        }
        return result;
    }

    /*///////////////////////////////////////////////////////////////////////////
     */
    public Vector selectByVisit(String pk) throws Exception {
        String sql = "select * from " + dbObj.table
                + " where " + dbObj.visit_id + " = '" + pk + "'"
                + " AND " + dbObj.visit_diag_map_active + "='" + Active.isEnable() + "'";

        Vector v = eQuery(sql);
        if (v.isEmpty()) {
            return null;
        } else {
            return v;
        }
    }

    /*///////////////////////////////////////////////////////////////////////////
     */
    public Vector selectAll() throws Exception {
        String sql = "select * from " + dbObj.table;

        Vector v = eQuery(sql);
        if (v.isEmpty()) {
            return null;
        } else {
            return v;
        }
    }

    /*////////////////////////////////////////////////////////////////////////////
     */
    public Vector eQuery(String sql) throws Exception {
        MapVisitDx p;
        Vector list = new Vector();
        rs = theConnectionInf.eQuery(sql);

        while (rs.next()) {
            p = new MapVisitDx();

            p.setObjectId(rs.getString(dbObj.pk_field));
            p.visit_id = rs.getString(dbObj.visit_id);
            p.dx_template_id = rs.getString(dbObj.dx_template_id);
            p.t_patient_id = rs.getString(dbObj.t_patient_id);
            p.visit_diag_map_active = rs.getString(dbObj.visit_diag_map_active);
            p.visit_diag_map_date_time = rs.getString(dbObj.visit_diag_map_date_time);
            p.visit_diag_map_dx = rs.getString(dbObj.visit_diag_map_dx);
            p.visit_diag_map_icd = rs.getString(dbObj.visit_diag_map_icd);
            p.visit_diag_map_staff = rs.getString(dbObj.visit_diag_map_staff);
            p.visit_diag_map_staff_cancel = rs.getString(dbObj.visit_diag_map_staff_cancel);
            p.visit_diag_map_cancel_date_time = rs.getString(dbObj.visit_diag_map_cancel_date_time);
            try {
                p.thaidescription = rs.getString("template_dx_thaidescription");
            } catch (SQLException ex) {
            }
            list.add(p);
        }
        rs.close();
        return list;
    }

    /*////////////////////////////////////////////////////////////////////////////
     */
    public Vector queryVisit(String visit) throws Exception {
        String sql = "select * from " + dbObj.table
                + " where "
                + dbObj.visit_id + " = '" + visit + "'"
                + " AND " + dbObj.visit_diag_map_active + "='" + Active.isEnable() + "'";

        return eQuery(sql);
    }

    /*////////////////////////////////////////////////////////////////////////////
     */
    public int delete(MapVisitDx o) throws Exception {
        String sql = "delete from " + dbObj.table
                + " where " + dbObj.pk_field + "='" + o.getObjectId() + "'";

        return theConnectionInf.eUpdate(sql);
    }

    /*////////////////////////////////////////////////////////////////////////////
     */
    public int deleteMVD(String vID) throws Exception {
        String sql = "delete from " + dbObj.table
                + " where " + dbObj.visit_id + "='" + vID + "'";

        return theConnectionInf.eUpdate(sql);
    }
}

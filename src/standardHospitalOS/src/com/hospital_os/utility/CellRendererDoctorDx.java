/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.utility;

import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class CellRendererDoctorDx extends JLabel implements TableCellRenderer {

    private static final long serialVersionUID = 1L;

    public CellRendererDoctorDx() {
        setOpaque(true);
        setHorizontalAlignment(SwingConstants.LEADING);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        setFont(table.getFont());
        if (value instanceof String) {
            String[] strings = value.toString().trim().split("\n");
            String text = "";
            String toolTipText = "";
            for (String string : strings) {
                text += text.isEmpty() ? string : ", " + string;
                toolTipText += toolTipText.isEmpty() ? string : "<br>" + string;
            }
            setText(text);
            setToolTipText("<html><BODY BGCOLOR = #E7FAAF>" + toolTipText + "</BODY></html>");
        }


        if (isSelected) {
            this.setBackground(table.getSelectionBackground());
            this.setForeground(table.getSelectionForeground());
        } else {
            this.setBackground(table.getBackground());
            this.setForeground(table.getForeground());
        }
        return this;
    }
}
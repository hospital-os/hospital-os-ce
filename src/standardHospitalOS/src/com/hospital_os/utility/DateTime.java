/*
 * DateTime.java
 *
 * Created on 16 ���Ҥ� 2548, 12:52 �.
 */
package com.hospital_os.utility;

import com.hospital_os.usecase.connection.*;
import java.sql.*;

/**
 *
 * @author tong
 */
@SuppressWarnings("ClassWithoutLogger")
public class DateTime {

    private ConnectionInf theConnectionInf;

    public DateTime(ConnectionInf c) {
        this.theConnectionInf = c;
    }
    
    public String getDate() throws Exception {
        String sql = "select CURRENT_DATE";
        String data = eQuery(sql);
        data = convertThaiDate(data);
        return data;
    }

    public String convertThaiDate(String date) {
        if (date.length() != 10) {
            return null;
        }

        String year = date.substring(0, 4);
        String month = date.substring(5, 7);
        String day = date.substring(8, 10);
        int y = Integer.parseInt(year);

        String yyyy = "0000" + String.valueOf(y + 543);
        yyyy = yyyy.substring(yyyy.length() - 4, yyyy.length());
        month = month.substring(month.length() - 2, month.length());
        day = day.substring(day.length() - 2, day.length());
        return yyyy + "-" + month + "-" + day;

    }

    public String getTime() throws Exception {
        String sql = "select CURRENT_TIME";
        String data = eQuery(sql);
        data = convertThaiTime(data);
        return data;
    }

    public String convertThaiTime(String time) {

        if (time.length() < 8) {
            return null;
        }
        String hour = time.substring(0, 2);
        String minute = time.substring(3, 5);
        String second = time.substring(6, 8);
        return hour + ":" + minute + ":" + second;

    }

    private String eQuery(String sql) throws Exception {
        ResultSet rs = theConnectionInf.eQuery(sql);
        String data = null;
        while (rs.next()) {
            data = rs.getString(1);
        }
        rs.close();
        return data;
    }
}

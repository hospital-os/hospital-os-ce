CREATE TABLE public.s_siriraj_version ( 
    s_siriraj_version_id              	varchar(255) NOT NULL,
    version_siriraj_number            	varchar(255) NULL,
    version_siriraj_description       	varchar(255) NULL,
    version_siriraj_application_number	varchar(255) NULL,
    version_siriraj_database_number   	varchar(255) NULL,
    version_siriraj_update_time       	varchar(255) NULL 
);

CREATE TABLE b_siriraj_receipt_sequence (
b_siriraj_receipt_sequence_id varchar(255) NOT NULL
, siriraj_receipt_sequence_ip_address varchar(255)
, siriraj_receipt_sequence_pattern varchar(255)
, siriraj_receipt_sequence_day varchar(255)
, siriraj_receipt_sequence_month varchar(255)
, siriraj_receipt_sequence_service_point_seq varchar(255)
, siriraj_receipt_sequence_service_point varchar(255) DEFAULT '0'::character varying 
, siriraj_receipt_sequence_book_no varchar(255) DEFAULT '0'::character varying 
, siriraj_receipt_sequence_begin_no varchar(255) DEFAULT '0'::character varying 
, siriraj_receipt_sequence_end_no varchar(255) DEFAULT '0'::character varying 
, siriraj_receipt_sequence_receipt_qty varchar(255) DEFAULT '0'::character varying 
, siriraj_receipt_sequence_value varchar(255) DEFAULT '0'::character varying 
, siriraj_receipt_sequence_active varchar(255) DEFAULT '0'::character varying 
, PRIMARY KEY (b_siriraj_receipt_sequence_id)
);

CREATE TABLE b_siriraj_receipt_dispatch_sequence (
b_siriraj_receipt_dispatch_sequence_id varchar(255) NOT NULL
, siriraj_receipt_dispatch_sequence_ip_address varchar(255)
, siriraj_receipt_dispatch_sequence_service_point_seq varchar(255)
, siriraj_receipt_dispatch_sequence_service_point varchar(255) DEFAULT '0'::character varying 
, siriraj_receipt_dispatch_sequence_active varchar(255) DEFAULT '0'::character varying 
, siriraj_receipt_dispatch_sequence_site_code varchar(255) DEFAULT '0'::character varying 
, PRIMARY KEY (b_siriraj_receipt_dispatch_sequence_id)
);

INSERT INTO s_siriraj_version VALUES ('9750000000001', '1', 'Siriraj, Community Edition', '1.00.120511', '1.00.120511', '2554-05-12 16:30:00');
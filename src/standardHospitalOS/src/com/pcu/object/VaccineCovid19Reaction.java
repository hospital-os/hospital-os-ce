/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.object;

import com.hospital_os.usecase.connection.Persistent;
import java.util.Date;

/**
 *
 * @author tanakrit
 */
public class VaccineCovid19Reaction extends Persistent {

    public String t_health_vaccine_covid19_id;
    public String t_patient_id;
    public String t_visit_id;
    public int reaction_no;
    public int reaction_stage;
    public String f_vaccine_reaction_stage_id = "1";
    public Date reaction_date;
    public int adverse_reaction = 0;
    public String adverse_description;
    public String f_vaccine_reaction_type_id;
    public String f_vaccine_reaction_symptom_id;
    public String symptom_description;
    public String active = "1";
    public Date record_date_time = new Date();
    public String user_record_id;
    public Date update_date_time = new Date();
    public String user_update_id;
    public Date cancel_date_time;
    public String user_cancel_id;
    public int ref_code;

}

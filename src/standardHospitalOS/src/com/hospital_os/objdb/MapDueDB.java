/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.MapDue;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class MapDueDB {

    private final ConnectionInf theConnectionInf;
    private final String idtable = "985";

    public MapDueDB(ConnectionInf theConnectionInf) {
        this.theConnectionInf = theConnectionInf;
    }

    public int insert(MapDue o) throws Exception {
        String sql = "INSERT INTO b_map_due (b_map_due_id, b_item_id, map_due_type,"
                + "b_due_type_id, active, user_record_id, record_date_time, user_update_id, update_date_time, contract_plans) "
                + "VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', ARRAY[%s])";
        return theConnectionInf.eUpdate(String.format(sql,
                o.getGenID(idtable),
                o.b_item_id,
                o.map_due_type,
                o.b_due_type_id,
                o.active,
                o.user_record_id,
                o.record_date_time,
                o.user_update_id,
                o.update_date_time,
                o.getContractPlans()));
    }

    public int update(MapDue o) throws Exception {
        String sql = "UPDATE b_map_due SET map_due_type = '%s',b_due_type_id = '%s',active = '%s'"
                + ", user_update_id = '%s', update_date_time = '%s'"
                + ", contract_plans = ARRAY[%s] "
                + "WHERE b_map_due_id = '%s'";
        return theConnectionInf.eUpdate(String.format(sql,
                o.map_due_type,
                o.b_due_type_id,
                o.active,
                o.user_update_id,
                o.update_date_time,
                o.getContractPlans(),
                o.getObjectId()));
    }

    public int delete(MapDue o) throws Exception {
        String sql = "delete from b_map_due WHERE b_map_due_id = '%s'";
        return theConnectionInf.eUpdate(String.format(sql,
                o.getObjectId()));
    }

    public MapDue selectById(String id) throws Exception {
        String sql = "select * from b_map_due where b_map_due_id = '%s'";
        List<MapDue> list = eQuery(String.format(sql, id));
        return list.isEmpty() ? null : list.get(0);
    }

    public MapDue selectByItemId(String id) throws Exception {
        String sql = "select * from b_map_due where b_item_id = '%s'";
        List<MapDue> list = eQuery(String.format(sql, id));
        return list.isEmpty() ? null : list.get(0);
    }

    public List<MapDue> listByDueTypeId(String id) throws Exception {
        String sql = "select * from b_map_due where b_due_type_id = '%s'";
        List<MapDue> list = eQuery(String.format(sql, id));
        return list;
    }

    public boolean isMappedDueByOrderId(String orderId) throws Exception {
        String sql = "select \n"
                + "count(b_map_due.b_map_due_id) \n"
                + "from \n"
                + "t_order\n"
                + "inner join b_map_due on b_map_due.b_item_id = t_order.b_item_id and b_map_due.active = '1'\n"
                + "inner join t_visit_payment on t_visit_payment.t_visit_id = t_order.t_visit_id\n"
                //+ "and t_visit_payment.visit_payment_priority = '0' "
                + "and t_visit_payment.visit_payment_active = '1'\n"
                + "where \n"
                + "t_order.t_order_id = '%s'\n"
                + "and t_order.f_order_status_id = '0'\n"
                + "and t_visit_payment.b_contract_plans_id = any(b_map_due.contract_plans)";
        List<Object[]> list = theConnectionInf.eComplexQuery(String.format(sql,
                orderId));
        return list.isEmpty() ? false : (list.get(0)[0]).toString().equals("1");
    }

    public List<Object[]> listMatchOrderByOrderIds(String visitId, String... ids) throws Exception {
        String sql = "select \n"
                + "t_order.t_order_id\n"
                + ", b_map_due.map_due_type\n"
                + ", b_map_due.b_due_type_id\n"
                + ", t_order.order_common_name\n"
                + ", b_item.item_trade_name\n"
                + ", count(t_order_due.t_order_due_id)\n"
                + "from t_order\n"
                + "inner join b_map_due on b_map_due.b_item_id = t_order.b_item_id and b_map_due.active = '1'\n"
                + "left join b_item on b_item.b_item_id = t_order.b_item_id\n"
                + "left join t_order_due on t_order_due.t_order_id = t_order.t_order_id\n"
                + "inner join t_visit_payment on t_visit_payment.t_visit_id = t_order.t_visit_id \n"
                //+ "and t_visit_payment.visit_payment_priority = '0' "
                + "and t_visit_payment.visit_payment_active = '1'\n"
                + "where t_order.f_order_status_id = '1'\n"
                + "and t_order.t_visit_id = '%s'\n"
                + "and t_order.t_order_id in (%s)\n"
                + "and t_visit_payment.b_contract_plans_id = any(b_map_due.contract_plans)\n"
                + "GROUP BY t_order.t_order_id, b_map_due.map_due_type, b_map_due.b_due_type_id, t_order.order_common_name, b_item.item_trade_name\n"
                + "order by t_order.order_common_name";
        StringBuilder sb = new StringBuilder();
        for (String id : ids) {
            sb.append("'").append(id).append("',");
        }
        List<Object[]> list = theConnectionInf.eComplexQuery(String.format(sql,
                visitId,
                sb.toString().substring(0, sb.toString().length() - 1)));
        return list;
    }

    public List<MapDue> eQuery(String sql) throws Exception {
        List<MapDue> list = new ArrayList<MapDue>();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        while (rs.next()) {
            MapDue p = new MapDue();
            p.setObjectId(rs.getString("b_map_due_id"));
            p.b_item_id = rs.getString("b_item_id");
            p.map_due_type = rs.getString("map_due_type");
            p.b_due_type_id = rs.getString("b_due_type_id");
            p.active = rs.getString("active");
            p.user_record_id = rs.getString("user_record_id");
            p.record_date_time = rs.getString("record_date_time");
            p.user_update_id = rs.getString("user_update_id");
            p.update_date_time = rs.getString("update_date_time");
            p.contract_plans = (String[]) rs.getArray("contract_plans").getArray();
            list.add(p);
        }
        rs.close();
        return list;
    }

    public List<Object[]> listMapContractPlans(String mapId) throws Exception {
        String sql = "select \n"
                + "b_contract_plans.b_contract_plans_id \n"
                + ", b_contract_plans.contract_plans_description\n"
                + "from b_contract_plans \n"
                + "inner join b_map_due on b_contract_plans.b_contract_plans_id = any(b_map_due.contract_plans) \n"
                + "where contract_plans_active = '1'\n"
                + "and b_map_due.b_map_due_id = '%s'\n"
                + "order by b_contract_plans.contract_plans_description";

        List<Object[]> list = theConnectionInf.eComplexQuery(String.format(sql,
                mapId));
        return list;
    }
}

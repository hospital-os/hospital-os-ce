/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;
import java.util.Date;

/**
 *
 * @author tanakrit
 */
public class VisitCovidLab extends Persistent {

    public String t_visit_id;
    public int sent_complete = 0;
    public String user_record_id;
    public Date record_date_time = new Date();
    public String user_update_id;
    public Date update_date_time = new Date();
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class MapOrderDoctor extends Persistent {

    private static final long serialVersionUID = 1L;
    public String b_item_subgroup_id;
    public String user_record_id;
    public String record_date_time;
    public String user_update_id;
    public String update_date_time;
    // in object only
    public String item_subgroup_number;
    public String item_subgroup_description;
}

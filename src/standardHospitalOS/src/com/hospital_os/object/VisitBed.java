/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;
import java.util.Date;

/**
 *
 * @author Somprasong
 */
public class VisitBed extends Persistent {

    public String t_visit_id = "";
    public String b_visit_bed_id = "";
    public String bed_number = "";
    public String[] continue_b_item_ids = new String[]{};
    public String current_bed = "1";
    public Date move_date_time;
    public Date move_out_date_time;
    public String reason = "";
    public String active = "1";
    public Date record_date_time = new Date();
    public String user_record = "";
    public Date modify_date_time = new Date();
    public String user_modify = "";
    // in object only
    public String wardName = "";
}

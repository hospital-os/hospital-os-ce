/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.gui.component;

import javax.swing.JButton;
import net.sf.jasperreports.swing.JRViewerController;
import net.sf.jasperreports.swing.JRViewerToolbar;

/**
 *
 * @author sompr
 */
public class HosOSJRViewerToolbar extends JRViewerToolbar {

    public HosOSJRViewerToolbar(JRViewerController viewerContext) {
        super(viewerContext);
    }

    public JButton getBtnSave() {
        return this.btnSave;
    }

    public JButton getBtnPrint() {
        return this.btnPrint;
    }

}

/*
 * ModuleInfTool.java
 *
 * Created on 1 �ԧ�Ҥ� 2548, 14:24 �.
 */
package com.hospital_os.usecase.connection;

import java.util.Vector;
import javax.swing.JMenu;
import javax.swing.JPanel;

/**
 *
 * @author nu_ojika
 * @author_modify amp :08/09/48: �������ö���� panel ������ö�кص��˹���
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public interface ModuleInfTool {

    /**
     * Function ����Ѻ get JPanel ����ͧ����ʴ��� HospitalOS ����Ѻ Module
     * ����ͧ��� �ʴ������ JPanel ����� ����Ѻ Module �������ʴ���� return ���
     * Null
     *
     */
    public JPanel getJPanel();

    /**
     * amp Function ����Ѻ get Vector �ͧ JPanel,���� ��� index ���й���ʴ���
     * HospitalOS �������ö���� panel ������ panel ������� panel
     * ����ö�кص��˹��� ����Ѻ Module �������ͧ�������ʴ� panel ��� return
     * ��� �ͧ panel �� Null ����Ѻ panel �������кص��˹觤������� ""
     * �ж������繡�ù�仵�ͷ���
     *
     */
    @SuppressWarnings("UseOfObsoleteCollectionType")
    public Vector getVectorJPanel();

    /**
     * Function ��Ǩ�ͺ������Է����ҹ����͵�ͧ������ Panel �ʴ��� Hospital OS
     * Version 3 ���Ѻ��� authentication �ͧ Employee ��� Login ������� Module
     * ��Ǩ�ͺ�������ö�ʴ� Panel ��������� ��ҵ�Ǩ�ͺ��������ö������� Login
     * �ʴ� Panel ��� Return true ��ҵ�Ǩ�ͺ�����������ö�������ҹ��� Login
     * �ʴ� Panel ��� Return false
     *
     */
    //public Vector getAuthenJPanel();
    public boolean isJPanelVisible(String authen);

    /**
     * Function ����Ѻ get ���� Panel ����ͧ����ʴ� �������բ������ʴ�
     * ����觤�ҷ����ҧ��
     *
     */
    public String getNamePanel();

    /**
     * Function ����Ѻ get JMenu ����ͧ����ʴ��� HospitalOS ����Ѻ Module
     * ����ͧ��� �ʴ������ JMenu ����� ����Ѻ Module �������ʴ���� return ���
     * Null
     *
     * @return
     * @auther ojika
     */
    public JMenu getJMenu();

    /**
     * Function ��Ǩ�ͺ������Է����ҹ����͵�ͧ������ Menu �ʴ��� Hospital OS
     * Version 3 ���Ѻ��� authentication �ͧ Employee ��� Login ������� Module
     * ��Ǩ�ͺ�������ö�ʴ� Menu ��������� ��ҵ�Ǩ�ͺ��������ö������� Login
     * �ʴ� Menu ��� Return true ��ҵ�Ǩ�ͺ�����������ö�������ҹ��� Login
     * �ʴ� Menu ��� Return false
     *
     */
    public boolean isJMenuVisible(String authen);

    /**
     * Function ����Ѻ get Vector �ͧ MenuItem ���͹���ʴ��� Menu Standard
     * Vector ��Сͺ���� JMenuItem ��� Authen �ͧ���е�� ����礨ҡ Authen
     * �������������Է��������� �ҡ����ͧ����ʴ� MenuItem ������ Return ���
     * NULL ��Ѻ�
     *
     */
    @SuppressWarnings("UseOfObsoleteCollectionType")
    public Vector getVectorJMenuItem(String authen);

    /**
     * Function ����Ѻ�͡��� Menu ����ͧ����ʴ���鹵�ͧ����ʴ��� Menu
     * �ҵðҹ�����¡ Menu �ѹ�͡� return ��� true �ҡ��ͧ��� add � Standard
     * return ��� false �ҡ��ͧ����ʴ��� Menu ����
     *
     */
    //public boolean getAddToMenuStandard();
    public boolean isInMenuStandard();

    /**
     * Function ����Ѻ get Vector ���й�� add � tree �� Vector �ͧ Object
     * SetupModule �������� standardHospitalOS ������������觤�� Null �͡��᷹
     *
     */
    @SuppressWarnings("UseOfObsoleteCollectionType")
    public Vector getVectorSetupModule();

    /**
     * Function ��Ǩ�ͺ������Է����ҹ����͵�ͧ������ Tree �ʴ��� Hospital OS
     * Version 3 ���Ѻ��� authentication �ͧ Employee ��� Login ������� Module
     * ��Ǩ�ͺ�������ö�ʴ� Tree ��������� ��ҵ�Ǩ�ͺ��������ö������� Login
     * �ʴ� Tree ��� Return true ��ҵ�Ǩ�ͺ�����������ö�������ҹ��� Login
     * �ʴ� Tree ��� Return false
     *
     */
    public boolean isJTreeVisible(String authen);

    /**
     * Object ��� HosControl ������ա�����¡�� �е�ͧ cast �� HosControl ��͹
     *
     */
    public void setHosControl(Object hc);

    /**
     * Object ��� Version � standardHospital Package com.hospital_os.object
     * �ҡ����բ����� ��� Return null 令��
     *
     */
    public Object getObjectVersion();
}

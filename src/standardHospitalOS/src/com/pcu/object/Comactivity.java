/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.object;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class Comactivity extends Persistent implements CommonInf {

    public String description;
    public String code;

    @Override
    public String getCode() {
        return getObjectId();
    }

    @Override
    public String getName() {
        return code + " : " + description;
    }

    @Override
    public String toString() {
        return description;
    }
}

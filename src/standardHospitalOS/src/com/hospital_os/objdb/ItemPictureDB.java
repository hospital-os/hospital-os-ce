/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.Item;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author tanakrit
 */
public class ItemPictureDB {

    private final ConnectionInf connectionInf;

    public ItemPictureDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public int insert(Item theItem, String userRecordId) throws Exception {
        String sql = "INSERT INTO b_item_picture(b_item_id, item_picture, user_record_id) VALUES (?, ?, ?)";
        try (PreparedStatement ePQuery = connectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, theItem.getObjectId());
            ePQuery.setBytes(index++, theItem.item_picture);
            ePQuery.setString(index++, userRecordId);
            return ePQuery.executeUpdate();
        }
    }

    public int delete(Item theItem) throws Exception {
        String sql = "DELETE FROM b_item_picture WHERE b_item_id = ? ";
        try (PreparedStatement ePQuery = connectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, theItem.getObjectId());
            return ePQuery.executeUpdate();
        }
    }

    public byte[] selectItemPictureByItemId(String ItemId) throws Exception {
        String sql = "select item_picture from b_item_picture where b_item_id=?";
        try (PreparedStatement ePQuery = connectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, ItemId);
            ResultSet rs = ePQuery.executeQuery();
            byte[] data = null;
            while (rs.next()) {
                data = rs.getBytes("item_picture");
            }
            return data;
        }
    }

    public int countByItemId(String ItemId) throws Exception {
        String sql = "select count(b_item_id) from b_item_picture where b_item_id = ?";
        try (PreparedStatement ePQuery = connectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, ItemId);
            ResultSet rs = ePQuery.executeQuery();
            int count = 0;
            while (rs.next()) {
                count = rs.getInt("count");
            }
            return count;
        }
    }

}

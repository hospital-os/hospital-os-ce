/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class LabResultItemAge extends Persistent {

    public String b_item_lab_result_id = "";
    public String age_min = "";
    public String age_max = "";
    public String male_min = "";
    public String male_max = "";
    public String female_min = "";
    public String female_max = "";
    public String male_critical_min = "";
    public String male_critical_max = "";
    public String female_critical_min = "";
    public String female_critical_max = "";
}

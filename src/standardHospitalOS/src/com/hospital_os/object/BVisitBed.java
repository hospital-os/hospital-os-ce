/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.Persistent;
import java.util.Date;

/**
 *
 * @author Somprasong
 */
public class BVisitBed extends Persistent implements CommonInf{

    public String b_visit_ward_id = "";
    public int sequences = 1;
    public String bed_number = "";
    public String[] continue_b_item_ids = new String[]{};
    public String active = "1";
    public Date record_date_time;
    public String user_record = "";
    public Date modify_date_time;
    public String user_modify = "";

    @Override
    public String getCode() {
        return getObjectId();
    }

    @Override
    public String getName() {
        return bed_number;
    }

    @Override
    public String toString() {
        return getName();
    }
}

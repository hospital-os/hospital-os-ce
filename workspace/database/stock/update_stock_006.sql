-- #576
ALTER TABLE b_hstock_item_set_detail ALTER COLUMN b_item_drug_caution_id TYPE TEXT USING b_item_drug_caution_id::varchar;
ALTER TABLE b_hstock_item_set_detail ALTER COLUMN b_item_drug_description_id TYPE TEXT USING b_item_drug_description_id::varchar;


INSERT INTO s_stock_version (version_app, version_db, description)VALUES ('2.0.5', '2.0.5', 'Add Req Stock Module');
INSERT INTO s_script_update_log values ('Stock_Module','update_stock_006.sql',(select current_date) || ','|| (select current_time),'Add Req Stock Module');

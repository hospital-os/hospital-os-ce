INSERT INTO s_tdrg_version VALUES ('9760000000002', '2', 'Grouper for Thai DRG version 6.2.1', '1.2.0', '1.2.0', (select current_date) || ','|| (select current_time), '6.2.1');

INSERT INTO s_script_update_log values ('DRG_Module','update_tdrg_003.sql',(select current_date) || ','|| (select current_time),'Update to version 6.2.1');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.objdb.objdbclass;

import com.hospital_os.usecase.connection.ConnectionInf;
import com.pcu.object.CommunityActivity;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class CommunityActivityDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "805";

    public CommunityActivityDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public int insert(CommunityActivity obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO t_health_community_activity(\n");
            sql.append(
                    "            t_health_community_activity_id, t_health_community_id, f_comactivity_id, start_date, \n");
            sql.append(
                    "            finish_date, record_date_time, \n");
            sql.append(
                    "            user_record_id, modify_date_time, user_modify_id, active)\n");
            sql.append(
                    "    VALUES (?, ?, ?, ?, \n");
            sql.append(
                    "            ?, ?, ?, ?,  \n");
            sql.append(
                    "            ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getGenID(tableId));
            preparedStatement.setString(2, obj.t_health_community_id);
            preparedStatement.setString(3, obj.f_comactivity_id);
            preparedStatement.setString(4, obj.start_date);
            preparedStatement.setString(5, obj.finish_date);
            preparedStatement.setString(6, obj.record_date_time);
            preparedStatement.setString(7, obj.user_record_id);
            preparedStatement.setString(8, obj.modify_date_time);
            preparedStatement.setString(9, obj.user_modify_id);
            preparedStatement.setString(10, obj.active);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(CommunityActivity obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_health_community_activity\n");
            sql.append(
                    "   SET t_health_community_id=?, f_comactivity_id=?, \n");
            sql.append(
                    "       start_date=?, finish_date=?, \n");
            sql.append(
                    "       modify_date_time=?, user_modify_id=?, \n");
            sql.append(
                    "       active=?\n");
            sql.append(" WHERE t_health_community_activity_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.t_health_community_id);
            preparedStatement.setString(2, obj.f_comactivity_id);
            preparedStatement.setString(3, obj.start_date);
            preparedStatement.setString(4, obj.finish_date);
            preparedStatement.setString(5, obj.modify_date_time);
            preparedStatement.setString(6, obj.user_modify_id);
            preparedStatement.setString(7, obj.active);
            preparedStatement.setString(8, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int inactive(CommunityActivity obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_health_community_activity\n");
            sql.append("   SET active=?, cancel_date_time=?, user_cancel_id=?\n");
            sql.append(" WHERE t_health_community_activity_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, "0");
            preparedStatement.setString(2, obj.cancel_date_time);
            preparedStatement.setString(3, obj.user_cancel_id);
            preparedStatement.setString(4, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "delete from t_health_community_activity where t_health_community_activity_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            // execute delete SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public CommunityActivity select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from t_health_community_activity where t_health_community_activity_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<CommunityActivity> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<CommunityActivity> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<CommunityActivity> list = new ArrayList<CommunityActivity>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                CommunityActivity obj = new CommunityActivity();
                obj.setObjectId(rs.getString("t_health_community_activity_id"));
                obj.t_health_community_id = rs.getString("t_health_community_id");
                obj.f_comactivity_id = rs.getString("f_comactivity_id");
                obj.start_date = rs.getString("start_date");
                obj.finish_date = rs.getString("finish_date");
                obj.record_date_time = rs.getString("record_date_time");
                obj.user_record_id = rs.getString("user_record_id");
                obj.modify_date_time = rs.getString("modify_date_time");
                obj.user_modify_id = rs.getString("user_modify_id");
                obj.cancel_date_time = rs.getString("cancel_date_time");
                obj.user_cancel_id = rs.getString("user_cancel_id");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<CommunityActivity> selectByCommuId(String commuId) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from t_health_community_activity where t_health_community_id = ? and active = '1'";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, commuId);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }    
}

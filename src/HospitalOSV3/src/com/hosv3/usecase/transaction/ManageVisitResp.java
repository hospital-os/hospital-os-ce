package com.hosv3.usecase.transaction;

/**
 *
 * @author amp
 */
public interface ManageVisitResp {

    public void notifyReadVisit(String str, int status);

    public void notifyReverseFinancial(String str, int status);

    public void notifyReverseDoctor(String str, int status);

    public void notifyVisitPatient(String str, int status);

    public void notifySendVisit(String str, int status);

    public void notifySendVisitBackWard(String str, int status);

    public void notifyObservVisit(String str, int status);

    public void notifyUnlockVisit(String str, int status);

    public void notifyDropVisit(String str, int status);

    public void notifyAdmitVisit(String str, int status);

    public void notifyCheckDoctorTreament(String msg, int state);

    public void notifyDischargeDoctor(String str, int status);

    public void notifyRemainDoctorDischarge(String str, int status);

    public void notifyDischargeFinancial(String str, int status);

    public void notifyDeleteVisitPayment(String str, int status);

    public void notifyReverseAdmit(String str, int status);

    public void notifyAdmitLeaveHome(String str, int status);
}

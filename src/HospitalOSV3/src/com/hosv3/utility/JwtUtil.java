/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.utility;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

/**
 *
 * @author sompr
 */
public class JwtUtil {

    public static DecodedJWT verify(String token, String hcode, String secretKey) throws Exception {
        try {
            Algorithm algorithm = Algorithm.HMAC256(secretKey);
            JWTVerifier verifier = JWT.require(algorithm)
                    .withAudience(hcode)
                    .build();
            DecodedJWT jwt = verifier.verify(token);
            return jwt;
        } catch (JWTVerificationException exception) {
            //Invalid signature/claims
            throw exception;
        }
    }

    public static DecodedJWT verify(String token, String secretKey) throws Exception {
        try {
            Algorithm algorithm = Algorithm.HMAC256(secretKey);
            JWTVerifier verifier = JWT.require(algorithm)
                    .build();
            DecodedJWT jwt = verifier.verify(token);
            return jwt;
        } catch (JWTVerificationException exception) {
            //Invalid signature/claims
            throw exception;
        }
    }

    public static DecodedJWT decode(String token) throws Exception {
        try {
            DecodedJWT jwt = JWT.decode(token);
            return jwt;
        } catch (JWTDecodeException exception) {
            //Invalid signature/claims
            throw exception;
        }
    }

    public static String sign(Map<String, Object> payloadClaims, Date expiresAt, String secretKey) throws Exception {
        try {
            Algorithm algorithm = Algorithm.HMAC256(secretKey);
            JWTCreator.Builder withPayload = JWT.create()
                    .withPayload(payloadClaims)
                    .withIssuer("Hospital-OS")
                    .withIssuedAt(new Date())
                    .withNotBefore(new Date())
                    .withJWTId(UUID.randomUUID().toString());
            if (expiresAt != null) {
                withPayload = withPayload.withExpiresAt(expiresAt);
            }
            String token = withPayload.sign(algorithm);
            return token;
        } catch (JWTCreationException exception) {
            //Invalid Signing configuration / Couldn't convert Claims.
            throw exception;
        }
    }
}

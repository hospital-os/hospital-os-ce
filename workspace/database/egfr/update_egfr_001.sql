-- GFR
CREATE TABLE b_map_egfr (
    b_map_egfr_id               character varying(255)   NOT NULL,
    b_item_id                   character varying(255)   NOT NULL default '',
    f_egfr_formula_id           character varying(1)   NOT NULL default '1',
    b_employee_ids              character varying[] DEFAULT array[''],
    enable                      character varying(1)   NOT NULL default '1',
CONSTRAINT b_map_egfr_pkey PRIMARY KEY (b_map_egfr_id)
);
INSERT INTO b_map_egfr (b_map_egfr_id, enable) VALUES ('1', '0');

CREATE TABLE f_egfr_formula (
    f_egfr_formula_id                   character varying(1)   NOT NULL,
    formula_name                       character varying(255)   NOT NULL,
    formula_unit                       character varying(255)   NOT NULL,
CONSTRAINT f_egfr_formula_pkey PRIMARY KEY (f_egfr_formula_id)
);

INSERT INTO f_egfr_formula VALUES ('1','CrCl', 'ccs/mm');
INSERT INTO f_egfr_formula VALUES ('2','MDRD', 'mL/min/1.73 m2');
INSERT INTO f_egfr_formula VALUES ('3','CKD-EPI', 'mL/min/1.73 m2');
INSERT INTO f_egfr_formula VALUES ('4','Thai eGFR', 'mL/min/1.73 m2');

CREATE TABLE f_egfr_interpretation (
    f_egfr_interpretation_id            character varying(1)   NOT NULL,
    stage                              character varying(255)   NOT NULL,
    description                        character varying(255)   NOT NULL,
CONSTRAINT f_egfr_interpretation_pkey PRIMARY KEY (f_egfr_interpretation_id)
);

INSERT INTO f_egfr_interpretation VALUES ('1','ระยะที่ 1', 'ตรวจพบพยาธิสภาพที่ไตแล้ว แต่ไตยังทำงานปกติ (จีเอฟอาร์ 90 มล./นาที ขึ้นไป)');
INSERT INTO f_egfr_interpretation VALUES ('2','ระยะที่ 2', 'ตรวจพบพยาธิสภาพที่ไตแล้ว และไตเริ่มทำงานผิดปกติเล็กน้อย (จีเอฟอาร์ 60-89 มล./นาที)');
INSERT INTO f_egfr_interpretation VALUES ('3','ระยะที่ 3', 'ไตทำงานผิดปกติปานกลาง ไม่ว่าจะตรวจพบพยาธิสภาพที่ไตหรือไม่ก็ตาม (จีเอฟอาร์ 30-59 มล./นาที)');
INSERT INTO f_egfr_interpretation VALUES ('4','ระยะที่ 4', 'ไตทำงานผิดปกติมาก (จีเอฟอาร์ 15-29 มล./นาที)');
INSERT INTO f_egfr_interpretation VALUES ('5','ระยะที่ 5', 'ระยะสุดท้าย (จีเอฟอาร์ต่ำกว่า 15 หรือต้องล้างไต)');

CREATE TABLE t_visit_egfr (
    t_visit_egfr_id                   character varying(255)   NOT NULL,
    t_visit_id                       character varying(255)   NOT NULL,
    b_item_id                        character varying(255)   NOT NULL,
    t_order_id                  character varying(255)   NOT NULL,
    f_egfr_formula_id                 character varying(1)   NOT NULL,
    gfr_value                        character varying(255)   NOT NULL,
    f_egfr_interpretation_id          character varying(1)   NOT NULL,
    update_datetime                  timestamp without time zone   NOT NULL DEFAULT CURRENT_TIMESTAMP,
CONSTRAINT t_visit_egfr_pkey PRIMARY KEY (t_visit_egfr_id)
);
CREATE INDEX t_visit_egfr_index1
   ON t_visit_egfr (t_visit_id ASC NULLS LAST);
CREATE INDEX t_visit_egfr_index2
   ON t_visit_egfr (b_item_id ASC NULLS LAST);
CREATE UNIQUE INDEX t_visit_egfr_index3
   ON t_visit_egfr (t_order_id ASC NULLS LAST);

-- create s_egfr_version
CREATE TABLE s_egfr_version (
    version_id            varchar(255) NOT NULL,
    version_number            	varchar(255) NULL,
    version_description       	varchar(255) NULL,
    version_application_number	varchar(255) NULL,
    version_database_number   	varchar(255) NULL,
    version_update_time       	varchar(255) NULL 
);

ALTER TABLE s_egfr_version
	ADD CONSTRAINT s_egfr_version_pkey
	PRIMARY KEY (version_id);


INSERT INTO s_egfr_version VALUES ('1', '1', 'eGFR Module', '1.0.0', '1.0.0', (select current_date) || ','|| (select current_time));

INSERT INTO s_script_update_log values ('eGFR_Module','update_egfr_001.sql',(select current_date) || ','|| (select current_time),'Initialize eGFR Module');

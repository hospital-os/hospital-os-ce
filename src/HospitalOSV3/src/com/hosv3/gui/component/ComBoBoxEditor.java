/*
 * CheckBoxEdit.java
 *
 * Created on 24 ����Ҥ� 2547, 21:31 �.
 */
package com.hosv3.gui.component;

import java.awt.Component;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.logging.Logger;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;

/**
 *
 * @author kingland
 */
public class ComBoBoxEditor extends DefaultCellEditor implements ItemListener {

    private static final long serialVersionUID = 1L;
    private JComboBox button;
    private JTable jtable;
    private String str;
    private JLabel label;
    private JTextArea jscr;
    private TextFieldResultLab textfieldlab;
    private BalloonTextField balloonTextField;//amp:07/04/2549
    private Component com;
    int c = 0;

    public ComBoBoxEditor(JComboBox comboBox) {
        super(comboBox);
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value,
            boolean isSelected, int row, int column) {
        jtable = table;
        if (value instanceof String) {
            str = (String) value;
            JLabel lbl = new JLabel();
            lbl.setText(str);
            c = 1;
            return lbl;
        } else if (value instanceof JComboBox) {
            button = (JComboBox) value;
            button.addItemListener(this);
            c = 2;
            return button;
        } else if (value instanceof TextFieldResultLab) {
            c = 3;
            textfieldlab = (TextFieldResultLab) value;
            return textfieldlab;
        } else if (value instanceof JTextArea) {
            c = 5;
            jscr = (JTextArea) value;
            JScrollPane js = new JScrollPane();
            js.setViewportView(jscr);
            return js;
        } else if (value instanceof BalloonTextField)//amp:10/04/2549
        {
            c = 6;
            balloonTextField = (BalloonTextField) value;
            return balloonTextField;
        } else {
            c = 4;
            com = (Component) value;
            return com;
        }
    }

    @Override
    public Object getCellEditorValue() {
        if (c == 1) {
            return label;
        } else if (c == 2) {
            try {
                button.removeItemListener(this);
            } catch (Exception ex) {
                LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            }
            return button;
        } else if (c == 3) {
            return this.textfieldlab;
        } else if (c == 4) {
            return com;
        } else if (c == 5) {
//            JScrollPane js = new JScrollPane();
//            js.setViewportView(jscr);
            return jscr;
        } else if (c == 6)//amp:07/04/2549
        {
            return balloonTextField;
        }
        return null;
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        super.fireEditingStopped();
    }
    private static final Logger LOG = Logger.getLogger(ComBoBoxEditor.class.getName());
}

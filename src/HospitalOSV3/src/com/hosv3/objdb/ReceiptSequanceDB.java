/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.objdb;

import com.hospital_os.objdb.VisitYearDB;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.Gutil;
import com.hosv3.object.ReceiptSequance;
import java.sql.ResultSet;
import java.util.Vector;
import java.util.logging.Logger;

@SuppressWarnings("UseOfObsoleteCollectionType")
public class ReceiptSequanceDB {

    public ConnectionInf theConnectionInf;
    public ReceiptSequance dbObj;
    final public String idtable = "";
    private VisitYearDB visitYearDB;

    /**
     * @param db
     * @roseuid 3F65897F0326
     */
    public ReceiptSequanceDB(ConnectionInf db) {
        theConnectionInf = db;
        visitYearDB = new VisitYearDB(db);
        dbObj = new ReceiptSequance();
        initConfig();
    }

    private boolean initConfig() {
        dbObj.table = "b_receipt_sequence";
        dbObj.pk_field = "b_receipt_sequence_id";
        dbObj.receipt_sequence_active = "receipt_sequence_active";
        dbObj.receipt_sequence_ip = "receipt_sequence_ip";
        dbObj.receipt_sequence_pattern = "receipt_sequence_pattern";
        dbObj.receipt_sequence_value = "receipt_sequence_value";
        dbObj.begin_no = "receipt_sequence_begin_no";
        dbObj.book_no = "receipt_sequence_book_no";
        dbObj.end_no = "receipt_sequence_end_no";
        dbObj.receipt_qty = "receipt_sequence_receipt_qty";
        return true;
    }

    public int insert(ReceiptSequance p) throws Exception {
        p.generateOID(this.idtable);
        String sql = "insert into " + dbObj.table + " ("
                + dbObj.pk_field
                + " ," + dbObj.receipt_sequence_active
                + " ," + dbObj.receipt_sequence_ip
                + " ," + dbObj.receipt_sequence_pattern
                + " ," + dbObj.receipt_sequence_value
                + " ," + dbObj.begin_no
                + " ," + dbObj.book_no
                + " ," + dbObj.end_no
                + " ," + dbObj.receipt_qty
                + " ) values ('"
                + p.getObjectId()
                + "','" + p.receipt_sequence_active
                + "','" + p.receipt_sequence_ip
                + "','" + Gutil.CheckReservedWords(p.receipt_sequence_pattern)
                + "','" + p.receipt_sequence_value
                + "','" + p.begin_no
                + "','" + p.book_no
                + "','" + p.end_no
                + "','" + p.receipt_qty
                + "')";
        sql = Gutil.convertSQLToMySQL(sql, theConnectionInf.gettypeDatabase());
        return theConnectionInf.eUpdate(sql);
    }

    public int update(ReceiptSequance o) throws Exception {
        String sql = "update " + dbObj.table + " set ";
        String field = ""
                + "', " + dbObj.receipt_sequence_active + "='" + o.receipt_sequence_active
                + "', " + dbObj.receipt_sequence_ip + "='" + o.receipt_sequence_ip
                + "', " + dbObj.receipt_sequence_pattern + "='" + Gutil.CheckReservedWords(o.receipt_sequence_pattern)
                + "', " + dbObj.receipt_sequence_value + "='" + o.receipt_sequence_value
                + "', " + dbObj.begin_no + "='" + o.begin_no
                + "', " + dbObj.book_no + "='" + o.book_no
                + "', " + dbObj.end_no + "='" + o.end_no
                + "', " + dbObj.receipt_qty + "='" + o.receipt_qty
                + "' where " + dbObj.pk_field + "='" + o.getObjectId() + "'";
        sql = Gutil.convertSQLToMySQL(sql + field.substring(2), theConnectionInf.gettypeDatabase());
        return theConnectionInf.eUpdate(sql);
    }

    public int delete(ReceiptSequance of) throws Exception {
        String sql = "delete from " + dbObj.table
                + " where " + dbObj.pk_field
                + " = '" + of.getObjectId() + "'";
        return theConnectionInf.eUpdate(sql);
    }

    public Vector selectAll() throws Exception {
        String sql = "select * from " + dbObj.table;
        return eQuery(sql);
    }

    public Vector select(String txt) throws Exception {
        String sql = "select * from " + dbObj.table + " where "
                + dbObj.receipt_sequence_active + " = '1' and "
                + dbObj.receipt_sequence_ip + " like '%" + Gutil.CheckReservedWords(txt) + "%'";
        return eQuery(sql);
    }

    public ReceiptSequance selectByID(String id) throws Exception {
        String sql = "select * from " + dbObj.table
                + " where "
                + dbObj.pk_field
                + " = '" + id + "'";

        Vector v = eQuery(sql);
        if (v.isEmpty()) {
            return null;
        } else {
            return (ReceiptSequance) v.get(0);
        }
    }

    public ReceiptSequance selectByIP(String ip) throws Exception {
        String sql = "select * from " + dbObj.table
                + " where "
                + dbObj.receipt_sequence_active + " = '1' and "
                + dbObj.receipt_sequence_ip
                + " = '" + ip + "'";

        Vector v = eQuery(sql);
        if (v.isEmpty()) {
            return null;
        } else {
            return (ReceiptSequance) v.get(0);
        }
    }

    public ReceiptSequance selectBySP2(ReceiptSequance receiptSequance) throws Exception {
        String sql = "select * from " + dbObj.table
                + " where "
                + dbObj.receipt_sequence_active + " = '1' "
                + " and " + dbObj.receipt_sequence_ip
                + " = '" + receiptSequance.receipt_sequence_ip + "' and " + dbObj.pk_field
                + " <> '" + receiptSequance.getObjectId() + "'";

        Vector v = eQuery(sql);
        if (v.isEmpty()) {
            return null;
        } else {
            return (ReceiptSequance) v.get(0);
        }
    }

    public String updateSequence(String str) throws Exception {
        return updateSequence(str, true);
    }

    public String updateSequence(String str, boolean isUpdate) throws Exception {
        String currentYear = visitYearDB.selectCurrentYear();
        int value;
        int sd_value = 1;
        ReceiptSequance sd = selectByIP(str);
        try {
            sd_value = Integer.parseInt(sd.receipt_sequence_value);
        } catch (Exception e) {
        }
        value = sd_value;
        String ret_value = getSeqPattern(sd.receipt_sequence_pattern, value, currentYear);
        if (isUpdate) {
            //101106 henbe ����������������Ţ���Ѵ价������͹Ҥ��������͹���
            sd.receipt_sequence_value = String.valueOf(value + 1);
            update(sd);
        }
        return ret_value;
    }    

    public int resetSeqValue() throws Exception {
        String sql = "update b_receipt_sequence set receipt_sequence_value = '1'";
        return theConnectionInf.eUpdate(sql);
    }
    
    public String getSeqPattern(String pattern, int value) throws Exception {
        return getSeqPattern(pattern, value, visitYearDB.selectCurrentYear());
    }
    
     public String getSeqPattern(String pattern, int value, String currentYear) throws Exception {
        return ReceiptSequance.getDBText(pattern, value, currentYear);
    }

    public Vector eQuery(String sql) throws Exception {
        ReceiptSequance p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            p = new ReceiptSequance();
            p.setObjectId(rs.getString(dbObj.pk_field));
            p.receipt_sequence_active = rs.getString(dbObj.receipt_sequence_active);
            p.receipt_sequence_ip = rs.getString(dbObj.receipt_sequence_ip);
            p.receipt_sequence_pattern = rs.getString(dbObj.receipt_sequence_pattern);
            p.receipt_sequence_value = rs.getString(dbObj.receipt_sequence_value);
            p.begin_no = rs.getString(dbObj.begin_no);
            p.book_no = rs.getString(dbObj.book_no);
            p.end_no = rs.getString(dbObj.end_no);
            p.receipt_qty = rs.getString(dbObj.receipt_qty);
            list.add(p);
        }
        rs.close();
        return list;
    }

    public ReceiptSequance selectByBookNumber(ReceiptSequance receiptSequance) throws Exception {
        String sql = "select * from " + dbObj.table
                + " where "
                + dbObj.receipt_sequence_active + " = '1' "
                + " and " + dbObj.book_no
                + " = '" + receiptSequance.book_no + "' and " + dbObj.pk_field
                + " <> '" + receiptSequance.getObjectId() + "'";

        Vector v = eQuery(sql);
        if (v.isEmpty()) {
            return null;
        } else {
            return (ReceiptSequance) v.get(0);
        }
    }
    private static final Logger LOG = Logger.getLogger(ReceiptSequanceDB.class.getName());
}

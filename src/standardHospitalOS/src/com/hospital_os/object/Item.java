package com.hospital_os.object;

import com.hospital_os.usecase.connection.CommonInf;
import java.util.Date;

@SuppressWarnings("ClassWithoutLogger")
public class Item extends X39Persistent implements CommonInf {

    private static final long serialVersionUID = 1L;
    final private String idtable = "";
    public String item_id = "";
    public String common_name = "";
    public String trade_name = "";
    public String nick_name = "";
    public String local_name = "";
    public String item_group_code_category = "";
    public String item_group_code_billing = "";
    public String active = "";
    public String secret = "0";
    private String item_group_code_category_name = "";
    public String item_16_group = "";
    public String unit_pack53 = "";
    public String rp_lab_type = "";
    public String item_general_number = "";
    public String warning_icd10 = "0";
    public String b_specimen_id = "";
    /**
     * ������ group 1 = group 2 = detail
     */
    public String type = "";
    // 3.9.39
    public Date record_date_time = new Date();
    public String user_record = "";
    public Date modify_date_time = new Date();
    public String user_modify = "";
    public String f_item_lab_location_id = "";
    public String item_lab_duration = "0";
    public String item_lab_duration_day = "0";
    public String f_lab_atk_product_id;

    // REQ ItemPackage
    public String f_item_type_id = "1";
    public String item_package_use_hstock = null;

    public byte[] item_picture;

    public String item_not_use_to_order = "0";

    /**
     * @roseuid 3F658BBB036E
     */
    public Item() {
    }

    public static Item initConfig() {
        Item dbObj = new Item();
        dbObj.table = "b_item";
        dbObj.setObjectId("b_item_id");
        dbObj.pk_field = "b_item_id";
        dbObj.item_id = "item_number";
        dbObj.common_name = "item_common_name";
        dbObj.trade_name = "item_trade_name";
        dbObj.nick_name = "item_nick_name";
        dbObj.local_name = "item_local_name";
        dbObj.active = "item_active";
        dbObj.item_group_code_category = "b_item_subgroup_id";
        dbObj.item_group_code_billing = "b_item_billing_subgroup_id";
        dbObj.secret = "item_secret";
        dbObj.item_16_group = "b_item_16_group_id";
        dbObj.unit_pack53 = "item_unit_packing_qty";
        dbObj.rp_lab_type = "f_item_lab_type_id";
        dbObj.item_general_number = "item_general_number";
        dbObj.warning_icd10 = "doctor_warning_icd10";
        dbObj.b_specimen_id = "b_specimen_id";
        dbObj.user_record = "user_record";
        dbObj.user_modify = "user_modify";
        dbObj.f_item_lab_location_id = "f_item_lab_location_id";
        dbObj.item_lab_duration = "item_lab_duration";
        dbObj.item_lab_duration_day = "item_lab_duration_day";
        dbObj.f_lab_atk_product_id = "f_lab_atk_product_id";
        dbObj.f_item_type_id = "f_item_type_id";
        dbObj.item_package_use_hstock = "item_package_use_hstock";
        dbObj.item_not_use_to_order = "item_not_use_to_order";
        return dbObj;
    }

    public void setItemGroupCodeCategoryName(String name) {
        item_group_code_category_name = name;
    }

    public String getItemGroupCodeCategoryName() {
        return item_group_code_category_name;
    }

    @Override
    public String getCode() {
        return this.getObjectId();
    }

    @Override
    public String getName() {
        return this.common_name;
    }

    @Override
    public String toString() {
        return getName();
    }

    public void setSecret(boolean b) {
        if (b) {
            secret = "1";
        } else {
            secret = "0";
        }
    }

    public boolean getSecret() {
        return secret.equals("1");
    }

    public void setActive(boolean b) {
        if (b) {
            active = "1";
        } else {
            active = "0";
        }
    }

    public boolean getActive() {
        return active.equals("1");
    }

    public boolean getWarningICD10() {
        return warning_icd10.equals("1");
    }

    public void setWarningICD10(boolean b) {
        this.warning_icd10 = b ? "1" : "0";
    }

    @Override
    public X39Persistent getInstant(String[] strd) {
        Item item = new Item();
        item.setStringArray(strd);
        return item;
    }

    @Override
    public String[] getStringArray() {

        return new String[]{
            getObjectId(),
            item_id,
            common_name,
            trade_name,
            nick_name,
            local_name,
            active,
            item_group_code_category,
            item_group_code_billing,
            secret,
            item_16_group,
            unit_pack53,
            rp_lab_type,
            item_general_number,
            warning_icd10,
            b_specimen_id,
            user_record,
            user_modify,
            f_item_lab_location_id,
            item_lab_duration,
            item_lab_duration_day,
            f_lab_atk_product_id,
            f_item_type_id,
            item_package_use_hstock,
            item_not_use_to_order
        };
    }

    @Override
    public void setStringArray(String[] array) {
        int index = 0;
        setObjectId(array[index++]);
        item_id = array[index++];
        common_name = array[index++];
        trade_name = array[index++];
        nick_name = array[index++];
        local_name = array[index++];
        active = array[index++];
        item_group_code_category = array[index++];
        item_group_code_billing = array[index++];
        secret = array[index++];
        item_16_group = array[index++];
        unit_pack53 = array[index++];
        rp_lab_type = array[index++];
        item_general_number = array[index++];
        warning_icd10 = array[index++];
        b_specimen_id = array[index++];
        user_record = array[index++];
        user_modify = array[index++];
        f_item_lab_location_id = array[index++];
        item_lab_duration = array[index++];
        item_lab_duration_day = array[index++];
        f_lab_atk_product_id = array[index++];
        f_item_type_id = array[index++];
        item_package_use_hstock = array[index++];
        item_not_use_to_order = array[index++];
    }

    @Override
    public String getIdTable() {
        return this.idtable;
    }

    public void setLabDuration(boolean b) {
        item_lab_duration = b ? "1" : "0";
    }

    public boolean getLabDuration() {
        return item_lab_duration.equals("1");
    }

    public boolean getItemNotUseToOrder() {
        return item_not_use_to_order.equals("1");
    }
}

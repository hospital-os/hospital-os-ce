ALTER TABLE t_drugfund_billing_receipt ADD COLUMN f_payment_type_id character varying(255) not null default '1';
ALTER TABLE t_drugfund_billing_receipt ADD COLUMN b_bank_info_id character varying(255);
ALTER TABLE t_drugfund_billing_receipt ADD COLUMN b_credit_card_type_id character varying(255);
ALTER TABLE t_drugfund_billing_receipt ADD COLUMN account_name character varying(255);
ALTER TABLE t_drugfund_billing_receipt ADD COLUMN account_number character varying(255);
ALTER TABLE t_drugfund_billing_receipt ADD COLUMN transaction_date date;
-- ประเภทการรับชำระ
update t_drugfund_billing_receipt set f_payment_type_id = '1' 
from t_drugfund_cash_type where t_drugfund_cash_type.drugfund_cash_type_description = 'เงินสด' 
and t_drugfund_cash_type.t_drugfund_billing_receipt_id = t_drugfund_billing_receipt.t_drugfund_billing_receipt_id;

update t_drugfund_billing_receipt set f_payment_type_id = '2' 
from t_drugfund_cash_type where t_drugfund_cash_type.drugfund_cash_type_description = 'บัตรเครดิต' 
and t_drugfund_cash_type.t_drugfund_billing_receipt_id = t_drugfund_billing_receipt.t_drugfund_billing_receipt_id;

update t_drugfund_billing_receipt set f_payment_type_id = '3' 
from t_drugfund_cash_type where t_drugfund_cash_type.drugfund_cash_type_description = 'เช็ค' 
and t_drugfund_cash_type.t_drugfund_billing_receipt_id = t_drugfund_billing_receipt.t_drugfund_billing_receipt_id;

update t_drugfund_billing_receipt set f_payment_type_id = '4' 
from t_drugfund_cash_type where t_drugfund_cash_type.drugfund_cash_type_description = 'เงินโอน' 
and t_drugfund_cash_type.t_drugfund_billing_receipt_id = t_drugfund_billing_receipt.t_drugfund_billing_receipt_id;

-- ธนาคาร
update t_drugfund_billing_receipt set account_name = b_bank_info.b_bank_info_id 
from b_bank_info 
inner join t_drugfund_cash_type on t_drugfund_cash_type.drugfund_cash_type_credit_bank = b_bank_info.code
where t_drugfund_cash_type.drugfund_cash_type_description = 'บัตรเครดิต' 
and t_drugfund_cash_type.t_drugfund_billing_receipt_id = t_drugfund_billing_receipt.t_drugfund_billing_receipt_id;

update t_drugfund_billing_receipt set b_bank_info_id = b_bank_info.b_bank_info_id 
from b_bank_info 
inner join t_drugfund_cash_type on t_drugfund_cash_type.drugfund_cash_type_cheque_bank = b_bank_info.code
where t_drugfund_cash_type.drugfund_cash_type_description = 'เช็ค'
and t_drugfund_cash_type.t_drugfund_billing_receipt_id = t_drugfund_billing_receipt.t_drugfund_billing_receipt_id;

update t_drugfund_billing_receipt set b_bank_info_id = b_bank_info.b_bank_info_id 
from b_bank_info 
inner join t_drugfund_cash_type on t_drugfund_cash_type.drugfund_cash_type_transfer_bank = b_bank_info.code
where t_drugfund_cash_type.drugfund_cash_type_description = 'เงินโอน'
and t_drugfund_cash_type.t_drugfund_billing_receipt_id = t_drugfund_billing_receipt.t_drugfund_billing_receipt_id;

-- ชื่อบัญชื่อ/ชื่อหน้าบัตร
update t_drugfund_billing_receipt set account_name = t_drugfund_cash_type.drugfund_cash_type_credit_cardname
from t_drugfund_cash_type where t_drugfund_cash_type.t_drugfund_billing_receipt_id = t_drugfund_billing_receipt.t_drugfund_billing_receipt_id;

-- เลขที่บัญชี/บัตรเครดิต
update t_drugfund_billing_receipt set account_number = t_drugfund_cash_type.drugfund_cash_type_number
from t_drugfund_cash_type where t_drugfund_cash_type.t_drugfund_billing_receipt_id = t_drugfund_billing_receipt.t_drugfund_billing_receipt_id;

-- วันที่ทำรายการ
update t_drugfund_billing_receipt set transaction_date = to_date(to_char(to_number(substring(t_drugfund_cash_type.drugfund_cash_type_cheque_date_time, 0,5),'9999') - 543, '9999') || substring(t_drugfund_cash_type.drugfund_cash_type_cheque_date_time, 5,6), 'YYYY-MM-DD')
from t_drugfund_cash_type 
where t_drugfund_cash_type.drugfund_cash_type_description = 'เช็ค'  and t_drugfund_cash_type.drugfund_cash_type_cheque_date_time is not null and t_drugfund_cash_type.drugfund_cash_type_cheque_date_time <> ''
and t_drugfund_cash_type.t_drugfund_billing_receipt_id = t_drugfund_billing_receipt.t_drugfund_billing_receipt_id;

update t_drugfund_billing_receipt set transaction_date = to_date(to_char(to_number(substring(t_drugfund_cash_type.drugfund_cash_type_transfer_date_time, 0,5),'9999') - 543, '9999') || substring(t_drugfund_cash_type.drugfund_cash_type_transfer_date_time, 5,6), 'YYYY-MM-DD')
from t_drugfund_cash_type 
where t_drugfund_cash_type.drugfund_cash_type_description = 'เงินโอน'  and t_drugfund_cash_type.drugfund_cash_type_transfer_date_time is not null and t_drugfund_cash_type.drugfund_cash_type_transfer_date_time <> ''
and t_drugfund_cash_type.t_drugfund_billing_receipt_id = t_drugfund_billing_receipt.t_drugfund_billing_receipt_id;

-- unused
--drop table t_drugfund_cash_type;

INSERT INTO s_drugfund_version VALUES ('9750000000004', '4', 'Drugfund, Community Edition', '2.5.100214', '1.2.100214', to_char(current_timestamp, 'YYYY-MM-DD HH24:MI:SS'));

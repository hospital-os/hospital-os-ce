/*
 * PrintSubject.java
 *
 * Created on 2 �á�Ҥ� 2548, 15:21 �.
 */
package com.hosv3.subject;

import com.hosv3.usecase.transaction.ManagePrintResp;
import java.util.Vector;

/**
 *
 * @author kingland
 */
@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public class PrintSubject implements ManagePrintResp {

    private Vector vPrintObserv;

    /**
     * Creates a new instance of PrintSubject
     */
    public PrintSubject() {
        vPrintObserv = new Vector();
    }

    public void removeAttach() {
        vPrintObserv.removeAllElements();

    }

    public void attachManagePrint(ManagePrintResp o) {
        vPrintObserv.add(o);
    }

    @Override
    public void notifyPrintDrugSticker(String msg, int status) {
        for (int i = 0, size = vPrintObserv.size(); i < size; i++) {
            ((ManagePrintResp) vPrintObserv.get(i)).notifyPrintDrugSticker(msg, status);
        }
    }

    @Override
    public void notifyPrintOPDCard(String msg, int status) {
        for (int i = 0, size = vPrintObserv.size(); i < size; i++) {
            ((ManagePrintResp) vPrintObserv.get(i)).notifyPrintOPDCard(msg, status);
        }
    }

    @Override
    public void notifyPrintAppointmentList(String msg, int status) {
        for (int i = 0, size = vPrintObserv.size(); i < size; i++) {
            ((ManagePrintResp) vPrintObserv.get(i)).notifyPrintAppointmentList(msg, status);
        }
    }

    @Override
    public void notifyPreviewAppointmentList(String msg, int status) {
        for (int i = 0, size = vPrintObserv.size(); i < size; i++) {
            ((ManagePrintResp) vPrintObserv.get(i)).notifyPreviewAppointmentList(msg, status);
        }
    }

    @Override
    public void notifyPrintChronicList(String msg, int status) {
        for (int i = 0, size = vPrintObserv.size(); i < size; i++) {
            ((ManagePrintResp) vPrintObserv.get(i)).notifyPrintChronicList(msg, status);
        }
    }

    @Override
    public void notifyPriviewChronicList(String msg, int status) {
        for (int i = 0, size = vPrintObserv.size(); i < size; i++) {
            ((ManagePrintResp) vPrintObserv.get(i)).notifyPriviewChronicList(msg, status);
        }
    }

    @Override
    public void notifyPreviewSelectDrugList(String msg, int status) {
        for (int i = 0, size = vPrintObserv.size(); i < size; i++) {
            ((ManagePrintResp) vPrintObserv.get(i)).notifyPreviewSelectDrugList(msg, status);
        }
    }

    @Override
    public void notifyPrintSelectDrugList(String msg, int status) {
        for (int i = 0, size = vPrintObserv.size(); i < size; i++) {
            ((ManagePrintResp) vPrintObserv.get(i)).notifyPrintSelectDrugList(msg, status);
        }
    }

    @Override
    public void notifyPrintSumByBillingGroup(String msg, int status) {
        for (int i = 0, size = vPrintObserv.size(); i < size; i++) {
            ((ManagePrintResp) vPrintObserv.get(i)).notifyPrintSumByBillingGroup(msg, status);
        }
    }

    @Override
    public void notifyPreviewSumByBillingGroup(String msg, int status) {
        for (int i = 0, size = vPrintObserv.size(); i < size; i++) {
            ((ManagePrintResp) vPrintObserv.get(i)).notifyPreviewSumByBillingGroup(msg, status);
        }
    }
}

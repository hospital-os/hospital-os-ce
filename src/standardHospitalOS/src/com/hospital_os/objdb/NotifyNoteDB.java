/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.NotifyNote;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class NotifyNoteDB {

    private ConnectionInf theConnectionInf;
    private NotifyNote dbObj;
    final public String idtable = "note1";/*
     * "178";
     */


    /**
     * @param ConnectionInf db
     * @roseuid 3F65897F0326
     */
    public NotifyNoteDB(ConnectionInf db) {
        theConnectionInf = db;
        dbObj = new NotifyNote();
        this.initConfig();
    }

    private NotifyNote initConfig() {
        dbObj.table = "t_notify_note";
        dbObj.pk_field = "t_notify_note_id";
        dbObj.patient_hn = "t_patient_hn";
        dbObj.visit_id_rec = "t_visit_id_rec";
        dbObj.visit_id_last_view = "t_visit_id_last_view";
        dbObj.notify_type_id = "f_notify_type_id";
        dbObj.note_subject = "note_subject";
        dbObj.note_detail = "note_detail";
        dbObj.active = "active";
        dbObj.rec_staff = "rec_staff";
        dbObj.rec_datetime = "rec_datetime";
        dbObj.mod_datetime = "mod_datetime";
        dbObj.del_datetime = "del_datetime";
        dbObj.noter = "noter";
        dbObj.show_on_patient = "show_on_patient";
        dbObj.show_on_visit = "show_on_visit";
        dbObj.other_remover = "other_remover";
        dbObj.show_with_custom = "show_with_custom";
        dbObj.custom_viewer = "custom_viewer";
        dbObj.auten_viewer = "authen_role_viewer";
        return dbObj;
    }

    public int insert(NotifyNote p) throws Exception {
        p.generateOID(idtable);
        StringBuilder sql = new StringBuilder("insert into ");
        sql.append(dbObj.table);
        sql.append(" (");
        sql.append(dbObj.pk_field);
        sql.append(" ,");
        sql.append(dbObj.patient_hn);
        sql.append(" ,");
        sql.append(dbObj.visit_id_rec);
        sql.append(" ,");
        sql.append(dbObj.notify_type_id);
        sql.append(" ,");
        sql.append(dbObj.note_subject);
        sql.append(" ,");
        sql.append(dbObj.note_detail);
        sql.append(" ,");
        sql.append(dbObj.active);
        sql.append(" ,");
        sql.append("notify_count");
        sql.append(" ,");
        sql.append(dbObj.rec_staff);
        sql.append(" ,");
        sql.append(dbObj.rec_datetime);
        sql.append(" ,");
        sql.append(dbObj.show_on_patient);
        sql.append(" ,");
        sql.append(dbObj.show_on_visit);
        sql.append(" ,");
        sql.append(dbObj.other_remover);
        sql.append(" ,");
        sql.append(dbObj.show_with_custom);
        sql.append(" ,");
        sql.append(dbObj.custom_viewer);
        sql.append(" ,");
        sql.append(dbObj.auten_viewer);
        sql.append(" ) values ('");
        sql.append(p.getObjectId());
        sql.append("','");
        sql.append(p.patient_hn);
        sql.append("','");
        sql.append(p.visit_id_rec);
        sql.append("','");
        sql.append(p.notify_type_id);
        sql.append("','");
        sql.append(p.note_subject);
        sql.append("','");
        sql.append(p.note_detail);
        sql.append("','");
        sql.append(p.active);
        sql.append("',");
        sql.append(p.notify_count);
        sql.append(",'");
        sql.append(p.rec_staff);
        sql.append("','");
        sql.append(p.rec_datetime);
        sql.append("','");
        sql.append(p.show_on_patient);
        sql.append("','");
        sql.append(p.show_on_visit);
        sql.append("','");
        sql.append(p.other_remover);
        sql.append("','");
        sql.append(p.show_with_custom);
        sql.append("','");
        sql.append(p.custom_viewer);
        sql.append("','");
        sql.append(p.auten_viewer);
        sql.append("')");
        return theConnectionInf.eUpdate(sql.toString());
    }

    public int update(NotifyNote p) throws Exception {
        StringBuilder sql = new StringBuilder("update ");
        sql.append(dbObj.table);
        sql.append(" set ");
        sql.append(dbObj.notify_type_id);
        sql.append("='");
        sql.append(p.notify_type_id);
        sql.append("', ");
        sql.append(dbObj.note_subject);
        sql.append("='");
        sql.append(p.note_subject);
        sql.append("', ");
        sql.append(dbObj.note_detail);
        sql.append("='");
        sql.append(p.note_detail);
        sql.append("', ");
        sql.append(dbObj.mod_datetime);
        sql.append("='");
        sql.append(p.mod_datetime);
        sql.append("', ");
        sql.append(dbObj.show_on_patient);
        sql.append("='");
        sql.append(p.show_on_patient);
        sql.append("', ");
        sql.append(dbObj.show_on_visit);
        sql.append("='");
        sql.append(p.show_on_visit);
        sql.append("', ");
        sql.append(dbObj.other_remover);
        sql.append("='");
        sql.append(p.other_remover);
        sql.append("', ");
        sql.append(dbObj.show_with_custom);
        sql.append("='");
        sql.append(p.show_with_custom);
        sql.append("', ");
        sql.append(dbObj.custom_viewer);
        sql.append("='");
        sql.append(p.custom_viewer);
        sql.append("', ");
        sql.append(dbObj.auten_viewer);
        sql.append("='");
        sql.append(p.auten_viewer);
        sql.append("' ");
        sql.append(" where ");
        sql.append(dbObj.pk_field);
        sql.append("='");
        sql.append(p.getObjectId());
        sql.append("'");
        return theConnectionInf.eUpdate(sql.toString());
    }

    public int inactive(NotifyNote p) throws Exception {
        StringBuilder sql = new StringBuilder("update ");
        sql.append(dbObj.table);
        sql.append(" set ");
        sql.append(dbObj.active);
        sql.append("='");
        sql.append(p.active);
        sql.append("', ");
        sql.append(dbObj.del_datetime);
        sql.append("='");
        sql.append(p.del_datetime);
        sql.append("' ");
        sql.append(" where ");
        sql.append(dbObj.pk_field);
        sql.append("='");
        sql.append(p.getObjectId());
        sql.append("'");
        return theConnectionInf.eUpdate(sql.toString());
    }

    public int countNotify(NotifyNote p) throws Exception {
        StringBuilder sql = new StringBuilder("update ");
        sql.append(dbObj.table);
        sql.append(" set ");
        sql.append("notify_count");
        sql.append("=");
        sql.append(p.notify_count);
        sql.append(" ");
        sql.append(" where ");
        sql.append(dbObj.pk_field);
        sql.append("='");
        sql.append(p.getObjectId());
        sql.append("'");
        return theConnectionInf.eUpdate(sql.toString());
    }

    public NotifyNote getById(String stringId) throws Exception {
        String sql = "SELECT * "
                + "FROM t_notify_note "
                + "Where t_notify_note.t_notify_note_id = '" + stringId + "' ";
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        NotifyNote object = null;
        if (rs.next()) {
            object = new NotifyNote();
            object.setObjectId(rs.getString(dbObj.pk_field));
            object.patient_hn = rs.getString(dbObj.patient_hn);
            object.visit_id_rec = rs.getString(dbObj.visit_id_rec);
            object.visit_id_last_view = rs.getString(dbObj.visit_id_last_view);
            object.notify_type_id = rs.getString(dbObj.notify_type_id);
            object.note_subject = rs.getString(dbObj.note_subject);
            object.note_detail = rs.getString(dbObj.note_detail);
            object.active = rs.getString(dbObj.active);
            object.rec_staff = rs.getString(dbObj.rec_staff);
            object.rec_datetime = rs.getString(dbObj.rec_datetime);
            object.mod_datetime = rs.getString(dbObj.mod_datetime);
            object.del_datetime = rs.getString(dbObj.del_datetime);
            object.show_on_patient = rs.getString(dbObj.show_on_patient);
            object.show_on_visit = rs.getString(dbObj.show_on_visit);
            object.other_remover = rs.getString(dbObj.other_remover);
            object.show_with_custom = rs.getString(dbObj.show_with_custom);
            object.custom_viewer = rs.getString(dbObj.custom_viewer);
            object.auten_viewer = rs.getString(dbObj.auten_viewer);
        }
        rs.close();

        return object;
    }

    public List<NotifyNote> listByHn(String hn) throws Exception {
        String sql = "SELECT * "
                + "FROM t_notify_note "
                + "Where t_notify_note.t_patient_hn = '" + hn + "' "
                + "Order by t_notify_note.rec_datetime desc";
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        List<NotifyNote> list = new ArrayList<NotifyNote>();
        while (rs.next()) {
            NotifyNote object = new NotifyNote();
            object.setObjectId(rs.getString(dbObj.pk_field));
            object.patient_hn = rs.getString(dbObj.patient_hn);
            object.visit_id_rec = rs.getString(dbObj.visit_id_rec);
            object.visit_id_last_view = rs.getString(dbObj.visit_id_last_view);
            object.notify_type_id = rs.getString(dbObj.notify_type_id);
            object.note_subject = rs.getString(dbObj.note_subject);
            object.note_detail = rs.getString(dbObj.note_detail);
            object.active = rs.getString(dbObj.active);
            object.rec_staff = rs.getString(dbObj.rec_staff);
            object.rec_datetime = rs.getString(dbObj.rec_datetime);
            object.mod_datetime = rs.getString(dbObj.mod_datetime);
            object.del_datetime = rs.getString(dbObj.del_datetime);
            object.show_on_patient = rs.getString(dbObj.show_on_patient);
            object.show_on_visit = rs.getString(dbObj.show_on_visit);
            object.other_remover = rs.getString(dbObj.other_remover);
            object.show_with_custom = rs.getString(dbObj.show_with_custom);
            object.custom_viewer = rs.getString(dbObj.custom_viewer);
            object.auten_viewer = rs.getString(dbObj.auten_viewer);
            list.add(object);
        }
        rs.close();

        return list;
    }

    public List<NotifyNote> listByHn2(String hn) throws Exception {
        String sql = "select t_notify_note.* \n"
                + ",case when f_patient_prefix.f_patient_prefix_id is not null then \n"
                + "case when f_patient_prefix.f_patient_prefix_id = '000' then '' \n"
                + "else f_patient_prefix.patient_prefix_description end else '' end\n"
                + "|| t_person.person_firstname || ' ' || t_person.person_lastname as noter \n"
                + "from t_notify_note \n"
                + "inner join b_employee on b_employee.b_employee_id = t_notify_note.rec_staff \n"
                + "inner join t_person on t_person.t_person_id = b_employee.t_person_id\n"
                + "left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_person.f_prefix_id\n"
                + "Where t_notify_note.t_patient_hn = '" + hn + "' \n"
                + "and t_notify_note.active = '1' \n"
                + "Order by t_notify_note.f_notify_type_id, t_notify_note.rec_datetime desc";
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        List<NotifyNote> list = new ArrayList<NotifyNote>();
        while (rs.next()) {
            NotifyNote object = new NotifyNote();
            object.setObjectId(rs.getString(dbObj.pk_field));
            object.patient_hn = rs.getString(dbObj.patient_hn);
            object.visit_id_rec = rs.getString(dbObj.visit_id_rec);
            object.visit_id_last_view = rs.getString(dbObj.visit_id_last_view);
            object.notify_type_id = rs.getString(dbObj.notify_type_id);
            object.note_subject = rs.getString(dbObj.note_subject);
            object.note_detail = rs.getString(dbObj.note_detail);
            object.active = rs.getString(dbObj.active);
            object.rec_staff = rs.getString(dbObj.rec_staff);
            object.rec_datetime = rs.getString(dbObj.rec_datetime);
            object.mod_datetime = rs.getString(dbObj.mod_datetime);
            object.del_datetime = rs.getString(dbObj.del_datetime);
            object.noter = rs.getString(dbObj.noter);
            object.show_on_patient = rs.getString(dbObj.show_on_patient);
            object.show_on_visit = rs.getString(dbObj.show_on_visit);
            object.other_remover = rs.getString(dbObj.other_remover);
            object.show_with_custom = rs.getString(dbObj.show_with_custom);
            object.custom_viewer = rs.getString(dbObj.custom_viewer);
            object.auten_viewer = rs.getString(dbObj.auten_viewer);
            list.add(object);
        }
        rs.close();

        return list;
    }

    public List<NotifyNote> listByHnAbdVisitId(String hn, String visitId,
            int mode, String empId, String authenID) throws Exception {

        String sql = "select t_notify_note.* \n"
                + ",case when f_patient_prefix.f_patient_prefix_id is not null then \n"
                + "case when f_patient_prefix.f_patient_prefix_id = '000' then '' \n"
                + "else f_patient_prefix.patient_prefix_description end else '' end\n"
                + "|| t_person.person_firstname || ' ' || t_person.person_lastname as noter \n"
                + "from t_notify_note \n"
                + "inner join b_employee on b_employee.b_employee_id = t_notify_note.rec_staff \n"
                + "inner join t_person on t_person.t_person_id = b_employee.t_person_id\n"
                + "left join f_patient_prefix on f_patient_prefix.f_patient_prefix_id = t_person.f_prefix_id \n"
                + "Where t_notify_note.t_patient_hn = '" + hn + "' \n"
                + "and t_notify_note.active = '1' \n"
                + "and t_notify_note.f_notify_type_id = '2' \n";
        switch (mode) {
            case 1: // when select patient
                sql += "and t_notify_note.show_on_patient = '1' \n";
                break;
            case 2: // when select visit
                sql += "and t_notify_note.show_on_visit = '1' \n";
                break;
            case 3: // when select patient with custom
                sql += "and (t_notify_note.show_with_custom = '1' and '" + empId + "' = any(string_to_array(custom_viewer, ',')) or '" + authenID + "' = any(string_to_array(authen_role_viewer, ','))) \n";
                break;
            default:
                break;
        }
        if (visitId != null && !visitId.isEmpty()) {
            sql += "or t_notify_note.t_notify_note_id in "
                    + "(select t_notify_note.t_notify_note_id from t_notify_note where t_notify_note.t_patient_hn = '" + hn + "' \n"
                    + "and t_notify_note.active = '1' \n"
                    + "and t_notify_note.f_notify_type_id = '1' \n"
                    + "and (t_notify_note.t_visit_id_last_view is null or t_notify_note.t_visit_id_last_view = '' or t_notify_note.t_visit_id_last_view = '" + visitId + "')) \n";
        }
        sql += "Order by t_notify_note.f_notify_type_id, t_notify_note.rec_datetime desc";
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        List<NotifyNote> list = new ArrayList<NotifyNote>();
        while (rs.next()) {
            NotifyNote object = new NotifyNote();
            object.setObjectId(rs.getString(dbObj.pk_field));
            object.patient_hn = rs.getString(dbObj.patient_hn);
            object.visit_id_rec = rs.getString(dbObj.visit_id_rec);
            object.visit_id_last_view = rs.getString(dbObj.visit_id_last_view);
            object.notify_type_id = rs.getString(dbObj.notify_type_id);
            object.note_subject = rs.getString(dbObj.note_subject);
            object.note_detail = rs.getString(dbObj.note_detail);
            object.active = rs.getString(dbObj.active);
            object.rec_staff = rs.getString(dbObj.rec_staff);
            object.rec_datetime = rs.getString(dbObj.rec_datetime);
            object.mod_datetime = rs.getString(dbObj.mod_datetime);
            object.del_datetime = rs.getString(dbObj.del_datetime);
            object.noter = rs.getString(dbObj.noter);
            object.show_on_patient = rs.getString(dbObj.show_on_patient);
            object.show_on_visit = rs.getString(dbObj.show_on_visit);
            object.other_remover = rs.getString(dbObj.other_remover);
            object.show_with_custom = rs.getString(dbObj.show_with_custom);
            object.custom_viewer = rs.getString(dbObj.custom_viewer);
            object.auten_viewer = rs.getString(dbObj.auten_viewer);
            list.add(object);
        }
        rs.close();
        if (visitId != null && !visitId.isEmpty()) {
            for (NotifyNote object : list) {
                sql = "update t_notify_note set t_visit_id_last_view = '" + visitId + "' where t_notify_note_id = '" + object.getObjectId() + "'";
                theConnectionInf.eUpdate(sql);
            }
        }

        return list;
    }

    public List<Object[]> listNotifyType() throws Exception {
        String sql = "SELECT * FROM f_notify_type order by f_notify_type_id";
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        List<Object[]> list = new ArrayList<Object[]>();

        while (rs.next()) {
            Object[] object = new Object[]{rs.getString("f_notify_type_id"), rs.getString("description")};
            list.add(object);
        }
        rs.close();

        return list;
    }
}

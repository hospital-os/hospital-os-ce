/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.DrugTMT;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.ComboFix;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class DrugTMTDB {

    public ConnectionInf connectionInf;
    final public String tableId = "858";

    public DrugTMTDB(ConnectionInf db) {
        connectionInf = db;
    }

    public int insert(DrugTMT obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO b_drug_tmt(\n"
                    + "            tpucode, fsn, manufacturer,changedate)\n"
                    + "    VALUES (?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.getObjectId());
            preparedStatement.setString(index++, obj.fsn);
            preparedStatement.setString(index++, obj.manufacturer);
            preparedStatement.setString(index++, obj.changedate);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(DrugTMT obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE b_drug_tmt\n"
                    + "   SET fsn=?, \n"
                    + "       manufacturer=?, changedate=?\n"
                    + " WHERE tpucode=?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.fsn);
            preparedStatement.setString(index++, obj.manufacturer);
            preparedStatement.setString(index++, obj.changedate);
            preparedStatement.setString(index++, obj.getObjectId());
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(DrugTMT obj) throws Exception {
        return delete(obj.getObjectId());
    }
    
    public int delete(String tpucode) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM b_drug_tmt\n");
            sql.append(" WHERE tpucode = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, tpucode);
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public DrugTMT selectById(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_drug_tmt\n"
                    + "where tpucode = ?\n";
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, id);
            List<DrugTMT> executeQuery = executeQuery(preparedStatement);
            return executeQuery.isEmpty() ? null : executeQuery.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<DrugTMT> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_drug_tmt order by fsn";
            preparedStatement = connectionInf.ePQuery(sql);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<DrugTMT> listByKeyword(String keyword) throws Exception {
        return listByKeyword(keyword, null);
    }

    public List<DrugTMT> listByKeyword(String keyword, String manufacturer) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_drug_tmt\n"
                    + "where (tpucode ilike ? or fsn ilike ?)\n";
            if (manufacturer != null && !manufacturer.isEmpty()) {
                sql += "and manufacturer = ?\n";
            }
            sql += "order by tpucode";
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, "%" + keyword + "%");
            preparedStatement.setString(index++, "%" + keyword + "%");
            if (manufacturer != null && !manufacturer.isEmpty()) {
                preparedStatement.setString(index++, manufacturer);
            }
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<String> listByKeywordManufacturer(String keyword) throws Exception {
        List<String> list = new ArrayList<String>();
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try {
            String sql = "select manufacturer from b_drug_tmt\n"
                    + "where manufacturer ilike ?\n"
                    + "group by manufacturer\n"
                    + "order by manufacturer";
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, "%" + keyword + "%");
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                list.add(rs.getString("manufacturer"));
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<DrugTMT> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<DrugTMT> list = new ArrayList<DrugTMT>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                DrugTMT obj = new DrugTMT();
                obj.setObjectId(rs.getString("tpucode"));
                obj.manufacturer = rs.getString("manufacturer");
                obj.fsn = rs.getString("fsn");
                obj.changedate = rs.getString("changedate");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (DrugTMT obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }

    public List<CommonInf> getComboboxDatasource(String keyword) throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (DrugTMT obj : listByKeyword(keyword)) {
            list.add((CommonInf) obj);
        }
        return list;
    }

    public List<ComboFix> getComboboxDatasourceManufacturer() throws Exception {
        return getComboboxDatasourceManufacturer("");
    }

    public List<ComboFix> getComboboxDatasourceManufacturer(String keyword) throws Exception {
        List<ComboFix> list = new ArrayList<ComboFix>();
        for (String obj : listByKeywordManufacturer(keyword)) {
            ComboFix comboFix = new ComboFix(obj, obj);
            list.add(comboFix);
        }
        return list;
    }
}

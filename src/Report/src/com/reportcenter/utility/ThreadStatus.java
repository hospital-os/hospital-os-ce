/*
 * TestThread.java
 *
 * Created on 17 �Զع�¹ 2548, 9:06 �.
 */
package com.reportcenter.utility;

import com.hospital_os.utility.Constant;
import java.awt.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

/**
 *
 * @author henbe
 */
public class ThreadStatus extends Thread {

    JLabel jLabelStatus;
    JFrame jf;

    /**
     * Creates a new instance of TestThread
     */
    public ThreadStatus(JFrame jf, JLabel us) {
        jLabelStatus = us;
        this.jf = jf;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(Constant.TIME_UPDATE_STATUS);
            jLabelStatus.setBackground(Color.GRAY);
            Thread.sleep(5000);
            jLabelStatus.setText("");
        } catch (Exception e) {
            LOG.log(Level.SEVERE, e.getMessage(), e);
        }

    }
    private static final Logger LOG = Logger.getLogger(ThreadStatus.class.getName());
}

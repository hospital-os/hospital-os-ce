/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.BankInfo;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class BankInfoDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "812";

    public BankInfoDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public int insert(BankInfo obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO b_bank_info(\n"
                    + "            b_bank_info_id, code, description, active, user_record, record_datetime, user_modify, modify_datetime)\n"
                    + "    VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getGenID(tableId));
            preparedStatement.setString(2, obj.code);
            preparedStatement.setString(3, obj.description);
            preparedStatement.setString(4, obj.active);
            preparedStatement.setString(5, obj.user_record);
            preparedStatement.setString(6, obj.record_datetime);
            preparedStatement.setString(7, obj.user_modify);
            preparedStatement.setString(8, obj.modify_datetime);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(BankInfo obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE b_bank_info\n");
            sql.append("   SET code=?, description=?, \n");
            sql.append("       active=?, user_modify=?, modify_datetime=?\n");
            sql.append(" WHERE b_bank_info_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.code);
            preparedStatement.setString(2, obj.description);
            preparedStatement.setString(3, obj.active);
            preparedStatement.setString(4, obj.user_modify);
            preparedStatement.setString(5, obj.modify_datetime);
            preparedStatement.setString(6, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
    
    public int delete(BankInfo obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM b_bank_info\n");
            sql.append(" WHERE b_bank_info_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int inactive(BankInfo obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE b_bank_info\n");
            sql.append("   SET active=?, user_modify=?, modify_datetime=?\n");
            sql.append(" WHERE b_bank_info_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, "0");
            preparedStatement.setString(2, obj.user_modify);
            preparedStatement.setString(3, obj.modify_datetime);
            preparedStatement.setString(4, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
    
    public BankInfo select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_bank_info where b_bank_info_id = ? order by code, description";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<BankInfo> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
    
    public BankInfo selectByCode(String code) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_bank_info where code = ? order by code, description";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, code);
            List<BankInfo> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<BankInfo> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_bank_info where active = '1' order by code, description";
            preparedStatement = connectionInf.ePQuery(sql);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
    
    public List<BankInfo> listByKeyword(String keyword, String active) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_bank_info where active = ? and (upper(code) like upper(?) or upper(description) like upper(?)) order by code, description";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, active);
            preparedStatement.setString(2, "%" + (keyword == null || keyword.isEmpty() ? "" : keyword) + "%");
            preparedStatement.setString(3, "%" + (keyword == null || keyword.isEmpty() ? "" : keyword) + "%");
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<BankInfo> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<BankInfo> list = new ArrayList<BankInfo>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                BankInfo obj = new BankInfo();
                obj.setObjectId(rs.getString("b_bank_info_id"));
                obj.code = rs.getString("code");
                obj.description = rs.getString("description");
                obj.active = rs.getString("active");
                obj.user_record = rs.getString("user_record");
                obj.record_datetime = rs.getString("record_datetime");
                obj.user_modify = rs.getString("user_modify");
                obj.modify_datetime = rs.getString("modify_datetime");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (BankInfo obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }
    
    public List<CommonInf> getComboboxDatasource(String keyword) throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (BankInfo obj : listByKeyword(keyword, "1")) {
            list.add((CommonInf) obj);
        }
        return list;
    }

    
}

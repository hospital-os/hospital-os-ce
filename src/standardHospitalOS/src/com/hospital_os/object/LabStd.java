/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author LionHeart
 */
public class LabStd extends Persistent implements CommonInf {

    public String f_lab_name = "";
    public String lab_set = "";
    public String lab_type = "";

    public String getCode() {
        return getObjectId();
    }

    public String getName() {
        return f_lab_name;
    }

    public String toString() {
        return f_lab_name;
    }
}

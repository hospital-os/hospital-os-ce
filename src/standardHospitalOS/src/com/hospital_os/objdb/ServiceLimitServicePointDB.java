/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.ServiceLimitServicePoint;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author sompr
 */
public class ServiceLimitServicePointDB {

    public ConnectionInf connectionInf;
    final public String tableId = "690";

    public ServiceLimitServicePointDB(ConnectionInf db) {
        connectionInf = db;
    }

    public int insert(ServiceLimitServicePoint obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO b_service_limit_service_point(\n"
                    + "            b_service_limit_service_point_id, time_start, time_end, limit_appointment, limit_walkin, b_service_point_id )\n"
                    + "    VALUES (?, ?, ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getGenID(tableId));
            preparedStatement.setTime(2, obj.time_start != null ? new java.sql.Time(obj.time_start.getTime()) : null);
            preparedStatement.setTime(3, obj.time_end != null ? new java.sql.Time(obj.time_end.getTime()) : null);
            preparedStatement.setInt(4, obj.limit_appointment);
            preparedStatement.setInt(5, obj.limit_walkin);
            preparedStatement.setString(6, obj.b_service_point_id);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(ServiceLimitServicePoint obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE b_service_limit_service_point\n"
                    + "   SET b_service_point_id=?, time_start=?, \n"
                    + "       time_end=?, limit_appointment=?, limit_walkin=?\n"
                    + " WHERE b_service_limit_service_point_id=?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.b_service_point_id);
            preparedStatement.setTime(2, obj.time_start != null ? new java.sql.Time(obj.time_start.getTime()) : null);
            preparedStatement.setTime(3, obj.time_end != null ? new java.sql.Time(obj.time_end.getTime()) : null);
            preparedStatement.setInt(4, obj.limit_appointment);
            preparedStatement.setInt(5, obj.limit_walkin);
            preparedStatement.setString(6, obj.getObjectId());
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(ServiceLimitServicePoint obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM b_service_limit_service_point\n");
            sql.append(" WHERE b_service_limit_service_point_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public ServiceLimitServicePoint selectById(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select b_service_limit_service_point.* \n"
                    + ", b_service_point.service_point_description\n"
                    + "from b_service_limit_service_point \n"
                    + "inner join b_service_point on b_service_point.b_service_point_id = b_service_limit_service_point.b_service_point_id \n"
                    + " where b_service_limit_clinic_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<ServiceLimitServicePoint> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<ServiceLimitServicePoint> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select b_service_limit_service_point.* \n"
                    + ", b_service_point.service_point_description\n"
                    + "from b_service_limit_service_point \n"
                    + "inner join b_service_point on b_service_point.b_service_point_id = b_service_limit_service_point.b_service_point_id \n"
                    + "order by  time_start asc, time_end asc";
            preparedStatement = connectionInf.ePQuery(sql);
            List<ServiceLimitServicePoint> list = executeQuery(preparedStatement);
            return list;
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<ServiceLimitServicePoint> listByServicePointIdAndIntervalTime(String servicePointId, Date startTime, Date endTime) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select b_service_limit_service_point.* \n"
                    + ", b_service_point.service_point_description\n"
                    + "from b_service_limit_service_point \n"
                    + "inner join b_service_point on b_service_point.b_service_point_id =  b_service_limit_service_point.b_service_point_id \n"
                    + "where b_service_point.b_service_point_id = ? and (time_start, time_end) OVERLAPS (?::time, ?::time) \n"
                    + "order by  time_start asc, time_end asc";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, servicePointId);
            preparedStatement.setTime(2, new java.sql.Time(startTime.getTime()));
            preparedStatement.setTime(3, new java.sql.Time(endTime.getTime()));
            List<ServiceLimitServicePoint> list = executeQuery(preparedStatement);

            return list;
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public ServiceLimitServicePoint selectByServicePointId(String servicePointId) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_service_limit_service_point \n"
                    + " where b_service_point_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, servicePointId);
            List<ServiceLimitServicePoint> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<ServiceLimitServicePoint> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<ServiceLimitServicePoint> list = new ArrayList<>();
        try (ResultSet rs = preparedStatement.executeQuery()) {
            while (rs.next()) {
                ServiceLimitServicePoint obj = new ServiceLimitServicePoint();
                obj.setObjectId(rs.getString("b_service_limit_service_point_id"));
                obj.time_start = rs.getTime("time_start");
                obj.time_end = rs.getTime("time_end");
                obj.limit_appointment = rs.getInt("limit_appointment");
                obj.limit_walkin = rs.getInt("limit_walkin");
                obj.b_service_point_id = rs.getString("b_service_point_id");
                try {
                    obj.serviceName = rs.getString("service_point_description");
                } catch (SQLException ex) {

                }
                list.add(obj);
            }
            return list;
        }
    }
}

package com.hosv3.utility;

import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.postgresql.util.PSQLException;

public class ConnectionDBMgr extends ConnectionDBMgr1 implements ConnectionInf {

    Connection theCon;
    Statement theST;

    public ConnectionDBMgr() {
        initCon("jdbc:postgresql://localhost:5432/hosv39", "huser", "huser", 0);
    }

    public ConnectionDBMgr(String di, String url, String uname, String passw, int type) {
        this.di = di;
        initCon(url, uname, passw, 0);
    }

    public ConnectionDBMgr(String server, String port, String db, String uname, String passw) {
        this.di = "org.postgresql.Driver";
        String strUrl = "jdbc:postgresql://" + server + ":" + port + "/" + db;
        initCon(strUrl, uname, passw, 0);
    }

    /**
     * 0server 1database 2port 3user 4password 5remind 6type
     *
     * @param target_db
     */
    public ConnectionDBMgr(String[] target_db) {
        this.di = "org.postgresql.Driver";
        String url1 = "jdbc:postgresql://" + target_db[0] + ":" + target_db[2] + "/" + target_db[1];
        initCon(url1, target_db[3], target_db[4], 0);
    }

    @Override
    public void initCon(String url, String uname, String passw, int typedatabase) {
        try {
            this.url = url;
            this.uname = uname;
            this.passw = passw;
            if (di == null || di.isEmpty()) {
                this.di = "org.postgresql.Driver";
            }
        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    @Override
    public boolean open() {
        return true;
    }

    @Override
    public boolean close() {
        return true;
    }

    @Override
    public int eUpdate(String sql) throws Exception {
        sql_total++;
        int ret = executeUpdate(sql);
        if (debug) {
            LOGGER.log(Level.INFO, "{0}\t\t{1}", new Object[]{ret, sql});
        }
        return ret;
    }

    @Override
    public java.sql.ResultSet eQuery(String sql) throws Exception {
        sql_total++;
        if (debug) {
            LOGGER.log(Level.INFO, "\t{0}", sql);
        }
        java.sql.ResultSet rs = executeQuery(sql);
        return rs;
    }

    @Override
    public ConnectionInf getClone() {
        return new ConnectionDBMgr(di, url, uname, passw, super.typeDatabase);
    }

    private Statement getST() throws SQLException, ClassNotFoundException {
//        if (theST == null) {
//            theST = getConnection().createStatement();
//        }
        return getConnection().createStatement();
    }

    private int executeUpdate(String sql) throws Exception {
        try {
            return getST().executeUpdate(sql);
        } catch (PSQLException e) {
            if (!e.getMessage().equals("An I/O error occured while sending to the backend.")) {
                throw e;
            }
        }
        clear();
        return getST().executeUpdate(sql);
    }

    private ResultSet executeQuery(String sql) throws Exception {
        try {
            return getST().executeQuery(sql);
        } catch (PSQLException e) {
            if (!e.getMessage().equals("An I/O error occured while sending to the backend.")) {
                throw e;
            }
        }
        clear();
        return getST().executeQuery(sql);
    }

    private void clear() {
        theCon = null;
        theST = null;
    }

    @Override
    public Connection getConnection() {
        if (theCon == null) {
            try {
                Class.forName(this.di);
                theCon = DriverManager.getConnection(url, this.uname, this.passw);
            } catch (SQLException ex) {
                LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
            } catch (ClassNotFoundException ex) {
                LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
            }
        }
        try {
            int timeoutSeconds = 5;
            if (!theCon.isValid(timeoutSeconds)) {
                String htmlMessage = "<html><body style='text-align: center;'>"
                        + "<h1>��س� Login ���������</h1>"
                        + "<p>���ͧ�ҡ����������͡Ѻ�ҹ�����żԴ��Ҵ</p></body></html>";
                JOptionPane.showMessageDialog(null, htmlMessage, "����͹", JOptionPane.ERROR_MESSAGE);
                System.exit(0);
            }
        } catch (Exception e) {
        }
        return theCon;
    }

    @Override
    public boolean begin() {
        return true;
    }

    @Override
    public boolean rollback() {
        return true;
    }

    @Override
    public boolean commit() {
        return true;
    }

    @Override
    public java.sql.PreparedStatement ePQuery(String sql) {
        try {
            return this.getConnection().prepareStatement(sql);
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
            return null;
        }
    }

    @Override
    public List<Object[]> eComplexQuery(String sql) throws Exception {
        if (debug) {
            LOGGER.log(Level.INFO, "\t{0}", sql);
        }
        Statement stmt = this.getConnection().createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        // Get result set meta data
        ResultSetMetaData rsmd = rs.getMetaData();
        int numColumns = rsmd.getColumnCount();
        String[] columnNames = new String[numColumns];
        // Get the column names; column indices start from 1
        for (int i = 1; i < numColumns + 1; i++) {
            columnNames[i - 1] = rsmd.getColumnName(i);
        }
        List<Object[]> resultList = new ArrayList<Object[]>();
        while (rs.next()) {
            Object[] objects = new Object[numColumns];
            for (int i = 0; i < numColumns; i++) {
                objects[i] = rs.getObject(columnNames[i]);
            }
            resultList.add(objects);
        }
        rs.close();
        stmt.close();
        return resultList;
    }
    private static final Logger LOGGER = Logger.getLogger(ConnectionDBMgr.class.getName());

    @Override
    public List<Map<String, Object>> eComplexQueryWithColumn(String sql) throws Exception {
        if (debug) {
            LOGGER.log(Level.INFO, "\t{0}", sql);
        }
        Statement stmt = this.getConnection().createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        // Get result set meta data
        ResultSetMetaData rsmd = rs.getMetaData();
        int numColumns = rsmd.getColumnCount();
        String[] columnNames = new String[numColumns];
        // Get the column names; column indices start from 1
        for (int i = 1; i < numColumns + 1; i++) {
            columnNames[i - 1] = rsmd.getColumnName(i);
        }
        List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
        Map<String, Object> map;
        while (rs.next()) {
            map = new LinkedHashMap<String, Object>();
            for (int i = 0; i < numColumns; i++) {
                map.put(columnNames[i], rs.getObject(columnNames[i]));
            }
            resultList.add(map);
        }
        rs.close();
        stmt.close();
        return resultList;
    }
}

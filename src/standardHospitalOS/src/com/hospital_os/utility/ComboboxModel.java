/*
 * ComboboxModel.java
 *
 * Created on 1 ���Ҥ� 2546, 23:57 �.
 */
package com.hospital_os.utility;

import com.hospital_os.usecase.connection.CommonInf;
import java.awt.Color;
import java.util.List;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.border.Border;

/**
 *
 * @author Administrator
 */
@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public class ComboboxModel {

    private final static Border defaultBorder = new JComboBox().getBorder();

    public static boolean initComboBox(JComboBox jc, Vector<?> xfix) {
        if (xfix == null || jc == null) {
            return false;
        }
        DefaultComboBoxModel dcbm = new DefaultComboBoxModel();
        for (int i = 0; i < xfix.size(); i++) {
            dcbm.addElement(xfix.get(i));
        }
        jc.setModel(dcbm);

        return true;
    }

    public static boolean initComboBox(JComboBox jc, List<?> xfix) {
        if (xfix == null || jc == null) {
            return false;
        }
        DefaultComboBoxModel dcbm = new DefaultComboBoxModel();
        for (int i = 0; i < xfix.size(); i++) {
            dcbm.addElement(xfix.get(i));
        }
        jc.setModel(dcbm);
        return true;
    }

    public static String getStringConboBox(JComboBox jc) {
        Object selectedItem = jc.getSelectedItem();
        if (selectedItem == null) {
            return jc.isEditable() ? String.valueOf(jc.getSelectedItem()) : "";
        }
        if (selectedItem instanceof ComboFix) {
            ComboFix hos = (ComboFix) selectedItem;
            return hos.name;
        }
        if (selectedItem instanceof CommonInf) {
            CommonInf hos = (CommonInf) selectedItem;
            return hos.getName();
        }
        return String.valueOf(jc.getSelectedItem());
    }

    public static String getCodeComboBox(JComboBox jc) {
        Object selectedItem = jc.getSelectedItem();
        if (selectedItem == null) {
            return "";
        }
        if (selectedItem instanceof ComboFix) {
            ComboFix hos = (ComboFix) selectedItem;
            return hos.code;
        }
        if (selectedItem instanceof CommonInf) {
            CommonInf hos = (CommonInf) selectedItem;
            return hos.getCode();
        }
        return "";
    }

    /**
     * @param jc
     * @param id
     * @return
     * @not deprecated henbe unused �ѹ����Ѻ combofix
     * ��ҹ�鹨֧���繵�ͧ����ѹ dep
     */
    public static String getDescriptionComboBox(JComboBox jc, String id) {
        if (jc == null) {
            return null;
        }
        if (jc.getItemCount() > 0) {
            int count = jc.getItemCount();
            for (int i = 0; i < count; i++) {
                Object selectedItem = jc.getItemAt(i);
                if (selectedItem instanceof ComboFix) {
                    ComboFix hos = (ComboFix) selectedItem;
                    if ((hos.code == null && id == null)
                            || (hos.code != null && hos.code.equals(id))) {
                        return hos.name;
                    }
                } else if (selectedItem instanceof CommonInf) {
                    CommonInf hos = (CommonInf) selectedItem;
                    if ((hos.getCode() == null && id == null)
                            || (hos.getCode() != null && hos.getCode().equals(id))) {
                        return hos.getName();
                    }
                }
            }
            return null;
        }
        return null;
    }

    public static String getOtherComboBox(JComboBox jc) {
        Object selectedItem = jc.getSelectedItem();
        if (selectedItem instanceof ComboFix) {
            ComboFix hos = (ComboFix) selectedItem;
            if (hos != null) {
                return hos.other;
            }
        }
//        else if (selectedItem instanceof CommonInf) {
//            CommonInf hos = (CommonInf) selectedItem;
//            if (hos != null) {
//                return hos.getXX();
//            }
//        }
        return "";
    }

    /*
     *@author henbe
     *@name new Pattern of setCodeComboBox
     */
    public static boolean setCodeComboBox(JComboBox jc, String id) {
        Object hos = null;
        int i;
        for (i = 0; i < jc.getItemCount(); i++) {
            Object selectedItem = jc.getItemAt(i);
            if (selectedItem instanceof ComboFix) {
                ComboFix item = (ComboFix) selectedItem;
                if ((item.code == null && id == null)
                        || (item.code != null && item.code.equals(id))) {
                    hos = item;
                    break;
                }
            } else if (selectedItem instanceof CommonInf) {
                CommonInf item = (CommonInf) selectedItem;
                if ((item.getCode() == null && id == null)
                        || (item.getCode() != null && item.getCode().equals(id))) {
                    hos = item;
                    break;
                }
            }
        }
        //�������ŵ������
        if (hos != null) {
            if (jc.getItemCount() > 0) {
                jc.setSelectedIndex(i);
            }
            jc.setBorder(defaultBorder);
            return true;
        } else { //��辺��������Ф�ҷ�������繤����ҧ
            if (jc.getItemCount() > 0) {
                jc.setSelectedIndex(0);
            }
            jc.setBorder(javax.swing.BorderFactory.createLineBorder(Color.YELLOW, 3));
            return false;
        }
    }
}

CREATE TABLE b_item_lab_advice (
    b_item_lab_advice_id        character varying(255)   NOT NULL,
    advice_topic                text   NOT NULL,
    advice_detail               text   NOT NULL,
    status                      character varying(1) default '0' NOT NULL,
    record_datetime             character varying(19)    NOT NULL,
    user_record_id              character varying(255)   NOT NULL,
    update_datetime             character varying(19)    NULL,
    user_update_id              character varying(255)   NULL,
CONSTRAINT b_item_lab_advice_pkey PRIMARY KEY (b_item_lab_advice_id)
);

CREATE TABLE b_item_lab_advice_set (
    b_item_lab_advice_set_id    character varying(255)   NOT NULL,
    b_item_lab_advice_id        character varying(255)   NOT NULL,
    b_item_id                   character varying(255)   NOT NULL,
CONSTRAINT b_item_lab_advice_set_pkey PRIMARY KEY (b_item_lab_advice_set_id)
);
CREATE INDEX b_item_lab_advice_set_index1
   ON b_item_lab_advice_set (b_item_lab_advice_id ASC NULLS LAST);

CREATE TABLE b_item_checkup(
   b_item_checkup_id    character varying(255) NOT NULL, 
   b_item_id            character varying(255) NOT NULL, 
   CONSTRAINT b_item_checkup_pkey PRIMARY KEY (b_item_checkup_id), 
   CONSTRAINT b_item_checkup_unique UNIQUE (b_item_id)
);


CREATE TABLE t_checkup (
    t_checkup_id                character varying(255)   NOT NULL,
    t_visit_id                  character varying(255)   NOT NULL,
    approve_status              character varying(1) default '0' NOT NULL,
    email_status                character varying(1) default '0' NOT NULL,
    email_sent_datetime         timestamp without time zone,
    active                      character varying(1) default '1' NOT NULL,
    record_datetime             character varying(19)    NOT NULL,
    user_record_id              character varying(255)   NOT NULL,
    update_datetime             character varying(19)    NULL,
    user_update_id              character varying(255)   NULL,
CONSTRAINT t_checkup_pkey PRIMARY KEY (t_checkup_id)
);
CREATE INDEX t_checkup_index1
   ON t_checkup (t_visit_id ASC NULLS LAST);

CREATE TABLE t_checkup_personal (
    t_checkup_personal_id       character varying(255)   NOT NULL,
    t_checkup_id                character varying(255)   NOT NULL,
    accident_history            character varying(255)   NULL,
    accident_history_date       character varying(10)    NULL,
    operate_history             character varying(255)   NULL,
    operate_history_date        character varying(10)    NULL,
    admit_hospital_code         character varying(5)     NULL,
    record_datetime             character varying(19)    NOT NULL,
    user_record_id              character varying(255)   NOT NULL,
    update_datetime             character varying(19)    NOT NULL,
    user_update_id              character varying(255)   NOT NULL,
CONSTRAINT t_checkup_personal_pkey PRIMARY KEY (t_checkup_personal_id)
);
CREATE INDEX t_checkup_personal_index1
   ON t_checkup_personal (t_checkup_id ASC NULLS LAST);

CREATE TABLE t_checkup_druguse (
    t_checkup_druguse_id       character varying(255)   NOT NULL,
    t_checkup_personal_id      character varying(255)   NOT NULL,
    b_item_id                  character varying(255)   NOT NULL,
    record_datetime            character varying(19)    NOT NULL,
    user_record_id             character varying(255)   NOT NULL,
    update_datetime            character varying(19)    NOT NULL,
    user_update_id             character varying(255)   NOT NULL,
CONSTRAINT t_checkup_druguse_pkey PRIMARY KEY (t_checkup_druguse_id)
);
CREATE INDEX t_checkup_druguse_index1
   ON t_checkup_druguse (t_checkup_personal_id ASC NULLS LAST);

CREATE TABLE t_checkup_sight (
    t_checkup_sight_id              character varying(255)   NOT NULL,
    t_checkup_id                    character varying(255)   NOT NULL,
    longsight_left                  character varying(255)   NULL,
    longsight_right                 character varying(255)   NULL,
    longsight_both                  character varying(255)   NULL,
    glass_longsight_left            character varying(255)   NULL,
    glass_longsight_right           character varying(255)   NULL,
    glass_longsight_both            character varying(255)   NULL,
    contectlens_longsight_left      character varying(255)   NULL,
    contectlens_longsight_right     character varying(255)   NULL,
    contectlens_longsight_both      character varying(255)   NULL,
    myopia_left                     character varying(255)   NULL,
    myopia_right                    character varying(255)   NULL,
    myopia_both                     character varying(255)   NULL,
    glass_myopia_left               character varying(255)   NULL,
    glass_myopia_right              character varying(255)   NULL,
    glass_myopia_both               character varying(255)   NULL,
    contectlens_myopia_left         character varying(255)   NULL,
    contectlens_myopia_right        character varying(255)   NULL,
    contectlens_myopia_both         character varying(255)   NULL,
    intraocular_pressure_left       character varying(255)   NULL,
    intraocular_pressure_right      character varying(255)   NULL,
    color_blindness                 character varying(1)     NOT NULL default '0' ,
    checkup_sight_summary           character varying(255)   NULL,
    record_datetime                 character varying(19)    NOT NULL,
    user_record_id                  character varying(255)   NOT NULL,
    update_datetime                 character varying(19)    NOT NULL,
    user_update_id                  character varying(255)   NOT NULL,
CONSTRAINT t_checkup_sight_pkey PRIMARY KEY (t_checkup_sight_id)
);
CREATE INDEX t_checkup_sight_index1
   ON t_checkup_sight (t_checkup_id ASC NULLS LAST);


CREATE TABLE t_checkup_hearing (
    t_checkup_hearing_id           character varying(255)   NOT NULL,
    t_checkup_id                   character varying(255)   NOT NULL,
    status_checkup                 character varying(1)     default '0' NOT NULL,
    five_hundred_left              character varying(255)   NULL,
    five_hundred_right             character varying(255)   NULL,
    one_thousand_left              character varying(255)   NULL,
    one_thousand_right             character varying(255)   NULL,
    two_thousand_left              character varying(255)   NULL,
    two_thousand_right             character varying(255)   NULL,
    three_thousand_left            character varying(255)   NULL,
    three_thousand_right           character varying(255)   NULL,
    four_thousand_left             character varying(255)   NULL,
    four_thousand_right            character varying(255)   NULL,
    six_thousand_left              character varying(255)   NULL,
    six_thousand_right             character varying(255)   NULL,
    eight_thousand_left            character varying(255)   NULL,
    eight_thousand_right           character varying(255)   NULL,
    average_left_first             character varying(255)   NULL,
    average_right_first            character varying(255)   NULL,
    average_left_second            character varying(255)   NULL,
    average_right_second           character varying(255)   NULL,
    result_checkup                 character varying(255)   NULL,
    advice_checkup                 character varying(255)   NULL,
    record_datetime                character varying(19)    NOT NULL,
    user_record_id                 character varying(255)   NOT NULL,
    update_datetime                character varying(19)    NOT NULL,
    user_update_id                 character varying(255)   NOT NULL,
CONSTRAINT t_checkup_hearing_pkey PRIMARY KEY (t_checkup_hearing_id)
);
CREATE INDEX t_checkup_hearing_index1
   ON t_checkup_hearing (t_checkup_id ASC NULLS LAST);

-- create s_checkup_version
CREATE TABLE s_checkup_version (
    version_id            varchar(255) NOT NULL,
    version_number            	varchar(255) NULL,
    version_description       	varchar(255) NULL,
    version_application_number	varchar(255) NULL,
    version_database_number   	varchar(255) NULL,
    version_update_time       	varchar(255) NULL 
);

ALTER TABLE s_checkup_version
	ADD CONSTRAINT s_checkup_version_pkey
	PRIMARY KEY (version_id);


INSERT INTO s_checkup_version VALUES ('1', '1', 'Checkup Module', '1.0.0', '1.0.0', (select current_date) || ','|| (select current_time));

INSERT INTO s_script_update_log values ('checkup_Module','update_checkup_001.sql',(select current_date) || ','|| (select current_time),'Initialize Checkup Module');

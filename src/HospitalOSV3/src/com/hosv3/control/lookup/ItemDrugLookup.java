/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.control.lookup;

import com.hospital_os.object.Item;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.LookupControlInf;
import com.hosv3.control.LookupControl;
import com.hosv3.control.SetupControl;
import com.hosv3.utility.connection.ExecuteControlInf;

/**
 *
 * @author tanakrit
 */
public class ItemDrugLookup implements LookupControlInf, ExecuteControlInf {

    boolean is_lookup = true;
    private LookupControl theLookup;
    private SetupControl theEC;

    /**
     * Creates a new instance of ItemLookup
     *
     * @param lookup
     */
    public ItemDrugLookup(LookupControl lookup) {
        theLookup = lookup;
    }

    public ItemDrugLookup(SetupControl lookup) {
        is_lookup = false;
        theEC = lookup;
    }

    public CommonInf readData(String str) {
        return null;
    }

    @Override
    public CommonInf readHosData(String pk) {
        return null;
    }

    @Override
    @SuppressWarnings("UseOfObsoleteCollectionType")
    public java.util.Vector listData(String str) {
        if (str == null || str.equals("") || str.equals("%")) {
            return null;
        } else {
            return theLookup.listItemDrugByName(str);
        }
    }

    @Override
    public boolean execute(Object str) {
        theEC.addItemRiskDx((Item) str);
        return true;
    }

}

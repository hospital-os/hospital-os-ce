/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.VisitBed;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class VisitBedDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "853";

    public VisitBedDB(ConnectionInf c) {
        this.connectionInf = c;
    }

    public int insert(VisitBed obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO t_visit_bed(\n"
                    + "            t_visit_bed_id, t_visit_id, b_visit_bed_id, bed_number, continue_b_item_ids, current_bed, move_date_time, move_out_date_time, reason, active, user_record, user_modify)\n"
                    + "    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.getGenID(tableId));
            preparedStatement.setString(index++, obj.t_visit_id);
            preparedStatement.setString(index++, obj.b_visit_bed_id);
            preparedStatement.setString(index++, obj.bed_number);
            preparedStatement.setArray(index++, connectionInf.getConnection().createArrayOf("varchar", obj.continue_b_item_ids));
            preparedStatement.setString(index++, obj.current_bed);
            preparedStatement.setTimestamp(index++, obj.move_date_time == null ? null
                    : new Timestamp(obj.move_date_time.getTime()));
            preparedStatement.setTimestamp(index++, obj.move_out_date_time == null ? null
                    : new Timestamp(obj.move_out_date_time.getTime()));
            preparedStatement.setString(index++, obj.reason);
            preparedStatement.setString(index++, obj.active);
            preparedStatement.setString(index++, obj.user_record);
            preparedStatement.setString(index++, obj.user_modify);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(VisitBed obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_visit_bed\n");
            sql.append("   SET b_visit_bed_id=?, bed_number=?, \n");
            sql.append("       continue_b_item_ids=?, current_bed=?,\n");
            sql.append("       move_date_time=?,\n");
            sql.append("       move_out_date_time=?, reason=?,active=?,user_modify=?, modify_date_time = current_timestamp\n");
            sql.append(" WHERE t_visit_bed_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.b_visit_bed_id);
            preparedStatement.setString(index++, obj.bed_number);
            preparedStatement.setArray(index++, connectionInf.getConnection().createArrayOf("varchar", obj.continue_b_item_ids));
            preparedStatement.setString(index++, obj.current_bed);
            preparedStatement.setTimestamp(index++, obj.move_date_time == null ? null
                    : new Timestamp(obj.move_date_time.getTime()));
            preparedStatement.setTimestamp(index++, obj.move_out_date_time == null ? null
                    : new Timestamp(obj.move_out_date_time.getTime()));
            preparedStatement.setString(index++, obj.reason);
            preparedStatement.setString(index++, obj.active);
            preparedStatement.setString(index++, obj.user_modify);
            preparedStatement.setString(index++, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int inactive(VisitBed obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_visit_bed\n");
            sql.append("   SET active=?, user_modify=?, modify_date_time = current_timestamp\n");
            sql.append(" WHERE t_visit_bed_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.active);
            preparedStatement.setString(index++, obj.user_modify);
            preparedStatement.setString(index++, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int updateUsageBed(VisitBed obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_visit_bed\n");
            sql.append("   SET current_bed=?,\n");
            sql.append("   active=?, user_modify=?, modify_date_time = current_timestamp\n");
            if (!obj.current_bed.equals("1")) {
                if (obj.move_out_date_time == null) {
                    sql.append("   , move_out_date_time = current_timestamp\n");
                } else {
                    sql.append("   , move_out_date_time = ?\n");
                }
            }
            sql.append(" WHERE t_visit_bed_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.current_bed);
            preparedStatement.setString(index++, obj.active);
            preparedStatement.setString(index++, obj.user_modify);
            if (!obj.current_bed.equals("1") && obj.move_out_date_time != null) {
                preparedStatement.setTimestamp(index++, new Timestamp(obj.move_out_date_time.getTime()));
            }
            preparedStatement.setString(index++, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int updateUnuseAllBedByVisitId(String visitId, String userModId) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_visit_bed\n");
            sql.append("   SET current_bed='0',\n");
            sql.append("       user_modify=?, modify_date_time = current_timestamp\n");
            sql.append(" WHERE t_visit_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, userModId);
            preparedStatement.setString(index++, visitId);
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int updateIactiveAllBedByVisitId(String visitId, String userModId) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_visit_bed\n");
            sql.append("   SET active='0',\n");
            sql.append("       user_modify=?, modify_date_time = current_timestamp\n");
            sql.append(" WHERE t_visit_id = ? and active = '1'");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, userModId);
            preparedStatement.setString(index++, visitId);
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

//    public int delete(VisitBed obj) throws Exception {
//        PreparedStatement preparedStatement = null;
//        try {
//            StringBuilder sql = new StringBuilder();
//            sql.append("DELETE FROM t_visit_bed\n");
//            sql.append(" WHERE t_visit_bed_id = ?");
//            preparedStatement = connectionInf.ePQuery(sql.toString());
//            preparedStatement.setString(1, obj.getObjectId());
//            // execute update SQL stetement
//            return preparedStatement.executeUpdate();
//
//        } finally {
//            if (preparedStatement != null) {
//                preparedStatement.close();
//            }
//        }
//    }
//
    public VisitBed select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select *, b_visit_ward.visit_ward_description as visit_ward_description\n"
                    + "from t_visit_bed \n"
                    + "inner join b_visit_bed on t_visit_bed.b_visit_bed_id =b_visit_bed.b_visit_bed_id\n"
                    + "inner join b_visit_ward on b_visit_bed.b_visit_ward_id =b_visit_ward.b_visit_ward_id\n"
                    + "where t_visit_bed.t_visit_bed_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<VisitBed> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<VisitBed> listByVisitId(String t_visit_id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * , b_visit_ward.visit_ward_description as visit_ward_description\n"
                    + "from t_visit_bed \n"
                    + "inner join b_visit_bed on t_visit_bed.b_visit_bed_id =b_visit_bed.b_visit_bed_id\n"
                    + "inner join b_visit_ward on b_visit_bed.b_visit_ward_id =b_visit_ward.b_visit_ward_id\n"
                    + "where t_visit_bed.t_visit_id = ? and t_visit_bed.active = '1' order by t_visit_bed.current_bed desc, t_visit_bed.move_date_time desc";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, t_visit_id);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public VisitBed getCurrentVisitBedByVisitId(String t_visit_id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select *, b_visit_ward.visit_ward_description as visit_ward_description\n"
                    + "from t_visit_bed \n"
                    + "inner join b_visit_bed on t_visit_bed.b_visit_bed_id =b_visit_bed.b_visit_bed_id\n"
                    + "inner join b_visit_ward on b_visit_bed.b_visit_ward_id =b_visit_ward.b_visit_ward_id\n"
                    + "where t_visit_bed.active = '1'\n"
                    + "and t_visit_bed.current_bed in ('1','2')\n"
                    + "and t_visit_bed.t_visit_id  = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, t_visit_id);
            List<VisitBed> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<VisitBed> listAllUsageBedByBVisitBedId(String bvbId) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select *, b_visit_ward.visit_ward_description as visit_ward_description\n"
                    + "from t_visit_bed \n"
                    + "inner join b_visit_bed on t_visit_bed.b_visit_bed_id =b_visit_bed.b_visit_bed_id\n"
                    + "inner join b_visit_ward on b_visit_bed.b_visit_ward_id =b_visit_ward.b_visit_ward_id\n"
                    + "where\n"
                    + "t_visit_bed.active = '1'\n"
                    + "and t_visit_bed.current_bed ='1'\n"
                    + "and t_visit_bed.b_visit_bed_id  = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, bvbId);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<VisitBed> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<VisitBed> list = new ArrayList<VisitBed>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                VisitBed obj = new VisitBed();
                obj.setObjectId(rs.getString("t_visit_bed_id"));
                obj.t_visit_id = rs.getString("t_visit_id");
                obj.bed_number = rs.getString("bed_number");
                obj.b_visit_bed_id = rs.getString("b_visit_bed_id");
                obj.current_bed = rs.getString("current_bed");
                obj.continue_b_item_ids = (String[]) rs.getArray("continue_b_item_ids").getArray();
                obj.move_date_time = rs.getTimestamp("move_date_time");
                obj.move_out_date_time = rs.getTimestamp("move_out_date_time");
                obj.reason = rs.getString("reason");
                obj.user_record = rs.getString("user_record");
                obj.record_date_time = rs.getTimestamp("record_date_time");
                obj.user_modify = rs.getString("user_modify");
                obj.modify_date_time = rs.getTimestamp("modify_date_time");
                try {
                    obj.wardName = rs.getString("visit_ward_description");
                } catch (Exception ex) {
                    obj.wardName = "";
                }
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }
}

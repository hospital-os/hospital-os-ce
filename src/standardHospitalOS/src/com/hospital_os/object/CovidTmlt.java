/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author tanakrit
 */
public class CovidTmlt extends Persistent {

    public String tmlt_code;
    public String tmlt_name;
    public String component;
    public String specimen;
    public String method;
}

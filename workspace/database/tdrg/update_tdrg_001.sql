--Create table t_diag_tdrg
CREATE TABLE t_diag_tdrg (
    t_diag_tdrg_id CHARACTER VARYING(255)  NOT NULL,
    t_visit_id CHARACTER VARYING(255)  NOT NULL,
    mdc CHARACTER VARYING(2)  NOT NULL,
    drg CHARACTER VARYING(5)  NOT NULL,
    rw DECIMAL(7,4) NOT NULL DEFAULT 0,
    adjrw DECIMAL(8,4) NOT NULL DEFAULT 0,
    wtlos DECIMAL(6,2) NOT NULL DEFAULT 0,
    ot INTEGER NOT NULL DEFAULT 0,
    err INTEGER NOT NULL DEFAULT 0,
    warn INTEGER NOT NULL DEFAULT 0,
    user_record_id CHARACTER VARYING(255)  NOT NULL,
    record_date_time CHARACTER VARYING(19)  NOT NULL,
    user_modify_id CHARACTER VARYING(255)  NOT NULL,
    modify_date_time CHARACTER VARYING(19)  NOT NULL,
    CONSTRAINT t_diag_tdrg_pkey PRIMARY KEY (t_diag_tdrg_id),
    CONSTRAINT t_diag_tdrg_unique1 UNIQUE (t_visit_id)
    
);


--Create table f_tdrg_error
CREATE TABLE f_tdrg_error (
    f_tdrg_error_id INTEGER  NOT NULL,
    tdrg_error_description CHARACTER VARYING(255) NOT NULL,
    CONSTRAINT f_tdrg_error_pkey PRIMARY KEY (f_tdrg_error_id)
);
--insert f_tdrg_error
INSERT INTO f_tdrg_error VALUES (1,'No Principal Diagnosis');
INSERT INTO f_tdrg_error VALUES (2,'Invalid Principal Diagnosis');
INSERT INTO f_tdrg_error VALUES (3,'Unacceptable Principal Diagnosis');
INSERT INTO f_tdrg_error VALUES (4,'Principal Diagnosis not valid for age');
INSERT INTO f_tdrg_error VALUES (5,'Principal Diagnosis not valid for sex');
INSERT INTO f_tdrg_error VALUES (6,'Age error');
INSERT INTO f_tdrg_error VALUES (7,'Ungroupable due to sex error');
INSERT INTO f_tdrg_error VALUES (8,'Ungroupable due to discharge type error');
INSERT INTO f_tdrg_error VALUES (9,'Length of stay error');
INSERT INTO f_tdrg_error VALUES (10,'Ungroupable due to admission weight error');


--Create table f_diag_tdrg_warning
CREATE TABLE f_tdrg_warning (
    f_tdrg_warning_id INTEGER  NOT NULL,
    tdrg_warning_description CHARACTER VARYING(255) NOT NULL,
    CONSTRAINT f_tdrg_warning_pkey PRIMARY KEY (f_tdrg_warning_id)
);
--insert f_tdrg_warning
INSERT INTO f_tdrg_warning VALUES (1,'SDx ใช้ไม่ได้ หรือซ้ำกับ PDx หรือซ้ำกันเอง');
INSERT INTO f_tdrg_warning VALUES (2,'SDx ไม่เหมาะกับอายุ หรือเป็นรหัสซึ่งเหมาะกับบางช่วงอายุแต่ไม่มีข้อมูลอายุ');
INSERT INTO f_tdrg_warning VALUES (4,'SDx ไม่เหมาะกับเพศ หรือเป็นรหัสสำหรับเพศใดเพศหนึ่ง แต่ไม่มีข้อมูลเพศ');
INSERT INTO f_tdrg_warning VALUES (8,'Proc ใช้ไม่ได้ หรือซ้ำกันเอง');
INSERT INTO f_tdrg_warning VALUES (16,'Proc ไม่เหมาะกับเพศ หรือเป็นรหัสสำหรับเพศใดเพศหนึ่ง แต่ไม่มีข้อมูลเพศ');
INSERT INTO f_tdrg_warning VALUES (32,'ไม่มีข้อมูลเพศ หรือใช้รหัสนอกเหนือจากที่กำหนด');
INSERT INTO f_tdrg_warning VALUES (64,'ไม่มีประเภทการจำหน่ายออกจากโรงพยาบาล หรือใช้รหัสนอกเหนือจากที่กำหนด');
INSERT INTO f_tdrg_warning VALUES (128,'ไม่มีวันที่ และ/หรือ เวลา ที่รับไว้ในรพ. หรือ มีแต่ไม่ถูกต้อง');
INSERT INTO f_tdrg_warning VALUES (256,'ไม่มีวันที่ และ/หรือ เวลา ที่จำหน่ายออกจากรพ. หรือ มีแต่ไม่ถูกต้อง');


--Create function concat_icd10_2
CREATE OR REPLACE FUNCTION "public"."concat_icd10_2" (visit_id text, icd_type text) RETURNS _varchar AS
'
select 
array(
select 
        diag_icd10_number 
from t_diag_icd10 inner join t_visit on t_visit.t_visit_id = t_diag_icd10.diag_icd10_vn
where t_diag_icd10.diag_icd10_active = ''1''
        and t_visit.t_visit_id = $1
        and t_diag_icd10.f_diag_icd10_type_id = $2
order by t_diag_icd10.diag_icd10_record_date_time
)

'
LANGUAGE 'sql';


--Create function concat_icd9_2
CREATE OR REPLACE FUNCTION "public"."concat_icd9_2" (visit_id text, icd_type text) RETURNS _varchar AS
'
select 
array(
select 
        t_diag_icd9.diag_icd9_icd9_number
from t_diag_icd9 inner join t_visit on t_visit.t_visit_id = t_diag_icd9.diag_icd9_vn
where t_diag_icd9.diag_icd9_active = ''1''
        and t_visit.t_visit_id = $1
        and t_diag_icd9.f_diagnosis_operation_type_id = $2
order by t_diag_icd9.diag_icd9_record_date_time asc
)

'
LANGUAGE 'sql';


CREATE TABLE s_tdrg_version (
    s_tdrg_version_id CHARACTER VARYING(255)  NOT NULL,
    version_tdrg_number CHARACTER VARYING(255)  NOT NULL,
    version_tdrg_description CHARACTER VARYING(255)  NOT NULL,
    version_tdrg_application_number CHARACTER VARYING(255)  NOT NULL,
    version_tdrg_database_number CHARACTER VARYING(255)  NOT NULL,
    version_tdrg_update_time CHARACTER VARYING(255)  NOT NULL, 
    CONSTRAINT s_tdrg_version_pkey PRIMARY KEY (s_tdrg_version_id),
    CONSTRAINT s_tdrg_version_unique1 UNIQUE (version_tdrg_number)
);

INSERT INTO s_tdrg_version VALUES ('9760000000001', '1', 'Grouper for Thai DRG version 5.1.1', '1.0.0', '1.0.0', (select current_date) || ','|| (select current_time));
INSERT INTO s_script_update_log values ('DRG_Module','update_tdrg_001.sql',(select current_date) || ','|| (select current_time),'Initialize DRG_Module');
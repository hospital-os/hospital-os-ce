/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.utility;

/**
 *
 * @author Somprasong
 */
public class Configuration {
    private boolean show_sql = false;

    public Configuration() {
    }

    public boolean isShow_sql() {
        return show_sql;
    }

    public void setShow_sql(boolean show_sql) {
        this.show_sql = show_sql;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author tanakrit
 */
public class LabTMLT extends Persistent implements CommonInf {

    public String tmlt_name = "";
    public String component = "";
    public String scale = "";
    public String unit = "";
    public String specimen = "";
    public String method = "";
    public String order_type = "";
    public String loinc = "";
    public String status = "";
    public String first_release_date = "";
    public String last_release_date = "";
    public String cscode = "";
    public String cs_name = "";
    public Double cs_price;

    @Override
    public String getCode() {
        return getObjectId();
    }

    @Override
    public String getName() {
        return tmlt_name;
    }

    @Override
    public String toString() {
        return getName();
    }

}

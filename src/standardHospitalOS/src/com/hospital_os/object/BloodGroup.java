package com.hospital_os.object;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.Persistent;

@SuppressWarnings("ClassWithoutLogger")
public class BloodGroup extends Persistent implements CommonInf {

    public String description;

    /**
     * @roseuid 3F658BBB036E
     */
    public BloodGroup() {
    }

    @Override
    public String getCode() {
        return getObjectId();
    }

    @Override
    public String getName() {
        return description;
    }

    @Override
    public String toString() {
        return description;
    }
}

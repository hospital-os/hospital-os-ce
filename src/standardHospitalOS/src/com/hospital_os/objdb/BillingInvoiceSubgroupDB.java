/*
 * BillingInvoiceSubgroupDB.java
 *
 * Created on 10 �Զع�¹ 2547, 16:48 �.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.*;
import com.hospital_os.usecase.connection.*;
import com.hospital_os.utility.*;
import java.sql.*;
import java.util.*;

/**
 *
 * @author tong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class BillingInvoiceSubgroupDB {

    /**
     * Creates a new instance of BillingInvoiceDB
     */
    public ConnectionInf theConnectionInf;
    public BillingInvoiceSubgroup dbObj;
    final private String idtable = "124";/*556";
     */


    public BillingInvoiceSubgroupDB(ConnectionInf db) {
        theConnectionInf = db;
        dbObj = new BillingInvoiceSubgroup();
        initConfig();
    }

    private boolean initConfig() {

        dbObj.table = "t_billing_invoice_billing_subgroup";
        dbObj.pk_field = "t_billing_invoice_billing_subgroup_id";
        dbObj.billing_invoice_id = "t_billing_invoice_id";
        dbObj.patient_id = "t_patient_id";
        dbObj.visit_id = "t_visit_id";
        dbObj.category_group_item_id = "b_item_subgroup_id";
        dbObj.billing_group_item_id = "b_item_billing_subgroup_id";
        dbObj.payer_share = "billing_invoice_billing_subgroup_payer_share";
        dbObj.patient_share = "billing_invoice_billing_subgroup_patient_share";
        dbObj.payer_share_ceil = "billing_invoice_billing_subgroup_payer_share_ceil";
        dbObj.patient_share_ceil = "billing_invoice_billing_subgroup_patient_share_ceil";
        dbObj.total = "billing_invoice_billing_subgroup_total";
        dbObj.payment_id = "t_payment_id";
        dbObj.draw = "billing_invoice_billing_subgroup_draw";
        dbObj.active = "billing_invoice_billing_subgroup_active";
        dbObj.billing_id = "t_billing_id";
        dbObj.discount_percentage = "discount_percentage";
        dbObj.discount = "discount";
        dbObj.final_patient_share = "final_patient_share";
        return true;
    }

    /**
     * @param cmd
     * @param o
     * @return int
     * @roseuid 3F6574DE0394
     */
    public int insert(BillingInvoiceSubgroup p) throws Exception {
        p.generateOID(idtable);
        String sql = "insert into " + dbObj.table + " ("
                + dbObj.pk_field
                + " ," + dbObj.billing_invoice_id
                + " ," + dbObj.category_group_item_id
                + " ," + dbObj.billing_group_item_id
                + " ," + dbObj.draw
                + " ," + dbObj.patient_id
                + " ," + dbObj.visit_id
                + " ," + dbObj.payer_share
                + " ," + dbObj.patient_share
                + " ," + dbObj.payer_share_ceil
                + " ," + dbObj.patient_share_ceil
                + " ," + dbObj.total
                + " ," + dbObj.discount_percentage
                + " ," + dbObj.discount
                + " ," + dbObj.final_patient_share
                + " ," + dbObj.payment_id
                + " ," + dbObj.active
                + " ," + dbObj.billing_id
                + " ) values ('"
                + p.getObjectId()
                + "','" + p.billing_invoice_id
                + "','" + p.category_group_item_id
                + "','" + p.billing_group_item_id
                + "','" + p.draw
                + "','" + p.patient_id
                + "','" + p.visit_id
                + "'," + (p.payer_share.isEmpty() ? "0.0" : p.payer_share)
                + "," + (p.patient_share.isEmpty() ? "0.0" : p.patient_share)
                + ",'" + p.payer_share_ceil
                + "','" + p.patient_share_ceil
                + "'," + (p.total.isEmpty() ? "0.0" : p.total)
                + "," + (p.discount_percentage.isEmpty() ? "0" : p.discount_percentage)
                + "," + (p.discount.isEmpty() ? "0.0" : p.discount)
                + "," + (p.final_patient_share.isEmpty() ? "0.0" : p.final_patient_share)
                + ",'" + p.payment_id
                + "','" + p.active
                + "','" + p.billing_id
                + "')";
        return theConnectionInf.eUpdate(sql);
    }

    public int update(BillingInvoiceSubgroup p) throws Exception {
        String sql = "update " + dbObj.table + " set ";
        String field = ""
                + "', " + dbObj.billing_invoice_id + "='" + p.billing_invoice_id
                + "', " + dbObj.category_group_item_id + "='" + p.category_group_item_id
                + "', " + dbObj.billing_group_item_id + "='" + p.billing_group_item_id
                + "', " + dbObj.draw + "='" + p.draw
                + "', " + dbObj.patient_id + "='" + p.patient_id
                + "', " + dbObj.visit_id + "='" + p.visit_id
                + "', " + dbObj.payer_share + "=" + (p.payer_share.isEmpty() ? "0.0" : p.payer_share)
                + ", " + dbObj.patient_share + "=" + (p.patient_share.isEmpty() ? "0.0" : p.patient_share)
                + ", " + dbObj.payer_share_ceil + "='" + p.payer_share_ceil
                + "', " + dbObj.patient_share_ceil + "='" + p.patient_share_ceil
                + "', " + dbObj.total + "=" + (p.total.isEmpty() ? "0.0" : p.total)
                + ", " + dbObj.discount_percentage + "=" + (p.discount_percentage.isEmpty() ? "0" : p.discount_percentage)
                + ", " + dbObj.discount + "=" + (p.discount.isEmpty() ? "0.0" : p.discount)
                + ", " + dbObj.final_patient_share + "=" + (p.final_patient_share.isEmpty() ? "0.0" : p.final_patient_share)
                + ", " + dbObj.payment_id + "='" + p.payment_id
                + "', " + dbObj.active + "='" + p.active
                + "', " + dbObj.billing_id + "='" + p.billing_id
                + "' where " + dbObj.pk_field + "='" + p.getObjectId() + "'";
        return theConnectionInf.eUpdate(sql + field.substring(2));
    }

    /*////////////////////////////////////////////////////////////////////////////
     */
    public int delete(BillingInvoiceSubgroup o) throws Exception {
        String sql = "delete from " + dbObj.table
                + " where " + dbObj.pk_field + "='" + o.getObjectId() + "'";

        return theConnectionInf.eUpdate(sql);
    }

    /*////////////////////////////////////////////////////////////////////////////
     */
    public BillingInvoiceSubgroup selectByPK(String pk) throws Exception {
        String sql = "select * from " + dbObj.table
                + " where " + dbObj.pk_field
                + " = '" + pk + "'";

        Vector v = eQuery(sql);
        if (v.isEmpty()) {
            return null;
        } else {
            return (BillingInvoiceSubgroup) v.get(0);
        }
    }

    /*////////////////////////////////////////////////////////////////////////////
     */
    public Vector selectAll() throws Exception {
        String sql = "select * from " + dbObj.table;

        Vector v = eQuery(sql);
        if (v.isEmpty()) {
            return null;
        } else {
            return v;
        }
    }

    public Vector listBillingInvoiceSubgroupByVisitIdBillingInvoiceID(String visit_id, String billing_invoice_id) throws Exception {
        String sql = "select * from " + dbObj.table
                + " where " + dbObj.visit_id + " = '" + visit_id + "'"
                + " and " + dbObj.billing_invoice_id + " = '" + billing_invoice_id + "'"
                + " order by " + dbObj.billing_group_item_id + " desc";

        Vector v = eQuery(sql);
        if (v.isEmpty()) {
            return null;
        } else {
            return v;
        }
    }

    public Vector listBillingInvoiceSubgroupByVisitIdBillingID(String visit_id, String billing_id) throws Exception {
        String sql = "select * from " + dbObj.table
                + " where " + dbObj.visit_id + " = '" + visit_id + "'"
                + " and " + dbObj.billing_id + " = '" + billing_id + "'"
                + " order by " + dbObj.billing_group_item_id + " desc";

        Vector v = eQuery(sql);
        if (v.isEmpty()) {
            return null;
        } else {
            return v;
        }
    }

    /*////////////////////////////////////////////////////////////////////////////
     */
    public Vector selectByPatientId(String pk) throws Exception {
        String sql = "select * from " + dbObj.table
                + " where " + dbObj.patient_id
                + " = '" + pk + "' order by " + dbObj.billing_group_item_id + " desc";

        Vector v = eQuery(sql);
        if (v.isEmpty()) {
            return null;
        } else {
            return v;
        }
    }

    /*////////////////////////////////////////////////////////////////////////////
     */
    public Vector selectByVisitIdBillingIDNULL(String pk) throws Exception {
        String sql = "select * from " + dbObj.table
                + " where " + dbObj.visit_id + " = '" + pk
                + "' and " + dbObj.active + " = '1' "
                + " and " + dbObj.billing_id + " = '' ";
        /*�ҡ仹���
         */

        Vector v = eQuery(sql);
        if (v.isEmpty()) {
            return null;
        } else {
            return v;
        }
    }

    /*////////////////////////////////////////////////////////////////////////////
     */
    public Vector selectByVisitId(String pk) throws Exception {
        String sql = "select * from " + dbObj.table
                + " where " + dbObj.visit_id
                + " = '" + pk + "' and " + dbObj.active + " = '1'  order by " + dbObj.billing_group_item_id + " desc";

        Vector v = eQuery(sql);
        if (v.isEmpty()) {
            return null;
        } else {
            return v;
        }
    }

    /*////////////////////////////////////////////////////////////////////////////
     */
    public Vector selectByVisitIdBillingID(String pk, String billing_id) throws Exception {
        String sql = "select * from " + dbObj.table
                + " where " + dbObj.visit_id + " = '" + pk
                + "' and " + dbObj.active + " = '1'  "
                + " and " + dbObj.billing_id + " = '" + billing_id + "'"
                + " order by " + dbObj.billing_group_item_id + " desc";

        Vector v = eQuery(sql);
        if (v.isEmpty()) {
            return null;
        } else {
            return v;
        }
    }

    /*////////////////////////////////////////////////////////////////////////////
     */
    public Vector eQuery(String sql) throws Exception {
        BillingInvoiceSubgroup p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);

        while (rs.next()) {
            p = new BillingInvoiceSubgroup();
            p.setObjectId(rs.getString(dbObj.pk_field));

            p.billing_invoice_id = rs.getString(dbObj.billing_invoice_id);
            p.category_group_item_id = rs.getString(dbObj.category_group_item_id);
            p.billing_group_item_id = rs.getString(dbObj.billing_group_item_id);
            p.draw = rs.getString(dbObj.draw);
            p.patient_id = rs.getString(dbObj.patient_id);
            p.visit_id = rs.getString(dbObj.visit_id);
            p.payer_share = Constant.doubleToDBString(rs.getDouble(dbObj.payer_share));
            p.patient_share = Constant.doubleToDBString(rs.getDouble(dbObj.patient_share));
            p.payer_share_ceil = rs.getString(dbObj.payer_share_ceil);
            p.patient_share_ceil = rs.getString(dbObj.patient_share_ceil);
            p.total = Constant.doubleToDBString(rs.getDouble(dbObj.total));
            p.discount_percentage = Constant.doubleToDBString(rs.getDouble(dbObj.discount_percentage));
            p.discount = Constant.doubleToDBString(rs.getDouble(dbObj.discount));
            p.final_patient_share = Constant.doubleToDBString(rs.getDouble(dbObj.final_patient_share));
            p.payment_id = rs.getString(dbObj.payment_id);
            p.active = rs.getString(dbObj.active);
            p.billing_id = rs.getString(dbObj.billing_id);

            list.add(p);
        }
        rs.close();
        return list;
    }

    public Vector<BillingInvoiceSubgroup> selectByBillingInvoiceId(String billingInvoiceId) throws Exception {
        String sql = "SELECT * FROM t_billing_invoice_billing_subgroup WHERE t_billing_invoice_id=? and billing_invoice_billing_subgroup_active='1'";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, billingInvoiceId);
            Vector<BillingInvoiceSubgroup> list = eQuery(ePQuery.toString());
            return list;
        }
    }
}

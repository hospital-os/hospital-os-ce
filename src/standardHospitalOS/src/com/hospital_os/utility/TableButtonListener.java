/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.utility;

import java.util.EventListener;

/**
 *
 * @author Somprasong
 */
public interface TableButtonListener extends EventListener {

    public void tableButtonClicked(int row, int col);
}

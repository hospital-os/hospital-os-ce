/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.LabResultItemGender;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class LabResultItemGenderDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "809";

    public LabResultItemGenderDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public int insert(LabResultItemGender obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO b_item_lab_result_gender(\n");
            sql.append("            b_item_lab_result_gender_id, b_item_lab_result_id, male_result_min, male_result_max, \n");
            sql.append("            female_result_min, female_result_max,\n");
            sql.append("            male_result_critical_min, male_result_critical_max, \n");
            sql.append("            female_result_critical_min, female_result_critical_max)\n");
            sql.append("    VALUES (?, ?, ?, ?, \n");
            sql.append("            ?, ?, \n");
            sql.append("            ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.getGenID(tableId));
            preparedStatement.setString(index++, obj.b_item_lab_result_id);
            preparedStatement.setString(index++, obj.male_result_min);
            preparedStatement.setString(index++, obj.male_result_max);
            preparedStatement.setString(index++, obj.female_result_min);
            preparedStatement.setString(index++, obj.female_result_max);
            preparedStatement.setString(index++, obj.male_result_critical_min);
            preparedStatement.setString(index++, obj.male_result_critical_max);
            preparedStatement.setString(index++, obj.female_result_critical_min);
            preparedStatement.setString(index++, obj.female_result_critical_max);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "delete from b_item_lab_result_gender where b_item_lab_result_gender_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            // execute delete SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int deleteByBItemLabResultId(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "delete from b_item_lab_result_gender where b_item_lab_result_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            // execute delete SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public LabResultItemGender select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_item_lab_result_gender where b_item_lab_result_gender_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<LabResultItemGender> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public LabResultItemGender selectByBItemLabResultId(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_item_lab_result_gender where b_item_lab_result_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<LabResultItemGender> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<LabResultItemGender> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<LabResultItemGender> list = new ArrayList<LabResultItemGender>();
        ResultSet rs = null;
        try {
            // execute insert SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                LabResultItemGender obj = new LabResultItemGender();
                obj.setObjectId(rs.getString("b_item_lab_result_gender_id"));
                obj.b_item_lab_result_id = rs.getString("b_item_lab_result_id");
                obj.male_result_min = rs.getString("male_result_min");
                obj.male_result_max = rs.getString("male_result_max");
                obj.female_result_min = rs.getString("female_result_min");
                obj.female_result_max = rs.getString("female_result_max");
                obj.male_result_critical_min = rs.getString("male_result_critical_min");
                obj.male_result_critical_max = rs.getString("male_result_critical_max");
                obj.female_result_critical_min = rs.getString("female_result_critical_min");
                obj.female_result_critical_max = rs.getString("female_result_critical_max");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }
}

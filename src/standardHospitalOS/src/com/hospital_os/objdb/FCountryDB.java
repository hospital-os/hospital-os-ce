/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.FCountry;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong Damyos <somprasong@hospital-os.com>
 */
public class FCountryDB {

    private final ConnectionInf theConnectionInf;

    public FCountryDB(ConnectionInf theConnectionInf) {
        this.theConnectionInf = theConnectionInf;
    }

    public FCountry select(String id) throws Exception {
        String sql = "select * from f_country where f_country_id = ?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, id);
            List<FCountry> v = eQuery(ePQuery.toString());
            return (FCountry) (v.isEmpty() ? null : v.get(0));
        }
    }

    public List<FCountry> list() throws Exception {
        return list(null);
    }

    public List<FCountry> list(String keyword) throws Exception {
        String sql = "select * from f_country\n";
        if (keyword != null && !keyword.isEmpty()) {
            sql += "where common_name_th ilike ?\n";
        }
        sql += "order by continent_th, common_name_th";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            if (keyword != null && !keyword.isEmpty()) {
                ePQuery.setString(index, "%" + keyword + "%");
            }
            return eQuery(ePQuery.toString());
        }
    }

    public List<FCountry> eQuery(String sql) throws Exception {
        FCountry p;
        List<FCountry> list = new ArrayList<>();
        try (ResultSet rs = theConnectionInf.eQuery(sql)) {
            while (rs.next()) {
                p = new FCountry();
                p.setObjectId(rs.getString("f_country_id"));
                p.continent_en = rs.getString("continent_en");
                p.continent_th = rs.getString("continent_th");
                p.common_name_en = rs.getString("common_name_en");
                p.common_name_th = rs.getString("common_name_th");
                p.official_name_en = rs.getString("official_name_en");
                p.official_name_th = rs.getString("official_name_th");
                p.capital_name_en = rs.getString("capital_name_en");
                p.capital_name_th = rs.getString("capital_name_th");
                list.add(p);
            }
        }
        return list;
    }
}

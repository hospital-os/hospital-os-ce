/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.FootExamAssess;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.Gutil;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class FootExamAssessDB {

    public ConnectionInf theConnectionInf;
    final public String idtable = "996";

    public FootExamAssessDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public int insert(FootExamAssess o) throws Exception {
        o.generateOID(idtable);
        String sql = "INSERT INTO t_foot_exam_assess( "
                + "t_foot_exam_assess_id, t_foot_exam_id, prodoscope_result, nail_problem,  "
                + "nail_problem_detail, wart_problem, wart_problem_detail, foot_deformities, "
                + "skin_color, hair_loss, skin_temperature, fungal_infections, record_date_time, "
                + "user_record_id, update_date_time, user_update_id) "
                + "VALUES ('%s', '%s', '%s', '%s',  "
                + "'%s', '%s', '%s', '%s',  "
                + "'%s', '%s', '%s', '%s', '%s',  "
                + "'%s', '%s', '%s')";
        Object[] values = new Object[]{
            o.getObjectId(), o.t_foot_exam_id, o.prodoscope_result, o.nail_problem,
            Gutil.CheckReservedWords(o.nail_problem_detail), o.wart_problem, Gutil.CheckReservedWords(o.wart_problem_detail), o.foot_deformities,
            o.skin_color, o.hair_loss, o.skin_temperature,
            o.fungal_infections, 
            o.record_date_time, o.user_record_id, o.update_date_time,
            o.user_update_id};
        return theConnectionInf.eUpdate(String.format(sql, values));
    }

    public int update(FootExamAssess o) throws Exception {
        String sql = "UPDATE t_foot_exam_assess "
                + "SET prodoscope_result='%s', nail_problem='%s',  "
                + "nail_problem_detail='%s', wart_problem='%s', wart_problem_detail='%s',  "
                + "foot_deformities='%s', skin_color='%s', hair_loss='%s',  "
                + "skin_temperature='%s', fungal_infections='%s',"
                + "update_date_time='%s',user_update_id='%s' "
                + "WHERE t_foot_exam_assess_id='%s'";
        Object[] values = new Object[]{
            o.prodoscope_result, o.nail_problem,
            Gutil.CheckReservedWords(o.nail_problem_detail), o.wart_problem, Gutil.CheckReservedWords(o.wart_problem_detail),
            o.foot_deformities, o.skin_color, o.hair_loss,
            o.skin_temperature, o.fungal_infections,
            o.update_date_time, o.user_update_id,
            o.getObjectId()};
        return theConnectionInf.eUpdate(String.format(sql, values));

    }

    public int delete(FootExamAssess o) throws Exception {
        String sql = "UPDATE t_foot_exam_assess "
                + "SET active='0',update_date_time='%s',user_update_id='%s' "
                + "WHERE t_foot_exam_assess_id='%s'";
        Object[] values = new Object[]{o.update_date_time,
            o.user_update_id, o.getObjectId()};
        return theConnectionInf.eUpdate(String.format(sql, values));
    }

    public FootExamAssess findByFootExamId(String visitId) throws Exception {
        String sql = "select * from t_foot_exam_assess where t_foot_exam_id = '%s'";
        List<FootExamAssess> list = eQuery(String.format(sql, visitId));
        return list.isEmpty() ? null : list.get(0);
    }

    private List<FootExamAssess> eQuery(String sql) throws Exception {
        List<FootExamAssess> list = new ArrayList<FootExamAssess>();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        while (rs.next()) {
            FootExamAssess o = new FootExamAssess();
            o.setObjectId(rs.getString("t_foot_exam_assess_id"));
            o.t_foot_exam_id = rs.getString("t_foot_exam_id");
            o.prodoscope_result = rs.getString("prodoscope_result");
            o.nail_problem = rs.getString("nail_problem");
            o.nail_problem_detail = rs.getString("nail_problem_detail");
            o.wart_problem = rs.getString("wart_problem");
            o.wart_problem_detail = rs.getString("wart_problem_detail");
            o.foot_deformities = rs.getString("foot_deformities");
            o.skin_color = rs.getString("skin_color");
            o.hair_loss = rs.getString("hair_loss");
            o.skin_temperature = rs.getString("skin_temperature");
            o.fungal_infections = rs.getString("fungal_infections");
            o.record_date_time = rs.getString("record_date_time");
            o.user_record_id = rs.getString("user_record_id");
            o.update_date_time = rs.getString("update_date_time");
            o.user_update_id = rs.getString("user_update_id");
            list.add(o);
        }
        rs.close();
        return list;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.InsuranceClaimBilling;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class InsuranceClaimBillingDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "821";

    public InsuranceClaimBillingDB(ConnectionInf c) {
        this.connectionInf = c;
    }

    public int insert(InsuranceClaimBilling obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO t_insurance_claim_billing(\n"
                    + "            t_insurance_claim_billing_id, b_finance_insurance_company_id, total_debt, total_discount, total_vat, total_tax, total_payment, f_payment_type_id, b_bank_info_id, bank_branch_name, account_number, payment_date, record_datetime, user_record_id)\n"
                    + "    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getGenID(tableId));
            preparedStatement.setString(2, obj.b_finance_insurance_company_id);
            preparedStatement.setDouble(3, obj.total_debt);
            preparedStatement.setDouble(4, obj.total_discount);
            preparedStatement.setDouble(5, obj.total_vat);
            preparedStatement.setDouble(6, obj.total_tax);
            preparedStatement.setDouble(7, obj.total_payment);
            preparedStatement.setString(8, obj.f_payment_type_id);
            preparedStatement.setString(9, obj.b_bank_info_id);
            preparedStatement.setString(10, obj.bank_branch_name);
            preparedStatement.setString(11, obj.account_number);
            preparedStatement.setDate(12, obj.payment_date == null ? null : new java.sql.Date(obj.payment_date.getTime()));
            preparedStatement.setString(13, obj.record_datetime);
            preparedStatement.setString(14, obj.user_record_id);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public InsuranceClaimBilling select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select t_insurance_claim_billing from t_insurance_claim_billing \n"
                    + "where t_insurance_claim_billing_id = ? ";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<InsuranceClaimBilling> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<InsuranceClaimBilling> listByCompanyId(String companyId) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select t_insurance_claim_billing from t_insurance_claim_billing \n"
                    + "where b_finance_insurance_company_id = ? order by payment_date";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, companyId);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<InsuranceClaimBilling> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<InsuranceClaimBilling> list = new ArrayList<InsuranceClaimBilling>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                InsuranceClaimBilling obj = new InsuranceClaimBilling();
                obj.setObjectId(rs.getString("t_insurance_claim_billing_id"));
                obj.b_finance_insurance_company_id = rs.getString("b_finance_insurance_company_id");
                obj.total_debt = rs.getDouble("total_debt");
                obj.total_discount = rs.getDouble("total_discount");
                obj.total_vat = rs.getDouble("total_vat");
                obj.total_payment = rs.getDouble("total_payment");
                obj.total_tax = rs.getDouble("total_tax");
                obj.f_payment_type_id = rs.getString("f_payment_type_id");
                obj.b_bank_info_id = rs.getString("b_bank_info_id");
                obj.bank_branch_name = rs.getString("bank_branch_name");
                obj.account_number = rs.getString("account_number");
                obj.payment_date = rs.getDate("payment_date");
                obj.record_datetime = rs.getString("record_datetime");
                obj.user_record_id = rs.getString("user_record_id");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }
}

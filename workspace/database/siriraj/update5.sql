CREATE TABLE b_map_srj_item_contract_plans
(
  b_map_srj_item_contract_plans_id character varying(255) NOT NULL,
  b_item_id character varying(255) NOT NULL,
  b_contract_plans_id character varying(255) NOT NULL,
  CONSTRAINT b_map_srj_item_contract_plans_pkey PRIMARY KEY (b_map_srj_item_contract_plans_id)
);

INSERT INTO s_siriraj_version VALUES ('9750000000005', '5', 'Siriraj, Community Edition', '2.4.011111', '1.1.011111', to_char(current_timestamp, 'YYYY-MM-DD HH24:MI:SS'));
/*
 * SequenceData.java
 *
 * Created on 18 ���Ҥ� 2546, 11:39 �.
 */
package com.hospital_os.object;

//import com.hospital_os.utility.*;
import com.hospital_os.usecase.connection.Persistent;
import java.text.DecimalFormat;

/**
 *
 * @author Administrator
 */
@SuppressWarnings("ClassWithoutLogger")
public class SequenceData extends Persistent {

    private static final long serialVersionUID = 1L;
    public String name = "";
    public String pattern = "";
    public String value = "";
    public String active = "";

    /**
     * Creates a new instance of SequenceData
     */
    public SequenceData() {
    }

    @Override
    public String toString() {
        return name;
    }

    /**
     * 00yy00000,12345/50 = 005012345 00yy00000,12345 = 005012345 c_date =
     * theHO.date_time = 2550-11-11,23:00
     *
     * @param pattern
     * @param text_in
     * @param c_date
     * @return
     */
    public static String getDBText(String pattern, String text_in, String c_date) {
        try {
            String text = text_in;
            int year = text_in.indexOf('/');
            String year2d;
            if (year != -1) {
                year2d = text.substring(year + 1);
                text = text.substring(0, year);
            } else {
                year2d = "";
            }
            int value = Integer.parseInt(text);
            return getDBText(pattern, value, year2d);
        } catch (Exception e) {
            return text_in;
        }
    }

    public static String getDBText(String pattern, int value, String year) {
        String patt = pattern;
        String prifix = "";
        if (pattern.indexOf("yy") != -1) {
            patt = pattern.substring(pattern.lastIndexOf("yy") + 2);
            prifix = year.isEmpty() ? "" : (pattern.substring(0, pattern.indexOf("yy")) + year);
        }
        if (pattern.indexOf('.') != -1) {
            patt = pattern.substring(pattern.lastIndexOf('.') + 1);
            prifix = pattern.substring(0, pattern.indexOf('.'));
        }
        DecimalFormat d = new DecimalFormat();
        d.applyPattern(patt);
        String show = prifix + d.format(value);
        return show;
    }

    public static String getGuiText(String pattern, String data) {
        try {
            int yy = pattern.indexOf("yy");
            String start = "";
            if (yy == -1) {
                return (start + Integer.parseInt(data));
            }

            String year = data.substring(yy, yy + 2);
            String number = data.substring(yy + 2);
            return (start + Integer.parseInt(number) + "/" + year);
        } catch (Exception e) {
            return data;
        }

    }
}

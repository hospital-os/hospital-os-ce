/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.RestfulUrl;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong Damyos <somprasong@hospital-os.com>
 */
public class RestfulUrlDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "999";

    public RestfulUrlDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public int insert(RestfulUrl obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO b_restful_url(\n");
            sql.append("b_restful_url_id, restful_code, restful_url, restful_headers, user_update_id, use_basic_auth, basic_auth_username, basic_auth_password)\n");
            sql.append("VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getGenID(tableId));
            preparedStatement.setString(2, obj.restful_code);
            preparedStatement.setString(3, obj.restful_url);
            preparedStatement.setString(4, obj.restful_headers);
            preparedStatement.setString(5, obj.user_update_id);
            preparedStatement.setBoolean(6, obj.use_basic_auth);
            preparedStatement.setString(7, obj.basic_auth_username);
            preparedStatement.setString(8, obj.basic_auth_password);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(RestfulUrl obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE b_restful_url\n");
            sql.append("   SET restful_code=?, restful_url=?, restful_headers=?, user_update_id=?, update_datetime=current_timestamp, use_basic_auth=?, basic_auth_username=?, basic_auth_password=?\n");
            sql.append(" WHERE b_restful_url_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.restful_code);
            preparedStatement.setString(2, obj.restful_url);
            preparedStatement.setString(3, obj.restful_headers);
            preparedStatement.setString(4, obj.user_update_id);
            preparedStatement.setBoolean(5, obj.use_basic_auth);
            preparedStatement.setString(6, obj.basic_auth_username);
            preparedStatement.setString(7, obj.basic_auth_password);
            preparedStatement.setString(8, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(RestfulUrl obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("delete from b_restful_url WHERE b_restful_url_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public RestfulUrl selectById(String id) throws Exception {
        if (id == null || id.isEmpty()) {
            return null;
        }
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_restful_url where b_restful_url_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<RestfulUrl> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public RestfulUrl selectByCode(String code) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_restful_url where restful_code = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, code);
            List<RestfulUrl> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<RestfulUrl> listAll() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_restful_url order by restful_code";
            preparedStatement = connectionInf.ePQuery(sql);
            List<RestfulUrl> list = executeQuery(preparedStatement);
            return list;
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<RestfulUrl> list(String keyword) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_restful_url where restful_code like ? order by restful_code";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, "%" + keyword + "%");
            List<RestfulUrl> list = executeQuery(preparedStatement);
            return list;
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<RestfulUrl> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<RestfulUrl> list = new ArrayList<>();
        try (ResultSet rs = preparedStatement.executeQuery()) {
            while (rs.next()) {
                RestfulUrl obj = new RestfulUrl();
                obj.setObjectId(rs.getString("b_restful_url_id"));
                obj.restful_code = rs.getString("restful_code");
                obj.restful_url = rs.getString("restful_url");
                obj.restful_headers = rs.getString("restful_headers");
                obj.user_update_id = rs.getString("user_update_id");
                obj.update_datetime = rs.getTimestamp("update_datetime");
                obj.is_system = rs.getBoolean("is_system");
                obj.use_basic_auth = rs.getBoolean("use_basic_auth");
                obj.basic_auth_username = rs.getString("basic_auth_username");
                obj.basic_auth_password = rs.getString("basic_auth_password");
                list.add(obj);
            }
            return list;
        }
    }

}

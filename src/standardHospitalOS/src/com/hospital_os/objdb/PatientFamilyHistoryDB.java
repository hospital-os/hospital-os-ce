/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.PatientFamilyHistory;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author sompr
 */
public class PatientFamilyHistoryDB {

    public ConnectionInf theConnectionInf;
    final public String idtable = "865";

    public PatientFamilyHistoryDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public int insert(PatientFamilyHistory p) throws Exception {
        String sql = "INSERT INTO t_patient_family_history(\n"
                + "            t_patient_id, f_patient_family_historys, patient_family_history_other, user_update_id, \n"
                + "            user_record_id, update_datetime, record_datetime)\n"
                + "    VALUES (?, ?::JSON, ?, ?, \n"
                + "            ?, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)";
        PreparedStatement ePQuery = null;
        try {
            ePQuery = theConnectionInf.ePQuery(sql);
            int index = 1;
            ePQuery.setString(index++, p.getObjectId());
            ePQuery.setString(index++, p.getFPatientFamilyHistorys());
            ePQuery.setString(index++, p.patient_family_history_other);
            ePQuery.setString(index++, p.user_update_id);
            ePQuery.setString(index++, p.user_record_id);
            return ePQuery.executeUpdate();
        } finally {
            if (ePQuery != null) {
                ePQuery.close();
            }
        }
    }

    public int update(PatientFamilyHistory p) throws Exception {
        String sql = "UPDATE t_patient_family_history SET f_patient_family_historys = ?::JSON, patient_family_history_other = ?,user_update_id = ?, update_datetime = CURRENT_TIMESTAMP where t_patient_id = ?";
        PreparedStatement ePQuery = null;
        try {
            ePQuery = theConnectionInf.ePQuery(sql);
            int index = 1;
            ePQuery.setString(index++, p.getFPatientFamilyHistorys());
            ePQuery.setString(index++, p.patient_family_history_other);
            ePQuery.setString(index++, p.user_update_id);
            ePQuery.setString(index++, p.getObjectId());
            return ePQuery.executeUpdate();
        } finally {
            if (ePQuery != null) {
                ePQuery.close();
            }
        }
    }

    public PatientFamilyHistory selectByPaitentId(String patientId) throws Exception {
        String sql = "select \n"
                + "        t_patient_id"
                + "        ,f_patient_family_historys->>'1' is not null as family_history1_select\n"
                + "        ,f_patient_family_historys->>'1' as family_history1_detail\n"
                + "        ,f_patient_family_historys->>'2' is not null as family_history2_select\n"
                + "        ,f_patient_family_historys->>'2' as family_history2_detail\n"
                + "        ,f_patient_family_historys->>'3' is not null as family_history3_select\n"
                + "        ,f_patient_family_historys->>'3' as family_history3_detail\n"
                + "        ,f_patient_family_historys->>'4' is not null as family_history4_select\n"
                + "        ,f_patient_family_historys->>'4' as family_history4_detail\n"
                + "        ,f_patient_family_historys->>'5' is not null as family_history5_select\n"
                + "        ,f_patient_family_historys->>'5' as family_history5_detail\n"
                + "        ,patient_family_history_other\n"
                + "from t_patient_family_history\n"
                + "where t_patient_id =  ?";
        PreparedStatement ePQuery = null;
        try {
            ePQuery = theConnectionInf.ePQuery(sql);
            int index = 1;
            ePQuery.setString(index++, patientId);
            PatientFamilyHistory p = new PatientFamilyHistory();
            ResultSet rs = ePQuery.executeQuery();
            while (rs.next()) {
                p = new PatientFamilyHistory();
                p.setObjectId(rs.getString("t_patient_id"));
                p.patient_family_history_other = rs.getString("patient_family_history_other");
                p.family_history1_select = rs.getBoolean("family_history1_select");
                p.family_history1_detail = rs.getString("family_history1_detail");
                p.family_history2_select = rs.getBoolean("family_history2_select");
                p.family_history2_detail = rs.getString("family_history2_detail");
                p.family_history3_select = rs.getBoolean("family_history3_select");
                p.family_history3_detail = rs.getString("family_history3_detail");
                p.family_history4_select = rs.getBoolean("family_history4_select");
                p.family_history4_detail = rs.getString("family_history4_detail");
                p.family_history5_select = rs.getBoolean("family_history5_select");
                p.family_history5_detail = rs.getString("family_history5_detail");
            }
            rs.close();
            return p;
        } finally {
            if (ePQuery != null) {
                ePQuery.close();
            }
        }
    }
}

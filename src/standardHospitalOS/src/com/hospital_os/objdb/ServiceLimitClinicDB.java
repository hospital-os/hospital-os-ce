/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.ServiceLimitClinic;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class ServiceLimitClinicDB {

    public ConnectionInf connectionInf;
    final public String tableId = "850";

    public ServiceLimitClinicDB(ConnectionInf db) {
        connectionInf = db;
    }

    public int insert(ServiceLimitClinic obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO b_service_limit_clinic(\n"
                    + "            b_service_limit_clinic_id, time_start, time_end, limit_appointment, limit_walkin, b_visit_clinic_id)\n"
                    + "    VALUES (?, ?, ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getGenID(tableId));
            preparedStatement.setString(2, obj.time_start);
            preparedStatement.setString(3, obj.time_end);
            preparedStatement.setInt(4, obj.limit_appointment);
            preparedStatement.setInt(5, obj.limit_walkin);
            preparedStatement.setString(6, obj.b_visit_clinic_id);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(ServiceLimitClinic obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE b_service_limit_clinic\n"
                    + "   SET b_visit_clinic_id=?, time_start=?, \n"
                    + "       time_end=?, limit_appointment=?, limit_walkin=?\n"
                    + " WHERE b_service_limit_clinic_id=?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.b_visit_clinic_id);
            preparedStatement.setString(2, obj.time_start);
            preparedStatement.setString(3, obj.time_end);
            preparedStatement.setInt(4, obj.limit_appointment);
            preparedStatement.setInt(5, obj.limit_walkin);
            preparedStatement.setString(6, obj.getObjectId());
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(ServiceLimitClinic obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM b_service_limit_clinic\n");
            sql.append(" WHERE b_service_limit_clinic_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public ServiceLimitClinic selectById(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select b_service_limit_clinic.* \n"
                    + ", b_visit_clinic.visit_clinic_description\n"
                    + "from b_service_limit_clinic \n"
                    + "inner join b_visit_clinic on b_visit_clinic.b_visit_clinic_id = b_service_limit_clinic = b_visit_clinic.b_visit_clinic_id \n"
                    + " where b_service_limit_clinic_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<ServiceLimitClinic> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<ServiceLimitClinic> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select b_service_limit_clinic.* \n"
                    + ", b_visit_clinic.visit_clinic_description\n"
                    + "from b_service_limit_clinic \n"
                    + "inner join b_visit_clinic on b_visit_clinic.b_visit_clinic_id = b_service_limit_clinic.b_visit_clinic_id \n"
                    + "order by  time_start::time asc, time_end::time asc";
            preparedStatement = connectionInf.ePQuery(sql);
            List<ServiceLimitClinic> list = executeQuery(preparedStatement);
            return list;
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
    
    public List<ServiceLimitClinic> listByClinicIdAndIntervalTime(String clinicId, String startTime, String endTime) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select b_service_limit_clinic.* \n"
                    + ", b_visit_clinic.visit_clinic_description\n"
                    + "from b_service_limit_clinic \n"
                    + "inner join b_visit_clinic on b_visit_clinic.b_visit_clinic_id =  b_service_limit_clinic.b_visit_clinic_id \n"
                    + "where b_visit_clinic.b_visit_clinic_id = ? and (time_start::time, time_end::time) OVERLAPS (?::time, ?::time) \n"
                    + "order by  time_start::time asc, time_end::time asc";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, clinicId);
            preparedStatement.setString(2, startTime);
            preparedStatement.setString(3, endTime);
            List<ServiceLimitClinic> list = executeQuery(preparedStatement);
            
            return list;
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
    

    public List<ServiceLimitClinic> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<ServiceLimitClinic> list = new ArrayList<ServiceLimitClinic>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                ServiceLimitClinic obj = new ServiceLimitClinic();
                obj.setObjectId(rs.getString("b_service_limit_clinic_id"));
                obj.time_start = rs.getString("time_start");
                obj.time_end = rs.getString("time_end");
                obj.limit_appointment = rs.getInt("limit_appointment");
                obj.limit_walkin = rs.getInt("limit_walkin");
                obj.b_visit_clinic_id = rs.getString("b_visit_clinic_id");
                try{
                    obj.clinicName = rs.getString("visit_clinic_description");
                }catch(Exception ex){
                    
                }
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }
}

CREATE TABLE t_admit_leave_day
(
    t_admit_leave_day_id character varying(255) NOT NULL,
    t_visit_id character varying(255) NOT NULL,
    leave_seq integer NOT NULL,
    date_out character varying(10) NOT NULL,
    time_out character varying(6) NOT NULL,
    comeback character varying(1) NOT NULL DEFAULT '0',
    date_in character varying(10),
    time_in character varying(6),
    leave_cause text NOT NULL,
    doctor_approve character varying(255) NOT NULL,
    staff_record character varying(255) NOT NULL,
    record_datetime character varying(19) NOT NULL,
    staff_modify character varying(255) NOT NULL,
    modify_datetime character varying(19) NOT NULL,
    active character varying(1) NOT NULL DEFAULT '1',
   CONSTRAINT t_admit_leave_day_pkey PRIMARY KEY (t_admit_leave_day_id)
);
CREATE INDEX t_visit_id_admit_leave_day
  ON t_admit_leave_day
  USING btree
  (t_visit_id);

CREATE TABLE t_eyes_exam
(
  t_eyes_exam_id character varying(255) NOT NULL,
  t_visit_id character varying(255) NOT NULL,
  left_eyes_result character varying(255),
  right_eyes_result character varying(255),
  left_eyes_diagnosis character varying(255),
  right_eyes_diagnosis character varying(255),
  left_eyes_csme character varying(255),
  right_eyes_csme character varying(255),
  left_eyes_opticnerve character varying(255),
  right_eyes_opticnerve character varying(255),
  left_eyes_opticnerve_detail character varying(255),
  right_eyes_opticnerve_detail character varying(255),
  left_eyes_haemorrhage character varying(255),
  right_eyes_haemorrhage character varying(255),
  left_eyes_exudates character varying(255),
  right_eyes_exudates character varying(255),
  left_eyes_bg character varying(255),
  right_eyes_bg character varying(255),
  left_eyes_macula_edema character varying(255),
  right_eyes_macula_edema character varying(255),
  other_diseases_found character varying(255),
  other_diseases_cataract character varying(255),
  other_diseases_glaucoma character varying(255),
  other_diseases_pterygium character varying(255),
  other_diseases_other character varying(255),
  other_diseases_other_detail character varying(255),
  doctor_recommend character varying(255),
  eyes_management character varying(255),
  appointment_date character varying(255),
  appointment_place character varying(255),
  doctor_diag character varying(25),
  screen_date character varying(25) NOT NULL,
  active character varying(1) NOT NULL,
  record_date_time character varying(19) NOT NULL,
  user_record_id character varying(255) NOT NULL,
  update_date_time character varying(19) NOT NULL,
  user_update_id character varying(255) NOT NULL,
  CONSTRAINT t_eyes_exam_pkey PRIMARY KEY (t_eyes_exam_id)
);
CREATE INDEX t_visit_id_eyes_exam
  ON t_eyes_exam
  USING btree
  (t_visit_id);

CREATE TABLE t_foot_exam
(
  t_foot_exam_id character varying(255) NOT NULL,
  t_visit_id character varying(255) NOT NULL,
  foot_ulcers character varying(255),
  foot_ulcers_detail character varying(255),
  lose_sensation character varying(255),
  shoes_regularly character varying(255),
  shoes_regularly_other_detail character varying(255),
  active character varying(1) NOT NULL,
  record_date_time character varying(19) NOT NULL,
  user_record_id character varying(255) NOT NULL,
  update_date_time character varying(19) NOT NULL,
  user_update_id character varying(255) NOT NULL,
  CONSTRAINT t_foot_exam_pkey PRIMARY KEY (t_foot_exam_id)
);
CREATE INDEX t_visit_id_foot_exam
  ON t_foot_exam
  USING btree
  (t_visit_id);

CREATE TABLE t_foot_exam_assess
(
  t_foot_exam_assess_id character varying(255) NOT NULL,
  t_foot_exam_id character varying(255) NOT NULL,
  prodoscope_result character varying(255),
  nail_problem character varying(255),
  nail_problem_detail character varying(255),
  wart_problem character varying(255),
  wart_problem_detail character varying(255),
  foot_deformities character varying(255),
  skin_color character varying(255),
  hair_loss character varying(255),
  skin_temperature character varying(255),
  fungal_infections character varying(255),
  record_date_time character varying(19) NOT NULL,
  user_record_id character varying(255) NOT NULL,
  update_date_time character varying(19) NOT NULL,
  user_update_id character varying(255) NOT NULL,
  CONSTRAINT t_foot_exam_assess_pkey PRIMARY KEY (t_foot_exam_assess_id)
);
CREATE INDEX t_foot_exam_id_foot_exam_assess
  ON t_foot_exam_assess
  USING btree
  (t_foot_exam_id);

CREATE TABLE t_foot_exam_assess_detail
(
  t_foot_exam_assess_detail_id character varying(255) NOT NULL,
  t_foot_exam_assess_id character varying(255) NOT NULL,
  f_foot_assess_type_id character varying(1) NOT NULL,
  other_detail character varying(255),
  record_date_time character varying(19) NOT NULL,
  user_record_id character varying(255) NOT NULL,
  update_date_time character varying(19) NOT NULL,
  user_update_id character varying(255) NOT NULL,
  CONSTRAINT t_foot_exam_assess_detail_pkey PRIMARY KEY (t_foot_exam_assess_detail_id)
);
CREATE INDEX t_foot_exam_assess_id_foot_exam_assess_detail
  ON t_foot_exam_assess_detail
  USING btree
  (t_foot_exam_assess_id);

CREATE TABLE t_foot_artery
(
  t_foot_artery_id character varying(255) NOT NULL,
  t_visit_id character varying(255) NOT NULL,
  left_dorsalis_pedis character varying(255),
  right_dorsalis_pedis character varying(255),
  left_posterior_tibial character varying(255),
  right_posterior_tibial character varying(255),
  left_gangrene character varying(255),
  right_gangrene character varying(255),
  left_sensory character varying(255),
  left_sensory_count character varying(255),
  right_sensory character varying(255),
  right_sensory_count character varying(255),
  wound character varying(255),
  wound_position character varying(255),
  wound_area character varying(255),
  wound_size character varying(255),
  active character varying(1) NOT NULL,
  record_date_time character varying(19) NOT NULL,
  user_record_id character varying(255) NOT NULL,
  update_date_time character varying(19) NOT NULL,
  user_update_id character varying(255) NOT NULL,
  CONSTRAINT t_foot_artery_pkey PRIMARY KEY (t_foot_artery_id)
);
CREATE INDEX t_visit_id_foot_artery
  ON t_foot_artery
  USING btree
  (t_visit_id);

CREATE TABLE t_foot_result
(
  t_foot_result_id character varying(255) NOT NULL,
  t_visit_id character varying(255) NOT NULL,
  f_foot_screen_result_id character varying(1) NOT NULL,
  appoint_date character varying(10),
  assessor character varying(255),
  assessor_date character varying(10),
  active character varying(1) NOT NULL,
  record_date_time character varying(19) NOT NULL,
  user_record_id character varying(255) NOT NULL,
  update_date_time character varying(19) NOT NULL,
  user_update_id character varying(255) NOT NULL,
  CONSTRAINT t_foot_result_pkey PRIMARY KEY (t_foot_result_id)
);
CREATE INDEX t_visit_id_foot_result
  ON t_foot_result
  USING btree
  (t_visit_id);

CREATE TABLE t_foot_protect
(
  t_foot_protect_id character varying(255) NOT NULL,
  t_foot_result_id character varying(255) NOT NULL,
  f_foot_protect_type_id character varying(1) NOT NULL,
  record_date_time character varying(19) NOT NULL,
  user_record_id character varying(255) NOT NULL,
  CONSTRAINT t_foot_protect_pkey PRIMARY KEY (t_foot_protect_id)
);
CREATE INDEX t_foot_result_id_foot_protect
  ON t_foot_protect
  USING btree
  (t_foot_result_id);

CREATE TABLE f_foot_exam_assess_type
(
  f_foot_exam_assess_type_id character varying(1) NOT NULL,
  description character varying(255),
  CONSTRAINT f_foot_exam_assess_type_pkey PRIMARY KEY (f_foot_exam_assess_type_id)
);

insert into f_foot_exam_assess_type values ('1','hammer toes ');
insert into f_foot_exam_assess_type values ('2','claw toes');
insert into f_foot_exam_assess_type values ('3','bunions');
insert into f_foot_exam_assess_type values ('9','nony_prominence ');
insert into f_foot_exam_assess_type values ('5','char_cot_foot  ');
insert into f_foot_exam_assess_type values ('6','other');

CREATE TABLE f_foot_screen_result
(
  f_foot_screen_result_id character varying(1) NOT NULL,
  description character varying(255),
  CONSTRAINT f_foot_screen_result_pkey PRIMARY KEY (f_foot_screen_result_id)
);

insert into f_foot_screen_result values ('1','low risk');
insert into f_foot_screen_result values ('2','Moderate risk');
insert into f_foot_screen_result values ('3','High risk');
insert into f_foot_screen_result values ('4','Very High risk (FOOT ULCER)');
insert into f_foot_screen_result values ('5','Very High risk (AMPUTATION)');
insert into f_foot_screen_result values ('9','ไม่ได้ตรวจ');

CREATE TABLE f_foot_protect_type
(
  f_foot_protect_type character varying(255) NOT NULL,
  description character varying(255),
  CONSTRAINT f_foot_protect_type_pkey PRIMARY KEY (f_foot_protect_type)
);

insert into f_foot_protect_type values ('1','ดูแลเท้าประจำวันและรองเท้าที่เหมาะสม');
insert into f_foot_protect_type values ('2','รองเท้าพิเศษ');
insert into f_foot_protect_type values ('3','ตัดเล็บ');
insert into f_foot_protect_type values ('4','ควบคุมน้ำตาล');
insert into f_foot_protect_type values ('5','ขูด callus');
insert into f_foot_protect_type values ('6','ทาโลชั่น');
insert into f_foot_protect_type values ('7','ส่งต่อคลินิกเท้า');

CREATE TABLE f_eyes_result_type
(
  f_eyes_result_type_id character varying(2) NOT NULL,
  description character varying(255),
  CONSTRAINT f_eyes_result_type_pkey PRIMARY KEY (f_eyes_result_type_id)
);

insert into f_eyes_result_type values ('0','ไม่ระบุ');
insert into f_eyes_result_type values ('1','No PL');
insert into f_eyes_result_type values ('2','PL');
insert into f_eyes_result_type values ('3','PJ');
insert into f_eyes_result_type values ('4','HM');
insert into f_eyes_result_type values ('5','FC');
insert into f_eyes_result_type values ('6','20/200(6/60)');
insert into f_eyes_result_type values ('7','20/100(6/36)');
insert into f_eyes_result_type values ('8','20/100(6/36)');
insert into f_eyes_result_type values ('9','20/70(6/24)');
insert into f_eyes_result_type values ('10','20/50');
insert into f_eyes_result_type values ('11','20/40');
insert into f_eyes_result_type values ('12','20/30(6/9)');
insert into f_eyes_result_type values ('13','20/20(6/6)');
insert into f_eyes_result_type values ('14','20/15');
insert into f_eyes_result_type values ('15','5/60');
insert into f_eyes_result_type values ('16','4/60');
insert into f_eyes_result_type values ('17','3/60');
insert into f_eyes_result_type values ('18','2/60');
insert into f_eyes_result_type values ('19','1/60');

CREATE TABLE f_foot_prodoscope_type
(
  f_foot_prodoscope_type_id character varying(1) NOT NULL,
  description character varying(255),
  CONSTRAINT f_foot_prodoscope_type_pkey PRIMARY KEY (f_foot_prodoscope_type_id)
);

insert into f_foot_prodoscope_type values ('1','Normal');
insert into f_foot_prodoscope_type values ('2','Flat Foot ');
insert into f_foot_prodoscope_type values ('3','High Arch Foot');
insert into f_foot_prodoscope_type values ('9','ไม่ระบุ');

CREATE TABLE f_eyes_diagnosis_type
(
  f_eyes_diagnosis_type_id character varying(1) NOT NULL,
  description character varying(255),
  CONSTRAINT f_eyes_diagnosis_type_pkey PRIMARY KEY (f_eyes_diagnosis_type_id)
);

insert into f_eyes_diagnosis_type values ('0','ไม่ระบุ');
insert into f_eyes_diagnosis_type values ('1','No DR');
insert into f_eyes_diagnosis_type values ('2','Mild NPDR');
insert into f_eyes_diagnosis_type values ('3','Moderate NPDR');
insert into f_eyes_diagnosis_type values ('4','Severe NPDR');
insert into f_eyes_diagnosis_type values ('5','CSME');
insert into f_eyes_diagnosis_type values ('6','PDR');

INSERT INTO b_item_billing_subgroup (b_item_billing_subgroup_id,item_billing_subgroup_number,item_billing_subgroup_description,f_item_billing_group_id,item_billing_subgroup_active)
VALUES ('TBBS01','TBBS01','ไม่ระบุ','09','1');
INSERT INTO b_item_subgroup (b_item_subgroup_id,item_subgroup_number,item_subgroup_description,f_item_group_id,item_subgroup_active)
VALUES ('TBS01','TBS01','ไม่ระบุ','2','1');

INSERT INTO b_item (b_item_id,item_number,item_common_name,item_active,b_item_subgroup_id,b_item_billing_subgroup_id,item_secret,b_item_16_group_id) 
VALUES ('174TB00001','LabTB001','Sputum AFB Collect','1','TBS01','TBBS01','sort','3120000000007');
INSERT INTO b_item (b_item_id,item_number,item_common_name,item_active,b_item_subgroup_id,b_item_billing_subgroup_id,item_secret,b_item_16_group_id) 
VALUES ('174TB00002','LabTB002','Sputum AFB Spot','1','TBS01','TBBS01','sort','3120000000007');

INSERT INTO b_item_price (b_item_price_id,b_item_id,item_price_active_date,item_price,item_price_cost)
VALUES ('175TB00001','174TB00001','2554-12-09,14:29','60','0.00');
INSERT INTO b_item_price (b_item_price_id,b_item_id,item_price_active_date,item_price,item_price_cost)
VALUES ('175TB00002','174TB00002','2554-12-09,14:29','60','0.00');

INSERT INTO b_item_lab_result (b_item_lab_result_id,item_lab_result_name,b_item_id,f_lab_result_type_id,item_ncd_fbs,item_ncd_hct)
VALUES ('TBLR01','Sputum AFB Collect','174TB00001','2','0','0');
INSERT INTO b_item_lab_result (b_item_lab_result_id,item_lab_result_name,b_item_id,f_lab_result_type_id,item_ncd_fbs,item_ncd_hct)
VALUES ('TBLR02','Sputum AFB Spot','174TB00002','2','0','0');

INSERT INTO b_group_chronic VALUES ('1650000000002','26','วัณโรค','Tuberculous','1');

UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002', group_icd10_b_group_chronic_id = '26' WHERE group_icd10_number = 'A15';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A15.0';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A15.1';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A15.2';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A15.3';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A15.4';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A15.5';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A15.6';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A15.7';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A15.8';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A15.9';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002', group_icd10_b_group_chronic_id = '26' WHERE group_icd10_number = 'A16';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A16.0';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A16.1';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A16.2';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A16.3';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A16.4';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A16.5';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A16.7';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A16.8';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A16.9';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002', group_icd10_b_group_chronic_id = '26' WHERE group_icd10_number = 'A17';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A17.0';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A17.1';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A17.8';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A17.9';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002', group_icd10_b_group_chronic_id = '26' WHERE group_icd10_number = 'A18';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A18.0';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A18.1';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A18.2';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A18.3';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A18.4';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A18.5';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A18.6';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A18.7';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A18.8';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002', group_icd10_b_group_chronic_id = '26' WHERE group_icd10_number = 'A19';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A19.0';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A19.1';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A19.2';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A19.8';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A19.9';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002', group_icd10_b_group_chronic_id = '26' WHERE group_icd10_number = 'A30';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A30.0';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A30.1';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A30.2';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A30.3';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A30.4';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A30.5';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A30.8';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A30.9';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002', group_icd10_b_group_chronic_id = '26' WHERE group_icd10_number = 'A31';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A31.0';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A31.1';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A31.8';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'A31.9';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002', group_icd10_b_group_chronic_id = '26' WHERE group_icd10_number = 'B90';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'B90.0';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'B90.1';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'B90.2';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'B90.8';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'B90.9';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002', group_icd10_b_group_chronic_id = '26' WHERE group_icd10_number = 'E35';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'E35.0';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'E35.1';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'E35.8';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002', group_icd10_b_group_chronic_id = '26' WHERE group_icd10_number = 'H03';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'H03.0';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'H03.1';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'H03.8';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002', group_icd10_b_group_chronic_id = '26' WHERE group_icd10_number = 'H22';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'H22.0';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'H22.1';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'H22.8';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002', group_icd10_b_group_chronic_id = '26' WHERE group_icd10_number = 'H67';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'H67.0';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'H67.1';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'H67.8';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002', group_icd10_b_group_chronic_id = '26' WHERE group_icd10_number = 'J61';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002', group_icd10_b_group_chronic_id = '26' WHERE group_icd10_number = 'J62';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'J62.0';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'J62.8';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002', group_icd10_b_group_chronic_id = '26' WHERE group_icd10_number = 'J63';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'J63.0';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'J63.1';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'J63.2';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'J63.3';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'J63.4';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'J63.5';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'J63.8';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002', group_icd10_b_group_chronic_id = '26' WHERE group_icd10_number = 'J64';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002', group_icd10_b_group_chronic_id = '26' WHERE group_icd10_number = 'J65';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002', group_icd10_b_group_chronic_id = '26' WHERE group_icd10_number = 'J86';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'J86.0';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'J86.9';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002', group_icd10_b_group_chronic_id = '26' WHERE group_icd10_number = 'M68';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M68.0';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M68.00';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M68.01';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M68.02';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M68.03';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M68.04';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M68.05';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M68.06';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M68.07';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M68.08';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M68.09';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M68.8';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002', group_icd10_b_group_chronic_id = '26' WHERE group_icd10_number = 'M90';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.0';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.00';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.01';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.02';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.03';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.04';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.05';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.06';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.07';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.08';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.09';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.1';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.10';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.11';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.12';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.13';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.14';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.15';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.16';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.17';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.18';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.19';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.2';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.20';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.21';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.22';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.23';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.24';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.25';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.26';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.27';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.28';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.29';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.3';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.30';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.31';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.32';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.33';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.34';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.35';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.36';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.37';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.38';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.39';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.4';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.40';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.41';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.42';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.43';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.44';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.45';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.46';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.47';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.48';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.49';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.5';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.50';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.51';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.52';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.53';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.54';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.55';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.56';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.57';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.58';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.59';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.6';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.60';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.61';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.62';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.63';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.64';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.65';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.66';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.67';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.68';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.69';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.7';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.70';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.71';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.72';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.73';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.74';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.75';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.76';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.77';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.78';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.79';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.8';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.80';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.81';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.82';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.83';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.84';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.85';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.86';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.87';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.88';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.89';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'M90.9';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002', group_icd10_b_group_chronic_id = '26' WHERE group_icd10_number = 'N29';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'N29.0';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'N29.1';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'N29.8';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002', group_icd10_b_group_chronic_id = '26' WHERE group_icd10_number = 'N51';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'N51.0';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'N51.1';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'N51.2';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'N51.8';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002', group_icd10_b_group_chronic_id = '26' WHERE group_icd10_number = 'N77';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'N77.0';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'N77.1';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'N77.8';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002', group_icd10_b_group_chronic_id = '26' WHERE group_icd10_number = 'O98';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'O98.0';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'O98.1';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'O98.2';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'O98.3';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'O98.4';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'O98.5';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'O98.6';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'O98.8';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'O98.9';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002', group_icd10_b_group_chronic_id = '26' WHERE group_icd10_number = 'P37';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'P37.0';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'P37.1';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'P37.2';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'P37.3';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'P37.4';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'P37.5';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'P37.8';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'P37.9';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002', group_icd10_b_group_chronic_id = '26' WHERE group_icd10_number = 'Z03';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'Z03.0';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'Z03.1';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'Z03.2';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'Z03.3';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'Z03.4';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'Z03.5';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'Z03.6';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'Z03.8';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'Z03.9';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002', group_icd10_b_group_chronic_id = '26' WHERE group_icd10_number = 'Z11';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'Z11.0';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'Z11.1';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'Z11.2';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'Z11.3';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'Z11.4';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'Z11.5';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'Z11.6';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'Z11.8';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'Z11.9';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002', group_icd10_b_group_chronic_id = '26' WHERE group_icd10_number = 'Z20';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'Z20.0';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'Z20.1';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'Z20.2';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'Z20.3';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'Z20.4';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'Z20.5';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'Z20.6';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'Z20.7';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'Z20.8';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'Z20.9';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002', group_icd10_b_group_chronic_id = '26' WHERE group_icd10_number = 'Z23';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'Z23.0';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'Z23.1';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'Z23.2';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'Z23.3';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'Z23.4';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'Z23.5';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'Z23.6';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'Z23.7';
UPDATE b_group_icd10 SET b_group_chronic_id = '1650000000002' WHERE group_icd10_number = 'Z23.8';

ALTER TABLE b_item ADD COLUMN item_general_number character varying(255);

ALTER TABLE t_visit_refer_in_out  ADD COLUMN with_nurse character varying(1) NOT NULL DEFAULT '0';
ALTER TABLE t_visit_refer_in_out  ADD COLUMN with_nurse_amount integer NOT NULL DEFAULT 0;
ALTER TABLE t_visit_refer_in_out  ADD COLUMN with_abl character varying(1) NOT NULL DEFAULT '0';
ALTER TABLE t_visit_refer_in_out  ADD COLUMN coordinate_hours character varying(3) NOT NULL DEFAULT '0';
ALTER TABLE t_visit_refer_in_out  ADD COLUMN coordinate_minute character varying(2) NOT NULL DEFAULT '0';
ALTER TABLE t_visit_refer_in_out  ADD COLUMN accept_time character varying(5) NOT NULL DEFAULT '00:00';
ALTER TABLE t_visit_refer_in_out  ADD COLUMN refer_expire character varying(10);
ALTER TABLE t_visit_refer_in_out  ALTER COLUMN visit_refer_in_out_othdetail SET DEFAULT '';

update t_visit_refer_in_out 
set record_date_time = (to_number(substring(to_char(current_date, 'YYYY-MM-DD') from 1 for 4), '9999') +543) 
|| substring(to_char(current_date, 'YYYY-MM-DD') from 5 for 6)
where record_date_time is null or record_date_time = '';

update t_visit_refer_in_out 
set refer_expire = to_char((to_date(((to_number(substring(record_date_time from 1 for 4), '9999') -543) 
|| substring(record_date_time from 5 for 6)),'YYYY-MM-DD') + 90), 'YYYY-MM-DD') 
where t_visit_refer_in_out_id in (select t_visit_refer_in_out.t_visit_refer_in_out_id from t_visit_refer_in_out
inner join t_visit on t_visit_refer_in_out.t_visit_id = t_visit.t_visit_id
where t_visit.f_visit_type_id = '0');

update t_visit_refer_in_out 
set refer_expire = to_char((to_date(((to_number(substring(record_date_time from 1 for 4), '9999') -543) 
|| substring(record_date_time from 5 for 6)),'YYYY-MM-DD') + 60), 'YYYY-MM-DD') 
where t_visit_refer_in_out_id in (select t_visit_refer_in_out.t_visit_refer_in_out_id from t_visit_refer_in_out
inner join t_visit on t_visit_refer_in_out.t_visit_id = t_visit.t_visit_id
where t_visit.f_visit_type_id = '1');

update t_visit_refer_in_out 
set refer_expire = (to_number(substring(to_char(current_date, 'YYYY-MM-DD') from 1 for 4), '9999') +543) 
|| substring(to_char(current_date, 'YYYY-MM-DD') from 5 for 6)
where refer_expire is null or refer_expire = '';

ALTER TABLE t_visit_refer_in_out ALTER COLUMN refer_expire SET NOT NULL;

ALTER TABLE t_visit ADD COLUMN visit_staff_financial_reverse character varying(255) default '';
ALTER TABLE t_visit ADD COLUMN visit_financial_reverse_date_time character varying(19) default '';
ALTER TABLE t_visit ADD COLUMN visit_staff_doctor_reverse character varying(255) default '';
ALTER TABLE t_visit ADD COLUMN visit_doctor_reverse_date_time character varying(19) default '';
ALTER TABLE t_visit ADD COLUMN visit_modify_staff character varying(255) default '';
ALTER TABLE t_visit ADD COLUMN visit_modify_date_time character varying(19) default '';

INSERT INTO f_gui_action (f_gui_action_id, gui_action_name, gui_action_note) 
	VALUES ('0608', 'ข้อมูลลากลับบ้าน', NULL);

INSERT INTO s_version VALUES ('9701000000051', '51', 'Hospital OS, Community Edition', '3.9.18', '3.18.091111', '2554-11-09 00:00:00');

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_18.sql',(select current_date) || ','|| (select current_time),'ปรับแก้สำหรับhospitalOS3.9.18');
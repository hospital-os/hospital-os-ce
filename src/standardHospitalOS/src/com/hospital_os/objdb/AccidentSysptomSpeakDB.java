/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.AccidentSysptomSpeak;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class AccidentSysptomSpeakDB {

    private final ConnectionInf connectionInf;

    public AccidentSysptomSpeakDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public List<AccidentSysptomSpeak> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_accident_symptom_speak";
            preparedStatement = connectionInf.ePQuery(sql);
            
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<AccidentSysptomSpeak> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<AccidentSysptomSpeak> list = new ArrayList<AccidentSysptomSpeak>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                AccidentSysptomSpeak obj = new AccidentSysptomSpeak();
                obj.setObjectId(rs.getString("f_accident_symptom_speak_id"));
                obj.description = rs.getString("accident_symptom_speak_description");
                obj.accident_symptom_speak_score = rs.getString("accident_symptom_speak_score");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (AccidentSysptomSpeak obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }
    
    public AccidentSysptomSpeak select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_accident_symptom_speak where f_accident_symptom_speak_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<AccidentSysptomSpeak> executeQuery = executeQuery(preparedStatement);
            return executeQuery.isEmpty() ? null : executeQuery.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
}

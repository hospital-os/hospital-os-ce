/*
 * LabReferControl.java
 *
 * Created on 22 ����Ҥ� 2547, 09:34 �.
 */
package com.hosv3.control;

import com.hospital_os.object.*;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hospital_os.utility.ComboFix;
import com.hospital_os.utility.Gutil;
import com.hospital_os.utility.Secure;
import com.hosv3.object.HosObject;
import com.hosv3.object.QueueLab2;
import com.hosv3.subject.HosSubject;
import com.hosv3.utility.ResourceBundle;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import java.util.logging.Logger;

/**
 *
 * @author Amp
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class ResultControl {

    private static final Logger LOG = Logger.getLogger(BillingControl.class.getName());
    ConnectionInf theConnectionInf;
    HosDB theHosDB;
    HosObject theHO;
    HosSubject theHS;
    UpdateStatus theUS;
    LookupControl theLookupControl;
    VisitControl theVisitControl;
    OrderControl theOrderControl;
    SystemControl theSystemControl;
    private HosControl hosControl;

    public ResultControl(ConnectionInf c, HosObject ho, HosDB hdb, HosSubject hs) {
        theConnectionInf = c;
        theHosDB = hdb;
        theHO = ho;
        theHS = hs;
    }

    public void setSystemControl(SystemControl systemControl) {
        theSystemControl = systemControl;
    }

    public void setHosControl(HosControl hosControl) {
        this.hosControl = hosControl;
    }

    public void setDepControl(LookupControl lc, VisitControl vc, OrderControl oc) {
        theLookupControl = lc;
        theVisitControl = vc;
        theOrderControl = oc;
    }

    public void setUpdateStatus(UpdateStatus us) {
        theUS = us;
    }

    /**
     * ��Ǩ�ͺ������� pid �������¤����㹰ҹ���������������ѧ
     * �Ѵ�͡�����������´ uc ���� 27/11/47
     *
     * @param pid
     * @return
     */
    public boolean checkPidPatientLabreferin(String pid) {
        theConnectionInf.open();
        boolean result = true;
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector answer = theHosDB.thePatientLabreferinDB.queryPid(pid);
            theConnectionInf.getConnection().commit();
            if (answer == null || answer.isEmpty()) {
                result = false;
            } else {
                result = true;
            }
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    /**
     * ���������ż����·���ա���� labreferin �Ѵ�͡�����������´ uc ����
     * 27/11/47
     *
     * @param p
     */
    public void savePatientLabreferin(PatientLabreferin p) {
        if (p == null) {
            theUS.setStatus(
                    ResourceBundle.getBundleText("com.hosv3.control.ResultControl.MISS.INFORMATION"), UpdateStatus.WARNING);
            return;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (p.getObjectId() == null) {
                theHosDB.thePatientLabreferinDB.insert(p);
            } else {
                theHosDB.thePatientLabreferinDB.update(p);
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.IMPORT.LAB") + " "
                    + ResourceBundle.getBundleText("TEXT.FAIL"), UpdateStatus.ERROR);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theResultSubject.notifyManagePatientLabReferIn(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.IMPORT.LAB") + " "
                    + ResourceBundle.getBundleText("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        }
    }

    /**
     * ���Ҽ����¨ҡ�������͹��ʡ�� �Ѵ�͡�����������´ uc ���� 27/11/47
     *
     * @param fname
     * @param lname
     * @return
     */
    public Vector listPatientLabreferinByName(String fname, String lname) {
        Vector result = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if ((!fname.isEmpty()) && (!lname.isEmpty())) {
                result = theHosDB.thePatientLabreferinDB.queryByFLName("%" + fname + "%", "%" + lname + "%");
            } else if (!fname.isEmpty()) {
                result = theHosDB.thePatientLabreferinDB.queryByFName("%" + fname + "%");
            } else if (!lname.isEmpty()) {
                result = theHosDB.thePatientLabreferinDB.queryBySName("%" + lname + "%");
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    /**
     * ź�������͡�ҡ�ҹ������ �Ѵ�͡�����������´ uc ���� 27/11/47
     *
     * @param patientLabreferin
     */
    public void deletePatientLabreferIn(PatientLabreferin patientLabreferin) {
        if (patientLabreferin == null) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.MISS.INFORMATION"), UpdateStatus.WARNING);
            return;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.thePatientLabreferinDB.delete(patientLabreferin);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
    }

    /**
     * ��㹡���ʴ�����ѵ� XN �ͧ������
     *
     * @param patient_id �� �Ţ patient_id �ͧ������
     * @param showinactive �� boolean ����˹��������ʴ��������(�ʴ��ŷ����
     * active ����)
     * @return �� Vector �ͧ Object PatientXN
     * @author padungrat(tong)
     * @date 18/04/2549,11:12
     */
    public Vector listPatientXnByPatientID(String patient_id, boolean showinactive) {
        return this.listPatientXnByPatientIDAndXn(patient_id, null, showinactive);
    }

    public Vector listPatientXnByPatientIDAndXn(String patient_id, String xn, boolean showinactive) {
        Vector vector = null;
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vector = theHosDB.thePatientXNDB.selectByPatientID(patient_id, xn, showinactive);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_QUERY, ResourceBundle.getBundleText("com.hosv3.control.ResultControl.SHOW.XN.HISTORY"));
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_QUERY, ResourceBundle.getBundleText("com.hosv3.control.ResultControl.SHOW.XN.HISTORY"));
        }
        return vector == null ? new Vector() : vector;
    }

    /**
     * @param xn
     * @param gen
     * @return �� boolean ��� �� true �ʴ���Ҥ�ҫ�� ����� false �����
     * @author padungrat(tong)
     * @date 04/04/2549,11:30 //��Ǩ�ͺ��Ҥ�� str �繤����ҧ�������
     * //����繤����ҧ //��Ǩ�ͺ��Ҥ�� str �Ѻ���� object �ͧ patient
     * �繤�����ǡѹ������� // ����繤�����ǡѺ // �ӡ�� update ŧ���ҧ
     * t_patient_xn �ͧ�Ţ xn � object �ͧ patient ��� active �� 0
     * //����õ�Ǩ�ͺ //��Ǩ�ͺ��� str ���������� t_patient_xn ������� //
     * ������ // ��Ǩ�ͺ�� �.�. ����繹��¡��������繻ջѨ�غѹ������� //
     * ����� ������ҧ�Ţ xn ���� ��зӡ�� insert ������ŧ���ҧ t_patient_xn //
     * �������ŧ� object �ͧ patient // ��������� //��Ǩ�ͺ��Ҥ�� str �Ѻ����
     * object �ͧ patient �繤�����ǡѹ������� // ����繤�����ǡѺ // �ӡ��
     * update ŧ���ҧ t_patient_xn �ͧ�Ţ xn � object �ͧ patient ��� active
     * �� 0 //�繤����ҧ // ���ҧ�Ţ xn ����
     *
     * ����¹��÷ӧҹ�ͧ XN �����Ǥ����Դ��� �������鹻��Ţ xn
     * �ͧ�����¨ж١ set �繤����ҧ�����������͡�úѹ�֡����
     * 㹡óַ��������Ѻ��ԡ�á�� xray ��觡�úѹ�֡�ҡ˹�� xray �� gen
     * �Ţ������� ��ǹ��úѹ�֡�ҡ˹�� ������ ���������
     *
     * �ҡ��ͧ�������¹�ŧ�Ţ�ͧ������¡������¹�Ţ ������Ш��ӷء�Ţ���
     * �ա������¹�ŧ �ҡ����¹�繤����ҧ��� ��úѹ�֡�ҡ˹�� xray �� gen
     * �Ţ������� ��úѹ�֡�ҡ˹�� ������ ���������
     */
    public boolean savePatientXn(String xn, boolean gen) {
        String year_now = this.theLookupControl.getTextCurrentDate();
        //��Ǩ�ͺ����Ţ Xn �繤����ҧ������� ��������
        if (theHO.thePatient == null) {
            return false;
        }
        year_now = year_now.substring(0, 4);
        boolean returnResult = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            //�������繤����ҧ����礫��
            boolean bresult = intCheckSamePatientXN(xn);
            if (!xn.isEmpty() && bresult) {
                theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.DUPLICATE.XN"), UpdateStatus.WARNING);
                return bresult;
            }
            //����繤����ҧ����������ա�� generate
            if (xn.isEmpty() && gen) {
                xn = theHosDB.theSequenceDataDB.updateSequence("xn", true);
                bresult = intCheckSamePatientXN(xn);
                if (!xn.isEmpty() && bresult) {
                    theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.DUPLICATE.XN"), UpdateStatus.WARNING);
                    return bresult;
                }
            }
            //��� xn ����繤����ҧ ��� �Ţ xn ����ѧ����� ���� �ҡ�����ǡ��ͧ����ӡѺ xn ���
            if (!xn.isEmpty() && (theHO.thePatientXN == null || !theHO.thePatientXN.patient_xray_number.equals(xn))) {
                theHosDB.thePatientXNDB.updateActiveByPtid(theHO.thePatient.getObjectId(), "0");
                PatientXN patientxn = new PatientXN();
                patientxn.patient_xn_active = Active.isEnable();
                patientxn.patient_xn_year = year_now;
                patientxn.patient_xray_number = xn;
                patientxn.t_patient_id = theHO.thePatient.getObjectId();
                theHosDB.thePatientXNDB.insert(patientxn);
                theHO.thePatientXN = (PatientXN) patientxn.clone();
            }
            //���������ӡ� set ���Ѻ������
            theHO.thePatient.xn = xn;
            theHosDB.thePatientDB.updateXN(theHO.thePatient);
            theConnectionInf.getConnection().commit();
            returnResult = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            hosControl.updateTransactionFail(HosControl.UNKONW, ResourceBundle.getBundleText("com.hosv3.control.ResultControl.SAVE.XN"));
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            try {
                if (!theConnectionInf.getConnection().getAutoCommit()) {
                    theConnectionInf.getConnection().commit();
                }
            } catch (SQLException ex) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
            }
            theConnectionInf.close();
        }
        if (returnResult) {
            hosControl.updateTransactionSuccess(HosControl.UNKONW, ResourceBundle.getBundleText("com.hosv3.control.ResultControl.SAVE.XN"));
        }
        return returnResult;
    }

    /**
     * ��㹡�õ�Ǩ�ͺ��� �Ţ xn ��������ҡ�ë�ӡѹ�������
     * ��ҫ�ӡѹ�е�ͧ������� true �������ӡѹ��������� false
     *
     * @param patient_xn �� String �ͧ �Ţ xn ����ͧ��ä���
     * @return �� bolean ��ҫ�ӡѹ�е�ͧ������� true
     * �������ӡѹ��������� false
     * @author padungrat(tong)
     * @date 21/04/2549,11:30
     */
    private boolean intCheckSamePatientXN(String patient_xn) throws Exception {
        boolean result = false;
        Vector vhn;
        vhn = this.theHosDB.thePatientDB.checkSamePatientXn(patient_xn, this.theHO.thePatient.getObjectId());
        this.theHO.vSameXN = vhn;
        //��ӡѹ
        if (vhn != null && vhn.size() > 0) {
            result = true;
        }
        return result;
    }

    /**
     * �ѹ�֡�����š������Ѻ��ԡ�âͧ������ �Ѵ�͡�����������´ uc ����
     * 29/11/47
     *
     * @param visitLabreferin
     */
    public void saveVisitPatientLabreferin(VisitLabreferin visitLabreferin) {
        if (visitLabreferin == null) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.MISS.INFORMATION"), UpdateStatus.WARNING);
            return;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            visitLabreferin.begin_visit_time = theLookupControl.intReadDateTime();
            theHosDB.theVisitLabreferinDB.insert(visitLabreferin);
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.UNKONW, ResourceBundle.getBundleText("com.hosv3.control.ResultControl.IMPORT.LAB"));
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            hosControl.updateTransactionFail(HosControl.UNKONW, ResourceBundle.getBundleText("com.hosv3.control.ResultControl.IMPORT.LAB"));
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
    }

    /**
     * ��Ǩ�ͺ��Ҽ�����������Ѻ��ԡ�����������ѧ
     * ���͹�������ª��㹡��ź�����ż������ա��˹�� �Ѵ�͡�����������´ uc
     * ���� 29/11/47
     *
     * @param patientLabreferinId
     * @return
     */
    public boolean checkPatientInTableVisit(String patientLabreferinId) {
        if (patientLabreferinId == null) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.MISS.INFORMATION"), UpdateStatus.WARNING);
            return false;
        }
        theConnectionInf.open();
        boolean result = true;
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector answer = theHosDB.theVisitLabreferinDB.queryPid(patientLabreferinId);
            if (answer == null || answer.isEmpty()) {
                result = false;
            } else {
                result = true;
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    /**
     * �ʴ���ª��ͼ����·����ѧ����Ѻ��ԡ�� �Ѵ�͡�����������´ uc ����
     * 29/11/47
     *
     * @return
     */
    public Vector listVisitLabreferin() {
        Vector result = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theVisitLabreferinDB.queryVisitInProcess();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    /**
     * ���Ң����ż����¨ҡ Primary Key �Ѵ�͡�����������´ uc ���� 29/11/47
     *
     * @param plriid
     * @return
     */
    public PatientLabreferin readPatientLabByPk(String plriid) {
        if (plriid == null) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.MISS.INFORMATION"), UpdateStatus.WARNING);
            return null;
        }
        PatientLabreferin p = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            p = theHosDB.thePatientLabreferinDB.selectByPK(plriid);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return p;
    }

    /**
     * ���Ң����š���Ѻ��ԡ�ä�������ش����ѧ��ʶҹ�����Ѻ��ԡ������
     * �Ѵ�͡�����������´ uc ���� 29/11/47
     *
     * @param plriid
     * @return
     */
    public VisitLabreferin readVisitInProcessByPatientId(String plriid) {
        if (plriid == null) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.MISS.INFORMATION"), UpdateStatus.WARNING);
            return null;
        }
        VisitLabreferin v = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            v = theHosDB.theVisitLabreferinDB.selectVisitByPId(plriid);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return v;
    }

    /**
     * ���Ң����š���Ѻ��ԡ�÷������ͧ������ �Ѵ�͡�����������´ uc ����
     * 29/11/47
     *
     * @param pid
     * @return
     */
    public Vector listVisitLabreferinByPId(String pid) {
        Vector result = new Vector();
        if (pid == null) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.MISS.INFORMATION"), UpdateStatus.WARNING);
            return result;
        }

        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theVisitLabreferinDB.queryVisitInProcessByPid(pid);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    /**
     * ������¡�� order �ҡ��ä�ԡ���͡���駡������Ѻ��ԡ�÷���ͧ��
     * �Ѵ�͡�����������´ uc ���� 29/11/47
     *
     * @param VId
     * @return
     */
    public Vector listOrderByVisitId(String VId) {
        Vector result = new Vector();
        if (VId == null) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.MISS.INFORMATION"), UpdateStatus.WARNING);
            return result;
        }

        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theOrderItemLabreferinDB.queryOrderByVid(VId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    /**
     * ź��¡�� order �Ѵ�͡�����������´ uc ���� 29/11/47
     *
     * @param orderItemLabreferin
     */
    public void deleteOrderItemLabReferIn(OrderItemLabreferin orderItemLabreferin) {
        if (orderItemLabreferin == null) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.MISS.INFORMATION"), UpdateStatus.WARNING);
            return;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theOrderItemLabreferinDB.delete(orderItemLabreferin);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
    }

    /**
     * �ѹ�֡��¡�� order �Ѵ�͡�����������´ uc ���� 30/11/47
     *
     * @param orderItemLabreferin
     */
    public void saveOrderItemLabReferin(Vector orderItemLabreferin) {
        if (orderItemLabreferin == null) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.MISS.INFORMATION"), UpdateStatus.WARNING);
            return;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (int i = 0; i < orderItemLabreferin.size(); i++) {
                OrderItemLabreferin olr = (OrderItemLabreferin) orderItemLabreferin.get(i);
                if (olr.getObjectId() == null) {
                    theHosDB.theOrderItemLabreferinDB.insert(olr);
                } else {
                    theHosDB.theOrderItemLabreferinDB.update(olr);
                }
            }
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
    }

    /**
     * �ʴ��� lab �����¡�� order �Ѵ�͡�����������´ uc ���� 30/11/47
     * �ѧ�������ҹ
     *
     * @param resultlab
     * @return
     */
    public OrderResultLabreferin readResultLabReferinByOrderID(OrderResultLabreferin resultlab) {
        if (resultlab == null) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.MISS.INFORMATION"), UpdateStatus.WARNING);
            return null;
        }
        OrderResultLabreferin rl = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            rl = theHosDB.theOrderResultLabreferinDB.selectByOrderItem(resultlab);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return rl;
    }

    /**
     * �ʴ��� lab �����¡�� order �Ѵ�͡�����������´ uc ���� 30/11/47
     *
     * @param itemID
     * @param visitID
     * @return
     */
    public OrderItemLabreferin readOrderItemByItemIdAndVisitId(String itemID, String visitID) {
        OrderItemLabreferin oilri = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            oilri = theHosDB.theOrderItemLabreferinDB.selectByItemIDAndVID(itemID, visitID);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return oilri;
    }

    public String updateOrderItemLabreferin(OrderItemLabreferin orderItemLabreferin) {
        if (orderItemLabreferin == null) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.MISS.INFORMATION"), UpdateStatus.WARNING);
            return "Error";
        }
        String theStatus;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theOrderItemLabreferinDB.update(orderItemLabreferin);
            theStatus = "Complete";
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_UPDATE);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            hosControl.updateTransactionFail(HosControl.TRANSACTION_UPDATE);
            theStatus = "Error";
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theStatus;
    }

    public void saveOrderResultLabrefrin(OrderResultLabreferin p) {
        if (p == null) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.MISS.INFORMATION"), UpdateStatus.WARNING);
            return;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (p.getObjectId() == null) {
                theHosDB.theOrderResultLabreferinDB.insert(p);
            } else {
                theHosDB.theOrderResultLabreferinDB.update(p);
            }
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
    }

    public void deleteOrderResultLabReferIn(String oilriId) {
        if (oilriId == null) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.MISS.INFORMATION"), UpdateStatus.WARNING);
            return;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theOrderResultLabreferinDB.deleteById(oilriId);
            theConnectionInf.getConnection().commit();
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE);
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            hosControl.updateTransactionFail(HosControl.TRANSACTION_DELETE);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
    }

    public boolean checkVisitInResult(String visitLabreferinId) {
        boolean result = true;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector answer = theHosDB.theOrderResultLabreferinDB.queryVid(visitLabreferinId);
            if (answer == null || answer.isEmpty()) {
                result = false;
            } else {
                result = true;
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    public Vector listResultByVId(String vId) {
        Vector result = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theOrderResultLabreferinDB.queryresultByVid(vId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    /**
     * @param v_oid
     * @return
     * @Author : henbe pongtorn
     * @date : 18/03/2549
     * @see : �鹼��ź�ٻẺ��������ա�ä������Ǣ��
     */
    public Vector listResultLabByOid(Vector v_oid) {
        Vector result = new Vector();
        if (v_oid == null || v_oid.isEmpty()) {
            return result;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (v_oid.size() > 0) {
                String sql = "select t_result_lab.* from t_result_lab "
                        + " left join t_order on t_order.t_order_id = t_result_lab.t_order_id "
                        + " where (";
                for (int i = 0; i < v_oid.size(); i++) {
                    OrderItem oi = (OrderItem) v_oid.get(i);
                    sql += " t_order.t_order_id = '" + oi.getObjectId() + "' or";
                }
                sql = sql.substring(0, sql.length() - 2) + ") ";
                sql += " and f_item_group_id = '2' "
                        + " order by "
                        + " order_date_time,order_common_name "
                        + " ,result_lab_index";
                ResultSet rs = theConnectionInf.eQuery(sql);
                result = theHosDB.theResultLabDB.eQuery(rs);
                if (!theHO.orderSecret.isEmpty()) {
                    for (int j = 0; j < result.size(); j++) {
                        ResultLab res = (ResultLab) result.get(j);
                        res.result = Secure.decode(res.result);
                    }
                }
            } else {
                for (int i = 0, size = v_oid.size(); i < size; i++) {
                    OrderItem oi = (OrderItem) v_oid.get(i);
                    Vector v = theHosDB.theResultLabDB.selectNewByOidActive(oi.getObjectId(), Active.isEnable());//�͹�׹�ѹ��¡�è��ա�úѹ�֡���ź�������͹���ǡóչ�����¤�������ҡ���ź�ѧ
                    //�պҧ��¡�÷���ѧ�����ѹ�֡�š��ʴ�����ѧ�բ������ź����ŧ���������
                    //��§ҹ�š��������繤����ҧ����
                    if (v.isEmpty()) {
                        return new Vector();
                    }
                    for (int j = 0; j < v.size(); j++) {
                        ResultLab rs = (ResultLab) v.get(j);
                        if (oi.secret.equals(Active.isEnable())) {
                            rs.result = Secure.decode(rs.result);
                        }
                        result.add(v.get(j));
                    }
                }
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    /**
     * @Author: tuk
     * @date: 07/08/2549
     * @see: ������Ѻ���Ҽ��Ż������ʴ��˹�Ҩ� Refer
     * @param: Vector �ͧ OrderItem
     * @return
     * @return: Vector �ͧ���ź
     * @param v_oid
     */
    public Vector listResultLabRefer(Vector v_oid) {
        Vector result = new Vector();
        Vector v;
        ResultLab rs;
        if (v_oid == null || v_oid.isEmpty()) {
            return result;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (int i = 0, size = v_oid.size(); i < size; i++) {
                OrderItem oi = (OrderItem) v_oid.get(i);
                //��¡�� Lab �����§ҹ��������ҹ��
                if (oi.status.equals(OrderStatus.REPORT)) {
                    v = theHosDB.theResultLabDB.selectNewByOidActive(oi.getObjectId(), Active.isEnable());
                    if (v != null) {
                        for (int j = 0; j < v.size(); j++) {
                            rs = (ResultLab) v.get(j);
                            if (oi.secret.equals(Active.isEnable())) {
                                rs.result = Secure.decode(rs.result);
                            }
                            result.add(v.get(j));
                        }
                    }
                }
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    /*
     * @Author : henbe pongtorn @date : 18/03/2549 @see :
     * �鹼��ź�ٻẺ��������ա�ä������Ǣ�� ���Ƿ���ش �ѧ��������Ϳѧ�ѹ�
     * db
     */
    public Vector listResultLabByVid(String vid) {
        Vector result = new Vector();
        if (vid == null || vid.isEmpty()) {
            return result;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theResultLabDB.selectNewByVid(vid);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    public String updateVisitLabreferin(VisitLabreferin visitLabreferin) {
        String theStatus;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theVisitLabreferinDB.update(visitLabreferin);
            theStatus = "Complete";
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            theStatus = "Error";
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return theStatus;
    }

    public boolean checkVisitInOrder(String visitLabreferinId) {
        boolean result = true;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector answer = theHosDB.theOrderItemLabreferinDB.queryVid(visitLabreferinId);
            if (answer == null || answer.isEmpty()) {
                result = false;
            } else {
                result = true;
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    public OrderItemLabreferin readOrderItemByOrderItemIdAndVisitId(String orderItemID) {
        OrderItemLabreferin oilri = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            oilri = theHosDB.theOrderItemLabreferinDB.selectByPK(orderItemID);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return oilri;
    }

    public Vector listLabSetByItemId(String itemId) {
        Vector result = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theLabSetDB.selectByItemId(itemId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    public LabGroup readLabGroupByPk(String labGroupId) {
        LabGroup p = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            p = theHosDB.theLabGroupDB.selectByPK(labGroupId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return p;
    }

    public Vector listLabSetByLabGroupId(String lgId) {
        Vector result = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theLabSetDB.selectByLabGroupID(lgId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    public boolean checkOrderItemIdInResult(String orderItemLabreferinId) {
        boolean result = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector answer = theHosDB.theOrderResultLabreferinDB.queryOrderItemId(orderItemLabreferinId);
            if (answer == null || answer.isEmpty()) {
                result = false;
            } else {
                result = true;
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    public OrderItemLabreferin readOrderItemByPk(String pk) {
        OrderItemLabreferin p = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            p = theHosDB.theOrderItemLabreferinDB.selectByPK(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return p;
    }

    public LabGroup readLabGroupByItemId(String itemId) {
        LabGroup p = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            p = theHosDB.theLabGroupDB.selectByItemID(itemId);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return p;
    }

    /**
     * @param voi
     * @param vresultlab
     * @param theUS
     * @return
     * @Author : henbe pongtorn
     * @date : 18/03/2549 �ѹ�֡���Ż�繿ѧ�ѹ��ҧ���������¡��ҡ�ء˹�Ҩ�
     * ��ѹ�֡�ҡ˹�Ҩ������ �зӡ���觼ŷ�������������ǹ�ͧ control
     * ������ͧ���������ҧ
     */
    public boolean saveResultLab(Vector voi, Vector vresultlab, UpdateStatus theUS) {
        int[] row = new int[voi.size()];
        for (int i = 0; i < row.length; i++) {
            row[i] = i;
        }
        return saveResultLab(voi, vresultlab, theUS, row);
    }

    public boolean saveResultLab(Vector voi, Vector vresultlab, UpdateStatus theUS, int[] rows) {
        if (theHO.theVisit == null) {
            theUS.setStatus(ResourceBundle.getBundleGlobal("VALIDATE.VISITED"), UpdateStatus.WARNING);
            return false;
        }
        if (voi == null || voi.isEmpty()) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.EMPLTY.LAB.ITEMS"), UpdateStatus.WARNING);
            return false;
        }
        if (vresultlab == null || vresultlab.isEmpty()) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.NO.LAB.ITEMS.TO.SAVE"), UpdateStatus.WARNING);
            return false;
        }
        Vector vvresultlab = getVVResultLab(voi, vresultlab);
        boolean returnResult = false;
        theConnectionInf.open();
        int save = 0;
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHO.date_time = theLookupControl.intReadDateTime();
            for (int i = 0; i < rows.length; i++) {
                OrderItem oi = (OrderItem) voi.get(rows[i]);
                if (!oi.status.equals(OrderStatus.EXECUTE) && !oi.status.equals(OrderStatus.REMAIN)) {
                    continue;
                }
                Vector vrl = (Vector) vvresultlab.get(rows[i]);
                boolean is_ok = intCheckResultLab(oi, vrl, theUS, true);
                if (!is_ok) {
                    continue;
                }
                boolean ret = intSaveResultLab(oi, vrl, theUS);
                if (!ret) {
                    return false;
                }
                save++;
            }
            if (save == 0) {
                returnResult = false;
            } else {
                refreshLabStatus(theHO.theVisit.getObjectId());
                returnResult = true;
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            hosControl.updateTransactionFail(HosControl.UNKONW, ResourceBundle.getBundleText("com.hosv3.control.ResultControl.SAVE.LAB"));
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (returnResult) {
            theHS.theResultSubject.notifySaveLabResult(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.SAVE.LAB") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.UNKONW, ResourceBundle.getBundleText("com.hosv3.control.ResultControl.SAVE.LAB"));
        } else {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.UNSAVE.LAB"), UpdateStatus.WARNING);
        }
        return returnResult;
    }

    protected String refreshLabStatus(String visit_id) throws Exception {
        Vector patient_order = theHosDB.theOrderItemDB.selectByVisitId(visit_id);
        String status = (String) OrderControl.checkLabAndXrayStatus(patient_order).get("lab");
        if (theHO.theListTransfer != null) {
            theHO.theListTransfer.labstatus = status;
            theHosDB.theQueueTransferDB.update(theHO.theListTransfer);
        }
        theHO.theVisit.queue_lab_status = status;
        theHosDB.theVisitDB.updateLabAndXrayStatus(theHO.theVisit);
        if (status.equals(QueueLabStatus.REPORT)) {
            theHosDB.theQueueLabDB.deleteByVisitID(theHO.theVisit.getObjectId());
        } else if (status.equals(QueueLabStatus.REMAIN)) {
            theHosDB.theQueueLabDB.deleteByVisitID(theHO.theVisit.getObjectId(), "0");
        }
        return status;
    }

    /**
     * ��ͧ�繼��ź���������зӡ�úѹ�֡�ҡ��駤�������Ф�������������������зӡ��ź���������
     * ��������
     *
     * @param oi
     * @param resultlab
     * @param theUS
     * @param allow_empty
     * @return
     * @throws Exception
     */
    protected boolean intCheckResultLab(OrderItem oi, Vector resultlab, UpdateStatus theUS, boolean allow_empty)
            throws Exception {
        StringBuffer warning = new StringBuffer();
        //�ѹ�֡���źŧ���ҹ�������������͹䢴ѧ���仹��
        for (int j = 0; j < resultlab.size(); j++) {
            ResultLab rl = (ResultLab) resultlab.get(j);
            //��Ǩ�ͺ���͹���Ҽ��ź�ж١��ͧ������͹���������ҡ���ç��������ѹ�֡���ѹ�Ҵ
            if (!isLabResultMath(rl, warning, allow_empty)) {
                theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.SAVE.LAB") + " " + warning, UpdateStatus.WARNING);
                return false;
            }
        }
        return true;
    }

    protected boolean intSaveResultLab(OrderItem oi, Vector resultlab, UpdateStatus theUS)
            throws Exception {
        theHosDB.theResultLabDB.deleteByOid(oi.getObjectId());
        boolean is_report = true;
        Item item = theHosDB.theItemDB.selectByPK(oi.item_code);
        for (int j = 0; j < resultlab.size(); j++) {
            ResultLab rl = (ResultLab) resultlab.get(j);
            //����͵ç������͹䢡�кѹ�֡��
            //����Ѻ��¡�÷���繤����ҧ�����ҧ�� ��ͧ�ѹ�֡���ú�ء��¡�è֧����§ҹ����
            rl.reported_time = theHO.date_time;
            rl.order_item_id = oi.getObjectId();
            rl.reporter = theHO.theEmployee.getObjectId();
            rl.active = Active.isEnable();
            if (!rl.result.isEmpty()) {
                rl.result_complete = Active.isEnable();
            } else {
                is_report = false;
            }
            //amp:22/02/2549 ���͵�Ǩ�ͺ������Ż���Դ�������� ������Ż���Դ����������������
            if ("1".equals(item.secret)) {
                rl.result = Secure.encode(rl.result);
            }
            theHosDB.theResultLabDB.insert(rl);
        }
//        //��� order ����� verify ������������¹�� execute ���
//        if (oi.status.equals(OrderStatus.VERTIFY)) {
//            // ��ͧ���� gen ln ��Ҩ���ҹ��� 3.9.32 b 01 �������ҹ��ǹ�������
//            oi.executer = theHO.theEmployee.getObjectId();
//            oi.executed_time = theHO.date_time;
//            oi.status = OrderStatus.EXECUTE;
//            theHosDB.theOrderItemDB.update(oi);
//        }
        //��� order �����§ҹ������ ������������¹�� ��§ҹ�� ���
        if (is_report && !resultlab.isEmpty()
                && !theHO.theEmployee.authentication_id.equals(Authentication.LAB)) {
            oi.reporter = theHO.theEmployee.getObjectId();
            oi.reported_time = theHO.date_time;
            oi.status = OrderStatus.REPORT;
            theHosDB.theOrderItemDB.update(oi);
        }
        return true;
    }

    /**
     * �������ŧ�����ż��ź������ѧ�ѹ��÷ӧҹ�ҵðҹ
     *
     * @param vor
     * @param vres
     * @return
     */
    public static Vector getVVResultLab(Vector vor, Vector vres) {
        Vector vv = new Vector();
        for (int j = 0; j < vor.size(); j++) {
            OrderItem oi = (OrderItem) vor.get(j);
            Vector v = new Vector();
            for (int i = 0; vres != null && i < vres.size(); i++) {
                ResultLab rl = (ResultLab) vres.get(i);
                if (rl.order_item_id.equals(oi.getObjectId())) {
                    v.add(rl);
                }
            }
            vv.add(v);
        }
        return vv;
    }

    /**
     * ��Ǩ�ͺ���ź��ҵç����ͺࢵ����˹��������
     * �ҡ�ç��ӡ�úѹ�֡������ҡ������ç������ѹ�֡����
     * �ѹ�֡�繤����ҧ᷹ save ���¶֧ ��Ǩ�ͺ�����������ͺѹ�֡�������
     * encode ���¶֧ ���Ż���������������� �ҡ���ͺѹ�֡���������
     * ����礤����ҧ �ҡ�����觼� ��������礤����ҧ����
     *
     * @param rsl
     * @param warning
     * @param allow_empty
     * @return
     * @throws Exception
     */
    protected boolean isLabResultMath(ResultLab rsl, StringBuffer warning, boolean allow_empty) throws Exception {
        String data = rsl.result;
        String lab_result_type = rsl.result_type_id;
        //����繤����ҧ������ͧ����Ҷ١���ͼԴ
        if (rsl.result.isEmpty()) {
            if (!allow_empty) {
                warning.append(" ").append(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.RESULT.LAB")).append(" ").append(rsl.name).append(" ").append(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.RESULT.LAB.NOT.EMPTY"));
                return false;
            } else {
                return true;
            }
        }
        if (lab_result_type.equals(LabResultType.INTEGER)) {
            try {
                Integer.parseInt(data);
            } catch (Exception ex) {
                warning.append(" ").append(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.RESULT.LAB")).append(" ").append(rsl.name).append(" ").append(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.RESULT.LAB.MUST.IS.INTEGER"));
                return false;
            }
        } else if (lab_result_type.equals(LabResultType.FLOAT)) {
            try {
                Double.parseDouble(data);
            } catch (Exception ex) {
                warning.append(" ").append(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.RESULT.LAB")).append(" ").append(rsl.name).append(" ").append(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.RESULT.LAB.MUST.IS.DECIMAL"));
                return false;
            }
        } else if (lab_result_type.equals(LabResultType.TEXT_ONE)) {
            boolean ret = (data.indexOf('\n') == -1);
            if (ret == false) {
                warning.append(" ").append(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.RESULT.LAB")).append(" ").append(rsl.name).append(" ").append(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.RESULT.LAB.MUST.IS.ONE.LINE.TEXT"));
            }
            return ret;
        } else if (lab_result_type.equals(LabResultType.TEXT_MANY)) {
        } else if (lab_result_type.equals(LabResultType.LIST)) {
            boolean check = false;
            ////henbe trick////////////////////////////////////////////////////////////
            if (data.equals("-")) {
                return true;
            }
            Vector vd = theHosDB.theLabResultDetailDB.selectCByResulteCode(rsl.result_group_id);
            loop:
            for (int j = 0; j < vd.size(); j++) {
                ComboFix cf = (ComboFix) vd.get(j);
                if (cf.getName().trim().equals(String.valueOf(data.trim()))) {
                    check = true;
                    break loop;
                } else {
                    check = false;
                }
            }
            //�ջѭ����Ż���Դ
            if (!check) {
                /////henbe trick///////////////////////////////////////////////////////////
                warning.append(" ").append(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.RESULT.LAB")).append(" ").append(rsl.name).append(" ").append(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.RESULT.LAB.MUST.SELECTED.RESULT"));
                return false;
            }
        }
        return true;
    }

    ////////////////////////////////////////////////////////////////////////////
    /**
     * @param orderitem
     * @param select
     * @Author : amp henbe
     * @date : 08/03/2549 21/03/2549
     * @see : ��ҧ���ź ������ѹź����ͧ��¶���������¡�÷���ͧ��§ҹ�����ǹ�
     */
    public void saveRemainResultLab(Vector orderitem, int[] select) {
        if (select == null || select.length == 0) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.VALIDATE.SELECTED.LAB.ITEMS"), UpdateStatus.WARNING);
            return;
        }
        for (int i = 0; i < select.length; i++) {
            OrderItem oi = (OrderItem) orderitem.get(select[i]);
            if (oi.status.equals(OrderStatus.REMAIN)) {
                theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.VALIDATE.STATUS.REMAINED"), UpdateStatus.WARNING);
                return;
            }
            if (oi.status.equals(OrderStatus.REPORT)) {
                theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.VALIDATE.STATUS.REPORTED"), UpdateStatus.WARNING);
                return;
            }
            if (!oi.status.equals(OrderStatus.EXECUTE)) {
                theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.VALIDATE.STATUS.EXECUTED"), UpdateStatus.WARNING);
                return;
            }
        }
        String orderId = "";
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String date_time = theLookupControl.intReadDateTime();
            /*
             * ૵ʶҹ���¡������繤�ҧ��
             */
            int ordersize = select.length;
            for (int i = 0; i < ordersize; i++) {
                OrderItem oi = (OrderItem) orderitem.get(select[i]);
                oi.status = OrderStatus.REMAIN;
                theHosDB.theOrderItemDB.update(oi);
                //amp:08/03/2549 ������Ż���Դ�� record ����
                if ("1".equals(oi.secret)) {
                    orderId = oi.getObjectId();
                }
            }

            //amp:08/03/2549 code ����
            //�鹤�Ǥ�ҧ�����������������Ҷ���ա�������ҧ���Ƕ������ա����ҧ����
            QueueLab2 ql = theHosDB.theQueueLabDB.select2ByVisitIDAndOrderId(
                    theHO.theVisit.getObjectId(), orderId);
            QueueLab2 ql_remain = theHosDB.theQueueLabDB.selectByVidRmnOid(
                    theHO.theVisit.getObjectId(), Active.isEnable(), orderId);
            if (ql_remain == null) {
                QueueLab2 ql_new = new QueueLab2();
                ql_new.visit_id = theHO.theVisit.getObjectId();
                ql_new.patient_id = theHO.theVisit.patient_id;
                ql_new.remain = Active.isEnable();
                ql_new.assign_time = date_time;
                ql_new.last_service = theHO.theServicePoint.getObjectId();
                //�ó��ź���Դ�����Ң����Ũҡ������
                if (ql != null) {
                    ql_new.number_order = ql.number_order;
                    ql_new.order_id = ql.order_id;
                    ql_new.secret_code = ql.secret_code;
                } //�ó��ź����������Ң����Ũҡ��äӹǹ
                else {
                    ql_new.number_order = String.valueOf(ordersize);
                    ql_new.order_id = "";
                    ql_new.secret_code = "";
                }
                theHosDB.theQueueLabDB.insert(ql_new);
                theHO.labQueueRemain = Active.isEnable();
            }
            /////////////////////////////////////////////////////
            //QueueLabStatus/////////////////////////////////////////////
//            String status =
            refreshLabStatus(theHO.theVisit.getObjectId());
            ////////////////////////////////////////////////////////////////////////////////////
            //��Ǩ�ͺ��Ҷ����¡�� order �������������¡�÷�� �׹�ѹ���ʹ��Թ�������
            if (HosObject.countOrderVerifyExe(orderitem) == 0) {
                theHosDB.theQueueLabDB.deleteByOrderId(theHO.theVisit.getObjectId(), "0", orderId);
                theVisitControl.intUnlockVisit(theHO.theVisit);
                theHO.clearFamily();
            }
            theConnectionInf.getConnection().commit();
            ////////////////////////////////////////////////////////////////////////////////////
            theHS.theResultSubject.notifySaveRemainLabResult(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.SAVE.REMAIN.LAB.RESULT") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.UNKONW, ResourceBundle.getBundleText("com.hosv3.control.ResultControl.SAVE.REMAIN.LAB.RESULT"));
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.UNKONW, ResourceBundle.getBundleText("com.hosv3.control.ResultControl.SAVE.REMAIN.LAB.RESULT"));
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
    }

    //�ӡ���觼��Ż
    public void sendDataResultLab(Vector voi, Vector vresultlab, int[] select) {
        //��Ǩ�ͺ���Ż��������ҵç������͹��������
        if (theHO.theVisit == null) {
            theUS.setStatus(ResourceBundle.getBundleGlobal("VALIDATE.VISITED"), UpdateStatus.WARNING);
            return;
        }
        if (select.length == 0) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.NO.SELECT.LAB.TO.SEND"), UpdateStatus.WARNING);
            return;
        }
        Vector vvresultlab = getVVResultLab(voi, vresultlab);
        boolean isComplete = false;
        String notify = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHO.date_time = theLookupControl.intReadDateTime();
            int count_exec = 0;
            String order_status = "";
            for (int i = 0; i < select.length; i++) {
                OrderItem oi = (OrderItem) voi.get(select[i]);
                if (oi.status.equals(OrderStatus.REPORT)
                        || oi.status.equals(OrderStatus.NOT_VERTIFY)
                        || oi.status.equals(OrderStatus.DIS_CONTINUE)
                        || oi.status.equals(OrderStatus.DISPENSE)) {
                    theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.VALIDATE.STATUS.EXECUTED"), UpdateStatus.WARNING);
                    continue;
                }
                Vector vrl = (Vector) vvresultlab.get(select[i]);
                if (!intCheckResultLab(oi, vrl, theUS, false)) {
                    continue;
                }

                count_exec++;
                oi.reporter = theHO.theEmployee.getObjectId();
                oi.reported_time = theHO.date_time;
                order_status = oi.status;
                oi.status = OrderStatus.REPORT;
                theHosDB.theOrderItemDB.update(oi);
                theOrderControl.intSaveQueueOfOrder(oi, false, theHO.date_time);//�觼��Ż
            }
            if (count_exec == 0) {
//                theUS.setStatus("�������¡���Ż����ͧ��è���",UpdateStatus.WARNING);
                return;
            }
            //////////////////////////////////////////////////////////////////////////
            //��ҹ�����Ţͧ���Transfer�ͧ������ �ҡ���ź���Դ���ź���������
            OrderItem ori = (OrderItem) voi.get(0);
            if (ori.secret.equals("1")) {
                if (!order_status.equals(OrderStatus.REMAIN)) {
                    theHosDB.theQueueLabDB.deleteByOrderId(theHO.theVisit.getObjectId(), "0", ori.getObjectId());
                } else {
                    theHosDB.theQueueLabDB.deleteByOrderId(theHO.theVisit.getObjectId(), Active.isEnable(), ori.getObjectId());
                }
            }
            //QueueLabStatus
            String status = refreshLabStatus(theHO.theVisit.getObjectId());
            if (status.equals(QueueLabStatus.REPORT)
                    || status.equals(QueueLabStatus.REMAIN)
                    || ori.secret.equals(Active.isEnable())) {
                theVisitControl.intUnlockVisit(theHO.theVisit);
                theHO.clearFamily();
                notify = ResourceBundle.getBundleText("com.hosv3.control.ResultControl.SEND.LAB") + " "
                        + ResourceBundle.getBundleGlobal("TEXT.SUCCESS");

            } else {
                notify = ResourceBundle.getBundleText("com.hosv3.control.ResultControl.SEND.SOME.LAB") + " "
                        + ResourceBundle.getBundleGlobal("TEXT.SUCCESS");
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, ResourceBundle.getBundleText("com.hosv3.control.ResultControl.SEND.SOME.LAB"));
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theResultSubject.notifySendResultLab(notify, UpdateStatus.COMPLETE);
            theUS.setStatus(notify, UpdateStatus.COMPLETE);
        }
    }

    /**
     * Return Vector �ͧ Lab_Result_Item
     *
     * @param resultlab
     * @return
     */
    public ResultLab readResultLabByOrderID(ResultLab resultlab) {
        ResultLab rl = new ResultLab();
        resultlab.name = Gutil.CheckReservedWords(resultlab.name);
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            rl = theHosDB.theResultLabDB.selectByOrderItem(resultlab);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return rl;
    }
    //////////////////////////////////////////////////////////////////////////

    public void deleteQueueLabByVisitID(String visit_id) {
        theConnectionInf.open();
        int i = 0;
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            i = theHosDB.theQueueLabDB.deleteByOrderId(visit_id, theHO.labQueueRemain, theHO.orderSecret);
            theVisitControl.intUnlockVisit(theHO.theVisit);
            theHO.clearFamily();
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.DELETE.QUEUE.LAB") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (i == 1) {
            theHS.theResultSubject.notifyDeleteQueueLab(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.DELETE.QUEUE.LAB") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        }
    }
    //////////////////////////////////////////////////////////////////////////

    public void deleteQueueXrayByVisitID(String visit_id) {
        boolean retRes = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            int i = theHosDB.theQueueXrayDB.deleteByVisitID(visit_id);
            if (theHO.isLockingVisit()) {
                theVisitControl.intUnlockVisit(theHO.theVisit);
                theHO.clearFamily();
            }
            theConnectionInf.getConnection().commit();
            retRes = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.DELETE.QUEUE.XRAY") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (retRes) {
            theHS.theResultSubject.notifyXrayReportComplete(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.DELETE.QUEUE.XRAY") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        }
    }
//
    ////////////////////////////////////////////////////////////////////////

    public void deleteQueueLab(String visit_id, String remain) {
        boolean retRes = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theQueueLabDB.deleteByVisitID(visit_id, remain);
            theConnectionInf.getConnection().commit();
            retRes = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            theUS.setStatus(("�ѹ�֡��ä�ҧ���Ż") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.ERROR);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (retRes) {
            theHS.theResultSubject.notifyDeleteQueueLab(("�ѹ�֡��ä�ҧ���Ż") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        }
    }

    public void deleteOrderItemLab(Vector vOrderlab, int[] row) {
        if (vOrderlab == null) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.VALIDATE.SELECTED.LAB.ITEMS"), UpdateStatus.WARNING);
            return;
        }
        boolean retRes = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (int i = row.length - 1; i >= 0; i--) {
                OrderItem theOrderItemLab = (OrderItem) vOrderlab.get(row[i]);
                vOrderlab.remove(row[i]);
                theHosDB.theOrderItemDB.delete(theOrderItemLab);
            }
            theHO.vOrderItem = theHosDB.theOrderItemDB.selectByVidTypeCancel(theHO.theVisit.getObjectId(), "", false);
            theConnectionInf.getConnection().commit();
            retRes = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.DELETE.LAB.ITEM") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.FAIL"), UpdateStatus.WARNING);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (retRes) {
            theHS.theResultSubject.notifyDeleteLabOrder(ResourceBundle.getBundleText("com.hosv3.control.ResultControl.DELETE.LAB.ITEM") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        }
    }

    public void deleteOrderItemLabByLab(OrderItem orderItem, Visit visit) {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theOrderItemDB.delete(orderItem);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
    }

    public Vector listResultLabByVisit(Visit visit) {
        if (visit == null) {
            return new Vector();
        }
        String active = "<>'1'";
        if (theHO.orderSecret != null && !theHO.orderSecret.isEmpty()) {
            active = "='1'";
        }
        Vector result = new Vector();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String sql = "select t_result_lab.* from t_result_lab "
                    + "inner join t_order on t_order.t_order_id = t_result_lab.t_order_id "
                    + "where t_order.t_visit_id = '" + visit.getObjectId() + "' "
                    + "and f_item_group_id = '2'"
                    + "and (f_order_status_id = '1' or f_order_status_id = '2' "
                    + " or f_order_status_id = '4' or f_order_status_id = '6') "
                    + "and order_secret " + active;
            if (theHO.orderSecret != null && !theHO.orderSecret.isEmpty()) {
                sql += " and  t_order.t_order_id = '" + theHO.orderSecret + "'";
            }
            sql += " Order by order_date_time,order_common_name,result_lab_index";
            result = theHosDB.theResultLabDB.eQuery(sql);
            if (!theHO.orderSecret.isEmpty()) {
                for (int j = 0; j < result.size(); j++) {
                    ResultLab res = (ResultLab) result.get(j);
                    res.result = Secure.decode(res.result);
                }
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }
}

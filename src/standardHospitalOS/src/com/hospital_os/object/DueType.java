/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
public class DueType extends Persistent implements CommonInf {

    public String due_code;
    public String due_name;
    public String active = "1";
    public String user_record_id;
    public String record_date_time;
    public String user_update_id;
    public String update_date_time;

    @Override
    public String getCode() {
        return getObjectId();
    }

    @Override
    public String getName() {
        return due_name;
    }

    @Override
    public String toString() {
        return due_name;
    }
}

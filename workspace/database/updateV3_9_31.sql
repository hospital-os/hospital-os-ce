-- add column passport_no
ALTER TABLE t_health_family ADD COLUMN passport_no character varying(8) default ''; 
-- vital sign
ALTER TABLE t_visit_vital_sign ADD COLUMN visit_vital_sign_chest character varying(255) default ''; 
ALTER TABLE t_visit_vital_sign ADD COLUMN visit_vital_sign_chest_inch character varying(255) default ''; 

-- risk factor
ALTER TABLE t_patient_risk_factor ADD COLUMN risk_alcoho_about_week  character varying(4) default ''; 
ALTER TABLE t_patient_risk_factor ADD COLUMN risk_cigarette_about_week character varying(4) default ''; 
ALTER TABLE t_patient_risk_factor ADD COLUMN risk_exercise_result character varying(1) default '';

-- patient
ALTER TABLE t_patient ADD COLUMN patient_patient_email character varying(255) default ''; 
ALTER TABLE t_patient ADD COLUMN patient_contact_email character varying(255) default ''; 

-- lab specimen
CREATE TABLE b_specimen (
    b_specimen_id                   character varying(255)   NOT NULL,
    code                            character varying(255)   NOT NULL,
    description                     character varying(255)   NOT NULL,
    active                          character varying(1)     NOT NULL,
    user_record character varying(255),
    record_datetime character varying(19),
    user_modify character varying(255),
    modify_datetime character varying(19),
CONSTRAINT b_specimen_pkey PRIMARY KEY (b_specimen_id)
);
CREATE UNIQUE INDEX b_specimen_index1
   ON b_specimen (code ASC NULLS LAST);

ALTER TABLE b_item ADD COLUMN b_specimen_id character varying(255) default ''; 
CREATE INDEX b_item_specimen_id
   ON b_item (b_specimen_id ASC NULLS LAST);

-- lab normal rage
-- 1 = by gender, 2 = by interval age, 3 = text, 4 = sql
ALTER TABLE b_item_lab_result ADD COLUMN normal_range_type character varying(1) default '1';

update b_item_lab_result set normal_range_type = '4' where item_lab_result_default != '';

-- 0= use flag from LIS, 1 = use min-max by gender, 2 =  use min-max by interval age, 3 = use default_values by text, 4 =  use min-max by sql
ALTER TABLE t_result_lab ADD COLUMN normal_range_type character varying(1) NOT NULL DEFAULT 0;
-- array of default data from normal_range_type = 3
ALTER TABLE t_result_lab ADD COLUMN default_values character varying[];
--update t_result_lab set normal_range_type = '0' where result_lab_normal_flag != '';

CREATE TABLE b_item_lab_result_gender (
    b_item_lab_result_gender_id character varying(255)   NOT NULL,
    b_item_lab_result_id        character varying(255)   NOT NULL,
    male_result_min             character varying(255)   NULL,
    male_result_max             character varying(255)   NULL,
    female_result_min           character varying(255)   NULL,
    female_result_max           character varying(255)   NULL,
CONSTRAINT b_item_lab_result_gender_pkey PRIMARY KEY (b_item_lab_result_gender_id)
);
CREATE UNIQUE INDEX b_item_lab_result_gender_index1
   ON b_item_lab_result_gender (b_item_lab_result_id ASC NULLS LAST);

CREATE TABLE b_item_lab_result_age (
    b_item_lab_result_age_id    character varying(255)   NOT NULL,
    b_item_lab_result_id        character varying(255)   NOT NULL,
    age_min                     character varying(255)   NULL,
    age_max                     character varying(255)   NULL,
    male_min                    character varying(255)   NULL,
    male_max                    character varying(255)   NULL,
    female_min                  character varying(255)   NULL,
    female_max                  character varying(255)   NULL,
CONSTRAINT b_item_lab_result_age_pkey PRIMARY KEY (b_item_lab_result_age_id)
);
CREATE INDEX b_item_lab_result_age_index1
   ON b_item_lab_result_age (b_item_lab_result_id ASC NULLS LAST);


CREATE TABLE b_item_lab_result_text (
    b_item_lab_result_text_id   character varying(255)   NOT NULL,
    b_item_lab_result_id        character varying(255)   NOT NULL,
    result_text                 character varying(255)   NOT NULL,
CONSTRAINT b_item_lab_result_text_pkey PRIMARY KEY (b_item_lab_result_text_id)
);
CREATE INDEX b_item_lab_result_text_index1
   ON b_item_lab_result_text (b_item_lab_result_id ASC NULLS LAST);

--
CREATE TABLE f_person_affiliated (
    f_person_affiliated_id         character varying(2)   NOT NULL,
    description                 character varying(255)   NOT NULL,
CONSTRAINT f_person_affiliated_pkey PRIMARY KEY (f_person_affiliated_id)
);

INSERT INTO f_person_affiliated VALUES ('0','ไม่ระบุ');
INSERT INTO f_person_affiliated VALUES ('1','บก.ทอ.');
INSERT INTO f_person_affiliated VALUES ('2','สลก.ทอ.');
INSERT INTO f_person_affiliated VALUES ('3','สบ.ทอ.');
INSERT INTO f_person_affiliated VALUES ('4','กพ.ทอ.');
INSERT INTO f_person_affiliated VALUES ('5','ขว.ทอ.');
INSERT INTO f_person_affiliated VALUES ('6','ยก.ทอ.');
INSERT INTO f_person_affiliated VALUES ('7','กบ.ทอ.');
INSERT INTO f_person_affiliated VALUES ('8','สปช.ทอ.');
INSERT INTO f_person_affiliated VALUES ('9','จร.ทอ.');
INSERT INTO f_person_affiliated VALUES ('10','ทสส.ทอ.');
INSERT INTO f_person_affiliated VALUES ('11','สนภ.ทอ.');
INSERT INTO f_person_affiliated VALUES ('12','ยศ.ทอ.');
INSERT INTO f_person_affiliated VALUES ('13','รร.นอ.');
INSERT INTO f_person_affiliated VALUES ('14','กง.ทอ.');
INSERT INTO f_person_affiliated VALUES ('15','สก.ทอ.');
INSERT INTO f_person_affiliated VALUES ('16','ศวอ.ทอ.');
INSERT INTO f_person_affiliated VALUES ('17','สตน.ทอ.');
INSERT INTO f_person_affiliated VALUES ('18','สน.ผบ.ดม.');
INSERT INTO f_person_affiliated VALUES ('19','สธน.ทอ.');
INSERT INTO f_person_affiliated VALUES ('20','คปอ.');
INSERT INTO f_person_affiliated VALUES ('21','อย.');
INSERT INTO f_person_affiliated VALUES ('22','บน.๖');
INSERT INTO f_person_affiliated VALUES ('23','ชอ.');
INSERT INTO f_person_affiliated VALUES ('24','สอ.ทอ.');
INSERT INTO f_person_affiliated VALUES ('25','สพ.ทอ.');
INSERT INTO f_person_affiliated VALUES ('26','พธ.ทอ.');
INSERT INTO f_person_affiliated VALUES ('27','ชย.ทอ.');
INSERT INTO f_person_affiliated VALUES ('28','ขส.ทอ.');
INSERT INTO f_person_affiliated VALUES ('29','พอ.');
INSERT INTO f_person_affiliated VALUES ('30','สวบ.ทอ.');
INSERT INTO f_person_affiliated VALUES ('31','กร.ทอ.');


CREATE TABLE f_person_rank (
    f_person_rank_id           character varying(2)   NOT NULL,
    description                 character varying(255)   NOT NULL,
CONSTRAINT f_person_rank_pkey PRIMARY KEY (f_person_rank_id)
);
INSERT INTO f_person_rank VALUES ('0','ไม่ระบุ');
INSERT INTO f_person_rank VALUES ('1','น.สัญญาบัตร');
INSERT INTO f_person_rank VALUES ('2','น.ประทวน');
INSERT INTO f_person_rank VALUES ('3','นร.ทหาร');
INSERT INTO f_person_rank VALUES ('4','ทหารกองประจำการ');
INSERT INTO f_person_rank VALUES ('5','ลูกจ้างประจำ และพนักงานราชการ');
INSERT INTO f_person_rank VALUES ('6','ข  ครอบครัว ทอ. / ข้าราชการบำนาญ ทอ.');
INSERT INTO f_person_rank VALUES ('7','ค  พลเรือน / ข้าราชการเหล่าทัพอื่น');

ALTER TABLE t_health_family ADD COLUMN f_person_affiliated_id character varying(2) default '0'; 
ALTER TABLE t_health_family ADD COLUMN f_person_rank_id character varying(1)  default '0'; 
ALTER TABLE t_person ADD COLUMN f_person_affiliated_id character varying(2) default '0'; 
ALTER TABLE t_person ADD COLUMN f_person_rank_id character varying(1)  default '0'; 

--Migrate data t_health_cancer
insert into t_health_cancer (t_health_cancer_id,t_person_id,t_visit_id,health_cancer_survey_date,health_cancer_breast_exam,health_cancer_cervix_exam,health_cancer_pregnant_exam,health_cancer_breast_exam_note,health_cancer_cervix_exam_note,health_cancer_note,health_cancer_first_check,active,user_record_id,record_date_time,user_modify_id,modify_date_time,user_cancel_id,cancel_date_time)
(select
        '808'||substr(t_health_family_planing.t_health_family_planing_id,4) as t_health_cancer_id --808
        ,t_health_family_planing.t_health_family_id as t_person_id
        ,t_health_family_planing.t_visit_id as t_visit_id
        ,case when t_health_family_planing.health_family_planing_date = ''
                    then substr(t_health_family_planing.record_date_time,1,10)
                    else t_health_family_planing.health_family_planing_date 
                    end as health_cancer_survey_date
        ,case when t_health_family_planing.health_family_planing_breast_exam = '' 
                    then '0' 
                    else t_health_family_planing.health_family_planing_breast_exam 
                    end  as health_cancer_breast_exam
        ,case when t_health_family_planing.health_family_planing_cervix_exam = '' 
                    then '0' 
                    else  t_health_family_planing.health_family_planing_cervix_exam 
                    end as health_cancer_cervix_exam
        ,t_health_family_planing.health_famlily_planing_pregnant_exam as health_cancer_pregnant_exam
        ,t_health_family_planing.health_family_planing_breast_exam_notice as health_cancer_breast_exam_note
        ,t_health_family_planing.health_family_planing_cervix_exam_notice as health_cancer_cervix_exam_note
        ,t_health_family_planing.health_family_planing_notice as health_cancer_note
        ,case  when t_health_family_planing.health_family_planing_first_check = '' 
                        then '0' 
                        else t_health_family_planing.health_family_planing_first_check 
                        end as health_cancer_first_check
        ,t_health_family_planing.health_family_planing_active as active
        ,t_health_family_planing.health_family_planing_staff_record as user_record_id
        ,t_health_family_planing.record_date_time as record_date_time
        ,t_health_family_planing.health_family_planing_staff_update as user_modify_id
        ,t_health_family_planing.update_record_date_time as modify_date_time
        ,t_health_family_planing.health_family_planing_staff_remove as user_cancel_id
        ,case when t_health_family_planing.health_family_planing_active = '0' 
                    then t_health_family_planing.update_record_date_time
                    else ''
                    end as cancel_date_time
from t_health_family_planing left join t_health_family on t_health_family_planing.t_health_family_id = t_health_family.t_health_family_id
where
      length(t_health_family_planing.record_date_time) >= 10
      and length(t_health_family_planing.update_record_date_time) >= 10
      and (t_health_family_planing.health_family_planing_breast_exam in ('1','2','3')
            or t_health_family_planing.health_family_planing_cervix_exam in ('1','2','3'))
      and t_health_family.f_sex_id = '2');

-- fixed bug DUE & NED
update b_map_due set contract_plans = array['']  where contract_plans is null;
ALTER TABLE b_map_due ALTER COLUMN contract_plans SET DEFAULT array['']::character varying[];

update b_map_ned 
set contract_plans = array[''] 
    , record_date_time =  (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS')
    , update_date_time = (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS')
where contract_plans is null;
ALTER TABLE b_map_ned ALTER COLUMN contract_plans SET DEFAULT array['']::character varying[];

--fixed bug patient no have prefix
update t_patient set f_patient_prefix_id = '000' where f_patient_prefix_id not in (select f_patient_prefix.f_patient_prefix_id from f_patient_prefix);

--SBP_DBP
update t_visit_vital_sign set visit_vital_sign_blood_presure =
        (case when position('.' in (substr(t_visit_vital_sign.visit_vital_sign_blood_presure,1,position('/' in t_visit_vital_sign.visit_vital_sign_blood_presure)-1))) > 1
                    then substr((substr(t_visit_vital_sign.visit_vital_sign_blood_presure,1,position('/' in t_visit_vital_sign.visit_vital_sign_blood_presure)-1)),1,position('.' in (substr(t_visit_vital_sign.visit_vital_sign_blood_presure,1,position('/' in t_visit_vital_sign.visit_vital_sign_blood_presure)-1)))  -1)
                when position('.' in (substr(t_visit_vital_sign.visit_vital_sign_blood_presure,1,position('/' in t_visit_vital_sign.visit_vital_sign_blood_presure)-1)))  = 1
                    then '0'
                    else(substr(t_visit_vital_sign.visit_vital_sign_blood_presure,1,position('/' in t_visit_vital_sign.visit_vital_sign_blood_presure)-1))
                    end 
         ||'/'||case when position('.' in (substr(t_visit_vital_sign.visit_vital_sign_blood_presure,position('/' in t_visit_vital_sign.visit_vital_sign_blood_presure)+1))) > 1
                    then substr((substr(t_visit_vital_sign.visit_vital_sign_blood_presure,position('/' in t_visit_vital_sign.visit_vital_sign_blood_presure)+1)),1,position('.' in (substr(t_visit_vital_sign.visit_vital_sign_blood_presure,position('/' in t_visit_vital_sign.visit_vital_sign_blood_presure)+1))) -1)
                when position('.' in (substr(t_visit_vital_sign.visit_vital_sign_blood_presure,position('/' in t_visit_vital_sign.visit_vital_sign_blood_presure)+1))) = 1
                    then '0'
                    else (substr(t_visit_vital_sign.visit_vital_sign_blood_presure,position('/' in t_visit_vital_sign.visit_vital_sign_blood_presure)+1))
                    end ) 
where 
        t_visit_vital_sign.visit_vital_sign_blood_presure ilike '%.%'
        and position('/' in t_visit_vital_sign.visit_vital_sign_blood_presure) > 0;


--visit_vital_sign_heart_rate
update t_visit_vital_sign set visit_vital_sign_heart_rate = (case when position('.' in visit_vital_sign_heart_rate) > 1
                                                                                            then substr(visit_vital_sign_heart_rate,1,position('.' in visit_vital_sign_heart_rate)-1)
                                                                                          when position('.' in visit_vital_sign_heart_rate) = 1
                                                                                            then '0'
                                                                                             else visit_vital_sign_heart_rate
                                                                                             end)
where 
        t_visit_vital_sign.visit_vital_sign_heart_rate ilike '%.%'; 

--visit_vital_sign_respiratory_rate
update t_visit_vital_sign set visit_vital_sign_respiratory_rate = (case when position('.' in visit_vital_sign_respiratory_rate) > 1
                                                                                                    then substr(visit_vital_sign_respiratory_rate,1,position('.' in visit_vital_sign_respiratory_rate)-1)
                                                                                                when position('.' in visit_vital_sign_respiratory_rate) = 1
                                                                                                    then '0'
                                                                                                    else visit_vital_sign_respiratory_rate
                                                                                                    end)
where 
        t_visit_vital_sign.visit_vital_sign_respiratory_rate ilike '%.%' ;

-- update db version
INSERT INTO s_version VALUES ('9701000000064', '64', 'Hospital OS, Community Edition', '3.9.31', '3.21.190313', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_31.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.31');
--t_order_drug set unique key order_drug_status
UPDATE t_order_drug SET t_order_id = 'BUG'||lpad(q.seq::text,2,'0')||q.t_order_id
FROM 
(select 
        t_order_drug.t_order_id
        ,row_number() over (partition by t_order_drug.t_order_id order by t_order_drug.order_drug_modify_datetime asc ,t_order_drug.t_order_drug_id asc) as seq
        ,t_order_drug.order_drug_modify_datetime as order_drug_modify_datetime
        ,t_order_drug.order_drug_order_status
        ,t_order_drug.t_order_drug_id as t_order_drug_id
from t_order_drug 
where (t_order_drug.t_order_id,t_order_drug.order_drug_order_status,t_order_drug.order_drug_active) 
in (select 
            t_order_drug.t_order_id
            ,t_order_drug.order_drug_order_status
            ,t_order_drug.order_drug_active
        from t_order_drug
        group by
            t_order_drug.t_order_id
            ,t_order_drug.order_drug_order_status
            ,t_order_drug.order_drug_active
            having count(*)  > 1)) as q
WHERE
        t_order_drug.t_order_drug_id = q.t_order_drug_id
        and q.seq != 1;
ALTER TABLE t_order_drug ADD CONSTRAINT order_drug_status_unique UNIQUE (t_order_id,order_drug_active,order_drug_order_status);

--t_lis_order set unique key order_id  
UPDATE t_lis_order SET t_order_id = 'BUG'||lpad(q.seq::text,2,'0')||q.t_order_id
FROM (select 
                t_lis_order.t_lis_order_id
                ,t_lis_order.t_order_id
                ,t_lis_order.lab_number
                ,row_number() over (partition by t_lis_order.t_order_id order by t_lis_order.lab_number asc ) as seq
        from t_lis_order 
        where t_order_id in (select 
                t_lis_order.t_order_id
        from t_lis_order
        group by
                t_lis_order.t_order_id
        having 
                count(*) > 1) ) as q
WHERE
        t_lis_order.t_lis_order_id = q.t_lis_order_id
        and q.seq != 1;
ALTER TABLE t_lis_order ADD CONSTRAINT lis_order_order_id_unique UNIQUE (t_order_id);

-- lis service
INSERT INTO b_restful_url (b_restful_url_id, restful_code, restful_url, user_update_id)
VALUES  ('999' || (select b_visit_office_id from b_site) || 'lisviewer.api','lisviewer.api','http://' || host(inet_server_addr()) || ':90/api/lis',(select rpad('157'||b_visit_office_id,18,'0')  from b_site));

INSERT INTO b_restful_url (b_restful_url_id, restful_code, restful_url, user_update_id)
VALUES  ('999' || (select b_visit_office_id from b_site) || 'lisviewer.file','lisviewer.file','http://' || host(inet_server_addr()) || ':90',(select rpad('157'||b_visit_office_id,18,'0')  from b_site));

insert into f_gui_action (f_gui_action_id, gui_action_name) VALUES ('2603', 'ดูไฟล์ผลแลปจาก LIS');
insert into f_gui_action (f_gui_action_id, gui_action_name) VALUES ('2709', 'ดูไฟล์ผลแลปจาก LIS');
insert into f_gui_action (f_gui_action_id, gui_action_name) VALUES ('3001', 'ดูไฟล์ผลแลปจาก LIS');

-- update db version
INSERT INTO s_version VALUES ('9701000000079', '79', 'Hospital OS, Community Edition', '3.9.46', '3.28.1', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_46.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.46');
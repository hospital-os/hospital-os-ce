/*
 * ListTransferDB.java
 *
 * Created on 11 �ԧ�Ҥ� 2547, 8:45 �.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.ListTransfer;
import com.hospital_os.object.VisitType;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

/**
 *
 * @author Administrator
 */
@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public class ListTransferDB {

    /**
     * Creates a new instance of ListTransferDB
     */
    public ConnectionInf theConnectionInf;

    public ListTransferDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    /**
     * ��㹡�� list �����·������㹨ش��ԡ�� �¨��觢��������
     *
     * @param visitType 0 = opd, 1 = ipd
     * @return
     * @throws Exception
     */
    public Vector listQueueXray(String visitType) throws Exception {
        String SQL = "select t_patient.patient_drugallergy\n"
                + ",t_visit.visit_locking\n"
                + ",t_visit.visit_hn\n"
                + ",t_visit.visit_vn\n"
                + ",t_patient.f_patient_prefix_id\n"
                + ",t_patient.patient_firstname\n"
                + ",t_patient.patient_lastname\n"
                + ",t_visit_queue_xray.assign_date_time\n"
                + ",t_visit.t_visit_id\n"
                + ",t_visit.t_patient_id\n"
                + ",t_visit.f_visit_type_id\n"
                + ",t_visit.visit_lab_status_id\n"
                + ",t_visit.f_emergency_status_id as emergency_status_id\n"
                + ",t_visit.xray_urgent_status as xray_urgent_status\n"
                + ",t_visit.lab_urgent_status as lab_urgent_status\n"
                + ",t_visit.drug_stat_status as drug_stat_status\n"
                + ",t_visit.visit_xray_status_id as visit_xray_status_id\n"
                + ", case when t_visit.f_visit_type_id = '0'\n"
                + "then t_visit_queue_transfer.service_point_description\n"
                + "else (select b_service_point.service_point_description from t_visit_service \n"
                + "inner join b_service_point on b_service_point.b_service_point_id = t_visit_service.b_service_point_id\n"
                + "where t_visit_id = t_visit.t_visit_id order by case when length(t_visit_service.assign_date_time) >= 10\n"
                + "then text_to_timestamp(t_visit_service.assign_date_time)\n"
                + "end desc limit 1)\n"
                + "end as service_point_description\n"
                + ", t_visit_queue_transfer.visit_queue_map_queue\n"
                + ", t_visit_queue_transfer.visit_queue_setup_queue_color\n"
                + ", t_visit_queue_transfer.visit_queue_setup_description\n"
                + ", t_visit.xray_urgent_status as xray_urgent_status \n"
                + ", b_visit_range_age.description as range_age\n"
                + ", t_visit_queue_xray.arrived_datetime\n"
                + ", t_visit_queue_xray.arrived_status\n"
                + "from t_visit_queue_xray \n"
                + "inner join t_visit on t_visit.t_visit_id = t_visit_queue_xray.t_visit_id \n"
                + "inner join t_patient on t_patient.t_patient_id = t_visit.t_patient_id \n"
                + "left join t_visit_queue_transfer on t_visit_queue_transfer.t_visit_id = t_visit_queue_xray.t_visit_id\n"
                + "left join b_visit_range_age on t_visit.b_visit_range_age_id = b_visit_range_age.b_visit_range_age_id\n"
                + "                                and b_visit_range_age.active = '1'";
        if (visitType.equalsIgnoreCase(VisitType.IPD)
                || visitType.equalsIgnoreCase(VisitType.OPD)) {
            SQL = SQL + "where t_visit.f_visit_type_id  = ?\n";
        }
        SQL += " order by t_visit_queue_xray.arrived_datetime\n"
                + ", case when length(t_visit_queue_xray.assign_date_time) >= 10\n"
                + "then text_to_timestamp(t_visit_queue_xray.assign_date_time) end";
        PreparedStatement ps = null;
        try {
            ps = theConnectionInf.ePQuery(SQL);
            int index = 1;
            if (visitType.equalsIgnoreCase(VisitType.IPD)
                    || visitType.equalsIgnoreCase(VisitType.OPD)) {
                ps.setString(index++, visitType);
            }
            return veQueryLabXrayICD(ps.toString());
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }

    /**
     * ******ListTransferDB
     *
     ********************
     * @param choose
     * @return
     * @throws Exception
     */
    public Vector listQueueLab(String visitType) throws Exception {
        //amp: 06/06/2549 �����ա������ Field 㹵��ҧ t_visit_queue_lab �����Ż���Դ
        String SQL = "select t_patient.patient_drugallergy"
                + ",t_visit.visit_locking"
                + ",t_visit.visit_hn"
                + ",t_visit.visit_vn"
                + ",t_patient.f_patient_prefix_id"
                + ",t_patient.patient_firstname"
                + ",t_patient.patient_lastname"
                + ",t_visit_queue_lab.assign_date_time"
                + ",t_visit.t_visit_id"
                + ",t_visit.t_patient_id"
                + ",t_visit.f_visit_type_id"
                + ",t_visit.visit_lab_status_id"
                + ",t_visit.f_emergency_status_id as emergency_status_id\n"
                + ",t_visit.xray_urgent_status as xray_urgent_status\n"
                + ",t_visit.lab_urgent_status as lab_urgent_status\n"
                + ",t_visit.drug_stat_status as drug_stat_status\n"
                + ",t_visit.visit_xray_status_id as visit_xray_status_id\n"
                + ",t_visit_queue_lab.visit_queue_order\n"
                + ",t_visit_queue_lab.visit_queue_secret_code\n"
                + ",t_visit_queue_transfer.visit_queue_map_queue\n"
                + ",t_visit_queue_transfer.visit_queue_setup_queue_color\n"
                + ",t_visit_queue_transfer.visit_queue_setup_description\n"
                + ",t_visit.lab_urgent_status as lab_urgent_status"
                + ",b_visit_range_age.description as range_age\n"
                + ",t_visit_queue_lab.arrived_datetime\n"
                + ",t_visit_queue_lab.arrived_status\n"
                + " from t_visit_queue_lab"
                + " inner join t_visit on t_visit_queue_lab.t_visit_id = t_visit.t_visit_id"
                + " inner join t_patient on t_patient.t_patient_id = t_visit.t_patient_id"
                + " left join t_visit_queue_transfer on t_visit_queue_transfer.t_visit_id = t_visit.t_visit_id"
                + " left join b_visit_range_age on t_visit.b_visit_range_age_id = b_visit_range_age.b_visit_range_age_id\n"
                + "                                and b_visit_range_age.active = '1'\n"
                + " where t_visit_queue_lab.visit_queue_lab_remain = '0'";
        if (visitType.equalsIgnoreCase(VisitType.IPD)
                || visitType.equalsIgnoreCase(VisitType.OPD)) {
            SQL = SQL + " and t_visit.f_visit_type_id  = ? \n";
        }
        SQL = SQL + " order by t_visit_queue_lab.arrived_datetime\n"
                + "    ,t_visit_queue_lab.assign_date_time";
        try (PreparedStatement ps = theConnectionInf.ePQuery(SQL)) {
            int index = 1;
            if (visitType.equalsIgnoreCase(VisitType.IPD)
                    || visitType.equalsIgnoreCase(VisitType.OPD)) {
                ps.setString(index++, visitType);
            }
            return veQueryLab(ps.toString());
        }
    }

    /**
     * ******ListTransferDB
     *
     ********************
     * @param visitType
     * @return
     * @throws Exception
     */
    public Vector listRemainQueueLab(String visitType) throws Exception {
        String SQL = "select t_patient.patient_drugallergy\n"
                + ",t_visit.visit_locking\n"
                + ",t_visit.visit_hn\n"
                + ",t_visit.visit_vn\n"
                + ",t_patient.f_patient_prefix_id\n"
                + ",t_patient.patient_firstname\n"
                + ",t_patient.patient_lastname\n"
                + ",t_visit_queue_lab.assign_date_time\n"
                + ",t_visit.t_visit_id\n"
                + ",t_visit.t_patient_id\n"
                + ",t_visit.f_visit_type_id\n"
                + ",t_visit.visit_lab_status_id\n"
                + ",t_visit.f_emergency_status_id as emergency_status_id\n"
                + ",t_visit.xray_urgent_status as xray_urgent_status\n"
                + ",t_visit.lab_urgent_status as lab_urgent_status\n"
                + ",t_visit.drug_stat_status as drug_stat_status\n"
                + ",t_visit.visit_xray_status_id as visit_xray_status_id\n"
                + ",t_visit_queue_lab.visit_queue_order\n"
                + ",t_visit_queue_lab.visit_queue_secret_code\n"
                + ",t_visit_queue_transfer.visit_queue_map_queue\n"
                + ",t_visit_queue_transfer.visit_queue_setup_queue_color\n"
                + ",t_visit_queue_transfer.visit_queue_setup_description\n"
                + ",t_visit.lab_urgent_status as lab_urgent_status\n"
                + ",b_visit_range_age.description as range_age\n"
                + ",t_visit_queue_lab.arrived_datetime\n"
                + " from t_visit_queue_lab"
                + " inner join t_visit on t_visit_queue_lab.t_visit_id = t_visit.t_visit_id"
                + " inner join t_patient on t_patient.t_patient_id = t_visit.t_patient_id"
                + " left join t_visit_queue_transfer on t_visit_queue_transfer.t_visit_id = t_visit.t_visit_id"
                + " left join b_visit_range_age on t_visit.b_visit_range_age_id = b_visit_range_age.b_visit_range_age_id\n"
                + "                                and b_visit_range_age.active = '1'\n"
                + " where t_visit_queue_lab.visit_queue_lab_remain = '1' ";
        if (visitType.equalsIgnoreCase(VisitType.IPD)
                || visitType.equalsIgnoreCase(VisitType.OPD)) {
            SQL = SQL + " and t_visit.f_visit_type_id  = ?\n";
        }
        SQL = SQL + " order by t_visit_queue_lab.arrived_datetime\n"
                + "    ,t_visit_queue_lab.assign_date_time";
        PreparedStatement ps = null;
        try {
            ps = theConnectionInf.ePQuery(SQL);
            int index = 1;
            if (visitType.equalsIgnoreCase(VisitType.IPD)
                    || visitType.equalsIgnoreCase(VisitType.OPD)) {
                ps.setString(index++, visitType);
            }
            return veQueryLab(ps.toString());
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }

    /**
     * ��㹡�� list �����·������㹨ش��ԡ�� �¨��觢��������
     * service_point_id = key ��ѡ�ͧ���ҧ �¨��� key ��ѡ�ͧ���ҧ
     * service_point employee_id_doctor = key ��ѡ�ͧ���ҧ �¨��� key
     * ��ѡ�ͧ���ҧ Employee ੾�Шش��ԡ�÷������ͧ��Ǩ choose =
     * ���͡����繼������ ���� �����¹͡
     *
     * @param ward_id
     * @return
     * @throws Exception
     */
    public Vector listQueueVisitInWard(String ward_id) throws Exception {
        String SQL = "select t_visit.visit_locking"
                + ",t_visit.visit_vn"
                + ",t_visit.visit_hn"
                + ",t_patient.patient_firstname"
                + ",t_patient.patient_lastname"
                + ",t_visit.t_patient_id"
                + ",t_visit.t_visit_id"
                + ",t_patient.patient_drugallergy"
                + ",t_visit.visit_patient_self_doctor as doctor"
                + ",t_visit.visit_begin_admit_date_time as assign_date_time"
                + ",t_patient.f_patient_prefix_id"
                + ",t_visit.visit_bed"
                + ",t_visit.visit_lab_status_id as visit_lab_status_id"
                + ",t_visit.f_emergency_status_id as emergency_status_id\n"
                + ",t_visit.visit_dx as doctor_dx"
                + ",t_visit.xray_urgent_status as xray_urgent_status"
                + ",t_visit.lab_urgent_status as lab_urgent_status"
                + ",t_visit.drug_stat_status as drug_stat_status"
                + ",t_visit.visit_xray_status_id as visit_xray_status_id"
                + ",t_visit.visit_vital_sign_score"
                + ",t_visit.visit_vital_sign_notify_datetime"
                + ",t_visit.visit_vital_sign_newspews_type"
                + " from t_visit,t_patient "
                + " where t_visit.b_visit_ward_id = ? "
                + " and t_visit.t_patient_id = t_patient.t_patient_id "
                + " and t_visit.f_visit_status_id = '1' "
                + " and t_visit.f_visit_type_id = '1' "
                + " order by  t_visit.visit_begin_admit_date_time ";
        PreparedStatement ps = null;
        try {
            ps = theConnectionInf.ePQuery(SQL);
            int index = 1;
            ps.setString(index++, ward_id);
            return veQueryWard(ps.toString());
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }

    public Vector veQueryLabXrayICD(String sql) throws Exception {
        ListTransfer p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            p = new ListTransfer();
            p.assign_time = rs.getString("assign_date_time");
            p.fname = rs.getString("patient_firstname");
            p.hn = rs.getString("visit_hn");
            p.lname = rs.getString("patient_lastname");
            p.locking = rs.getString("visit_locking");
            p.name = rs.getString("service_point_description");
            p.patient_id = rs.getString("t_patient_id");
            p.visit_id = rs.getString("t_visit_id");
            p.vn = rs.getString("visit_vn");
            p.visit_type = rs.getString("f_visit_type_id");
            p.prefix = rs.getString("f_patient_prefix_id");
            p.patient_allergy = rs.getString("patient_drugallergy");
            p.labstatus = rs.getString("visit_lab_status_id");
            p.queue = rs.getString("visit_queue_map_queue");
            p.color = rs.getString("visit_queue_setup_queue_color");
            p.description = rs.getString("visit_queue_setup_description");
            p.emergency_status = rs.getString("emergency_status_id");
            try {
                p.lab_urgent_status = rs.getString("lab_urgent_status");
                p.xray_urgent_status = rs.getString("xray_urgent_status");
                p.drug_stat_status = rs.getString("drug_stat_status");
            } catch (SQLException e) {
            }
            p.xraystatus = rs.getString("visit_xray_status_id");
            try {
                p.range_age = rs.getString("range_age");
            } catch (SQLException e) {
            }
            try {
                p.arrived_datetime = rs.getTimestamp("arrived_datetime");
            } catch (SQLException e) {
            }
            try {
                p.arrived_status = rs.getString("arrived_status");
            } catch (SQLException e) {
            }
            list.add(p);
        }
        rs.close();
        return list;
    }

    /**
     * @Author: amp
     * @date : 06/03/2549
     * @param sql
     * @return
     * @throws Exception
     */
    public Vector veQueryLab(String sql) throws Exception {
        ListTransfer p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            p = new ListTransfer();
            p.assign_time = rs.getString("assign_date_time");
            p.fname = rs.getString("patient_firstname");
            p.hn = rs.getString("visit_hn");
            p.lname = rs.getString("patient_lastname");
            p.locking = rs.getString("visit_locking");
            p.patient_id = rs.getString("t_patient_id");
            p.visit_id = rs.getString("t_visit_id");
            p.vn = rs.getString("visit_vn");
            p.visit_type = rs.getString("f_visit_type_id");
            p.prefix = rs.getString("f_patient_prefix_id");
            p.patient_allergy = rs.getString("patient_drugallergy");
            p.labstatus = rs.getString("visit_lab_status_id");
            p.order_id = rs.getString("visit_queue_order");
            p.specimen_code = rs.getString("visit_queue_secret_code");
            p.queue = rs.getString("visit_queue_map_queue");
            p.color = rs.getString("visit_queue_setup_queue_color");
            p.description = rs.getString("visit_queue_setup_description");
            p.emergency_status = rs.getString("emergency_status_id");
            try {
                p.lab_urgent_status = rs.getString("lab_urgent_status");
                p.xray_urgent_status = rs.getString("xray_urgent_status");
                p.drug_stat_status = rs.getString("drug_stat_status");
            } catch (SQLException e) {
            }
            p.xraystatus = rs.getString("visit_xray_status_id");
            try {
                p.range_age = rs.getString("range_age");
            } catch (SQLException e) {
            }
            try {
                p.arrived_datetime = rs.getTimestamp("arrived_datetime");
            } catch (SQLException e) {
            }
            try {
                p.arrived_status = rs.getString("arrived_status");
            } catch (SQLException e) {
            }
            list.add(p);
        }
        rs.close();
        return list;
    }

    public Vector veQueryWard(String sql) throws Exception {
        ListTransfer p;
        Vector list = new Vector();
        try (ResultSet rs = theConnectionInf.eQuery(sql)) {
            while (rs.next()) {
                p = new ListTransfer();
                p.assign_time = rs.getString("assign_date_time");
                p.prefix = rs.getString("f_patient_prefix_id");
                p.fname = rs.getString("patient_firstname");
                p.hn = rs.getString("visit_hn");
                p.lname = rs.getString("patient_lastname");
                p.locking = rs.getString("visit_locking");
                p.patient_id = rs.getString("t_patient_id");
                p.visit_id = rs.getString("t_visit_id");
                p.vn = rs.getString("visit_vn");
                p.bed = rs.getString("visit_bed");
                p.patient_allergy = rs.getString("patient_drugallergy");
                p.labstatus = rs.getString("visit_lab_status_id");
                p.emergency_status = rs.getString("emergency_status_id");
                p.doctor_dx = rs.getString("doctor_dx");
                p.doctor = rs.getString("doctor");
                try {
                    p.lab_urgent_status = rs.getString("lab_urgent_status");
                    p.xray_urgent_status = rs.getString("xray_urgent_status");
                    p.drug_stat_status = rs.getString("drug_stat_status");
                } catch (SQLException e) {
                }
                p.xraystatus = rs.getString("visit_xray_status_id");
                p.visit_vital_sign_score = rs.getString("visit_vital_sign_score");
                p.visit_vital_sign_notify_datetime = rs.getTimestamp("visit_vital_sign_notify_datetime");
                p.visit_vital_sign_newspews_type = rs.getString("visit_vital_sign_newspews_type");
                list.add(p);
            }
        }
        return list;
    }
}

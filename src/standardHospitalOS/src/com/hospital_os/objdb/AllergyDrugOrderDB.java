/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.AllergyDrugOrder;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.Gutil;
import java.sql.ResultSet;
import java.util.Vector;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class AllergyDrugOrderDB {

    private final ConnectionInf theConnectionInf;
    private final String tableId = "992";

    public AllergyDrugOrderDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public int insert(AllergyDrugOrder obj) throws Exception {
        String sql = "INSERT INTO t_allergy_drug_order( "
                + "t_allergy_drug_order_id, t_patient_drug_allergy_id, t_order_id,  "
                + "allergy_drug_order_cause, user_record,  "
                + "record_date_time, user_modify, modify_date_time, active) "
                + "VALUES ('%s', '%s', '%s',  "
                + "'%s', '%s', '%s',  "
                + "'%s', '%s', '%s')";
        sql = String.format(sql, obj.getGenID(tableId), obj.t_patient_drug_allergy_id,
                obj.t_order_id,
                Gutil.CheckReservedWords(obj.allergy_drug_order_cause),
                obj.user_record, obj.record_date_time,
                obj.user_modify, obj.modify_date_time, "1");
        return theConnectionInf.eUpdate(sql);
    }

    public int update(AllergyDrugOrder obj) throws Exception {
        String sql = "UPDATE t_allergy_drug_order "
                + "SET t_patient_drug_allergy_id='%s', t_order_id='%s',  "
                + "allergy_drug_order_cause='%s',  "
                + "user_modify='%s', modify_date_time='%s', active ='%s' "
                + "WHERE t_allergy_drug_order_id='%s' ";
        sql = String.format(sql, obj.t_patient_drug_allergy_id,
                obj.t_order_id,
                Gutil.CheckReservedWords(obj.allergy_drug_order_cause),
                obj.user_modify, obj.modify_date_time, obj.active, obj.getObjectId());
        return theConnectionInf.eUpdate(sql);
    }

    public AllergyDrugOrder selectById(String id) throws Exception {
        String sql = "select * from t_allergy_drug_order where t_allergy_drug_order_id = '%s'";
        Vector eQuery = eQuery(String.format(sql, id));
        return (AllergyDrugOrder) (eQuery.isEmpty() ? null : eQuery.get(0));
    }

    public Vector eQuery(String sql) throws Exception {
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        while (rs.next()) {
            AllergyDrugOrder p = new AllergyDrugOrder();
            p.setObjectId(rs.getString("t_allergy_drug_order_id"));
            p.t_patient_drug_allergy_id = rs.getString("t_patient_drug_allergy_id");
            p.t_order_id = rs.getString("t_order_id");
            p.allergy_drug_order_cause = rs.getString("allergy_drug_order_cause");
            p.user_record = rs.getString("user_record");
            p.record_date_time = rs.getString("record_date_time");
            p.user_modify = rs.getString("user_modify");
            p.modify_date_time = rs.getString("modify_date_time");
            p.active = rs.getString("active");
            list.add(p);
        }
        rs.close();
        return list;
    }
}

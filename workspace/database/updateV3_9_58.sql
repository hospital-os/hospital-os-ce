alter table t_visit add column visit_ipd_authen_code varchar(255) DEFAULT ''::character varying;
alter table t_visit add column visit_ipd_authen_date_time timestamp;


-- update db version
INSERT INTO s_version VALUES ('9701000000091', '91', 'Hospital OS, Community Edition', '3.9.58', '3.38.0', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_58.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.58');
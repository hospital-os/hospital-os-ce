/*
 * SpecialQueryOrderDrugDB.java
 *
 * Created on 27 ���Ҥ� 2548, 22:06 �.
 */
package com.hospital_os.objdb.specialQuery;

import com.hospital_os.objdb.*;
import com.hospital_os.object.*;
import com.hospital_os.usecase.connection.*;
import java.sql.*;
import java.util.*;

/**
 *
 * @author Administrator
 */
@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public class SpecialQueryOrderDrugDB {

    /**
     * Creates a new instance of SpecialQueryOrderDrugDB
     */
    OrderItemDrug dbOrderItemDrug = null;
    OrderItem dbOrderItem = null;
    public ConnectionInf theConnectionInf;
    OrderItemDrugDB theOrderItemDrugDB = null;
    OrderItemDB theOrderItemDB = null;
    Vector vOrder = null;
    Vector vOrderDrug = null;
    vOrderSpecial thevOrderSpecial = new vOrderSpecial();
    String orderitem = "";

    public SpecialQueryOrderDrugDB(ConnectionInf db) {
        theConnectionInf = db;
        theOrderItemDrugDB = new OrderItemDrugDB(db);
        theOrderItemDB = new OrderItemDB(db);
        dbOrderItemDrug = theOrderItemDrugDB.initConfig();
        dbOrderItem = theOrderItemDB.initConfig();

        orderitem = dbOrderItemDrug.table + "." + dbOrderItemDrug.order_item_id;
        dbOrderItem.pk_field = dbOrderItem.table + "." + dbOrderItem.pk_field;

        dbOrderItemDrug.item_id = dbOrderItemDrug.table + "." + dbOrderItemDrug.item_id;
        /*  dbOrderItem.item_code =  dbOrderItem.table + "." + dbOrderItem.item_code ;
         */

    }

    public vOrderSpecial queryOrderItemAndOrderItemDrugByVisitID(String visit_id) throws Exception {
        String sql = "SELECT "
                + dbOrderItemDrug.caution + ","
                + dbOrderItemDrug.day_time + ","
                + dbOrderItemDrug.description + ","
                + dbOrderItemDrug.dose + ","
                + dbOrderItemDrug.frequency + ","
                + dbOrderItemDrug.instruction + ","
                + dbOrderItemDrug.item_id + ","
                + orderitem + ","
                + dbOrderItemDrug.pk_field + ","
                + dbOrderItemDrug.printing + ","
                + dbOrderItemDrug.purch_uom + ","
                + dbOrderItemDrug.usage_special + ","
                + dbOrderItemDrug.usage_text + ","
                + dbOrderItemDrug.use_uom + " "
                + " FROM " + dbOrderItemDrug.table + "," + dbOrderItem.table + ""
                + " WHERE " + orderitem + "=" + dbOrderItem.pk_field + ""
                + " AND " + dbOrderItem.visit_id + "='" + visit_id + "'"
                + " AND " + dbOrderItem.status + " <> '" + OrderStatus.DIS_CONTINUE + "'"
                + " ORDER BY " + dbOrderItem.vertify_time + "," + dbOrderItem.status + " DESC ," + dbOrderItem.category_group;

        return eQuery(sql);

    }

    public vOrderSpecial eQuery(String sql) throws Exception {
        vOrder = new Vector();
        vOrderDrug = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            OrderItemDrug orderitemdrug = new OrderItemDrug();
            orderitemdrug.caution = rs.getString(dbOrderItemDrug.caution);
            orderitemdrug.day_time = rs.getString(dbOrderItemDrug.day_time);
            orderitemdrug.description = rs.getString(dbOrderItemDrug.description);
            orderitemdrug.dose = rs.getString(dbOrderItemDrug.dose);
            orderitemdrug.frequency = rs.getString(dbOrderItemDrug.frequency);
            orderitemdrug.instruction = rs.getString(dbOrderItemDrug.instruction);
            orderitemdrug.item_id = rs.getString(dbOrderItem.item_code);
            orderitemdrug.order_item_id = rs.getString(dbOrderItemDrug.order_item_id);
            orderitemdrug.setObjectId(rs.getString(dbOrderItemDrug.pk_field));
            orderitemdrug.printing = rs.getString(dbOrderItemDrug.printing);
            orderitemdrug.purch_uom = rs.getString(dbOrderItemDrug.purch_uom);
            orderitemdrug.usage_special = rs.getString(dbOrderItemDrug.usage_special);
            orderitemdrug.usage_text = rs.getString(dbOrderItemDrug.usage_text);
            orderitemdrug.use_uom = rs.getString(dbOrderItemDrug.use_uom);
            vOrderDrug.add(orderitemdrug);
        }
        thevOrderSpecial.vorder = vOrder;
        thevOrderSpecial.vorderitemdrug = vOrderDrug;
        rs.close();
        return thevOrderSpecial;
    }

    public Vector getOrderItem() {
        return this.vOrder;
    }

    public Vector getOrderItemDrug() {
        return this.vOrderDrug;
    }
}

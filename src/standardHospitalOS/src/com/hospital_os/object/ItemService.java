package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

public class ItemService extends Persistent {

    public String item_id = "";
    public String description = "";
    public String icd9_code = "";
    public String active = "1";
    public String record_date_time = "";

    /**
     * @roseuid 3F658BBB036E
     */
    public ItemService() {
    }
}

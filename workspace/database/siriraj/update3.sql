ALTER TABLE t_billing_receipt_cash_type ADD COLUMN billing_receipt_cash_type_cardname character varying(255) DEFAULT '';

INSERT INTO s_siriraj_version VALUES ('9750000000003', '3', 'Siriraj, Community Edition', '2.4.251011', '1.1.251011', to_char(current_timestamp, 'YYYY-MM-DD HH24:MI:SS'));
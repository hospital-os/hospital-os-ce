/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.NewsPewsMessagePattern;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tanakrit
 */
public class NewsPewsMessagePatternDB {

    public ConnectionInf theConnectionInf;
    final public String idtable = "982";

    public NewsPewsMessagePatternDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public int insert(NewsPewsMessagePattern p) throws Exception {
        p.generateOID(idtable);
        String sql = "insert into b_news_pews_msg_pattern ( \n"
                + "               b_news_pews_msg_pattern_id , message_pattern, is_opd,  \n"
                + "               user_record_id, user_update_id \n"
                + "     ) values (?, ?, ?, \n"
                + "               ?, ?)";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, p.getObjectId());
            ePQuery.setString(index++, p.message_pattern);
            ePQuery.setBoolean(index++, p.is_opd);
            ePQuery.setString(index++, p.user_record_id);
            ePQuery.setString(index++, p.user_update_id);
            return ePQuery.executeUpdate();
        }
    }

    public int update(NewsPewsMessagePattern p) throws Exception {
        String sql = "UPDATE b_news_pews_msg_pattern \n"
                + "      SET message_pattern = ? , is_opd = ? , user_update_id = ? ,update_date_time = current_timestamp\n"
                + "    where b_news_pews_msg_pattern_id=?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, p.message_pattern);
            ePQuery.setBoolean(index++, p.is_opd);
            ePQuery.setString(index++, p.user_update_id);
            ePQuery.setString(index++, p.getObjectId());
            return ePQuery.executeUpdate();
        }
    }

    public List<NewsPewsMessagePattern> selectAll() throws Exception {
        String sql = "select * from b_news_pews_msg_pattern";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            return executeQuery(ePQuery);
        }
    }

    public List<NewsPewsMessagePattern> selectByType(String newsPewsType) throws Exception {
        String sql = "select * from b_news_pews_msg_pattern where newspews_type = ?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setObject(index++, newsPewsType, java.sql.Types.OTHER);
            return executeQuery(ePQuery);
        }
    }

    public List<NewsPewsMessagePattern> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<NewsPewsMessagePattern> list = new ArrayList<>();
        try (ResultSet rs = preparedStatement.executeQuery()) {
            while (rs.next()) {
                NewsPewsMessagePattern p = new NewsPewsMessagePattern();
                p.setObjectId(rs.getString("b_news_pews_msg_pattern_id"));
                p.message_pattern = rs.getString("message_pattern");
                p.is_opd = rs.getBoolean("is_opd");
                p.newspews_type = rs.getString("newspews_type");
                p.user_record_id = rs.getString("user_record_id");
                p.record_date_time = rs.getTimestamp("record_date_time");
                p.user_update_id = rs.getString("user_update_id");
                p.update_date_time = rs.getTimestamp("update_date_time");
                list.add(p);
            }
            return list;
        }
    }
}

<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="rp_drug_history" language="groovy" pageWidth="595" pageHeight="842" columnWidth="539" leftMargin="28" rightMargin="28" topMargin="28" bottomMargin="28" uuid="7676d4fd-fbd2-4a61-ae30-34390346d484">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="employee_id" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="keyword" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="start_date" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="end_date" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="patient_id" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[select
        t_patient.patient_hn as hn
        ,f_patient_prefix.patient_prefix_description||t_patient.patient_firstname||'   '||t_patient.patient_lastname as name
        ,substring(' ' ||age(to_date(substr(t_patient.patient_birthday,1,10),'YYYY-MM-DD') - interval '543 year') from '(...)year') as age
        ,order_drug.order_dispen_date_time
        ,order_drug.order_dispense_date_time
        ,order_drug.order_common_name
        ,order_drug.order_qty
        ,order_drug.drug_purch
        ,order_drug.drug_instruction
        ,prefix_person.patient_prefix_description || t_person.person_firstname || ' ' || t_person.person_lastname as employee
from t_patient left join
                        (select
                                    t_order.t_patient_id as t_patient_id
                                    ,t_order.order_dispense_date_time as  order_dispen_date_time
                                    ,substr(t_order.order_dispense_date_time,9,2)||'/'||substr(t_order.order_dispense_date_time,6,2)||'/'||substr(t_order.order_dispense_date_time,1,4)
                                        ||substr(t_order.order_dispense_date_time,11,6) as order_dispense_date_time
                                    ,t_order.order_common_name as order_common_name
                                    ,t_order.order_qty as order_qty
                                    ,drug_purch.item_drug_uom_description as drug_purch
                                    ,b_item_drug_instruction.item_drug_instruction_description||'  '||to_char(t_order_drug.order_drug_dose, '0D9') ||'  '||drug_use.item_drug_uom_description as drug_instruction

                          from
                                    t_order
                                    left join t_order_drug on t_order.t_order_id = t_order_drug.t_order_id
                                    left join b_item_drug_uom as drug_purch on t_order_drug.b_item_drug_uom_id_purch = drug_purch.b_item_drug_uom_id
                                    left join b_item_drug_uom as drug_use on t_order_drug.b_item_drug_uom_id_use = drug_use.b_item_drug_uom_id
                                    left join b_item_drug_instruction on t_order_drug.b_item_drug_instruction_id = b_item_drug_instruction.b_item_drug_instruction_id
                           where
                                    t_order.f_order_status_id = '5'
			     and t_order.f_item_group_id = '1'
                                    and t_order_drug.order_drug_active = '1'
                                    and t_order.order_common_name ilike '%$P!{keyword}%'
                                    and substr(t_order.order_dispense_date_time,1,10) between $P{start_date} and  $P{end_date}
                         )as order_drug on t_patient.t_patient_id = order_drug.t_patient_id
        left join f_patient_prefix on t_patient.f_patient_prefix_id = f_patient_prefix.f_patient_prefix_id
        cross join b_employee
        inner join t_person on t_person.t_person_id = b_employee.t_person_id
        left join f_patient_prefix as prefix_person on prefix_person.f_patient_prefix_id = t_person.f_prefix_id
where
        t_patient.t_patient_id = $P{patient_id}
        and b_employee.b_employee_id = $P{employee_id}
order by
        order_drug.order_dispen_date_time desc
        ,order_drug.order_common_name asc]]>
	</queryString>
	<field name="hn" class="java.lang.String"/>
	<field name="name" class="java.lang.String"/>
	<field name="age" class="java.lang.String"/>
	<field name="order_dispen_date_time" class="java.lang.String"/>
	<field name="order_dispense_date_time" class="java.lang.String"/>
	<field name="order_common_name" class="java.lang.String"/>
	<field name="order_qty" class="java.lang.Double"/>
	<field name="drug_purch" class="java.lang.String"/>
	<field name="drug_instruction" class="java.lang.String"/>
	<field name="employee" class="java.lang.String"/>
	<title>
		<band height="109" splitType="Stretch">
			<staticText>
				<reportElement uuid="163fcf22-1915-4817-916c-5b95698e8418" x="150" y="0" width="238" height="30"/>
				<textElement textAlignment="Center">
					<font fontName="TH SarabunPSK" size="24" isBold="true"/>
				</textElement>
				<text><![CDATA[ประวัติการใช้ยา]]></text>
			</staticText>
			<textField pattern="###0.00">
				<reportElement uuid="5726141e-8d7d-4755-955f-3f4497b13882" x="84" y="30" width="371" height="20"/>
				<textElement textAlignment="Center">
					<font fontName="TH SarabunPSK" size="16"/>
				</textElement>
				<textFieldExpression><![CDATA["ตั้งแต่วันที่ "+
new java.text.SimpleDateFormat("dd/MM/yyyy", java.util.Locale.US).format(new java.text.SimpleDateFormat("yyyy-MM-dd",java.util.Locale.US).parse($P{start_date}))
+" ถึงวันที่ "+
new java.text.SimpleDateFormat("dd/MM/yyyy", java.util.Locale.US).format(new java.text.SimpleDateFormat("yyyy-MM-dd",java.util.Locale.US).parse($P{end_date}))]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement uuid="fc1531ff-c938-4e65-9277-8cd16cbbc9e0" x="24" y="63" width="25" height="20"/>
				<textElement>
					<font fontName="TH SarabunPSK" size="16"/>
				</textElement>
				<text><![CDATA[HN :]]></text>
			</staticText>
			<componentElement>
				<reportElement uuid="e1e60189-5c9c-4667-8895-4d140e40d75e" x="49" y="52" width="127" height="50"/>
				<jr:Code39 xmlns:jr="http://jasperreports.sourceforge.net/jasperreports/components" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports/components http://jasperreports.sourceforge.net/xsd/components.xsd" textPosition="bottom">
					<jr:codeExpression><![CDATA[$F{hn}]]></jr:codeExpression>
				</jr:Code39>
			</componentElement>
			<staticText>
				<reportElement uuid="8f62dc84-6952-45ac-97a7-132ca4e86364" x="190" y="63" width="45" height="20"/>
				<textElement>
					<font fontName="TH SarabunPSK" size="16"/>
				</textElement>
				<text><![CDATA[ชื่อ - สกุล]]></text>
			</staticText>
			<textField>
				<reportElement uuid="a18a2d2b-61ab-4ef6-b095-1b4dfbc9a8ec" x="235" y="63" width="199" height="20"/>
				<textElement>
					<font fontName="TH SarabunPSK" size="16"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{name}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement uuid="25894dbc-fe9d-4edd-bd00-d30ab3cc8a58" x="449" y="63" width="26" height="20"/>
				<textElement>
					<font fontName="TH SarabunPSK" size="16"/>
				</textElement>
				<text><![CDATA[อายุ]]></text>
			</staticText>
			<textField>
				<reportElement uuid="0243f174-4ba5-4b32-8b0e-3019ed3a4a19" x="475" y="63" width="65" height="20"/>
				<textElement>
					<font fontName="TH SarabunPSK" size="16"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{age}+" ปี"]]></textFieldExpression>
			</textField>
		</band>
	</title>
	<columnHeader>
		<band height="20" splitType="Stretch">
			<staticText>
				<reportElement uuid="83d68e91-831c-4554-b790-7bba7985403b" x="2" y="0" width="92" height="20"/>
				<textElement textAlignment="Center">
					<font fontName="TH SarabunPSK" size="16" isBold="true"/>
				</textElement>
				<text><![CDATA[วันที่ เวลา]]></text>
			</staticText>
			<line>
				<reportElement uuid="6dfd9372-292d-48ec-95bd-66ed92ee71c7" x="0" y="19" width="539" height="1"/>
			</line>
			<line>
				<reportElement uuid="c0131f13-04df-4235-a64f-7d444bab9fc4" x="0" y="0" width="539" height="1"/>
			</line>
			<staticText>
				<reportElement uuid="1124ce00-92e9-4fea-8431-b045c3aac8cb" x="100" y="0" width="210" height="20"/>
				<textElement textAlignment="Center">
					<font fontName="TH SarabunPSK" size="16" isBold="true"/>
				</textElement>
				<text><![CDATA[ชื่อยา]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="8ca0b029-c64d-4c69-831b-465b5fc400c1" x="314" y="0" width="41" height="20"/>
				<textElement textAlignment="Center">
					<font fontName="TH SarabunPSK" size="16" isBold="true"/>
				</textElement>
				<text><![CDATA[จำนวน]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="d5d34688-6c47-4a96-9610-113df1800205" x="354" y="0" width="41" height="20"/>
				<textElement textAlignment="Center">
					<font fontName="TH SarabunPSK" size="16" isBold="true"/>
				</textElement>
				<text><![CDATA[หน่วย]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="1f60179c-ccd2-4379-a2eb-be5873c64238" x="396" y="0" width="142" height="20"/>
				<textElement textAlignment="Center">
					<font fontName="TH SarabunPSK" size="16" isBold="true"/>
				</textElement>
				<text><![CDATA[วิธีการใช้ยา]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="20" splitType="Stretch">
			<textField isBlankWhenNull="true">
				<reportElement uuid="42c541d8-d6a0-4143-9870-5d9d6b17a56e" x="2" y="0" width="92" height="20"/>
				<textElement textAlignment="Center">
					<font fontName="TH SarabunPSK" size="16"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{order_dispense_date_time}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement uuid="08b49212-f7ce-43d1-890c-2b943a558313" x="100" y="0" width="208" height="20" isPrintWhenDetailOverflows="true"/>
				<textElement>
					<font fontName="TH SarabunPSK" size="16"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{order_common_name}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement uuid="4dd3d4bf-ddce-4517-ac86-a5fc7c0557b0" x="314" y="0" width="29" height="20"/>
				<textElement textAlignment="Right">
					<font fontName="TH SarabunPSK" size="16"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{order_qty}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement uuid="bcf397d1-b2aa-4bd1-89cd-df642686d6b5" x="354" y="0" width="41" height="20"/>
				<textElement textAlignment="Center">
					<font fontName="TH SarabunPSK" size="16"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{drug_purch}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="e411eecf-dfab-4ef4-8dc0-f139b469ff8d" x="396" y="0" width="142" height="20" isPrintWhenDetailOverflows="true"/>
				<textElement>
					<font fontName="TH SarabunPSK" size="16"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{drug_instruction}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<pageFooter>
		<band height="31" splitType="Stretch">
			<textField>
				<reportElement uuid="60277935-9ed4-45ea-b94d-f96376c5dd54" x="3" y="2" width="204" height="20"/>
				<textElement>
					<font fontName="TH SarabunPSK" size="16"/>
				</textElement>
				<textFieldExpression><![CDATA["ผู้พิมพ์ "+$F{employee}]]></textFieldExpression>
			</textField>
			<textField pattern="วันที่พิมพ์ dd/MM/yyyy">
				<reportElement uuid="a0db3a33-a648-4ff1-b6e0-90843f2aa009" x="207" y="2" width="125" height="20"/>
				<textElement textAlignment="Center">
					<font fontName="TH SarabunPSK" size="16"/>
				</textElement>
				<textFieldExpression><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="10f13278-0e5e-4c6a-99d2-e5226d0d86e9" x="449" y="2" width="70" height="20"/>
				<textElement textAlignment="Right">
					<font fontName="TH SarabunPSK" size="16"/>
				</textElement>
				<textFieldExpression><![CDATA["Page  "+$V{PAGE_NUMBER}+" of "]]></textFieldExpression>
			</textField>
			<line>
				<reportElement uuid="62222884-bcde-40bd-9cfe-a9b8cfe0a5c2" x="0" y="0" width="539" height="1"/>
			</line>
			<textField evaluationTime="Report">
				<reportElement uuid="3629f79a-e3d4-426a-913a-c2a1880b7176" x="519" y="2" width="20" height="20"/>
				<textElement>
					<font fontName="TH SarabunPSK" size="16"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
</jasperReport>

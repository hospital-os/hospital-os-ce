/*
 * HosPanelSetup.java
 *
 * Created on 16 ����Ҥ� 2548, 20:57 �.
 */
package com.hosv3.gui.panel.setup;

import com.hospital_os.object.Active;
import com.hospital_os.object.Authentication;
import com.hospital_os.object.SetupModule;
import com.hospital_os.object.Version;
import com.hospital_os.usecase.connection.ModuleInfTool;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hospital_os.utility.IconData;
import com.hosv3.control.HosControl;
import com.hosv3.gui.component.PanelSetup;
import com.hosv3.gui.component.PanelSetupImp;
import com.hosv3.gui.component.PanelSetupXPer;
import com.hosv3.gui.dialog.HosDialog;
import com.hosv3.gui.panel.detail.PanelNews;
import com.hosv3.gui.panel.detail.PanelSD16Group;
import com.hosv3.gui.panel.detail.PanelSDAutoOrderItem;
import com.hosv3.gui.panel.detail.PanelSDBankInfo;
import com.hosv3.gui.panel.detail.PanelSDBill;
import com.hosv3.gui.panel.detail.PanelSDBodyOrgan;
import com.hosv3.gui.panel.detail.PanelSDClinic;
import com.hosv3.gui.panel.detail.PanelSDContract;
import com.hosv3.gui.panel.detail.PanelSDCreditCardType;
import com.hosv3.gui.panel.detail.PanelSDDrugDoseShortcut;
import com.hosv3.gui.panel.detail.PanelSDDrugFrequency;
import com.hosv3.gui.panel.detail.PanelSDDrugInstruction;
import com.hosv3.gui.panel.detail.PanelSDDrugItemDistributor;
import com.hosv3.gui.panel.detail.PanelSDDrugItemManufacturer;
import com.hosv3.gui.panel.detail.PanelSDDrugStandard;
import com.hosv3.gui.panel.detail.PanelSDDxTemplate;
import com.hosv3.gui.panel.detail.PanelSDEmployee;
import com.hosv3.gui.panel.detail.PanelSDGroupIcd10;
import com.hosv3.gui.panel.detail.PanelSDGuide;
import com.hosv3.gui.panel.detail.PanelSDICD10;
import com.hosv3.gui.panel.detail.PanelSDICD9;
import com.hosv3.gui.panel.detail.PanelSDInsuranceCompany;
import com.hosv3.gui.panel.detail.PanelSDInsuranceDiscount;
import com.hosv3.gui.panel.detail.PanelSDLabOrderCause;
import com.hosv3.gui.panel.detail.PanelSDLabSpecimen;
import com.hosv3.gui.panel.detail.PanelSDLanguage;
import com.hosv3.gui.panel.detail.PanelSDModuleLicense;
import com.hosv3.gui.panel.detail.PanelSDNCDGroup;
import com.hosv3.gui.panel.detail.PanelSDOffice;
import com.hosv3.gui.panel.detail.PanelSDOpinionRecommendation;
import com.hosv3.gui.panel.detail.PanelSDOrderGroup;
import com.hosv3.gui.panel.detail.PanelSDPayer;
import com.hosv3.gui.panel.detail.PanelSDPlan;
import com.hosv3.gui.panel.detail.PanelSDQueueVisit;
import com.hosv3.gui.panel.detail.PanelSDRangeAgeVisit;
import com.hosv3.gui.panel.detail.PanelSDRestfulUrl;
import com.hosv3.gui.panel.detail.PanelSDResultListType;
import com.hosv3.gui.panel.detail.PanelSDSequenceData;
import com.hosv3.gui.panel.detail.PanelSDSetupTariff;
import com.hosv3.gui.panel.detail.PanelSDUom2;
import com.hosv3.gui.panel.detail.PanelSDWard;
import com.hosv3.gui.panel.detail.PanelSDXRayLeteral;
import com.hosv3.gui.panel.detail.PanelSDXRayModality;
import com.hosv3.gui.panel.detail.PanelSDXRayPosition;
import com.hosv3.object.HosObject;
import com.hosv3.utility.Constant;
import com.hosv3.utility.ReadModule;
import java.awt.CardLayout;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

/**
 *
 * @author administrator
 */
@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public class HosPanelSetup {

    public DefaultTreeModel theTreeModel;
    public JPanel thePanelCard;
    private DefaultMutableTreeNode theTreeNode;
    private DefaultMutableTreeNode theHeadNode;
    private HosObject theHO;
    private HosControl theHC;
    private HosDialog theHD;
    private UpdateStatus theUS;
    private final String PANEL_SETUP_XRAY_FILMSIZE = Constant.getTextBundle("PANEL_SETUP_XRAY_FILMSIZE");
    private final String PANEL_SETUP_GAA = Constant.getTextBundle("�Է�ԡ����ҹ");
    private final String PANEL_SETUP_CONTRACT = Constant.getTextBundle("PANEL_SETUP_CONTRACT");
    private final String PANEL_SETUP_RECEIPT_SEQUANCE = Constant.getTextBundle("�Ţ Receipt Sequence");
    private final String PANEL_SETUP_NEWS = Constant.getTextBundle("�駢���");
    public final String PANEL_SETUP_EMPLOYEE = Constant.getTextBundle("PANEL_SETUP_EMPLOYEE");
    private final String PANEL_SETUP_OFFICE_INCUP = Constant.getTextBundle("PANEL_SETUP_OFFICE_INCUP");
    private final String PANEL_SETUP_OPTION = Constant.getTextBundle("PANEL_SETUP_OPTION");
    private final String PANEL_SETUP_ORDER_ITEM = Constant.getTextBundle("PANEL_SETUP_ORDER_ITEM");
    private final String PANEL_SETUP_REMAIN_0 = Constant.getTextBundle("�����¤�ҧ����");
    private final String PANEL_SETUP_VITAL_TEMPLATE = Constant.getTextBundle("PANEL_SETUP_VITAL_TEMPLATE");
    private final String PANEL_SETUP_SERVICE_POINT = Constant.getTextBundle("�ش��ԡ��");
    private final String PANEL_SETUP_SITE = Constant.getTextBundle("PANEL_SETUP_SITE");
    private final String PANEL_SETUP_DOCTOR_DRUGSET = Constant.getTextBundle("PANEL_SETUP_DOCTOR_DRUGSET");
    private final String PANEL_SETUP_FORLIFE = Constant.getTextBundle("PANEL_SETUP_FORLIFE");
    private final String PANEL_SETUP_CHRONIC = Constant.getTextBundle("PANEL_SETUP_CHRONIC");
    private final String PANEL_SETUP_NUTRITIONMAP = Constant.getTextBundle("�Ѻ����дѺ����ҡ��");//amp:26/04/2549
    private final String PANEL_SETUP_ORDER_ITEM_PRICE = Constant.getTextBundle("��˹��Ҥ� Order");//pu:04/08/2549
    private final String PANEL_SETUP_ORDER_ITEM_DOSE = Constant.getTextBundle("��˹� Dose ��");
    private final String GROUP_SETUP_ADMIN = Constant.getTextBundle("GROUP_SETUP_ADMIN");
    private final String GROUP_SETUP_SERVICE = Constant.getTextBundle("GROUP_SETUP_SERVICE");
    private final String GROUP_SETUP_ORDER = Constant.getTextBundle("GROUP_SETUP_ORDER");
    private final String GROUP_SETUP_LABXRAY = Constant.getTextBundle("GROUP_SETUP_LABXRAY");
    private final String GROUP_SETUP_TREAT = Constant.getTextBundle("GROUP_SETUP_TREAT");
    private final String GROUP_SETUP_DRUG = Constant.getTextBundle("GROUP_SETUP_DRUG");
    private final String GROUP_SETUP_PAY = Constant.getTextBundle("GROUP_SETUP_PAY");
    private final String GROUP_SETUP_OTHER = Constant.getTextBundle("GROUP_SETUP_OTHER");
    private final String GROUP_SETUP_ICD = Constant.getTextBundle("GROUP_SETUP_ICD");
    private final Icon ICONMain = new ImageIcon(getClass().getResource("/com/hospital_os/images/Host16.gif"));
    private final Icon ICON = new ImageIcon(getClass().getResource("/com/hospital_os/images/MainProgram16.gif"));
    private final DefaultMutableTreeNode c_admin = new DefaultMutableTreeNode(new IconData(ICON, GROUP_SETUP_ADMIN));
    private final DefaultMutableTreeNode c_serv = new DefaultMutableTreeNode(new IconData(ICON, GROUP_SETUP_SERVICE));
    private final DefaultMutableTreeNode c_order = new DefaultMutableTreeNode(new IconData(ICON, GROUP_SETUP_ORDER));
    private final DefaultMutableTreeNode c_labxray = new DefaultMutableTreeNode(new IconData(ICON, GROUP_SETUP_LABXRAY));
    private final DefaultMutableTreeNode c_treat = new DefaultMutableTreeNode(new IconData(ICON, GROUP_SETUP_TREAT));
    private final DefaultMutableTreeNode c_drug = new DefaultMutableTreeNode(new IconData(ICON, GROUP_SETUP_DRUG));
    private final DefaultMutableTreeNode c_icd = new DefaultMutableTreeNode(new IconData(ICON, GROUP_SETUP_ICD));
    private final DefaultMutableTreeNode c_pay = new DefaultMutableTreeNode(new IconData(ICON, GROUP_SETUP_PAY));
    private final DefaultMutableTreeNode c_other = new DefaultMutableTreeNode(new IconData(ICON, GROUP_SETUP_OTHER));
    private final DefaultMutableTreeNode c_insurance = new DefaultMutableTreeNode(new IconData(ICON, "��Сѹ���Ե"));
    private PanelSetupServicePoint panelSetupServicePoint;
    private PanelSetupItem panelSetupItem;
    private PanelSetupItemPrice panelSetupItemPrice;
    private PanelSetupVitalTemplate panelSetupVitalTemplate;
    private PanelSetupDoctorDrugSet panelSetupDoctorDrugSet;
    private PanelSDPlan panelSetupPlan;
    private PanelSetupPayment0 panelSetupPayment0;
    private PanelSDEmployee panelSetupEmployee;
    private PanelSetupGroupChronic panelSetupGroupChronic;
    private PanelSetupItemDose panelSetupItemDose;

    public HosPanelSetup(HosDialog hd, HosControl hc, UpdateStatus us) {
        setControl(hd, hc, us);
    }

    private void setControl(HosDialog hd, HosControl hc, UpdateStatus us) {
        theHO = hc.theHO;
        theHC = hc;
        theUS = us;
        theHD = hd;
        theHeadNode = new DefaultMutableTreeNode(new IconData(ICONMain, "Setup"));
        theTreeModel = new DefaultTreeModel(theHeadNode);
        theTreeNode = new DefaultMutableTreeNode(new IconData(ICONMain, "HospitalOS Setup"));
        theHeadNode.add(theTreeNode);
        thePanelCard = new JPanel();
        thePanelCard.setSize(400, 400);
        thePanelCard.setLayout(new CardLayout());
        initTreeModel();
    }

    public void refreshComboBox() {
        theHC.theLookupControl.refreshLookup();
        if (panelSetupDoctorDrugSet != null) {
            panelSetupDoctorDrugSet.setupLookup();
        }
        if (panelSetupEmployee != null) {
            panelSetupEmployee.setupLookup();
        }

        if (panelSetupItem != null) {
            panelSetupItem.setupLookup();
        }
        if (panelSetupItemPrice != null) {
            panelSetupItemPrice.setupLookup();
        }
        if (panelSetupPlan != null) {
            panelSetupPlan.setupLookup();
        }
        if (panelSetupPayment0 != null) {
            panelSetupPayment0.setupLookup();
        }
        if (panelSetupServicePoint != null) {
            panelSetupServicePoint.setupLookup();
        }
        if (panelSetupVitalTemplate != null) {
            panelSetupVitalTemplate.setupLookup();
        }
        theHC.theHS.theSetupSubject.notifyRefreshComboBox();
    }

    private void initTreeNodePS(DefaultMutableTreeNode category, PanelSetup jp, String pname) {
        if (pname.equals(PANEL_SETUP_OFFICE_INCUP)) {
            jp = new PanelSetupOfficeInCup(theHC, theUS);
            ((PanelSetupOfficeInCup) jp).setHosDialog(theHD);
        }
        jp.setControl(theHC, theUS);
        jp.setTitleLabel(pname);
        JPanel jp1 = (JPanel) jp;
        thePanelCard.add(jp1, pname);
        DefaultMutableTreeNode book = new DefaultMutableTreeNode(new IconData(ICONMain, pname));
        category.add(book);
    }

    private void initTreeNode(DefaultMutableTreeNode category, JPanel jp, String pname) {
        thePanelCard.add(jp, pname);
        DefaultMutableTreeNode book = new DefaultMutableTreeNode(new IconData(ICONMain, pname));
        category.add(book);
    }

    public boolean showModule(Vector vModule) {
        boolean ret = true;
        if (vModule == null) {
            return true;
        }
        for (int i = 0, size = vModule.size(); i < size; i++) {
            ModuleInfTool mi = (ModuleInfTool) vModule.get(i);
            try {
                if (!ReadModule.checkLicense(theHC, mi)) {
                    Version v = (Version) mi.getObjectVersion();
                    String msg = "����� " + v.appName + " �������ö��ҹ��\n���ͧ�ҡ��辺 License ����������ء����ҹ";
                    JOptionPane.showMessageDialog(theUS.getJFrame(), msg, "��Ǩ�ͺ License �����ҹ", JOptionPane.WARNING_MESSAGE);
                    continue;
                }
                mi.setHosControl(theHC);
                boolean enable = mi.isJTreeVisible(theHO.theEmployee.authentication_id);
                if (enable) {
                    Vector vSetupModule = mi.getVectorSetupModule();
                    int size1 = 0;
                    if (vSetupModule != null) {
                        size1 = vSetupModule.size();
                    }
                    for (int j = 0; j < size1; j++) {
                        SetupModule theSetupModule = (SetupModule) vSetupModule.get(j);
//                        if (size1 > 1) {
                        theHeadNode.add(theSetupModule.theMainTreeNode);
                        if (theSetupModule.theMainTreeNode != theSetupModule.theTreeNode) {
                            theSetupModule.theMainTreeNode.add(theSetupModule.theTreeNode);
                        }
                        theSetupModule.thePanel.setSize(400, 400);
                        thePanelCard.add(theSetupModule.pname, theSetupModule.thePanel);
                        DefaultMutableTreeNode book = new DefaultMutableTreeNode(
                                new IconData(ICONMain, theSetupModule.pname));
                        theSetupModule.theTreeNode.add(book);
//                        } else {
//                            initTreeNode(theHeadNode, theSetupModule.thePanel, theSetupModule.pname);
//                        }
                    }
                }
            } catch (Exception ex) {
                //��ͧ�ѹ��������� module ����˹��軡��
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
                ret = false;
            }
        }
        return ret;
    }

    public void initTreeModel() {
        String auth = theHO.theEmployee.authentication_id;
        boolean is_admin = auth.equals(Authentication.ADMIN);
        if (is_admin) {
            theTreeNode.add(c_admin);
            if (is_admin || theHO.theGActionAuthV.isReadTreeAdminOption()) {
                PanelSetupOption panelSetupOption = new PanelSetupOption(theHC, theUS);
                initTreeNodePS(c_admin, panelSetupOption, PANEL_SETUP_OPTION);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeAdminLanguage()) {
                PanelSetupXPer panel = new PanelSetupXPer(theHC, theUS, (PanelSetupImp) new PanelSDLanguage(theHC, theUS));
                panel.setActiveVisible();
                panel.setVisibleDeleteBtn(false);
                if (!theHO.theGActionAuthV.isWriteTreeAdminLanguage()) {
                    panel.setAuthenWritable(false);
                }
                initTreeNode(c_admin, panel, PanelSDLanguage.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeAdminOptionReport()) {
                PanelSetupOptionReport panelSetupOption = new PanelSetupOptionReport();
                panelSetupOption.setControl(theHC, theUS);
                initTreeNode(c_admin, panelSetupOption, PanelSetupOptionReport.TITLE);
            }

            if (is_admin || theHO.theGActionAuthV.isReadTreeAdminLock()) {
                PanelLockedPatient panelLockedPatient = new PanelLockedPatient(theHC, theUS);
                initTreeNodePS(c_admin, panelLockedPatient, PANEL_SETUP_FORLIFE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeAdminAuthen()) {
                PanelSetupGActionAuth panelSetupGActionAuth = new PanelSetupGActionAuth(theHC, theUS);
                initTreeNodePS(c_admin, panelSetupGActionAuth, PANEL_SETUP_GAA);
            }
        }
        if (is_admin || theHO.theGActionAuthV.isReadTreeServicePoint()) { //default false
            theTreeNode.add(c_serv);
            if (is_admin || theHO.theGActionAuthV.isReadTreeServicePointServicePoint()) {
                panelSetupServicePoint = new PanelSetupServicePoint(theHC, theUS);
                initTreeNodePS(c_serv, panelSetupServicePoint, PANEL_SETUP_SERVICE_POINT);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeServicePointWard()) {
                PanelSDWard psi = new PanelSDWard(theHC, theUS);
                PanelSetupXPer panelSetupWard = new PanelSetupXPer(theHC, theUS, psi);
                panelSetupWard.setActiveVisible();
                panelSetupWard.setVisibleDeleteBtn(false);
                initTreeNode(c_serv, panelSetupWard, PanelSDWard.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeServicePointClinic()) {
                PanelSDClinic psi = new PanelSDClinic(theHC, theUS);
                PanelSetupXPer panelSetupClinic = new PanelSetupXPer(theHC, theUS, psi);
                panelSetupClinic.setActiveVisible();
                initTreeNode(c_serv, panelSetupClinic, PanelSDClinic.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeServicePointOffice()) {
                PanelSDOffice psi = new PanelSDOffice(theHC, theUS);
                PanelSetupXPer panelSetupOffice = new PanelSetupXPer(theHC, theUS, psi);
                initTreeNodePS(c_serv, panelSetupOffice, PanelSDOffice.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeServicePointOffice()) {
                PanelSetupOfficeInCup panelSetupOfficeInCup = new PanelSetupOfficeInCup(theHC, theUS);
                initTreeNodePS(c_serv, panelSetupOfficeInCup, PANEL_SETUP_OFFICE_INCUP);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeServicePointQueue()) {
                PanelSDQueueVisit psi = new PanelSDQueueVisit(theHC, theUS);
                PanelSetupXPer panelSetupQueueVisit = new PanelSetupXPer(theHC, theUS, psi);
                panelSetupQueueVisit.setActiveVisible();
                initTreeNode(c_serv, panelSetupQueueVisit, PanelSDQueueVisit.TITLE);
            }
            if (is_admin) {
                PanelSDRangeAgeVisit psi = new PanelSDRangeAgeVisit(theHC, theUS);
                PanelSetupXPer panelSetupRangeAgeVisit = new PanelSetupXPer(theHC, theUS, psi);
                panelSetupRangeAgeVisit.setVisibleDeleteBtn(false);
                initTreeNode(c_serv, panelSetupRangeAgeVisit, PanelSDRangeAgeVisit.TITLE);
            }
        }
        if (is_admin || theHO.theGActionAuthV.isReadTreeGroupItem()) { //default true
            theTreeNode.add(c_order);
            if (is_admin || theHO.theGActionAuthV.isReadTreeGroupItemOrder()) {
                PanelSDOrderGroup psi = new PanelSDOrderGroup(theHC, theUS);
                PanelSetupXPer panelSetupOrderGroup = new PanelSetupXPer(theHC, theUS, psi);
                panelSetupOrderGroup.setActiveVisible();
                initTreeNode(c_order, panelSetupOrderGroup, PanelSDOrderGroup.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeGroupItemBill()) {
                PanelSDBill psi = new PanelSDBill(theHC, theUS);
                PanelSetupXPer panelSetupBill = new PanelSetupXPer(theHC, theUS, psi);
                panelSetupBill.setActiveVisible();
                initTreeNode(c_order, panelSetupBill, PanelSDBill.TITLE);

            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeGroupItemStandard()) {
                PanelSD16Group psi = new PanelSD16Group(theHC, theUS);
                PanelSetupXPer panelSetup16Group = new PanelSetupXPer(theHC, theUS, psi);
                panelSetup16Group.setActiveVisible();
                initTreeNode(c_order, panelSetup16Group, PanelSD16Group.TITLE);

            }
        }
        if (is_admin || theHO.theGActionAuthV.isReadTreeItem()) {
            theTreeNode.add(c_treat);
            if (is_admin || theHO.theGActionAuthV.isReadTreeItemOrder()) {
                if (panelSetupItem == null) {
                    panelSetupItem = new PanelSetupItem(theHC, theUS);
                }
                initTreeNode(c_treat, panelSetupItem, PANEL_SETUP_ORDER_ITEM);

                PanelSetupItemPackage panelSetupItemPackage = new PanelSetupItemPackage(theHC, theUS);
                initTreeNode(c_treat, panelSetupItemPackage, panelSetupItemPackage.TITLE);

                if (panelSetupItemPrice == null) {
                    panelSetupItemPrice = new PanelSetupItemPrice();
                    panelSetupItemPrice.setControl(theHC, theUS);
                }
                initTreeNode(c_treat, panelSetupItemPrice, PANEL_SETUP_ORDER_ITEM_PRICE);
                if (panelSetupItemDose == null) {
                    panelSetupItemDose = new PanelSetupItemDose(theHC, theUS);
                }
                initTreeNode(c_treat, panelSetupItemDose, PANEL_SETUP_ORDER_ITEM_DOSE);
                // 3.9.31
                PanelSDLabSpecimen psi = new PanelSDLabSpecimen(theHC, theUS);
                PanelSetupXPer panelSetupSpecimen = new PanelSetupXPer(theHC, theUS, psi);
                panelSetupSpecimen.setActiveVisible();
                initTreeNode(c_treat, panelSetupSpecimen, PanelSDLabSpecimen.TITLE);
                // 3.9.66
                PanelSDLabOrderCause psdloc = new PanelSDLabOrderCause(theHC, theUS);
                PanelSetupXPer per = new PanelSetupXPer(theHC, theUS, psdloc);
                per.setActiveVisible();
                initTreeNode(c_treat, per, PanelSDLabOrderCause.TITLE);
                // 3.9.32
                PanelItemMapDoctorFee pimdf = new PanelItemMapDoctorFee();
                pimdf.setControl(theHC);
                initTreeNode(c_treat, pimdf, PanelItemMapDoctorFee.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeItemOrderAuto()) {
                PanelSDAutoOrderItem psi = new PanelSDAutoOrderItem(theHC, theUS);
                PanelSetupXPer setupAutoOrderItem = new PanelSetupXPer(theHC, theUS, psi);
                setupAutoOrderItem.setActiveVisible();
                initTreeNode(c_treat, setupAutoOrderItem, PanelSDAutoOrderItem.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeItemVitalTemplate()) {
                panelSetupVitalTemplate = new PanelSetupVitalTemplate(theHC, theUS);
                initTreeNode(c_treat, panelSetupVitalTemplate, PANEL_SETUP_VITAL_TEMPLATE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeItemOrderSet()) {
                panelSetupDoctorDrugSet = new PanelSetupDoctorDrugSet(theHC, theUS);
                initTreeNode(c_treat, panelSetupDoctorDrugSet, PANEL_SETUP_DOCTOR_DRUGSET);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeDrugFavorite()) {
                PanelSetupDrugFavorite psif = new PanelSetupDrugFavorite();
                psif.setControl(theHC, theUS);
                initTreeNode(c_treat, psif, PanelSetupDrugFavorite.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeItemDxTemplate()) {
                PanelSDGuide psi = new PanelSDGuide(theHC, theUS);
                PanelSetupXPer PanelSetupGuide = new PanelSetupXPer(theHC, theUS, psi);
                PanelSetupGuide.setActiveVisible();
                initTreeNode(c_treat, PanelSetupGuide, PanelSDGuide.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isWriteTreeOpinionRecommendation()) {
                PanelSDOpinionRecommendation psi = new PanelSDOpinionRecommendation(theHC, theUS);
                PanelSetupXPer PanelSetupDxTemplate = new PanelSetupXPer(theHC, theUS, psi);
                PanelSetupDxTemplate.setActiveVisible();
                initTreeNode(c_treat, PanelSetupDxTemplate, PanelSDOpinionRecommendation.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeItemDxTemplate()) {
                PanelSDDxTemplate psi = new PanelSDDxTemplate(theHC, theUS);
                PanelSetupXPer PanelSetupDxTemplate = new PanelSetupXPer(theHC, theUS, psi);
                PanelSetupDxTemplate.setActiveVisible();
                initTreeNode(c_treat, PanelSetupDxTemplate, PanelSDDxTemplate.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeBodyOrgan()) {
                PanelSDBodyOrgan psi = new PanelSDBodyOrgan(theHC, theUS);
                PanelSetupXPer PanelSetupBodyOrgan = new PanelSetupXPer(theHC, theUS, psi);
                PanelSetupBodyOrgan.setActiveVisible();
                initTreeNode(c_treat, PanelSetupBodyOrgan, PanelSDBodyOrgan.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeNutritionMap()) {
                PanelSetupNutritionMap panel = new PanelSetupNutritionMap(theHC, theUS);
                initTreeNode(c_treat, panel, PANEL_SETUP_NUTRITIONMAP);
            }
        }
        if (is_admin || theHO.theGActionAuthV.isReadTreeItem()) {//default true
            theTreeNode.add(c_labxray);
            if (is_admin || theHO.theGActionAuthV.isReadTreeLabResultType()) {
                PanelSDResultListType psi = new PanelSDResultListType(theHC, theUS);
                PanelSetupXPer panelSetupResultListType = new PanelSetupXPer(theHC, theUS, psi);
                panelSetupResultListType.setActiveVisible();
                initTreeNode(c_labxray, panelSetupResultListType, PanelSDResultListType.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeItemXraySide()) {
                PanelSDXRayLeteral psi = new PanelSDXRayLeteral(theHC, theUS);
                PanelSetupXPer panelSetupXRayLeteral = new PanelSetupXPer(theHC, theUS, psi);
                panelSetupXRayLeteral.setActiveVisible();
                initTreeNode(c_labxray, panelSetupXRayLeteral, PanelSDXRayLeteral.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeItemXrayPosition()) {
                PanelSDXRayPosition psi = new PanelSDXRayPosition(theHC, theUS);
                PanelSetupXPer panelSetupXRayPosition = new PanelSetupXPer(theHC, theUS, psi);
                panelSetupXRayPosition.setActiveVisible();
                initTreeNode(c_labxray, panelSetupXRayPosition, PanelSDXRayPosition.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeItemXrayFilm()) {
                PanelSetupXRayFilmSize panel = new PanelSetupXRayFilmSize(theHC, theUS);
                initTreeNode(c_labxray, panel, PANEL_SETUP_XRAY_FILMSIZE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeItemXrayModality()) {
                PanelSDXRayModality psi = new PanelSDXRayModality(theHC, theUS);
                PanelSetupXPer panelSetupXRayPosition = new PanelSetupXPer(theHC, theUS, psi);
                panelSetupXRayPosition.setActiveVisible();
                initTreeNode(c_labxray, panelSetupXRayPosition, PanelSDXRayModality.TITLE);
            }
            if (is_admin) {
                PanelSetupMapLabTMLT panel = new PanelSetupMapLabTMLT();
                panel.setControl(theHC);
                initTreeNode(c_labxray, panel, PanelSetupMapLabTMLT.TITLE);
            }
        }
        if (is_admin || theHO.theGActionAuthV.isReadTreeDrugDetail()) {//default true
            theTreeNode.add(c_drug);
            if (is_admin || theHO.theGActionAuthV.isReadTreeDrugDetailFrequency()) {
                PanelSDDrugFrequency psi = new PanelSDDrugFrequency(theHC, theUS);
                PanelSetupXPer panelSetupDrugFrequency = new PanelSetupXPer(theHC, theUS, psi);
                panelSetupDrugFrequency.setActiveVisible();
                initTreeNode(c_drug, panelSetupDrugFrequency, PanelSDDrugFrequency.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeDrugDetailInstruction()) {
                PanelSDDrugInstruction psi = new PanelSDDrugInstruction(theHC, theUS);
                PanelSetupXPer panelSetupDrugInstruction = new PanelSetupXPer(theHC, theUS, psi);
                panelSetupDrugInstruction.setActiveVisible();
                initTreeNode(c_drug, panelSetupDrugInstruction, PanelSDDrugInstruction.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeDrugDetailUom()) {
                PanelSDUom2 psi = new PanelSDUom2(theHC, theUS);
                PanelSetupXPer panelSetupUom2 = new PanelSetupXPer(theHC, theUS, psi);
                panelSetupUom2.setActiveVisible();
                initTreeNode(c_drug, panelSetupUom2, PanelSDUom2.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeDrugDoseShortcut()) {
                PanelSDDrugDoseShortcut psi = new PanelSDDrugDoseShortcut(theHC, theUS);
                PanelSetupXPer panelSetupDrugDoseShortcut = new PanelSetupXPer(theHC, theUS, psi);
                panelSetupDrugDoseShortcut.setActiveVisible();
                initTreeNode(c_drug, panelSetupDrugDoseShortcut, PanelSDDrugDoseShortcut.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeDrugStandard()) {
                PanelSDDrugStandard psi = new PanelSDDrugStandard(theHC, theUS);
                PanelSetupXPer panelSetupDrugStandard = new PanelSetupXPer(theHC, theUS, psi);
                panelSetupDrugStandard.setActiveVisible();
                initTreeNode(c_drug, panelSetupDrugStandard, PanelSDDrugStandard.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeDrugStandardMapItem()) {
                PanelSetupDrugStandardMapItem panel = new PanelSetupDrugStandardMapItem(theHC, theUS);
                initTreeNode(c_drug, panel, panel.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeDrugInteraction()) {
                PanelSetupDrugInteraction panel = new PanelSetupDrugInteraction(theHC, theUS);
                initTreeNode(c_drug, panel, PanelSetupDrugInteraction.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeWarningInformIntolerance()) {
                PanelSetupMapWarningInformIntolerance panel = new PanelSetupMapWarningInformIntolerance();
                panel.setControl(theHC);
                initTreeNode(c_drug, panel, PanelSetupMapWarningInformIntolerance.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeWarningDrugAllergy()) {
                PanelSetupMapWarningDrugAllergy panel = new PanelSetupMapWarningDrugAllergy();
                panel.setControl(theHC);
                initTreeNode(c_drug, panel, PanelSetupMapWarningDrugAllergy.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeDrugMapG6pd()) {
                PanelItemDrugMapG6PD panel = new PanelItemDrugMapG6PD();
                panel.setControl(theHC);
                initTreeNode(c_drug, panel, PanelItemDrugMapG6PD.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeDrugMapUnit()) {
                PanelSetupMapDrugUnit panel = new PanelSetupMapDrugUnit();
                panel.setControl(theHC);
                initTreeNode(c_drug, panel, PanelSetupMapDrugUnit.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeDueType()) {
                PanelSetupDueType panel = new PanelSetupDueType();
                panel.setControl(theHC);
                initTreeNode(c_drug, panel, PanelSetupDueType.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeMapDue()) {
                PanelSetupMapDue panel = new PanelSetupMapDue();
                panel.setControl(theHC);
                initTreeNode(c_drug, panel, PanelSetupMapDue.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeMapNed()) {
                PanelSetupMapNED panel = new PanelSetupMapNED();
                panel.setControl(theHC);
                initTreeNode(c_drug, panel, PanelSetupMapNED.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeDrugDosageForm()) {
                PanelSetupDrugDosageForm panel = new PanelSetupDrugDosageForm();
                panel.setControl(theHC);
                initTreeNode(c_drug, panel, PanelSetupDrugDosageForm.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeDrugMapDrug()) {
                PanelSetupMapDrugAndSupplyWithProductCategory panel = new PanelSetupMapDrugAndSupplyWithProductCategory();
                panel.setControl(theHC);
                initTreeNode(c_drug, panel, PanelSetupMapDrugAndSupplyWithProductCategory.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeDrugMapStandardTMT()) {
                PanelSetupMapDrugTMT panel = new PanelSetupMapDrugTMT();
                panel.setControl(theHC);
                initTreeNode(c_drug, panel, PanelSetupMapDrugTMT.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeDrugMapDrug24()) {
                PanelSetupMapDrug24 panel = new PanelSetupMapDrug24();
                panel.setControl(theHC);
                initTreeNode(c_drug, panel, PanelSetupMapDrug24.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeDrugItemManufacturer()) {
                PanelSDDrugItemManufacturer psi = new PanelSDDrugItemManufacturer(theHC, theUS);
                PanelSetupXPer panel = new PanelSetupXPer(theHC, theUS, psi);
                panel.setActiveVisible();
                panel.setVisibleDeleteBtn(false);
                initTreeNode(c_drug, panel, PanelSDDrugItemManufacturer.TITLE);
            }

            if (is_admin || theHO.theGActionAuthV.isReadTreeDrugItemDistributor()) {
                PanelSDDrugItemDistributor psi = new PanelSDDrugItemDistributor(theHC, theUS);
                PanelSetupXPer panel = new PanelSetupXPer(theHC, theUS, psi);
                panel.setActiveVisible();
                panel.setVisibleDeleteBtn(false);
                initTreeNode(c_drug, panel, PanelSDDrugItemDistributor.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeMapOrderDoctor()) {
                PanelSetupMapOrderDoctor panel = new PanelSetupMapOrderDoctor();
                panel.setControl(theHC);
                initTreeNode(c_drug, panel, PanelSetupMapOrderDoctor.TITLE);
            }
        }
        if (is_admin || theHO.theGActionAuthV.isReadTreeIcd()) {//default true
            theTreeNode.add(c_icd);
            if (is_admin || theHO.theGActionAuthV.isReadTreeIcdIcd10()) {
                PanelSDICD10 psi = new PanelSDICD10(theHC, theUS);
                PanelSetupXPer panel = new PanelSetupXPer(theHC, theUS, psi);
                initTreeNode(c_icd, panel, PanelSDICD10.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeIcdIcd9()) {
                PanelSDICD9 psi = new PanelSDICD9(theHC, theUS);
                PanelSetupXPer panel = new PanelSetupXPer(theHC, theUS, psi);
                initTreeNode(c_icd, panel, PanelSDICD9.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeIcdChronic()) {
                panelSetupGroupChronic = new PanelSetupGroupChronic(theHC, theUS);
                initTreeNode(c_icd, panelSetupGroupChronic, PANEL_SETUP_CHRONIC);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeNutritionMap()) {
                PanelSDNCDGroup psi = new PanelSDNCDGroup(theHC, theUS);
                PanelSetupXPer setupNCDGroup = new PanelSetupXPer(theHC, theUS, psi);
                setupNCDGroup.setActiveVisible();
                initTreeNode(c_icd, setupNCDGroup, PanelSDNCDGroup.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeIcdGroupIcd10()) {
                PanelSDGroupIcd10 psi = new PanelSDGroupIcd10(theHC, theUS);
                PanelSetupXPer setupGroupIcd10 = new PanelSetupXPer(theHC, theUS, psi);
                setupGroupIcd10.setActiveVisible();
                initTreeNode(c_icd, setupGroupIcd10, PanelSDGroupIcd10.TITLE);
            }
        }
        if (is_admin || theHO.theGActionAuthV.isReadTreeBilling()) {//default true
            theTreeNode.add(c_pay);
            if (is_admin || theHO.theGActionAuthV.isReadTreeBillingPayer()) {
                PanelSDPayer psi = new PanelSDPayer(theHC, theUS);
                PanelSetupXPer panelSetupPayer = new PanelSetupXPer(theHC, theUS, psi);
                panelSetupPayer.setActiveVisible();
                initTreeNode(c_pay, panelSetupPayer, PanelSDPayer.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeBillingBankInfo()) {
                PanelSDBankInfo bankInfo = new PanelSDBankInfo(theHC, theUS);
                PanelSetupXPer panel2 = new PanelSetupXPer(theHC, theUS, bankInfo);
                panel2.setVisibleDeleteBtn(false);
                panel2.setActiveVisible();
                initTreeNode(c_pay, panel2, PanelSDBankInfo.TITLE);

                PanelSDCreditCardType cardType = new PanelSDCreditCardType(theHC, theUS);
                PanelSetupXPer setupXPer = new PanelSetupXPer(theHC, theUS, cardType);
                setupXPer.setVisibleDeleteBtn(false);
                setupXPer.setActiveVisible();
                initTreeNode(c_pay, setupXPer, PanelSDCreditCardType.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeBillingContract()) {
                PanelSDContract psi = new PanelSDContract(theHC, theUS);
                PanelSetupXPer panel = new PanelSetupXPer(theHC, theUS, psi);
                initTreeNode(c_pay, panel, PANEL_SETUP_CONTRACT);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeBillingPlan()) {
                PanelSDPlan psi = new PanelSDPlan(theHC, theUS);
                PanelSetupXPer panel = new PanelSetupXPer(theHC, theUS, psi);
                panel.setActiveVisible();
                initTreeNode(c_pay, panel, PanelSDPlan.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeBillingPlan()) {
                PanelSetupMapPlanGovOffical panel = new PanelSetupMapPlanGovOffical();
                panel.setControl(theHC);
                initTreeNode(c_pay, panel, PanelSetupMapPlanGovOffical.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeBillingPlan()) {
                PanelSetupMapPlanSocialsec panel = new PanelSetupMapPlanSocialsec();
                panel.setControl(theHC);
                initTreeNode(c_pay, panel, PanelSetupMapPlanSocialsec.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeBillingPlan()) {
                PanelSetupMapPlanClaimPrice panel = new PanelSetupMapPlanClaimPrice();
                panel.setControl(theHC);
                initTreeNode(c_pay, panel, PanelSetupMapPlanClaimPrice.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeBillingBehindhand()) {
                panelSetupPayment0 = new PanelSetupPayment0(theHC, theUS);
                initTreeNode(c_pay, panelSetupPayment0, PANEL_SETUP_REMAIN_0);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeItemSubgroupCheckBeforeCalulate()) {
                PanelItemSubgroupCheckBeforeCalulate panel = new PanelItemSubgroupCheckBeforeCalulate();
                panel.setControl(theHC);
                initTreeNode(c_pay, panel, PanelItemSubgroupCheckBeforeCalulate.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeBillingTariff()) {
                PanelSDSetupTariff psd = new PanelSDSetupTariff(theHC, theUS);
                PanelSetupXPer psx = new PanelSetupXPer(theHC, theUS, psd);
                psx.setVisibleDeleteBtn(false);
                psx.setActiveVisible();
                initTreeNode(c_pay, psx, PanelSDSetupTariff.TITLE);
            }
        }
        if (theHO.theGActionAuthV.isReadTreeInsurance()) {//default true
            theTreeNode.add(c_insurance);
            if (theHO.theGActionAuthV.isReadTreeInsuranceMapping()) {
                PanelInsuraceMapping panel = new PanelInsuraceMapping();
                panel.setControl(theHC);
                initTreeNode(c_insurance, panel, PanelInsuraceMapping.TITLE);
            }
            if (theHO.theGActionAuthV.isReadTreeInsuranceCompany()) {
                PanelSetupXPer panel = new PanelSetupXPer(theHC, theUS, (PanelSetupImp) new PanelSDInsuranceCompany(theHC, theUS));
                panel.setActiveVisible();
                panel.setVisibleDeleteBtn(false);
                if (!theHO.theGActionAuthV.isWriteTreeInsuranceCompany()) {
                    panel.setAuthenWritable(false);
                }
                initTreeNode(c_insurance, panel, PanelSDInsuranceCompany.TITLE);
            }
            if (theHO.theGActionAuthV.isReadTreeInsuranceDiscount()) {
                PanelSetupXPer panel = new PanelSetupXPer(theHC, theUS, (PanelSetupImp) new PanelSDInsuranceDiscount(theHC, theUS));
                panel.setActiveVisible();
                panel.setVisibleDeleteBtn(false);
                if (!theHO.theGActionAuthV.isWriteTreeInsuranceDiscount()) {
                    panel.setAuthenWritable(false);
                }
                initTreeNode(c_insurance, panel, PanelSDInsuranceDiscount.TITLE);
            }
        }
        if (is_admin || theHO.theGActionAuthV.isReadTreeOther()) {//default true
            theTreeNode.add(c_other);
            if (is_admin || theHO.theGActionAuthV.isReadTreeOtherUser()) {
                panelSetupEmployee = new PanelSDEmployee(theHC, theUS);
                panelSetupEmployee.setHosDialog(theHD);
                PanelSetupXPer panel = new PanelSetupXPer(theHC, theUS, panelSetupEmployee);
                panel.setActiveVisible();
                initTreeNode(c_other, panel, PanelSDEmployee.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeDefaultUserDeleteNotify()) {
                PanelDefaultUserDeleteNotify panel = new PanelDefaultUserDeleteNotify();//amp:14/03/2549
                panel.setControl(theHC);
                initTreeNode(c_other, panel, PanelDefaultUserDeleteNotify.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeOtherSite()) {
                PanelSetupSite panel = new PanelSetupSite(theHC, theUS);
                initTreeNode(c_other, panel, PANEL_SETUP_SITE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeOtherDayOff()) {
                PanelSetupDayOff panel = new PanelSetupDayOff();
                panel.setControl(theHC);
                initTreeNode(c_other, panel, PanelSetupDayOff.TITLE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeOtherSequence()) {
                PanelSDSequenceData psi = new PanelSDSequenceData(theHC, theUS);
                PanelSetupXPer panel = new PanelSetupXPer(theHC, theUS, psi);
                panel.setVisibleDeleteBtn(false);
                panel.setVisibleAddBtn(false);
                initTreeNode(c_other, panel, PanelSDSequenceData.TITLE);
            }
            if (is_admin && theHC.theLookupControl.readOption().receipt_sequance.equals(Active.isEnable())) {
                PanelSetupRecieptSequance psi = new PanelSetupRecieptSequance();
                psi.setControl(theHC, theUS);
                initTreeNode(c_other, psi, PANEL_SETUP_RECEIPT_SEQUANCE);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeNews()) {
                PanelNews psi = new PanelNews();
                psi.setControl(theHC.theSystemControl, theUS);
                initTreeNode(c_other, psi, PANEL_SETUP_NEWS);
            }
            if (is_admin || theHO.theGActionAuthV.isReadTreeRESTFulUrl()) {
                PanelSDRestfulUrl psi = new PanelSDRestfulUrl();
                psi.setControl(theHC, theUS);
                PanelSetupXPer panel = new PanelSetupXPer(theHC, theUS, psi);
                initTreeNode(c_other, panel, PanelSDRestfulUrl.TITLE);
            }
            if (is_admin) {
                PanelSetupNewsPewsNotify panelSetupLineNotifyToken = new PanelSetupNewsPewsNotify();
                panelSetupLineNotifyToken.setControl(theHC, theUS);
                initTreeNode(c_other, panelSetupLineNotifyToken, PanelSetupNewsPewsNotify.TITLE);
            }
            if (is_admin) {
                PanelSDModuleLicense psi = new PanelSDModuleLicense();
                psi.setControl(theHC, theUS);
                PanelSetupXPer panel = new PanelSetupXPer(theHC, theUS, psi);
                initTreeNode(c_other, panel, PanelSDModuleLicense.TITLE);
            }
        }
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.CommonInf;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class FPatientFamilyHistory implements CommonInf {
    
    public int f_patient_family_history_id = -1;
    public String description = "";

    @Override
    public String getCode() {
        return String.valueOf(f_patient_family_history_id);
    }

    @Override
    public String getName() {
        return description;
    }

    @Override
    public String toString() {
        return description;
    }
}

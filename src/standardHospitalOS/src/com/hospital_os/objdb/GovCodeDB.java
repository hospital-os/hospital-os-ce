/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.GovCode;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class GovCodeDB {

    private final ConnectionInf connectionInf;

    public GovCodeDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public GovCode select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_govcode where f_govcode_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<GovCode> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<GovCode> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_govcode where active = '1' order by f_govcode_id";
            preparedStatement = connectionInf.ePQuery(sql);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<GovCode> list(String... ids) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_govcode where f_govcode_id in (%s) and active = '1' order by f_govcode_id";
            StringBuilder sb = new StringBuilder();
            for (String id : ids) {
                sb.append("'").append(id).append("',");
            }
            preparedStatement = connectionInf.ePQuery(String.format(sql,
                    sb.toString().substring(0, sb.toString().length() - 1)));
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<GovCode> listByKeyword(String keyword) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_govcode where upper(description) like upper(?) and active = '1' order by description";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, "%" + (keyword == null || keyword.isEmpty() ? "" : keyword) + "%");
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<GovCode> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<GovCode> list = new ArrayList<GovCode>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                GovCode obj = new GovCode();
                obj.setObjectId(rs.getString("f_govcode_id"));
                obj.description = rs.getString("description");
                obj.active = rs.getString("active");
                obj.maininscl = rs.getString("maininscl");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (GovCode obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }

    public List<CommonInf> getComboboxDatasource(String... ids) throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (GovCode obj : list(ids)) {
            list.add((CommonInf) obj);
        }
        return list;
    }
}

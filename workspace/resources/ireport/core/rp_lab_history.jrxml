<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="rp_lab_history" language="groovy" pageWidth="595" pageHeight="842" columnWidth="539" leftMargin="28" rightMargin="28" topMargin="28" bottomMargin="28" uuid="7676d4fd-fbd2-4a61-ae30-34390346d484">
	<property name="ireport.zoom" value="2.0"/>
	<property name="ireport.x" value="195"/>
	<property name="ireport.y" value="0"/>
	<parameter name="employee_id" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="keyword" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="start_date" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="end_date" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="patient_id" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[select
        t_patient.patient_hn as hn
        ,f_patient_prefix.patient_prefix_description||t_patient.patient_firstname||'   '||t_patient.patient_lastname as name
        ,substring(' ' ||age(to_date(substr(t_patient.patient_birthday,1,10),'YYYY-MM-DD') - interval '543 year') from '(...)year') as age
        ,order_lab.t_order_id
        ,order_lab.order_exec_date_time
        ,order_lab.order_executed_date_time
        ,order_lab.order_common_name
        ,order_lab.lab_name
        ,order_lab.lab_index
        ,order_lab.result_lab_value
        ,order_lab.lab_min_max

        ,prefix_person.patient_prefix_description || t_person.person_firstname || ' ' || t_person.person_lastname as employee

from t_patient left join
               ( select
                                    t_order.t_patient_id as t_patient_id
                                    ,t_order.t_order_id as t_order_id
                                    ,t_order.order_executed_date_time as order_exec_date_time ,substr(t_order.order_executed_date_time,9,2)||'/'||substr(t_order.order_executed_date_time,6,2)||'/'||substr(t_order.order_executed_date_time,1,4)
                                            ||substr(t_order.order_executed_date_time,11,6) as order_executed_date_time
                                    ,t_order.order_common_name as order_common_name
                                    ,t_result_lab.result_lab_name as lab_name
                                    ,t_result_lab.result_lab_index as lab_index
                                    ,t_result_lab.result_lab_value||'  '||t_result_lab.result_lab_unit as result_lab_value
                                    ,t_result_lab.result_lab_min||' - '||t_result_lab.result_lab_max as lab_min_max
                from
                         t_order left join t_result_lab on t_order.t_order_id = t_result_lab.t_order_id
                where
                        t_order.f_order_status_id not in ('0','3')
                        and substr(t_order.order_executed_date_time,1,10) between $P{start_date} and  $P{end_date}
                        and ( t_order.order_common_name ilike '%$P!{keyword}%'  or t_result_lab.result_lab_name ilike '%$P!{keyword}%' )
                        and t_result_lab.result_lab_active = '1'
               ) as order_lab on t_patient.t_patient_id = order_lab.t_patient_id

        left join f_patient_prefix on t_patient.f_patient_prefix_id = f_patient_prefix.f_patient_prefix_id
        cross join b_employee
        inner join t_person on t_person.t_person_id = b_employee.t_person_id
        left join f_patient_prefix as prefix_person on prefix_person.f_patient_prefix_id = t_person.f_prefix_id
where
        t_patient.t_patient_id = $P{patient_id}
        and b_employee.b_employee_id = $P{employee_id}
order by
	order_exec_date_time desc
	,order_common_name asc
	,lab_index asc]]>
	</queryString>
	<field name="hn" class="java.lang.String"/>
	<field name="name" class="java.lang.String"/>
	<field name="age" class="java.lang.String"/>
	<field name="t_order_id" class="java.lang.String"/>
	<field name="order_executed_date_time" class="java.lang.String"/>
	<field name="order_common_name" class="java.lang.String"/>
	<field name="lab_name" class="java.lang.String"/>
	<field name="lab_index" class="java.lang.String"/>
	<field name="result_lab_value" class="java.lang.String"/>
	<field name="lab_min_max" class="java.lang.String"/>
	<field name="employee" class="java.lang.String"/>
	<title>
		<band height="109" splitType="Stretch">
			<staticText>
				<reportElement uuid="163fcf22-1915-4817-916c-5b95698e8418" x="150" y="0" width="238" height="30"/>
				<textElement textAlignment="Center">
					<font fontName="TH SarabunPSK" size="24" isBold="true"/>
				</textElement>
				<text><![CDATA[ประวัติการตรวจรักษาของแลป]]></text>
			</staticText>
			<textField pattern="###0.00">
				<reportElement uuid="5726141e-8d7d-4755-955f-3f4497b13882" x="84" y="30" width="371" height="20"/>
				<textElement textAlignment="Center">
					<font fontName="TH SarabunPSK" size="16"/>
				</textElement>
				<textFieldExpression><![CDATA["ตั้งแต่วันที่ "+
new java.text.SimpleDateFormat("dd/MM/yyyy", java.util.Locale.US).format(new java.text.SimpleDateFormat("yyyy-MM-dd",java.util.Locale.US).parse($P{start_date}))
+" ถึงวันที่ "+
new java.text.SimpleDateFormat("dd/MM/yyyy", java.util.Locale.US).format(new java.text.SimpleDateFormat("yyyy-MM-dd",java.util.Locale.US).parse($P{end_date}))]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement uuid="fc1531ff-c938-4e65-9277-8cd16cbbc9e0" x="24" y="63" width="25" height="20"/>
				<textElement>
					<font fontName="TH SarabunPSK" size="16"/>
				</textElement>
				<text><![CDATA[HN :]]></text>
			</staticText>
			<componentElement>
				<reportElement uuid="e1e60189-5c9c-4667-8895-4d140e40d75e" x="49" y="52" width="127" height="50"/>
				<jr:Code39 xmlns:jr="http://jasperreports.sourceforge.net/jasperreports/components" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports/components http://jasperreports.sourceforge.net/xsd/components.xsd" textPosition="bottom">
					<jr:codeExpression><![CDATA[$F{hn}]]></jr:codeExpression>
				</jr:Code39>
			</componentElement>
			<staticText>
				<reportElement uuid="8f62dc84-6952-45ac-97a7-132ca4e86364" x="190" y="63" width="45" height="20"/>
				<textElement>
					<font fontName="TH SarabunPSK" size="16"/>
				</textElement>
				<text><![CDATA[ชื่อ - สกุล]]></text>
			</staticText>
			<textField>
				<reportElement uuid="a18a2d2b-61ab-4ef6-b095-1b4dfbc9a8ec" x="235" y="63" width="199" height="20"/>
				<textElement>
					<font fontName="TH SarabunPSK" size="16"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{name}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement uuid="25894dbc-fe9d-4edd-bd00-d30ab3cc8a58" x="449" y="63" width="26" height="20"/>
				<textElement>
					<font fontName="TH SarabunPSK" size="16"/>
				</textElement>
				<text><![CDATA[อายุ]]></text>
			</staticText>
			<textField>
				<reportElement uuid="0243f174-4ba5-4b32-8b0e-3019ed3a4a19" x="475" y="63" width="65" height="20"/>
				<textElement>
					<font fontName="TH SarabunPSK" size="16"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{age}+" ปี"]]></textFieldExpression>
			</textField>
		</band>
	</title>
	<columnHeader>
		<band height="20" splitType="Stretch">
			<staticText>
				<reportElement uuid="83d68e91-831c-4554-b790-7bba7985403b" x="2" y="0" width="92" height="20"/>
				<textElement textAlignment="Center">
					<font fontName="TH SarabunPSK" size="16" isBold="true"/>
				</textElement>
				<text><![CDATA[วันที่ เวลา]]></text>
			</staticText>
			<line>
				<reportElement uuid="6dfd9372-292d-48ec-95bd-66ed92ee71c7" x="0" y="19" width="539" height="1"/>
			</line>
			<line>
				<reportElement uuid="c0131f13-04df-4235-a64f-7d444bab9fc4" x="0" y="0" width="539" height="1"/>
			</line>
			<staticText>
				<reportElement uuid="b3d5ce76-c8d3-43a5-aa33-664f2c1bd34e" x="96" y="0" width="139" height="20"/>
				<textElement textAlignment="Center">
					<font fontName="TH SarabunPSK" size="16" isBold="true"/>
				</textElement>
				<text><![CDATA[แลป]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="745b232b-5156-496b-aafa-9c14b1bd9bfe" x="235" y="0" width="120" height="20"/>
				<textElement textAlignment="Center">
					<font fontName="TH SarabunPSK" size="16" isBold="true"/>
				</textElement>
				<text><![CDATA[เทส]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="4b517ca8-7c59-45a1-88a5-d0e0c08f5168" x="355" y="0" width="95" height="20"/>
				<textElement textAlignment="Center">
					<font fontName="TH SarabunPSK" size="16" isBold="true"/>
				</textElement>
				<text><![CDATA[ผลแลป]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="939a5d2c-006c-418f-8dce-0194f27c24a6" x="449" y="0" width="90" height="20"/>
				<textElement textAlignment="Center">
					<font fontName="TH SarabunPSK" size="16" isBold="true"/>
				</textElement>
				<text><![CDATA[ค่าปกติ]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="20" splitType="Stretch">
			<textField isBlankWhenNull="true">
				<reportElement uuid="42c541d8-d6a0-4143-9870-5d9d6b17a56e" x="2" y="0" width="92" height="20"/>
				<textElement textAlignment="Center">
					<font fontName="TH SarabunPSK" size="16"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{order_executed_date_time}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="04a6fb5e-e7a5-4359-b65c-a20094db6359" x="96" y="0" width="139" height="20"/>
				<textElement>
					<font fontName="TH SarabunPSK" size="16"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{order_common_name}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="090db68e-ece7-45e2-82d8-9207bb0b57ee" x="235" y="0" width="120" height="20"/>
				<textElement>
					<font fontName="TH SarabunPSK" size="16"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{lab_name}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement uuid="44170e0b-fa20-4f85-8fcd-609d5f37bc92" x="355" y="0" width="95" height="20"/>
				<textElement>
					<font fontName="TH SarabunPSK" size="16"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{result_lab_value}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement uuid="87eab6b1-c5a7-46b3-86d9-3266c2d79232" x="449" y="0" width="90" height="20"/>
				<textElement>
					<font fontName="TH SarabunPSK" size="16"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{lab_min_max}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<pageFooter>
		<band height="31" splitType="Stretch">
			<textField>
				<reportElement uuid="60277935-9ed4-45ea-b94d-f96376c5dd54" x="3" y="2" width="204" height="20"/>
				<textElement>
					<font fontName="TH SarabunPSK" size="16"/>
				</textElement>
				<textFieldExpression><![CDATA["ผู้พิมพ์ "+$F{employee}]]></textFieldExpression>
			</textField>
			<textField pattern="วันที่พิมพ์ dd/MM/yyyy">
				<reportElement uuid="a0db3a33-a648-4ff1-b6e0-90843f2aa009" x="207" y="2" width="125" height="20"/>
				<textElement textAlignment="Center">
					<font fontName="TH SarabunPSK" size="16"/>
				</textElement>
				<textFieldExpression><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="10f13278-0e5e-4c6a-99d2-e5226d0d86e9" x="434" y="2" width="85" height="20"/>
				<textElement textAlignment="Right">
					<font fontName="TH SarabunPSK" size="16"/>
				</textElement>
				<textFieldExpression><![CDATA["Page  "+$V{PAGE_NUMBER}+" of "]]></textFieldExpression>
			</textField>
			<line>
				<reportElement uuid="62222884-bcde-40bd-9cfe-a9b8cfe0a5c2" x="0" y="0" width="539" height="1"/>
			</line>
			<textField evaluationTime="Report">
				<reportElement uuid="3629f79a-e3d4-426a-913a-c2a1880b7176" x="519" y="2" width="20" height="20"/>
				<textElement>
					<font fontName="TH SarabunPSK" size="16"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
</jasperReport>

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.objdb.objdbclass;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.pcu.object.FRehabilitationDevice;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sompr
 */
public class FRehabilitationDeviceDB {

    private final ConnectionInf connectionInf;

    public FRehabilitationDeviceDB(ConnectionInf db) {
        connectionInf = db;
    }

    public FRehabilitationDevice select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_rehabilitation_device where f_rehabilitation_device_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<FRehabilitationDevice> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<FRehabilitationDevice> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_rehabilitation_device order by f_rehabilitation_device_id";
            preparedStatement = connectionInf.ePQuery(sql);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<FRehabilitationDevice> list(String... ids) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_rehabilitation_device where f_rehabilitation_device_id in (?) order by f_rehabilitation_device_id";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setArray(1, connectionInf.getConnection().createArrayOf("varchar", ids));
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<FRehabilitationDevice> listByKeyword(String keyword) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_rehabilitation_device where f_rehabilitation_device.f_rehabilitation_device_id||' : '||f_rehabilitation_device.description ilike ? order by description";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, "%" + (keyword == null || keyword.isEmpty() ? "" : keyword) + "%");
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<FRehabilitationDevice> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<FRehabilitationDevice> list = new ArrayList<FRehabilitationDevice>();
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()) {
            FRehabilitationDevice obj = new FRehabilitationDevice();
            obj.setObjectId(rs.getString("f_rehabilitation_device_id"));
            obj.description = rs.getString("description");
            list.add(obj);
        }
        rs.close();
        return list;
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (FRehabilitationDevice obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }

    public List<CommonInf> getComboboxDatasource(String... ids) throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (FRehabilitationDevice obj : list(ids)) {
            list.add((CommonInf) obj);
        }
        return list;
    }
}

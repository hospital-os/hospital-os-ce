/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.objdb.objdbclass;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.ComboFix;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tanakrit
 */
public class EpidemCovidSymtomTypeDB {

    private final ConnectionInf connectionInf;

    public EpidemCovidSymtomTypeDB(ConnectionInf db) {
        connectionInf = db;
    }

    public List<ComboFix> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_epidem_covid_symptom_type order by f_epidem_covid_symptom_type_id";
            preparedStatement = connectionInf.ePQuery(sql);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<ComboFix> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<ComboFix> list = new ArrayList<>();
        try (ResultSet rs = preparedStatement.executeQuery()) {
            while (rs.next()) {
                ComboFix obj = new ComboFix();
                obj.code = rs.getString("f_epidem_covid_symptom_type_id");
                obj.name = rs.getString("epidem_covid_symptom_type_name");
                list.add(obj);
            }
        }
        return list;
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<>();
        for (ComboFix obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }

}

/*
 * ControlThread.java
 *
 * Created on 16 �á�Ҥ� 2550, 8:54 �.
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package com.hosv3.control.thread;

import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hosv3.control.HosControl;
import com.hosv3.control.HosDB;
import com.hosv3.object.HosObject;

/**
 *
 * @author Aut
 */
public abstract class ControlThread extends Thread {

    protected ConnectionInf theConnectionInf;
    protected HosDB theHosDB;
    protected HosObject theHO;
    protected UpdateStatus theUS;
    protected HosControl hosControl;

    /**
     * Creates a new instance of ControlThread
     */
    public ControlThread() {
    }
    
    public void setHosControl(HosControl hosControl){
        this.hosControl = hosControl;
    }

    public abstract void setControl(ConnectionInf con, HosDB hdb, HosObject ho, UpdateStatus us, Object control);

    protected abstract void runTask();

    @Override
    public void run() {
        runTask();
    }
}

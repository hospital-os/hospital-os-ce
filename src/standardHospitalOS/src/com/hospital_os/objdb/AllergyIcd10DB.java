/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.AllergyIcd10;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class AllergyIcd10DB {

    private final ConnectionInf connectionInf;

    public AllergyIcd10DB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public AllergyIcd10 select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_allergy_icd10 where f_allergy_icd10_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<AllergyIcd10> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<AllergyIcd10> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_allergy_icd10 order by allergy_icd10_description";
            preparedStatement = connectionInf.ePQuery(sql);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<AllergyIcd10> listByKeyword(String keyword) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_allergy_icd10 where allergy_icd10_number ilike ? or allergy_icd10_description ilike ? order by allergy_icd10_description";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, "%" + (keyword == null || keyword.isEmpty() ? "" : keyword) + "%");
            preparedStatement.setString(2, "%" + (keyword == null || keyword.isEmpty() ? "" : keyword) + "%");
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<AllergyIcd10> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<AllergyIcd10> list = new ArrayList<AllergyIcd10>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                AllergyIcd10 obj = new AllergyIcd10();
                obj.setObjectId(rs.getString("f_allergy_icd10_id"));
                obj.allergy_icd10_number = rs.getString("allergy_icd10_number");
                obj.description = rs.getString("allergy_icd10_description");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (AllergyIcd10 obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }
}

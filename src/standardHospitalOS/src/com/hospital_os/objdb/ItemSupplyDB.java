/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.ItemSupply;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class ItemSupplyDB {

    private ConnectionInf theConnectionInf;
    final private String idtable = "899";

    public ItemSupplyDB(ConnectionInf theConnectionInf) {
        this.theConnectionInf = theConnectionInf;
    }

    public int insert(ItemSupply p) throws Exception {
        p.generateOID(idtable);
        String sql = "insert into b_item_supply ("
                + " b_item_supply_id,"
                + " b_item_id,"
                + " item_supply_printable,"
                + " print_mar_type,"
                + " supplement_label,"
                + " user_record_id,"
                + " record_date_time,"
                + " user_update_id,"
                + " update_date_time"
                + " ) values ('"
                + p.getObjectId()
                + "','" + p.b_item_id
                + "','" + p.item_supply_printable
                + "','" + p.print_mar_type
                + "','" + p.supplement_label
                + "','" + p.user_record_id
                + "','" + p.record_date_time
                + "','" + p.user_update_id
                + "','" + p.update_date_time
                + "')";
        return theConnectionInf.eUpdate(sql);
    }

    public int update(ItemSupply p) throws Exception {
        String sql = "update b_item_supply set "
                + "item_supply_printable = '" + p.item_supply_printable
                + "', print_mar_type = '" + p.print_mar_type
                + "', supplement_label = '" + p.supplement_label
                + "', user_update_id = '" + p.user_update_id
                + "', update_date_time = '" + p.update_date_time
                + "' where b_item_supply_id = '" + p.getObjectId() + "'";
        return theConnectionInf.eUpdate(sql);
    }

    public int delete(String itemId) throws Exception {
        String sql = "delete from b_item_supply "
                + "where b_item_id = '" + itemId + "'";
        return theConnectionInf.eUpdate(sql);
    }

    public ItemSupply selectByItemId(String itemId) throws Exception {
        String sql = "select * from b_item_supply "
                + "where b_item_id = '" + itemId + "'";
        List<ItemSupply> list = eQuery(sql);
        return list.isEmpty() ? null : list.get(0);
    }

    private List<ItemSupply> eQuery(String sql) throws Exception {
        List<ItemSupply> list = new ArrayList<>();
        try (ResultSet rs = theConnectionInf.eQuery(sql)) {
            while (rs.next()) {
                ItemSupply p = new ItemSupply();
                p.setObjectId(rs.getString("b_item_supply_id"));
                p.b_item_id = rs.getString("b_item_id");
                p.item_supply_printable = rs.getString("item_supply_printable");
                p.print_mar_type = rs.getString("print_mar_type");
                p.supplement_label = rs.getString("supplement_label");
                p.user_record_id = rs.getString("user_record_id");
                p.record_date_time = rs.getString("record_date_time");
                p.user_update_id = rs.getString("user_update_id");
                p.update_date_time = rs.getString("update_date_time");
                list.add(p);
            }
        }
        return list;
    }
}

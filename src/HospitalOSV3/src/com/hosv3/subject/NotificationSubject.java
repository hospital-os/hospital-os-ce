/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.subject;

import com.hosv3.usecase.transaction.NotificationObserver;

/**
 *
 * @author Somprasong
 */
public interface NotificationSubject {

    public void addObserver(NotificationObserver o);

    public void removeObserver(NotificationObserver o);
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.CardiovascularType;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tanakrit
 */
public class CardiovascularTypeDB {

    public ConnectionInf theConnectionInf;

    public CardiovascularTypeDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public List<CardiovascularType> selectAll() throws Exception {
        String sql = "select * from f_cardiovascular_type";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            return executeQuery(ePQuery);
        }
    }

    public CardiovascularType selectByPK(String pkId) throws Exception {
        String sql = "select * from f_cardiovascular_type \n"
                + " where f_cardiovascular_type_id = ?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, pkId);
            List<CardiovascularType> v = executeQuery(ePQuery);
            return (CardiovascularType) (v.isEmpty() ? null : v.get(0));
        }
    }

    public List<CardiovascularType> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<CardiovascularType> list = new ArrayList<>();
        try (ResultSet rs = preparedStatement.executeQuery()) {
            while (rs.next()) {
                CardiovascularType p = new CardiovascularType();
                p.setObjectId(rs.getString("f_cardiovascular_type_id"));
                p.description = rs.getString("description");
                p.cap_score = rs.getInt("cap_score");
                list.add(p);
            }
            return list;
        }
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.G6PD;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.Gutil;
import java.sql.ResultSet;
import java.util.Vector;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class G6PDDB {

    private final ConnectionInf theConnectionInf;
    private final String tableId = "991";

    public G6PDDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public int insert(G6PD obj) throws Exception {
        String sql = "INSERT INTO t_g_6_pd( "
                + "t_g_6_pd_id, t_patient_id, g_6_pd,  "
                + "user_record, record_date_time, user_modify, modify_date_time, active) "
                + "VALUES ('%s', '%s', '%s', "
                + "'%s', '%s', '%s', '%s', '%s')";
        sql = String.format(sql, obj.getGenID(tableId), obj.t_patient_id,
                obj.g_6_pd,
                obj.user_record, obj.record_date_time,
                obj.user_modify, obj.modify_date_time, "1");
        return theConnectionInf.eUpdate(sql);
    }

    public int update(G6PD obj) throws Exception {
        String sql = "UPDATE t_g_6_pd "
                + "SET g_6_pd='%s',  "
                + "user_modify='%s', modify_date_time='%s' "
                + "WHERE t_g_6_pd_id='%s' ";
        sql = String.format(sql, 
                obj.g_6_pd,
                obj.user_modify, obj.modify_date_time, obj.getObjectId());
        return theConnectionInf.eUpdate(sql);
    }

    public G6PD selectById(String id) throws Exception {
        String sql = "select * from t_g_6_pd where t_g_6_pd_id = '%s'";
        Vector eQuery = eQuery(String.format(sql, id));
        return (G6PD) (eQuery.isEmpty() ? null : eQuery.get(0));
    }

    public G6PD selectByPatientId(String patient_id) throws Exception {
        String sql = "select * from t_g_6_pd where t_patient_id = '%s'";
        Vector eQuery = eQuery(String.format(sql, patient_id));
        return (G6PD) (eQuery.isEmpty() ? null : eQuery.get(0));
    }

    public Vector eQuery(String sql) throws Exception {
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        while (rs.next()) {
            G6PD p = new G6PD();
            p.setObjectId(rs.getString("t_g_6_pd_id"));
            p.t_patient_id = rs.getString("t_patient_id");
            p.g_6_pd = rs.getString("g_6_pd");
            p.user_record = rs.getString("user_record");
            p.record_date_time = rs.getString("record_date_time");
            p.user_modify = rs.getString("user_modify");
            p.modify_date_time = rs.getString("modify_date_time");
            p.active = rs.getString("active");
            list.add(p);
        }
        rs.close();
        return list;
    }
}

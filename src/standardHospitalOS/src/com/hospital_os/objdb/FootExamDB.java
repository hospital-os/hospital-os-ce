/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.FootExam;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.Gutil;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class FootExamDB {

    public ConnectionInf theConnectionInf;
    final public String idtable = "997";

    public FootExamDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public int insert(FootExam o) throws Exception {
        o.generateOID(idtable);
        String sql = "INSERT INTO t_foot_exam("
                + "t_foot_exam_id, t_visit_id, foot_ulcers, foot_ulcers_detail, "
                + "lose_sensation, shoes_regularly, shoes_regularly_other_detail, "
                + "active, record_date_time, user_record_id, update_date_time, user_update_id)"
                + "VALUES ('%s', '%s', '%s', '%s', "
                + "'%s', '%s', '%s', "
                + "'%s', '%s', '%s', '%s', '%s')";
        Object[] values = new Object[]{
            o.getObjectId(), o.t_visit_id, o.foot_ulcers, Gutil.CheckReservedWords(o.foot_ulcers_detail),
            o.lose_sensation, o.shoes_regularly, Gutil.CheckReservedWords(o.shoes_regularly_other_detail),
            o.active, o.record_date_time, o.user_record_id, o.update_date_time,
            o.user_update_id};
        return theConnectionInf.eUpdate(String.format(sql, values));
    }

    public int update(FootExam o) throws Exception {
        String sql = "UPDATE t_foot_exam "
                + "SET foot_ulcers='%s', foot_ulcers_detail='%s', "
                + "lose_sensation='%s', shoes_regularly='%s', shoes_regularly_other_detail='%s', "
                + "update_date_time='%s',user_update_id='%s' "
                + "WHERE t_foot_exam_id='%s'";
        Object[] values = new Object[]{
            o.foot_ulcers, Gutil.CheckReservedWords(o.foot_ulcers_detail),
            o.lose_sensation, o.shoes_regularly, Gutil.CheckReservedWords(o.shoes_regularly_other_detail),
            o.update_date_time, o.user_update_id,
            o.getObjectId()};
        return theConnectionInf.eUpdate(String.format(sql, values));

    }

    public int delete(FootExam o) throws Exception {
        String sql = "UPDATE t_foot_exam "
                + "SET active='0',update_date_time='%s',user_update_id='%s' "
                + "WHERE t_foot_exam_id='%s'";
        Object[] values = new Object[]{o.update_date_time,
            o.user_update_id, o.getObjectId()};
        return theConnectionInf.eUpdate(String.format(sql, values));
    }

    public FootExam findByVisitId(String visitId) throws Exception {
        String sql = "select * from t_foot_exam where t_visit_id = '%s' and active = '1'";
        List<FootExam> list = eQuery(String.format(sql, visitId));
        return list.isEmpty() ? null : list.get(0);
    }

    private List<FootExam> eQuery(String sql) throws Exception {
        List<FootExam> list = new ArrayList<FootExam>();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        while (rs.next()) {
            FootExam o = new FootExam();
            o.setObjectId(rs.getString("t_foot_exam_id"));
            o.t_visit_id = rs.getString("t_visit_id");
            o.foot_ulcers = rs.getString("foot_ulcers");
            o.foot_ulcers_detail = rs.getString("foot_ulcers_detail");
            o.lose_sensation = rs.getString("lose_sensation");
            o.shoes_regularly = rs.getString("shoes_regularly");
            o.shoes_regularly_other_detail = rs.getString("shoes_regularly_other_detail");
            o.active = rs.getString("active");
            o.record_date_time = rs.getString("record_date_time");
            o.user_record_id = rs.getString("user_record_id");
            o.update_date_time = rs.getString("update_date_time");
            o.user_update_id = rs.getString("user_update_id");
            list.add(o);
        }
        rs.close();
        return list;
    }
}

package com.hosv3.objdb;

import com.hospital_os.objdb.BillingDB;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.util.Vector;

@SuppressWarnings("UseOfObsoleteCollectionType")
public class Billing2DB extends BillingDB {

    public Billing2DB(ConnectionInf db) {
        super(db);
    }

    @Override
    public Vector selectByPatientId(String pk) throws Exception {
        String sql = "select t_billing.*,t_visit.visit_vn as vn  from t_billing\n"
                + "left join t_visit on t_visit.t_visit_id = t_billing.t_visit_id\n"
                + "where t_billing.t_patient_id = '" + pk + "' and t_billing.billing_active ='1'\n"
                + "order by t_billing.billing_billing_date_time desc";

        return eQuery(sql);
    }
}

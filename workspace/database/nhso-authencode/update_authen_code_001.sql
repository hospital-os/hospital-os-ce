INSERT INTO b_restful_url (b_restful_url_id, restful_code, restful_url, user_update_id, is_system)
VALUES  ('999' || (select b_visit_office_id from b_site) || 'nhso.authencode','nhso.authencode',
'https://authenservice.nhso.go.th/authencode',
(select rpad('157'||b_visit_office_id,18,'0')  from b_site), true);

CREATE TABLE f_nhso_authencode_claimtype
(
  id            character varying(10) NOT NULL,
  description   character varying(255) NOT NULL,
  CONSTRAINT f_nhso_authencode_claimtype_pkey PRIMARY KEY (id)
);
INSERT INTO f_nhso_authencode_claimtype VALUES
('PG0060001','เข้ารับบริการรักษาทั่วไป (OPD/ IPD/ PP)'),
('PG0090001','การดูแลรักษาในที่พัก (Home Isolation)'),
('PG0080001','การดูแลรักษาในชุมชน (Community Isolation)'),
('PG0110001','"Self Isolation'),
('PG0120001','UCEP PLUS (ผู้ป่วยกลุ่มอาการสีเหลืองและสีแดง)'),
('PG0130001','บริการฟอกเลือดด้วยเครื่องไตเทียม (HD)');

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TYPE authen_froms AS ENUM ('hospital-os', 'kiosk');

CREATE TABLE t_nhso_authencode
(
    t_nhso_authencode_id    UUID NOT NULL DEFAULT uuid_generate_v4(),
    vn                      character varying(20) NOT NULL,
    hn                      character varying(20) NOT NULL,
    pid                     character varying(13) NOT NULL,
    mobile                  character varying(10) NOT NULL,
    correlation_id          character varying(50) NOT NULL,
    claim_type              character varying(10) NOT NULL,
    claim_code              character varying(20) NOT NULL,
    created_date            character varying(20) NOT NULL,
    authen_from             authen_froms NOT NULL DEFAULT 'hospital-os'::authen_froms,
    record_date_time        timestamp with time zone NOT NULL default current_timestamp,
    user_record_id          character varying(255) NULL,
    CONSTRAINT t_nhso_authencode_pkey PRIMARY KEY (t_nhso_authencode_id)
);

CREATE INDEX t_nhso_authencode_vn_idx ON public.t_nhso_authencode (vn);
CREATE INDEX t_nhso_authencode_hn_idx ON public.t_nhso_authencode (hn);
CREATE INDEX t_nhso_authencode_pid_idx ON public.t_nhso_authencode (pid);
CREATE INDEX t_nhso_authencode_claim_code_idx ON public.t_nhso_authencode (claim_code);

-- create s_nhso_authencode_version
CREATE TABLE s_nhso_authencode_version
(
   id			serial,
   version_app 		text NOT NULL, 
   version_db 		text NOT NULL, 
   description		text NOT NULL, 
   update_datetime 	timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
   CONSTRAINT s_nhso_authencode_version_pk PRIMARY KEY (id),
   CONSTRAINT s_nhso_authencode_version_uk UNIQUE (version_db)
);
INSERT INTO s_nhso_authencode_version (version_app, version_db, description)VALUES ('1.0.0', '1.0.0', 'NHSO Authencode Module');

INSERT INTO s_script_update_log values ('NHSO_Authencode_Module','update_authen_code_001.sql',(select current_date) || ','|| (select current_time),'Initialize NHSO Authencode Module');
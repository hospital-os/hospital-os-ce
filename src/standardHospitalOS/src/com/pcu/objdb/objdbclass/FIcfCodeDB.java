/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.objdb.objdbclass;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.pcu.object.FIcfCode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sompr
 */
public class FIcfCodeDB {

    private final ConnectionInf connectionInf;

    public FIcfCodeDB(ConnectionInf db) {
        connectionInf = db;
    }

    public FIcfCode select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_icf_code where f_icf_code_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<FIcfCode> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<FIcfCode> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_icf_code order by f_icf_code_id";
            preparedStatement = connectionInf.ePQuery(sql);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<FIcfCode> list(String... ids) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_icf_code where f_icf_code_id in (?) order by f_icf_code_id";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setArray(1, connectionInf.getConnection().createArrayOf("varchar", ids));
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<FIcfCode> listByKeyword(String keyword) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_icf_code where length(f_icf_code.f_icf_code_id) = 4\n"
                    + "and f_icf_code.f_icf_code_id||' : '||f_icf_code.description ilike ? order by description";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, "%" + (keyword == null || keyword.isEmpty() ? "" : keyword) + "%");
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
    
    public List<FIcfCode> listByCodeId(String codeId) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_icf_code where length(f_icf_code.f_icf_code_id) > 4\n"
                    + "and substr(f_icf_code.f_icf_code_id,1,4) = ? order by description";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, codeId);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<FIcfCode> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<FIcfCode> list = new ArrayList<FIcfCode>();
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()) {
            FIcfCode obj = new FIcfCode();
            obj.setObjectId(rs.getString("f_icf_code_id"));
            obj.description = rs.getString("description");
            list.add(obj);
        }
        rs.close();
        return list;
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (FIcfCode obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }

    public List<CommonInf> getComboboxDatasource(String... ids) throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (FIcfCode obj : list(ids)) {
            list.add((CommonInf) obj);
        }
        return list;
    }
}

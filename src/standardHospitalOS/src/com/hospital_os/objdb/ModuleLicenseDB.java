/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.ModuleLicense;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class ModuleLicenseDB {

    private final ConnectionInf connectionInf;

    public ModuleLicenseDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public int upsert(ModuleLicense obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "INSERT INTO t_module_licenses(\n"
                    + "id, hcode, module_id, license, expires_at, user_record_id)\n"
                    + "VALUES (?::uuid, ?, ?, ?, ?, ?)\n"
                    + "ON CONFLICT (module_id)\n"
                    + "DO \n"
                    + "   UPDATE SET\n"
                    + "id = EXCLUDED.id::uuid,\n"
                    + "license = EXCLUDED.license,\n"
                    + "expires_at = EXCLUDED.expires_at,\n"
                    + "user_record_id = EXCLUDED.user_record_id";
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, obj.getObjectId());
            preparedStatement.setString(index++, obj.hcode);
            preparedStatement.setString(index++, obj.module_id);
            preparedStatement.setString(index++, obj.license);
            preparedStatement.setTimestamp(index++, new java.sql.Timestamp(obj.expires_at.getTime()));
            preparedStatement.setString(index++, obj.user_record_id);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "delete from t_module_licenses where id = ?::uuid";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            // execute delete SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public ModuleLicense select(String moduleId) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from t_module_licenses where module_id = ? and expires_at > current_timestamp";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, moduleId);
            List<ModuleLicense> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<ModuleLicense> list(String keyword) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from t_module_licenses where 1=1 " + (keyword == null || keyword.isEmpty() ? "" : "and module_id ilike ?") + " order by module_id";
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            if (keyword != null && !keyword.isEmpty()) {
                preparedStatement.setString(index++, "%" + keyword + "%");
            }
            List<ModuleLicense> list = executeQuery(preparedStatement);
            return list;
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<ModuleLicense> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<ModuleLicense> list = new ArrayList<>();
        try (ResultSet rs = preparedStatement.executeQuery()) {
            while (rs.next()) {
                ModuleLicense obj = new ModuleLicense();
                obj.setObjectId(rs.getString("id"));
                obj.module_id = rs.getString("module_id");
                obj.hcode = rs.getString("hcode");
                obj.license = rs.getString("license");
                obj.expires_at = rs.getTimestamp("expires_at");
                obj.user_record_id = rs.getString("user_record_id");
                obj.record_date_time = rs.getTimestamp("record_date_time");
                list.add(obj);
            }
            return list;
        }
    }

}

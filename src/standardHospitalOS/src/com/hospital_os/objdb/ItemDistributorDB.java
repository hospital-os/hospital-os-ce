/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.ItemDistributor;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class ItemDistributorDB {

    public ConnectionInf connectionInf;
    final public String tableId = "857";

    public ItemDistributorDB(ConnectionInf db) {
        connectionInf = db;
    }

    public int insert(ItemDistributor obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO b_item_distributor(\n"
                    + "            b_item_distributor_id, item_distributor_number, item_distributor_description,\n"
                    + "            active, user_record,user_modify)\n"
                    + "    VALUES (?, ?, ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.getGenID(tableId));
            preparedStatement.setString(index++, obj.item_distributor_number);
            preparedStatement.setString(index++, obj.item_distributor_description);
            preparedStatement.setString(index++, obj.active);
            preparedStatement.setString(index++, obj.user_record);
            preparedStatement.setString(index++, obj.user_modify);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(ItemDistributor obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE b_item_distributor\n"
                    + "   SET item_distributor_number=?, \n"
                    + "       item_distributor_description=?, active=?, modify_date_time=current_timestamp, user_modify=?\n"
                    + " WHERE b_item_distributor_id=?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.item_distributor_number);
            preparedStatement.setString(index++, obj.item_distributor_description);
            preparedStatement.setString(index++, obj.active);
            preparedStatement.setString(index++, obj.user_modify);
            preparedStatement.setString(index++, obj.getObjectId());
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(ItemDistributor obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM b_item_distributor\n");
            sql.append(" WHERE b_item_distributor_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public ItemDistributor selectById(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_item_distributor\n"
                    + "where b_item_distributor_id = ?\n";
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, id);
            List<ItemDistributor> executeQuery = executeQuery(preparedStatement);
            return executeQuery.isEmpty() ? null : executeQuery.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<ItemDistributor> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_item_distributor where active = '1' order by item_distributor_number, item_distributor_description";
            preparedStatement = connectionInf.ePQuery(sql);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<ItemDistributor> listByKeyword(String keyword, String active) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_item_distributor\n"
                    + "where (item_distributor_number ilike ? or item_distributor_description ilike ?)\n"
                    + "and active = ?\n"
                    + "order by item_distributor_number, item_distributor_description";
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, "%" + keyword + "%");
            preparedStatement.setString(index++, "%" + keyword + "%");
            preparedStatement.setString(index++, active);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<ItemDistributor> listByCode(String code) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_item_distributor\n"
                    + "where item_distributor_number = ? \n"
                    + "and active = '1'\n"
                    + "order by item_distributor_number, item_distributor_description";
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, code);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<ItemDistributor> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<ItemDistributor> list = new ArrayList<ItemDistributor>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                ItemDistributor obj = new ItemDistributor();
                obj.setObjectId(rs.getString("b_item_distributor_id"));
                obj.item_distributor_description = rs.getString("item_distributor_description");
                obj.item_distributor_number = rs.getString("item_distributor_number");
                obj.active = rs.getString("active");
                obj.record_date_time = rs.getTimestamp("record_date_time");
                obj.user_record = rs.getString("user_record");
                obj.modify_date_time = rs.getTimestamp("modify_date_time");
                obj.user_modify = rs.getString("user_modify");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (ItemDistributor obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }

    public List<CommonInf> getComboboxDatasource(String keyword) throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (ItemDistributor obj : listByKeyword(keyword, "1")) {
            list.add((CommonInf) obj);
        }
        return list;
    }
}

/*
 * Educate2DB.java
 *
 * Created on 21 �ѹ��¹ 2548, 15:53 �.
 */
package com.hosv3.objdb;

import com.hospital_os.objdb.EducateDB;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.util.Vector;

/**
 *
 * @author kingland
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class Educate2DB extends EducateDB {

    /**
     * Creates a new instance of Educate2DB
     */
    public Educate2DB(ConnectionInf db) {
        super(db);
    }

    @Override
    public Vector selectAll() throws Exception {
        String sql = "select * from " + dbObj.table + " order by "
                + "patient_education_type_sort_index";
        Vector vc = veQuery(sql);
        if (vc.isEmpty()) {
            return null;
        } else {
            return vc;
        }
    }
}

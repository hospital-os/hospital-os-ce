/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class FinanceInsuranceCompany extends Persistent implements CommonInf {

    public String code = "";
    public String company_name = "";
    public String company_address = "";
    public String company_telephone = "";
    public String company_fax = "";
    public int alert_repay_day = 0;
    public String active = "1";
    public String record_datetime = "";
    public String update_datetime = "";
    public String user_record_id = "";
    public String user_update_id = "";

    @Override
    public String getCode() {
        return getObjectId();
    }

    @Override
    public String getName() {
        return company_name;
    }

    @Override
    public String toString() {
        return company_name;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class MapDue extends Persistent {

    private static final long serialVersionUID = 1L;
    public String b_item_id;
    public String map_due_type;
    public String b_due_type_id;
    public String active = "1";
    public String user_record_id;
    public String record_date_time;
    public String user_update_id;
    public String update_date_time;
    public String[] contract_plans;

    public String getContractPlans() {
        if (contract_plans == null) {
            contract_plans = new String[]{};
        }
        String cp = "";
        for (String string : contract_plans) {
            cp += ",'" + string + "'";
        }
        return cp.startsWith(",") ? cp.substring(1) : "''";
    }
}

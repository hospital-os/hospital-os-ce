package com.hospital_os.object;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.Persistent;

public class Nation extends Persistent implements CommonInf {

    public String description;

    /**
     * @roseuid 3F658BBB036E
     */
//   public Nation() 
//   {
//    
//   }
    @Override
    public String toString() {
        return description;
    }

    @Override
    public String getCode() {
        return getObjectId();
    }

    @Override
    public String getName() {
        return description;
    }
}

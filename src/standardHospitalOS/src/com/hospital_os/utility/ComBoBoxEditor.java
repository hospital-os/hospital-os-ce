/*
 * CheckBoxEdit.java
 *
 * Created on 24 ����Ҥ� 2547, 21:31 �.
 */
package com.hospital_os.utility;

import java.awt.*;
import java.awt.event.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

/**
 *
 * @author tong
 */
public class ComBoBoxEditor extends DefaultCellEditor implements ItemListener {

    private static final long serialVersionUID = 1L;
    private JComboBox button;

    public ComBoBoxEditor(JComboBox comboBox) {
        super(comboBox);
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value,
            boolean isSelected, int row, int column) {
        if (value == null) {
            return null;
        }
        button = (JComboBox) value;
        button.addItemListener(this);
        return (Component) value;
    }

    @Override
    public Object getCellEditorValue() {
        try {
            button.removeItemListener(this);
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return button;
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        super.fireEditingStopped();
    }
    private static final Logger LOG = Logger.getLogger(ComBoBoxEditor.class.getName());
}

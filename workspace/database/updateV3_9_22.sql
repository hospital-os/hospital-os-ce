insert into r_rp1855_occupation values ('797','9000','นักเรียนหรือนักศึกษา');
insert into r_rp1855_occupation values ('798','9001','แม่บ้าน หรือพ่อบ้าน');
insert into r_rp1855_occupation values ('799','9002','ในปกครอง');
insert into r_rp1855_occupation values ('800','9003','ข้าราชการบำนาญ');
insert into r_rp1855_occupation values ('801','9004','ลูกจ้างประจำ');
insert into r_rp1855_occupation values ('802','9005','ลูกจ้างชั่วคราว');
insert into r_rp1855_occupation values ('803','9995','ไม่มีงานทำ');

INSERT INTO s_version VALUES ('9701000000055', '55', 'Hospital OS, Community Edition', '3.9.22', '3.18.220612', (to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD HH24:MI:SS'));

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_22.sql',(to_number(to_char(current_date, 'YYYY'), '9999') + 543) || to_char(current_timestamp, '-MM-DD,HH24:MI:SS'),'ปรับแก้สำหรับhospitalOS3.9.22');
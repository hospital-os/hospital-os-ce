/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.PictureProfile;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class PictureProfileDB {

    private final ConnectionInf connectionInf;

    public PictureProfileDB(ConnectionInf c) {
        this.connectionInf = c;
    }

    public int upsert(PictureProfile obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("insert into public.t_picture_profile \n"
                    + "(t_person_id,picture_profile,user_update_id)\n"
                    + "values\n"
                    + "(?,?,?)\n"
                    + "on conflict (t_person_id)\n"
                    + "DO \n"
                    + "   UPDATE SET picture_profile = EXCLUDED.picture_profile, user_update_id = EXCLUDED.user_update_id, update_date_time = current_timestamp\n"
                    + "RETURNING *");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.t_person_id);
            preparedStatement.setBytes(index++, obj.picture_profile);
            preparedStatement.setString(index++, obj.user_update_id);
            List<PictureProfile> list = executeQuery(preparedStatement);
            if (list.isEmpty()) {
                return 0;
            }
            obj = ((PictureProfile) list.get(0));
            return 1;
//            // execute insert SQL stetement
//            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

//    public int insert(PictureProfile obj) throws Exception {
//        PreparedStatement preparedStatement = null;
//        try {
//            StringBuilder sql = new StringBuilder();
//            sql.append("INSERT INTO t_picture_profile(\n"
//                    + "            t_picture_profile_id, t_person_id, picture_profile, user_update_id)\n"
//                    + "    VALUES (?, ?, ?, ?)");
//            preparedStatement = connectionInf.ePQuery(sql.toString());
//            int index = 1;
//            preparedStatement.setString(index++, obj.getGenID(tableId));
//            preparedStatement.setString(index++, obj.t_person_id);
//            preparedStatement.setBytes(index++, obj.picture_profile);
//            preparedStatement.setString(index++, obj.user_update_id);
//            // execute insert SQL stetement
//            return preparedStatement.executeUpdate();
//        } finally {
//            if (preparedStatement != null) {
//                preparedStatement.close();
//            }
//        }
//    }
//
//    public int update(PictureProfile obj) throws Exception {
//        PreparedStatement preparedStatement = null;
//        try {
//            StringBuilder sql = new StringBuilder();
//            sql.append("UPDATE t_picture_profile\n");
//            sql.append("   SET picture_profile=?, \n");
//            sql.append("       user_update_id=?, update_datetime = current_timestamp\n");
//            sql.append(" WHERE t_picture_profile_id = ?");
//            preparedStatement = connectionInf.ePQuery(sql.toString());
//            int index = 1;
//            preparedStatement.setBytes(index++, obj.picture_profile);
//            preparedStatement.setString(index++, obj.user_update_id);
//            preparedStatement.setString(index++, obj.getObjectId());
//            // execute update SQL stetement
//            return preparedStatement.executeUpdate();
//
//        } finally {
//            if (preparedStatement != null) {
//                preparedStatement.close();
//            }
//        }
//    }
//
//    public PictureProfile select(String id) throws Exception {
//        PreparedStatement preparedStatement = null;
//        try {
//            String sql = "select * from t_picture_profile where t_picture_profile_id = ?";
//            preparedStatement = connectionInf.ePQuery(sql);
//            preparedStatement.setString(1, id);
//            List<PictureProfile> list = executeQuery(preparedStatement);
//            return list.isEmpty() ? null : list.get(0);
//        } finally {
//            if (preparedStatement != null) {
//                preparedStatement.close();
//            }
//        }
//    }
    public PictureProfile selectByPersonId(String t_person_id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from t_picture_profile  where t_person_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, t_person_id);
            List<PictureProfile> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<PictureProfile> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<PictureProfile> list = new ArrayList<>();
        try (ResultSet rs = preparedStatement.executeQuery()) {
// execute SQL stetement
            while (rs.next()) {
                PictureProfile obj = new PictureProfile();
                obj.setObjectId(rs.getString("t_picture_profile_id"));
                obj.t_person_id = rs.getString("t_person_id");
                obj.picture_profile = rs.getBytes("picture_profile");
                obj.user_update_id = rs.getString("user_update_id");
                obj.update_date_time = rs.getTimestamp("update_date_time");
                list.add(obj);
            }
            return list;
        }
    }
}

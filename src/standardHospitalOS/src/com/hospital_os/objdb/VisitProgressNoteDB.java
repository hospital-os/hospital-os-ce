/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.VisitProgressNote;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author tanakrit
 */
public class VisitProgressNoteDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "999";

    public VisitProgressNoteDB(ConnectionInf c) {
        this.connectionInf = c;
    }

    public int insert(VisitProgressNote p) throws Exception {
        p.generateOID(tableId);
        String sql = "INSERT INTO t_visit_progress_note(\n"
                + "               t_visit_progress_note_id, t_visit_id, subjective, "
                + "               assessment, objective, plan, "
                + "               active, user_record_id ,user_update_id) \n"
                + "       VALUES (?, ?, ?, "
                + "               ?, ?, ?, "
                + "               ?, ?, ?)";
        try (PreparedStatement ePQuery = connectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, p.getObjectId());
            ePQuery.setString(index++, p.t_visit_id);
            ePQuery.setString(index++, p.subjective);
            ePQuery.setString(index++, p.assessment);
            ePQuery.setString(index++, p.objective);
            ePQuery.setString(index++, p.plan);
            ePQuery.setString(index++, p.active);
            ePQuery.setString(index++, p.user_record_id);
            ePQuery.setString(index++, p.user_update_id);
            return ePQuery.executeUpdate();
        }
    }

    public int update(VisitProgressNote p) throws Exception {
        String sql = "UPDATE t_visit_progress_note "
                + "      SET t_visit_id=? ,subjective=? \n"
                + "         ,assessment=? ,objective=? ,plan=? \n"
                + "         ,active=? ,update_date_time=current_timestamp ,user_update_id=? \n";
        if (p.active.equals("0")) {
            sql += "        ,cancel_date_time=current_timestamp ,user_cancel_id=? \n";
        }
        sql += "       WHERE t_visit_progress_note_id = ? ";
        try (PreparedStatement ePQuery = connectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, p.t_visit_id);
            ePQuery.setString(index++, p.subjective);
            ePQuery.setString(index++, p.assessment);
            ePQuery.setString(index++, p.objective);
            ePQuery.setString(index++, p.plan);
            ePQuery.setString(index++, p.active);
            ePQuery.setString(index++, p.user_update_id);
            if (p.active.equals("0")) {
                ePQuery.setString(index++, p.user_cancel_id);
            }
            ePQuery.setString(index++, p.getObjectId());
            return ePQuery.executeUpdate();
        }
    }

    public List<VisitProgressNote> listProgressNote(Date dateFrom, Date dateTo, String pk, String doctor) throws Exception {
        String sql = "select * from t_visit_progress_note "
                + " where t_visit_id=? ";
        if (dateFrom != null) {
            sql += " and record_date_time::date >= ? "
                    + " and record_date_time::date <= ? ";
        }
        if (doctor != null && !doctor.equals("")) {
            sql += " and user_record_id = ? ";
        }
        sql += " and active = '1' order by record_date_time DESC";
        try (PreparedStatement ePQuery = connectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, pk);
            if (dateFrom != null) {
                ePQuery.setDate(index++, new java.sql.Date(dateFrom.getTime()));
                ePQuery.setDate(index++, new java.sql.Date(dateTo.getTime()));
            }
            if (doctor != null && !doctor.equals("")) {
                ePQuery.setString(index++, doctor);
            }
            return executeQuery(ePQuery);
        }
    }

    public List<VisitProgressNote> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<VisitProgressNote> list = new ArrayList<>();
        try (ResultSet rs = preparedStatement.executeQuery()) {
            while (rs.next()) {
                VisitProgressNote p = new VisitProgressNote();
                p.setObjectId(rs.getString("t_visit_progress_note_id"));
                p.t_visit_id = rs.getString("t_visit_id");
                p.subjective = rs.getString("subjective");
                p.assessment = rs.getString("assessment");
                p.objective = rs.getString("objective");
                p.plan = rs.getString("plan");
                p.active = rs.getString("active");
                p.user_record_id = rs.getString("user_record_id");
                p.record_date_time = rs.getTimestamp("record_date_time");
                p.user_update_id = rs.getString("user_update_id");
                p.update_date_time = rs.getTimestamp("update_date_time");
                p.user_cancel_id = rs.getString("user_record_id");
                p.cancel_date_time = rs.getTimestamp("record_date_time");
                list.add(p);
            }
            return list;
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.ComboFix;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tanakrit
 */
public class UcumItemUnitDB {

    private final ConnectionInf theConnectionInf;

    public UcumItemUnitDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public List<ComboFix> selectAll() throws Exception {
        String sql = "select * from f_ucum_item_unit order by code";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            return executeQuery(ePQuery);
        }
    }

    public List<ComboFix> listByKeyword(String keyword) throws Exception {
        String sql = "select * from f_ucum_item_unit \n";
        if (keyword != null && !keyword.isEmpty()) {
            sql += "where (code ilike ? or description ilike ?) \n";
        }
        sql += "order by code ";
        try (PreparedStatement ps = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            if (keyword != null && !keyword.trim().isEmpty()) {
                ps.setString(index++, "%" + keyword + "%");
                ps.setString(index++, "%" + keyword + "%");
            }
            return executeQuery(ps);
        }
    }

    public List<CommonInf> getComboboxDatasource(String key) throws Exception {
        List<CommonInf> list = new ArrayList<>();
        for (ComboFix obj : listByKeyword(key)) {
            list.add((CommonInf) obj);
        }
        return list;
    }

    public List<ComboFix> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<ComboFix> list = new ArrayList<>();
        try (ResultSet rs = preparedStatement.executeQuery()) {
            while (rs.next()) {
                ComboFix p = new ComboFix();
                p.code = rs.getString("code");
                p.name = (p.code + ":" + rs.getString("description"));
                list.add(p);
            }
            return list;
        }
    }
}

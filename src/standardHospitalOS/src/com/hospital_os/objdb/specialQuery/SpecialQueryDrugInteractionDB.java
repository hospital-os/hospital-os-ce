/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb.specialQuery;

import com.hospital_os.object.specialQuery.SpecialQueryDrugInteraction;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sompr
 */
public class SpecialQueryDrugInteractionDB {

    public ConnectionInf connectionInf;

    public SpecialQueryDrugInteractionDB(ConnectionInf db) {
        connectionInf = db;
    }

    public List<SpecialQueryDrugInteraction> checkDrugInteraction(String visitId, String itemId) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select \n"
                    + "interaction.drug_name\n"
                    + ", interaction.interaction_name\n"
                    + ", interaction.interaction_action\n"
                    + ", interaction.interaction_fix\n"
                    + ", interaction.interaction_level\n"
                    + "from t_order o\n"
                    + "inner join (\n"
                    + "select\n"
                    + "io.item_common_name as drug_name\n"
                    + ", ii.item_common_name as interaction_name\n"
                    + ", ii.b_item_id as interaction_id, idi.item_drug_interaction_act as interaction_action\n"
                    + ", idi.item_drug_interaction_repair as interaction_fix\n"
                    + ", idi.item_drug_interaction_force as interaction_level\n"
                    + "from b_item_drug_interaction idi\n"
                    + "--inner join b_item_drug_standard ids on ids.b_item_drug_standard_id = idi.drug_standard_original_id\n"
                    + "inner join b_item_drug_standard_map_item idsmi_o on idsmi_o.b_item_drug_standard_id = idi.drug_standard_original_id\n"
                    + "inner join b_item io on io.b_item_id = idsmi_o.b_item_id \n"
                    + "inner join b_item_drug_standard_map_item idsmi_i on idsmi_i.b_item_drug_standard_id = idi.drug_standard_interaction_id \n"
                    + "inner join b_item ii on ii.b_item_id = idsmi_i.b_item_id \n"
                    + "where idsmi_o.b_item_id = ?\n"
                    + "and idi.item_drug_interaction_force = '1'\n"
                    + "union \n"
                    + "select\n"
                    + "ii.item_common_name as drug_name\n"
                    + ", io.item_common_name as interaction_name\n"
                    + ", io.b_item_id as interaction_id\n"
                    + ", idi.item_drug_interaction_act as interaction_action\n"
                    + ", idi.item_drug_interaction_repair as interaction_fix\n"
                    + ", idi.item_drug_interaction_force as interaction_level\n"
                    + "from b_item_drug_interaction idi\n"
                    + "--inner join b_item_drug_standard ids on ids.b_item_drug_standard_id = idi.drug_standard_original_id\n"
                    + "inner join b_item_drug_standard_map_item idsmi_o on idsmi_o.b_item_drug_standard_id = idi.drug_standard_original_id\n"
                    + "inner join b_item io on io.b_item_id = idsmi_o.b_item_id \n"
                    + "inner join b_item_drug_standard_map_item idsmi_i on idsmi_i.b_item_drug_standard_id = idi.drug_standard_interaction_id \n"
                    + "inner join b_item ii on ii.b_item_id = idsmi_i.b_item_id \n"
                    + "where idsmi_i.b_item_id = ?\n"
                    + "and idi.item_drug_interaction_force = '1'\n"
                    + ") as interaction on interaction.interaction_id = o.b_item_id \n"
                    + "-- drug only and not cancel\n"
                    + "where o.t_visit_id = ? and f_item_group_id = '1' and f_order_status_id <> '3'";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, itemId);
            preparedStatement.setString(2, itemId);
            preparedStatement.setString(3, visitId);
            // execute SQL stetement
            return eQuery(preparedStatement.toString());
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }

    }

    private List<SpecialQueryDrugInteraction> eQuery(String sql) throws Exception {
        List<SpecialQueryDrugInteraction> list = new ArrayList<>();
        try (ResultSet rs = connectionInf.eQuery(sql)) {
            while (rs.next()) {
                SpecialQueryDrugInteraction p = new SpecialQueryDrugInteraction();
                p.drugName = rs.getString("drug_name");
                p.interactionName = rs.getString("interaction_name");
                p.interactionAction = rs.getString("interaction_action");
                p.interactionFix = rs.getString("interaction_fix");
                p.interactionLevel = rs.getString("interaction_level");
                list.add(p);
            }
        }
        return list;
    }
}

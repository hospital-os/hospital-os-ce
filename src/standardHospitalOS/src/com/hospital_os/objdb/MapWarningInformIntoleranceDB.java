/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.MapWarningInformIntolerance;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class MapWarningInformIntoleranceDB {

    private final ConnectionInf connectionInf;
    private final String idtable = "999";

    public MapWarningInformIntoleranceDB(ConnectionInf theConnectionInf) {
        this.connectionInf = theConnectionInf;
    }

    public int insert(MapWarningInformIntolerance o) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO b_map_warning_inform_intolerance (b_map_warning_inform_intolerance_id, f_employee_authentication_id"
                    + ", user_record_id)\n"
                    + "    VALUES (?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, o.getGenID(idtable));
            preparedStatement.setString(index++, o.f_employee_authentication_id);
            preparedStatement.setString(index++, o.user_record_id);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int delete(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM b_map_warning_inform_intolerance\n");
            sql.append(" WHERE b_map_warning_inform_intolerance_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, id);
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public MapWarningInformIntolerance selectById(String id) throws Exception {
        String sql = "select b_map_warning_inform_intolerance.*, f_employee_authentication.employee_authentication_description "
                + "from b_map_warning_inform_intolerance "
                + "inner join f_employee_authentication on f_employee_authentication.f_employee_authentication_id = b_map_warning_inform_intolerance.f_employee_authentication_id "
                + "where b_map_warning_inform_intolerance_id = ?";

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            preparedStatement.setString(index++, id);
            List<MapWarningInformIntolerance> list = eQuery(preparedStatement.toString());
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<MapWarningInformIntolerance> listMapped(String keyword) throws Exception {
        String sql = "select b_map_warning_inform_intolerance.*, f_employee_authentication.employee_authentication_description\n"
                + "from b_map_warning_inform_intolerance\n"
                + "inner join f_employee_authentication on f_employee_authentication.f_employee_authentication_id = b_map_warning_inform_intolerance.f_employee_authentication_id\n";
        if (keyword != null && !keyword.isEmpty()) {
            sql += "where f_employee_authentication.employee_authentication_description ilike ?\n";
        }
        sql += " order by f_employee_authentication.employee_authentication_description";

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            if (keyword != null && !keyword.isEmpty()) {
                preparedStatement.setString(index++, "%" + keyword + "%");
            }

            List<MapWarningInformIntolerance> list = eQuery(preparedStatement.toString());
            return list;
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<Object[]> listUnmap(String keyword) throws Exception {
        String sql = "select f_employee_authentication.f_employee_authentication_id\n"
                + ",f_employee_authentication.employee_authentication_description\n"
                + "from f_employee_authentication\n"
                + "where f_employee_authentication.f_employee_authentication_id not in ("
                + "select b_map_warning_inform_intolerance.f_employee_authentication_id from b_map_warning_inform_intolerance )\n";
        if (keyword != null && !keyword.isEmpty()) {
            sql += "and f_employee_authentication.employee_authentication_description ilike ?\n";
        }
        sql += " order by f_employee_authentication.employee_authentication_description";

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connectionInf.ePQuery(sql);
            int index = 1;
            if (keyword != null && !keyword.isEmpty()) {
                preparedStatement.setString(index++, "%" + keyword + "%");
            }

            List<Object[]> list = connectionInf.eComplexQuery(preparedStatement.toString());
            return list;
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<MapWarningInformIntolerance> eQuery(String sql) throws Exception {
        List<MapWarningInformIntolerance> list = new ArrayList<MapWarningInformIntolerance>();
        ResultSet rs = connectionInf.eQuery(sql.toString());
        while (rs.next()) {
            MapWarningInformIntolerance p = new MapWarningInformIntolerance();
            p.setObjectId(rs.getString("b_map_warning_inform_intolerance_id"));
            p.f_employee_authentication_id = rs.getString("f_employee_authentication_id");
            try {
                p.authenticationDescription = rs.getString("employee_authentication_description");
            } catch (Exception ex) {
            }
            list.add(p);
        }
        rs.close();
        return list;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.DentType;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class DentTypeDB {

    private final ConnectionInf connectionInf;

    public DentTypeDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public List<DentType> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from f_dent_type";
            preparedStatement = connectionInf.ePQuery(sql);
            
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<DentType> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<DentType> list = new ArrayList<DentType>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                DentType obj = new DentType();
                obj.setObjectId(rs.getString("f_dent_type_id"));
                obj.description = rs.getString("description");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (DentType obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }
}

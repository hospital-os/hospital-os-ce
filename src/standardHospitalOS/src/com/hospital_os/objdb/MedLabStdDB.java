/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.MedLabStd;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.ResultSet;
import java.util.Vector;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class MedLabStdDB {

    private final ConnectionInf theConnectionInf;

    public MedLabStdDB(ConnectionInf db) {
        theConnectionInf = db;
    }
    public MedLabStd selectById(String id) throws Exception {
        String sql = "select * from b_med_lab_std where b_med_lab_std_id = '%s'";
        Vector eQuery = eQuery(String.format(sql, id));
        return (MedLabStd) (eQuery.isEmpty() ? null : eQuery.get(0));
    }
    
    public Vector<MedLabStd> listAll() throws Exception {
        String sql = "select * from b_med_lab_std";
        Vector eQuery = eQuery(sql);
        return eQuery;
    }

    public Vector eQuery(String sql) throws Exception {
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        while (rs.next()) {
            MedLabStd p = new MedLabStd();
            p.setObjectId(rs.getString("b_med_lab_std_id"));
            p.code = rs.getString("code");
            p.englishname = rs.getString("englishname");
            p.active = rs.getString("active");
            p.code_std = rs.getString("code_std");
            list.add(p);
        }
        rs.close();
        return list;
    }
}

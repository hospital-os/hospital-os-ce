/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.MapOrderDoctor;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.Gutil;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class MapOrderDoctorDB {

    private final ConnectionInf theConnectionInf;
    private final String idtable = "999";

    public MapOrderDoctorDB(ConnectionInf theConnectionInf) {
        this.theConnectionInf = theConnectionInf;
    }

    public int insert(MapOrderDoctor o) throws Exception {
        String sql = "INSERT INTO b_map_order_by_doctor (b_map_order_by_doctor_id, b_item_subgroup_id"
                + ", user_record_id, record_date_time, user_update_id, update_date_time) "
                + "VALUES ('%s', '%s', '%s', '%s', '%s', '%s')";
        return theConnectionInf.eUpdate(String.format(sql,
                o.getGenID(idtable),
                o.b_item_subgroup_id,
                o.user_record_id,
                o.record_date_time,
                o.user_update_id,
                o.update_date_time));
    }

    public int delete(String id) throws Exception {
        String sql = "DELETE FROM  b_map_order_by_doctor  "
                + "WHERE b_map_order_by_doctor_id = '%s'";
        return theConnectionInf.eUpdate(String.format(sql, id));
    }

    public MapOrderDoctor selectById(String id) throws Exception {
        String sql = "select b_map_order_by_doctor.*, b_item_subgroup.item_subgroup_number, b_item_subgroup.item_subgroup_description "
                + "from b_map_order_by_doctor "
                + "inner join b_item_subgroup on b_item_subgroup.b_item_subgroup_id = b_map_order_by_doctor.b_item_subgroup_id "
                + "and item_subgroup_active = '1' "
                + "where b_map_order_by_doctor_id = '%s'";
        List<MapOrderDoctor> list = eQuery(String.format(sql, id));
        return list.isEmpty() ? null : list.get(0);
    }

    public MapOrderDoctor selectBySubgroupId(String id) throws Exception {
        String sql = "select b_map_order_by_doctor.*, b_item_subgroup.item_subgroup_number, b_item_subgroup.item_subgroup_description "
                + "from b_map_order_by_doctor "
                + "inner join b_item_subgroup on b_item_subgroup.b_item_subgroup_id = b_map_order_by_doctor.b_item_subgroup_id "
                + "and item_subgroup_active = '1' "
                + "where b_map_order_by_doctor.b_item_subgroup_id = '%s'";
        List<MapOrderDoctor> list = eQuery(String.format(sql, id));
        return list.isEmpty() ? null : list.get(0);
    }

    public List<MapOrderDoctor> listMapped(String keyword) throws Exception {
        String sql = "select b_map_order_by_doctor.*, b_item_subgroup.item_subgroup_number, b_item_subgroup.item_subgroup_description "
                + "from b_map_order_by_doctor "
                + "inner join b_item_subgroup on b_item_subgroup.b_item_subgroup_id = b_map_order_by_doctor.b_item_subgroup_id "
                + "and b_item_subgroup.item_subgroup_active = '1' ";
        if (keyword != null && !keyword.isEmpty()) {
            sql += "where (UPPER(b_item_subgroup.item_subgroup_number) like UPPER('%s') "
                    + "or UPPER(b_item_subgroup.item_subgroup_description) like UPPER('%s'))";
        }
        sql += " order by b_item_subgroup.item_subgroup_description";
        List<MapOrderDoctor> list = eQuery(keyword != null && !keyword.isEmpty() ? String.format(sql, "%" + Gutil.CheckReservedWords(keyword) + "%", "%" + Gutil.CheckReservedWords(keyword) + "%") : sql);
        return list;
    }

    public List<Object[]> listUnmap(String keyword) throws Exception {
        String sql = "select b_item_subgroup.b_item_subgroup_id, b_item_subgroup.item_subgroup_description "
                + "from b_item_subgroup "
                + "where b_item_subgroup.item_subgroup_active = '1' "
                + "and b_item_subgroup.b_item_subgroup_id not in ("
                + "select b_map_order_by_doctor.b_item_subgroup_id from b_map_order_by_doctor )";
        if (keyword != null && !keyword.isEmpty()) {
            sql += "and (UPPER(b_item_subgroup.item_subgroup_number) like UPPER('%s') "
                    + "or UPPER(b_item_subgroup.item_subgroup_description) like UPPER('%s'))";
        }
        sql += " order by b_item_subgroup.item_subgroup_description";
        List<Object[]> list = theConnectionInf.eComplexQuery(keyword != null && !keyword.isEmpty() ? String.format(sql, "%" + Gutil.CheckReservedWords(keyword) + "%", "%" + Gutil.CheckReservedWords(keyword) + "%") : sql);
        return list;
    }

    public boolean isMappedOrderDoctorByOrderId(String orderId) throws Exception {
        String sql = "select \n"
                + "count(b_map_order_by_doctor.b_map_order_by_doctor_id) \n"
                + "from \n"
                + "t_order\n"
                + "inner join b_map_order_by_doctor on b_map_order_by_doctor.b_item_subgroup_id = t_order.b_item_subgroup_id\n"
                + "where \n"
                + "t_order.t_order_id = '%s'\n"
                + "and t_order.f_order_status_id = '0'";
        List<Object[]> list = theConnectionInf.eComplexQuery(String.format(sql,
                orderId));
        return list.isEmpty() ? false : String.valueOf(list.get(0)[0]).equals("1");
    }

    public List<MapOrderDoctor> eQuery(String sql) throws Exception {
        List<MapOrderDoctor> list = new ArrayList<MapOrderDoctor>();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        while (rs.next()) {
            MapOrderDoctor p = new MapOrderDoctor();
            p.setObjectId(rs.getString("b_map_order_by_doctor_id"));
            p.b_item_subgroup_id = rs.getString("b_item_subgroup_id");
            try {
                p.item_subgroup_number = rs.getString("item_subgroup_number");
            } catch (Exception ex) {
            }
            try {
                p.item_subgroup_description = rs.getString("item_subgroup_description");
            } catch (Exception ex) {
            }
            list.add(p);
        }
        rs.close();
        return list;
    }
}

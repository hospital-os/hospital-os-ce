/*
 * VisitAControl.java
 *
 * Created on 20 ���Ҥ� 2546, 13:33 �.
 */
package com.hosv3.control;

import com.hospital_os.object.Accident;
import com.hospital_os.object.Active;
import com.hospital_os.object.Authentication;
import com.hospital_os.object.Chronic;
import com.hospital_os.object.Death;
import com.hospital_os.object.DiagIcd10;
import com.hospital_os.object.DiagIcd9;
import com.hospital_os.object.DischargeOpd;
import com.hospital_os.object.DischargeStatus;
import com.hospital_os.object.DischargeType;
import com.hospital_os.object.DxTemplate;
import com.hospital_os.object.Dxtype;
import com.hospital_os.object.Employee;
import com.hospital_os.object.Group506;
import com.hospital_os.object.GroupIcd10;
import com.hospital_os.object.ICD10;
import com.hospital_os.object.ICD9;
import com.hospital_os.object.MapVisitDx;
import com.hospital_os.object.MarryStatus;
import com.hospital_os.object.Nation;
import com.hospital_os.object.Occupat;
import com.hospital_os.object.Optype;
import com.hospital_os.object.ParticipateOr;
import com.hospital_os.object.Patient;
import com.hospital_os.object.Relation;
import com.hospital_os.object.Surveil;
import com.hospital_os.object.Transfer;
import com.hospital_os.object.TypeDish;
import com.hospital_os.object.Visit;
import com.hospital_os.object.VisitStatus;
import com.hospital_os.object.VisitType;
import com.hospital_os.object.X39Persistent;
import com.hospital_os.object.specialQuery.ChronicReport;
import com.hospital_os.object.specialQuery.SurveilReport;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.usecase.connection.UpdateStatus;
import com.hospital_os.utility.Gutil;
import com.hosv3.gui.dialog.DialogWarningDiagIcd10;
import com.hosv3.object.ChronicDischargeStatus;
import com.hosv3.object.HosObject;
import com.hosv3.subject.HosSubject;
import com.hosv3.utility.Constant;
import com.hosv3.utility.DateUtil;
import com.hosv3.utility.ResourceBundle;
import com.pcu.object.Disease;
import com.pcu.object.Family;
import com.pcu.object.Uncontagious;
import java.sql.SQLException;
import java.util.Date;
import java.util.Vector;
import java.util.logging.Logger;

/**
 *
 * @author henbe
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class DiagnosisControl {

    ConnectionInf theConnectionInf;
    HosDB theHosDB;
    LookupControl theLookupControl;
    HosObject theHO;
    HosSubject theHS;
    UpdateStatus theUS;
    SystemControl theSystemControl;
//    public Family theFamily;
//    public Patient thePatient;
    // henbe comment 230210 kong ���һ�С�ȵ�����������������¹͸Ժ�´�������������������
    /**
     * ������������� ��ͧ��è��ʴ� dialog �ͧ�ä������ѧ ����
     * ������ѧ������� true = �ʴ� false = ����ʴ�
     */
    //��Ѻ pattern ���������� ����õ�ǹ�������繵�ͧ�� global
    //private boolean isShowDialog = true;
    private VisitControl theVisitControl;
    private HosControl hosControl;

    /**
     * Creates a new instance of LookupControl
     *
     * @param con
     * @param ho
     * @param hdb
     * @param hs
     */
    public DiagnosisControl(ConnectionInf con, HosObject ho, HosDB hdb, HosSubject hs) {
        theConnectionInf = con;
        theHosDB = hdb;
        theHS = hs;
        theHO = ho;
    }

    public void setSystemControl(SystemControl systemControl) {
        theSystemControl = systemControl;
    }

    public void setDepControl(LookupControl lc, VisitControl vc) {
        theLookupControl = lc;
        theVisitControl = vc;
    }

    public void setUpdateStatus(UpdateStatus us) {
        theUS = us;
    }

    public boolean deleteChronic(Chronic chronic, UpdateStatus theUS) {
        if (chronic == null) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.DiagnosisControl.NO.CHRONIC.DATA"), UpdateStatus.WARNING);
            return false;
        }
        if (chronic.vn_id != null && !chronic.vn_id.isEmpty()) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.DiagnosisControl.WARNNIG.CHRONIC.RELATE.ICD"), UpdateStatus.WARNING);
            return false;
        }
        if (!theUS.confirmBox(ResourceBundle.getBundleText("com.hosv3.control.DiagnosisControl.WARNNIG.CHRONIC.RELATE.DIAG"), UpdateStatus.WARNING)) {
            return false;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theChronicDB.delete(chronic);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INACTIVE);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INACTIVE);
        }
        return isComplete;
    }

    public boolean deleteDeath(Death v, Visit visit, Patient patient, Family family, UpdateStatus theUS) {
        if (!v.vn_id.isEmpty() && visit.is_discharge_doctor.equals("1")) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.DiagnosisControl.WARNNIG.DELETE.DEATH.DOCTOR.DISCHARGE"), UpdateStatus.WARNING);
            return false;
        }
        boolean confirm = theUS.confirmBox(ResourceBundle.getBundleText("com.hosv3.control.DiagnosisControl.CONFIRM.DELETE.DEATH"), UpdateStatus.WARNING);
        if (!confirm) {
            return false;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (family != null) {
                theHO.theFamily.discharge_status_id = Active.isDisable();
                theHO.theFamily.discharge_date_time = theHO.date_time;
                theHosDB.theFamilyDB.updateDischarge(theHO.theFamily);
            }
            if (patient != null) {
                theHO.thePatient.discharge_status_id = "0";
                theHO.thePatient.discharge_date_time = theHO.date_time;
                theHosDB.thePatientDB.updatePatientDischar(theHO.thePatient);
            }
            v.active = Active.isDisable();
            theHosDB.theDeathDB.update(v);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INACTIVE, ResourceBundle.getBundleText("com.hosv3.control.DiagnosisControl.DELETE.DEATH"));
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVisitSubject.notifyDischargeDoctor(ResourceBundle.getBundleText("com.hosv3.control.DiagnosisControl.DELETE.DEATH") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);

            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INACTIVE, ResourceBundle.getBundleText("com.hosv3.control.DiagnosisControl.DELETE.DEATH"));
        }
        return isComplete;
    }

    /**
     * amp:19/04/2549
     *
     * @param visit
     * @param vDx10
     * @param row
     * @return
     */
    public boolean deleteDiagnosisIcd10(Visit visit, Vector vDx10, int[] row) {
        if (row.length == 0) {
            theUS.setStatus(ResourceBundle.getBundleGlobal("TEXT.NO.SELECTED.ITEM.DELETE"), UpdateStatus.WARNING);
            return false;
        }
        if (!theUS.confirmBox(ResourceBundle.getBundleText("com.hosv3.control.DiagnosisControl.CONFIRM.DELETE.DX.ICD10"), UpdateStatus.WARNING)) {
            return false;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (visit == null) {
                theUS.setStatus(ResourceBundle.getBundleGlobal("VALIDATE.VISITED"), UpdateStatus.WARNING);
                return false;
            }
            for (int i = 0; i < row.length; i++) {
                DiagIcd10 diagicd10 = (DiagIcd10) vDx10.get(row[i]);
                if (diagicd10 == null) {
                    theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.DiagnosisControl.NO.ICD10"), UpdateStatus.WARNING);
                    continue;
                }
                int ret = intDeleteDiagIcd10(diagicd10, true);
                if (ret != 0) {
                    return false;
                }
            }
            //��Ǩ�ͺ�ó�ź��¡�� Icd10 ��� ���ǵ�ͧ��͹�����͹��˹��·ҧ���ᾷ��
            theHO.vDiagIcd10 = theHosDB.theDiagIcd10DB.selectByVidSort(visit.getObjectId());
            theConnectionInf.getConnection().commit();
            isComplete = true;

        } catch (Exception ex) {
            theHO.vDiadIcd10Cancel = null;
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INACTIVE, ResourceBundle.getBundleText("com.hosv3.control.DiagnosisControl.DELETE.ICD10"));
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theDiagnosisSubject.notifyManageDiagIcd10(ResourceBundle.getBundleText("com.hosv3.control.DiagnosisControl.DELETE.ICD10") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);

            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INACTIVE, ResourceBundle.getBundleText("com.hosv3.control.DiagnosisControl.DELETE.ICD10"));
        }
        return isComplete;

    }

    /**
     * ��Ǩ�ͺ������ջѭ�ҡ��ź�����Ũ�����Ǩ�ͺ���ź�� ��Ш�������ź����
     *
     * @param diagicd10
     * @param can_delete
     * @return
     * @throws Exception
     */
    public int intDeleteDiagIcd10(DiagIcd10 diagicd10, boolean can_delete) throws Exception {
        String cur_emp = theHO.theEmployee.getObjectId();
        if (cur_emp.equals(diagicd10.diag_icd10_staff_record) && !can_delete) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.DiagnosisControl.NO.DELETE.ICD10.BY.OTHER"), UpdateStatus.WARNING);
            return 1;
        }
        diagicd10.diag_icd10_active = Active.isDisable();
        diagicd10.diag_icd10_cancel_date_time = this.theLookupControl.intReadDateTime();
        diagicd10.diag_icd10_staff_cancel = this.theHO.theEmployee.getObjectId();
        theHosDB.theDiagIcd10DB.update(diagicd10);

        //pu:������Ҽ��������բ������ä������ѧ㹵��ҧ t_surveil �������
        Surveil surveil = theHosDB.theSurveilDB.selectByPatientIdAndIcd(theHO.thePatient.getObjectId(), diagicd10.icd10_code);
        if (surveil != null && surveil.getObjectId() != null) {
            if (surveil.vn_id.equals(diagicd10.visit_id)) {
                theHosDB.theSurveilDB.delete(surveil);
            } else {
                theHosDB.theSurveilDB.update(surveil);
            }
        }

        //pu:������Ҽ��������բ������ä������ѧ㹵��ҧ t_chronic �������
//        Chronic chronic = theHosDB.theChronicDB.selectByPatientAndIcd(theHO.thePatient.getObjectId(), diagicd10.icd10_code);
        // ��������ҡ�÷ӧҹ���ѹ�֡����ء���駵�����ŧ icd10 ����� visit �١������� ���� 1 �� ����� 1 rec ��ҹ�� ��͹ź�ѹź�繢ͧ����� ����¹��ź�ҡ visit ���᷹
        Chronic chronic = theHosDB.theChronicDB.selectByVisitAndIcd(theHO.theVisit.getObjectId(), diagicd10.icd10_code);
        if (chronic != null && chronic.getObjectId() != null) {
            if (chronic.vn_id.equals(diagicd10.visit_id)) {
                theHosDB.theChronicDB.delete(chronic);
            } else {
                chronic.active = "0";
                chronic.staff_cancel = theHO.theEmployee.getObjectId();
                chronic.cancel_datetime = theHO.date_time;
                theHosDB.theChronicDB.update(chronic);
            }
        }

        Uncontagious uncontagious = intCheckUncontagious(theHO.thePatient.family_id, diagicd10);
        if (uncontagious != null
                && uncontagious.getObjectId() != null
                && uncontagious.visit_id.equals(diagicd10.visit_id)) {
            theHosDB.theUncontagiousDB.delete(uncontagious);
        }
        //����յ����蹷������� Vector ���ä�������ӡѺ��������� ����� insert ����
        for (int i = 0; i < theHO.vDiagIcd10.size(); i++) {
            DiagIcd10 diagicd10Temp = (DiagIcd10) theHO.vDiagIcd10.get(i);
            if (diagicd10Temp.getObjectId().equals(diagicd10.getObjectId())) {
                continue;
            }
            uncontagious = intCheckUncontagious(theHO.thePatient.family_id, diagicd10Temp);
            if (uncontagious != null) {
                intSaveUncontagious(uncontagious);
                break;
            }
        }
        return 0;
    }

    public void deleteDiagnosisIcd9(Vector dx9, int row, Vector paticipateor) {
        if (row == -1) {
            theUS.setStatus(ResourceBundle.getBundleGlobal("TEXT.NO.SELECTED.ITEM.DELETE"), UpdateStatus.WARNING);
            return;
        }
        DiagIcd9 diagicd9 = (DiagIcd9) dx9.get(row);
        if (!theUS.confirmBox(ResourceBundle.getBundleText("com.hosv3.control.DiagnosisControl.CONFIRM.DELETE.ICD9"), UpdateStatus.WARNING)) {
            return;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            intDeleteDiagnosisIcd9(diagicd9, paticipateor);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INACTIVE, ResourceBundle.getBundleText("com.hosv3.control.DiagnosisControl.DELETE.ICD9"));
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theDiagnosisSubject.notifyManageDiagIcd9(ResourceBundle.getBundleText("com.hosv3.control.DiagnosisControl.DELETE.ICD9") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INACTIVE, ResourceBundle.getBundleText("com.hosv3.control.DiagnosisControl.DELETE.ICD9"));
        }
    }

    public void intDeleteDiagnosisIcd9(DiagIcd9 diagicd9, Vector paticipateor) throws Exception {
        if (diagicd9 == null) {
            return;
        }
        diagicd9.diag_icd9_active = Active.isDisable();
        diagicd9.diag_icd9_cancel_date_time = theLookupControl.intReadDateTime();
        diagicd9.diag_icd9_staff_cancel = this.theHO.theEmployee.getObjectId();
        theHosDB.theDiagIcd9DB.updateActive(diagicd9);
        theHO.vDiagIcd9 = theHosDB.theDiagIcd9DB.selectByVid(theHO.theVisit.getObjectId(), Active.isEnable());
        intDeleteParticipate(diagicd9, paticipateor);
    }

    public boolean deleteSurveil(Surveil v, UpdateStatus theUS) {
        if (v == null || v.getObjectId() == null) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.DiagnosisControl.NO.SURVEIL.DATA"), UpdateStatus.WARNING);
            return false;
        }
        if (!v.vn_id.equals(theHO.theVisit.getObjectId())) {
            theUS.setStatus(ResourceBundle.getBundleGlobal("com.hosv3.control.DiagnosisControl.CANNOT.EDIT.LAST.VISIT"), UpdateStatus.WARNING);
            return false;
        }
        if (!theUS.confirmBox(ResourceBundle.getBundleText("com.hosv3.control.DiagnosisControl.WARNNIG.CHRONIC.RELATE.DIAG"), UpdateStatus.WARNING)) {
            return false;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theHosDB.theSurveilDB.delete(v);
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return isComplete;
    }

    /*
     * public Vector listChronicByIcd10(String pk) { Vector vc = new Vector();
     * ChronicDB theChronicDB = new ChronicDB(theConnectionInf);
     * theConnectionInf.open(); try{ vc = theChronicDB.selectChronicByIcd10(pk);
     * theConnectionInf.close(); } catch(Exception ex) {
     * ex.printStackTrace(Constant.getPrintStream()); theConnectionInf.close();
     * return null; } return vc; }
     */
    /**
     * Creates a new instance of editChronicReq
     */
    private int intSaveChronic(Chronic chronic, UpdateStatus theUS) throws Exception {
        return intSaveChronic(chronic, theUS, 0);
    }

    private int intSaveChronic(Chronic chronic, UpdateStatus theUS, int panel) throws Exception {
        if (chronic == null) {
            return 1;
        }
        if (panel == 0 && chronic.vn_id != null && !chronic.vn_id.isEmpty()
                && !chronic.vn_id.equals(theHO.theVisit.getObjectId())) {
            theUS.setStatus(ResourceBundle.getBundleGlobal("com.hosv3.control.DiagnosisControl.CANNOT.EDIT.LAST.VISIT"), UpdateStatus.WARNING);
            return 1;
        }
        int date_valid = DateUtil.countDateDiff(chronic.date_dx, DateUtil.getTextCurrentDate(theConnectionInf));
        if (date_valid > 0) {
            theUS.setStatus(ResourceBundle.getBundleText("com.hosv3.control.DiagnosisControl.CHECK.FEATURE.DATE.ONSET.SYMPTOMS"), UpdateStatus.WARNING);
            return 2;
        }
        chronic.modify_datetime = theHO.date_time;
        chronic.staff_modify = theHO.theEmployee.getObjectId();
        if (chronic.getObjectId() == null) {
            chronic.active = Active.isEnable();
            if ((chronic.family_id == null || chronic.family_id.isEmpty()) && this.theHO.theFamily != null) {
                chronic.family_id = this.theHO.theFamily.getObjectId();
            }
            if ((chronic.patient_id == null || chronic.patient_id.isEmpty()) && this.theHO.thePatient != null) {
                chronic.patient_id = this.theHO.thePatient.getObjectId();
                chronic.hn = this.theHO.thePatient.hn;
            }
            chronic.staff_record = theHO.theEmployee.getObjectId();
            chronic.date_update = theHO.date_time;
            if ((chronic.vn_id == null || chronic.vn_id.isEmpty()) && this.theHO.theVisit != null) {
                chronic.vn = theHO.theVisit.vn;
                chronic.vn_id = theHO.theVisit.getObjectId();
            }
            theHosDB.theChronicDB.insert(chronic);
        } else {
            theHosDB.theChronicDB.update(chronic);
        }
        return 0;
    }

    /**
     * @Author amp
     * @date 18/04/2549
     * @deprecated henbe unused non complex function not need tobe function
     */
    private void intSaveUncontagious(Uncontagious uncontagious) throws Exception {
        if (uncontagious.getObjectId() == null) {
            theHosDB.theUncontagiousDB.insert(uncontagious);
        } else {
            theHosDB.theUncontagiousDB.update(uncontagious);
        }
    }

    /**
     * Creates a new instance of editDeathReq
     *
     * @param patient
     * @param family
     * @param theUS
     * @param death
     * @return
     */
    public String saveDeath(Patient patient, Family family, Death death, UpdateStatus theUS) {
        if (death == null) {
            theUS.setStatus(("����բ����š�õ�·��зӡ�úѹ�֡"), UpdateStatus.WARNING);
            return "";
        }
        if (death.ddeath.length() < 10) {
            theUS.setStatus(("��س��к��ѹ ���� ������ª��Ե"), UpdateStatus.WARNING);
            return "";
        }

        if (death.cdeath.length() > 6) {
            theUS.setStatus(("��س��к����˵ء�õ�� ������ ICD-10 5 ��ѡ"), UpdateStatus.WARNING);
            return "";
        }
        if (death.odiseae.length() > 6) {
            theUS.setStatus(("��س��к��ä����������蹷�����˵�˹ع ������ ICD-10 5 ��ѡ"), UpdateStatus.WARNING);
            return "";
        }
        boolean res = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            int date_valid = 0;
            if (death.ddeath.length() >= 16) {
                date_valid = DateUtil.countDateDiff(theLookupControl.intReadDateTime(), death.ddeath);
            }
            if (date_valid < 0) {
                theUS.setStatus(("�ѹ������ª��Ե��ͧ������ѹ�͹Ҥ�"), UpdateStatus.WARNING);
                throw new Exception("�ѹ������ª��Ե��ͧ������ѹ�͹Ҥ�");
            }

            /////////////////////////////////////////////////
            if (death.getObjectId() == null) {
                if (patient != null) {
                    patient.discharge_status_id = Active.isEnable();
                    patient.discharge_date_time = theHO.date_time;
                    theHosDB.thePatientDB.updatePatientDischar(patient);
                    death.patient_id = patient.getObjectId();
                    if (theHO.thePatient != null && theHO.thePatient.getObjectId().equals(patient.getObjectId())) {
                        theHO.thePatient = patient;
                    }
                }
                if (family != null) {
                    family.discharge_status_id = Active.isEnable();
                    theHosDB.theFamilyDB.updateDischarge(family);
                    death.family_id = family.getObjectId();
                    if (theHO.theFamily != null && theHO.theFamily.getObjectId().equals(family.getObjectId())) {
                        theHO.theFamily = family;
                    }
                }
                theHosDB.theDeathDB.insert(death);
                if (theHO.theVisit != null && theHO.theVisit.getObjectId().equals(death.vn_id)) {
                    if (theHO.theVisit.visit_type.equals(VisitType.OPD)) {
                        theHO.theVisit.discharge_opd_status = DischargeOpd.DEATH_OPD;
                    } else {
                        theHO.theVisit.discharge_ipd_type = DischargeType.DEATH;
                        theHO.theVisit.discharge_ipd_status = DischargeStatus.DEATH;
                    }
                    theHosDB.theVisitDB.updateDischargeDoctor(theHO.theVisit);
                }
            } else {
                //pu �红����š�õ�¢���ѧ����˹��·ҧ����Թ
                theHosDB.theDeathDB.update(death);
            }
            theHO.theDeath = death;
            theUS.setStatus(ResourceBundle.getBundleText("�ѹ�֡�����š�õ��") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            res = true;
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
        } finally {
            theConnectionInf.close();
        }
        if (res) {
            theHS.thePatientSubject.notifySavePatient(ResourceBundle.getBundleText("�ѹ�֡�����š�õ��") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        }
        return res ? "Complete" : "Error Exception";
    }

    public ICD10 getICD10ByNumber(String code) {
        ICD10 icd10 = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            icd10 = theHosDB.theICD10DB.selectEqCode(code);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return icd10;
    }

    /*
     * private Surveil intCheckSurveil(DiagIcd10 dx10) throws Exception ������
     * ICD10 ���ѹ�֡仵�Ǩ�ͺ�Ѻ���ҧ������ä ICD10 -
     * �鹢������ä������ѧ�ͧ������������ä���ǡѹ ��зӡ�� update
     * �ѹ����ѡ��������ѹ�Ѩ�غѹ �ҡ����ա�кѹ�֡�������ä surveil �������
     * - ����¹����ա���觤�������������ͺѹ�֡�� record
     * �����������������е�ͧ�ա�����繻���ѵ��������� @modifier pu @date
     * 17/09/2008
     */
    private Surveil intCheckSurveil(String patient_id, DiagIcd10 dx10) throws Exception {
        if (dx10.icd10_code.isEmpty()) {
            return null;
        }
        // Sompraosng 3.9.28b10 fix to select from icd code (BUG # 47)
        Vector vIcd10SurveilAll = theHosDB.theGroupIcd10DB.selectByIcd(dx10.icd10_code);
        //pu: �������� ICD10 㹵��ҧ b_group_icd10 �ʴ����������ä������ѧ
        if (vIcd10SurveilAll == null || vIcd10SurveilAll.isEmpty()) {
            return null;
        }
        GroupIcd10 groupicd10 = (GroupIcd10) vIcd10SurveilAll.get(0);
        //pu:������ʷ��������繤����ҧ �ӡ�úѹ�֡������� t_surveil �����
        if (groupicd10 == null
                || groupicd10.group506.isEmpty()
                || groupicd10.group506.equals("99")) {
            return null;
        }
        Surveil old_surveil = theHosDB.theSurveilDB.selectByPatientIdAndIcd(
                patient_id, dx10.icd10_code);
        //�ա�û�Ѻ�������������Ǩ�ͺ��дѺ visit ��ҹ���ҡ������ visit ���ǡѹ�кѹ�֡�������
        if (old_surveil != null && old_surveil.vn_id.equals(theHO.theVisit.getObjectId())) {
            return old_surveil;
        }
        //������е�Ǩ�ͺ���������¡�������������������������ѧ������
        Surveil yes = new Surveil();
        yes.vn_id = theHO.theVisit.getObjectId();
        yes.patient_id = theHO.thePatient.getObjectId();
        yes.family_id = theHO.theFamily.getObjectId();
        yes.hn = theHO.thePatient.hn;
        yes.vn = theHO.theVisit.vn;
        yes.icd_code = dx10.icd10_code;
        // ���������ѹ���������ҡ PrimarySymptom �����·���ش�ء���� ������������ѹ��� visit (3.9.28b08)
        String illdate = theHosDB.thePrimarySymptomDB.startIllDateByVisitId(theHO.theVisit.getObjectId());
        if (illdate != null && !illdate.isEmpty()) {
            yes.illdate = illdate;
        } else {
            yes.illdate = theHO.theVisit.begin_visit_time.substring(0, 10);
        }
        yes.patient_status = ChronicDischargeStatus.NoData;
        yes.complica = "";// "226"; // ����Һ
        yes.organism = "";// "8010"; // ����к� ����Һ
        return yes;
    }

    /*
     * private Chronic intCheckChronic(DiagIcd10 dx10) throws Exception ������
     * ���ѹ�֡仵�Ǩ�ͺ�Ѻ���ҧ����駤���ä������ѧ - �鹢����� Chronic
     * �ͧ������������ä���ǡѹ ��зӡ�� update �ѹ����ѡ��������ѹ�Ѩ�غѹ
     * �ҡ����ա�кѹ�֡�������ä chronic ������� @modifier pu @date 17/09/2008
     */
    private Chronic intCheckChronic(String patient_id, DiagIcd10 dx10) throws Exception {
        if (dx10.icd10_code.isEmpty()) {
            return null;
        }
        //pu 16/10/2551 : ������ICD10 㹵��ҧ b_group_icd10 �·�������ä������ѧ��ͧ���١¡��ԡ
        String code3d = dx10.icd10_code.substring(0, 3);
        Vector vcIcdChronicAll = theHosDB.theGroupIcd10DB.selectByChonicActive(code3d);
        //pu: �������� ICD10 㹵��ҧ b_group_icd10 �ʴ����������ä������ѧ
        if (vcIcdChronicAll == null || vcIcdChronicAll.isEmpty()) {
            return null;
        }
        GroupIcd10 icd10_chronic = (GroupIcd10) vcIcdChronicAll.get(0);
        //pu:������ʷ��������繤����ҧ �ӡ�úѹ�֡������� t_chronic �����
        if (icd10_chronic == null || icd10_chronic.groupchronic.isEmpty()
                || icd10_chronic.groupchronic.equals("99")) {
            return null;
        }
        Chronic old_chronic = theHosDB.theChronicDB.selectByPatientAndIcd(patient_id, dx10.icd10_code);
        if (old_chronic != null && old_chronic.vn_id.equals(theHO.theVisit.getObjectId())) {
            return old_chronic;
        }

        //������е�Ǩ�ͺ���������¡�������������������������ѧ������
        Chronic yes = new Chronic();
        yes.family_id = theHO.theFamily.getObjectId();
        yes.hn = theHO.thePatient.hn;
        yes.patient_id = theHO.thePatient.getObjectId();
        yes.vn = theHO.theVisit.vn;
        yes.vn_id = theHO.theVisit.getObjectId();
        yes.chronic_icd = dx10.icd10_code;
        // ���������ѹ����������������Ҩҡ��������� �����������PrimarySymptom �����·���ش ������������ѹ����Ѻ��ԡ�� (3.9.28b08)
        if (old_chronic == null || old_chronic.date_dx.isEmpty()) {
            String illdate = theHosDB.thePrimarySymptomDB.startIllDateByVisitId(theHO.theVisit.getObjectId());
            if (illdate != null && !illdate.isEmpty()) {
                yes.date_dx = illdate;
            } else {
                yes.date_dx = theHO.theVisit.begin_visit_time.substring(0, 10);
            }
        } else {
            yes.date_dx = old_chronic.date_dx;//�ѹ����������������Ҩҡ���������
        }
        yes.type_dish = ChronicDischargeStatus.NoData;
        yes.date_update = "";
        yes.detail = "";
        return yes;
    }

    /*
     * @author amp @date 18/04/2549 @see ��Ǩ�ͺ������� Icd10
     * ���ѹ�֡仵ç�Ѻ�ä�Դ��������ä���Դ������ú�ҧ @param
     * patient_id,DiagIcd10 @return Uncontagious
     */
    private Uncontagious intCheckUncontagious(String family_id, DiagIcd10 dx10) throws Exception {
        if (dx10.icd10_code.isEmpty()) {
            return null;
        }
        String key = dx10.icd10_code.substring(0, 3);
        GroupIcd10 groupicd10 = theHosDB.theGroupIcd10DB.selectByIcdCode(key);
        if (groupicd10 == null) {
            return null;
        }
        if ("7830000000000".equals(groupicd10.group_disease)) {
            return null;
        }
        Uncontagious yes = theHosDB.theUncontagiousDB.selectByFamilyIdAndDiseaseId(family_id, groupicd10.group_disease);
        if (yes == null) {
            yes = new Uncontagious();
            yes.active = Active.isEnable();
            yes.cancel_datetime = "";
            Disease di = theHosDB.theDiseaseDB.selectByPK(groupicd10.group_disease);
            if (di != null) {
                yes.contagious_type = di.isContagiousDisease;
            } else {
                yes.contagious_type = "";
            }
            yes.disease_id = groupicd10.group_disease;
            yes.family_id = family_id;
            yes.getwell = "0";
            yes.icd10 = dx10.icd10_code;
            yes.modify_datetime = "";
            yes.patient_id = theHO.thePatient.getObjectId();
            yes.record_datetime = theLookupControl.intReadDateTime();
            yes.staff_cancel = "";
            yes.staff_modify = "";
            yes.staff_recode = theHO.theEmployee.getObjectId();
            yes.survey_date = "";
            yes.visit_id = theHO.theVisit.getObjectId();
        }
        return yes;
    }

    public void intSaveDiagIcd10(DiagIcd10 diagicd10, Vector vDx10, UpdateStatus theUS) throws Exception {
        intSaveDiagIcd10(diagicd10, vDx10, theUS, false);
    }

    /**
     *
     * pattern ���١��ͧ��Ϳѧ�ѹ������¡�֡ŧ������� parameter
     * �е�ͧ�ҡ��������� ��ǹ�ѧ�ѹ����е�ͧ����������觤�� default
     * ����ѧ�ѹ����᷹
     *
     * @param diagicd10
     * @param vDx10
     * @param theUS
     * @param isShowDialog
     * @return
     * @throws Exception
     */
    public void intSaveDiagIcd10(DiagIcd10 diagicd10, Vector vDx10, UpdateStatus theUS, boolean isShowDialog) throws Exception {
        String visit_id = theHO.theVisit.getObjectId();
        String patient_id = theHO.thePatient.getObjectId();
        String family_id = theHO.theFamily.getObjectId();

        if (diagicd10 == null || diagicd10.icd10_code.isEmpty()) {
            theUS.setStatus(ResourceBundle.getBundleText("��س����͡�����ä ICD10")
                    + ResourceBundle.getBundleText("...."), UpdateStatus.WARNING);
            throw new Exception("cn");
        }
        if (vDx10 == null) {
            vDx10 = theHosDB.theDiagIcd10DB.selectByVidSort(visit_id);
        }
        if (checkCODEDx10Same(diagicd10, vDx10)) {
            theUS.setStatus(("�������ä���ѹ�֡���Ѻ�����«�ӡѹ"), UpdateStatus.WARNING);
            throw new Exception("cn");
        }
        if (diagicd10.doctor_kid.isEmpty()) {
            theUS.setStatus(("��س��кت���ᾷ��"), UpdateStatus.WARNING);
            throw new Exception("cn");
        }
        Employee doctor = theLookupControl.readEmployeeById(diagicd10.doctor_kid);
        if (doctor != null && !(doctor.authentication_id.equals(Authentication.DOCTOR)
                || doctor.authentication_id.equals(Authentication.THAI_DOCTOR)
                || doctor.authentication_id.equals(Authentication.PHYSICAL_THERAPY))) {
            theUS.setStatus(("���ŧ���ʵ�ͧ�� ᾷ��/ᾷ��Ἱ��/�ѡ����Ҿ�ӺѴ ��ҹ��"), UpdateStatus.WARNING);
            throw new Exception("cn");
        }
        // Somprasong 01102012 check primary diag
        // IPD 1 visit / 1 primary
        // OPD 1 clinic / 1 primary
        if (diagicd10.type.equals("1")) {
            boolean canAddPrimary = true;
            Vector<X39Persistent> listDiagIcd10s = vDx10;
            String msg = "";
            if (listDiagIcd10s != null) {
                for (X39Persistent icx : listDiagIcd10s) {
                    DiagIcd10 ic = (DiagIcd10) icx;
                    if (ic.type.equals("1") && !ic.getObjectId().equals(diagicd10.getObjectId())) {
                        // IPD must have only 1 primary
                        if ("1".equals(theHO.theVisit.visit_type)) {
                            msg = "IPD �ѹ�֡�� 1 Visit : 1 Primary Diagnosis.";
                            canAddPrimary = false;
                            break;
                        } else // OPD 1 clinic have 1 primary
                        if (ic.clinic_kid.equals(diagicd10.clinic_kid)) {
                            msg = "OPD �ѹ�֡�� 1 Clinic : 1 Primary Diagnosis.";
                            canAddPrimary = false;
                            break;
                        }
                    }
                }
                if (!canAddPrimary) {
                    theUS.setStatus(String.format("�������ö�ѹ�֡ Primary Diagnosis �Թ��˹��� (%s)", msg), UpdateStatus.WARNING);
                    throw new Exception("cn");
                }
            }
        }
        // �������غѵ��˵بҡ��� V,X,W,Y �繵�Ǩ�ҡ diag_icd10_accident = 1 ᷹
        if (diagicd10.diag_icd10_accident.equals("1")) {
            if (diagicd10.type.equals(Dxtype.getPrimaryDiagnosis())) {
                theUS.setStatus(("�������ö�ѹ�֡�����ä㹡���� ExternalCause ����������ä��ѡ��"), UpdateStatus.WARNING);
                throw new Exception("cn");
            }
        }
        ICD10 icd = theHosDB.theICD10DB.selectEqCode(diagicd10.icd10_code);
        if (icd == null) {
            theUS.setStatus(("��辺���� ICD10 ����͡��سҤ����ʨҡ˹�Ҩʹ�ҹ����"), UpdateStatus.WARNING);
            throw new Exception("cn");
        }
        //amp:03/01/2550:��Ǩ�ͺ�ѹ����͹Ҥ�
        try {
            if (theLookupControl.isDateFuture(diagicd10.diagnosis_date)) {
                theUS.setStatus(("�ѹ������ҷ��ŧ�����ä �������͹Ҥ������"), UpdateStatus.WARNING);
                throw new Exception("cn");
            }
        } catch (Exception e) {
            theUS.setStatus(("�ѹ������ҷ��ŧ�����ä���١��ͧ"), UpdateStatus.WARNING);
            throw new Exception("cn");
        }
        if (diagicd10.getObjectId() == null) {
            diagicd10.visit_id = visit_id;
            if (diagicd10.diag_icd10_staff_record.isEmpty()) {
                diagicd10.diag_icd10_staff_record = this.theHO.theEmployee.getObjectId();
            }
            diagicd10.diag_icd10_record_date_time = theLookupControl.intReadDateTime();
            theHosDB.theDiagIcd10DB.insert(diagicd10);
            Surveil surveil = intCheckSurveil(patient_id, diagicd10);
            intSaveSurveil(surveil, theUS);
            theHO.theSurveil = surveil;// SOmprasong add 241209
            Chronic chronic = intCheckChronic(patient_id, diagicd10);
            intSaveChronic(chronic, theUS);
            theHO.theChronic = chronic;// SOmprasong add 241209
            //amp18/04/2549 ��Ǩ�ͺ������ä�����(�Դ����������Դ���)
            //henbe 5/9/2549 ����ͧ��Ǩ�ͺ㹡óշ�����غѵ��˵�����������ä�Դ�����͹��
            //�ѹ���� null � family �����
            if (family_id != null) {
                Uncontagious uncontagious = intCheckUncontagious(family_id, diagicd10);
                if (uncontagious != null) {
                    intSaveUncontagious(uncontagious);
                }
            }
            if (!isShowDialog) {
                theHO.theSurveil = null;
                theHO.theChronic = null;
            }
        } else {
            diagicd10.visit_id = visit_id;
            if (diagicd10.diag_icd10_staff_update.isEmpty()) {
                diagicd10.diag_icd10_staff_update = this.theHO.theEmployee.getObjectId();
            }
            diagicd10.diag_icd10_update_date_time = theLookupControl.intReadDateTime();
            theHosDB.theDiagIcd10DB.update(diagicd10);
        }
    }

    /*
     *
     *
     */
    public void saveDxByStat(Visit visit, String auth) {
        if (visit == null) {
            theUS.setStatus(ResourceBundle.getBundleGlobal("VALIDATE.VISITED"), UpdateStatus.WARNING);
            return;
        }
        if (!auth.equals(Authentication.STAT) && visit.is_discharge_doctor.equals(Active.isEnable())) {
            theUS.setStatus(("����Ѻ��ԡ�âͧ�����¶١��˹��·ҧ���ᾷ������"), UpdateStatus.WARNING);
            return;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            if (auth.equals(Authentication.STAT)) {
                visit.visit_modify_date_time = theLookupControl.intReadDateTime();
                visit.visit_modify_staff = theHO.theEmployee.getObjectId();
                theHosDB.theVisitDB.updateDxByStat(visit);
                theUS.setStatus(ResourceBundle.getBundleText("��úѹ�֡ Dx ���ǪʶԵ�") + " "
                        + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            } else {
                theUS.setStatus(("���͹حҵ�������ҹ��蹷�������ҹ�ǪʶԵԺѹ�֡Dx"), UpdateStatus.WARNING);
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT, "�ѹ�֡��õ�Ǩ�ͧ�ǪʶԵ�");
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT, "�ѹ�֡��õ�Ǩ�ͧ�ǪʶԵ�");
        }
    }

    public int saveSurveil(Surveil s, UpdateStatus theUS) {
        return saveSurveil(s, theUS, 0);
    }

    public int saveSurveil(Surveil s, UpdateStatus theUS, int panel) {
        int ret = 99;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ret = intSaveSurveil(s, theUS, panel);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
        } finally {
            theConnectionInf.close();
        }
        if (ret == 0) {
            theUS.setStatus(ResourceBundle.getBundleText("��úѹ�֡�������ä������ѧ") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);

            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        }
        return ret;
    }

    public int saveChronic(Chronic s, UpdateStatus theUS) {
        return saveChronic(s, theUS, 0);
    }

    public int saveChronic(Chronic s, UpdateStatus theUS, int panel) {
        int ret = -1;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ret = intSaveChronic(s, theUS, panel);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
        } finally {
            theConnectionInf.close();
        }
        if (ret == 0) {
            theUS.setStatus(ResourceBundle.getBundleText("��úѹ�֡�������ä������ѧ") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        }
        return ret;
    }

    /**
     *
     * @modifier pu
     * @date 17/09/2008
     */
    private int intSaveSurveil(Surveil surveil, UpdateStatus theUS) throws Exception {
        return intSaveSurveil(surveil, theUS, 0);
    }

    private int intSaveSurveil(Surveil surveil, UpdateStatus theUS, int panel) throws Exception {
        if (surveil == null) {
            return 5;
        }
        if (panel == 0 && surveil.vn_id != null && !surveil.vn_id.isEmpty()
                && !surveil.vn_id.equals(theHO.theVisit.getObjectId())) {
            theUS.setStatus(ResourceBundle.getBundleGlobal("com.hosv3.control.DiagnosisControl.CANNOT.EDIT.LAST.VISIT"), UpdateStatus.WARNING);
            return 1;
        }
        if (surveil.illdate.isEmpty()) {
            theUS.setStatus(("��س��к��ѹ������������"), UpdateStatus.WARNING);
            return 2;
        }
        if (surveil.illdate.startsWith("20")) {
            theUS.setStatus(ResourceBundle.getBundleText("�ѹ������������ �դ�����١��ͧ") + " " + surveil.illdate
                    + " " + ResourceBundle.getBundleText("��سҵ�Ǩ�ͺ�к�"), UpdateStatus.WARNING);
            return 3;
        }
        if (surveil.illdate.compareTo(theHO.date_time) > 0) {
            theUS.setStatus(("�ѹ�����������µ�ͧ������ѹ����͹Ҥ�"), UpdateStatus.WARNING);
            return 4;
        }
        surveil.modify_date_time = theHO.date_time;
        surveil.staff_modify = theHO.theEmployee.getObjectId();
        if (surveil.getObjectId() == null) {
            surveil.staff_record = theHO.theEmployee.getObjectId();
            surveil.record_date_time = theHO.date_time;
            if (surveil.family_id.isEmpty()) {
                surveil.family_id = theHO.theFamily.getObjectId();
            }
            if (theHO.theVisit != null) {
                surveil.vn = theHO.theVisit.vn;
                surveil.vn_id = theHO.theVisit.getObjectId();
            }
            if (theHO.thePatient != null) {
                surveil.patient_id = theHO.thePatient.getObjectId();
                surveil.hn = theHO.thePatient.hn;
            }
            theHosDB.theSurveilDB.insert(surveil);
        } else {
            theHosDB.theSurveilDB.update(surveil);
        }
        return 0;
    }

    /**
     * @param participateor
     * @param diagicd9
     * @param dx9
     * @roseuid 3F83EA240222 public void saveDiagnosisIcd9(Vector
     * participateor,DiagIcd9 diagicd9,Vector dx9)
     */
    public void saveDiagnosisIcd9(Vector participateor, DiagIcd9 diagicd9, Vector dx9) {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            isComplete = intSaveDiagIcd9(participateor, diagicd9, dx9);
            theConnectionInf.getConnection().commit();

        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theDiagnosisSubject.notifyManageDiagIcd9(ResourceBundle.getBundleText("��úѹ�֡�����ѵ����") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);

            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        }
    }

    public boolean intSaveDiagIcd9(Vector participateor, DiagIcd9 diagicd9, Vector dx9) throws Exception {
        if (theHO.theVisit == null) {
            theUS.setStatus(ResourceBundle.getBundleGlobal("VALIDATE.VISITED"), UpdateStatus.WARNING);
            return false;
        }
        if (theHO.theVisit.visit_status.equals(VisitStatus.isOutProcess())) {
            theUS.setStatus(("�����¨���кǹ��������������ö��䢢�������"), UpdateStatus.WARNING);
            return false;
        }
        if (diagicd9 == null) {
            theUS.setStatus(ResourceBundle.getBundleText("��辺�����š��ŧ�����ѵ����") + " "
                    + ResourceBundle.getBundleText("��سҵ�Ǩ�ͺ"), UpdateStatus.WARNING);
            return false;
        }
        if (diagicd9.icd9_code.isEmpty()) {
            theUS.setStatus(("��س����͡ ICD9 ����ͧ��á�͹�ӡ�úѹ�֡"), UpdateStatus.WARNING);
            return false;
        }
        if (diagicd9.time_in.length() < 16) {
            theUS.setStatus(("��س��к��ѹ������ҷ��������ѵ���÷���ͧ��á�͹�ӡ�úѹ�֡"), UpdateStatus.WARNING);
            return false;
        }
        if (diagicd9.time_out.length() < 16) {
            theUS.setStatus(("��س��к��ѹ������ҷ������ش�ѵ���÷���ͧ��á�͹�ӡ�úѹ�֡"), UpdateStatus.WARNING);
            return false;
        }
        //henbe:07/06/2550:��Ǩ�ͺ�ѹ����͹Ҥ�
        try {
            if (theLookupControl.isDateFuture(diagicd9.time_in)) {
                theUS.setStatus(("�ѹ������ҷ��������ѵ���� �������͹Ҥ������"), UpdateStatus.WARNING);
                return false;
            }
        } catch (Exception e) {
            theUS.setStatus(("�ѹ������ҷ��������ѵ���� ���١��ͧ"), UpdateStatus.WARNING);
            return false;
        }
        //henbe:07/06/2550:��Ǩ�ͺ�ѹ����͹Ҥ�
        try {
            if (theLookupControl.isDateFuture(diagicd9.time_out)) {
                theUS.setStatus(("�ѹ������ҷ������ش�ѵ���� �������͹Ҥ������"), UpdateStatus.WARNING);
                return false;
            }
        } catch (Exception e) {
            theUS.setStatus(("�ѹ������ҷ������ش�ѵ���� ���١��ͧ"), UpdateStatus.WARNING);
            return false;
        }
        Date datein = DateUtil.getDateFromText(diagicd9.time_in);
        Date dateout = DateUtil.getDateFromText(diagicd9.time_out);
        if (datein != null && dateout != null) {
            boolean date_invalid = DateUtil.isDateTimeFuture(diagicd9.time_in, diagicd9.time_out);
            if (date_invalid) {
                theUS.setStatus(("�ѹ���������ѵ��������ѹ�������ش�ѵ�����ժ�ǧ������١��ͧ"), UpdateStatus.WARNING);
                return false;
            }
        }
        if (diagicd9.doctor_kid == null || diagicd9.doctor_kid.isEmpty()) {
            theUS.setStatus(("��س��кت���ᾷ��"), UpdateStatus.WARNING);
            return false;
        }
        String auth = theHO.theEmployee.authentication_id;
        diagicd9.vn = theHO.theVisit.getObjectId();
        if (!auth.equals(Authentication.STAT) && theHO.theVisit.is_discharge_doctor.equals(Active.isEnable())) {
            theUS.setStatus(("��˹��·ҧ���ᾷ������"), UpdateStatus.WARNING);
            return false;
        }
        if (diagicd9.type.equals(Active.isEnable()) && dx9 != null) {
            for (int i = 0, size = dx9.size(); i < size; i++) {
                DiagIcd9 ic = (DiagIcd9) dx9.get(i);
                if (ic.type.equals(Active.isEnable())
                        && diagicd9.getObjectId() != null
                        && !diagicd9.getObjectId().equals(ic.getObjectId())) {

                    theUS.setStatus(("����¡���ѵ���� Principal procedure 2 ��¡�������"), UpdateStatus.WARNING);
                    return false;
                }
                if (ic.type.equals(Active.isEnable()) && diagicd9.getObjectId() == null) {

                    theUS.setStatus(("����¡���ѵ���� Principal procedure 2 ��¡�������"), UpdateStatus.WARNING);
                    return false;
                }
            }
        }
        diagicd9.diag_icd9_staff_update = this.theHO.theEmployee.getObjectId();
        diagicd9.diag_icd9_update_date_time = theHO.date_time;
        if (diagicd9.getObjectId() == null) {
            diagicd9.diag_icd9_record_date_time = theLookupControl.intReadDateTime();
            diagicd9.diag_icd9_staff_record = this.theHO.theEmployee.getObjectId();
            theHosDB.theDiagIcd9DB.insert(diagicd9);
            if (dx9 == null) {
                dx9 = new Vector();
            }
            dx9.add(diagicd9);
        } else {
            theHosDB.theDiagIcd9DB.update(diagicd9);
        }
        theHO.vDiagIcd9 = theHosDB.theDiagIcd9DB.selectByVid(theHO.theVisit.getObjectId(), Active.isEnable());
        intSaveParticipateOr(participateor, diagicd9, theHO.theVisit);
        return true;
    }

    public Vector listICD9ByIdName(String pk) {
        Vector vector = null;
        theConnectionInf.open();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vector = theHosDB.theICD9DB.selectLikeCodeName(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vector;
    }

    public Vector listDiagnosisIcd9(String vn) {
        Vector result = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theDiagIcd9DB.selectByVid(vn, Active.isEnable());
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    public ICD9 listIcd9ByPk(String pk) {
        ICD9 ic9 = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ic9 = theHosDB.theICD9DB.selectEqCode(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ic9;
    }

    public Vector listIcd10ByCode(String code) {
        return theLookupControl.listIcd10ByIdNameGroup(code, "", true);
    }

    public Vector listIcd10ByIdNameGroup(String pk, String group) {
        return listIcd10ByIdNameGroup(pk, group, 1);
    }

    public Vector listIcd10ByIdNameGroup(String pk, String group, int active) {
        return theLookupControl.listIcd10ByIdNameGroup(pk, group, false, active);
    }

    public Vector listDiagnosisIcd10(String vid) {
        Vector vector = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vector = theHosDB.theDiagIcd10DB.selectByVidSort(vid);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vector;
    }

    public ICD10 listIcd10ByPk(String pk) {
        ICD10 ic10 = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ic10 = theHosDB.theICD10DB.selectByPK(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ic10;
    }

    public Dxtype listDxtypeByPk(String pk) {
        Dxtype dx = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            dx = theHosDB.theDxtypeDB.selectByPK(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return dx;
    }

    /**
     * @Author: sumo
     * @date: 2/6/2549
     * @see: ��Ѻ����ź������������ҵѴ���纻���ѵ���ź, ���ҷ��ź
     * ���ʶҹТͧ������������ҵѴ�� '0'
     * @param participateor
     * @param row
     */
    public void deleteParticipateOr(Vector participateor, int[] row) {
        if (row.length == 0) {
            theUS.setStatus(ResourceBundle.getBundleText("��س����͡����������") + " "
                    + ResourceBundle.getBundleText("��͹�ӡ��ź����������"), UpdateStatus.WARNING);
            return;
        }
        if (!theUS.confirmBox(ResourceBundle.getBundleText("�׹�ѹ����¡��ԡ��Һ�ż��������� ���������"), UpdateStatus.WARNING)) {
            return;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (int i = row.length - 1; i >= 0; i--) {
                ParticipateOr nurse = (ParticipateOr) participateor.get(row[i]);
                nurse.staff_update = theHO.theEmployee.getObjectId();
                nurse.time_update = theLookupControl.intReadDateTime();
                nurse.active = Active.isDisable();
                theHosDB.theParticipateOrDB.update(nurse);
                participateor.remove(row[i]);
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.TRANSACTION_DELETE);
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theDiagnosisSubject.notifyDeleteParticipateOr(ResourceBundle.getBundleText("���ź��Һ�ż�����������ҵѴ")
                    + " " + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_DELETE);
        }
    }

    public Vector listParticipateOrByDiagIcd9(String dx9id) {
        Vector result = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theParticipateOrDB.selectByDxIcd9(dx9id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    public Vector listParticipateOr(String code, String vn) {
        Vector result = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            result = theHosDB.theParticipateOrDB.selectByIcd9(code, vn);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return result;
    }

    /**
     * @Author: sumo
     * @date: 2/6/2549
     * @see: ��Ѻ��������������������ҵѴ���纻���ѵ�������, ���ҷ������
     * ���ʶҹТͧ������������ҵѴ�� '1'
     * @param participateor
     * @param diagicd9
     * @param visit
     * @throws Exception
     */
    public void intSaveParticipateOr(Vector participateor, DiagIcd9 diagicd9, Visit visit) throws Exception {
        if (participateor == null || participateor.isEmpty()) {
            return;
        }
        ParticipateOr par = (ParticipateOr) participateor.get(0);
        Vector beforPar = theHosDB.theParticipateOrDB.selectByIcd9(par.icd9_id, visit.getObjectId());
        for (int n = 0; n < participateor.size(); n++) {
            ParticipateOr part = (ParticipateOr) participateor.get(n);
            ((ParticipateOr) participateor.get(n)).t_diag_icd9_id = diagicd9.getObjectId();
            part.t_diag_icd9_id = diagicd9.getObjectId();
            //bad pattern ������ͧ�����ҧ����������������áѹ�������� diag_id ���ǹ���
            ICD9 icd = theHosDB.theICD9DB.selectByCode(diagicd9.icd9_code);
            part.icd9_id = icd.getObjectId();
            part.vn = visit.getObjectId();
            part.staff_insert = theHO.theEmployee.getObjectId();
            part.time_insert = theLookupControl.intReadDateTime();
            part.active = Active.isEnable();
            if (beforPar == null) {
                theHosDB.theParticipateOrDB.insert(part);
            } else {
                int savePO = 0;
                int end = beforPar.size();
                participateor.remove(n);
                participateor.add(n, part);
                for (int i = 0; i < beforPar.size(); i++) {
                    if (((ParticipateOr) beforPar.get(i)).employee.equals(part.employee)) {
                        savePO = 1;
                        i = end;
                    }
                }
                if (savePO == 0) {
                    theHosDB.theParticipateOrDB.insert(part);
                }
            }
        }
    }

    public ParticipateOr listParticipateOrByEM(ParticipateOr o) {
        ParticipateOr po = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            po = theHosDB.theParticipateOrDB.selectByEmAndIcd(o);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return po;
    }

    /**
     * @Author: sumo
     * @date: 2/6/2549
     * @deprecated henbe unused add in delete diagicd10 not call from gui
     *
     * @see: ��Ѻ����ź������������ҵѴ�ҡ���ź ICD9 ���纻���ѵ���ź,
     * ���ҷ��ź ���ʶҹТͧ������������ҵѴ�� '0'
     * @param diagicd9
     * @param paticipateor
     */
    public void deleteParticipateOrByIcd(DiagIcd9 diagicd9, Vector paticipateor) {
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            intDeleteParticipate(diagicd9, paticipateor);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
    }

    public void intDeleteParticipate(DiagIcd9 diagicd9, Vector paticipateor) throws Exception {
        if (paticipateor == null) {
            return;
        }
        for (int i = paticipateor.size() - 1; i >= 0; i--) {
            ParticipateOr nurse = (ParticipateOr) paticipateor.get(i);
            nurse.staff_update = theHO.theEmployee.getObjectId();
            nurse.time_update = theLookupControl.intReadDateTime();
            nurse.active = Active.isDisable();
            theHosDB.theParticipateOrDB.update(nurse);
        }
        paticipateor.removeAllElements();
    }

    public Group506 listGroup506ByIcdCode(String code) {
        Group506 po = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            po = theHosDB.theGroup506DB.selectByGroup506(code);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return po;
    }

    /**
     * @param code
     * @return
     * @deprecated henbe bad function name
     */
    public GroupIcd10 listGroupIcd10ByIcdCode(String code) {
        return readGroupIcd10ByIcdCode(code);
    }

    public GroupIcd10 readGroupIcd10ByIcdCode(String code) {
        GroupIcd10 gi = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            gi = theHosDB.theGroupIcd10DB.selectByIcdCode(code);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return gi;
    }

    public Surveil listSurveil(String vn, String icd) {
        Surveil su = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            su = theHosDB.theSurveilDB.selectByVnAndIcd(vn, icd);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return su;
    }

    /*
     * function
     */
    public Surveil listSurveilByPatientIdAndIcd(String patientId, String icd) {
        Surveil su = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            su = theHosDB.theSurveilDB.selectByPatientIdAndIcd(patientId, icd);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return su;
    }

    public Vector listIdx10V3All(String pk) {
        Vector icd10v3 = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            icd10v3 = theHosDB.theIdx10V3DB.selectByDescription(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return icd10v3;
    }

    public ICD10 listIcd10ById(String icd10Id) {
        ICD10 id = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            id = theHosDB.theICD10DB.selectByIdGroupOnly(icd10Id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return id;
    }

    public Vector selectMapVisitDx(String vn) {
        Vector v = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            v = theHosDB.theMapVisitDxDB.selectByVisit(vn);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return v;
    }

    public DxTemplate selectDxTemplateByPk(String pk) {
        DxTemplate dt = new DxTemplate();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            dt = theHosDB.theDxTemplate2DB.selectByPK(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return dt;
    }

    public Vector listDeath(String hn) {
        return listDeath("0000", "9999", hn);
    }

    public Vector listDeath(String dateFrom, String dateTo, String hn) {
        Vector vc = null;
        if (dateFrom.isEmpty() && dateTo.isEmpty() && hn.isEmpty()) {
            theUS.setStatus(("��س��к������Ţ HN ���ͪ�ǧ�ѹ������ͧ��ä������ҧ����ҧ˹��"), UpdateStatus.WARNING);
            return vc;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String sql = "select t_death.*,"
                    + "t_health_family.patient_name ||'  '|| t_health_family.patient_last_name as family_name"
                    + " from t_death "
                    + "left join t_health_family on t_health_family.t_health_family_id = t_death.t_health_family_id "
                    + "left join t_patient on t_patient.t_patient_id = t_death.t_patient_id "
                    + " where death_active  = '1'"
                    + " and (t_patient.patient_hn like '%" + hn + "' "
                    + " or t_health_family.patient_name like '" + hn + "%')";
            if (!dateFrom.equals("0000")) {
                sql += " and death_date_time >= '" + dateFrom + "' "
                        + " and death_date_time <= '" + dateTo + ",23:59'";
            }
            sql += " order by family_name ";
            vc = theHosDB.theDeathDB.eQuery(sql);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listSurveilByHn(String hn) {
        return listSurveilByHnDate("0000", "9999", hn);
    }

    public Vector listSurveilByHnDate(String dateFrom, String dateTo, String hn) {
        Vector su = null;
        if (dateFrom.isEmpty() && dateTo.isEmpty() && hn.isEmpty()) {
            theUS.setStatus(("��س��к������Ţ HN ���ͪ�ǧ�ѹ������ͧ��ä������ҧ����ҧ˹��"), UpdateStatus.WARNING);
            return su;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String sql = "select * from t_surveil"
                    + " left join t_visit on t_visit.t_visit_id = t_surveil.t_visit_id where t_surveil_id <> ''";

            if (hn.trim().length() != 0) {
                sql += " and surveil_hn like '%" + hn + "' ";
            }

            if (!dateFrom.isEmpty() && !dateTo.isEmpty()) {
                sql += " and substr(t_visit.visit_begin_visit_time,1,10) >= '" + dateFrom + "'"
                        + " and substr(t_visit.visit_begin_visit_time,1,10) <= '" + dateTo + "'";
            }
            sql += " order by surveil_hn,surveil_vn desc,surveil_icd10_number ";
            su = theHosDB.theSurveilDB.eQuery(sql);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return su;
    }

    public Chronic listChronic(String hn, String icd) {
        Chronic cn = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            cn = theHosDB.theChronicDB.selectByHnAndIcd(hn, icd);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return cn;
    }

    /*
     * function
     */
    public Chronic listChronicForDelete(String visit_id, String icd) {
        Chronic cn = new Chronic();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            cn = theHosDB.theChronicDB.selectByVisitIdAndIcd(visit_id, icd);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return cn;
    }

    public Vector listChronicByHn(String hn) {
        return listChronicByHnDate("", "", hn);
    }

    public Vector listChronicByHnDate(String dateFrom, String dateTo, String hn) {
        Vector vc = null;
        if (dateFrom.isEmpty() && dateTo.isEmpty() && hn.isEmpty()) {
            theUS.setStatus(("��س��к������Ţ HN ���ͪ�ǧ�ѹ������ͧ��ä������ҧ����ҧ˹��"), UpdateStatus.WARNING);
            return vc;
        }
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theChronicDB.selectByHnDate("%" + hn, dateFrom, dateTo);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Vector listChronicByPtidVid(String ptid, String vid) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String sql = "select * from t_chronic"
                    + " where t_patient_id = '" + ptid + "'";
            if (vid != null) {
                sql += " and t_visit_id = '" + vid + "'";
            }
            sql += " order by chronic_vn,chronic_icd10";
            vc = theHosDB.theChronicDB.eQuery(sql);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public Optype listOptypeByPk(String pk) {
        Optype op = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            op = theHosDB.theOptypeDB.selectByPK(pk);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return op;
    }

    public Death listDeathByVNHN(String vn_id, String patient_id) {
        Death d = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            d = theHosDB.theDeathDB.selectByVNHN(vn_id, patient_id);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return d;
    }

    public Transfer listDoctorTransferByVn(String vn) {
        Transfer tf = new Transfer();
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            tf = theHosDB.theTransferDB.selectDoctorByVN(vn);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return tf;
    }

    public ICD10 readIcd10ByCode(String code) {
        ICD10 icd10 = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            icd10 = theHosDB.theICD10DB.selectByCode(code);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return icd10;
    }

    public ICD9 readIcd9ByCode(String code) {
        ICD9 icd9 = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            icd9 = theHosDB.theICD9DB.selectByCode(code);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return icd9;
    }

    /**
     * ojika ���Ҽ����·�����ä Chronic ��������ŷ���˹�
     *
     * @param dateStart
     * @param dateEnd
     * @param status
     * @return
     */
    public Vector listChronicByStatusDate(String dateStart, String dateEnd, String status) {
        Vector vListChronic = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vListChronic = new Vector();
            Vector vcChronic = theHosDB.theChronicDB.selectByStatusDate(status, dateStart, dateEnd);
            for (int i = 0; i < vcChronic.size(); i++) {
                Chronic theChronic = (Chronic) vcChronic.get(i);
                ChronicReport theChronicReport = new ChronicReport();
                if (theChronic != null) {
                    theChronicReport.hn = theChronic.hn;
                    theChronicReport.vn = theChronic.vn;
                    theChronicReport.patient_address = "";
                    theChronicReport.ban = "";
                    theChronicReport.moo = "";
                    theChronicReport.road = "";
                    theChronicReport.tambon = "";
                    theChronicReport.amphur = "";
                    theChronicReport.province = "";
                    if (theChronic.patient_id.isEmpty()) {
                        continue;
                    }
                    Patient patient = theHosDB.thePatientDB.selectByPK(theChronic.patient_id);
                    if (patient == null) {
                        continue;
                    }
                    {
                        theChronicReport.fname = patient.patient_name;
                        theChronicReport.lname = patient.patient_last_name;
                        // ������������
                        String address = theLookupControl.intReadPatientAddress(patient);
                        if (!address.equalsIgnoreCase("null") && !address.isEmpty()) {
                            theChronicReport.patient_address = address;
                            theChronicReport.ban = patient.house;
                            theChronicReport.moo = patient.village;
                            theChronicReport.road = patient.road;
                            theChronicReport.tambon = theLookupControl.intReadAddressString(" " + ResourceBundle.getBundleGlobal("TEXT.SHORT.SUBDISTRICT"), patient.tambon);
                            theChronicReport.amphur = theLookupControl.intReadAddressString(" " + ResourceBundle.getBundleGlobal("TEXT.SHORT.DISTRICT"), patient.ampur);
                            theChronicReport.province = theLookupControl.intReadAddressString(" " + ResourceBundle.getBundleGlobal("TEXT.SHORT.PROVINCE"), patient.changwat);
                        }
                        theChronicReport.sex = theLookupControl.readSexSById(patient.f_sex_id);
                        theChronicReport.age = DateUtil.calculateAge(patient.getBirthDay(), theHO.date_time);
                        if (patient.getBirthDay() != null && !patient.getBirthDay().isEmpty()) {
                            if (patient.patient_birthday_true.equals("0")) {
                                String age = DateUtil.calculateAge(patient.getBirthDay(), theHO.date_time);
                                theChronicReport.age = age + " " + ResourceBundle.getBundleGlobal("TEXT.YEAR");
                                theChronicReport.age_year = age + " " + ResourceBundle.getBundleGlobal("TEXT.YEAR");
                                theChronicReport.age_month = "";
                                theChronicReport.age_day = "";
                            } else {
                                String age1 = DateUtil.calculateAgeLong(patient.getBirthDay(), theHO.date_time);
                                theChronicReport.age = age1;
                                theChronicReport.age_year = Constant.getYear(age1);
                                theChronicReport.age_month = Constant.getMonth(age1);
                                theChronicReport.age_day = Constant.getDay(age1);
                            }
                        }
                    }
                    theChronicReport.icd10 = theChronic.chronic_icd;
                    theChronicReport.date_dx = DateUtil.getDateToString(DateUtil.getDateFromText(theChronic.date_dx), false);
                    theChronicReport.date_discharge = theChronic.date_dish;
                    theChronicReport.status = Gutil.getVectorName(theLookupControl.listTypeDish(), theChronic.type_dish);
                    if (!theChronic.date_update.equalsIgnoreCase("")) {
                        theChronicReport.date_update = DateUtil.getDateToString(DateUtil.getDateFromText(theChronic.date_update), false);
                    } else {
                        theChronicReport.date_update = "";
                    }
                    theChronicReport.patient_id = theChronic.patient_id;
                    theChronicReport.visit_id = theChronic.vn_id;
                    vListChronic.add(theChronicReport);
                }
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vListChronic;
    }

    /**
     * ojika ���Ҽ����·�����ä Surveil ��������ŷ���˹�
     *
     * @param dateStart
     * @param dateEnd
     * @param status
     * @return
     */
    public Vector listSurveilByStatusDate(String dateStart, String dateEnd, String status) {
        Vector vListSurveil = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            Vector vSurveil = theHosDB.theSurveilDB.selectByStatusDate(status, dateStart, dateEnd);
            // �֧��ҨѴ��� Object chronicReport
            vListSurveil = new Vector();
            for (int i = 0, size = vSurveil.size(); i < size; i++) {
                Surveil theSurveil = (Surveil) vSurveil.get(i);
                SurveilReport theSurveilReport = new SurveilReport();
                theSurveilReport.hn = theSurveil.hn;
                theSurveilReport.vn = theSurveil.vn;
                theSurveilReport.icd10 = theSurveil.icd_code;
                theSurveilReport.date_dx = DateUtil.getDateToString(DateUtil.getDateFromText(theSurveil.illdate), false);
                theSurveilReport.date_discharge = theSurveil.illdate;
                if (theSurveil.patient_id.isEmpty()) {
                    continue;
                }

                Patient patient = theHosDB.thePatientDB.selectByPK(theSurveil.patient_id);
                theSurveilReport.fname = patient.patient_name;
                theSurveilReport.lname = patient.patient_last_name;
                theSurveilReport.contact = patient.contact_fname + " " + patient.contact_lname;
                Relation rl = theHosDB.theRelationDB.selectByPK(patient.relation);
                theSurveilReport.relation = (rl != null) ? rl.description : "";
                // ������������
                String address = theLookupControl.intReadPatientAddress(patient);
                if (!address.equalsIgnoreCase("null") && !address.isEmpty()) {
                    theSurveilReport.patient_address = address;
                    theSurveilReport.ban = patient.house;
                    theSurveilReport.moo = patient.village;
                    theSurveilReport.road = patient.road;
                    theSurveilReport.tambon = theLookupControl.intReadAddressString(" " + ResourceBundle.getBundleGlobal("TEXT.SHORT.SUBDISTRICT"), patient.tambon);
                    theSurveilReport.amphur = theLookupControl.intReadAddressString(" " + ResourceBundle.getBundleGlobal("TEXT.SHORT.DISTRICT"), patient.ampur);
                    theSurveilReport.province = theLookupControl.intReadAddressString(" " + ResourceBundle.getBundleGlobal("TEXT.SHORT.PROVINCE"), patient.changwat);
                }
                theSurveilReport.sex = theLookupControl.readSexSById(patient.f_sex_id);
                if (patient.getBirthDay() != null && !patient.getBirthDay().isEmpty()) {
                    if (patient.patient_birthday_true.equals(Active.isDisable())) {
                        String age = DateUtil.calculateAgeShort(patient.getBirthDay().substring(0, 4), theHO.date_time.substring(0, 4));
                        theSurveilReport.age = age + " " + ResourceBundle.getBundleGlobal("TEXT.YEAR");
                        theSurveilReport.age_year = age + " " + ResourceBundle.getBundleGlobal("TEXT.YEAR");
                        theSurveilReport.age_month = "";
                        theSurveilReport.age_day = "";
                    } else {
                        String age1 = DateUtil.calculateAgeLong(patient.getBirthDay(), theHO.date_time);
                        theSurveilReport.age = age1;
                        theSurveilReport.age_year = Constant.getYear(age1);
                        theSurveilReport.age_month = Constant.getMonth(age1);
                        theSurveilReport.age_day = Constant.getDay(age1);
                    }
                }
                Visit theVisit = theHosDB.theVisitDB.selectByPK(theSurveil.vn_id);
                if (theVisit != null) {
                    theSurveilReport.visit_type = theVisit.visit_type.equals(Active.isEnable()) ? ResourceBundle.getBundleGlobal("TEXT.IPD") : ResourceBundle.getBundleGlobal("TEXT.OPD");///////////////////////
                    theSurveilReport.diagnosis = theVisit.doctor_dx.replace('\n', ',');
                    theSurveilReport.date_discharge = theVisit.doctor_discharge_time;
                    theSurveilReport.visit_date = DateUtil.getDateToString(DateUtil.getDateFromText(theVisit.begin_visit_time), false);
                }
                TypeDish td = theHosDB.theTypeDishDB.selectByPK(theSurveil.patient_status);
                theSurveilReport.status = (td != null) ? td.description : "";///////////////////////
                if (!theSurveil.illdate.isEmpty()) {
                    theSurveilReport.date_update = DateUtil.getDateToString(DateUtil.getDateFromText(theSurveil.illdate), false);
                }
                theSurveilReport.patient_id = theSurveil.patient_id;
                theSurveilReport.visit_id = theSurveil.vn_id;

                MarryStatus ms = theHosDB.theMarryStatusDB.selectByPK(patient.marriage_status_id);///////////////////////
                theSurveilReport.marriage = (ms != null) ? ms.description : "";
                Nation na = theHosDB.theNationDB.selectByPK(patient.nation_id);
                theSurveilReport.nation = (na != null) ? na.description : "";
                Occupat occ = theHosDB.theOccupatDB.selectByPK(patient.occupation_id);
                theSurveilReport.occupation = (occ != null) ? occ.description : "";
                vListSurveil.add(theSurveilReport);
            }
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vListSurveil;
    }

    /**
     * ��Ѻ������Ǩ�ͺ������ʫ��������ҫ����������ҡ�ѧ��ӡѹ����������ǹ�ͧ���Ҵ���
     *
     * @param diagicd9
     * @param dx9
     * @return
     */
    public boolean checkCODEDx9Same(DiagIcd9 diagicd9, Vector dx9) {
        boolean same = false;
        if (diagicd9.getObjectId() == null) {
            for (int i = 0; dx9 != null && i < dx9.size(); i++) {
                DiagIcd9 ic = (DiagIcd9) dx9.get(i);
                if (diagicd9.icd9_code.equals(ic.icd9_code.trim())) {
                    if (diagicd9.time_in.equals(ic.time_in)) {
                        same = true;
                        break;
                    }
                }
            }
        }
        return same;
    }

    public boolean checkCODEDx10Same(DiagIcd10 diagicd10, Vector Dx10) {
        String diag = diagicd10.icd10_code.trim();
        if (Dx10 == null) {
            return false;
        }
        boolean same = false;
        if (diagicd10.getObjectId() == null) {
            for (int i = 0; i < Dx10.size(); i++) {
                DiagIcd10 ic = (DiagIcd10) Dx10.get(i);
                if (diag.equalsIgnoreCase(ic.icd10_code.trim())) {
                    same = true;
                    break;
                }
            }
        }
        return same;
    }

    /**
     * @param accident
     * @param dx10
     * @param theUS
     * @return
     * @author henbe �ѹ�֡�������غѵ��˵� �����Ҩҡ VisitControl
     */
    public int saveAccident(Accident accident, DiagIcd10 dx10, UpdateStatus theUS) {
        int result_loc = 0;
        if (accident == null) {
            theUS.setStatus(("��س����͡��¡���غѵ��˵�"), UpdateStatus.WARNING);
            return 0;
        }
        if (accident.patient_id.isEmpty()) {
            theUS.setStatus(("��س����͡������"), UpdateStatus.WARNING);
            return 0;
        }
        if (accident.vn_id.isEmpty()) {
            theUS.setStatus(ResourceBundle.getBundleGlobal("VALIDATE.VISITED"), UpdateStatus.WARNING);
            return 0;
        }
        if (accident.date_accident.isEmpty()) {
            theUS.setStatus(("��س��к��ѹ����Դ�غѵ��˵�"), UpdateStatus.WARNING);
            return 0;
        }
        int date_valid = DateUtil.countDateDiff(theLookupControl.getTextCurrentDate(), accident.date_accident);
        if (date_valid < 0) {
            theUS.setStatus(("�ѹ����Դ�غѵ��˵ص�ͧ���ѹ����ʹյ"), UpdateStatus.WARNING);
            return 0;
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            theLookupControl.intReadDateTime();
            //////////////�Ҥ��������ͨ���ź��١////////////////////////////////////
            String icd_old = "";
            if (accident.getObjectId() != null) {
                Accident accOld = theHosDB.theAccidentDB.selectByPK(accident.getObjectId());
                if (accOld != null) {
                    icd_old = accOld.icd10_number;
                }
            }
            String icd = accident.icd10_number;
            if (accident.getObjectId() == null) {
                if (theHosDB.theAccidentDB.selectByVN(accident.vn) != null) {
                    theUS.setStatus(("�к��������ö�ѹ�֡�������غѵ��˵��ҡ���� 1 ����㹡���Ѻ��ԡ�ä���������"), UpdateStatus.WARNING);
                    return 0;
                }
                accident.reporter = theHO.theEmployee.getObjectId();
                accident.record_date_time = theHO.date_time;
                result_loc = theHosDB.theAccidentDB.insert(accident);
            } else {
                accident.staff_update = theHO.theEmployee.getObjectId();
                accident.update_date_time = theHO.date_time;
                result_loc = theHosDB.theAccidentDB.update(accident);
            }
            if (!icd.equals(icd_old)) {
                theHosDB.theDiagIcd10DB.deleteByIcdCode(accident.vn_id, icd_old, theHO.date_time, theHO.theEmployee.getObjectId());
                if (dx10 != null) {
                    dx10.icd10_code = icd;
                    dx10.diag_icd10_accident = "1"; // alway accident
                    dx10.type = Dxtype.getExternalCauseofInjuryDiagnosis(); // ŧ�� external cause ����
                    dx10.diagnosis_date = accident.to_hos_date;
                    if (theHO.theVisit.getObjectId().equals(accident.vn_id)) {
                        intSaveDiagIcd10(dx10, null, theUS, false);
                        theHO.vDiagIcd10 = theHosDB.theDiagIcd10DB.selectByVidSort(theHO.theVisit.getObjectId());
                    }
                }
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            if (!(ex.getMessage() != null && ex.getMessage().equals("cn"))) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theDiagnosisSubject.notifyManageDiagIcd10(ResourceBundle.getBundleText("��úѹ�֡�������غѵ��˵�") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        }
        return result_loc;
    }

    public Vector listOrderIcd9(String visit_id) {
        Vector vc = null;
        String sql = "select * from b_icd9 "
                + "where icd9_number in "
                + "    (select b_item_service.icd9_number from t_order"
                + "        inner join b_item_service on b_item_service.b_item_id = t_order.b_item_id and item_service_active = '1'"
                + "    where t_visit_id = '" + visit_id + "')";
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            vc = theHosDB.theICD9DB.eQuery(sql);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }

    public boolean saveReDiag() {
        if (theHO.theVisit == null) {
            theUS.setStatus(ResourceBundle.getBundleGlobal("VALIDATE.VISITED"), UpdateStatus.WARNING);
            return false;
        }
        if (theHO.theVisit.visit_status.equals(VisitStatus.isOutProcess())) {
            theUS.setStatus(("�����¨���кǹ��������������ö��䢢�������"), UpdateStatus.WARNING);
            return false;
        }
        if (theHO.vVisit != null && theHO.vVisit.size() < 2) {
            theUS.setStatus(("�������ѧ����ջ���ѵԡ���Ѻ��ԡ�á�͹˹��"), UpdateStatus.WARNING);
            return false;
        }
        Visit last_visit = (Visit) theHO.vVisit.get(theHO.vVisit.size() - 2);
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            last_visit = theHosDB.theVisitDB.selectByPK(last_visit.getObjectId());
            theHO.theVisit.doctor_dx = last_visit.doctor_dx;
            theHosDB.theVisitDB.updateDiagnosis(theHO.theVisit);
            Vector vDx = theHosDB.theMapVisitDxDB.selectMapVisitDxByVisitID(last_visit.getObjectId(), Active.isEnable());
            for (int i = 0; i < vDx.size(); i++) {
                MapVisitDx mvd = (MapVisitDx) vDx.get(i);
                mvd.visit_id = theHO.theVisit.getObjectId();
                theHosDB.theMapVisitDxDB.update(mvd);
            }
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            theUS.setStatus(ResourceBundle.getBundleText("�ѹ�֡��õ�Ǩ�ͧᾷ��ҡ���駡�͹") + " "
                    + ResourceBundle.getBundleText("�Դ��Ҵ"), UpdateStatus.ERROR);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            this.theHS.theVitalSubject.notifySaveDiagDoctor(ResourceBundle.getBundleText("�ѹ�֡��õ�Ǩ�ͧᾷ��ҡ���駡�͹") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
        }
        return isComplete;
    }

    public boolean dischargeDoctor() {
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            isComplete = theVisitControl.intDischargeDoctor(theHO.theVisit, null);//, unlock);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
            hosControl.updateTransactionFail(HosControl.UNKONW, ResourceBundle.getBundleGlobal("TEXT.DOCTOR.DISCHARGE"));
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theVisitSubject.notifyDischargeDoctor(ResourceBundle.getBundleGlobal("TEXT.DOCTOR.DISCHARGE") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.UNKONW, ResourceBundle.getBundleGlobal("TEXT.DOCTOR.DISCHARGE"));
        }
        return isComplete;
    }

    public boolean saveDxByStat(String string) {
        theHO.theVisit.stat_dx = string;
        saveDxByStat(theHO.theVisit, theHO.theEmployee.authentication_id);
        return true;
    }

    public boolean deleteDiagIcd10(int[] row) {
        return deleteDiagnosisIcd10(theHO.theVisit, theHO.vDiagIcd10, row);
    }

    /**
     *
     * ��Ǩ�ͺ�������¡�� Icd10 ���� V,W,X,Y ���˵��غѵ��˵�
     * �������ͧ������ͧ�ͧ���ŧ Icd10 ��úҴ�� S,T
     *
     * @param theDiagIcd10
     * @return
     */
    public boolean saveDiagIcd10(DiagIcd10 theDiagIcd10) {
        return saveDiagIcd10(theDiagIcd10, false);
    }

    /**
     * ��Ǩ�ͺ�������¡�� Icd10 ���� V,W,X,Y ���˵��غѵ��˵�
     * �������ͧ������ͧ�ͧ���ŧ Icd10 ��úҴ�� S,T
     *
     * @param theDiagIcd10
     * @param isshow
     * @return
     */
    public boolean saveDiagIcd10(DiagIcd10 theDiagIcd10, boolean isshow) {
        Vector<DiagIcd10> diags = new Vector<DiagIcd10>();
        diags.add(theDiagIcd10);
        if (theDiagIcd10.icd10_code.startsWith("S")
                || theDiagIcd10.icd10_code.startsWith("T")) {
            boolean isPass = false;
            // ��Ǩ�ͺ�������¡�� Icd10 ���������������������� V,W,X,Y ������� �����������غѵ��˵����������������᷹
            for (int i = 0; i < theHO.vDiagIcd10.size(); i++) {
                DiagIcd10 diagicd10 = (DiagIcd10) theHO.vDiagIcd10.get(i);
                if (diagicd10.diag_icd10_accident.equals("1")) {
                    isPass = true;
                    break;
                }
            }
            if (!isPass) {
                DialogWarningDiagIcd10 theDWDI = new DialogWarningDiagIcd10(theHO, theLookupControl, theUS);
                DiagIcd10 showDialog = theDWDI.showDialog(theDiagIcd10);
                if (showDialog != null) {
                    diags.add(showDialog);
                }
            }
        } // ��ҡ V,W,X,Y �繵�Ǩ�ҡ column diag_icd10_accident = 1 ᷹ ��������غѵ��˵بҡ˹�� setup ᷹
        // ����ͺѹ�֡���� V,W,X,Y (������ External) ����ʴ� dialog ��͹���ŧ���� S,T (�ء������ ¡��鹻����� External)
        // ����ѧŧ����ջ����� primary ��� default �繻����� primary ����ջ����� primary ������� default ������ Comorbidity
        else if (theDiagIcd10.type.equals(Dxtype.getExternalCauseofInjuryDiagnosis())
                && theDiagIcd10.diag_icd10_accident.equals("1")) {
            // ��Ǩ�ͺ��Ҷ��ŧ���˵آͧ��úҴ��(V,W,X,Y)�� ���������ʡ���ԹԨ��¡�úҴ��(S, T) ����� primary ���� como ����������
            boolean isPass = false;
            for (int i = 0; i < theHO.vDiagIcd10.size(); i++) {
                DiagIcd10 diagicd10 = (DiagIcd10) theHO.vDiagIcd10.get(i);
                if (diagicd10.icd10_code.startsWith("S")
                        || diagicd10.icd10_code.startsWith("T")) {
                    // ત��� s,t ����������ͧ����� 5 (���˵آͧ��úҴ��)
                    if (!diagicd10.type.equals(Dxtype.getExternalCauseofInjuryDiagnosis())) {
                        isPass = true;
                        break;
                    }
                }
            }
            if (!isPass) {
                DialogWarningDiagIcd10 theDWDI = new DialogWarningDiagIcd10(theHO, theLookupControl, theUS);
                DiagIcd10 showDialog = theDWDI.showDialog(theDiagIcd10);
                if (showDialog == null) {
                    theUS.setStatus("¡��ԡ�¼����ҹ", UpdateStatus.WARNING);
                    return false;
                } else {
                    diags.add(showDialog);
                }
            }
        }
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (DiagIcd10 diagIcd10 : diags) {
                intSaveDiagIcd10(diagIcd10, theHO.vDiagIcd10, theUS, isshow);
            }
            theHO.vDiagIcd10 = theHosDB.theDiagIcd10DB.selectByVidSort(theHO.theVisit.getObjectId());
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            if (!(ex.getMessage() != null && ex.getMessage().equals("cn"))) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex);
                hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            }
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theDiagnosisSubject.notifyManageDiagIcd10(ResourceBundle.getBundleText("��úѹ�֡�����ä") + " "
                    + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        }
        return isComplete;
    }

    /**
     * ��úѹ�֡�����ä��ѡ����Ѻ�͡��§ҹ
     *
     * @param theIcd9
     * @return
     */
    public int savePrimaryReport(DiagIcd9 theIcd9) {
        int ret = 1;
        if (theIcd9.getObjectId() == null) {
            theUS.setStatus(("��س����͡��¡�÷��ѹ�֡����"), UpdateStatus.WARNING);
            return 11;
        }
        if (!theIcd9.type.equals(Dxtype.getPrimaryDiagnosis())) {
            theUS.setStatus(("��س����͡��¡�÷���� Primary"), UpdateStatus.WARNING);
            return 12;
        }
        String uc_name = ResourceBundle.getBundleText("��úѹ�֡�����ä��ѡ����Ѻ�͡��§ҹ");
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (int i = 0; i < theHO.vDiagIcd10.size(); i++) {
                DiagIcd10 dx10 = (DiagIcd10) theHO.vDiagIcd10.get(i);
                if (dx10.primary_report.equals("1")) {
                    dx10.primary_report = "0";
                    theHosDB.theDiagIcd10DB.update(dx10);
                }
            }
            theIcd9.primary_report = "1";
            theHosDB.theDiagIcd9DB.update(theIcd9);
            theHO.vDiagIcd10 = theHosDB.theDiagIcd10DB.selectByVidSort(theHO.theVisit.getObjectId());
            theConnectionInf.getConnection().commit();
            ret = 0;
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (ret == 0) {
            theHS.theDiagnosisSubject.notifyManageDiagIcd10(uc_name + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        }
        return ret;
    }

    /**
     * ��úѹ�֡�����ä��ѡ����Ѻ�͡��§ҹ
     *
     * @param theDx10
     * @return
     */
    public int savePrimaryReport(DiagIcd10 theDx10) {
        if (theDx10.getObjectId() == null) {
            theUS.setStatus(("��س����͡��¡�÷��ѹ�֡����"), UpdateStatus.WARNING);
            return 11;
        }
        if (!theDx10.type.equals(Dxtype.getPrimaryDiagnosis())) {
            theUS.setStatus(("��س����͡��¡�÷���� Primary"), UpdateStatus.WARNING);
            return 12;
        }
        String uc_name = ResourceBundle.getBundleText("��úѹ�֡�����ä��ѡ����Ѻ�͡��§ҹ");
        boolean isComplete = false;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            for (int i = 0; i < theHO.vDiagIcd10.size(); i++) {
                DiagIcd10 dx10 = (DiagIcd10) theHO.vDiagIcd10.get(i);
                if (dx10.primary_report.equals("1")) {
                    dx10.primary_report = "0";
                    theHosDB.theDiagIcd10DB.update(dx10);
                }
            }
            theDx10.primary_report = "1";
            theHosDB.theDiagIcd10DB.update(theDx10);
            theHO.vDiagIcd10 = theHosDB.theDiagIcd10DB.selectByVidSort(theHO.theVisit.getObjectId());
            theConnectionInf.getConnection().commit();
            isComplete = true;
        } catch (Exception ex) {
            hosControl.updateTransactionFail(HosControl.TRANSACTION_INSERT);
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        if (isComplete) {
            theHS.theDiagnosisSubject.notifyManageDiagIcd10(uc_name + ResourceBundle.getBundleGlobal("TEXT.SUCCESS"), UpdateStatus.COMPLETE);
            hosControl.updateTransactionSuccess(HosControl.TRANSACTION_INSERT);
        }
        return isComplete ? 0 : 1;
    }

    /**
     * @deprecate henbe unused must send theUS
     *
     * @param chronic
     */
    public void saveChronic(Chronic chronic) {
        saveChronic(chronic, theUS);
    }

    public void saveChronic(Chronic chronic, int panel) {
        saveChronic(chronic, theUS, panel);
    }

    public Vector listSurveilPVid(String patient_id, String visit_id) {
        Vector vc = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            String sql = "select * from t_surveil"
                    + " where t_patient_id = '" + patient_id + "'";
            if (visit_id != null) {
                sql += " and t_visit_id = '" + visit_id + "'";
            }
            sql += " order by surveil_hn,surveil_icd10_number ";
            vc = theHosDB.theSurveilDB.eQuery(sql);
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return vc;
    }
    private static final Logger LOG = Logger.getLogger(DiagnosisControl.class.getName());

    public void setHosControl(HosControl hosControl) {
        this.hosControl = hosControl;
    }

    public DiagIcd10 getPrimaryDiag(Visit visit) {
        DiagIcd10 ret = null;
        theConnectionInf.open();
        try {
            theConnectionInf.getConnection().setAutoCommit(false);
            ret = theHosDB.theDiagIcd10DB.selectPrimaryByVid(visit.getObjectId());
            theConnectionInf.getConnection().commit();
        } catch (Exception ex) {
            LOG.log(java.util.logging.Level.SEVERE, ex.getMessage(), ex);
            try {
                theConnectionInf.getConnection().rollback();
            } catch (SQLException ex1) {
                LOG.log(java.util.logging.Level.SEVERE, null, ex1);
            }
        } finally {
            theConnectionInf.close();
        }
        return ret;
    }
}

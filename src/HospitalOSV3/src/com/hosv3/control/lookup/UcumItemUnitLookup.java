/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.control.lookup;

import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.LookupControlInf;
import com.hosv3.control.LookupControl;

/**
 *
 * @author tanakrit
 */
public class UcumItemUnitLookup implements LookupControlInf {

    private final LookupControl theLookup;

    public UcumItemUnitLookup(LookupControl lookup) {
        theLookup = lookup;
    }

    @Override
    @SuppressWarnings("UseOfObsoleteCollectionType")
    public java.util.Vector listData(String str) {
        return new java.util.Vector(theLookup.listUcmItemUnit(str.trim()));
    }

    @Override
    public CommonInf readHosData(String str) {
        return null;
    }
}

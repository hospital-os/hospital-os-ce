/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.objdb.objdbclass;

import com.hospital_os.usecase.connection.ConnectionInf;
import com.pcu.object.THealthIcf;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sompr
 */
public class THealthIcfDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "PCU99";

    public THealthIcfDB(ConnectionInf c) {
        this.connectionInf = c;
    }

    public int insert(THealthIcf obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO t_health_icf(\n"
                    + "            t_health_icf_id, t_visit_id, survey_date, f_icf_code_id, active, user_record_id, user_update_id)\n"
                    + "    VALUES (?, ?, ?, ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.getGenID(tableId));
            preparedStatement.setString(index++, obj.t_visit_id);
            preparedStatement.setDate(index++, new Date(obj.survey_date.getTime()));
            preparedStatement.setString(index++, obj.f_icf_code_id);
            preparedStatement.setString(index++, obj.active);
            preparedStatement.setString(index++, obj.user_record_id);
            preparedStatement.setString(index++, obj.user_update_id);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(THealthIcf obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_health_icf\n");
            sql.append("   SET survey_date=?, f_icf_code_id=?, \n");
            sql.append("       active=?, user_update_id=?, update_date_time = current_timestamp\n");
            sql.append(" WHERE t_health_icf_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setDate(index++, new Date(obj.survey_date.getTime()));
            preparedStatement.setString(index++, obj.f_icf_code_id);
            preparedStatement.setString(index++, obj.active);
            preparedStatement.setString(index++, obj.user_update_id);
            preparedStatement.setString(index++, obj.getObjectId());
            // execute update SQL stetement


            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int inactive(THealthIcf obj) throws Exception {
        int ret = 0;
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_health_icf\n");
            sql.append("  SET active=?, user_cancel_id=?, cancel_date_time=current_timestamp\n");
            sql.append(" WHERE t_health_icf_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, "0");
            preparedStatement.setString(2, obj.user_cancel_id);
            preparedStatement.setString(3, obj.getObjectId());
            // execute update SQL stetement
            ret = preparedStatement.executeUpdate();
        } catch (Exception ex) {
            ret = 0;
            throw ex;
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
        return ret;
    }

    public THealthIcf select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select t_health_icf.* from t_health_icf\n"
                    + "where t_health_icf.t_health_icf_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<THealthIcf> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<THealthIcf> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<THealthIcf> list = new ArrayList<THealthIcf>();
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()) {
            THealthIcf obj = new THealthIcf();
            obj.setObjectId(rs.getString("t_health_icf_id"));
            obj.t_visit_id = rs.getString("t_visit_id");
            obj.survey_date = rs.getDate("survey_date");
            obj.f_icf_code_id = rs.getString("f_icf_code_id");
            obj.active = rs.getString("active");
            obj.user_record_id = rs.getString("user_record_id");
            obj.record_date_time = rs.getTimestamp("record_date_time");
            obj.user_update_id = rs.getString("user_update_id");
            obj.update_date_time = rs.getTimestamp("update_date_time");
            obj.user_cancel_id = rs.getString("user_cancel_id");
            obj.cancel_date_time = rs.getTimestamp("cancel_date_time");
            list.add(obj);
        }
        rs.close();
        return list;
    }
}

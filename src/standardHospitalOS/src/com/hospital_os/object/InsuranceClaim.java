/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;
import java.util.Date;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class InsuranceClaim extends Persistent {

    public String t_billing_invoice_id = "";
    public String t_visit_insurance_plan_id = "";
    public String claim_code = "";
    public Date claim_code_datetime;
    public String status_insurance_approve = "1";
    public String reason_not_approve = "";
    public String record_datetime = "";
    public String user_record_id = "";
    public String update_datetime = "";
    public String user_update_id = "";
    public Date alert_date;
    public String status_insurance_receive = "1";
    // object only
    public String insuranceCompanyName = "";
    public String insurancePlanName = "";
}

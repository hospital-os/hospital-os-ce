/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
public class FootExamAssess extends Persistent {

    public String t_foot_exam_id = "";
    public String prodoscope_result = "";
    public String nail_problem = "";
    public String nail_problem_detail = "";
    public String wart_problem = "";
    public String wart_problem_detail = "";
    public String foot_deformities = "";
    public String skin_color = "";
    public String hair_loss = "";
    public String skin_temperature = "";
    public String fungal_infections = "";
    public String record_date_time = "";
    public String user_record_id = "";
    public String update_date_time = "";
    public String user_update_id = "";
}

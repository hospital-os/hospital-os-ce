/*
 * DoseEpiSetDB.java
 *
 * Created on 27 �Զع�¹ 2548, 16:30 �.
 */
package com.pcu.objdb.objdbclass;

import com.hospital_os.usecase.connection.*;
import com.pcu.object.*;
import java.sql.*;
import java.util.*;

/**
 *
 * @author Administrator
 */
@SuppressWarnings({"ClassWithoutLogger", "UseOfObsoleteCollectionType"})
public class DoseEpiSetDB {

    /**
     * Creates a new instance of DoseEpiSetDB
     */
    public DoseEpiSetDB() {
    }
    public ConnectionInf theConnectionInf;
    public DoseEpiSet dbObj;
    final private String idtable = "769";//"139";

    public DoseEpiSetDB(ConnectionInf db) {
        theConnectionInf = db;
        dbObj = new DoseEpiSet();
        initConfig();
    }

    public boolean initConfig() {
        dbObj.table = "b_health_epi_item_drug_setup";
        dbObj.pk_field = "b_health_epi_item_drug_setup_id";//"b_health_epi_set_drug_id";
        dbObj.epi_set_key_id = "b_health_epi_item_id";//"b_health_epi_set_id";
        dbObj.description = "b_health_epi_item_drug_setup_description";
        dbObj.dose = "health_epi_item_drug_setup_dose";
        dbObj.use_uom = "health_epi_item_drug_setup_use_uom";
        dbObj.qty = "health_epi_item_drug_setup_qty";
        dbObj.purch_uom = "health_epi_item_drug_setup_purch_uom";
        dbObj.caution = "health_epi_item_drug_setup_caution";
        dbObj.day_time = "f_item_day_time_id";
        dbObj.printting = "health_epi_item_drug_setup_printable";
        dbObj.instruction = "b_item_drug_instruction_id";
        dbObj.frequency = "b_item_drug_frequency_id";
        dbObj.usage_special = "health_epi_item_drug_setup_special_prescription";
        dbObj.usage_text = "health_epi_item_drug_setup_usage_text";
        dbObj.item_code = "b_item_id";

        return true;
    }

    public int insert(DoseEpiSet o) throws Exception {
        DoseEpiSet p = o;
        p.generateOID(idtable);
        String sql = "insert into b_health_epi_item_drug_setup ( \n"
                + "               b_health_epi_item_drug_setup_id ,b_health_epi_item_id ,b_health_epi_item_drug_setup_description \n"
                + "              ,health_epi_item_drug_setup_dose ,health_epi_item_drug_setup_use_uom ,health_epi_item_drug_setup_qty \n"
                + "              ,health_epi_item_drug_setup_purch_uom ,health_epi_item_drug_setup_caution ,f_item_day_time_id \n"
                + "              ,health_epi_item_drug_setup_printable ,b_item_drug_instruction_id ,b_item_drug_frequency_id \n"
                + "              ,health_epi_item_drug_setup_special_prescription ,health_epi_item_drug_setup_usage_text ,b_item_id "
                + "     ) values (?,?,? \n"
                + "              ,?,?,? \n"
                + "              ,?,?,? \n"
                + "              ,?,?,? \n"
                + "              ,?,?,?)";
        try (PreparedStatement ps = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ps.setString(index++, p.getObjectId());
            ps.setString(index++, p.epi_set_key_id);
            ps.setString(index++, p.description);
            ps.setDouble(index++, Double.parseDouble(p.dose == null || p.dose.isEmpty() ? "0" : p.dose));
            ps.setString(index++, p.use_uom);
            ps.setDouble(index++, Double.parseDouble(p.qty == null || p.qty.isEmpty() ? "0" : p.qty));
            ps.setString(index++, p.purch_uom);
            ps.setString(index++, p.caution);
            ps.setString(index++, p.day_time);
            ps.setString(index++, p.printting);
            ps.setString(index++, p.instruction);
            ps.setString(index++, p.frequency);
            ps.setString(index++, p.usage_special);
            ps.setString(index++, p.usage_text);
            ps.setString(index++, p.item_code);
            return ps.executeUpdate();
        }
    }

    public int update(DoseEpiSet p) throws Exception {
        String sql = "update b_health_epi_item_drug_setup \n"
                + "      set b_health_epi_item_id=?, b_health_epi_item_drug_setup_description=?, health_epi_item_drug_setup_dose=? \n"
                + "        , health_epi_item_drug_setup_use_uom=? , health_epi_item_drug_setup_qty=? \n"
                + "        , health_epi_item_drug_setup_purch_uom=? , health_epi_item_drug_setup_caution=? \n"
                + "        , f_item_day_time_id=?, health_epi_item_drug_setup_printable=?, b_item_drug_instruction_id=? \n"
                + "        , b_item_drug_frequency_id=?, health_epi_item_drug_setup_special_prescription=? \n"
                + "        , health_epi_item_drug_setup_usage_text=?, b_item_id=? \n"
                + "    where b_health_epi_item_drug_setup_id=?";
        try (PreparedStatement ps = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ps.setString(index++, p.epi_set_key_id);
            ps.setString(index++, p.description);
            ps.setDouble(index++, Double.parseDouble(p.dose == null || p.dose.isEmpty() ? "0" : p.dose));
            ps.setString(index++, p.use_uom);
            ps.setDouble(index++, Double.parseDouble(p.qty == null || p.qty.isEmpty() ? "0" : p.qty));
            ps.setString(index++, p.purch_uom);
            ps.setString(index++, p.caution);
            ps.setString(index++, p.day_time);
            ps.setString(index++, p.printting);
            ps.setString(index++, p.instruction);
            ps.setString(index++, p.frequency);
            ps.setString(index++, p.usage_special);
            ps.setString(index++, p.usage_text);
            ps.setString(index++, p.item_code);
            ps.setString(index++, p.getObjectId());
            return ps.executeUpdate();
        }
    }
    //////////////////////////////////////////////////////////////////////////////

    public int delete(DoseEpiSet o) throws Exception {
        String sql = "delete from b_health_epi_item_drug_setup where b_health_epi_item_drug_setup_id=?";
        try (PreparedStatement ps = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ps.setString(index++, o.getObjectId());
            return ps.executeUpdate();
        }
    }
    //////////////////////////////////////////////////////////////////////////////

    public DoseEpiSet selectByPK(String pk) throws Exception {
        String sql = "select * from b_health_epi_item_drug_setup where b_health_epi_item_drug_setup_id = '" + pk + "'";

        Vector v = eQuery(sql);
        if (v.isEmpty()) {
            return null;
        } else {
            return (DoseEpiSet) v.get(0);
        }
    }
    //////////////////////////////////////////////////////////////////////////////

    public Vector selectAll() throws Exception {
        String sql = "select * from b_health_epi_item_drug_setup";

        Vector v = eQuery(sql);
        if (v.isEmpty()) {
            return null;
        } else {
            return v;
        }
    }
    //////////////////////////////////////////////////////////////////////////////

    public DoseEpiSet selectByKeyEpiSet(String key_drugset) throws Exception {
        String sql = "select * from b_health_epi_item_drug_setup where b_health_epi_item_id = '" + key_drugset + "'";

        Vector v = eQuery(sql);
        if (v.isEmpty()) {
            return null;
        } else {
            return (DoseEpiSet) v.get(0);
        }
    }
    //////////////////////////////////////////////////////////////////////////////

    public Vector eQuery(String sql) throws Exception {
        DoseEpiSet p;
        Vector list = new Vector();
        try (ResultSet rs = theConnectionInf.eQuery(sql)) {
            while (rs.next()) {
                p = new DoseEpiSet();
                p.setObjectId(rs.getString("b_health_epi_item_drug_setup_id"));
                p.epi_set_key_id = rs.getString("b_health_epi_item_id");
                p.description = rs.getString("b_health_epi_item_drug_setup_description");
                p.dose = rs.getString("health_epi_item_drug_setup_dose");
                p.use_uom = rs.getString("health_epi_item_drug_setup_use_uom");
                p.qty = rs.getString("health_epi_item_drug_setup_qty");
                p.purch_uom = rs.getString("health_epi_item_drug_setup_purch_uom");
                p.caution = rs.getString("health_epi_item_drug_setup_caution");
                p.day_time = rs.getString("f_item_day_time_id");
                p.printting = rs.getString("health_epi_item_drug_setup_printable");
                p.instruction = rs.getString("b_item_drug_instruction_id");
                p.frequency = rs.getString("b_item_drug_frequency_id");
                p.usage_special = rs.getString("health_epi_item_drug_setup_special_prescription");
                p.usage_text = rs.getString("health_epi_item_drug_setup_usage_text");
                p.item_code = rs.getString("b_item_id");
                list.add(p);
            }
        }
        return list;
    }
    //////////////////////////////////////////////////////////////////////////////
}

ALTER TABLE t_hstock_card DROP CONSTRAINT hstock_card_order_status_unique;
ALTER TABLE t_hstock_card ADD CONSTRAINT hstock_card_order_status_unique UNIQUE (t_hstock_mgnt_id,t_order_id,f_hstock_adjust_type_id,update_datetime);

INSERT INTO f_hstock_adjust_type VALUES ('20','รับคืนจากการจ่าย','0','1','0');

CREATE OR REPLACE FUNCTION update_hstock_order_return()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
DECLARE 
         hstock_card_count_order_id boolean :=  (select count(*)
                                                                    from t_hstock_card
                                                                    where t_hstock_card.t_order_id = NEW.t_order_id) > 0 ;
        views RECORD;
BEGIN
    IF (hstock_card_count_order_id AND (NEW.receive_qty is not null OR NEW.receive_qty > 0)) THEN
    FOR views IN 
    (select distinct t_hstock_card.t_hstock_mgnt_id as hstock_mgnt_id
             , null::float8 as previous_qty
             , null::float8 as previous_qty_lot
             , NEW.receive_qty as qty
             , null::float8 as update_qty
             , null::float8 as update_qty_lot
             , t_hstock_card.small_unit_cost as small_unit_cost
             , '20' as f_hstock_adjust_type_id 
             , t_hstock_card.t_order_id as t_order_id
             , t_hstock_card.order_seq as order_seq
             , null::date as expire_date_adjust
             , '' as reason_adjust
             , current_timestamp as update_datetime
             , NEW.user_receiver_id as user_update_id    
    from t_hstock_card inner join t_hstock_mgnt on t_hstock_card.t_hstock_mgnt_id = t_hstock_mgnt.t_hstock_mgnt_id
    where
            t_hstock_card.t_order_id = NEW.t_order_id
    order by
            order_seq asc)
    LOOP
        EXECUTE insert_t_hstock_card( views.hstock_mgnt_id::text
             , views.previous_qty::float8
             , views.previous_qty_lot::float8
             , views.qty::float8
             , views.update_qty::float8
             , views.update_qty_lot::float8
             , views.small_unit_cost::float8
             , views.f_hstock_adjust_type_id::text
             , views.t_order_id::text
             , views.order_seq::integer
             , views.expire_date_adjust::date
             , views.reason_adjust::text
             , views.update_datetime::timestamp
             , views.user_update_id::text);
        END LOOP;
        END IF;
        RETURN NEW;
END;
$function$
;

INSERT INTO s_stock_version (version_app, version_db, description)VALUES ('2.0.1', '2.0.1', 'Stock Module for Hos v4');
INSERT INTO s_script_update_log values ('Stock_Module','update_stock_002.sql',(select current_date) || ','|| (select current_time),'Stock Module for Hos v4');

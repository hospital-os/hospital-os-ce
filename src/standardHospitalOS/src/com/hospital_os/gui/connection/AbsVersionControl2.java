/*
 * InfVersionControl.java
 *
 * Created on 21 ���Ҥ� 2549, 8:39 �.
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
package com.hospital_os.gui.connection;

import java.util.Vector;

/**
 *
 * @author Administrator
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public abstract class AbsVersionControl2 extends AbsVersionControl {

    public abstract String getFinalVersion();

    public abstract Vector getFileUpdate(String cur_version);

    @Override
    public boolean isVersionCorrect() {
        return getCurrentVersion().equals(getFinalVersion());
    }

    @Override
    public String getWarningMessage() {
        String curr = getCurrentVersion();
        String final_version = getFinalVersion();
        return curr.equals(final_version) ? "-" : (curr + " -> " + final_version);
    }

    @Override
    protected Vector getFileUpdate(int index) {
        return null;
    }

    @Override
    protected String getVersion(int index) {
        return null;
    }

    @Override
    protected int getVersionCount() {
        return 0;
    }
}

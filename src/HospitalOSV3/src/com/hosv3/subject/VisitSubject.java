/*
 * visitSubject.java
 *
 * Created on 17 ���Ҥ� 2546, 17:09 �.
 */
package com.hosv3.subject;

import com.hosv3.usecase.transaction.ManageVisitResp;
import java.util.Vector;

/**
 *
 * @author tong
 */
@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public class VisitSubject implements ManageVisitResp {

    Vector theVisitObsV;

    /**
     * Creates a new instance of visitSubject
     */
    public VisitSubject() {
        theVisitObsV = new Vector();
    }

    public void removeAttach() {
        theVisitObsV.removeAllElements();
    }

    public void attachManageVisit(ManageVisitResp o) {
        theVisitObsV.add(o);
    }

    /**
     * @roseuid 3F8400140271
     */
    @Override
    public void notifyVisitPatient(String msg, int state) {
        for (int i = 0, size = theVisitObsV.size(); i < size; i++) {
            ((ManageVisitResp) theVisitObsV.get(i)).notifyVisitPatient(msg, state);
        }
    }

    @Override
    public void notifyReverseFinancial(String msg, int state) {
        for (int i = 0, size = theVisitObsV.size(); i < size; i++) {
            ((ManageVisitResp) theVisitObsV.get(i)).notifyReverseFinancial(msg, state);
        }
    }

    @Override
    public void notifyReadVisit(String msg, int state) {
        for (int i = 0, size = theVisitObsV.size(); i < size; i++) {
            ((ManageVisitResp) theVisitObsV.get(i)).notifyReadVisit(msg, state);
        }
    }

    @Override
    public void notifyObservVisit(String msg, int state) {
        for (int i = 0, size = theVisitObsV.size(); i < size; i++) {
            ((ManageVisitResp) theVisitObsV.get(i)).notifyObservVisit(msg, state);
        }
    }

    @Override
    public void notifySendVisit(String msg, int state) {
        for (int i = 0, size = theVisitObsV.size(); i < size; i++) {
            ((ManageVisitResp) theVisitObsV.get(i)).notifySendVisit(msg, state);
        }
    }

    @Override
    public void notifyUnlockVisit(String msg, int state) {
        for (int i = 0, size = theVisitObsV.size(); i < size; i++) {
            ((ManageVisitResp) theVisitObsV.get(i)).notifyUnlockVisit(msg, state);
        }
    }

    @Override
    public void notifyDropVisit(String msg, int state) {
        for (int i = 0, size = theVisitObsV.size(); i < size; i++) {
            ((ManageVisitResp) theVisitObsV.get(i)).notifyDropVisit(msg, state);
        }
    }

    @Override
    public void notifyAdmitVisit(String msg, int state) {
        for (int i = 0, size = theVisitObsV.size(); i < size; i++) {
            ((ManageVisitResp) theVisitObsV.get(i)).notifyAdmitVisit(msg, state);
        }
    }

    @Override
    public void notifyDischargeFinancial(String msg, int state) {
        for (int i = 0, size = theVisitObsV.size(); i < size; i++) {
            ((ManageVisitResp) theVisitObsV.get(i)).notifyDischargeFinancial(msg, state);
        }
    }

    @Override
    public void notifyCheckDoctorTreament(String msg, int state) {
        for (int i = 0, size = theVisitObsV.size(); i < size; i++) {
            ((ManageVisitResp) theVisitObsV.get(i)).notifyCheckDoctorTreament(msg, state);
        }
    }

    @Override
    public void notifyReverseDoctor(String str, int status) {
        for (int i = 0, size = theVisitObsV.size(); i < size; i++) {
            ((ManageVisitResp) theVisitObsV.get(i)).notifyReverseDoctor(str, status);
        }
    }

    @Override
    public void notifyDeleteVisitPayment(String str, int status) {
        for (int i = 0, size = theVisitObsV.size(); i < size; i++) {
            ((ManageVisitResp) theVisitObsV.get(i)).notifyDeleteVisitPayment(str, status);
        }
    }

    @Override
    public void notifyDischargeDoctor(String str, int status) {
        for (int i = 0, size = theVisitObsV.size(); i < size; i++) {
            ((ManageVisitResp) theVisitObsV.get(i)).notifyDischargeDoctor(str, status);
        }
    }

    @Override
    public void notifySendVisitBackWard(String str, int status) {
        for (int i = 0, size = theVisitObsV.size(); i < size; i++) {
            ((ManageVisitResp) theVisitObsV.get(i)).notifySendVisitBackWard(str, status);
        }
    }

    @Override
    public void notifyRemainDoctorDischarge(String str, int status) {
        for (int i = 0, size = theVisitObsV.size(); i < size; i++) {
            ((ManageVisitResp) theVisitObsV.get(i)).notifyRemainDoctorDischarge(str, status);
        }
    }

    @Override
    public void notifyReverseAdmit(String str, int status) {
        for (int i = 0, size = theVisitObsV.size(); i < size; i++) {
            ((ManageVisitResp) theVisitObsV.get(i)).notifyReverseAdmit(str, status);
        }
    }

    @Override
    public void notifyAdmitLeaveHome(String str, int status) {
        for (int i = 0, size = theVisitObsV.size(); i < size; i++) {
            ((ManageVisitResp) theVisitObsV.get(i)).notifyAdmitLeaveHome(str, status);
        }
    }

    public boolean detach(ManageVisitResp o) {
        return theVisitObsV.remove(o);
    }
}

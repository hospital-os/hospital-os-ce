package com.hospital_os.objdb;
import com.hospital_os.usecase.connection.*;
import com.hospital_os.utility.*;
import java.util.*;
import java.sql.*;
import com.hospital_os.object.LabStd;

@SuppressWarnings("UseOfObsoleteCollectionType")
public class LabStdDB
{
    public ConnectionInf theConnectionInf;
    public LabStd dbObj;
    final public String idtable = "257";
    /**
     * @param ConnectionInf db
     * @roseuid 3F65897F0326
     */
    public LabStdDB(ConnectionInf db)
    {
        theConnectionInf=db;
        dbObj = new LabStd();
        initConfig();
    }

    private boolean initConfig()
    {
        dbObj.table="f_lab_std";
        dbObj.pk_field="f_lab_std_id";
        dbObj.f_lab_name = "f_lab_name";
        dbObj.lab_set = "lab_set";
        dbObj.lab_type = "lab_type";
        return true;
    }

    /**
     * @param cmd
     * @param o
     * @return int
     * @roseuid 3F6574DE0394
     */
    public int insert(LabStd o) throws Exception
    {
        String sql="";
        LabStd p = o;
        p.generateOID(idtable);
        sql="insert into " + dbObj.table + " ("
        + dbObj.pk_field
        + " ,"	+ dbObj.f_lab_name
        + " ,"	+ dbObj.lab_set
        + " ,"	+ dbObj.lab_type
        + " ) values ('"
        + p.getObjectId()
        + "','" + p.f_lab_name
        + "','" + p.lab_set
        + "','" + p.lab_type
        + "')";
        sql = Gutil.convertSQLToMySQL(sql,theConnectionInf.gettypeDatabase());
        return theConnectionInf.eUpdate(sql);
    }

    public int update(LabStd o) throws Exception
    {
        String sql="update " + dbObj.table + " set ";
        LabStd p=o;
        String field =""
        + "', " + dbObj.f_lab_name + "='" + p.f_lab_name
        + "', " + dbObj.lab_set + "='" + p.lab_set
        + "', " + dbObj.lab_type + "='" + p.lab_type
        + "' where " + dbObj.pk_field + "='" + p.getObjectId() +"'";
        sql = Gutil.convertSQLToMySQL(sql+field.substring(2),theConnectionInf.gettypeDatabase());
        return theConnectionInf.eUpdate(sql);
    }

    public int delete(LabStd o) throws Exception
    {
        String sql="delete from " + dbObj.table
        + " where " + dbObj.pk_field + "='" + o.getObjectId() +"'";
        return theConnectionInf.eUpdate(sql);
    }

    public LabStd selectByPK(String pk) throws Exception
    {
        String sql="select * from " + dbObj.table
        + " where " + dbObj.pk_field
        + " = '" + pk + "'";

        Vector v=eQuery(sql);
        if(v.isEmpty())
            return null;
        else
            return (LabStd)v.get(0);
    }

    public Vector selectAllCombofix() throws Exception
    {
        Vector v = new Vector();
        String sql ="select * from " + dbObj.table
        + " order by " + dbObj.f_lab_name;
        v = eQuery(sql);
        if(v.isEmpty())
            return null;
        else
            return v;
    }

    public Vector selectAllCombofixDistinct() throws Exception
    {
        Vector v = new Vector();
        String sql ="SELECT DISTINCT labgrp_name FROM b_lab_std";
        v = eQuery2(sql);
        if(v.isEmpty())
            return null;
        else
            return v;
    }
    public Vector selectByGroup(String grp) throws Exception
    {
        Vector v = new Vector();
        String sql ="select * from b_lab_std where labgrp_name = '" + grp + "'";
        v = eQuery3(sql);
        if(v.isEmpty())
            return null;
        else
            return v;
    }

    public Vector selectAll() throws Exception
    {
        Vector v = new Vector();
        String sql ="select * from " + dbObj.table
        + " order by " + dbObj.pk_field;
        v = eQuery(sql);
        if(v.isEmpty())
            return null;
        else
            return v;
    }

    public Vector selectOld() throws Exception
    {
        Vector v = new Vector();
        String sql ="select * from " + dbObj.table
        + " order by " + dbObj.pk_field;
        v = eQuery(sql);
        if(v.isEmpty())
            return null;
        else
            return v;
    }

    public Vector eQuery(String sql) throws Exception
    {
        LabStd p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while(rs.next())
        {
            p = new LabStd();
            p.setObjectId(rs.getString(dbObj.pk_field));
            p.f_lab_name = rs.getString(dbObj.f_lab_name);
            p.lab_set = rs.getString(dbObj.lab_set);
            p.lab_type = rs.getString(dbObj.lab_type);
            list.add(p);
        }
        rs.close();
        return list;
    }

    public Vector eQuery2(String sql) throws Exception
    {
        LabStd p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while(rs.next())
        {
            p = new LabStd();
            p.setObjectId(rs.getString("labgrp_name"));
            p.f_lab_name = rs.getString("labgrp_name");
            p.lab_set = rs.getString("labgrp_name");
            p.lab_type = rs.getString("labgrp_name");
            list.add(p);
        }
        rs.close();
        return list;
    }
    public Vector eQuery3(String sql) throws Exception
    {
        LabStd p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while(rs.next())
        {
            p = new LabStd();
            p.setObjectId(rs.getString("lab_std_id"));
            p.f_lab_name = rs.getString("f_lab_name");
            p.lab_set = rs.getString("f_lab_name");
            p.lab_type = rs.getString("f_lab_name");
            list.add(p);
        }
        rs.close();
        return list;
    }

}

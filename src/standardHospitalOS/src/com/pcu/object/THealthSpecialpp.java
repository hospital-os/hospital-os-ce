/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.object;

import com.hospital_os.usecase.connection.Persistent;
import java.util.Date;

/**
 *
 * @author sompr
 */
public class THealthSpecialpp extends Persistent {

    public String t_visit_id;
    public String t_person_id;
    public Date survey_date = new Date();
    public String survey_place = "1"; // 1 = ในสถานบริการ , 2 = นอกสถานบริการ
    public String f_specialpp_code_id;
    public String hospital;
    public String active = "1";
    public Date record_date_time;
    public String user_record_id = "";
    public Date update_date_time;
    public String user_update_id = "";
    public Date cancel_date_time;
    public String user_cancel_id = "";
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.objdb.objdbclass;

import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.ComboFix;
import com.pcu.object.AncPeriod;
import java.sql.ResultSet;
import java.util.Vector;

/**
 *
 * @author sompr
 */
@SuppressWarnings({"ClassWithoutLogger", "UseOfObsoleteCollectionType"})
public class AncPeriodDB {

    /**
     * Creates a new instance of AncSectionDB
     */
    public AncPeriodDB() {
    }
    public ConnectionInf theConnectionInf;

    public AncPeriodDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public int insert(AncPeriod p) throws Exception {
        String sql = "";
        sql = "insert into f_health_anc_period ("
                + " f_health_anc_period_id"
                + " ,description"
                + " ) values ('"
                + p.getObjectId()
                + "','" + p.description
                + "')";

        return theConnectionInf.eUpdate(sql);
    }

    public int update(AncPeriod p) throws Exception {
        String sql = "update f_health_anc_periodset ";
        String field = ""
                + "', description='" + p.description
                + "' where f_health_anc_period_id='" + p.getObjectId() + "'";
        sql = sql + field.substring(2);
        return theConnectionInf.eUpdate(sql);
    }

    public int delete(AncPeriod p) throws Exception {
        String sql = "delete from f_health_anc_period"
                + " where f_health_anc_period_id='" + p.getObjectId() + "'";
        return theConnectionInf.eUpdate(sql);
    }

    public Vector selectAll() throws Exception {
        Vector vAncSection = new Vector();

        String sql = "select * from f_health_anc_period order by f_health_anc_period_id";

        vAncSection = veQuery(sql);

        if (vAncSection.isEmpty()) {
            return null;
        } else {
            return vAncSection;
        }
    }

    public Vector eQuery(String sql) throws Exception {
        AncPeriod p;
        Vector list = new Vector();
        try (ResultSet rs = theConnectionInf.eQuery(sql)) {
            while (rs.next()) {
                p = new AncPeriod();
                p.setObjectId(rs.getString("f_health_anc_period_id"));
                p.description = rs.getString("description");
                list.add(p);
            }
        }
        return list;
    }

    public Vector veQuery(String sql) throws Exception {
        ComboFix p;
        Vector list = new Vector();
        try (ResultSet rs = theConnectionInf.eQuery(sql)) {
            while (rs.next()) {
                p = new ComboFix();
                p.code = rs.getString("f_health_anc_period_id");
                p.name = rs.getString("description");
                list.add(p);
            }
        }
        return list;
    }
}

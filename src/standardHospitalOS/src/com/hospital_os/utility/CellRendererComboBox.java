/*
 * CheckBoxRenderer.java
 *
 * Created on 22 ����Ҥ� 2547, 20:01 �.
 */
package com.hospital_os.utility;

import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author tong
 */
@SuppressWarnings("ClassWithoutLogger")
public class CellRendererComboBox implements TableCellRenderer {

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value,
            boolean isSelected, boolean hasFocus, int row, int column) {
        if (value == null) {
            return null;
        }
        return (Component) value;
    }
}

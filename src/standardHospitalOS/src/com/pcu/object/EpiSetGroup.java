/*
 * EpiSetGroup.java
 *
 * Created on 24 �Զع�¹ 2548, 16:31 �.
 */
package com.pcu.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Administrator
 */
@SuppressWarnings("ClassWithoutLogger")
public class EpiSetGroup extends Persistent {

    private static final long serialVersionUID = 1L;
    public String other_description = "";
    public String description = "";
    public String active = "1";
    public boolean sent_data = false;
    public String hl7_vaccine_code = "0";
    public String b_item_manufacturer_id;
    public String act_site_code = "0";
    public String immunization_route_code = "0";

    /**
     * Creates a new instance of EpiSetGroup
     */
    public EpiSetGroup() {
    }

    public boolean getActive() {
        if (active.equals("1")) {
            return true;
        } else {
            return false;
        }
    }

    public void setActive(boolean act) {
        if (act) {
            active = "1";
        } else {
            active = "0";
        }
    }
}

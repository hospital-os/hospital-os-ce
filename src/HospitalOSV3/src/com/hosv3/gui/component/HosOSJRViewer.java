/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.gui.component;

import java.io.InputStream;
import java.util.Locale;
import java.util.ResourceBundle;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.swing.JRViewer;
import net.sf.jasperreports.swing.JRViewerToolbar;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class HosOSJRViewer extends JRViewer {

    private static final long serialVersionUID = 1L;

    public HosOSJRViewer(JasperPrint jrPrint, Locale locale, ResourceBundle resBundle) {
        super(jrPrint, locale, resBundle);
    }

    public HosOSJRViewer(InputStream is, boolean isXML, Locale locale, ResourceBundle resBundle) throws JRException {
        super(is, isXML, locale, resBundle);
    }

    public HosOSJRViewer(String fileName, boolean isXML, Locale locale, ResourceBundle resBundle) throws JRException {
        super(fileName, isXML, locale, resBundle);
    }

    public HosOSJRViewer(JasperPrint jrPrint, Locale locale) {
        super(jrPrint, locale);
    }

    public HosOSJRViewer(InputStream is, boolean isXML, Locale locale) throws JRException {
        super(is, isXML, locale);
    }

    public HosOSJRViewer(String fileName, boolean isXML, Locale locale) throws JRException {
        super(fileName, isXML, locale);
    }

    public HosOSJRViewer(JasperPrint jrPrint) {
        super(jrPrint);
    }

    public HosOSJRViewer(InputStream is, boolean isXML) throws JRException {
        super(is, isXML);
    }

    public HosOSJRViewer(String fileName, boolean isXML) throws JRException {
        super(fileName, isXML);
    }

    @Override
    protected JRViewerToolbar createToolbar() {
        return new HosOSJRViewerToolbar(this.viewerContext);
    }

    public void setVisibleSave(boolean isEnable) {
        ((HosOSJRViewerToolbar) tlbToolBar).getBtnSave().setVisible(isEnable);
    }

    public void setVisiblePrint(boolean isEnable) {
        ((HosOSJRViewerToolbar) tlbToolBar).getBtnPrint().setVisible(isEnable);
    }

    public void doClickSave() {
        ((HosOSJRViewerToolbar) tlbToolBar).getBtnSave().doClick();
    }

    public void doClickPrint() {
        ((HosOSJRViewerToolbar) tlbToolBar).getBtnPrint().doClick();
    }

    protected void loadReport(String fileName, boolean isXmlReport) throws JRException {
        viewerContext.loadReport(fileName, isXmlReport);
    }

    protected void loadReport(InputStream is, boolean isXmlReport) throws JRException {
        viewerContext.loadReport(is, isXmlReport);
    }

    protected void loadReport(JasperPrint jrPrint) {
        viewerContext.loadReport(jrPrint);
    }

    protected void refreshPage() {
        viewerContext.refreshPage();
    }
}

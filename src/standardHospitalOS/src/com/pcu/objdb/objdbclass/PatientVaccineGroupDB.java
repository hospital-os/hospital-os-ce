/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pcu.objdb.objdbclass;

import com.hospital_os.usecase.connection.ConnectionInf;
import com.pcu.object.PatientVaccineGroup;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tanakrit
 */
public class PatientVaccineGroupDB {

    public ConnectionInf theConnectionInf;
    final private String idtable = "912";

    public PatientVaccineGroupDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public int insert(PatientVaccineGroup p) throws Exception {
        p.generateOID(idtable);
        String sql = "INSERT INTO t_patient_vaccine_group( \n"
                + "               t_patient_vaccine_group_id, t_patient_id, f_vaccine_person_risk_type_id, need_vaccine, \n"
                + "               sent_data, user_record_id, user_update_id) \n"
                + "       values (?, ?, ?, ?, \n"
                + "               ?, ?, ?) ";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, p.getObjectId());
            ePQuery.setString(index++, p.t_patient_id);
            ePQuery.setInt(index++, p.f_vaccine_person_risk_type_id);
            ePQuery.setInt(index++, p.need_vaccine);
            ePQuery.setBoolean(index++, p.sent_data);
            ePQuery.setString(index++, p.user_record_id);
            ePQuery.setString(index++, p.user_update_id);
            return ePQuery.executeUpdate();
        }
    }

    public int update(PatientVaccineGroup p) throws Exception {
        String sql = "UPDATE t_patient_vaccine_group \n"
                + "      SET t_patient_id=?, f_vaccine_person_risk_type_id=?, need_vaccine=?, \n"
                + "          sent_data=?, user_record_id=?, user_update_id=? \n"
                + "    WHERE t_patient_vaccine_group_id=?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, p.t_patient_id);
            ePQuery.setInt(index++, p.f_vaccine_person_risk_type_id);
            ePQuery.setInt(index++, p.need_vaccine);
            ePQuery.setBoolean(index++, p.sent_data);
            ePQuery.setString(index++, p.user_record_id);
            ePQuery.setString(index++, p.user_update_id);
            ePQuery.setString(index++, p.getObjectId());
            return ePQuery.executeUpdate();
        }
    }

    public PatientVaccineGroup selectByPatientId(String patientId) throws Exception {
        String sql = "select * from t_patient_vaccine_group where t_patient_id = ?";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, patientId);
            List<PatientVaccineGroup> list = executeQuery(ePQuery);
            return list.isEmpty() ? null : list.get(0);
        }
    }

    public List<PatientVaccineGroup> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<PatientVaccineGroup> list = new ArrayList<>();
        try (ResultSet rs = preparedStatement.executeQuery()) {
            while (rs.next()) {
                PatientVaccineGroup p = new PatientVaccineGroup();
                p.setObjectId(rs.getString("t_patient_vaccine_group_id"));
                p.t_patient_id = rs.getString("t_patient_id");
                p.f_vaccine_person_risk_type_id = rs.getInt("f_vaccine_person_risk_type_id");
                p.need_vaccine = rs.getInt("need_vaccine");
                p.sent_data = rs.getBoolean("sent_data");
                p.record_datetime = rs.getTimestamp("record_datetime");
                p.user_record_id = rs.getString("user_record_id");
                p.update_datetime = rs.getTimestamp("update_datetime");
                p.user_update_id = rs.getString("user_update_id");
                list.add(p);
            }
            return list;
        }
    }

    public String queryImmunizationTargetByPatientId(String patientId) throws Exception {
        String sql = "select * from moph_immunization_target_V39(?)";
        try (PreparedStatement ePQuery = theConnectionInf.ePQuery(sql)) {
            int index = 1;
            ePQuery.setString(index++, patientId);
            String jsonData = "";
            try (ResultSet rs = ePQuery.executeQuery()) {
                while (rs.next()) {
                    jsonData = rs.getString(1);
                }
            }
            return jsonData;
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hosv3.gui.dialog;

import com.hospital_os.object.Patient;
import com.hospital_os.object.PatientAdl;
import com.hosv3.control.HosControl;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.Enumeration;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import org.json.JSONObject;

/**
 *
 * @author tanakrit
 */
public class DialogPatientAdl extends javax.swing.JDialog {

    private final ButtonGroup[] btnGroups;
    private HosControl theHC;
    private Patient patient;
    private PatientAdl patientAdl;
    private final ActionListener theActionListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            calResultScore();
        }
    };

    /**
     * Creates new form DialogFormAdl
     */
    public DialogPatientAdl(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.btnGroups = new ButtonGroup[]{btnGroupFeeding, btnGroupGrooming, btnGroupTransfer,
            btnGroupToiletUse, btnGroupMobility, btnGroupDressing,
            btnGroupStairs, btnGroupBathing, btnGroupBowels, btnGroupBladder};
        addActionListener();
        jScrollPane1.getVerticalScrollBar().setUnitIncrement(60);
        jScrollPane1.getVerticalScrollBar().setBlockIncrement(60);
    }

    public void setControl(HosControl hc) {
        theHC = hc;
    }

    public void showDialog(Patient patient) {
        if (patient == null) {
            return;
        }
        this.patient = patient;
        setPatientAdl(null);
        openDialog();
    }

    public void showEditDialog(Patient patient, String t_patient_adl_id) {
        if (t_patient_adl_id == null || t_patient_adl_id.isEmpty()) {
            return;
        }
        this.patient = patient;
        patientAdl = theHC.thePatientControl.selectPatientAdlById(t_patient_adl_id);
        setPatientAdl(patientAdl);
        openDialog();
    }

    private void openDialog() {
        this.setSize(1024, 768);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    private void addActionListener() {
        for (ButtonGroup btnGroup : btnGroups) {
            for (Enumeration<AbstractButton> buttons = btnGroup.getElements(); buttons.hasMoreElements();) {
                JRadioButton radioButton = (JRadioButton) buttons.nextElement();
                radioButton.addActionListener(theActionListener);
            }
        }
    }

    private void clearUI() {
        dateScreen.setDate(new Date());
        txtTotal.setText("0");
        jrBtnFeeding3.setSelected(true);
        jrBtnGrooming1.setSelected(true);
        jrBtnTransfer4.setSelected(true);
        jrBtnToiletUse3.setSelected(true);
        jrBtnMobility4.setSelected(true);
        jrBtnDressing3.setSelected(true);
        jrBtnStairs3.setSelected(true);
        jrBtnBathing2.setSelected(true);
        jrBtnBowels3.setSelected(true);
        jrBtnBladder3.setSelected(true);
    }

    private void setPatientAdl(PatientAdl thePatientAdl) {
        clearUI();
        patientAdl = thePatientAdl;
        if (patientAdl == null) {
            patientAdl = new PatientAdl();
            patientAdl.screen_date = new Date();
        }

        if (patientAdl.details != null && !patientAdl.details.isEmpty()) {
            JSONObject jsonObject = new JSONObject(patientAdl.details);
            setSelected(panelFeeding, jsonObject.getString("f_adl_feeding_id"));
            setSelected(panelGrooming, jsonObject.getString("f_adl_grooming_id"));
            setSelected(panelTransfer, jsonObject.getString("f_adl_transfer_id"));
            setSelected(panelToiletUse, jsonObject.getString("f_adl_toilet_id"));
            setSelected(panelMobility, jsonObject.getString("f_adl_mobility_id"));
            setSelected(panelDressing, jsonObject.getString("f_adl_dressing_id"));
            setSelected(panelStairs, jsonObject.getString("f_adl_stairs_id"));
            setSelected(panelBathing, jsonObject.getString("f_adl_bathing_id"));
            setSelected(panelBowels, jsonObject.getString("f_adl_bowels_id"));
            setSelected(panelBladder, jsonObject.getString("f_adl_bladder_id"));
        }
        dateScreen.setDate(patientAdl.screen_date);
        calResultScore();
    }

    private PatientAdl getPatientAdl() {
        patientAdl.screen_date = dateScreen.getDate();
        patientAdl.result_score = Integer.parseInt(txtTotal.getText());
        patientAdl.t_patient_id = patient.getObjectId();
        patientAdl.details = getDetails();
        return patientAdl;
    }

    private String getDetails() {
        JSONObject jsonObject = new JSONObject();
        for (ButtonGroup btnGroup : btnGroups) {
            jsonObject.put(getKeyId(btnGroup), getNameSelected(btnGroup));
            jsonObject.put(getKeyScore(btnGroup), getValueSelected(btnGroup));
        }
        return jsonObject.toString();
    }

    private String getKeyId(ButtonGroup btnGroup) {
        if (btnGroup == btnGroupFeeding) {
            return "f_adl_feeding_id";
        } else if (btnGroup == btnGroupGrooming) {
            return "f_adl_grooming_id";
        } else if (btnGroup == btnGroupTransfer) {
            return "f_adl_transfer_id";
        } else if (btnGroup == btnGroupToiletUse) {
            return "f_adl_toilet_id";
        } else if (btnGroup == btnGroupMobility) {
            return "f_adl_mobility_id";
        } else if (btnGroup == btnGroupDressing) {
            return "f_adl_dressing_id";
        } else if (btnGroup == btnGroupStairs) {
            return "f_adl_stairs_id";
        } else if (btnGroup == btnGroupBathing) {
            return "f_adl_bathing_id";
        } else if (btnGroup == btnGroupBowels) {
            return "f_adl_bowels_id";
        } else {
            return "f_adl_bladder_id";
        }
    }

    private String getKeyScore(ButtonGroup btnGroup) {
        if (btnGroup == btnGroupFeeding) {
            return "feeding_score";
        } else if (btnGroup == btnGroupGrooming) {
            return "grooming_score";
        } else if (btnGroup == btnGroupTransfer) {
            return "transfer_score";
        } else if (btnGroup == btnGroupToiletUse) {
            return "toilet_score";
        } else if (btnGroup == btnGroupMobility) {
            return "mobility_score";
        } else if (btnGroup == btnGroupDressing) {
            return "dressing_score";
        } else if (btnGroup == btnGroupStairs) {
            return "stairs_score";
        } else if (btnGroup == btnGroupBathing) {
            return "bathing_score";
        } else if (btnGroup == btnGroupBowels) {
            return "bowels_score";
        } else {
            return "bladder_score";
        }
    }

    private void setSelected(JPanel panel, String id) {
        for (Component component : panel.getComponents()) {
            if (component instanceof JRadioButton) {
                if (((JRadioButton) component).getName().equals(id)) {
                    ((JRadioButton) component).setSelected(true);
                }
            }
        }
    }

    private String getNameSelected(ButtonGroup btnGroup) {
        for (Enumeration<AbstractButton> buttons = btnGroup.getElements(); buttons.hasMoreElements();) {
            AbstractButton button = buttons.nextElement();

            if (button.isSelected()) {
                return button.getName();
            }
        }
        return "";
    }

    private int getValueSelected(ButtonGroup btnGroup) {
        return Integer.parseInt(btnGroup.getSelection().getActionCommand());
    }

    private void calResultScore() {
        int sum = 0;
        for (ButtonGroup btnGroup : btnGroups) {
            sum += Integer.parseInt(btnGroup.getSelection().getActionCommand());
        }
        txtTotal.setText(String.valueOf(sum));
    }

    private void doSave() {
        PatientAdl thePatientAdl = getPatientAdl();
        int ret = theHC.thePatientControl.saveOrUpdatePatientAdl(thePatientAdl);
        if (ret == 1) {
            this.dispose();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        btnGroupFeeding = new javax.swing.ButtonGroup();
        btnGroupGrooming = new javax.swing.ButtonGroup();
        btnGroupTransfer = new javax.swing.ButtonGroup();
        btnGroupToiletUse = new javax.swing.ButtonGroup();
        btnGroupMobility = new javax.swing.ButtonGroup();
        btnGroupDressing = new javax.swing.ButtonGroup();
        btnGroupStairs = new javax.swing.ButtonGroup();
        btnGroupBathing = new javax.swing.ButtonGroup();
        btnGroupBowels = new javax.swing.ButtonGroup();
        btnGroupBladder = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        panelContent = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel4 = new javax.swing.JPanel();
        panelFeeding = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jrBtnFeeding1 = new javax.swing.JRadioButton();
        jrBtnFeeding2 = new javax.swing.JRadioButton();
        jrBtnFeeding3 = new javax.swing.JRadioButton();
        panelGrooming = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jrBtnGrooming1 = new javax.swing.JRadioButton();
        jrBtnGrooming2 = new javax.swing.JRadioButton();
        panelTransfer = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jrBtnTransfer1 = new javax.swing.JRadioButton();
        jrBtnTransfer2 = new javax.swing.JRadioButton();
        jrBtnTransfer3 = new javax.swing.JRadioButton();
        jrBtnTransfer4 = new javax.swing.JRadioButton();
        panelToiletUse = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jrBtnToiletUse1 = new javax.swing.JRadioButton();
        jrBtnToiletUse2 = new javax.swing.JRadioButton();
        jrBtnToiletUse3 = new javax.swing.JRadioButton();
        panelMobility = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jrBtnMobility1 = new javax.swing.JRadioButton();
        jrBtnMobility2 = new javax.swing.JRadioButton();
        jrBtnMobility3 = new javax.swing.JRadioButton();
        jrBtnMobility4 = new javax.swing.JRadioButton();
        panelDressing = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jrBtnDressing1 = new javax.swing.JRadioButton();
        jrBtnDressing2 = new javax.swing.JRadioButton();
        jrBtnDressing3 = new javax.swing.JRadioButton();
        panelStairs = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jrBtnStairs1 = new javax.swing.JRadioButton();
        jrBtnStairs2 = new javax.swing.JRadioButton();
        jrBtnStairs3 = new javax.swing.JRadioButton();
        panelBathing = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jrBtnBathing1 = new javax.swing.JRadioButton();
        jrBtnBathing2 = new javax.swing.JRadioButton();
        panelBowels = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jrBtnBowels1 = new javax.swing.JRadioButton();
        jrBtnBowels2 = new javax.swing.JRadioButton();
        jrBtnBowels3 = new javax.swing.JRadioButton();
        panelBladder = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jrBtnBladder1 = new javax.swing.JRadioButton();
        jrBtnBladder2 = new javax.swing.JRadioButton();
        jrBtnBladder3 = new javax.swing.JRadioButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        txtTotal = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        dateScreen = new com.hosos.comp.jcalendar.JDateChooser();
        panelTool = new javax.swing.JPanel();
        btnSave = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Ẻ�����Թ�Ԩ�ѵû�Ш��ѹ �Ѫ�պ������ʹ���� (Barthel Activites of Daily Living : ADL)");
        getContentPane().setLayout(new java.awt.GridBagLayout());

        jLabel1.setFont(jLabel1.getFont().deriveFont(jLabel1.getFont().getStyle() | java.awt.Font.BOLD, jLabel1.getFont().getSize()+6));
        jLabel1.setText("Ẻ�����Թ�Ԩ�ѵû�Ш��ѹ �Ѫ�պ������ʹ���� (Barthel Activites of Daily Living : ADL)");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(15, 0, 10, 0);
        getContentPane().add(jLabel1, gridBagConstraints);

        panelContent.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        panelContent.setLayout(new java.awt.GridBagLayout());

        jScrollPane1.setAlignmentX(1.0F);
        jScrollPane1.setAlignmentY(1.0F);
        jScrollPane1.setAutoscrolls(true);
        jScrollPane1.setMinimumSize(new java.awt.Dimension(300, 200));
        jScrollPane1.setName(""); // NOI18N

        jPanel4.setLayout(new java.awt.GridBagLayout());

        panelFeeding.setLayout(new java.awt.GridBagLayout());

        jLabel2.setFont(jLabel2.getFont().deriveFont(jLabel2.getFont().getStyle() | java.awt.Font.BOLD, jLabel2.getFont().getSize()+2));
        jLabel2.setText("1. Feeding : �Ѻ��зҹ�������������������Ѻ���������º���µ��˹��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        panelFeeding.add(jLabel2, gridBagConstraints);

        btnGroupFeeding.add(jrBtnFeeding1);
        jrBtnFeeding1.setFont(jrBtnFeeding1.getFont());
        jrBtnFeeding1.setText("�������ö�ѡ�������һҡ�� ��ͧ�դ���͹���");
        jrBtnFeeding1.setActionCommand("0");
        jrBtnFeeding1.setName("1"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 0, 0);
        panelFeeding.add(jrBtnFeeding1, gridBagConstraints);

        btnGroupFeeding.add(jrBtnFeeding2);
        jrBtnFeeding2.setFont(jrBtnFeeding2.getFont());
        jrBtnFeeding2.setText("�ѡ������ͧ�����ͧ�դ����� �� �������͹�ѡ��������������͵Ѵ�繪������ �����ǧ˹��");
        jrBtnFeeding2.setActionCommand("1");
        jrBtnFeeding2.setName("2"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        panelFeeding.add(jrBtnFeeding2, gridBagConstraints);

        btnGroupFeeding.add(jrBtnFeeding3);
        jrBtnFeeding3.setFont(jrBtnFeeding3.getFont());
        jrBtnFeeding3.setSelected(true);
        jrBtnFeeding3.setText("�ѡ�������Ъ��µ���ͧ���繻���");
        jrBtnFeeding3.setActionCommand("2");
        jrBtnFeeding3.setName("3"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        panelFeeding.add(jrBtnFeeding3, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 0, 0);
        jPanel4.add(panelFeeding, gridBagConstraints);

        panelGrooming.setLayout(new java.awt.GridBagLayout());

        jLabel3.setFont(jLabel3.getFont().deriveFont(jLabel3.getFont().getStyle() | java.awt.Font.BOLD, jLabel3.getFont().getSize()+2));
        jLabel3.setText("2. Grooming : ��ҧ˹�� ��ռ� �ç�ѹ ⡹˹Ǵ ��������� 24-48 �����������ҹ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        panelGrooming.add(jLabel3, gridBagConstraints);

        btnGroupGrooming.add(jrBtnGrooming1);
        jrBtnGrooming1.setFont(jrBtnGrooming1.getFont());
        jrBtnGrooming1.setSelected(true);
        jrBtnGrooming1.setText("����ͧ�� (�����駷�������ͧ���������ػ�ó�������)");
        jrBtnGrooming1.setActionCommand("1");
        jrBtnGrooming1.setName("2"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        panelGrooming.add(jrBtnGrooming1, gridBagConstraints);

        btnGroupGrooming.add(jrBtnGrooming2);
        jrBtnGrooming2.setFont(jrBtnGrooming2.getFont());
        jrBtnGrooming2.setText("��ͧ��ä������������");
        jrBtnGrooming2.setActionCommand("0");
        jrBtnGrooming2.setName("1"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 0, 0);
        panelGrooming.add(jrBtnGrooming2, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 0, 0);
        jPanel4.add(panelGrooming, gridBagConstraints);

        panelTransfer.setLayout(new java.awt.GridBagLayout());

        jLabel4.setFont(jLabel4.getFont().deriveFont(jLabel4.getFont().getStyle() | java.awt.Font.BOLD, jLabel4.getFont().getSize()+2));
        jLabel4.setText("3. Transfer : �ء��觨ҡ���͹ ���ͨҡ��§��ѧ������");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        panelTransfer.add(jLabel4, gridBagConstraints);

        btnGroupTransfer.add(jrBtnTransfer1);
        jrBtnTransfer1.setFont(jrBtnTransfer1.getFont());
        jrBtnTransfer1.setText("�������ö����� (������Ǩ��������) ���͵�ͧ�餹�ͧ�����¡ѹ¡���");
        jrBtnTransfer1.setActionCommand("0");
        jrBtnTransfer1.setName("1"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 0, 0);
        panelTransfer.add(jrBtnTransfer1, gridBagConstraints);

        btnGroupTransfer.add(jrBtnTransfer2);
        jrBtnTransfer2.setFont(jrBtnTransfer2.getFont());
        jrBtnTransfer2.setText("��ͧ��ä���������������ҧ�ҡ�֧�й���� �� ��ͧ�餹���ç�����շѡ�� 1 �� �����餹����� 2 ����ا�ѹ����Ҩ֧�й��������");
        jrBtnTransfer2.setActionCommand("1");
        jrBtnTransfer2.setName("2"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        panelTransfer.add(jrBtnTransfer2, gridBagConstraints);

        btnGroupTransfer.add(jrBtnTransfer3);
        jrBtnTransfer3.setFont(jrBtnTransfer3.getFont());
        jrBtnTransfer3.setText("��ͧ��ä�����������ͺ�ҧ �� �͡���ӵ�� ���ͪ��¾�ا��硹��� ���͵�ͧ�դ��������ͤ�����ʹ���");
        jrBtnTransfer3.setActionCommand("2");
        jrBtnTransfer3.setName("3"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        panelTransfer.add(jrBtnTransfer3, gridBagConstraints);

        btnGroupTransfer.add(jrBtnTransfer4);
        jrBtnTransfer4.setFont(jrBtnTransfer4.getFont());
        jrBtnTransfer4.setSelected(true);
        jrBtnTransfer4.setText("������ͧ");
        jrBtnTransfer4.setActionCommand("3");
        jrBtnTransfer4.setName("4"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        panelTransfer.add(jrBtnTransfer4, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 0, 0);
        jPanel4.add(panelTransfer, gridBagConstraints);

        panelToiletUse.setLayout(new java.awt.GridBagLayout());

        jLabel5.setFont(jLabel5.getFont().deriveFont(jLabel5.getFont().getStyle() | java.awt.Font.BOLD, jLabel5.getFont().getSize()+2));
        jLabel5.setText("4. Toilet use : ����ͧ���");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        panelToiletUse.add(jLabel5, gridBagConstraints);

        btnGroupToiletUse.add(jrBtnToiletUse1);
        jrBtnToiletUse1.setFont(jrBtnToiletUse1.getFont());
        jrBtnToiletUse1.setText("��������͵���ͧ�����");
        jrBtnToiletUse1.setActionCommand("0");
        jrBtnToiletUse1.setName("1"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 0, 0);
        panelToiletUse.add(jrBtnToiletUse1, gridBagConstraints);

        btnGroupToiletUse.add(jrBtnToiletUse2);
        jrBtnToiletUse2.setFont(jrBtnToiletUse2.getFont());
        jrBtnToiletUse2.setText("����ͧ���ҧ (���ҧ���·Ӥ������Ҵ����ͧ��ѧ�ҡ���稸���) ���ͧ���������㹺ҧ���");
        jrBtnToiletUse2.setActionCommand("1");
        jrBtnToiletUse2.setName("2"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        panelToiletUse.add(jrBtnToiletUse2, gridBagConstraints);

        btnGroupToiletUse.add(jrBtnToiletUse3);
        jrBtnToiletUse3.setFont(jrBtnToiletUse3.getFont());
        jrBtnToiletUse3.setSelected(true);
        jrBtnToiletUse3.setText("��������͵���ͧ��� (��鹹�����ŧ�ҡ������ͧ�� �Ӥ������Ҵ�����º������ѧ�ҡ���稸��� �ʹ�������ͼ�������º����)");
        jrBtnToiletUse3.setActionCommand("2");
        jrBtnToiletUse3.setName("3"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        panelToiletUse.add(jrBtnToiletUse3, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 0, 0);
        jPanel4.add(panelToiletUse, gridBagConstraints);

        panelMobility.setLayout(new java.awt.GridBagLayout());

        jLabel6.setFont(jLabel6.getFont().deriveFont(jLabel6.getFont().getStyle() | java.awt.Font.BOLD, jLabel6.getFont().getSize()+2));
        jLabel6.setText("5. Mobility : �������͹���������ͧ���ͺ�ҹ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        panelMobility.add(jLabel6, gridBagConstraints);

        btnGroupMobility.add(jrBtnMobility1);
        jrBtnMobility1.setFont(jrBtnMobility1.getFont());
        jrBtnMobility1.setText("����͹�����˹�����");
        jrBtnMobility1.setActionCommand("0");
        jrBtnMobility1.setName("1"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 0, 0);
        panelMobility.add(jrBtnMobility1, gridBagConstraints);

        btnGroupMobility.add(jrBtnMobility2);
        jrBtnMobility2.setFont(jrBtnMobility2.getFont());
        jrBtnMobility2.setText("��ͧ��ö�繪��µ���ͧ�������͹������ͧ (����ͧ�դ������) ��е�ͧ����͡�����ͧ���ͻ�е���");
        jrBtnMobility2.setActionCommand("1");
        jrBtnMobility2.setName("2"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        panelMobility.add(jrBtnMobility2, gridBagConstraints);

        btnGroupMobility.add(jrBtnMobility3);
        jrBtnMobility3.setFont(jrBtnMobility3.getFont());
        jrBtnMobility3.setText("�Թ��������͹������դ����� �� ��ا �͡���ӵ�� ���͵�ͧ������ʹ㨴������ͤ�����ʹ���");
        jrBtnMobility3.setActionCommand("2");
        jrBtnMobility3.setName("3"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        panelMobility.add(jrBtnMobility3, gridBagConstraints);

        btnGroupMobility.add(jrBtnMobility4);
        jrBtnMobility4.setFont(jrBtnMobility4.getFont());
        jrBtnMobility4.setSelected(true);
        jrBtnMobility4.setText("�Թ��������͹������ͧ");
        jrBtnMobility4.setActionCommand("3");
        jrBtnMobility4.setName("4"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        panelMobility.add(jrBtnMobility4, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 0, 0);
        jPanel4.add(panelMobility, gridBagConstraints);

        panelDressing.setLayout(new java.awt.GridBagLayout());

        jLabel7.setFont(jLabel7.getFont().deriveFont(jLabel7.getFont().getStyle() | java.awt.Font.BOLD, jLabel7.getFont().getSize()+2));
        jLabel7.setText("6. Dressing : �������������ͼ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        panelDressing.add(jLabel7, gridBagConstraints);

        btnGroupDressing.add(jrBtnDressing1);
        jrBtnDressing1.setFont(jrBtnDressing1.getFont());
        jrBtnDressing1.setText("��ͧ�դ���������� ���µ���᷺ͧ��������������");
        jrBtnDressing1.setActionCommand("0");
        jrBtnDressing1.setName("1"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 0, 0);
        panelDressing.add(jrBtnDressing1, gridBagConstraints);

        btnGroupDressing.add(jrBtnDressing2);
        jrBtnDressing2.setFont(jrBtnDressing2.getFont());
        jrBtnDressing2.setText("���µ���ͧ�����ҳ������ 50 �������͵�ͧ�դ�����");
        jrBtnDressing2.setActionCommand("1");
        jrBtnDressing2.setName("2"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        panelDressing.add(jrBtnDressing2, gridBagConstraints);

        btnGroupDressing.add(jrBtnDressing3);
        jrBtnDressing3.setFont(jrBtnDressing3.getFont());
        jrBtnDressing3.setSelected(true);
        jrBtnDressing3.setText("���µ���ͧ��� (�����駡�õԴ��д�� �ٴ�Ի ����������ͼ�ҷ��Ѵ�ŧ��������������)");
        jrBtnDressing3.setActionCommand("2");
        jrBtnDressing3.setName("3"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        panelDressing.add(jrBtnDressing3, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 0, 0);
        jPanel4.add(panelDressing, gridBagConstraints);

        panelStairs.setLayout(new java.awt.GridBagLayout());

        jLabel8.setFont(jLabel8.getFont().deriveFont(jLabel8.getFont().getStyle() | java.awt.Font.BOLD, jLabel8.getFont().getSize()+2));
        jLabel8.setText("7. Stairs : ��â��ŧ�ѹ� 1 ���");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        panelStairs.add(jLabel8, gridBagConstraints);

        btnGroupStairs.add(jrBtnStairs1);
        jrBtnStairs1.setFont(jrBtnStairs1.getFont());
        jrBtnStairs1.setText("�������ö�����");
        jrBtnStairs1.setActionCommand("0");
        jrBtnStairs1.setName("1"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 0, 0);
        panelStairs.add(jrBtnStairs1, gridBagConstraints);

        btnGroupStairs.add(jrBtnStairs2);
        jrBtnStairs2.setFont(jrBtnStairs2.getFont());
        jrBtnStairs2.setText("��ͧ��ä�����");
        jrBtnStairs2.setActionCommand("1");
        jrBtnStairs2.setName("2"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        panelStairs.add(jrBtnStairs2, gridBagConstraints);

        btnGroupStairs.add(jrBtnStairs3);
        jrBtnStairs3.setFont(jrBtnStairs3.getFont());
        jrBtnStairs3.setSelected(true);
        jrBtnStairs3.setText("���ŧ���ͧ (��ҵ�ͧ������ͧ�����Թ �� Walker �е�ͧ��Ң��ŧ�����)");
        jrBtnStairs3.setActionCommand("2");
        jrBtnStairs3.setName("3"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        panelStairs.add(jrBtnStairs3, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 0, 0);
        jPanel4.add(panelStairs, gridBagConstraints);

        panelBathing.setLayout(new java.awt.GridBagLayout());

        jLabel9.setFont(jLabel9.getFont().deriveFont(jLabel9.getFont().getStyle() | java.awt.Font.BOLD, jLabel9.getFont().getSize()+2));
        jLabel9.setText("8. Bathing : ����Һ���");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        panelBathing.add(jLabel9, gridBagConstraints);

        btnGroupBathing.add(jrBtnBathing1);
        jrBtnBathing1.setFont(jrBtnBathing1.getFont());
        jrBtnBathing1.setText("��ͧ�դ��������ͷ�����");
        jrBtnBathing1.setActionCommand("0");
        jrBtnBathing1.setName("1"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 0, 0);
        panelBathing.add(jrBtnBathing1, gridBagConstraints);

        btnGroupBathing.add(jrBtnBathing2);
        jrBtnBathing2.setFont(jrBtnBathing2.getFont());
        jrBtnBathing2.setSelected(true);
        jrBtnBathing2.setText("�Һ������ͧ");
        jrBtnBathing2.setActionCommand("1");
        jrBtnBathing2.setName("2"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        panelBathing.add(jrBtnBathing2, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 0, 0);
        jPanel4.add(panelBathing, gridBagConstraints);

        panelBowels.setLayout(new java.awt.GridBagLayout());

        jLabel10.setFont(jLabel10.getFont().deriveFont(jLabel10.getFont().getStyle() | java.awt.Font.BOLD, jLabel10.getFont().getSize()+2));
        jLabel10.setText("9. Bowels : ��á��鹡�ö����ب��������� 1 �ѻ�������ҹ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        panelBowels.add(jLabel10, gridBagConstraints);

        btnGroupBowels.add(jrBtnBowels1);
        jrBtnBowels1.setFont(jrBtnBowels1.getFont());
        jrBtnBowels1.setText("��������� ���͵�ͧ��á���ǹ�ب������������");
        jrBtnBowels1.setActionCommand("0");
        jrBtnBowels1.setName("1"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 0, 0);
        panelBowels.add(jrBtnBowels1, gridBagConstraints);

        btnGroupBowels.add(jrBtnBowels2);
        jrBtnBowels2.setFont(jrBtnBowels2.getFont());
        jrBtnBowels2.setText("���������ҧ���� (�����ҧ���� 1 ���駵���ѻ����)");
        jrBtnBowels2.setActionCommand("1");
        jrBtnBowels2.setName("2"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        panelBowels.add(jrBtnBowels2, gridBagConstraints);

        btnGroupBowels.add(jrBtnBowels3);
        jrBtnBowels3.setFont(jrBtnBowels3.getFont());
        jrBtnBowels3.setSelected(true);
        jrBtnBowels3.setText("�������繻���");
        jrBtnBowels3.setActionCommand("2");
        jrBtnBowels3.setName("3"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        panelBowels.add(jrBtnBowels3, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 0, 0);
        jPanel4.add(panelBowels, gridBagConstraints);

        panelBladder.setLayout(new java.awt.GridBagLayout());

        jLabel11.setFont(jLabel11.getFont().deriveFont(jLabel11.getFont().getStyle() | java.awt.Font.BOLD, jLabel11.getFont().getSize()+2));
        jLabel11.setText("10. Bladder : ��á��鹻����������� 1 �ѻ�������ҹ��");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        panelBladder.add(jLabel11, gridBagConstraints);

        btnGroupBladder.add(jrBtnBladder1);
        jrBtnBladder1.setFont(jrBtnBladder1.getFont());
        jrBtnBladder1.setText("��������� �����������ǹ������� ���������ö�����ͧ��");
        jrBtnBladder1.setActionCommand("0");
        jrBtnBladder1.setName("1"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 0, 0);
        panelBladder.add(jrBtnBladder1, gridBagConstraints);

        btnGroupBladder.add(jrBtnBladder2);
        jrBtnBladder2.setFont(jrBtnBladder2.getFont());
        jrBtnBladder2.setText("���������ҧ���� (�繹��¡����ѹ�� 1 ����)");
        jrBtnBladder2.setActionCommand("1");
        jrBtnBladder2.setName("2"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        panelBladder.add(jrBtnBladder2, gridBagConstraints);

        btnGroupBladder.add(jrBtnBladder3);
        jrBtnBladder3.setFont(jrBtnBladder3.getFont());
        jrBtnBladder3.setSelected(true);
        jrBtnBladder3.setText("�������繻���");
        jrBtnBladder3.setActionCommand("2");
        jrBtnBladder3.setName("3"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        panelBladder.add(jrBtnBladder3, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 0, 0);
        jPanel4.add(panelBladder, gridBagConstraints);

        jScrollPane1.setViewportView(jPanel4);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelContent.add(jScrollPane1, gridBagConstraints);

        jPanel3.setLayout(new java.awt.GridBagLayout());

        jLabel12.setFont(jLabel12.getFont());
        jLabel12.setText("�ѹ�������Թ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        jPanel3.add(jLabel12, gridBagConstraints);

        txtTotal.setEditable(false);
        txtTotal.setFont(txtTotal.getFont().deriveFont(txtTotal.getFont().getStyle() | java.awt.Font.BOLD, txtTotal.getFont().getSize()+2));
        txtTotal.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtTotal.setText("0");
        txtTotal.setMinimumSize(new java.awt.Dimension(80, 24));
        txtTotal.setPreferredSize(new java.awt.Dimension(80, 24));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_END;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel3.add(txtTotal, gridBagConstraints);

        jLabel13.setFont(jLabel13.getFont());
        jLabel13.setText("�����ṹ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_END;
        gridBagConstraints.weightx = 1.0;
        jPanel3.add(jLabel13, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_END;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel3.add(dateScreen, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelContent.add(jPanel3, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        getContentPane().add(panelContent, gridBagConstraints);

        panelTool.setLayout(new java.awt.GridBagLayout());

        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/hosv3/gui/images/btn_save24.png"))); // NOI18N
        btnSave.setFocusable(false);
        btnSave.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnSave.setMaximumSize(new java.awt.Dimension(26, 26));
        btnSave.setMinimumSize(new java.awt.Dimension(26, 26));
        btnSave.setPreferredSize(new java.awt.Dimension(26, 26));
        btnSave.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_END;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        panelTool.add(btnSave, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        getContentPane().add(panelTool, gridBagConstraints);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        doSave();
    }//GEN-LAST:event_btnSaveActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup btnGroupBathing;
    private javax.swing.ButtonGroup btnGroupBladder;
    private javax.swing.ButtonGroup btnGroupBowels;
    private javax.swing.ButtonGroup btnGroupDressing;
    private javax.swing.ButtonGroup btnGroupFeeding;
    private javax.swing.ButtonGroup btnGroupGrooming;
    private javax.swing.ButtonGroup btnGroupMobility;
    private javax.swing.ButtonGroup btnGroupStairs;
    private javax.swing.ButtonGroup btnGroupToiletUse;
    private javax.swing.ButtonGroup btnGroupTransfer;
    private javax.swing.JButton btnSave;
    private com.hosos.comp.jcalendar.JDateChooser dateScreen;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JRadioButton jrBtnBathing1;
    private javax.swing.JRadioButton jrBtnBathing2;
    private javax.swing.JRadioButton jrBtnBladder1;
    private javax.swing.JRadioButton jrBtnBladder2;
    private javax.swing.JRadioButton jrBtnBladder3;
    private javax.swing.JRadioButton jrBtnBowels1;
    private javax.swing.JRadioButton jrBtnBowels2;
    private javax.swing.JRadioButton jrBtnBowels3;
    private javax.swing.JRadioButton jrBtnDressing1;
    private javax.swing.JRadioButton jrBtnDressing2;
    private javax.swing.JRadioButton jrBtnDressing3;
    private javax.swing.JRadioButton jrBtnFeeding1;
    private javax.swing.JRadioButton jrBtnFeeding2;
    private javax.swing.JRadioButton jrBtnFeeding3;
    private javax.swing.JRadioButton jrBtnGrooming1;
    private javax.swing.JRadioButton jrBtnGrooming2;
    private javax.swing.JRadioButton jrBtnMobility1;
    private javax.swing.JRadioButton jrBtnMobility2;
    private javax.swing.JRadioButton jrBtnMobility3;
    private javax.swing.JRadioButton jrBtnMobility4;
    private javax.swing.JRadioButton jrBtnStairs1;
    private javax.swing.JRadioButton jrBtnStairs2;
    private javax.swing.JRadioButton jrBtnStairs3;
    private javax.swing.JRadioButton jrBtnToiletUse1;
    private javax.swing.JRadioButton jrBtnToiletUse2;
    private javax.swing.JRadioButton jrBtnToiletUse3;
    private javax.swing.JRadioButton jrBtnTransfer1;
    private javax.swing.JRadioButton jrBtnTransfer2;
    private javax.swing.JRadioButton jrBtnTransfer3;
    private javax.swing.JRadioButton jrBtnTransfer4;
    private javax.swing.JPanel panelBathing;
    private javax.swing.JPanel panelBladder;
    private javax.swing.JPanel panelBowels;
    private javax.swing.JPanel panelContent;
    private javax.swing.JPanel panelDressing;
    private javax.swing.JPanel panelFeeding;
    private javax.swing.JPanel panelGrooming;
    private javax.swing.JPanel panelMobility;
    private javax.swing.JPanel panelStairs;
    private javax.swing.JPanel panelToiletUse;
    private javax.swing.JPanel panelTool;
    private javax.swing.JPanel panelTransfer;
    private javax.swing.JTextField txtTotal;
    // End of variables declaration//GEN-END:variables
}

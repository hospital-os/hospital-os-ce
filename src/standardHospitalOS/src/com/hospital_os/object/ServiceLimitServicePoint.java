/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;
import java.util.Date;

/**
 *
 * @author Somprasong
 */
public class ServiceLimitServicePoint extends Persistent {

    public String b_service_point_id;
    public Date time_start;
    public Date time_end;
    public int limit_appointment = 0;
    public int limit_walkin = 0;
    // in object only
    public String serviceName = "";
}

/*
 * CheckSQLDB.java
 *
 * Created on 20 �Զع�¹ 2547, 16:52 �.
 */
package com.hospital_os.objdb;

import com.hospital_os.usecase.connection.*;
import com.hospital_os.utility.*;
import java.sql.*;

/**
 *
 * @author tong
 */
@SuppressWarnings({"UseOfObsoleteCollectionType", "ClassWithoutLogger"})
public class CheckSQLDB {

    /**
     * Creates a new instance of CheckSQLDB
     */
    public ConnectionInf theConnectionInf;

    public CheckSQLDB(ConnectionInf db) {
        theConnectionInf = db;
    }

    public boolean checkSQL(String sql) throws SQLException {
        boolean result;
        PreparedStatement ps = this.theConnectionInf.ePQuery(sql);
        ResultSet rs = ps.executeQuery();
        if (rs != null) {
            result = true;
        } else {
            result = false;
        }

        return result;
    }

    public boolean checkSQLParam(String sql) throws SQLException {
        boolean result;
        PreparedStatement ps = this.theConnectionInf.ePQuery(sql);
        ps.setString(1, "");
        ps.setString(2, "");
        ResultSet rs = ps.executeQuery();
        if (rs != null) {
            result = true;
        } else {
            result = false;;
        }
        return result;
    }

    public ResultSet querySQL(String sql) throws Exception {
        return theConnectionInf.eQuery(sql);
    }

//    public ResultSet querySQLParam(String sql, String startdate, String endddate) throws SQLException {
//        ResultSet rs = null;
//
//        String tmp_sql = sql;
//        int cur_index = 0;
//        int num_qt = 0;
//        while (cur_index != -1) {
//            cur_index++;
//            cur_index = tmp_sql.indexOf("?", cur_index);
//            num_qt = num_qt + 1;
//        }
//        num_qt = num_qt - 1;     /*�ٻ�Թ�� 1 �ͺ
//         */
//
//        PreparedStatement ps = this.theConnectionInf.ePQuery(sql);
//        ps.setString(1, startdate);
//        ps.setString(2, endddate);
//        if (num_qt > 2) {
//            for (int i = 3; i <= num_qt; i++) {
//                ps.setString(i, startdate);
//                i++;
//                ps.setString(i, endddate);
//            }
//        }
//        rs = ps.executeQuery();
//        return rs;
//    }
}

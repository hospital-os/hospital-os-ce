/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.VisitSocialsecPlan;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
public class VisitSocialsecPlanDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "850";

    public VisitSocialsecPlanDB(ConnectionInf c) {
        this.connectionInf = c;
    }

    public int insert(VisitSocialsecPlan obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO t_visit_socialsec_plan(\n"
                    + "            t_visit_socialsec_plan_id, t_visit_payment_id, socialsec_number, approve_status, user_record_id, user_update_id)\n"
                    + "    VALUES (?, ?, ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            int index = 1;
            preparedStatement.setString(index++, obj.getGenID(tableId));
            preparedStatement.setString(index++, obj.t_visit_payment_id);
            preparedStatement.setString(index++, obj.socialsec_number);
            preparedStatement.setString(index++, obj.approve_status);
            preparedStatement.setString(index++, obj.user_record_id);
            preparedStatement.setString(index++, obj.user_update_id);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(VisitSocialsecPlan obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_visit_socialsec_plan\n");
            sql.append("   SET socialsec_number=?, \n");
            sql.append("       approve_status=?, active=?,\n");
            sql.append("       user_update_id=?, update_datetime = current_timestamp\n");
            sql.append(" WHERE t_visit_socialsec_plan_id = ?");
            int index = 1;
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(index++, obj.socialsec_number);
            preparedStatement.setString(index++, obj.approve_status);
            preparedStatement.setString(index++, obj.active);
            preparedStatement.setString(index++, obj.user_update_id);
            preparedStatement.setString(index++, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int inActive(VisitSocialsecPlan obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE t_visit_socialsec_plan\n");
            sql.append("   SET active=?, user_update_id=?, update_datetime = current_timestamp\n");
            sql.append(" WHERE t_visit_socialsec_plan_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.active);
            preparedStatement.setString(2, obj.user_update_id);
            preparedStatement.setString(3, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
//
    public VisitSocialsecPlan select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from t_visit_socialsec_plan where t_visit_socialsec_plan_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<VisitSocialsecPlan> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public VisitSocialsecPlan selectByVisitPaymentId(String t_visit_payment_id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from t_visit_socialsec_plan  where t_visit_payment_id = ?";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, t_visit_payment_id);
            List<VisitSocialsecPlan> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<VisitSocialsecPlan> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<VisitSocialsecPlan> list = new ArrayList<VisitSocialsecPlan>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                VisitSocialsecPlan obj = new VisitSocialsecPlan();
                obj.setObjectId(rs.getString("t_visit_socialsec_plan_id"));
                obj.t_visit_payment_id = rs.getString("t_visit_payment_id");
                obj.socialsec_number = rs.getString("socialsec_number");
                obj.approve_status = rs.getString("approve_status");
                obj.active = rs.getString("active");
                obj.user_record_id = rs.getString("user_record_id");
                obj.record_datetime = rs.getTimestamp("record_datetime");
                obj.user_update_id = rs.getString("user_update_id");
                obj.update_datetime = rs.getTimestamp("update_datetime");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }
}

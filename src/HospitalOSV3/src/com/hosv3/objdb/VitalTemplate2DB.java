/*
 * theVitalTemplate2DB.java
 *
 * Created on 20 �ѹ��¹ 2548, 10:48 �.
 */
package com.hosv3.objdb;

import com.hospital_os.objdb.VitalTemplateDB;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hosv3.object.VitalTemplate2;
import java.sql.ResultSet;
import java.util.Vector;

/**
 *
 * @author kingland
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class VitalTemplate2DB extends VitalTemplateDB {

    /**
     * Creates a new instance of theVitalTemplate2DB
     */
    public VitalTemplate2DB(ConnectionInf db) {
        super(db);
    }

    @Override
    public Vector eQuery(String sql) throws Exception {
        VitalTemplate2 p;
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql);
        while (rs.next()) {
            p = new VitalTemplate2();
            p.setObjectId(rs.getString(dbObj.pk_field));
            p.vital_template_id = rs.getString(dbObj.vital_template_id);
            p.description = rs.getString(dbObj.description);
            p.service_point = rs.getString(dbObj.service_point);
            list.add(p);
        }
        rs.close();
        return list;
    }
}

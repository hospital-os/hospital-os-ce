/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Somprasong
 */
public class FootExamAssessDetail extends Persistent {

    public String t_foot_exam_assess_id = "";
    public String f_foot_assess_type_id = "";
    public String other_detail = "";
    public String record_date_time = "";
    public String user_record_id = "";
    public String update_date_time = "";
    public String user_update_id = "";
}

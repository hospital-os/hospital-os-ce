/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.CreditCardType;
import com.hospital_os.usecase.connection.CommonInf;
import com.hospital_os.usecase.connection.ConnectionInf;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("ClassWithoutLogger")
public class CreditCardTypeDB {

    private final ConnectionInf connectionInf;
    private final String tableId = "836";

    public CreditCardTypeDB(ConnectionInf connectionInf) {
        this.connectionInf = connectionInf;
    }

    public int insert(CreditCardType obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO b_credit_card_type(\n"
                    + "            b_credit_card_type_id, code, description, active, user_record, record_datetime, user_modify, modify_datetime)\n"
                    + "    VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getGenID(tableId));
            preparedStatement.setString(2, obj.code);
            preparedStatement.setString(3, obj.description);
            preparedStatement.setString(4, obj.active);
            preparedStatement.setString(5, obj.user_record);
            preparedStatement.setString(6, obj.record_datetime);
            preparedStatement.setString(7, obj.user_modify);
            preparedStatement.setString(8, obj.modify_datetime);
            // execute insert SQL stetement
            return preparedStatement.executeUpdate();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int update(CreditCardType obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE b_credit_card_type\n");
            sql.append("   SET code=?, description=?, \n");
            sql.append("       active=?, user_modify=?, modify_datetime=?\n");
            sql.append(" WHERE b_credit_card_type_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.code);
            preparedStatement.setString(2, obj.description);
            preparedStatement.setString(3, obj.active);
            preparedStatement.setString(4, obj.user_modify);
            preparedStatement.setString(5, obj.modify_datetime);
            preparedStatement.setString(6, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
    
    public int delete(CreditCardType obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM b_credit_card_type\n");
            sql.append(" WHERE b_credit_card_type_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public int inactive(CreditCardType obj) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE b_credit_card_type\n");
            sql.append("   SET active=?, user_modify=?, modify_datetime=?\n");
            sql.append(" WHERE b_credit_card_type_id = ?");
            preparedStatement = connectionInf.ePQuery(sql.toString());
            preparedStatement.setString(1, "0");
            preparedStatement.setString(2, obj.user_modify);
            preparedStatement.setString(3, obj.modify_datetime);
            preparedStatement.setString(4, obj.getObjectId());
            // execute update SQL stetement
            return preparedStatement.executeUpdate();

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
    
    public CreditCardType select(String id) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_credit_card_type where b_credit_card_type_id = ? order by description";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, id);
            List<CreditCardType> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
    
    public CreditCardType selectByCode(String code) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_credit_card_type where code = ? order by description";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, code);
            List<CreditCardType> list = executeQuery(preparedStatement);
            return list.isEmpty() ? null : list.get(0);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<CreditCardType> list() throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_credit_card_type where active = '1' order by description";
            preparedStatement = connectionInf.ePQuery(sql);
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
    
    public List<CreditCardType> listByKeyword(String keyword, String active) throws Exception {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "select * from b_credit_card_type where active = ? and (upper(code) like upper(?) or upper(description) like upper(?)) order by description";
            preparedStatement = connectionInf.ePQuery(sql);
            preparedStatement.setString(1, active);
            preparedStatement.setString(2, "%" + (keyword == null || keyword.isEmpty() ? "" : keyword) + "%");
            preparedStatement.setString(3, "%" + (keyword == null || keyword.isEmpty() ? "" : keyword) + "%");
            return executeQuery(preparedStatement);
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }

    public List<CreditCardType> executeQuery(PreparedStatement preparedStatement) throws Exception {
        List<CreditCardType> list = new ArrayList<CreditCardType>();
        ResultSet rs = null;
        try {
            // execute SQL stetement
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                CreditCardType obj = new CreditCardType();
                obj.setObjectId(rs.getString("b_credit_card_type_id"));
                obj.code = rs.getString("code");
                obj.description = rs.getString("description");
                obj.active = rs.getString("active");
                obj.user_record = rs.getString("user_record");
                obj.record_datetime = rs.getString("record_datetime");
                obj.user_modify = rs.getString("user_modify");
                obj.modify_datetime = rs.getString("modify_datetime");
                list.add(obj);
            }
            return list;
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    public List<CommonInf> getComboboxDatasource() throws Exception {
        List<CommonInf> list = new ArrayList<CommonInf>();
        for (CreditCardType obj : list()) {
            list.add((CommonInf) obj);
        }
        return list;
    }

    
}

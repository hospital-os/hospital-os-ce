/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.object;

import com.hospital_os.usecase.connection.Persistent;

/**
 *
 * @author Khanate
 */
public class ReferPriority extends Persistent {

    public String priority_description;
    public String priority_value;

    public ReferPriority() {
    }

    public String getPriority_description() {
        return priority_description;
    }

    public void setPriority_description(String priority_description) {
        this.priority_description = priority_description;
    }

    public String getPriority_value() {
        return priority_value;
    }

    public void setPriority_value(String priority_value) {
        this.priority_value = priority_value;
    }

}

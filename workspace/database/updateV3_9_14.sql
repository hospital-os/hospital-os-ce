CREATE TABLE f_lab_std (
f_lab_std_id varchar(255) NOT NULL
, f_lab_name varchar(255)
, lab_set varchar(255)
, lab_type varchar(255)
, PRIMARY KEY (f_lab_std_id)
);

CREATE TABLE b_lab_std (
lab_std_id varchar(255) NOT NULL
, labgrp varchar(255)
, labgrp_name varchar(255)
, f_lab_std_id varchar(255)
, f_lab_name varchar(255)
, PRIMARY KEY (lab_std_id)
);

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('3343', 'HCV-RNA Viral ioad ( Quantitative)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C001', 'Glucose', '1', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C002', 'Blood Urea Nitrogen (BUN)', '1', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C003', 'Creatinine', '1', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C004', 'Uric acid', '1', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C005', 'Sodium', '1', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C006', 'Potassium', '1', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C007', 'Chloride', '1', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C008', 'Carbondioxide', '1', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C009', 'Total protein', '1', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C010', 'Albumin', '1', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C011', 'Total Bilirubin', '1', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C012', 'Direct Bilirubin', '1', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C013', 'Cholesterol', '1', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C014', 'Calcium', '1', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C015', 'Phosphorus', '1', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C016', 'Triglyceride', '1', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C017', 'SGOT', '1', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C018', 'SGPT', '1', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C019', 'Alkaline Phosphatase', '1', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C020', 'Acid Phosphatase', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C021', 'Amylase', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C022', 'LDH', '1', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C023', 'Creatine Kinase (CK)', '1', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C024', 'Creatine Kinase Isoenzyme MB', '1', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C025', 'Magnesium', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C026', 'LDL', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C027', 'HDL', '1', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C028', 'Ketone', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C029', 'Osmolarity', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C03', 'GTT ชั่วโมงที่ 1', '1', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C030', 'Glucose loading test', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C031', 'Glucose Post pandrial (2 hrs.)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C032', 'Glucose Tolerance test', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C033', 'Phenobarbital', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C034', 'Phenytoin', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C035', 'Theophylline', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C036', 'Digoxin', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C037', 'Cyclosporine', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C038', 'Globulin', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C039', 'Gamma GT', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C04', 'GTT ชั่วโมงที่ 2', '1', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C040', 'Glucose Post pandrial (1 hrs.)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C047', 'Troponin T', '1', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C05', 'GTT ชั่วโมงที่ 3', '1', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C051', 'Glucose (CSF)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C052', 'Protein CSF', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C053', 'Chloride (CSF)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C054', 'Glucose (fluid)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C055', 'Protein (fluid)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C056', 'Lactate Dehydrogenase (fluid)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C057', 'Amylase (fluid)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C058', 'Albumin (fluid)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C059', 'Urea Nitrogen (fluid)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C060', 'Creatinine (fluid)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C061', 'Sodium (fluid)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C062', 'Potassium (fluid)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C063', 'Chloride (fluid)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C064', 'Carbondioxide (fluid)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C065', 'Cholesterol (fluid)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C066', 'Triglyceride (fluid)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C067', 'Total Bilirubin (fluid)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C068', 'Direct Bilirubin (fluid)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C069', 'LDH (CSF)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C070', 'Micro Albumin', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C071', 'Protein (urine)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C072', 'Urea Nitrogen (urine)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C073', 'Creatinine (urine)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C074', 'Sodium (urine)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C075', 'Potassium (urine)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C076', 'Chloride (urine)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C077', 'Calcium (urine)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C078', 'Phosphorus (urine)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C079', 'Ketone (urine)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C080', 'Osmolarity (urine)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C081', 'Amylase (urine)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C082', 'Uric acid (urine)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C083', 'VMA (Urine)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C084', 'Creatinine (urine 24 hrs)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C085', 'Glucose (urine)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C086', 'Magnesium (urine)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C087', 'Albumin (urine)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C088', 'Protein (urine 24 hrs.)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C089', 'Urea Nitrogen (urine 24 hrs)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C090', 'Sodium (urine 24 hrs)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C091', 'Potassium (urine 24 hrs)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C092', 'Chloride (urine 24 hrs)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C093', 'Calcium (urine 24 hrs)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C094', 'Phosphorus (urine 24 hrs)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C095', 'Magnesium (urine 24 hrs)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C096', 'Uric acid (urine 24 hrs)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C097', 'Osmolarity (urine 24 hrs)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C098', 'Anti HIV (STAT)', '1', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C099', 'Pregnancy test', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C100', 'HBs Ag (STAT)', '1', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C101', 'C1 (BUN,CRE,Na,K,CI,CO2)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C102', 'C2 (Glu,BUN,CRE,Na,K,CI,CO2)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C103', 'C3 (C2,LFT)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C104', 'C4 (C2,LFT,LIPID,CARDIAC,URIC,CA,P)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C105', 'ELECTROLYTE (NA,K,CL, CO2 )', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C106', 'LFT (PROT,ALB,TBIL,DBIL,SGOT,SGPT,ALP) ', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C107', 'RENAL (CL,URIC,ALB,CA,P,ALK)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C108', 'Cardiac Enzyme (SGOT,LDH,Ck,Ck-MB)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C109', 'LIPID (CHOL.TG,HDL)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C110', 'HYPERTENSION (GLU,BUN,CR,CHOL,TG)', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C111', 'Stroke fast track', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C112', 'Acute MI fast track', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C201', 'Blood Gas Analysis', '0', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C202', 'pH', '1', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C203', 'pCO2', '1', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C204', 'pO2', '1', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C205', 'Hct', '1', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C206', 'HCO3-', '1', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C207', 'HCO3std', '1', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C208', 'TCO2', '1', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C209', 'BEecf', '1', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C210', 'BE (B)', '1', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('C211', 'SO2c', '1', 'เคมีคลินิก');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F001', 'FECAL EXAMINATION{OCCULT BLOOD,COLOR,CONSISTENCY,MOCOUS,MICROSCOPIC EXAMINATION}', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F002', 'Occult Blood', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F003', 'MICROSCOPIC EXAMINATION', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F101', 'Color', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F102', 'Consistency', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F103', 'Mucus', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F300', 'Parasite', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F301', 'Wbc', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F302', 'Rbc', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F303', 'Entamoeba histolytica', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F304', 'Entamoeba coli', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F305', 'Giardia lamblia', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F306', 'Strongyloides stercoralis', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F307', 'Hook worm', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F308', 'Trichomonas hominis', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F309', 'Enbterobius vermicularis', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F310', 'Aacaris lumbricoides', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F311', 'Taenia spp', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F312', 'Opisthorchis viverrini', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F313', 'Yeasts.', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F314', 'Pseudohypha', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F315', 'Fungus', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F316', 'Shooting star', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F317', 'Endolimax nana', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F318', 'Iodamoeba butschlii', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F319', 'Paragonimus heterotremus', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F320', 'Schistosoma japonicum', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F321', 'Schitosoma hematobium', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F322', 'Schistosoma mansoni', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F323', 'Crytosporidium spp', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F324', 'Chilomastix mesnili', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F325', 'Balantidium coli', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F326', 'Isospora', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F327', 'Truchuris trichiura', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F328', 'Capillaria philippinensis', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F329', 'Hymenolepis nana', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F330', 'Hymenolepis diminuta', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F331', 'Dipylidium caninum', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F332', 'Echinococcus granulosus', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F333', 'Echinococcus  muttilocular', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F334', 'Fasiolopsis buski', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F335', 'Opisthorchis sinensis', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F336', 'Paragonimus mekongi', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F337', 'Schistosoma mekongi', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F338', 'Heterophyes heterophyes', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F339', 'Metagonimus  yokogawi', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F340', 'Echinostoma  spp.', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('F341', 'Blastocystic hominis', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H001', 'Complete Blood Count (CBC)', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H002', 'Wbc', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H003', 'Rbc', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H004', 'Hb', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H005', 'Hct', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H006', 'Platelet', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H007', 'Differential count (DIFF)', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H008', 'RBC MORPHOLOGY', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H009', 'RBC INDICIES (MCV, MCH, MCHC)', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H010', 'ESR (SEDIMENTATION RATE)', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H011', 'Malaria (THINFILM)', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H012', 'Reticulocyte Count', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H013', 'ABO (SLIDE METHOD)', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H014', 'Rh  GROUP (SLIDE METHOD)', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H015', 'Bleeding Time', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H016', 'Coagulation Time', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H017', 'LE Cell', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H018', 'PT', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H019', 'PTT', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H020', 'TT', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H021', 'INR', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H0214', 'HCT/MB', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H022', 'OF', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H023', 'DCIP', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H024', 'HB TYPING', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H025', 'HB A1C', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H026', 'INCLUSION BODY', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H0261', 'BODY FLUID PH', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H0262', 'BODY FLUID SP. GR.', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H027', 'BODY FLUID EXAMINATION', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H028', 'Semen analysis', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H029', 'HEINZ BODY', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H030', 'G-6-PD', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H104', 'Microalbumin', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H111', 'Wucheria bancrofti', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H112', 'Brugia Malayi', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H1209', 'OF,DCIP,HCT,BL GR', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H1210', 'OF,HB TYPE', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H181', 'Mean Normal PT', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H183', 'PT- Based fibrinogen', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H191', 'Mean Normal  APTT', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H192', 'APTT Ratio', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H201', 'Mean Normal TT', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H231', 'Hemoglobin A2', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H232', 'Hemoglobin A', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H233', 'Hemoglobin F', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H234', 'Hemoglobin E', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H235', 'Hemoglobin H', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H236', 'Hemoglobin Bart s', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H237', 'Hemoglobin Constant spring', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H238', 'Hemoglobin Portland', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H239', 'Hb.S', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H240', 'Hb.C', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H241', 'Abnormal  Hb.', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H251', 'Heinz bodies', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H260', 'Color', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H261', 'Characteristic', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H262', 'WBC Count', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H263', 'RBC Count', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H264', 'Neutrophil', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H265', 'Lymphocyte', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H266', 'Monocyte', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H267', 'Eosinophil', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H268', 'Mesothelial cell', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H269', 'Reactive mosothelial cell', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H270', 'Plasma cell', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H271', 'Monosodium urate monohydrate', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H272', 'Calcium pyrophosphate dihydrat', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H273', 'Polymorphonuclear', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H274', 'Mononuclear', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H275', 'Sample partial clot', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H281', 'Appearance', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H282', 'Volume', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H283', 'Sperm cell  count', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H284', 'Total cell count', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H285', 'Motolity', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H286', 'Agglutination', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H287', 'Sperm morphology', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H288', 'RBC', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H289', 'WBC', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H701', 'Neutrophil', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H702', 'Lymphocyte', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H703', 'Monocyte', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H704', 'Eosinophil', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H705', 'Basophil', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H706', 'Atypical Lymphocyte', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H707', 'Band Form Neutrophil', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H708', 'Metamyelocyte', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H709', 'Myelocyte', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H710', 'Promyelocyte', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H711', 'Blast', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H712', 'Myeloblast', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H713', 'Lymphoblast', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H714', 'Monoblast', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H715', 'Erythroblast', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H716', 'NRBC', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H717', 'Toxic', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H718', 'Vacuolization', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H719', 'Promonocyte', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H720', 'Prolymphocyte', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H721', 'Megakaryoblast', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H722', 'Promegakaryocyte', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H723', 'Megakaryocyte', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H724', 'Pronormoblast', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H725', 'Normoblast', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H726', 'Promegaloblast', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H727', 'Megaloblast', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H728', 'Bilope neutrophil', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H729', 'Hypersecmented neutrophil', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H730', 'Baskets /smudge cell', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H801', 'Poikilocytosis', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H802', 'Anisocytosis', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H803', 'Hypochromia', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H804', 'Microcytosis', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H805', 'Macrocytosis', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H806', 'Schistocyte', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H807', 'Ovalocyte', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H808', 'Tear drop cell', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H809', 'Spherocyte', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H810', 'Target cell', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H811', 'Polychromasia', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H812', 'Basophilic stippling', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H813', 'Rouleaux formation', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H814', 'Stomatocyte', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H815', 'Echinocyte', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H816', 'Keratocyte', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H817', 'Acantocyte', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H818', 'Howell jolly bodies', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H819', 'Cabot s ring', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H820', 'Sickle cell', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H821', 'Burr cell', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H822', 'Agglutination', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H851', 'Rapid progressive', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H852', 'Slow progressive', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H853', 'Non progressive', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H854', 'Immotile', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H861', 'Non', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H862', 'Head-Head', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H863', 'Tail-Tail', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H864', 'Head-Tail', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H871', 'Normal', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H872', 'Head defect', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H873', 'Tail defect', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H874', 'Immature', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H901', 'MCV', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H902', 'MCH', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H903', 'MCHC', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H904', 'RDW', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H905', 'MPV', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('H906', 'PDW', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I001', 'RPR CARD TEST', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I002', 'TPHA', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I003', 'ASO', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I004', 'Rheumatoid Factor', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I005', 'CRP', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I006', 'C3 Complement', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I007', 'C4 Complement', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I008', 'WIDAL S TEST (O,H)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I009', 'WEIL FELIX TEST {OX2,OX19,OXK}', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I010', 'H. INFLUENZA TYPE B/AG CSF', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I011', 'N. meningitidis gr. A/Ag CSF', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I012', 'N. MENINGITIDIS GR. B/E, COLI K1 / AG CSF', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I013', 'N. meningitidis gr. C/Ag CSF ', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I015', 'S. pneumoniae / Ag CSF ', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I016', 'Strep. Group B / Ag CSF', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I017', 'Cryptococus Ag', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I018', 'E. Histolytica Ab', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I019', 'Rubella IgG', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I020', 'Rubella IgM, Titer', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I021', 'HBs Ag', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I022', 'HBs Ab', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I023', 'HBc Ab ? IgG', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I024', 'HBe Ag', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I025', 'HBe Ab', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I026', 'HBc Ab IgM', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I027', 'Ant-HAV total', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I028', 'Anti HAV-IgM', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I029', 'Anti HCV', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I030', 'Anti HIV (ปกติ)', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I031', 'Anti HIV (ฉุกเฉิน)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I032', 'Alpha-Fetoprotein (AFP)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I033', 'CEA', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I034', 'Serum HCG', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I035', 'PSA', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I036', 'CA 125', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I037', 'CA-19-9', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I038', 'Ig G', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I039', 'Ig M', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I040', 'Ig A', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I041', 'T4', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I042', 'Free T4', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I043', 'T3', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I044', 'TSH', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I045', 'Free T3', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I046', 'LH', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I047', 'FSH', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I048', 'E2(ESTRADIOL)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I049', 'Prolactin', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I050', 'Cortisol', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I051', 'ANA, Titer ', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I052', 'Anti DNA', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I053', 'Anti Sm', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I054', 'Anti nRNP', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I055', 'NT-PRO BNP', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I056', 'Beta - Crosslaps', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I057', 'N - MID Osteocalcin', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I058', 'Total P1NP', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I059', 'Rapid for Influenza', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I061', 'TFT:Thyroid Function Test', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I105', 'Anti Cardiolipin (IgA & IgG & IgM)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I1061', 'TFT (T4, FREE T4,T3,TSH)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I1062', 'ANA PROFILE (ANF, ANTI  DNA, ANTI SM, ANTI RNP)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I1063', 'ชุด ANC PACK (I001,I021,I030,H001,H013,H014,H023)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I108', 'Anti La(SSB)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I109', 'Anti Mitochondria Ab. (AMA), Titer', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I110', 'Anti Neutrophil Cytoplasmic Ab.(ANCA),Screening', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I111', 'Anti Ro(SSA)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I112', 'Anti Thrombin III', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I117', 'Bence Jone protein (Urine)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I120', 'CD4', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I121', 'CD4(NA PHA) + CBC', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I122', 'CA 15-3', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I123', 'Carbamazepine', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I125', 'Ceruloplasmin', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I126', 'CH50', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I128', 'Copper (Blood)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I129', 'Copper (Urine)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I130', 'Cryoglobulin', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I131', 'Cysticercosis Ab(Serum)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I132', 'CMV IgG Ab. , Titer', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I133', 'CMV IgM Ab. , Titer', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I135', 'Dengue IgG Ab.', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I136', 'EBV-CA IgA Ab., Titer', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I137', 'EBV-CA Ab,IgG , Titer', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I138', 'EBV-CA Ab,IgM , Titer', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I140', 'Dengue IgM Ab.', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I143', 'Gamma GT', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I144', 'Gnathostoma Ab.', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I145', 'Growth Hormone', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I146', 'HSV IgG Ab.  (Type I&II) , Serum', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I147', 'HSV IgM Ab.  (Type I&II) , Serum', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I148', 'Heterophile Ab (Mono  Test)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I150', 'HLA-B27', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I151', 'Iron', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I152', 'TIBC', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I154', 'Ferritin', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I157', 'Lead (Blood)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I158', 'Leptospira antibody (IgG) , Titer', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I159', 'Lipase', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I160', 'Lithium', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I161', 'Lupus Anticoagulant (Screening)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I162', 'FTA-ABS IgG', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I163', 'FTA-ABS IgM', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I164', 'Melioides Ab (IgG) , titer', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I166', 'Microsomal Ab.', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I167', 'Mycoplasma IgG, Titer', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I168', 'Parathyroid hormone-Intact ( PTH)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I173', 'Progesterone', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I174', 'Protein C', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I175', 'Protein S', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I176', 'Protein electrophoresis', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I181', 'Testosterone', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I182', 'Thyroglobulin Ab', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I183', 'Thyroglobulin Level', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I184', 'Toxoplasma IgG', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I185', 'Toxoplasma IgM', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I186', 'Transferrin', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I187', 'VDRL (CSF)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I188', 'Vitamin B12', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I190', 'Western s Blot ( HIV- I  Ab)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I191', 'TORCH', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I193', 'Toxoplasma IgG (CSF)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I194', 'Toxoplasma IgM (CSF)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I195', 'JE IgG Ab.(Serum)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I196', 'JE IgM Ab.(Serum)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I197', 'JE IgG Ab (CSF)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I198', 'JE IgM Ab (CSF)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I220', 'Lipase (Urine)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I302', 'Arsenic (Blood)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I303', 'Arsenic (Urine)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I304', 'Chromosome study, Blood', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I306', 'D-Dimer', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I307', 'Depakine (Valporic acid)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I308', 'Diazepam (Valium)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I309', 'Folic acid (Folate, serum)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I310', 'Hemocysteine', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I311', 'Insulin', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I312', 'Mercury (Hg), Blood', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I313', 'Measle IgM, Titer', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I314', 'Myoglobin', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I315', 'Pemphigus Ab (Serum)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I316', 'Scrub Typhus Ab., Titer', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I319', 'ACTH', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I320', 'Anti DN ase-B', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I321', 'Aldosterone(Serum)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I322', 'Aldolase', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I323', 'Anti-Scl-70', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I324', 'Anti-Smooth muscle Ab (ASMA), Screening', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I325', 'C-Peptide', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I326', 'Factor V', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I327', 'Factor VIII', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I328', 'Factor IX', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I329', 'Von Willebrand Factor', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I330', 'Fibrinogen', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I331', 'Insulin-Like Growth Factor1', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I332', 'Insulin-Like Growth Factor-BP3', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I333', 'Lactate', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I334', 'Legionella-IgG, Titer', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I335', 'Legionella-IgM, Titer', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I336', 'Measle IgG, Titer', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I337', 'Mycoplasma IgM, Titer', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I338', 'Theophylline II', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I339', 'Zinc(Zn), Blood', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I340', 'HBV-DNA PCR (Qualitative)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I341', 'HBV-DNA Viral load (Quantitative)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I342', 'HCV-RNA PCR (Qualitative)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I343', 'HCV-RNA Viral load (Quantitative)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I344', 'HCV-Genotype', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I345', 'HIV DNA PCR (Qualitative)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I346', 'HIV RNA Viral load  (Quantitative)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I347', 'PCR for CMV-DNA (Qualitative)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I348', 'PCR for HSV-DNA (Qualitative)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I349', 'PCR for Alpha-Thalassemia1', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I350', 'IgE Total', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I351', 'Anti-B2 Glycoprotein', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I352', 'Alcohol (Ethanol), Blood ', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I353', 'Anti-Liver-Kidney Microsome (LKM-1)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I354', 'Anti- thyroid Peroxidase (TPO)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I355', 'Erythropoietin (EPO)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I356', 'Galactomanan', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I359', 'Prograf Level (Tacrolimus)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I360', 'Renin blood', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I361', 'PCR for DENGUE (Qualitative)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I362', 'PCR for EBV-DNA (Qualitative)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I363', 'PCR for ENTEROVIRUS(Qualitative)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I364', 'PCR for VZV-DNA (Qualitative)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I371', 'Copper (urine 24 hr)', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I372', 'Alpha -thalassemia 1 (SEA Type)', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I373', 'Alpha-thalassemia 1 (Thai Type)', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I401', 'TIBC', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I402', 'T-sat', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I411', 'Fibrinogen', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I412', 'Leptospira (IgG + IgM) Titer', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I901', 'Widal : O', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I902', 'Widal : H', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I903', 'Weil Felix : OX-2', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I904', 'Weil Felix : OX-19', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I905', 'Weil Felix : OX-K', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I906', 'Transferin Saturation', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I907', 'Scrub Typhus IgM', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I908', 'Scrub Typhus IgG', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I909', 'Murine Typhus IgM', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I910', 'Murine Typhus IgM', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I911', 'Dengue IgG : DEN 1 Ab', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I912', 'Dengue IgG : DEN 3 Ab', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I913', '%CD4', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I914', 'CD4 Count', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I921', 'Albumin', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I922', 'Alpha 1 globulin', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I923', 'Alpha 2 globulin', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I924', 'Beta globulin', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I925', 'Gamma globulin', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I926', 'Total protein', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I927', 'A/G ratio', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I931', 'Anti Cardiolipin IgG', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I932', 'Anti Cardiolipin IgA', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I933', 'Anti Cardiolipin IgM', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I935', 'P-ANCA', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I936', 'C-ANCA', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I937', 'HIV RNA', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I938', 'HIV 1 RNA Log', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I939', 'Metanephrine', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I940', 'Normetanephine', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I941', 'HBV-DNA', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I942', 'HBV-DNA Log', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I943', 'HCV-RNA', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I944', 'HCV-RNA Log', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I945', 'Lupus Screening (LA1)', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I946', 'Lupus Screening (LA2)', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I947', 'LA1/LA2 ratio', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I948', 'Lupus Comment', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I961', 'ANA Homogeneous pattern', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I962', 'ANA Peripheral pattern', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I963', 'ANA Speckled pattern', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I964', 'Fine speckled pattern', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I965', 'Centromere speckled pattern', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I966', 'Mutiple nucleare dots pattern', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I967', 'ANA Necleolar pattern', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I968', 'Cytoplasmic Staining pattern', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I969', 'Fine granular pattern', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I970', 'Coarse granular pattern', '0', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('I971', 'ANA Speckle', '1', 'อิมมูนวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('M011', 'AFB culture, conventional method ', '0', 'จุลชืววืทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('M017', 'Adenosine Deaminase (ADA)', '0', 'จุลชืววืทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('M018', 'Clostidium Deffcile Toxin (A & B)', '0', 'จุลชืววืทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('M031', 'PCR for TB (Qualitative)', '0', 'จุลชืววืทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('T001', 'ETHANOL(ALCOHOL)', '0', 'พิษวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('T002', 'ACETAMINOPHEN (PARACETAMOL)', '0', 'พิษวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('T007', 'SALICYLATE (ASPIRIN)', '0', 'พิษวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('T008', 'CHOLIN ESTERASE', '0', 'พิษวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('T009', 'METHAMPHETAMINE GROUP (AMPHETAMINE)', '0', 'พิษวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('T010', 'OPIATE GROUP (MORPHINE/CODEINE/HEROIN)', '0', 'พิษวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('T011', 'CANNABINOIDS (MARIJUANA)', '0', 'พิษวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('T013', 'BARBITURATES,', '0', 'พิษวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('T014', 'BENZODIAZEPINES,', '0', 'พิษวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('T015', 'PHENOTHIAZINE', '0', 'พิษวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('T016', 'PARAQUAT (GRAMMOXONE),', '0', 'พิษวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('T017', 'CHOLIN ESTERASE INHIBITOR (ORGANOPHOSPHATE)', '0', 'พิษวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('T019', 'TRICYCLIC  ANTIDEPRESSANTS,', '0', 'พิษวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U001', 'UA (URINALYSIS)', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U002', 'PHYSICAL AND CHEMICAL EXAMINATION', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U003', 'Specific Gravity', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U004', 'PH', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U005', 'Protein', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U006', 'Sugar', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U007', 'Ketone', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U008', 'Blood', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U009', 'Urobilinogen', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U010', 'Bilirubin', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U011', 'Nitrite', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U012', 'MICROSCOPIC EXAMINATION', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U013', 'Pregnancy test (Urine)', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U015', 'STRIP TEST (URINE)', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U101', 'Wbc', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U102', 'Rbc', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U103', 'Bacteria', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U104', 'Squamous epithelial', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U105', 'Transitional epithelial', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U106', 'Renal epithelial', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U107', 'WBC Cast', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U108', 'RBC Cast', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U109', 'Coarse granular cast', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U110', 'Fine granular cast', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U111', 'Fatty cast', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U112', 'Hyaline cast', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U113', 'Mix cell cast', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U114', 'Epithelial cast', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U115', 'Heme cast', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U116', 'Mucous thread', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U117', 'Amorphous', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U118', 'Calcium oxalate', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U119', 'Triple posphate', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U120', 'Uric acid', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U121', 'Oval fat body', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U122', 'T. vaginalis', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U123', 'Yeast cells', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U124', 'Pseudohyphae', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U125', 'Spermatozoa', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U126', 'Hippuric acid', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U127', 'Cystine', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U128', 'Leucine', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U129', 'Tyrosine', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U130', 'Bilirubin crystal', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U131', 'Cholesterol crystal', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U132', 'Hemosiderin', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U133', 'Sulfa crystal', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U134', 'Calcium plate', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U135', 'Ammonium biurate', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U136', 'Ammonium urate', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U137', 'Cylindroid', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U138', 'Calcium sulphate', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U139', 'Calcium carbonate', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U140', 'Suspected malignancy cells', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U141', 'Fat droplet', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U142', 'T.hominis', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U143', 'E.histolytica', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U144', 'Enterobius vermicularis', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U145', 'Schistosoma', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U146', 'Starch granule', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U147', 'Waxy cast', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U148', 'Undenti  Cystal', '0', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U201', 'Color', '1', 'โลหิตวิทยา');

INSERT INTO f_lab_std (f_lab_std_id, f_lab_name, lab_set, lab_type) VALUES ('U202', 'Transparency', '1', 'โลหิตวิทยา');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000001', 'C032', 'Glucose Tolerance test', 'C001', 'Glucose');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000002', 'C032', 'Glucose Tolerance test', 'C03', 'GTT ชั่วโมงที่ 1');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000003', 'C032', 'Glucose Tolerance test', 'C04', 'GTT ชั่วโมงที่ 2');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000004', 'C032', 'Glucose Tolerance test', 'C05', 'GTT ชั่วโมงที่ 3');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000005', 'C101', 'C1 (BUN,Cre,Na,K,Cl,CO2)', 'C002', 'Blood Urea Nitrogen (BUN)');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000006', 'C101', 'C1 (BUN,Cre,Na,K,Cl,CO2)', 'C003', 'Creatinine');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000007', 'C101', 'C1 (BUN,Cre,Na,K,Cl,CO2)', 'C005', 'Sodium');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000008', 'C101', 'C1 (BUN,Cre,Na,K,Cl,CO2)', 'C006', 'Potassium');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000009', 'C101', 'C1 (BUN,Cre,Na,K,Cl,CO2)', 'C007', 'Chloride');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000010', 'C101', 'C1 (BUN,Cre,Na,K,Cl,CO2)', 'C008', 'Carbondioxide');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000011', 'C102', 'C2 (Glu,BUN,Cre,Na,K,Cl,CO2)', 'C001', 'Glucose');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000012', 'C102', 'C2 (Glu,BUN,Cre,Na,K,Cl,CO2)', 'C002', 'Blood Urea Nitrogen (BUN)');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000013', 'C102', 'C2 (Glu,BUN,Cre,Na,K,Cl,CO2)', 'C003', 'Creatinine');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000014', 'C102', 'C2 (Glu,BUN,Cre,Na,K,Cl,CO2)', 'C005', 'Sodium');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000015', 'C102', 'C2 (Glu,BUN,Cre,Na,K,Cl,CO2)', 'C006', 'Potassium');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000016', 'C102', 'C2 (Glu,BUN,Cre,Na,K,Cl,CO2)', 'C007', 'Chloride');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000017', 'C102', 'C2 (Glu,BUN,Cre,Na,K,Cl,CO2)', 'C008', 'Carbondioxide');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000018', 'C103', 'C3 (C2+LFT)', 'C001', 'Glucose');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000019', 'C103', 'C3 (C2+LFT)', 'C002', 'Blood Urea Nitrogen (BUN)');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000020', 'C103', 'C3 (C2+LFT)', 'C003', 'Creatinine');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000021', 'C103', 'C3 (C2+LFT)', 'C005', 'Sodium');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000022', 'C103', 'C3 (C2+LFT)', 'C006', 'Potassium');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000023', 'C103', 'C3 (C2+LFT)', 'C007', 'Chloride');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000024', 'C103', 'C3 (C2+LFT)', 'C008', 'Carbondioxide');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000025', 'C103', 'C3 (C2+LFT)', 'C009', 'Total protein');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000026', 'C103', 'C3 (C2+LFT)', 'C010', 'Albumin');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000027', 'C103', 'C3 (C2+LFT)', 'C011', 'Total Bilirubin');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000028', 'C103', 'C3 (C2+LFT)', 'C012', 'Direct Bilirubin');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000029', 'C103', 'C3 (C2+LFT)', 'C017', 'SGOT');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000030', 'C103', 'C3 (C2+LFT)', 'C018', 'SGPT');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000031', 'C103', 'C3 (C2+LFT)', 'C019', 'Alkaline Phosphatase');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000032', 'C104', 'C4 (C2+LFT+Lipid+Cardia+Uric,Ca,P)', 'C001', 'Glucose');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000033', 'C104', 'C4 (C2+LFT+Lipid+Cardia+Uric,Ca,P)', 'C002', 'Blood Urea Nitrogen (BUN)');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000034', 'C104', 'C4 (C2+LFT+Lipid+Cardia+Uric,Ca,P)', 'C003', 'Creatinine');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000035', 'C104', 'C4 (C2+LFT+Lipid+Cardia+Uric,Ca,P)', 'C004', 'Uric acid');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000036', 'C104', 'C4 (C2+LFT+Lipid+Cardia+Uric,Ca,P)', 'C005', 'Sodium');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000037', 'C104', 'C4 (C2+LFT+Lipid+Cardia+Uric,Ca,P)', 'C006', 'Potassium');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000038', 'C104', 'C4 (C2+LFT+Lipid+Cardia+Uric,Ca,P)', 'C007', 'Chloride');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000039', 'C104', 'C4 (C2+LFT+Lipid+Cardia+Uric,Ca,P)', 'C008', 'Carbondioxide');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000040', 'C104', 'C4 (C2+LFT+Lipid+Cardia+Uric,Ca,P)', 'C009', 'Total protein');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000041', 'C104', 'C4 (C2+LFT+Lipid+Cardia+Uric,Ca,P)', 'C010', 'Albumin');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000042', 'C104', 'C4 (C2+LFT+Lipid+Cardia+Uric,Ca,P)', 'C011', 'Total Bilirubin');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000043', 'C104', 'C4 (C2+LFT+Lipid+Cardia+Uric,Ca,P)', 'C012', 'Direct Bilirubin');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000044', 'C104', 'C4 (C2+LFT+Lipid+Cardia+Uric,Ca,P)', 'C013', 'Cholesterol');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000045', 'C104', 'C4 (C2+LFT+Lipid+Cardia+Uric,Ca,P)', 'C014', 'Calcium');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000046', 'C104', 'C4 (C2+LFT+Lipid+Cardia+Uric,Ca,P)', 'C015', 'Phosphorus');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000047', 'C104', 'C4 (C2+LFT+Lipid+Cardia+Uric,Ca,P)', 'C016', 'Triglyceride');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000048', 'C104', 'C4 (C2+LFT+Lipid+Cardia+Uric,Ca,P)', 'C017', 'SGOT');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000049', 'C104', 'C4 (C2+LFT+Lipid+Cardia+Uric,Ca,P)', 'C018', 'SGPT');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000050', 'C104', 'C4 (C2+LFT+Lipid+Cardia+Uric,Ca,P)', 'C019', 'Alkaline Phosphatase');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000051', 'C104', 'C4 (C2+LFT+Lipid+Cardia+Uric,Ca,P)', 'C022', 'LDH');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000052', 'C104', 'C4 (C2+LFT+Lipid+Cardia+Uric,Ca,P)', 'C023', 'Creatine Kinase (CK)');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000053', 'C104', 'C4 (C2+LFT+Lipid+Cardia+Uric,Ca,P)', 'C024', 'Creatine Kinase Isoenzyme MB');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000054', 'C104', 'C4 (C2+LFT+Lipid+Cardia+Uric,Ca,P)', 'C027', 'HDL');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000055', 'C105', 'E+ (Na,K,Cl,CO2)', 'C005', 'Sodium');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000056', 'C105', 'E+ (Na,K,Cl,CO2)', 'C006', 'Potassium');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000057', 'C105', 'E+ (Na,K,Cl,CO2)', 'C007', 'Chloride');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000058', 'C105', 'E+ (Na,K,Cl,CO2)', 'C008', 'Carbondioxide');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000059', 'C106', 'LFT (TP,Alb,T-Bil,D-Bil,SGOT,SGPT,Alk P)', 'C009', 'Total protein');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000060', 'C106', 'LFT (TP,Alb,T-Bil,D-Bil,SGOT,SGPT,Alk P)', 'C010', 'Albumin');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000061', 'C106', 'LFT (TP,Alb,T-Bil,D-Bil,SGOT,SGPT,Alk P)', 'C011', 'Total Bilirubin');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000062', 'C106', 'LFT (TP,Alb,T-Bil,D-Bil,SGOT,SGPT,Alk P)', 'C012', 'Direct Bilirubin');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000063', 'C106', 'LFT (TP,Alb,T-Bil,D-Bil,SGOT,SGPT,Alk P)', 'C017', 'SGOT');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000064', 'C106', 'LFT (TP,Alb,T-Bil,D-Bil,SGOT,SGPT,Alk P)', 'C018', 'SGPT');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000065', 'C106', 'LFT (TP,Alb,T-Bil,D-Bil,SGOT,SGPT,Alk P)', 'C019', 'Alkaline Phosphatase');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000066', 'C107', 'Renal (C1+Uric,Alb,Ca,P,Alk P)', 'C002', 'Blood Urea Nitrogen (BUN)');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000067', 'C107', 'Renal (C1+Uric,Alb,Ca,P,Alk P)', 'C003', 'Creatinine');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000068', 'C107', 'Renal (C1+Uric,Alb,Ca,P,Alk P)', 'C004', 'Uric acid');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000069', 'C107', 'Renal (C1+Uric,Alb,Ca,P,Alk P)', 'C005', 'Sodium');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000070', 'C107', 'Renal (C1+Uric,Alb,Ca,P,Alk P)', 'C006', 'Potassium');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000071', 'C107', 'Renal (C1+Uric,Alb,Ca,P,Alk P)', 'C007', 'Chloride');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000072', 'C107', 'Renal (C1+Uric,Alb,Ca,P,Alk P)', 'C008', 'Carbondioxide');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000073', 'C107', 'Renal (C1+Uric,Alb,Ca,P,Alk P)', 'C010', 'Albumin');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000074', 'C107', 'Renal (C1+Uric,Alb,Ca,P,Alk P)', 'C014', 'Calcium');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000075', 'C107', 'Renal (C1+Uric,Alb,Ca,P,Alk P)', 'C015', 'Phosphorus');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000076', 'C107', 'Renal (C1+Uric,Alb,Ca,P,Alk P)', 'C019', 'Alkaline Phosphatase');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000077', 'C108', 'Cardiac (SGOT,LDH,CK,CK-MB)', 'C017', 'SGOT');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000078', 'C108', 'Cardiac (SGOT,LDH,CK,CK-MB)', 'C022', 'LDH');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000079', 'C108', 'Cardiac (SGOT,LDH,CK,CK-MB)', 'C023', 'Creatine Kinase (CK)');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000080', 'C108', 'Cardiac (SGOT,LDH,CK,CK-MB)', 'C024', 'Creatine Kinase Isoenzyme MB');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000081', 'C109', 'Lipid (Chol,TG,HDL) Fasting 12 Hrs.', 'C013', 'Cholesterol');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000082', 'C109', 'Lipid (Chol,TG,HDL) Fasting 12 Hrs.', 'C016', 'Triglyceride');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000083', 'C109', 'Lipid (Chol,TG,HDL) Fasting 12 Hrs.', 'C027', 'HDL');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000084', 'C110', 'HT (Glu,Bun,Cre,Chol,TG)', 'C001', 'Glucose');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000085', 'C110', 'HT (Glu,Bun,Cre,Chol,TG)', 'C002', 'Blood Urea Nitrogen (BUN)');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000086', 'C110', 'HT (Glu,Bun,Cre,Chol,TG)', 'C003', 'Creatinine');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000087', 'C110', 'HT (Glu,Bun,Cre,Chol,TG)', 'C013', 'Cholesterol');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000088', 'C110', 'HT (Glu,Bun,Cre,Chol,TG)', 'C016', 'Triglyceride');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000089', 'C111', 'Stroke fast track', 'C001', 'Glucose');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000090', 'C111', 'Stroke fast track', 'C002', 'Blood Urea Nitrogen (BUN)');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000091', 'C111', 'Stroke fast track', 'C003', 'Creatinine');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000092', 'C111', 'Stroke fast track', 'C005', 'Sodium');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000093', 'C111', 'Stroke fast track', 'C006', 'Potassium');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000094', 'C111', 'Stroke fast track', 'C007', 'Chloride');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000095', 'C111', 'Stroke fast track', 'C008', 'Carbondioxide');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000096', 'C111', 'Stroke fast track', 'H001', 'Complete Blood Count (CBC)');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000097', 'C111', 'Stroke fast track', 'H002', 'Wbc');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000098', 'C111', 'Stroke fast track', 'H003', 'Rbc');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000099', 'C111', 'Stroke fast track', 'H004', 'Hb');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000100', 'C111', 'Stroke fast track', 'H005', 'Hct');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000101', 'C111', 'Stroke fast track', 'H006', 'Platelet');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000102', 'C111', 'Stroke fast track', 'H007', 'Differential count (DIFF)');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000103', 'C111', 'Stroke fast track', 'H009', 'RBC INDICIES (MCV, MCH, MCHC)');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000104', 'C111', 'Stroke fast track', 'H018', 'PT');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000105', 'C111', 'Stroke fast track', 'H018', 'PT');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000106', 'C111', 'Stroke fast track', 'H019', 'PTT');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000107', 'C111', 'Stroke fast track', 'H019', 'PTT');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000108', 'C111', 'Stroke fast track', 'H021', 'INR');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000109', 'C111', 'Stroke fast track', 'H181', 'Mean Normal PT');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000110', 'C111', 'Stroke fast track', 'H191', 'Mean Normal  APTT');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000111', 'C111', 'Stroke fast track', 'H192', 'APTT Ratio');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000112', 'C111', 'Stroke fast track', 'H701', 'Neutrophil');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000113', 'C111', 'Stroke fast track', 'H702', 'Lymphocyte');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000114', 'C111', 'Stroke fast track', 'H703', 'Monocyte');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000115', 'C111', 'Stroke fast track', 'H704', 'Eosinophil');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000116', 'C111', 'Stroke fast track', 'H705', 'Basophil');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000117', 'C111', 'Stroke fast track', 'H706', 'Atypical Lymphocyte');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000118', 'C111', 'Stroke fast track', 'H707', 'Band Form Neutrophil');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000119', 'C111', 'Stroke fast track', 'H708', 'Metamyelocyte');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000120', 'C111', 'Stroke fast track', 'H709', 'Myelocyte');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000121', 'C111', 'Stroke fast track', 'H710', 'Promyelocyte');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000122', 'C111', 'Stroke fast track', 'H711', 'Blast');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000123', 'C111', 'Stroke fast track', 'H716', 'NRBC');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000124', 'C111', 'Stroke fast track', 'H901', 'MCV');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000125', 'C111', 'Stroke fast track', 'H902', 'MCH');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000126', 'C111', 'Stroke fast track', 'H903', 'MCHC');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000127', 'C112', 'Acute MI fast track', 'C001', 'Glucose');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000128', 'C112', 'Acute MI fast track', 'C002', 'Blood Urea Nitrogen (BUN)');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000129', 'C112', 'Acute MI fast track', 'C003', 'Creatinine');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000130', 'C112', 'Acute MI fast track', 'C005', 'Sodium');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000131', 'C112', 'Acute MI fast track', 'C006', 'Potassium');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000132', 'C112', 'Acute MI fast track', 'C007', 'Chloride');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000133', 'C112', 'Acute MI fast track', 'C008', 'Carbondioxide');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000134', 'C112', 'Acute MI fast track', 'C023', 'Creatine Kinase (CK)');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000135', 'C112', 'Acute MI fast track', 'C024', 'Creatine Kinase Isoenzyme MB');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000136', 'C112', 'Acute MI fast track', 'C047', 'Troponin T');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000137', 'C112', 'Acute MI fast track', 'C098', 'Anti HIV (STAT)');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000138', 'C112', 'Acute MI fast track', 'C100', 'HBs Ag (STAT)');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000139', 'C112', 'Acute MI fast track', 'H001', 'Complete Blood Count (CBC)');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000140', 'C112', 'Acute MI fast track', 'H002', 'Wbc');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000141', 'C112', 'Acute MI fast track', 'H003', 'Rbc');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000142', 'C112', 'Acute MI fast track', 'H004', 'Hb');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000143', 'C112', 'Acute MI fast track', 'H005', 'Hct');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000144', 'C112', 'Acute MI fast track', 'H006', 'Platelet');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000145', 'C112', 'Acute MI fast track', 'H007', 'Differential count (DIFF)');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000146', 'C112', 'Acute MI fast track', 'H009', 'RBC INDICIES (MCV, MCH, MCHC)');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000147', 'C112', 'Acute MI fast track', 'H018', 'PT');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000148', 'C112', 'Acute MI fast track', 'H018', 'PT');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000149', 'C112', 'Acute MI fast track', 'H019', 'PTT');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000150', 'C112', 'Acute MI fast track', 'H019', 'PTT');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000151', 'C112', 'Acute MI fast track', 'H021', 'INR');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000152', 'C112', 'Acute MI fast track', 'H181', 'Mean Normal PT');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000153', 'C112', 'Acute MI fast track', 'H191', 'Mean Normal  APTT');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000154', 'C112', 'Acute MI fast track', 'H192', 'APTT Ratio');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000155', 'C112', 'Acute MI fast track', 'H701', 'Neutrophil');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000156', 'C112', 'Acute MI fast track', 'H702', 'Lymphocyte');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000157', 'C112', 'Acute MI fast track', 'H703', 'Monocyte');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000158', 'C112', 'Acute MI fast track', 'H704', 'Eosinophil');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000159', 'C112', 'Acute MI fast track', 'H705', 'Basophil');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000160', 'C112', 'Acute MI fast track', 'H706', 'Atypical Lymphocyte');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000161', 'C112', 'Acute MI fast track', 'H707', 'Band Form Neutrophil');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000162', 'C112', 'Acute MI fast track', 'H708', 'Metamyelocyte');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000163', 'C112', 'Acute MI fast track', 'H709', 'Myelocyte');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000164', 'C112', 'Acute MI fast track', 'H710', 'Promyelocyte');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000165', 'C112', 'Acute MI fast track', 'H711', 'Blast');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000166', 'C112', 'Acute MI fast track', 'H716', 'NRBC');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000167', 'C112', 'Acute MI fast track', 'H901', 'MCV');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000168', 'C112', 'Acute MI fast track', 'H902', 'MCH');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000169', 'C112', 'Acute MI fast track', 'H903', 'MCHC');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000170', 'C201', 'Blood Gas Analysis', 'C202', 'pH');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000171', 'C201', 'Blood Gas Analysis', 'C203', 'pCO2');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000172', 'C201', 'Blood Gas Analysis', 'C204', 'pO2');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000173', 'C201', 'Blood Gas Analysis', 'C205', 'Hct');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000174', 'C201', 'Blood Gas Analysis', 'C206', 'HCO3-');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000175', 'C201', 'Blood Gas Analysis', 'C207', 'HCO3std');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000176', 'C201', 'Blood Gas Analysis', 'C208', 'TCO2');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000177', 'C201', 'Blood Gas Analysis', 'C209', 'BEecf');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000178', 'C201', 'Blood Gas Analysis', 'C210', 'BE (B)');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000179', 'C201', 'Blood Gas Analysis', 'C211', 'SO2c');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000180', 'F001', 'FECAL EXAMINATION', 'F002', 'Occult Blood');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000181', 'F001', 'FECAL EXAMINATION', 'F003', 'MICROSCOPIC EXAMINATION');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000182', 'F001', 'FECAL EXAMINATION', 'F101', 'Color');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000183', 'F001', 'FECAL EXAMINATION', 'F102', 'Consistency');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000184', 'F001', 'FECAL EXAMINATION', 'F103', 'Mucus');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000185', 'F001', 'FECAL EXAMINATION', 'F300', 'Parasite');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000186', 'F001', 'FECAL EXAMINATION', 'F301', 'Wbc');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000187', 'F001', 'FECAL EXAMINATION', 'F302', 'Rbc');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000188', 'F001', 'FECAL EXAMINATION', 'F303', 'Entamoeba histolytica');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000189', 'F001', 'FECAL EXAMINATION', 'F304', 'Entamoeba coli');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000190', 'F001', 'FECAL EXAMINATION', 'F305', 'Giardia lamblia');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000191', 'F001', 'FECAL EXAMINATION', 'F306', 'Strongyloides stercoralis');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000192', 'F001', 'FECAL EXAMINATION', 'F307', 'Hook worm');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000193', 'F001', 'FECAL EXAMINATION', 'F308', 'Trichomonas hominis');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000194', 'F001', 'FECAL EXAMINATION', 'F309', 'Enbterobius vermicularis');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000195', 'F001', 'FECAL EXAMINATION', 'F310', 'Aacaris lumbricoides');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000196', 'F001', 'FECAL EXAMINATION', 'F311', 'Taenia spp');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000197', 'F001', 'FECAL EXAMINATION', 'F313', 'Yeasts.');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000198', 'F001', 'FECAL EXAMINATION', 'F314', 'Pseudohypha');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000199', 'F001', 'FECAL EXAMINATION', 'F315', 'Fungus');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000200', 'F001', 'FECAL EXAMINATION', 'F316', 'Shooting star');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000201', 'F001', 'FECAL EXAMINATION', 'F327', 'Truchuris trichiura');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000202', 'H001', 'CBC (H002 - H009)', 'H002', 'Wbc');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000203', 'H001', 'CBC (H002 - H009)', 'H003', 'Rbc');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000204', 'H001', 'CBC (H002 - H009)', 'H004', 'Hb');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000205', 'H001', 'CBC (H002 - H009)', 'H005', 'Hct');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000206', 'H001', 'CBC (H002 - H009)', 'H006', 'Platelet');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000207', 'H001', 'CBC (H002 - H009)', 'H007', 'Differential count (DIFF)');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000208', 'H001', 'CBC (H002 - H009)', 'H009', 'RBC INDICIES (MCV, MCH, MCHC)');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000209', 'H001', 'CBC (H002 - H009)', 'H701', 'Neutrophil');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000210', 'H001', 'CBC (H002 - H009)', 'H702', 'Lymphocyte');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000211', 'H001', 'CBC (H002 - H009)', 'H703', 'Monocyte');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000212', 'H001', 'CBC (H002 - H009)', 'H704', 'Eosinophil');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000213', 'H001', 'CBC (H002 - H009)', 'H705', 'Basophil');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000214', 'H001', 'CBC (H002 - H009)', 'H706', 'Atypical Lymphocyte');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000215', 'H001', 'CBC (H002 - H009)', 'H707', 'Band Form Neutrophil');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000216', 'H001', 'CBC (H002 - H009)', 'H708', 'Metamyelocyte');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000217', 'H001', 'CBC (H002 - H009)', 'H709', 'Myelocyte');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000218', 'H001', 'CBC (H002 - H009)', 'H710', 'Promyelocyte');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000219', 'H001', 'CBC (H002 - H009)', 'H711', 'Blast');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000220', 'H001', 'CBC (H002 - H009)', 'H716', 'NRBC');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000221', 'H001', 'CBC (H002 - H009)', 'H901', 'MCV');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000222', 'H001', 'CBC (H002 - H009)', 'H902', 'MCH');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000223', 'H001', 'CBC (H002 - H009)', 'H903', 'MCHC');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000224', 'H001', 'CBC (H002 - H009)', 'H904', 'RDW');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000225', 'H007', 'DIFFERENTIAL', 'H701', 'Neutrophil');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000226', 'H007', 'DIFFERENTIAL', 'H702', 'Lymphocyte');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000227', 'H007', 'DIFFERENTIAL', 'H703', 'Monocyte');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000228', 'H007', 'DIFFERENTIAL', 'H704', 'Eosinophil');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000229', 'H007', 'DIFFERENTIAL', 'H705', 'Basophil');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000230', 'H007', 'DIFFERENTIAL', 'H706', 'Atypical Lymphocyte');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000231', 'H007', 'DIFFERENTIAL', 'H707', 'Band Form Neutrophil');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000232', 'H007', 'DIFFERENTIAL', 'H708', 'Metamyelocyte');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000233', 'H007', 'DIFFERENTIAL', 'H709', 'Myelocyte');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000234', 'H007', 'DIFFERENTIAL', 'H711', 'Blast');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000235', 'H007', 'DIFFERENTIAL', 'H716', 'NRBC');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000236', 'H007', 'DIFFERENTIAL', 'H717', 'Toxic');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000237', 'H007', 'DIFFERENTIAL', 'H718', 'Vacuolization');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000238', 'H008', 'RBC MORPHOLOGY', 'H801', 'Poikilocytosis');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000239', 'H008', 'RBC MORPHOLOGY', 'H802', 'Anisocytosis');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000240', 'H008', 'RBC MORPHOLOGY', 'H803', 'Hypochromia');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000241', 'H008', 'RBC MORPHOLOGY', 'H804', 'Microcytosis');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000242', 'H008', 'RBC MORPHOLOGY', 'H805', 'Macrocytosis');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000243', 'H008', 'RBC MORPHOLOGY', 'H806', 'Schistocyte');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000244', 'H008', 'RBC MORPHOLOGY', 'H807', 'Ovalocyte');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000245', 'H008', 'RBC MORPHOLOGY', 'H808', 'Tear drop cell');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000246', 'H008', 'RBC MORPHOLOGY', 'H809', 'Spherocyte');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000247', 'H008', 'RBC MORPHOLOGY', 'H810', 'Target cell');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000248', 'H008', 'RBC MORPHOLOGY', 'H811', 'Polychromasia');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000249', 'H009', 'RBC INDEX', 'H901', 'MCV');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000250', 'H009', 'RBC INDEX', 'H902', 'MCH');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000251', 'H009', 'RBC INDEX', 'H903', 'MCHC');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000252', 'H018', 'PT', 'H018', 'PT');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000253', 'H018', 'PT', 'H021', 'INR');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000254', 'H018', 'PT', 'H181', 'Mean Normal PT');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000255', 'H019', 'PTT', 'H019', 'PTT');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000256', 'H019', 'PTT', 'H191', 'Mean Normal  APTT');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000257', 'H019', 'PTT', 'H192', 'APTT Ratio');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000258', 'H020', 'TT', 'H020', 'TT');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000259', 'H020', 'TT', 'H201', 'Mean Normal TT');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000260', 'H024', 'Hb Typing', 'H231', 'Hemoglobin A2');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000261', 'H024', 'Hb Typing', 'H232', 'Hemoglobin A');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000262', 'H024', 'Hb Typing', 'H233', 'Hemoglobin F');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000263', 'H024', 'Hb Typing', 'H234', 'Hemoglobin E');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000264', 'H024', 'Hb Typing', 'H235', 'Hemoglobin H');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000265', 'H024', 'Hb Typing', 'H236', 'Hemoglobin Bart s');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000266', 'H024', 'Hb Typing', 'H237', 'Hemoglobin Constant spring');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000267', 'H027', 'BODY FLUID EXAMINATION', 'H260', 'Color');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000268', 'H027', 'BODY FLUID EXAMINATION', 'H261', 'Characteristic');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000269', 'H027', 'BODY FLUID EXAMINATION', 'H262', 'WBC Count');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000270', 'H027', 'BODY FLUID EXAMINATION', 'H263', 'RBC Count');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000271', 'H027', 'BODY FLUID EXAMINATION', 'H264', 'Neutrophil');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000272', 'H027', 'BODY FLUID EXAMINATION', 'H265', 'Lymphocyte');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000273', 'H027', 'BODY FLUID EXAMINATION', 'H266', 'Monocyte');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000274', 'H027', 'BODY FLUID EXAMINATION', 'H267', 'Eosinophil');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000275', 'H027', 'BODY FLUID EXAMINATION', 'H268', 'Mesothelial cell');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000276', 'I008', 'WIDAL S TEST (O,H)', 'I901', 'Widal : O');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000277', 'I008', 'WIDAL S TEST (O,H)', 'I902', 'Widal : H');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000278', 'I009', 'WEIL FELIX TEST {OX2,OX19,OXK}', 'I903', 'Weil Felix : OX-2');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000279', 'I009', 'WEIL FELIX TEST {OX2,OX19,OXK}', 'I904', 'Weil Felix : OX-19');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000280', 'I009', 'WEIL FELIX TEST {OX2,OX19,OXK}', 'I905', 'Weil Felix : OX-K');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000281', 'I051', 'ANA, Titer', 'I961', 'ANA Homogeneous pattern');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000282', 'I051', 'ANA, Titer', 'I962', 'ANA Peripheral pattern');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000283', 'I051', 'ANA, Titer', 'I967', 'ANA Necleolar pattern');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000284', 'I051', 'ANA, Titer', 'I971', 'ANA Speckle');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000285', 'I061', 'TFT:Thyroid Function Test', 'I041', 'T4');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000286', 'I061', 'TFT:Thyroid Function Test', 'I042', 'Free T4');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000287', 'I061', 'TFT:Thyroid Function Test', 'I043', 'T3');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000288', 'I061', 'TFT:Thyroid Function Test', 'I044', 'TSH');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000289', 'I105', 'Anti Cardiolipin (IgA & IgG & IgM)', 'I931', 'Anti Cardiolipin IgG');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000290', 'I105', 'Anti Cardiolipin (IgA & IgG & IgM)', 'I932', 'Anti Cardiolipin IgA');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000291', 'I105', 'Anti Cardiolipin (IgA & IgG & IgM)', 'I933', 'Anti Cardiolipin IgM');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000292', 'I1063', 'ANC PACK (I001,I021,I030,H001,H013,H014,H023)', 'H001', 'Complete Blood Count (CBC)');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000293', 'I1063', 'ANC PACK (I001,I021,I030,H001,H013,H014,H023)', 'H002', 'Wbc');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000294', 'I1063', 'ANC PACK (I001,I021,I030,H001,H013,H014,H023)', 'H003', 'Rbc');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000295', 'I1063', 'ANC PACK (I001,I021,I030,H001,H013,H014,H023)', 'H004', 'Hb');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000296', 'I1063', 'ANC PACK (I001,I021,I030,H001,H013,H014,H023)', 'H005', 'Hct');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000297', 'I1063', 'ANC PACK (I001,I021,I030,H001,H013,H014,H023)', 'H006', 'Platelet');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000298', 'I1063', 'ANC PACK (I001,I021,I030,H001,H013,H014,H023)', 'H007', 'Differential count (DIFF)');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000299', 'I1063', 'ANC PACK (I001,I021,I030,H001,H013,H014,H023)', 'H009', 'RBC INDICIES (MCV, MCH, MCHC)');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000300', 'I1063', 'ANC PACK (I001,I021,I030,H001,H013,H014,H023)', 'H013', 'ABO (SLIDE METHOD)');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000301', 'I1063', 'ANC PACK (I001,I021,I030,H001,H013,H014,H023)', 'H014', 'Rh  GROUP (SLIDE METHOD)');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000302', 'I1063', 'ANC PACK (I001,I021,I030,H001,H013,H014,H023)', 'H023', 'DCIP');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000303', 'I1063', 'ANC PACK (I001,I021,I030,H001,H013,H014,H023)', 'H701', 'Neutrophil');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000304', 'I1063', 'ANC PACK (I001,I021,I030,H001,H013,H014,H023)', 'H702', 'Lymphocyte');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000305', 'I1063', 'ANC PACK (I001,I021,I030,H001,H013,H014,H023)', 'H703', 'Monocyte');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000306', 'I1063', 'ANC PACK (I001,I021,I030,H001,H013,H014,H023)', 'H704', 'Eosinophil');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000307', 'I1063', 'ANC PACK (I001,I021,I030,H001,H013,H014,H023)', 'H705', 'Basophil');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000308', 'I1063', 'ANC PACK (I001,I021,I030,H001,H013,H014,H023)', 'H706', 'Atypical Lymphocyte');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000309', 'I1063', 'ANC PACK (I001,I021,I030,H001,H013,H014,H023)', 'H707', 'Band Form Neutrophil');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000310', 'I1063', 'ANC PACK (I001,I021,I030,H001,H013,H014,H023)', 'H708', 'Metamyelocyte');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000311', 'I1063', 'ANC PACK (I001,I021,I030,H001,H013,H014,H023)', 'H709', 'Myelocyte');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000312', 'I1063', 'ANC PACK (I001,I021,I030,H001,H013,H014,H023)', 'H711', 'Blast');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000313', 'I1063', 'ANC PACK (I001,I021,I030,H001,H013,H014,H023)', 'H716', 'NRBC');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000314', 'I1063', 'ANC PACK (I001,I021,I030,H001,H013,H014,H023)', 'H901', 'MCV');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000315', 'I1063', 'ANC PACK (I001,I021,I030,H001,H013,H014,H023)', 'H902', 'MCH');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000316', 'I1063', 'ANC PACK (I001,I021,I030,H001,H013,H014,H023)', 'H903', 'MCHC');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000317', 'I1063', 'ANC PACK (I001,I021,I030,H001,H013,H014,H023)', 'I001', 'RPR CARD TEST');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000318', 'I1063', 'ANC PACK (I001,I021,I030,H001,H013,H014,H023)', 'I021', 'HBs Ag');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000319', 'I1063', 'ANC PACK (I001,I021,I030,H001,H013,H014,H023)', 'I022', 'HBs Ab');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000320', 'I1063', 'ANC PACK (I001,I021,I030,H001,H013,H014,H023)', 'I030', 'Anti HIV (ปกติ)');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000321', 'I110', 'Anti Neutrophil Cytoplasmic Ab.(ANCA),Screening', 'I935', 'P-ANCA');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000322', 'I110', 'Anti Neutrophil Cytoplasmic Ab.(ANCA),Screening', 'I936', 'C-ANCA');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000323', 'I120', 'CD4 + Lymphocyte', 'I913', '%CD4');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000324', 'I120', 'CD4 + Lymphocyte', 'I914', 'CD4 Count');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000325', 'I135', 'Dengue IgG Ab.', 'I911', 'Dengue IgG : DEN 1 Ab');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000326', 'I135', 'Dengue IgG Ab.', 'I912', 'Dengue IgG : DEN 3 Ab');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000327', 'I161', 'Lupus Anticoagulant (Screening)', 'I945', 'Lupus Screening (LA1)');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000328', 'I161', 'Lupus Anticoagulant (Screening)', 'I946', 'Lupus Screening (LA2)');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000329', 'I161', 'Lupus Anticoagulant (Screening)', 'I947', 'LA1/LA2 ratio');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000330', 'I161', 'Lupus Anticoagulant (Screening)', 'I948', 'Lupus Comment');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000331', 'I176', 'Protein electrophoresis', 'I921', 'Albumin');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000332', 'I176', 'Protein electrophoresis', 'I922', 'Alpha 1 globulin');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000333', 'I176', 'Protein electrophoresis', 'I923', 'Alpha 2 globulin');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000334', 'I176', 'Protein electrophoresis', 'I924', 'Beta globulin');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000335', 'I176', 'Protein electrophoresis', 'I925', 'Gamma globulin');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000336', 'I176', 'Protein electrophoresis', 'I926', 'Total protein');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000337', 'I176', 'Protein electrophoresis', 'I927', 'A/G ratio');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000338', 'I191', 'TORCH', 'I020', 'Rubella IgM, Titer');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000339', 'I191', 'TORCH', 'I133', 'CMV IgM Ab. , Titer');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000340', 'I191', 'TORCH', 'I147', 'HSV IgM Ab.  (Type I&II) , Serum');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000341', 'I191', 'TORCH', 'I163', 'FTA-ABS IgM');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000342', 'I191', 'TORCH', 'I185', 'Toxoplasma IgM');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000343', 'I316', 'Scrub Typhus Ab., Titer', 'I907', 'Scrub Typhus IgM');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000344', 'I316', 'Scrub Typhus Ab., Titer', 'I908', 'Scrub Typhus IgG');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000345', 'I316', 'Scrub Typhus Ab., Titer', 'I909', 'Murine Typhus IgM');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000346', 'I316', 'Scrub Typhus Ab., Titer', 'I910', 'Murine Typhus IgM');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000347', 'I341', 'HBV-DNA Viral load (Quantitative)', 'I941', 'HBV-DNA');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000348', 'I341', 'HBV-DNA Viral load (Quantitative)', 'I942', 'HBV-DNA Log');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000349', 'I343', 'HCV-RNA Viral load (Quantitative)', 'I943', 'HCV-RNA');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000350', 'I343', 'HCV-RNA Viral load (Quantitative)', 'I944', 'HCV-RNA Log');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000351', 'I346', 'HIV RNA Viral load  (Quantitative)', 'I937', 'HIV RNA');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000352', 'I346', 'HIV RNA Viral load  (Quantitative)', 'I938', 'HIV 1 RNA Log');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000353', 'I349', 'PCR for Alpha-thalassemia 1', 'I372', 'Alpha -thalassemia 1 (SEA Type)');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000354', 'I349', 'PCR for Alpha-thalassemia 1', 'I373', 'Alpha-thalassemia 1 (Thai Type)');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000355', 'U001', 'UA (URINALYSIS)', 'U002', 'PHYSICAL AND CHEMICAL EXAMINATION');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000356', 'U001', 'UA (URINALYSIS)', 'U003', 'Specific Gravity');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000357', 'U001', 'UA (URINALYSIS)', 'U004', 'PH');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000358', 'U001', 'UA (URINALYSIS)', 'U005', 'Protein');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000359', 'U001', 'UA (URINALYSIS)', 'U006', 'Sugar');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000360', 'U001', 'UA (URINALYSIS)', 'U007', 'Ketone');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000361', 'U001', 'UA (URINALYSIS)', 'U008', 'Blood');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000362', 'U001', 'UA (URINALYSIS)', 'U009', 'Urobilinogen');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000363', 'U001', 'UA (URINALYSIS)', 'U010', 'Bilirubin');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000364', 'U001', 'UA (URINALYSIS)', 'U011', 'Nitrite');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000365', 'U001', 'UA (URINALYSIS)', 'U012', 'MICROSCOPIC EXAMINATION');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000366', 'U001', 'UA (URINALYSIS)', 'U101', 'Wbc');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000367', 'U001', 'UA (URINALYSIS)', 'U102', 'Rbc');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000368', 'U001', 'UA (URINALYSIS)', 'U103', 'Bacteria');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000369', 'U001', 'UA (URINALYSIS)', 'U104', 'Squamous epithelial');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000370', 'U001', 'UA (URINALYSIS)', 'U105', 'Transitional epithelial');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000371', 'U001', 'UA (URINALYSIS)', 'U106', 'Renal epithelial');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000372', 'U001', 'UA (URINALYSIS)', 'U107', 'WBC Cast');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000373', 'U001', 'UA (URINALYSIS)', 'U108', 'RBC Cast');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000374', 'U001', 'UA (URINALYSIS)', 'U109', 'Coarse granular cast');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000375', 'U001', 'UA (URINALYSIS)', 'U110', 'Fine granular cast');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000376', 'U001', 'UA (URINALYSIS)', 'U111', 'Fatty cast');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000377', 'U001', 'UA (URINALYSIS)', 'U112', 'Hyaline cast');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000378', 'U001', 'UA (URINALYSIS)', 'U113', 'Mix cell cast');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000379', 'U001', 'UA (URINALYSIS)', 'U114', 'Epithelial cast');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000380', 'U001', 'UA (URINALYSIS)', 'U115', 'Heme cast');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000381', 'U001', 'UA (URINALYSIS)', 'U116', 'Mucous thread');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000382', 'U001', 'UA (URINALYSIS)', 'U117', 'Amorphous');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000383', 'U001', 'UA (URINALYSIS)', 'U118', 'Calcium oxalate');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000384', 'U001', 'UA (URINALYSIS)', 'U119', 'Triple posphate');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000385', 'U001', 'UA (URINALYSIS)', 'U120', 'Uric acid');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000386', 'U001', 'UA (URINALYSIS)', 'U121', 'Oval fat body');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000387', 'U001', 'UA (URINALYSIS)', 'U122', 'T. vaginalis');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000388', 'U001', 'UA (URINALYSIS)', 'U123', 'Yeast cells');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000389', 'U001', 'UA (URINALYSIS)', 'U124', 'Pseudohyphae');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000390', 'U001', 'UA (URINALYSIS)', 'U125', 'Spermatozoa');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000391', 'U001', 'UA (URINALYSIS)', 'U126', 'Hippuric acid');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000392', 'U001', 'UA (URINALYSIS)', 'U201', 'Color');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000393', 'U001', 'UA (URINALYSIS)', 'U202', 'Transparency');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000394', 'U012', 'MICROSCOPIC EXAMINATION', 'U101', 'Wbc');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000395', 'U012', 'MICROSCOPIC EXAMINATION', 'U102', 'Rbc');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000396', 'U012', 'MICROSCOPIC EXAMINATION', 'U103', 'Bacteria');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000397', 'U012', 'MICROSCOPIC EXAMINATION', 'U104', 'Squamous epithelial');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000398', 'U012', 'MICROSCOPIC EXAMINATION', 'U105', 'Transitional epithelial');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000399', 'U012', 'MICROSCOPIC EXAMINATION', 'U106', 'Renal epithelial');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000400', 'U012', 'MICROSCOPIC EXAMINATION', 'U107', 'WBC Cast');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000401', 'U012', 'MICROSCOPIC EXAMINATION', 'U108', 'RBC Cast');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000402', 'U012', 'MICROSCOPIC EXAMINATION', 'U109', 'Coarse granular cast');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000403', 'U012', 'MICROSCOPIC EXAMINATION', 'U110', 'Fine granular cast');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000404', 'U012', 'MICROSCOPIC EXAMINATION', 'U111', 'Fatty cast');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000405', 'U012', 'MICROSCOPIC EXAMINATION', 'U112', 'Hyaline cast');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000406', 'U012', 'MICROSCOPIC EXAMINATION', 'U113', 'Mix cell cast');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000407', 'U012', 'MICROSCOPIC EXAMINATION', 'U114', 'Epithelial cast');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000408', 'U012', 'MICROSCOPIC EXAMINATION', 'U115', 'Heme cast');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000409', 'U012', 'MICROSCOPIC EXAMINATION', 'U116', 'Mucous thread');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000410', 'U012', 'MICROSCOPIC EXAMINATION', 'U117', 'Amorphous');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000411', 'U012', 'MICROSCOPIC EXAMINATION', 'U118', 'Calcium oxalate');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000412', 'U012', 'MICROSCOPIC EXAMINATION', 'U119', 'Triple posphate');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000413', 'U012', 'MICROSCOPIC EXAMINATION', 'U120', 'Uric acid');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000414', 'U012', 'MICROSCOPIC EXAMINATION', 'U121', 'Oval fat body');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000415', 'U012', 'MICROSCOPIC EXAMINATION', 'U122', 'T. vaginalis');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000416', 'U012', 'MICROSCOPIC EXAMINATION', 'U123', 'Yeast cells');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000417', 'U012', 'MICROSCOPIC EXAMINATION', 'U124', 'Pseudohyphae');

INSERT INTO b_lab_std (lab_std_id, labgrp, labgrp_name, f_lab_std_id, f_lab_name) VALUES ('101000000000418', 'U012', 'MICROSCOPIC EXAMINATION', 'U125', 'Spermatozoa');

CREATE TABLE b_lab_mapping (
lab_mapping_id varchar(255) NOT NULL
, labgrp varchar(255)
, labgrp_name varchar(255)
, f_lab_std_id varchar(255)
, f_lab_name varchar(255)
, b_item_id varchar(255)
, b_item_lab_group_id varchar(255)
, PRIMARY KEY (lab_mapping_id)
);

ALTER TABLE t_health_family ADD COLUMN health_family_revenue character varying(255) DEFAULT '';

ALTER TABLE b_employee ADD COLUMN record_date_time character varying(255) DEFAULT '';

ALTER TABLE b_employee ADD COLUMN update_date_time character varying(255) DEFAULT '';

ALTER TABLE t_health_grow_history ADD COLUMN grow_result character varying(255) DEFAULT '';

ALTER TABLE t_health_grow_history ADD COLUMN grow_help character varying(255) DEFAULT '';

ALTER TABLE t_health_grow_history ADD COLUMN grow_treat character varying(255) DEFAULT '';

ALTER TABLE t_health_grow_history ADD COLUMN grow_first_check character varying(255) DEFAULT '';

ALTER TABLE t_health_grow_history ADD COLUMN read_story_times_per_wks character varying(255) DEFAULT '';

INSERT INTO s_version VALUES ('9701000000047', '47', 'Hospital OS, Community Edition', '3.9.14', '3.18.280411', '2554-04-28 15:28:00');

INSERT INTO s_script_update_log VALUES ('hospitalOS','updateV3_9_14.sql',(select current_date) || ','|| (select current_time),'ปรับแก้สำหรับ hospitalOS3.9.14');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital_os.objdb;

import com.hospital_os.object.NaranjoInterpretation;
import com.hospital_os.usecase.connection.ConnectionInf;
import com.hospital_os.utility.ComboFix;
import java.sql.ResultSet;
import java.util.Vector;

/**
 *
 * @author Somprasong
 */
@SuppressWarnings("UseOfObsoleteCollectionType")
public class NaranjoInterpretationDB {

    private final ConnectionInf theConnectionInf;

    public NaranjoInterpretationDB(ConnectionInf db) {
        theConnectionInf = db;
    }
    public NaranjoInterpretation selectById(String id) throws Exception {
        String sql = "select * from f_naranjo_interpretation where f_naranjo_interpretation_id = '%s'";
        Vector eQuery = eQuery(String.format(sql, id));
        return (NaranjoInterpretation) (eQuery.isEmpty() ? null : eQuery.get(0));
    }
    
    public Vector<NaranjoInterpretation> listAll() throws Exception {
        String sql = "select * from f_naranjo_interpretation";
        Vector eQuery = eQuery(sql);
        return eQuery;
    }

    public Vector eQuery(String sql) throws Exception {
        Vector list = new Vector();
        ResultSet rs = theConnectionInf.eQuery(sql.toString());
        while (rs.next()) {
            NaranjoInterpretation p = new NaranjoInterpretation();
            p.setObjectId(rs.getString("f_naranjo_interpretation_id"));
            p.naranjo_interpretation_detail = rs.getString("naranjo_interpretation_detail");
            p.max_score = rs.getInt("max_score");
            p.min_score = rs.getInt("min_score");
            list.add(p);
        }
        rs.close();
        return list;
    }

    public Vector<ComboFix> getComboboxDatasources() throws Exception {
        Vector<ComboFix> list = new Vector<ComboFix>();
        Vector<NaranjoInterpretation> listAll = listAll();
        for (NaranjoInterpretation obj : listAll) {
            list.add(new ComboFix(obj.getObjectId(), obj.naranjo_interpretation_detail));
        }
        return list;
    }
}
